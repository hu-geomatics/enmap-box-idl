import sys, os, six, fnmatch, re
import urllib

def file_search(rootdir, wildcard, recursive=False, ignoreCase=False):
    assert rootdir is not None
    if not os.path.isdir(rootdir):
        six.print_("Path is not a directory:{}".format(rootdir), file=sys.stderr)

    results = []

    for root, dirs, files in os.walk(rootdir):
        for file in files:
            if (ignoreCase and fnmatch.fnmatch(file.lower(), wildcard.lower())) \
                    or fnmatch.fnmatch(file, wildcard):
                results.append(os.path.join(root, file))
        if not recursive:
            break
    return results


jp = os.path.join
mkdir = lambda p : os.makedirs(p, exist_ok=True)



def migrate_wiki_images():

    dir_wiki = r'C:\Users\geo_beja\Repositories\wiki_new'
    dir_img = r'C:\Users\geo_beja\Repositories\wiki_new\img\migrated'

    pages = file_search(dir_wiki, '*.wiki', recursive=True)
    matchImgLink = re.compile(r'(?<={{)https://bitbucket.org/repo/86neRy[^|]+(?=[|])')

    if False: #download images from old repository

        for page in pages:
            with open(page, 'r', encoding='utf') as f:
                lines = f.readlines()

            for line in lines:
                for url in matchImgLink.findall(line):
                    bn = url.split('/')[-1]

                    path_to_store = jp(dir_img, bn)
                    if True and os.path.exists(path_to_store):
                        print('File already downloaded: {}'.format(path_to_store))
                    else:
                        print('Download {}'.format(url))
                        result = urllib.request.urlretrieve(url,path_to_store)

                    s = ""

    if True: #replace pages
        for page in pages:
            with open(page, 'r', encoding='utf') as f:
                lines = f.readlines()

            for i in range(len(lines)):
                line = lines[i]
                #change path to new, relative path
                lines[i] = re.sub('https://bitbucket.org/repo/86neRy/images', 'img/migrated', lines[i])

            with open(page, 'w', encoding='utf') as f:
                f.writelines(lines)
                s = ""







if __name__ == '__main__':
    migrate_wiki_images()
    print('Done')