import importlib, sys

def test_package_import(module):
    assert type(module) is str

    try:
        importlib.import_module(module)
    except:
        print('Failed to import module: {}'.format(str(module)))
        return False
    return True

requiredModules = ['numpy','idlpy']

def python_bridge_check():
    errors = list()
    for module in requiredModules:
        if not test_package_import(module):
            errors.append(module)
    if len(errors) > 0:
        errors.insert(0, 'Python Distribution '+sys.version + '\n misses modules to run IDL_Python Bridge:')
        raise Exception('\n'.join(errors))
    else:

        print('Python Distribution seems to be ok: ' + sys.version)

if __name__ == "__main__":
    python_bridge_check()
