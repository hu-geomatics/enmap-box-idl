from osgeo import gdal, ogr, osr
import numpy as np

def _get_raster_formats():

    drivers = [gdal.GetDriver(i) for i in range(gdal.GetDriverCount)]
    pass



def gdal_translate(pathSrc, pathDst, drvDst, options=None):

    print(pathSrc, pathDst, drvDst)
    drv = gdal.GetDriverByName(drvDst)
    dsSrc = gdal.Open(pathSrc)
    dsDst = drv.CreateCopy(pathDst, dsSrc, options=options)

    if dsDst is None:
        raise Exception('Unable to write {}'.format(pathDst))
    else:
        dsDst.SetProjection(dsSrc.GetProjection())
        trans = dsSrc.GetGeoTransform()
        if trans is not None and len(trans) == 6:
            dsDst.SetGeoTransform(trans)
        dsDst.FlushCache()
        dsDst = None
    return dsDst


if __name__ == "__main__":

    pathsrc = r'C:\Users\geo_beja\Repositories\enmap-box_svn\trunk\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Image'
    pathdst = r'C:\Users\geo_beja\AppData\Local\Temp\tmp.png'

    drvdst = 'ENVI'
    gdal_translate(pathsrc, pathdst ,drvdst)