imageSVM version 3

Copyright: Humboldt-Universit�t zu Berlin

The imageSVM software was developed at Humboldt-Universit�t zu Berlin, Geography Department, Geomatics Lab.

The authors of this software tool accept no responsibility for errors or omissions in this work and shall not be liable for any damage caused by these.


Programming: Andreas Rabe
Concept: Sebastian van der Linden and Andreas Rabe.

Homepage: www.hu-geomatics.de

Contact (technical support): Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
Contact (other queries): Sebastian van der Linden (sebastian.linden@geo.hu-berlin.de)

License and Disclaimer
see <EnMAP-Box installation folder>\enmapProject\applications\imageSVM\copyright\licence_imageSVM.txt

Third Parties

1. Coyote Library (hosted at http://idl-coyote.googlecode.com) developed by David Fanning (http://www.idlcoyote.com). All Coyote Library routines use the BSD Open Source License 
(see <EnMAP-Box installation folder>\enmapProject\lib\coyote\copyrights\license.txt).

2. Source code documentation was created by using IDLdoc (http://idldoc.idldev.com) developed by Michael Galloy (http://michaelgalloy.com). IDLdoc is released under a BSD-type license 
(see <EnMAP-Box installation folder>\enmapProject\lib\IDLdoc\copyrights\license.txt).

3. LIBSVM (http://www.csie.ntu.edu.tw/~cjlin/libsvm) developed by Chih-Chung Chang and Chih-Jen Lin, Taiwan. Please note the copyright notice at
http://www.csie.ntu.edu.tw/~cjlin/libsvm/COPYRIGHT.
