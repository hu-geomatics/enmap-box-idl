# autoPLSR changelog
#--------------------

2012-11-27
1.11
- Adapted to EnMap-Box 1.4

2012-10-24
1.10
- Fixed brightness normalization
- several bugfixes

2012-10-19
1.09
- Adapted to R version
- Fixed a lot of bugs

1.0.0 Final Version released

2012-07-04
1.0.0rc5 Release candidate 5 released
- Fixed error withn scaled data in influence plot

2012-03-20
1.0.0rc4 Release candidate 4 released

2012-03-12
1.0.0rc3 Release candidate 3 released

2012-03-05
1.0.0rc2 Release candidate 2 released

2012-03-01
1.0.0rc1 Release candidate 1 released