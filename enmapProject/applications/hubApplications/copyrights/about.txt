﻿hubApplications version 1.1

Copyright: Humboldt-Universität zu Berlin, 2012

hubApplications is a collection of EnMAP-Box applications developed at Humboldt-Universität zu Berlin, Geography Department, Geomatics Lab. 

It includes the following applications:
AccuracyAssessment Accuracy - (stratified) metrics for classification / regression results 
ImageScaling                - linear scaling and z-transformation
ImageStatistics             - (stratified) metrics of images, stratified
ImageSubsetting             - spectral and spatial image subsets 
LeastSquaresRegression      - train and use standard LSR models
Masking                     - creates masked images
RandomSampling              - (stratified) random sampling 
ScatterPlot                 - (stratified) 2D scatter plots / 2D histograms 
Stacking                    - stacking of images / image bands

Programming: Benjamin Jakimow and Andreas Rabe
Concept: Andreas Rabe, Benjamin Jakimow and Sebastian van der Linden

Homepage
EnMAP Project:    http://www.enmap.org/
EnMAP-Box Portal: http://indus.caf.dlr.de/forum/
HU Geomatics Lab: www.geographie.hu-berlin.de/abteilungen/geomatik

Contact
Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
Andreas Rabe (andreas.rabe@geo.hu-berlin.de)

Disclaimer
The authors of this software tool accept no responsibility for errors or omissions in this work and shall not be liable for any damage caused by these.

Third Parties

1. Coyote Library (hosted at http://idl-coyote.googlecode.com) developed by David Fanning (http://www.idlcoyote.com). All Coyote Library routines use a BSD Open Source License 
(see <EnMAP-Box installation folder>\enmapProject\lib\coyote\copyrights\license.txt).

2. Source code documentation was created by using IDLdoc (http://idldoc.idldev.com) developed by Michael Galloy (http://michaelgalloy.com). IDLdoc is released under a BSD-type license 
(see <EnMAP-Box installation folder>\enmapProject\lib\IDLdoc\copyrights\license.txt).