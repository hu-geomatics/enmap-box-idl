;+
; :Private:
; :Hidden:
; 
; :Description:
;    Writes information about the model to the volume.
;    The informations are saved in the CSV-format.
;
; :Params:
;    settings
;    oobError
;    fn_model
;
; 
;-
PRO imageRF_writeTableOob $
  ,settings $
  ,oobError $
  ,fn_model


  case settings.rfType of 
    'RFR' : begin
          textString = $
            ['Image Filename: ' + settings.inputImage $
            ,'Training Data: ' + settings.inputLabels $
            ,'Number of Trees: ' + STRING(9B) + STRING(settings.numberOfTrees) $ 
        ;    ,'Number of Classes: ' + STRING(9B) + STRING(settings.numberOfClasses) $
            ,'Number of Features: ' + STRING(LONG(settings.numberOfFeatures)) $
            ,'Number of Bands: ' + STRING(settings.numberOfBands) $
        ;    ,'Impurity-Function: ' + impurityText $
            ]
          
          titleText = 'Out-Of-Bag-Error'
          yTitle = 'Error'
          oobError = SQRT(oobError)
          yrange=[FLOOR(MIN(oobError)),CEIL(MAX(oobError))]
          fn_table = FILE_DIRNAME(fn_model,/MARK_DIRECTORY) + FILE_BASENAME(fn_model,'.rfr') 
            end
     'RFC': begin
              case settings.impurityFunction of
                'gini'  : impurityText = 'Gini-Coefficient'
                'entropy'  : impurityText = 'Entropy'
                else : message, 'wrong impurity function'
              endcase
              textString = $
                ['Image Filename: ' + settings.inputImage $
                ,'Training Data: ' + settings.inputLabels $
                ,'Number of Trees: ' + STRING(9B) + STRING(settings.numberOfTrees) $ 
                ,'Number of Classes: ' + STRING(9B) + STRING(settings.numberOfClasses) $
                ,'Number of Features: ' + STRING(LONG(settings.numberOfFeatures)) $
                ,'Number of Bands: ' + STRING(settings.numberOfBands) $
                ,'Impurity-Function: ' + impurityText $
              ]
              
              titleText = 'Out-Of-Bag-Accuracy'
              yTitle = 'Accuracy [%]'
              yrange = [0D,100D]
              fn_table = FILE_DIRNAME(fn_model,/MARK_DIRECTORY) + FILE_BASENAME(fn_model,'.rfc')
            end
  endcase
 
  WRITE_CSV, fn_table+'.csv', TRANSPOSE(oobError), HEADER=titleText, TABLE_HEADER=textString

END

;+
; :Private:
; :Hidden:
; 
; :Description:
;    Computes the index of the out-of-bag-data.
;
; :Params:
;    remove
;    totalClassSamples
;
;-
FUNCTION imageRF_ComputeOobIndex $
  ,remove $
  ,totalClassSamples

  !except=0
  
  oob_index = ULINDGEN(totalClassSamples)
  keep = WHERE(HISTOGRAM(remove,MIN=0,MAX=totalClassSamples-1) eq 0,count)
  IF count ne 0 then oob_index=oob_index[keep]
  RETURN, oob_index
END


;+
; :Description:
;    Returns a struct with features and corresponding labels.
;    
;    Result: `{features:finalFeatures, labels:finalLabels}`
;    
; :Params:
;    imgFeatures: in, required, type = hubIOImgInputImage()
;    imgLabels: in, required, type = hubIOImgInputImage()
;
; :Keywords:
;    imgMask: in, optional, type = hubIOImgInputImage()
;     A mask image.
;    
;    pBar: in, optional, type = hubProgressBar
;     A progress bar
;
; :Examples:
;
;-
function imageRF_parameterize_getFeaturesAndLabels, imgFeatures, imgLabels, imgMask=imgMask, pBar=pBar
    
  nBands = imgFeatures.getMeta('bands')
  nSamples = imgFeatures.getMeta('samples')
  nLines   = imgFeatures.getMeta('lines')
  if isa(pBar) then begin
    pBar.setInfo, 'Read features and labels...'
    pBar.setRange, [0l, long(nLines) * nSamples]
  endif
  
  divFeatures = imgFeatures.getMeta('data ignore value')
  divLabels = imgLabels.isClassification() ? 0b : imgLabels.getMeta('data ignore value')
  
  tileLines = hubIOImg_getTileLines(nSamples, nBands+3,  imgLabels.getMeta('data type'))
  imgFeatures.initReader, tileLines, /TileProcessing, /Slice
  imgLabels.initReader, tileLines, /TileProcessing, /Slice
  if isa(imgMask) then imgMask.initReader, tileLines, /TileProcessing, /Slice, /Mask
  
  finalFeatures = []
  finalLabels   = []
  pixelsDone = 0l
  while ~imgFeatures.tileProcessingDone() do begin
    featureData = imgFeatures.getData()
    labelData   = imgLabels.getData()
    
    nPixels = n_elements(labelData)
  
    mask = make_array(nPixels, /Byte, value=1)
    ;exclude features with a data ignore value
    if isa(divFeatures) then begin
      for iB = 0, nBands-1 do begin
        mask and= featureData[iB, *] ne divFeatures
      endfor  
    endif
    
    ;exclude features without corresponding label
    if isa(divLabels) then begin
      mask and= labelData ne divLabels
    endif 
    
    ;exclude explicitely masked features 
    if isa(imgMask) then mask and= imgMask.getData()
    
     
    iValid = where(mask ne 0, /NULL)
    if isa(iValid) then begin
      finalFeatures = [[temporary(finalFeatures)], [featureData[*,iValid]]]
      finalLabels = [[temporary(finalLabels)], [labelData[*,iValid]]]
    endif 
    
    nLabels = n_elements(finalLabels) 
    if nLabels gt 100000l then begin
      
      info = [string(format='(%"Your training data set has large number of samples (%i).")', nLabels) $
             ,'Depending on your machine this can lead to memory problems.' $
             ,'Are you sure to continue?'] 
             
      result = STRUPCASE(DIALOG_MESSAGE(info, /QUESTION))
      if result ne 'YES' then begin
        message, 'Parameterization aborted'
      endif
    endif
    
    pixelsDone += nPixels
    if isa(pBar) then pBar.setProgress, pixelsDone
    
  endwhile
  
  imgFeatures.finishReader
  imgLabels.finishReader
   return, {features:finalFeatures, labels:finalLabels}
end

;+
; :Description:
;    This routine starts the parameterization of the random forests and writes
;    the model to the volume. Information about the model are also
;    saved, it includes the filenames of the image and the training data, 
;    the number of bands and features and the used impurity function. 
;    The model is saved as IDL SAVE file.
;    
; :Examples:
;    This example shows how to parameterize a random forest classification model using the 
;    default parameterization. After the model was build, its parameters will be shown with 
;    `imageRF_viewModel_processing`::
;       
;        parameters=Hash()
;        parameters['rfType'] = 'RFC'   
;        parameters['inputImage'] = hub_getTestImage('Hymap_Berlin-A_Image')
;        parameters['inputLabels'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
;        parameters['model']   = DIALOG_PICKFILE(Title='Please set your model file', /WRITE)
;        parameters['numberOfFeatures'] = round(SQRT(114))
;        parameters['impurityFunction'] = 'gini'
;        ;calculate the model
;        imagerf_parameterize_processing, parameters
;        ;show parameterization
;        imageRF_viewModel_processing, parameters
;        print, 'Example done!'
;
; :Params:
;    PARAMETERS : in, required, type=string
;       A hash that contains the following parameters (* = optional)::
; 
;           hash key                 | type    | description
;         ---------------------------+---------+----------------------------------------------------------------
;           'inputImage'             | string  | path of spectral image file
;           'inputLabels'            | string  | path of label file
;           'model'                  | string  | path of random forest model (usually *.rfc or *.rfr) 
;         * 'rfType'                 | string  | type of random forest algorithm {'RFC' or 'RFR'}
;                                    |         | the default value is derived fom the inputLabel image
;         * 'numberOfFeatures'       | int     | number of features used to find the best split in an node
;                                    |         | default = sqrt(number of bands)
;         * 'minNumberOfSamples'     |         | Minimum number of samples to stop splitting. 
;                                    |         | min=1, default = 1                        
;         * 'numberOfTrees'          | int     | number of trees to grow random forests, default=100                   
;         * 'impurityThreshold'      | double  | [0.0,1.1] Minimum impurity to stop splitting, default=0.0 
;         * 'categoricalArray'       | byte[]  | Array has the same number of fields as number of bands. 
;                                    |         | 1 = categorical variable, 0 = numerical variable (default)
;         * 'tag'                    |         | A variable that is to be tagged to the RF model. Default: empty hash() 
;                                    
;         classification specific settings (rfType = 'RFC')
;         ---------------------------------------------------
;         * 'impurityFunction'       | string  | Impurity function to determine the impurity. 
;                                    |         | 'gini' = Gini index (default), 'entropy' = Entropy
;         * 'writeProbabilityImage'  | {0 | 1} | adds the ruleimage in case of a classification
;                                    |         | default = 0 = no rule image
;                                    
;         regression specific settings (rfType = 'RFR')
;         ---------------------------------------------------
;         * 'dataIgnoreValue'        | int     | data ignore value / background value in the output file
;                                    |         | default = -999  
;         ---------------------------+---------+----------------------------------------------------------------
;
;    GUISettings : in, optional, type=hash
;    
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          noShow      | int  | set this to avoid showing a progress bar
;          ------------+------+-----------------
;          
; :uses:
;     Uses the following enmap-routines::
;       `hubApp_accuracyAssessment_dataprocessing_classification`
;       `hubApp_accuracyAssessment_dataprocessing_dataprocessing`
;       `ComputeOobIndex`
;       `imageRF_computeErrorRegression`
;       `imageRF_computeErrorRegression`
;       `imageRF_writeTableOob`
;     
;-
PRO imageRF_Parameterize_Processing, parameters, GUISettings $
    , groupLeader = groupLeader $
    , NoShow = Noshow
  imageRFVersion = '2.0.1'
  
  HELPER = hubHelper()
  
  if ~isa(GUISettings) then GUISettings = hash()
  ;backward compatibility
  if isa(groupLeader) then GUISettings['groupLeader'] = groupLeader 
  if isa(noShow) then GUISettings['noShow'] = noShow
  
  
  ;check parameters
  ;required / non-optional keywords
  required = ['model', 'inputImage', 'inputLabels']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
 
 rfType = parameters.hubGetValue('rfType')
 if ~isa(rfType) then begin
  inputLabels =  hubIOImgInputImage(parameters['inputLabels'])
  rfType = inputLabels.isClassification() ? 'RFC' : 'RFR'
  inputLabels.cleanup 
 endif
   
 inputImage = hubIOImgInputImage(parameters['inputImage'])
 case rfType of
    'RFR' : inputLabels = hubIOImgInputImage(parameters['inputLabels'],/REGRESSION)
    'RFC' : inputLabels = hubIOImgInputImage(parameters['inputLabels'],/CLASSIFICATION )
 endcase
 if ~total(rfType eq ['RFC','RFR'],/INTEGER) then message, 'Type of random forests is not valid!!' 
 
 if parameters.hubIsa('inputMask') then inputMask = hubIOImgInputImage(parameters['inputMask'], /Mask)
 nBands   = inputImage.getMeta('bands')
 datatype = inputImage.getMeta('data type')
 bandNames = inputImage.hasMeta('band names') ? inputImage.getMeta('band names') : 'Band '+strtrim(indgen(nBands)+1, 2)
  
  impurityFunction = ''
  case rfType of
    'RFC' : begin
              extension = ['.rfc']
              impurityFunction = parameters.hubGetValue('impurityFunction', default='gini')
              IF ~TOTAL(STRCMP(impurityFunction,['gini','entropy'], /FOLD_CASE),/INTEGER) THEN begin
                    message, 'Impurity function is not valid!!'
              endif
            end
    'RFR' : begin
              extension = ['.rfc']
              impurityFunction = 'regression'
            end
    else : message, "rfType is unknown. Choose 'RFR' or 'RFC'"
  endcase
  
  if  (FILE_INFO(parameters['model']+extension)).exists and ~(FILE_INFO(parameters['model']+extension)).WRITE THEN message, 'Model is not writable!!'
  if ~(FILE_INFO(parameters['inputImage'])).EXISTS then message, "Image doesn't exist!!"
  if ~(FILE_INFO(parameters['inputLabels'])).EXISTS then message, "Labeled reference doesn't exist!!"
  
  nTrees = parameters.hubGetValue('numberOfTrees', default=100)
  if nTrees lt 1 || nTrees gt 60000 then message, 'Number of trees is not valid!!'
  
  minNumberOfSamples = parameters.hubGetValue('minNumberOfSamples', default=1)
  if minNumberOfSamples lt 1 || minNumberOfSamples gt 60000 then message, 'Minimum number of samples is not valid!!'
  
  impurityThreshold = parameters.hubGetValue('impurityThreshold', default=0d)
  if impurityThreshold lt 0d || impurityThreshold gt 1d then message, 'Minimum impurity threshold is not valid!!'
  
  nFeatures = parameters.hubGetValue('numberOfFeatures', default=fix(sqrt(nBands)))
  nFeatures = fix(round(nFeatures))
  if nFeatures le 0 || nFeatures gt nBands then begin
    message, 'Number of features must be in range [0, number of bands]
  endif
  
  progressBarInfo = ['Model:     ' + parameters['model'] $
                    ,'Image:     ' + parameters['inputImage'] $
                    ,'Reference: ' + parameters['inputLabels'] $
                    ]
  progressBar = hubProgressBar( $
            title = (parameters.hubIsa( 'title')) ? parameters['title'] : !NULL $
          , info = progressBarInfo $
          , GroupLeader = GUISettings.hubgetValue('groupLeader') $
          , NoShow = GUISettings.hubgetValue('noShow'))
  
  
  
 

 
 ; Load training data
 trainingData = imageRF_parameterize_getFeaturesAndLabels( $
                            inputImage, inputLabels $
                            ,IMGMASK=inputMask,PBAR=progressBar) 
                            

  
 features = trainingData.features
 dependentVariable = trainingData.labels
 !NULL = temporary(trainingData)                           
 

  
  CASE rfType OF
    'RFR' :   BEGIN
                numberOfClassSamples = N_ELEMENTS(dependentVariable)
                referenceClasses = REPLICATE(1B,numberOfClassSamples)
                class_names = ''
                lookup = 0
                numberOfClasses = 1
                totalClassSamples = numberOfClassSamples
                aprioriProbs = 1
                ; Oob-Error
                regressionCount = INTARR(totalClassSamples)
                meanDependent = MEAN(dependentVariable)
                dataIgnoreValue = parameters.hubGetValue('dataIgnoreValue', default=inputLabels.getMeta('data ignore value'))
              END
    'RFC' : BEGIN
              numberOfClasses = inputLabels.getMeta('classes')
              maxLabel = numberOfClasses-1
              class_names = inputLabels.getMeta('class names')
              lookup      = inputLabels.getMeta('class lookup')
              
              referenceClasses = byte(dependentVariable)
              if max(referenceClasses) gt numberOfClasses-1 then begin
                iValid = where(referenceClasses ge 0 and referenceClasses le maxLabel $
                  , nValid,NCOMPLEMENT=nFalse,  /NULL)
                if nValid gt 0 then begin
                  
                  info = ['Reference image '+parameters['inputLabels'] $
                         , string(format='(%"contains %i pixels out of valid range [0,%i]")', nFalse, maxLabel) $
                         ,'These pixels will be not considered for model training' ]
                  
                  !NULL = DIALOG_MESSAGE(strjoin(info, string(10b)), /INFORMATION)
                   
                  referenceClasses = referenceClasses[iValid]
                  features = features[*, iValid]
                endif else begin
                  message, string(format='(%"Could not found any label pixel value within valid range of [0,%i]")', maxLabel)
                endelse
              endif
              
              numberOfClassSamples = HISTOGRAM(referenceClasses,MIN=1B,MAX=numberOfClasses-1)
              
              totalClassSamples = TOTAL(numberOfClassSamples, /INTEGER)
              aprioriProbs = DOUBLE(numberOfClassSamples[*]) / totalClassSamples
              dataIgnoreValue = 0
          END
    ELSE : message, 'rfType is unknown'
  ENDCASE
  
 inputImage.cleanup
 inputlabels.cleanup
 if isa(inputMask) then inputMask.cleanup
  
  if parameters.hubIsa('categoricalArray') then begin
    categoricalArray = parameters['categoricalArray']
    if n_elements(categoricalArray) ne nBands then message, "number of elements in 'categoricalArray' must be equal to number of bands of input image!"
  endif else begin
    ;use the default setting: all input variables are continuous numeric 
    categoricalArray = MAKE_ARRAY(nBands, VALUE=0, /BYTE)
  endelse
  
    
  
  
  ;---------------------------------------------
  ; Erschaffen sonstiger Matrizen und Variablen
  ;---------------------------------------------
 
  if isa(progressBar) then begin
    progressBar.setInfo, [progressBarInfo, 'Train Random Forests...']
    progressBar.setProgress, 0
    progressBar.setRange, [0, nTrees -1]
  endif
  seed = SYSTIME(1)
  !NULL = RANDOMU(seed,1)
  
  RF_Model = Hash()
  
  rfSettings = { $
    numberOfFeatures : nFeatures $
    ,totalClassSamples : totalClassSamples $
    ,numberOfClassSamples : numberOfClassSamples $
    ,aprioriProbs : aprioriProbs $
    ,datatype : datatype $
    ,numberOfClasses : numberOfClasses $
    ,numberOfBands : nBands $
    ,impurityFunction : impurityFunction $
    ,minNumberOfSamples : minNumberOfSamples $
    ,impurityThreshold : impurityThreshold $
    ,randomSeed : seed $
    ,rfType : rfType $
    ,categoricalArray :  categoricalArray $
    ,dataIgnoreValue : dataIgnoreValue $
  }

  ;retrieve Data Ignore Value
  
 
  ; Array für CARTs erzeugen
  ;--------------------------
  rfModelSplitpointPtrArr = PTRARR(nTrees)
  rfModelSplitvariablePtrArr = PTRARR(nTrees)
  rfModelNodeclassPtrArr = PTRARR(nTrees)
  ; OOB-Fehler
  ;-----------
  ; Array für Pixeldaten der OOB-Punkte
  oobArray = ULONARR(totalClassSamples,numberOfClasses)
  ; Variable für # falsch klassifizierte Punkte
  oobError = FLTARR(nTrees)
  hubOOBPerformances = list(length=nTrees)
  ;
  
  ;----------------
  ; Random Forests
  ;----------------
  ptr_features = PTR_NEW(features)
  ptr_classes = PTR_NEW(referenceClasses)
  ptr_dependentVariable = PTR_NEW(dependentVariable)
  ;---------------------
  ; Variable Importance
  ;---------------------
  oobIndex_arr = PTRARR(nTrees)
  errorrate = FLTARR(nTrees)
  
  ; parameterization
  ;------------------
  useStratifiedSampling = rfType eq 'RFC' && ~keyword_set(DisableStratifiedSampling)
  
  FOR ntree=0,nTrees-1 DO BEGIN
    
    ; Bootstrap-Trainingsdaten
    seed = rfsettings.randomSeed
    
    
    ;stratified proportional sampling
    if useStratifiedSampling then begin
      bootstrapIndex = make_array(totalClassSamples, /ULONG)
      i1 = 0
      foreach nCS, numberOfClassSamples, iClass do begin
        i2 = i1 + nCS - 1
        if nCS gt 0 then begin
          classIndices = where(referenceClasses eq iClass+1)
          bootstrapIndex[i1:i2] = classIndices[ULONG(nCS * RANDOMU(seed, nCS))]  
        endif
        i1 = i2+1
      endforeach
      
    endif else begin
      bootstrapIndex = ULONG(totalClassSamples * RANDOMU(seed, totalClassSamples))
    endelse
    
    rfsettings.randomSeed = seed
    
    rfModelSplitpointPtrArr[ntree] = PTR_NEW([])
    rfModelSplitvariablePtrArr[ntree] = PTR_NEW([])
    rfModelNodeclassPtrArr[ntree] = PTR_NEW([])
    
    ; Baum erzeugen
    imageRF_createrootnode, ptr_features, ptr_classes, ptr_dependentVariable, $
      bootstrapIndex, rfSettings, rfModelSplitpointPtrArr[ntree], rfModelSplitvariablePtrArr[ntree], rfModelNodeclassPtrArr[ntree]

    ; compute OOB-Error
    ;-------------------
    oobIndex_arr[ntree] = PTR_NEW(imageRF_ComputeOobIndex(bootstrapIndex, totalClassSamples))

    lfnr = 0
    imgsize = [N_ELEMENTS((*ptr_features)[0,*]),1]
    CASE rfType OF
      'RFR' : ptr_oobArray = PTR_NEW(DBLARR(imgsize[0], imgsize[1])) 
      'RFC' : ptr_oobArray = PTR_NEW(INTARR(imgsize[0], numberOfClasses))
      ELSE : message, 'rfType unknown'
    ENDCASE
    imageRF_Ruling,lfnr,ptr_features,ptr_oobArray,*oobIndex_arr[ntree],rfType, imgsize, categoricalArray, rfModelSplitpointPtrArr[ntree], rfModelSplitvariablePtrArr[ntree], rfModelNodeclassPtrArr[ntree]
    
    oobArray = TEMPORARY(oobArray) + *ptr_oobArray
    
    CASE rfType OF
      'RFR' : BEGIN
                regressionCount[*oobIndex_arr[ntree]]++
                meanOobArray = oobArray / regressionCount
                finiteValues = WHERE(~FINITE(meanOobArray),count)
                IF count gt 0 THEN meanOobArray[finiteValues] = meanDependent
                oobError[ntree] = imageRF_computeErrorRegression(meanOobArray, dependentVariable,hubOOBPerformance=hubOOBPerformance)
                errorrate[ntree] = imageRF_computeErrorRegression((*ptr_oobArray)[*oobIndex_arr[ntree]], dependentVariable[*oobIndex_arr[ntree]])                
                hubOOBPerformances[nTree] = hubOOBPerformance
              END
      'RFC' : BEGIN
                oobError[nTree] = imageRF_computeErrorClassification(oobArray, referenceClasses $
                                  , CONFUSIONMATRIX=confusionmatrix $
                                  , hubOOBPerformance=hubOOBPerformance)
                errorrate[ntree] = imageRF_computeErrorClassification((*ptr_oobArray)[*oobIndex_arr[ntree],*], referenceClasses[*oobIndex_arr[ntree]])
                
                hubOOBPerformances[nTree] = hubOOBPerformance
              END
      ELSE : message, 'rfType unknown'
      
   ENDCASE
    
    ; Aufräumen
    PTR_FREE, ptr_oobArray
    if isa(progressBar) then progressBar.setProgress, ntree
  ENDFOR
  
  ;Save Model
  ;------------

  settings = { $
    numberOfTrees : nTrees $
    ,bandnames: bandNames $
    ,numberOfClasses : numberOfClasses $
    ,namesOfClasses : class_names $
    ,totalClassSamples : totalClassSamples $
    ,lookup : lookup $
    ,numberOfFeatures :nFeatures $
    ,numberOfBands : nBands $
    ,impurityFunction : impurityFunction $
    ,rfType : rfType $
    ,inputImage : parameters['inputImage'] $
    ,inputLabels : parameters['inputLabels'] $
    ,datatype : size(features, /Type) $
    ,datatype_y : size(dependentVariable, /Type) $
    ,categoricalArray : categoricalArray $
    ,dataIgnoreValue:dataIgnoreValue $
  }
  
  
  tag = parameters.hubgetValue('tag', default=Hash())
  
  IF rfType eq 'RFR' THEN BEGIN
    
    SAVE, rfModelSplitpointPtrArr, rfModelSplitvariablePtrArr, rfModelNodeclassPtrArr, settings, $
      oobError, oobIndex_arr, features, errorrate, hubOOBPerformances, dependentVariable, $
      Filename = parameters['model'], tag, /COMPRESS
    enmapBox_openFile, parameters['model']
  ENDIF ELSE BEGIN
    matrix = confusionmatrix
    SAVE, rfModelSplitpointPtrArr, rfModelSplitvariablePtrArr, rfModelNodeclassPtrArr $
        , settings, oobError, hubOOBPerformances  $
        , matrix, oobIndex_arr, features, errorrate, referenceClasses $
        , Filename = parameters['model'], tag, /COMPRESS
    enmapBox_openFile, parameters['model']
  ENDELSE
  
  ; Cleanup everything
  PTR_FREE $
    ,ptr_features $
    ,ptr_classes $
    ,ptr_dependentVariable $
    ,oobIndex_arr $
    ,rfModelSplitpointPtrArr $
    ,rfModelSplitvariablePtrArr $
    ,rfModelNodeclassPtrArr
  
  
  imageRF_writeTableOob, settings, oobError, parameters['model']    
  if isa(progressBar) then progressBar.cleanup

END

pro test_imageRF_parameterize_processing, model=model, rfType=rfType
  p = dictionary()
  ;rfType = 'RFC'
  if ~isa(rfType) then rfType = 'RFR'
  p['numberOfFeatures'] = sqrt(114)
  p['numberOfTrees'] = 50
  if rfType eq 'RFC' then begin
    if 0b then begin
      p['inputImage']  = hub_getTestImage('Hymap_Berlin-A_Image')
      p['inputLabels'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
    endif 
      
    
    p['model'] = FILEPATH('model.rfc',/TMP) 
    guiSettings = Hash('noShow', 1b, 'noOpen', 1b)
    
    
    
  endif else begin
    if 1b then begin
    p['inputImage'] = hub_getTestImage('Hymap_Berlin-B_Image')
    p['inputLabels'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
    p['inputMask'] = hub_getTestImage('Hymap_Berlin-B_Mask')
    endif
    
    if 0b then begin
      p['inputImage']  = 'G:\temp\temp_bj\malicious_testdata\feature_EnMAP_E02_subset_scaled.bsq'
      p['inputLabels'] = 'G:\temp\temp_bj\malicious_testdata\label_fractions30m_masked.bsq'
    endif

    p['model'] = FILEPATH('model.rfr',/TMP)
    
  endelse
  
  imageRF_parameterize_processing, p, guiSettings
  
  
  report = imageRF_viewModel_getReport(p)
  report.saveHTML, /Show
  
end
