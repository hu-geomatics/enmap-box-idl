;+
; :Description:
;    This function is used to perform a consistency check of values. It is used within 
;    the `imageRF_parameterize_getParameters` function.
;
; :Params:
;    resultHash, in required, type=hash
;
; :Keywords: 
;    Message, out, required, type=string[]
;       This keyword returns an array containing error messages.
;
;-
FUNCTION imageRF_parameterize_getParameters_consistency_check $
  , resultHash $
  , Message=message
  
  message = list()
  message = "Number of features must be less than number of bands."
  
  
  if resultHash['numberOfFeaturesSelection'] eq 2 then begin
    numberOfBands = (hubIOImgInputImage((resultHash['inputSampleSet'])['featureFilename'])).getMeta('bands')
    isConsistent = resultHash['numberOfFeatures'] le numberOfBands
  endif else begin
    isConsistent = 1B
  endelse
  return, isConsistent
END

;+
; :Description:
;    This function opens a widget dialog to collect the parameter values required to
;    parameterize a random forest model. The parameters are returned in a hash
;    that can be used as argument for `imageRF_parameterize_processing`. 
;    
;    parameters : in, required, type=hash
;     A hash containing the following values::
;       
;       hash key | type   | description
;       ---------+--------+------------
;       rfType   | string | type of RF: RFC or RFR
;       ---------+--------+------------
;       
;     This hash can also be used to provide default values.
;     
;    GUISettings : in, optional, type=hash
;    
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          ------------+------+-----------------
;          
; :returns: 
;    This function returns a hash that contains the selected parameter keys associated values. 
;    Use this hash as input for `imageRF_parameterize_processing`
;    
;-
function imageRF_parameterize_getParameters, parameters, guiSettings
  if ~isa(guiSettings) then guiSettings = hash()
  if ~isa(parameters) then parameters = hash()
  
  
  rfType = parameters.hubGetValue('rfType')
 
  case rfType of
    'RFR' : begin
              title = 'imageRF: RFR Parameterization'
              extension = 'rfr'
              Regression = 1
              ReferenceTitle = ['Reference Areas','Background Value']
            end
    'RFC' : begin
              title = 'imageRF: RFC Parameterization'
              extension = 'rfc'
              Classification = 1        
              ReferenceTitle = 'Reference Areas'
            end
    else : message, 'rfType unknown'
  endcase
  
  
  
  tsize=100
     
  hubAMW_program, title=title, guiSettings.hubGetValue('groupLeader')
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleset,'inputSampleSet', Title='Image' $
    , value = parameters.hubGetValue('inputImage') $
    , REFERENCEVALUE = parameters.hubGetValue('inputLabels') $
    , Regression = Regression, Classification=Classification $
    , ReferenceTitle=ReferenceTitle, tsize=tsize
  hubAMW_frame, Title='Parameters'
  hubAMW_parameter, 'numberOfTrees', Title='Number of Trees' $
                  ,Size=5,/Integer, IsGE=1, ISLE=60000 $
                  ,VALUE=parameters.hubGetValue('numberOfTrees', default=100)

  hubAMW_frame, Title='Advanced Parameters', /ADVANCED
  hubAMW_label, 'Number of Features'
  hubAMW_subframe, 'numberOfFeaturesSelection'  ,Title='square root of all features', /Row,/SetButton
  hubAMW_subframe, 'numberOfFeaturesSelection'  ,Title='log of all features', /Row
  hubAMW_subframe, 'numberOfFeaturesSelection'  ,Title='by user', /Row
  hubAMW_parameter, 'numberOfFeatures'  ,Title='Number of Features ' $
                  , Size=3,/Integer,IsGE=1 $
                  , value = parameters.hubGetValue('numberOfFeatures')
  
  if rfType eq 'RFC' then begin
    hubAMW_subframe, Title='Impurity Function', /Row
    rfc_impurityFunctions_values =['gini', 'entropy']
    rfc_impurityFunctions_names  =['Gini Coefficient','Entropy']
    hubAMW_checklist, 'impurityFunction', title='' $
                    , List = rfc_impurityFunctions_names $
                    , EXTRACT= rfc_impurityFunctions_values $
                    , Value=0
  endif
  
  ;hubAMW_frame, Title='Stop Criteria', /ADVANCED
  
  hubAMW_subframe, Title='Stop Criteria', /Row
  hubAMW_parameter      ,'minNumberOfSamples'   $
                        ,Title='Min number of samples in node' $
                        ,Size=5,/Integer,IsGE=1,ISLE=50000 $
                        ,VALUE= parameters.hubGetValue('minNumberOfSamples', default=1)
                        
  hubAMW_parameter      ,'impurityThreshold'    ,Title='  Min impurity = '  $
                        ,Size=5,/Integer,IsGE=0D,ISLE=1D $
                        ,VALUE=parameters.hubGetValue('impurityThreshold',default=0D)

  hubAMW_frame, Title='Output'
  hubAMW_outputFilename,'model', Title= rfType + ' Model', EXTENSION=extension $
                       , VALUE=parameters.hubGetValue('model', default= extension+'Model.'+extension) $
                       , tsize=tsize
                       

  parameters = hubAMW_manage(ConsistencyCheckFunction='imageRF_parameterize_getParameters_consistency_check')
  
  if parameters['accept'] then begin 
    parameters['title'] = title
    numberOfBands = (hubIOImgInputImage((parameters['inputSampleSet'])['featureFilename'])).getMeta('bands')
    case  parameters['numberOfFeaturesSelection'] of
      0 : parameters['numberOfFeatures'] = round(sqrt(numberOfBands))
      1 : parameters['numberOfFeatures'] = fix(alog10(numberOfBands+1)/alog10(2))
      2 : ; do nothing 
    endcase
    parameters.remove, 'numberOfFeaturesSelection'
    
    
    if rfType eq 'RFR' then begin
        parameters['impurityFunction'] = 'regression'
    endif else begin
        parameters['impurityFunction'] = (parameters['impurityFunction'])[0]
    endelse
       
    ; TODO: categorical variables in EnMap-Box
    parameters['categoricalArray'] = intarr(numberOfBands)
    parameters['rfType']= rfType    
    parameters['inputImage']  = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputLabels'] = (parameters['inputSampleSet'])['labelFilename']
    parameters.remove, 'inputSampleSet'
    
  endif 
  
  return, parameters
end

