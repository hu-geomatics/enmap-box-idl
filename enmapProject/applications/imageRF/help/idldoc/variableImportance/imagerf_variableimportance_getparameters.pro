
;+
; :Description:
;    This function opens a widget dialog to select a RFC or RFR model.
;    The returned hash can be used as input argument for 
;   `imageRF_viewModel_processing`. 
;
;
; :Params:
;    parameters : in, required, type=hash
;     A hash containing the following values::
;       
;       hash key | type   | description
;       ---------+--------+------------
;       rfType   | string | type of RF: RFC or RFR
;       ---------+--------+------------
;       
;     This hash can also be used to provide default values.
;     
;    GUISettings : in, optional, type=hash
;    
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          ------------+------+-----------------
;
;
; :returns: 
;    This function returns a hash that contains the selected parameter keys and its associated values. 
;    Use this hash as input for `imageRF_viewModel_processing`
;-
function imageRF_variableImportance_getParameters, parameters, guiSettings
  if ~isa(guiSettings) then guiSettings = hash()
  rfType = parameters['rfType']
  if ~isa(rfType) then message, 'Unknown rfType'
  
  title = 'imageRF: Variable Importance'
  hubAMW_program  ,Title=title ,guiSettings.hubGetValue('groupLeader')
  hubAMW_frame    ,Title='Input'
  case rfType of 
    'RFR' : hubAMW_inputFilename ,'model',Title= rfType + ' Model',EXTENSION='rfr' $
            , Value=parameters.hubGetValue('model')
    'RFC' : hubAMW_inputFilename ,'model',Title= rfType + ' Model',EXTENSION='rfc' $
            , Value=parameters.hubGetValue('model')

  endcase
  parameters = hubAMW_manage()
  
  if parameters['accept'] then begin 
    parameters['title'] = title
  endif 
  
  return, parameters
end