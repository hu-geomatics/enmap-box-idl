;+
; :Description:
;    This procedure is the main routine to parameterize a random forest model. 
;    It is called by the `imageRF_parameterize_event` event handler and uses 
;    the `imageRF_parameterize_getParameters` to collect all required parameter values. 
;
;
; :Keywords:
;    GroupLeader: in, optional, type=int
;       Use this keyword to specify the group leader for used widgets.
;       
;    rfType: in, required, type=string
;        Use this keyword to specify the type of random forest model you want to 
;        parameterize. The following values are allowed::
;        
;           'RFC' for random forest classification
;           'RFR' for random forest regression
;           
;    action: in, required, type = string
;       Can be one of the following actions::
;       
;        `train` to train a new RF model
;        
;        `apply` to apply an existing RF model and create a new label image
;        
;        `validate` to validate an existing RF model with an reference image
;        
;        `viewModel` to view an RF model's parameters
;        
;        `variableImportance` to calculatr and view the variable importance of an RF model
;         
;           
;-
pro imageRF_application $
  ,GroupLeader = groupLeader $
  ,rfType = rfType $
  ,action = action
  
  ;restore last parameters
  case rfType of 
        'RFC' : lastParameters = hub_getAppState('imageRF', 'stateRFC')
        'RFR' : lastParameters = hub_getAppState('imageRF', 'stateRFR')
        else : message, 'rfType must be set to RFC or RFR'
  endcase
  if ~isa(lastParameters) then lastParameters = hash()
  lastParameters['rfType'] = rfType
  
  guiSettings = Hash('groupLeader', GroupLeader, 'noShow', 0b)
  
  ;1. Get parameters
  case action of
    'train'               : parameters = imageRF_parameterize_getParameters(lastParameters, guiSettings)
    'apply'               : parameters = imageRF_apply_getParameters(lastParameters, guiSettings)
    'validate'            : parameters = imageRF_validate_getParameters(lastParameters, guiSettings)
    'variableImportance'  : parameters = imageRF_variableImportance_getParameters(lastParameters, guiSettings)
    'viewModel'           : parameters = imageRF_viewModel_getParameters(lastParameters, guiSettings)
  endcase
  
  
  if parameters['accept'] then begin
     ;save parameters
      case rfType of 
            'RFC' : hub_setAppState,'imageRF', 'stateRFC', parameters
            'RFR' : hub_setAppState,'imageRF', 'stateRFR', parameters
      endcase
      
      
      ;2. Run requested action
      case action of
        'train' : begin
               stopWatch = hubProgressStopWatch()
               imageRF_parameterize_processing, parameters, guiSettings
               stopWatch.showResults, title='imageRF', description='Parameterization completed'
               stopWatch.cleanup  
               ; classify an image of model parameterization?
               ok = dialog_message('Do you want to apply the model to an image?', /QUESTION, TITLE=parameters['title'])
               if ok eq 'Yes' then begin
                  imageRF_application,GroupLeader= groupLeader, RFTYPE=rfType, action='apply'
               endif
             end
        'apply' : begin
              stopWatch = hubProgressStopWatch()
              imageRF_apply_imageProcessing,parameters, guiSettings
              stopWatch.showResults, title='imageRF', description='Calculations completed.'
              stopWatch.cleanup    
            end
        'validate' : begin
               performance = imageRF_validate_Processing(parameters, guiSettings)
               ;create a text report and show it
               case rfType of 
                'RFC' : parameters['type'] = 'RFC (Random Forest Classification)'
                'RFR' : parameters['type'] = 'RFR (Random Forest Regression)'
               endcase
               report = hubApp_accuracyAssessment_getReport(performance, guiSettings)
               report.SaveHTML, /show
               
               file_delete, performance['inputEstimation']  $
                          , performance['inputEstimation'] + '.hdr', /ALLOW_NONEXISTENT
            
            
            end
        'variableImportance' : begin
              results = imageRF_variableImportance_processing(parameters, guiSettings)
              report = imageRF_variableImportance_getReport(results, parameters) 
              report.saveHTML, /SHOW
            end
        
        'viewModel' : begin
            report = imageRF_viewModel_getReport(parameters, groupLeader=guiSettings.hubGetValue('groupLeader'))
            report.saveHTML, /Show
          end
      endcase
      
  endif
  
END
