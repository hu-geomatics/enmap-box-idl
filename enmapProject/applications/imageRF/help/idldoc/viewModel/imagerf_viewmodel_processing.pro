;+
; :Description:
;    This routine shows information about an RFC or RFR model.
;
; :Params:
;    parameters: in, required, type=Hash
;       A hash that contains the following parameters (* = optional)::
;       
;           key      | type   | description
;           ---------+--------+--------------------------------------------------
;           model    | string | file path of RFC or RFR model
;         * title    | string | title to be shown in the progress bar and 
;                    |        | the window that plots the variable importances.
;           ---------+--------+-------------------------------------------------- 
;
;    guiSettings: in, optional, type=hash
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          ------------+------+-----------------
;
;-
PRO imageRF_viewModel_processing, parameters, guiSettings
  if ~isa(guiSettings) then guiSettings = hash()
  
  IF ~(FILE_INFO(parameters['model'])).EXISTS THEN MESSAGE, "File doesn't exist!!"
  
  imageRF_loadModel $
    ,parameters['model'] $
    ,SETTINGS=settings $
    ,OOBERROR=oobError
  groupLeader = guiSettings.hubGetValue('groupLeader')
  base = WIDGET_BASE(/COLUMN)
  
  window = WIDGET_WINDOW(base, X_SCROLL_SIZE=600, Y_SCROLL_SIZE=450, SENSITIVE=0 $
                       , Group_Leader=groupLeader )
  WIDGET_CONTROL, base, /REALIZE, BASE_SET_TITLE=(parameters.hasKey('title'))? parameters['title'] : !NULL
  WIDGET_CONTROL, window, GET_VALUE=oWin
  oWin.Select
    
   textString = $
      ['ImageRF Model:   ' + parameters['model'] $
      ,'' $
      ,'Training Data:' $
      ,'  Image:           ' + settings.inputImage $
      ,'  Reference Areas: ' + settings.inputLabels $
      ,'' $
      ,'Random Forest:' $
      ,'  Number of... ' $ 
      ,'    Features/Bands:             ' + STRTRIM(settings.numberOfBands,2) $
      ,'    Randomly Selected Features: ' + STRTRIM(settings.numberOfFeatures,2) $
      ,'    Training Samples:           ' + STRTRIM(settings.totalClassSamples, 2) $
      ,'    Trees:                      ' + STRTRIM(settings.numberOfTrees,2) $
      ]
    
  IF settings.rfType eq 'RFR' THEN BEGIN
    titleText = 'Learning Curve (Out-Of-Bag-Error)'
    yTitle = 'Error'
    oobError = SQRT(oobError)
    yrange=[FLOOR(MIN(oobError)),CEIL(MAX(oobError))] 
  ENDIF ELSE BEGIN
    CASE settings.impurityFunction OF
      'gini'    : impurityText = 'Gini-Coefficient'
      'entropy' : impurityText = 'Entropy'
      ELSE : message, 'unknown impurity function'
    ENDCASE
     textString = [textString $
                ,'    Classes:                    ' + STRTRIM(settings.numberOfClasses-1,2) $
                ,'  Impurity Function:            ' + impurityText $
                ] 
    
    titleText = 'Learning Curve (Out-Of-Bag-Accuracy)'
    yTitle = 'Accuracy [%]'
    yrange = [0D,100D]
  ENDELSE   
  
  text = WIDGET_TEXT(base,YSIZE=15);,/WRAP)
  widget_control, text, SET_VALUE=textString

  ; label x-axis (no label with data type float)
  nVar = N_ELEMENTS(oobError)
  IF nVar le 10 THEN BEGIN
    xTickVal = LINDGEN(nVar)
    xminor=0
  ENDIF  

  plot = PLOT(oobError, "r1-",TITLE=titleText,YRANGE=yrange,XTITLE='Number of Trees', $
    YTITLE=yTitle,XTICKVALUES=xTickVal,/CURRENT)
  plot.FONT_SIZE=11
  plot.XTICKFONT_SIZE=11
  plot.YTICKFONT_SIZE=11
END
