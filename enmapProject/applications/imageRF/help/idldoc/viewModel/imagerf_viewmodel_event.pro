;+
; :Description:
;    This is the event handler for a EnMAP-Box Menu button. 
;    It is called when clicking on the button, starts the 
;    `imageRF_viewModel_application` procedure and catches 
;    all errors that orrure during processing.
;
; :Params:
;    event: in, required, type=button event structure
;    
;-
PRO imageRF_viewModel_event $
  ,event
  @huberrorcatch
  eventInfo = hubProEnvHelper.getMenuEventInfo(event)
  imageRF_application, GroupLeader=event.top,rfType=eventInfo.argument, action='viewModel'
END