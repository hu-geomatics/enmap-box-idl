function imageRF_getDirname, SourceCode=sourceCode
  
  result = filepath('imageRF', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result

end

;+
; :Hidden:
;-
pro test_imageRF_getDirname

  print, imageRF_getDirname()
  print, imageRF_getDirname(/SourceCode)

end

