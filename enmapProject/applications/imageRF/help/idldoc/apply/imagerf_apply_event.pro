;+
; :Description:
;    This procedure is the event handler for a EnMAP-Box Menu button. 
;    It is called when clicking on the button, starts the 
;    `imageRF_apply_application` procedure and catches 
;    all errors that orrure during processing.
;    
; :Params:
;    event: in, required, type=button event structure
;
;
;
;-
PRO imageRF_apply_event $
  ,event
  
  @huberrorcatch
  eventInfo = hubProEnvHelper.getMenuEventInfo(event)
  imageRF_application,GroupLeader=event.top,rfType=eventInfo.argument,ACTION='apply' 

END