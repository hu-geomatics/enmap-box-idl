;+
; :Description:
;    This routine applies a RF model (RFC or RFR) to a raster image. The image values must have
;    the same data type as that on which the model was trained. 
;    
;
; :Params:
;    PARAMETERS : in, required, type=string
;       A hash that contains the following parameters (* = optional)::
; 
;             hash key                | type    | description
;           --------------------------+---------+-----------------------------------------------------
;             'inputImage'            | string  | path of image file
;             'model'                 | string  | path of random forest model (usually *.rfc or *.rfr) 
;             'outputImgEst'          | string  | path and name of the output file
;           * 'inputMask'             | string  | path of a mask file 
;           * 'dataIgnoreValue'       | int     | data ignore value in the output file in case of a regression
;                                     |         | The default value is given by the models training data 
;           * 'writeProbabilityImage' | {0 | 1} | adds the probability image in case of a classification
;                                     |         | default = 0 = no probability image
;           --------------------------+---------+-----------------------------------------------------
;    
;    
;    guiSettings : in, optional, type=hash
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          noShow      | bool | set this to avoid showing a progress bar
;          noOpen      | bool | set this to avoid opening the created image
;          ------------+------+-----------------
;-
PRO imageRF_apply_imageProcessing, parameters, guiSettings
   if ~isa(guiSettings) then guiSettings=hash('noOpen',1b)
  
   HELPER = hubHelper()
   progressBarInfo = ['Model:  ' + parameters['model'] $
                     ,'Image:  ' + parameters['inputImage'] $
                     ,'Mask:   ' + parameters.hubGetValue('inputMask',default='---') $
                     ,'Result: ' + parameters['outputImgEst'] $
                     ]
                     
   progressBar = hubProgressBar( $
            title = (parameters.hubIsa( 'title')) ? parameters['title'] : !NULL $
          , info = progressBarInfo $
          , GroupLeader = guiSettings.hubGetValue('groupLeader') $
          , NoShow = guiSettings.hubGetValue('noShow'))  
                     

  ;check parameters 
  required =  ['model', 'inputImage', 'outputImgEst']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  IF ~(FILE_INFO(parameters['model'])).EXISTS THEN MESSAGE, "Model doesn't exist!!"
  
  imageRF_loadModel $
    ,parameters['model'] $
    ,SETTINGS=settings $
    ,RFMODELSPLITPOINTPTRARR=rfModelSplitpointPtrArr $
    ,RFMODELSPLITVARIABLEPTRARR=rfModelSplitvariablePtrArr $
    ,RFMODELNODECLASSPTRARR=rfModelNodeclassPtrArr 
  
  parameters['rfType'] = settings.rfType
  
  
  
   
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  nBands   = inputImage.getMeta('bands')
  nSamples = inputImage.getMeta('samples')
  nLines   = inputImage.getMeta('lines')
  inputDIV = inputImage.getMeta('data ignore value')
  if parameters.hubIsa('inputMask') then begin
    maskImage = hubIOImgInputImage(parameters['inputMask'], /Mask)
  endif
  outputImage = hubIOImgOutputImage(parameters['outputImgEst'], NoOpen=guiSettings.hubGetValue('noOpen'))
  
  
  ;Default Values
  tilelines = hubIOImg_getTileLines(inputImage.getMeta('samples') $
                                      , inputImage.getMeta('bands') $
                                      , inputImage.getMeta('data type'))
  
  useMask = isa(inputDIV) || isa(maskImage)
  
  IF settings.numberOfBands ne nBands THEN MESSAGE, "Number of bands don't match!!"
;  IF settings.datatype      ne inputImage.getMeta('data type') THEN MESSAGE, "Data types don't match!!"
  
  
  outputImage.copyMeta,inputImage,['samples','lines','map info','coordinate system string','x start','y start']
  outputImage.setMeta,'bands',1
  outputImage.setMeta,'interleave','bsq'
  IF settings.rfType eq 'RFR' THEN BEGIN
    outputImage.setMeta,'file type','ENVI Standard'
    outputImage.setMeta,'data type', 4 ;data_type
    outputImage.setMeta,'data ignore value', settings.dataIgnoreValue
    writeProbabilityImage = 0
  ENDIF ELSE BEGIN
    outputImage.setMeta,'file type','ENVI Classification'
    outputImage.setMeta,'data type', 1
    outputImage.setMeta,'classes',settings.numberOfClasses
    outputImage.setMeta,'class lookup',settings.lookup
    outputImage.setMeta,'class names',settings.namesOfClasses
    ;outputImage.setMeta,'class lookup',settings.lookup
    writeProbabilityImage = parameters.hubKeywordSet('writeProbabilityImage')
    
    IF writeProbabilityImage THEN BEGIN
      bn = FILE_BASENAME(parameters['outputImgEst'])
      dn = FILE_DIRNAME(parameters['outputImgEst'])
      ext = stregex(bn, '[.](dat|bsq|bil|bip)$',/FOLD_CASE, /EXTRACT)
      bn = FILE_BASENAME(bn, ext)
      pathProbImg = FILEPATH(bn+'_prob'+ext, ROOT_DIR=dn)
      probabilityImageFile = hubIOImgOutputImage(pathProbImg, NOOPEN=guiSettings.hubGetValue('noOpen'))
      probabilityImageFile.copyMeta,inputImage,['map info','coordinate system string','x start','y start']
      probabilityImageFile.setMeta, 'band names', settings.namesofclasses[1:*]
      if useMask then probabilityImageFile.setMeta, 'data ignore value', settings.dataIgnoreValue
    ENDIF
  ENDELSE
 
 
  ;init readers & writers
  inputImage.initReader, tilelines, /Slice, /TileProcessing
  writerSettings = inputImage.getWriterSettings(SetBands=1, SetDataType=outputImage.getMeta('data type'))
  outputImage.initWriter, writerSettings
  if isa(maskImage) then begin
    maskImage.initReader, tilelines, /Slice, /TileProcessing, /Mask
  endif
  
  IF writeProbabilityImage THEN BEGIN 
    ;use same writerSetting as for input Image (with exception of the number of bands)
    probabilityImageWriterSettings = inputImage.getWriterSettings(SetBands = settings.numberOfClasses - 1, SetDataType='float')
    probabilityImageFile.initWriter, probabilityImageWriterSettings 
  ENDIF
  
  ;start tile processing
  progress_max = float(outputImage.getMeta('lines')) 
  progress_lines = 0l 
  
  while ~inputImage.tileProcessingDone() do begin
    
    progressBar.setProgress, progress_lines/progress_max
    progress_lines += tilelines
    features = inputImage.getData()
    
    if useMask then begin
      mask = make_array(n_elements(features[0,*]), /Byte, value=1b)
      
      if isa(inputDIV) then begin
        if finite(inputDIV) then begin
          for iBand = 0, nBands - 1 do begin
            mask and= features[iBand,*] ne inputDIV
          endfor
        endif else begin
          for iBand = 0, nBands - 1 do begin
            mask and= finite(features[iBand,*])
          endfor
        endelse
      endif
      if isa(maskImage) then begin
        mask and= maskImage.getData()
      endif
    endif
    
    results = imageRF_apply_dataProcessing( $
        features, settings $;required
        ,rfModelSplitpointPtrArr $;required, TODO
        ,rfModelSplitvariablePtrArr $;required, TODO
        ,rfModelNodeclassPtrArr $;required, TODO
        ,mask= mask $
        ,returnProbabilityImage=writeProbabilityImage) ;optional
    
    outputImage.writeData, results['estimates']
    if writeProbabilityImage then begin
      probabilityImageFile.writeData, results['probabilityImage'];transpose removed
    endif
  endwhile ;end Tile-Processing
  
  ;clean & close everything
  inputImage.cleanup
  outputImage.cleanup
  if isa(maskImage) then maskImage.cleanup
  if isa(probabilityImageFile) then probabilityImageFile.cleanup
  PTR_FREE, rfModelSplitpointPtrArr, rfModelSplitvariablePtrArr, rfModelNodeclassPtrArr
 
  progressBar.cleanup

  
END
;
;;+
;; :Hidden:
;;-
;PRO test_imageRF_apply_imageProcessing
;

; 
;  types = ['RFC'] ;, 1 = 'RFR'
;  foreach type, types do begin
;    parameters = Hash()
;    parameters['rfType'] = type
;    parameters['writeProbabilityImage'] = 1
;    parameters['accept'] = 1
;    case type of 
;      'RFR' : BEGIN
;          parameters['inputImage']      = 'D:\Dokumente\temp\data\regression\Hymap_Berlin-B_Valide'
;          parameters['model']      = 'D:\Dokumente\temp\data\regression\results\model_B_Train.rfr'
;          parameters['outputImgEst'] = 'D:\Dokumente\temp\data\regression\results\model_B_Valide_estimation_fullscene'      
;              END
;      'RFC' : BEGIN
;          parameters['inputImage']      = 'D:\Dokumente\temp\data\classification\Hymap_Berlin-A_Image'
;          parameters['model']      = 'D:\Dokumente\temp\data\classification\results\model_A_Train.rfc'
;          parameters['outputImgEst'] = 'D:\Dokumente\temp\data\classification\results\model_A_Train_estimation_fullscene'
;              END
;       ELSE: message, 'unknown type'
;    endcase
;    imageRF_apply_imageprocessing, parameters
;    print, parameters
;  endforeach
;  print, 'Done!'
;END