function imageRF_apply_getParametersCheck, resultHash, Message=message

  msg = !NULL
  isConsistent = 1b
  
  inputImageFilename = (resultHash['inputSampleSet'])['featureFilename']
  inputImage = hubIOImgInputImage(inputImageFilename)
  imageRF_loadModel, resultHash['model'] , SETTINGS=settings
  
  if inputImage.getMeta('bands') ne settings.numberOfBands then begin
    msg = [msg $ 
          , string(settings.rftype, settings.numberOfBands $
          , format='(%"This %s model requires an input image with %i bands.")') $
          , string(file_basename(inputImageFilename), inputImage.getMeta('bands') $
          , format='(%"(Bands in \"%s\": %i)")') $
          
          ]
    isConsistent *= 0b
  
  endif
  inputImage.cleanup
  message=msg
  return, isConsistent
end

;+
; :Description:
;    This function opens a widget dialog to collect the parameter values required to
;    apply a RF model. All collected parameters are returned in a hash that can be 
;    used as argument for `imageRF_apply_imageprocessing`.
;
; :Params:
;    parameters : in, required, type=hash
;     A hash containing the following values::
;       
;       hash key | type   | description
;       ---------+--------+------------
;       rfType   | string | type of RF: RFC or RFR
;       ---------+--------+------------
;       
;     This hash can also be used to provide default values.
;     
;    GUISettings : in, optional, type=hash
;    
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          ------------+------+-----------------
;-
function imageRF_apply_getParameters, parameters, guiSettings
  if ~isa(guiSettings) then guiSettings=hash()
  
  rfType = parameters.hubGetValue('rfType')
  
  case rfType of
    'RFR' : begin
              title_prog  = 'imageRF: Apply RFR to Image'
              title_model = rfType + ' Model'
              file_ext    = 'rfr'
              ask_for_probabilityImage = 0
            end
    'RFC' : begin
              title_prog  = 'imageRF: Classify Image'
              title_model = rfType + ' Model'
              file_ext    = 'rfc'
              ask_for_probabilityImage = 1
            end
    else: message, 'rfType unknown'
  endcase
  
  
  
  tsize=75
  hubAMW_program ,Title=title_prog                 ,guiSettings.hubGetValue('groupLeader')
    hubAMW_frame ,Title='Input'
    
    hubAMW_inputFilename  ,'model'        ,Title=title_model, EXTENSION=file_ext $
          , Value=parameters.hubGetValue('model'), tsize=tsize 
    hubAMW_inputSampleSet ,'inputSampleSet'  ,Title='Image    ' $
          , /Masking, /ReferenceOptional $
          , value = parameters.hubGetValue('inputImage'), tsize=tsize
          
    hubAMW_frame                             ,Title='Output'
    
    hubAMW_outputFilename ,'outputImgEst'   ,Title= rfType + ' Result' $
      , VALUE=parameters.hubGetValue('outputImgEst', default = file_ext+'Estimation') $
      , tsize=tsize
      
  if ask_for_probabilityImage then begin
    hubAMW_checkbox, 'writeProbabilityImage' ,Title='Output Class Probabilities'
  endif
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='imageRF_apply_getParametersCheck')
  if parameters['accept'] then begin
    parameters['title'] = title_prog
    parameters['rfType'] = rfType
    parameters['inputImage'] = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputMask']  = (parameters['inputSampleSet'])['labelFilename']
    parameters.remove, 'inputSampleSet'    
  endif 
  
  return, parameters
END