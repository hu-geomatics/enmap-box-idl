;+
; :Description:
;    This is the event handler for an EnMAP-Box Menu button. 
;    It is called when clicking on the button, starts the 
;    `imageRF_Validate_application` procedure and catches all 
;    errors that orrure during processing.
;
; :Params:
;    event: in, required, type=button event structure
;
;-
pro imageRF_Validate_Event,event
  
  @huberrorcatch
  menueEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  imageRF_application,GroupLeader=event.top,rfType=menueEventInfo.argument, ACTION='validate'
end