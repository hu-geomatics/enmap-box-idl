/* Index used for searching */
/*
   Fields used:
     url, name, type, filename, authors, routine name, comments, parameters,
     categories, and attributes
*/
title = "EnWaterMAP Documentation";
subtitle = "EnWaterMAP Subtitle";
libdata = new Array();
libdataItem = 0;



libdata[libdataItem++] = new Array("./enwatermap_application.html", "enwatermap_application.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_application.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_application.html#EnWaterMAP_application", "EnWaterMAP_application", 'routine in <a href="./enwatermap_application.html">enwatermap_application.pro</a>', "enwatermap_application.pro", "", "EnWaterMAP_application", "    This is the application main procedure. It queries user input (see EnWaterMAP_getParameters)     via a widget program. The user input is passed to the application processing routine     (see EnWaterMAP_processing). For detailed information about implementing image processing     routines ToDo Tutorial. Finally, all results are presented in a HTML report     (see EnWaterMAP_showReport).   ", "applicationInfo       See enmapBoxUserApp_getApplicationInfo.   ", "          -1", "");
  
  

libdata[libdataItem++] = new Array("./enwatermap_event.html", "enwatermap_event.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_event.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_event.html#EnWaterMAP_event", "EnWaterMAP_event", 'routine in <a href="./enwatermap_event.html">enwatermap_event.pro</a>', "enwatermap_event.pro", "", "EnWaterMAP_event", "    This is the application event handler. It is called when a user starts the application     from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler     (see huberrorcatch batch file provided by the hubAPI library)     and starts the applications main procedure EnWaterMAP_application.     The error handler catches every unhandled error and displays the traceback report     on the screen.   ", "event", "          -1", "");
  
  

libdata[libdataItem++] = new Array("./enwatermap_getdirname.html", "enwatermap_getdirname.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_getdirname.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_getdirname.html#EnWaterMAP_getDirname", "EnWaterMAP_getDirname", 'routine in <a href="./enwatermap_getdirname.html">enwatermap_getdirname.pro</a>', "enwatermap_getdirname.pro", "", "EnWaterMAP_getDirname", "", "", "          -1", "");
  
  

libdata[libdataItem++] = new Array("./enwatermap_getparameters.html", "enwatermap_getparameters.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_getparameters.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de), Mathias Bochow (mathias.bochow@gfz-potsdam.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_getparameters.html#EnWaterMAP_getParametersConsistencyCheckFunction", "EnWaterMAP_getParametersConsistencyCheckFunction", 'routine in <a href="./enwatermap_getparameters.html">enwatermap_getparameters.pro</a>', "enwatermap_getparameters.pro", "", "EnWaterMAP_getParametersConsistencyCheckFunction", "", "MessageresultHash", "          -1", "");
  
  libdata[libdataItem++] = new Array("./enwatermap_getparameters.html#EnWaterMAP_getParameters", "EnWaterMAP_getParameters", 'routine in <a href="./enwatermap_getparameters.html">enwatermap_getparameters.pro</a>', "enwatermap_getparameters.pro", "", "EnWaterMAP_getParameters", "    This is the application user input function. It queries all required user input     via a widget program (for details see auto-managed widget programs provided     by the hubAPI library).   ", "settings ", "          -1", "");
  
  

libdata[libdataItem++] = new Array("./enwatermap_getsettings.html", "enwatermap_getsettings.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_getsettings.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_getsettings.html#EnWaterMAP_getSettings", "EnWaterMAP_getSettings", 'routine in <a href="./enwatermap_getsettings.html">enwatermap_getsettings.pro</a>', "enwatermap_getsettings.pro", "", "EnWaterMAP_getSettings", "    Use this function to query application settings. The settings file is located under:     filepath('EnWaterMAP.conf', ROOT=EnWaterMAP_getDirname(), SUBDIR='resource').   ", "", "          -1", "    Print settings stored inside settings file: <span class= code-output > print, EnWaterMAP_getSettings()</span>    IDL prints something like: <span class= code-output > appName: NewApp</span> <span class= code-output > version: 1.0</span>      Returns a hash with all settings.   ");
  
  

libdata[libdataItem++] = new Array("./enwatermap_make.html", "enwatermap_make.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_make.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_make.html#EnWaterMAP_make", "EnWaterMAP_make", 'routine in <a href="./enwatermap_make.html">enwatermap_make.pro</a>', "enwatermap_make.pro", "", "EnWaterMAP_make", "    This routine builds the final application. It uses the following steps:      1. Create the application directory (enmapProject/EnWaterMAP) and its subdirectories      2. Copy all non *.pro files the application directory, e.g. the enmap.men      3. Compile all *.pro files and save its routines to EnWaterMAP/lib/EnWaterMAP.sav      4. Create the source code documentation by IDLDoc and write it to EnWaterMAP/help/   ", "Cleanup     Set this to remove this application from its installation directory.   CopyOnly     Set this to create the final folder and file structure only without compiling or running IDLDoc.   NoIDLDoc     Set this to suppress running IDLDoc.   Distribution      Set this to specify an other installation directory. If used as booleand keyword,       a dialog will appear and ask you for the installation directory.   ", "          -1", "");
  
  

libdata[libdataItem++] = new Array("./enwatermap_processing.html", "enwatermap_processing.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_processing.pro", "Mathias Bochow (mathias.bochow@gfz-potsdam.de), Andreas Rabe (andreas.rabe@geo.hu-berlin.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_processing.html#EnWaterMAP_processing", "EnWaterMAP_processing", 'routine in <a href="./enwatermap_processing.html">enwatermap_processing.pro</a>', "enwatermap_processing.pro", "", "EnWaterMAP_processing", "    This is the application processing function. Since the programming code for the EnWaterMAP     application is developed in the Python language, the main task of this function is to call     an executable file compiled from the Python code and to pass the collected input parameters     to the Python program. The Python code can be viewed in the resource folder.     Finally, a hash with report information is returned and can be passed to the application     report routine (EnWaterMAP_showReport).   ", "parameters settings ", "          -1", "   Returns information for the application report routine.   ");
  
  

libdata[libdataItem++] = new Array("./enwatermap_showreport.html", "enwatermap_showreport.pro", '.pro file in <a href="./dir-overview.html">.\ directory</a>', "enwatermap_showreport.pro", "Andreas Rabe (andreas.rabe@geo.hu-berlin.de), Mathias Bochow (mathias.bochow@gfz-potsdam.de)  ", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./enwatermap_showreport.html#EnWaterMAP_showReport", "EnWaterMAP_showReport", 'routine in <a href="./enwatermap_showreport.html">enwatermap_showreport.pro</a>', "enwatermap_showreport.pro", "", "EnWaterMAP_showReport", "    This is the application report routine. It creates a HTML report for presenting     usefull result to the user in the form of text, image or table.   ", "reportInfo      Pass the return value of the EnWaterMAP_processing function to this argument.   settings ", "          -1", "");
  
  

