;+
; :Author: Mathias Bochow (mathias.bochow@gfz-potsdam.de), Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application processing function. Since the programming code for the EnWaterMAP 
;    application is developed in the Python language, the main task of this function is to call 
;    an executable file compiled from the Python code and to pass the collected input parameters
;    to the Python program. The Python code can be viewed in the resource folder. 
;    Finally, a hash with report information is returned and can be passed to the application 
;    report routine (`EnWaterMAP_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
function EnWaterMAP_processing, parameters, settings

  catch, Error_status
  IF (Error_status ne 0) THEN BEGIN
    PRINT, 'Error index: ', Error_status
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    catch, /CANCEL
    GOTO, CleanUpAndExit
  ENDIF

  ; generate parameter string
  parameterString = hash()
  IF parameters.hasKey('inputImage') THEN parameterString['-i'] = parameters['inputImage']
  IF parameters.hasKey('outputImage') THEN parameterString['-ofn'] = parameters['outputImage']
  IF parameters.hasKey('r100') THEN parameterString['-r100'] = parameters['r100']
  IF parameters.hasKey('pxsize') THEN parameterString['-pxsize'] = parameters['pxsize']
  parameterString['-v'] = '-v'
  
  ;print, parameterString
  
  ; define script location and input parameters 
  scriptFilename = filepath('WaterMask_EnMAP-Box.py', ROOT_DIR=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY=['EnWaterMAP','_lib']) 
  ;print, scriptFilename 
  
  ; run script and print result 
  pyMessage = hubPython_runScript(scriptFilename, parameterString) 
  ;print, pyMessage
  
  
  CleanUpAndExit: 
  
  ; store results to be reported inside a hash
  result = hash()
  
  ; Check if the output image exsists
  IF (STRCMP(FILE_search(pyMessage['filename_wmask']), pyMessage['filename_wmask'], /FOLD_CASE) EQ 1) THEN BEGIN
  
    ; check file modification date
    openr, lun, pyMessage['filename_wmask'], /get_lun
    fstatus = fstat(lun)
    close, lun
    IF (systime(/SECONDS) - fstatus.MTIME LT 60) THEN BEGIN

      ; open image in EnMAP-Box
      enmapBox_openImage, pyMessage['filename_wmask']
      
      result['summary'] = ['EnWaterMAP finished processing sucessfully.' $
      ,'You may inspect the output image by displaying it in the EnMAP-Box. If the image looks totally black, use the "define color table" function and change min and max to 0 and 100, respectively.'  $ 
      ,'' $
      ,'You may also use the Accuracy Assessment for Classification Tool of the EnMAP-Box to check the accuracy of the result.'  $
      , 'To do this you have to change some entries in the header file of the output image ' + pyMessage['filename_wmask']+' :' $
      ,'' $
      , '1. Change the file type to "ENVI Classification"' $
      ,'' $
      ,'2. After the entry "sensor type" add:' $
      ,'' $
      ,'classes = 2' $
      ,'class lookup = { ' $
      ,'0,   0,   0, 255,   0,   0} ' $
      ,'class names = { ' $ 
      ,'Unclassified, water}']
    
      inputImage = hubIOImgInputImage(parameters['inputImage'])
      rgb_chan = inputImage.locateWavelength(/TrueColor)
      inputImage.initReader, /Cube, SubsetBandPositions=[rgb_chan]
      result['rgb'] = reverse(ClipScl(inputImage.getdata(), 2),2)
      obj_destroy, inputImage
                  
      outputImage = hubIOImgInputImage(pyMessage['filename_wmask'])
      outputImage.initReader, /Cube
      result['wmask'] = reverse(BytScl(outputImage.getdata()),2)
      obj_destroy, outputImage
      
    ENDIF ELSE BEGIN
    
      ; Output image exists but is older than 20 seconds
      result['summary'] = 'It seems that the processing was not successful. An output image ' + pyMessage['filename_wmask'] + ' exsists but the modification date is not recent. This can have two reasons:<br>' + $
      '1. You have checked the "Do not close DOS window" option in EnWaterMAP and you have closed the window more than 20 seconds after successful processing (then, everthing is fine!).<br>' + $ 
      '2. The output image is a result of an earlier run. Please check the modification date in your file manager.<br><br>' + $
      'You may also check if the text output in the DOS window says that the output image has sucessfully been written and ' + $
      'that no error message was displayed in the DOS window. To do this re-run EnWaterMAP with the "Do not close DOS window" option. In case of displayed error messages you may contact the developer (mathias.bochow@gfz-potsdam.de).'
      
    ENDELSE
    
  ENDIF ELSE BEGIN
  
    result['summary'] = 'It seems that the processing was not successful since the output image ' + pyMessage['filename_wmask'] + $
    ' does not exsist. Please check this manually. Then you may run EnWaterMAP again with the "Do not close DOS window" option to be able to read error messages in the DOS window.' + $
    'You may then report the error message to the developer (mathias.bochow@gfz-potsdam.de).'
  
  ENDELSE
  
  
  return, result

end

;+
; :Hidden:
;-
pro test_EnWaterMAP_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  parameters['inputImage'] = 'c:\tmp\test\Hymap_Berlin-A_Image'
  parameters['outputImage'] = 'c:\tmp\test\Hymap_Berlin-A_Image_wmask.bsq'
;  parameters['inputImage'] = 'c:\tmp\test\helgo-1027_sub2.bsq'
;  parameters['outputImage'] = 'c:\tmp\test\helgo-1027_sub2_wmask.bsq'
  parameters['showTerminal'] = 1b
  settings = EnWaterMAP_getSettings()
  
  reportInfo = EnWaterMAP_processing(parameters, settings)
  EnWaterMAP_showReport, reportInfo, settings
  ;print, result

end  
