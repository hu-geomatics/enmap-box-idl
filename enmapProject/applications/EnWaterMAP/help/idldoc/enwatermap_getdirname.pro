;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function EnWaterMAP_getDirname
  
  result = filepath('EnWaterMAP', ROOT=enmapBox_getDirname(/Applications))
  ;result = filepath('kMeans',     ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnWaterMAP_getDirname

  print, EnWaterMAP_getDirname()
  print, EnWaterMAP_getDirname(/SourceCode)

end
