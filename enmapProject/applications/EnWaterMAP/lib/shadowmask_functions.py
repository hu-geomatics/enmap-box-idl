# -*- coding: utf-8 -*-
###############################################################################
#                                                                             #
#   Functions for shadow and water identification                             #
#                                                                             #
#   Version: 1.0                                                              #
#                                                                             #
#   Changes since v0.2:                                                       #
#   - Adaptation to the needs of the EnMAP-Box                                #
#                                                                             #
#   Changes since v0.1:                                                       #
#   - Implemetation of white water recognition to fill holes                  #
#     within the water mask                                                   #
#                                                                             #
#   Written for Python 2.5 and higher                                         #
#   Aditionally to Python the following modules have to be installed:         #
#   - NumPy                                                                   #
#   - MatPlotLib                                                              #
#   - SciPy                                                                   #
#                                                                             #
#                                                                             #
#                                                                             #
#   Written by Mathias Bochow                                                 #
#   Section 1.4, Helmholtz Centre Potsdam - GFZ German Research Centre for    #
#   Geosciences                                                               #
#   Contact: mathias.bochow@gfz-potsdam.de                                    #
#                                                                             #
#                                                                             #
#   LICENCE                                                                   #
#                                                                             #
#                                                                             #
#                                                                             #
#*****************************************************************************#

# **** IMPORTING LIBRARIES ****************************************************

# standard
import numpy
import sys
import os
import string
import time
from scipy import ndimage


#**** DEFINE CLASSES ****************************************************

class C_Image(object):
  def __init__(self):
    self.grid = None
    self.nCols = None
    self.nRows = None
    self.nBands = None
    self.fType = None
    self.dType = None
    self.iLeave = None
    self.bOrder = None
    self.pxSize = None
    self.pySize = None
    self.pUnit = None
    self.wvlUnit = None
    self.wvl = None
    self.bNames = None




# **** READ SPECTRA AS TEXT DATA **********************************************
#
# 1.col             2. col                  ...   n.col
# wavelength [nm]   radiance / reflectance  ...   radiance / reflectance

def ReadTextDataSpectra(filename,ncol,v):
  # filename = string, path and name of text file
  # ncol = int, number of columns in the textfile
  # return [ndarray] spectra

  filename_ = string.split(filename,"/")
  if(v == 1):
    print "\n      Reading text file \"%s\" ... \n" % (filename_[-1])
  spectra = numpy.array([])
  fp_hdr = open(filename,"r")
  for line in fp_hdr:
    line = line.strip()
    values = string.split(line," ")
    for val in values:
      try:
        spectra = numpy.append(spectra,float(val.strip()))
      except:
        try:
          spectra = numpy.append(spectra,float(int(val.strip())))
        except:
          print "   >>> ERROR : ReadTextDataSpectra() !"
          print "               \"%s\" can't import as number !" % (val)
          print "               \"%s\"\n" % (line)
          fp_hdr.close()
          sys.stdout.flush()
          os._exit(1)

  fp_hdr.close()
  spectra.shape = (int(len(spectra)/ncol),ncol)
  if(v == 1):
    print "          Lines\t\t = " + str(spectra.shape[0])
    print "          Spectra\t = " + str(ncol-1) + "\n"
  return spectra


# **** WRITES A MATRIX 2 ENVI IMAGE *******************************************

def Matrix2EnviImage(matrix,filename,description,v,wvl=None,wvlUnit=None,bNames=None):
  # matrix = matrix to write
  # filename = name of the image to write including path
  # description [string] = description of image content
  # wvl [ndarray 1D] = wavelength of image bands
  # wvlUnit [string] = name of wavelength units
  filename_ = string.split(filename,"/")
  if(v == 1):
    print "\n      Writing image file \"%s\" ... \n" % (filename_[-1])
    if(len(matrix.shape) == 3):
      print "          Samples\t= " + str(matrix.shape[2])
      print "          Lines\t\t= " + str(matrix.shape[1])
      print "          Bands\t\t= " + str(matrix.shape[0]) + "\n"
    elif(len(matrix.shape) == 2):
      print "          Samples\t= " + str(matrix.shape[1])
      print "          Lines\t\t= " + str(matrix.shape[0])
      print "          Bands\t\t= 1\n"
    else:
      print "   >>> ERROR : While writing matrix to file!"
      print "               Matrix must have 2 or 3 dimensions, has %d dimensions." % (len(matrix.shape))

  matrix.tofile(filename)

  if(filename[-4] == "."):
    fp_hdr = open(filename[:-3] + "hdr","w")
  else:
    fp_hdr = open(filename + ".hdr","w")

  fp_hdr.writelines("ENVI\n")
  fp_hdr.writelines("description = {\n")
  fp_hdr.writelines(" " + description + " [%s]}\n" % (time.ctime()))
  if(len(matrix.shape) == 2):
    fp_hdr.writelines("samples = " + str(matrix.shape[1]) + "\n")
    fp_hdr.writelines("lines   = " + str(matrix.shape[0]) + "\n")
    fp_hdr.writelines("bands   = 1\n")
  elif(len(matrix.shape) == 3):
    fp_hdr.writelines("samples = " + str(matrix.shape[2]) + "\n")
    fp_hdr.writelines("lines   = " + str(matrix.shape[1]) + "\n")
    fp_hdr.writelines("bands   = " + str(matrix.shape[0]) + "\n")
  fp_hdr.writelines("header offset = 0\n")
  fp_hdr.writelines("file type = ENVI Standard\n")
  if(matrix.dtype == "uint8"):
    fp_hdr.writelines("data type = 1\n")
  elif(matrix.dtype == "int16"):
    fp_hdr.writelines("data type = 2\n")
  elif(matrix.dtype == "int32"):
    fp_hdr.writelines("data type = 3\n")
  elif(matrix.dtype == "float32"):
    fp_hdr.writelines("data type = 4\n")
  elif(matrix.dtype == "float64"):
    fp_hdr.writelines("data type = 5\n")
  elif(matrix.dtype == "complex64"):
    fp_hdr.writelines("data type = 6\n")
  elif(matrix.dtype == "complex128"):
    fp_hdr.writelines("data type = 9\n")
  elif(matrix.dtype == "uint16"):
    fp_hdr.writelines("data type = 12\n")
  elif(matrix.dtype == "uint32"):
    fp_hdr.writelines("data type = 13\n")
  elif(matrix.dtype == "int64"):
    fp_hdr.writelines("data type = 14\n")
  elif(matrix.dtype == "uint64"):
    fp_hdr.writelines("data type = 15\n")
  fp_hdr.writelines("interleave = bsq\n")
  fp_hdr.writelines("byte order = 0\n")
  if(wvl != None):
    if(wvlUnit != None):
      fp_hdr.writelines("wavelength units = %s\n" % (wvlUnit))
    else:
      fp_hdr.writelines("wavelength units = Unknown\n")
  if bNames == None:
    fp_hdr.writelines("band names = {\n")
    if(len(matrix.shape) == 3):

      dim = int(numpy.floor(matrix.shape[0]) / 5.0)
      if((matrix.shape[0] % 5) == 0):
        test5 = 1
      else:
        test5 = 0
      line = " "
      for step in numpy.arange(0,dim,1):
        if(test5 == 0):
          start = step * 5
          del(line)
          line = " "
          for i in numpy.arange(start,start+5,1):
            bandNr = "band" + str(i+1)
            line = "%s%s, " % (line,bandNr)
          fp_hdr.writelines(line[:-1] + "\n")
        elif(test5 == 1):
          start = step * 5
          del(line)
          line = " "
          if(step < (dim-1)):
            for i in numpy.arange(start,start+5,1):
              bandNr = "band" + str(i+1)
              line = "%s%s, " % (line,bandNr)
            fp_hdr.writelines(line[:-1] + "\n")
          else:
            for i in numpy.arange(start,start+5,1):
              bandNr = "band" + str(i+1)
              line = "%s%s, " % (line,bandNr)
            fp_hdr.writelines(line[:-2] + "}\n")
        else:
          print "\n   >>> WARNING : Error while writing Envi spectral library header"
          print "                 At parameter \"spectra names\" !"
      if(test5 == 0):
        del(line)
        line = " "
        for i in numpy.arange((5*dim),matrix.shape[0],1):
          bandNr = "band" + str(i+1)
          line = "%s%s, " % (line,bandNr)
        fp_hdr.writelines(line[:-2] + "}\n")
    else:
      fp_hdr.writelines(" band 1}\n")
  else:

    if(len(matrix.shape) == 3):


      if len(bNames) != matrix.shape[0]:
        print "WARNING: Number of band names is unequal to number of image bands!"

        fp_hdr.writelines("band names = {\n")

        dim = int(numpy.floor(matrix.shape[0]) / 5.0)
        if((matrix.shape[0] % 5) == 0):
          test5 = 1
        else:
          test5 = 0
        line = " "
        for step in numpy.arange(0,dim,1):
          if(test5 == 0):
            start = step * 5
            del(line)
            line = " "
            for i in numpy.arange(start,start+5,1):
              bandNr = "band" + str(i+1)
              line = "%s%s, " % (line,bandNr)
            fp_hdr.writelines(line[:-1] + "\n")
          elif(test5 == 1):
            start = step * 5
            del(line)
            line = " "
            if(step < (dim-1)):
              for i in numpy.arange(start,start+5,1):
                bandNr = "band" + str(i+1)
                line = "%s%s, " % (line,bandNr)
              fp_hdr.writelines(line[:-1] + "\n")
            else:
              for i in numpy.arange(start,start+5,1):
                bandNr = "band" + str(i+1)
                line = "%s%s, " % (line,bandNr)
              fp_hdr.writelines(line[:-2] + "}\n")
          else:
            print "\n   >>> WARNING : Error while writing Envi spectral library header"
            print "                 At parameter \"spectra names\" !"
        if(test5 == 0):
          del(line)
          line = " "
          for i in numpy.arange((5*dim),matrix.shape[0],1):
            bandNr = "band" + str(i+1)
            line = "%s%s, " % (line,bandNr)
          fp_hdr.writelines(line[:-2] + "}\n")

      else:
        fp_hdr.writelines("band names = {\n")

        dim = int(numpy.floor(matrix.shape[0]) / 5.0)
        if((matrix.shape[0] % 5) == 0):
          test5 = 1
        else:
          test5 = 0
        line = " "
        for step in numpy.arange(0,dim,1):
          if(test5 == 0):
            start = step * 5
            del(line)
            line = " "
            for i in numpy.arange(start,start+5,1):
              line = "%s%s, " % (line,str(bNames[i]))
            fp_hdr.writelines(line[:-1] + "\n")
          elif(test5 == 1):
            start = step * 5
            del(line)
            line = " "
            if(step < (dim-1)):
              for i in numpy.arange(start,start+5,1):
                line = "%s%s, " % (line,str(bNames[i]))
              fp_hdr.writelines(line[:-1] + "\n")
            else:
              for i in numpy.arange(start,start+5,1):
                line = "%s%s, " % (line,str(bNames[i]))
              fp_hdr.writelines(line[:-2] + "}\n")
        if(test5 == 0):
          del(line)
          line = " "
          for i in numpy.arange((5*dim),matrix.shape[0],1):
            line = "%s%s, " % (line,str(bNames[i]))
          fp_hdr.writelines(line[:-2] + "}\n")

    else:
      if len(bNames) != matrix.shape[0]:
        print "WARNING: Number of band names is unequal to number of image bands!"
        fp_hdr.writelines(" band 1}\n")
      else:
        fp_hdr.writelines(" %s}\n" % (str(bNames[0])))


  if(wvl != None):
    line = " "
    fp_hdr.writelines("wavelength = {\n")
    dim = int(numpy.floor(len(wvl) / 7.0))
    if((len(wvl) % 7) == 0):
      test7 = 1
    else:
      test7 = 0
    for step in numpy.arange(0,dim,1):
      if(test7 == 0):
        start = step * 7
        del(line)
        line = " "
        for i in numpy.arange(start,start+7,1):
          value = "%.3f" % (wvl[i])
          line = "%s%s, " % (line,str(value))
        fp_hdr.writelines(line[:-1] + "\n")
      elif(test7 == 1):
        start = step * 7
        del(line)
        line = " "
        if(step < (dim-1)):
          for i in numpy.arange(start,start+7,1):
            value = "%.3f" % (wvl[i])
            line = "%s%s, " % (line,str(value))
          fp_hdr.writelines(line[:-1] + "\n")
        else:
          for i in numpy.arange(start,start+7,1):
            value = "%.3f" % (wvl[i])
            line = "%s%s, " % (line,str(value))
          fp_hdr.writelines(line[:-2] + "}\n")
      else:
        print "\n   >>> WARNING : Error while writing Envi spectral library header"
        print "                 At parameter \"wavelength\" !"
    if(test7 == 0):
      del(line)
      line = " "
      for i in numpy.arange((7*dim),len(wvl),1):
        value = "%.3f" % (wvl[i])
        line = "%s%s, " % (line,str(value))
      fp_hdr.writelines(line[:-2] + "}\n")

  fp_hdr.close()




# **** CHECKS ENVIRONMENT *****************************************************

def CheckPython25():
  version = sys.version[0:3]
  if (float(version) < 2.5):
    print "  Please update your system! You need Python version >= 2.5"
    print "  You have Python version " + str(version) + "!\n"
    sys.stdout.flush()
    os._exit(1)

# **** CHECKS INPUT FILE ******************************************************

def CheckInputFile(argument,liste=sys.argv):
  # argument = position of input parameters
  # liste = list containing the argument
  # returns the input filename
  filename = liste[argument]
  if(os.path.isfile(filename) == False):
    print "   ERROR while checking input file!"
    print "   File %s not found!" % (filename)
    sys.stdout.flush()
    os._exit(1)
  return filename



# **** Functions *******************************************************************

def PrintTimeAndMemory(t01):

  t02 = time.time()
  sec = numpy.round(t02-t01)
  min = 0.0
  h = 0.0
  if(sec/60.0 > 1.0):
    min = numpy.floor(sec/60.0)
    sec = sec - min*60.0
  if(min/60.0 > 1.0):
    h = numpy.floor(min/60.0)
    min = min - h*60.0

  if(h < 10.0):
    h_ = "0"+str(int(h))
  else:
    h_ = str(int(h))
  if(min < 10.0):
    m_ = "0"+str(int(min))
  else:
    m_ = str(int(min))
  if(sec < 10.0):
    s_ = "0"+str(int(sec))
  else:
    s_ = str(int(sec))
  print "\n  Processing time %s:%s:%s" % (h_,m_,s_)
  print "  Processing time %.3f seconds" % (t02-t01)
  finish = time.ctime()
  print "  %s" % (finish[11:-5])
  print "\n"
  print "====================================================================="
  print "\n"
  return

def FindBandWithValue(image, value2D, wvl):
  # image = FE image with two or more spectral channels
  # value2D = one band with values
  # wvl = 1D array of the wavelength of image
  #
  # return:
  # wvl2D = wavelength at which the given value occurs in the input image

  wvl2D = numpy.zeros((image.shape[1],image.shape[2]),dtype=numpy.int16)
  for b in range(image.shape[0]):
    wvl2D = numpy.where(image[b] == value2D,wvl[b],wvl2D)

  return wvl2D




def FilterSum2D(kernel,grid):
  # kernel [class ndarray 2D] = kernel for convolution, spatial domain
  # grid [class ndarray 2D] = image to convolve
  # return [class ndarray 2D] = convolved image
  [krows,kcols] = kernel.shape
  [rows,cols] = grid.shape
  #print "        Convolving %d x %d by %d x %d ...\n" % (rows,cols,krows,kcols)
  # add border to grid
  in_grid = numpy.zeros((grid.shape[0]+krows-1, grid.shape[1]+kcols-1),numpy.float32)
  in_grid[(krows-1)/2:in_grid.shape[0]-((krows-1)/2), (kcols-1)/2:in_grid.shape[1]-((kcols-1)/2)] = grid
  out_grid = numpy.zeros((grid.shape),numpy.float32)
  #for y in range(((krows-1)/2),rows-((krows-1)/2),1):
  #  for x in range(((kcols-1)/2),cols-((kcols-1)/2),1):
  for y in range(((krows-1)/2),rows+((krows-1)/2),1):
    for x in range(((kcols-1)/2),cols+((kcols-1)/2),1):
      sub_grid = in_grid[(y-((krows-1)/2)):(y+((krows-1)/2)+1),(x-((kcols-1)/2)):(x+((kcols-1)/2)+1)]
      sub_grid = sub_grid * kernel
      out_grid[y-((krows-1)/2)][x-((kcols-1)/2)] = numpy.sum(sub_grid)
  return out_grid




def LowAlbedoThreshbyMean(bgmask, inImageGrid, shadowim, saturatedThresh):
  # bgmask : array_like, shape (y,x)
      # background = 0, foreground = 1
  # shadowim : array_like, shape (b,y,x)
      # simulated totally shadowed image
  # nm680 : index for band at wavelength 680 nm
  # saturatedThresh : array_like, shape (b,)

  low_albedo_mask = bgmask * 1

  sorted_specmean = numpy.mean(shadowim,axis=0)
  sorted_specmean = numpy.sort(sorted_specmean,axis=None)
  sorted_specmean = sorted_specmean[numpy.where(sorted_specmean > 0)]

  #meanmean = numpy.mean(sorted_specmean)
  #sdevmean = numpy.std(sorted_specmean)
  #sorted_specmean_thresh_sdev2 = meanmean + 2 * sdevmean
  #sorted_specmean_thresh_sdev3 = meanmean + 3 * sdevmean
  #print "Mean-Variante:", meanmean + 2 * sdevmean, meanmean + 3 * sdevmean

  wsize = int(sorted_specmean.shape[0] / 100.0 * 5)  # ToDo: Die Position des ermittelten Kruemmungspunktes haengt derzeit stark von der Filterfensterbreite wsize ab, sollte davon aber eigentlich recht unabhaengig sein. Loesung unbekannt ...
  if wsize % 2 == 0:
    wsize += 1
  order = 1
  
  #approx_sorted_specmean_clip = savitzky_golay(sorted_specmean_clip,window_size=wsize, order=order)
  
  # gleitender Mittelwertsfilter
  #wsizem = int(sorted_specmean.shape[0] / 1000.0)
  wsizem = 10
  approx_sorted_specmean = numpy.zeros_like(sorted_specmean)
  approx_sorted_specmean[0:wsizem] = sorted_specmean[0:wsizem]
  approx_sorted_specmean[sorted_specmean.shape[0]-wsizem::] = sorted_specmean[sorted_specmean.shape[0]-wsizem::]
  for i in range (wsizem, sorted_specmean.shape[0]-wsizem):
    approx_sorted_specmean[i] = numpy.mean(sorted_specmean[i-wsizem:i+wsizem+1])
  dummy_approx = savitzky_golay(approx_sorted_specmean,window_size=wsize, order=1)
  sorted_specmean_deriv1 = -1 * savitzky_golay(approx_sorted_specmean,window_size=wsize, order=order,deriv=1)
  sorted_specmean_deriv2 = -1 * savitzky_golay(sorted_specmean_deriv1,window_size=wsize, order=order,deriv=1)


  # Kruemmung
  krm_mean = (sorted_specmean_deriv2) / numpy.sqrt(numpy.power((1 + numpy.square(sorted_specmean_deriv1)),3))
  
  # Clipping wegen Randproblem und weil Thresh zwischen ca. 2.5 % und 6 % Albedo liegen muss
  sorted_specmean_clip = sorted_specmean[numpy.where(sorted_specmean >= 2.5)] # Knick bei Grauwerten zwischen 2.5 und 6.0 suchen; Rand hinzufuegen wegen savitzky_golay Filter
  approx_sorted_specmean_clip = approx_sorted_specmean[numpy.where(sorted_specmean >= 2.5)]
  sorted_specmean_deriv1_clip = sorted_specmean_deriv1[numpy.where(sorted_specmean >= 2.5)]
  sorted_specmean_deriv2_clip = sorted_specmean_deriv2[numpy.where(sorted_specmean >= 2.5)]
  krm_mean_clip = krm_mean[numpy.where(sorted_specmean >= 2.5)]
  
  sorted_specmean_clip = sorted_specmean_clip[numpy.where(sorted_specmean_clip <= numpy.mean(saturatedThresh))] # saturatedThresh[b]: So hell ist der Mean des niedrigsten gesaettigten Pixels
  approx_sorted_specmean_clip = approx_sorted_specmean_clip[numpy.where(sorted_specmean_clip <= numpy.mean(saturatedThresh))]
  sorted_specmean_deriv1_clip = sorted_specmean_deriv1_clip[numpy.where(sorted_specmean_clip <= numpy.mean(saturatedThresh))]
  sorted_specmean_deriv2_clip = sorted_specmean_deriv2_clip[numpy.where(sorted_specmean_clip <= numpy.mean(saturatedThresh))]
  krm_mean_clip = krm_mean_clip[numpy.where(sorted_specmean_clip <= numpy.mean(saturatedThresh))]

  krmThresh_mean = sorted_specmean_clip[numpy.where(krm_mean_clip == numpy.max(krm_mean_clip))][0]

  low_albedo_mask = numpy.where(numpy.mean(inImageGrid,axis=0) > krmThresh_mean, 0, low_albedo_mask)
  
  
  #pylab.figure(343)
  #pylab.plot(sorted_specmean,'r-')
  #pylab.plot(approx_sorted_specmean,'g-')
  #pylab.title("order =" + str(order) + "wsize =" + str(wsize))
  ##pylab.figure(344)
  #pylab.plot(dummy_approx,'b-')
  ##pylab.title("approx")
  
  #pylab.figure()
  #pylab.plot(sorted_specmean,'r-')
  #pylab.plot(sorted_specmean_clip,'r-')
  #pylab.plot(approx_sorted_specmean_clip,'g-')
  #pylab.title("Point of maximum curvature for plot of sorted shadowed spectral mean values")
  #pylab.plot(numpy.where(krm_mean == numpy.max(krm_mean)), krmThresh_mean ,'go')
  #pylab.ylabel("Reflection")

  #pylab.plot(numpy.where(krm_mean_clip == numpy.max(krm_mean_clip)), krmThresh_mean ,'go')
  #pylab.figure(b+202)
  #pylab.plot(sorted_specmean_deriv1_clip,'b-')
  #pylab.figure(346)
  #pylab.title("2. Ableitung mean")
  #pylab.plot(sorted_specmean_deriv2_clip,'y-')
  #pylab.figure(347)
  #pylab.title("Kruemmung mean")
  #pylab.plot(krm_mean_clip,'y-')

  return low_albedo_mask





def LowAlbedoThreshbyMaxPerBand(bgmask, inImageGrid, shadowim, nm680, saturatedThresh):
  # bgmask : array_like, shape (y,x)
      # background = 0, foreground = 1
  # shadowim : array_like, shape (b,y,x)
      # simulated totally shadowed image
  # nm680 : index for band at wavelength 680 nm
  # saturatedThresh : array_like, shape (b,)
  
  low_albedo_mask = bgmask * 1
  
  #shadow_max_per_band_thresh_sdev3 = numpy.zeros(inImageGrid.shape[0],dtype=numpy.float)
  #shadow_max_per_band_thresh_sdev2 = numpy.zeros(inImageGrid.shape[0],dtype=numpy.float)
  #krmThresh = numpy.zeros(inImageGrid.shape[0],dtype=numpy.float)
  shadow_max_per_band_thresh_sdev3 = numpy.zeros(nm680+1,dtype=numpy.float)
  shadow_max_per_band_thresh_sdev2 = numpy.zeros(inImageGrid.shape[0],dtype=numpy.float)
  krmThresh = numpy.zeros(inImageGrid.shape[0],dtype=numpy.float)
  shadowim = numpy.reshape(shadowim,(shadowim.shape[0], shadowim.shape[1] * shadowim.shape[2]))
  shadow_max_per_band = numpy.sort(shadowim,axis=1)

  for b in range(nm680+1): # nur im VIS, da bei Gruenschatten besonders im NIR mehr Licht verfuegbar ist als eDif wegen Transmission und Volume Scattering.
  #for b in range(inImageGrid.shape[0]):
    sorted_gv = numpy.sort(shadowim[b])
    sorted_gv = sorted_gv[numpy.where(sorted_gv > 0)]

    bandmean = numpy.mean(sorted_gv)
    bandsdev = numpy.std(sorted_gv)
    shadow_max_per_band_thresh_sdev3[b] = bandmean + 3 * bandsdev
    shadow_max_per_band_thresh_sdev2[b] = bandmean + 2 * bandsdev

    sorted_gv_clip = sorted_gv[numpy.where(sorted_gv >= 1.5)] # Knick bei Grauwerten zwischen 2.5 und 6.0 suchen; Rand hinzufuegen wegen savitzky_golay Filter
    sorted_gv_clip = sorted_gv_clip[numpy.where(sorted_gv_clip <= saturatedThresh[b])] # saturatedThresh[b]: So hell ist der niedrigste gesaettigte Grauwert pro Band

    wsize = int(sorted_gv_clip.shape[0] / 25)
    if wsize %2 == 0:
      wsize += 1
    order = 2

    approx_sorted_gv_clip = savitzky_golay(sorted_gv_clip,window_size=wsize, order=order)
    sorted_gv_clip_deriv1 = -1 * savitzky_golay(approx_sorted_gv_clip,window_size=wsize, order=order,deriv=1)
    sorted_gv_clip_deriv2 = -1 * savitzky_golay(sorted_gv_clip_deriv1,window_size=wsize, order=order,deriv=1)


    # Kruemmung
    krm = (sorted_gv_clip_deriv2) / numpy.sqrt(numpy.power((1 + numpy.square(sorted_gv_clip_deriv1)),3))

    # Rand beschneiden wegen Auffuellung bei Filterung
    #krm = krm[numpy.where(sorted_gv_clip >= 2.5)]
    #approx_sorted_gv_clip = approx_sorted_gv_clip[numpy.where(sorted_gv_clip >= 2.5)]
    #sorted_gv_clip_deriv1 = sorted_gv_clip_deriv1[numpy.where(sorted_gv_clip >= 2.5)]
    #sorted_gv_clip_deriv2 = sorted_gv_clip_deriv2[numpy.where(sorted_gv_clip >= 2.5)]
    #sorted_gv_clip = sorted_gv_clip[numpy.where(sorted_gv_clip >= 2.5)]
    #krm = krm[numpy.where(sorted_gv_clip <= 6.0)]
    #approx_sorted_gv_clip = approx_sorted_gv_clip[numpy.where(sorted_gv_clip <= 6.0)]
    #sorted_gv_clip_deriv1 = sorted_gv_clip_deriv1[numpy.where(sorted_gv_clip <= 6.0)]
    #sorted_gv_clip_deriv2 = sorted_gv_clip_deriv2[numpy.where(sorted_gv_clip <= 6.0)]
    #sorted_gv_clip = sorted_gv_clip[numpy.where(sorted_gv_clip <= 6.0)]

    # determine thresh by max konkav Kruemmung
    #shadow_max_per_band_thresh_sdev3[b] = sorted_gv_clip[numpy.where(krm == numpy.max(krm))]
    krmThresh[b] = sorted_gv_clip[numpy.where(krm == numpy.max(krm))][0]

    #hmin = numpy.min(sorted_gv)
    #hmax = numpy.max(sorted_gv)
    #bins = numpy.arange(hmin,hmax,(hmax-hmin)/200)
    #histo = numpy.histogram(sorted_gv, bins)
    #histo_cumsum = numpy.cumsum(histo[0])
    #if b == 2 or b == 8 or b == 14 or b == 25:
    #if b == 70:
      #pylab.figure(b+200)
      #pylab.plot(sorted_gv_clip,'r-')
      #pylab.plot(approx_sorted_gv_clip,'g-')
      #pylab.figure(b+202)
      #pylab.plot(sorted_gv_clip_deriv1,'b-')
      #pylab.figure(b+203)
      #pylab.title("2. Ableitung")
      #pylab.plot(sorted_gv_clip_deriv2,'y-')
      #pylab.figure(b+204)
      #pylab.title("Kruemmung")
      #pylab.plot(krm,'y-')
      #pylab.plot(bins[0:len(bins)-1],histo[0])
      #pylab.plot(bins[0:len(bins)-1],histo_cumsum * 100.0 / histo_cumsum[-1])
    print "Max-per-band-Variante:", bandmean + 2 * bandsdev, bandmean + 3 * bandsdev, bandmean + 4 * bandsdev, krmThresh[b]

    
    #low_albedo_mask = numpy.where(inImageGrid[b] > shadow_max_per_band_thresh_sdev2[b], 0, low_albedo_mask)
    #low_albedo_mask = numpy.where(inImageGrid[b] > shadow_max_per_band_thresh_sdev3[b], 0, low_albedo_mask)
    low_albedo_mask = numpy.where(inImageGrid[b] > krmThresh[b], 0, low_albedo_mask)
    #low_albedo_mask = numpy.where(inImageGrid[b] > saturatedThresh[b], 0, low_albedo_mask)
  
  #pylab.figure(223)
  #pylab.plot(shadow_max_per_band_thresh_sdev3,'r-')
  #pylab.plot(shadow_max_per_band_thresh_sdev2,'g-')
  #pylab.plot(krmThresh,'b--')
  #pylab.plot(saturatedThresh,'m--')

  #pylab.figure(224)
  #pylab.imshow(low_albedo_mask)
  #pylab.show()
  
  return low_albedo_mask



# savitzky_golay() taken from: http://scipy-cookbook.readthedocs.org/items/SavitzkyGolay.html
def savitzky_golay(y, window_size, order, deriv=0):
    #Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    #The Savitzky-Golay filter removes high frequency noise from data.
    #It has the advantage of preserving the original shape and
    #features of the signal better than other types of filtering
    #approaches, such as moving averages techhniques.
    #Parameters
    #----------
    #y : array_like, shape (N,)
        #the values of the time history of the signal.
    #window_size : int
        #the length of the window. Must be an odd integer number.
    #order : int
        #the order of the polynomial used in the filtering.
        #Must be less then `window_size` - 1.
    #deriv: int
        #the order of the derivative to compute (default = 0 means only smoothing)
    #Returns
    #-------
    #ys : ndarray, shape (N)
        #the smoothed signal (or it's n-th derivative).
    #Notes
    #-----
    #The Savitzky-Golay is a type of low-pass filter, particularly
    #suited for smoothing noisy data. The main idea behind this
    #approach is to make for each point a least-square fit with a
    #polynomial of high order over a odd-sized window centered at
    #the point.
    #Examples
    #--------
    #import numpy as np
    #t = numpy.linspace(-4, 4, 500)
    #y = numpy.exp( -t**2 ) + numpy.random.normal(0, 0.05, t.shape)
    #ysg = savitzky_golay(y, window_size=31, order=4)
    #import matplotlib.pyplot as plt
    #plt.plot(t, y, label='Noisy signal')
    #plt.plot(t, numpy.exp(-t**2), 'k', lw=1.5, label='Original signal')
    #plt.plot(t, ysg, 'r', label='Filtered signal')
    #plt.legend()
    #plt.show()
    #References
    #----------
    #.. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       #Data by Simplified Least Squares Procedures. Analytical
       #Chemistry, 1964, 36 (8), pp 1627-1639.
    #.. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       #W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       #Cambridge University Press ISBN-13: 9780521880688

    try:
        window_size = numpy.abs(numpy.int(window_size))
        order = numpy.abs(numpy.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size must be a positive odd number")
    if window_size < order + 2:
        print window_size, order
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = numpy.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = numpy.linalg.pinv(b).A[deriv]
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - numpy.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + numpy.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = numpy.concatenate((firstvals, y, lastvals))
    return numpy.convolve( m, y, mode='valid')
    
    
    
    
    
def savitzky_golay_mod(y, window_size, order, deriv=0):
    # Modified after ScyPy Cookbook, http://www.scipy.org/Cookbook/SavitzkyGolay
    #Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    #The Savitzky-Golay filter removes high frequency noise from data.
    #It has the advantage of preserving the original shape and
    #features of the signal better than other types of filtering
    #approaches, such as moving averages techhniques.
    #Parameters
    #----------
    #y : array_like, shape (N,)
        #the values of the time history of the signal.
    #window_size : int
        #the length of the window. Must be an odd integer number.
    #order : int
        #the order of the polynomial used in the filtering.
        #Must be less then `window_size` - 1.
    #deriv: int
        #the order of the derivative to compute (default = 0 means only smoothing)
    #Returns
    #-------
    #ys : ndarray, shape (N)
        #the smoothed signal (or it's n-th derivative).
    #Notes
    #-----
    #The Savitzky-Golay is a type of low-pass filter, particularly
    #suited for smoothing noisy data. The main idea behind this
    #approach is to make for each point a least-square fit with a
    #polynomial of high order over a odd-sized window centered at
    #the point.
    #Examples
    #--------
    #import numpy as np
    #t = numpy.linspace(-4, 4, 500)
    #y = numpy.exp( -t**2 ) + numpy.random.normal(0, 0.05, t.shape)
    #ysg = savitzky_golay(y, window_size=31, order=4)
    #import matplotlib.pyplot as plt
    #plt.plot(t, y, label='Noisy signal')
    #plt.plot(t, numpy.exp(-t**2), 'k', lw=1.5, label='Original signal')
    #plt.plot(t, ysg, 'r', label='Filtered signal')
    #plt.legend()
    #plt.show()
    #References
    #----------
    #.. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       #Data by Simplified Least Squares Procedures. Analytical
       #Chemistry, 1964, 36 (8), pp 1627-1639.
    #.. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       #W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       #Cambridge University Press ISBN-13: 9780521880688

    try:
        window_size = numpy.abs(numpy.int(window_size))
        order = numpy.abs(numpy.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = numpy.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = numpy.linalg.pinv(b).A[deriv]
    # pad the signal at the extremes with
    # values taken from the signal itself
    perc20 = int(y.shape[0] / 100.0 * 10)
    
    #x_pad_start = numpy.arange(0,perc20+1)
    #poly_first = numpy.polyfit(x_pad_start,y[0:perc20+1],17)
    #start = numpy.polyval(poly_first, x_pad_start)
    #firstvals = numpy.polyval(poly_first, x_pad_start - x_pad_start.shape[0])
    #x_pad_end = numpy.arange(y.shape[0]-perc20, y.shape[0])
    #poly_last = numpy.polyfit(x_pad_end,y[y.shape[0]-perc20::],17)
    #end = numpy.polyval(poly_last, x_pad_end)
    #lastvals = numpy.polyval(poly_last, x_pad_end + x_pad_end.shape[0])
    
    
    firstvals = y[0] - numpy.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + numpy.abs(y[-half_window-1:-1][::-1] - y[-1])
    #firstvals = y[0] - numpy.abs( y[1:perc20+1][::-1] - y[0] )
    #lastvals = y[-1] + numpy.abs(y[-perc20-1:-1][::-1] - y[-1])
    y = numpy.concatenate((firstvals, y, lastvals))
    y = numpy.convolve( m, y, mode='valid')
    #y = y[firstvals.shape[0] - window_size : y.shape[0] - lastvals.shape[0] - 1]
    
    #pylab.figure(977)
    #pylab.plot(y,'r-')
    #pylab.plot(x_pad_start,start,'b-')
    #pylab.plot(x_pad_end,end,'b-')
    #pylab.title("mit rand")
    return y


# **** READS AND IMPORTS ENVI IMAGE 2 C_IMAGE *********************************

def ReadEnviImage(filename,v):
  myImageIn = C_Image()
  wvl_tag = 0

  filename_ = string.split(filename,"/")
  if(v == 1):
    print "\n      Reading image file \"%s\" ... \n" % (filename_[-1])

  if(filename[-4] == "."):
    try:
      fp_hdr = open(filename[:-3] + "hdr","r")
    except:
      try:
        fp_hdr = open(filename + ".hdr","r")
      except:
        print "   >>> ERROR : Can not find header file of %s" % (filename_[-1])
        sys.stdout.flush()
        os._exit(1)
  else:
    fp_hdr = open(filename + ".hdr","r")

  # parse header
  for line in fp_hdr:
    feature = string.split(line[:-1],"=",1)
    if(feature[0][0:4] == "samp"):
      myImageIn.nCols = int(feature[1])
    elif(feature[0][0:4] == "line"):
      myImageIn.nRows = int(feature[1])
    elif(feature[0][0:5] == "bands"):
      myImageIn.nBands = int(feature[1])
    elif(feature[0][0:4] == "file"):
      myImageIn.fType = str(feature[1][1:])
    elif(feature[0][0:9] == "data type"):
      myImageIn.dType = int(feature[1])
    elif(feature[0][0:5] == "inter"):
      myImageIn.iLeave = str(feature[1][1:])
    elif(feature[0][0:4] == "byte"):
      myImageIn.bOrder = int(feature[1])
    elif(feature[0][0:5] == "pixel"):
      pixel = string.split(feature[1],",")
      myImageIn.pxSize = float(pixel[0][2:])
      myImageIn.pySize = float(pixel[1][1:])
      unit = string.split(pixel[2],"=")
      myImageIn.pUnit = unit[1].strip()[:-1]
    elif(feature[0][0:3] == "map" and myImageIn.pxSize == None):
      if feature[1][-1] == "\r" or feature[1][-1] == "\n":
        feature[1] = feature[1][0:-1]
      tokens = string.split(feature[1],",")
      if len(tokens[-1].strip()) == 0:
        tokens = tokens[0:-1]
      while tokens[-1].strip()[-1] != "}":
        tokens = numpy.append(tokens,string.split(fp_hdr.next(),","))
        if len(tokens[-1].strip()) == 0:
          tokens = tokens[0:-1]
      myImageIn.pxSize = float(tokens[5])
      myImageIn.pySize = float(tokens[6])
      for i in range(7,len(tokens),1):
        unit = string.split(tokens[i],"=")
        if len(unit) > 1:
          myImageIn.pUnit = unit[1].strip()[:-1]
    elif(feature[0][0:12] == "wavelength u"):
      myImageIn.wvlUnit = feature[1][1:].strip()
  fp_hdr.close()

  #if(v == 1):
  #  print "          Samples\t= " + str(myImageIn.nCols)
  #  print "          Lines\t\t= " + str(myImageIn.nRows)
  #  print "          Bands\t\t= " + str(myImageIn.nBands)
  #if myImageIn.pxSize != None:
  #  print "          PixelSizeX\t= " + str(myImageIn.pxSize) + " " + str(myImageIn.pUnit)
  #  print "          PixelSizeY\t= " + str(myImageIn.pySize) + " " + str(myImageIn.pUnit) + "\n"
  #  else:
  #    print ""

  #if(myImageIn.iLeave != "bsq"):
  #  print "    Attribute Interleave of %s is %s," % (filename,myImageIn.iLeave)
  #  print "    it should be bsq for correct importing."
  #  sys.stdout.flush()
  #  os._exit(1)

  # load wavelength
  if(filename[-4] == "."):
    try:
      fp_hdr = open(filename[:-3] + "hdr","r")
    except:
      try:
        fp_hdr = open(filename + ".hdr","r")
      except:
        print "   >>> ERROR : Can not find header file of %s" % (filename_[-1])
        sys.stdout.flush()
        os._exit(1)
  else:
    fp_hdr = open(filename + ".hdr","r")
  line = fp_hdr.readline()
  wlength = numpy.array([])
  while line:
    if(line[0:12] == "wavelength ="):
      if len(line) > 16:
        if line[12] == "{":
          val = string.split(line[13:],",")
          for vv in val:
            try:
              wlength = numpy.append(wlength,float(vv))
            except:
              try:
                wlength = numpy.append(wlength,float(vv[:-1]))
              except:
                try:
                  wlength = numpy.append(wlength,float(vv[:-2]))
                except:
                  continue
        elif line[13] == "{":
          val = string.split(line[14:],",")
          for vv in val:
            try:
              wlength = numpy.append(wlength,float(vv))
            except:
              try:
                wlength = numpy.append(wlength,float(vv[:-1]))
              except:
                try:
                  wlength = numpy.append(wlength,float(vv[:-2]))
                except:
                  continue
        elif line[14] == "{":
          val = string.split(line[15:],",")
          for vv in val:
            try:
              wlength = numpy.append(wlength,float(vv))
            except:
              try:
                wlength = numpy.append(wlength,float(vv[:-1]))
              except:
                try:
                  wlength = numpy.append(wlength,float(vv[:-2]))
                except:
                  continue
        else:
          print "  >>> ERROR : Reading header attribute \"wavelength\" failed !\n"
          sys.stdout.flush()
          os._exit(1)
      #fp = fp_hdr.tell()
      wvl_tag = 1
      break
    else:
      line = fp_hdr.readline()
  if wvl_tag == 1:
    #if(fp != None):
    #  fp_hdr.seek(fp)
    values = fp_hdr.readline()
    while values:
      val = string.split(values[:-1],",")
      for vv in val:
        try:
          wlength = numpy.append(wlength,float(vv))
        except:
          try:
            wlength = numpy.append(wlength,float(vv[:-1]))
          except:
            try:
              wlength = numpy.append(wlength,float(vv[:-2]))
            except:
              continue
      #print "\"%s\"" % (values[-3])
      if values[-1] == "}":
        break
      elif values[-2] == "}":
        break
      elif values[-3] == "}":
        break
      else:
        values = fp_hdr.readline()
  fp_hdr.close()

  if(myImageIn.wvlUnit == "Micrometers" or myImageIn.wvlUnit == "MICROMETERS" or myImageIn.wvlUnit == "micrometers"):
    myImageIn.wvlUnit = "Nanometers"
    if wvl_tag == 1:
      wlength = wlength*1000.0
  else:
    if wvl_tag == 1:
      if wlength[0] < 100.0:
        wlength = wlength*1000.0
        myImageIn.wvlUnit = "Nanometers"
      else:
        myImageIn.wvlUnit = "Nanometers"
  if wvl_tag == 1:
    myImageIn.wvl = wlength

  # load grid
  fp = open(filename,"rb")
  if(myImageIn.dType == 1):
    myImageIn.grid = numpy.fromfile(fp,numpy.uint8) # unsigned int 8 bit
  elif(myImageIn.dType == 2):
    myImageIn.grid = numpy.fromfile(fp,numpy.int16) # signed int 16 bit
  elif(myImageIn.dType == 3):
    myImageIn.grid = numpy.fromfile(fp,numpy.int32) # signed int 32 bit
  elif(myImageIn.dType == 4):
    myImageIn.grid = numpy.fromfile(fp,numpy.float32) # float 32 bit
  elif(myImageIn.dType == 5):
    myImageIn.grid = numpy.fromfile(fp,numpy.float64) # double 64 bit
  elif(myImageIn.dType == 6):
    myImageIn.grid = numpy.fromfile(fp,numpy.complex64) # complex 2x32 bit
  elif(myImageIn.dType == 9):
    myImageIn.grid = numpy.fromfile(fp,numpy.complex128) # complex 2x64 bit
  elif(myImageIn.dType == 12):
    myImageIn.grid = numpy.fromfile(fp,numpy.uint16) # unsigned int 16 bit
  elif(myImageIn.dType == 13):
    myImageIn.grid = numpy.fromfile(fp,numpy.uint32) # unsigned int 32 bit
  elif(myImageIn.dType == 14):
    myImageIn.grid = numpy.fromfile(fp,numpy.int64) # signed int 64 bit
  elif(myImageIn.dType == 15):
    myImageIn.grid = numpy.fromfile(fp,numpy.uint64) # unsigned int 64 bit
  fp.close

  # formatting grid with nRows, nCols and nBands
  if(myImageIn.nBands == 1):
    myImageIn.grid.shape = (myImageIn.nRows,myImageIn.nCols)
  elif(myImageIn.nBands > 1):
    myImageIn.iLeave = myImageIn.iLeave.strip()
    if myImageIn.iLeave == "bsq" or myImageIn.iLeave == "BSQ":
      myImageIn.grid.shape = (myImageIn.nBands,myImageIn.nRows,myImageIn.nCols)
    elif myImageIn.iLeave == "bil" or myImageIn.iLeave == "BIL":
      dummy = numpy.reshape(myImageIn.grid,(myImageIn.nRows,myImageIn.nBands,myImageIn.nCols)) * 1
      myImageIn.grid = numpy.reshape(myImageIn.grid, (myImageIn.nBands,myImageIn.nRows,myImageIn.nCols)) * 0
      for bd in range(myImageIn.nBands):
        for row in range(myImageIn.nRows):
          myImageIn.grid[bd,row,:] = dummy[row,bd,:]
      myImageIn.iLeave = "bsq"
    elif myImageIn.iLeave == "bip" or myImageIn.iLeave == "BIP":
      dummy = numpy.reshape(myImageIn.grid, (myImageIn.nRows,myImageIn.nCols,myImageIn.nBands)) * 1
      myImageIn.grid = numpy.reshape(myImageIn.grid, (myImageIn.nBands,myImageIn.nRows,myImageIn.nCols)) * 0
      for col in range(myImageIn.nCols):
        for row in range(myImageIn.nRows):
          myImageIn.grid[:,row,col] = dummy[row,col,:]
      myImageIn.iLeave = "bsq"
    else:
      print "\n >>> ERROR : Shaping grid failed !\n"
      print "             Header attribute \"interleave\" not readable or not set.\n"
      print "             Needs bsq, BSQ, bil, BIL, bip, or BIP.\n"
      sys.stdout.flush()
      os._exit(1)

  if(v == 1):
    print "          Samples\t= " + str(myImageIn.nCols)
    print "          Lines\t\t= " + str(myImageIn.nRows)
    print "          Bands\t\t= " + str(myImageIn.nBands)
    if myImageIn.pxSize != None:
      print "          PixelSizeX\t= " + str(myImageIn.pxSize) + " " + str(myImageIn.pUnit)
      print "          PixelSizeY\t= " + str(myImageIn.pySize) + " " + str(myImageIn.pUnit) + "\n"
    else:
      print ""

  return myImageIn




def ImportAndScale(file_1,file_2,datamax,v):
  # file_1: path and file name of hyperspectral VNIR input image
  # file_2: path and file name of ASCII output file of CalcMODTRANvalues.py

  # read ENVI image
  if v == 1:
    print "\n\n   (1) Importing image ...\n"
  inImage = ReadEnviImage(file_1,v)

  # read wavelenghts from ENVI header into array
  wvl = inImage.wvl
  wvl.shape = (len(wvl),1)


  # read ASCII file
  if v == 1 and file_2 != None:
    print "   (1.5) Importing eDIF/eTot spectrum ...\n"
  if file_2 != None: 
    eDif_eTot = ReadTextDataSpectra(file_2,8,v)
    eDif_eTot_wvl = eDif_eTot[:,0:1]
    eDif_eTot = eDif_eTot[:,7:8]
    eDif_eTot = numpy.append(eDif_eTot_wvl,eDif_eTot,axis=1)
    FWHM = numpy.zeros_like(eDif_eTot[:,0])
    #FWHM = numpy.reshape(FWHM, (FWHM.shape[0],1))
    FWHM[0:-1] = eDif_eTot[1::,0] - eDif_eTot[0:-1,0]
    FWHM[-1] = FWHM[-2]
    weights = GenerateSpectralFilterGaussian(eDif_eTot[:,0],wvl[:,0],FWHM)
    #print weights.shape, FWHM.shape, eDif_eTot[:,1:2].shape,wvl.shape
    eDif_eTot_resamp = SpectralResampling(weights,eDif_eTot[:,1:2],wvl)
    #pylab.figure(66)
    #pylab.plot(eDif_eTot[:,0],eDif_eTot[:,1])
    #pylab.plot(eDif_eTot_resamp[:,0],eDif_eTot_resamp[:,1])

  # scale input image to range [0-100]
  #ToDo: Skalierung von Christians IDL-Version uebernehmen
  if v == 1:
    print "   (2) Scaling image to range [0-100] ...\n"


  # Alternative 1 works also with images containing saturated pixel > 100% reflectance.
  # However, this scaling fails with scenes containing a very high percentage (about 80% or more) of very dark pixels like water or shadow. Try alternative 2 then.

  # alternative 1:
  #globalmax = 0
  #for y in range(inImage.grid.shape[1]):
    #xindices = numpy.where(numpy.max(inImage.grid[:,y],0) > 0)
    ##print xindices[0]
    ##print inImage.grid[:,y,xindices[0]]
    #ymean = numpy.mean(inImage.grid[:,y,xindices[0]],axis=1) # mean spectrum over all non-background px per line
    #zmax = numpy.max(ymean)
    #if zmax > globalmax:
      #globalmax = zmax

  # alternative 2:
  #globalmax = numpy.max(inImage.grid)

  # alternative 3: (90% Quantil), die sicherste Variante
  if datamax != 0:
    globalmax = datamax
  else:
    if inImage.grid.shape[1] > inImage.grid.shape[2]:
      x1 = int(inImage.grid.shape[2] / 4)
      x2 = x1 * 2
      x3 = x1 * 3
      x1_2_3 = numpy.append(inImage.grid[:,:,x1],numpy.append(inImage.grid[:,:,x2],inImage.grid[:,:,x3],axis=0),axis=0)
      x1_2_3 = numpy.sort(x1_2_3,axis=None)
      x1_2_3 = x1_2_3[numpy.where(x1_2_3 > 0)]
      globalmax = x1_2_3[int(x1_2_3.shape[0] / 100.0 * 90.0)] # 90% Quantil
    else:
      y1 = int(inImage.grid.shape[1] / 4)
      y2 = y1 * 2
      y3 = y1 * 3
      y1_2_3 = numpy.append(inImage.grid[:,y1,:],numpy.append(inImage.grid[:,y2,:],inImage.grid[:,y3,:],axis=0),axis=0)
      y1_2_3 = numpy.sort(y1_2_3,axis=None)
      y1_2_3 = y1_2_3[numpy.where(y1_2_3 > 0)]
      globalmax = y1_2_3[int(y1_2_3.shape[0] / 100.0 * 90.0)] # 90% Quantil


  if globalmax > 10000:
    inImage.grid = numpy.float32(inImage.grid) / 1000.0
    print "       Scaling input image from range [0,100000] to [0,100]"
    newglobalmax = numpy.max(inImage.grid)
    if newglobalmax > 100:
      print "       WARNING: Image contains oversaturated pixel ( new max = ", newglobalmax, ")"
  elif globalmax > 1000:
    inImage.grid = numpy.float32(inImage.grid) / 100.0
    print "       Scaling input image from range [0,10000] to [0,100]"
    newglobalmax = numpy.max(inImage.grid)
    if newglobalmax > 100:
      print "       WARNING: Image contains oversaturated pixel ( new max = ", newglobalmax, ")"
  elif globalmax > 100:
    inImage.grid = numpy.float32(inImage.grid) / 10.0
    print "       Scaling input image from range [0,1000] to [0,100]"
    newglobalmax = numpy.max(inImage.grid)
    if newglobalmax > 100:
      print "       WARNING: Image contains oversaturated pixel ( new max = ", newglobalmax, ")"
  elif globalmax > 10:
    inImage.grid = numpy.float32(inImage.grid)
    print "       Scaling input image from range [0,100] to [0,100]"
    newglobalmax = numpy.max(inImage.grid)
    if newglobalmax > 100:
      print "       WARNING: Image contains oversaturated pixel ( new max = ", newglobalmax, ")"
  elif globalmax > 1.01:
    inImage.grid = numpy.float32(inImage.grid) * 10.0
    print "       Scaling input image from range [0,10] to [0,100]"
    newglobalmax = numpy.max(inImage.grid)
    if newglobalmax > 100:
      print "       WARNING: Image contains oversaturated pixel ( new max = ", newglobalmax, ")"
  else:
    inImage.grid = numpy.float32(inImage.grid) * 100.0
    print "       Scaling input image from range [0,1] to [0,100]"
    newglobalmax = numpy.max(inImage.grid)
    if newglobalmax > 100:
      print "       WARNING: Image contains oversaturated pixel ( new max = ", newglobalmax, ")"
  print ""
            
  if file_2 == None:
    return inImage, wvl
  else:
    return inImage, wvl, eDif_eTot_resamp, FWHM






def SelectBands(inImage,wvl,eDif_eTot_resamp,v):
  
  # spectral bands or intervals for shadow identification. Numbers are wavelenghts in nanometers.

  first_band = FindBand(wvl,450)  # first band of interest
  last_band = FindBand(wvl,950)  # last band of interest

  #print first_band, last_band, wvl[first_band], wvl[last_band]


  # save RAM, remove SWIR
  if wvl[-1,0] > wvl[last_band]:
    inImage.grid = inImage.grid[first_band:last_band+1]
    wvl = wvl[first_band:last_band+1]
    if eDif_eTot_resamp != None:
      eDif_eTot_resamp = eDif_eTot_resamp[first_band:last_band+1]

  nm860 =  FindBand(wvl,860) # NIR first band

  mean_wvl_nir = numpy.mean(wvl[nm860:last_band+1])
  nir = FindBand(wvl,mean_wvl_nir) # central band of NIR range; algae_slope815

  nm500 =  FindBand(wvl,500) # VIS first band: start where the fraction of diffuse irradiation is low enough
  nm640 = FindBand(wvl,640) # Miller algorithm for calculation of red edge inflexion point
  nm675 =  FindBand(wvl,675) # VIS last band: end so that red edge is not included!
  nm680 = FindBand(wvl,680) # VI
  nm710 = FindBand(wvl,710) # VI, algae_slope710
  nm720 = FindBand(wvl,720) # VI, algae_slope710
  nm740 = FindBand(wvl,740) # algae_slope710
  nm815 = FindBand(wvl,815) # algae_slope815

  nm470w = FindBand(wvl,470) # water_slope 1
  nm520w = FindBand(wvl,520) # water_slope 1
  nm572w = FindBand(wvl,572) # water_slope 2
  nm604w = FindBand(wvl,604) # water_slope 2
  nm650w = FindBand(wvl,650) # water_slope 3
  nm665w = FindBand(wvl,665) # water_slope 3
  nm680w = FindBand(wvl,680) # water_slope 4
  nm740w = FindBand(wvl,740) # water_slope 4
  nm810w = FindBand(wvl,810) # water_slope 5
  nm845w = FindBand(wvl,845) # water_slope 5
  nm910w = FindBand(wvl,910) # water_slope 6
  nm950w = FindBand(wvl,950) # water_slope 6
  
  nm670w = FindBand(wvl,670) # water_slope 7
  nm677w = FindBand(wvl,677) # water_slope 7
  nm765w = FindBand(wvl,765) # water_slope 8
  nm809w = FindBand(wvl,809) # water_slope 8
  
  
  # damit Schatten auf Wasser drin bleibt.
  #first_band = FindBand(wvl,400) # erstes Band
  #purple_blue_start = FindBand(wvl,400) # vom ersten Band an
  #purple_blue_end = FindBand(wvl,490) # bis vor den Cyan/Green Peak von Wasser/Vegetation


  # Wenn Kanal nicht im Range: shiften
  if wvl[nm470w,0] < 470:
    nm470w += 1
  if wvl[nm572w,0] < 572:
    nm572w += 1
  if wvl[nm650w,0] < 650:
    nm650w += 1
  if wvl[nm670w,0] < 670:
    nm670w += 1
  if wvl[nm680w,0] < 680:
    nm680w += 1
  if wvl[nm765w,0] < 765:
    nm765w += 1
  if wvl[nm810w,0] < 810:
    nm810w += 1
  if wvl[nm910w,0] < 910:
    nm910w += 1
    
  # Wenn Kanal nicht im Range: shiften
  if wvl[nm520w,0] > 520:
    nm520w -= 1
  if wvl[nm604w,0] > 604:
    nm604w -= 1
  if wvl[nm665w,0] > 665:
    nm665w -= 1
  if wvl[nm677w,0] > 677:
    nm677w -= 1
  if wvl[nm740w,0] > 740:
    nm740w -= 1
  if wvl[nm809w,0] < 809:
    nm809w += 1
  if wvl[nm845w,0] > 845:
    nm845w -= 1
  if wvl[nm950w,0] > 950:
    nm950w -= 1


  # Wenn durch shift vertauscht: zurueckshiften
  if nm470w > nm520w:
    dummy = nm470w
    nm470w = nm520w
    nm520w = dummy
  if nm572w > nm604w:
    dummy = nm572w
    nm572w = nm604w
    nm604w = dummy
  if nm650w > nm665w:
    dummy = nm650w
    nm650w = nm665w
    nm665w = dummy
  if nm670w > nm677w:
    dummy = nm670w
    nm670w = nm677w
    nm677w = dummy
  if nm680w > nm740w:
    dummy = nm680w
    nm680w = nm740w
    nm740w = dummy
  if nm765w > nm809w:
    dummy = nm765w
    nm765w = nm809w
    nm809w = dummy
  if nm810w > nm845w:
    dummy = nm810w
    nm810w = nm845w
    nm845w = dummy
  if nm910w > nm950w:
    dummy = nm910w
    nm910w = nm950w
    nm950w = dummy


  # Wenn durch shiften gleich: einen zurueckshiften
  if nm470w == nm520w:
    difflinks = 470 - wvl[nm470w-1,0]
    diffrechts = wvl[nm470w+1,0] - 520
    if difflinks < diffrechts:
      nm470w -= 1
    else:
      nm520w += 1
  if nm572w == nm604w:
    difflinks = 572 - wvl[nm572w-1,0]
    diffrechts = wvl[nm572w+1,0] - 604
    if difflinks < diffrechts:
      nm572w -= 1
    else:
      nm604w += 1
  if nm650w == nm665w:
    difflinks = 650 - wvl[nm650w-1,0]
    diffrechts = wvl[nm650w+1,0] - 665
    if difflinks < diffrechts:
      nm650w -= 1
    else:
      nm665w += 1
  if nm670w == nm677w:
    difflinks = 670 - wvl[nm670w-1,0]
    diffrechts = wvl[nm670w+1,0] - 677
    if difflinks < diffrechts:
      nm670w -= 1
    else:
      nm677w += 1
  if nm680w == nm740w:
    difflinks = 680 - wvl[nm680w-1,0]
    diffrechts = wvl[nm680w+1,0] - 740
    if difflinks < diffrechts:
      nm680w -= 1
    else:
      nm740w += 1
      
  if nm765w == nm809w:
    difflinks = 765 - wvl[nm765w-1,0]
    diffrechts = wvl[nm765w+1,0] - 809
    if difflinks < diffrechts:
      nm765w -= 1
    else:
      nm809w += 1
  if nm810w == nm845w:
    difflinks = 810 - wvl[nm810w-1,0]
    diffrechts = wvl[nm810w+1,0] - 845
    if difflinks < diffrechts:
      nm810w -= 1
    else:
      nm845w += 1
      
  if nm910w == nm950w:
    difflinks = 910 - wvl[nm910w-1,0]
    diffrechts = wvl[nm910w+1,0] - 950
    if difflinks < diffrechts:
      nm910w -= 1
    else:
      nm950w += 1

  if eDif_eTot_resamp == None:
    return inImage,wvl,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w
  else:
    return inImage,wvl,eDif_eTot_resamp,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w



def FindBand(spectra,bandCentre):
  # spectra [2D nadarray] = input spectra
  # bandCentre [float] = value of band centre to find
  dist = 9999999.9
  for i in range(spectra.shape[0]):
    if(dist > abs(bandCentre - spectra[i][0])):
      dist = abs(bandCentre - spectra[i][0])
      index = i
  return index



def SelectBands_EARSeL(inImage,wvl,eDif_eTot_resamp,v):
  
  # spectral bands or intervals for shadow identification. Numbers are wavelenghts in nanometers.

  first_band = FindBand(wvl,450)  # first band of interest
  last_band = FindBand(wvl,900)  # last band of interest
  
  #print first_band, last_band, wvl[first_band], wvl[last_band]


  # save RAM, remove SWIR
  if wvl[-1,0] > wvl[last_band]:
    inImage.grid = inImage.grid[first_band:last_band+1]
    wvl = wvl[first_band:last_band+1]
    if eDif_eTot_resamp != None:
      eDif_eTot_resamp = eDif_eTot_resamp[first_band:last_band+1]

  nm860 =  FindBand(wvl,860) # NIR first band

  mean_wvl_nir = numpy.mean(wvl[nm860:last_band+1])
  nir = FindBand(wvl,mean_wvl_nir) # central band of NIR range; algae_slope815

  nm500 =  FindBand(wvl,500) # VIS first band: start where the fraction of diffuse irradiation is low enough
  nm640 = FindBand(wvl,640) # Miller algorithm for calculation of red edge inflexion point
  nm675 =  FindBand(wvl,675) # VIS last band: end so that red edge is not included!
  nm680 = FindBand(wvl,680) # VI
  nm710 = FindBand(wvl,710) # VI, algae_slope710
  nm720 = FindBand(wvl,720) # VI, algae_slope710
  nm740 = FindBand(wvl,740) # algae_slope710
  nm815 = FindBand(wvl,815) # algae_slope815

  nm470w = FindBand(wvl,470) # water_slope 1
  nm520w = FindBand(wvl,520) # water_slope 1
  nm572w = FindBand(wvl,572) # water_slope 2
  nm604w = FindBand(wvl,604) # water_slope 2
  nm650w = FindBand(wvl,650) # water_slope 3
  nm665w = FindBand(wvl,665) # water_slope 3
  nm680w = FindBand(wvl,680) # water_slope 4
  nm740w = FindBand(wvl,740) # water_slope 4
  nm810w = FindBand(wvl,810) # water_slope 5
  nm845w = FindBand(wvl,845) # water_slope 5
  nm910w = FindBand(wvl,910) # water_slope 6
  nm950w = FindBand(wvl,950) # water_slope 6
  
  nm670w = FindBand(wvl,670) # water_slope 7
  nm677w = FindBand(wvl,677) # water_slope 7
  nm765w = FindBand(wvl,765) # water_slope 8
  nm809w = FindBand(wvl,809) # water_slope 8
  
  
  # damit Schatten auf Wasser drin bleibt.
  #first_band = FindBand(wvl,400) # erstes Band
  #purple_blue_start = FindBand(wvl,400) # vom ersten Band an
  #purple_blue_end = FindBand(wvl,490) # bis vor den Cyan/Green Peak von Wasser/Vegetation


  # Wenn Kanal nicht im Range: shiften
  if wvl[nm470w,0] < 470:
    nm470w += 1
  if wvl[nm572w,0] < 572:
    nm572w += 1
  if wvl[nm650w,0] < 650:
    nm650w += 1
  if wvl[nm670w,0] < 670:
    nm670w += 1
  if wvl[nm680w,0] < 680:
    nm680w += 1
  if wvl[nm765w,0] < 765:
    nm765w += 1
  if wvl[nm810w,0] < 810:
    nm810w += 1
  if wvl[nm910w,0] < 910:
    nm910w += 1
    
  # Wenn Kanal nicht im Range: shiften
  if wvl[nm520w,0] > 520:
    nm520w -= 1
  if wvl[nm604w,0] > 604:
    nm604w -= 1
  if wvl[nm665w,0] > 665:
    nm665w -= 1
  if wvl[nm677w,0] > 677:
    nm677w -= 1
  if wvl[nm740w,0] > 740:
    nm740w -= 1
  if wvl[nm809w,0] < 809:
    nm809w += 1
  if wvl[nm845w,0] > 845:
    nm845w -= 1
  if wvl[nm950w,0] > 950:
    nm950w -= 1


  # Wenn durch shift vertauscht: zurueckshiften
  if nm470w > nm520w:
    dummy = nm470w
    nm470w = nm520w
    nm520w = dummy
  if nm572w > nm604w:
    dummy = nm572w
    nm572w = nm604w
    nm604w = dummy
  if nm650w > nm665w:
    dummy = nm650w
    nm650w = nm665w
    nm665w = dummy
  if nm670w > nm677w:
    dummy = nm670w
    nm670w = nm677w
    nm677w = dummy
  if nm680w > nm740w:
    dummy = nm680w
    nm680w = nm740w
    nm740w = dummy
  if nm765w > nm809w:
    dummy = nm765w
    nm765w = nm809w
    nm809w = dummy
  if nm810w > nm845w:
    dummy = nm810w
    nm810w = nm845w
    nm845w = dummy
  if nm910w > nm950w:
    dummy = nm910w
    nm910w = nm950w
    nm950w = dummy


  # Wenn durch shiften gleich: einen zurueckshiften
  if nm470w == nm520w:
    difflinks = 470 - wvl[nm470w-1,0]
    diffrechts = wvl[nm470w+1,0] - 520
    if difflinks < diffrechts:
      nm470w -= 1
    else:
      nm520w += 1
  if nm572w == nm604w:
    difflinks = 572 - wvl[nm572w-1,0]
    diffrechts = wvl[nm572w+1,0] - 604
    if difflinks < diffrechts:
      nm572w -= 1
    else:
      nm604w += 1
  if nm650w == nm665w:
    difflinks = 650 - wvl[nm650w-1,0]
    diffrechts = wvl[nm650w+1,0] - 665
    if difflinks < diffrechts:
      nm650w -= 1
    else:
      nm665w += 1
  if nm670w == nm677w:
    difflinks = 670 - wvl[nm670w-1,0]
    diffrechts = wvl[nm670w+1,0] - 677
    if difflinks < diffrechts:
      nm670w -= 1
    else:
      nm677w += 1
  if nm680w == nm740w:
    difflinks = 680 - wvl[nm680w-1,0]
    diffrechts = wvl[nm680w+1,0] - 740
    if difflinks < diffrechts:
      nm680w -= 1
    else:
      nm740w += 1
      
  if nm765w == nm809w:
    difflinks = 765 - wvl[nm765w-1,0]
    diffrechts = wvl[nm765w+1,0] - 809
    if difflinks < diffrechts:
      nm765w -= 1
    else:
      nm809w += 1
  if nm810w == nm845w:
    difflinks = 810 - wvl[nm810w-1,0]
    diffrechts = wvl[nm810w+1,0] - 845
    if difflinks < diffrechts:
      nm810w -= 1
    else:
      nm845w += 1
      
  if nm910w == nm950w:
    difflinks = 910 - wvl[nm910w-1,0]
    diffrechts = wvl[nm910w+1,0] - 950
    if difflinks < diffrechts:
      nm910w -= 1
    else:
      nm950w += 1

  if eDif_eTot_resamp == None:
    return inImage,wvl,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w
  else:
    return inImage,wvl,eDif_eTot_resamp,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w

  


def CalcSpecFea(justCombIm,inImage,wvl,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w,v):

  if v == 1:
    print "   (3) Calculating spectral features ...\n"


  ### spectral features ###

  nm710_max = numpy.zeros_like(inImage.grid[0:2])
  nm710_max[1] = numpy.max(inImage.grid[nm710:nm720+1], axis=0) # value
  nm710_max[0] = FindBandWithValue(inImage.grid[nm710:nm720+1],nm710_max[1],wvl[nm710:nm720+1]) # wvl

  specmean = numpy.mean(numpy.float64(inImage.grid[first_band:last_band+1]), dtype=numpy.float64, axis=0)
  inImage.grid[nm680] = numpy.where(inImage.grid[nm680] < 1.0e-6,1.0e-6,inImage.grid[nm680])
  VI = nm710_max[1] / inImage.grid[nm680] # max(710,720) / 680
  mean_nir = numpy.mean(inImage.grid[nm860:last_band+1],axis=0)
  mean_nir = numpy.where(mean_nir < 1.0e-6, 1.0e-6, mean_nir)
  algae_slope710 = (inImage.grid[nm740] - nm710_max[1]) / (wvl[nm740,0] - nm710_max[0])  # ToDo: stattdessen slope mit numpy.polyfit() testen
  algae_slope815 = (mean_nir - inImage.grid[nm815]) / (wvl[nir,0] - wvl[nm815,0])  # ToDo: stattdessen slope mit numpy.polyfit() testen
  max_vis = numpy.max(inImage.grid[nm500:nm675+1],axis=0)
  vis_nir_ratio = max_vis / mean_nir
  #max_purple_blue = numpy.max(inImage.grid[purple_blue_start:purple_blue_end+1,y,x])
  #purple_vis_ratio = max_purple_blue / max_vis
  
  
  # water slopes
  waterslopes = numpy.zeros((6, inImage.grid.shape[1] * inImage.grid.shape[2]), dtype=numpy.uint8) # speichert die Kombinationen der Steigungen ab
 
  specs_in_cols = numpy.reshape(inImage.grid[nm470w:nm520w+1],(inImage.grid[nm470w:nm520w+1].shape[0],-1))
  coeff1 = numpy.polyfit(wvl[nm470w:nm520w+1,0],specs_in_cols, 1, full=False)
  #coeff1 = numpy.polyfit(wvl[nm470w:nm520w+1,0],specs_in_cols, 1, full=True)
  waterslopes[0] = numpy.where(coeff1[0,:] < 0,1,0)
  if justCombIm:
    del coeff1

  specs_in_cols = numpy.reshape(inImage.grid[nm572w:nm604w+1],(inImage.grid[nm572w:nm604w+1].shape[0],-1))
  coeff2 = numpy.polyfit(wvl[nm572w:nm604w+1,0],specs_in_cols, 1, full=False)
  waterslopes[1] = numpy.where(coeff2[0,:] < 0,2,0)
  if justCombIm:
    del coeff2

  specs_in_cols = numpy.reshape(inImage.grid[nm650w:nm665w+1],(inImage.grid[nm650w:nm665w+1].shape[0],-1))
  coeff3 = numpy.polyfit(wvl[nm650w:nm665w+1,0],specs_in_cols, 1, full=False)
  waterslopes[2] = numpy.where(coeff3[0,:] < 0,4,0)
  if justCombIm:
    del coeff3

  specs_in_cols = numpy.reshape(inImage.grid[nm680w:nm740w+1],(inImage.grid[nm680w:nm740w+1].shape[0],-1))
  coeff4 = numpy.polyfit(wvl[nm680w:nm740w+1,0],specs_in_cols, 1, full=False)
  waterslopes[3] = numpy.where(coeff4[0,:] < 0,8,0)
  if justCombIm:
    del coeff4

  specs_in_cols = numpy.reshape(inImage.grid[nm810w:nm845w+1],(inImage.grid[nm810w:nm845w+1].shape[0],-1))
  coeff5 = numpy.polyfit(wvl[nm810w:nm845w+1,0],specs_in_cols, 1, full=False)
  waterslopes[4] = numpy.where(coeff5[0,:] < 0,16,0)
  if justCombIm:
    del coeff5

  specs_in_cols = numpy.reshape(inImage.grid[nm910w:nm950w+1],(inImage.grid[nm910w:nm950w+1].shape[0],-1))
  coeff6 = numpy.polyfit(wvl[nm910w:nm950w+1,0],specs_in_cols, 1, full=False)
  waterslopes[5] = numpy.where(coeff6[0,:] < 0,32,0)
  if justCombIm:
    del coeff6

  #specs_in_cols = numpy.reshape(inImage.grid[nm670w:nm677w+1],(inImage.grid[nm670w:nm677w+1].shape[0],-1))
  #coeff7 = numpy.polyfit(wvl[nm670w:nm677w+1,0],specs_in_cols, 1, full=True)

  #specs_in_cols = numpy.reshape(inImage.grid[nm765w:nm809w+1],(inImage.grid[nm765w:nm809w+1].shape[0],-1))
  #coeff8 = numpy.polyfit(wvl[nm765w:nm809w+1,0],specs_in_cols, 1, full=True)

  #specs_in_cols = numpy.reshape(inImage.grid[first_band:last_band+1],(inImage.grid[first_band:last_band+1].shape[0],-1))
  #fullspec_polyfit1_coeff = numpy.polyfit(wvl[first_band:last_band+1,0],specs_in_cols, 1, full=False)
  #fullspec_polyfit1_coeff = numpy.reshape(fullspec_polyfit1_coeff[0][0,:],(inImage.grid.shape[1],inImage.grid.shape[2]))

  # coeff1 = tuple; coeff1[0] = 2darray mit Polynomkoeffizienten; coeff1[1] = 1darray mit RMSE der Pixel
  #print coeff1[0][:,0] # Polynomkoeffizienten des ersten Pixels

  #waterslopes[0] = numpy.where(coeff1[0][0,:] < 0,1,0)
  #waterslopes[1] = numpy.where(coeff2[0][0,:] < 0,2,0)
  #waterslopes[2] = numpy.where(coeff3[0][0,:] < 0,4,0)
  #waterslopes[3] = numpy.where(coeff4[0][0,:] < 0,8,0)
  #waterslopes[4] = numpy.where(coeff5[0][0,:] < 0,16,0)
  #waterslopes[5] = numpy.where(coeff6[0][0,:] < 0,32,0) 
  #waterslopes[2] = numpy.where(coeff3[0][0,:] > 0,4,0) # Achtung: groesser !
  
  if not justCombIm:
    coeff1 = numpy.reshape(coeff1,(coeff1.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff2 = numpy.reshape(coeff2,(coeff2.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff3 = numpy.reshape(coeff3,(coeff3.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff4 = numpy.reshape(coeff4,(coeff4.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff5 = numpy.reshape(coeff5,(coeff5.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff6 = numpy.reshape(coeff6,(coeff6.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
  waterslopes = numpy.reshape(waterslopes,(waterslopes.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
  
  #pylab.figure()
  #pylab.title('specmean')
  #pylab.imshow(specmean)
  #pylab.colorbar()

  #pylab.figure()
  #pylab.title('VI')
  #pylab.imshow(VI)
  #pylab.colorbar()

  if justCombIm:
    return nm710_max,specmean,VI,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,specs_in_cols,waterslopes
  else:
    return nm710_max,specmean,VI,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,specs_in_cols,coeff1,coeff2,coeff3,coeff4,coeff5,coeff6,waterslopes





def CalcSpecFea_5Slopes(justCombIm,inImage,wvl,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w,v):

  if v == 1:
    print "   (3) Calculating spectral features ...\n"


  ### spectral features ###

  nm710_max = numpy.zeros_like(inImage.grid[0:2])
  nm710_max[1] = numpy.max(inImage.grid[nm710:nm720+1], axis=0) # value
  nm710_max[0] = FindBandWithValue(inImage.grid[nm710:nm720+1],nm710_max[1],wvl[nm710:nm720+1]) # wvl

  specmean = numpy.mean(numpy.float64(inImage.grid[first_band:last_band+1]), dtype=numpy.float64, axis=0)
  inImage.grid[nm680] = numpy.where(inImage.grid[nm680] < 1.0e-6,1.0e-6,inImage.grid[nm680])
  VI = nm710_max[1] / inImage.grid[nm680] # max(710,720) / 680
  mean_nir = numpy.mean(inImage.grid[nm860:last_band+1],axis=0)
  mean_nir = numpy.where(mean_nir < 1.0e-6, 1.0e-6, mean_nir)
  algae_slope710 = (inImage.grid[nm740] - nm710_max[1]) / (wvl[nm740,0] - nm710_max[0])  # ToDo: stattdessen slope mit numpy.polyfit() testen
  algae_slope815 = (mean_nir - inImage.grid[nm815]) / (wvl[nir,0] - wvl[nm815,0])  # ToDo: stattdessen slope mit numpy.polyfit() testen
  max_vis = numpy.max(inImage.grid[nm500:nm675+1],axis=0)
  vis_nir_ratio = max_vis / mean_nir
  #max_purple_blue = numpy.max(inImage.grid[purple_blue_start:purple_blue_end+1,y,x])
  #purple_vis_ratio = max_purple_blue / max_vis
  
  
  # water slopes
  waterslopes = numpy.zeros((5, inImage.grid.shape[1] * inImage.grid.shape[2]), dtype=numpy.uint8) # speichert die Kombinationen der Steigungen ab
  
  specs_in_cols = numpy.reshape(inImage.grid[nm572w:nm604w+1],(inImage.grid[nm572w:nm604w+1].shape[0],-1))
  coeff1 = numpy.polyfit(wvl[nm572w:nm604w+1,0],specs_in_cols, 1, full=False)
  waterslopes[0] = numpy.where(coeff1[0,:] < 0,1,0)
  if justCombIm:
    del coeff1

  specs_in_cols = numpy.reshape(inImage.grid[nm650w:nm665w+1],(inImage.grid[nm650w:nm665w+1].shape[0],-1))
  coeff2 = numpy.polyfit(wvl[nm650w:nm665w+1,0],specs_in_cols, 1, full=False)
  waterslopes[1] = numpy.where(coeff2[0,:] < 0,2,0)
  if justCombIm:
    del coeff2

  specs_in_cols = numpy.reshape(inImage.grid[nm670w:nm677w+1],(inImage.grid[nm670w:nm677w+1].shape[0],-1))
  coeff3 = numpy.polyfit(wvl[nm670w:nm677w+1,0],specs_in_cols, 1, full=False)
  waterslopes[2] = numpy.where(coeff3[0,:] > 0,4,0)
  if justCombIm:
    del coeff3

  specs_in_cols = numpy.reshape(inImage.grid[nm680w:nm740w+1],(inImage.grid[nm680w:nm740w+1].shape[0],-1))
  coeff4 = numpy.polyfit(wvl[nm680w:nm740w+1,0],specs_in_cols, 1, full=False)
  waterslopes[3] = numpy.where(coeff4[0,:] < 0,8,0)
  if justCombIm:
    del coeff4

  specs_in_cols = numpy.reshape(inImage.grid[nm810w:last_band+1],(inImage.grid[nm810w:last_band+1].shape[0],-1))
  coeff5 = numpy.polyfit(wvl[nm810w:last_band+1,0],specs_in_cols, 1, full=False)
  waterslopes[4] = numpy.where(coeff5[0,:] < 0,16,0)
  if justCombIm:
    del coeff5

  #specs_in_cols = numpy.reshape(inImage.grid[nm670w:nm677w+1],(inImage.grid[nm670w:nm677w+1].shape[0],-1))
  #coeff7 = numpy.polyfit(wvl[nm670w:nm677w+1,0],specs_in_cols, 1, full=True)

  #specs_in_cols = numpy.reshape(inImage.grid[nm765w:nm809w+1],(inImage.grid[nm765w:nm809w+1].shape[0],-1))
  #coeff8 = numpy.polyfit(wvl[nm765w:nm809w+1,0],specs_in_cols, 1, full=True)

  #specs_in_cols = numpy.reshape(inImage.grid[first_band:last_band+1],(inImage.grid[first_band:last_band+1].shape[0],-1))
  #fullspec_polyfit1_coeff = numpy.polyfit(wvl[first_band:last_band+1,0],specs_in_cols, 1, full=False)
  #fullspec_polyfit1_coeff = numpy.reshape(fullspec_polyfit1_coeff[0][0,:],(inImage.grid.shape[1],inImage.grid.shape[2]))

  # coeff1 = tuple; coeff1[0] = 2darray mit Polynomkoeffizienten; coeff1[1] = 1darray mit RMSE der Pixel
  #print coeff1[0][:,0] # Polynomkoeffizienten des ersten Pixels

  #waterslopes[0] = numpy.where(coeff1[0][0,:] < 0,1,0)
  #waterslopes[1] = numpy.where(coeff2[0][0,:] < 0,2,0)
  #waterslopes[2] = numpy.where(coeff3[0][0,:] < 0,4,0)
  #waterslopes[3] = numpy.where(coeff4[0][0,:] < 0,8,0)
  #waterslopes[4] = numpy.where(coeff5[0][0,:] < 0,16,0)
  #waterslopes[5] = numpy.where(coeff6[0][0,:] < 0,32,0) 
  #waterslopes[2] = numpy.where(coeff3[0][0,:] > 0,4,0) # Achtung: groesser !
  
  if not justCombIm:
    coeff1 = numpy.reshape(coeff1,(coeff1.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff2 = numpy.reshape(coeff2,(coeff2.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff3 = numpy.reshape(coeff3,(coeff3.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff4 = numpy.reshape(coeff4,(coeff4.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
    coeff5 = numpy.reshape(coeff5,(coeff5.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
  waterslopes = numpy.reshape(waterslopes,(waterslopes.shape[0],inImage.grid.shape[1],inImage.grid.shape[2]))
  
  #pylab.figure()
  #pylab.title('specmean')
  #pylab.imshow(specmean)
  #pylab.colorbar()

  #pylab.figure()
  #pylab.title('VI')
  #pylab.imshow(VI)
  #pylab.colorbar()

  if justCombIm:
    return nm710_max,specmean,VI,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,specs_in_cols,waterslopes
  else:
    return nm710_max,specmean,VI,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,specs_in_cols,coeff1,coeff2,coeff3,coeff4,coeff5,waterslopes




def CalcLocalMinimaIndices(vals, window=5):
  # Modified after http://mail.scipy.org/pipermail/scipy-user/2008-August/017886.html
  if len(vals) < window:
    print "\n  >>> ERROR in CalcLocalMinimaIndices(): Array vals is too short for finding local minima by filtering: len(vals) = ", len(vals)
    sys.stdout.flush()
    os._exit(1)
  minvals = ndimage.minimum_filter(vals, size=window, mode="nearest")
  minima_mask = vals == minvals
  indices = numpy.arange(len(vals))[minima_mask]
  return indices




def CalcLocalMaximaIndices(vals, window=5):
  # Modified after http://mail.scipy.org/pipermail/scipy-user/2008-August/017886.html
  if len(vals) < window:
    print "\n  >>> ERROR in CalcLocalMaximaIndices(): Array vals is too short for finding local maxima by filtering: len(vals) = ", len(vals)
    sys.stdout.flush()
    os._exit(1)
  maxvals = ndimage.maximum_filter(vals, size=window, mode="nearest")
  maxima_mask = vals == maxvals
  indices = numpy.arange(len(vals))[maxima_mask]
  return indices



def CalcLowAlbedoMaskByHistoThresh(inImage,wvl,bgmask,mean_nir,filename):
  
  infilename = os.path.split(filename)[-1]

  mean_nir = numpy.reshape(mean_nir,inImage.grid.shape[1] * inImage.grid.shape[2])
  mean_nir_histo = numpy.histogram(mean_nir, bins=101, range=(0,100))
  
  thresh = 10 # bei 10 % Refl. befindet sich im Histogramm in etwa das Minimum zwischen Wasser und Nicht-Wasser
  index = numpy.where(mean_nir > thresh)
  if len(index[0]) == 0:
    imeanval = 30
  else:
    imeanval = numpy.int(numpy.mean(mean_nir[index] + 0.5))
  if (imeanval - thresh) < 4:
    imeanval = 30
  if imeanval >= len(mean_nir_histo[0]):
    imeanval = len(mean_nir_histo[0]) - 1 
    
  #print imeanval
  #print len(mean_nir_histo[0])
  
  mean_nir = numpy.reshape(mean_nir,(inImage.grid.shape[1], inImage.grid.shape[2]))
    
  left_end = 1 # die Null ausschliessen, weil Background value
  
  # index of first maximum
  ifirstmax = CalcLocalMaximaIndices(mean_nir_histo[0][left_end:imeanval+1],window=3)[0] + left_end
  #print ifirstmax, imeanval
  if imeanval - ifirstmax < 4:
    imeanval = imeanval + (4 - (imeanval - ifirstmax))
  inextmin = CalcLocalMinimaIndices(mean_nir_histo[0][ifirstmax:imeanval+1])[0] + ifirstmax
  if imeanval - inextmin < 4:
    imeanval = imeanval + (4 - (imeanval - inextmin))
  right_end = CalcLocalMaximaIndices(mean_nir_histo[0][inextmin:imeanval+1]) + inextmin
  if len(right_end) > 1:
    i = 0
    while  right_end[i+1] - right_end[i] == 1: # wenn benachbarte Histogrammwerte gleich sind, werden sie alle faelschlicherweise als Maxima erkannt
      i = i + 1
      if i == len(right_end) - 1:
        break
    if i < len(right_end) - 1 and i > 0:
      i = i + 1
    right_end = right_end[i]
  else:
    right_end = right_end
    
  # Wenn sehr viel Wasser oder Schatten die Szene dominiert, ist der Polynomfit schlecht, weil mean_nir_histo[ifirstmax] sehr hoch ist und das Polynom schwingt.
  shift = 0
  dummy = None
  while mean_nir_histo[0][ifirstmax + shift] > 5 * mean_nir_histo[0][right_end]:
    shift = shift + 1
  if ifirstmax + shift == inextmin:
    shift = shift - 1
    dummy = mean_nir_histo[0][ifirstmax + shift]
    mean_nir_histo[0][ifirstmax + shift] = 3 * mean_nir_histo[0][right_end]
  
  #print "Mittelwert im NIR (% refl) = ", imeanval #/ 2.0
  #print "linker max (% refl) = ", ifirstmax #/ 2.0
  #print "erster min (% refl) = ", inextmin #/ 2.0
  #print "rechter max (% refl) = ", right_end #/ 2.0
  
  # Polynomial approx of histogram
  histo_approx = numpy.polyfit(mean_nir_histo[1][ifirstmax + shift:right_end+1],mean_nir_histo[0][ifirstmax + shift:right_end+1], 3, full=False)
  p = numpy.poly1d(histo_approx)

  # Korrektur zuruecksetzen
  if dummy != None:
    mean_nir_histo[0][ifirstmax + shift] = dummy

  # index of first minimum of polynom
  localmins = CalcLocalMinimaIndices(p(mean_nir_histo[1][ifirstmax:right_end+1])) + ifirstmax
  if len(localmins) == 1:
    histothresh = localmins
  else:
    histothresh = localmins[0]

  # Wenn kaum Wasser und Schatten im Bild, ist der Polynomfit schlecht, weil mean_nir_histo[ifirstmax] sehr niedrig ist und das Polynom vorne nach unten zieht. Das erste Minimum des Polynoms liegt dann direkt auf ifirstmax. Das ist etwas zu gering. Einer mehr reicht aber schon.
  if histothresh == ifirstmax:
    histothresh = histothresh + 1
  #print histothresh

  low_albedo_mask = numpy.where(mean_nir >= histothresh,0,bgmask)

  
  #pylab.figure(1779)
  #pylab.plot(mean_nir_histo[1][:-1],mean_nir_histo[0],'k')
  #pylab.plot([mean_nir_histo[1][ifirstmax]],[mean_nir_histo[0][ifirstmax]],'ro')
  #pylab.plot([mean_nir_histo[1][right_end]],[mean_nir_histo[0][right_end]],'ro')
  ##pylab.plot([mean_nir_histo[1][imeanval]],[mean_nir_histo[0][imeanval]],'yo')
  #pylab.plot(mean_nir_histo[1][ifirstmax:right_end+1],p(mean_nir_histo[1][ifirstmax:right_end+1]),'m--',lw=2)
  #pylab.plot(mean_nir_histo[1][histothresh],p(mean_nir_histo[1][histothresh]),'go')
  #pylab.title("Histogram of NIR spectral mean image for " + infilename)
  #pylab.xlabel('Reflectance [%]')
  #pylab.ylabel('Number of pixels')
  
  #pylab.figure(1781)
  #pylab.plot(mean_nir_histo[1][ifirstmax:right_end+1],mean_nir_histo[0][ifirstmax:right_end+1],'k')
  #pylab.plot(mean_nir_histo[1][ifirstmax:right_end+1],p(mean_nir_histo[1][ifirstmax:right_end+1]),'m--',lw=2)
  ##x = mean_nir_histo[1][numpy.int((histothresh - 2) * 2 + 0.5)]
  ##y = p(mean_nir_histo[1][numpy.int((histothresh - 2) * 2 + 0.5)])
  #pylab.plot(mean_nir_histo[1][histothresh],p(mean_nir_histo[1][histothresh]),'go')
  ##pylab.plot([10], [3],'ro') # test
  #pylab.title("Subset for polynomial approximation (Rheinsberg)") # for " + infilename)
  #pylab.xlabel('Reflectance [%]')
  #pylab.ylabel('Number of pixels')

  
  #pylab.figure(18)
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0.8,0,0)], N=None)
  #pylab.imshow(low_albedo_mask,interpolation="Nearest",cmap=myCmap)
  #pylab.title("Low albedo mask")
  
  #pylab.figure(155)
  #pylab.title('True color composite')
  #rgb = numpy.append(numpy.append(numpy.reshape(inImage.grid[FindBand(wvl,460)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), numpy.reshape(inImage.grid[FindBand(wvl,550)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), axis=0), numpy.reshape(inImage.grid[FindBand(wvl,640)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), axis=0)
  #rgb_bip = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2],3),dtype=numpy.float)
  #for y in range(inImage.grid.shape[1]):
    #for x in range(inImage.grid.shape[2]):
      #bb=0
      #for b in range(2,0,-1):
        #rgb_bip[y,x,bb] = rgb[b,y,x] / 30
        #bb+=1
  #rgb_bip = numpy.where(rgb_bip > 1.0,1.0,rgb_bip)
  #pylab.imshow(rgb_bip,interpolation="Nearest")
  
  #pylab.show()
  
  return low_albedo_mask



def CalcLowAlbedoMaskByShadowSimulation(inImage,bgmask,eDif_eTot_resamp,nm680):
  # specmax ueber x und y auf Datensatz (Erg: 1 Spektrum mit hellstem Reflexionswert pro Kanal). Das mit eDif / eTot multiplizieren. Alle Pixel die in jedem Kanal unter dem Wert dieses Spektrums liegen, bilden die vorlaeufige Schattenmaske.
  
  # generate shadow image
  shadowim = numpy.zeros_like(inImage.grid)
  for y in range(inImage.grid.shape[1]):
    for x in range(inImage.grid.shape[2]):
      shadowim[:,y,x] = inImage.grid[:,y,x] * eDif_eTot_resamp[:,1]
      #print shadowim[:,y,x]
  saturatedPxMask = numpy.where(numpy.max(inImage.grid,axis=0) > 100.0, 1, 0)
  saturatedThresh = numpy.zeros(inImage.grid.shape[0],dtype=numpy.float)
  for b in range(inImage.grid.shape[0]):
    try:
      saturatedThresh[b] = numpy.min(numpy.sort(shadowim[b][numpy.where(inImage.grid[b] > 100.0)]))
    except:
      saturatedThresh[b] = numpy.max(shadowim[b]) + 1
  #print saturatedThresh[b]
  shadowim[b][numpy.where(saturatedPxMask == 1)] = 0
  #pylab.figure(300)
  #pylab.plot(numpy.sort(shadowim[numpy.where(inImage.grid > 100.0)]))
  #pylab.show()
  
  # Variante max_per_band
  #low_albedo_mask = smf.LowAlbedoThreshbyMaxPerBand(smask, inImage.grid, shadowim, nm680, saturatedThresh)
  
  # Variante shadow_specmean
  low_albedo_mask = LowAlbedoThreshbyMean(bgmask, inImage.grid, shadowim[0:nm680+1], saturatedThresh)
  low_albedo_mask *= LowAlbedoThreshbyMean(bgmask, inImage.grid, shadowim[nm680+4::], saturatedThresh)
  
  #pylab.figure(18)
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0.8,0,0)], N=None)
  #pylab.imshow(low_albedo_mask,interpolation="Nearest",cmap=myCmap)
  #pylab.title("Low albedo mask")
  #pylab.show()
  return low_albedo_mask
  
  
  
  
def FillHoles(smask,wmask):  
  # fill up one pixel sized holes
  
  # ToDo: Um eine thematische Karte mit Gewaessern (als separate Ausgabe) zu erzeugen, waere es sinnvoll, durch Schiffe erzeugte Loecher in der Wassermaske zu fuellen
  
  countim = numpy.zeros_like(wmask)
  #countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[0:-2,0:-2], 1, 0) # Kommentare entfernen fuer N8-Nachbarschaft
  countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[0:-2,1:-1], 1, 0)
  #countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[0:-2,2::], 1, 0)
  countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[1:-1,0:-2], 1, 0)
  countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[1:-1,2::], 1, 0)
  #countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[2::,0:-2], 1, 0)
  countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[2::,1:-1], 1, 0)
  #countim[1:-1,1:-1] += numpy.where(wmask[1:-1,1:-1] != wmask[2::,2::], 1, 0)
  wmask_filled = wmask * 1
  wmask_filled[1:-1,1:-1] = numpy.where(wmask[1:-1,1:-1] != 1, numpy.where(countim[1:-1,1:-1] == 4, 1, wmask_filled[1:-1,1:-1]),wmask_filled[1:-1,1:-1])
  smask[numpy.where(wmask_filled == 1)] = 0  # Die Verbesserung durch das spatial enhancement wird auf die Schattenmaske uebertragen
  
  countim[:,:] = 0
  #countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[0:-2,0:-2], 1, 0) # Kommentare entfernen fuer N8-Nachbarschaft
  countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[0:-2,1:-1], 1, 0)
  #countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[0:-2,2::], 1, 0)
  countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[1:-1,0:-2], 1, 0)
  countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[1:-1,2::], 1, 0)
  #countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[2::,0:-2], 1, 0)
  countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[2::,1:-1], 1, 0)
  #countim[1:-1,1:-1] += numpy.where(smask[1:-1,1:-1] != smask[2::,2::], 1, 0)
  smask_filled = smask * 1
  smask_filled[1:-1,1:-1] = numpy.where(smask[1:-1,1:-1] != 1, numpy.where(countim[1:-1,1:-1] == 4, 1, smask_filled[1:-1,1:-1]),smask_filled[1:-1,1:-1])
  wmask[numpy.where(smask_filled == 1)] = 0  # Die Verbesserung durch das spatial enhancement wird auf die Wassermaske uebertragen
  
  return smask_filled, wmask_filled
  
  
  
def WriteOutputImages(switch_shadow1_water2,file_1,outpath,outfile,v,suffix,inImage,justCombIm,secondary_data_flag,smask,wmask,nm710_max,VI,specmean,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,bgmask,low_albedo_mask,waterslopes,coeff1,coeff2,coeff3,coeff4,coeff5,plants_under_water_mask,combim,cmask_shadow_static,cmask_water_static,cmask_unsure_static,smask_filled,smask_SpatEnh2,wmask_filled):
  # Writes output images for water and shadow classification
  # switch_shadow1_water2 = defines wether shadowmask.py or watermask.py was the calling main program. Set 1 for shadow and 2 for water.
  # All the rest: just pass the respective variables of the main program to this function.  
  # returns nothing
  
  if justCombIm:
    secondary_data = numpy.float32(combim.reshape(1,smask.shape[0],smask.shape[1]))
  elif secondary_data_flag:
    secondary_data = numpy.append(numpy.float32(nm710_max), numpy.float32(VI.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(specmean.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(mean_nir.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(algae_slope710.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(algae_slope815.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(max_vis.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(vis_nir_ratio.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(bgmask.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(low_albedo_mask.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(waterslopes), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(coeff1[0:1]), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(coeff2[0:1]), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(coeff3[0:1]), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(coeff4[0:1]), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(coeff5[0:1]), axis=0)
    #secondary_data = numpy.append(secondary_data, fullspec_polyfit1_coeff.reshape(1,smask.shape[0],smask.shape[1]), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(plants_under_water_mask.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(combim.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(cmask_shadow_static.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(cmask_water_static.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(cmask_unsure_static.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(smask_filled.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(smask_SpatEnh2.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(wmask_filled.reshape(1,smask.shape[0],smask.shape[1])), axis=0)
    secondary_data = numpy.append(secondary_data, numpy.float32(wmask.reshape(1,smask.shape[0],smask.shape[1]) + (smask * 2)), axis=0)


  if outfile == None:
    # auf abschliessenden Slash im Pfad pruefen
    if os.name == "nt": # windows
      if outpath[-1] != '\\':
        outpath = outpath + '\\'
    else: # fuer linux ist os.name == "posix"
      if outpath[-1] != '/':
        outpath = outpath + '/'
        
    basefilename = os.path.split(file_1)[-1]
    outfilename = string.split(basefilename,".")
  else:
    outfilename = string.split(outfile,".")

  # Keine Dateiendung?
  if len(outfilename) == 1:
    outfilename.append('bsq')
  # Punkte im Dateinamen
  elif len(outfilename) > 2:
    #if v == 1:
      #print outfilename[0]
    for i in range(1,len(outfilename)-1):
      outfilename[0] = outfilename[0] + '.' + outfilename[i]
      #if v == 1:
        #print outfilename[0]

  if outfile == None:      
    outfilename_smask = outpath + outfilename[0] + suffix + "." + outfilename[-1]
    #outfilename_inImage = outpath + outfilename[0] + "_scaled." + outfilename[-1] ## output scaled image
    if justCombIm:
      outfilename_secondary = outpath + outfilename[0] + suffix + "_justCombIm." + outfilename[-1]
    elif secondary_data_flag:
      outfilename_secondary = outpath + outfilename[0] + suffix + "_secondaryData." + outfilename[-1]
  else:
    outfilename_smask = outfile
    if justCombIm:
      outfilename_secondary = outfilename[0] + "_justCombIm." + outfilename[-1]
    outfilename_secondary = outfilename[0] + "_secondaryData." + outfilename[-1]
  
  if justCombIm:
    bandnames = ["combim"]
  else:
    bandnames = ["nm710_max_wvl","nm710_max_refl","VI","specmean","mean_nir","algae_slope710","algae_slope815","max_vis","vis_nir_ratio","bgmask","low_albedo_mask","waterslope1","waterslope2","waterslope3","waterslope4","waterslope5","coeff1","coeff2","coeff3","coeff4","coeff5","plants_under_water_mask","combim","cmask_shadow","cmask_water","cmask_unsure","smask_filled","smask_SpatEnhBanks","watermask_filled","water/shadow classification"]


  # write results to ENVI images
  #Matrix2EnviImage(smask,outfilename_smask,"Shadow mask created by ShadowMask.py",v)
  if justCombIm:
    Matrix2EnviImage(secondary_data,outfilename_secondary, "Water slope combinations image",v,bNames=bandnames)
  else:
    dummy = numpy.zeros((1,inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.uint8)
    if switch_shadow1_water2 == 1:
      dummy[0] = numpy.uint8(smask_SpatEnh2) # smask or smask_filled or smask_SpatEnh2
      Matrix2EnviImage(dummy,outfilename_smask,"Shadow mask created by ShadowMask.py",v,bNames=["shadow mask"])
      if secondary_data_flag:
        Matrix2EnviImage(secondary_data,outfilename_secondary, "Secondary data created by ShadowMask.py",v,bNames=bandnames)
    elif switch_shadow1_water2 == 2:
      dummy[0] = numpy.uint8(wmask_filled) # wmask or wmask_filled
      Matrix2EnviImage(dummy,outfilename_smask,"Water mask created by WaterMask.py",v,bNames=["water mask"])
      if secondary_data_flag:
        Matrix2EnviImage(secondary_data,outfilename_secondary, "Secondary data created by WaterMask.py",v,bNames=bandnames)
    #Matrix2EnviImage(inImage.grid,outfilename_inImage, "Scaled input image created by ShadowMask.py",v)  ## output scaled image
    
  return outfilename_smask
  
  
def SpatialClassification(inImage,cmask_shadow,cmask_water,cmask_unsure,tmp_file_name):
  # Spatial classification of water and shadow according to the spectrally identified water and shadow pixels in the neighborhood
  # inImage = hyperspectral input image (C_Image)
  # cmask_shadow, cmask_water, cmask_unsure = masks (2D arrays)
  # returns two masks (2D arrays)
  
  if tmp_file_name != None:
    tmp_file = open(tmp_file_name,"w")
    tmp_file.seek(0)
    tmp_file.truncate()
    tmp_file.flush()
  
  if len(numpy.where(cmask_unsure > 0)[0]) > 0:
    print "    Spatial classification of water and shadow ..."
    i = 0
    ksize = 3
    factor = 3
    #cmask_unsure2shadow = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.int16) # nur fuer plot
    #cmask_unsure2water = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.int16) # nur fuer plot
    #shadow_score_static = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.int16) # nur fuer plot
    #water_score_static = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.int16) # nur fuer plot
    unsure_px_at_start = len(numpy.where(cmask_unsure > 0)[0])
    total = 0.
    while (len(numpy.where(cmask_unsure > 0)[0]) > 0):
      ksize += i * 2
      i += 1
      #print len(numpy.where(cmask_unsure > 0)[0])
      #print numpy.where(cmask_unsure > 0)
      print "      ksize =", ksize
      kernel = numpy.ones((ksize, ksize))

      changecount = inImage.grid.shape[1] * inImage.grid.shape[2]
      while (changecount > inImage.grid.shape[1] * inImage.grid.shape[2] / 100.0 * 0.01):
        shadow_score = FilterSum2D(kernel,cmask_shadow) * cmask_unsure
        water_score = FilterSum2D(kernel,cmask_water) * cmask_unsure
        #shadow_score_static[numpy.where(shadow_score > 0)] = shadow_score[numpy.where(shadow_score > 0)] # nur fuer plot
        #water_score_static[numpy.where(water_score > 0)] = water_score[numpy.where(water_score > 0)] # nur fuer plot
        dummy = numpy.where(water_score > factor * shadow_score,1,0)
        dummy *= numpy.where(water_score > 1,1,0) # Entscheidung nur bei > 1 px im Filter
        changecount = len(numpy.where(dummy > 0)[0])
        #cmask_unsure2water += dummy # nur fuer plot
        cmask_water += dummy
        cmask_unsure -= dummy
        dummy = numpy.where(shadow_score > factor * water_score,1,0)
        dummy *= numpy.where(shadow_score > 1,1,0) # Entscheidung nur bei > 1 px im Filter
        changecount += len(numpy.where(dummy > 0)[0])
        #cmask_unsure2shadow += dummy # nur fuer plot
        cmask_shadow += dummy
        cmask_unsure -= dummy
        total = total + changecount
        total_per = total * 100.0 / unsure_px_at_start
        print '%s %6d %s %3.2f %s' % ("        No. of identified pixels:", changecount, " (total:", total_per, "%)")
        if tmp_file_name != None:
          tmp_file.seek(0)
          tmp_file.truncate()
          tmp_file.write(str(total_per))
          tmp_file.flush()
      
      if factor > 1 and ksize > 30:
        factor -= 1
        ksize = 3
        i = 0
        print "    Reducing factor to " + str(factor)
      
      # if no sure pixels can be found within ksize > 100 then decide according to the majority class within the total image
      if factor == 1 and ksize > 100:
        shadowcount = len(numpy.where(cmask_shadow > 0)[0])
        watercount = len(numpy.where(cmask_water > 0)[0])
        if watercount >= shadowcount:
          cmask_water += cmask_unsure
          cmask_unsure -= cmask_unsure
        else:
          cmask_shadow += cmask_unsure
          cmask_unsure -= cmask_unsure
  else:
    print "No unsure pixels after spectral classification!"

  if tmp_file_name != None:
    tmp_file.close()
    
  return cmask_shadow, cmask_water  # entspricht return smask, wmask
  


# **** GENERATE WEIGHTS FILTER FOR SPECTRAL RESAMPLING ************************

def GenerateSpectralFilterGaussian(wvl_in,wvl_out,FWHM):
  # wvl_in = input wavelength [class ndarray 1D]
  # wvl_out = output / filtered wavelength [class ndarray 1D]
  # FWHM = full width of half maximum (gaussian function) [float/int] or [1D ndarray]
  # return [class ndarray 2D] shape = (len(wvl_out),len(wvl_out))
  n_in = len(wvl_in)
  n_out = len(wvl_out)
  #print "        number of input wavelength = %d" % (wvl_in.shape[0])
  #print "        number of output wavelength = %d" % (wvl_out.shape[0])
  [Win,Wout] = numpy.meshgrid(wvl_in,wvl_out)
  try:
    Weights = numpy.exp(-1*(numpy.power(Win-Wout,2.0)*numpy.log(16.0)/(numpy.power(FWHM,2.0))))
  except:
    Weights = numpy.exp(-1*(((Win-Wout)**2.0)*numpy.log(16.0)/(FWHM**2.0)))
  #print "        shape of weights matrix = " + str(Weights.shape)
  for i in range(Weights.shape[0]):
    Weights[i] = Weights[i]/numpy.sum(Weights[i])
  return Weights




# **** SPECTRAL RESAMPLING ****************************************************

def SpectralResampling(weights,spectra,wvl_out):
  # weights = matrix with weights for spectral resampling (a,b)
  # spectra = matrix or 1D-array to sample (b,c) or (b,1)
  # return sampled spectra (a,c) or (a,1)
  resample = numpy.dot(weights,spectra)
  wvl_out.shape = (len(wvl_out),1)
  resample = numpy.append(wvl_out,resample,axis=1)
  return resample #numpy.dot(weights,spectra)




def FillWhiteWaterHoles(inImage, wvl, wmask, maxsize):
  # wmask = water mask = 2D ndarray
  # maxsize = thresh for max size of white water holes in water mask to be filled
  # returns wmask with filled white water holes
  
  # segmentation of wask
  struc_el = [[0,1,0],[1,1,1],[0,1,0]]
  (segim, numseg) = ndimage.measurements.label(numpy.int8(-1*(wmask-1)),struc_el) 
  # auf 32 bit Systemen wirft numpy.ndimage (numpy 1.6.2) einen Fehler, wenn ein Bild von Datentyp int32 als erstes Argument an ndimage.measurements.label uebergeben wird. 
  #Wurde als bug gemeldet. Bei numpy 1.5.1 passiert das nicht. Mit der Konvertierung numpy.int8(-1*(wmask-1)) sollte es ueberall laufen.
  
  # calc size of black segments
  sizeim = numpy.zeros_like(segim)
  for i in range(1,numseg+1):
    index = numpy.where(segim == i)
    sizeim[index] = len(index[0])
  
  # get small black segments by size thresh
  holes_within_water = numpy.where(sizeim <= maxsize,1,0)
  holes_within_water = numpy.where(sizeim == 0,0,holes_within_water)
  
  # calc spectral correlation between an open ocean water spectrum and pixel spectra within the small black segments
  #numpy.cov(m, y, rowvar=1, bias=1)
  index = numpy.where(holes_within_water == 1)
  if len(index[0]) == 0: # keine white water px
    return wmask
  
  oceanspec_wvl = numpy.array([401.54,405.89,410.23,414.58,418.92,423.27,427.61,431.96,436.34,440.86,445.4,449.94,454.47,459.01,463.55,468.09,472.62,477.16,481.7,486.24,490.77,495.31,499.85,504.39,508.92,513.46,518,522.54,527.07,531.61,536.15,540.69,545.23,549.91,554.63,559.35,564.07,568.79,573.51,578.23,582.95,587.67,592.39,597.11,601.83,606.55,611.27,615.99,620.71,625.43,630.15,634.87,639.59,644.32,649.06,653.8,658.54,663.28,668.01,672.75,677.49,682.23,686.96,691.7,696.44,701.18,705.92,710.66,715.4,720.14,724.88,729.62,734.36,739.11,743.85,748.59,753.39,758.23,763.08,767.92,772.76,777.61,782.45,787.29,792.13,796.98,801.82,806.66,811.51,816.35,821.19,826.03,830.87,835.7,840.54,845.37,850.2,855.04,859.87,864.7,869.54,874.37,879.2,884.04,888.87,893.7,898.54,903.37,908.2,913.04,917.89,922.74,927.59,932.44,937.29,942.14,946.99,951.84,956.69,961.54])
  oceanspec_vals = numpy.array([295.81,300.74,305.18,306.19,316.85,325.32,334.71,317.62,325.37,328.49,331.61,335.81,339.16,344.67,344.73,351.93,354.35,361.23,371.21,375.35,384.04,389.03,392.1,398.49,400.54,399.01,400.6,405.79,412.85,414.34,415.29,415.3,415.91,420.58,426.75,428.26,432.69,428.8,420.7,402.54,375.51,345.04,311.84,278.88,246.61,233.99,226.58,219.93,213.7,209.06,201.95,196,194.02,187.73,180.01,173.62,167.18,162.28,157.61,155.78,154.83,155.43,149.44,139.79,129.97,119.12,108.92,101.66,93.61,83.29,77.11,73.09,72.21,69.84,71.1,70.5,70.26,75.52,77.15,69.89,69.13,68.82,68.36,62.73,63.95,63.34,58.04,63.59,61.6,54.99,37.17,38.32,44.26,45.31,46.83,38.62,39.56,40.19,36.31,35.84,39.4,35.56,32.61,32.82,31.5,22.14,0.7,6.14,4.78,0.9,9.69,30.33,10.04,0,0,0,0,0,0,0])
  
  oceanspec_vals = oceanspec_vals / 100.0
  oceanspec = numpy.append(numpy.reshape(oceanspec_wvl,(len(oceanspec_wvl),1)),numpy.reshape(oceanspec_vals,(len(oceanspec_wvl),1)),axis=1)
  # oceanspec.shape
  
  FWHM = numpy.zeros_like(oceanspec_wvl)
  #FWHM = numpy.reshape(FWHM, (FWHM.shape[0],1))
  FWHM[0:-1] = oceanspec_wvl[1::] - oceanspec_wvl[0:-1]
  FWHM[-1] = FWHM[-2]
  weights = GenerateSpectralFilterGaussian(oceanspec[:,0],wvl[:,0],FWHM)
  #print weights.shape, FWHM.shape, eDif_eTot[:,1:2].shape,wvl.shape
  oceanspec = SpectralResampling(weights,oceanspec[:,1:2],wvl)
  #print oceanspec.shape
  
  covarray = numpy.corrcoef(numpy.append(oceanspec[:,1:2],inImage.grid[:,index[0],index[1]],axis=1),rowvar=0,bias=1)
  #print covarray
  #print covarray[:,0]
  #print covarray.shape
  covmask = numpy.where(covarray[1:,0] > 0.7,1,0)
  #print index[0].shape
  #print covarray.shape
  white_water_mask = holes_within_water * 1
  white_water_mask[index] = covmask
  
  wmask_temp = wmask * 1
  wmask = wmask + white_water_mask
  
  #pylab.figure(2243)
  #pylab.imshow(wmask_temp + 3 * holes_within_water + 2 * white_water_mask,interpolation="Nearest")
  
  #bins = numpy.arange(-1.0,1.1,0.1)
  #print bins
  #histo = numpy.histogram(covarray[:,0], bins)
  #print histo
  #pylab.figure(2244)
  #pylab.plot(histo[1][0:-1],histo[0])
  
  #pylab.figure(2246)
  #pylab.plot(wvl,inImage.grid[:,index[0][-16],index[1][-16]],'b-')
  #pylab.plot(wvl,inImage.grid[:,index[0][-17],index[1][-17]],'r-')
  #pylab.plot(wvl,inImage.grid[:,48,177],'r-')
  #print "x ", index[1][-16], "y ", index[0][-16], "cov ", covarray[len(index[0][:-15]),0]
  #print "x ", index[1][-17], "y ",index[0][-17], "cov ", covarray[len(index[0][0:-16]),0]
  #pylab.plot(wvl,oceanspec[:,1:2],'g-')
  
  #pylab.show()
  
  return wmask