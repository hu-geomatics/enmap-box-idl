# -*- coding: utf-8 -*-
#*****************************************************************************#
#                                                                             #
#   DERIVES A WATER MASK FROM HYPERSPECTRAL IMAGES                            #
#                                                                             #
#   Version: 1.0                                                              #
#                                                                             #
#   Changes since v0.2:                                                       #
#   - Adaptation to the needs of the EnMAP-Box                                #
#                                                                             #
#   Changes since v0.1:                                                       #
#   - Implemetation of white water recognition to fill holes                  #
#     within the water mask                                                   #
#                                                                             #
#   Written for Python 2.5 and higher                                         #
#   Aditionally to Python the following modules have to be installed:         #
#   - NumPy                                                                   #
#   - MatPlotLib                                                              #
#                                                                             #
#                                                                             #
#                                                                             #
#   Written by Mathias Bochow and Theres Kuester                              #
#   Section 1.4, Helmholtz Centre Potsdam - GFZ German Research Centre for    #
#   Geosciences                                                               #
#   Contact: mathias.bochow@gfz-potsdam.de                                    #
#                                                                             #
#                                                                             #
#   LICENCE                                                                   #
#                                                                             #
#                                                                             #
#                                                                             #
#*****************************************************************************#


# **** IMPORTING LIBRARIES ****************************************************

# standard
#import string
#from matplotlib import pylab
import numpy
import sys
import os
import time
import json

# custom
import shadowmask_functions as smf



# **** MAIN *******************************************************************

if __name__ == "__main__":

  print "\n"
  print "====================================================================="
  print "\n  .------------."
  print "  | WaterMask  |"
  print "  '------------'\n"
  t01 = time.time()


  ### checking environment 

  try:
    smf.CheckPython25()
  except:
    print "\n  >>> ERROR while checking Python version!\n"
    sys.stdout.flush()
    os._exit(1)

  for i in range(len(sys.argv)):
    if(sys.argv[i] == "-H"):
      print "   Usage: python WaterMask.py\n"
      print "       -i    :  hyperspectral VNIR input image"
      print "      [-o]   :  output path [DEFAULT: path of input image]"
      print "      [-ofn]  :  output path and image"
      print "      [-su]  :  suffix for output file name [DEFAULT: _wmask]"
      print "      [-r100]   :  grey value representing 100 % reflection in the input image"
      print "      [-pxsize] :  pixel size of input image in meters"
      print "      [-sec] :  set this flag to write an output secondary data file"
      print "      [-tf]  : path and file name to a temporary ASCII file containing the input parameters for the program call"
      print "      [-v]   :  verbose"
      print "      [-H]   :  help\n"
      sys.stdout.flush()
      os._exit(1)

  v = 0
  suffix = "_wmask"
  secondary_data_flag = None
  datamax = 0
  pxsize = None
  outpath = None
  outfile = None
  print_params = None
  param_tokens = []
  index_list = []
  tmp_file_name = None

  
  # Anpassung an EnMAP-Box
  jsonFile = sys.argv[1]
  try:
    fileObject = open(jsonFile, 'r')
    jsonString = fileObject.read()
    param = json.loads(jsonString)
    keys = param.keys()
    param_tokens = []
    for i in keys:
      param_tokens.append(i)
      param_tokens.append(param[i])
  except:
    param_tokens = []

  

  
  ### importing parameters
  if len(param_tokens) == 0:
    param_tokens = sys.argv
    
  for i in range(len(param_tokens)):
    if(param_tokens[i] == "-r100" or param_tokens[i] == "-pxsize"):
      print_params = 1    
  if print_params:
    print "\n    User defined parameters:\n"
  
  for i in range(len(param_tokens)):   
    if(param_tokens[i] == "-i"):
      try:
        file_1 = smf.CheckInputFile(i+1,param_tokens)
        if outpath == None:
          path_ = os.path.split(file_1)
          outpath = path_[0]
      except:
        print "\n  >>> ERROR : While checking input file -i !"
        sys.stdout.flush()
        os._exit(1)
      continue
    elif(param_tokens[i] == "-o"):
      outpath = param_tokens[i+1].strip() # Python build-in function to remove leading and trailing spaces
      continue
    elif(param_tokens[i] == "-su"):
      suffix = "_" + param_tokens[i+1].strip()
      continue
    elif(param_tokens[i] == "-r100"):
      try:
        datamax = float(param_tokens[i+1])
        print "      Grey value for 100 % reflectance value = ", datamax
      except:
        print "\n  >>> ERROR : value of parameter -r100 (%s) cannot be converted into a float number !" % (param_tokens[i+1])
        sys.stdout.flush()
        os._exit(1)
      continue
    elif(param_tokens[i] == "-pxsize"):
      try:
        pxsize = float(param_tokens[i+1])
        print "      Image pixel size = ", pxsize, " Meters"
      except:
        print "\n  >>> ERROR : value of parameter -pxsize (%s) cannot be converted into a float number !" % (param_tokens[i+1])
        sys.stdout.flush()
        os._exit(1)
      continue
    elif(param_tokens[i] == "-ofn"):
      outfile = param_tokens[i+1]
      continue
    elif(param_tokens[i] == "-sec"):
      secondary_data_flag = 1
      continue
    elif(param_tokens[i] == "-v"):
      v = 1
      continue

  
  ### open image and scale it 

  inImage, wvl = smf.ImportAndScale(file_1,None,datamax,v)

  # In case of missing pixel size information
  if inImage.pxSize == None:
    if pxsize != None:
      inImage.pxSize = inImage.pySize = pxsize
    else:
      print "\n  >>> ERROR : No pixel size given in image header file. Please provide pixel size in meters using the -pxsize parameter"
      sys.stdout.flush()
      os._exit(1)
  
  ### select suitable spectral bands

  inImage,wvl,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w = smf.SelectBands_EARSeL(inImage,wvl,None,v)

  ### spectral feature calculations

  nm710_max,specmean,VI,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,specs_in_cols,coeff1,coeff2,coeff3,coeff4,coeff5,waterslopes = smf.CalcSpecFea_5Slopes(None,inImage,wvl,first_band,last_band,nm860,mean_wvl_nir,nir,nm500,nm640,nm675,nm680,nm710,nm720,nm740,nm815,nm470w,nm520w,nm572w,nm604w,nm650w,nm665w,nm670w,nm677w,nm680w,nm740w,nm765w,nm809w,nm810w,nm845w,nm910w,nm950w,v)



  # **** Deriving shadow mask ****************************************************

  if v == 1:
    print "   (4) Deriving water mask ...\n"

  ### ignore background pixels
  
  wmask = numpy.where(numpy.sum(inImage.grid,axis=0) > 0.00001, 1, 0) # TRUE Wert und FALSE Wert muessen keine Arrays sein. Geht ueber Broadcasting.
  bgmask = wmask * 1

  ### low albedo mask
  
  # histogram thresholding approach
  wmask = bgmask * smf.CalcLowAlbedoMaskByHistoThresh(inImage,wvl,wmask,mean_nir,file_1)
  if secondary_data_flag:
    low_albedo_mask = wmask * 1
  else:
    low_albedo_mask = 1 # dummy
  
  
  ### ToDo: Warum werden die dunklen Schatten mit negativen Werten in DD2004 als Wasser klassifiziert?
  ### Due to imprecision of atmospheric corrections there can be spectra with zero reflectance in some VIS channels although there was radiance > zero. Those belong usually to shadow pixels.
  cmask_shadow = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.int16)
  #countzero = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2]),dtype=numpy.int16)
  #a = inImage.grid[first_band:nm640+1].shape[0]
  #for i in range(first_band,first_band + a):
    #countzero[numpy.where(inImage.grid[i] == 0)] += 1
  #cmask_shadow[numpy.where(countzero >= a/4.0)] = 1
  
  
  ### detect macrophytes in water
  plants_under_water_mask = wmask * numpy.where(VI > 1.0, 1, 0) * numpy.where(algae_slope710 < -0.001, 1, numpy.where(algae_slope815 < -0.01,1,0))
  wmask -= plants_under_water_mask # kommt hinterher wieder drauf

  ### evaluate the frequence of combinations of water slopes
  for i in range(waterslopes.shape[0]):
    dummy = numpy.where(wmask == 0,0,waterslopes[i])
    waterslopes[i] = dummy
  combim = numpy.sum(waterslopes,axis=0,dtype=numpy.uint8)
  combim[numpy.where(wmask == 0)] = 0

  combim = numpy.reshape(combim,inImage.grid.shape[1] * inImage.grid.shape[2])
  comb_histo = numpy.histogram(combim, numpy.arange(0,31))
  combim = numpy.reshape(combim,(inImage.grid.shape[1],inImage.grid.shape[2]))

  #pylab.figure(172)
  #pylab.imshow(waterslopes[0])
  #pylab.title("water slope 1")
  #pylab.figure(173)
  #pylab.imshow(waterslopes[1])
  #pylab.title("water slope 2")
  #pylab.figure(174)
  #pylab.imshow(waterslopes[2])
  #pylab.title("water slope 3")
  #pylab.figure(175)
  #pylab.imshow(waterslopes[3])
  #pylab.title("water slope 4")
  #pylab.figure(176)
  #pylab.imshow(waterslopes[4])
  #pylab.title("water slope 5")
  #pylab.figure(170)
  #pylab.imshow(combim)
  #pylab.title("combinations of water slopes")
  #pylab.colorbar()
  #pylab.figure(171)
  #pylab.plot(numpy.arange(1,31),comb_histo[0])
  #pylab.title("Histgram of combinations of water slopes")

  # gueltige Kombinationen fuer Schatten
  cmask_shadow = numpy.where(combim ==  1, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  2, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  3, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  4, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  5, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  6, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  7, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  16, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  18, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  20, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  21, 1, cmask_shadow)
  cmask_shadow = numpy.where(combim ==  22, 1, cmask_shadow)

  # gueltige Kombinationen fuer Wasser
  cmask_water = numpy.where(combim == 9, 1,0)
  cmask_water = numpy.where(combim ==  10, 1, cmask_water)
  cmask_water = numpy.where(combim ==  11, 1, cmask_water)
  cmask_water = numpy.where(combim ==  15, 1, cmask_water)
  cmask_water = numpy.where(combim ==  26, 1, cmask_water)
  cmask_water = numpy.where(combim ==  27, 1, cmask_water)
  cmask_water = numpy.where(combim ==  31, 1, cmask_water)
  
  # strittige Kombinationen
  cmask_unsure = numpy.where(combim ==  8, 1, 0)
  cmask_unsure = numpy.where(combim ==  12, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  13, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  14, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  17, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  19, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  23, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  24, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  25, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  28, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  29, 1, cmask_unsure)
  cmask_unsure = numpy.where(combim ==  30, 1, cmask_unsure)

  if secondary_data_flag:
    cmask_unsure_static = cmask_unsure * 1
    cmask_water_static = cmask_water * 1
    cmask_shadow_static = cmask_shadow * 1
  else:
    cmask_unsure_static = cmask_water_static = cmask_shadow_static = 1
 
  # Spatial classification of water and shadow
  smask, wmask = smf.SpatialClassification(inImage,cmask_shadow,cmask_water,cmask_unsure,tmp_file_name)
  #smask = cmask_shadow # fuer schnelles Debuggen ohne spatial classification
  #wmask = cmask_water # fuer schnelles Debuggen ohne spatial classification

  # Algen unter Wasser zur Wassermaske hinzufuegen
  wmask += plants_under_water_mask

  # Fill white water holes in wmask
  maxsize = numpy.int(500 / (inImage.pxSize * inImage.pxSize) + 0.5) # number of pixels corresponding to 200 m2 ~ Schiff mit Heckwelle
  #print "maxsize = " + str(maxsize)
  
  # ToDo: White water ist nicht in der low albedo mask. Daher Loecher in der Wassermaske pruefen, ob sie White water sind und ggf. fuellen. Testdatensatz: moenchsgut_sub1
  # Die derzeitige White water Pruefung sollte vom Korrellationstest auf den Spectral Feature Test umgestellt werden!
  wmask = smf.FillWhiteWaterHoles(inImage, wvl, wmask, maxsize)
  wmask = wmask * bgmask


  #pylab.figure(153)
  #pylab.title('Spectrally identified water pixels')
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0,0,0.9)], N=None)
  #pylab.imshow(cmask_water_static,cmap=myCmap,interpolation="Nearest")
  
  #pylab.figure(154)
  #pylab.title('Spectrally identified no water pixels')
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0,0,0)], N=None)
  #pylab.imshow(cmask_shadow_static,cmap=myCmap,interpolation="Nearest")
  
  #pylab.figure(547)
  #pylab.title('No spectral decision')
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0.8,0,0)], N=None)
  #pylab.imshow(cmask_unsure_static,cmap=myCmap,interpolation="Nearest")
  
  #pylab.figure(155)
  #pylab.title('True color composite')
  #rgb = numpy.append(numpy.append(numpy.reshape(inImage.grid[smf.FindBand(wvl,460)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), numpy.reshape(inImage.grid[smf.FindBand(wvl,550)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), axis=0), numpy.reshape(inImage.grid[smf.FindBand(wvl,640)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), axis=0)
  #rgb_bip = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2],3),dtype=numpy.float)
  #for y in range(inImage.grid.shape[1]):
    #for x in range(inImage.grid.shape[2]):
      #bb=0
      #for b in range(2,0,-1):
        #rgb_bip[y,x,bb] = rgb[b,y,x] / 30
        #bb+=1
  #rgb_bip = numpy.where(rgb_bip > 1.0,1.0,rgb_bip)
  #pylab.imshow(rgb_bip,interpolation="Nearest")
  
  #max_score = numpy.max(shadow_score_static)
  #if numpy.max(water_score_static) > max_score:
    #max_score = numpy.max(water_score_static)
   
  #norm1 = matplotlib.colors.LogNorm(vmin=1.0,vmax=100,clip=False)
  #shadow_score_static[numpy.where(cmask_unsure_static < 1)] = -10
  #water_score_static[numpy.where(cmask_unsure_static < 1)] = -10
  
  #pylab.figure(543)
  #pylab.title('Shadow Score')
  #pylab.imshow(shadow_score_static,interpolation="Nearest",norm=norm1)
  #pylab.colorbar()
  #pylab.figure(544)
  #pylab.title('Water Score')
  #pylab.imshow(water_score_static,interpolation="Nearest",norm=norm1)
  #pylab.colorbar()
  
  #pylab.figure(545)
  #pylab.title('Spatially identified water pixels')
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0,0,0.9)], N=None)
  #pylab.imshow(cmask_unsure2water,cmap=myCmap,interpolation="Nearest")
  
  #pylab.figure(546)
  #pylab.title('Spatially identified no water pixels')
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0,0,0)], N=None)
  #pylab.imshow(cmask_unsure2shadow,cmap=myCmap,interpolation="Nearest")
  
  #pylab.figure(130)
  #pylab.title('water mask')
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0,0,0.9)], N=None)
  #pylab.imshow(wmask,interpolation="Nearest",cmap=myCmap)
  

  ### remove tar roofs
  ## might be useful for runs without eDif/eTot spectrum but is not totally worked out yet.
  #smask_rm_tar = smask * 1
  #tar = smask * 1
  #smask_rm_tar *= numpy.where(fullspec_polyfit1_coeff[0,:] > 0,numpy.where(VI < 1.2,0,1),1)
  #tar *= numpy.where(fullspec_polyfit1_coeff[0,:] > 0,numpy.where(VI < 1.2,1,0),0)

  #fullspec_polyfit1_coeff *= numpy.where(smask == 1, 1, numpy.nan)
  #pylab.figure(156)
  #pylab.imshow(fullspec_polyfit1_coeff)
  #pylab.colorbar()

  #pylab.figure(155)
  #pylab.title('RGB')
  #rgb = numpy.append(numpy.append(numpy.reshape(inImage.grid[smf.FindBand(wvl,460)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), numpy.reshape(inImage.grid[smf.FindBand(wvl,550)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), axis=0), numpy.reshape(inImage.grid[smf.FindBand(wvl,640)], (1,inImage.grid.shape[1],inImage.grid.shape[2])), axis=0)
  #rgb_bip = numpy.zeros((inImage.grid.shape[1],inImage.grid.shape[2],3),dtype=numpy.float)
  #for y in range(inImage.grid.shape[1]):
    #for x in range(inImage.grid.shape[2]):
      #bb=0
      #for b in range(2,0,-1):
        #rgb_bip[y,x,bb] = rgb[b,y,x] / 30
        #bb+=1
  #rgb_bip = numpy.where(rgb_bip > 1.0,1.0,rgb_bip)
  #pylab.imshow(rgb_bip)

  #pylab.figure(15)
  #pylab.imshow(smask)
  #pylab.title('smask');
  #pylab.colorbar()

  #pylab.figure(16)
  #pylab.imshow(tar)
  #pylab.title('tar');
  #pylab.colorbar()
  
  #pylab.show()



  ### spatial enhancement ##

  smask_filled, wmask_filled = smf.FillHoles(smask,wmask)

  #pylab.figure(21)
  #myCmap = matplotlib.colors.ListedColormap([(0.9,0.9,0.9),(0.8,0,0)], N=None)
  #pylab.title("wmask_filled elim island px")
  #pylab.imshow(wmask_filled,interpolation="Nearest",cmap=myCmap)



  # **** writing output ****************************************************

  outfilename_smask = smf.WriteOutputImages(2,file_1,outpath,outfile,v,suffix,inImage,0,secondary_data_flag,smask,wmask,nm710_max,VI,specmean,mean_nir,algae_slope710,algae_slope815,max_vis,vis_nir_ratio,bgmask,low_albedo_mask,waterslopes,coeff1,coeff2,coeff3,coeff4,coeff5,plants_under_water_mask,combim,cmask_shadow_static,cmask_water_static,cmask_unsure_static,smask_filled,smask_filled,wmask_filled)
  

  # **** time estimation ****************************************************

  smf.PrintTimeAndMemory(t01)
  
  
  # **** communication with IDL ****************************************************
  r = {}
  r['message'] = "WaterMask finished"
  r['filename_wmask'] = outfilename_smask
  fileCon = open(param['scriptOutputFilename'], 'w')
  fileCon.write(json.dumps(r))
  fileCon.close()
  

  #pylab.figure(2)
  #pylab.title('final shadow mask of ' + infilename)
  #myCmap = matplotlib.colors.ListedColormap([(0.7,0,0),(0,0.7,0)], N=None)
  #pylab.imshow(smask,cmap=myCmap,interpolation="Nearest")
  #pylab.imshow(smask,interpolation="Nearest")

  #pylab.figure(13)
  #pylab.title('smask_reipthresh of ' + infilename)
  #pylab.imshow(smask_reipthresh,interpolation="Nearest")

  #pylab.figure(14)
  #pylab.title('wmask_filled of ' + infilename)
  #pylab.imshow(wmask_filled,interpolation="Nearest")


  #pylab.figure(countfig+1)
  #pylab.title(infilename)
  #pylab.imshow(smask)
  #pylab.figure(2)
  #pylab.plot(wvl[:,0],inImage.grid[:,192,58])
  #pylab.figure(2)
  #pylab.imshow(slopeim[0])
  #pylab.colorbar()
  #pylab.figure(3)
  #pylab.imshow(slopeim[1])
  #pylab.colorbar()
  #pylab.figure(4)
  #pylab.imshow(slopeim[2])
  #pylab.colorbar()
  #pylab.show()


# **** END OF SHADOWMASK.PY ************************************************