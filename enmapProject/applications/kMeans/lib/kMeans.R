require(rjson)
  require(raster)

settings <- commandArgs(TRUE)
p <- readLines(settings)
parameters <- fromJSON(p)

inputFilePath <- parameters$inputFilePath # (character) file path of feature image
outputFilePath <- parameters$outputFilePath # (character) file path of created cluster image (ENVI Classification Image)
numberOfClusters <- parameters$numberOfClusters # (numeric) maximal number of clusters to be tested

# Not used or not implemented yet
#randomSamples <- parameters$randomSamples # (numeric), (optional), number of samples drawn from input image. Currently parameters$randomSamples not present if not used. Can be changed to NULL if desired.
#inputMask <- parameters$inputMask # (character) file path of mask image. Currently set to NULL if not used 
#tmpFilename <- parameters$tmpFilename # (character) file path to users temp folder (for creating report figures)

# Define function
kMeansRaster <- function(inputFilePath,
                         outputFilePath,
                         numberOfClusters
){
  
  # Data transformation
  inputStack <- stack(inputFilePath)
  inputMatrix <- as.matrix(inputStack)
  inputMatrix <- scale(inputMatrix)
  
  # Kmeans
  fit <- kmeans(inputMatrix, centers=numberOfClusters)
  
  # Create output raster
  outRas <- raster(ncol=ncol(inputStack), nrow=nrow(inputStack), ext=extent(inputStack), crs=projection(inputStack))
  values(outRas) <- fit$cluster
  
  # Write output raster to HD
  outputFilePath <- paste(outputFilePath, "envi", sep=".")
  writeRaster(outRas, outputFilePath, format="ENVI", overwrite=TRUE, NAflag=0, dataType="INT2U")
  file.rename(outputFilePath, strsplit(outputFilePath,".envi")[[1]][1])
  file.remove(paste(outputFilePath,"aux.xml",sep="."))
  
  header <- paste(strsplit(outputFilePath,".envi")[[1]][1], ".hdr", sep="")
  headerFile <- readLines(header,-1)
  headerFile[8] <- "file type = ENVI Classification"
  writeLines(headerFile, header)
  cat(paste("classes =", numberOfClusters+1), file=header, append=T)
    
  print(paste("Written classification successfully to:", strsplit(outputFilePath,".envi")[[1]][1]))
}

# Files for application test
# inputFilePath <- "Y:/temp/temp_cornelius/Hymap_Berlin-A_Image"
# numberOfClusters <- 6
# outputFilePath <- "Y:/temp/temp_cornelius/kmeans_test"

# Run function on input data
kMeansRaster(inputFilePath, outputFilePath, numberOfClusters)
