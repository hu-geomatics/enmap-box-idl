;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to print text to the EnMAP-Box console.
;
; :Params:
;    text: in, required, type=string[]
;      Text to be printed.
;
;-
pro enmapBox_print, text, Print=print
  (hubProEnvEnmapHelper()).print, text
  if keyword_set(print) then print, transpose(text[*])
end