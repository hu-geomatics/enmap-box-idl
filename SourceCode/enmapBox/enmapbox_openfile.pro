;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to open a non-image/speclib file inside the EnMAP-Box.
;
; :Params:
;    filename: in, required, type=filename
;      The filename.
;
; :Examples:
;   Start the EnMAP-Box and open the header of a test image (provided by the hubAPI library)::
;     enmapBox
;     enmapBox_openFile, hub_getTestImage('Hymap_Berlin-A_Image')+'.hdr'
;   The header file should now be available inside the EnMAP-Box filelist.
;-
pro enmapBox_openFile, filename
  hubProEnvHelper.openFile, filename
end