;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function enmapBoxDevPath::getRootDir
  compile_opt static, strictarr
  ; find EnMAP-Box installation directory, e.g. D:\EnMAP-Box
  
  defsysv, '!enmapDir', EXISTS = exists
  if ~exists then begin
    pathArray = strsplit((routine_info('enmapBoxDevPath__define',/Source)).path, path_sep(), /EXTRACT,/PRESERVE_NULL)
    pathSubArray = pathArray[0:-5]
    enmapDir = strjoin(pathSubArray, path_sep())
    defsysv, '!enmapDir', enmapDir
  endif
  return, !enmapDir
end

function enmapBoxDevPath::getVMDir, os ; virtual machine folder
  compile_opt static, strictarr
  case os of
    'win32'         : basename = 'IDLVMWin32'
    'win64'         : basename = 'IDLVMWin64'
    'lin32'         : basename = 'IDLVMLin32'
    'lin64'         : basename = 'IDLVMLin64'
    'osx64'         : basename = 'IDLVMOSX64'
    'crossPlatform' : basename = 'IDLVMCrossPlatform'
    else : message, 'OS '+string(os)+' not supported.'
  endcase
  return, filepath(basename, ROOT=enmapBoxDevPath.getRootDir())
  
end

function enmapBoxDevPath::getCodeDir $
  , Root=root $
  , Applications=applications $
  , EnmapBox=enmapBox $
  , Lib=lib
  
  compile_opt static, strictarr
  codeRoot = filepath('SourceCode', ROOT_DIR=enmapBoxDevPath.getRootDir())
  codeApplications = filepath('applications', ROOT_DIR=codeRoot)

  result = codeRoot
  if keyword_set(addOn) then begin
    result = codeAddOn
  endif
  if keyword_set(enmapBox) then begin
    result = filepath('enmapBox', ROOT_DIR=codeRoot)
  endif
  if keyword_set(Applications) then begin
    result = codeApplications
  endif
  if keyword_set(lib) then begin
    result = filepath('lib', ROOT_DIR=codeRoot)
  endif
  
  return, result
  
end

function enmapBoxDevPath::getProjectDir $ ; cross-plattform folder
  , Root=root $
  , Applications=applications $
  , EnmapBox=enmapBox $
  , Lib=lib

  compile_opt static, strictarr
  projectRoot = filepath('enmapProject',  ROOT_DIR=enmapBoxDevPath.getRootDir())
  projectApplications = filepath('applications', ROOT_DIR=projectRoot)
  
  if keyword_set(root) then begin
    result = projectRoot
  endif
  if keyword_set(enmapBox) then begin
    result = filepath('enmapBox', ROOT_DIR=projectRoot)
  endif
  if keyword_set(applications) then begin
    result = projectApplications
  endif
  if keyword_set(lib) then begin
    result = filepath('lib', ROOT_DIR=projectRoot)
  endif
  
  return, result
  
end

function enmapBoxDevPath::getSaveFile $
  , VMLauncher=VMLauncher $
;  , EnviLauncherClassic=enviLauncherClassic $
;  , EnviLauncherNew=enviLauncherNew $
  , EnviInstaller=enviInstaller
  
  compile_opt static, strictarr
  
  if keyword_set(VMLauncher) then begin
    result = filepath('enmapBoxVMLauncher.sav', ROOT_DIR=enmapBoxDevPath.getVMDir('crossPlatform'))
  endif

;  if keyword_set(enviLauncherClassic) then begin
;    result = filepath('enmapBoxEnviLauncherClassic.sav', ROOT_DIR=enmapBoxDevPath.getVMDir('crossPlatform'))
;  endif
;  
;  if keyword_set(enviLauncherNew) then begin
;    result = filepath('enmapBoxEnviLauncherNew.sav', ROOT_DIR=enmapBoxDevPath.getVMDir('crossPlatform'))
;  endif
  
  if keyword_set(enviInstaller) then begin
    result = filepath('enmapBoxEnviInstaller.sav', ROOT_DIR=enmapBoxDevPath.getVMDir('crossPlatform'))
  endif
  
;  if keyword_set(enmapBox) then begin
;    result = filepath('enmapBox.sav', ROOT_DIR=enmapBoxDevPath.getProjectDir(/EnMAPBox), SUBDIRECTORY='lib')
;  endif
  return, result
  
end

pro enmapBoxDevPath__define
  struct = {enmapBoxDevPath $
    , inherits IDL_Object $
  }
end