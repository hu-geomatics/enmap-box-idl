;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxDev_createDistributionOS, os, SourceCode=sourceCode, ZIP=zip, TARGZ=targz

  if ~(keyword_set(zip) xor keyword_set(targz)) then begin
    message, 'Set either ZIP or TARGZ keyword.'
  endif

  ; get dirnames
  
  vmDir = []
  if isa(os) then foreach os_i,os do vmDir = [vmDir, (enmapBoxDevPath()).getVMDir(os_i)]
  cpDir = (enmapBoxDevPath()).getVMDir('crossPlatform')
  codeDir = keyword_set(sourceCode) ? (enmapBoxDevPath()).getCodeDir(/Root) : []  
  projectDir = (enmapBoxDevPath()).getProjectDir(/Root)
  outputRootDir = filepath('software', ROOT=!distDir)
  outputDir = filepath('EnMAP-Box', Root=outputRootDir)

  ; copy files

  file_delete, outputDir, /ALLOW_NONEXISTENT, /RECURSIVE
  sourceDirs = [vmDir, cpDir, codeDir, projectDir]
  targetDirs = filepath(file_basename(sourceDirs), ROOT=outputDir)
  foreach dir, targetDirs do begin
    file_mkdir, dir
  endforeach
  
  file_copy, sourceDirs, FILE_DIRNAME(targetDirs), /NOEXPAND_PATH, /OVERWRITE, /RECURSIVE, /REQUIRE_DIRECTORY

  ; delete .PROJECT file
  
  projectFiles = file_search(outputDir, '.project', COUNT=numberOfFiles)
  if numberOfFiles ne 0 then begin
    file_delete, projectFiles, /ALLOW_NONEXISTENT, /QUIET, /RECURSIVE, /NOEXPAND_PATH
  endif

  ; delete .SVN files

  svnFiles = file_search(outputDir, '.svn', /TEST_DIRECTORY, COUNT=numberOfFiles)
  if numberOfFiles ne 0 then begin
    file_delete, svnFiles, /ALLOW_NONEXISTENT, /QUIET, /RECURSIVE, /NOEXPAND_PATH
  endif
  
  ; create zip file
  
  zipFilename = filepath('EnMAP-Box', ROOT=outputRootDir)
  zipFilename += isa(os) ? '_vm' : ''
  zipFilename += keyword_set(sourceCode) ? '_src' : ''
  cd, file_dirname(outputDir)
  case !VERSION.os_family of
    'Windows' : begin
      spawn, 'zip -9 -m -r "'+zipFilename+'.zip" "'+file_basename(outputDir)+'"'
    end
    'unix' : begin
      if keyword_set(targz) then begin
        spawn, 'tar cfvz '+zipFilename+'.tar.gz '+file_basename(outputDir)
        file_delete, outputDir, /RECURSIVE, /NOEXPAND_PATH, /ALLOW_NONEXISTENT
      endif
      if keyword_set(zip) then begin
        spawn, 'zip -9 -m -r '+zipFilename+'.zip '+file_basename(outputDir)
      endif
    end
  endcase
end

pro enmapBoxDev_createDistributionDocumentation

  projectDir = (enmapBoxDevPath()).getProjectDir(/Root)
  distDir = !distDir
  
  ; copy enmapProject folder and rename it to documentation
  
  docuDir = filepath('documentation', ROOT=distDir)
  file_delete, docuDir, /ALLOW_NONEXISTENT, /RECURSIVE
  file_copy, projectDir, docuDir, /RECURSIVE

  ; delete all files not necessary

  file_delete, file_search(docuDir, 'resource', /TEST_DIRECTORY), /RECURSIVE
  file_delete, file_search(docuDir, '*.sav')

  ; copy index.html and stylesheet.css
  
;  webFiles = ['index.html', 'stylesheet.css']
;    
;  indexFilenameSource = filepath(webFiles, ROOT=file_dirname((routine_info('enmapBoxDev_createDistributionDocumentation',/SOURCE)).path))
;  indexFilenameTarget = filepath(webFiles, ROOT=distDir)
;  file_copy, indexFilenameSource, indexFilenameTarget, /RECURSIVE, /OVERWRITE

end

pro enmapBoxDev_createDistribution
; 0. set compile/make flags in the static class enmapboxmake__define.pro

; 1. build box
  
  enmapBoxMake_make

; 2. create distribution  
  defsysv, '!distDir', 'C:\Users\geo_mahe\Desktop\'
;  enmapBoxDev_createDistributionOS, ['win64'], /ZIP
  enmapBoxDev_createDistributionOS, ['osx64','win32','win64','lin64'], /ZIP
  enmapBoxDev_createDistributionOS, ['osx64','win32','win64','lin64'], /SourceCode, /ZIP
  enmapBoxDev_createDistributionDocumentation

end