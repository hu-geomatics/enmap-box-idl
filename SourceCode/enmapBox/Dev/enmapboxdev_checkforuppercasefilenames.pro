;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to check if all '*.pro' filenames inside code directory,
;    given by `codeDirectory` are written in lower case letters. 
;    This is important for correct auto-compiling, especially under Unix systems.
;    If one or more filenames are incorrect, an error is thrown. 
;
; :Params:
;    codeDirectory: in, required, type=directory name
;      Source code root directory. 
;
;-
pro enmapBoxDev_checkForUppercaseFilenames, codeDirectory

  allFiles = file_search(codeDirectory, '*.pro', /FOLD_CASE)
  wrongFiles = allFiles[where(/NULL, file_basename(allFiles) ne strlowcase(file_basename(allFiles)))]
  if isa(wrongFiles) then begin
    print, 'Filenames to be changed:'
    print, transpose(wrongFiles)
    message, 'All filenames must be lowcase.'  
  endif
end