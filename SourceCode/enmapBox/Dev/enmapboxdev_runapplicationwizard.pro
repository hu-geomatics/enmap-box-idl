;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Private:
; :Description:
;    This is a helper routine for `enmapBoxDev_runApplicationWizard`. For a given ASCII file, it
;    replaces all occurrence of the string 'template', by the application name `appName`. 
;
; :Params:
;    filename : in, required, type=filepath
;      Filename of the ASCII file. 
;    appName : in, required, type=string
;      Application name.
;
;-
pro enmapBoxDev_runApplicationWizard_replaceTemplate, filename, appName

  lines = (hubIOASCIIHelper()).readFile(filename)
  foreach line, lines, i do begin
    lines[i] = strjoin(strsplit(line, 'template', /REGEX, /EXTRACT, /PRESERVE_NULL, /FOLD_CASE), appName)
  endforeach
  (hubIOASCIIHelper()).writeFile, filename, lines
  
end

;+
; :Description:
;    The Application Wizard is a widget program for creating an application skeleton 
;    that can be used as a starting point for writing your own EnMAP-Box application.
;
; :Keywords:
;    RScript : in, optional, type=boolean
;      Set this keyword to create a R-script based application skeleton.
;      
;-
pro enmapBoxDev_runApplicationWizard, RScript=rScript, PythonScript=pythonScript

  ; get application name and source code directory
  case 1b of
    keyword_set(rScript) : appName = 'NewRApp'
    keyword_set(pythonScript) : appName = 'NewPythonApp'
    else : appName = 'NewApp'
  endcase

  hubAMW_program, Title='EnMAP-Box Application Wizard'
  hubAMW_parameter, 'appName',              Title='Application Name           ', Value=appName, /String, SIZE=30
  hubAMW_inputDirectoryName, 'codeRootDir', Title='Source Code Root Directory ', VALUE=enmapBox_getDirname(/Applications, /SourceCode)
  result = hubAMW_manage()
  if ~result['accept'] then return
  progressBar = hubProgressbar(title='EnMAP-Box Application Wizard')
  
  appName = strcompress(result['appName'])
  appCodeDir = filepath(appName, ROOT=result['codeRootDir'])

  ; copy template files
  templateCodeDir = filepath('TEMPLATE', ROOT=enmapBox_getDirname(/Applications, /SourceCode))
  file_delete, appCodeDir, /RECURSIVE, /ALLOW_NONEXISTENT
  file_copy, templateCodeDir, appCodeDir, /RECURSIVE 
  if keyword_set(rScript) then begin
    templateCodeDirRContent = file_search(filepath('*', ROOT=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY='TEMPLATE_R'))
    file_copy, templateCodeDirRContent, appCodeDir, /OVERWRITE, /RECURSIVE
  endif
  if keyword_set(pythonScript) then begin
    templateCodeDirRContent = file_search(filepath('*', ROOT=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY='TEMPLATE_PYTHON'))
    file_copy, templateCodeDirRContent, appCodeDir, /OVERWRITE, /RECURSIVE
  endif

  ; rename all PRO files (lower case!) 
  proFilenames = file_search(appCodeDir, '*.pro', COUNT=numberOfFiles)
  for i=0,numberOfFiles-1 do begin
    targetFilename = filepath(strlowcase(appName)+strmid(file_basename(proFilenames[i]),8), ROOT=file_dirname(proFilenames[i]))
    file_move, proFilenames[i], targetFilename
  endfor

  ; rename all CONF files
  confFilenames = file_search(appCodeDir, '*.conf', COUNT=numberOfFiles)
  for i=0,numberOfFiles-1 do begin
    targetFilename = filepath(appName+strmid(file_basename(confFilenames[i]),8), ROOT=file_dirname(confFilenames[i]))
    file_move, confFilenames[i], targetFilename
  endfor

  ; replace "template" by <appName> string inside PRO, MEN, TXT, IDLDOC files
  allFilenames = [ $
    file_search(appCodeDir, '*.pro', /TEST_REGULAR, COUNT=count1), $
    file_search(appCodeDir, '*.men', /TEST_REGULAR, COUNT=count2), $
    file_search(appCodeDir, '*.idldoc', /TEST_REGULAR, COUNT=count3), $
    file_search(appCodeDir, '*.conf', /TEST_REGULAR, COUNT=count4), $
    file_search(appCodeDir, '*.txt', /TEST_REGULAR, COUNT=count5)]
  numberOfFiles = count1+count2+count3+count4+count5
  
  for i=0,numberOfFiles-1 do begin
    enmapBoxDev_runApplicationWizard_replaceTemplate, allFilenames[i], appName
  endfor

  progressBar.cleanup
  
end

