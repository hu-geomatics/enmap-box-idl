; ENVI Registration Polynomial Coefficients
; Polynomial Degree = 1
;
; kx =
       75.0000000000       -0.1938037071
        0.9807066194        0.0000000000
;
; ky =
      182.5000000000       -0.9853919154
       -0.1766465386        0.0000000000
