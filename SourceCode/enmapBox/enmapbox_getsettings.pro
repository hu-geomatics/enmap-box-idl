;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query EnMAP-Box setting. 
;    
; :Returns:
;    Returns a hash with all settings. 
;
;-
function enmapBox_getSettings
  return, enmapBoxSettings_getSettings()
end
