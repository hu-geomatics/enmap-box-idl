function enmapBoxToolbox::init
  ; create box frame structure
  appInfo = enmapBoxSettingsLocal_getApplicationInformation()
  title = appInfo.description+' '+appInfo.version
  icon = self.getIcon()
  self.toolbox = hubTBToolbox(Title=title, /Column, XRegisterName='enmapBox', Icon=icon)

  frame1 =hubTBFrame(self.toolbox, Title='Frame 1')
  frame11 = hubTBFrame(frame1, Title='Filelist', /FRAME, ShowButtonRight=1, IsFixedSize=[1,0], FixedSize=[300,0])
  frame12 = hubTBFrame(frame1, /Column)
  frame2 = hubTBFrame(self.toolbox, Title='Frame 2', /FRAME, /ShowButtonUp, IsFixedSize=[0,1], FixedSize=[0,300])
  self.tabFrameManager = hubTBFrameTabManager(frame2, /FRAME)
  self.gridFrameManager = hubTBFrameGridManager(frame12, self.tabFrameManager, Title='', /FRAME)
  self.toolbox.start

  ; add menu
  menuFiles = list()
  menuFiles.add, filepath('enmapInit.men', ROOT=enmapBox_getDirname(/ENMAPBOX), SUBDIR='resource')
  menuFiles.add, filepath('enmapMain.men', ROOT=enmapBox_getDirname(/ENMAPBOX), SUBDIR='resource')
  menuFiles.add, file_search(enmapBox_getDirname(/Applications), 'enmap.men', Count=numberOfFiles), /EXTRACT
  menuFiles.add, filepath('enmapFinal.men', ROOT=enmapBox_getDirname(/ENMAPBOX), SUBDIR='resource')
  self.toolbox.createMenu, menuFiles, Log=logAddMenu
  ; insert content
  ; - default frame for grid manager
  self.gridFrameManager.ensureDefaultFrame
  
  ; - filelist
  self.filelist = hubTBContentFilelist(self.toolbox)
  self.toolbox.showContentInFrame, self.filelist, frame11
  
  ; - console
  self.console = hubTBContentConsole(self.toolbox)
  !null = self.tabFrameManager.addContent(self.console, /ShowTitleBar, /ShowDetach)
  self.toolbox.updateView
  self.toolbox.updateContent
  self.print, 'Parsing menu files:'
  self.print, '  '+logAddMenu

  self.toolbox.setFinishProcedure, 'enmapBoxToolboxFinish'
  return, 1b
end

;pro enmapBoxToolbox::initTest
;  
;  ; do some stuff
;  ; - open some files
;  foreach imageFile, hub_getTestImage() do begin
;    self.openImage, imageFile
;  endforeach
;  foreach speclibFile, hub_getTestSpeclib() do begin
;    self.openImage, speclibFile
;  endforeach
;  
;  
;  for i=1,3 do begin
;    filename = filepath(/TMP, 'file'+strtrim(i,2)+'.txt')
;    save, dummy, FILENAME=filename
;    self.openFile, filename
;  endfor
;    
;  ; - add some managed frames
;  imageContent = hubTBManagedContentImage(hub_getTestImage('Hymap_Berlin-A_Image'), [12,7,1], self.toolbox, self.gridFrameManager)
;  imageFrame = self.gridFrameManager.addFrameWithContent(Content=imageContent, /ShowTitleBar, /ShowDetach, /ShowClose)
;  imageContent.showLabelingTool
;
;;;  imageContent = hubTBManagedContentImage('e:\subset', [12,7,1], self.toolbox, self.tabFrameManager, self.gridFrameManager)
;;;  imageContent.setImageLink, 1, 1
;;;  imageFrame = self.gridFrameManager.addFrameWithContent(Content=imageContent, /ShowTitleBar, /ShowDetach, /ShowClose)
;;  imageContent = hubTBManagedContentImage(hub_getTestImage('Spot_Berlin'), [2,1,0], self.toolbox, self.gridFrameManager)
;;  imageContent.setImageLink, 1, 1
;;  imageFrame = self.gridFrameManager.addFrameWithContent(Content=imageContent, /ShowTitleBar, /ShowDetach, /ShowClose)
;;  self.gridFrameManager.setImageLinkType, 'geo link'
;
;  speclibContent = hubTBManagedContentSpeclib(self.toolbox, self.gridFrameManager)
;
;;  profileLibrary = test_hubTBManagedContentSpeclibProfileLibrary_getTestLibrary()
;;  foreach profile, profileLibrary.getProfiles(),i do begin
;;    speclibContent.addProfile, profile
;;if i ge 4 then break
;;  endforeach
;;  speclibContent.controller.resetPlot
;  speclibFrame = self.gridFrameManager.addFrameWithContent(Content=speclibContent, /ShowTitleBar, /ShowDetach, /ShowClose)
;  speclibContent.controller.importENVISpeclib, hub_getTestSpeclib('ClassificationSpeclib')
;  speclibContent.showLabelingTool
;  speclibLabelingTool = speclibContent.getLabelingTool()
;  
;  roiLayer = hubROILayerReader.importRaster(hub_getTestSpeclib('ClassificationLabels'))
;  speclibLabelingTool.controller.appendROILayer, roiLayer
;  ;  imageContent = hubTBManagedContentImage('D:\bigImage', [0,1,2], self.toolbox, self.tabFrameManager)
;  ;  imageContent = hubTBManagedContentImage(enmapBox_getTestImage('AF_Image'), [12,7,1], self.toolbox, self.tabFrameManager)
;
;
;
;  ; - print something to the console
;  self.console.print, 'Hallo'+sindgen(10)
;  
;  self.toolbox.update
;;  return
;;  imageContent.hideLabelingTool
;;  self.toolbox.update
;;  
;;  
;;  
;;  imageLabelingTool = hubTBContentImageLabelingTool(self.toolbox)
;;  self.toolbox.showContentInFrame, imageLabelingTool, frame2
;;  imageLabelingTool.setFeatureList, test_hubVectorFeatureList()
;;  imageLabelingTool.linkImageContent, imageContent
;;  imageContent.linkImageLabelingTool, imageLabelingTool
;;
;;;  imageLabelingTool.setFeatureListSelection, 0
;;  imageLabelingTool.update
; 
;  
;;  
;;  self.toolbox.update
;;  return
;;  
;;  self.toolbox.finish
;  
;  ; save and load enmap speclib
;;  filename = filepath(/TMP, 'speclib.enmapSLI')
;;  speclibContent.controller.saveEnmapSpeclib, filename
;;  speclibContent.controller.loadEnmapSpeclib, filename
;  
;  ; import envi speclib
;;  speclibContent.controller.importENVISpeclib, hub_getTestSpeclib('ClassificationSpeclib')
;;  speclibContent.controller.importENVISpeclib, enmapbox_getTestSpeclib('AF_Speclib')
;  
;
;  ; link image to speclib
;  imageContent.setSpeclibLinked, speclibContent, 1b
;end

function enmapBoxToolbox::getIcon
  fileDirectory = filepath('', ROOT_DIR=enmapBox_getDirname(/EnmapBox),Subdirectory=['resource','icon'])
  fileBasename = 'tlb'+['16x16','32x32','48x48']+'.bmp'
  filename = filepath(fileBasename, ROOT_DIR=fileDirectory)
  return, filename
end

pro enmapBoxToolbox::openImage, filename

  self.closeFile, filename
  inputImage = hubIOImgInputImage(filename)
  
  if inputImage.isSpectralLibrary() then begin
    self.filelist.openFile, filename, 'speclibs'
    self.print, 'Open speclib: '+filename
  endif else begin
    self.filelist.openFile, filename, 'images'
    self.print, 'Open image: '+filename
  endelse
end

pro enmapBoxToolbox::openFile, filename
  self.closeFile, filename
  self.filelist.openFile, filename, /Extension
  enmapBox_print, 'Open file: '+filename
end

pro enmapBoxToolbox::closeFile, filename
  self.filelist.closeFile, filename
end

pro enmapBoxToolbox::print, text
  self.console.print, text
end

pro enmapBoxToolbox::close
  self.toolbox.finish
end

pro enmapBoxToolbox::getProperty, filelist=filelist, console=console, tabManager=tabManager, toolbox=toolbox
  if arg_present(filelist) then filelist = self.filelist
  if arg_present(console) then console = self.console
  if arg_present(tabManager) then tabManager = self.tabFrameManager
  if arg_present(toolbox) then toolbox = self.toolbox
end

pro enmapBoxToolbox__define
  define = {enmapBoxToolbox, inherits IDL_Object, $
    toolbox:obj_new(),$
    gridFrameManager:obj_new(),$
    tabFrameManager:obj_new(),$
    filelist:obj_new(),$
    console:obj_new()}
end