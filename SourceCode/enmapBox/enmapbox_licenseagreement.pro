;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Hidden:
;-
pro enmapBox_licenseAgreement_setAgreement
  confirmationFlagFile = filepath('licenseAgreementFlag', ROOT_DIR=enmapBox_getUserDirname())
  openw, unit, confirmationFlagFile, /GET_LUN
  free_lun, unit 
end

;+
; :Hidden:
;-
function enmapBox_licenseAgreement_getAgreement
  confirmationFlagFile = filepath('licenseAgreementFlag', ROOT_DIR=enmapBox_getUserDirname())
  return, file_test(confirmationFlagFile)
end

;+
; :Description:
;    EnMAP-Box license agreement.  
;-
pro enmapBox_licenseAgreement

  ; return if licenses already agreed
  if enmapBox_licenseAgreement_getAgreement() then begin
    return
  endif

  ; read license files
  hubIOASCIIHelper = hubIOASCIIHelper()
  text = []
  
  
  
  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
  text = [text, '', '+++++++++++++++++', '+++ EnMAP-Box +++', '+++++++++++++++++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
  text = [text, '','+++ hubAPI +++']
  text = [text, 'Is released under the EnMAP-Box license.']

  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
  text = [text, '','+++ hubApplications +++']
  text = [text, 'Is released under the EnMAP-Box license.']

  text = [text, '', '', '+++++++++++++++++++++++++++++', '+++ Third Party Libraries +++', '+++++++++++++++++++++++++++++']

;  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/Lib), SUBDIRECTORY=['IDLdoc','copyrights'])
;  text = [text, '','+++ IDLdoc +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]

  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/Lib), SUBDIRECTORY=['coyote','copyrights'])
  text = [text, '','+++ Coyote Graphics System +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  text = [text, '', '', '++++++++++++++++++++++++++++++', '+++ EnMAP-Box Applications +++', '++++++++++++++++++++++++++++++']

  ;optional
    
  fileNames = List()
  headings = List()
  
  filenames.add, filepath('license_imageSVM.txt', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ imageSVM +++'
  
  filenames.add, filepath('license_libSVM.txt', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ libSVM +++'
  
  filenames.add, filepath('license_imageRF.txt', ROOT_DIR=imageRF_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ imageRF +++'
  
  filenames.add, filepath('license.txt', ROOT_DIR=imageMath_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ imageMath +++'
  
  filenames.add, filepath('license_autoPLSR.txt', ROOT_DIR=autoPLSR_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ autoPLSR +++'
  
  filenames.add, filepath('license_MaxEntWrapper.txt', ROOT_DIR=MaxEntWrapper_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ MaxEntWrapper +++'
  
  filenames.add, filepath('license.txt', ROOT_DIR=SpInMine_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ SpInMine +++'
  
  filenames.add, filepath('license.txt', ROOT_DIR=hubLibMix_getDirname(), SUBDIRECTORY='copyrights')
  headings.add, '+++ LibMix +++'
  
  for i=0, n_elements(filenames)-1 do begin
    if (FILE_INFO(filenames[i])).exists then begin
      text = [text, '', headings[i]]
      text = [text, hubIOASCIIHelper.readFile(filenames[i])]
    endif
  endfor
  ;##########################
  
;  
;  
;  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
;  text = [text, '', '+++++++++++++++++', '+++ EnMAP-Box +++', '+++++++++++++++++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ hubAPI +++']
;  text = [text, 'Is released under the EnMAP-Box license.']
;
;  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ hubApplications +++']
;  text = [text, 'Is released under the EnMAP-Box license.']
;
;  text = [text, '', '', '+++++++++++++++++++++++++++++', '+++ Third Party Libraries +++', '+++++++++++++++++++++++++++++']
;
;  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/Lib), SUBDIRECTORY=['IDLdoc','copyrights'])
;  text = [text, '','+++ IDLdoc +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/Lib), SUBDIRECTORY=['coyote','copyrights'])
;  text = [text, '','+++ Coyote Graphics System +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  text = [text, '', '', '++++++++++++++++++++++++++++++', '+++ EnMAP-Box Applications +++', '++++++++++++++++++++++++++++++']
;
;  filename = filepath('license_imageSVM.txt', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ imageSVM +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license_libSVM.txt', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ libSVM +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license_imageRF.txt', ROOT_DIR=imageRF_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ imageRF +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;  
;  filename = filepath('license.txt', ROOT_DIR=imageMath_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ imageMath +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license_autoPLSR.txt', ROOT_DIR=autoPLSR_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ autoPLSR +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license_MaxEntWrapper.txt', ROOT_DIR=MaxEntWrapper_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ MaxEntWrapper +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license.txt', ROOT_DIR=SpInMine_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ SpInMine +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
;
;  filename = filepath('license.txt', ROOT_DIR=hubLibMix_getDirname(), SUBDIRECTORY='copyrights')
;  text = [text, '','+++ LibMix +++']
;  text = [text, hubIOASCIIHelper.readFile(filename)]
  
  title = 'EnMAP-Box License Agreement'
  hubAMW_program, Title=title
  hubAMW_label, ['License Agreement',$
                 'Please read the following License Agreement.',$
                 'Use the scroll bar to view the rest of the agreement.']
  hubAMW_text, 'text', Value=text, /Scroll, /Wrap, XSIZE=(get_screen_size())[0]*0.5, YSIZE=(get_screen_size())[1]*0.3
  hubAMW_label, ['Do you accept all the terms of the preceding License Agreement?',$
                 'To start the EnMAP-Box, you must accept this agreement.']
  result = hubAMW_manage()

  if result['accept'] then begin
    enmapBox_licenseAgreement_setAgreement
  endif else begin
    message, 'huberror-ignore', /NONAME
  endelse

end