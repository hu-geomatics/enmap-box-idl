;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns enmapBox test speclib filenames.
;    
; :Params:
;    basename: in, required, type=file basename
;    
; :Examples:
;    Print all test speclib filenames::
;    
;      print, enmapBox_getTestSpeclib()
;      
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\speclib\AF_Speclib
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\speclib\AF_Speclib_LC
;    
;    Print a specific filename::
;    
;      print, enmapBox_getTestSpeclib('AF_Speclib')
;      
;    IDL prints something like::
;    
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\speclib\AF_Speclib
;    
;-
function enmapBox_getTestSpeclib, basename

  getAll = ~isa(basename)
  root = filepath('', ROOT=enmapBox_getDirname(/EnmapBox), SUBDIR=['resource','testData','speclib'])
  if getAll then begin
    result = list(file_search(root, '*.hdr'), /EXTRACT)
    foreach filename, result, i do begin
      if strcmp(file_basename(filename), 'pyramid.', 8) then begin
        result[i] = ''
      endif else begin
        result[i] = hubHelper.removeFileExtension(filename)
      endelse
    endforeach
    result.remove, result.where('')
  endif else begin
    result = filepath(basename, ROOT=root)
    if ~(hubIOHelper()).fileReadable(result) then begin
      message, 'File is not part of hubAPI test speclib set: '+result
    endif
  endelse
  return, result
end

;+
; :Hidden:
;-
pro test_enmapBox_getTestSpeclib
  
  print, enmapBox_getTestSpeclib(/All)
  print, enmapBox_getTestSpeclib('ClassificationSpeclib')
end
