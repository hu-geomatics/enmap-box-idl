;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns specific EnMAP-Box file locations.
;
; :Keywords:
;    Settings: in, optional, type=boolean
;      Return settings file location.
;
; :Examples:
;    Print file locations::    
;      print, enmapBox_getFilename(/Settings)
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\enmap_settings.txt
;-
function enmapBox_getFilename $
  , Settings=settings
  
  enmapBoxDir = enmapBox_getDirname(/EnmapBox)
  if keyword_set(settings) then begin
    result = filepath('enmap_settings.txt', ROOT_DIR=enmapBoxDir, SUBDIRECTORY='resource')
  endif
  return, result

end

;+
; :Hidden:
;-
pro test_enmapBox_getDirectory

  print, enmapBox_getFilename(/Settings)
  print, enmapBox_getFilename(/Menu)

end