;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

; save EnMAP-Box folder to system variable

  defsysv, '!enmapDir', enmapDir

; restore EnMAP-Box

  enmapSAV = filepath('enmapBox.sav', ROOT_DIR=!enmapDir, SUBDIRECTORY=['enmapProject','enmapBox','lib'])
  restore, enmapSAV
  IDLVersion = ((obj_new('IDL_Savefile', enmapSAV)).contents()).release

; restore libraries/applications

  libSavFiles = file_search((enmapBoxDevPath()).getProjectDir(/Lib),'*.sav', /FOLD_CASE)
  appSavFiles = file_search((enmapBoxDevPath()).getProjectDir(/Applications),'*.sav', /FOLD_CASE)
  savFiles = [libSavFiles, appSavFiles]
  foreach savFile, savFiles do begin

      ; check if SAVE file was created with the correct IDL version
      
      savIDLVersion = ((obj_new('IDL_Savefile', savFile)).contents()).release
      if ~strcmp(IDLVersion, savIDLVersion, 3) then begin
        info = [ 'The EnMAP-Box Virtual Machine is based on IDL Version '+IDLVersion+'.' $
               , 'The following SAVE file is created with IDL version '+savIDLVersion+':' $
               , savFile $
               , 'This may lead to incorrect behaviors.']
        ok = dialog_message(info, /Information, Title='Restoring SAVE Files')
      endif
      
      ; get routine names saved inside SAV file
      
      oSAV = obj_new('IDL_Savefile', savFile)
      functionNames = oSAV.Names(/Function)
      procedureNames = oSAV.Names(/Procedure)
      
      ; check if routine names already exist
      
      functionFilepathes = routine_filepath(functionNames, /Is_function)
      conflictingFunctions = where( ~( (functionFilepathes eq '') or (functionFilepathes eq savFile[0])), /Null)

      procedureFilepathes = routine_filepath(procedureNames)
      conflictingprocedures = where( ~( (procedureFilepathes eq '') or (procedureFilepathes eq savFile[0])), /Null)

      if file_basename(savFile) ne 'idl.sav' then begin 
        if isa(conflictingFunctions) then begin
          info = [ 'Conflicting function definition found in SAVE file:' $
                 , savFile $
                 , '' $
                 , 'The following functions are replaced (original source):' $
                 , functionNames[conflictingFunctions]+' ('+(functionFilepathes[conflictingFunctions])+')']
          ok = dialog_message(info, /Information, Title='Restoring SAVE Files')
     
        endif
        
        if isa(conflictingProcedures) then begin
          info = [ 'Conflicting procedure definition found in SAVE file:' $
                 , savFile $
                 , '' $
                 , 'The following procedures are replaced (original):' $
                 , procedureNames[conflictingProcedures]+' ('+(procedureFilepathes[conflictingProcedures])+')']
          ok = dialog_message(info, /Information, Title='Restoring SAVE Files')
          
        endif
      endif
      
      restore, savFile
      
  endforeach

