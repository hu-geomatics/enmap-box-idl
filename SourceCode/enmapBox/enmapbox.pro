;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to start the EnMAP-Box. Note that only one EnMAP-Box session can exist. 
;-
pro enmapBox

  @huberrorcatch

  ; check if user has agreed to the license
  ;enmapBox_licenseAgreement

  ; check if the box is already running
  if (hubProEnvEnmapHelper()).isEnmapSession() then begin
    ; only one EnMAP-Box session allowed, return quietly
    return
  endif

  ; check if ENVI is already running
  if (hubProEnvEnviHelper()).isEnviSession() then begin
    ok = dialog_message(/INFORMATION, TITLE='EnMAP-Box', 'EnMAP-Box can not run beside ENVI.')
    return
  endif
  
  ;check IDL Version
  appInfo = enmapBoxSettingsLocal_getApplicationInformation()
  if !VERSION.release LT appInfo.idl_version then begin
    info = ['Your IDL release '+!VERSION.release+' is outdated.' $
           ,'This EnMAP-Box ' + appInfo.version + ' was developed for IDL >= ' + appInfo.idl_version+'.' $
           +' Please update your IDL to avoid problems with outdated releases.']
           
    !NULL = DIALOG_MESSAGE(info,/INFORMATION, TITLE='Warning') 
  endif
  
  ; start IDLJavaBridge
  enmapJavaBridge_init
  
  ; start the box
  enmapToolbox = enmapBoxToolbox()

  ; save box reference globally
  hub_setAppState, 'enmapBox', 'enmapBoxObject', enmapToolbox
  
  ; set working directory to test image folder
  cd, filepath('', ROOT=hub_getDirname(),SUBDIR=['resource','testData','image'])

  !DEBUG_PROCESS_EVENTS=0
end

;+
; :Description:
;    Use this function to return the EnMAP-Box object referencend return the reference to the enmapBox object. 
;-
function enmapBox

  if ~(hubProEnvEnmapHelper()).isEnmapSession() then begin
    enmapBox
  endif
  return, hub_getAppState('enmapBox', 'enmapBoxObject')
end