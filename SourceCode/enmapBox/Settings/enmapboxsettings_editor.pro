pro enmapBoxSettings_editor, Title=title, GroupLeader=groupLeader
  localSettingsFilename = enmapBoxSettingsLocal_getFilename()
  globalSettingsFilename = enmapBoxSettingsGlobal_getFilename()
  hubSettingsManager.editSettingFiles, localSettingsFilename, globalSettingsFilename, Title=title, GroupLeader=groupLeader
end