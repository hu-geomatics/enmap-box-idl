function enmapBoxSettings_getSettings
  
  ; read EnMAP-Box global settings
  settings = enmapBoxSettingsGlobal_getSettings()
  
  ; overwrite with EnMAP-Box local settings
  settings = settings + enmapBoxSettingsLocal_getSettings()
  
  return, settings
end