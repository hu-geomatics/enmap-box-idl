pro enmapBoxSettingsLocal_editSettings
  settings = enmapBoxSettings_getSettings()
  settingsManager = hubSettingsManager()
  settingsManager.setValues, settings
  description = hash()
  settingsManager.editSettings, TITLE='EnMAP-Box Settings', Accepted=accepted
  if accepted then begin
    settingsManager.writeSettingsFile, enmapBoxSettingsLocal_getFilename()
  endif
end