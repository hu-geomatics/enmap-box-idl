function enmapBoxSettingsGlobal_getSettings
  filename = enmapBoxSettingsGlobal_getFilename()
  settingsManager = hubSettingsManager()
  settingsManager.readSettingsFile, filename
  settings = settingsManager.getValue()
  return, settings
end