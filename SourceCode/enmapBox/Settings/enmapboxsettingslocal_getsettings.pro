function enmapBoxSettingsLocal_getSettings
  filename = enmapBoxSettingsLocal_getFilename()
  if file_test(filename) then begin
    settingsManager = hubSettingsManager()
    settingsManager.readSettingsFile, filename
    settings = settingsManager.getValue()
  endif else begin
    settings = orderedHash()
  endelse
  return, settings
end
