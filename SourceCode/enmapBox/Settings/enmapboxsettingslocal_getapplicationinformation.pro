function enmapBoxSettingsLocal_getApplicationInformation
  result = dictionary()
  result.dirname       = 'enmapBox'
  result.description   = 'EnMAP-Box'
  result.version       = '2.2.1'
  result.readmeText    = 'This is the configuration directory for the EnMAP-Box toolbox.'
  result.readmeVersion = 1
  result.idl_version   = '8.5.1'
  return, result
end