;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function enmapBoxUserApp_expandPath, relativeFilename, AddFilePrefix=addFilePrefix    
  
  if ~strcmp(relativeFilename, '%', 1, /FOLD_CASE) then begin
    filename = relativeFilename
  endif

  if strcmp(relativeFilename, '%appdir%', 8, /FOLD_CASE) then begin
    rootDir = enmapBox_getDirname(/Applications)
    filenamePrefixRemoved = strmid(relativeFilename, 8)
    basename = file_basename(filenamePrefixRemoved)
    subDirs = strsplit(/Extract, file_dirname(filenamePrefixRemoved), '/\')
    filename = filepath(basename, ROOT=rootDir, SUBDIR=subDirs)
    filePrefix = 1b
  endif
  
  if strcmp(relativeFilename, '%enmapboxdir%', 13, /FOLD_CASE) then begin
    rootDir = enmapBox_getDirname(/EnmapBox)
    filenamePrefixRemoved = strmid(relativeFilename, 13)
    basename = file_basename(filenamePrefixRemoved)
    subDirs = strsplit(/Extract, file_dirname(filenamePrefixRemoved), '/\')
    filename = filepath(basename, ROOT=rootDir, SUBDIR=subDirs)
    filePrefix = 1b
  endif
  
  if strcmp(relativeFilename, '%libdir%', 8, /FOLD_CASE) then begin
    rootDir = enmapBox_getDirname(/Lib)
    filenamePrefixRemoved = strmid(relativeFilename, 8)
    basename = file_basename(filenamePrefixRemoved)
    subDirs = strsplit(/Extract, file_dirname(filenamePrefixRemoved), '/\')
    filename = filepath(basename, ROOT=rootDir, SUBDIR=subDirs)
    filePrefix = 1b
  endif
  
  if keyword_set(filePrefix) and keyword_set(addFilePrefix) then begin
    filename = 'file:///'+filename
  endif

  return, filename

end