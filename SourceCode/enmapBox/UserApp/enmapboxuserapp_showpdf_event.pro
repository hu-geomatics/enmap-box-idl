;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Displays the PDF file that is given by the buttons argument. 
;    Under Windows the file is displayed via the default PDF viewer.
;    Under Unix the file is displayed via the `online_help` procedure.
;
; :Params:
;    event: in, required, type=structure
;      Button event structure.
;-
pro enmapBoxUserApp_showPDF_event, event        

  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event, /ToStructure)
  filename = enmapBoxUserApp_expandPath(applicationInfo.argument)
  enmapBox_print, 'Open PDF file: '+filename
  hubHelper.openFile, /PDF, filename
end