;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Displays the ASCII file that is given by the buttons argument. 
;    The file is displayed via the `xdisplayfile` procedure.
;
; :Params:
;    event: in, required, type=structure
;      Button event structure.
;-
pro enmapBoxUserApp_showASCII_event, event        

  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event, /ToStructure)
  filename = enmapBoxUserApp_expandPath(applicationInfo.argument)
  enmapBox_print, 'Open text file: '+filename
  xdisplayfile, filename, RETURN_ID=return_id, /GROW_TO_SCREEN
  (hubGUIHelper()).centerTLB, return_id
end