;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns a hash with information about the menu button that created the event::
;    
;        result['name']        = button name
;        result['argument']    = button argument stored inside the button UNAME (under EnMAP-Box) or UVALUE (under ENVI)
;        result['handler']     = event handler procedure
;        result['groupLeader'] = top level base of the EnMAP-Box or ENVI menu bar
;
; :Params:
;    event: in, required, type=structure
;      Button event structure.
;
; :Keywords:
;    ToStructure: in, optional, type=boolean
;      Returns a structure instead of a hash.
;
;-
function enmapBoxUserApp_getApplicationInfo, event, ToStructure=toStructure

  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)

  result = hash()
  result['name'] = menuEventInfo.name
  result['argument'] = menuEventInfo.argument
  result['handler'] = menuEventInfo.handler
  result['groupLeader'] = event.top

  if keyword_set(toStructure) then begin
    result = result.ToStruct()
  endif
  return, result

end