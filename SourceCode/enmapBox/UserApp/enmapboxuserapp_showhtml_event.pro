;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Displays the HTML file that is given by the buttons argument. 
;    Under Windows the file is displayed via the default web browser.
;    Under Unix the file is displayed via the `online_help` procedure.
;
; :Params:
;    event: in, required, type=structure
;      Button event structure.
;-
pro enmapBoxUserApp_showHTML_event, event        

  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event, /ToStructure)
  url = enmapBoxUserApp_expandPath(applicationInfo.argument, /AddFilePrefix)
  enmapBox_print, 'Open URL: '+url
  
  forwardContent = [$
    '<!DOCTYPE html>',$
    '<html>',$
      '<head>',$
        '<meta http-equiv="refresh" content="0; url='+url+'">',$
      '</head>',$
      '<body>',$
      '</body>',$
    '</html>']
    
  filename = filepath('forward.html', /TMP)
  (hubIOASCIIHelper()).writeFile, filename, forwardContent 
  hubHelper.openFile, /HTML, filename
  
end