;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to open an image file inside the EnMAP-Box.
;
; :Params:
;    filename: in, required, type=filename
;      Image filename.
;
; :Examples:
;   Start the EnMAP-Box and open a test image (provided by the hubAPI library)::
;     enmapBox
;     enmapBox_openImage, hub_getTestImage('Hymap_Berlin-A_Image')
;   The image should now be available inside the EnMAP-Box filelist.
;-
pro enmapBox_openImage, filename
  hubIOImg_openImage, filename
end