;+
; :Description:
;    Returns enmap box test filenames.
;    
; :Params:
;    basename: in, required, type=file basename
;      Use this argument to specify the file basename for the file that should be returned.
;      If omitted, all test filenames are returned.
;-
function enmapBox_getTestFile, basename, Images=Images, ASCII=ASCII

  root = filepath('', ROOT=enmapBox_getDirname(/EnmapBox), SUBDIR=['resource','testData'])
  result = !null
  helper = hubHelper()
  filter = '*.{hdr,csv,txt,tab}'
  if ~isa(basename) then begin
    case 1b of
     keyword_set(Images) : filter = '*.hdr'
     keyword_set(ASCII)  : filter = '*.{csv,txt,tab}'
     else : ;nothing
    endcase
  endif
  
  testFiles = file_search(root, filter)
  
  
  if testFiles[0] eq '' then message, 'could not find any test files'
    
  if isa(basename) then begin
    testFileBaseNames = FILE_BASENAME(testFiles)
    for i=0, n_elements(testFileBaseNames) -1 do begin
          testFileBaseNames[i] = helper.removeFileExtension(testFileBaseNames[i])
    endfor
    iBasename = where(testfileBaseNames eq basename, /NULL)
    if isa(iBasename) then begin 
      result = testFiles[iBasename]
    endif else begin
       message, 'File is not part of enmapBox test file set: '+basename
    endelse
  endif else begin
    ;remove extension from images only
    foreach filename, testFiles, i do begin
      if stregex(filename, '.hdr$', /Boolean) then begin
        testFiles[i] = helper.removeFileExtension(filename)
      endif
    endforeach
    result = transpose(testFiles)
  endelse
  
  if n_elements(basename) eq 1 then result = result[0]
  return, result

end

;+
; :Hidden:
;-
pro test_enmapBox_getTestFile
  
  print, transpose(enmapBox_getTestFile())
  print, enmapBox_getTestFile('AF_Image')
end