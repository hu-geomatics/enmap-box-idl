;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to start the EnMAP-Box in Virtual Machine Mode. 
;    
;    Note that under Unix systems, this will block your workbench. 
;    Any idea how to prevent this? 
;    If so, let me know: andreas.rabe@geo.hu-berlin.de
;-
pro enmapBoxVM

  @huberrorcatch

  case !VERSION.os_family of
    'Windows' : begin
      if !version.memory_bits eq 32 then cd, filepath('', SUBDIR=['bin','bin.x86'])
      if !version.memory_bits eq 64 then cd, filepath('', SUBDIR=['bin','bin.x86_64'])
      savLauncher = (enmapBoxDevPath()).getSaveFile(/VMLauncher)
      command = 'idlrt.exe -rt="'+savLauncher+'"'
      spawn, command, result, errResult, /NOSHELL, /NOWAIT
    end
    'unix' : begin
      cd, filepath('', SUBDIR='bin')
      savLauncher = (enmapBoxDevPath()).getSaveFile(/VMLauncher)
      command = ['idl', '-rt='+savLauncher]
      command = 'PATH=$PATH:/usr/bin; idl -rt='+savLauncher
      spawn, command, result, errResult
    end
  endcase
  print, result, errResult

end