;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns EnMAP-Box user directory. 
;    This directory is used to save user-specific information. 
;    
; :Examples:
;    Print user directory::
;      print, enmapBox_getUserDirname()
;      
;    IDL prints something like::
;      C:\Users\janzandr\.idl\hub\enmapboxproject
;      
;-
function enmapBox_getUserDirname
  
  result = app_user_dir('hub', 'Humbold-Universitaet zu Berlin', 'enmapBoxProject', 'EnMAP-Box Project', '', 1)
  return, result

end

;+
; :Hidden:
;-
pro test_enmapBox_getUserDirname
  print, enmapBox_getUserDirname()
end