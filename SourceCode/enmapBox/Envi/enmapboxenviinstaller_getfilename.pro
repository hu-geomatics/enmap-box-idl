function enmapBoxEnviInstaller_getFilename, INI=ini, Installer=installer, LuncherClassic=luncherClassic, LuncherNew=lunchernew, EnmapDir=enmapDir

  case strmid(!version.release, 0, 3) of
    '8.3' : enviClassicDir = file_expand_path(filepath('', ROOT_DIR=!DIR, SUBDIRECTORY=['..','ENVI51','classic']))
    '8.4' : enviClassicDir = file_expand_path(filepath('', ROOT_DIR=!DIR, SUBDIRECTORY=['..','ENVI52','classic']))
    '8.5' : enviClassicDir = file_expand_path(filepath('', ROOT_DIR=!DIR, SUBDIRECTORY=['..','ENVI53','classic']))
  endcase
    
  enviClassicSaveAddDir = filepath('save_add', ROOT_DIR=enviClassicDir)

  enviDir = file_expand_path(filepath('..', ROOT_DIR=enviClassicDir))
  enviExtensionsDir = filepath('extensions', ROOT_DIR=enviDir)

  if lmgr(/RUNTIME) then begin
    enmapBoxCrossPlatformDir = file_dirname((routine_info('enmapBoxEnviInstaller', /SOURCE)).path) ; need to use routine_info() instead of enmapBox_getDirname() because of VM issues
  endif else begin
    enmapBoxCrossPlatformDir = filepath('IDLVMCrossPlatform', ROOT_DIR=enmapBox_getDirname(/Root))
  endelse
  enmapBoxDir = file_expand_path(filepath('..', ROOT_DIR=enmapBoxCrossPlatformDir))
  
  case 1b of
    keyword_set(ini) : result = filepath('enmapBoxEnviLauncherClassic.ini', ROOT_DIR=enviClassicSaveAddDir)
    keyword_set(installer) : result = filepath('enmapBoxEnviInstaller.sav', ROOT_DIR=enmapBoxCrossPlatformDir)
    keyword_set(luncherClassic) : result = filepath('enmapBoxEnviLauncherClassic.sav', ROOT_DIR=enviClassicSaveAddDir)
    keyword_set(luncherNew) : result = filepath('enmapBoxEnviLauncherNew.sav', ROOT_DIR=enviExtensionsDir)
    keyword_set(enmapDir) : result = enmapBoxDir
  endcase

  return, result
end
