pro enmapBoxEnviLauncherNew_extensions_init, event
  compile_opt IDL2
  
  ; init EnMAP-Box

  if lmgr(/RUNTIME) then begin
    enmapDir = enmapBoxEnviLauncher_getEnmapDir()
;    if ~isa(enmapDir) then begin
;      envi.AddExtension, 'EnMAP-Box is not running :-(', 'enmapBoxEnviLauncherNew', PATH='EnMAP-Box is not running :-('
;      return
;    endif
    @enmapbox_init
  endif
  
  ; parse menu files and extend ENVI menu
  
  hubGUIMenu = hubGUIMenu()
  mainMenuFile = file_search(enmapBox_getDirname(/EnmapBox), 'enmapMain.men', Count=numberOfFiles)
  hubGUIMenu.extendMenu, mainMenuFile
  userMenuFilenames = file_search(enmapBox_getDirname(/Applications), 'enmap.men', Count=numberOfFiles)
  for i=0, numberOfFiles-1 do begin
    userMenuFilename = userMenuFilenames[i]
    print, 'Parse application menu file: '+userMenuFilename
    hubGUIMenu.extendMenu, userMenuFilename
  endfor

  hubGUIMenu.defineEnviToolbar, 'EnMAP-Box'

  ; init enmap
  
  enmapJavaBridge_init
  (hubGUIHelper()).setFixedWidgetFont

end

pro enmapBoxEnviLauncherNew, event
end