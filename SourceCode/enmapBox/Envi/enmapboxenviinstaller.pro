pro enmapBoxEnviInstallerClassic
  ; copy luncher to ENVI Classic save_add directory
  filenameInstaller = enmapBoxEnviInstaller_getFilename(/Installer)
  filenameLuncher = enmapBoxEnviInstaller_getFilename(/LuncherClassic)
  file_copy, filenameInstaller, filenameLuncher, /NOEXPAND_PATH, /OVERWRITE

  ; save EnMAP-Box installation directory to ini file
  enmapBoxEnviInstaller_saveEnmapDir
end

pro enmapBoxEnviInstallerNew
  ; copy luncher to ENVI extensions directory
  filenameInstaller = enmapBoxEnviInstaller_getFilename(/Installer)
  filenameLuncher = enmapBoxEnviInstaller_getFilename(/LuncherNew)
  file_copy, filenameInstaller, filenameLuncher, /NOEXPAND_PATH, /OVERWRITE
end

pro enmapBoxEnviInstaller

  title = 'EnMAP-Box for ENVI Installation'
  
  alreadyInstalled = file_test(enmapBoxEnviInstaller_getFilename(/INI), /REGULAR) 
  if alreadyInstalled  then begin

    answer = dialog_message(/QUESTION, TITLE=title, ['EnMAP-Box for ENVI is already installed!', 'Do you want to de-install?'])
    if answer eq 'Yes' then begin
      file_delete, enmapBoxEnviInstaller_getFilename(/INI),$
                   enmapBoxEnviInstaller_getFilename(/LuncherClassic),$
                   enmapBoxEnviInstaller_getFilename(/LuncherNew), /NOEXPAND_PATH, /ALLOW_NONEXISTENT 
      !null = dialog_message(/INFORMATION, TITLE=title, 'EnMAP-Box for ENVI successfully de-installed!')
    endif

  endif else begin
    enmapBoxEnviInstallerClassic
    enmapBoxEnviInstallerNew
    !null = dialog_message(/INFORMATION, TITLE=title, 'EnMAP-Box for ENVI successfully installed!')
  
  endelse

end