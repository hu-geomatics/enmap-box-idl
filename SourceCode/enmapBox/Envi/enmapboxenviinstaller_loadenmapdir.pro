;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function enmapBoxEnviInstaller_loadEnmapDir

  filename = enmapBoxEnviInstaller_getFilename(/INI)  
  if file_test(filename, /REGULAR) then begin
    ascii = strarr(file_lines(filename))
    openr, unit, filename, /GET_LUN
    readf, unit, ascii
    free_lun, unit
    enmapDir = strtrim(strjoin(ascii),2)
  endif
  return, enmapDir

end