;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function enmapBoxEnviLauncher_getEnmapDir

  enmapDir = enmapBoxEnviInstaller_loadEnmapDir()
  
  correctEnmapDir = file_test(filepath('enmapProject', ROOT_DIR=enmapDir), /DIRECTORY)
  while ~correctEnmapDir do begin
   
    !null = dialog_message(/INFORMATION, Title='EnMAP-Box Installation for ENVI', $
      ['EnMAP-Box installation directory is unknown.', $
       'Please select it manually.', $
       '(e.g. D:\EnMAP-Box)'])
    enmapDir = dialog_pickfile(/DIRECTORY, /MUST_EXIST, TITLE='Select EnMAP-Box Installation Folder')
  
    if enmapDir eq '' then begin
      return, !null
    endif
    saveEnmapDir = 1b
  endwhile
  
  if keyword_set(saveEnmapDir) then begin
    enmapBoxEnviInstaller_saveEnmapDir, enmapDir
  endif
  
  return, enmapDir

end