;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxEnviLauncherClassic_define_buttons, buttonInfo

  ; init EnMAP-Box

  if lmgr(/RUNTIME) then begin
    enmapDir = enmapBoxEnviLauncher_getEnmapDir()
    if ~isa(enmapDir) then begin
      envi_define_menu_button, buttonInfo, VALUE='EnMAP-Box', /MENU, REF_VALUE='File', /SIBLING, POSITION='before'
      envi_define_menu_button, buttonInfo, VALUE='EnMAP-Box is not running :-(', REF_VALUE='EnMAP-Box', POSITION='last'
      return
    endif
    @enmapbox_init
  endif
  
  ; parse menu files and extend ENVI menu
  
  hubGUIMenu = hubGUIMenu()
  mainMenuFile = file_search(enmapBox_getDirname(/EnmapBox), 'enmapMain.men', Count=numberOfFiles)
  hubGUIMenu.extendMenu, mainMenuFile
  userMenuFilenames = file_search(enmapBox_getDirname(/Applications), 'enmap.men', Count=numberOfFiles)
  for i=0, numberOfFiles-1 do begin
    userMenuFilename = userMenuFilenames[i]
    print, 'Parse application menu file: '+userMenuFilename
    hubGUIMenu.extendMenu, userMenuFilename
  endfor
  hubGUIMenu.defineEnviMenu, buttonInfo, 'EnMAP-Box'
  
  ; init enmap
  
  enmapJavaBridge_init
  (hubGUIHelper()).setFixedWidgetFont

end