;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxEnviInstaller_saveEnmapDir
  enmapDir = enmapBoxEnviInstaller_getFilename(/EnmapDir)
  filenameINI = enmapBoxEnviInstaller_getFilename(/INI)
  openw, unit, filenameINI, /GET_LUN, WIDTH=9999
  printf, unit, enmapDir
  free_lun, unit
end