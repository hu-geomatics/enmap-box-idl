;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxVM_createSplash

  splashRaw = filepath('splash.bmp', ROOT_DIR=(enmapBoxDevPath()).getCodeDir(/EnmapBox), SUBDIR='VM')
  splashVersioned = filepath('splash.bmp', ROOT_DIR=(enmapBoxDevPath()).getVMDir('crossPlatform'))
  appInfo = enmapBoxSettingsLocal_getApplicationInformation()
  version = 'v'+appInfo.version
  !null = query_bmp(splashRaw, info)
  raw = read_bmp(splashRaw,/Rgb)
  window,/FREE,XSIZE=info.dimensions[0],YSIZE=info.dimensions[1]
  tv, raw, TRUE=1
;  cgtext, 0.95, 0.05, version, Color='black', /Normal, Charsize=1, Width=2, Alignment=1
  xyouts, 0.95, 0.05, version, ALIGNMENT=1
  write_bmp, splashVersioned, tvrd(TRUE=1), /RGB

  wdelete

end