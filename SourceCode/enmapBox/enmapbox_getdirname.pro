;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns specific EnMAP-Box directories. Set one of the mutually exclusive keywords
;    `EnmapProject`, `EnmapBox`, `Applications` or `Lib`. 
;    Optionally, set `SourceCode` to return the associated source code directory.
;    
; :Keywords:
;    EnmapProject: in, optional, type=boolean
;    EnmapBox:     in, optional, type=boolean
;    Applications: in, optional, type=boolean
;    Lib:          in, optional, type=boolean
;    SourceCode:   in, optional, type=boolean
;      Set this keyword to return the associated source code folder.
;
; :Examples:
;    Print directory locations::
;      print, enmapBox_getDirname(/EnmapProject)
;      print, enmapBox_getDirname(/EnmapBox)
;      print, enmapBox_getDirname(/Applications)
;      print, enmapBox_getDirname(/Lib)
;      
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject
;      D:\EnMAP-Box\enmapProject\enmapBox
;      D:\EnMAP-Box\enmapProject\applications
;      D:\EnMAP-Box\enmapProject\lib
;      
;    Print directory locations of associated source code::
;      print, enmapBox_getDirname(/EnmapProject, /SourceCode)
;      print, enmapBox_getDirname(/EnmapBox, /SourceCode)
;      print, enmapBox_getDirname(/Applications, /SourceCode)
;      print, enmapBox_getDirname(/Lib, /SourceCode)
;      
;    IDL prints something like::
;      D:\EnMAP-Box\SourceCode
;      D:\EnMAP-Box\SourceCode\enmapBox
;      D:\EnMAP-Box\SourceCode\applications
;      D:\EnMAP-Box\SourceCode\lib
;      
;-
function enmapBox_getDirname $
  , EnmapProject=enmapProject $
  , EnmapBox=enmapBox $
  , Applications=applications $
  , Lib=lib $
  , SourceCode=sourceCode $
  , Root=root
  
  if keyword_set(root) then begin
    result = enmapBoxDevPath.getRootDir()
  endif else begin
    if keyword_set(sourceCode) then begin
      result = enmapBoxDevPath.getCodeDir(Root=enmapProject, EnmapBox=enmapBox, Applications=applications, Lib=lib)
    endif else begin
      result = enmapBoxDevPath.getProjectDir(Root=enmapProject, EnmapBox=enmapBox, Applications=applications, Lib=lib)
    endelse
  endelse
  return, result
end

;+
; :Hidden:
;-
pro test_enmapBox_getDirname

  print, enmapBox_getDirname(/EnmapProject)
  print, enmapBox_getDirname(/EnmapBox)
  print, enmapBox_getDirname(/Applications)

  print
  print, enmapBox_getDirname(/EnmapProject,/SourceCode)
  print, enmapBox_getDirname(/EnmapBox,/SourceCode)
  print, enmapBox_getDirname(/Applications,/SourceCode)
  

end