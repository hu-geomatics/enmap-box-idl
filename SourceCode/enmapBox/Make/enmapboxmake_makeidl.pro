;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_makeIDL 

  ; copy files

  codeDir = filepath('IDL',ROOT_DIR=enmapBox_getDirname(/EnMAPProject, /SourceCode), SUBDIRECTORY='lib')
  targetDir = filepath('IDL', ROOT_DIR=enmapBox_getDirname(/EnMAPProject), SUBDIRECTORY='lib')
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir

  if ~enmapboxmake.CopyOnly() then begin
    ; create SAVE
  
    codeDir =  filepath('lib')
    SAVEFile = filepath('idl.sav', ROOT_DIR=targetDir)
    hubDev_compileDirectory, codeDir, SAVEFile, nolog=enmapboxmake.noLog()
  endif
end