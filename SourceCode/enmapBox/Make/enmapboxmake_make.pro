;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_make
  
  CopyOnly = enmapboxmake.copyOnly() 
  NoIDLDoc = enmapboxmake.noIDLDoc()
  nolog    = enmapboxmake.nolog()
  
  enmapBoxDev_checkForUppercaseFilenames, enmapBox_getDirname(/EnmapProject, /SourceCode)
  file_delete, enmapBox_getDirname(/EnmapProject), /ALLOW_NONEXISTENT, /RECURSIVE
  
  enmapBoxMake_makeBox
  enmapBoxMake_makeIDL
  enmapBoxMake_makeIDLdoc 
  enmapBoxMake_makeVM
  enmapBoxMake_makeEnvi
  hub_make
  hubApp_make

  ;compile external IDL libraries
;  enmapBoxMake_makeCoyote, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc
;  enmapBoxMake_makeMortCanty, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc
  
  ;compile further Apps that are not part of the EnMAP-Box Core Distribution
  applications_makeExternals
  
  print, 'Done'
end