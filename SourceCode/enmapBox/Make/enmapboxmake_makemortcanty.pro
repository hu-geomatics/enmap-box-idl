;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_makeMortCanty, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc

  ; copy files

  codeDir = filepath('mortCanty',ROOT_DIR=(enmapBoxDevPath()).getCodeDir(/Root), SUBDIRECTORY='lib')
  targetDir = filepath('mortCanty', ROOT_DIR=(enmapBoxDevPath()).getProjectDir(/Root), SUBDIRECTORY='lib')
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir

  if ~keyword_set(CopyOnly) then begin
    SAVEFile = filepath('mortCanty.sav', ROOT_DIR=targetDir)
    hubDev_compileDirectory, codeDir, SAVEFile
  endif
end