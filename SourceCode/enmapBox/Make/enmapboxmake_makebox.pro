;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    
;
;
;
; :Keywords:
;    CopyOnly: in, optional, type=boolean
;     Set this keyword to copy the source code and resources files only without creating or compiling 
;     anything. This is useful when you want to change the setting/help files only.
;
;-
pro enmapBoxMake_makeBox

  ; copy files 

  codeDir = enmapBox_getDirname(/EnmapBox, /SourceCode)
  targetDir = enmapBox_getDirname(/EnmapBox)
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir
  
  if ~enmapboxmake.CopyOnly() then begin
    ; create SAVE
  
    SAVFile = filepath('enmapBox.sav', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='lib')
    hubDev_compileDirectory, codeDir, SAVFile, nolog=enmapboxmake.nolog()
    
    ; create documentation
    if ~enmapboxmake.NoIDLDoc() then begin
      docDir = filepath('', ROOT_DIR=enmapBox_getDirname(/EnmapBox), Subdirectory=['help','idldoc'])
      title='enmapBox Documentation'
      hub_idlDoc, codeDir, docDir, title,NOSHOW=enmapboxmake.noShow() 
    endif
  endif
end