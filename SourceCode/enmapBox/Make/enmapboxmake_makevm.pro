;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_makeVM

  if ~enmapboxmake.CopyOnly() then begin
    ; compile VM launcher
  
    codeDir =  filepath('VM', ROOT_DIR=enmapBox_getDirname(/EnmapBox, /SourceCode))
    SAVFile = (enmapBoxDevPath()).getSaveFile(/VMLauncher)
    hubDev_compileDirectory, codeDir, SAVFile, nolog=enmapboxmake.nolog()
    
    ; create splash screen
    
    enmapBoxVM_createSplash
  endif
end