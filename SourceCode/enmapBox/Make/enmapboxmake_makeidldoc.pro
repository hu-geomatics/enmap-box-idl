;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_makeIDLdoc

  codeDir = filepath('IDLdoc',ROOT_DIR=(enmapBoxDevPath()).getCodeDir(/Root), SUBDIRECTORY='lib')
  targetDir = filepath('IDLdoc', ROOT_DIR=(enmapBoxDevPath()).getProjectDir(/Root), SUBDIRECTORY='lib')
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir
  
end