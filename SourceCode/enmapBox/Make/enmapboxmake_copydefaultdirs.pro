;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_copyDefaultDirs, rootSourceDir, rootTargetDir
  
  defaultSourceSubdirs   = ['_copyrights','_help','_lib','_resource']
  defaultTargetSubdirs = [ 'copyrights', 'help', 'lib', 'resource']
  sourceDirs = filepath(defaultSourceSubdirs, ROOT=rootSourceDir)
  targetDirs = filepath(defaultTargetSubdirs, ROOT=rootTargetDir)
  numberOfDirs = n_elements(sourceDirs) 
  for i=0,numberOfDirs-1 do begin
    if file_test(sourceDirs[i]) then begin
      file_delete, targetDirs[i], /ALLOW_NONEXISTENT, /RECURSIVE, QUIET=0b
      file_copy, sourceDirs[i], targetDirs[i], /NOEXPAND_PATH, /OVERWRITE, /RECURSIVE
    endif
  endfor
  
end