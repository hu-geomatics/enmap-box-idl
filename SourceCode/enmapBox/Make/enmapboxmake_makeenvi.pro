;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_makeEnvi
  codeDir =  filepath('Envi', ROOT_DIR=enmapBox_getDirname(/EnmapBox, /SourceCode))
  SAVFile = enmapBoxDevPath.getSaveFile(/EnviInstaller)
  hubDev_compileDirectory, codeDir, SAVFile
end