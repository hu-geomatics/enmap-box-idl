;+
; :Author: geo_beja
; 
; :Description: The idea is to use this file to collect all required make/compile flags
;   Further it is planned to add basic compile & destribution routines here.
; 
;-

function enmapboxmake::NoIDLDoc
  COMPILE_OPT static
  return, 0b
end

function enmapboxmake::CopyOnly
  COMPILE_OPT static
  return, 0b
end

function enmapboxmake::NoShow
  COMPILE_OPT static
  return, 0b
end

function enmapboxmake::NoLog
  COMPILE_OPT static
  return, 0b
end

function enmapboxmake::clean
  COMPILE_OPT static
  return, 0b
end


;+
; :Hidden:
;-
pro enmapboxmake__define
  struct = {enmapboxmake $
    ,inherits IDL_Object $
  }
end