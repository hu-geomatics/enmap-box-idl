;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro enmapBoxMake_makeCoyote, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc

  ; copy files

  codeDir = filepath('coyote',ROOT_DIR=(enmapBoxDevPath()).getCodeDir(/Root), SUBDIRECTORY='lib')
  targetDir = filepath('coyote', ROOT_DIR=(enmapBoxDevPath()).getProjectDir(/Root), SUBDIRECTORY='lib')
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir

  if ~keyword_set(CopyOnly) then begin
    ; create SAVE
  
    SAVEFile = filepath('coyote.sav', ROOT_DIR=targetDir)
    hubDev_compileDirectory, codeDir, SAVEFile

    ; create documentation
;    if ~keyword_set(noIDLDoc) then begin
;      docDir = filepath('', ROOT_DIR=targetDir, SUBDIR=['help','idldoc'])
;      title='Coyote Graphics Documentation'
;      hub_idlDoc, codeDir, docDir, title
;    endif
  endif
end