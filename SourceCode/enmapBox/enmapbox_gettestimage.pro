;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns a specific or all EnMAP-Box test image filenames.
;    
; :Params:
;    basename: in, required, type=file basename
;      Use this argument to specify the file basename for the file that should be returned.
;      If omitted, all test image filenames are returned.
;    
; :Examples:
;    Print all test image filenames::
;      print, transpose(enmapBox_getTestImage())
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_AdminBorders
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_Image
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_LAI
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_LAI_Training
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_LAI_Validation
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_LC
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_LC_Training
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_LC_Validation
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_Mask
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_MaskVegetation
;    
;    Print a specific filename::
;      print, enmapBox_getTestImage('AF_Image')
;      
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\AF_Image
;    
;    Try to get a filename that is not part of the test image set::
;      print, enmapBox_getTestImage('MyImage')
;      
;    IDL trows an error::
;      % ENMAPBOX_GETTESTIMAGE: File is not part of enmapBox test image set: D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\image\MyImage
;-
function enmapBox_getTestImage, basename
  
  getAll = ~isa(basename)
  root = filepath('', ROOT=enmapBox_getDirname(/EnmapBox), SUBDIR=['resource','testData','image'])
  if getAll then begin
    result = list(file_search(root, '*.hdr'), /EXTRACT)
    foreach filename, result, i do begin
      if strcmp(file_basename(filename), 'pyramid.', 8) then begin
        result[i] = ''
      endif else begin
        result[i] = hubHelper.removeFileExtension(filename)
      endelse
    endforeach
    result.remove, result.where('') 
  endif else begin
    result = filepath(basename, ROOT=root)
    if ~(hubIOHelper()).fileReadable(result) then begin
      message, 'File is not part of enmapBox test image set: '+result
    endif
  endelse
  return, result

end

;+
; :Hidden:
;-
pro test_enmapBox_getTestImage
  
  print, enmapBox_getTestImage(/All)
  print, enmapBox_getTestImage('AF_Image')
end
