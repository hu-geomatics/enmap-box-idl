;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the EnMAP-Box installer procedure.  
;-
pro enmapBox_installer

  @huberrorcatch

  ; read license files
  hubIOASCIIHelper = hubIOASCIIHelper()
  text = []
  
  filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
  text = [text, '', '+++++++++++++++++', '+++ EnMAP-Box +++', '+++++++++++++++++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
  text = [text, '','+++ hubAPI +++']
  text = [text, 'Is released under the EnMAP-Box license.']

  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/EnmapBox), SUBDIRECTORY='copyrights')
  text = [text, '','+++ hubApplications +++']
  text = [text, 'Is released under the EnMAP-Box license.']

  text = [text, '', '', '+++++++++++++++++++++++++++++', '+++ Third Party Libraries +++', '+++++++++++++++++++++++++++++']

  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/Lib), SUBDIRECTORY=['IDLdoc','copyrights'])
  ;text = [text, '','+++ IDLdoc +++']
  ;text = [text, hubIOASCIIHelper.readFile(filename)]

  ;filename = filepath('license.txt', ROOT_DIR=enmapBox_getDirname(/Lib), SUBDIRECTORY=['coyote','copyrights'])
  ;text = [text, '','+++ Coyote Graphics System +++']
  ;text = [text, hubIOASCIIHelper.readFile(filename)]

  text = [text, '', '', '++++++++++++++++++++++++++++++', '+++ EnMAP-Box Applications +++', '++++++++++++++++++++++++++++++']

  filename = filepath('license_imageSVM.txt', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='copyrights')
  text = [text, '','+++ imageSVM +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  filename = filepath('license_libSVM.txt', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='copyrights')
  text = [text, '','+++ libSVM +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  filename = filepath('license_imageRF.txt', ROOT_DIR=imageRF_getDirname(), SUBDIRECTORY='copyrights')
  text = [text, '','+++ imageRF +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]
  
  filename = filepath('license.txt', ROOT_DIR=imageMath_getDirname(), SUBDIRECTORY='copyrights')
  text = [text, '','+++ imageMath +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  filename = filepath('license_autoPLSR.txt', ROOT_DIR=autoPLSR_getDirname(), SUBDIRECTORY='copyrights')
  text = [text, '','+++ autoPLSR +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  ;filename = filepath('license_MaxEntWrapper.txt', ROOT_DIR=MaxEntWrapper_getDirname(), SUBDIRECTORY='copyrights')
  ;text = [text, '','+++ MaxEntWrapper +++']
  ;text = [text, hubIOASCIIHelper.readFile(filename)]

  filename = filepath('license.txt', ROOT_DIR=SpInMine_getDirname(), SUBDIRECTORY='copyrights')
  text = [text, '','+++ SpInMine +++']
  text = [text, hubIOASCIIHelper.readFile(filename)]

  ;filename = filepath('license.txt', ROOT_DIR=hubLibMix_getDirname(), SUBDIRECTORY='copyrights')
  ;text = [text, '','+++ LibMix +++']
  ;text = [text, hubIOASCIIHelper.readFile(filename)]
  
  title = 'EnMAP-Box Installer'
  hubAMW_program, Title=title
  hubAMW_label, ['License Agreement',$
                 'Please read the following License Agreement.',$
                 'Use the scroll bar to view the rest of the agreement.']
  hubAMW_text, 'text', Value=text, /Scroll, /Wrap, XSIZE=(get_screen_size())[0]*0.5, YSIZE=(get_screen_size())[1]*0.3
  hubAMW_label, ['Do you accept all the terms of the preceding License Agreement?',$
                 'If you choose Cancel, Installer will close.',$
                 'To install EnMAP-Box, you must accept this agreement.']
  result = hubAMW_manage()

end