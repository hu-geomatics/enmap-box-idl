pro enmapBoxCoreApp_settings_event, event

  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event, /ToStructure)
  case applicationInfo.argument of
    'enmapBox' : enmapBoxSettings_editor, Title=applicationInfo.name, GroupLeader=applicationInfo.groupLeader
    'hubAPI' : hubSettings_editor, Title=applicationInfo.name, GroupLeader=applicationInfo.groupLeader
  endcase

end