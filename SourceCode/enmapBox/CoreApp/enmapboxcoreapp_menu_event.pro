pro enmapBoxCoreApp_menu_event, event

  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event, /ToStructure)
  enmapBox = enmapBox()
  
  case applicationInfo.argument of
    'open_image' : begin
      filenames = dialog_pickfile(DIALOG_PARENT=event.top, MULTIPLE_FILES=1, /MUST_EXIST, TITLE='Select Image or Spectral Library Files')
      if ~keyword_set(filenames) then return
      cd, file_dirname(filenames[0])
      foreach filename, filenames do begin
        enmapBox.openImage, filename
      endforeach
    end
    'open_file' : begin
      filenames = dialog_pickfile(DIALOG_PARENT=event.top, MULTIPLE_FILES=1, /MUST_EXIST, TITLE='Select Files')
      if ~keyword_set(filenames) then return
      cd, file_dirname(filenames[0])
      foreach filename, filenames do begin
        enmapBox.openFile, filename
      endforeach
    end
    'quit' : begin
      enmapBox.close
    end
  endcase
  
end