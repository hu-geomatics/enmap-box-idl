pro test_CS_runSVCModels
  labelFile      = 'T:\2CS\training\training'
  dirModels      = 'T:\2CS\models\'
  dirImages      = 'T:\2CS\images\'
  dirEstimations = 'T:\2CS\estimations\'
  FILE_MKDIR, dirEstimations
  
  inputFiles = file_search(dirImages, '*', /FOLD_CASE)
  inputImages = list()
  modelFilenames = list()
  estimationFilenames = list()
  foreach file, inputFiles do begin
    if stregex(file, '.*\.hdr$', /Boolean) then continue
    bn = FILE_BASENAME(file, '.dat', /FOLD_CASE)
    inputImages.add, file
    modelFilenames.add, FILEPATH(bn+'.svc', root_dir = dirModels)
    estimationFilenames.add, FILEPATH(bn+'_est', root_dir = direstimations)
  endforeach
  

  ;create a new Task Manager
 ; taskMGR = hubIDLTaskManager()
  
  foreach img, inputimages, i do begin

;
     test_cs_runsvcmodel, modelFilename=modelFilenames[i] $
                        , featureFilename=img $
                        , labelFilename=labelFile $
                        , outputLabelFilename=estimationFilenames[i]
;    break
 
;    taskMGR.addTask, 'test_cs_runsvcmodel' $
;                   , modelFilename=modelFilenames[i] $
;                   , featureFilename=img $
;                   , labelFilename=labelFile $
;                   , outputLabelFilename=estimationFilenames[i] 
  print, 'done with '+strtrim(i+1,2)
  endforeach
  
  ;run the task manager
  ;taskMGR.runTaskManager
  
  print, 'done'

end