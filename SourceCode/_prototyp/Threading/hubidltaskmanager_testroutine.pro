pro hubIDLTaskManager_testRoutine, time2wait=time2wait, runFFT=runFFT, infostruct=infostruct, raiseError=raiseError
  print, 'Start test routine'
  print, systime(0)
  t0 = systime(1)
  
  if isa(time2wait) then begin
    print, 'Wait for '+strtrim(time2wait,2)+ ' seconds...'
    wait, time2wait
  endif 
  
  if keyword_set(runFFT) then begin
    print, 'Run FFT...'
    max = 10000
    seed = systime(1)
    d1 = 1 > randomu(seed) * max < max 
    d2 = 1 > randomu(seed) * max < max
    
    help, fft(randomu(seed, d1, d2), /DOUBLE),OUTPUT=helpoutput
    print, helpoutput
  endif 
  
  if isa(infoStruct) then begin
    print, 'Print the infoStruct argument...'
    print, infoStruct
  endif
  
  if KEYWORD_SET(raiseError) then begin
    print, 'Raise an error message...'
    message, 'This is a hubIDLTaskManager_testRoutine test error message'
  endif
  
  print, 'Required Time: ', systime(1)-t0
  ;print, infostruct
  print, 'Finished '+(SCOPE_TRACEBACK())[-1]
end