;+
; :Author: geo_beja
; :version: 2.1
; 
; :description:
;
;   The hubIDLTaskManager object can be used to run multiple IDL routines in parallel and to control
;   this using a GUI interface.
;   
;   Last modifications
;     
;     2015-06-17 close unused IDL_Bridges and reduce size of bridge-pool dynamically
;     2015-05-11 fixed ENVI Preference file error pass-through
;     2015-04-22 fixed error logs for IDL_IDLBridge errors 
;   
;-


; Begin the event handler routine for the EXAMPLE widget:
PRO hubIDLTaskManager_GUICallback_event, ev
  ;get the Object
  WIDGET_CONTROL, ev.top, GET_UVALUE = oTaskManager
  tname = typename(oTaskManager)
  if tname eq 'HUBIDLTASKMANAGER' then begin
    oTaskManager.GUICallback, ev
  endif else begin
    self._printInfo, string(format='(%"Unknown GUI Callback object %s")',tname), /AddToConsole 
  endelse
  
END

pro hubIDLTaskManager_BridgeCallback, status, Error, oBridge, taskInfo
  if isa(taskInfo) then begin
    taskManager = taskInfo.taskManager
    taskManager.BridgeCallback, status, Error, oBridge, taskInfo
  endif else begin
    
    print, 'IDL bridge callback without taskInfo object'
    if isa(status) then print, 'Status: ', status
    if isa(error) then print, 'Error:', error
    if isa(oBridge) then print, 'oBridge', oBridge
  endelse
end

;+
; :Description:
;    Constructor to create a new hubIDLTaskManager instance.
;    
; :Keywords:
;    maxNumberOfActiveBridges:in, optional, type=int
;      The maximum number of tasks that is processed in parallel.
;      Can be any value between zero and the number of CPU / CPU cores.
;      
;    numberOfCPUs: in, optional, type=int
;     The max. number of CPUs that can be used . The default value is set to the systems value.
;      
;    callbackWaitTime: in, optional, type=float, default=0.5
;     Time in seconds to wait for a callback.
;     
;    dirlogfiles: in, optional, type=string
;     Directory to save the logfiles.
;     
;    dirtmpfiles: in, optional, type=string
;     Directory to store temporary data.
;     Default = IDL temp folder.
;     
;    createLogFiles: in, optional, type=boolean
;     Set this to generate a logfile for each task added by addTask().
;    
;    skipErrors: in, optional, type=boolean, default=False
;     Set this to not block if errors messages occured 
;     
;    GC: in, optional, type=boolean, default=true
;     Set this to start an automatic garbage collection (using the `HEAP_GC` command) after a task has finished.
;
; :Author: geo_beja
;-
function hubIDLTaskManager::init $
  , maxNumberOfActiveBridges=maxNumberOfActiveBridges $
  , numberOfCPUs=numberOfCPUs $
  , callbackWaitTime=callbackWaitTime $
  , dirlogfiles=dirlogfiles $
  , dirtmpfiles=dirtmpfiles $ 
  , createLogFiles = createLogFiles $
  , skipErrors = skipErrors $
  , GC=GC 
   
   
  self.workState = 'initial'
  self.nCPUs = isa(numberOfCPUs) ? numberOfCPUs : !CPU.TPOOL_NTHREADS < !CPU.HW_NCPU
  self.nBridgesMax = isa(maxNumberOfActiveBridges)? maxNumberOfActiveBridges < self.nCPUs : self.nCPUs
  self.callbackWaitTime= isa(callbackWaitTime)? callbackWaitTime : 0.5
  self.skipErrors = KEYWORD_SET(skipErrors)
  
  if isa(dirtmpfiles) then begin
    IF FILE_TEST(dirtmpFiles) then message, dirtmpfiles + ' is an existing file but not a possible directory for temporary files (dirTmpFiles).'
    self.dirtmpfiles = dirtmpfiles
  endif else begin
    self.dirtmpfiles = FILEPATH('', /TMP)
  endelse
  
  if isa(dirlogfiles) then begin
    IF FILE_TEST(dirlogfiles) then message, dirlogfiles + ' is an existing file but not a possible directory for log files (dirLogFiles).'
    self.dirlogfiles = dirlogfiles
  endif else begin
    self.dirlogfiles = self.dirTmpFiles
  endelse
  
   
  self.createLogfiles = keyword_set(createLogFiles) or isa(dirLogFiles) 
  self.callGC = isa(GC)? keyword_set(GC) : 1b
  self.bridgePool = list()
  
  
  
  FILE_MKDIR, self.dirTMPFiles 
  if self.createLogFiles then FILE_MKDIR, self.dirLogFiles
  
  
  self.bridgeSet=Hash()
  self.taskInfoList=List()
  
  self._initGUI
  return, 1b
  
end


pro hubIDLTaskManager::_initGUI
  
  baseWidth = 600
  
  ; Create the top-level base for the widget:
  base = WIDGET_BASE(TITLE='HUB IDL Task Manager' $
        ;, XSIZE=600 $
        , /COLUMN, /ALIGN_LEFT, UNAME='widBase', UVALUE=self, SENSITIVE=0 $
        , /TLB_KILL_REQUEST_EVENTS)
  self.topLevelBase = base
  
  
  base1 = WIDGET_BASE(base, /ALIGN_LEFT, /GRID_LAYOUT, ROW=2, /BASE_ALIGN_BOTTOM)
    !NULL = WIDGET_LABEL(base1, VALUE='Max. active threads:', /ALIGN_RIGHT)
    !NULL = WIDGET_SLIDER(base1 $
      , UNAME = 'widNumberOfBridgesMax', MINIMUM=0 $
      , MAXIMUM= self.nCPUs $
      , VALUE = self.nBridgesMax $
      ,/ALIGN_LEFT)
      
    !NULL = WIDGET_LABEL(base1, VALUE='Active threads:', /ALIGN_RIGHT)
    !NULL = WIDGET_LABEL(base1, VALUE='  ', UNAME='widNumberOfBridgesRunning', /ALIGN_LEFT, XSIZE=15)
  
  !NULL = WIDGET_LABEL(base, VALUE='Tasks Info:',/ALIGN_LEFT )
  !NULL = WIDGET_TEXT(base, VALUE='0', UNAME='widTaskInfo' $
                    , FONT='Courier*fixed*5' $
                    , EDITABLE=0, FRAME=0, YSIZE=15 $
                    , XSIZE=200, SCR_XSIZE=baseWidth)
  
  
  colLabels = ['ID','UID', 'routine   ', 'state   ','logfile  ']
  !NULL = WIDGET_TABLE(base, UNAME='widInfoTable' $
          , /DISJOINT_SELECTION $
          , /NO_COPY $
          , /RESIZEABLE_COLUMNS $
          , COLUMN_LABELS=colLabels $
          , COLUMN_WIDTHS=strlen(colLabels)*15. $
          , /ROW_MAJOR $
          , Editable=0 $
          , XSIZE=n_elements(colLabels) $
          , X_SCROLL_SIZE = baseWidth $
          , SCR_XSIZE=baseWidth $
          , SCR_YSIZE=250 $
          )
          
  baseTableOptions = WIDGET_BASE(base, /ALIGN_LEFT, /ROW) 
    
    !NULL = WIDGET_BUTTON(baseTableOptions, VALUE = 'Start'  , UNAME = 'widTaskStart' $
                          , YSIZE=25, SENSITIVE=0 $
                          , TOOLTIP='Start selected tasks')
    !NULL = WIDGET_BUTTON(baseTableOptions, VALUE = 'Restart', UNAME = 'widTaskRestart' $
                          , YSIZE=25, SENSITIVE=0 $
                          , TOOLTIP='Restart selected tasks')
    !NULL = WIDGET_BUTTON(baseTableOptions, VALUE = 'Cancel' , UNAME = 'widTaskCancel' $
                          , YSIZE=25, SENSITIVE=0 $
                          , TOOLTIP='Cancel selected tasks')
    !NULL = WIDGET_BUTTON(baseTableOptions, VALUE = 'Remove' , UNAME = 'widTaskRemove' $
                          , YSIZE=25, SENSITIVE=1 $
                          , TOOLTIP='Remove selected tasks from list')                      
    !NULL = WIDGET_BUTTON(baseTableOptions, VALUE = 'Show Logfile' , UNAME = 'widTaskShowLogfile', YSIZE=25, SENSITIVE=1)
    
  !NULL = WIDGET_TEXT(base, VALUE='', UNAME = 'widConsole' $
            , scr_YSIZE=150, scr_xsize=baseWidth, XSIZE=72 $
            , SENSITIVE=1, /SCROLL)
 
  baseBottom = WIDGET_BASE(base, /ALIGN_LEFT, /ROW)
          !NULL = WIDGET_BUTTON(baseBottom, VALUE = 'Do Work' , UNAME = 'widDoWork' $
                              , XSIZE=100, YSIZE=50 $
                              , TOOLTIP='Press to start working on all tasks')
          !NULL = WIDGET_BUTTON(baseBottom, VALUE = 'Cancel Work'  , UNAME = 'widCancelWork' $
                              , XSIZE=100, YSIZE=50 $
                              , TOOLTIP = 'Cancel all running or waiting tasks')
          !NULL = WIDGET_BUTTON(baseBottom, VALUE = 'Close' , UNAME = 'widClose' $
                              , XSIZE=50, YSIZE=50 $
                              , TOOLTIP = 'Cancel all tasks and close the Task Manager')
  
  ; Realize the widget (i.e., display it on screen):
  ;WIDGET_CONTROL, base, /REALIZE
  
  ;store each widget with it's widget ID in a hash
  
  
  ; Register the widget with the XMANAGER, leaving the IDL command
  ; line active:
  
  XMANAGER, 'hubIDLTaskManager_GUICallback', base, /NO_BLOCK
  ;XMANAGER, 'hubIDLtaskManager',EVENT_HANDLER='hubObjectCallback', base, /NO_BLOCK
  self._setWorkStateButtons, 'initial'
  ; End of the widget creation part:
  
end



pro hubIDLTaskManager::_refreshTaskInfo
  
  if self.workState eq 'finished' then return
  
  
  wid = self._getWidgetID('widNumberOfBridgesRunning')
  if isa(wid) then widget_control,wid ,SET_VALUE = strtrim(self._getNumberOfActiveBridges(),2)
  
  ;refresh the info table
  
  infoArray = (self.taskInfoList).toArray()
    wid = self._getWidgetID('widInfoTable')
    if isa(wid) then begin
      view = widget_info(wid, /TABLE_VIEW)
      
      ;widget_control,wid, SET_VALUE=[infoArray], SET_TABLE_VIEW=view
      if n_elements(infoArray) eq 1 then begin
        widget_control,wid, SET_VALUE=infoArray[0] $
          , YSIZE=n_elements(infoArray)        
      endif else begin
        widget_control,wid, SET_VALUE=infoArray $
                      , YSIZE=n_elements(infoArray)
      endelse 
    endif
  
  
    ;refresh
    nTasks = 0ul
    nTasksWaiting  = 0ul
    nTasksCanceled = 0ul
    nTasksCompleted = 0ul
    nTasksRunning  = 0ul
    nTasksError  = 0ul
    foreach info, self.taskInfoList do begin
      nTasks++
      case info.state of
        'await start'   : nTasksWaiting++
        'await restart' : nTasksWaiting++
        'unstarted'     : nTasksWaiting++
        'canceled'      : nTasksCanceled++
        'completed'     : nTasksCompleted++
        'error'         : nTasksError++
        'running'       : nTasksRunning++
      endcase
    endforeach
    
    infoText = ['Jobs:' $
               ,'  Total:     ' + strtrim(n_elements(self.taskInfoList),2) $
               ,'  ------------------' $
               ,'  Waiting:   ' + strtrim(nTasksWaiting,2) $
               ,'  Running:   ' + strtrim(nTasksRunning,2) $
               ,'  ------------------' $
               ,'  Canceled:  ' + strtrim(nTasksCanceled,2) $
               ,'  Error:     ' + strtrim(nTasksError,2) $
               ,'  ------------------' $
               ,'  Completed: ' + strtrim(nTasksCompleted,2) $
               ,'' $
               ]
                
    wid = self._getWidgetID('widTaskInfo')
    if isa(wid) then widget_control,wid ,SET_VALUE = infoText
  
end


function hubIDLTaskManager::_getWidgetID, uname
  if ~widget_info(self.topLevelBase, /VALID_ID) then return, !NULL
  return, widget_info(self.topLevelBase, FIND_BY_UNAME=uname) 
end

pro hubIDLTaskManager::GUICallback, ev
;  if ~WIDGET_INFO(self.topLevelBase, /VALID_ID) then begin
;    return
;  endif
;  if ~isa(uname) then return
  uname = WIDGET_INFO(ev.id, /UNAME)
  case uname of
      'widBase' : begin
        if typename(ev) eq 'WIDGET_KILL_REQUEST' then begin
          self.workState = 'finished'
        endif
        end
      'DONE' :  stop
      'widNumberOfBridgesMax' : begin
        ;change max number of processes
        WIDGET_CONTROL, self._getWidgetID(uname), GET_VALUE = nBridgesMax
        self.nBridgesMax = nBridgesMax
        self._printInfo, 'Max. number of active threads changed to '+strtrim(nBridgesMax,2)
        self._setWorkStateButtons
      end
      'widDoWork' : begin
        self.workState = 'started'
        self._setWorkStateButtons
      end
      
      'widCancelWork': begin
        self.workState = 'stopped'
        ;kill all open processes
        for i= 0, n_elements( self.taskInfoList) - 1 do begin
          taskInfo = ((self.taskInfoList)[i])
          if  total(taskInfo.state eq ['completed','finished','error']) gt 0 then begin
            taskInfo.state = 'canceled'
            self._setTaskInfo, taskInfo
          endif
          self._setWorkStateButtons
        endfor
        
        self._checkForCallback
        foreach bridgeKey, (self.bridgeSet).keys() do begin
          oBridge = (self.bridgeSet)[bridgeKey]  
          if OBJ_VALID(oBridge) then begin  
            oBridge.abort
          endif
        endforeach
        self.flagRefreshTaskInfo = 1b
        
      end
      
      'widTaskStart' : begin
          taskIDs = self._getSelectedTaskIDs()
          foreach taskID, taskIDs do begin
            self._startTask, taskID
          endforeach
        end
     
      'widTaskRestart' : begin
          taskIDs = self._getSelectedTaskIDs()
          foreach taskID, taskIDs do begin
            self._restartTask, taskID
          endforeach
        end
        
      'widTaskCancel' : begin
          taskIDs = self._getSelectedTaskIDs()
          foreach taskID, taskIDs do begin
            self._cancelTask, taskID
          endforeach
        end
        
      'widTaskRemove' : begin
          taskIDs = self._getSelectedTaskIDs()
          foreach taskID, taskIDs do begin
            self._removeTask,taskID
          endforeach
      end
      'widTaskShowLogfile' : begin
          taskIDs = self._getSelectedTaskIDs()
          foreach taskID, taskIDs do begin
            taskInfo = self._getTaskInfo(taskID)
            if isa(taskInfo) && FILE_TEST(taskInfo.logfile) then begin
              if !VERSION.OS_FAMILY eq 'Windows' then begin
                spawn, '"'+taskinfo.logfile+'"'
              endif else begin
                xdisplayfile, taskInfo.logfile
              endelse
              
            endif
          endforeach
        end
      'widClose' : begin
        self.workState = 'finished'
                   end
      else : begin
         test=''
         end
    endcase
    
    
end

pro hubIDLTaskManager::BridgeCallback, status, Error, oBridge, taskInfo

  if ~OBJ_VALID(self) then return

  routineInfo = string(format='(%"%s (%i \"%s\")")',taskInfo.routine, taskInfo.id, taskInfo.userid)
  info = !NULL
  
  if isa(error) then begin
    error = strtrim(error,2)
  endif else begin
    error = ''
  endelse
  
  if status eq 3 then begin
    
  
    if self.skipErrors || $
       ;some known but not important error message 
       stregex(error, "Attempt to call undefined procedure.*HUBIOIMGMETA_.*", /BOOLEAN, /FOLD_CASE) || $
       stregex(error, "Program caused arithmetic error: Floating illegal operand", /BOOLEAN) || $
       stregex(STRCOMPRESS(error, REMOVE_ALL=1), "FILE_DELETE:.*envi_preferen.*ces\.json.*", /BOOLEAN, /FOLD_CASE) || $
       stregex(error, "Procedure HASH can't be restored while active", /BOOLEAN) $
     then begin
      
      status = 2
      error = ''
      
    endif
  endif

  CASE status of
    2: begin ;completed
      taskInfo.state = 'completed'
      info = string(routineInfo, format='(%"hubIDLTaskManager::Session completed (%s)")')
    end
    3: begin ;error
      taskInfo.state = 'error'
      info = string(routineInfo, error, format='(%"hubIDLTaskManager::Session error: %s \"%s\"")')
      
    end
    4: begin ;aborted
      taskInfo.state = 'canceled'
      info = string(routineInfo, error, format='(%"hubIDLTaskManager::Session canceled: %s")')
    end
    else : stop
  ENDCASE
  
  self._setTaskInfo, taskInfo
  oBridge = self._getBridge(taskInfo.id, /Remove)
  
  
  if isa(info) then begin
    info = strjoin(info,string(13b))
    self._printInfo, info
    ;add info to the log file, if existant
    ;print on the bridge thread to no mess up access rights between different threads
    oBridge.SetVar, 'info', info
    oBridge.Execute, 'print, info'
  endif
  
  
  if status eq 3 then begin
    oBridge.Execute, 'print, "hubIDLTaskManager::close this IDL_IDLBridge"'
    oBridge.GetProperty, output=logfile 
    wait, 0.5
    obj_destroy, oBridge
    
    dn = FILE_DIRNAME(logfile)
    bn = FILE_BASENAME(logfile)
    newName= FILEPATH('error_'+bn, ROOT_DIR=dn)
    i = 1
    while FILE_TEST(newName) do begin
      newName = FILEPATH('error'+strtrim(i,2)+'_'+bn, ROOT_DIR=dn)
      i++
    endwhile
    
    file_copy, logfile, newName, /ALLOW_SAME, /OVERWRITE, /NOEXPAND_PATH  
    
  endif else begin
    if self.nBridgesMax GT n_elements(self.bridgePool) then begin
      ;put bridge back to bridge pool
      self.bridgePool.add, oBridge
    endif else begin
      ;close bridge, release memory resources
      oBridge.Execute, 'print, "hubIDLTaskManager:: Run garbage collection... " & HEAP_GC'
      oBridge.Execute, 'print, "hubIDLTaskManager:: Close this IDL_IDLBridge"'
      wait, 0.5
      obj_destroy, oBridge
    endelse
  endelse 
 
  self._refreshTaskInfo
  
end

;+
; :Description:
;    Prints some information on the state of the hubIDLtaskManager Object
;
;-
pro hubIDLTaskManager::_printInfo, info, AddToConsole=AddToConsole
  if ~isa(info) then info = ''
  
  if keyword_set(AddToConsole) then print, info 
  
  widConsole = self._getWidgetID('widConsole')
  if isa(widConsole) then begin
      buffer = 1000l
      WIDGET_Control, widConsole, GET_VALUE=oldText
      n = n_elements(oldtext)
      i = (n - buffer) > 0 
      WIDGET_Control,widConsole, SET_VALUE = [oldtext[i:*],info]
      WIDGET_Control,widConsole, SET_TEXT_TOP_LINE=n
  endif
end

function hubIDLTaskManager::_getNumberOfTasks, waiting=waiting, error=error
  n = 0l
  
  if keyword_set(waiting) then begin
    foreach ti, self.taskInfoList do begin
      n += self.isWaitingTask(ti)
    endforeach
    return, n
  endif
  
  if keyword_set(error) then begin
    foreach ti, self.taskInfoList do begin
      n += strcmp(ti.state, 'error', /FOLD_CASE)
    endforeach
    return, n
  endif

  return, n_elements(self.taskInfoList)
end

function hubIDLTaskManager::isWaitingTask, info
  return, total(strcmp(info.state, ['unstarted','await start','await restart'], /FOLD_CASE)) ne 0
end

function hubIDLTaskManager::_getSelectedTaskIDs
  selected = WIDGET_INFO(self._getWidgetID('widInfoTable'), /TABLE_SELECT)
  iColumnZero = where(selected[0,*] eq 0, /NULL)
  taskIDList = list()
  if isa(iColumnZero) then begin
    iRow = selected[1,iColumnZero]
    widget_control, self._getWidgetID('widInfoTable'), GET_VALUE=Values
    foreach i, iRow do begin
      if i lt n_elements(Values) then taskIDList.add, Values[i].id
    endforeach
  endif
  return, taskIDList
end

pro hubIDLTaskManager::_setTaskStateSensitivity, sensitivity
  WIDGET_CONTROL, self._getWidgetID('widTaskStart')  , SENSITIVE=sensitivity
  WIDGET_CONTROL, self._getWidgetID('widTaskRestart'), SENSITIVE=sensitivity
  WIDGET_CONTROL, self._getWidgetID('widTaskCancel') , SENSITIVE=sensitivity
end

pro hubIDLTaskManager::_setWorkStateButtons, workState
  if ~isa(workState) then workState = self.workState
  case workState of
    'initial' : begin
        WIDGET_CONTROL, self._getWidgetID('widDoWork'), SENSITIVE=self.nBridgesMax gt 0
        WIDGET_CONTROL, self._getWidgetID('widCancelWork'), SENSITIVE=0
        WIDGET_CONTROL, self._getWidgetID('widClose'), SENSITIVE=1
        self._setTaskStateSensitivity, 0
    end
    'started' : begin
            WIDGET_CONTROL, self._getWidgetID('widDoWork'), SENSITIVE=0
            WIDGET_CONTROL, self._getWidgetID('widCancelWork'), SENSITIVE=1
            WIDGET_CONTROL, self._getWidgetID('widClose'), SENSITIVE=1
            self._setTaskStateSensitivity, 1
                end
    'stopped' : begin
                  WIDGET_CONTROL, self._getWidgetID('widDoWork'), SENSITIVE=1
                  WIDGET_CONTROL, self._getWidgetID('widCancelWork'), SENSITIVE=0
                  WIDGET_CONTROL, self._getWidgetID('widClose'), SENSITIVE=1
                  self._setTaskStateSensitivity, 0
                end
    'canceled': begin
        WIDGET_CONTROL, self._getWidgetID('widDoWork'), SENSITIVE=0
        WIDGET_CONTROL, self._getWidgetID('widCancelWork'), SENSITIVE=0
        WIDGET_CONTROL, self._getWidgetID('widClose'), SENSITIVE=0
        self._setTaskStateSensitivity, 0
      end
    else : stop
  endcase
  self.workState = workState
end


function hubIDLTaskManager::_getNumberOfActiveBridges
  return, n_elements(self.bridgeSet)
end


function hubIDLTaskManager::_getBridge, sessionID, remove=remove
  oBridge = !NULL
  if (self.bridgeSet).hasKey(sessionID) then begin
    oBridge = keyword_set(remove)? (self.bridgeSet).remove(sessionID) : (self.bridgeSet)[sessionID] 
  endif
  return, oBridge
end



function hubIDLTaskManager::_getTaskInfo, taskID
  foreach taskInfo, self.taskInfoList do begin
    if taskInfo.id eq taskID then begin
      return, taskInfo
    endif
  endforeach
  return, !NULL
end


pro hubIDLTaskManager::_setTaskInfo, taskInfo
  self.flagRefreshTaskInfo = 1b
  for i = 0, N_ELEMENTS(self.taskInfoList)-1 do begin
    if ((self.taskInfoList)[i]).id eq taskInfo.id then begin
      (self.taskInfoList)[i] = taskInfo
      return
    endif
  endfor
  (self.taskInfoList).add, taskInfo
end


pro hubIDLTaskManager::_removeTaskInfo, taskInfo
 !NULL = self._removeTaskInfo(taskInfo)
end


function hubIDLTaskManager::_removeTaskInfo, taskInfo
  i = (self.taskInfoList).where(taskInfo)
  return, (self.taskInfoList).remove(i)
end


function hubIDLTaskManager::_checkTaskInfo, taskID
  if typename(taskID) eq 'TASKINFO' then return, taskID
  taskID = fix(taskID)
  foreach info, self.taskInfoList do begin
    if info.id eq taskID then return, info
  endforeach
  self._printInfo, 'Task ID does not exist: '+string(taskID)
end


pro hubIDLTaskManager::_startBridge, taskInfo
  taskInfo = self._checkTaskInfo(taskInfo)
  if (self.bridgeSet).hasKey(taskInfo.id) then begin
    tmp = taskInfo.userid ne '' ? '"'+taskInfo.userid+'" ' : ''
    self._printInfo, string(format='(%"Task with ID %i %s is already running.")', taskInfo.id, tmp)
    return 
  endif
  
  if self.debug then begin
    keywordstring = ''
    foreach key, (taskInfo.keywords).keys() do begin
      keywordstring += ', '+key+"='"+strtrim((taskInfo.keywords)[key],2)+"'"
    endforeach
    
    ;call_procedure, taskInfo.routine, keywordstring
    command = string(taskInfo.routine, keywordstring, format='(%"%s%s")')
    self._printInfo, "Debug: execute("+command+")"
    void = execute(command)
    taskInfo.state = 'completed'
  endif else begin
    
    
    if self.BridgePool.LENGTH gt 0 then begin
      oBridge = self.BridgePool.remove(0)
      if keyword_set(self.createLogFiles) then begin
        oBridge.getProperty, output=logfile
        taskInfo.logfile = logfile
      endif
    endif else begin
      logfile = !NULL
      if keyword_set(self.createLogFiles) then begin
        FILE_MKDIR, self.dirLogFiles
        i = self.BridgePool.LENGTH-1
        repeat begin
          i++
          logfile = filepath(string(format='(%"IDLBridge%i.log")', i),ROOT_DIR=self.dirLogFiles)
        endrep until (~file_test(logfile))
        taskInfo.logfile = logfile
      endif
      
      oBridge = IDL_IDLBridge(CALLBACK='hubIDLTaskManager_BridgeCallback', OUTPUT=logfile)
      ;copy recent IDL path
      oBridge.Execute, 'print, "hubIDLTaskManager: restore IDL path..."', NOWAIT=0
      oBridge.setVar, 'path', [!PATH]
      oBridge.Execute, '!PATH += PATH_SEP(/SEARCH_PATH) + path[0]'
    endelse
    ;tmp = oBridge.getVar('!PATH')
  
    ;copy variables
    keywordstring = ''
    objectdata = Hash()
    
    foreach key, (taskInfo.keywords).keys() do begin
      value = (taskInfo.keywords)[key]
      keywordstring += ', '+key+'='+key  
      case size(value,/TYPE) of
         8 : objectdata[key] = value
        10 : objectdata[key] = value
        11 : objectdata[key] = value
        else : begin
          oBridge.SetVar, key, value          
        end
      endcase
    endforeach
    
    if n_elements(objectdata) gt 0 then begin
      tmpSAV = FILEPATH(string(format='(%"objectdata_task%i")', taskInfo.id), ROOT_DIR=self.dirTMPFiles)
      save, objectdata, FILENAME=tmpSAV
      ;oBridge.setVar, 'path', tmpSAV
      oBridge.Execute, 'print, "hubIDLTaskManager: restore object data..."', NOWAIT=0
      oBridge.execute, 'restore, "'+ tmpSAV+'"', nowait=0
      
      ;oBridge.execute, 'help, objectdata'
      oBridge.execute, 'foreach key, objectdata.keys() do (scope_varfetch(key, /ENTER)) = objectdata[key]', nowait=0
      ;oBridge.execute, 'print, scope_varname(level=0)', nowait=0
      FILE_DELETE, tmpSAV
    endif
    !NULL = temporary(objectdata)
    
    oBridge.SetProperty, USERDATA=taskInfo
    (self.bridgeSet)[taskInfo.id] = oBridge
    command = string(taskInfo.routine, keywordstring, format='(%"%s%s")')
    if self.callGC then command += ' & print, "hubIDLTaskManager: Run garbage collection... " & HEAP_GC '
    command += ' & print, "hubIDLTaskManager: this task is done. "'
    oBridge.Execute, 'print, "hubIDLTaskManager: routine '+taskInfo.routine+' starts now"', NOWAIT=0
    oBridge.Execute, command, /NOWAIT
    taskInfo.state = 'running'
    self._setTaskInfo, taskInfo
    
    self._printInfo, string(format='(%"Task %i started.")', taskInfo.id)
  endelse
  
end


pro hubIDLTaskManager::addTask, routine_name, userid, _EXTRA = KeyValuePairs
  !NULL = self.addTask(routine_name, userid, _EXTRA = KeyValuePairs)

end

function hubIDLTaskManager::addTask, routine_name, userid, _EXTRA = KeyValuePairs

  id = self.taskCounter
  
  if isa(userid) then begin
    _userid = strtrim(userid,2)
    foreach ti, self.taskInfoList do begin
      if ti.userid eq _userid then message, "User id '"+userid+"' already exists'
    endforeach
  endif else begin
    _userid = strtrim(id,2)
  endelse
  
  ;logfile = ''
  ;if self.createLogFiles then begin
  ;  logfile = FILEPATH('taskmgr'+_userid+'.log', ROOT_DIR=self.dirLogFiles)
  ;endif
  
  taskInfo = {taskInfo $
    , id:id $
    , userid:_userid $
    , routine:routine_name $
    , state:'unstarted' $
    , logfile:'' $
    , taskManager:self $
    , keywords:Hash() $
  }
  
  
  self.taskCounter++

  if n_params() gt 1 && isa(logfile) then taskInfo.logfile = logfile

  keynames = tag_names(KeyValuePairs)
  ;keywordstring = ''
  for iKey = 0, N_TAGS(KeyValuePairs)-1 do begin
    key = keynames[iKey]
    ;keywordstring += ', '+key+'='+key
    (taskInfo.keywords)[key] = KeyValuePairs.(iKey)
    ;oBridge.SetVar, key, KeyValuePairs.(iKey)
  endfor

  self._setTaskInfo, taskInfo
  return, taskInfo.id
end


pro hubIDLTaskManager::_startTask, taskInfo
  taskInfo = self._checkTaskInfo(taskInfo)
  taskInfo.state = 'await start'
  self._setTaskInfo, taskInfo
end

pro hubIDLTaskManager::_restartTask, taskInfo
  taskInfo = self._checkTaskInfo(taskInfo)
  oBridge = self._getBridge(taskInfo.id)
  if isa(oBridge) then begin
    oBridge.abort
    self.bridgePool.add, oBridge
  endif
  taskInfo.state = 'await restart'
  self._setTaskInfo, taskInfo
end


pro hubIDLTaskManager::_cancelTask, taskInfo
  taskInfo = self._checkTaskInfo(taskInfo)
  oBridge = self._getBridge(taskInfo.id)
  if OBJ_VALID(oBridge) then begin
    ;print, oBridge.status()
    oBridge.abort
    self.bridgePool.add, oBridge
  endif
  taskInfo.state = 'canceled'
  self._setTaskInfo, taskInfo
end

pro hubIDLTaskManager::_removeTask, taskInfo
  taskInfo = self._checkTaskInfo(taskInfo)
  self._cancelTask, taskInfo
  self._removeTaskInfo, taskInfo
  self._printInfo, string(format='(%"Task %i removed")', taskInfo.id)
end


;+
; :Description:
;    Starts the task manager and opens it's GUI to control all processes
;
; :Keywords:
;    start: in, boolean, optional
;     Set this to start processing added tasks immediately.
;    
;    closeAfterWork: in, boolean
;     Set this for closing the task manager in case all tasks are done.
;     
;     Set closeAfterwork=2 to close the manager also after tasks finished with errors. 
;    
;    skipErrors: in, boolean
;-
pro hubIDLTaskManager::runTaskManager, start=start, closeafterwork=closeafterwork, debug=debug, skipErrors=skipErrors
  WIDGET_CONTROL, self.topLevelBase, /REALIZE
  WIDGET_CONTROL, self.topLevelBase, SENSITIVE=1
  
  self.debug = keyword_set(debug)
  self.skipErrors = KEYWORD_SET(skipErrors)
  
  if keyword_set(start) then self.workState = 'started'
  
  
  
  while self.workState ne 'finished' do begin  
    case self.workState of
      'initial' : ;nothing 
      'started' : begin        
          if self._getNumberOfActiveBridges() lt self.nBridgesMax then begin
            taskInfo = !NULL
            foreach info, self.taskInfoList do begin
              if info.state eq 'unstarted' or $
                info.state  eq 'await start' or $
                info.state  eq 'await restart' then begin
                self._startBridge, info
                break
              endif
            endforeach
          endif

          if keyword_set(closeAfterWork) $
            && self._getNumberOfActiveBridges() eq 0 $
            && self._getNumberOfTasks(/waiting) eq 0 $
            && (closeAfterWork gt 1 or self._getNumberOfTasks(/error) eq 0) then begin
            self.workState = 'finished'
          endif else begin
            ;do nothing in case all bridges are bussy
          endelse
        end
      'stopped' : begin
          foreach oBridge, self.bridgeSet do begin
            oBridge.abort
            self.bridgePool.add, oBridge
          endforeach
          
          
      end
      else : stop
    endcase
    
    ;refresh task info if necessary 
    if self.flagRefreshTaskInfo then begin
      self._refreshTaskInfo
      self.flagRefreshTaskInfo = 0
    endif
    
    ;wait for next user/callback interaction
    
    self._checkForCallback 
  endwhile
  
  WIDGET_CONTROL, self.topLevelBase, /DESTROY
  foreach oBridge, self.bridgeSet do obj_destroy, oBridge
  foreach oBridge, self.bridgePool do obj_destroy, oBridge
end

  

pro hubIDLTaskManager::cleanup
  if WIDGET_INFO(self.topLevelBase, /VALID_ID) then begin
    WIDGET_CONTROL, self.topLevelBase, /DESTROY
  endif
end

pro hubIDLTaskManager::_checkForCallback
  wait, self.callbackWaitTime
  !null = widget_event(/NOWAIT)
end


pro hubIDLTaskManager__define
  struct = {hubIDLTaskManager $
    , inherits IDL_Object $
    , workState:''        $
    , nCPUs:0l           $
    , nBridgesMax:0l     $
    , taskCounter:0l      $
    , callbackWaitTime:0d $
    , bridgeSet:Hash()    $
    , bridgePool:list() $
    , taskInfoList:List() $
    , flagRefreshTaskInfo:0b   $
    , topLevelBase:0l     $
    , debug:0b $
    , dirLogFiles:'' $
    , dirTMPFiles:'' $
    , createLogFiles: 0b $ 
    , skipErrors: 0b $
    , callGC:0b $
  }
end



pro hubIDLTaskManager_example
  
  ;create a new Task Manager
  ;dirlogfiles = 'T:\TMP'
  dirlogfiles = 'D:\temp\Rplot.png'
  dirlogfiles = 'D:\temp\Logfiledir'
  taskMGR = hubIDLTaskManager(dirlogfiles=dirLogfiles,CREATELOGFILES=1,MAXNUMBEROFACTIVEBRIDGES=3 )
  h = Hash('aa',9,'bb',10)
  sh = JSON_SERIALIZE(h)
  h2 = JSON_PARSE(sh)
  
  
  nTASKs = 10
  ;add all tasks to the task manager
  for i=1, nTasks do begin
    infostruct = {msg:'My Message'+strtrim(i,2), num:i+2}
    uid = string(i, format='(%"myid=%i")')
    taskMGR.addTask, 'hubIDLTaskManager_testRoutine' $
    ;hubIDLTaskManager_testRoutine $
      , runFFT = 0b $
      , raiseError = randomu(seed,1) gt 0.5 $
      , time2wait = 5 > randomu(seed,1)*5, infostruct=JSON_SERIALIZE(infostruct)
  endfor
  
  ;run the task manager
  taskMGR.runTaskManager,START=1, CLOSEAFTERWORK=1 
  
  print, 'done'
end