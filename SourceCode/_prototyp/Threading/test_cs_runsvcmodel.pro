pro test_CS_runsvcmodel $
    , modelFilename=modelFilename $
    , featureFilename=featureFilename $
    , labelFilename=labelFilename $
    , outputLabelFilename=outputLabelFilename 
  
  parameters=Hash()
  parameters['modelFilename']   = modelFilename
  parameters['featureFilename'] = featureFilename
  parameters['labelFilename']   = labelFilename  
  parameters['svmType']         = 'svc'
  imageSVM_parameterize_processing, parameters
  parameters['outputLabelFilename'] = outputLabelFilename


  imageSVM_apply_processing, parameters
  
end