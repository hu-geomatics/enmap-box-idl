pro test_comboboxDelete
  tlb = widget_base()
  combobox = widget_combobox(tlb, Value=['dummy','a', 'b', 'c'], /EDITABLE, XSIZE=200)
  widget_control, tlb, /REALIZE
  xmanager, 'test_comboboxDelete', tlb, /NO_BLOCK
  
  widget_control, combobox, GET_VALUE=list
  list[0] = 'hello world'
  widget_control, combobox, SET_VALUE=list
  
end