function stringToCharacters, str
  
  characters = list(byte(str),/EXTRACT)
  foreach character, characters, i do begin
    characters[i] = string(character)
  endforeach
  return, characters.toArray()

end

; operators
; precedence   operators       associativity
; 4            ^               right to left
; 3            * / %           left to right
; 2            + -             left to right

function op_preced, c
  case c of
    '~': return, 4
    '*': return, 3
    '/': return, 3
    '+': return, 2
    '-': return, 2
    else: return,0
  endcase
end

function op_left_assoc, c
  case c of
    '~': return, 0b
    '*': return, 1b
    '/': return, 1b
    '+': return, 1b
    '-': return, 1b
  endcase
end

function op_arg_count, c
  case c of
    '~': return, 1
    '*': return, 2
    '/': return, 2
    '+': return, 2
    '-': return, 2
  endcase
end

function is_operator, c
  case c of
    '~': return, 1b
    '*': return, 1b
    '/': return, 1b
    '+': return, 1b
    '-': return, 1b
    else : return, 0b
  endcase
end

function is_function, c
  return, c ge 'A' and c le 'Z'
end

function is_ident, c
  return, ((c ge '0' and c le '9') or (c ge 'a' and c le 'z'))
end

function shunting_yard, input, output

  stack =  list()
  output = list()
 
  foreach c, input,ci do begin
    if c eq ' ' then begin
      continue
    endif
    ; If the token is a number (identifier), then add it to the output queue.
    if is_ident(c) then begin
      
      output.add, c
    endif
    ; If the token is a function token, then push it onto the stack.
    if is_function(c) then begin
      stack.add, c
    endif
    ; If the token is a function argument separator (e.g., a comma):
    if c eq ',' then begin
      correct = 0b
      while(~stack.hubIsEmpty()) do begin 
        sc = stack[-1]
        if sc eq '(' then begin
          correct = 1b
          break
        endif else begin
          ; Until the token at the top of the stack is a left parenthesis,
          ; pop operators off the stack onto the output queue.
          output.add, stack.remove()
        endelse
      endwhile
      ; If no left parentheses are encountered, either the separator was misplaced
      ; or parentheses were mismatched.
      if ~correct then begin
        print, 'Error: separator or parentheses mismatched'
        return, 0b
      endif
    endif
    ; If the token is an operator, op1, then:
    if is_operator(c) then begin
      while ~stack.hubIsEmpty() do begin
        sc = stack[-1]
        ; While there is an operator token, o2, at the top of the stack
        ; op1 is left-associative and its precedence is less than or equal to that of op2,
        ; or op1 has precedence less than that of op2,
        ; Let + and ^ be right associative.
        ; Correct transformation from 1^2+3 is 12^3+
        ; The differing operator priority decides pop / push
        ; If 2 operators have equal priority then associativity decides.
        if ( is_operator(sc) and $
           ( (op_left_assoc(c) and (op_preced(c) le op_preced(sc))) or $
             (op_preced(c) lt op_preced(sc)))) then begin
          ; Pop o2 off the stack, onto the output queue;
          output.add, stack.remove()
        endif else begin
          break
        endelse
      endwhile
      ; push op1 onto the stack.
      stack.add, c
    endif
    ; If the token is a left parenthesis, then push it onto the stack.
    if c eq '(' then begin
      stack.add, c
    endif
    ; If the token is a right parenthesis:
    if c eq ')' then begin
      correct = 0b
      ; Until the token at the top of the stack is a left parenthesis,
      ; pop operators off the stack onto the output queue
      while ~stack.hubIsEmpty() do begin
        sc = stack[-1]
        if sc eq '(' then begin
          correct = 1b
          break
        endif else begin
          output.add, sc
          stack.remove, -1
        endelse
      endwhile
      ; If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
      if ~correct then begin
        print, 'Error: parentheses mismatched'
        return, 0b
      endif
      ; Pop the left parenthesis from the stack, but not onto the output queue.
      stack.remove, -1
      ; If the token at the top of the stack is a function token, pop it onto the output queue.
      if ~stack.hubIsEmpty() then begin
        sc = stack[-1]
        if is_function(sc) then begin
          output.add, sc
          stack.remove, -1
        endif
      endif
    endif
  endforeach
        
  ; When there are no more tokens to read:
  ; While there are still operator tokens in the stack:
  while ~stack.hubIsEmpty() do begin
    sc = stack[-1]
    if sc eq '(' or sc eq ')' then begin
      print, 'Error: parentheses mismatched'
      return, 0b
    endif
    output.add, stack.remove()
  endwhile
  return, 1b
end

;function execution_order(input) {
;    printf("order:\n");
;    const char *strpos = input, *strend = input + strlen(input);
;    char c, res[4];
;    unsigned int sl = 0, sc, stack[32], rn = 0;
;        // While there are input tokens left
;    while(strpos < strend)  {
;                // Read the next token from input.
;        c = *strpos;
;                // If the token is a value or identifier
;        if(is_ident(c))    {
;                        // Push it onto the stack.
;            stack[sl] = c;
;            ++sl;
;        }
;                // Otherwise, the token is an operator  (operator here includes both operators, and functions).
;        else if(is_operator(c) || is_function(c))    {
;                        sprintf(res, "_%02d", rn);
;                        printf("%s = ", res);
;                        ++rn;
;                        // It is known a priori that the operator takes n arguments.
;                        unsigned int nargs = op_arg_count(c);
;                        // If there are fewer than n values on the stack
;                        if(sl < nargs) {
;                                // (Error) The user has not input sufficient values in the expression.
;                                return false;
;                        }
;                        // Else, Pop the top n values from the stack.
;                        // Evaluate the operator, with the values as arguments.
;                        if(is_function(c)) {
;                                printf("%c(", c);
;                                while(nargs > 0){
;                                        sc = stack[sl - nargs]; // to remove reverse order of arguments
;                                        if(nargs > 1)   {
;                                                printf("%s, ", &sc);
;                                        }
;                                        else {
;                                                printf("%s)\n", &sc);
;                                        }
;                                        --nargs;
;                                }
;                                sl-=op_arg_count(c);
;                        }
;                        else    {
;                                if(nargs == 1) {
;                                        sc = stack[sl - 1];
;                                        sl--;
;                                        printf("%c %s;\n", c, &sc);
;                                }
;                                else    {
;                                        sc = stack[sl - 2];
;                                        printf("%s %c ", &sc, c);
;                                        sc = stack[sl - 1];
;                                        sl--;sl--;
;                                        printf("%s;\n",&sc);
;                                }
;                        }
;                        // Push the returned results, if any, back onto the stack.
;            stack[sl] = *(unsigned int*)res;
;            ++sl;
;        }
;        ++strpos;
;    }
;        // If there is only one value in the stack
;        // That value is the result of the calculation.
;        if(sl == 1) {
;                sc = stack[sl - 1];
;                sl--;
;                printf("%s is a result\n", &sc);
;                return true;
;        }
;        // If there are more values in the stack
;        // (Error) The user input has too many values.
;        return false;
;}
; 
pro main, input
  ; functions: A() B(a) C(a, b), D(a, b, c) ...
  ; identifiers: 0 1 2 3 ... and a b c d e ...
  ; operators: = - + / * ^
  input = charArray(input)
    
  print, 'input: ', input
  if(shunting_yard(input, output)) then begin
    print, 'output: ', output.toArray()
;    if(!execution_order(output))
;            printf("\nInvalid input\n");
  endif
end

pro test_associativity

  !null = a() + b() + c()

end