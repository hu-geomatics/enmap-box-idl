function nextToken, input

  ; try 
  , regularExpression
  start = stregex(input, '^'+regularExpression, LENGTH=length)

  return, token
end

pro tokenizer

  input = 'a + 0 + 123.456 - doit(a,b+c,9)
  input = strcompress(/REMOVE_ALL, input)
  zero = '0'
  ziffer  = '(0|1|2|3|4|5|6|7|8|9)'
  zifferNoZero = '(1|2|3|4|5|6|7|8|9)'
  integer = zero+'|('+zifferNoZero+ziffer+'*)'
  floating = integer+'.'+ziffer+'*'
  operator = '(\+|-|\*|/|\^)'
  identifier = '(_|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z)'
  
  while 1 do begin
    
    start = stregex(input, operator, LENGTH=length)
    found = start[0] ne -1
    
      regex = strmid(input, start, length)
      print,regex 
    endif else begin
      print, 'No match.'
    endelse
    
    help, token
    print,self.input
    stop
  endwhile
end