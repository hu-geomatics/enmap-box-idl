function hubIOImgVirtualInputImage::init, filename, grid 
  self.image = hubIOImgInputImage(filename)
  self.samples = grid.samples
  self.lines = grid.lines

  ; setup map info for virtual grid
  imageHeader = self.image.getHeader()
  sourceMapInfo = self.image.getMeta('map info')
  targetMapInfo = sourceMapInfo + dictionary()
  targetMapInfo.northing = double(grid.northing)
  targetMapInfo.easting = double(grid.easting)
  targetMapInfo.sizex = double(grid.sizex)
  targetMapInfo.sizey = double(grid.sizey)
  targetMapInfo.pixelx = 0d
  targetMapInfo.pixely = 0d
  self.mapInfoObj = hubIOImgMeta()
  self.mapInfoObj.setValue, targetMapInfo

  ; init resampling parameters
  imageBoundingBox = self.image.getMapCoordinates(/BoundingBox)
  source = dictionary()
  target = dictionary()
  
  sourceUpperLeft.map = imageBoundingBox[0:1]
  sourceUpperLeft.pixel.source = [0d,0d]
  sourceUpperLeft.pixel.target = self.getPixelCoordinates(sourceUpperLeft.map)
  
  targetUpperLeft.map = [targetMapInfo.northing, targetMapInfo.easting]
  targetUpperLeft.pixel.source = self.image.getPixelCoordinates(targetUpperLeft.map)
  targetUpperLeft.pixel.target = [0d,0d]

  sourcePixelSize.map = [sourceMapInfo.sizex,sourceMapInfo.sizey]
  sourcePixelSize.pixel.source = [1d,1d]


  sourcePixelSize.pixel.target = ???

  
  targetPixelSize.map = [targetMapInfo.sizex,targetMapInfo.sizey]
  
  sourcePixelSize.inSourcePixelUnit = [1d,1d]
  sourcePixelSize.inTargetPixelUnit = sourcePixelSize.inMapUnit / [targetMapInfo.sizex,targetMapInfo.sizey]



  return,1b
end


pro hubIOImgVirtualInputImage::initReader $
;  , numberOfTileLines $
;  , neighborhoodHeight $
  , Band=band $
  , Slice=slice $
  , Cube=cube $
;  , Profiles=profiles $
  , DataType=dataType $
;  , TileProcessing=tileProcessing $
  , SubsetBandPositions=bandPositions
;  , SubsetSampleRange=sampleRange $
;  , SubsetLineRange=lineRange $
;  , SubsetRectangle=rectangle $
;  , Mask=mask $
;  , ForegroundMaskValue=foregroundMaskValue $
;  , BackgroundMaskValue=backgroundMaskValue
    
  ; define image subset


end

function hubIOImgVirtualInputImage::getMeta, metaName, Default=default
  case strlowcase(metaName) of
    'samples'  : metaValue = self.samples
    'lines'    : metaValue = self.lines
    'map info' : metaValue = self.mapInfoObj.getValue()
    else : metaValue = self.image.getMeta(metaName, Default=default)
  endcase
  return, metaValue
end

function hubIOImgVirtualInputImage::getMapCoordinates, pixelX, pixelY, BoundingBox=boundingBox
  if keyword_set(boundingBox) then begin
    return, self.getMapCoordinates([0,self.samples], [0,self.lines])
  endif
  return, self.mapInfoObj.getMapCoordinates(pixelX, pixelY)
end

function hubIOImgVirtualInputImage::getPixelCoordinates, mapX, mapY
  return, self.mapInfoObj.getPixelCoordinates(mapX, mapY)
end

pro hubIOImgVirtualInputImage::cleanup
  self.image.finishReader
end

;+
; :Hidden:
;-
pro hubIOImgVirtualInputImage__define
  struct = {hubIOImgVirtualInputImage, inherits IDL_Object,$
    image : obj_new(),$
    mapInfoObj : obj_new(),$
    samples : 0ll, lines : 0ll}
end

pro test_hubIOImgVirtualInputImage

  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  sourceBoundingBox = (hubIOImgInputImage(inputFilename)).getMapCoordinates(/BoundingBox)
  sourceBoundingBoxSize = [sourceBoundingBox[2]-sourceBoundingBox[0], sourceBoundingBox[1]-sourceBoundingBox[3]]
  print, sourceBoundingBoxSize
  ;virtualBoundingBoxApprox =  

  targetGrid = dictionary()
  targetGrid.northing = 390749.250
  targetGrid.easting  = 5820819.800
  targetGrid.sizex = 10
  targetGrid.sizey = 10
  targetGrid.samples = 300
  targetGrid.lines = 300
  

  
  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  virtualInputImage = hubIOImgVirtualInputImage(inputFilename, targetGrid)
  boundingBox = virtualInputImage.getMapCoordinates(/BoundingBox)
  print, boundingBox
  print, virtualInputImage.getPixelCoordinates(boundingBox)



end
