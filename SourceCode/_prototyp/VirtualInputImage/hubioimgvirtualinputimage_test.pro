pro hubIOImgVirtualInputImage_test

  targetGrid = dictionary()
  targetGrid.ul = [390749.250, 5820819.800] ; [northing,easting] in map units
  targetGrid.pixelSize = [10,10]            ; [xsize,ysize] in map units 
  targetGrid.samples = 300                  ; nunber of samples
  targetGrid.lines = 200                    ; nunber of lines
  
  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  virtualInputImage = hubIOImgVirtualInputImage(inputFilename, targetGrid)

  spectralSubset = virtualInputImage.image.getMeta('default bands')
  virtualInputImage.initReader, /Cube, SubsetRectangle=spatialSubset, SubsetBandPositions=spectralSubset

  cubeData = virtualInputImage.getData()

  virtualInputImage.cleanup

  tvscl, cubeData, /Order, True=3
end