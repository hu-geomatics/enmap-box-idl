pro puzzle_createGrid, grid, arrangement, invalid
  seed = systime(1)
  grid = bytarr(4,4)
  arrangement = list()
  
  invalid = 0b
  catch, errorStatus
  if (errorStatus ne 0) then begin
    Catch, /CANCEL
    invalid = 1b
  endif
  
  for i=1,8 do begin
    if invalid then break
    index = (where(/NULL, grid eq 0))[0]
    xy = array_indices(grid, index)
    case 1b of
      xy[0] eq 3         : orientation = 'v'
      grid[xy[0]+1,xy[1]] ne 0 : orientation = 'v'
      xy[1] eq 3         : orientation = 'h'
      else               : orientation = (['h','v'])[randomu(seed, 1, /LONG) mod 2]    
    endcase
    case orientation of
      'h' : grid[xy[0]:xy[0]+1, xy[1]] = i
      'v' : grid[xy[0],         xy[1]:xy[1]+1]         = i
    endcase
    arrangement.add, orientation
  endfor
  catch, /CANCEL
      
  print, grid
  if invalid then print, 'invalid'
end

pro puzzle
  puzzle_createGrid, grid, arrangement, invalid
  
end