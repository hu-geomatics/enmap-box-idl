pro test_twoImages_event, event
  help, event
  widget_control, event.top, GET_UVALUE=state
  print,state
  selectedView = (state['window']).select(state['scene'], [event.x, event.y])
  if ~isa(selectedView, 'IDLgrView') then return
  selectedView.getProperty, Location=location
  print, [event.x, event.y]-location
    
 ; window.select, 
end

pro test_twoImages
  data1 = bytscl(randomu(seed, 100, 100, 3))
  view1 = IDLgrView(viewplane_rect=[0, 0, 100, 100], location=[50,50], DIMENSIONS=[100,100])
  data2 = bytscl(randomu(seed, 100, 100, 3))
  view2 = IDLgrView(viewplane_rect=[0, 0, 100, 100], location=[350,350], DIMENSIONS=[100,100])
  
  model1 = IDLgrModel()
  view1->add, model1
  image1 = IDLgrImage(data1, INTERLEAVE=2)
  model2 = IDLgrModel()
  view2->add, model2
  image2 = IDLgrImage(data2, INTERLEAVE=2)
  
  model1->add, image1
  model2->add, image2
  
  scene = IDLgrScene()
  scene.add, view1
  scene.add, view2
  state = hash()
  state['view1'] = view1
  state['view2'] = view2
  state['scene'] = scene
  
  tlb = widget_base(UVALUE=state)
  draw = widget_draw(tlb, SCR_XSIZE=500, SCR_YSIZE=500, BUTTON_EVENTS=1,GRAPHICS_LEVEL=2 )
  widget_control, tlb, /REALIZE
  xmanager, 'test', tlb, /NO_BLOCK, EVENT_HANDLER='test_twoImages_event'
  
  widget_control, draw, GET_VALUE=window
 ; window = IDLgrWindow(dimensions=[500, 500])
  window->draw, scene
  state['window'] = window
  
end