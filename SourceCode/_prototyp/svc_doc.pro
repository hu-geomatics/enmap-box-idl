pro svc_doc

enmapBox
; training data
  trainingFeaturesRaw = hub_getTestImage('Hymap_Berlin-A_Image')
  traingLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')

; scale features
  imageScalingParameters=hash()
  imageScalingParameters['inputImage'] = trainingFeaturesRaw
  imageScalingParameters['outputImage'] = filepath('scaledFeatures', /TMP)
  imageScalingParameters['mode'] = 'zTransformation'
  imageScalingParameters['bandwise'] = 1b
  hubApp_imageScaling_processing, imageScalingParameters

  ; init idl-java bridge

  enmapJavaBridge_init
  imageSVM = obj_new('imagesvm')
  svm_type = 'svm_type'
  memory = 100
  fn_model = filepath('my_model.svc', /TMP)
  
  imageSVM->set_imagesvm_parameter,/SET_DEFAULT
  imageSVM->set_imagesvm_parameter,/GET_SETTING, IMAGESVM_PARAMETER=imagesvm_parameter
  enum = imageSVM->get_enum()
  imageSVM_parameter.svm_type = enum.svm_type.svc
  imageSVM_parameter.param_type = enum.param_type.auto
  imageSVM_parameter.gs_e = 0.10000000
  imageSVM_parameter.gs_cv = 3
  imageSVM_parameter.gs_ncv = 10
  imageSVM_parameter.gs_c1 = 0.10000000
  imageSVM_parameter.gs_c2 = 1000.0000
  imageSVM_parameter.gs_c3 = 10.000000
  imageSVM_parameter.gs_g1 = 0.10000000
  imageSVM_parameter.gs_g2 = 1000.0000
  imageSVM_parameter.gs_g3 = 10.000000
  imageSVM_parameter.gs_pm_type = enum.pm_type.svc.kappa
  imageSVM_parameter.p = 0.010000000
  imageSVM_parameter.final_e = 0.00001
  imageSVM_parameter.final_c = 0.00000000
  imageSVM_parameter.final_g = 0.00000000
  imageSVM_parameter.final_e = 0.00000001
  imageSVM->set_imagesvm_parameter, IMAGESVM_PARAMETER=imagesvm_parameter

;--------------------------
;calculation
  imagesvm_parameterization $
    ,FN_MODEL=fn_model $
    ,FN_TRAIN_X=fn_train_x $
    ,FN_TRAIN_Y=fn_train_y $
    ,FN_TEST_X=fn_test_x $
    ,FN_TEST_Y=fn_test_y $
    ,GROUP_LEADER=group_leader $
    ,MASK_VALUE_TRAIN=mask_value_train $
    ,MASK_VALUE_TEST=mask_value_test $
    ,MEMORY=memory $
    ,OIMAGESVM=oimagesvm $
    ,POS=pos



end