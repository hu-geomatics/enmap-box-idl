pro mh_histoMatch
  nruns = float(120)
  nfeatures = float(6)
  hEqual = replicate(nruns/nfeatures, nfeatures)
  hTest = replicate(0, nfeatures)
  
;  hTest[0] = nruns ; most stable = peak histo
;  hTest = hEqual ; leased stable = equal histo
  hTest[0:nfeatures/2-1] = nruns/nfeatures*2 ; middle stable = half 0, half 2 
  
  ; check vectors
  if total(hTest) ne nruns then message, 'check histo vectors'

  maxValue = nruns*(2-2/nfeatures) ;   maxValue = (nfeatures-1)*nruns/nfeatures + nruns-nruns/nfeatures
  stability = total(abs(hEqual-hTest))/maxValue 
  print, hTest
  print, stability

end

pro mh_histoMatch
  hTest = [40,40,40,0,0,0]

  ; calculate stability  
  nruns = total(nruns)
  nfeatures = n_elements(hTest)
  hEqual = replicate(nruns/nfeatures, nfeatures)
  maxValue = nruns*(2-2/nfeatures) ;   maxValue = (nfeatures-1)*nruns/nfeatures + nruns-nruns/nfeatures
  stability = total(abs(hEqual-hTest))/maxValue

  print, hTest
  print, stability

end