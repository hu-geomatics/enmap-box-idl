pro test_pixelAggregate
  ; function should do the same as ENVIs RESIZE_DOIT,
  ; but is more flexible in setting up the target pixel grid
  ; http://www.exelisvis.com/Support/HelpArticlesDetail/TabId/219/ArtMID/900/ArticleID/13827/How-does-the-Pixel-Aggregate-method-work-when-resizing-data-with-ENVI-.aspx

  ;inputs
  sourceArray = fltarr(6,6)
  sourceSamples = n_elements(sourceArray[*,0])
  sourceLines = n_elements(sourceArray[0,*])

  ul = [0d,0d]
  samples = 4
  lines = 4
  pixelSizeX = 1.5d
  pixelSizeY = 1.5d

  ulXSubpixelVector = ul[0]+dindgen(samples)*pixelSizeX
  ulYSubpixelVector = ul[0]+dindgen(lines)*pixelSizeY
  lrXSubpixelVector = ulXSubpixelVector+pixelSizeX
  lrYSubpixelVector = ulYSubpixelVector+pixelSizeY

  ulXPixelVector = floor(ulXSubpixelVector)
  ulYPixelVector = floor(ulYSubpixelVector)
  lrXPixelVector = floor(lrXSubpixelVector)
  lrYPixelVector = floor(lrYSubpixelVector)
  
  dX1Vector = ceil(ulXSubpixelVector)-ulXSubpixelVector
  dY1Vector = ceil(ulYSubpixelVector)-ulYSubpixelVector
  dX2Vector = lrXSubpixelVector-floor(lrXSubpixelVector)
  dY2Vector = lrYSubpixelVector-floor(lrYSubpixelVector)
  
  ; fix special cases where grids touch each other
  dX1Vector[where(/NULL, dX1Vector eq 0)] = 1d
  dY1Vector[where(/NULL, dY1Vector eq 0)] = 1d
  touched = where(/NULL, dX2Vector eq 0)
  dX2Vector[touched] = 1d
  lrXPixelVector[touched] -= 1
  touched = where(/NULL, dY2Vector eq 0)
  dY2Vector[touched] = 1d
  lrYPixelVector[touched] -= 1

  ; create grids
  dX1Grid = rebin(dX1Vector, samples, lines, /SAMPLE)
  dY1Grid = rebin(transpose(dY1Vector), samples, lines, /SAMPLE)
  dX2Grid = rebin(dX2Vector, samples, lines, /SAMPLE)
  dY2Grid = rebin(transpose(dY2Vector), samples, lines, /SAMPLE)

  ; calculate fractions
  ; - corners
  ulFractionGrid = dX1Grid*dY1Grid
  urFractionGrid = dX2Grid*dY1Grid
  llFractionGrid = dX1Grid*dY2Grid
  lrFractionGrid = dX2Grid*dY2Grid

  ; - edges (all pixel on a edge have the same fraction)
  upperEdgeFraction = temporary(dY1Grid)
  lowerEdgeFraction = temporary(dY2Grid)
  leftEdgeFraction = temporary(dX1Grid)
  rightEdgeFraction = temporary(dX2Grid)
  
  targetArray = make_array(samples,lines)
  
  for sample=0,samples-1 do begin
    for line=0,lines-1 do begin
      stop 
      targetArray[sample,line] 
    endfor
  endfor
  
  
  
;  print,ulXPixelVector
;  print,ulXPixels
;  print,dX1Vector
;  print,dY1s


  print,lrXSubpixels
  print,lrXPixels
  print,dX2s
  print,dY2s

  


  
end