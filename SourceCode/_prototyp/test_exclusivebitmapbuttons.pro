pro test_exclusiveBitmapButtons

  tlb = widget_base(/COLUMN)
  !null = widget_label(tlb, Value='exclusive')
  base = widget_base(tlb, /EXCLUSIVE, /ROW)
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_label(tlb, Value='normal')
  base = widget_base(tlb, /ROW)
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_label(tlb, Value='non-exclusive')
  base = widget_base(tlb, /NONEXCLUSIVE, /ROW)
  !null = widget_button(base, Value=bindgen(16,16,3))
  !null = widget_button(base, Value=bindgen(16,16,3))  
  widget_control, /REALIZE, tlb

end