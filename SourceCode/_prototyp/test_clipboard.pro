pro test_clipboard
  ; create function graphic and get image content
  window = window()
  !null = plot(indgen(10), /CURRENT)
  imageData = window.CopyWindow()
  imageSize = (size(/DIMENSIONS, imageData))[1:2] 
  
  ; create object graphic hierarchy
  oImage = IDLgrImage(imageData);, INTERLEAVE=0)
  oView = IDLgrView(VIEWPLANE_RECT=[0,0,imageSize])
  oModel = IDLgrModel()
  oView.add, oModel
  oModel.add, oImage
  
  ; draw to clipboard
  oClipboard = IDLgrClipboard(DIMENSIONS=imageSize)
  oClipboard.draw, oView
  obj_destroy, oClipboard
end