function getDifferenceToUnity, hTest
  nruns = total(hTest)
  nfeatures = float(n_elements(hTest))

  ; contruct equal histo
  hEqual = replicate(nruns/nfeatures, nfeatures)

  ; calculate scaling factor to scale measure into [0,1] range
  maxValue = nruns*(2-2/nfeatures) ;   maxValue = (nfeatures-1)*nruns/nfeatures + nruns-nruns/nfeatures

  ; calculate result and return
  result = total(abs(hEqual-hTest))/maxValue
  return, result
end

pro test_getDifferenceToUnity
  hTest1 = [0,120, 0, 0, 0, 0] ; most stable
  hTest2 = [20,20,20,20,20,20] ; leased stable
  hTest3 = [40,40,40,0,0,0]    ; in between

  print, 'hTest1: ', getDifferenceToUnity(hTest1)
  print, 'hTest2: ', getDifferenceToUnity(hTest2)
  print, 'hTest3: ', getDifferenceToUnity(hTest3)
end