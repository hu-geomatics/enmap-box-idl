function testObj::init
  self.h = hash()
  (self.h)['value'] = 1
  return, 1b
end

function testObj::_overloadPrint
  return, (self.h)._overloadPrint()
end 

pro testObj__define
  struct = {testObj, inherits IDL_Object,$
    h : hash()}
end

pro test_save
  testObj = testObj()
  print, testObj

  save, testObj, FILENAME='d:\test
  testObj = !null
  restore, FILENAME='d:\test
  print, testObj
end
