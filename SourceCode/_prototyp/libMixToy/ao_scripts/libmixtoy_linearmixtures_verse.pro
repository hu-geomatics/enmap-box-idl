pro libMixToy_linearMixtures_versE, $
  num_mixStep=num_mixStep, $ ;6
  filename=filename,$ ;'g:\temp\temp_ar\4ar\Tree_C1_SVR.txt'
  head_len=head_len,$ ; 23
  t_end=t_end,$ ; 2
  num_row=num_row,$ ; 125
  BandPositions=bandPositions,$  
  ResultFilenameMixtureImage=filenameMixtureImage, $
  ResultFilenameFractionImage=filenameFractionImage
  
  


;****************************************************************************************************************
; OPEN SPECTRAL DATA AND SPECIFY UNDERLYING CLASSIFICATION SYSTEM

print, '...read ASCII data'
   
   
    ; read ASCII data
    filename = filename
    
    t_start = 1  
    r_start = t_end+1 
    r_end =  head_len-2
     
    OPENR, lun, filename, /GET_LUN
    
    header = STRARR(head_len)  
    READF, lun, header
      
    num_col = head_len-1 
    num_row = 125 ;*****
     
    
    
    data = FLTARR(num_col,num_row)
    READF, lun, data

    ; speclib classification system
    wavelengths = data[0,*]
    wavelengths_header = header(1)
    
   
    
    target_data = data[t_start:t_end,*]  
    target_header = header(t_start+1:t_end+1) 
  
    rest_data = data[r_start:r_end,*]
    rest_header = header(r_start+1:r_end+1)
    
    

;****************************************************************************************************************
; PREPARE BINARY MIXTURES
    

    
    S1_data = target_data ;****
    S1_header = target_header ;****
    S1_size = size(S1_data)
    S1_num = S1_size(1)
    
    S2_data = rest_data;[fuimp_data,paimp_data,other_data,gcv_data,tree_data,soil_data,water_data] ;****
    S2_header = rest_header;[fuimp_header, paimp_header, other_header, gcv_header, tree_header, soil_header, water_header]  ;****
    S2_size = size(S2_data)
    S2_num = S2_size(1)
 

;****************************************************************************************************************
; CALCULATE MIX SPECTRA

print, '...calculate mix spectra'
 
  ; prepare mixing
    num_mixSpec = S1_num*S2_num ; number of binary mixtures --> s1,s2; s1,s3 ... s1,Sn
    steps = 100/(num_mixStep-1)
    
    ; mixspec output array
    mixSpec_data = FLTARR(num_mixStep,num_mixSpec,num_row)
    ; percentage image
    perc_data = FLTARR(num_mixStep,num_mixSpec)
    ; header of mixtures
    mixSpec_header = STRARR(num_mixSpec) 

   
  ; calculate mix spectra   
    current_mixSpec =0
    
      ;read spectra 1
        for current_S1=0,S1_num-1 do begin
          spec1head = S1_header(current_S1)
          spec1 = reform(S1_data[current_S1,*]) 

        ;read spectra 2
          for current_S2=0,S2_num-1 do begin
            spec2head = S2_header(current_S2)
            spec2 = reform(S2_data[current_S2,*]) 
   
          ; calculate mixtures and write into mix spectra array    
            for current_mixStep=0,num_mixStep-1 do begin
              current_perc = current_mixStep/(num_mixStep-1.) 
              mixspec = (spec1*current_perc)+(spec2*(1-current_perc))              
              mixSpec_data(current_mixStep,current_mixSpec,*) = mixspec(*)       
              mixSpec_header(current_mixSpec)= 'lin. mixture ('+spec1head+', '+spec2head+')'      
              ; fill percentage image
              perc_data(current_mixStep,current_mixSpec) = current_perc(*)                     
            endfor         
      
          current_mixSpec=current_mixSpec+1       
          endfor
              
        endfor


;****************************************************************************************************************
; PLOT MIXED SPECTRA

  
   plotmix = 0b ; 0 = false 1 = true
   if plotmix then begin

print, '...plot mix spectra'
      
     for current_mixSpec=0,num_mixSpec-1 do begin  
       window,/Free,XSIZE=600,YSIZE=400
       device,DECOMPOSED=0
        
       plot,mixspec_data(0,current_mixSpec,*),YRANGE=[0,1.5],THICK=4,CHARSIZE=.8,TITLE=mixSpec_header(current_mixSpec)
       oplot,mixspec_data(num_mixStep-1,current_mixSpec,*),THICK=4
       
       for current_mixStep = 0,num_mixStep-1 do begin         
         oplot,mixspec_data(current_mixStep,current_mixSpec,*)
       endfor 
  
     endfor
    
    endif

; extract band position 1 and 2

mixSpec_data = mixSpec_data[*,*,bandPositions]
num_row = 2
bnames = 'Band'+strcompress(bandPositions+1)


;****************************************************************************************************************
; WRITE MIXED SPECTRA TO IMAGE FILE     

print, '...write mixed spectra image'
 
   fileNameMixSpecImage = filename+'_MixImg'+STRTRIM(steps,1)
   openw,unit,fileNameMixSpecImage,/get_lun
   writeu,unit,mixSpec_data
   envi_setup_head,fname=fileNameMixSpecImage,ns=num_mixStep,nl=num_mixSpec,nb=num_row, $
     data_type=4,interleave=0,file_type=0,bnames=bnames, /write,/open
   free_lun, unit

   fileNamePercData = filename+'_PercImg'+STRTRIM(steps,1) ;****

   openw,unit,fileNamePercData,/get_lun
   writeu,unit,perc_data
   envi_setup_head,fname=fileNamePercData,ns=num_mixStep,nl=num_mixSpec,nb=1, $
     data_type=4,interleave=0,file_type='ENVI Standard',DATA_IGNORE_VALUE=-99, /write,/open
;     WAVELENGTH_UNIT=???, WL=wavelengths
   free_lun, unit
   
filenameMixtureImage = fileNameMixSpecImage
filenameFractionImage=fileNamePercData
  
print, n_elements(perc_data)
;****************************************************************************************************************
print, '...analysis completed'
end