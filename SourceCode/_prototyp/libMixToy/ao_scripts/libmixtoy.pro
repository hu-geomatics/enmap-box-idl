pro libMixToy

  envi
  ; settings
   
  synthetic = 0b
  epsilonLoss = 0.0001
  ; 
 ; wd = 'G:\temp\temp_ar\4AR'
  
  ; define data: 2 features, 2 classes
  if synthetic then begin
    stop
    
  endif else begin
    num_mixStep = 6
    filename = 'g:\temp\temp_ar\4ar\Tree_C1_SVR'
    head_len = 23
    t_end= 2
    num_row = 125
    bandPositions = [14,29]
    xrange = [0,1.5]
    yrange = [0,1.5]
  endelse
  
  ; calculate mixed spectra
  if synthetic then begin
    stop
  endif else begin
    libMixToy_linearMixtures_versE, num_mixStep=num_mixStep, filename=filename, head_len=head_len, t_end=t_end, num_row=num_row, $
      BandPositions=bandPositions,$
      ResultFilenameMixtureImage=filenameMixtureImage, $
      ResultFilenameFractionImage=filenameFractionImage
  endelse
  
  ; train SVR
  enmapJavaBridge_init
  fn_model = 't:\model.svr'
  
  imagesvm_session_settings,INFO=info,/GET
  oImageSVM = info.oimagesvm
    
  oImageSVM->set_imagesvm_parameter,/GET_SETTING,IMAGESVM_PARAMETER=imagesvm_parameter
  imagesvm_parameter.p = epsilonLoss  ; epsilon-loss
  imagesvm_parameter.gs_e = 0.0001
  imagesvm_parameter.final_e = 0.0001  ; stopping criterion
  imagesvm_parameter.svm_type=3 ; SVR
  oImageSVM->set_imagesvm_parameter,IMAGESVM_PARAMETER=imagesvm_parameter
  imagesvm_parameterization $
    ,FN_MODEL=fn_model $
    ,FN_TRAIN_X=filenameMixtureImage $
    ,FN_TRAIN_Y=filenameFractionImage $
    ,MASK_VALUE_TRAIN=-99 $
    ,MEMORY=100 $
    ,OIMAGESVM=oimagesvm
    
  ; apply SVR
  
  n = 300
  xlimits = [0., 1.5]
  ylimits = [0., 1.5]
  filenameFeatureSpace = libMixToy_createFeatureSpace(n, xlimits, ylimits)
  filenameEstimation = 't:\estimation'
  
  imagesvm_apply, FN_MODEL=fn_model, FN_FEATURE=filenameFeatureSpace $
    ,FN_LABEL=filenameEstimation $
    ,MEMORY=100 $
    ,OIMAGESVM=oimagesvm $
    ,OPEN=1  
  
  ; plot
  
  window,/Free, xsize=n, ysize=n
  
  ; 
  ;- svrEstimation
  inputImage = hubIOImgInputImage(filenameEstimation)
  svrEstimation = inputImage.getCube()
  DEVICE, DECOMPOSED=0
  loadct, 13
  tvscl, svrEstimation
  
  ;- get pure spectra
  inputImage = hubIOImgInputImage(filenameMixtureImage)
  spectra = inputImage.getCube()
  nclass2 = t_end
  nclass1 = head_len - nclass2 - 2
  spectra1 = reform(spectra[0,0:nclass1-1,*])
  spectra2 = reform(spectra[-1,0:*:nclass1,*])
  loadct, 0
  plot, spectra1[*,0],spectra1[*,1],POSITION=[0,0,1,1],/NORMAL,/NOERASE,XRANGE=xlimits,YRANGE=ylimits,XSTYLE=1+4,YSTYLE=1+4,PSYM=5
  plot, spectra2[*,0],spectra2[*,1],POSITION=[0,0,1,1],/NORMAL,/NOERASE,XRANGE=xlimits,YRANGE=ylimits,XSTYLE=1+4,YSTYLE=1+4,PSYM=6
  for i=0,nclass1-1 do begin
    for k=0,nclass2-1 do begin
      oplot, [spectra1[i,0],spectra2[k,0]], [spectra1[i,1],spectra2[k,1]]
    endfor
  endfor


  
  ; - get pure pixel

  ;- linie zwischen pureSpectra und evtl. punkte bei konkreten fractions
  ;
  ;
  
  
  
end