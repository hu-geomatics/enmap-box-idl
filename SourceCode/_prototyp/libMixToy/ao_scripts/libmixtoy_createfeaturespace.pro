function libMixToy_createFeatureSpace, n, xlimits, ylimits

  xrange = xlimits[1] - xlimits[0]
  yrange = ylimits[1] - ylimits[0]
  
  xvector = findgen(n)/(n-1)*xrange+xlimits[0]
  yvector = findgen(n)/(n-1)*yrange+ylimits[0]
  
  xarray = rebin(xvector,n,n)
  yarray = rebin(transpose(yvector),n,n)

  ; write to file
  ; define the writer setting
  writerSettings = hash()
  writerSettings['samples'] = n
  writerSettings['lines'] = n
  writerSettings['bands'] = 2
  writerSettings['data type'] = 'float'
  writerSettings['dataFormat'] = 'band'

  ; create output image object and initialize the writer
  outputFilename = 't:\featureSpace'
  outputImage = hubIOImgOutputImage(outputFilename)
  outputImage.initWriter, writerSettings
  ; write the data and cleanup
  outputImage.writeData, xarray
  outputImage.writeData, yarray
  outputImage.cleanup
  return, outputFilename

end