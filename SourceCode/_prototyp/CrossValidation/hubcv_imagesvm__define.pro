function hubCV_imageSVM::init, inputImg, inputRef, nFolds, SVMparameters
  init = self.hubCVBase::init(inputImg, inputRef, nFolds) 
  if ~init then return, 0b
  
  modelParameters = hash()
  foreach tag, SVMparameters.keys() do begin
    modelParameters[tag] = SVMparameters[tag] 
  endforeach
  
  ;set default values
  imgRef = hubIOImgInputImage(inputRef)
  if imgRef.isClassification() then begin
    modelParameters['svmType'] = 'svc' 
    self.title = 'SVC Cross Validation'
    self.modelFileExtension = 'svc'
  endif else begin
    modelParameters['svmType'] = 'svr'
    self.title = 'SVR Cross Validation'  
  endelse  
  self.modelFileExtension = strlowcase(modelParameters['svmType'])
  
;  if ~modelParameters.hubIsa('cArray') then modelParameters['cArray'] = self.getGridSearchVector(0.01, 1000.,10)
;  if ~modelParameters.hubIsa('gArray') then modelParameters['gArray'] = self.getGridSearchVector(0.01, 1000.,10)
;  if ~modelParameters.hubIsa('numberOfFolds') then modelParameters['numberOfFolds'] = 3
;  if ~modelParameters.hubIsa('stopGridSearch') then modelParameters['stopGridSearch'] = 0.01d
;  if ~modelParameters.hubIsa('stopTraining') then modelParameters['stopTraining'] = 0.0001d
  
  self.modelParameters = modelParameters
  return, 1b
end

function hubCV_imageSVM::getGridSearchVector, pMin, pMax, pMultiplier

    exp1 = double(round(alog10(pMin)/alog10(pMultiplier)))
    exp2 = double(round(alog10(pMax)/alog10(pMultiplier)))
    vector = pMultiplier^(dindgen(exp2-exp1+1)+exp1)
    return, vector

end

pro hubCV_imageSVM::trainModel, inputImage, inputLabels, modelPath
  parameters = self.modelParameters
  parameters['featureFilename'] = inputImage
  parameters['labelFilename'] = inputLabels
  parameters['modelFilename'] = modelPath
  imageSVM_parameterize_processing, parameters, Title=self.title, GroupLeader=self.groupLeader
end

pro hubCV_imageSVM::mergeProbabilityImages, pathImage
   ;find probability images 
   foldInfos = self.getFoldInfos()
   self.mergeEstimations, foldInfos[*].pathEstimation + 'ClassProbabilities', pathImage,DATAIGNOREVALUE=-999 
end

pro hubCV_imageSVM::applyModel, inputImage, inputMask, modelPath, outputLabels
  
  parameters = self.modelParameters
  parameters['modelFilename']   = modelPath 
  parameters['featureFilename'] = inputImage
  parameters['outputLabelFilename']   = outputLabels
  parameters['maskFilename'] = inputMask
  imageSVM_apply_processing, parameters, Title=self.title, GroupLeader=self.groupLeader

end




pro hubCV_imageSVM__define
 struct = {hubCV_imageSVM $
   , pathMergedProbabilityImages:'' $
   , inherits hubCVBase $
  
   }
end

pro test_hubCV_imageSVM
  imgRef   = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  imgImg   = hub_getTestImage('Hymap_Berlin-A_Image')
  ;imgImg   = 'D:\Sandbox\CVTest\AF_ImageScaled'
  ;imgStrat = enmapBox_getTestImage('AF_LC')
  ;workingDirectory = 'D:\Sandbox\CVTest\'
  workingDirectory = 'D:\work\BJ\Temp\'
  ;imgImg = 'D:\Sandbox\cv_CBF\small2005-193_CBF_PBC+Stats_stack.bsq'
  ;imgRef = 'D:\Sandbox\cv_CBF\small2005_CBF_6classes_classimage.bsq'
  ;imgStrat = 'D:\Sandbox\cv_CBF\small2005_CBF_6classes_classimage.bsq'
  
  SVMParameters = Hash()
  SVMParameters['outputProbabilities'] = 1b
  nFolds = 2
 
  CV = hubCV_imageSVM(imgImg, imgRef, nFolds, SVMParameters)
  CV.setProperty, WORKINGDIRECTORY=workingDirectory $ 
                , DEBUG=1 $
                , inputStratification = imgStrat $
                , inputMask = imgMask
  
  CV.createFoldImage
  print, 'test: run validation'
  CV.runValidation
  CV.mergeProbabilityImages, 'D:\work\BJ\Temp\ProbMerge'
  CV.cleanup

end