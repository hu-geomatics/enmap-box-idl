function hubCVBase::init, inputImg, inputRef, nFolds 
   
   
   ;set required values
   self.pathInputImgCal = inputImg
   self.pathInputImgVal = inputImg
   self.pathInputRefCal = inputRef
   self.pathInputRefVal = inputRef
   
   ;set default values
   self.groupLeader = WIDGET_BASE()
   self.title='Cross Validation'
   if nFolds lt 2 then message, 'nFolds must be >= 2'
   self.nFolds = nFolds
   self.workingDirectory = file_dirname(filepath('xyz', /TMP))
   self.pathFoldImage = ''  
   self.pathReport = FILEPATH('CVReport.html', root_dir=self.workingDirectory)
   self.nBins = 2u^12
   self.reportNotes = list()
   ;file_mkdir, self.workingDirectory
   
   
   imgRef = hubIOImgInputImage(self.pathInputRefCal)
  case 1b of
    imgRef.isClassification() : self.estimationType = 'classification'
    imgRef.isRegression()     : self.estimationType = 'regression'
    else : message, 'can not derive estimationType'
  endcase
   
  return, 1b
end


;+
; :Description:
;    Use this routine to set properties
;
;
;
; :Keywords:
;    reportPath
;    inputReferenceCalibration
;    inputReferenceValidation
;    inputReference
;    inputMask
;    inputStratification
;    estimationType
;    workingDirectory
;    title
;    debug
;    groupLeader
;
; :Author: geo_beja
;-
pro hubCVBase::setProperty $
    , reportPath=reportPath $
    , foldImagePath=foldImagePath $
    , inputImageCalibration=inputImageCalibration $
    , inputImageValidation=inputImageValidation $
    , inputReferenceCalibration=inputReferenceCalibration $
    , inputReferenceValidation=inputReferenceValidation $
    , inputMask = inputMask $ 
    , inputStratification = inputStratification $
    , outputMergedEstimations = outputMergedEstimations $
    , estimationType = estimationType $
    , workingDirectory = workingDirectory $
    , title = title $
    , debug = debug $
    , groupLeader = groupLeader
  
  if isa(foldImagePath) then begin
    imgFold = hubIOImgInputImage(foldImagePath)
    if ~imgFold.isClassification() then message, 'Fold image must have file type ENVI Classification'
    self.pathFoldImage = foldImagePath
  endif
  
  if isa(estimationType) then self.estimationType = estimationType
  if isa(inputImageCalibration)then self.pathInputImgCal = inputImageCalibration
  if isa(inputImageValidation) then self.pathInputImgVal = inputImageValidation
  if isa(inputReferenceCalibration)then self.pathInputRefCal = inputReferenceCalibration
  if isa(inputReferenceValidation) then self.pathInputRefVal = inputReferenceValidation
  
  if isa(inputMask)               then self.pathInputMask = inputMask
  if isa(inputStratification)     then self.pathInputStrat = inputStratification
  if isa(outputMergedEstimations) then self.pathMergedEstimations = outputMergedEstimations
  if isa(reportPath)              then self.pathReport = reportPath
  if isa(workingDirectory)        then self.workingDirectory = workingDirectory
  if isa(title)                   then self.title=title
  if isa(debug)                   then self.debug = keyword_set(debug)
  if isa(groupLeader)             then self.groupLeader = groupLeader
end

pro hubCVBase::addReportNotes, values
  foreach note, values do begin
    (self.reportNotes).add, note
  endforeach

end

function hubCVBase::isMasked
 return, self.pathInputMask ne ''
end

function hubCVBase::isStratified
 return, self.pathInputStrat ne ''
end

pro hubCVBase::checkParameters
  
  ;image sizes
  img = hubIOImgInputImage(self.pathInputImgCal)
  spatialSize = img.getSpatialSize()
  checkSize = transpose([self.pathInputImgVal, self.pathInputMask $
                        , self.pathInputRefCal, self.pathInputRefVal $
                        , self.pathInputStrat])
  
  foreach path, checkSize do begin
    if path eq '' then continue
    img2 = hubIOImgInputImage(path)
    if ~img2.isCorrectSpatialSize(spatialSize) then begin
      message, string(format='(%"Image %s has wrong spatial size")', path) 
    endif
  endforeach
  
  ;spectral size in case of differing calibration and validation images
  if self.pathInputImgCal ne self.pathInputImgVal then begin
    img2 = hubIOImgInputImage(self.pathInputImgVal)
    if ~img2.isCorrectSpectralSize(img.getSpectralSize()) then begin
      message, string(format='(%"Image %s has wrong spectral size")', self.pathInputImgVal)
    endif
  endif
  
  
end

pro hubCVBase::extractFromMask, pathInputImages, pathMask, pathOutputImages
  if n_elements(pathInputImages) ne n_elements(pathOutputImages) then begin
    message, 'different number of input and output paths'
  endif
  
  tileLines = 1000 
  maskImg = hubIOImgInputImage(pathMask, /Mask)
  maskImg.initReader, tileLines, /TILEPROCESSING, /SLICE, /Mask
  
  nImages = n_elements(pathInputImages)
  nSamples = 1
  nLines = 0l
  while ~maskImg.tileProcessingDone() do begin
    maskData = maskImg.getData()
    nLines += total(maskData, /Integer)
  endwhile
  maskImg.finishReader
  
  inputImages = List()
  outputImages = List()
  maskImg.initReader, tileLines, /TileProcessing, /Slice, /MASK
  for i=0, nImages - 1 do begin
    imgIn = hubIOImgInputImage(pathInputImages[i])
    imgOut = hubIOImgOutputImage(pathOutputImages[i],NOOPEN=1 )
    imgOut.setMeta, 'samples', 1
    imgOut.setMeta, 'lines', nLines
    imgOut.copyMeta, imgIn, /COPYFILETYPE, /COPYLABELINFORMATION, /COPYSPECTRALINFORMATION  
    imgOut.setMeta, 'bands', imgIn.getMeta('bands')
    
    imgIn.initReader, tileLines, /TILEPROCESSING, /SLICE
    imgOut.initWriter, imgIn.getWriterSettings(SETLINES=nLines,SETSAMPLES=1) 
    inputImages.add, imgIn
    outputImages.add, imgOut
  endfor
  
  ;read & write
  cntLines = 0l
  while ~maskImg.tileProcessingDone() do begin
    maskData = maskImg.getData()
    iValid = where(maskData[0,*] ne 0, /NULL, nValid)
    cntLines += nValid
    for i=0, nImages - 1 do begin
      imgData = (inputImages[i]).getData()
      if nValid gt 0 then begin
        (outputImages[i]).writeData, imgData[*,iValid]
      endif
    endfor
    
  endwhile
  
  ;finish readers & writers
  for i=0, nImages - 1 do begin
      (inputImages[i]).finishReader
      (outputImages[i]).finishWriter
      
      
      (inputImages[i]).cleanup
      (outputImages[i]).cleanup
  endfor
    
end

pro hubCVBase::runValidation, fastCV=fastCV, ShowReport=ShowReport

  
  ;set defaults
  if self.pathFoldImage eq '' then self.createFoldImage
  if self.pathMergedEstimations eq '' then begin
    self.pathMergedEstimations  = filepath('labelsEstimated', root_dir = self.workingDirectory)
  endif
  self.checkParameters
  
  ;extract intersection features and reference samples using the fold image.
  ;this avoids estimation values for features without any reference to validate estimated values
  if keyword_set(fastCV) then begin
    self.debugMessage, 'Extract spatial intersections between features and reference for faster cross validation...'
    inputImages = [self.pathInputImgCal $
                  ,self.pathInputImgVal $
                  ,self.pathInputRefCal $
                  ,self.pathInputRefVal $
                  ,self.pathInputMask $ 
                  ,self.pathInputStrat $
                  ,self.pathFoldImage]
    inputImages = transpose(inputImages)
    outputImages = FILEPATH('Intersection_'+FILE_BASENAME(inputImages), ROOT_DIR=self.workingDirectory)
    iMissing = where(inputImages eq '', /NULL)
    if isa(iMissing) then outputImages[iMissing] = ''
    
    
    iUniq = uniq(inputImages, sort(inputImages))
    inNames  = inputimages[iUniq]
    outNames = outputimages[iUniq]
    iExists = where(inNames ne '')
    self.extractFromMask, inNames[iExists], self.pathFoldImage, outNames[iExists]
    
    ;set new file paths
    self.pathInputImgCal = outputImages[0]
    self.pathInputImgVal = outputImages[1]
    self.pathInputRefCal = outputImages[2]
    self.pathInputRefVal = outputImages[3]
    self.pathInputMask   = outputImages[4]
    self.pathInputStrat  = outputImages[5]
    self.pathFoldImage   = outputImages[6]

  endif
  
  
  
  acc = self.getCrossValidationAccuraries()
  rep = self.getReport(acc)
  
  htmlPath = self.pathReport
  if ~stregex(htmlPath, '[.](xml|xhtml|html|htm)$', /Boolean,/FOLD_CASE) then begin
      htmlPath += 'html'
  endif 
  rep.saveHTML, htmlPath, Show=isa(ShowReport)? ShowReport : 1b
end

function hubCVBase::getReport, accuracies
  self.debugMessage, 'Get accuracy report'
  foldAccuracies = accuracies['foldAccuracies']
  mergedAccuracies = accuracies['mergedAccuracies']
  
  repList = hubReportList(title='Cross Validation Report')
  repFiles = repList.newReport(title='Input Files')
  
  info = list()
  if self.pathInputImgCal ne self.pathInputImgVal then begin
    info.add, 'Feature Image Calibration: ' + self.pathInputImgCal
    info.add, 'Feature Image Validation : ' + self.pathInputImgVal
  endif else begin
    info.add, 'Feature Image: ' + self.pathInputImgCal
  endelse
    
    
    
  if self.pathInputRefCal ne self.pathInputRefVal then begin
    info.add, 'Label Image Calibration:   ' + self.pathInputRefCal
    info.add, 'Label Image Validation :   ' + self.pathInputRefVal
  endif else begin
    info.add, 'Label Image  :   ' + self.pathInputRefCal
  endelse
  if self.pathInputMask  ne '' then info.add, 'Mask:    '+self.pathInputMask
  if self.pathInputStrat ne '' then info.add, 'Stratification: '+self.pathInputStrat
  repFiles.addMonospace, info.toArray()
  
  
  repSettings = repList.newReport(title='CV Settings')
  info = list()
  info.add, 'Folds: '+strtrim(self.nFolds , 2)
  for i = 0, self.nFolds-1 do begin
      info.add, string(format='(%"Fold %2i Training:   %s")', i+1, foldAccuracies[i].pathTraining)
      info.add, string(format='(%"Fold %2i Model:      %s")', i+1, foldAccuracies[i].pathModel)
      info.add, string(format='(%"Fold %2i Estimation: %s")', i+1, foldAccuracies[i].pathEstimation)
      info.add, string(format='(%"Fold %2i Validation: %s")', i+1, foldAccuracies[i].pathValidation)
  endfor
  repSettings.addMonospace, info.toArray()
    
  repModelSettings = repList.newReport(title='Model Settings')
  table = hubReportTable(['Key','Value'])
  modelParameters = self.modelParameters
  foreach key, modelParameters.keys() do begin
    table.addRow, List(key, strjoin(modelParameters[key], ' '))
  endforeach
  repModelSettings.addHTML, table.getHTMLTable()
  
  
  
  repFoldACC = repList.newReport(title='Fold Accuracies')
  repFoldACC.addHeading, 'Summary'
  case self.estimationType of
      'classification' : begin
                headings = ['#Fold', 'Training', 'Validation','Masked','OAA','Prod','User','Kappa','F1']
                formats  = ['(%"%i")','(%"%i")','(%"%i")','(%"%i")','(%"%f")','(%"%f")','(%"%f")','(%"%f")','(%"%f")']
                table = hubReportTable(headings, formats=formats)
                for i = 0, self.nFolds-1 do begin
                  perfAll = foldAccuracies[i].performance
                  perfRest  = (perfAll['performance'])[0]
                  perfVal   = (perfAll['performance'])[1]
                  argList = list()
                  argList.add, i+1
                  argList.add, foldAccuracies[i].nTraining
                  argList.add, foldAccuracies[i].nValidation
                  argList.add, foldAccuracies[i].nMasked
                  argList.add, perfVal.OVERALLACCURACY
                  argList.add, perfVal.AVERAGEPRODUCERACCURACY
                  argList.add, perfVal.AVERAGEPRODUCERACCURACY
                  argList.add, perfVal.KAPPAACCURACY
                  argList.add, perfVal.AVERAGEF1ACCURACY
                  table.addRow, argList
                endfor
                repFoldACC.addHTML, table.getHTMLTable()
                     end
      'regression' : begin
          message, 'not implemented yet'
    
    
                     end
                     
    else: message, 'not implemented yet'
    
  endcase
  
  repFoldACC.addHeading, 'Class wise F1 accuracies'
   case self.estimationType of
      'classification' : begin
                foldNumbers = indgen(self.nFolds)+1
                nClasses = (foldAccuracies[0].performance)['classes']
                classNames = (foldAccuracies[0].performance)['class names']
                headings = ['#', 'Class/Fold', strtrim(foldNumbers,2), 'Mean', 'Std']
                formats  = ['(%"%i")','(%"%s")',make_array(self.nFolds+2,value='(%"%f")')]
                table = hubReportTable(headings, formats=formats)
                table.addColumn, indgen(nClasses-1)+1
                table.addColumn, classNames[1:*]
                M_F1 = make_array(self.nFolds, nClasses-1)
                for i = 0, self.nFolds-1 do begin
                  perfAll = foldAccuracies[i].performance
                  perfVal   = (perfAll['performance'])[1]
                  M_F1[i,*] = perfVal.F1ACCURACY
                endfor
                
                for i = 0, self.nFolds-1 do begin
                  table.addColumn, M_F1[i,*]
                endfor
                table.addColumn, mean(M_F1, dimension=1)
                table.addColumn, stddev(M_F1, dimension=1)
                
                repFoldACC.addHTML, table.getHTMLTable()
                     end
      'regression' : begin
          message, 'not implemented yet'
    
    
                     end
                     
    else: message, 'not implemented yet'
    
  endcase
  
  
  repTotalACCList = hubApp_accuracyAssessment_getReport(mergedAccuracies, /createPlots)
  repList.addReport, repTotalACCList.getReport(1)
  
  if n_elements(self.reportNotes) ne 0  then begin
    notesRep = repList.newReport(title='Notes')
    notesRep.addMonospace, (self.reportNotes).toArray()
  endif
  
  return, repList
end


function hubCVBase::getCrossValidationAccuraries
  
  imgFold   = hubIOImgInputImage(self.pathFoldImage)
  imgRefCal = hubIOImgInputImage(self.pathInputRefCal) ;Reference for calibration
  imgRefVal = hubIOImgInputImage(self.pathInputRefVal) ;Reference for validation
  
  
  
  
  case self.estimationType of
    'classification' : imgRefDIV = 0b
    'regression'     : begin
          imgRefDIV = imgRefCal.getMeta('data ignore value')
          
          if imgRefDIV ne imgRefVal.getMeta('data ignore value') then $
            message, 'Reference images must have same data ignore value'
            
                       end
  endcase
  
  tileLines = hubIOImghubIOImg_getTileLines(imgRefCal.getMeta('samples') $
                                   ,imgRefCal.getMeta('bands')+3 $
                                   ,imgRefCal.getMeta('data type'))

  ;hubApp_accuracyAssessment_processing_getAccuraciesClassification, par, imgRef, imgEst $
  ;  , imgErr=imgErr, imgMask=imgMask, imgStrat=imgStrat, pBar=pBar
  
  foldAccuracies = replicate({performance:Hash() $
                             ,foldNumber:-1 $
                             ,nTraining:0l $
                             ,nValidation:0l $
                             ,nMasked:0l $
                             ,pathTraining: '' $
                             ,pathValidation: '' $
                             ,pathEstimation: '' $
                             ,pathModel:'' $
                             }, self.nFolds)
  nFoldPixels = *self.nFoldPixels
  
  
  for fold = 1, self.nFolds do begin
    
    iFold = fold-1
    ;temp file paths
    nameLabelsTrain = string(format='(%"labelsFold%i_train")', fold)
    nameLabelsVal   = string(format='(%"labelsFold%i_val")', fold)
    nameLabelsEst   = string(format='(%"labelsFold%i_est")', fold)
    nameModel       = string(format='(%"CVmodel%i.%s")', fold, (self.modelFileExtension ne '' ? self.modelFileExtension :'model'))
    foldAccuracies[iFold].pathTraining   = filepath(nameLabelsTrain, root_dir = self.workingDirectory)
    foldAccuracies[iFold].pathValidation = filepath(nameLabelsVal, root_dir = self.workingDirectory)
    foldAccuracies[iFold].pathEstimation = filepath(nameLabelsEst, root_dir = self.workingDirectory)
    foldAccuracies[iFold].pathModel      = filepath(nameModel, root_dir = self.workingDirectory)
    
    ;create temporary label files
    outLabelsCal = hubIOImgOutputImage(foldAccuracies[iFold].pathTraining)
    outLabelsVal = hubIOImgOutputImage(foldAccuracies[iFold].pathValidation)
    
    
    outLabelsCal.copyMeta, imgRefCal, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation
    outLabelsCal.setMeta, 'band names', nameLabelsTrain
    outLabelsCal.setMeta, 'description', ['Cross Validation Fold Labels for Model Calibration' $
                                         ,'This is a subset of '+self.pathInputRefCal $
                                           ]
    outLabelsVal.copyMeta, imgRefVal, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation
    outLabelsVal.setMeta, 'description', ['Cross Validation Fold Labels for Model Validation' $
                                           ,'This is a subset of '+self.pathInputRefVal $
                                           ]
    
    ;init readers and writers
    imgFold.initReader, tileLines, /Tileprocessing, /Slice
    imgRefCal.initReader, tileLines, /Tileprocessing, /Slice
    imgRefVal.initReader, tileLines, /Tileprocessing, /Slice
    outLabelsCal.initWriter, imgRefCal.getWriterSettings()
    outLabelsVal.initWriter, imgRefVal.getWriterSettings()
    
    ;write fold label files
    self.debugMessage, string(format='(%"Fold %i : write cal/val data...")', fold)
    while ~imgFold.tileProcessingDone() do begin
      foldData    = imgFold.getData()
      refDataCal  = imgRefCal.getData()
      refDataVal  = imgRefVal.getData()
      
      !NULL = where(foldData eq 0, nMasked)
      ;all non-masked & non-fold pixels for calibration
      refDataCal[where(foldData eq fold  or foldData eq 0, /NULL, nComplement=nCal)] = imgRefDIV
      ;all non-masked & fold pixels for validation
      refDataVal[where(foldData ne fold,                   /NULL, nComplement=nVal)] = imgRefDIV
      
      foldAccuracies[iFold].nTraining    += nCal
      foldAccuracies[iFold].nValidation  += nVal
      foldAccuracies[iFold].nMasked      += nMasked
      
      outLabelsCal.writeData, refDataCal
      outLabelsVal.writeData, refDataVal
    endwhile
    
    imgFold.finishReader
    imgRefCal.finishReader
    imgRefVal.finishReader
    ;
    outLabelsCal.finishWriter
    outLabelsVal.finishWriter
    
    ;run model training
    self.debugMessage, string(format='(%"Fold %i : calibrate model %s...")', fold, foldAccuracies[iFold].pathModel)
    self.trainModel, self.pathInputImgCal, foldAccuracies[iFold].pathTraining, foldAccuracies[iFold].pathModel
    
    ;apply model to get estimations for the validation pixels
    self.debugMessage, string(format='(%"Fold %i : apply model %s...")', fold, foldAccuracies[iFold].pathModel)
    self.applyModel, self.pathInputImgVal, foldAccuracies[iFold].pathValidation, foldAccuracies[iFold].pathModel, foldAccuracies[iFold].pathEstimation
    
    ;run accuracy assessment for single fold
    self.debugMessage, string(format='(%"Fold %i : accuracy assessment...")', fold)
    accAssPar = hash()
    accAssPar['inputReference'] = self.pathInputRefVal
    accAssPar['inputEstimation'] = foldAccuracies[iFold].pathEstimation
    foldPerformance = hubApp_accuracyAssessment_processing(accAssPar, settings)
    
    foldAccuracies[iFold].foldNumber = fold
    foldAccuracies[iFold].performance = foldPerformance
    
    self.foldInfos = ptr_new(foldAccuracies)
  endfor 
  
  
  ;merge images
  self.mergeEstimations, foldAccuracies[*].pathEstimation, self.pathMergedEstimations
  
  ;run accuracy assessment on merged estimation image
  accAssPar = hash()
  accAssPar['inputReference'] = self.pathInputRefVal
  accAssPar['inputEstimation'] = self.pathMergedEstimations 
  mergedAcc = hubApp_accuracyAssessment_processing(accAssPar, settings)
  
  return, Hash('foldAccuracies',foldAccuracies,'mergedAccuracies',mergedAcc)
end




pro hubCVBase::mergeEstimations, estimationFiles, outputImage, dataIgnoreValue=dataIgnoreValue
  self.debugMessage, 'Merge estimations'
  imgList = list()
  if ~isa(estimation) then estimationType = self.estimationType 
  
  fileType = (hubIOImgInputImage(estimationFiles[0])).getMeta('file type')
  
  foreach estimationFile, estimationFiles do begin
    if stregex('Classification',fileType, /FOLD_CASE, /BOOLEAN) then begin
      imgList.add, hubIOImgInputImage(estimationFile, /Classification)
    endif else begin
      imgList.add, hubIOImgInputImage(estimationFile)
    endelse
  endforeach
  
  nSamples = (imgList[0]).getMeta('samples')
  nLines   = (imgList[0]).getMeta('lines')
  nBands   = (imgList[0]).getMeta('bands')
  nImages  = n_elements(estimationFiles)
  
  
  if isa(dataIgnoreValue) then begin
    div = dataIgnoreValue 
  endif else begin
    case self.estimationType of
      'regression': div = (imgList[0]).getMeta('data ignore value')
      'classification' : div = 0b
    endcase
  endelse
  
  tileLines = hubIOImg_getTileLines(nSamples $
                                    , nImages * nBands $
                                    , 'double')
  
  ;init readers
  foreach img, imgList do begin
    img.initReader, tileLines, /TileProcessing, /Slice
  endforeach
  
  ;create & init writer
  outImg = hubIOImgOutputImage(outputImage)
  outImg.copyMeta, (imgList[0]), /COPYFILETYPE, /COPYLABELINFORMATION, /COPYSPATIALINFORMATION, /COPYSPECTRALINFORMATION
  outImg.initWriter, (imgList[0]).getWriterSettings()
  
  while ~(imgList[0]).tileProcessingDone() do begin
    data = (imgList[0]).getData()
    for i = 1, nImages-1 do begin
      data2 = (imgList[i]).getData()
      iValues = where(data2 ne div, /NULL)
      if isa(iValues) then begin
        data[iValues] = data2[iValues]
      endif
    endfor
    outImg.writeData, data
  endwhile
  
  ;finish reading & writing
  foreach img, imgList do begin
    img.finishReader
  endforeach
  
  outImg.finishWriter
  
  

end




pro hubCVBase::trainModel, inputImage, inputLabels, modelPath
  message, "This routine is abstract. Overwrite it with your own 'trainModel' logic"
end


pro hubCVBase::applyModel, inputImage, inputMask, modelPath, outputLabels
  message, "This routine is abstract. Overwrite it with your own 'applyModel' logic"
end

pro hubCVBase::debugMessage, message
  if self.debug then print, message
  
end

function hubCVBase::getFoldInfos
  return, *(self.foldInfos)
end

pro hubCVBase::createFoldImage, pathFoldImage 
 
 
 file_mkdir, self.workingDirectory
 if isa(pathFoldImage) then self.pathFoldImage = pathFoldImage
 if self.pathFoldImage eq '' then self.pathFoldImage = filepath('CVFoldImage', root_dir = self.workingDirectory)
 
 imgRef = hubIOImgInputImage(self.pathInputRefCal)
 if self.isMasked() then begin
   imgMask =  hubIOImgInputImage(self.pathInputMask)
 endif

 if self.isStratified() then begin
   imgStrat =  hubIOImgInputImage(self.pathInputStrat)
   nStrata = imgStrat.getMeta('classes')-1 ;unclassified class is excluded!
 endif else begin
   nStrata = 1
 endelse
 
 nLines = imgRef.getMeta('lines')
 nSamples = imgRef.getMeta('samples')
 tileLines = nLines * nSamples
 hubApp_manageReaders, imgStrat, tileLines, MASKIMAGES=list(imgRef,imgMask), /Init, /Slice
 strataMask = imgRef.getData() 
 if isa(imgMask) then strataMask *= imgMask.getData()
 if isa(imgStrat) then begin
    stratData = imgStrat.getData()
    strataMask *= stratData
 endif
 
 if nStrata ne max(strataMask) then message, 'Header of stratification image is wrong'
 
 pxPerStratum = make_array(nStrata, value=-1, /Long)
 for stratum = 1, nStrata do begin
   pxPerStratum[stratum - 1] = n_elements(where(strataMask eq stratum, /NULL)) 
 endfor
 
 pxPerFold = long(round(double(pxPerStratum) / self.nFolds)) 
 foldMask = make_array(n_elements(strataMask), /Byte, value = 0b)
 
 seed = systime(1)
 for stratum = 1 , nStrata do begin
  iStrat = where(strataMask eq stratum, nStrat)
  if nStrat eq 0 then continue
  if nStrat gt 0 and nStrat lt self.nFolds then begin
    message, string(format='(%"Stratum %i has more than one and less than %i pixels")', stratum, self.nFolds)
  endif
  
  for fold = 1, self.nFolds - 1 do begin
    nRequired = pxPerFold[stratum-1]
    while nRequired gt 0 do begin
      indicesToSampleFrom = where(foldMask eq 0 and strataMask eq stratum, nAvailable)
      
      iiFold = long(randomu(seed, nRequired) * nAvailable)
      iiFold = uniq(iiFold, sort(iiFold))
      foldMask[indicesToSampleFrom[iiFold]] = fold
      nRequired -= n_elements(iiFold)  
    endwhile
  endfor
 endfor
 ;assign remaining pixels to last fold
 foldMask[where(foldMask eq 0 and strataMask ne 0)] = self.nFolds
  

 ;write the fold mask
 imgFolds = hubIOImgOutputImage(self.pathFoldImage)
 imgFolds.setMeta, 'classes', self.nFolds+1
 imgFolds.setMeta, 'class names', ['unclassified', 'Fold '+strtrim(indgen(self.nFolds)+1,2)]
 rgb = byte(randomu(systime(1), 3, self.nfolds+1)*255)
 rgb[*,0] = 0
 imgFolds.setMeta, 'class lookup', rgb
 imgFolds.setMeta, 'file type', 'ENVI Classification'
 imgFolds.setMeta, 'data type', 'byte'
 imgFolds.initWriter, imgRef.getWriterSettings(SETBANDS=1,SETDATATYPE='byte')
 imgFolds.writeData, foldMask
 imgFolds.finishWriter

 hubApp_manageReaders, list(imgRef, imgStrat), MASKIMAGES=imgMask, /Finish
 hubApp_manageReaders, list(imgRef, imgStrat), MASKIMAGES=imgMask, /Cleanup
 
 foldPixels = make_array(self.nFolds, /Long, value=0)
 for i = 0, self.nFolds-1 do begin
  foldPixels[i] = n_elements(where(foldMask eq (i+1), /NULL)) 
 endfor
 self.nFoldPixels = ptr_new(foldPixels)
 self.debugMessage, 'Fold pixels: '+strjoin(strtrim(*(self.nFoldPixels),2),' ')
  
 
  
end



pro hubCVBase__define
 struct = {hubCVBase $
   , title:'' $
   , pathInputImgCal:'' $
   , pathInputImgVal:'' $
   , pathInputRefCal:''   $
   , pathInputRefVal:''   $
   , pathInputStrat:'' $
   , pathInputMask:''  $
   , pathFoldImage:''  $
   , pathMergedEstimations:'' $
   , pathReport:'' $
   , modelFileExtension:'' $
   , estimationType:'' $
   , workingDirectory:'' $
   , nBins:0u $
   , nFolds: 0 $
   , nFoldPixels: ptr_new() $
   , foldInfos:ptr_new() $
   , groupLeader:0ul $
   , modelParameters:Hash() $
   , reportNotes:list() $
   , debug:0b $
   , inherits IDL_Object $
   }
   ;, hasMapInfo : 0b $
 ;}
end

