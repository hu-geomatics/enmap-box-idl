function hubCV_imageRF::init, inputImg, inputRef, nFolds, RFParameters 
;$
;   , workingDirectory=workingDirectory $
;   , inputStrat=inputStrat $
;   , inputMask=inputMask $
;   , debug=debug
  
  modelParameters = hash()
  foreach tag, RFParameters.keys() do begin
    modelParameters[tag] = RFParameters[tag] 
  endforeach
  
  imgRef = hubIOImgInputImage(inputRef)
  modelParameters['rfType'] = imgRef.isClassification() ? 'RFC' : 'RFR'  
  
  self.modelFileExtension = strlowcase(modelParameters['rfType'])
  
  self.modelParameters = modelParameters
  return, self.hubCVBase::init(inputImg, inputRef, nFolds)
end


pro hubCV_imageRF::trainModel, inputImage, inputLabels, modelPath
  parameters = self.modelParameters
  parameters['inputImage']  = inputImage
  parameters['inputLabels'] = inputLabels
  parameters['model']       = modelPath
  imageRF_Parameterize_Processing, parameters
end


pro hubCV_imageRF::applyModel, inputImage, inputMask, modelPath, outputLabels
  parameters = Hash()
  parameters['inputImage'] = inputImage
  parameters['inputMask']  = inputMask
  parameters['model']      = modelPath
  parameters['outputImgEst'] = outputLabels
  imageRF_apply_imageProcessing, parameters
end




pro hubCV_imageRF__define
 struct = {hubCV_imageRF $
   , inherits hubCVBase $
   }
end

pro TEST_HUBCV_imageRF
  
  imgImg   = hub_getTestImage('Hymap_Berlin-A_Image')
  imgRef   = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  imgStrat = imgref
  workingDirectory = 'D:\work\BJ\WD\Sandbox'
  
  ;imgImg = 'D:\Sandbox\cv_CBF\small2005-193_CBF_PBC+Stats_stack.bsq'
  ;imgRef = 'D:\Sandbox\cv_CBF\small2005_CBF_6classes_classimage.bsq'
  ;imgStrat = 'D:\Sandbox\cv_CBF\small2005_CBF_6classes_classimage.bsq'
  
  RFParameters = Hash()
  CV = hubCV_imageRF(imgImg, imgRef, 3, RFParameters)
  CV.setProperty, workingDirectory=workingDirectory $
                , inputStrat=imgStrat, inputMask=imgMask
          
  CV.runValidation
  CV.cleanup
end
