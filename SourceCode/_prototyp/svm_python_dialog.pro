function svm_getParameters, svmType, Title=title, GroupLeader=groupLeader

  tsize=120
     
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleset,'sampleSet', Title='Image',$
    Regression=svmType eq 'svr', Classification=svmType eq 'svc', TSize=tSize,$
    VALUE=hub_getappState('imageSVM', svmType+'FeatureFilename'), ReferenceValue=hub_getAppState('imageSVM',  svmType+'LabelFilename')

  hubAMW_frame, Title='Parameters', /Advanced
;  hubAMW_label, 'Gaussian RBF Kernel Parameter Range' 
  hubAMW_subframe, 'kernelIndex', /Row, Title='Linear Kernel'
  hubAMW_subframe, 'kernelIndex', /Row, Title='Gaussian RBF Kernel Parameter Range', /SetButton
  hubAMW_parameter, 'minG', Title='min(g)', Size=10, /Float, IsGT=0, Value=0.01
  hubAMW_parameter, 'maxG', Title=' max(g)', Size=10, /Float, IsGT=0, Value=1000.
  hubAMW_parameter, 'multiplierG', Title=' multiplier(g)', Size=10, /Float, IsGT=1, Value=10.
  hubAMW_subframe, /Column
  hubAMW_label, 'Regularization Parameter Range' 
  hubAMW_subframe, /Row
  hubAMW_parameter, 'minC', Title='   min(C)', Size=10, /Float, IsGT=0, Value=0.01
  hubAMW_parameter, 'maxC', Title=' max(C)', Size=10, /Float, IsGT=0, Value=1000.
  hubAMW_parameter, 'multiplierC', Title=' multiplier(C)', Size=10, /Float, IsGT=1, Value=10.
  hubAMW_subframe, /Column
  hubAMW_label, 'Cross Validation'
  hubAMW_parameter, 'numberOfFolds',      Title='   number of folds                          ', Size=3, /Integer, IsGE=3, Value=3
  hubAMW_label, 'Termination Criterion'
  hubAMW_parameter, 'stopGridSearch',     Title='   termination criterion for grid search    ', Size=10, /Float, IsGT=0, Value=0.1
  hubAMW_parameter, 'stopTraining',       Title='   termination criterion for final training ', Size=10, /Float, IsGT=0, Value=0.001
  if svmType eq 'svr' then begin
    hubAMW_label, 'Epsilon insensitive Loss Function Parameter'
    hubAMW_subframe, 'lossSelection', Title='automatic search', /ROW, /SetButton
    hubAMW_subframe, 'lossSelection', Title='user defined', /ROW
    hubAMW_parameter, 'lossUser',     Title='epsilon', Size=10, /Float, IsGE=0, Value='0.'
    hubAMW_subframe, /Column     
  endif
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputLabelFilename', Title=strupcase(svmType)+' Estimation',$
    Value=filepath(svmType+'Estimation', /TMP), TSize=tSize
  result = hubAMW_manage(/Flat)

  if result['accept'] then begin 
    parameters = result
    v1 = parameters.remove('minC')
    v2 = parameters.remove('maxC')
    v3 = parameters.remove('multiplierC')    
    exp1 = double(round(alog10(v1)/alog10(v3)))
    exp2 = double(round(alog10(v2)/alog10(v3)))
    vector = v3^(dindgen(exp2-exp1+1)+exp1)
    parameters['cArray'] = vector

    case parameters.remove('kernelIndex') of
      0 : vector = [0d]
      1 : begin
        v1 = parameters.remove('minG')
        v2 = parameters.remove('maxG')
        v3 = parameters.remove('multiplierG')
        exp1 = double(round(alog10(v1)/alog10(v3)))
        exp2 = double(round(alog10(v2)/alog10(v3)))
        vector = v3^(dindgen(exp2-exp1+1)+exp1)
      end
    endcase
    parameters['gArray'] = vector

    parameters['featureFilename'] = parameters.remove('sampleSet_featureFilename')
    parameters['labelFilename'] = parameters.remove('sampleSet_labelFilename')
    parameters['spectralSubset'] = parameters.remove('sampleSet_spectralSubset')
    
    parameters['svmType'] = svmType
    if svmType eq 'svr' then begin
      if parameters.remove('lossSelection') eq 1 then begin
        parameters['loss'] = parameters.remove('lossUser')
      endif
    endif

  endif else begin
    parameters = !NULL
  endelse

  return, parameters
end

pro test_svm_getParameters

  svmType = 'svr'
  parameters = svm_getParameters(svmType, Title='Support Vector Regression')
  print,parameters

end
