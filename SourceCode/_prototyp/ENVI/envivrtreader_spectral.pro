;fid: The file ID of the input image. (Note: you must use ENVI_FILE_QUERY to obtain the file information; you cannot currently use ENVIRasterToFID.)
;pos: The band positions for the desired spectra. POS is a long array with values ranging from 0 to 1, minus the number of bands. If POS is undefined, all the bands for the requested spectra are returned.
;xs: The x starting pixel for the spectral request (in file coordinates)
;xe: The x ending pixel for the spectral request (in file coordinates)
;y: The y line number for the spectral request (in file coordinates)
;spectra: The name of the variable in which the requested spectra are to be stored. The read routine must define this variable before the routine exits. If the spectra variable is not defined in the routine, ENVI will issue an error.
;_extra: You must specify this keyword in order to collect keywords not used by custom file readers. The use of _extra prevents errors when calling a routine with a keyword that is not listed in its definition.

pro EnviVRTReader_spectral, fid, pos, xs=xs, xe=xe, y=y, spectra=spectra, _extra=_extra
  compile_opt idl2

  ; open VRT as GDAL Dataset inside Python and cache new Datasets for for later usage
  fromidl = pyimport('hub.gdal.fromidl')
  envi_file_query, fid, FNAME=vrtfile
  common EnviVRTReader, datasets
  if ~isa(datasets) then datasets = hash()
  if ~datasets.hasKey(string(fid)) then begin
    datasets[string(fid)] = fromidl.openVRT(vrtfile)
  endif

  ; read image subset from the dataset
  dataset = datasets[string(fid)]
  spectra = fromidl.readCube(dataset, string(xs), string(y), string(xe-xs+1), string(1)) ; passing arguments as int did not work :-(
  spectra = spectra[*]
  print, spectra
  
end