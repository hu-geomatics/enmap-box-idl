pro ENVI4DCubeViewer_event, event

  compile_opt idl2
  widget_control, event.top,    GET_UVALUE=info
  
  if (event.id eq info.idtime) or (event.id eq info.idapply1) or (event.id eq info.idapply2) or (event.id eq info.idapply3) then begin
   ; if ~obj_valid(info.profile.window) then obj_destroy, info.profile.window
    widget_control, info.idtime, GET_VALUE=timeband
    widget_control, info.idred, GET_VALUE=r
    widget_control, info.idgreen, GET_VALUE=g
    widget_control, info.idblue, GET_VALUE=b
    widget_control, info.idtimelabel, SET_VALUE=(info.header.getMeta('timestamps'))[timeband]
    print, timeband

    rgb = [r,b,g]
    if  (event.id eq info.idapply3) then begin ; open all dates
      for timeband=0,info.header.getMeta('timebands')-1 do begin
        pos = rgb + info.header.getMeta('wavebands')*(timeband)
        envi_display_bands, info.fid, pos, WINDOW_STYLE=2, /NEW
      endfor
    endif else begin
      pos = rgb + info.header.getMeta('wavebands')*(timeband)
      new = (event.id eq info.idapply2) or ((envi_get_display_numbers())[0] eq -1)
      envi_display_bands, info.fid, pos, WINDOW_STYLE=0, NEW=new ;, SCROLL_REBIN=100
    endelse
    if obj_valid(info.profile.window) then ENVI4DCubeViewer_drawProfile, info
    widget_control, event.top, /INPUT_FOCUS
  endif
end

function ENVI4DCubeViewer_userPlot, x, profiles, margin, title, xtitle, _extra=extra
  nir = profiles[2,*]
  red = profiles[1,*]
  ndvi = float(nir-red)/float(nir+red)
  return, plot(x, ndvi, NAME='NDVI', MIN_VALUE=-1, MAX_VALUE=1, YRANGE=[-1,1], /CURRENT, /OVERPLOT, _extra = extra, MARGIN = margin, TITLE = title,XTITLE = xtitle)
end

pro ENVI4DCubeViewer_drawProfile, info
  widget_control, /HOURGLASS
  widget_control, info.idtext1, GET_VALUE=text1
  widget_control, info.idtext2, GET_VALUE=text2
  widget_control, info.idtime, GET_VALUE=timeband
  widget_control, info.idplot, GET_VALUE=showplot
  
  if ~obj_valid(info.profile.window) then begin
    info.profile.window = window(WINDOW_TITLE='Temporal Profiles')  
  endif

  if max(showplot) eq 0 then begin
    info.profile.window.SetCurrent
    info.profile.window.erase
    info.profile.window.refresh
    return
  endif
      
  info.profile.window.Refresh, /DISABLE
  info.profile.window.SetCurrent
  info.profile.window.erase
    
  !null = execute(strjoin(text1))
  extra = (hash(Global)+hash('MIN_VALUE',[], 'MAX_VALUE',[])).toStruct()

  flt = machar()
  !null = plot([0], /CURRENT, /NODATA)
  plots = list()
  profiles = info.profile.data
  timestamps = info.header.getMeta('timestamps')
  times = hubTimeUTC(timestamps+'Z')
  x = times.dyear
  for i=0,info.header.getMeta('wavebands')-1 do begin
    profile = profiles[i:-1:info.header.getMeta('wavebands')]

    !null = execute(strjoin(text2))
    extra = (hash(extraGlobal)+hash(extraLocal)).toStruct()
    if showplot[i] then begin
      plots.add, plot(x, profile, NAME=(info.header.getMeta('waveband names'))[i], /CURRENT, /OVERPLOT, $
                      _extra = extra,$
                      MARGIN=[0.175,0.15,0.05,0.1],$
                      TITLE = 'pixel location = '+strjoin(strtrim([info.xloc,info.yloc]+1,2),', '),$
                      XTITLE='acquisition time')
    endif
  endfor

  if showplot[info.header.getMeta('wavebands')-1+1] then begin
    plots.add, ENVI4DCubeViewer_userPlot(x, reform(profiles,info.header.getMeta('wavebands'),info.header.getMeta('timebands')), _extra=extraGlobal)
  endif
  
  
  ; plot bar for selected date
  ymin = +!VALUES.F_INFINITY
  ymax = -!VALUES.F_INFINITY
  foreach plot, plots do begin
    plot.GetData, dummy, values
    ymin <= min(values) > plot.min_value
    ymax >= max(values) < plot.max_value
  endforeach
  !null = plot([1,1]*x[timeband],[ymin, ymax], /CURRENT, /OVERPLOT, COLOR='red', TRANSPARENCY=50, THICK=10)
  !null = legend(TARGET=plots, $
    POSITION=[1,1], SHADOW=0, /RELATIVE, $
    /AUTO_TEXT_COLOR)
  info.profile.window.Refresh

end

pro ENVI4DCubeViewer

  compile_opt idl2
  widget_control, Default_font='ZAPFCHANCERY*14*PROOF*FIXED'

  ; is ENVI Classic already running?
  version = envi_query_version()

  ; select image
  envi_select, FID=fid, TITLE='Select 4D Data Cube Image', /NO_DIMS, /NO_SPEC, /FILE_ONLY
  if fid eq -1 then return
  
  ; query metadata
  envi_file_query, fid, NB=bands, FNAME=filename
  header = hubIOImg_getMeta(filename)
  
  ; query wavebands if not defined in header 
  if ~header.hasMeta('wavebands') then begin
    possibleWavebands = list()
    for timebands=1,bands do if (bands mod timebands) eq 0 then possibleWavebands.add, strtrim(bands/timebands,2), 0
  
    tlb = widget_auto_base(title='Select Number of Wavebands')
    !null = widget_menu(tlb, list=possibleWavebands, uvalue='id', /AUTO_MANAGE, /EXCLUSIVE)
    !null = widget_label(tlb, VALUE='Note: specify "waveband names = {<wl name1>,<wl name2>,...}"', /ALIGN_LEFT)
    !null = widget_label(tlb, VALUE='      inside header to avoid this dialog.', /ALIGN_LEFT)
    result = auto_wid_mng(tlb)
    if (result.accept eq 0) then return
    header.setMeta, 'wavebands', possibleWavebands[result.id]
  end

  if ~header.hasMeta('waveband names') then begin
    header.setMeta, 'waveband names', 'Band '+strtrim([1:header.getMeta('wavebands')],2)
  endif

  if ~header.hasMeta('timebands') then begin
    header.setMeta, 'timebands', bands/header.getMeta('wavebands')
   endif
   
;  if ~header.hasMeta('timestamps') then begin
;     header.setMeta, 'timestamps', 'acquisition #'+strtrim([1:header.getMeta('timebands')],2)
;  endif
    
  ; save infos and cache header for later usage inside ZPlotFunction
  info = dictionary()
  common Envi4DCubeViewer, infos
  if ~isa(infos) then infos = hash()
  infos[string(fid[0])] = info
  info.fid = fid[0]
  info.header = header

 ; build widget
  tlb = widget_base(TITLE='4D Cube Viewer', /ROW, UVALUE=info)
  tlbA = widget_base(tlb, /COLUMN)
  !null = widget_label(tlbA, VALUE='Image:     '+file_basename(filename), /ALIGN_LEFT)
  !null = widget_label(tlbA, VALUE='Wavebands: '+strtrim(header.getMeta('wavebands'),2), /ALIGN_LEFT)
  !null = widget_label(tlbA, VALUE='Timebands: '+strtrim(header.getMeta('timebands'),2), /ALIGN_LEFT)

  base = widget_base(tlbA, /ROW)
  info.idred = widget_menu(base, PROMPT=  'R:    ', DEFAULT_PTR=0, LIST=header.getMeta('waveband names'), /EXCLUSIVE)
  base = widget_base(tlbA, /ROW)
  info.idgreen = widget_menu(base, PROMPT='G:    ', DEFAULT_PTR=0, LIST=header.getMeta('waveband names'), /EXCLUSIVE)
  base = widget_base(tlbA, /ROW)
  info.idblue = widget_menu(base, PROMPT= 'B:    ',  DEFAULT_PTR=0, LIST=header.getMeta('waveband names'), /EXCLUSIVE)
  
  base = widget_base(tlbA, /ROW)
  info.idplot = widget_menu(base, PROMPT= 'Plot: ',  DEFAULT_PTR=[], LIST=[header.getMeta('waveband names'),'User-Plot'])
  
  base = widget_base(tlbA, /ROW)

  base = widget_base(tlbA, /ROW)
  info.idapply1 = widget_button(base, VALUE='Load to Current Display')
  info.idapply2 = widget_button(base, VALUE='Load to New Display')
  info.idapply3 = widget_button(base, VALUE='Open All')

  info.idtime = widget_slider(tlbA, minimum=0, maximum=header.getMeta('timebands')-1, value=0, scr_xsize=400, /DRAG, /SUPPRESS_VALUE)
  info.idtimelabel = widget_label(tlbA, VALUE=(header.getMeta('timestamps'))[0], /DYNAMIC_RESIZE, /ALIGN_CENTER)
    
  !null = widget_label(tlbA, VALUE='')
  if ~header.hasMeta('timestamps') then begin
    !null = widget_label(tlbA, VALUE='Note: specify "timestamps = {<2010-05-01>,<2010-06-30T12:30:55>,...}" inside the header.', /ALIGN_LEFT)
  endif

  t = list()
  t.add, 'extraGlobal = {'
  t.add, '  MIN_VALUE:0,'
  t.add, '  MAX_VALUE:3000,'
  t.add, '  LINESTYLE:0,'
  t.add, '  THICK:2,'
  t.add, '  SYM_SIZE:2,'
  t.add, '  SYM_THICK:2,'
  t.add, '  SYMBOL:"Diamond"
  t.add, '  }'
  value1 = t.toArray()

  t = list()
  t.add, 'extraLocal = {
  t.add, "  COLOR:(['green', 'red', 'blue', 'cyan'])[i]"
  t.add, '  }'
  value2 = t.toArray()
  tlbB = widget_base(tlb, /COLUMN)
  info.idtext1 = widget_text(tlbB, VALUE=value1, /EDITABLE, /SCROLL, SCR_XSIZE=400, SCR_YSIZE=150)
  info.idtext2 = widget_text(tlbB, VALUE=value2, /EDITABLE, /SCROLL, SCR_XSIZE=400, SCR_YSIZE=150)
 
  widget_control, /realize, tlb
  xmanager, 'ENVI4DCubeViewer', tlb, /no_block

  info.profile = dictionary()
  info.profile.window = obj_new()
  info.profile.xloc = 0
  info.profile.yloc = 0
  info.profile.data = 0
end