pro EnviVRTReader_spatial, unit, r_data, fid, band, xs, ys, xe, ye, SPECTRAL=spectral, _extra=_extra
  compile_opt idl2
    
  ; open VRT as GDAL Dataset inside Python and cache new Datasets for later usage
  fromidl = pyimport('hub.gdal.fromidl')
  envi_file_query, fid, FNAME=vrtfile
  common EnviVRTReader, datasets, header
  if ~isa(datasets) then datasets = hash()
  if ~datasets.hasKey(string(fid)) then begin
    datasets[string(fid)] = fromidl.openVRT(vrtfile)
  endif
  

  
  ;envi_file_query, fid, FNAME=vrtfile
  wavebands = 4
  waveband_stretch_min = [230,  150, 1300,  900]
  waveband_stretch_max = [875, 1150, 3900, 3000]
  waveband = band mod wavebands

  ; read image subset from the dataset
  skipOverview = 0
  trimData = 1
  if ~keyword_set(spectral) and skipOverview and ys eq ye then begin
    r_data = congrid([waveband_stretch_min[waveband],waveband_stretch_max[waveband]], xe-xs+1)
  endif else begin
    dataset = datasets[string(fid)]
    r_data = fromidl.readBand(dataset, string(band), string(xs), string(ys), string(xe-xs+1), string(ye-ys+1)) ; passing arguments as int did not work :-(
    if ~keyword_set(spectral) and trimData then begin
      r_data = (r_data > waveband_stretch_min[waveband]) < waveband_stretch_max[waveband]
    endif
   endelse
end
