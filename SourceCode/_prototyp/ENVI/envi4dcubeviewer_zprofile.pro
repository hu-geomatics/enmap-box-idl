function ENVI4DCubeViewer_zprofile, x, y, bbl, bbl_list, _extra=_extra
  
  p_data = scope_varfetch('p_data', LEVEL=-1)
  displayNumber = (*p_data).dn
  envi_disp_query, displayNumber, FID=fid
  disp_get_location, displayNumber, xloc, yloc
  
  common Envi4DCubeViewer, infos
  if ~isa(infos) || ~infos.hasKey(string(fid[0])) then begin
    ok = DIALOG_MESSAGE(/ERROR, 'Image is not managed by the 4d Cube Viewer!')
    return, y
  endif
  info = infos[string(fid[0])]
  info.xloc = xloc
  info.yloc = yloc
  info.profile.data = y
  ENVI4DCubeViewer_drawProfile, info
  return, y
end

