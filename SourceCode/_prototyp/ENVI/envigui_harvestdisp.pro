pro ENVIGUI_harvestWidgetStructure_event, event
  
end

function ENVIGUI_harvestDisp, EventPro=eventPro, UNAME=uname

  result = dictionary()

  common managed, ids, names, outermodal
  result.imageTLB = (ids[(where(/NULL, names eq 'disp'))])[0]
  result.imageMenu = (widget_info(result.imageTLB, /ALL_CHILDREN))[0]
  result.imageDraw = (widget_info(result.imageTLB, /ALL_CHILDREN))[1]

  result.imageEventPro = widget_info(result.imageDraw, /EVENT_PRO)
  widget_control, result.imageDraw,  EVENT_PRO=eventPro, SET_UNAME=uname


  return, result
end

pro test_ENVIGUI_harvestWidgetStructure
  common managed, ids, names, outermodal
  id = ids[0]
  widget_control, id, GET_UVALUE=uvalue
  (*uvalue).AUTO_RESET = 0
  
  ;find button for stretching
  stop
end