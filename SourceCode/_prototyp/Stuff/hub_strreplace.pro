function hub_strReplace, text, pattern, replaceText, FOLD_CASE=FOLD_CASE
  r = isa(replaceText)? replaceText : ''
  return, strjoin(strsplit(text, pattern, /REGEX, /Extract, FOLD_CASE=FOLD_CASE), r)
  

end

pro test_hub_strReplace
  
  text = 'Replace replace text matched by a regex.'
  print, hub_strReplace(text, 'Replace', 'Remove a', /FOLD_CASE)
  print, hub_strReplace(text, 'Replace')
  
end