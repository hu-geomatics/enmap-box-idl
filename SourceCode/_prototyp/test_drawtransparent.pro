pro test_drawTransparent

  size = [500,500]
  tlb = widget_base()
  widget_control, tlb, /REALIZE
  windowID = widget_draw(tlb, XSIZE=size[0], YSIZE=size[1], GRAPHICS_LEVEL=2 )
  widget_control, windowID, GET_VALUE=oWindow
  
  oView = IDLgrView(VIEWPLANE_RECT=[0,0,size], DIMENSIONS=size, LOCATION=[0,0])
  oModel = IDLgrModel()
  oImage = IDLgrImage(randomu(seed, size[0], size[1], 3, /LONG) mod 255, INTERLEAVE=2, DIMENSIONS=size, LOCATION=[0,0])
  
  x = [0,1,1,0]*size[0]/2+size[0]/4
  y = [0,0,1,1]*size[1]/2+size[1]/4
  oPolygon = IDLgrPolygon(x, y, FILL_PATTERN=IDLgrPattern(0), COLOR=[255,0,0], ALPHA_CHANNEL=0.5)

  oView.add, oModel
  oModel.add, oImage
  oModel.add, oPolygon
   
  oWindow.draw, oView
  
end