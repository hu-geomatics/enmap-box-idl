;+
; :Author: <author name> (<email>)
;-

function ShrinkToValidPixels_getDirname
  
  result = filepath('ShrinkToValidPixels', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_ShrinkToValidPixels_getDirname

  print, ShrinkToValidPixels_getDirname()
  print, ShrinkToValidPixels_getDirname(/SourceCode)

end
