;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`ShrinkToValidPixels_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
pro ShrinkToValidPixels_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputImages','prefix','outDir']
  
  if ~isa(settings) then settings = hash()
  
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressbar( $
          title=settings.hubGetValue('title', default='Shrink Image') $
        , GroupLeader=settings.hubGetValue('groupLeader') $
        , info='Please wait...')
    pBarProgress = 0l
  endif
  
  rootDir = parameters['outDir']
  FILE_MKDIR, rootDir
  
  prefix = parameters.hubGetValue('prefix')
  nImages = n_elements(parameters['inputImages'])
  outInfo = replicate({nSamples:0l $
                      ,nLines  :0l $
                      ,nBands  :0l $
                      ,nDataType:0b $
                      ,div:ptr_new() $
                      ,inputImage:obj_new() $
                      ,outputImage:obj_new() $
                      },nImages)
  foreach inPath, parameters['inputImages'], i do begin
    outPath = filepath(prefix + file_basename(inPath), ROOT_DIR=rootDir)
    inImg = hubIOImgInputImage(inPath)
    outImg = hubIOImgOutputImage(outPath)
    outImg.copyMeta, inImg, /COPYFILETYPE, /COPYLABELINFORMATION, /COPYSPECTRALINFORMATION
     
    outInfo[i].nSamples = inImg.getMeta('samples')
    outInfo[i].nLines = inImg.getMeta('lines')
    outInfo[i].nBands = inImg.getMeta('bands')
    outInfo[i].nDataType = inImg.getMeta('data type')
    div = inImg.isClassification() ? 0:inImg.getMeta('data ignore value')
    outInfo[i].div = ptr_new(div)
    outInfo[i].inputImage = inImg 
    outInfo[i].outputImage = outImg
  endforeach
  
  
  nSamples = outInfo[0].nSamples
  nLines   = outInfo[0].nLines
  if isa(where(outInfo[*].nSamples ne nSamples, /NULL)) then begin
    message, 'All files must have same number of samples'
  endif
  if isa(where(outInfo[*].nLines ne nLines, /NULL)) then begin
    message, 'All files must have same number of lines'
  endif  
  
  if isa(pBar) then begin
    pBar.setRange, [0, nLines]
  endif
  
  tileLines = hubIOImg_getTileLines(nSamples, max(outInfo[*].nBands), 'double')  

  ;init all readers and writers
  for i=0, nImages-1 do begin
    inImg = outInfo[i].inputImage
    outImg = outInfo[i].outputImage
    inImg.initReader, tileLines, /Slice, /TileProcessing
    outImg.initWriter,inImg.getWriterSettings()
  endfor
    
  ;write
  nPixelsTotal = 0ll
  while ~(outInfo[0].inputImage).tileProcessingDone() do begin
    inDataValues = MAKE_ARRAY(nImages, /PTR)
    ;read slice data
    for i=0, nImages-1 do begin
      inDataValues[i] = ptr_new((outInfo[i].inputImage).getData())
    endfor
  
    ;get mask
    nPixels = n_elements((*inDataValues[0])[0,*])
    mask = make_array(nPixels, /Byte, value=1)
    for i=0, nImages-1 do begin
      inData = (*inDataValues[i])
      if isa(outInfo[i].div) then begin
        div = *(outInfo[i].div)
        for b=0, n_elements(inData[*,0])-1 do begin
          mask and= inData[b,*] ne div
        endfor
      endif
    endfor
    
    ;apply mask
    iValid = where(mask ne 0, /NULL, nValid)
    if isa(iValid) then begin
      nPixelsTotal += nValid
      for i=0, nImages-1 do begin
        inData = (*inDataValues[i])
        (outInfo[i].outputImage).writeData, inData[*,iValid]
      endfor
    endif
    
    if isa(pBar) then begin
      pBarProgress += tileLines
      pBar.setProgress, pBarProgress
    endif
    
  endwhile
  print, nPixelsTotal

  ;set spatial dimensions, cleanup
  for i=0, nImages-1 do begin
    outImg = (outInfo[i].outputImage) 
    outImg.setMeta, 'samples', 1
    outImg.setMeta, 'lines',  nPixelsTotal
    outImg.cleanup
  endfor

  print, 'done!'
end

;+
; :Hidden:
;-
pro test_ShrinkToValidPixels_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  fileList = list()
  fileList.add, enmapBox_getTestImage('AF_Image')
  fileList.add, enmapBox_getTestImage('AF_LC_Training')
  
  parameters['inputImages'] = fileList
  parameters['prefix'] = 'shrink'
  parameters['outDir'] = 'D:\TestShrink\'
  ShrinkToValidPixels_processing, parameters, settings

end  
