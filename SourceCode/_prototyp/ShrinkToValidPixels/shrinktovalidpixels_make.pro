;+
; :Author: <author name> (<email>)
;-

pro ShrinkToValidPixels_make, Cleanup=cleanup, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc

  if keyword_set(cleanup) then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    
    appDir = ShrinkToValidPixels_getDirname()
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ; find source code and application directory  
  
  codeDir =  file_dirname((routine_info('ShrinkToValidPixels_make',/SOURCE)).path)
  appDir = ShrinkToValidPixels_getDirname()
  
  ; check lower case *.pro filenames (important for auto-compiling under unix systems
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  if ~keyword_set(CopyOnly) then begin
     ; save routines inside SAVE file
     
    SAVFile = filepath('ShrinkToValidPixels.sav', ROOT=appDir, SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, /NoLog
    
    if ~keyword_set(NoIDLDoc) then begin
      ; create IDLDOC documentation
      
      helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
    
      idldoc, ROOT=codeDir $
            , OUTPUT=helpOutputDir $
            , TITLE='ShrinkToValidPixels Documentation' $
            , SUBTITLE='remote sensing made easy' $
            , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') $
            , FORMAT_STYLE='rst' $
            , /COLOR_OUTPUTLOG $
            , /QUIET $
            , USER=1
    
      ; open documentation
      
      hubHelper.openFile, /HTML, filepath('index.html', ROOT_DIR=helpOutputDir)
    endif
  endif
end
