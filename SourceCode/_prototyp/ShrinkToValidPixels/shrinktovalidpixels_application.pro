;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `ShrinkToValidPixels_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `ShrinkToValidPixels_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `ShrinkToValidPixels_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro ShrinkToValidPixels_application, applicationInfo
  
  ; get global settings for this application
  settings = hash()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  settings['title'] = 'Shrink/Reduce/Remove masked pixels'
  parameters = ShrinkToValidPixels_getParameters(settings)
  
  if parameters['accept'] then begin
  
    ShrinkToValidPixels_processing, parameters, settings
    
  endif
  
end

;+
; :Hidden:
;-
pro test_ShrinkToValidPixels_application
  applicationInfo = Hash('title','Shrink!')
  ShrinkToValidPixels_application, applicationInfo

end  
