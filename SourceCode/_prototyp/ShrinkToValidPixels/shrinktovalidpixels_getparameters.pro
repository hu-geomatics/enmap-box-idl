function ShrinkToValidPixels_getParametersCheck, results, message=message
  msg =[]
  
  imgA = !NULL
  
  foreach imgPath, results['inputImages'] do begin
    if ~isa(imgA) then imgA = hubIOImgInputImage(imgPath)
    imgB = hubIOImgInputImage(imgPath)
    if ~imgA.iscorrectSpatialSize(imgB.getSpatialSize()) then begin
        pathShort = file_basename(imgPath)
        msg = [msg, 'wrong spatial dimensions:'+pathShort]
    endif 
  endforeach
  
  message = msg
  return, ~isa(msg)
end

function ShrinkToValidPixels_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  imageFileList = hubProEnvHelper.getFileNames(/Image)
  if ~isa(imageFileList) then message, 'Please add images to your file list first'
  fileBaseNames = FILE_BASENAME(imageFileList)
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_list, 'inputImages', Title='Images' $
             , XSIZE=400, YSIZE = n_elements(imageFileList) * 15 < 300 $
             , list=fileBaseNames, extract=imagefileList $
             , /Multipleselection
  hubAMW_parameter, 'prefix', title='File Prefix', value='small', /String
  hubAMW_outputDirectoryName, 'outDir', Title='OutputDirectory'
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='ShrinkToValidPixels_getParametersCheck')
  return, parameters
end

;+
; :Hidden:
;-
pro test_ShrinkToValidPixels_getParameters

  ; test your routine
  settings = hash('title','Title')
  parameters = ShrinkToValidPixels_getParameters(settings)
  print, parameters

end  
