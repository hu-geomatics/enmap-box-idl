pro test_drawPoint

  size = [500,500]
  tlb = widget_base()
  widget_control, tlb, /REALIZE
  windowID = widget_draw(tlb, XSIZE=size[0], YSIZE=size[1], GRAPHICS_LEVEL=2 )
  widget_control, windowID, GET_VALUE=oWindow
  
  oView = IDLgrView(VIEWPLANE_RECT=[0,0,size/10], DIMENSIONS=size, LOCATION=[0,0])
  oModel = IDLgrModel()
  oSymbolData = IDLgrPolygon([-1,1,1,-1],[-1,-1,1,1], COLOR=[255,0,0], ALPHA_CHANNEL=0.5)
  oSymbol = IDLgrSymbol(oSymbolData, Size=0.5)
  oPoints = IDLgrROI(randomu(seed, 1000, /LONG) mod size[0], randomu(seed, 1000, /LONG) mod size[1], STYLE=0, Symbol=oSymbol, LINESTYLE=6)
  oView.add, oModel
  oModel.add, oPoints
  oWindow.draw, oView
  
end