function minibug_featuresetCheck, results, message=message
  msg = !NULL
  
  print, results
  
  
  ;warum kein stop durch impliziten consistency check
  ;wenn kein Image ausgewählt wurde? 
  ;eigentlich sollte er gar nicht in minibug_featuresetCheck reingehen, oder?
  stop

  return, ~isa(msg)
end

pro minibug_featureset
   hubAMW_program , Title = 'SampleSet ohne Auswahl'
   hubAMW_frame, title='Frame'
   hubAMW_inputSampleSet, 'inputSampleSet' $
          , Title          ='Image    ' $
          , REFERENCETITLE ='Reference' $
          , /REGRESSION
   parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='minibug_featuresetCheck')
   print, parameters
end