pro test_gridsearchplot
  
  ;input
  Matrix = findgen(10, 15)
  cArray = indgen(n_elements(Matrix[*,0]))
  gArray = indgen(n_elements(Matrix[*,0]))
  
  
  ;derived from input
  nC = n_elements(cArray)
  nG = n_elements(gArray)
  
  xvec = findgen(nC+1)/(nC+1)
  yvec = findgen(nG+1)/(nG+1)
  colorMatrix = 255 - BYTSCL(Matrix)
  
  grWindow = IDLgrWindow(Retain=1)
  grView   = IDLgrView(VIEWPLANE_RECT=[-0.5, -0.5, 1.5, 1.5])
  grModel  = IDLgrModel() 
  
  
  ;add polygons
  foreach c, cArray, iC do begin
    foreach g, gArray, ig do begin
      ixvec = iC + [0,1,1,0]
      iyvec = iG + [0,0,1,1]
      grModel.add, IDLgrPolygon(xvec[ixvec], yvec[iyvec], COLOR=colorMatrix[iC, ig] * [1,1,1])
    endforeach
  endforeach
  ;rectangle around plot
  grModel.add, IDLgrPolyLine(xvec[[0,nC,nC,0,0]],yvec[[0,0,nG,nG,0]], color=[255,0,0],thick=3)
  
  
  ;axe descriptions
  foreach c, cArray, iC do begin
    grModel.add, IDLgrText(strtrim(c), LOCATION=[xvec[iC], -0.08], COLOR=[0,0,0])
  endforeach
;    
;  ;add grid lines
;  for iC = 0, nC do begin
;    grModel.add, IDLgrPolyline([iC,iC],[0, nG] , COLOR=[0,0,0], thick=4)
;  endfor
;  
  
  
    grView.add, grModel
    grWindow.Draw, grView

end