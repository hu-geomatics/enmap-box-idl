pro test_maskextraction
  inputFilename = hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth')
  inputImage = hubIOImgInputImage(inputFilename)
  outputFilename = filepath('my_image', /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)
  numberOfTileLines = hubIOImg_getTileLines(image=inputImage)
  inputImage.initReader, numberOfTileLines, /Cube, /TileProcessing, /Mask
  writerSettings = inputImage.getWriterSettings(SetBands=1)
  outputImage.initWriter, writerSettings
  
    while ~inputImage.tileProcessingDone() do begin          
      cubeTile = inputImage.getData()                        
      maskTile = product(cubeTile, 3)  
      outputImage.writeData, maskTile                      
    endwhile
  
  inputImage.cleanup
  outputImage.cleanup
  
  ; show result
  (hubIOImgInputImage(outputFilename)).quickLook
end