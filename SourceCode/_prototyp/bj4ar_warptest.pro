pro bj4ar_warptest
  root = 'T:\4AR\'
  inImages = file_search(root+'L8', '*B4*')
  gdal = hubGDALWrapper(/SHOWCOMMANDS)
  enviFiles= list()
  indexFiles = list()
  foreach inImg, inImages do begin
    bn = FILE_BASENAME(inImg, '.tif', /FOLD_CASE)
    outENVI = FILEPATH(bn+'.bsq', ROOT_DIR=root, SUBDIRECTORY=['ENVI'])
    if ~file_test(outENVI) then begin
      FILE_MKDIR, FILE_DIRNAME(outENVI)
      gdal.gdal_translate, inImg, outENVI, OF='ENVI'
    endif
    enviFiles.add, outENVI
  endforeach
  
  foreach file, enviFiles do begin
    img = hubIOImgInputImage(file)
    bn = FILE_BASENAME(file, '.bsq')
    pathOut = FILEPATH(bn+'_idx.bsq', ROOT_DIR=root, SUBDIRECTORY=['Index'])
    indexFiles.add, pathOut
    if 1b || ~file_test(pathOut) then begin
      print, 'write '+pathOut+'...'
      ns = img.getMeta('samples')
      nl = img.geTMeta('lines')
      mpaInfo = img.getMeta('map info')
      data = ULINDGEN(ns, nl)+1
      FILE_MKDIR, FILE_DIRNAME(pathOut)
      outImg = hubIOImgOutputImage(pathOut)
      outImg.copyMeta, img,COPYSPATIALINFORMATION=1
      outImg.setMeta, 'compression', 1
      outImg.writeImage, data 
    endif
  endforeach
  
  
  foreach file, indexFiles, i do begin
    srs = '"'+strjoin(strtrim(gdal.gdalSRSInfo(file, o1='wkt_simple'),2))+'"'
    if i eq 0 then begin
      refSRS = srs
      continue
    endif 
     
    if refSRS ne srs then begin
        bn = FILE_BASENAME(file, '.bsq')
        dn = FILE_DIRNAME(file)
        pathDst= FILEPATH(bn+'_warped.bsq', root=dn)
        gdal.gdalwarp, file, pathDst, T_SRS=refSRS, r1='near', of_ = 'ENVI', /OVERWRITE
    endif
      
  endforeach
  
  
  print, 'Done'


end