pro prototyp_dataToR

  filenameFeatures = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  sampleSet = hubIOImgSampleSet(filenameFeatures, filenameLabels)
  sample = sampleSet.getSample()
  sampleSet.cleanup
  
  parameters = hash()
  parameters['features'] = sample.features
  parameters['labels'] = fix(sample.labels)

  help, parameters['features']
  help, parameters['labels']

  code = list()
;  code.add, 'print("Labels")'
;  code.add, 'print(p$labels)'
;  code.add, 'print("Features")'
;  code.add, 'print(p$features)'

;  # Simulate data set
  code.add, 'features1 <- runif(100, 500, 5000)
  code.add, 'features2 <- (0.85*features1+20)+rnorm(100, mean=0, sd=1000)
  code.add, 'labels <- sample(1:9, 100, replace=TRUE)

;  # Plot scatterplot
  code.add, 'plotFilename <- "D:/plot.tiff"
  code.add, 'tiff(plotFilename, res=300, height=1500, width=1500)
  code.add, 'plot(features1, features2, col=labels, pch=labels)
  code.add, 'dev.off()'

  code = code.toArray()
  hubR_runCode, code, parameters
  hubHelper.openFile, 'D:\plot.tiff'

end