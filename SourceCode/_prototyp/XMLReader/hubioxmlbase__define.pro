function hubIOXMLBase::init, filename
  
  self.filename = filename
  self.doc = IDLffXMLDOMDocument(filename=filename,schema_checking=0)
  return, 1
  
end
function hubIOXMLBase::_getTokenDefinitions
  h = list()
  h.add, ['GENERAL_COMP','(=|!=|<|<=|>|>=)']
  h.add, ['VALUE_COMP','(eq|ne|lt|le|gt|ge)']
  h.add, ['SLASHSLASH','//']
  h.add, ['SLASH','/']
  h.add, ['AT','@']
  h.add, ['DOLLAR','\$']
  h.add, ['COMMA',',']
  h.add, ['LPAREN','\(']
  h.add, ['RPAREN','\)']
  h.add, ['LBRACKET','\[']
  h.add, ['RBRACKET','\]']
  h.add, ['QM','\?']
  h.add, ['DOTDOT','\.\.']
  h.add, ['DOT','\.']
  h.add, ['STAR','\*']
  h.add, ['FOR','for']
  h.add, ['IN','in']
  h.add, ['RETURN','return']
  h.add, ['SOME','some']
  h.add, ['EVERY','every']
  h.add, ['SATISFIES','satisfies']
  h.add, ['IF','if']
  h.add, ['THEN','then']
  h.add, ['ELSE','else']
  h.add, ['AND','and']
  h.add, ['OR','or']
  h.add, ['PLUS','plus']
  h.add, ['MINUS','minus']
  h.add, ['TO','to']
  h.add, ['UNION','union']
  h.add, ['INTERSECT','intersect']
  h.add, ['EXCEPT','except']
  h.add, ['DIV','div']
  h.add, ['IDIV','idiv']
  h.add, ['MOD','mode']
  h.add, ['INSTANCE','instance']
  h.add, ['OF','of']
  h.add, ['AS','as']
  h.add, ['IS','is']
  h.add, ['TREAT','treat']
  
  h.add, ['CAST','cast']
  
  h.add, ['CASTABLE','castable']
  h.add, ['CHILD','child::']
  h.add, ['DESCENDANT','descendant::']
  h.add, ['ATTRIBUTE','attribute::']
  h.add, ['SELF','self::']
  h.add, ['DESCENDANT_OR_SELF','descendant-or-self::']
  h.add, ['FOLLOWING_SIBLING','following_sibling::']
  h.add, ['NAMESPACE','namespace::']
  ;reverse axis specifiers
  h.add, ['PARENT','parent']
  h.add, ['ANCESTOR','ancestor']
  h.add, ['PRECEDING_SIBLING','preceding-sibling']
  h.add, ['PRECEDING','preceding']
  h.add, ['ANCESTOR_OR_SELF','ancestor-or-self']
  h.add, ['EMPTY_SEQUENCE','empty-sequence']
  h.add, ['ITEM','item']
  h.add, ['NODE','node']
  h.add, ['DOCNODE','document-node']
  h.add, ['TEXT','text']
  h.add, ['COMMENT','comment']
  h.add, ['PI','processing-instruction']
  h.add, ['SCHEMA_ATTR','schema-attribute']
  h.add, ['SCHEMA_ELEM','schema-element']
  h.add, ['ELEMENT','element']
  h.add, ['LCHEVRON','<<']
  h.add, ['RCHEVRON','>>']
  h.add, ['WS', '( |'+strjoin(string([9b,10b,13b,17b]),'|')+')+']
  
  h.add, ['WORD', '[a-z][a-z0-9]*']
  h.add, ['FLOAT', '[+-]?(\.[0-9]+|[0-9]+\.[0-9]*)']
  h.add, ['INTEGER', '[+-]?[0-9]+']
  return, h
end

function hubIOXMLBase::_eatText, textToRemove, expression
  return, strmid(expression, strlen(textToRemove))
end

function hubIOXMLBase::_tokenizer, xpathExpression
  ;see http://www.antlr.org/grammar/1264460091565/XPath2.g for an XPath 2.0 Grammar
  ex = xpathExpression
  tokenDefs = self._getTokenDefinitions()
  tokenStream = List()
  
  token = !NULL
  while strlen(ex) gt 0 do begin
    
    foreach tdef, tokenDefs do begin
      regex = '^'+tdef[1]
      textValue = stregex(ex, regex, /Extract, /FOLD_CASE)
      if textValue ne '' then begin
        token = {hubIOXPathToken, type:tdef[0], value:textValue}
        tokenStream.add, token
        ex = self._eatText(textValue, ex)
        break
      endif      
    endforeach
    if ~isa(token) then begin
      stop
    endif    
  endwhile
  tokenStream.add, {type:'EOF'}
  return, tokenStream
end

function hubIOXMLBase::_prepareExpression, xpathExpression
  tokenStream = self._tokenizer()
  
end

;grammar taken from http://www.w3.org/TR/xpath/
;XML Path Language (XPath)
;Version 1.0
;W3C Recommendation 16 November 1999

;  ## Location Paths
;  [1]     LocationPath     ::=    RelativeLocationPath  
;  | AbsoluteLocationPath  
function hubIOXMLBase::_IsLocationPath, ts
  return, self._isRelativeLocationPath(ts) || self._isAbsoluteLocationPath(ts)
end 

;  [2]     AbsoluteLocationPath     ::=    '/' RelativeLocationPath? 
;  | AbbreviatedAbsoluteLocationPath 
function hubIOXMLBase::_isAbsoluteLocationPath, ts
  if self._isToken(ts, 'SLASH') && self._isRelativeLocationPath(ts[1:*]) then return, 1b
  if self._isToken(ts, 'SLASH') then return, 1b
  if self._AbbreviatedAbsoluteLocationPath(ts) then return, 1b
  return, 0b 
end 

;  [3]     RelativeLocationPath     ::=    Step  
;  | RelativeLocationPath '/' Step 
;  | AbbreviatedRelativeLocationPath 
;  
;  ## Location Steps
;  
;  [4]     Step     ::=    AxisSpecifier NodeTest Predicate* 
;  | AbbreviatedStep 
;  [5]     AxisSpecifier    ::=    AxisName '::' 
;  | AbbreviatedAxisSpecifier
;  
;  ## Axes
;  
;  [6]     AxisName     ::=    'ancestor'  
;  | 'ancestor-or-self'  
;  | 'attribute' 
;  | 'child' 
;  | 'descendant'  
;  | 'descendant-or-self'  
;  | 'following' 
;  | 'following-sibling' 
;  | 'namespace' 
;  | 'parent'  
;  | 'preceding' 
;  | 'preceding-sibling' 
;  | 'self'  
;  
;  ## Node Tests
;  [7]     NodeTest     ::=    NameTest  
;  | NodeType '(' ')'  
;  | 'processing-instruction' '(' Literal ')'  
;  
;  ## Predicates
;  
;  [8]     Predicate    ::=    '[' PredicateExpr ']' 
;  [9]     PredicateExpr    ::=    Expr  
;  
;  ## Abbreviations
;  
;  [10]    AbbreviatedAbsoluteLocationPath    ::=    '//' RelativeLocationPath 
;  [11]    AbbreviatedRelativeLocationPath    ::=    RelativeLocationPath '//' Step  
;  [12]    AbbreviatedStep    ::=    '.' 
;  | '..'  
;  [13]    AbbreviatedAxisSpecifier     ::=    '@'?
;  
;  ## Basics 
;  
;  [14]    Expr     ::=    OrExpr  
;  [15]    PrimaryExpr    ::=    VariableReference 
;  | '(' Expr ')'  
;  | Literal 
;  | Number  
;  | FunctionCall  
;  
;  ## Function Calls
;  
;  [16]    FunctionCall     ::=    FunctionName '(' ( Argument ( ',' Argument )* )? ')'  
;  [17]    Argument     ::=    Expr
;  
;  ## Node Sets
;  
;  [18]    UnionExpr    ::=    PathExpr  
;  | UnionExpr '|' PathExpr  
;  [19]    PathExpr     ::=    LocationPath  
;  | FilterExpr  
;  | FilterExpr '/' RelativeLocationPath 
;  | FilterExpr '//' RelativeLocationPath  
;  [20]    FilterExpr     ::=    PrimaryExpr 
;  | FilterExpr Predicate  
;  
;  ## Booleans
;  
;  [21]    OrExpr     ::=    AndExpr 
;  | OrExpr 'or' AndExpr 
;  [22]    AndExpr    ::=    EqualityExpr  
;  | AndExpr 'and' EqualityExpr  
;  [23]    EqualityExpr     ::=    RelationalExpr  
;  | EqualityExpr '=' RelationalExpr 
;  | EqualityExpr '!=' RelationalExpr  
;  [24]    RelationalExpr     ::=    AdditiveExpr  
;  | RelationalExpr '<' AdditiveExpr 
;  | RelationalExpr '>' AdditiveExpr 
;  | RelationalExpr '<=' AdditiveExpr  
;  | RelationalExpr '>=' AdditiveExpr  
;  
;  ## Numeric Expressions
;  
;  [25]    AdditiveExpr     ::=    MultiplicativeExpr  
;  | AdditiveExpr '+' MultiplicativeExpr 
;  | AdditiveExpr '-' MultiplicativeExpr 
;  [26]    MultiplicativeExpr     ::=    UnaryExpr 
;  | MultiplicativeExpr MultiplyOperator UnaryExpr 
;  | MultiplicativeExpr 'div' UnaryExpr  
;  | MultiplicativeExpr 'mod' UnaryExpr  
;  [27]    UnaryExpr    ::=    UnionExpr 
;  | '-' UnaryExpr
;  
;  ## Expression Lexical Structure
;  
;  [28]    ExprToken    ::=    '(' | ')' | '[' | ']' | '.' | '..' | '@' | ',' | '::' 
;  | NameTest  
;  | NodeType  
;  | Operator  
;  | FunctionName  
;  | AxisName  
;  | Literal 
;  | Number  
;  | VariableReference 
;  [29]    Literal    ::=    '"' [^"]* '"' 
;  | "'" [^']* "'" 
;  [30]    Number     ::=    Digits ('.' Digits?)? 
;  | '.' Digits  
;  [31]    Digits     ::=    [0-9]+  
;  [32]    Operator     ::=    OperatorName  
;  | MultiplyOperator  
;  | '/' | '//' | '|' | '+' | '-' | '=' | '!=' | '<' | '<=' | '>' | '>=' 
;  [33]    OperatorName     ::=    'and' | 'or' | 'mod' | 'div'  
;  [34]    MultiplyOperator     ::=    '*' 
;  [35]    FunctionName     ::=    QName - NodeType  
;  [36]    VariableReference    ::=    '$' QName 
;  [37]    NameTest     ::=    '*' 
;  | NCName ':' '*'  
;  | QName 
;  [38]    NodeType     ::=    'comment' 
;  | 'text'  
;  | 'processing-instruction'  
;  | 'node'  
;  [39]    ExprWhitespace     ::=    S


;+
; :Hidden:
;-
pro hubIOXMLBase__define
  struct = {hubIOXMLBase $
    ,inherits IDL_Object $
    ,doc:obj_new() $
    ,filename:'' $
    ,filelines: 0l $
  }
end