
pro printNodes, node
    
    
    data = ''
    firstChild = node.getFirstChild()
    if isa(firstChild) && firstChild.getNodeType() eq 3 then begin
      text = firstChild.getNodeValue()
      isEmpty = '^[ '+string(9b)+string(10b)+']+$'
      if ~stregex(text, isEmpty, /Boolean) then data = text
    
    endif
    
 
    
    print, typename(node), ' ', node.getNodeName(), ' ', data
    if ~node.hasChildNodes() then begin
      return
    endif
    childs = node.getChildNodes()
    nChilds = childs.getLength()
    for i=0, nChilds-1  do begin
        child = childs.item(i)
        
        if child.getNodeType() ne 3 then printNodes, child
        
    endfor
  ;endelse
end

pro readerSandBox

  path = '/Users/benjaminjakimow/Documents/SVNCheckouts/EnMAP-Box/SourceCode/_prototyp/XMLReader/testXML.xml'
  doc = IDLffXMLDOMDocument(filename=path)
  
  
  node = doc.getDocumentElement()
  printNodes, node
  
  res = doc.getElementsByTagName('title')  
  for i=0, res.GetLength() -1 do begin
    
    node = res.item(i)
    
    child1 = node.getFirstChild()
    childs = node.getChildNodes()
    child2 = childs.item(0)
    
    print, 'tname: ', TYPENAME(node)
    print, 'name : ',node.getNodeName()
    print, 'value : ',(node.getFirstChild()).getNodeValue()
    print, 'value2: ',child1.getNodeValue()
    print, 'value3: ',child2.getNodeValue()
    
    print, 'type : ',node.getNodeType()
    print, 'attr : ',node.getAttributes()
    print, 'has  : ',node.hasChildNodes()
    
  
    stop
  endfor
  
  print, 'done'
  ;reader = hubIOXMLReader(path)
  ;print, reader.getData, ''
  ;reader.cleanup

end