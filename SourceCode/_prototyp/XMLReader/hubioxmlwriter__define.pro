function hubIOXMLWriter::init, filename
  
  self.filename = filename
  self.doc = IDLffXMLDOMDocument(filename=filename,schema_checking=0, )
          
  return, 1
end




pro hubIOXMLWriter::setActiveNode

end

pro hubIOXMLWriter__define
  struct = {hubIOXMLWriter $
    ,inherits IDL_Object $
    ,doc:obj_new() $
    ,filename:'' $
    ,filelines: 0l $
    ,activeNode:'' $
  }
end