
function hubIOXMLReader::init, filename
  
  if ~(FILE_INFO(filename)).exists then begin
    message, 'File does not exists: '+filename
  endif
  
  self.recentExpression = ''
  self.filename = filename
  self.doc = IDLffXMLDOMDocument(filename=filename,schema_checking=0)
  return, 1
  
end

function hubIOXMLReader::getTokenDefinitions
  h = list()
  h.add, ['GENERAL_COMP','(=|!=|<|<=|>|>=)']
  ;h.add, ['OPERATOR','(=|!=|<|<=|>|>=)']
  h.add, ['VALUE_COMP','(eq|ne|lt|le|gt|ge)']
  h.add, ['SLASHSLASH','//']
  h.add, ['SLASH','/']
  h.add, ['AT','@']
  h.add, ['DOLLAR','\$']
  h.add, ['COMMA',',']
  h.add, ['LPAREN','\(']
  h.add, ['RPAREN','\)']
  h.add, ['LBRACKET','\[']
  h.add, ['RBRACKET','\]']
  h.add, ['QM','\?']
  h.add, ['DOTDOT','\.\.']
  h.add, ['DOT','\.']
  h.add, ['STAR','\*']
  h.add, ['FOR','for']
  h.add, ['IN','in']
  h.add, ['RETURN','return']
  h.add, ['SOME','some']
  h.add, ['EVERY','every']
  h.add, ['SATISFIES','satisfies']
  h.add, ['IF','if']
  h.add, ['THEN','then']
  h.add, ['ELSE','else']
  h.add, ['AND','and']
  h.add, ['OR','or']
  h.add, ['PLUS','plus']
  h.add, ['MINUS','minus']
  h.add, ['TO','to']
  h.add, ['UNION','union']
  h.add, ['INTERSECT','intersect']
  h.add, ['EXCEPT','except']
  h.add, ['DIV','div']
  h.add, ['IDIV','idiv']
  h.add, ['MOD','mode']
  h.add, ['INSTANCE','instance']
  h.add, ['OF','of']
  h.add, ['AS','as']
  h.add, ['IS','is']
  h.add, ['TREAT','treat']
  
  h.add, ['CAST','cast']
  
  h.add, ['CASTABLE','castable']
  h.add, ['CHILD','child::']
  h.add, ['DESCENDANT','descendant::']
  h.add, ['ATTRIBUTE','attribute::']
  h.add, ['SELF','self::']
  h.add, ['DESCENDANT_OR_SELF','descendant-or-self::']
  h.add, ['FOLLOWING_SIBLING','following_sibling::']
  h.add, ['NAMESPACE','namespace::']
  ;reverse axis specifiers
  h.add, ['PARENT','parent']
  h.add, ['ANCESTOR','ancestor']
  h.add, ['PRECEDING_SIBLING','preceding-sibling']
  h.add, ['PRECEDING','preceding']
  h.add, ['ANCESTOR_OR_SELF','ancestor-or-self']
  h.add, ['EMPTY_SEQUENCE','empty-sequence']
  h.add, ['ITEM','item']
  h.add, ['NODE','node']
  h.add, ['DOCNODE','document-node']
  h.add, ['TEXT','text']
  h.add, ['COMMENT','comment']
  h.add, ['PI','processing-instruction']
  h.add, ['SCHEMA_ATTR','schema-attribute']
  h.add, ['SCHEMA_ELEM','schema-element']
  h.add, ['ELEMENT','element']
  h.add, ['LCHEVRON','<<']
  h.add, ['RCHEVRON','>>']
  h.add, ['WS', '( |'+strjoin(string([9b,10b,13b,17b]),'|')+')+']
  
  h.add, ['LITERAL', '"[^"]*"']
  h.add, ['WORD', '[a-z][a-z0-9]*']
  h.add, ['FLOAT', '[+-]?(\.[0-9]+|[0-9]+\.[0-9]*)']
  h.add, ['INTEGER', '[+-]?[0-9]+']
  return, h
end


function hubIOXMLReader::_eat, textToRemove, expression
  return, strmid(expression, strlen(textToRemove))
end

function hubIOXMLReader::tokenizer, expression
  ;see http://www.antlr.org/grammar/1264460091565/XPath2.g for an XPath 2.0 Grammar
  ex = expression
  tokenDefs = self.getTokenDefinitions()
  tokenList = List()
  
  token = !NULL
  while strlen(ex) gt 0 do begin
    
    foreach tdef, tokenDefs do begin
      regex = '^'+tdef[1]
      value = stregex(ex, regex, /Extract, /FOLD_CASE)
      if value ne '' then begin
        ex = self._eat(value, ex)
        ;special case LITERAL
        if tdef[0] eq 'LITERAL' then begin
          value = strjoin(strsplit(value, '"', /Extract))
        endif
        token = {hubIOXPathToken, type:tdef[0], value:value}
        tokenList.add, token
        break
      endif      
    endforeach
    if ~isa(token) then begin
      stop
    endif    
  endwhile
  tokenList.add, {type:'EOF'}
  return, tokenList
end

function hubIOXMLReader::selectAttributeNodes, nodeSet
  newNodeSet = list()
  case typename(nodeSet) of
     'IDLFFXMLDOMNAMEDNODEMAP' : begin
          for i=0, nodeSet.getLength() do begin
                node = nodeSet.Item(i)            
                newNodeSet.add, node            
          endfor
      end
     'IDLFFXMLDOMNODELIST' : begin
          for i=0, nodeSet.getLength() do begin
            node = nodeSet.Item(i)            
            newNodeSet.add, self.selectAttributeNodes(node), /Extract            
          endfor
      end
     'LIST' : begin
        foreach node, nodeSet, i do begin
          newNodeSet.add, self.selectAttributeNodes(node), /Extract
        endforeach
      end
      'IDLFFXMLDOMELEMENT' : begin
        ;a single node
       newNodeSet.add, nodeSet.getAttributes()
      end
      else : stop
  endcase
  return, newNodeSet
end

function hubIOXMLReader::selectNodesByAttribute, nodeSet, attribute, attributeValue

  newNodeSet = list()
  case typename(nodeSet) of
     'IDLFFXMLDOMNODELIST' : begin
          for i=0, nodeSet.getLength() do begin
            node = nodeSet.Item(i)
            newNodeSet.add, self.selectNodesByAttribute(node, attribute, attributeValue), /Extract
          endfor
      end
     'LIST' : begin
        foreach node, nodeSet, i do begin
          newNodeSet.add, self.selectNodesByAttribute(node, attribute, attributeValue), /Extract
        endforeach
      end
      
      else : begin
         ;a single node : check if node contains attribute
         ;self.printNode, nodeSet
         attributes = nodeSet.getAttributes()
         for i=0, attributes.getLength() - 1 do begin
            attr = attributes.item(i)
            if attr.getNodeName() ne attribute then continue
            if isa(attributevalue) && attr.getNodeValue() ne attributevalue then continue
            newNodeSet.add, nodeSet
         endfor  
      end
  endcase
  return, newNodeSet
end

function hubIOXMLReader::selectNodesByPredicate, nodeSet, predicateExpression
  p = list(predicateExpression, /Extract)
  newNodeSet = list()
  while n_elements(p) gt 0 do begin
    token = p[0]
    case 1b of 
      token.type eq 'INTEGER' : begin
            newNodeSet = self.selectNodesByPosition(nodeSet, fix(p[0].value))
            p.remove, 0
      end
      self._isMatch(p, ['AT','WORD','GENERAL_COMP','LITERAL'] $
           , matchValues = List(!NULL, !NULL, '=', !NULL)) : begin
           newNodeSet = self.selectNodesByAttribute(nodeSet, p[1].value, p[3].value)
           p.remove, [0,1,2,3]
      end
      self._isMatch(p, ['AT','WORD']) : begin
            newNodeSet = self.selectNodesByAttribute(nodeSet, p[1].value)
            p.remove, [0,1]
      end
       
      else : begin
              message, 'Evaluation of predicate "'+self._tokenStream2String(predicateExpression) +'" is not supported'
             end
      
    endcase
  endwhile
  return, newNodeSet
end

function hubIOXMLReader::selectNodesByName, nodeSet, name
  newNodeSet = List()
  case typename(nodeSet) of
     'IDLFFXMLDOMNODELIST' : begin
          for i=0, nodeSet.getLength() do begin
            node = nodeSet.Item(i)
            if node.getNodeName() eq name then newNodeSet.add, node
          endfor
      end
     'LIST' : begin
        foreach node, nodeSet, i do begin
          if node.getNodeName() eq name then newNodeSet.add, node
        endforeach
      end
      
      else : begin
       ;a single node
       if nodeSet.getNodeName() eq name then newNodeSet.add, nodeSet
        
             end
  
  endcase
  return, newNodeSet
end

function hubIOXMLReader::selectNodesByPosition, nodeSet, positions
  newNodeSet = List()
  if min(positions) le 0 then message, 'node position numbers must be greater than zero'
  
  case typename(nodeSet) of
    'IDLFFXMLDOMNODELIST' : begin
          for i=0, nodeSet.getLength() do begin
            if total(positions eq i+1) ne 0 then newNodeSet.add, nodeSet.Item(i)
          endfor
      end
     'LIST' : begin
        foreach node, nodeSet, i do begin
          if total(positions eq i+1) ne 0 then newNodeSet.add, node
        endforeach
      
      end
      else : begin
       ;a single node
       if total(positions eq 1) ne 0 then newNodeSet.add, nodeSet
        
             end
  endcase
  return, newNodeSet
end

;+
; :Description:
;    Returns all subnodes of a) a single node or b) all nodes in node set
;
; :Params:
;    nodeSet
;
; :Keywords:
;    generation
;    recursive
;
; :Examples:
;
;-
function hubIOXMLReader::getSubNodes, nodeSet, generation=generation, recursive=recursive
  g = isa(generation) ? generation : 0
  if ~isa(nodeSet) || $
    (~keyword_set(recursive) && g gt 1) then return, List()
   
   
  subnodes = List()
  
  
  case typename(nodeSet) of
  'IDLFFXMLDOMNODELIST' : begin
        for i=0, nodeSet.getLength() do begin
          node = nodeSet.item(i)
          subnodes.add, self.getSubNodes(node, recursive=recursive, generation=g), /Extract
        endfor
    end
   'LIST' : begin
      foreach node, nodeSet do begin
        subnodes.add, self.getSubNodes(node, recursive=recursive, generation=g), /Extract
      endforeach
    end
   'IDLFFXMLDOMELEMENT' : begin
      if g gt 0 then begin
        subnodes.add, nodeSet
      endif
      if nodeSet.hasChildNodes() then begin
        subnodes.add, self.getSubNodes(nodeSet.getChildNodes(), generation=g+1, recursive=recursive), /Extract
      endif
   end
   else : ;nothing
  endcase
  
  return, subnodes
end


function hubIOXMLReader::_extractNodeText, node $
    , removeLineBreaks=removeLineBreaks, separator=separator
    
  values = List(node.getNodeValue())
  sep = isa(separator) ? separator : ''
  
  if node.hasChildNodes() then begin
    childNodes = node.getChildNodes()
    for i=0, childNodes.getLength() - 1 do begin
      values.add, self._extractNodeText(childNodes.Item(i), separator=separator)
    endfor
  endif
  
  text = strtrim(values.toArray(),2)
  i = where(strcmp(text, '') ne 1, /NULL)
  if ~isa(i) then return, !NULL
  
  text = text[i]
  text = strtrim(strjoin(text, sep),2)
  if keyword_set(removeLineBreaks) then begin
    text = strjoin(STRSPLIT(text,string([13b, 10b]), /Extract),sep)
  endif
  
  return, text
  
end


function hubIOXMLReader::getNodeValues, nodeSet
  if ~isa(nodeSet) then return, List()
  results = List()
  
  case typename(nodeSet) of
    'LIST': begin
              foreach node, nodeSet do begin
                results.add, self.getNodeValues(node), /Extract
              endforeach
            end
    'IDLFFXMLDOMNODELIST' : begin
                    for i = 0, nodeSet.getLength() do begin
                      results.add, self.getNodeValues(nodeSet.Item(i)), /Extract
                    endfor
    end
    'IDLFFXMLDOMNAMEDNODEMAP' : begin
        for i = 0, nodeSet.getLength() do begin
                      results.add, self.getNodeValues(nodeSet.Item(i)), /Extract
        endfor
    end
    
    'IDLFFXMLDOMELEMENT' : begin
        tagName = nodeSet.getTagName()
        value   = STRCOMPRESS(nodeSet.getNodeValue(), /REMOVE_ALL)      
        if nodeSet.hasChildnodes() then begin
          results.add, self.getNodeValues(nodeSet.getChildNodes()), /Extract 
        endif
        
    end
    
    'IDLFFXMLDOMTEXT' : begin
                    name = nodeSet.getNodeName()
                    value = STRCOMPRESS(nodeSet.getNodeValue())
                    if ~stregex(value, '^[ '+string([7b,8b,9b,10b,11b,12b,13b])+']*$', /Boolean) then begin
                        results.add, value, /Extract
                    endif
    end
    else: stop
  endcase
  return, results
end

function hubIOXMLReader::_tokenStream2String, tokenStream 
  str = ''
  foreach token, tokenStream do begin
    part = ''
    case token.type of
      'EOF'     : ;nothing
      'LITERAL' : part = string(format='(%"\"%s\"")', token.value)
      else      : part = string(format='(%"%s")', token.value)
    endcase
    str += part
  endforeach
  return, str
end

pro hubIOXMLReader::printNode, nodeSet, generation=generation, All=All
  g = isa(generation)? generation : 1
  formatString = '(%"%'+strtrim(g,2)+'s%s")'  
  if ~isa(nodeSet) then return
  type = typename(nodeSet)
  case type of
    'LIST': begin
              foreach subSet, nodeSet do begin
                self.printNode, subSet, generation=g
              endforeach
            end
    'IDLFFXMLDOMNODELIST' : begin
                    for i = 0, nodeSet.getLength() do begin
                      self.printNode, nodeSet.Item(i), generation=g
                    endfor
    end
    'IDLFFXMLDOMELEMENT' : begin
        tagName = nodeSet.getTagName()
        value   = STRCOMPRESS(nodeSet.getNodeValue(), /REMOVE_ALL)      
        print, string(format=formatString,' ',tagName + ' '+ value)    
        self.printNode, nodeSet.getchildNodes(), generation=g+1
    end
    'IDLFFXMLDOMTEXT' : begin
                    if keyword_set(ALL) then begin
                      name = nodeSet.getNodeName()
                      value = STRCOMPRESS(nodeSet.getNodeValue(), /REMOVE_ALL)
                      if ~stregex(value, '^[ '+string([7b,8b,9b,10b,11b,12b,13b])+']*$', /Boolean) then begin
                        print, string(format=formatString,' ',name + '::'+value)
                      endif
                    endif
                    
                    return
                   end
   else: stop
  endcase
  
  
  
end


;function hubIOXMLReader::_getAxis, token, nodeSet
;  
;  result = !NULL
;  case token.type of
;    'SLASHSLASH' : result = nodeSet.getChildNodes()
;    'STAR'       : result = nodeSet.getChildNodes() 
;    'AT'         : result = nodeset.getAttributes()
;    'PARENT'     : result = nodeSet.getParentNode()
;    'DOT'        : result = nodeSet
;    'CHILD'      : result = nodeSet.getFirstChild() 
;    else : message, 'token not handled:'+token.type
;  endcase
;  
;  return, result
;end

function hubIOXMLReader::_isNodeTest, tokenList
  
  nTokens = n_elements(tokenList)
  if nTokens eq 0 then return, 0b
  
;    NodeTest ::= NameTest  
;               | NodeType '(' ')'  
;               | 'processing-instruction' '(' Literal ')'
                 
  token = tokenList[0]
  
  if token.type eq 'STAR' then return, 1b
  if stregex(token.type, '(TEXT|COMMENT|NODE|PI)', /BOOLEAN) $
     && nTokens ge 3 $
     && tokenList[1] eq 'LPAREN' $
     && tokenList[1] eq 'RPAREN' then return, 1b
  return, 0b
end

function hubIOXMLReader::_isPredicate, tokenList
  n = n_elements(tokenList)
  if n eq 0 || tokenList[0].type ne 'LBRACKET' then return, 0b
  
  for i=1, n-1 do begin
    if tokenList[i].type eq 'RBRACKET' then return, 1b
  endfor
  
  message, "Missing closing bracket ']'"   
end

function hubIOXMLReader::_getPredicate, tokenList
  if ~self._isPredicate(tokenList) then message, 'TokenList contains no predicate'
  predicateTokeList = List()
  i = 1
  while tokenList[i].type ne 'RBRACKET' do i++
  return, tokenList[1:i-1]
  message, "Missing closing bracket ']'"   
end

function hubIOXMLReader::_isAxisSpecifier, tokenList
  n = n_elements(tokenList)
;  AxisSpecifier    ::= AxisName '::'  <- '::' already detected by tokenizer
;                     | AbbreviatedAxisSpecifier


  return, self._isAxisName(tokenList) || $
          self._isToken(tokenlist, 'AT') 
end

function hubIOXMLReader::_isAxisName, tokenList
  return, self._isToken(tokenList $
         , ['ANCESTOR','ANCESTOR_OR_SELF','ATTRIBUTE','CHILD','DESCENDANT','DESCENDANT_OR_SELF','FOLLOWING' $
         ,  'FOLLOWING_SIBLING','NAMESPACE','PARENT','PRECEDING','PRECEDING_SIBLING','SELF'])
end

function hubIOXMLReader::_isFunctionCall, tokenList
  n = n_elements(tokenList)
  if n lt 3 then return, 0b
end

function hubIOXMLReader::_getNodeSetFunction, tokenList
  case STRLOWCASE(tokenList[0].value) of
    'last' :
    'position' :
    'count' :
    'id' :
    else : message, 'unknown value:'+tokenList[0].value
  endcase
  return, 1b
end

function hubIOXMLReader::_isToken, tokenList, matches
  if n_elements(tokenList) eq 0 then return, 0b
  foreach m, matches do begin
    if strcmp(tokenList[0].type, m,/FOLD_CASE) then begin
      return, 1b 
    endif
  endforeach
  return, 0b
end

function hubIOXMLReader::_isMatch, tokenList, toMatch, matchValues = matchValues
  if n_elements(tokenList) lt n_elements(toMatch) then return, 0b
   
  foreach m, toMatch, i do begin
    token = tokenList[i]
    
    if ~strcmp(token.type, m,/FOLD_CASE) then return, 0b
    if isa(matchValues) $
      && isa(matchValues[i]) $
      && ~strcmp(token.value, matchValues[i], /FOLD_CASE) then return, 0b
          
          
  endforeach
  return, 1b
end


function hubIOXMLReader::getNodes, tokenList, nodeSet=nodeSet
   
   results = List()
   ;get axis
   lookBehind = !NULL
   ;do the locatization steps
   while n_elements(tokenList) gt 0 && tokenList[0].type ne 'EOF' do begin
     case 1b of
      self._isToken(tokenList, 'SLASHSLASH') : begin 
              if ~isa(nodeSet) then nodeSet = (self.doc).getDocumentElement()
              nodeSet = self.getSubNodes(nodeSet, /RECURSIVE)
      end
      
      self._isToken(tokenList, 'SLASH') : begin
              if isa(nodeSet) then begin
                nodeSet = self.getSubNodes(nodeSet) 
              endif else begin
                nodeSet = (self.doc).getDocumentElement()
              endelse  
                                          end
            
      self._isPredicate(tokenList) : begin
            ;extract predicate tokens
            predicateList = self._getPredicate(tokenList)
            nodeSet = self.selectNodesByPredicate(nodeSet, predicateList)
            tokenList.remove, indgen(n_elements(predicateList)+1)
         
                                     end
      
;      self._isAxisSpecifier(tokenList) : begin
;                      ;consume axis specifier
;                      
;                      ;consume node test
;                      stop
;                      ;optional: consume predicate
;                                         end
                    
       self._isToken(tokenList, 'WORD') : begin
                      if isa(nodeSet) then begin
                        nodeSet = self.selectNodesByName(nodeSet, tokenList[0].value) 
                      endif else begin
                        nodeSet = (self.doc).getDocumentElement()
                      endelse
                                          end
       self._isMatch(tokenList, ['AT','WORD']) && $
       self._isToken(lookBehind, ['SLASH','SLASHSLASH']) : begin
                     
                nodeSet = self.selectNodesByAttribute(nodeSet, tokenList[1].value)
                tokenList.remove, 0
                      
       end
      
      else : message, 'Can not evaluate expression: '+self._tokenStream2String(List(lookbehind) + tokenList)
     endcase
     lookBehind = tokenList.remove(0)
   endwhile
   return, nodeSet
end

function hubIOXMLReader::getElement, expression, separator=separator

  tokenList = self.tokenizer(expression)  
  nodeSet = self.getNodes(tokenList) 
  
  results = self.getNodeValues(nodeSet)
  return, results.toArray(type='STRING')

end





;+
; :Hidden:
;-
pro hubIOXMLReader__define
  struct = {hubIOXMLReader $
    ,inherits IDL_Object $
    ,doc:obj_new() $
    ,filename:'' $
    ,filelines: 0l $
    ,recentExpression:'' $
  }
end

pro test_hubIOXMLReader
  path = '/Users/benjaminjakimow/Documents/SVNCheckouts/EnMAP-Box/SourceCode/_prototyp/XMLReader/testXML.xml'
  ;path = 'D:\EnMAP-Box\SourceCode\_prototyp\XMLReader\testXML.xml'
  reader = hubIOXMLReader(path)
  
  print, '## Test 1'
  print, reader.getElement('//title') 
 ; stop
  print, '## Test 2:'
  print, reader.getElement('/bookstore')
  ;stop
  ;simple predicates
  print, '## Test 3.1:'
  print, reader.getElement('/bookstore/book/author')
  ;stop
  print, '## Test 3.2:'
  print, reader.getElement('/bookstore/book/author[2]')
  ;stop
  ;print, '## Test 3.3:'
  ;print, reader.getElement('/bookstore/book/author[last()]')
  ;stop
  print, '## Test 4.1:'
  print, reader.getElement('/bookstore/book[@category="COOKING"]')
  stop
  print, '## Test 4.2:'
  print, reader.getElement('/bookstore/book/@category')
  stop
  reader.cleanup
end

