pro debug_bitmapButton
;  tlb = widget_base()
;  buttonID = widget_button(tlb, Value=bytscl(randomu(seed,20,20,3)), /BITMAP, FLAT=0, FRAME=0, UVALUE=1, UNAME='colorButton')
;  widget_control, /REALIZE, tlb
  
  tmpGL = widget_base()
  tlb = widget_base(TITLE='Color Selection',/Modal, Group_leader=tmpGL, /COLUMN)
 
  ; predefined envi colors
  allColors = [ $
       0,   0,   0,$
     255, 255, 255,$
     255,   0,   0,$
       0, 255,   0,$
       0,   0, 255,$
     255, 255,   0,$
       0, 255, 255,$
     255,   0, 255,$
     176,  48,  96,$
      46, 139,  87,$
     160,  32, 240,$
     255, 127,  80,$
     127, 255, 212,$
     218, 112, 214,$
     160,  82,  45,$
     127, 255,   0,$
     216, 191, 216,$
     238,   0,   0,$
     205,   0,   0,$
     139,   0,   0,$
       0, 238,   0,$
       0, 205,   0,$
       0, 139,   0,$
       0,   0, 238,$
       0,   0, 205,$
       0,   0, 139,$
     238, 238,   0,$
     205, 205,   0,$
     139, 139,   0,$
       0, 238, 238,$
       0, 205, 205,$
       0, 139, 139,$
     238,   0, 238,$
     205,   0, 205,$
     139,   0, 139,$
     238,  48, 167,$
     205,  41, 144,$
     139,  28,  98,$
     145,  44, 238,$
     125,  38, 205,$
      85,  26, 139,$
     255, 165,   0,$
     238, 154,   0,$
     205, 133,   0,$
     139,  90,   0,$
     238, 121,  66,$
     205, 104,  57,$
     139,  71,  38,$
     238, 210, 238,$
     205, 181, 205]

  !null = reform(allColors,3,n_elements(allColors)/3,/OVERWRITE)
  
  for i=0,n_elements(allColors[0,*])-1 do begin
    color = allColors[*,i]
    image = byte(rebin(reform(color,1,1,3),20,20,3))
  ;  image = rebin(/SAMPLE,bytscl(findgen(20,20)),20,20,3)
    rand = bytscl(randomu(seed,20,20,3))
    buttonID = widget_button(tlb, Value=image, /BITMAP, FLAT=0, FRAME=0, MASK=0)
  endfor
  
  widget_control, tlb, /REALIZE
 ; xmanager, 'hubAMWColor', tlb, /NO_BLOCK, EVENT_HANDLER='hubObjCallback_event'
  
  
end