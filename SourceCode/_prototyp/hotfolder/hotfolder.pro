pro hotfolder_callback, fw, data
  
  print, data.file
  
  bn = STRLOWCASE(FILE_BASENAME(data.file))
  if data.added then begin
    fw.stop
    flag_exit = 0b
    if bn eq 'exit.txt' then begin    
        FILE_DELETE, data.file
        exit
    endif
    
    openr, lun, data.file, /GET_LUN
    ; Read one line at a time, saving the result into array
    array = ''
    line = ''
    WHILE NOT EOF(lun) DO BEGIN & $
      READF, lun, line & $
      array = [array, line] & $
    ENDWHILE
    FREE_LUN, lun
    ;FILE_DELETE, data.file
    
    ;TODO: get event structure
    proc_name = 'imagesvm_parameterize_event'
    proc_title = 'Parameterize SV Regression (SVR)'
    proc_arg = 'svr'
    ;{Parameterize SV Regression (SVR)}  {svr} {imagesvm_parameterize_event}
    ;template_application, Title=title, GroupLeader=groupLeader, Argument=argument
    ;CALL_PROCEDURE, proc_name, title=proc_title, argument=proc_arg
    CALL_PROCEDURE, 'imagesvm_parameterize_application', 'svr', title=proc_title
    
  endif
    fw.start
    

end



pro hotfolder, folder=folder


  if not isa(folder) then folder = 'D:\temp\idlhotfolder'
  
  FILE_MKDIR, folder
  
  
  response = 2
  time2wait = 2
  flag_exit = 0
  
  fw = FOLDERWATCH(folder, 'hotfolder_callback', Frequency=response)
  
  abortfile = FILEPATH('exit.txt', ROOT_DIR=folder)
  while 1b do begin
    print, "wait"
    wait, time2wait
    
  endwhile
   
end