pro enmapboxfile_metadata_read, file, UVALUE = uvalue, R_FID = r_fid
  IF (N_elements(filename) EQ 0) THEN BEGIN
  
    file = DIALOG_PICKFILE(/READ)
    
  ENDIF
  
  IF (~KEYWORD_SET(file)) THEN RETURN
  
  
  ; Open the file and read the embedded header
  
  OPENR, unit, file, /GET_LUN
  
  byteOrder = BYTE(0)
  
  headerString = string('', FORMAT = '(A15)')
  
  READU, unit, byteOrder, headerString
  
  
  
  ; If header string does not read 'ENVIEmbeddedHDR' then the file is not
  
  ; the file type defined in this example.
  
  IF (headerString NE 'ENVIEmbeddedHDR') THEN RETURN
  
  ns = LONG64(0)
  
  nl = LONG64(0)
  
  nb = LONG64(0)
  
  interleave = BYTE(0)
  
  scaleFactor = DOUBLE(0)
  
  minValue = DOUBLE(0)
  
  storageType = BYTE(0)
  
  descriptionLen = 0s
  
  READU, unit, ns, nl, nb, interleave, scaleFactor, minValue, $
  
    storageType, descriptionLen
    
  IF (descriptionLen GT 0) THEN BEGIN
  
    format = STRCOMPRESS('(A'+STRING(descriptionLen)+')', /REMOVE_ALL)
    
    description = STRING('', FORMAT=format)
    
    READU, unit, description
    
  ENDIF ELSE BEGIN
  
    description = ''
    
  ENDELSE
  
  
  
  ; Use the UVALUE to determine which button was clicked.
  
  ; This determines whether the data will be opened as float or double.
  
  
  
  IF (uvalue EQ 'float') THEN dataType = 4 ELSE dataType = 5
  
  
  
  info = {scaleFactor:scaleFactor, $
  
    minValue:minValue, $
    
    storageType:storageType}
    
    
    
  headerOffset = 60+descriptionLen
  
  
  
  readProcedures = ['example_custom_metadata_spatial', $
  
    'example_custom_metadata_spectral']
    
    
    
  ENVI_SETUP_HEAD, BYTE_ORDER = byteOrder, $
    DATA_TYPE = dataType, $
    DESCRIP = description, $
    FNAME = file, $
    INTERLEAVE = interleave, $
    NB = nb, $
    NL = nl, $
    NS = ns, $
    OFFSET = headerOffset, $
    POINTER_INFO = info, $
    R_FID = r_fid, $
    READ_PROCEDURE = readProcedures, $
    /OPEN

end