;unit:  The file unit number already open for reading
;r_data:The parameter variable for the returned data. The read procedure must define this variable before exiting.
;fid:   The file ID of the input image file. (Note: you must use ENVI_FILE_QUERY to obtain the file information; you cannot currently use ENVIRasterToFID.)
;band:  The band position for the desired data. The band variable is a long-integer value ranging from 0 to 1, minus the number of bands.
;xs:    The x starting pixel for the spatial request (in file coordinates)
;ys:    The y starting pixel for the spatial request (in file coordinates)
;xe:    The x ending pixel for the spatial request (in file coordinates)
;ye:    The y ending pixel for the spatial request (in file coordinates)
;_extra:You must specify this keyword in order to collect keywords not used by custom file readers. The use of _extra prevents errors when calling a routine with a keyword that is not listed in its definition.

PRO enmapboxfile_spatial, unit, r_data, fid, band, xs, ys, xe, ye, _extra=_extra
  
  
  
  
end

