;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro prototyp_enviMenu_define_buttons, buttonInfo

  ; parse enmap main menu and application menu files
  
  hubGUIMenu = hubGUIMenu()
  mainMenuFile = file_search(enmapBox_getDirname(/EnmapBox), 'enmapMain.men', Count=numberOfFiles)
  hubGUIMenu.extendMenu, mainMenuFile
  userMenuFilenames = file_search(enmapBox_getDirname(/Applications), 'enmap.men', Count=numberOfFiles)
  for i=0, numberOfFiles-1 do begin
    userMenuFilename = userMenuFilenames[i]
    print, 'Parse application menu file: '+userMenuFilename
    hubGUIMenu.extendMenu, userMenuFilename
  endfor
  hubGUIMenu.defineEnviClassicMenu, buttonInfo, 'EnMAP-Box'

end

pro prototyp_enviMenu

  ; copy pro file to save_add folder
  proDir = 'D:\EnMAP-Box\SourceCode\_prototyp\enviMenu'
  saveFile = 'C:\Program Files\ITT\IDL\IDL80\products\envi48\save_add\prototyp_envimenu.sav'
  hubDev_compileDirectory, proDir, saveFile, /NoLog
;  cd, 'C:\Program Files\ITT\IDL\IDL80\bin\bin.x86_64\'
;  spawn, 'idlrt.exe -nodemowarn -novm "C:\Program Files\ITT\IDL\IDL80\lib\hook\envi.sav"', /NOSHELL, /NOWAIT
  
end


