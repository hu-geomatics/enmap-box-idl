pro svc_default

; training data
  trainingFeaturesRaw = hub_getTestImage('Hymap_Berlin-A_Image')
  traingLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')

; scale features
  trainingFeatures = filepath('scaledFeatures', /TMP)
  imageScalingParameters=hash()
  imageScalingParameters['inputImage'] = trainingFeaturesRaw
  imageScalingParameters['outputImage'] = trainingFeatures
  imageScalingParameters['mode'] = 'zTransformation'
  imageScalingParameters['bandwise'] = 1b
  hubApp_imageScaling_processing, imageScalingParameters

; init idl-java bridge
  enmapJavaBridge_init
  imageSVM = obj_new('imagesvm')
  fn_model = filepath('my_model.svc', /TMP)
  
  imageSVM->set_imagesvm_parameter,/SET_DEFAULT
  imageSVM->set_imagesvm_parameter,/GET_SETTING, IMAGESVM_PARAMETER=imagesvm_parameter
  enum = imageSVM->get_enum()
  imageSVM_parameter.svm_type = enum.svm_type.svc
  imageSVM->set_imagesvm_parameter, IMAGESVM_PARAMETER=imagesvm_parameter

; train model
  imagesvm_parameterization $
    ,FN_MODEL=fn_model $
    ,FN_TRAIN_X=trainingFeatures $
    ,FN_TRAIN_Y=traingLabels $
    ,MEMORY=100 $
    ,OIMAGESVM=imagesvm

; view model
  enmapJavaBridge_init
  imageSVM = obj_new('imagesvm')
  fn_model = filepath('my_model.svc', /TMP)
  imagesvm_view_model,FN_MODEL=fn_model, OIMAGESVM=imageSVM

end