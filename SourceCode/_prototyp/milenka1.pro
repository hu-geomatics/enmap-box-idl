pro milenka1
  compile_opt idl2

  e = envi(/HEADLESS)
  
  ; calculate EVI = (Band4–Band3) / (Band4 + 6*Band3 – 7.5*Band1+ 1)

  files     = 'T:\AR\milenka\'+['band01','band03','band04','fmask']+'.bsq'
  result_file = 'T:\AR\milenka\evi.bsq'

  fids = lonarr(4)  
  foreach file, files, i do begin
    envi_open_file, file, r_fid=r_fid, /no_realize
    fids[i] = r_fid
  endforeach
  envi_file_query, fids[0], nb=nb, dims=dims, wl=wl, bnames=bnames

  ;loop through each band and apply the equation
  out_fid=lonarr(nb)
  for pos=0,nb-1 do begin
    print, pos+1,'/',nb
    envi_doit, 'math_doit', dims=dims, exp='(b4/10000.-b3/10000.)/(b4/10000.+6*b3/10000.-7.5*b1/10000.+1)*(b10 eq 0)', pos=[pos,pos,pos,pos], $
      fid=fids, r_fid=r_fid, /in_memory
    out_fid[pos]=r_fid
  endfor
  
  ;stack the output bands into one file
  pos=lonarr(nb)
  envi_doit, 'cf_doit', dims=dims, fid=out_fid, pos=pos, out_dt=4, $
    /remove, r_fid=result_fid, out_name=result_file

;  envi_file_mng, /REMOVE, ID=b1_fid
;  envi_file_mng, /REMOVE, ID=fmask_fid

raster = e.OpenRaster(result_file)
metadata = raster.METADATA
;metadata.AddItem, 'Author', 'EXELISVIS'
;metadata.UpdateItem, 'Author', 'Exelis Visual Information Solutions'
metadata.UpdateItem, 'band names', bnames
metadata.AddItem, 'wavelength', wl
metadata.AddItem, 'z plot range', [0.00, 1.00]
metadata.AddItem, 'default stretch', '0.000000 0.300000 linear'
metadata.AddItem, 'z plot titles', ['Time','EVI']
raster.WriteMetadata
raster.Close

print, 'done'

end