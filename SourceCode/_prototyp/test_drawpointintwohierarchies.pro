pro test_drawPointInTwoHierarchies

  size = [500,500]
  tlb = widget_base(/ROW)
  widget_control, tlb, /REALIZE
  windowID1 = widget_draw(tlb, XSIZE=size[0], YSIZE=size[1], GRAPHICS_LEVEL=2 )
  widget_control, windowID1, GET_VALUE=oWindow1
  windowID2 = widget_draw(tlb, XSIZE=size[0], YSIZE=size[1], GRAPHICS_LEVEL=2 )
  widget_control, windowID2, GET_VALUE=oWindow2
  
  oView1 = IDLgrView(VIEWPLANE_RECT=[0,0,size], DIMENSIONS=size, LOCATION=[0,0])
  oModel1 = IDLgrModel()
  oView1.add, oModel1
  
  oView2 = IDLgrView(VIEWPLANE_RECT=[0,0,size], DIMENSIONS=size, LOCATION=[0,0])
  oModel2 = IDLgrModel()
  oView2.add, oModel2

  oSymbolData = IDLgrPolygon([-1,1,1,-1],[-1,-1,1,1], COLOR=[255,0,0], ALPHA_CHANNEL=0.1)
  oSymbol = IDLgrSymbol(oSymbolData, /FILLED, Size=5)
  oPoints = IDLgrROI(randomu(seed, 1000, /LONG) mod size[0], randomu(seed, 1000, /LONG) mod size[1], STYLE=0, Symbol=oSymbol, LINESTYLE=6)

  oModel1.add, oPoints
  oWindow1.draw, oView1
  
  oModel1.remove, oPoints
  oModel2.add, oPoints
  oWindow2.draw, oView2
  
  
end