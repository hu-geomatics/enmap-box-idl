pro batch_stackTIFFImages

  tiffFolder = 'G:\Rohdaten\Alentejo\Raster\Hyperion\L1GST\EO1H2030342004084110PZ'
  stackFilename = 'd:\myStack'
  
  tiffFiles = file_search(tiffFolder, '*.tif')
  !null = query_tiff(tiffFiles[0], info)
  
  writerSettings = hash()
  writerSettings['samples'] = info.dimensions[0]
  writerSettings['lines'] = info.dimensions[1]
  writerSettings['bands'] = n_elements(tiffFiles)
  writerSettings['data type'] = info.pixel_type
  writerSettings['dataFormat'] = 'band'

  outputImage = hubIOImgOutputImage(stackFilename)
  outputImage.setMeta, 'band names', file_basename(tiffFiles)
  outputImage.initWriter, writerSettings
  print, 'start batch...'
  foreach tiffFile, tiffFiles do begin
    print, tiffFile
    band = read_tiff(tiffFile, INTERLEAVE=2)
    outputImage.writeData, band
  endforeach
  print, '...done'
  outputImage.cleanup

end