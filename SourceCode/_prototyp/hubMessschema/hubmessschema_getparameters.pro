

function hubMessschema_getParameters, settings, defaultValues=defaultValues
  CANCELED = Hash('accept',0b)
  p1 = hubMessschema_getParameters_Dialog1(settings, defaultValues=defaultValues)
  if ~p1['accept'] then return, CANCELED
  
  p2 = hubMessschema_getParameters_Dialog2(settings, p1, defaultValues=defaultValues)
  if ~p2['accept'] then return, CANCELED
  return, p1+p2
end
;+
; :Hidden:
;-
pro test_hubMessschema_getParameters

  ; test your routine
  settings = hubMessschema_getSettings()
  parameters = hubMessschema_getParameters(settings)
  print, parameters

end  
