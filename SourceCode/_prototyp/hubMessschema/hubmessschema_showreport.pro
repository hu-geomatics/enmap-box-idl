;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `hubMessschema_processing` function to this argument. 
;      
;    settings : in, optional, type=hash
;
;
;-
pro hubMessschema_showReport, reportInfo, settings, parameters
  pixmap = 1b

  
  bands = reportInfo['bands']
  nBands = n_elements(bands)
  stats = reportInfo['targetStats']
  refl  = reportInfo['reflections']
  rad = reportInfo['radiances']
  wr1 = stats['WR1']
  wr2 = stats['WR2']
    
  reportList = hubReportList(Title=string(format='(%"Spectra Report Spot: %s")', parameters['spotname']))
  
  oldMulti = !p.multi
  width = 1200
  height = 600    
  !p.multi = [0,2]
  
  report = reportList.newReport(Title='Input Files')
  lines = list()
  
  headings = ['file','type','time' $
             ,'channels','ch1_wavel [nm]','wavel_step [nm]' $
             ,'it [ms]','last dc [sec]' $
             ,'dc_count','ref_count','sample_count' $
             ,'swir1_gain','swir2_gain' $
             ,'swir1_offset','swir2_offset']
  
  ;formats  = ['(%"%s")','(%"%s")','(%"%s")','(%"%i")','()'] 
  
  table = hubReportTable(headings)
  
  foreach target, reportInfo['targets'], iTarget do begin
    targetData = (reportInfo['radiances'])[target]
    files = (reportInfo['targetFilenames'])[target]
    lines.add, string(format='(%"%4s : %s")', target $
             , strcompress(strjoin(files,',')))
    
    foreach filename, targetData['filenames'], iSample do begin
      timeString = (targetData['timeStrings'])[iSample]
      hdr = (targetData['headers'])[iSample]
      
      dccTime = bin_date(systime(0, hdr.dc_time))
      secDCC = dccTime[5] + 60 * dccTime[4] + 60 * 60 * dccTime[3]
      secSpec = long(hdr.when.tm_sec) + long(hdr.when.tm_min) * 60 + long(hdr.when.tm_hour) * 60 * 60 

      table.addRow, list(file_basename(filename), target, timeString $
                        , hdr.channels, hdr.ch1_wavel, hdr.wavel_step $
                        , hdr.it, secSpec-secDCC $
                        , hdr.dc_count, hdr.ref_count, hdr.sample_count $
                        , hdr.swir1_gain, hdr.swir2_gain $
                        , hdr.swir1_offset, hdr.swir2_offset $
                        )
                        
    endforeach
  endforeach
  report.addMonospace, lines.toArray()
  
  report.addMonospace, table.getFormatedASCII(/Trim)
  
  
  
  ;REPORT 1: Radiances of each target
  report = reportList.newReport(Title='White Reference and Target Radiances', short='Radiances')
  pl = hubGraphicWindow(Pixmap=pixmap, XSIZE=width, YSIZE=height)
  foreach target, reportInfo['targets'], i do begin
    report.addHeading, target, 2
    xrange = [min(bands),max(bands)]
    spectra = (rad[target])['spectra']
    filenames = (reportInfo['targetFilenames'])[target]
    yrange = [0, max(spectra)]
    nSamples = n_elements(spectra[*,0]) 
    
    ;Blue/Red Gradient
    lineColors = make_array(3,nSamples, value=0)
    grad = bytscl(indgen(nSamples)*2, top=200)
    lineColors[0,*] = 255 - grad
    lineColors[2,*] = grad
    meanColor = color24([0,200,0])
    pl.call, 'cgPlot', /NODATA $
        , xrange, yrange  $
        , color='black', back='white' $
        , xrange=xrange, yrange=yrange $
        , xstyle=1,title='Radiances '+target $
        , xtitle='wavelength [nm]', ytitle='Radiance'
    
    baseLine = (float(yrange[1])/(nSamples+2))
    cnt = nSamples + 2
    for iS = 0, nSamples - 1 do begin
      color = color24(lineColors[*,iS])
      pl.call, 'cgPlot', bands, spectra[iS,*], color=color, /Overplot
      pl.call, 'cgText', 2000, baseLine * cnt $
             , filenames[iS], color=color
      cnt--
    endfor
    pl.call, 'cgPlot', bands, (stats[target])['mean'], color=meanColor, /Overplot
    pl.call, 'cgText', 2000, baseLine * cnt, 'mean', color=meanColor
    
    
    ;normalized by mean
    normalizedSpectra = rebin((stats[target])['mean'], nSamples, nBands)
    normalizedSpectra = spectra - normalizedSpectra 
    yrange = [min(normalizedSpectra), max(normalizedSpectra)]
     pl.call, 'cgPlot', /NODATA $
        , xrange, yrange  $
        , color='black', back='white' $
        , xrange=xrange, yrange=yrange $
        , xstyle=1,title='Normalized Radiances '+target $
        , xtitle='wavelength [nm]', ytitle='Normalized Radiance'
     
    cnt = nSamples + 2
    for iS = 0, nSamples - 1 do begin
      color = color24(lineColors[*,iS])
      pl.call, 'cgPlot', bands, normalizedSpectra[iS,*], color=color, /Overplot
      pl.call, 'cgText', 2000, baseLine * cnt $
             , filenames[iS], color=color
      cnt--
    endfor
    pl.call, 'cgPlot', bands, bands*0, color=meanColor, /Overplot
    pl.call, 'cgText', 2000, baseLine * cnt, 'mean', color=meanColor    

    report.addImage, pl.tvrd(true=3), 'Left: Sample Radiances, Right: Sample radiances normalized by mean radiance' 
  endforeach
  
  
  ;REPORT 2: Variation / Quality
  
  report = reportList.newReport(Title='Sample Variation')
  nWR = n_elements(reportInfo['targetsWR'])
  WRstd = make_array(nWR, nBands, value=0.)
  WRvarcoeff = make_array(nWR, nBands, value=0.)
  ;red gradient for WR
  grad = bytscl(indgen(nWR)*2, top=100)
  WRcolors = make_array(3,nWR, value=0)
  WRcolors[0,*] = 255 - grad
  
  nTargets = n_elements(reportInfo['targetsNonWR'])
  Tstd = make_array(nTargets, nBands, value=0.)
  Tvarcoeff = make_array(nTargets, nBands, value=0.)
  ;blue gradient for targets
  grad = bytscl(indgen(nTargets)*2, top=200)
  Tcolors = make_array(3,nTargets, value=0)
  Tcolors[2,*] = 255 - grad
 
  
  foreach wr, reportInfo['targetsWR'],i do begin
    WRstd[i,*] = (stats[wr])['stdDev']
    WRvarcoeff[i,*] = (stats[wr])['relStdDev']
  endforeach
  foreach target, reportInfo['targetsNonWR'],i do begin
    Tstd[i,*] = (stats[target])['stdDev']
    Tvarcoeff[i,*] = (stats[target])['relStdDev']
  endforeach
  rangeStd = [0, max([WRstd,Tstd])]
  rangeVarCoeff = [0, 10]
  
  ;plot standard deviation
  pl.call, 'cgPlot', bands, bands*0, color='black', back='white' $
        , xrange=xrange, yrange=rangeStd $
        , xstyle=1 $
        , title='Standard Deviation' $
        , xtitle='wavelength [nm]', ytitle='StdDev'
  
  cnt = nWR+nTargets+2
  baseLine = (float(yrange[1])/cnt)
 
  for i = 0, nWR-1 do begin
    pl.call, 'cgPlot', bands, WRstd[i,*], color=color24(WRColors[*,i]), /Overplot
    pl.call, 'cgText', 2000, baseLine * cnt $
             ,(reportInfo['targetsWR'])[i] , color=color24(WRcolors[*,i])
      cnt--
  endfor
  for i = 0, nTargets-1 do begin
    pl.call, 'cgPlot', bands, Tstd[i,*], color=color24(TColors[*,i]), /Overplot
    pl.call, 'cgText', 2000, baseLine * cnt $
             ,(reportInfo['targetsNonWR'])[i] , color=color24(Tcolors[*,i])
      cnt--
  endfor
  
  
  ;plot variation coefficient
  pl.call, 'cgPlot', bands, bands*0, color='black', back='white' $
        , xrange=xrange, yrange=rangeVarCoeff $
        , xstyle=1 $
        , title='Variation Coefficient' $
        , xtitle='wavelength [nm]', ytitle='StdDev/Mean'
  
  cnt = nWR+nTargets+2
  baseLine = (float(yrange[1])/cnt)
 
  for i = 0, nWR-1 do begin
    pl.call, 'cgPlot', bands, WRvarCoeff[i,*], color=color24(WRColors[*,i]), /Overplot
    pl.call, 'cgText', 2000, baseLine * cnt $
             ,(reportInfo['targetsWR'])[i] , color=color24(WRcolors[*,i])
      cnt--
  endfor
  for i = 0, nTargets-1 do begin
    pl.call, 'cgPlot', bands, TvarCoeff[i,*], color=color24(TColors[*,i]), /Overplot
    pl.call, 'cgText', 2000, baseLine * cnt $
             ,(reportInfo['targetsNonWR'])[i] , color=color24(Tcolors[*,i])
      cnt--
  endfor
  ;add plot to report
  report.addImage, pl.tvrd(true=3), 'Standard Deviations (left) and Variation Coefficient (right)'
  report.addMonospace, ['Variation Coefficient = Standard Deviation / Mean Radiance * 100']
  
  pl.cleanup
  
  
  ;
  ;REPORT 3: Target Reflectance
  ;
  
  report = reportList.newReport(Title='Target Reflectance')
  width = 600
  height = 600    
  !p.multi = [0,1]
  pl = hubGraphicWindow(Pixmap=pixmap, XSIZE=width, YSIZE=height)
  
  foreach target, reportInfo['targetsNonWR'], i do begin
    tString = string(format='(%"Target %i")',i+1)
    report.addHeading,  tString, 2
    
    tstats = stats[target]
     ;Plot Target Reflectances
    targetRef = (reportInfo['reflections'])[target]
    yrange=[0.,1.0]
    pl.call, 'cgPlot', bands, targetRef['byWR1'], color='black' $
        , xrange=xrange, yrange=yrange $
        , xstyle=1 $
        , title='Target Reflectances' $
        , xtitle='wavelength [nm]', ytitle='Reflectance '+cgSymbol('rho')+' [-]'
    pl.call, 'cgPlot', bands, targetRef['byWR2'], color='red', /Overplot
    pl.call, 'cgPlot', bands, targetRef['byMean(WR1,WR2)'], color='blue', /Overplot
    pl.call, 'cgPlot', bands, targetRef['byTimeReflectance'], color='forest green', /Overplot
    
    baseLine = (float(yrange[1])/10)
    pl.call, 'cgText', 380, baseLine*9, 'T/WR1', color='black'
    pl.call, 'cgText', 380, baseLine*8, 'T/WR2', color='red'
    pl.call, 'cgText', 380, baseLine*7, 'T/mean(WR1,WR2)', color='blue'
    pl.call, 'cgText', 380, baseLine*6, 'T/time weighted mean(WR1,WR2)', color='forest green'
    
    report.addImage, pl.tvrd(true=3), 'Reflectance calculations:'
    report.addMonospace, ['T/WR1 -> ref = mean(target radiance) / mean(WR1 radiance)' $
                         ,'T/WR2 -> ref = mean(target radiance) / mean(WR2 radiance)' $
                         ,'T/mean(WR1,WR2) -> ref = mean(target radiance) / mean(WR1,WR2 radiance)' $
                         ,'T/time weighted mean(WR1,WR2) -> ref = mean(target radiance) / WR1 + WR2 radiance, weighted by time distance' $
                         ]  
  endforeach
  pl.cleanup
  
  
  filename = filepath(parameters['spotname']+'.html', root_dir=parameters['outputDir'] $
                         , SUBDIRECTORY=['reports'])
  reportList.saveHTML, filename, /Show
  !p.multi=oldMulti
end
