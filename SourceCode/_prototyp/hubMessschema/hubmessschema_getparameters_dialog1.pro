function hubMessschema_getParameters_Dialog1, settings, defaultValues=defaultValues
  groupLeader = settings.hubGetValue('groupLeader')
  default = isa(defaultValues) ? defaultValues : Hash()
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_frame, Title=' Allgemeines Setting'
    hubAMW_parameter, 'basename', Title="Basisname (z.B. 'messung')", /String $
      , value=default.hubGetValue('basename', default=settings.hubGetValue('basename', default='gi'))
    hubAMW_parameter, 'iFirst'  , Title="Erste Endnummer (z.B. '42') ", /Integer, SIZE=4 $
      , value = default.hubGetValue('iFirst', default=0)
    
    hubAMW_subframe, /Row
      hubAMW_parameter, 'nTargets', Title='Anzahl Targets ', /Integer, IsGT=0, SIZE=2 $
        , value= default.hubGetValue('nTargets', default=3)
      
      hubAMW_parameter, 'nSamples', Title='Spektren pro WR/Target', /Integer, IsGT=0, SIZE=2 $
        , value= default.hubGetValue('nSamples', default=5)
      
  hubAMW_frame ,   Title =  'Verzeichnisse'
    hubAMW_inputDirectoryName, 'inputDir' , Title='Input ' $
      , Value=default.hubGetValue('inputDir', default='D:\Sandbox\Messprotokoll\In\')
    hubAMW_inputDirectoryName, 'outputDir', Title='Output' $
      , Value=default.hubGetValue('outputDir', default='D:\Sandbox\Messprotokoll\Out\')
    
  parameters = hubAMW_manage()  
  return, parameters
end
