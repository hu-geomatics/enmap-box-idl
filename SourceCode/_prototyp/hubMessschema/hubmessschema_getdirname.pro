;+
; :Author: <author name> (<email>)
;-

function hubMessschema_getDirname
  
  result = filepath('hubMessschema', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_hubMessschema_getDirname

  print, hubMessschema_getDirname()
  print, hubMessschema_getDirname(/SourceCode)

end
