function _temp_hubIOASDWriter::init, header
  if isa(header) then self.setHeader, header
  return, 1b
end

pro _temp_hubIOASDWriter::setHeader, header
  self.headerStruct = ptr_new(header)
end 
 
pro _temp_hubIOASDWriter::writeSpectrum, fileName, spectrum
  if ~isa(*self.headerStruct) then message, 'header is undefined'
  hdr = *self.headerStruct
  if hdr.channels ne n_elements(spectrum) then begin
    message, string(format='(%"%i spectral values but %i channels defined in header")', n_elements(spectrum), hdr.channels)
  endif
  
  file_mkdir, file_dirname(fileName)
  
  ;write
  openw, lun, fileName, /get_lun
    self._writeHeader, lun
    self._writeSpectrum, lun, spectrum
    
  close, lun
  free_lun, lun
  self.bytePosition = 0
end

pro _temp_hubIOASDWriter::_writeHeader, lun
  hdr = *self.headerStruct
  self._writeBytes, lun, 'ASD'
  self._writeBytes, lun, hdr.comments
  
  ;todo: support time when
  self._writeBytes, lun, hdr.program_version, position=178
  self._writeBytes, lun, hdr.file_version , position=179
  self._writeBytes, lun, hdr.itime 
  self._writeBytes, lun, hdr.dc_corr
  self._writeBytes, lun, hdr.dc_time
  self._writeBytes, lun, hdr.data_type
  self._writeBytes, lun, hdr.ref_time
  self._writeBytes, lun, hdr.ch1_wavel
  self._writeBytes, lun, hdr.wavel_step
  self._writeBytes, lun, hdr.data_format
  self._writeBytes, lun, hdr.old_dc_count
  self._writeBytes, lun, hdr.old_ref_count
  self._writeBytes, lun, hdr.old_sample_count
  self._writeBytes, lun, hdr.application
  self._writeBytes, lun, hdr.channels
  
  ;app_data and gps_data not supported
  self._writeEmptyBytes, lun ,128 ;fill app_data
  self._writeEmptyBytes, lun ,56 ;fill gps_data
 
  self._writeBytes, lun, hdr.it, position=390
  self._writeBytes, lun, hdr.fo
  self._writeBytes, lun, hdr.dcc
  self._writeBytes, lun, hdr.calibration
  self._writeBytes, lun, hdr.instrument_num
  self._writeBytes, lun, hdr.ymin
  self._writeBytes, lun, hdr.ymax
  self._writeBytes, lun, hdr.xmin
  self._writeBytes, lun, hdr.xmax
  self._writeBytes, lun, hdr.ip_numbits 
  self._writeBytes, lun, hdr.xmode
  self._writeBytes, lun, hdr.flags
  self._writeBytes, lun, hdr.dc_count
  self._writeBytes, lun, hdr.ref_count
  self._writeBytes, lun, hdr.sample_count
  self._writeBytes, lun, hdr.instrument
  self._writeBytes, lun, hdr.bulb
  self._writeBytes, lun, hdr.swir1_gain
  self._writeBytes, lun, hdr.swir2_gain
  self._writeBytes, lun, hdr.swir1_offset
  self._writeBytes, lun, hdr.swir2_offset
  self._writeBytes, lun, hdr.splice1_wavelength
  self._writeBytes, lun, hdr.splice2_wavelength
  self._writeEmptyBytes, lun, 12 ;fill when_in_ms
  self._writeEmptyBytes, lun, 20 ;fill spare
end

pro _temp_hubIOASDWriter::_writeSpectrum, lun, values
  
  if self.bytePosition ne 484 then message, 'wrong byte offset'
  case (*self.headerStruct).data_format of
    0 : toWrite = float(values)
    1 : toWrite = fix(values)
    2 : toWrite = double(values)
    else: message, 'unknown asd data format (0 = float, 1 = int, 2 = double)'
  endcase
  self._writeBytes, lun, toWrite
  
end


pro _temp_hubIOASDWriter::_writeBytes, lun, value, position=position
 bytesToWrite = self._numberOfBytes(value)
 if isa(position) then begin
  diff = long(position) - self.bytePosition
  if diff lt 0 then message, 'back-jumps not supported'
  if diff gt 0 then begin
    self._writeEmptyBytes, lun, diff
  endif
 endif
 
 writeu, lun, value
 self.bytePosition += bytesToWrite
 
end

pro _temp_hubIOASDWriter::_writeEmptyBytes, lun, nBytes
  writeu, lun, bytarr(nBytes)*0b
  self.bytePosition += nBytes
end

function _temp__temp_hubIOASDWriter::_numberOfBytes, var
  info = size(var, /Structure)
  case info.type of
    1 : return, 1 * info.n_elements ;byte
    2 : return, 2 * info.n_elements ;int
    3 : return, 4 * info.n_elements ;long
    4 : return, 4 * info.n_elements ;float
    5 : return, 8 * info.n_elements ;double
    6 : message, 'COMPLEX is not supported'
    7 : return, strlen(var) ;String
    8 : message, 'STRUCT is not supported'
    9 : message, 'DCOMPLEX is not supported'
   10 : message, 'POINTER is not supported'
   11 : message, 'OBJREF is not supported'
   12 : return, 2 * info.n_elements ;uint
   13 : return, 4 * info.n_elements ;ulong
   14 : return, 8 * info.n_elements ;long64
   15 : return, 8 * info.n_elements ;ulong64
   else : message, 'unknown type code'
    
  endcase

end

pro _temp_hubIOASDWriter__define
  struct = {_temp__temp_hubIOASDWriter $
    ,inherits IDL_Object $
    ,headerStruct: ptr_new() $
    ,bytePosition: 0ul $
  }
end

