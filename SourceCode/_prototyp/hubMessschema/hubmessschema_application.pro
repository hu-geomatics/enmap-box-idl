;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `hubMessschema_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `hubMessschema_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `hubMessschema_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubMessschema_application, applicationInfo
  
  ; get global settings for this application
  settings = hubMessschema_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  ;set default values
  
  parameters = settings.hubGetSubHash(['inputDir','outputDir','basename','spotname','nTargets'])
  p1 = hubMessschema_getParameters_Dialog1(settings, defaultValues=parameters)
  if p1['accept'] then begin
    repeat begin
      p2 = hubMessschema_getParameters_Dialog2(settings, p1, defaultValues=parameters)
      if ~p2['accept'] then return
      parameters = p1 + p2
      
      reportInfo = hubMessschema_processing(parameters, settings)
      hubMessschema_showReport, reportInfo, settings, parameters
      
    endrep until ~(p2['accept'] && ~p2['writeSpectra'])  
  endif
end

;+
; :Hidden:
;-
pro test_hubMessschema_application
  applicationInfo = Hash()
  hubMessschema_application, applicationInfo

end  
