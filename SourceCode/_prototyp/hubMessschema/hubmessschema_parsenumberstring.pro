function hubMessschema_parseNumberString, str
  numbers = []
  splitted = strtrim(strsplit(str, ',', /Extract),2)
  
  foreach split,splitted do begin
    if stregex(split, '-', /Boolean) then begin
      parts = strsplit(split,'-', /Extract)
      parts= fix(parts)
      for i=min(parts), max(parts) do begin
        numbers = [numbers, i]
      endfor 
    endif else begin
      numbers = [numbers, fix(split)]
    endelse
  endforeach 
  numbers = numbers[sort(numbers)]
  numbers = numbers[uniq(numbers)] 
  return, numbers
end

pro test_hubMessschema_parseNumberString

  print, hubMessschema_parseNumberString('6-10,7, 11-5')
end 