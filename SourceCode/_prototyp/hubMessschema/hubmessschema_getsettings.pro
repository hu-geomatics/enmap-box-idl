;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('hubMessschema.conf', ROOT=hubMessschema_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, hubMessschema_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function hubMessschema_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('hubMessschema.conf', ROOT=hubMessschema_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = 'Messschema Version '+settings['version']
  settings['nTargets'] = fix(settings.hubGetValue('nTargets', default=3))
  settings['nSamples'] = fix(settings.hubGetValue('nSamples', default=3))
  return, settings

end

;+
; :Hidden:
;-
pro test_hubMessschema_getSettings

  print, hubMessschema_getSettings()

end
