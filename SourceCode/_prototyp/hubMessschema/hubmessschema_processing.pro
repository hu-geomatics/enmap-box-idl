function hubMessschema_processing_getJulDay, hdr
  t = hdr.when
  j = julday(t.tm_mon, t.tm_mday, t.tm_year + 1900, t.tm_hour, t.tm_min, t.tm_sec)
  return, j
end


function hubMessschema_processing_getTimeString, julDay
  caldat,julDay, mm, dd, year, hour, minute, second 
  return, string(format='(%"%4.4i-%2.2i-%2.2i %2.2i:%2.2i:%2.2i")', year, mm, dd, hour, minute, second)
end

function hubMessschema_processing_secondOfDay, hdr
  t = hdr.when
  s = t.tm_hour * 60l * 60l + t.tm_min * 60 + t.tm_sec
  return, s
end

function hubMessschema_processing_getSpectraGroup, directory, basename, endnumbers
  reader = hubIOASDInputReader()
  files = !NULL
  nFiles = n_elements(endnumbers)
  
  results = Hash()
  
  timeJulDay = make_array(nFiles, /Double)
  timeStrings = make_array(nFiles, /String)
  timeSecOfDay = make_array(nFiles, /Long)
  filenames = make_array(nFiles, /String)
  for i=0, nFiles-1 do begin
    filename = string(format='(%"%s%s.%3.3i")', directory, basename, endnumbers[i])
    filenames[i] = filename
    reader.setFileName, filename
    hdr = reader.getHeader()
    bands = reader.getBands()
    if i eq 0 then begin
      headers = replicate(hdr, nFiles)
      spectra = make_array(nFiles, hdr.channels, /Double)
      bands0 = bands
    endif else begin
      if n_elements(bands) ne n_elements(bands0) then message, 'Number of bands/channels is different'
      if total(bands ne bands0) gt 0 then message, 'Wavelength definition is different'
    endelse

    time = hubMessschema_processing_getJulDay(hdr)
    timeJulDay[i] = hubMessschema_processing_getJulDay(hdr)
    caldat,time, mm, dd, year, hour, minute, second 
    timeStrings[i] = hubMessschema_processing_getTimeString(time)
    timeSecOfDay[i] = hubMessschema_processing_secondOfDay(hdr)
    spectra[i,*] = reader.getSpectrum()
    headers[i] = hdr 
  endfor
  
  results['filenames'] = filenames
  results['headers'] = headers
  results['nSpectra'] = nFiles
  results['spectra'] = spectra
  results['bands'] = bands0
  results['nBands'] = nChannels0
  results['timeStrings'] = timeStrings
  results['timeSecOfDay'] = timeSecOfDay
  results['timeJulDay'] = timeJulDay 
  return, results
end

function hubMessschema_processing, parameters, settings
  reader = hubIOASDInputReader()
  targets = parameters['targets']
  inDir = parameters['inputDir']
  basename = parameters['basename']
  targets = parameters['targets']
  targetsWR    = targets[where(stregex(targets, 'WR', /Boolean) eq 1, /Null, COMPLEMENT=targetsNonWR)]
  targetsNonWR = targets[targetsNonWR]
  
  ;get the data
  targetData = Hash()
  foreach target, targets  do begin
    endnumbers = (parameters['targetEndNumbers'])[target]
    targetData[target] = hubMessschema_processing_getSpectraGroup(inDir, basename, endnumbers)
  endforeach
  
  ;Consistency check for all spectra of this spot
  allHeaders = !NULL
  foreach target, targets, i do begin
    allHeaders = [allHeaders, (targetData[target])['headers']]    
  endforeach
  
  if total(allHeaders[*].data_type ne allHeaders[0].data_type) gt 0 then message, "All spectra must have same data type (e.g. 'RAW','REF','RAD')"
  if stdev(allHeaders[*].it) ne 0. then message, 'Integration time is not the same'
  if stdev(allHeaders[*].swir1_gain) ne 0. then message, 'SWIR1 Gain is not the same'
  if stdev(allHeaders[*].swir2_gain) ne 0. then message, 'SWIR2 Gain is not the same'
  if stdev(allHeaders[*].swir1_offset) ne 0. then message, 'SWIR1 Offset is not the same'
  if stdev(allHeaders[*].swir2_offset) ne 0. then message, 'SWIR2 Offset is not the same'
  ;get intra-target specific measures 
  targetStats = Hash()
  foreach target, targets do begin 
    data = targetData[target]
    spectra = data['spectra']
    nSpectra = data['nSpectra']
    nChannels = data['nBands'] 
    
    stats = Hash()
    stats['meanTimeJulDay'] = mean(data['timeJulDay'])
    stats['meanTimeSecOfDay'] = long(mean(data['timeSecOfDay']))
    stats['meanTimeString'] = hubMessschema_processing_getTimeString(stats['meanTimeJulDay'])
    
    ;mean = mean radiance
    stats['mean'] = mean(spectra, Dimension=1)
    
    ;stdDeviation = deviation fo radiance
    if nSpectra gt 1 then begin
      stats['stdDev'] = stddev(spectra, DIMENSION=1)  
    endif else begin
      stats['stdDev'] = make_array(0, nChannels, /Double, VALUE=0.)
    endelse
    
    ;ensure easy print-outs
    stats['mean'] = transpose(stats['mean'])
    stats['stdDev'] = transpose(stats['stdDev'])
    
    
    
    ;relative stdDev: relative to mean value 
    ;stdDev / mean * 100 (Percent)
    stats['relStdDev'] = stats['stdDev'] / stats['mean'] * 100  
    
    ;    rfirstWR_std=100/firstWR*firstWR_std
    ;    rsecondWR_std=100/secondWR*secondWR_std
    ;    rsample_std=100/sample*sample_std
    ;    RMSE=sqrt((firstWR-secondWR)^2)
    ;    rRMSE=100/firstWR * RMSE
    
    
    targetStats[target] = stats
  endforeach
  
  reflections = Hash()
  ;get spectra from WR and non-WR targets
  foreach target, targetsNonWR do begin
    refl = Hash()
    
    ;calculate mean target reflectance related to different interpretation of WR
    meanWR1 = (targetStats['WR1'])['mean']
    meanWR2 = (targetStats['WR2'])['mean']
    meanTarget = (targetStats[target])['mean']
    refl['meanTargetRadiance'] = meanTarget
    refl['byWR1'] = meanTarget / meanWR1
    refl['byWR2'] = meanTarget / meanWR2
    refl['byMean(WR1,WR2)'] = meanTarget / mean([meanWR1, meanWR2], dimension=1) 
    
    ;by order of targets
    ;wr1 = 1, wr2 = n+2, target 1
    
    ;by weighting of mean WR / Target time 
    timeTarget = (targetStats[target])['meanTimeSecOfDay']
    timeWR1 = (targetStats['WR1'])['meanTimeSecOfDay']
    timeWR2 = (targetStats['WR2'])['meanTimeSecOfDay']
    timeSteps = [timeTarget, timeWR1, timeWR2] 
    timeSpan = max(timeSteps) - min(timeSteps)  
    
    weightWR1 = 1. - (float(timeTarget - timeWR1) / timeSpan)
    weightWR2 = 1. - (float(timeWR2 - timeTarget) / timeSpan)
    refl['byTimeWRRadiance'] = meanWR1 * weightWR1 + meanWR2 * weightWR2 
    refl['byTimeReflectance'] = meanTarget / refl['byTimeWRRadiance'] 
    refl['byTimeWeigths'] = [weightWR1, weightWR2]
     
    reflections[target] = refl
  endforeach 
  
  ;write mean radiances and nonWR Target reflection
  if parameters.hubKeywordSet('writeSpectra') then begin
    writer = _temp_hubIOASDWriter()
    
    ;prepare header templates
    hdrRAD = allHeaders[0]
    hdrREF = allHeaders[0]
    hdrREF.data_type = 1b
    
    ;1. SPOT TARGET MEAN VALUES
    ;1.1. write mean radiance for each WR/Target
    dir = filepath('', root_dir=parameters['outputDir'] $
                              , SUBDIRECTORY=['radiance','rad_single']) 
    
    writer.setHeader, hdrRAD
    
    foreach target, targets, i do begin
      bugTarget = target
      !NULL = dialog_message('vor strlowcase')
      ;bugTarget = STRLOWCASE(bugTarget)
      !NULL = dialog_message('nach strlowcase')
      help, bugTarget, output=out0
      filename = string(format='(%"rad_%s_%s.asd")', parameters['spotname'], bugTarget)
      filename = filepath(filename, root_dir=dir)
      
      if ~(targetStats.hasKey(target)) then begin
        help, target, output=out1
        help, (targetStats.keys()).toArray(), output=out2
        help, targets[i], output=out3
                
        !NULL = dialog_message([out0,out1,out2,out3])
        
      endif
      writer.writeSpectrum, filename, (targetStats[target])['mean']
    endforeach 

    !NULL = DIALOG_MESSAGE('Done!')
    ;1.2 write the mixed/averaged radiance that is finally used to calculate a targets reflection
    foreach target, targetsNonWR do begin
      filename = string(format='(%"rad_%s_%s_wr.asd")', parameters['spotname'], bugTarget)
      filename = filepath(filename, root_dir=dir)
      writer.writeSpectrum, filename, (reflections[target])['byTimeWRRadiance'] 
    endforeach 
    
    ;1.3. write spot reflectances for each target (use weighted time methode)
    dir = filepath('', root_dir=parameters['outputDir'] $
                              , SUBDIRECTORY=['reflectance','ref_single'])
    writer.setHeader, hdrREF
    foreach target, targetsNonWR do begin
      filename = string(format='(%"ref_%s_%s.asd")', parameters['spotname'], bugTarget)
      filename = filepath(filename, root_dir=dir)
      targetRefl = reflections[target]
      writer.writeSpectrum, filename, targetRefl['byTimeReflectance'] 
    endforeach

    ;2. SPOT MEAN VALUES
    ;2.1 Mean Spot Radiance Targets/WR (only those used for reflections)
    dir = filepath('', root_dir=parameters['outputDir'] $
                         , SUBDIRECTORY=['radiance','rad_mean'])
    writer.setHeader, hdrRAD
    filenameWR = string(format='(%"rad_%s_mean_wr.asd")', parameters['spotname'])
    filenameSU = string(format='(%"rad_%s_mean_su.asd")', parameters['spotname'])
    filenameWR = filepath(filenameWR, root_dir=dir)
    filenameSU = filepath(filenameSU, root_dir=dir)
    
    meanSpotRAD_WR = !NULL
    meanSpotRAD_SU = !NULL
    foreach target, targetsNonWR do begin
      ;mean surface radiance for all targets
      meanSpotRAD_SU = [meanSpotRAD_SU, (targetStats[target])['mean']]
      ;mean white-reference radiance: average over all weighted radiances   
      meanSpotRAD_WR = [meanSpotRAD_WR, (reflections[target])['byTimeWRRadiance']]
    endforeach
    writer.writeSpectrum, filenameWR, mean(meanSpotRAD_WR, DIMENSION=1)
    writer.writeSpectrum, filenameSU, mean(meanSpotRAD_SU, DIMENSION=1)
    
    ;2.2. Mean Spot Reflectance
    dir = filepath('', root_dir=parameters['outputDir'] $
                         , SUBDIRECTORY=['reflectance','ref_mean'])
    writer.setHeader, hdrREF
    filename = string(format='(%"ref_%s_mean.asd")', parameters['spotname'])
    filename = filepath(filename, root_dir=dir)
    meanSpotREF = !NULL
    foreach target, targetsNonWR do begin
      meanSpotREF = [meanSpotREF, (reflections[target])['byTimeReflectance']]   
    endforeach
    writer.writeSpectrum, filename, mean(meanSpotREF, DIMENSION=1)
                  
  endif
  
  filenames = Hash()
  foreach target, targets do begin
    filenames[target] = file_basename((targetData[target])['filenames'])
  endforeach
  
  results = Hash()
  results['targets'] = targets
  results['targetsWR'] = targetsWR
  results['targetsNonWR'] = targetsNonWR
  
  results['bands'] = (targetData['WR1'])['bands']
  results['nBands'] = n_elements(results['bands'])
  results['targetFilenames'] = filenames 
  results['targetStats'] = targetStats
  results['reflections'] = reflections
  results['radiances'] = targetData
  return, results
end



;+
; :Hidden:
;-
pro test_hubMessschema_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = hubMessschema_getSettings()
  samplesPerTarget = 5
  targets = ['WR1','T1','T2','T3','WR2']
  endnumbers = Hash()
  foreach t, targets, i do begin
    endnumbers[t] = indgen(5) + i * samplesPerTarget
  endforeach
  
  parameters['basename'] ='gi'
  parameters['outputDir:']='D:\Sandbox\Messprotokoll\Out\'
  parameters['accept'] = 1b
  parameters['targets'] = targets
  parameters['iFirst']= 0
  parameters['nTargets'] = 3
  parameters['inputDir']='D:\Sandbox\Messprotokoll\In\'
  parameters['targets'] = targets
  parameters['targetEndNumbers'] = endnumbers
  
  result = hubMessschema_processing(parameters, settings)
  print, result

end  
