function hubMessschema_getParameters_Dialog2, settings, parameters1, defaultValues=defaultValues
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title']
  default = isa(defaultValues) ? defaultValues : Hash()
  
  
  offset = parameters1['iFirst']
  samples = 5
  
  hubAMW_frame ,   Title =  'Spot'
    hubAMW_label, ['Der Punkt an dem WR + Targets','gemessen wurden (z.B. "Wiese3")']
    hubAMW_parameter, 'spotname', Title='Name', /String, value=default.hubGetValue('spotname', default='spot')
    
  hubAMW_frame ,   Title =  'WR und Target Spektren'
    hubAMW_Label, ['z.B. 0-4 oder 0,1,2,3,4 um' $
                  ,'"datei.000" bis "datei.004" anzugeben']
    nT = parameters1['nTargets']
    nTA = indgen(nT)+1 
    tKeys = string(format='(%"T%i")', nTA)
    tNames = string(format='(%"Target%i")', nTA)
    tKeys  = ['WR1', tKeys, 'WR2']
    tNames = ['    WR1', tNames, '    WR2']
    foreach key, tKeys, i do begin
      if default.hubIsa('targetEndNumberStrings') && (default['targetEndNumberStrings']).hubIsa(key) then begin
        hubAMW_parameter, key, Title=tNames[i], /String $
            , value = (default['targetEndNumberStrings'])[key]
      endif else begin
        hubAMW_parameter, key, Title=tNames[i], /String $
            , value = string(format='(%"%i-%i")', offset, offset + samples-1)
      endelse
      offset += samples
    endforeach
  hubAMW_frame ,   Title =  'Ausgabe'
    hubAMW_subframe, 'writeSpectra', title='Nur Statistiken anschauen', SETBUTTON=1, /Row
    hubAMW_subframe, 'writeSpectra', title='Statistiken + berechnete Spektren abspeichern', /Row
    
  parameters = hubAMW_manage()
  if parameters['accept'] then begin
    parameters['targets'] = tkeys
    targetNumbers = Hash()
    targetNumberStrings = Hash()
    foreach key, tKeys do begin
      if parameters.hubIsa(key) then begin
        targetNumberStrings[key] = parameters[key]
        targetNumbers[key] = hubMessschema_parseNumberString(parameters[key])
        parameters.hubRemove, key
      endif
    endforeach
    parameters['targetEndNumberStrings'] = targetNumberStrings
    parameters['targetEndNumbers'] = targetNumbers
    parameters['writeSpectra'] = parameters['writeSpectra'] eq 1
  endif  
  return, parameters  
end