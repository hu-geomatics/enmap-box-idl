;+
; :Author: <author name> (<email>)
;-

pro hubMessschema_make, Cleanup=cleanup

  if keyword_set(cleanup) then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    
    appDir = hubMessschema_getDirname()
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ; find source code and application directory  
  
  codeDir =  file_dirname((routine_info('hubMessschema_make',/SOURCE)).path)
  appDir = hubMessschema_getDirname()
  
  ; check lower case *.pro filenames (important for auto-compiling under unix systems
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
   ; save routines inside SAVE file
   
  SAVFile = filepath('hubMessschema.sav', ROOT=appDir, SUBDIR='lib')
  hubDev_compileDirectory, codeDir, SAVFile, /NoLog
  
  ; create IDLDOC documentation
  
  
;  helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
;
;  idldoc, ROOT=codeDir $
;        , OUTPUT=helpOutputDir $
;        , TITLE='hubMessschema Documentation' $
;        , SUBTITLE='remote sensing made easy' $
;        , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') $
;        , FORMAT_STYLE='rst' $
;        , /COLOR_OUTPUTLOG $
;        , /QUIET $
;        , USER=1

  ; open documentation
  
;  hubHelper.openFile, /HTML, filepath('index.html', ROOT_DIR=helpOutputDir)
  
end
