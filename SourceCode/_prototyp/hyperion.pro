pro hyperion
  enmapbox
  filename = hub_getTestImage('Hymap_Berlin-A_Image')
  cube = hubIOImg_readImage(filename)
  subcube = cube[100:*,200:*,70:*]
  cube[100,200-1,70] = subcube
  cube = cube[*,0:-2,*]
  resultFilename = hub_getUserTemp('testOutput')
  hubIOImg_writeImage, cube, resultFilename

  
end