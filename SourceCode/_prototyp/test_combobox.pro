pro test_combobox
  tlb = widget_base()
  value = ['a', 'b', 'c']
  combobox = widget_combobox(tlb, Value=value, /EDITABLE, XSIZE=200)
  widget_control, /REALIZE, tlb
  widget_control, combobox, COMBOBOX_ADDITEM='hello world', COMBOBOX_INDEX=0
  widget_control, combobox, SET_COMBOBOX_SELECT=0
end