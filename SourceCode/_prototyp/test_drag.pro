function test_dragNotify, widgetID, sourceID, X, Y, Modifiers, Default
  return, 3
end

pro test_drag_event
  ; do nothing
end

pro test_drag

  tlb = widget_base(/ROW)
  draw1 = widget_draw(tlb, DRAG_NOTIFY='test_dragNotify', /DROP_EVENTS)
  draw2 = widget_draw(tlb, DRAG_NOTIFY='test_dragNotify', /DROP_EVENTS)
  tree = widget_tree(tlb, /DRAGGABLE)
  node = widget_tree(tree, Value='hello world', /DRAGGABLE)
  widget_control, tlb, /REALIZE
  xmanager, 'test_drag', tlb, EVENT_HANDLER='test_drag_event', /NO_BLOCK

end