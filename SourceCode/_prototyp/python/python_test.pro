pro python_test
  inputFileName = 'e:\\hubPython_inputParameters.txt'
  outputFilename = 'e:\\hubPython_outputParameters.txt'
  scriptFilename = 'E:\AndreasHU\EnMAP-Box\SourceCode\_prototyp\python\application.py'
  pythonDirname = 'D:\Program Files\Python 3.4.0'
  
  parameters = hash()
  parameters['inputRaster'] = 'E:\\AndreasHU\\EnMAP-Box\\enmapProject\\enmapBox\\resource\\testData\\image\\AF_LC'
  parameters['tupelList'] = [[0,0],[1,1],[2,2],[3,1],[4,1]]
  parameters['outputRaster'] = 'E:\\AF_LC_pythonResult'
  parameters['type_Output'] = 'ENVI'
  openw, lun, inputFileName, width=9999, /GET_LUN
  printf, lun, json_serialize(parameters+hash('scriptOutputFilename', outputFilename))
  close, lun

;  parameterFilePath = 'Y:\temp\temp_held\python\hubPython_parameters.txt'
  
  cd, pythonDirname
  spawn, 'python.exe '+scriptFilename+' '+ inputFileName, spawnResult, spawnError
  print, spawnResult
  print, spawnError
  enmapbox
  enmapBox_openImage, parameters['outputRaster']

end

pro python_test2
  scriptFilename = 'E:\AndreasHU\EnMAP-Box\SourceCode\_prototyp\python\application.py'
  parameters = hash()
;  parameters['inputRaster'] = 'E:\\AndreasHU\\EnMAP-Box\\enmapProject\\enmapBox\\resource\\testData\\image\\AF_LC'
;  parameters['outputRaster'] = 'E:\\AF_LC_pythonResult'
  parameters['inputRaster'] = enmapBox_getTestImage('AF_LC')
  parameters['outputRaster'] = filepath(/TMP, 'pythonResult')
  parameters['tupelList'] = [[0,0],[1,1],[2,2],[3,1],[4,1]]
  parameters['type_Output'] = 'ENVI'
  
  hubPython_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title

  enmapbox
  enmapBox_openImage, parameters['outputRaster']
end