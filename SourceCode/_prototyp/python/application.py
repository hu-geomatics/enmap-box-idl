import gdal
import numpy as np
import sys
import json

GA_ReadOnly = 1

p = sys.argv[1]
file_object = open(p, 'r')
json_string = file_object.read()
parameters = json.loads(json_string)

inputRaster = parameters['inputRaster']
tupelList = parameters['tupelList']
outputRaster = parameters['outputRaster']
type_Output = parameters['type_Output']

# Re-Classify a raster based on tupel values, e.g., tupel = [[0,0],[1,1],[2,2],[3,1],[4,1]]
def ReclassifyRaster(inputRaster, tupelList, outputRaster, type_Output): # "GTiff"=tif, "HFA"=img, "ENVI"=bsq
    # Open the input-file
    ds = gdal.Open(inputRaster, GA_ReadOnly)
    cols = ds.RasterXSize
    rows = ds.RasterYSize
    raster = ds.GetRasterBand(1)
    out_dataType = raster.DataType
    # Create and format the output-file
    outDrv = gdal.GetDriverByName(type_Output)
    out = outDrv.Create(outputRaster, cols, rows, 1, out_dataType)
    out.SetProjection(ds.GetProjection())
    out.SetGeoTransform(ds.GetGeoTransform())
    out_ras = out.GetRasterBand(1)
    out_ras.SetCategoryNames(raster.GetCategoryNames())
    out_ras.SetColorTable(raster.GetColorTable())
    # Process the raster
    for y in range(rows):
        tmp = raster.ReadAsArray(0,y,cols,1)
        dataOut = tmp
        for combo in tupelList:
            in_val = combo[0]
            out_val = combo[1]
            np.putmask(dataOut, tmp == in_val, out_val)
        out_ras.WriteArray(dataOut, 0, y)
    ds = None

#inputRaster = 'D:\\Users\\geo_mahe\\EnMAPBox\\SourceCode\\enmapBox\\_resource\\testData\\image\\AF_LC'
#tupelList = [[0,1],[1,1],[2,2],[3,1],[4,1]]
#outputRaster = 'D:\\Users\\geo_mahe\\EnMAPBox\\SourceCode\\enmapBox\\_resource\\testData\\image\\AF_LC_python_test'
#type_Output = 'ENVI'

    
ReclassifyRaster(inputRaster, tupelList, outputRaster, type_Output)
