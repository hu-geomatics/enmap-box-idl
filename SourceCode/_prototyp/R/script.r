require(rjson)

settings <- commandArgs(TRUE)
text <- readLines(settings)
p <- fromJSON(text)

filenamePlot <- p$filenamePlot
outputFilename <- p$scriptOutputFilename
x <- p$x
y <- p$y

# define my function
myFunction <- function(myArgument
){
  tiff(filenamePlot, res=300, height=1500, width=1500) 
  plot(x=x, y=y, 
	  type=p$type,
	  col=p$col,
	  main=p$main,
	  sub=p$sub,
	  xlab=p$xlab,
	  ylab=p$ylab,
	  pch=p$pch,
	  lty=p$lty,
	  lwd=p$lwd,
	  cex=p$cex,
	  xlim=p$xlim,
	  ylim=p$ylim,
	  bty=p$bty,
	  xaxt=p$xaxt,
	  font=p$font)
  dev.off()
  
  # return some results back to EnMAP-Box
#  result <- list(myResult = "Hello World")
#  fileConn<-file(outputFilename)
#  writeLines(toJSON(result), fileConn)
#  close(fileConn)
}

# run my function
myFunction(myArgument)
