pro rwrapper_plot, x, y, $
  type=type,$
  col=col,$
  main=main,$
  sub=sub,$
  xlab=xlab,$
  ylab=ylab,$
  pch=pch,$
  lty=lty,$
  lwd=lwd,$
  cex=cex,$
  xlim=xlim,$
  ylim=ylim,$
  bty=bty,$
  xaxt=xaxt,$
  yaxt=yaxt,$
  font=font
  
  
  scriptFilename = 'E:\AndreasHU\EnMAP-Box\SourceCode\_prototyp\R\script.r'
    
  parameters = hash($
    'x', x, 'y', y,$
    'type', type,$
    'col', col,$
    'main', main,$
    'sub', sub,$
    'xlab', xlab,$
    'ylab', ylab,$
    'pch', pch,$
    'lty', lty,$
    'lwd', lwd,$
    'cex', cex,$
    'xlim', xlim,$
    'ylim', ylim,$
    'bty', bty,$
    'xaxt', xaxt,$
    'yaxt', yaxt,$
    'font', font,$
    'filenamePlot', filepath(/TMP, 'rwrapper_plot.tiff'))
  result = hubR_runScript(scriptFilename, parameters)
  hubHelper.openFile, parameters['filenamePlot']
end

pro test_rwrapper_plot
  x = [1:10]
  y = x^2
  main = 'Main Title'
  sub = 'Sub Title'
  col = 'blue'

  rwrapper_plot, x, y, $
    type=type,$
    col=col,$
    main=main,$
    sub=sub,$
    xlab=xlab,$
    ylab=ylab,$
    pch=pch,$
    lty=lty,$
    lwd=lwd,$
    cex=cex,$
    xlim=xlim,$
    ylim=ylim,$
    bty=bty,$
    xaxt=xaxt,$
    yaxt=yaxt,$
    font=font

end