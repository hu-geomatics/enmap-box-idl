; Add the extension to the toolbox. Called automatically on ENVI startup.
pro enmapbox_extension_test_extensions_init

  ; Set compile options
  compile_opt IDL2

  ; Get ENVI session
  e = ENVI(/CURRENT)

  ; Add the extension to a subfolder
  e.AddExtension, 'test', 'enmapbox_extension_test', PATH='EnMAP-Box/Help/Applications/Classification/Support Vector Classification', UVALUE='svc'
;  e.AddExtension, 'svr regression', 'enmapbox_extension_test', PATH='Applications/Regression/Support Vector Regression', UVALUE='svr'

end

; ENVI Extension code. Called when the toolbox item is chosen.
pro enmapbox_extension_test, event

  ; Set compile options
  compile_opt IDL2

  ; General error handler
  CATCH, err
  if (err ne 0) then begin
    CATCH, /CANCEL
    if OBJ_VALID(e) then $
      e.ReportError, 'ERROR: ' + !error_state.msg
    MESSAGE, /RESET
    return
  endif

  ;Get ENVI session
  e = ENVI(/CURRENT)
 
  WIDGET_CONTROL, event.ID, GET_UVALUE = svmType

  imageSVM_parameterize_application, svmType

end

