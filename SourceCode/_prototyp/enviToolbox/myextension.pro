; Add the extension to the toolbox. Called automatically on ENVI startup.
pro myExtension_extensions_init
  compile_opt IDL2

  ; Get ENVI session
  e = ENVI(/CURRENT)

  ; Add the extension to a subfolder
  e.AddExtension, 'Parameterize SV Classifier (SVC)', 'imageSVM_parameterize_event', PATH='Applications/Classification/Support Vector Classification', UVALUE='svc'
  
end

pro myExtension_compile
codeDir = 'E:\AndreasHU\EnMAP-Box\SourceCode\_prototyp\enviToolbox'
SAVEFile = 'D:\Program Files\Exelis\ENVI51\extensions\myExtension.sav'
  hubDev_compileDirectory, codeDir, SAVEFile
end