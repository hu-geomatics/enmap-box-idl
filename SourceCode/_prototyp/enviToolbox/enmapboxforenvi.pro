; Add the extension to the toolbox. Called automatically on ENVI startup.
pro enmapBoxForENVI_extensions_init
  compile_opt IDL2
    
end

pro myExtension_compile
codeDir = 'E:\AndreasHU\EnMAP-Box\SourceCode\_prototyp\enviToolbox'
SAVEFile = 'D:\Program Files\Exelis\ENVI51\extensions\myExtension.sav'
  hubDev_compileDirectory, codeDir, SAVEFile
end