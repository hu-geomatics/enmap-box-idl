pro test_sameROIInTwoWindows

  view1 = IDLgrView(viewplane_rect=[0, 0, 100, 100], TRANSPARENT=1)
  view2 = IDLgrView(viewplane_rect=[0, 0, 100, 100])
  model1 = IDLgrModel()
  model2 = IDLgrModel()
  view1.add, model1
  view2.add, model2
  roi1 = IDLgrROI([0,100],[0,100])
  roi2 = IDLgrROI([0,100],[0,100])
  model1.add, roi1
  model2.add, roi2
  
  scene1 = IDLgrScene()
  scene2 = IDLgrScene()
  scene1.add, view1
  scene2.add, view2
  
  tlb = widget_base(/ROW)
  draw1 = widget_draw(tlb, SCR_XSIZE=500, SCR_YSIZE=500, GRAPHICS_LEVEL=2 )
  draw2 = widget_draw(tlb, SCR_XSIZE=500, SCR_YSIZE=500, GRAPHICS_LEVEL=2 )
  
  widget_control, tlb, /REALIZE
  widget_control, draw1, GET_VALUE=oWindow1
  widget_control, draw2, GET_VALUE=oWindow2
  oWindow1.erase, Color=[255,0,0]
  oWindow1.draw, view1
  oWindow2.draw, scene2

  
end