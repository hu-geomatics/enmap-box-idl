Hi Andreas,

The RBF kernel is

k(x,y) = exp(-gamma*||x-y||^2)

where

gamma = 1/(2*(NSCALE*sigma)^2)

and sigma is the average distance between training observations. NSCALE defaults to 1.0, but you can "play" with smaller or larger values to sharpen or flatten the Gaussian.

I guess the manual is Appendix C in my book .

Hope this helps you.

Mort