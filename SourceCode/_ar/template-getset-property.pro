pro class::setProperty, _EXTRA=_extra, property=property
  self.superClass::setProperty, _EXTRA=_extra
  if isa(property) then self.property = property
end

pro class::getProperty, _REF_EXTRA=_extra, property=property
  self.superClass::getProperty, _EXTRA=_extra
  if arg_present(property) then property = self.property
end