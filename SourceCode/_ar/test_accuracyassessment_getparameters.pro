function test_accuracyAssessment_getParameters,type, defaultValues, settings, GroupLeader=groupLeader
  if ~isa(settings) then settings = hash() 
  if ~isa(defaultValues) then defaultValues = hash() 
  case type of
    'regression'    : begin
                title = 'Accuracy Assessment for Regression'
                      end
    'classification': begin
                title = 'Accuracy Assessment for Classification'
                      end
    else : begin
              title='Accuracy Assessment'
           end
  endcase
  tsize=200
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = title
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputImageFilename, 'inputEstimation', Title='Estimation', tsize=tsize $
                           , value = defaultValues.hubGetValue('inputEstimation') 
  hubAMW_inputImageFilename, 'inputReference', Title='Reference', tsize=tsize $
                           , value = defaultValues.hubGetValue('inputReference')                                                                             
  hubAMW_inputImageFilename ,'inputMask', Title='Mask', Optional=2, tsize=tsize $
                            , value = defaultValues.hubGetValue('inputMask')                                            
  hubAMW_inputImageFilename ,'inputROIs', Title='Regions of Interest', Optional=2, tsize=tsize $
                            , value = defaultValues.hubGetValue('inputROIs')
  
  hubAMW_frame ,   Title =  'Correct for Sampling Bias', /ADVANCED
  hubAMW_subframe , 'samplingBias', /ROW, Title =  'reference sample is unbiased (e.g. simple random or proportional stratified random sampling)', /SetButton
  hubAMW_subframe , 'samplingBias', /ROW, Title =  'reference sample is biased and bias can not be corrected (e.g. manual reference sample selection)'
  hubAMW_subframe , 'samplingBias', /COLUMN, Title =  'reference sample is biased due to disproportional stratified random sampling'
    hubAMW_inputImageFilename ,'inputSamplingStratification', Title='Sampling Stratification', value = defaultValues.hubGetValue('inputSamplingStratification')
    options = ['calculate strata proportions from stratification image','enter strata proportions manually']
    hubAMW_checklist, 'strataProportionSource', /Column, List=options, Value=0, Title=''
  
  hubAMW_frame, Title='Advanced Settings', /Advanced
  if type eq 'regression' then begin
    
    hubAMW_parameter, 'numberOfBins', Title='Number of Histogram Bins' $
                    , IsGE=2, /Integer $
                    , value=defaultValues.hubGetValue('numberOfBins', default=50) 
    hubAMW_checkbox,  'equalBins'   , Title='Use Equal Bin Ranges for all Strata' $
                   , value=defaultValues.hubGetValue('equalBins', default=50)
  endif 
  
  ;hubAMWGroup_report, 'report' $
;                    , ADDPLOTSSTYLE = type eq 'regression' ? 2 : 0 $
;                    , SAVEFILEVALUE = type + 'Accuracy' $
;                    , SAVEFILESIZE=521
                    
  hubAMW_outputFilename, 'outputErrorImage', title='Save Errors', Optional=2 $
                       ,SIZE=521, tsize=tsize $
                       , value=defaultValues.hubGetValue('outputErrorImage', default='errorImage')
              
  
  result = hubAMW_manage(ConsistencyCheckFunction='hubApp_accuracyAssessment_getParameters_Check' $
                         , UserInformation = Hash('type',type))
  if result['accept'] then begin                          
    result['type'] = type
  endif 
  return, result
end

;+
; :Hidden:
;-
pro test_accuracyassessment_getparameters
 type= 'regression'
 type= 'classification'
  res = test_accuracyAssessment_getParameters(type)
  print, res
end