pro test_accuracyassessment_congalten_example

; table 7.1 from page 108 (same as page 116)
  nij = [[65, 4,22,24],$
         [ 6,81, 5, 8],$
         [ 0,11,85,19],$
         [ 4, 7, 3,90]]
         
; table 7.2 from page 109
;  nij = [[45, 4,12,24],$
;         [ 6,91, 5, 8],$
;         [ 0, 8,55, 9],$
;         [ 4, 7, 3,55]]

  n_j = total(nij,1)
  ni_ = total(nij,2)
  nii = nij[indgen(4),indgen(4)]
  n = total(nij)

  Wi = [0.3,0.4,0.1,0.2] ; ist PIj
  ; let map proportions fit sample proportions 
  Wi = n_j/n

  pij = make_array(4,4)
  for i=0,3 do for j=0,3 do pij[i,j] = Wi[j] * nij[i,j] / n_j[j]
  p_j = total(pij,1)
  pi_ = total(pij,2)
  
  producer = make_array(4)
  for i=0,3 do producer[i] = pij[i,i]/pi_[i]

  user = make_array(4)
  for j=0,3 do user[j] = pij[j,j]/p_j[j]  ; or  user[j] = nij[j,j]/n_j[j]
  
  overall = 0.
  for j=0,3 do overall += pij[j,j]
  
  kappa = 0.
  kappa = (n*total(nii) - total(ni_*n_j)) / (n^2 - total(ni_*n_j))
  
  VarOverall = 0.
  for i=0,3 do VarOverall += pij[i,i]*(Wi[i]-pij[i,i])/(Wi[i]*n)

  a1 = total(nii)/n
  a2 = total(ni_*n_j)/n^2
  a3 = total(nii*(ni_+n_j))/n^2
  a4 = 0.
  for i=0,3 do for j=0,3 do a4 += nij[i,j]*(ni_[j]+n_j[i])^2
  a4 /= n^3
  b1 = a1*(1-a1)/(1-a2)^2
  b2 = 2*(1-a1)*(2*a1*a2-a3)/(1-a2)^3
  b3 = (1-a1)^2*(a4-4*a2^2)/(1-a2)^4 
  VarKappa = (b1+b2+b3)/n

  VarProducer = make_array(4)
  for i=0,3 do begin
    sum = 0.
    for j=0,3 do begin
      if i eq j then continue
      sum += pij[i,j]*(Wi[j]-pij[i,j])/(Wi[j]*n)
    endfor
    VarProducer[i] = pij[i,i]*pi_[i]^(-4) * (pij[i,i]*sum + (Wi[i]-pij[i,i])*(pi_[i]-pij[i,i])^2/(Wi[i]*n))
  endfor
    
; example from congalton hat ein paar Fehler
; - falsche Klammerung: Wi*n muss geklammert werden -> (Wi*n)
; - class 3 is missing
;     
;  p11      [  p12  (W2  - p12  )/  W2 * n  
;             *** class 3 is missing? ***
;            + p14  (W4  - p14  )/  W4 * n  ]  +(W1  - p11  )(p1_   - p11)  ^2 / W1 * n
;         
;   0.170    [  0.024(0.4 − 0.024)/(0.4)(434)
;             + 0.008(0.2 − 0.008)/(0.2)(434)]  +(0.3 − 0.170)(0.202 − 0.170)^2 /(0.3)(434)

  VarUser = make_array(4)
  for i=0,3 do begin
    VarUser[i] = pij[i,i]*(Wi[i]-pij[i,i])/(Wi[i]^2*n)
  endfor
  
;##########
; use hub equations

  perf = hubMathClassificationPerformance([], [], 4, nij, /AdjustAreaEstimation, MapClassProportions=Wi, TotalArea=1000000)  
  
  print, 'nij (error matrix)'
  print, nij
  print, 'pij (individual cell probabilities)'
  print, pij
  print, 'pi_ (true marginal proportions)'
  print, pi_
  print, 'user'
  print, user
  print, perf['userAccuracy']
  print, 'producer'
  print, producer
  print, perf['producerAccuracy']
  print, 'overall'
  print, overall
  print, perf['overallAccuracy']
  print, 'kappa'
  print, kappa
  print, perf['kappaAccuracy']
  print, 'Variance(overall)'
  print, VarOverall
  print, perf['overallAccuracyStandardError']^2
  print, 'Variance(kappa)'
  print, VarKappa
  print, perf['kappaAccuracyStandardError']^2
  print, 'Variance(producer)'
  print, VarProducer
  print, perf['producerAccuracyStandardError']^2
  print, 'Variance(user)'
  print, VarUser
  print, perf['userAccuracyStandardError']^2  
  
  
  
  

 
;  hubMathClassificationPerformance, reference, estimation, numberOfClasses, initialConfusionMatrix, $
;    AdjustSamplingBias=adjustSamplingBias, StrataLabel=strataLabel, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions,$
;    AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions=mapClassProportions,$
;    TotalArea=totalArea, classNames=classNames, classColors=classColors
    
end