pro bug_treeNoBitmap
  tlb = widget_base()
  treeRoot = widget_tree(tlb, NO_BITMAPS=0)
  treeLeaf = widget_tree(treeRoot, VALUE='old value')
  widget_control, tlb, /REALIZE
  widget_control, treeLeaf, SET_VALUE='this is the new value'

  stop ; Bug: value is not updated 
       ; Workaround: change top level base size  
  
  widget_control, tlb, SCR_XSIZE=300
end

;
;pro bug_treeValue
;  tlb = widget_base()
;  treeBase = widget_base(tlb, SCR_XSIZE=300)
;  treeRoot = widget_tree(treeBase, SCR_XSIZE=3000, YSIZE=25, /DRAGGABLE, /NO_BITMAPS)
;  treeLeaf = widget_tree(treeRoot, VALUE=strjoin(replicate(' ', 1000)) , SCR_XSIZE=3000, /DRAGGABLE)
;  widget_control, tlb, /REALIZE
;  widget_control, treeLeaf, SET_VALUE='hello world'
;  widget_control, treeBase, SCR_XSIZE=300+1
;  
;end