pro bug_float
  str = '1.2'
  flt = float(str)
  help, flt, OUTPUT=output
  ok = dialog_message(['Before JavaBridgeInit:', output], /INFORMATION)
  oBridgeSession = obj_new("IDLJavaObject$IDLJAVABRIDGESESSION")
  flt = float(str)
  help, flt, OUTPUT=output
  ok = dialog_message(['After JavaBridgeInit:', output], /INFORMATION)
end

pro bug_float_create_sav
  filename = '/tmp/bug_float.sav'
  save, /ROUTINES, 'bug_float', FILENAME=filename 
end
