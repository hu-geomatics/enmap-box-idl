pro test_drawContext_event, event
  widget_control, event.top, GET_UVALUE=context
  if event.type eq 0 and event.press eq 4 then begin
    widget_displaycontextmenu, event.id, event.X, event.Y, context
  endif
end

pro test_drawContext

  base = widget_base()
  draw = widget_draw(base, GRAPHICS_LEVEL=2, RETAIN=2, $
    /WHEEL_EVENTS, /BUTTON_EVENTS, /KEYBOARD_EVENTS, /DROP_EVENTS)
  
  ; image context menu
  context = widget_base(draw, /CONTEXT_MENU)
  button1 = widget_button(context, VALUE='First button')
  button2 = widget_button(context, VALUE='Second button')
  widget_control, base, SET_UVALUE=context, /REALIZE
  XMANAGER, 'test_drawContext', base, /NO_BLOCK
end