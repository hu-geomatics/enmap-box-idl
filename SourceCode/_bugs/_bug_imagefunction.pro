pro _bug_imagefunction
  w = getWindows()
  if isa(w) then begin
    w.erase
  endif else begin
     w = window(DIMENSIONS=[800,600])
  endelse
    
 
  x = 1000
  y = 100
  a = make_array(x,y, /UINT, value=0)
  
  for i=0, x-1 do begin
    a[i,*] = i
  endfor
  pos = [.1,.1,.8,.8]
  ;levels = 100
  cTable = Colortable('Rainbow', NCOLORS=levels)
  cTable[0,*] = 255
  ;a = bytscl(a)
  sa = bytscl(a, min=0, max=max(a))
  img = image(a $
      , RGB_TABLE=cTable $
      , MIN_VALUE=min(a), MAX_VALUE=max(a) $
      , AXIS_STYLE=1 $
      , /CURRENT, POSITION=pos)
  cBar = COLORBAR(TARGET=img, ORIENTATION=1)
  stop
end