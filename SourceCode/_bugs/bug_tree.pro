pro bug_tree
  tlb = widget_base()
  dummyBase = widget_base(tlb)
  tree = widget_tree(dummyBase)
  widget_control, tlb, /REALIZE
  widget_control, dummyBase, XOFFSET=100, YOFFSET=100
  xmanager, 'test', tlb, /NO_BLOCK
  !null = widget_tree(tree, Value='hello world')

end