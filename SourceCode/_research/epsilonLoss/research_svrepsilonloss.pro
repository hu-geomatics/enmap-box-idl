pro research_svrEpsilonLoss

  case 1 of
    1 : begin
          featureFilename = hub_getTestImage('Hymap_Berlin-B_Image')
          trainingFilename = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
          testingFilename = hub_getTestImage('Hymap_Berlin-B_Regression-Validation-Sample')
          g = 0.001
          c = 100.
        end
    2 : begin
          featureFilename = 'd:\Dropbox\svrDaten\portugal\image180'
          trainingFilename = 'd:\Dropbox\svrDaten\portugal\frac180_500'
          testingFilename = 'd:\Dropbox\svrDaten\portugal\frac180'
          g = 0.01
          c = 0.1
        end
    3 : begin
          featureFilename = 'd:\Dropbox\svrDaten\portugal540\image540'
          trainingFilename = 'd:\Dropbox\svrDaten\portugal540\frac540'
          testingFilename = 'd:\Dropbox\svrDaten\portugal540\frac540'
          g = 0.01
          c = 0.1
        end
    4 : begin
          featureFilename = 'D:\Dropbox\svrDaten\berlin2\features'
          trainingFilename = 'D:\Dropbox\svrDaten\berlin2\treeFractions'
          testingFilename = trainingFilename
          g = 0.01
          c = 100.
          
          
        end
  endcase
  
  modelFilename = filepath('svrModel.svr', /TMP)
  numberOfFolds = 3

  stop = 0.1
  trainingSampleSet = imageSVMSampleSet(/SVR, Features=featureFilename, Labels=trainingFilename, /File)
  testingSampleSet = imageSVMSampleSet(/SVR, Features=featureFilename, Labels=testingFilename, /File)

  parameter = imageSVMTrainingParameter(/SVR, C=c, G=g, Stop=stop)
  enmapJavaBridge_init

  repetition = 1

  ; search heuristic
  factor = 0.6166
  lossListSH = list()
  maeListSH = list()
  loss = stddev(trainingSampleSet.getLabels(),/DOUBLE)*factor
  for i=0,9 do begin
    print,i
    parameter.setLoss, loss
      
    mae = []
    for k=1,repetition do begin
      crossValidationPerformance = imageSVM_crossValidation(trainingSampleSet, parameter, NumberOfFolds=numberOfFolds, UsedImageSVMLibSVM=usedImageSVMLibSVM)
      mae = [mae, crossValidationPerformance['meanAbsoluteError']]
    endfor
      
    lossListSH.add, loss      
    maeListSH.add, [mean(mae), stddev(mae), min(mae), max(mae)]
    loss = crossValidationPerformance['residualsStandardDeviation'] * factor
    if i ge 1 then begin
      change = 1.-min([lossListSH[-2],lossListSH[-1]])/max([lossListSH[-2],lossListSH[-1]])
      changeMAE = 1.- (maeListSH[-1])[0]/(maeListSH[-2])[0]
      print, change, changeMAE
      if change le 0.05 or changeMAE le 0.05 then break
    endif
  endfor
  titleSH = 'Search Heuristic'
  maeMeanSH =  (maeListSH.toArray())[*,0]
  maeStddevSH = (maeListSH.toArray())[*,1]
  lossSH = lossListSH.toArray()
  
  ;grid search
  
  lossListGS = list()
  maeListGS = list()
  maeIV = []
  nsvIV = []
  stddevGS = []
  n = 20
  for i=0,n do begin
   ; print,i
    loss = 1.*i/n * stddev(trainingSampleSet.getLabels(),/DOUBLE)
    parameter.setLoss, loss
      
    mae = []
    stddev = []
    for k=1,repetition do begin
      crossValidationPerformance = imageSVM_crossValidation(trainingSampleSet, parameter, NumberOfFolds=numberOfFolds, UsedImageSVMLibSVM=usedImageSVMLibSVM)
      mae = [mae, crossValidationPerformance['meanAbsoluteError']]
      stddev = [stddev, crossValidationPerformance['residualsStandardDeviation']]
    endfor
    
    model = imageSVM_training(trainingSampleSet, parameter, UsedImageSVMLibSVM=usedImageSVMLibSVM)
    testingEstimation = model.apply(testingSampleSet.getFeatures())
    testingPerformance = hubMathRegressionPerformance(testingSampleSet.getLabels(), testingEstimation['labels'])
    
    nsvIV = [nsvIV, (model.libSVMOutput)['numberOfTotalSV']]
    maeIV = [maeIV, testingPerformance['meanAbsoluteError']]
    stddevGS = [stddevGS, mean(stddev)]
    lossListGS.add, loss     
    maeListGS.add, [mean(mae), stddev(mae), min(mae), max(mae)]
  endfor  
  titleGS = 'Grid Search'
  maeMeanGS =  (maeListGS.toArray())[*,0]
  maeStddevGS = (maeListGS.toArray())[*,1]
  lossGS = lossListGS.toArray()

  ; plot results

  xTitle = 'Epsilon Loss'
  yTitle = 'Mean Absolute Error'
  xRange = [0, stddev(trainingSampleSet.getLabels(),/DOUBLE)]*1.05
  yRange = xRange

  ; - plot SH

  !null = plot(lossSH, maeMeanSH, COLOR='black', Thick=2, $ 
    Title=titleSH, XTitle=xTitle, YTitle=yTitle, XRange=xrange, YRange=yrange)
  !null = plot(/OVERPLOT, lossGS, maeMeanGS, COLOR='green') 
  !null = plot(/OVERPLOT, lossGS, stddevGS, COLOR='grey', Thick=2)
  for i=0,n_elements(lossSH)-1 do !null = text(lossSH[i], maeMeanSH[i], strtrim(i+1, 2), /Data, FONT_STYLE='Bold', Font_Color='red')
  !null = plot(/OVERPLOT, xrange, [maeMeanSH[-1],maeMeanSH[-1]], COLOR='red')
  !null = plot(/OVERPLOT, [lossSH[-1],lossSH[-1]], yrange, COLOR='red')
  
  !null = plot(/OVERPLOT, lossGS, maeIV, COLOR='blue', LINESTYLE='--')
  nsvIVNormalized = 1.*nsvIV/max(nsvIV)*yrange[1]
  !null = plot(/OVERPLOT, lossGS, nsvIVNormalized, COLOR='grey', /HISTOGRAM)
  
  return
  ; - plot GS
  
  !null = plot(lossGS, maeMeanGS, COLOR='black', Thick=2, $ 
    Title=titleGS, XTitle=xTitle, YTitle=yTitle, XRange=xrange, YRange=yrange)
  !null = plot(/OVERPLOT, lossGS, maeMeanGS-maeStddevGS, COLOR='grey', Thick=2)
  !null = plot(/OVERPLOT, lossGS, maeMeanGS+maeStddevGS, COLOR='grey', Thick=2)
  !null = plot(/OVERPLOT, lossGS, maeIV, COLOR='blue')

end