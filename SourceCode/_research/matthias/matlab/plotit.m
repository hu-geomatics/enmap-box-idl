function plotit(filename)

fileID = fopen(filename,'r');
jsonString = fscanf(fileID,'%s');

parameters = loadjson(jsonString)
x = parameters.x
y = parameters.y
plotFilename = parameters.plotFilename
main = parameters.title
xlab = parameters.xlab
ylab = parameters.ylab
%type = parameters.type

h=figure;
%x=[-2*pi:.1:2*pi];
%y = times(cos(x),x);
plot(x,y,'blue');
title(main);
xlabel(xlab);
ylabel(ylab);
saveas(h,plotFilename)
exit;