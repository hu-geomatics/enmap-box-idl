pro test_adjustedAccuracyAssessment_ar

  strataSizes = [1000, 1000, 2000, 4000]
  
  sceneOverallSize = total(strataSizes)
  strataProportions = strataSizes/sceneOverallSize
  strataMap = []
  foreach strataSize, strataSizes, i do begin
    strataMap = [strataMap, replicate(i+1, strataSize)]
  endforeach
  print, 'scene size:           ', sceneOverallSize
  print, 'strata sizes:         ', strataSizes
  print, 'strata proportions:   ', strataProportions
  print
  
  sampleSizes = [400, 400, 400, 400]
  sampleOverallSize = total(sampleSizes)
  sampleProportions = sampleSizes/sampleOverallSize
  print, 'sample size:          ', sampleOverallSize
  print, 'sample sizes:         ', sampleSizes
  print, 'sample proportions:   ', sampleProportions
  print
  
  correctionFactors = strataProportions/sampleProportions
  print, 'correction factors:   ', correctionFactors
  print, 'unbiased sample sizes:', correctionFactors*sampleSizes
  print
  
  trueP = [0.90, 0.10]
  mapError = 0.30
  print, 'true proportions:', trueP
  print, 'map error:       ', mapError
  print, ' adjusted proportion (relative error)                           map proportion (relative error)                               reference sample size (stratified design %)'
  
  seed = systime(1)

  ; generate true map
  trueMap = make_array(sceneOverallSize, VALUE=2l)
  trueMap[0:sceneOverallSize*trueP[0]-1] = 1

  ; generate estimated map with specific error rate
  estMap = trueMap
  offset = [0, sceneOverallSize*trueP[0]]
  if mapError ne 0. then begin
    for i=0,1 do begin
      indices = sampling_random_sample(N=sceneOverallSize*trueP[i], K=sceneOverallSize*trueP[i]*mapError, SEED=seed)
      estMap[offset[i]+indices] = i+1 eq 1 ? 2 : 1 ; create errors by flipping labels
    endfor
  endif

  ; generate reference sample
  referenceIndices = []
  foreach sampleSize, sampleSizes, i do begin
    strataIndices = where(/NULL, strataMap eq i+1)
    strataIndicesSample = strataIndices[sampling_random_sample(N=strataSizes[i], K=sampleSize, SEED=seed)]
    referenceIndices = [referenceIndices, strataIndicesSample]
  endforeach
  referenceCorrectionFactors = correctionFactors[strataMap[referenceIndices]-1]

  referenceSize1 = n_elements(where(/NULL,trueMap[referenceIndices] eq 1))
  referenceSize2 = n_elements(where(/NULL,trueMap[referenceIndices] eq 2))

  nij = fltarr(2,2)
  for i=0,1 do for j=0,1 do begin
;    nij[i,j] = n_elements(where(/NULL, (estMap[referenceIndices] eq j+1) and (trueMap[referenceIndices] eq i+1)))
    nij[i,j] = total(((estMap[referenceIndices] eq j+1) and (trueMap[referenceIndices] eq i+1))*referenceCorrectionFactors)
  endfor
    
  ni_ = total(nij, 1)
  n_j = total(nij, 2)
  Wi = histogram(estMap, MIN=1, MAX=2, BINSIZE=1)/sceneOverallSize 
  
;  pij = nij*rebin(/SAMPLE, reform(Wi/ni_,1,2), 2, 2)
;  p_j = total(pij, 2)
  p_j = n_j/sampleOverallSize

  print, strcompress(strjoin(p_j))+'   ('+strjoin(p_j/trueP*100-100,'% ')+'%)           '+strcompress(strjoin(Wi))+'('+strjoin(Wi/trueP*100-100,'% ')+'%)              '+strtrim(long(referenceSize1),2)+' '+strtrim(long(referenceSize2),2)+' ('+strtrim(strcompress(strjoin(sampleProportions*100)),2)+')' 

  print
  print, 'nij:'
  print, nij
  
;  print
;  print, 'ni_: ',ni_
;  print, 'n_j: ',n_j

;  print, 'Wi:  ', Wi

;  print, 'pij: '
;  print, pij
;  
;  print
;  print, 'p_j: ', p_j
;  print
  print, 'Aj:  ', p_j*sceneOverallSize
;  print, 'mappedClassSizes: ', float(mappedClassSizes)
  print, 'True:', trueP*sceneOverallSize
;  print, '    SampleID    Reference   Estimation               Strata  ConfusionCounts'
;  print,  transpose([[string(indgen(sampleOverallSize)+1)],$
;                     [string(trueMap[referenceIndices])],$
;                     [string(estMap[referenceIndices])],$
;                     [string(strataMap[referenceIndices])],$
;                     [string(referenceCorrectionFactors)]]     )
;
;  

  result = hubMathClassificationPerformance(trueMap[referenceIndices], estMap[referenceIndices], 2, $
    /Stratified, StrataLabel=strataMap[referenceIndices], StrataProportions=strataProportions, StrataSampleProportions=sampleProportions,$
    TotalArea=sceneOverallSize)
  areaEstimation = result['areaEstimation']

    print, '   Class  Estimated Area       95% Confidence Intervall'
  for i=0,result['numberOfClasses']-1 do begin
    print, i+1, (areaEstimation['classArea'])[i], (areaEstimation['classAreaConfidence95Lower'])[i], (areaEstimation['classAreaConfidence95Upper'])[i] 
  endfor

;  print, res['confusionMatrix']

  

end