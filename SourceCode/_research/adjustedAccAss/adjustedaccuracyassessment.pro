pro adjustedAccuracyAssessment


  adjustSamplingBias = 0    ; 1 for adjusting , 0 for not adjusting
  adjustAreaEstimation = 0  ; 1 for adjusting , 0 for not adjusting

  ; define filenames  
  referenceFilename = hub_getTestImage('Hymap_Berlin-A_Classification-Validation-Sample')
  estimationFilename = hub_getTestImage('Hymap_Berlin-A_Classification-Estimation')
  stratificationFilename = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth')
  
  ; read data and meta information
  numberOfClasses = hubIOImg_getMeta(referenceFilename, 'classes')-1
  classNames = (hubIOImg_getMeta(referenceFilename, 'class names'))[1:*]
  classColors = (hubIOImg_getMeta(referenceFilename, 'class lookup'))[*,1:*]
  referenceImage = hubIOImg_readImage(referenceFilename)
  referenceIndices = where(/NULL, referenceImage ne 0)
  referenceLabels = referenceImage[referenceIndices]
  estimationImage = hubIOImg_readImage(estimationFilename)
  estimationLabels = estimationImage[referenceIndices]

  ; calculate map class proportions
  mapClassHistogram = histogram(estimationImage, MIN=1, MAX=numberOfClasses, BINSIZE=1)
  totalMapArea = total(mapClassHistogram)
  mapClassProportions = mapClassHistogram/float(totalMapArea)

  if adjustSamplingBias then begin
    
    ; calculate strata proportions
    numberOfStrata = hubIOImg_getMeta(stratificationFilename, 'classes')-1
    strataImage = hubIOImg_readImage(stratificationFilename)
    strataLabels = strataImage[referenceIndices]
    strataHistogram = histogram(strataImage, MIN=1, MAX=numberOfStrata, BINSIZE=1)
    totalStrataArea = total(strataHistogram)
    strataProportions = strataHistogram/totalStrataArea
    
    ; calculate strata proportions inside the reference sample
    strataSampleHistogram = histogram(strataLabels, MIN=1, MAX=numberOfStrata, BINSIZE=1)
    sampleSize = n_elements(referenceIndices)
    strataSampleProportions = strataSampleHistogram/float(sampleSize)

    ; check if areas match
    if totalStrataArea ne totalMapArea then begin
      print, 'Warning: Size of mapped area and strata area do not match. Estimates might be inaccurate.'
    endif
  endif

  ; accuracy assessment
  result = hubMathClassificationPerformance(referenceLabels, estimationLabels, numberOfClasses, $
    AdjustSamplingBias=adjustSamplingBias, StrataLabel=strataLabels, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions, $
    AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions=mapClassProportions,$
    TotalArea=totalMapArea, ClassNames=classNames, ClassColors=classColors)

  ; report
  
  print
  print, 'Overall Accuracy =    ', string(FORMAT='(f6.2)', result['overallAccuracy'])
  print, 'Average F1 Accuracy = ', string(FORMAT='(f6.2)', result['averageF1Accuracy'])
  print, 'Kappa =               ', string(FORMAT='(f8.4)', result['kappaAccuracy'])

  print
  print, 'Class | User Accuracy | Producer Accuracy | F1 Accuracy
  for i=0,result['numberOfClasses']-1 do begin
    print, string(FORMAT='(i5)', i+1)+'   ',$
           string(FORMAT='(f13.2)',(result['userAccuracy'])[i])+'   ',$
           string(FORMAT='(f17.2)',(result['producerAccuracy'])[i])+'   ',$
           string(FORMAT='(f11.2)',(result['f1Accuracy'])[i])+'   '
            
  endfor
  
  if adjustAreaEstimation then begin
    areaEstimation = result['areaEstimation']
    print
    print, 'Class | Estimated Area* |   95% Confidence Intervall'
    for i=0,result['numberOfClasses']-1 do begin
      print, string(FORMAT='(i5)', i+1)+'   ',$
             string(FORMAT='(f14.2)',(areaEstimation['classArea'])[i])+'   ',$
             string(FORMAT='(f12.2)', (areaEstimation['classAreaConfidence95Lower'])[i])+', '+string(FORMAT='(f12.2)', (areaEstimation['classAreaConfidence95Upper'])[i]) 
    endfor
    print, '*measured in pixels
  endif

end