pro test_adjustedAccuracyAssessment

  trueP = [0.8, 0.2]
  Atot = 10000.
  mapError = 0.2
  stratifiedDesign = 0.5
;  drawFromEstimatedMap = 0b
  print, 'true proportions:', trueP
  print, 'map error:       ', mapError
  print, ' adjusted proportion (relative error)                           map proportion (relative error)                               reference sample size (stratified design %)'
  
  seed = systime(1)

  ; generate true map
  trueMap = make_array(Atot, VALUE=2l)
  trueMap[0:Atot*trueP[0]-1] = 1

  ; generate estimated map with specific error rate

  estMap = trueMap

  offset = [0, Atot*trueP[0]]
  if mapError ne 0. then for i=0,1 do begin
    indices = sampling_random_sample(N=Atot*trueP[i], K=Atot*trueP[i]*mapError, SEED=seed)
    estMap[offset[i]+indices] = i+1 eq 1 ? 2 : 1
  endfor

  ; generate reference sample
  n = 1000

  mapIndices1 = where(/NULL, estMap eq 1, Ami1)
  mapIndices2 = where(/NULL, estMap eq 2, Ami2)

  Ami = lonarr(2)
  Ami[0] = Ami1
  Ami[1] = Ami2
  n1 = n*stratifiedDesign
  n2 = n*(1-stratifiedDesign)
  refIndex1 = mapIndices1[sampling_random_sample(N=Ami1, K=n1, SEED=seed)]
  refIndex2 = mapIndices2[sampling_random_sample(N=Ami2, K=n2, SEED=seed)]
  refIndex = [refIndex1, refIndex2]
  refN1 = n_elements(where(/NULL,trueMap[refIndex] eq 1))
  refN2 = n_elements(where(/NULL,trueMap[refIndex] eq 2))

  nij = lonarr(2,2)
  for i=0,1 do for j=0,1 do begin
    nij[i,j] = n_elements(where(/NULL, (estMap[refIndex] eq j+1) and (trueMap[refIndex] eq i+1)))
  endfor

  ni_ = total(nij, 1)
  n_j = total(nij, 2)
  Wi = Ami/Atot 
  pij = nij*rebin(/SAMPLE, reform(Wi/ni_,1,2), 2, 2)
  p_j = total(pij, 2)

  Aj = Atot*p_j
  SEp_j = fltarr(2)
  for j=0,1 do begin
    for i=0,1 do begin
      SEp_j[j] += Wi[i]^2 * ( (nij[i,j]/ni_[i])*(1-nij[i,j]/ni_[i]) )/(ni_[i]-1)
    endfor 
  endfor
  SEp_j = sqrt(SEp_j)
  SEAj = Atot*SEp_j
  
;  print, strcompress(strjoin(p_j))+'   ('+strjoin(p_j/trueP*100-100,'% ')+'%)           '+strcompress(strjoin(Wi))+'('+strjoin(Wi/trueP*100-100,'% ')+'%)              '+strtrim(long(refN1),2)+' '+strtrim(long(refN2),2)+' ('+strtrim(strcompress(strjoin([stratifiedDesign,1-stratifiedDesign]*100)),2)+')' 

;  print
;  print, 'nij:'
;  print, nij
;  
;  print
;  print, 'ni_: ',ni_
;  print, 'n_j: ',n_j
;
;  print, 'Wi:  ', Wi
;
;  print, 'pij: '
;  print, pij
;  
;  print
;  print, 'p_j: ', p_j
;  print

  ; z=1.98 for 95% confidence, z=2.58 for 99% confidence
  zScore = 1.98
  Aj95low = Aj-zScore*SEAj
  Aj95up  = Aj+zScore*SEAj
  print
  print, '               A1           A2   A1-Lower95%  A1-Upper95%  A2-Lower95%  A2-Upper95%'
  print, 'Paper', Aj[0], Aj[1], Aj95low[0], Aj95up[0], Aj95low[1], Aj95up[1]
  

; ###########
  
  numberOfClasses = 2
  strataLabel = estMap[refIndex]
  strataProportions = Wi
  strataSampleProportions = ni_/n
  
  result = hubMathClassificationPerformance(trueMap[refIndex], estMap[refIndex], 2, $
    /Stratified, StrataLabel=strataLabel, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions,$
    TotalArea=Atot)
  areaEstimation = result['areaEstimation']

  Aj = float(areaEstimation['classArea'])
  Aj95low = float(areaEstimation['classAreaConfidence95Lower'])
  Aj95up = float(areaEstimation['classAreaConfidence95Upper'])
  
  print, 'New  ', Aj[0], Aj[1], Aj95low[0], Aj95up[0], Aj95low[1], Aj95up[1]
 ; areaEstimation['classArea']   print, 'Confidence 95% Class 1: ', (areaEstimation['classAreaConfidence95Lower'])[0], (areaEstimation['classAreaConfidence95Upper'])[0]
;  print, 'Confidence 95% Class 2: ', (areaEstimation['classAreaConfidence95Lower'])[1], (areaEstimation['classAreaConfidence95Upper'])[1]
;  print, 'mij'
;  print, result['confusionMatrix']

;###
;dirk pij
  refprop_hat = [Wi[0]*replicate(1,n1)/n1, Wi[1]*replicate(1,n2)/n2]
  pij = fltarr(2,2)
  for i=0,1 do for j=0,1 do begin
    pij[i,j] = total(((estMap[refIndex] eq j+1) and (trueMap[refIndex] eq i+1)) * refprop_hat)
  endfor
;  print
;  print, 'dirks mij = pij*n
;  print, pij*n

  print, 'Dirk ', total(pij,2)*Atot
  
  
  
  
end