pro test_adjustedAccuracyAssessment_dp

  trueP = [0.9, 0.1]
  Atot = 1000000.
  mapError = 0.20
  stratifiedDesign = 0.5 
;  drawFromEstimatedMap = 0b
  print, 'true proportions:', trueP
  print, 'map error:       ', mapError
  print, ' adjusted proportion (relative error)                           map proportion (relative error)                               reference sample size (stratified design %)'
  
 ; seed = 1

for drawFromEstimatedMap=0,1 do begin 

  ; generate true map
  trueMap = make_array(Atot, VALUE=2l)
  trueMap[0:Atot*trueP[0]-1] = 1

  ; generate estimated map with specific error rate

  estMap = trueMap

  offset = [0, Atot*trueP[0]]
  if mapError ne 0. then for i=0,1 do begin
    indices = sampling_random_sample(N=Atot*trueP[i], K=Atot*trueP[i]*mapError, SEED=seed)
    estMap[offset[i]+indices] = i+1 eq 1 ? 2 : 1
  endfor

  ; generate reference sample
  n = 1000
  if drawFromEstimatedMap then begin
    mapIndices1 = where(/NULL, estMap eq 1, Ami1)
    mapIndices2 = where(/NULL, estMap eq 2, Ami2)
  endif else begin
    strataMap = trueMap*0
    frac = 0.70
    strataMap[randomu(/LONG, seed, frac*Atot) mod Atot] = 1
    strataMap += 1
    mapIndices1 = where(/NULL, strataMap eq 1, Ami1)
    mapIndices2 = where(/NULL, strataMap eq 2, Ami2)
  endelse
  Ami = lonarr(2)
  Ami[0] = Ami1
  Ami[1] = Ami2
  Wi = Ami/Atot 
  
  n1 = n*stratifiedDesign
  n2 = n*(1-stratifiedDesign)
    
  refIndex1 = mapIndices1[sampling_random_sample(N=Ami1, K=n1, SEED=seed)]
  refIndex2 = mapIndices2[sampling_random_sample(N=Ami2, K=n2, SEED=seed)]
  refIndex = [refIndex1, refIndex2]
  refN1 = n_elements(where(/NULL,trueMap[refIndex] eq 1))
  refN2 = n_elements(where(/NULL,trueMap[refIndex] eq 2))

  refprop_hat = [Wi[0]*replicate(1,n1)/n1, Wi[1]*replicate(1,n2)/n2]
  pij = fltarr(2,2)
  for i=0,1 do for j=0,1 do begin
    pij[i,j] = total(((estMap[refIndex] eq j+1) and (trueMap[refIndex] eq i+1)) * refprop_hat)
  endfor
  

  p_j = total(pij, 2)

  print, strcompress(strjoin(p_j))+'   ('+strjoin(p_j/trueP*100-100,'% ')+'%)      '+strcompress(strjoin(Wi))+'('+strjoin(Wi/trueP*100-100,'% ')+'%)              '+strtrim(long(refN1),2)+' '+strtrim(long(refN2),2)+' ('+strtrim(strcompress(strjoin([stratifiedDesign,1-stratifiedDesign]*100)),2)+')' 

;
;  print
;  print, 'nij:'
;  print, nij
;  
;  print
;  print, 'ni_: ',ni_
;  print, 'n_j: ',n_j
;
;  print, 'Wi:  ', Wi
;
  print, 'pij: '
  print, pij
;  
;  print
;  print, 'p_j: ', p_j
;  print
;  print, 'Aj:  ', p_j*Atot
;  print, 'Ami: ', float(Ami)
;  print, 'True:', trueP*Atot
;  
endfor
  
  

end