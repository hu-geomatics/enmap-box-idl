pro test_multiSensor

  ; define files
  wd = 'E:\Andreas\Dropbox\testImages\'
;  wd = 'd:\Dropbox\testImages\'

  imageFiles = wd+'image'+['1','2','3']+'Stretched'
  maskFiles  = wd+'mask'+['1','2','3']
  labelFile =  wd+'train'

  ; read features and labels
  numberOfSensors = n_elements(imageFiles)
  features = list()
  masks = list()
  numberOfFeaturesList = list()
  for i=0,numberOfSensors-1 do begin
    features.add, transpose((hubIOImg_readSample(imageFiles[i], labelFile, OutNumberOfFeatures=outNumberOfFeatures)).features)
    masks.add,    transpose((hubIOImg_readSample(maskFiles[i], labelFile)).features)
    numberOfFeaturesList.add, outNumberOfFeatures
  endfor
  numberOfFeaturesList = numberOfFeaturesList.toArray()
  numberOfFeatures = total(numberOfFeaturesList, /INTEGER)

  labels = transpose((hubIOImg_readSample(labelFile, labelFile, OutNumberOfSamples=numberOfSamples)).features)
  
  ; calculate sensor combinations from masks
  numberOfCombinations = 2l^numberOfSensors
  combiIDs = lonarr(numberOfSamples)
  for i=0,numberOfSensors-1 do begin
    combiIDs += 2l^i*masks[i]
  endfor
  
  ; initialize label combinations
  labelCombiAll = rebin(/SAMPLE, labels, numberOfSamples, numberOfCombinations)
  labelCombiMask = labelCombiAll*0
  for i=0,numberOfCombinations-1 do begin
    labelCombiMask[0,i] = combiIDs eq i
  endfor
  labelCombiMask[*,0] = 0 ; delete samples if all sensors are unavailable 
  labelCombiInit = labelCombiAll*labelCombiMask
  
  
  ; create synthetic samples
  labelCombiSynthetic = labelCombiInit
  for i=0,numberOfCombinations-1 do begin
    iReverse = numberOfCombinations-1-i
    labeledIndices = where(/NULL, labelCombiSynthetic[*,iReverse])
    if ~isa(labeledIndices) then continue
    sensorFlags = fix((binary(iReverse))[-numberOfSensors:-1])    
    foreach sensorFlag,sensorFlags,k do begin
      if sensorFlag eq 1 then begin
        sensorFlagsSubset = sensorFlags
        sensorFlagsSubset[k] = 0
        iSubset = total(sensorFlagsSubset*2l^reverse(indgen(numberOfSensors)), /INTEGER)
        if iSubset eq 0 then continue ; don't create samples with all sensors unavailable
        labelCombiSynthetic[labeledIndices,iSubset] = labelCombiSynthetic[labeledIndices,iReverse]
      endif
    endforeach
  endfor
  
  ; create feature stack
  featuresStack = make_array(numberOfSamples, 1, numberOfFeatures)
  featureOffset = [0, total(/CUMULATIVE, numberOfFeaturesList, /INTEGER)]
  for i=0,numberOfSensors-1 do begin
    featuresStack[0,0,featureOffset[i]] = transpose(hub_fix3d(features[i]),[0,2,1])
  endfor
  featureMeans = list()
  foreach iFeatures, features do begin
    featureMeans.add, mean(DIMENSION=1,iFeatures)
  endforeach
  featureStackMeans = reform(mean(DIMENSION=1,featuresStack))
  featuresStackCombi =  rebin(/SAMPLE, featuresStack, numberOfSamples, numberOfCombinations, numberOfFeatures)
  ; - mask out features (set value to feature mean)
  
  for i=0,numberOfCombinations-1 do begin
    sensorFlags = fix((binary(i))[-numberOfSensors:-1])
    for k=0,numberOfSensors-1 do begin
      if ~sensorFlags[k] then begin
        for m=featureOffset[k],featureOffset[k+1]-1 do begin
          featuresStackCombi[*,i,featureOffset[k]:featureOffset[k+1]-1] = featureStackMeans[m]
        endfor
      endif
    endfor
  endfor

  ; write output files
  ; - read images
  imageCubes = list()
  foreach imageFile,imageFiles do begin
    imageCubes.add, hubIOImg_readImage(imageFile)
  endforeach
  ; - read masks
  maskBands = list()
  foreach maskFile,maskFiles do begin
    maskBands.add, hubIOImg_readImage(maskFile)
  endforeach
  ; - mask images
  for i=0,numberOfSensors-1 do begin
    maskedIndices = where(/NULL, maskBands[i] eq 0)
    if ~isa(maskedIndices) then continue
    maskedIndices2d = array_indices(maskBands[i], maskedIndices)
    imageCube = imageCubes.remove(i)
    foreach meanValue, featureMeans[i],k do begin
      band = imageCube[*,*,k]
      band[maskedIndices] = meanValue
      imageCube[0,0,k] = band
    endforeach
    imageCubes.add, imageCube, i
  endfor
  ; - stack images 
  imageSize = (size(imageCube, /DIMENSIONS))[0:1]
  imageStack = make_array([imageSize,numberOfFeatures])
  for i=0,numberOfSensors-1 do begin
    imageStack[0,0,featureOffset[i]] = imageCubes[i]
  endfor
  
  ; - write image stack 
  hubIOImg_writeImage, imageStack, filepath('image', /TMP)
    
  ; - write sample set
  hubIOImg_writeImage, featuresStackCombi, filepath('featureCombi', /TMP)
  metaNames = ['classes','class lookup', 'class names', 'file type']
  metaValues = hubIOImg_getMeta(labelFile, metaNames)
  hubIOImg_writeImage, hub_fix3d(labelCombiInit), filepath('initLabelCombi', /TMP), MetaNames=metaNames, MetaValues=metaValues
  hubIOImg_writeImage, hub_fix3d(labelCombiSynthetic), filepath('finalLabelCombi', /TMP), MetaNames=metaNames, MetaValues=metaValues 

end