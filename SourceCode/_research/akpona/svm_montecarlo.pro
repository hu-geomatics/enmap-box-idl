pro svm_montecarlo

  enmapbox
  wd = 't:\temp\temp_ar\svm\'
  runs = 3

  oa  = list()
  k   = list()
  of1 = list()
  cf1 = list()
  
  for run=1,runs do begin
    print, 'run:', run
    tic
    p1 = DICTIONARY()
    p1.inputImage = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
    p1.outputSamples = wd+'train'
    p1.outputComplement = wd+'test'
    p1.stratificationMask = p1.inputImage
    p1.deleteStrataMask = 0b
    counts = [0, 10, 10, 10, 10, 10]
    p1.samplesPerStratum = long(counts)*80/100
    p1.seedChoice = 'systime'
    hubApp_randomSampling_processing, p1

    p2 = DICTIONARY()
    p2.featureFilename = hub_getTestImage('Hymap_Berlin-A_Image')
    p2.labelFilename = p1.outputSamples
    p2.modelFilename = wd+'model.svc'
    p2.svmType = 'svc'
    imageSVM_parameterize_processing, p2
    
    p3 = DICTIONARY()
    p3.featureFilename = p2.featureFilename
    p3.modelFilename = p2.modelFilename
    p3.outputLabelFilename = wd+'prediction'
    p3.maskFilename = p1.outputComplement
    imageSVM_apply_processing, p3
  
    p4 = DICTIONARY()
    p4['inputEstimation'] = p3.outputLabelFilename
    p4['inputReference'] = p1.outputComplement

    p4.accuracyType = 'classification'
    accAss = hubApp_accuracyAssessment_processing(p4, /NOSHOW)
    
    oa.add, accAss['imagePerformance','overallAccuracy']
    k.add, accAss['imagePerformance','kappaAccuracy']
    of1.add, accAss['imagePerformance','averageF1Accuracy']
    cf1.add, accAss['imagePerformance','f1Accuracy']
    toc
  endfor
  
  ; print single runs?
  print, 'number of runs:', runs
  if 1 then begin
    print, 'overallAccuracy', oa
    print, 'kappaAccuracy', k
    print, 'averageF1Accuracy', of1
    print, 'f1Accuracy', cf1
  endif
  print, '                          Mean            StdDev' 
  print, 'overallAccuracy   ', mean(oa.ToArray()),stddev(oa.ToArray())
  print, 'kappaAccuracy     ', mean(k.ToArray()),stddev(k.ToArray())
  print, 'averageF1Accuracy ', mean(of1.ToArray()),stddev(of1.ToArray())
  cf1Mean = mean(cf1.toarray(), DIMENSION=1)
  cf1Std =  stddev(cf1.ToArray(), DIMENSION=1)
;  for i=0b,8 do print, 'f1Accuracy', i+1b,'    ', cf1Mean[i], cf1Std[i] 

end