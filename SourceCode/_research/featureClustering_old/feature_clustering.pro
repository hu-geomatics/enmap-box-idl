pro feature_clustering_initialize_ $
  ,CLUSTER_INDEX=cluster_index $
  ,CLUSTER_SIMILARITY= cluster_similarity $
  ,DATA=data
  
  ;on_error,2
  nb = n_elements(data[*,0])
  ns = n_elements(data[0,*])
  cluster_index      = lindgen(nb)
  cluster_similarity = fltarr(nb,nb)-1
  
  str=['Number of Features = '+strcompress(/REMOVE_ALL,nb) $
      ,'Number of Samples  = '+strcompress(/REMOVE_ALL,ns)]
  aux_report_init,str,BASE=base,TITLE='Feature Clustering Initialization'
  for i=0UL,nb-2 do for k=i+1,nb-1 do begin
    aux_report_state,base,i,nb-2
    coef = 1 & eval = 1 & var = 1
    stop
    first_pc = pcomp(data[[i,k],*],COEFFICIENTS=coef,EIGENVALUES=eval,VARIANCES=var,NVARIABLES=1,/DOUBLE)
    cluster_similarity[i,k] = var[0]
    cluster_similarity[k,i] = var[0]
  endfor
  aux_report_init,BASE=base,/FINISH
end

pro feature_clustering_combine_most_similar_cluster_ $
  ,CLUSTER_INDEX=cluster_index $
  ,CLUSTER_SIMILARITY= cluster_similarity $
  ,DATA=data $
  ,R_MAX_SIMILARITY=max_similarity $
  ,R_C1=c1,R_C2=c2

  on_error,2
  
;find most similar clusters C1,C2
  nb = n_elements(cluster_index)
  max_similarity = max(cluster_similarity)
  tmp = (where(cluster_similarity eq max_similarity))[0]
  tmp = [tmp mod nb, tmp / nb]
  c1 = min(tmp)
  c2 = max(tmp)

;select all features of C1 and C2
  c12_features = where((cluster_index eq c1) or (cluster_index eq c2))

;update CLUSTER_INDEX and CLUSTER_SIMILARITY
  cluster_index[where(cluster_index eq c2)] = c1
  cluster_similarity[c1,*] = -1
  cluster_similarity[*,c1] = -1
  cluster_similarity[c2,*] = -1
  cluster_similarity[*,c2] = -1
  ;calculate similarity between C12 and old clusters
  for i_c=0UL,nb-1 do begin
    if i_c eq c1 then continue
    if i_c eq c2 then continue
    if (where(cluster_index eq i_c))[0] eq -1 then continue
    
    i_c_features = where(cluster_index eq i_c)
    coef = 1 & eval = 1 & var = 1
    first_pc = pcomp(data[[c12_features,i_c_features],*],COEFFICIENTS=coef,EIGENVALUES=eval,VARIANCES=var,NVARIABLES=1,/DOUBLE)
    cluster_similarity[c1,i_c] = var[0]
    cluster_similarity[i_c,c1] = var[0]
  endfor 
end

pro feature_clustering_ $
  ,FNAME=fname $
  ,NCLUSTER=ncluster $
  ,R_CLUSTER_INDEX=cluster_index
  
  if n_elements(ncluster) eq 0 then ncluster=1
  inputImage = hubIOImgInputImage(fname)
  data = inputImage.getCube()
  ns = inputImage.getMeta('samples')
  nl = inputImage.getMeta('lines')
  nb = inputImage.getMeta('bands')
  data = reform(/OVERWRITE,temporary(data),ns*nl,nb)
;random sample
  draw_sample = 1
  if draw_sample then begin
    k= 1000
    data = data[sampling_random_sample(N=ns*nl,K=k),0:nb-1:1]
    ns = k
  endif
  data = transpose(data) ;format is [nb,ns]
  

  nb=n_elements(data[*,0])

  feature_clustering_initialize $
    ,CLUSTER_INDEX=cluster_index $
    ,CLUSTER_SIMILARITY= cluster_similarity $
    ,DATA=data
  
  frame1 = '|'
  frame2 = '|'
  frame3 = '_'
  empty_dendro = replicate(' ',nb)
  
  report = $
    [fname $
    ,'Number of Features = '+strcompress(/REMOVE_ALL,nb) $
    ,'Number of Samples  = '+strcompress(/REMOVE_ALL,ns),'']
    
  i_cluster_index = cluster_index+1
  for i=0,2 do begin
    tmp = i_cluster_index / 10^(2-i)
    str_tmp=strjoin(strcompress(/REMOVE_ALL,tmp))
    if i eq 1 then str_tmp += '       explained variance
    if i eq 2 then str_tmp += '      (number of clusters) 
    print,str_tmp
    report = [report,str_tmp]
    i_cluster_index -= tmp*10^(2-i)
  endfor 

  str=['Number of Features = '+strcompress(/REMOVE_ALL,nb) $
      ,'Number of Samples  = '+strcompress(/REMOVE_ALL,ns)]
  aux_report_init,str,BASE=base,TITLE='Feature Clustering'
  for i=1,nb-1 do begin
    aux_report_state,base,i,nb-1
    feature_clustering_combine_most_similar_cluster_ $
      ,CLUSTER_INDEX=cluster_index $
      ,CLUSTER_SIMILARITY= cluster_similarity $
      ,DATA=data $
      ,R_MAX_SIMILARITY=max_similarity $
      ,R_C1=c1,R_C2=c2
;print,'Ähnlichkeitsmatrix (Schritt '+strcompress(/REMOVE_ALL,i)+') '
;print,byte(cluster_similarity*100>0)


    i_dendro = empty_dendro & i_dendro[c1] = frame1 & i_dendro[c2] = frame2 & i_dendro[cluster_index[uniq(cluster_index,sort(cluster_index))]] = frame1
    if c2-c1 gt 1 then i_dendro[c1+1:c2-1] = frame3
    
    str_tmp = strjoin(i_dendro)+string(max_similarity*100)+' ('+strcompress(/REMOVE_ALL,nb-i)+')'+' C1='+strcompress(/REMOVE_ALL,c1+1)+' C2='+strcompress(/REMOVE_ALL,c2+1)
    print,str_tmp
    report = [report,str_tmp]
    
;     print,strcompress(cluster_index)

    if nb-i eq ncluster then break
  endfor
  aux_report_init,BASE=base,/FINISH
  print,frame1
  report = [report,frame1]
  
  if keyword_set(show_report) then aux_widget_report,report,TITLE='Feature Clustering Report',XSIZE=1400,YSIZE=1000

  ;set cluster indexes to the 1...NCLUSTER range
  
  index_arr = cluster_index[uniq(cluster_index,sort(cluster_index))]
  index_flag = cluster_index*0
  for i=0ULL,n_elements(index_arr)-1 do begin
    tmp = where((cluster_index eq index_arr[i]) and (index_flag ne 1))
    cluster_index[tmp] = i+1
    index_flag[tmp] = 1
  endfor
  
end

pro test
  fname = hub_getTestImage('Hymap_Berlin-A_Image')
  ;ncluster = 5

  feature_clustering $
    ,FNAME=fname $
    ,NCLUSTER=ncluster $
    ,R_CLUSTER_INDEX=cluster_index
    

    print,cluster_index
  
end