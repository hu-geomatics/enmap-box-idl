pro test_r
  scriptFilename = 'Y:\temp\temp_held\testing\kMeans.R'
  parameters = hash($
    'inputFilePath', hub_getTestImage('Hymap_Berlin-A_Image'),$
    'numberOfClusters', 6,$
    'outputFilePath', filepath(/TMP, 'kmeans'),$
    'tmpFilename', filepath(/TMP, ''))
      
  hubR_runScript, scriptFilename, parameters, result, errorResult
  print, result
  print, transpose(errorResult)
  enmapBox_openImage, parameters['outputFilePath']
end