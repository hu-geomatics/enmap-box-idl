pro hubProEnvEnviTimeseriesViewer_event, event

  imageMenu = long(widget_info(event.id, /UNAME))
  widget_control, imageMenu, GET_UVALUE=viewer
  viewer.harvestWidgetStructureProfile ; check if z-profile exist

  ; call original ENVI event handler
  case event.id of
    viewer.info.id.image.draw : viewer.handleImageEvent, event
    viewer.info.id.scroll.draw : viewer.handleScrollEvent, event
    viewer.info.id.zoom.draw : viewer.handleZoomEvent, event
    viewer.info.id.profile.draw : viewer.handleProfileEvent, event
  endcase  
end