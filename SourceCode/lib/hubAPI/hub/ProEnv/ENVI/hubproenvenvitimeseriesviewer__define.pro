function hubProEnvEnviTimeseriesViewer::init, fid, fidRGB, _EXTRA=_extra
  self.info = dictionary()
  self.info._extra =_extra+dictionary()
  self.info.locationX = 0
  self.info.locationY = 0
  self.info.fid = dictionary()
  self.info.display = dictionary()
  self.info.pInfo = dictionary()
  self.info.displayNumber = dictionary()
  self.info.fid.timeseries = fid
  (self.info.fid)['rgb'] = isa(fidRGB) ? fidRGB : !null
  self.info.fid.chips1 = -999
  self.info.fid.chips2 = -999
  ; display timeseries
  self.info.displayNumber.timeseries = hubProEnvEnviHelper.newDisplay()
  envi_display_bands, self.info.fid.timeseries, 0
  self.harvestWidgetStructure
  ; display rgb
  if isa(self.info.fid.rgb) then begin
    self.info.displayNumber.rgb = hubProEnvEnviHelper.newDisplay()
    envi_display_bands, self.info.fid.rgb, [0,0,0]
    ; display chips
    self.info.displayNumber.chips1 = hubProEnvEnviHelper.newDisplay()
    self.info.displayNumber.chips2 = hubProEnvEnviHelper.newDisplay()
  endif
  !DEBUG_PROCESS_EVENTS=0
  return, 1b
end

pro hubProEnvEnviTimeseriesViewer::setBand, pos

  if self.profileAvailable() then begin
    ; add current date to plot xtitle
    envi_file_query, self.info.fid.timeseries, WL=decimalYears
    disp_get_location, self.info.displayNumber.timeseries, x, y
    time = hubTime(decimalYears[pos[0]])
    stime = time.syear+'-'+time.smonth+'-'+time.sday+' / '+time.syear+'-'+time.sdoy
    slocation = strcompress(/REMOVE_ALL, 'pixel:'+string(x+1)+','+string(y+1))
;    (*self.info.pInfo.profile).axis.title = ['Decimal Year ('+stime+')','Value']
    (*self.info.pInfo.profile).axis.title = [stime+' / '+slocation,'Value']

  endif
  
  hubProEnvEnviHelper.setDisplay, self.info.displayNumber.timeseries
  envi_display_bands, self.info.fid.timeseries, pos
  hubProEnvEnviHelper.setDisplay, self.info.displayNumber.rgb
  envi_display_bands, self.info.fid.rgb, pos
  
  self.drawImageChips
end

pro hubProEnvEnviTimeseriesViewer::drawImageChips

  compile_opt idl2
  
  disp_get_location, self.info.displayNumber.timeseries, x, y
  self.info.locationX = x
  self.info.locationY = y

  ; draw image chips
  envi_disp_query, self.info.displayNumber.timeseries, POS=pos
  print, pos; = pos-1
  envi_disp_query, self.info.displayNumber.timeseries, NS=ns, NL=nl
  envi_file_query, self.info.fid.timeseries, WL=decimalYears, NB=nb
  time = hubTime(decimalYears)
  chipSize = [151,151]
  chip = bytarr([chipSize,3])
  dims = [-1,x-chipSize[0]/2,x+chipSize[0]/2,y-chipSize[1]/2,y+chipSize[1]/2] > [-1,0,0,0,0] < ([0,ns,ns,nl,nl]-1)
  rgbDefaultStretch = self.info._extra.rgbDefaultStretch
  window, /FREE, XSIZE=chipSize[0], YSIZE=chipSize[1], /PIXMAP
  chipWindow = !D.WINDOW
  
  ; - draw inter-annual observations
  interAnnualDoys = hubTime(floor([min(decimalYears, MAX=max):max])+(decimalYears[pos[0]] mod 1))
  targetPoss = time.locate(interAnnualDoys, Distance=distance)
  stime = (strmid(time.stamp,0,10)+'/'+time.sdoy)[targetPoss]
  nchips = n_elements(targetPoss)
  ncols = ceil(sqrt(nchips))
  nrows = ceil(1.*nchips/ncols)
  cube = bytarr([chipSize*[ncols,nrows],3])
  cubeSize = size(/DIMENSIONS, cube)
  xoff = (indgen(nchips) mod ncols)*chipSize[0]
  yoff = (indgen(nchips) /   ncols)*chipSize[1]
  wset, chipWindow
  foreach targetPos, targetPoss, i do begin
    for channel=0,2 do begin
      band = envi_get_data(DIMS=dims, FID=(self.info.fid.rgb)[channel], POS=targetPos)
      band = bytscl(band, MIN=rgbDefaultStretch[0], MAX=rgbDefaultStretch[1])
      color = ([255b,0b,0b])[channel]
      if targetPos eq pos[0] then begin
        band[*,[[0:2],chipSize[1]-[3:1]]] = color
        band[[[0:2],chipSize[0]-[3:1]],*] = color
      endif
      band[chipSize[0]/2+1+[-5:5],chipSize[1]/2+1+[-5:5] ] = color
      band[chipSize[0]/2+1+[5:-5],chipSize[1]/2+1+[-5:5] ] = color
      chip[0,0,channel] = band
    endfor
    tv, chip, TRUE=3, /ORDER
    xyouts, 5, chipSize[1]-15, stime[i], /DEVICE, CHARTHICK=5, CHARSIZE=0.7, Color=0
    xyouts, 5, chipSize[1]-15, stime[i], /DEVICE, CHARTHICK=1, CHARSIZE=0.7
    chip = tvrd(TRUE=3,/ORDER)
    cube[xoff[i],yoff[i],0] = chip
  endforeach
  ;   - save as envi inMemory and display
  odlFidChips = self.info.fid.chips1
  envi_write_envi_file, temporary(cube), R_FID=fidChips, /IN_MEMORY, BNAMES=strmid((time.stamp)[targetPoss],0,10), INTERLEAVE=0, /NO_COPY, DEF_STRETCH=envi_default_stretch_create(/LINEAR, VAL1=0,  VAL2=255)
  self.info.fid.chips1 = fidChips
  if isa(where(/NULL, hubProEnvEnviHelper.getDisplays() eq self.info.displayNumber.chips1)) then begin
    hubProEnvEnviHelper.setDisplay, self.info.displayNumber.chips1
  endif else begin
    self.info.displayNumber.chips1 = hubProEnvEnviHelper.newDisplay()
  endelse
  envi_display_bands, fidChips, [0,1,2], IMAGE_OFFSET=[0,0], IMAGE_SIZE=cubeSize[0:1]+10, WINDOW_STYLE=2
  envi_file_mng, ID=odlFidChips, /REMOVE

  ; - draw intra-annual observations
  n = 10
  targetPoss = [pos[0]-n:pos[0]+n]
  stime = (strmid(time.stamp,0,10)+'/'+time.sdoy)[targetPoss]
  nchips = n_elements(targetPoss)
  cube = bytarr([chipSize*[2*n+1,1],3])
  cubeSize = size(/DIMENSIONS, cube)
  xoff = indgen(nchips)*chipSize[0]
  yoff = 0
  wset, chipWindow
  foreach targetPos, targetPoss, i do begin
    if targetPos lt 0 or targetPos ge nb then continue
    for channel=0,2 do begin
      band = envi_get_data(DIMS=dims, FID=(self.info.fid.rgb)[channel], POS=targetPos)
      band = bytscl(band, MIN=rgbDefaultStretch[0], MAX=rgbDefaultStretch[1])
      color = ([255b,0b,0b])[channel]
      if targetPos eq pos[0] then begin
        band[*,0:2] = color
        band[*,-3:-1] = color
        band[0:2,*] = color
        band[-3:-1,*] = color
      endif
      band[chipSize[0]/2+1+[-5:5],chipSize[1]/2+1+[-5:5] ] = color
      band[chipSize[0]/2+1+[5:-5],chipSize[1]/2+1+[-5:5] ] = color
      chip[0,0,channel] = band
    endfor
    tv, chip, TRUE=3, /ORDER
    xyouts, 5, chipSize[1]-15, stime[i], /DEVICE, CHARTHICK=5, CHARSIZE=0.7, Color=0
    xyouts, 5, chipSize[1]-15, stime[i], /DEVICE, CHARTHICK=1, CHARSIZE=0.7
    chip = tvrd(TRUE=3,/ORDER)
    cube[xoff[i],yoff,0] = chip
  endforeach
  odlFidChips = self.info.fid.chips2
  envi_write_envi_file, temporary(cube), R_FID=fidChips, /IN_MEMORY, BNAMES=['r','g','b'], INTERLEAVE=0, /NO_COPY, DEF_STRETCH=envi_default_stretch_create(/LINEAR, VAL1=0,  VAL2=255)
  self.info.fid.chips2 = fidChips
  if isa(where(/NULL, hubProEnvEnviHelper.getDisplays() eq self.info.displayNumber.chips2)) then begin
    hubProEnvEnviHelper.setDisplay, self.info.displayNumber.chips2
  endif else begin
    self.info.displayNumber.chips2 = hubProEnvEnviHelper.newDisplay()
  endelse
  envi_display_bands, fidChips, [0,1,2], IMAGE_OFFSET=[0,0], IMAGE_SIZE=cubeSize[0:1]+10, WINDOW_STYLE=2

  envi_file_mng, ID=odlFidChips, /REMOVE
  wdelete, chipWindow
  widget_control, self.info.id.image.draw, /INPUT_FOCUS

end

pro hubProEnvEnviTimeseriesViewer::setProfileDefaults

  envi_file_query, self.info.fid.timeseries, DEF_ZRANGE =def_zrange

  ; change tlb window title
  widget_control, self.info.id.profile.tlb, TLB_SET_TITLE='#'+strtrim(self.info.displayNumber.timeseries+1,2)+' Temporal Plot'


  pInfo = self.info.pInfo.profile
  (*pInfo).PTITLE = 'Temporal Plot'
  (*pInfo).axis.title = ['Decimal Year','Value']
  (*pInfo).MIN_VAL = def_zrange[0]+0.0001
  (*pInfo).MAX_VAL = def_zrange[1]
  pPlotParams = (*pInfo).p_params
  (*pPlotParams).COLOR = 1
  (*pPlotParams).LSTYLE = 0         ; 0=sold, 1=dotted, 2=dashed 3=dash dot, 4=dash dot dot, 5=long dashes, 6=no line
  (*pPlotParams).THICK = 1
  (*pPlotParams).PSYM = 4           ; 1=Plus sign (+), 2=Asterisk (*), 3=Period (.), 4=Diamond, 5=Triangle, 6=Square, 7=X
  (*pPlotParams).PSYMSIZE = 8
  (*pPlotParams).LFAC = 1           ; -1=show line, 1=hide line   

  ; redraw for correct initial plot
  envi_disp_query, self.info.displayNumber.timeseries, POS=pos
  self.setBand, pos
end

function hubProEnvEnviTimeseriesViewer::profileAvailable
  return, widget_info(/VALID_ID, self.info.id.profile.tlb)
end

;pro hubProEnvEnviTimeseriesViewer::showZProfile
;  stop; is not workin :-(
;  self.pressButton, ['Tools','Profiles','Z Profile (Spectrum)...']
;end

pro hubProEnvEnviTimeseriesViewer::harvestWidgetStructure

  common managed, ids, names, outermodal
  imageTLB = (ids[(where(/NULL, names eq 'disp'))])[0]
  imageMenu = (widget_info(imageTLB, /ALL_CHILDREN))[0]
  imageDraw = (widget_info(imageTLB, /ALL_CHILDREN))[1]

  scrollTLB = (ids[(where(/NULL, names eq 'scroll'))])[0]
  scrollDraw = (widget_info(scrollTLB, /ALL_CHILDREN))[0]
  
  zoomTLB = (ids[(where(/NULL, names eq 'zoom'))])[0]
  zoomDraw = (widget_info(zoomTLB, /ALL_CHILDREN))[0]

  ;overwrite event handler and set image menu id in uname of all draw widgets
  self.info.handler = dictionary()
  self.info.handler.image = widget_info(imageDraw, /EVENT_PRO)
  self.info.handler.scroll = widget_info(scrollTLB, /EVENT_PRO)
  self.info.handler.zoom = widget_info(zoomTLB, /EVENT_PRO)

  widget_control, imageDraw,  EVENT_PRO='hubProEnvEnviTimeseriesViewer_event', DRAW_WHEEL_EVENTS=1b, SET_UNAME=string(imageMenu)
  widget_control, scrollDraw, EVENT_PRO='hubProEnvEnviTimeseriesViewer_event', DRAW_WHEEL_EVENTS=1b, SET_UNAME=string(imageMenu)
  widget_control, zoomDraw,   EVENT_PRO='hubProEnvEnviTimeseriesViewer_event', DRAW_WHEEL_EVENTS=1b, SET_UNAME=string(imageMenu)

  ; store object reference inside image draw uvalue
  widget_control, imageMenu, SET_UVALUE=self

  self.info.id = dictionary()
  self.info.id.envi = dictionary()
  self.info.id.envi.availableBands = (ids[(where(/NULL, names eq 'envi_band_select'))])[0]
  self.info.id.envi.menu = (ids[(where(/NULL, names eq 'envi_misc'))])[0]
  self.info.id.image = dictionary()
  self.info.id.image.tlb = imageTLB
  self.info.id.image.menu = imageMenu
  self.info.id.image.draw = imageDraw
  self.info.id.scroll = dictionary()
  self.info.id.scroll.tlb = scrollTLB
  self.info.id.scroll.draw = scrollDraw
  self.info.id.zoom = dictionary()
  self.info.id.zoom.tlb = zoomTLB
  self.info.id.zoom.draw = zoomDraw
  self.info.id.profile = dictionary()
  self.info.id.profile.tlb = 0
  
end

pro hubProEnvEnviTimeseriesViewer::harvestWidgetStructureProfile

  if widget_info(/VALID_ID, self.info.id.profile.tlb) then return

  ; check for Z-profile plot and change the event handler
  common managed, ids, names, outermodal
  spTLBs = ids[where(/NULL, names eq 'sp')]
  foreach spTLB,spTLBs do begin
    if widget_info(spTLB,/VALID_ID) then begin
      widget_control, spTLB, GET_UVALUE=pInfo
      if self.info.displayNumber.timeseries eq (*pInfo).dn then begin
        profileTLB = spTLB
        break
      endif
    endif
  endforeach
  
  if isa(profileTLB) then begin
    self.info.id.profile.tlb = profileTLB
    self.info.id.profile.menu = (widget_info(profileTLB, /ALL_CHILDREN))[0]
    self.info.id.profile.draw = (widget_info(profileTLB, /ALL_CHILDREN))[1]
    widget_control, self.info.id.profile.draw, GET_VALUE=drawWindow
    self.info.id.profile.drawWindow = drawWindow
    
    self.info.handler.profile = widget_info(self.info.id.profile.draw, /EVENT_PRO)
    widget_control, self.info.id.profile.draw, EVENT_PRO='hubProEnvEnviTimeseriesViewer_event', DRAW_WHEEL_EVENTS=1b, SET_UNAME=string(self.info.id.image.menu)
    ; save info pointer
    widget_control, profileTLB, GET_UVALUE=pInfo
    self.info.pInfo.profile = pInfo
    ; set zprofile XYSTR string to current band
    if self.profileAvailable() then begin
      envi_disp_query, self.info.displayNumber.timeseries, POS=pos
      (*self.info.pInfo.profile).xystr = '!3'+strtrim(pos[0]+1,2)+':0.,0.'
    endif
    self.setProfileDefaults
  endif

end

pro hubProEnvEnviTimeseriesViewer::pressButton, path, menu
  if ~isa(menu) then begin
    menu = self.info.id.disp.menu
  endif
  
  foreach button,widget_info(/ALL_CHILDREN, menu) do begin 
    widget_control, button, GET_VALUE=value, GET_UVALUE=uvalue 
    if value eq path[0] then begin 
      print, value, uvalue 
      if keyword_set(uvalue) then begin
       ; widget_control, button, EVENT_PRO='myEvent'
        event = {WIDGET_BUTTON, ID:button, TOP:self.info.id.disp.menu, HANDLER:button, SELECT:1}
        stop
        profile_event, event
       stop
        call_procedure, widget_info(button,/EVENT_PRO), event
      endif else begin
        self.pressButton,path[1:*], button
      endelse
      return
    endif
  endforeach 
  stop
end

pro hubProEnvEnviTimeseriesViewer::handleImageEvent, event
  self.handleEvent, event
  call_procedure, self.info.handler.image, event
end

pro hubProEnvEnviTimeseriesViewer::handleScrollEvent, event
  self.handleEvent, event
  call_procedure, self.info.handler.scroll, event
end

pro hubProEnvEnviTimeseriesViewer::handleZoomEvent, event
  self.handleEvent, event
  call_procedure, self.info.handler.zoom, event
end

pro hubProEnvEnviTimeseriesViewer::handleProfileEvent, event

  ; call ENVI event handler first to update the info structure
  call_procedure, self.info.handler.profile, event

  ; handle mouse wheel events
  if event.type eq 7 then begin
    ; set new band
    envi_disp_query, self.info.displayNumber.timeseries, FID=fid, POS=pos
    envi_file_query, fid, NB=nb
    newPos = (pos-event.clicks) > 0 < (nb-1)
    self.setBand, newPos
    widget_control, event.id, /INPUT_FOCUS
    ; set zprofile XYSTR string to new band
    if self.profileAvailable() then begin
      (*self.info.pInfo.profile).xystr = '!3'+strtrim(newPos[0]+1,2)+':0.,0.'
    endif
  endif

  ; update image band
  envi_disp_query, self.info.displayNumber.timeseries, POS=pos, FID=fid
  envi_file_query, fid, NB=nb, WL=wl, DEF_ZRANGE=defaultZRange
  XYSTR = (*self.info.pInfo.profile).XYSTR
  selectedPos = fix(strmid(XYSTR, 2,strpos(XYSTR,':')-2))-1
  if selectedPos ne pos[0] then begin
    ; set new band
    self.setBand, selectedPos
    widget_control, event.id, /INPUT_FOCUS
  endif

end

pro hubProEnvEnviTimeseriesViewer::handleEvent, event_

  compile_opt idl2

  event = (dictionary('TYPE',-1, 'ENTER', 0)+event_).toStruct()
  self.harvestWidgetStructureProfile
  
  ; set input fokus on enter
  if event.enter eq 1 then begin
    widget_control, event.id, /INPUT_FOCUS
  endif

  ; handle mouse wheel events
  if event.type eq 7 then begin
    ; set new band
    envi_disp_query, self.info.displayNumber.timeseries, FID=fid, POS=pos
    envi_file_query, fid, NB=nb
    newPos = (pos-event.clicks) > 0 < (nb-1)
    self.setBand, newPos
    widget_control, event.id, /INPUT_FOCUS
    ; set zprofile XYSTR string to new band
    if self.profileAvailable() then begin
      (*self.info.pInfo.profile).xystr = '!3'+strtrim(newPos[0]+1,2)+':0.,0.'
    endif
  endif

  disp_get_location, self.info.displayNumber.timeseries, x, y
  if self.info.locationX ne x or self.info.locationY ne y then begin
    self.drawImageChips
  endif  

end


pro hubProEnvEnviTimeseriesViewer::getProperty, info=info
  if arg_present(info) then info = self.info
end

pro hubProEnvEnviTimeseriesViewer__define
  struct = {hubProEnvEnviTimeseriesViewer, inherits IDL_Object, info:obj_new()}
end