;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the header filename for a given ENVI data file.
; 
; :Params:
;    filenameData: in, required, type=filename
;      Filename of the ENVI data file.
;-
function hubProEnvEnviHelper::findHeaderFile, filenameData, AllowNonexistent=allowNonexistent
  compile_opt static, strictarr
  ; check data file
  
  if ~file_test(filenameData) then begin
    if keyword_set(allowNonexistent) then begin
      return, ' '
    endif else begin
      message, 'Can not find image data file: '+filenameData
    endelse
  endif
  
  ; look for header file
  ; option 1: filenameHeader = <filenameData>+'.hdr' (extension in upper or lower letters)
  filenameHeader = filenameData+'.hdr'
  if file_test(filenameHeader) then return, filenameHeader
  filenameHeader = filenameData+'.HDR'
  if file_test(filenameHeader) then return, filenameHeader
   
  ; option 2: filenameHeader = <filenameData without extension>+'.hdr' (extension in upper or lower letters)
  filenameDataNoExtension =hubHelper.removeFileExtension(filenameData)
  filenameHeader = filenameDataNoExtension+'.hdr'
  if file_test(filenameHeader) then return, filenameHeader
  filenameHeader = filenameDataNoExtension+'.HDR'
  if file_test(filenameHeader) then return, filenameHeader

  if keyword_set(allowNonexistent) then return, ' '

  message, 'Can not find image header file for image data file: '+filenameData

end

function hubProEnvEnviHelper::findDataFile, filenameHeader, AllowNonexistent=allowNonexistent
  compile_opt static, strictarr
 
  if ~file_test(filenameHeader) then begin
    if keyword_set(allowNonexistent) then begin
      return, ' '
    endif else begin
      message, 'Can not find image header file: '+filenameHeader
    endelse
  endif
  
  
  ; look for data file
  ; option 1: filenameData = filenameHeader without extension
  filenameData = hubHelper.removeFileExtension(filenameHeader)
 ; filenameData = helper.removeFileExtension(filenameHeader)

  if file_test(filenameData) then return, filenameData
  
  ; option 2: filenameData plus interleave extensions
  interleaveExtensions = '.'+['bip','bil','bsq','BIP','BIL','BSQ']
  foreach extension, interleaveExtensions do begin
    if file_test(filenameData+extension) then return, filenameData+extension
  endforeach
  
  if keyword_set(allowNonexistent) then return, ' '

  message, 'Can not find image data file for image header file: '+filenameHeader
  
end

;+
; :Description:
;    Opens an image file inside the ENVI.
;
; :Params:
;    filename: in, required, type=filename
;-
pro hubProEnvEnviHelper::openImage, filename
  compile_opt static, strictarr
  if hubProEnvEnviHelper.isEnviSession() then begin
    forward_function envi_open_file
    forward_function envi_get_file_ids
        
    fids = envi_get_file_ids()  
    if fids[0] ne -1 then begin  
      for i=0,n_elements(fids)-1 do begin  
         envi_file_query, fids[i], FNAME=currentFilename  
         if strcmp(filename, currentFilename) then begin
           envi_file_mng, ID=fids[i], /REMOVE
         endif
      endfor  
    endif
    envi_open_file, filename
  endif

end

;+
; :Description:
;    Opens a non-image file inside the ENVI.
;
; :Params:
;    filename: in, required, type=filename
;-
pro hubProEnvEnviHelper::openFile, filename
  compile_opt static, strictarr
  ; cannot open non-image files in ENVI
end

pro hubProEnvEnviHelper::closeFile, filename
  compile_opt static, strictarr
  if hubProEnvEnviHelper.isEnviSession() and isa(filename) then begin
;    message, 'not implemented' 
  endif
end

function hubProEnvEnviHelper::getDisplays, ButtonIDs=buttonIDs, TopID=bandlistTLB
  compile_opt static, idl2
  
  common managed, ids, names, outermodal
  bandlistTLB = ids[where(/NULL, names eq 'envi_band_select')]
  if ~isa(bandlistTLB) || ~widget_info(/VALID_ID, bandlistTLB) then begin
    message, 'Available Bands List not available.'
  endif
  
  lastBase = (widget_info(bandlistTLB, /ALL_CHILDREN))[-1]
  menu = (widget_info(widget_info(lastBase, /ALL_CHILDREN),/ALL_CHILDREN))[1]
  buttonIDs = widget_info(menu, /ALL_CHILDREN)

  ; check if display number existes
  displayNumbers = list(-1)
  for i=1,n_elements(buttonIDs)-1 do begin
    widget_control, buttonIDs[i], GET_UVALUE=uvalue
    displayNumbers.add, fix(strmid(uvalue, 11))-1
  endfor

  return, displayNumbers.toArray()
end

pro hubProEnvEnviHelper::setDisplay, displayNumber
  compile_opt static, idl2
  
;  common managed, ids, names, outermodal
;  bandlistTLB = ids[where(/NULL, names eq 'envi_band_select')]
;  lastBase = (widget_info(bandlistTLB, /ALL_CHILDREN))[-1]
;  menu = (widget_info(widget_info(lastBase, /ALL_CHILDREN),/ALL_CHILDREN))[1]
;  menuButtons = widget_info(menu, /ALL_CHILDREN)
;  
  ; check if display number existes
  displayNumbers = hubProEnvEnviHelper.getDisplays(ButtonIDs=buttonIDs, TopID=topID)
  buttonID =  buttonIDs[where(/NULL, displayNumbers eq displayNumber)]
  if ~isa(buttonID) then begin
    message, 'Display '+strtrim(displayNumber,2)+' not available.'
  endif

  event = {ID:buttonID,$
           TOP:topID,$
           HANDLER:topID,$
           SELECT:1}

  envi_band_select_event, event
;      
;  stop
;  missingDisplays = displayNumber-n_elements(menuButtons)+2
;  for i=0,missingDisplays do begin
;    hubProEnvEnviHelper.setDisplayNumber, -1 ; press "New Display"
;  endfor
;  menuButtons = widget_info(menu, /ALL_CHILDREN)
;  

end

function hubProEnvEnviHelper::newDisplay
  compile_opt static, idl2
  oldDisplays = hubProEnvEnviHelper.getDisplays()
  oldDisplays = [oldDisplays, -1] ; fill dummy for comparison
  hubProEnvEnviHelper.setDisplay, -1 ; create new display
  newDisplays = hubProEnvEnviHelper.getDisplays()
  displayNumber = newDisplays[(where(newDisplays ne oldDisplays))[0]]
  return, displayNumber
end

;+
; :Description:
;    Returns 1 if ENVI is running and 0 otherwise.
;-
function hubProEnvEnviHelper::isEnviSession
  compile_opt static, strictarr
  common managed, ids, names, outermodal
  
  if ~isa(names) then begin
    isEnviSession = 0b
  endif else begin
    isEnviSession = isa(where(/NULL, string(names) eq 'envi_misc'))
  endelse
  return, isEnviSession

end 

;+
; :Description:
;    Returns an array of image or speclib filenames that are currently opened inside the ENVI.
;
; :Keywords:
;    Image: in, optional, type=boolean
;      Return all open images.
;      
;    SpectralLibrary: in, optional, type=boolean
;      Return all open SpecLibs.
;-
function hubProEnvEnviHelper::getFilenames $
  ,Image=image $
  ,SpectralLibrary=spectralLibrary
  
  compile_opt static, strictarr
  fileNames = []
  forward_function envi_get_file_ids
  fids = envi_get_file_ids() 
  if fids[0] ne -1 then begin
    for i=0, n_elements(fids)- 1 do begin
      envi_file_query, fids[i], FNAME=fname, FILE_TYPE=file_type
      if file_type eq 4 then begin ; (=SpecLib)
      
        if keyword_set(spectralLibrary) then begin
          fileNames = [fileNames, fname]
        endif  
      
      endif else begin
      
        if keyword_set(image) then begin
          fileNames = [fileNames, fname]
        endif
      
      endelse
    endfor
  endif

  return, fileNames

end 

;+
; :Description:
;    Returns a structure with button event information of an executed application
;    (executed from the EnMAP-Box menu). 
;    Note that this information is defined inside the menu file, like for example for::
;      2 {hubAPI} {%libdir%/hubAPI/help/idldoc/index.html} {enmapBoxUserApp_showHTML_event}
;    the result structure would be::
;      result.name     = 'hubAPI'
;      result.argument = '%libdir%/hubAPI/help/idldoc/index.html'
;      result.handler  = 'enmapBoxUserApp_showHTML_event'
; 
; :Params:
;    event: in, required, type=event structure
;-
function hubProEnvEnviHelper::getMenuEventInfo $
  , event

  compile_opt static, strictarr
  widget_control, event.id, GET_VALUE=name
  widget_control, event.id, GET_UVALUE=argument
  handler = widget_info(/EVENT_PRO, event.id)
  return, {name:name, argument:argument, handler:handler}
  
end

;+
; :Hidden:
;-
pro hubProEnvEnviHelper__define
  struct = {hubProEnvEnviHelper $
    , inherits IDL_Object $
  }
end