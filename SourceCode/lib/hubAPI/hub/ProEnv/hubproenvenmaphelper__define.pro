;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Opens a non-image file inside the EnMAP-Box.
;
; :Params:
;    filename: in, required, type=filename
;-
pro hubProEnvEnmapHelper::openFile, filename
  compile_opt static, strictarr
  if hubProEnvEnmapHelper.isEnmapSession() and isa(filename) then begin
    (enmapBox()).openFile, filename
  endif
end

;+
; :Description:
;    Opens an image file inside the EnMAP-Box.
;
; :Params:
;    filename: in, required, type=filename
;-
pro hubProEnvEnmapHelper::openImage, filename
  compile_opt static, strictarr
  if hubProEnvEnmapHelper.isEnmapSession() and isa(filename) then begin
    (enmapBox()).openImage, filename
  endif
end

;+
; :Description:
;    Closes a file inside the EnMAP-Box.
;
; :Params:
;    filename: in, required, type=filename
;-
pro hubProEnvEnmapHelper::closeFile, filename
  compile_opt static, strictarr
  if hubProEnvEnmapHelper.isEnmapSession() and isa(filename) then begin
    (enmapBox()).closeFile, filename
  endif
end

pro hubProEnvEnmapHelper::print, text
  compile_opt static, strictarr
  if hubProEnvEnmapHelper.isEnmapSession() then begin
    (enmapBox()).print, text
  endif
end

;+
; :Description:
;    Returns an array of image, speclib or other filenames that are currently opened inside the EnMAP-Box.
;
; :Keywords:
;    Image: in, optional, type=boolean
;      Return all open images.
;      
;    SpectralLibrary: in, optional, type=boolean
;      Return all open SpecLibs.
;      
;    Type: in, optional, type=string
;      Return all files of a specific type.
;
;-
function hubProEnvEnmapHelper::getFilenames, Image=image, SpectralLibrary=spectralLibrary, Type=type
  enmapBox = enmapBox()
  filelist = enmapBox.filelist
  
  compile_opt static, strictarr
  files = list()
  if keyword_set(image) then begin
    files = files + filelist.getFiles('images')
  endif
  if keyword_set(spectralLibrary) then begin
    files = files + filelist.getFiles('speclibs')
  endif
  if isa(type) then begin
    files = files + filelist.getFiles(type)
  endif
  filenames = list()
  foreach header, files do begin
    filenames.add, header.getMeta('filename data')
  endforeach
  return, filenames.toArray()
end 

;+
; :Description:
;    Returns 1 if the EnMAP-Box is running and 0 otherwise.
;-
function hubProEnvEnmapHelper::isEnmapSession
  compile_opt static, strictarr
  isEnmapSession = xregistered('enmapBox', /NOSHOW) ne 0
  return, isEnmapSession
end 

;+
; :Description:
;    Returns a structure with button event information of an executed application
;    (executed from the EnMAP-Box menu). 
;    Note that this information is defined inside the menu file, like for example for::
;      2 {hubAPI} {%libdir%/hubAPI/help/idldoc/index.html} {enmapBoxUserApp_showHTML_event}
;    the result structure would be::
;      result.name     = 'hubAPI'
;      result.argument = '%libdir%/hubAPI/help/idldoc/index.html'
;      result.handler  = 'enmapBoxUserApp_showHTML_event'
;	
; :Params:
;    event: in, required, type=event structure
;-
function hubProEnvEnmapHelper::getMenuEventInfo $
  , event
  
  compile_opt static, strictarr
  widget_control, event.id, GET_UVALUE=uvalue
  return, {name:uvalue['name'], argument:uvalue['argument'], handler:uvalue['handler']}
   
end

;+
; :Hidden:
;-
pro hubProEnvEnmapHelper__define
  struct = {hubProEnvEnmapHelper, inherits IDL_Object}
end

pro test_hubProEnvEnmapHelper__getFilenames 
  ; open some files manually inside the box
  ; ...
  
  ; query opened files  
  filenames = (hubProEnvEnmapHelper()).getFilenames(Image=0, SpectralLibrary=0, Type='*.svr')
  print, (filenames)
end