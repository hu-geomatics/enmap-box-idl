;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the name of the current processing environment. ::
;    
;      name  | processing environment
;      ------|-----------------------
;      enmap | EnMAP-Box session
;      envi  | ENVI session
;      idl   | pure IDL 
;
;-
function hubProEnvHelper::getEnvironmentType
  compile_opt static, strictarr
  if (hubProEnvEnmapHelper()).isEnmapSession() then begin
    environmentType = 'enmap'
  endif else begin
    if (hubProEnvEnviHelper()).isEnviSession() then begin
      environmentType = 'envi'
    endif else begin
      environmentType = 'idl'
    endelse
  endelse

  return, environmentType

end

;+
; :Description:
;    Opens an image file inside the current processing environment.
;    
; :Params:
;    filename: in, required, type=filename
;-
pro hubProEnvHelper::openImage, filename
  compile_opt static, strictarr
  hubProEnvEnmapHelper.openImage, filename
  hubProEnvEnviHelper.openImage, filename
end

pro hubProEnvHelper::openFile, filename
  compile_opt static, strictarr
  hubProEnvEnmapHelper.openFile, filename
  hubProEnvEnviHelper.openFile, filename
end

pro hubProEnvHelper::closeFile, filename
  compile_opt static, strictarr
  hubProEnvEnmapHelper.closeFile, filename
  hubProEnvEnviHelper.closeFile, filename
end

pro hubProEnvHelper::manageImage, filenameData, newFilenameData, Copy=copy, Move=move, Delete=delete
  compile_opt static, strictarr
  widget_control, /HOURGLASS
  
  filenameHeader = (hubProEnvEnviHelper()).findHeaderFile(filenameData, /AllowNonexistent)
  filenamePyramidDirectory = hubIOImgImagePyramid_getDirectory(filenameData, Exists=pyramidExists)

  if keyword_set(delete) then begin
    pyramidFilenames = file_search(filenamePyramidDirectory, '*')
    if pyramidFilenames[0] ne '' then begin
      file_delete, pyramidFilenames, /ALLOW_NONEXISTENT, /NOEXPAND_PATH
    endif
    file_delete, filenameData, filenameHeader, filenamePyramidDirectory, /ALLOW_NONEXISTENT, /NOEXPAND_PATH
    hubProEnvHelper.closeFile, filenameData
    return
  endif

  newFilenameHeader = newFilenameData+'.hdr'
  newFilenamePyramidDirectory = hubIOImgImagePyramid_getDirectory(newFilenameData)
  
  if filenameData eq newFilenameData then return

  dirname = file_dirname(newFilenameData)
  if ~file_test(dirname, /DIRECTORY) then begin
    file_mkdir, dirname, /NOEXPAND_PATH
  endif
  
  if keyword_set(copy) then begin
    hubProEnvHelper.deleteImage, newFilenameData
    file_copy, filenameData, newFilenameData, /NOEXPAND_PATH, /ALLOW_SAME
    file_copy, filenameHeader, newFilenameHeader, /NOEXPAND_PATH, /ALLOW_SAME
    if pyramidExists then begin
      file_copy, filenamePyramidDirectory, dirname, /NOEXPAND_PATH, /ALLOW_SAME, /RECURSIVE, /REQUIRE_DIRECTORY
    endif
  endif

  if keyword_set(move) then begin
    hubProEnvHelper.deleteImage, newFilenameData
    file_move, filenameData, newFilenameData, /NOEXPAND_PATH, /ALLOW_SAME 
    file_move, filenameHeader, newFilenameHeader, /NOEXPAND_PATH, /ALLOW_SAME
    if pyramidExists then begin
      file_move, filenamePyramidDirectory, dirname, /NOEXPAND_PATH, /ALLOW_SAME, /REQUIRE_DIRECTORY
    endif
  endif
end

pro hubProEnvHelper::copyImage, filenameData, newFilenameData
  compile_opt static, strictarr
  hubProEnvHelper.manageImage, filenameData, newFilenameData, /Copy
end

pro hubProEnvHelper::moveImage, filenameData, newFilenameData
  compile_opt static, strictarr
  hubProEnvHelper.manageImage, filenameData, newFilenameData, /Move
end

pro hubProEnvHelper::deleteImage, filenameData
  compile_opt static, strictarr
  hubProEnvHelper.manageImage, filenameData, /Delete
end


;+
; :Description:
;    Returns an array of image, speclib or other filenames that are opened 
;    in the current processing environment.
;
; :Keywords:
;    Image: in, optional, type=boolean
;      Return all open images.
;      
;    SpectralLibrary: in, optional, type=boolean
;      Return all open SpecLibs.
;      
;    Type: in, optional, type=string
;      Return all files of a specific type.
;-
function hubProEnvHelper::getFilenames $
  ,Image=image $
  ,SpectralLibrary=spectralLibrary $
  ,Type=type 
  
  compile_opt static, strictarr
  case hubProEnvHelper.getEnvironmentType()  of
    'enmap' : return, hubProEnvEnmapHelper.getFilenames(Image=image, SpectralLibrary=spectralLibrary, Type=type)
    'envi'  : return, hubProEnvEnviHelper.getFilenames(Image=image, SpectralLibrary=spectralLibrary)
    else    : return, []
  endcase
end 

;+
; :Description:
;    Returns a structure with button event information of an executed application
;    (executed from the EnMAP-Box or ENVI menu). 
;    Note that this information is defined inside the menu file, like for example for::
;      2 {hubAPI} {%libdir%/hubAPI/help/idldoc/index.html} {enmapBoxUserApp_showHTML_event}
;    the result structure would be::
;      result.name     = 'hubAPI'
;      result.argument = '%libdir%/hubAPI/help/idldoc/index.html'
;      result.handler  = 'enmapBoxUserApp_showHTML_event'
; 
; :Params:
;    event: in, required, type=event structure
;-
function hubProEnvHelper::getMenuEventInfo $
  , event
  
  compile_opt static, strictarr
  case hubProEnvHelper.getEnvironmentType()  of
    'enmap' : menuEventInfo = hubProEnvEnmapHelper.getMenuEventInfo(event)
    'envi'  : menuEventInfo = hubProEnvEnviHelper.getMenuEventInfo(event)
  endcase

  return, menuEventInfo
  
end

;+
; :Hidden:
;-
pro hubProEnvHelper__define
  struct = {hubProEnvHelper $
    , inherits IDL_Object $
    , enviHelper : obj_new() $
    , enmapHelper : obj_new() $
  }
end