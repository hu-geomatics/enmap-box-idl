;+
; This class is a wrapper to the IDL direct graphics window. It stores the window ID and properly 
; set it before any interaction with the associated window takes place. This is important if you 
; want to draw multiple plots in parallel.
; 
; :Examples:
;    Here is an example:: 
;      
;      ; create a new window of size 300x300
;      
;      oWindow = hubGraphicWindow(XSIZE=300, YSIZE=300)
;      
;      ; plot some stuff to it using normal IDL direct graphics routines 
;      
;      x = findgen(10)
;      y = x^2
;      oWindow.call, 'plot', x, y
;      oWindow.call, 'oplot', x, y, PSYM=1
;      
;      ; read the window content
;      
;      image = oWindow.tvrd(TRUE=3)
;      help, image
;      
;      ; destroy the object (and the IDL window)
;      
;      oWindow.cleanup
;      
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    The constructor calls the IDL `window` procedure with keyword `FREE` and stores the window
;    ID. 
;
; :Keywords:
;    _REF_EXTRA
;      All keywords are passed to `window`.
;
;-
function hubGraphicWindow::init $
  , _REF_EXTRA=_extra
  window, _EXTRA=_extra, /FREE
  self.windowID = !D.window
  return, 1b
  
end

;+
; :Description:
;    Wrapper to IDL `tvrd` function.
;
; :Keywords:
;    _REF_EXTRA
;      All keywords are passed to `tvrd`.
;-
function hubGraphicWindow::tvrd $
  , _REF_EXTRA=_extra

  wset, self.windowID
  return, tvrd(_EXTRA=_extra)
end

;+
; :Description:
;    Generic wrapper for calling direct graphics procedures.
;
; :Params:
;    procedure: in, required, type=procedure name
;      Name of the direct graphics procedure
;    a1
;      First argument.
;    a2
;      Second argument.
;    a3
;      Third argument.  
;
; :Keywords:
;    _REF_EXTRA
;      All keywords are passed to the direct graphics procedures.
;
;-
pro hubGraphicWindow::call $
  , procedure, a1, a2, a3 $
  , _REF_EXTRA=_extra
  
  wset, self.windowID
  case n_params()-1 of
    0 : call_procedure, procedure,                     _EXTRA=_extra
    1 : call_procedure, procedure, a1,                 _EXTRA=_extra
    2 : call_procedure, procedure, a1, a2,             _EXTRA=_extra
    3 : call_procedure, procedure, a1, a2, a3,         _EXTRA=_extra
  endcase  
  
end

;+
; :Description:
;    Deletes the window.
;-
pro hubGraphicWindow::cleanup

  wdelete, self.windowID
  
end

;+
; :Hidden:
;-
pro hubGraphicWindow__define
  struct = {hubGraphicWindow $
    , inherits IDL_Object $
    , windowID : 0l $
  }
end

;+
; :Hidden:
;-
pro test_hubGraphicWindow
;  cgPlot,[1,2], [5,9], background='Green', color='blue'
;  stop

  @huberrorcatch
  win1 = hubGraphicWindow(Pixmap=0, XSIZE=300, YSIZE=300, RETAIN=1)
  
 image = win1.tvrd(true=3)
  win1.cleanup
 tv,image,True=3
 return
  image3D = [ [[r[image]]], [[g[image]]], [[b[image]]] ]
  win1.destroy
  window, /FREE
  help, image1, image2, image3
  tv, image1
  tv, image2
  tv, image3
 ; tv, image2
end