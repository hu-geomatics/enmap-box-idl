;+
; :Description:
;    This routine allows to store values in a way that they can be returned even after the 
;    your application has been finished. Use `hub_getAppState` to return the values.  
;       
;
; :Params:
;    application: in, required, type = string
;     Name of your application.
;     
;    appStateKey: in, required, type = string
;     Key that identifies the `appStateValue`.
;     
;    appStateValue: in, required, type = any
;     This is the value, e.g. a single variable, you want to store.
;     
;  :Examples:
;     
;     
;
;-
pro hub_setAppState, application, appStateKey, appStateValue
  
  ;checks
  if ~isa(application)   then message, "'application' undefined"
  if ~isa(appStateKey)   then message, "'appStateKey' undefined"
  
  defsysv, '!hubAppStates', EXISTS = exists
  if ~exists then begin
    defsysv, '!hubAppStates', {appStates:Hash()}
  endif
  
  appStates = !hubAppStates.appStates
  
  if ~appStates.hasKey(application) then begin
    appStates[application] = Hash()
  endif
  
  (appStates[application])[appStateKey] = appStateValue
    
end



