
function hubOTBwrapper_codeGenerator_getModuleInfo, otbWrapper, filter
  moduleFiles = FILE_SEARCH(otbWrapper.getAppDirectory(), 'otbapp_*')

  if isa(filter) then moduleFiles = moduleFiles[where(stregex(FILE_BASENAME(moduleFiles), filter, /BOOLEAN, /FOLD_CASE) eq 1, /NULL)]
  if ~isa(moduleFiles) || N_ELEMENTS(moduleFiles) eq 0 || moduleFiles[0] eq '' then message, 'No OTB modules/applications found'
  ; moduleFiles = moduleFiles[0:1]

  moduleNames = strmid(FILE_BASENAME(moduleFiles), 7)
  moduleNames = stregex(moduleNames, '^[^.]+', /EXTRACT)
  moduleKeys = STRLOWCASE(moduleNames)
 
  
  moduleInfos = DICTIONARY()
  foreach moduleKey, modulekeys, i do begin
    print, format='(%"Load info for module %i \"%s\"")',i+1, moduleKey 
    helpCmd = otbWrapper.getAppLauncherPath() + ' '+moduleNames[i]
    spawn, helpCmd, result, info


    moduleParameters = list()
    
    infoText = info[2:*]
    j = 0
    foreach line, info do begin
      if stregex(info[j], '^Parameters', /BOOLEAN) then break
      j++
      
    endforeach
    if j EQ N_ELEMENTS(info) then begin
      print, info
      continue
    endif
      
    ;repeat j++ until stregex(info[j], '^Parameters', /BOOLEAN)
    moduleDescription = info[2:j-1]
    
    moduleDocumentationLink = ''
    iLine = where(stregex(moduleDescription, 'Complete documentation:[ ]*http', /BOOLEAN), NULL)
    if isa(iLine) then begin
      moduleDocumentationLink = stregex(moduledescription[iLine], '(http|www).*', /EXTRACT, /FOLD_CASE)
      moduleDocumentationLink = strtrim(moduleDocumentationLink,2)
    endif
    
    j++
    len = n_elements(info)
    while ~stregex(info[j], '^([ ]+|Examples.*)$', /BOOLEAN) || j ge len do begin
      line = STRTRIM(info[j])
      if STRLEN(line) eq 0 then break
      parts = strsplit(line, '[<> ]', /REGEX, /EXTRACT)
      if ~strcmp(parts[0],'MISSING', /FOLD_CASE) then begin
        parts = ['OPTIONAL',parts]
      endif
      
      
      otbName = strmid(parts[1], 1)
      otbType = parts[2]
      case strlowcase(otbType) of
        'float': idlType = 'float'
        'double': idlType = 'double'
        'int32'   : idlType = 'int'
        'int64'   : idlType = 'long'
        'boolean' : idlType = 'byte'
        'string'  : idlType = 'string'
        else: stop
      endcase
      isRequired = strcmp(parts[0], 'MISSING',  /FOLD_CASE)
      idlName = strjoin(strsplit(otbname, '[.]', /REGEX, /EXTRACT),'_')
      if stregex(idlName, '^[0123456789]', /BOOLEAN) then begin
        idlName = '_'+idlName
      endif
      
      otbDescription = strjoin(parts[3:*], ' ') 
      otbDefault = '' ;no default value string
      tmp = stregex(line, 'default value[^)]*', /EXTRACT, /FOLD_CASE) 
      if strlen(tmp) gt 0 then begin
        prefix = stregex(tmp, 'default value( is)?[ ]*', /EXTRACT)
        otbDefault = strmid(tmp, strlen(prefix), strlen(line)-1)
      endif
      
      
      ;if not empty, this list stores possible values/options
      otbOptions = stregex(line, '\[[^=]+(/[^=]+)+\]', /EXTRACT)
      otbOptions = strlen(otbOptions) eq 0 ? list() : list(strsplit(otbOptions, '[]/', /EXTRACT), /EXTRACT)
      
      ;describe a single parameter
      parInfo = {otbName: otbName $
        , idlName:idlName $
        , idlType:idlType $
        , isrequired:isRequired $
        , isChanged:0b $
        , otbOptions:otbOptions $
        , otbType:otbType $
        , otbDefault:otbDefault $
        , otbDescription:otbDescription $
      }
      ;HELP, parInfo
      moduleParameters.add, parInfo
      j++
    endwhile
    tmp = replicate(moduleParameters[0], N_ELEMENTS(moduleParameters))
    foreach mPar, moduleParameters, iPar do begin
      tmp[iPar] = mPar
    endforeach
    
   
    
    
    
    ;solve ambiguous keyword problem
    foreach idlName, tmp[*].idlName, iPar do begin
      len = strlen(idlName)
      if total(strcmp(idlName, tmp[*].idlName,len, FOLD_CASE=1)) gt 1 then begin
        case idlName of
          'in' : newName = 'dataIn'
          'ref' : newName = 'dataRef'
          'out' : newName = 'dataOut'
          ;'op'  : newName = 'operation'
          else : begin
            num = 1
            repeat begin
              newName = string(format='(%"%s%i")', idlName, num++)
            endrep until total(strcmp(newName, tmp[*].idlName,strlen(newName), FOLD_CASE=1)) le 1
          end
        endcase
        tmp[iPar].idlName = newName
        
      endif
    endforeach
    
    ;solve app-specific keyword-problems
    if STRCMP(moduleKey, 'VertexComponentAnalysis', /FOLD_CASE) then begin
      iPar = where(strcmp(tmp[*].idlName, 'NE', /FOLD_CASE), /NULL)
      tmp[iPar].idlName = 'nEndMembers'
      tmp[iPar].isChanged = 1b
    endif
    
    tmp[where(~strcmp(tmp[*].otbName, tmp[*].idlName, /FOLD_CASE), /NULL)].isChanged = 1b
    

    moduleInfos[moduleKey] = {key:moduleKey $
      , name:moduleNames[i] $
      , parameters:tmp $
      , documentationLink:moduleDocumentationLink $ 
      , description:moduleDescription}

  endforeach


  return, moduleInfos

end
pro hubOTBWrapper_codeGenerator, codeDir=codeDir
  if ~isa(codeDir) then begin
    codeDir = filepath('', ROOT_DIR=enmapBox_getDirname(/SOURCECODE, /LIB), SUBDIRECTORY=['hubAPI','hub','OTBWrapper','autogenerated'])
  endif
  
  ;appLauncher = 'C:\OSGeo4W64\bin\otbcli.bat'
  otbWrapper = hubOTBWrapper(/_skipRegistration)
  
  
  filter = 'otbapp_Segmentation'
  filter = '.*'
  mInfos = hubOTBWrapper_codeGenerator_getModuleInfo(otbWrapper, filter)
  
  foreach mi, mInfos do begin
    funcName = mi.name 
    fileName = filepath('hubOTBWrapper__'+strlowcase(funcName)+'.pro', ROOT_DIR=codeDir)
    fileName = STRLOWCASE(fileName)
    
    linesIDLDoc = list()
    linesIDLDoc.add, ';+'
    linesIDLDoc.add, '; :Description:'
    linesIDLDoc.add, ';   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :'+systime(0)
    linesIDLDoc.add, ';   '+mi.name
    foreach line, mi.description do begin
      linesIDLDoc.add, ';   '+line
    endforeach
    linesIDLDoc.add, ';  :Keywords: '
    

    linesDef= list()
    linesDef.add, 'pro hubOTBWrapper::'+funcName + ' $'
    
    nPar = n_elements(mi.parameters)
    iPar = 0
    foreach mPar, mi.parameters do begin
       tmp1 = mPar.isRequired ? 'required':'optional'
       tmp2 = iPar lt nPar-1 ? '$':'' 
       linesDef.add, string(format='(%"       , %s = %s %s")',mPar.idlname,mPar.idlname, '$')
       
       
       linesIDLDoc.add, string(format='(%";     %s:in,%s, type = %s")', mPar.idlname, tmp1, mPar.idlType)
       if mPar.isChanged then begin
        linesIDLDoc.add, string(format='(%";       !Note that IDL keyword name differs from OTB parameter name %s!")', mPar.otbname)
       endif
       linesIDLDoc.add, string(format='(%";        %s")', mPar.otbDescription)
       linesIDLDoc.add, ';'
       iPar++
    endforeach

    ;add otbWrapper specific commands
    linesDef.add, "       , spawnCmd = spawnCmd $"
    linesDef.add, "       , spawnResult = spawnResult $"
    linesDef.add, "       , spawnError  = spawnError $"
    linesDef.add, "       , passErrors  = passErrors $"
    linesDef.add, "       , skipCheck  = skipCheck $"
    linesDef.add, "       , _EXTRA = _EXTRA "

    linesIDLDoc.add, ';     skipCheck: in, optional, type=boolean'
    linesIDLDoc.add, ';        Set this to skip the IDL based check of input parameters.'
    linesIDLDoc.add, ';     '
    linesIDLDoc.add, ';     spawnResult: out, optional, type=string[]'
    linesIDLDoc.add, ';        Returns the command result as returned from the SPAWN command.'
    linesIDLDoc.add, ';      '
    linesIDLDoc.add, ';     spawnError: out, optional, type=string[]'
    linesIDLDoc.add, ';        Returns the command error as returned from the SPAWN command.'
    linesIDLDoc.add, ';     '
    linesIDLDoc.add, ';     spawnCmd: out, optional, type=string'
    linesIDLDoc.add, ';        Returns the full command string used by the SPAWN command to run the OTB module.'
    linesIDLDoc.add, ';     '
    linesIDLDoc.add, ';     passErrors: in, optional, type=string'
    linesIDLDoc.add, ';        Set this to not throw an IDL error message in case of an OTB error.'
    linesIDLDoc.add, ';-'
    
    
    strFuncName = "'"+funcName+"'"
    linesDef.add, ""
    linesDef.add, "  ;add module info (contains parameter definitions etc.)"
    linesDef.add, "  registerOnly = keyword_set(_registerModuleInfo) "
    linesDef.add, "  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) "
    linesDef.add, "  if ~(self.commands).hasKey("+strFuncName+") or registerOnly then begin "
    linesDef.add, "     defJSON = '"+JSON_SERIALIZE(mi)+"'"
    linesDef.add, "     self._registerModuleInfo, "+strFuncName+", defJSON"
    linesDef.add, "     if registerOnly then return    "
    linesDef.add, "  endif"
    linesDef.add, "" 
    linesDef.add, " !NULL = self._runCommand("+strfuncName+" $ "
    iPar = 0
    foreach mPar, mi.parameters do begin
      ;tmp2 = iPar++ lt nPar-1 ? '$':')'
      tmp2 = "$"
      linesDef.add, string(format='(%"                , %s = %s %s")', mPar.idlName, mPar.idlName, tmp2)
    endforeach
    linesDef.add, "                , spawnCmd = spawnCmd $"
    linesDef.add, "                , spawnResult = spawnResult $"
    linesDef.add, "                , spawnError  = spawnError $"
    linesDef.add, "                , passErrors  = passErrors $"
    linesDef.add, "                , skipCheck  = skipCheck $"
    linesDef.add, "                , _EXTRA = _EXTRA )"
    linesDef.add, "end"
    
    
    ;write file
    openw, lun, fileName, /GET_LUN
    foreach line, linesIDLDoc do printf, lun, line
    foreach line, linesDef do printf, lun, line
    CLOSE, lun, /FORCE 
    FREE_LUN, lun

  endforeach
  
  ;call OTB Wrapper with initial registration of all OTB modules
  
  print, 'Call hubOTBWrapper. This migth require a refresh of the IDL path to find generated functions'
  
  otb = hubOTBWrapper()
  
  stop
  
  print, 'Done'
end