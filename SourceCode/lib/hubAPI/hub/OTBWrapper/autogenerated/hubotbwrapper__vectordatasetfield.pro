;+
; :Description:
;   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :Wed Mar 11 17:20:33 2015
;   VectorDataSetField
;   This is the VectorDataSetField application, version 4.2.0
;   Set a field in vector data.
;   
;   Complete documentation: http://www.orfeo-toolbox.org/Applications/VectorDataSetField.html
;   
;  :Keywords: 
;     progress:in,optional, type = byte
;        Report progress
;
;     dataIn:in,required, type = string
;       !Note that IDL keyword name differs from OTB parameter name in!
;        Input (mandatory)
;
;     out:in,required, type = string
;        Output (mandatory)
;
;     fn:in,required, type = string
;        Field (mandatory)
;
;     fv:in,required, type = string
;        Value (mandatory)
;
;     inxml:in,optional, type = string
;        Load otb application from xml file (optional, off by default)
;
;     skipCheck: in, optional, type=boolean
;        Set this to skip the IDL based check of input parameters.
;     
;     spawnResult: out, optional, type=string[]
;        Returns the command result as returned from the SPAWN command.
;      
;     spawnError: out, optional, type=string[]
;        Returns the command error as returned from the SPAWN command.
;     
;     spawnCmd: out, optional, type=string
;        Returns the full command string used by the SPAWN command to run the OTB module.
;     
;     passErrors: in, optional, type=string
;        Set this to not throw an IDL error message in case of an OTB error.
;-
pro hubOTBWrapper::VectorDataSetField $
       , progress = progress $
       , dataIn = dataIn $
       , out = out $
       , fn = fn $
       , fv = fv $
       , inxml = inxml $
       , spawnCmd = spawnCmd $
       , spawnResult = spawnResult $
       , spawnError  = spawnError $
       , passErrors  = passErrors $
       , skipCheck  = skipCheck $
       , _EXTRA = _EXTRA 

  ;add module info (contains parameter definitions etc.)
  registerOnly = keyword_set(_registerModuleInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey('VectorDataSetField') or registerOnly then begin 
     defJSON = '{"KEY":"vectordatasetfield","NAME":"VectorDataSetField","PARAMETERS":[{"OTBNAME":"progress","IDLNAME":"progress","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Report progress"},{"OTBNAME":"in","IDLNAME":"dataIn","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Input (mandatory)"},{"OTBNAME":"out","IDLNAME":"out","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Output (mandatory)"},{"OTBNAME":"fn","IDLNAME":"fn","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Field (mandatory)"},{"OTBNAME":"fv","IDLNAME":"fv","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Value (mandatory)"},{"OTBNAME":"inxml","IDLNAME":"inxml","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Load otb application from xml file (optional, off by default)"}],"DOCUMENTATIONLINK":["http://www.orfeo-toolbox.org/Applications/VectorDataSetField.html"],"DESCRIPTION":["This is the VectorDataSetField application, version 4.2.0","Set a field in vector data.","","Complete documentation: http://www.orfeo-toolbox.org/Applications/VectorDataSetField.html",""]}'
     self._registerModuleInfo, 'VectorDataSetField', defJSON
     if registerOnly then return    
  endif

 !NULL = self._runCommand('VectorDataSetField' $ 
                , progress = progress $
                , dataIn = dataIn $
                , out = out $
                , fn = fn $
                , fv = fv $
                , inxml = inxml $
                , spawnCmd = spawnCmd $
                , spawnResult = spawnResult $
                , spawnError  = spawnError $
                , passErrors  = passErrors $
                , skipCheck  = skipCheck $
                , _EXTRA = _EXTRA )
end
