;+
; :Description:
;   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :Wed Mar 11 17:20:33 2015
;   SFSTextureExtraction
;   This is the SFSTextureExtraction application, version 4.2.0
;   Computes Structural Feature Set textures on every pixel of the input image selected channel
;   
;   Complete documentation: http://www.orfeo-toolbox.org/Applications/SFSTextureExtraction.html
;   
;  :Keywords: 
;     progress:in,optional, type = byte
;        Report progress
;
;     dataIn:in,required, type = string
;       !Note that IDL keyword name differs from OTB parameter name in!
;        Input Image (mandatory)
;
;     channel:in,optional, type = int
;        Selected Channel (mandatory, default value is 1)
;
;     ram:in,optional, type = int
;        Available RAM (Mb) (optional, off by default, default value is 128)
;
;     parameters_spethre:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name parameters.spethre!
;        Spectral Threshold (mandatory, default value is 50)
;
;     parameters_spathre:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name parameters.spathre!
;        Spatial Threshold (mandatory, default value is 100)
;
;     parameters_nbdir:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name parameters.nbdir!
;        Number of Direction (mandatory, default value is 20)
;
;     parameters_alpha:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name parameters.alpha!
;        Alpha (mandatory, default value is 1)
;
;     parameters_maxcons:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name parameters.maxcons!
;        Ratio Maximum Consideration Number (mandatory, default value is 5)
;
;     out:in,required, type = string
;        [pixel] Feature Output Image [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)
;
;     inxml:in,optional, type = string
;        Load otb application from xml file (optional, off by default)
;
;     skipCheck: in, optional, type=boolean
;        Set this to skip the IDL based check of input parameters.
;     
;     spawnResult: out, optional, type=string[]
;        Returns the command result as returned from the SPAWN command.
;      
;     spawnError: out, optional, type=string[]
;        Returns the command error as returned from the SPAWN command.
;     
;     spawnCmd: out, optional, type=string
;        Returns the full command string used by the SPAWN command to run the OTB module.
;     
;     passErrors: in, optional, type=string
;        Set this to not throw an IDL error message in case of an OTB error.
;-
pro hubOTBWrapper::SFSTextureExtraction $
       , progress = progress $
       , dataIn = dataIn $
       , channel = channel $
       , ram = ram $
       , parameters_spethre = parameters_spethre $
       , parameters_spathre = parameters_spathre $
       , parameters_nbdir = parameters_nbdir $
       , parameters_alpha = parameters_alpha $
       , parameters_maxcons = parameters_maxcons $
       , out = out $
       , inxml = inxml $
       , spawnCmd = spawnCmd $
       , spawnResult = spawnResult $
       , spawnError  = spawnError $
       , passErrors  = passErrors $
       , skipCheck  = skipCheck $
       , _EXTRA = _EXTRA 

  ;add module info (contains parameter definitions etc.)
  registerOnly = keyword_set(_registerModuleInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey('SFSTextureExtraction') or registerOnly then begin 
     defJSON = '{"KEY":"sfstextureextraction","NAME":"SFSTextureExtraction","PARAMETERS":[{"OTBNAME":"progress","IDLNAME":"progress","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Report progress"},{"OTBNAME":"in","IDLNAME":"dataIn","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Input Image (mandatory)"},{"OTBNAME":"channel","IDLNAME":"channel","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"1","OTBDESCRIPTION":"Selected Channel (mandatory, default value is 1)"},{"OTBNAME":"ram","IDLNAME":"ram","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"128","OTBDESCRIPTION":"Available RAM (Mb) (optional, off by default, default value is 128)"},{"OTBNAME":"parameters.spethre","IDLNAME":"parameters_spethre","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"50","OTBDESCRIPTION":"Spectral Threshold (mandatory, default value is 50)"},{"OTBNAME":"parameters.spathre","IDLNAME":"parameters_spathre","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"100","OTBDESCRIPTION":"Spatial Threshold (mandatory, default value is 100)"},{"OTBNAME":"parameters.nbdir","IDLNAME":"parameters_nbdir","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"20","OTBDESCRIPTION":"Number of Direction (mandatory, default value is 20)"},{"OTBNAME":"parameters.alpha","IDLNAME":"parameters_alpha","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"1","OTBDESCRIPTION":"Alpha (mandatory, default value is 1)"},{"OTBNAME":"parameters.maxcons","IDLNAME":"parameters_maxcons","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"5","OTBDESCRIPTION":"Ratio Maximum Consideration Number (mandatory, default value is 5)"},{"OTBNAME":"out","IDLNAME":"out","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"float","OTBDESCRIPTION":"[pixel] Feature Output Image [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)"},{"OTBNAME":"inxml","IDLNAME":"inxml","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Load otb application from xml file (optional, off by default)"}],"DOCUMENTATIONLINK":["http://www.orfeo-toolbox.org/Applications/SFSTextureExtraction.html"],"DESCRIPTION":["This is the SFSTextureExtraction application, version 4.2.0","Computes Structural Feature Set textures on every pixel of the input image selected channel","","Complete documentation: http://www.orfeo-toolbox.org/Applications/SFSTextureExtraction.html",""]}'
     self._registerModuleInfo, 'SFSTextureExtraction', defJSON
     if registerOnly then return    
  endif

 !NULL = self._runCommand('SFSTextureExtraction' $ 
                , progress = progress $
                , dataIn = dataIn $
                , channel = channel $
                , ram = ram $
                , parameters_spethre = parameters_spethre $
                , parameters_spathre = parameters_spathre $
                , parameters_nbdir = parameters_nbdir $
                , parameters_alpha = parameters_alpha $
                , parameters_maxcons = parameters_maxcons $
                , out = out $
                , inxml = inxml $
                , spawnCmd = spawnCmd $
                , spawnResult = spawnResult $
                , spawnError  = spawnError $
                , passErrors  = passErrors $
                , skipCheck  = skipCheck $
                , _EXTRA = _EXTRA )
end
