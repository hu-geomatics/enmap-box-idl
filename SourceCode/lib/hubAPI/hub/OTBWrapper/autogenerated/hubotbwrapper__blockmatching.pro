;+
; :Description:
;   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :Wed Mar 11 17:20:32 2015
;   BlockMatching
;   This is the BlockMatching application, version 4.2.0
;   Performs block-matching to estimate pixel-wise disparities between two images
;   
;   Complete documentation: http://www.orfeo-toolbox.org/Applications/BlockMatching.html
;   
;  :Keywords: 
;     progress:in,optional, type = byte
;        Report progress
;
;     io_inleft:in,required, type = string
;       !Note that IDL keyword name differs from OTB parameter name io.inleft!
;        Left input image (mandatory)
;
;     io_inright:in,required, type = string
;       !Note that IDL keyword name differs from OTB parameter name io.inright!
;        Right input image (mandatory)
;
;     io_out1:in,required, type = string
;       !Note that IDL keyword name differs from OTB parameter name io.out!
;        [pixel] The output disparity map [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)
;
;     io_outmask:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name io.outmask!
;        [pixel] The output mask corresponding to all criterions [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
;
;     io_outmetric:in,optional, type = byte
;       !Note that IDL keyword name differs from OTB parameter name io.outmetric!
;        Output optimal metric values as well (optional, off by default)
;
;     mask_inleft:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name mask.inleft!
;        Discard left pixels from mask image (optional, off by default)
;
;     mask_inright:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name mask.inright!
;        Discard right pixels from mask image (optional, off by default)
;
;     mask_nodata:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name mask.nodata!
;        Discard pixels with no-data value (optional, off by default, default value is 0)
;
;     mask_variancet:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name mask.variancet!
;        Discard pixels with low local variance (optional, off by default, default value is 100)
;
;     bm_metric1:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name bm.metric!
;        Block-matching metric [ssd/ncc/lp] (mandatory, default value is ssd)
;
;     bm_metric_lp_p:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name bm.metric.lp.p!
;        p value (mandatory, default value is 1)
;
;     bm_radius:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.radius!
;        Radius of blocks (mandatory, default value is 3)
;
;     bm_minhd:in,required, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.minhd!
;        Minimum horizontal disparity (mandatory)
;
;     bm_maxhd:in,required, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.maxhd!
;        Maximum horizontal disparity (mandatory)
;
;     bm_minvd:in,required, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.minvd!
;        Minimum vertical disparity (mandatory)
;
;     bm_maxvd:in,required, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.maxvd!
;        Maximum vertical disparity (mandatory)
;
;     bm_subpixel:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name bm.subpixel!
;        Sub-pixel interpolation [none/parabolic/triangular/dichotomy] (mandatory, default value is none)
;
;     bm_step:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.step!
;        Computation step (optional, on by default, default value is 1)
;
;     bm_startx:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.startx!
;        X start index (optional, on by default, default value is 0)
;
;     bm_starty:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.starty!
;        Y start index (optional, on by default, default value is 0)
;
;     bm_medianfilter_radius:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.medianfilter.radius!
;        Radius (optional, off by default)
;
;     bm_medianfilter_incoherence:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name bm.medianfilter.incoherence!
;        Incoherence threshold (optional, off by default)
;
;     bm_initdisp1:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp!
;        Initial disparities [none/uniform/maps] (mandatory, default value is none)
;
;     bm_initdisp_uniform_hdisp:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.uniform.hdisp!
;        Horizontal initial disparity (mandatory, default value is 0)
;
;     bm_initdisp_uniform_vdisp:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.uniform.vdisp!
;        Vertical initial disparity (mandatory, default value is 0)
;
;     bm_initdisp_uniform_hrad:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.uniform.hrad!
;        Horizontal exploration radius (mandatory, default value is 0)
;
;     bm_initdisp_uniform_vrad:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.uniform.vrad!
;        Vertical exploration radius (mandatory, default value is 0)
;
;     bm_initdisp_maps_hmap:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.maps.hmap!
;        Horizontal initial disparity map (mandatory)
;
;     bm_initdisp_maps_vmap:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.maps.vmap!
;        Vertical initial disparity map (mandatory)
;
;     bm_initdisp_maps_hrad:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.maps.hrad!
;        Horizontal exploration radius (mandatory, default value is 0)
;
;     bm_initdisp_maps_vrad:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name bm.initdisp.maps.vrad!
;        Vertical exploration radius (mandatory, default value is 0)
;
;     ram:in,optional, type = int
;        Available RAM (Mb) (optional, off by default, default value is 128)
;
;     inxml:in,optional, type = string
;        Load otb application from xml file (optional, off by default)
;
;     skipCheck: in, optional, type=boolean
;        Set this to skip the IDL based check of input parameters.
;     
;     spawnResult: out, optional, type=string[]
;        Returns the command result as returned from the SPAWN command.
;      
;     spawnError: out, optional, type=string[]
;        Returns the command error as returned from the SPAWN command.
;     
;     spawnCmd: out, optional, type=string
;        Returns the full command string used by the SPAWN command to run the OTB module.
;     
;     passErrors: in, optional, type=string
;        Set this to not throw an IDL error message in case of an OTB error.
;-
pro hubOTBWrapper::BlockMatching $
       , progress = progress $
       , io_inleft = io_inleft $
       , io_inright = io_inright $
       , io_out1 = io_out1 $
       , io_outmask = io_outmask $
       , io_outmetric = io_outmetric $
       , mask_inleft = mask_inleft $
       , mask_inright = mask_inright $
       , mask_nodata = mask_nodata $
       , mask_variancet = mask_variancet $
       , bm_metric1 = bm_metric1 $
       , bm_metric_lp_p = bm_metric_lp_p $
       , bm_radius = bm_radius $
       , bm_minhd = bm_minhd $
       , bm_maxhd = bm_maxhd $
       , bm_minvd = bm_minvd $
       , bm_maxvd = bm_maxvd $
       , bm_subpixel = bm_subpixel $
       , bm_step = bm_step $
       , bm_startx = bm_startx $
       , bm_starty = bm_starty $
       , bm_medianfilter_radius = bm_medianfilter_radius $
       , bm_medianfilter_incoherence = bm_medianfilter_incoherence $
       , bm_initdisp1 = bm_initdisp1 $
       , bm_initdisp_uniform_hdisp = bm_initdisp_uniform_hdisp $
       , bm_initdisp_uniform_vdisp = bm_initdisp_uniform_vdisp $
       , bm_initdisp_uniform_hrad = bm_initdisp_uniform_hrad $
       , bm_initdisp_uniform_vrad = bm_initdisp_uniform_vrad $
       , bm_initdisp_maps_hmap = bm_initdisp_maps_hmap $
       , bm_initdisp_maps_vmap = bm_initdisp_maps_vmap $
       , bm_initdisp_maps_hrad = bm_initdisp_maps_hrad $
       , bm_initdisp_maps_vrad = bm_initdisp_maps_vrad $
       , ram = ram $
       , inxml = inxml $
       , spawnCmd = spawnCmd $
       , spawnResult = spawnResult $
       , spawnError  = spawnError $
       , passErrors  = passErrors $
       , skipCheck  = skipCheck $
       , _EXTRA = _EXTRA 

  ;add module info (contains parameter definitions etc.)
  registerOnly = keyword_set(_registerModuleInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey('BlockMatching') or registerOnly then begin 
     defJSON = '{"KEY":"blockmatching","NAME":"BlockMatching","PARAMETERS":[{"OTBNAME":"progress","IDLNAME":"progress","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Report progress"},{"OTBNAME":"io.inleft","IDLNAME":"io_inleft","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Left input image (mandatory)"},{"OTBNAME":"io.inright","IDLNAME":"io_inright","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Right input image (mandatory)"},{"OTBNAME":"io.out","IDLNAME":"io_out1","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"float","OTBDESCRIPTION":"[pixel] The output disparity map [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)"},{"OTBNAME":"io.outmask","IDLNAME":"io_outmask","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"float","OTBDESCRIPTION":"[pixel] The output mask corresponding to all criterions [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)"},{"OTBNAME":"io.outmetric","IDLNAME":"io_outmetric","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Output optimal metric values as well (optional, off by default)"},{"OTBNAME":"mask.inleft","IDLNAME":"mask_inleft","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Discard left pixels from mask image (optional, off by default)"},{"OTBNAME":"mask.inright","IDLNAME":"mask_inright","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Discard right pixels from mask image (optional, off by default)"},{"OTBNAME":"mask.nodata","IDLNAME":"mask_nodata","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"0","OTBDESCRIPTION":"Discard pixels with no-data value (optional, off by default, default value is 0)"},{"OTBNAME":"mask.variancet","IDLNAME":"mask_variancet","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"100","OTBDESCRIPTION":"Discard pixels with low local variance (optional, off by default, default value is 100)"},{"OTBNAME":"bm.metric","IDLNAME":"bm_metric1","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":["ssd","ncc","lp"],"OTBTYPE":"string","OTBDEFAULT":"ssd","OTBDESCRIPTION":"Block-matching metric [ssd/ncc/lp] (mandatory, default value is ssd)"},{"OTBNAME":"bm.metric.lp.p","IDLNAME":"bm_metric_lp_p","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"1","OTBDESCRIPTION":"p value (mandatory, default value is 1)"},{"OTBNAME":"bm.radius","IDLNAME":"bm_radius","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"3","OTBDESCRIPTION":"Radius of blocks (mandatory, default value is 3)"},{"OTBNAME":"bm.minhd","IDLNAME":"bm_minhd","IDLTYPE":"int","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"","OTBDESCRIPTION":"Minimum horizontal disparity (mandatory)"},{"OTBNAME":"bm.maxhd","IDLNAME":"bm_maxhd","IDLTYPE":"int","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"","OTBDESCRIPTION":"Maximum horizontal disparity (mandatory)"},{"OTBNAME":"bm.minvd","IDLNAME":"bm_minvd","IDLTYPE":"int","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"","OTBDESCRIPTION":"Minimum vertical disparity (mandatory)"},{"OTBNAME":"bm.maxvd","IDLNAME":"bm_maxvd","IDLTYPE":"int","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"","OTBDESCRIPTION":"Maximum vertical disparity (mandatory)"},{"OTBNAME":"bm.subpixel","IDLNAME":"bm_subpixel","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":["none","parabolic","triangular","dichotomy"],"OTBTYPE":"string","OTBDEFAULT":"none","OTBDESCRIPTION":"Sub-pixel interpolation [none/parabolic/triangular/dichotomy] (mandatory, default value is none)"},{"OTBNAME":"bm.step","IDLNAME":"bm_step","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"1","OTBDESCRIPTION":"Computation step (optional, on by default, default value is 1)"},{"OTBNAME":"bm.startx","IDLNAME":"bm_startx","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"X start index (optional, on by default, default value is 0)"},{"OTBNAME":"bm.starty","IDLNAME":"bm_starty","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Y start index (optional, on by default, default value is 0)"},{"OTBNAME":"bm.medianfilter.radius","IDLNAME":"bm_medianfilter_radius","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"","OTBDESCRIPTION":"Radius (optional, off by default)"},{"OTBNAME":"bm.medianfilter.incoherence","IDLNAME":"bm_medianfilter_incoherence","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"","OTBDESCRIPTION":"Incoherence threshold (optional, off by default)"},{"OTBNAME":"bm.initdisp","IDLNAME":"bm_initdisp1","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":["none","uniform","maps"],"OTBTYPE":"string","OTBDEFAULT":"none","OTBDESCRIPTION":"Initial disparities [none/uniform/maps] (mandatory, default value is none)"},{"OTBNAME":"bm.initdisp.uniform.hdisp","IDLNAME":"bm_initdisp_uniform_hdisp","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Horizontal initial disparity (mandatory, default value is 0)"},{"OTBNAME":"bm.initdisp.uniform.vdisp","IDLNAME":"bm_initdisp_uniform_vdisp","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Vertical initial disparity (mandatory, default value is 0)"},{"OTBNAME":"bm.initdisp.uniform.hrad","IDLNAME":"bm_initdisp_uniform_hrad","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Horizontal exploration radius (mandatory, default value is 0)"},{"OTBNAME":"bm.initdisp.uniform.vrad","IDLNAME":"bm_initdisp_uniform_vrad","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Vertical exploration radius (mandatory, default value is 0)"},{"OTBNAME":"bm.initdisp.maps.hmap","IDLNAME":"bm_initdisp_maps_hmap","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Horizontal initial disparity map (mandatory)"},{"OTBNAME":"bm.initdisp.maps.vmap","IDLNAME":"bm_initdisp_maps_vmap","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Vertical initial disparity map (mandatory)"},{"OTBNAME":"bm.initdisp.maps.hrad","IDLNAME":"bm_initdisp_maps_hrad","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Horizontal exploration radius (mandatory, default value is 0)"},{"OTBNAME":"bm.initdisp.maps.vrad","IDLNAME":"bm_initdisp_maps_vrad","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"0","OTBDESCRIPTION":"Vertical exploration radius (mandatory, default value is 0)"},{"OTBNAME":"ram","IDLNAME":"ram","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"128","OTBDESCRIPTION":"Available RAM (Mb) (optional, off by default, default value is 128)"},{"OTBNAME":"inxml","IDLNAME":"inxml","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Load otb application from xml file (optional, off by default)"}],"DOCUMENTATIONLINK":["http://www.orfeo-toolbox.org/Applications/BlockMatching.html"],"DESCRIPTION":["This is the BlockMatching application, version 4.2.0","Performs block-matching to estimate pixel-wise disparities between two images","","Complete documentation: http://www.orfeo-toolbox.org/Applications/BlockMatching.html",""]}'
     self._registerModuleInfo, 'BlockMatching', defJSON
     if registerOnly then return    
  endif

 !NULL = self._runCommand('BlockMatching' $ 
                , progress = progress $
                , io_inleft = io_inleft $
                , io_inright = io_inright $
                , io_out1 = io_out1 $
                , io_outmask = io_outmask $
                , io_outmetric = io_outmetric $
                , mask_inleft = mask_inleft $
                , mask_inright = mask_inright $
                , mask_nodata = mask_nodata $
                , mask_variancet = mask_variancet $
                , bm_metric1 = bm_metric1 $
                , bm_metric_lp_p = bm_metric_lp_p $
                , bm_radius = bm_radius $
                , bm_minhd = bm_minhd $
                , bm_maxhd = bm_maxhd $
                , bm_minvd = bm_minvd $
                , bm_maxvd = bm_maxvd $
                , bm_subpixel = bm_subpixel $
                , bm_step = bm_step $
                , bm_startx = bm_startx $
                , bm_starty = bm_starty $
                , bm_medianfilter_radius = bm_medianfilter_radius $
                , bm_medianfilter_incoherence = bm_medianfilter_incoherence $
                , bm_initdisp1 = bm_initdisp1 $
                , bm_initdisp_uniform_hdisp = bm_initdisp_uniform_hdisp $
                , bm_initdisp_uniform_vdisp = bm_initdisp_uniform_vdisp $
                , bm_initdisp_uniform_hrad = bm_initdisp_uniform_hrad $
                , bm_initdisp_uniform_vrad = bm_initdisp_uniform_vrad $
                , bm_initdisp_maps_hmap = bm_initdisp_maps_hmap $
                , bm_initdisp_maps_vmap = bm_initdisp_maps_vmap $
                , bm_initdisp_maps_hrad = bm_initdisp_maps_hrad $
                , bm_initdisp_maps_vrad = bm_initdisp_maps_vrad $
                , ram = ram $
                , inxml = inxml $
                , spawnCmd = spawnCmd $
                , spawnResult = spawnResult $
                , spawnError  = spawnError $
                , passErrors  = passErrors $
                , skipCheck  = skipCheck $
                , _EXTRA = _EXTRA )
end
