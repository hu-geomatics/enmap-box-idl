;+
; :Description:
;   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :Wed Mar 11 17:20:33 2015
;   Superimpose
;   This is the Superimpose application, version 4.2.0
;   Using available image metadata, project one image onto another one
;   
;   Complete documentation: http://www.orfeo-toolbox.org/Applications/Superimpose.html
;   
;  :Keywords: 
;     progress:in,optional, type = byte
;        Report progress
;
;     inr:in,required, type = string
;        Reference input (mandatory)
;
;     inm:in,required, type = string
;        The image to reproject (mandatory)
;
;     elev_dem:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name elev.dem!
;        DEM directory (optional, off by default)
;
;     elev_geoid:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name elev.geoid!
;        Geoid File (optional, off by default)
;
;     elev_default:in,optional, type = float
;       !Note that IDL keyword name differs from OTB parameter name elev.default!
;        Default elevation (mandatory, default value is 0)
;
;     lms:in,optional, type = float
;        Spacing of the deformation field (optional, on by default, default value is 4)
;
;     out:in,required, type = string
;        [pixel] Output image [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)
;
;     mode:in,optional, type = string
;        Mode [default/phr] (mandatory, default value is default)
;
;     interpolator1:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name interpolator!
;        Interpolation [bco/nn/linear] (mandatory, default value is bco)
;
;     interpolator_bco_radius:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name interpolator.bco.radius!
;        Radius for bicubic interpolation (mandatory, default value is 2)
;
;     ram:in,optional, type = int
;        Available RAM (Mb) (optional, off by default, default value is 128)
;
;     inxml:in,optional, type = string
;        Load otb application from xml file (optional, off by default)
;
;     skipCheck: in, optional, type=boolean
;        Set this to skip the IDL based check of input parameters.
;     
;     spawnResult: out, optional, type=string[]
;        Returns the command result as returned from the SPAWN command.
;      
;     spawnError: out, optional, type=string[]
;        Returns the command error as returned from the SPAWN command.
;     
;     spawnCmd: out, optional, type=string
;        Returns the full command string used by the SPAWN command to run the OTB module.
;     
;     passErrors: in, optional, type=string
;        Set this to not throw an IDL error message in case of an OTB error.
;-
pro hubOTBWrapper::Superimpose $
       , progress = progress $
       , inr = inr $
       , inm = inm $
       , elev_dem = elev_dem $
       , elev_geoid = elev_geoid $
       , elev_default = elev_default $
       , lms = lms $
       , out = out $
       , mode = mode $
       , interpolator1 = interpolator1 $
       , interpolator_bco_radius = interpolator_bco_radius $
       , ram = ram $
       , inxml = inxml $
       , spawnCmd = spawnCmd $
       , spawnResult = spawnResult $
       , spawnError  = spawnError $
       , passErrors  = passErrors $
       , skipCheck  = skipCheck $
       , _EXTRA = _EXTRA 

  ;add module info (contains parameter definitions etc.)
  registerOnly = keyword_set(_registerModuleInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey('Superimpose') or registerOnly then begin 
     defJSON = '{"KEY":"superimpose","NAME":"Superimpose","PARAMETERS":[{"OTBNAME":"progress","IDLNAME":"progress","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Report progress"},{"OTBNAME":"inr","IDLNAME":"inr","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Reference input (mandatory)"},{"OTBNAME":"inm","IDLNAME":"inm","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"The image to reproject (mandatory)"},{"OTBNAME":"elev.dem","IDLNAME":"elev_dem","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"DEM directory (optional, off by default)"},{"OTBNAME":"elev.geoid","IDLNAME":"elev_geoid","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Geoid File (optional, off by default)"},{"OTBNAME":"elev.default","IDLNAME":"elev_default","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"0","OTBDESCRIPTION":"Default elevation (mandatory, default value is 0)"},{"OTBNAME":"lms","IDLNAME":"lms","IDLTYPE":"float","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"4","OTBDESCRIPTION":"Spacing of the deformation field (optional, on by default, default value is 4)"},{"OTBNAME":"out","IDLNAME":"out","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"float","OTBDESCRIPTION":"[pixel] Output image [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)"},{"OTBNAME":"mode","IDLNAME":"mode","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":["default","phr"],"OTBTYPE":"string","OTBDEFAULT":"default","OTBDESCRIPTION":"Mode [default/phr] (mandatory, default value is default)"},{"OTBNAME":"interpolator","IDLNAME":"interpolator1","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":["bco","nn","linear"],"OTBTYPE":"string","OTBDEFAULT":"bco","OTBDESCRIPTION":"Interpolation [bco/nn/linear] (mandatory, default value is bco)"},{"OTBNAME":"interpolator.bco.radius","IDLNAME":"interpolator_bco_radius","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"2","OTBDESCRIPTION":"Radius for bicubic interpolation (mandatory, default value is 2)"},{"OTBNAME":"ram","IDLNAME":"ram","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"128","OTBDESCRIPTION":"Available RAM (Mb) (optional, off by default, default value is 128)"},{"OTBNAME":"inxml","IDLNAME":"inxml","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Load otb application from xml file (optional, off by default)"}],"DOCUMENTATIONLINK":["http://www.orfeo-toolbox.org/Applications/Superimpose.html"],"DESCRIPTION":["This is the Superimpose application, version 4.2.0","Using available image metadata, project one image onto another one","","Complete documentation: http://www.orfeo-toolbox.org/Applications/Superimpose.html",""]}'
     self._registerModuleInfo, 'Superimpose', defJSON
     if registerOnly then return    
  endif

 !NULL = self._runCommand('Superimpose' $ 
                , progress = progress $
                , inr = inr $
                , inm = inm $
                , elev_dem = elev_dem $
                , elev_geoid = elev_geoid $
                , elev_default = elev_default $
                , lms = lms $
                , out = out $
                , mode = mode $
                , interpolator1 = interpolator1 $
                , interpolator_bco_radius = interpolator_bco_radius $
                , ram = ram $
                , inxml = inxml $
                , spawnCmd = spawnCmd $
                , spawnResult = spawnResult $
                , spawnError  = spawnError $
                , passErrors  = passErrors $
                , skipCheck  = skipCheck $
                , _EXTRA = _EXTRA )
end
