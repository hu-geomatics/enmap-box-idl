;+
; :Description:
;   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :Wed Mar 11 17:20:33 2015
;   MultivariateAlterationDetector
;   This is the MultivariateAlterationDetector application, version 4.2.0
;   Multivariate Alteration Detector
;   
;   Complete documentation: http://www.orfeo-toolbox.org/Applications/MultivariateAlterationDetector.html
;   
;  :Keywords: 
;     progress:in,optional, type = byte
;        Report progress
;
;     in1:in,required, type = string
;        Input Image 1 (mandatory)
;
;     in2:in,required, type = string
;        Input Image 2 (mandatory)
;
;     out:in,required, type = string
;        [pixel] Change Map [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)
;
;     ram:in,optional, type = int
;        Available RAM (Mb) (optional, off by default, default value is 128)
;
;     inxml:in,optional, type = string
;        Load otb application from xml file (optional, off by default)
;
;     skipCheck: in, optional, type=boolean
;        Set this to skip the IDL based check of input parameters.
;     
;     spawnResult: out, optional, type=string[]
;        Returns the command result as returned from the SPAWN command.
;      
;     spawnError: out, optional, type=string[]
;        Returns the command error as returned from the SPAWN command.
;     
;     spawnCmd: out, optional, type=string
;        Returns the full command string used by the SPAWN command to run the OTB module.
;     
;     passErrors: in, optional, type=string
;        Set this to not throw an IDL error message in case of an OTB error.
;-
pro hubOTBWrapper::MultivariateAlterationDetector $
       , progress = progress $
       , in1 = in1 $
       , in2 = in2 $
       , out = out $
       , ram = ram $
       , inxml = inxml $
       , spawnCmd = spawnCmd $
       , spawnResult = spawnResult $
       , spawnError  = spawnError $
       , passErrors  = passErrors $
       , skipCheck  = skipCheck $
       , _EXTRA = _EXTRA 

  ;add module info (contains parameter definitions etc.)
  registerOnly = keyword_set(_registerModuleInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey('MultivariateAlterationDetector') or registerOnly then begin 
     defJSON = '{"KEY":"multivariatealterationdetector","NAME":"MultivariateAlterationDetector","PARAMETERS":[{"OTBNAME":"progress","IDLNAME":"progress","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Report progress"},{"OTBNAME":"in1","IDLNAME":"in1","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Input Image 1 (mandatory)"},{"OTBNAME":"in2","IDLNAME":"in2","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Input Image 2 (mandatory)"},{"OTBNAME":"out","IDLNAME":"out","IDLTYPE":"string","ISREQUIRED":1,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"float","OTBDESCRIPTION":"[pixel] Change Map [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)"},{"OTBNAME":"ram","IDLNAME":"ram","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"128","OTBDESCRIPTION":"Available RAM (Mb) (optional, off by default, default value is 128)"},{"OTBNAME":"inxml","IDLNAME":"inxml","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Load otb application from xml file (optional, off by default)"}],"DOCUMENTATIONLINK":["http://www.orfeo-toolbox.org/Applications/MultivariateAlterationDetector.html"],"DESCRIPTION":["This is the MultivariateAlterationDetector application, version 4.2.0","Multivariate Alteration Detector","","Complete documentation: http://www.orfeo-toolbox.org/Applications/MultivariateAlterationDetector.html",""]}'
     self._registerModuleInfo, 'MultivariateAlterationDetector', defJSON
     if registerOnly then return    
  endif

 !NULL = self._runCommand('MultivariateAlterationDetector' $ 
                , progress = progress $
                , in1 = in1 $
                , in2 = in2 $
                , out = out $
                , ram = ram $
                , inxml = inxml $
                , spawnCmd = spawnCmd $
                , spawnResult = spawnResult $
                , spawnError  = spawnError $
                , passErrors  = passErrors $
                , skipCheck  = skipCheck $
                , _EXTRA = _EXTRA )
end
