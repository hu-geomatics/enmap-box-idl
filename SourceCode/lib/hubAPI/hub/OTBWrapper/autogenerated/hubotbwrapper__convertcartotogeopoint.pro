;+
; :Description:
;   hubOTBWrapper routine, autogenerated by hubotbwrapper_codeGenerator :Wed Mar 11 17:20:32 2015
;   ConvertCartoToGeoPoint
;   This is the ConvertCartoToGeoPoint application, version 4.2.0
;   Convert cartographic coordinates to geographic one.
;   
;   Complete documentation: http://www.orfeo-toolbox.org/Applications/ConvertCartoToGeoPoint.html
;   
;  :Keywords: 
;     progress:in,optional, type = byte
;        Report progress
;
;     carto_x:in,required, type = float
;       !Note that IDL keyword name differs from OTB parameter name carto.x!
;        X cartographic coordinates (mandatory)
;
;     carto_y:in,required, type = float
;       !Note that IDL keyword name differs from OTB parameter name carto.y!
;        Y cartographic coordinates (mandatory)
;
;     mapproj1:in,optional, type = string
;       !Note that IDL keyword name differs from OTB parameter name mapproj!
;        Output Cartographic Map Projection [utm/lambert2/lambert93/wgs/epsg] (mandatory, default value is utm)
;
;     mapproj_utm_zone:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name mapproj.utm.zone!
;        Zone number (mandatory, default value is 31)
;
;     mapproj_utm_northhem:in,optional, type = byte
;       !Note that IDL keyword name differs from OTB parameter name mapproj.utm.northhem!
;        Northern Hemisphere (optional, off by default)
;
;     mapproj_epsg_code:in,optional, type = int
;       !Note that IDL keyword name differs from OTB parameter name mapproj.epsg.code!
;        EPSG Code (mandatory, default value is 4326)
;
;     inxml:in,optional, type = string
;        Load otb application from xml file (optional, off by default)
;
;     skipCheck: in, optional, type=boolean
;        Set this to skip the IDL based check of input parameters.
;     
;     spawnResult: out, optional, type=string[]
;        Returns the command result as returned from the SPAWN command.
;      
;     spawnError: out, optional, type=string[]
;        Returns the command error as returned from the SPAWN command.
;     
;     spawnCmd: out, optional, type=string
;        Returns the full command string used by the SPAWN command to run the OTB module.
;     
;     passErrors: in, optional, type=string
;        Set this to not throw an IDL error message in case of an OTB error.
;-
pro hubOTBWrapper::ConvertCartoToGeoPoint $
       , progress = progress $
       , carto_x = carto_x $
       , carto_y = carto_y $
       , mapproj1 = mapproj1 $
       , mapproj_utm_zone = mapproj_utm_zone $
       , mapproj_utm_northhem = mapproj_utm_northhem $
       , mapproj_epsg_code = mapproj_epsg_code $
       , inxml = inxml $
       , spawnCmd = spawnCmd $
       , spawnResult = spawnResult $
       , spawnError  = spawnError $
       , passErrors  = passErrors $
       , skipCheck  = skipCheck $
       , _EXTRA = _EXTRA 

  ;add module info (contains parameter definitions etc.)
  registerOnly = keyword_set(_registerModuleInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerModuleInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey('ConvertCartoToGeoPoint') or registerOnly then begin 
     defJSON = '{"KEY":"convertcartotogeopoint","NAME":"ConvertCartoToGeoPoint","PARAMETERS":[{"OTBNAME":"progress","IDLNAME":"progress","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Report progress"},{"OTBNAME":"carto.x","IDLNAME":"carto_x","IDLTYPE":"float","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"","OTBDESCRIPTION":"X cartographic coordinates (mandatory)"},{"OTBNAME":"carto.y","IDLNAME":"carto_y","IDLTYPE":"float","ISREQUIRED":1,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"float","OTBDEFAULT":"","OTBDESCRIPTION":"Y cartographic coordinates (mandatory)"},{"OTBNAME":"mapproj","IDLNAME":"mapproj1","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":["utm","lambert2","lambert93","wgs","epsg"],"OTBTYPE":"string","OTBDEFAULT":"utm","OTBDESCRIPTION":"Output Cartographic Map Projection [utm/lambert2/lambert93/wgs/epsg] (mandatory, default value is utm)"},{"OTBNAME":"mapproj.utm.zone","IDLNAME":"mapproj_utm_zone","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"31","OTBDESCRIPTION":"Zone number (mandatory, default value is 31)"},{"OTBNAME":"mapproj.utm.northhem","IDLNAME":"mapproj_utm_northhem","IDLTYPE":"byte","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"boolean","OTBDEFAULT":"","OTBDESCRIPTION":"Northern Hemisphere (optional, off by default)"},{"OTBNAME":"mapproj.epsg.code","IDLNAME":"mapproj_epsg_code","IDLTYPE":"int","ISREQUIRED":0,"ISCHANGED":1,"OTBOPTIONS":[],"OTBTYPE":"int32","OTBDEFAULT":"4326","OTBDESCRIPTION":"EPSG Code (mandatory, default value is 4326)"},{"OTBNAME":"inxml","IDLNAME":"inxml","IDLTYPE":"string","ISREQUIRED":0,"ISCHANGED":0,"OTBOPTIONS":[],"OTBTYPE":"string","OTBDEFAULT":"","OTBDESCRIPTION":"Load otb application from xml file (optional, off by default)"}],"DOCUMENTATIONLINK":["http://www.orfeo-toolbox.org/Applications/ConvertCartoToGeoPoint.html"],"DESCRIPTION":["This is the ConvertCartoToGeoPoint application, version 4.2.0","Convert cartographic coordinates to geographic one.","","Complete documentation: http://www.orfeo-toolbox.org/Applications/ConvertCartoToGeoPoint.html",""]}'
     self._registerModuleInfo, 'ConvertCartoToGeoPoint', defJSON
     if registerOnly then return    
  endif

 !NULL = self._runCommand('ConvertCartoToGeoPoint' $ 
                , progress = progress $
                , carto_x = carto_x $
                , carto_y = carto_y $
                , mapproj1 = mapproj1 $
                , mapproj_utm_zone = mapproj_utm_zone $
                , mapproj_utm_northhem = mapproj_utm_northhem $
                , mapproj_epsg_code = mapproj_epsg_code $
                , inxml = inxml $
                , spawnCmd = spawnCmd $
                , spawnResult = spawnResult $
                , spawnError  = spawnError $
                , passErrors  = passErrors $
                , skipCheck  = skipCheck $
                , _EXTRA = _EXTRA )
end
