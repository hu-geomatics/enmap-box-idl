;+
; :Description:
;    Constructor to initialize a hubOTBWrapper object.
;
; :Keywords:
;    dirOTBCLI: in, required, type=string
;     Directory with OTB command line tools, e.g. `E.g. `C:\OSGeo4W64\bin\`.
;     This directory must contain the `otbApplicationLauncherCommandLine` executable.
;     
;    dirOTBApps: in, optional, type=string
;     Directory that contains the OTB module library files named like `otbapp_*`. 
;      
;    showCommands: in, optional, type=boolean
;     Set this to print the command used to run an OTB application on IDLs command line.
;       
;-
function hubOTBWrapper::init $
    , dirOTBCLI=dirOTBCLI $
    , dirOTBApps=dirOTBApps $
    , showCommands=showCommands $
    , _skipRegistration=_skipRegistration 
  
  
  pathAppLauncher = self._findAppLauncher(dirOTBCLI)
  if ~isa(pathAppLauncher) then begin
    info = ['Can not find otbApplicationLauncherCommandLine executable. ' $
           ,'Please specify keyword dirOTBCLI (OTB command line tools folder) ' $
           ,'or the environmental variable OTB_CLI_LAUNCHER']
    
    message, strjoin(info, string(10b))
  endif
  
  dirCLI = FILE_DIRNAME(pathAppLauncher) 
  
  dirApps = self._findOTBAppDirectory(dirOTBApps=dirOTBApps, dirOTBCLI=dirCLI)
  if ~isa(dirApps) then begin
    info = ['Can not find OTB application directory. ' $
           ,'Please use keyword dirOTBApps or the environmental variable ITK_AUTOLOAD_PATH' $
           ,'to specify it.']
    message, strjoin(info, string(10b))
  endif
  
  setenv, 'ITK_AUTOLOAD_PATH='+dirApps 
  
  self.pathAppLauncher = pathAppLauncher
  self.dirOTBApps = dirApps
  self.dirOTBCLI = dirCLI
  self.commands = Dictionary()
  self.showCommands = keyword_set(showCommands)

  ;list apps & register them
  if ~keyword_set(_skipRegistration) then begin
    otbApps = FILE_SEARCH(self.dirOTBapps, 'otbapp_*', /FOLD_CASE)
    otbApps = file_basename(otbApps)
    otbApps = stregex(otbApps, '[^.]+', /EXTRACT)
    otbApps = strmid(otbapps, 7)
    foreach otbApp, otbApps do begin
      self._registerModuleInfo, otbApp
    endforeach
  endif
  return, 1b
end


;+
; :Description:
;    Returns the OTB module/application directory, as specified by `dirOTBApps` in the constructor or 
;    by the environmental variable `ITK_AUTOLOAD_PATH`
;-
function hubOTBWrapper::getAppDirectory
  return, self.dirOTBApps
end

;+
; :Description:
;    Returns the path of the executable, e.g.  `otbApplicationLauncherCommandLine.exe`, that is used to launch a OTB application.
;-
function hubOTBWrapper::getAppLauncherPath
  return, self.pathAppLauncher
end

;+
; :Description:
;    Returns the path of the application launcher that is used to start OTB modules from the command line.
;    
; :Params:
;    dirOTBCLI: in, optional, type=string
;     Directory where to search first for the application launcher.
;
;
;-
function hubOTBWrapper::_findAppLauncher, dirOTBCLI
  
  
  ;list potential directories
  possibleDirs = list() 
  if isa(dirOTBCLI) then begin
    if ~file_test(dirOTBCLI, /DIRECTORY) then message, dirOTBCLI+ ' does not exist'
    possibleDirs.add, dirOTBCLI
  endif
  
  ;known plattform specific directories
  case STRUPCASE(!VERSION.OS_FAMILY) of
    'WINDOWS' : begin
        possibleDirs.add, ['C','D','E']+':\OSGeo4W64\bin', /EXTRACT
        possibleDirs.add, ['C','D','E']+':\OSGeo4W\bin', /EXTRACT
       ;todo: add more
      end
    'UNIX'    : begin
        ;todo: add more
      end
    else : message, 'Unsupportet OS: ' + !VERSION.OS_FAMILY
  endcase
  
  ;list environmental variables
  tmp = getenv('OTB_CLI_LAUNCHER')
  if FILE_TEST(tmp) then possibledirs.add, FILE_DIRNAME(tmp)
  
  ;list potential system paths
  paths = getenv('PATH')
  if isa(paths) then begin
    paths = STRSPLIT(paths, '[;:]', /REGEX, /EXTRACT)
    iCand = where(stregex(paths, '(QGIS|OSGeo4|otb|orpheo)', /BOOLEAN), /NULL)
    if isa(iCand) then begin
      possibleDirs.add, paths[iCand], /EXTRACT
    endif
  endif
  
  foreach dir, possibleDirs do begin
    possibleLaunchers = FILE_SEARCH(dir, 'otbApplicationLauncherCommandLine*', /FOLD_CASE, count=cnt)
    if cnt gt 0 then begin
      ;return the first successful match
      return, possibleLaunchers[0]
    endif
  endforeach
  return, !NULL
end

;+
; :Description:
;    This function tries to find the OTB module directory.
;
; :Keywords:
;    dirOTBCLI: in, optional, type=string
;       Path of OTB command line tools directory.
;
;
;-
function hubOTBWrapper::_findOTBAppDirectory, dirOTBCLI=dirOTBCLI, dirOTBApps=dirOTBApps
  
  
  ;try to find the Module directory
  possibleDirs = list()
  if isa(dirOTBApps) then begin
    if ~FILE_TEST(dirOTBApps, /DIRECTORY) then message, dirOTBApps + ' is not a directory' 
    possibleDirs.add, dirOTBApps
  endif
  
  ;add potential directories here:
  possibleDirs.add, GETENV('ITK_AUTOLOAD_PATH')
  
  if isa(dirOTBCLI) then begin
    bd = FILE_DIRNAME(dirOTBCLI)
    bds = [bd, FILE_DIRNAME(bd)]
    foreach bd, bds do begin
      possibleDirs.add, FILEPATH('',ROOT_DIR=bd, SUBDIRECTORY=['lib','otb','applications'])
      possibleDirs.add, FILEPATH('',ROOT_DIR=bd, SUBDIRECTORY=['apps','orfeotoolbox','applications'])
    endforeach
    ;bd = FILE_DIRNAME(bd)
    
  endif
  

  
  foreach dir, possibleDirs do begin
    if file_test(dir, /DIRECTORY) then return, dir
  endforeach
  
  return, !NULL
end

;+
; :Description:
;    
;
; :Params:
;    name: in, required, type=string
;     Name of OTB module.
;
; :Keywords:
;    skipCheck: in, optional, type=boolean
;     Set this to skip the IDL based check of input parameters.
;     
;-
;+
; :Description:
;    Used to start a OTB module.
;
; :Params:
;    name: in, required, type=string
;     Name of OTB module
;
; :Keywords:
;    skipCheck: in, optional, type=boolean
;     Set this to skip the IDL based check of input parameters.
;     
;    spawnResult: out, optional, type=string[]
;     Returns the command result as returned from the SPAWN command.
;      
;    spawnError: out, optional, type=string[]
;     Returns the command error as returned from the SPAWN command.
;     
;    spawnCmd: out, optional, type=string
;     Returns the full command string used by the SPAWN command to run the OTB module.
;     
;    passErrors: in, optional, type=string
;     Set this to not throw an IDL error message in case of an OTB error.
;     
;
; :Author: geo_beja
;-
function hubOTBWrapper::_runCommand, name, skipCheck=SkipCheck $
        , spawnResult=spawnResult, spawnError=spawnError $
        , spawnCmd = spawnCmd $
        , passErrors=passErrors, _EXTRA=_EXTRA
  
  if ~(self.commands).hasKey(name) then message, 'OTB application not found :'+name
  
  moduleInfos = (self.commands)[name] 
  parInfos = moduleInfos.parameters
  
  ;check for required arguments
  tagNames = STRLOWCASE(TAG_NAMES(_EXTRA))
  if ~keyword_set(SkipCheck) then begin
    missing = list()
    falseOptions = Hash()
    foreach parInfo, parInfos, key do begin
      iTag = where(STRCMP(tagNames, parInfo.idlName, /FOLD_CASE), /NULL, nTags)
      
      if parInfo.isRequired &&  nTags eq 0 then begin
        missing.add, paRInfo.idlName
        CONTINUE
      endif
      
      if nTags eq 0 then continue
      
      if n_elements(parInfo.otbOptions) gt 0 then begin
        parValue = _EXTRA.(iTag[0])
        validOption = 0b
        foreach option, parInfo.otbOptions do validOption or= strcmp(strtrim(parValue,2), option)   
        if ~validOption then begin
          falseOptions[parInfo.idlName] = {false:parValue, valid:parInfo.otbOptions}
        endif
      endif
    endforeach
    
    if n_elements(missing) gt 0 then message, "Required keyword(s) not set: '"+strjoin(missing.toArray(), "', '")+"'"
    if n_elements(falseOptions) gt 0 then begin
      info = list('Wrong options used for keyword(s):')
      foreach falseOption, falseOptions, key do begin
        info.add, string(format='(%"%s = ''%s''. Use instead [%s]")', key, falseOption.false, strjoin((falseOption.valid).toArray(),'/') )
      endforeach
      message, strjoin(info.toArray(), string(10b))
    endif
    
  endif
  
  
  otbCommand = list(self.pathAppLauncher+ ' '+name+' ')
  foreach tag, tagNames, i do begin
    iParInfo = where(STRCMP(parInfos[*].idlName, tag, /FOLD_CASE), /NULL)
    if isa(iParInfo) then begin
      parInfo = parInfos[iParInfo[0]] 
      otbCommand.add, string(format='(%"-%s %s")' $
            , parInfo.otbName $
            , strjoin(strtrim(_EXTRA.(i),2), ' '))
    endif
  endforeach
  
  otbCommand= strjoin(otbCommand.toArray(), ' ')
  if self.showCommands then print, otbCommand
  spawnCmd = otbCommand
  isUnix = strcmp(!VERSION.OS_FAMILY, 'UNIX', /FOLD_CASE)
  case STRUPCASE(!VERSION.OS_FAMILY) of
    'WINDOWS' : SPAWN,  otbCommand, spawnResult, spawnError, count=count, /HIDE, exit_status=exit_status, /NOSHELL
    'UNIX'    : SPAWN,  otbCommand, spawnResult, spawnError, count=count, exit_status=exit_status
    else : message, 'Unsupportet OS: ' + !VERSION.OS_FAMILY
  endcase
  
  if ~keyword_set(passErrors) then begin
    if max(strlen(spawnError)) gt 0 then begin
      message, strjoin(spawnError, string(10b))
    endif
    if total(stregex(spawnResult, '\(FATAL\)', /BOOLEAN)) then begin
      
      message, strjoin(spawnResult, string(10b))
      
    endif
  endif
  
  return, 1b
end

;+
; :Description:
;    Registers each module / application and its parameters.
;
; :Params:
;    moduleName: in, required
;     Name of OTB module/ application
;    JSON: in, required
;     JSON string as generated by hubOTBWrapper_codeGenerator
;
;
;-
pro hubOTBWrapper::_registerModuleInfo, moduleName, JSON
    if ~isa(JSON) then begin
      ;register by calling the auto-generated function definition.
      ;within this function a constant JSON is defined and used to re-call _registerModuleInfo 
      catch, error
      if error ne 0 then begin
        print, !Error_state.msg
        print, string(format='(%"Unable to find hubotbwrapper_%s.pro. Maybe the hubOTBWrapper bindings are outdated?")', STRLOWCASE(moduleName))
        catch, /CANCEL
        return
      endif
      
      CALL_METHOD, moduleName, (self), /_registerModuleInfo
        
    endif else begin
      moduleInfo = JSON_PARSE(JSON, /DICTIONARY)
      ;restructure for better handling
      parInfos = ((moduleInfo.PARAMETERS)[0]).toStruct()
      parInfos = replicate(parInfos, n_elements(moduleInfo.PARAMETERS))
      foreach parInfo, moduleInfo.PARAMETERS, i do  parInfos[i] = parInfo.toStruct() 
      moduleInfo.PARAMETERS = parInfos
      moduleInfo.documentationLink = (moduleInfo.documentationLink)[0]
      moduleInfo.description = (moduleInfo.description).toArray() 
      (self.commands)[moduleName] = moduleInfo
    endelse
end

function hubOTBWrapper::_getModuleInfo, moduleName
  if ~(self.commands).hasKey(moduleName) then message, 'Module '+moduleName+ ' does not exists/is not registered'
  return, (self.commands)[moduleName]
end

function hubOTBWrapper::_getModuleNames
  return, (self.commands).keys()
end

function hubOTBWrapper::_overloadPrint
  info = list()
  info.add, 'hubOTBWrapper:'
  info.add, string(format='(%"  App Launcher: %s")', self.pathAppLauncher)
  info.add, string(format='(%"  OTB Apps: %s")', self.dirOTBApps)
  info.add, string(format='(%"  OTB CLI: %s")', self.dirOTBCLI)
;  info.add, self.commands.keys()
  return, strjoin(info.toArray(), string(13b))
end

;+
; :Hidden:
;-
pro hubOTBWrapper__define
  struct = {hubOTBWrapper $
    , inherits IDL_Object $
    , pathAppLauncher : '' $
    , dirOTBApps : '' $
    , dirOTBCLI : '' $
    , commands : obj_new() $
    , requiredBinaries:list() $ ;names of requried executables
    , showCommands : 0b $
    , debug:0b $
  }
end

pro test_hubOTBWrapper
  
  ;dirOTBApps = 'C:\OSGeo4W64\apps\orfeotoolbox\applications' 
  ;dirCLI = 'C:\OSGeo4W64\bin' 
  ;pathAL = 'C:\OSGeo4W64\bin\otbApplicationLauncherCommandLine.exe'
  pathIn = 'R:\bj_temp\S2Workshop\02_Preprocessed\227_065\LE72270652013196CUB00\LE72270652000017CUB00_BLA_SR.bsq'
  ;pathMask = 'R:\bj_temp\S2Workshop\02_Preprocessed\227_065\LE72270652013196CUB00\LE72270652000017CUB00_BLA_SR.bsq'
  
 
  ;pathIn = hub_getTestImage('Hymap_Berlin-B_Image')
  pathROut= 'C:\tmp\segmentedMS_default2.tif'
  pathVOut = 'C:\tmp\segmentedMS_default2.shp'
 
  otb = hubOTBWrapper(/SHOWCOMMANDS)
;  moduleInfo = otb._getModuleInfo('Segmentation')
;  help, moduleInfo[0]
;  otb.Segmentation, dataIn=pathIn +  '?&skipcarto=true' $
;                  , /MODE_VECTOR_STITCH $
;                  , MODE1 ='vector' $
;                  , MODE_VECTOR_OUT1 = pathVOut $
;                  , MODE_VECTOR_OUTMODE='ovw' $
;                  , FILTER1='meanshift'
;
;    
  ;Band math
  ;http://wiki.orfeo-toolbox.org/index.php/ExtendedFileName
  ;+ '?&skipcarto=true'
  inputImages = '"' + [hub_getTestImage('Hymap_Berlin-A_Image') $
                      ,hub_getTestImage('Hymap_Berlin-B_Image')] ;+'"?&skipcarto=true'
  pathOut= 'C:\tmp\example3_BandMath.hdr'+'?&gdal:of:ENVI'
  otb = hubOTBWrapper(/SHOWCOMMANDS)
  otb.BandMath,IL=inputImages $
              ;,OUT= pathOut +'?&gdal:co:"of"="ENVI"' $
              ,OUT= pathOut  $
              ,EXP='imERROR1b40 * im2b40' $
              , spawnResult=spawnResult, spawnError=spawnError, /passErrors    
  
  print, spawnResult
  print, spawnError
  
  otb.EdgeExtraction,DATAIN=pathIn, CHANNEL=50,OUT=pathOut 
  print, 'Test done'
end