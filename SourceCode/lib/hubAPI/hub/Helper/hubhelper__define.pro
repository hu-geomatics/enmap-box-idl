;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;          Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;-


;+
; :Description:
;    This function returns a mask resulting from the combination of a 2D images internal mask, given by it's data ingore value,
;    and an explicit external mask, given as 1D mask-vector. 
;    
;    Each image has an implicit 'internal mask', which is normally set on true for all values. 
;    In case the image uses a data ignore value (DIV) the internal mask is set to false where an
;    image value is euqal to the DIV. Further an external mask can be given, usually showing which image pixel 
;    (and all of its values accross all image bands) is masked and which is not.
;    
;    
;    
;  :Examples:
;    Let's say we got image data with values for five pixel with in 3 bands. The image uses a data ignore value 
;    of `-1` and is accompanied by a mask file::
;       helper = HubHelper()
;       imageData = [[ 3.7, 2.2, 4.7, 9.2, -1] $
;                  , [ 5.9, 3.8, -1 , -1 , -1] $
;                  , [ 1.6, 3.2, 3.1, -1 , -1]]
;       mask      = [    1,   0,   0,   1,  0]
;
;    To combine image data, it internal data ingore value and the external mask,
;    we can interprete the image data in four ways:
;    1. ingore the data ignore value:
;      
;       print, helper.getImageMask(imageData, 0)
;       print, helper.getImageMask(imageData, 0, externalMask=mask)
;       
;    2. create a mask for each band:
;       
;       print, helper.getImageMask(imageData, 1)
;       print, helper.getImageMask(imageData, 1, externalMask=mask)
;       
;    3. create a mask for the total image. Mask pixels where the data ignore value is set in ALL bands:
;    
;       print, helper.getImageMask(imageData, 2)
;       print, helper.getImageMask(imageData, 2, externalMask=mask)
;    
;    4. create a mask for the total image. Mask pixels where the data ignore value is set in ANY band:
;    
;       print, helper.getImageMask(imageData, 3)
;       print, helper.getImageMask(imageData, 3, externalMask=mask)       
;      
;
; :Params:
;    imageData: in, required, type=numeric[][]
;     The image data as a 'slice of image pixels'. It has the dimensions `[<number of bands>,<number of pixels>]`.
;     
;    DIVHandling: in, required , type = int
;       This value defines how the data ignore value of the image data is interpreted::
;       
;         DIVHandling = 0 -> mask a pixel if ANY band is equal `imageDIV`
;         DIVHandling = 1 -> mask a pixel if ALL bands are equal `imageDIV`
;         DIVHandling = 2 -> neglect the `imageDIV` and consider an external mask only
;         DIVHandling = 3 -> combine the external mask with the internal mask
;                            only available in combination with `/PreserveDimensions` 
;
;
; :Keywords:
;    imageDIV: in, optional, type=numeric
;     A numeric value (DIV = Data Ignore Value) that, if existing in the image data, marks masked / not valid values that are to 
;     be excluded from further calculations.
;     
;    externalMask: in, optional, type=numeric[]
;       A vector showing which pixel/sample of the image data is to be masked out.
;    
;    PreserveDimensions: in, optional, type=bool
;      Set this to return a mask with the same number of bands as in `imageData`.
;       
;-
function hubHelper::getImageMask, imageData, DIVHandling $
    , imageDIV=imageDIV $
    , externalMask=externalMask $
    , PreserveDimensions=PreserveDimensions
  
  compile_opt static, strictarr
  
  if typename(imageDIV) eq 'STRING' then message, 'imageDIV must be numeric'
  
  ;0 => mask if ANY band is DIV
  ;1 => mask if ALL bands are DIV  
  ;2 => use external mask only, do not care about internal mask
  ;3 => map external mask on internal mask, 
  ;     only available in combination with /PreserveDimensions 
  if total(DIVHandling eq [0,1,2,3]) ne 1 then message, 'DIVHandling must be 0,1,2 or 3!'
  
  nB = keyword_set(PreserveDimensions) ? n_elements(imageData[*,0]) : 1
  nP = n_elements(imageData[0,*])

  ;create internal mask as base for the final result
  mask = make_array(nB, nP, value=1b)
  
  
  ;add character of internal mask
  if isa(imageDIV) then begin
    case DIVHandling of
        0 : mask[*,*] = rebin(transpose(product(imageData ne imageDIV, 1)) ne 0, nB, nP) ;mask if ANY band is DIV    
        1 : mask[*,*] = rebin(transpose(total(imageData ne imageDIV, 1)) gt 0, nB, nP) ;mask if ALL bands are DIV
      
        2 : ;nothing
        3 : mask = imageData ne imageDIV
    endcase
    
  endif
  
  ;add character of external mask
  ;external mask -> pixel = 0 -> masked, pixel != 0 -> not masked
  if isa(externalMask) then begin
      for i = 0, nB-1 do begin
        mask[i,*] = mask[i,*] * externalMask
      endfor
  endif 
  return, mask
end

function hubHelper::hexColor, rgb
stop
;  compile_opt, idl2, static
;  return, strjoin(string(rgb, Format='(Z)'),'')
end
  
function hubHelper::getHistogramLocationIndices, reverse_indices, numberOfBins, Sparse=sparse

  compile_opt static, strictarr

  on_error,2
  
  indicesList = list()

  ;foreach location,locations,bin do begin
  for bin=0,numberOfBins-1 do begin
    
  
    ; get value and indices for the current bin

    binIsNotEmpty = reverse_indices[bin] ne reverse_indices[bin+1]
    if binIsNotEmpty then begin 
      indices = reverse_indices[reverse_indices[bin] : reverse_indices[bin+1]-1]
    endif else begin
      indices = !null
    endelse
    
    ; save value and indices

    if binIsNotEmpty or ~keyword_set(sparse) then begin
      indicesList.add, indices
    endif
    
  endfor
  
  return, indicesList

end

;+
; :hidden:
;-
pro test_hubHelper_getImageMask

h = hubHelper()
imageData = [[ 3.7, 2.2, 4.7] $ ;pixel 1: all bands valid
           , [ 6.2, 1.3, 5.4] $ ;      
           , [ 5.9, 3.8, -1 ] $ ;pixel 3: one band masked 
           , [ 1.6, -1 , -1 ] $ ;
           , [ -1 , -1 , -1 ] $ ;pixel 5: all bands masked
           ]
help, imageData           

;mask      = transpose([    1,   1,   0,   1,  0])

result = h.getImageMask(imageData, 0 $
      , externalMask=mask $
      , imageDIV=-1 $
      , preserveDimension=0)  
help, result
print, result 
end


;+
; :Description:
;    Tests if a variable can be converted into a numeric target type
;
; :Params:
;    variable: in, required
;     The variable.
;    targetType: in, required, type = idl type code | type string
;     The type you want to convert `variable` into.
;-
function hubHelper::canConvert, variable, targetType
  
  compile_opt static, strictarr

  if TYPENAME(targetType) eq 'STRING' then begin
    typenames = hubHelper.getDataTypeInfo(/TYPENAMES)
    typecodes = hubHelper.getDataTypeInfo(/TYPECODES)
    idx = where(STRLOWCASE(targetType) eq typenames, /NULL)
    if ~isa(idx) then message, 'unknown data type as targetType'
    ttype = typecodes[idx]
  endif else begin
    ttype = targetType
  endelse
  
  on_ioerror, ioerror
  catch, conversion_error
  
  if conversion_error ne 0 then begin
    ioerror:
    CATCH, /CANCEL
    return, 0b
  endif
  
  
  case ttype of
       1         : !NULL = byte(variable)
       2         : !NULL = fix(variable)
       3         : !NULL = long(variable)
       4         : !NULL = float(variable)
       5         : !NULL = double(variable)
       6         : !NULL = complex(variable)
       9         : !NULL = dcomplex(variable)         
       12        : !NULL = uint(variable)
       13        : !NULL = ulong(variable)
       14        : !NULL = long64(variable)
       15        : !NULL = ulong64(variable)
       else : message, 'cast not implemented for type "'+strtrim(targetType,2)+'"'
  endcase   
  
  return, 1b
end 

;+
; :Description:
;    Performs a numeric cast on a certain variable.
;
; :Params:
;    targetType: in, required, type = type code | type string
;     The numeric type you want to convert the `variable` into.
;
; :Keywords:
;    variable: in, required
;     The variable you want to convert the data type of.
;s
;-
pro hubHelper::doNumericCast, variable=variable, targetType

  compile_opt static, strictarr

  if ~isa(variable) then message, 'Variable to get casted must be set'
  vtype = size(variable, /TYPE)
  if TYPENAME(targetType) eq 'STRING' then begin
    typenames = hubHelper.getDataTypeInfo(/TYPENAMES)
    typecodes = hubHelper.getDataTypeInfo(/TYPECODES)
    idx = where(STRLOWCASE(targetType) eq typenames, /NULL)
    if ~isa(idx) then message, 'unknown data type as targetType'
    ttype = typecodes[idx]
  endif else begin
    ttype = targetType
  endelse
  
  if vtype ne ttype then begin
    
    case ttype of
         1         : variable = byte(temporary(variable))
         2         : variable = fix(temporary(variable))
         3         : variable = long(temporary(variable))
         4         : variable = float(temporary(variable))
         5         : variable = double(temporary(variable))
         6         : variable = complex(temporary(variable))
         9         : variable = dcomplex(temporary(variable))         
         12        : variable = uint(temporary(variable))
         13        : variable = ulong(temporary(variable))
         14        : variable = long64(temporary(variable))
         15        : variable = ulong64(temporary(variable))
         else : message, 'cast not implemented for type "'+strtrim(targetType,2)+'"'
    endcase 
  endif
end


;+
; :hidden:
;-
pro test_hubHelper_doNumericCast
 h = hubHelper()
 t = [['0.45','.045','1.2','0.0034'] $
     ,['0.45','.045','1.2','0.0034'] $
     ]
     
 help, t
 help, byte(t)    
 ;h.doNumericCast, variable=t, 'byte' & print, t & help, t
 stop
end


;+
; :Description:
;   This function returns information on supported file types and their restrictions.
;   Might be useful for widget where a user can choose between different file types.  
;
; :Keywords:
;    Names: in, optional, type=bool
;     Set this keyword to return supported file types.
;     
;    NumberOfBands: in, optional, type=bool
;     Set this keyword to return a vector with maximum number of bands a 
;     supported file type can have. In case a value is -1 the number of bands is not limited 
;    
;
;-
function hubHelper::getFileTypeInfo, Names=Names, NumberOfBands=NumberOfBands

  compile_opt static, strictarr

  temp = keyword_set(Names) + keyword_set(NumberOfBands)  
  if temp gt 1 or temp eq 0 then begin
    message, 'Set either keyword "Names" or "NumberOfBands"'
  endif
  if keyword_set(Names) then begin
    return, ['ENVI Standard','ENVI Classification','ENVI Spectral Library', 'EnMAP-Box Regression']
  endif
  if keyword_set(NumberOfBands) then begin
    return, [-1,1,1, 1]
  endif
  return, !NULL
end

;+
; :Description:
;    Returns information on data types-
;
; :Params:
;    dataType: in, optional, type = type code | type string
;     A certain data type you want to get information for.
;
; :Keywords:
;    TypeCodes
;    TypeNames
;    TypeDescriptions
;    TypeSizes
;
;-
function hubHelper::getDataTypeInfo, dataType $
          , TypeCodes=TypeCodes $
          , TypeNames=TypeNames $
          , TypeDescriptions=TypeDescriptions $
          , TypeSizes=TypeSizes
  
  compile_opt static, strictarr

  exclusive = hub_isExclusiveKeyword(TypeCodes, TypeNames, TypeDescriptions, TypeSizes)
  if ~exclusive then begin
    message, 'Set one of the exclusive keywords: TypCode, TypeName, TypeDescription or TypeSizes'
  endif
  
  descriptions   = ['Byte (8 bits)' $
                   ,'Integer (16 bits)' $
                   ,'Long integer (32 bits)' $
                   ,'Floating-point (32 bits)' $
                   ,'Double-precision floating-point (64 bits)' $
                   ,'Unsigned integer (16 bits)' $
                   ,'Unsigned long integer (32 bits)' $
                   ,'Long 64-bit integer' $
                   ,'Unsigned long 64-bit integer']
  
  names = ['byte','int','long', 'float', 'double','uint', 'ulong','long64','ulong64']
  codes = [1     ,2        ,3     , 4      , 5       ,12    , 13     ,14      ,15       ]
  sizes = [1     ,2        ,4     , 4      , 8       , 2    , 4      ,8       ,8        ]
  indices = [0,1,2,3,4,5,0,0,0,0,0,0,6,7,8,9]-1
  
  inputCodes = isa(dataType) ? hubMathHelper.resolveDataTypeAlias(dataType) : codes
  inputIndices = indices[inputCodes]
  
  if keyword_set(TypeDescriptions) then result = descriptions[inputIndices]
  if keyword_set(TypeNames) then result = names[inputIndices]
  if keyword_set(TypeCodes) then result = codes[inputIndices]
  if keyword_set(TypeSizes) then result = sizes[inputIndices]
  
  result = n_elements(result) eq 1 ? result[0] : result
  return, result
end

;+
; :Description:
;    Returns for each value in `values` the index in `array` where it occurres first. 
;
; :Params:
;    array: in, required, type = array
;    values: in, required, type = array
;    
; :Returns:
;    List with same length as in `values`. Contains the index of first occurrence in `array` or
;    !NULL.  
;    
; :Keywords:
;    ExcludeMissing: in, optional, type = boolean
;     Set this to exclude NULL values from the resulting list.
;     This will return only indices of elements in `array` that exist in `values` too.
;
;-
function hubHelper::getFirstIndex, array, values, ExcludeMissing=ExcludeMissing

  compile_opt static, strictarr

  l = list()
  foreach value, values do begin
    indices = where(array eq value, /NULL)
    if isa(indices) then begin
      l.add, indices[0]
    endif else begin
      if ~keyword_set(ExcludeMissing) then l.add, !NULL
    endelse
  endforeach
  return, l
end

function hubHelper::getShortenedString, text, maxLength, replacementText=ReplacementText

  compile_opt static, strictarr


 if maxLength lt 3 then message, 'maxLength must be >= 3' 
  textLength = strlen(text)
  if textLength le maxLength then return, text
  
  if ~keyword_set(replacementText) then replacementText = '...'
  length_replacement = strlen(replacementText)
  toRemove = (textLength - maxLength + length_replacement)/2
  pos1 = textLength/2 - toRemove
  pos2 = textLength/2 + toRemove
  return, strmid(text, 0, pos1) + replacementText + strmid(text, pos2, textLength-1)
end
;
;pro test_hubHelper_getShortenedString
;  
;  text = "W:\Something\XSW\FileFolder\Filepath.xsl"
;  maxLength = 20
;  result = hubHelper.getShortenedString(text, maxLength)
;  print, result
;  print, strlen(result)
;end
;
;+
; :Description:
;    This function returns a natural number that can be used as data ingore value 
;    since it is out of the range given by maxRange. By default the data ignore value will be 
;    something like this: -1, -10, -100, ... 
;    
; :Params:
;    maxRange: in, required, type=numeric[]
;     The maximum possible range. 
; :Keywords:
;    nine: in, optional, type=boolean
;     The data ignore value is a number of the following row: -9, -99, -999, ...
;
;-
function hubHelper::getDataIgnoreValueSuggestion, maxRange, nine=nine
  Nine=Nine
  maxExpectedRange = [floor(maxRange[0]), ceil(maxRange[1])]
  absRange = abs(maxExpectedRange)
  iNeg = where(maxExpectedRange lt 0, /NULL) 

  compile_opt static, strictarr

  exponent = fix(ALOG10(absRange+10))
  if keyword_set(nine) then begin
    divs = 10^(exponent+2) - 1
  endif else begin
    divs = 10^(exponent+1)
  endelse
  if isa(iNeg) then divs[iNeg] = -divs[iNeg]  
  divs= divs[sort(divs)]
  return, divs[0]
end

function hubHelper::getTimeString

  compile_opt static, strictarr

  return, strjoin((strsplit(/EXTRACT,systime(),' :'))[[8,1,2,3,4,5]]+['_','_','_','h','m','s'])

end

function hubHelper::filepath, filename, _REF_EXTRA=_extra, replace

  compile_opt static, strictarr

  ; wrapper for IDL filepath function

  filepath = filepath(filename, _EXTRA=_extra)

  ; filter forbidden characters

  case !VERSION.OS_FAMILY of
    'Windows' : forbidden = '\/:*?"<>|'
    'unix'    : forbidden = ':'
  endcase
  
  basename = file_basename(filepath)
  if isa(replace) then begin
    filteredBasename = strjoin(strsplit(basename, forbidden, /EXTRACT), replace)
  endif else begin
    filteredBasename = strjoin(strsplit(basename, forbidden, /EXTRACT), '_')
  endelse
  valid = basename eq filteredBasename
  if ~valid and ~isa(replace) then begin
    message, 'Invalid filepath: '+string(filepath)
  endif
  
  return, filepath(filteredBasename, ROOT_DIR=file_dirname(filepath))
  
end

function hubHelper::removeFileExtension, filename
  
  compile_opt static, strictarr
 

  fileDirectoryName = file_dirname(filename)
  fileBasename = file_basename(filename)
  splitResult = strsplit(fileBasename,'.',/Extract)
  if n_elements(splitResult) gt 1 then begin
    fileBasenameWithoutExtension = strjoin(splitResult[0:-2],'.')
  endif else begin
    fileBasenameWithoutExtension = strjoin(splitResult)
  endelse
  
  if filedirectoryName ne '.' then begin
    filenameWithoutExtension = filepath(fileBasenameWithoutExtension, ROOT_DIR=fileDirectoryName)
  endif else begin
    filenameWithoutExtension = fileBasenameWithoutExtension
  endelse
  return, filenameWithoutExtension

end

function hubHelper::getFileExtension, filename

  compile_opt static, strictarr

  splitResult = strsplit(file_basename(filename),'.',/Extract)
  if n_elements(splitResult) gt 1 then begin
    fileExtension = splitResult[-1]
  endif else begin
    fileExtension = ''
  endelse
  return, fileExtension
end

pro hubHelper::openFileLocation $
  , filename

  compile_opt static, strictarr

  case !VERSION.OS of
    'Win32' : begin
      command = 'explorer /select, '+filename
      spawn, command
    end
    'darwin' : begin
      command = 'PATH=$PATH:/usr/bin; open -a Finder '+file_dirname(filename)
      spawn, command
    end
    'linux' : begin
      xdisplayfile, TEXT='File location: '+filename
    end
  endcase

end

pro hubHelper::openFile $
  , filename $
  , ASCII=ascii $
  , PDF=pdf $
  , HTML=html $
  , AllowNonExistence=allowNonExistence

  compile_opt static, strictarr

  if ~keyword_set(html) and ~file_test(filename) then begin
    if keyword_set(allowNonExistence) then begin
      return
    endif else begin
      message, 'File does not exist: '+filename
    endelse
  endif

  case !VERSION.OS_FAMILY of
    'Windows' : begin
      hasBlanks = stregex(filename, ' ', /BOOLEAN)
      if hasBlanks then begin
        spawn, '"'+filename+'"'
      endif else begin
        spawn, 'start '+filename
      endelse
    end
    'unix' : begin
      if strcmp(!VERSION.OS, 'darwin',/FOLD_CASE)  then begin
        spawn, 'PATH=$PATH:/usr/bin; open '+filename+' &'
        return
      endif

      if keyword_set(ascii) then begin
        xdisplayfile, filename, /EDITABLE
        return
      endif

      if keyword_set(html) then begin
        viewerNames =    ['Firefox', 'Chromium']
        viewerCommands = ['firefox', 'chromium-browser']
 
        foreach viewerCommand, viewerCommands, i do begin
          spawn, 'PATH=$PATH:/usr/bin; which '+viewerCommand, spawnResult
          if spawnResult[0] eq '' then continue
          spawn, 'PATH=$PATH:/usr/bin; '+viewerCommand+' '+filename+' &', spawnResult, spawnError
          return
        endforeach
      endif

      if keyword_set(pdf) then begin
        viewerNames =    ['Okular', 'Evince', 'Acrobat Reader'];, 'Xpdf']
        viewerCommands = ['okular', 'evince', 'acroread'];, 'xpdf']
 
        foreach viewerCommand, viewerCommands, i do begin
          spawn, 'PATH=$PATH:/usr/bin; which '+viewerCommand, spawnResult
          if spawnResult[0] eq '' then continue
          spawn, 'PATH=$PATH:/usr/bin; '+viewerCommand+' '+filename+' &', spawnResult, spawnError
          return
        endforeach
      endif
      
      hubAMW_program ,Title='Open File in System Editor'
      hubAMW_label, ['Sorry, up to now, files can not be opened automatically.' $
                 ,'Please enter the command for starting the correct file viewer' $
                 ,'' $
                 ,'Filename: '+filename]
      hubAMW_parameter, 'command', Title='Command',Size=100, /STRING
      result = hubAMW_manage()
      if result['accept'] then begin
        viewerCommand = result['command']
        spawn, 'PATH=$PATH:/usr/bin; which '+viewerCommand, spawnResult
        if spawnResult[0] eq '' then begin
          ok = dialog_message("No command '"+viewerCommand+"' found.", /ERROR)
        endif else begin
          spawn, 'PATH=$PATH:/usr/bin; '+viewerCommand+' '+filename+' &', spawnResult, spawnError
        endelse
      endif
   
    end
  endcase

end

function hubHelper::getTypeInfo $
  ,type

  compile_opt static, strictarr

  typeName=size(value,/TNAME)
  
  case type of
    1:  typeSize = 1  ;Byte (8 bits)
    2:  typeSize = 2  ;Integer (16 bits)
    3:  typeSize = 4  ;Long integer (32 bits)
    4:  typeSize = 4  ;Floating-point (32 bits)
    5:  typeSize = 8  ;Double-precision floating-point (64 bits)
    12: typeSize = 2  ;Unsigned integer (16 bits)
    13: typeSize = 4  ;Unsigned long integer (32 bits)
    14: typeSize = 8  ;Long 64-bit integer
    15: typeSize = 8  ;Unsigned long 64-bit integer
    else: message,'Wrong type.'
  endcase

  result = {name:typeName,size:typeSize}
  return,result
  
end

function hubHelper::getByteOrder

  compile_opt static, strictarr

  testNumber = 1
  byteorder,testNumber,/SWAP_IF_LITTLE_ENDIAN
  byteOrder = fix(testNumber eq 1)
  return,byteOrder
  
end

function hubHelper::getRoutineName
  
  compile_opt static, strictarr

  callStack = scope_traceback()
  routineName = (strsplit(/Extract,callstack[-2]))[0]
  return,routineName

end

function hubHelper::isElementOfArray, scalar, array
  compile_opt static, strictarr

  return, total(scalar eq array) gt 0
end

pro hubHelper__define
  compile_opt static, strictarr
  struct = {hubHelper $
    ,inherits IDL_Object $
  }
end

pro test_helper
  print, (hubHelper()).getByteOrder()
end