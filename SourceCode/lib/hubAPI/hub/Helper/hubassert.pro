
;+
; :Description:
;    checks an assertion 
;
; :Params:
;    assertion
;    msg
;
;
;
; :Author: geo_beja
;-
pro hubAssert, assertion, msg
  
  on_error, 2

  if n_elements(assertion) ne 1 then begin
    message, 'assert argument is not a scalar'
  endif

  if assertion then return

  help,/trace, output = out
  if n_elements(msg) ne 0 then begin
    message, msg + out[1]
  endif else begin
    message, 'Assertion failed at: '+out[1]
  endelse
end