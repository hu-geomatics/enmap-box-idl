; wrapper for string() to correctly handle byte data
function hubString, s
  if isa(s, 'byte') then begin
    result = string(fix(s))
  endif else begin
    result = string(s)
  endelse
  return, result
end