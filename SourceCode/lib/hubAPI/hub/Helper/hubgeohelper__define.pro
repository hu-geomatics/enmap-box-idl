;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2014.
;-

;+
; :Description:
;    Constructor
;
; :Params:
;    inputImage: in, optional, type=hubIOImgInputImage
;     Use this to initialize the geo helper to the map info of an image.
;
;
;-
function hubGeoHelper::init, inputImage
  if isa(inputImage) then self.initByImage, inputImage
  return, 1b
end

;+
; :Description:
;    Initializes the hubGeoHelper to the spatial properties of an image
;
; :Params:
;    hubIOImage: in, required, type=hubIOInputImage or hubIOOutputImage
;     An image with `map info` meta tag. 
;     
;
;
;-
pro hubGeoHelper::initByImage, hubIOImage
  img = TYPENAME(hubIOImage) EQ 'STRING' ? hubIOImgInputImage(hubIOImage) : hubIOImage 
  if ~img.hasMeta('map info') then message, 'image map info meta tag is undefined'
  
  self.initByMapInfo, img.getMeta('map info') $
                    , img.getMeta('samples') $
                    , img.getMeta('lines') 
end

;+
; :Description:
;    Initializes the geo helper using a mapInfo structure.
;
; :Params:
;    mapInfo: in, required, type=mapInfo structure
;     
;    samples: in, required, type=int
;     Number of image samples / columns.
;    lines: in, required, type=int
;     Number of image lines / rows.
;
;
;-
pro hubGeoHelper::initByMapInfo, mapInfo, samples, lines
  ;copy mapInfo struct
  mI = mapInfo
  self.mapInfo = ptr_new(mI)
  self.nLines   = lines
  self.nSamples = samples
  ;upper left corner and lower right corner as references
  self.UL.x = mapInfo.Easting  - double(mapInfo.PixelX) * mapInfo.sizeX 
  self.UL.y = mapInfo.Northing + double(mapInfo.PixelY) * mapInfo.sizeY 
  self.LR.x = self.UL.x + self.nSamples * mapInfo.sizeX
  self.LR.y = self.UL.y - self.nLines * mapInfo.sizeY
  
  bb = self.createGeometry([self.UL.X,self.LR.X], [self.UL.Y, self.LR.Y])
  bb = self.getBoundingBox(bb)
  self.boundaries = ptr_new(bb)
  self.isInitialized = 1b
end



;+
; :Description:
;    Checks whether the image this geoHelper is related intersects with a spatial geometry. 
;
; :Params:
;    geometry: in, required, type=geometry structure
;     This structure describes a geometry.
;
;-
function hubGeoHelper::imageContains, geometry
  self._checkInitialization
  bounds = *self.boundaries
  
  iOut = where(geometry.x lt bounds.X[0] or $
               geometry.x gt bounds.X[1] or $
               geometry.y lt bounds.Y[0] or $
               geometry.y gt bounds.y[1], /NULL)
               
  
  return, ~isa(iOut)
end


function hubGeoHelper::imageIntersection, geometry
  self._checkInitialization
  bounds = *self.boundaries
  iOut = where((geometry.x ge bounds.X[0] and geometry.x le bounds.X[1]) and $
               (geometry.y ge bounds.Y[0] and geometry.y le bounds.y[1]), /NULL)
  return, isa(iOut)
end



;+
; :Description:
;    This function returns the boundaries or minimum bounding box of the image the geoHelper is set to.
;    The boundary coordinates are returned using a geometry struct as defined by `hubGeoHelper::createGeometry`.
;
;-
function hubGeoHelper::getImageBoundaries
  self._checkInitialization
  return, *self.boundaries
end

function hubGeoHelper::isOverlap, avec, bvec
  a = min(avec, MAX=b)
  c = min(bvec, MAX=d)
  
  r =    (a le c && c le b) $ ; c between a-b 
      || (a le d && d le b) $ ; d between a-b
      || (c le a && a le d) $ ; a between c-d
      || (c le b && b le d) $ ; b between c-d
      || (a le c && d le b) $ ; a-b encloses c-d 
      || (c le a && b le d)   ; c-d encloses a-b

  return, r
end

function hubGeoHelper::isBoundingBoxIntersection, geom1, geom2
  bb1 = self.getBoundingBox(geom1)
  bb2 = self.getBoundingBox(geom2)
  
  overlapX = self.isOverlap(bb1.x, bb2.x) 
  overlapY = self.isOverlap(bb1.y, bb2.y)
  
  return, overlapX && overlapY
end


;+
; :Description:
;    Returns the bounding box of a geometry.
;    
;    The bounding box is a geometry struct with::
;     
;     bb.x = [minX, maxX]
;     bb.y = [minY, maxY]
;     bb.type = 'boundingBox'
;
; :Params:
;    geometry: in, required, type = geometry struct
;
;
;-
function hubGeoHelper::getBoundingBox, geometry
  minX = min(geometry.x, max=maxX)
  minY = min(geometry.y, max=maxY)
  return, self.createGeometry([minX,maxX], [minY,maxY], type='boundingBox')
end

;+
; :Description:
;    Creates a simple geometry struct
;
; :Params:
;    xValues: in, required, type = numeric | numeric[]
;     X-Values
;    yValues: in, required, type = numeric | numeric[]
;
; :Keywords:
;    type: in, optional, type = string
;     A name that specifies the type of geometry.
;     By default the following names are used considering the number of 
;     coordinates specified by `xValues` and `yValues`::
;       
;       n_elements(xValues) | type name
;       --------------------+---------------
;                1          | point
;                2          | lines
;                3          | polygon
;       --------------------+---------------
;
;-
function hubGeoHelper::createGeometry, xValues, yValues, type=type
  if n_elements(xValues) ne n_elements(yValues) then message, 'Different number of x- and y coordinates'
  
  if ~isa(type) then begin
    case n_elements(xValues) of
      1 : type = 'point'
      2 : type = 'line'
      else : type = 'polygon'
    endcase
  endif 
  return, {x:xValues, y:yValues, type:type}
end

;+
; :Description:
;    Returns a hash containing the GCTP codes as defined in 
;    http://www.mathworks.de/de/help/matlab/ref/matlab.io.hdfeos.gd.spherecodetoname.html
; :Author: geo_beja
;-
function hubGeoHelper::getGCTPSpheroidCodes
  compile_opt static, strictarr
  Codes = Hash()
  Codes[0] = 'Clarke 1866'
  Codes[1] = 'Clarke 1880'
  Codes[2] = 'Bessel'
  Codes[3] = 'International 1967'
  Codes[4] = 'International 1909'
  Codes[5] = 'WGS-72'
  Codes[6] = 'Everest'
  Codes[7] = 'WGS-66'
  Codes[8] = 'GRS-1980'
  Codes[9] = 'Airy'
  Codes[10] = 'Modified Airy'
  Codes[11] = 'Modified Everest'
  Codes[12] = 'WGS-84'
  Codes[13] = 'Southeast Asia'
  Codes[14] = 'Australian National'
  Codes[15] = 'Krassovsky'
  Codes[16] = 'Hough'
  Codes[17] = 'Mercury 1960'
  Codes[18] = 'Modified Mercury 1968'
  Codes[19] = 'Sphere of radius 6370997m'
  Codes[20] = 'Sphere of radius 6371228m'
  Codes[21] = 'Sphere of radius 6371007.181m'
  return, Codes
end

function hubGeoHelper::getEPSGTypeCodes $
  , LinearUnits = LinearUnits $
  , AngularUnits = AngularUnits $
  , GeographicCSTypeCodes = GeographicCSTypeCodes $
  , GeodeticDatumCodes = GeodeticDatumCodes $
  , EllipsoidCodes = EllipsoidCodes $
  , PrimeMeridianCodes =  PrimeMeridianCodes $
  , ProjectedCSTypeCodes = ProjectedCSTypeCodes $
  , ProjectionCodes =  ProjectionCodes $
  , VerticalCSTypeCodes = VerticalCSTypeCodes $
  , CoordinateTransformationCodes = CoordinateTransformationCodes

  ;see http://www.remotesensing.org/geotiff/spec/geotiff6.html#6.3.2.2 for details
  compile_opt static, strictarr

  filters = list()
  if keyword_set(LinearUnits) then filters.add, 'Linear'
  if keyword_set(AngularUnits) then filters.add, 'Angular'
  if keyword_set(GeographicCSTypeCodes) then filters.add, ['GCS', 'GCSE'], /EXTRACT
  if keyword_set(GeodeticDatumCodes) then filters.add, ['Datum', 'DatumE'], /EXTRACT
  if keyword_set(EllipsoidCodes) then filters.add, 'Ellipse'
  if keyword_set(PrimeMeridianCodes) then filters.add, 'PM'
  if keyword_set(ProjectedCSTypeCodes) then filters.add, 'PCS'
  if keyword_set(ProjectionCodes) then filters.add, 'Proj'
  if keyword_set(CoordinateTransformationCodes) then filters.add, 'CT'
  if keyword_set(VerticalCSTypeCodes) then filters.add, 'VertCS'

  TCT = Hash()
  TCT[9001] = "Linear_Meter"
  TCT[9002] = "Linear_Foot"
  TCT[9003] = "Linear_Foot_US_Survey"
  TCT[9004] = "Linear_Foot_Modified_American"
  TCT[9005] = "Linear_Foot_Clarke"
  TCT[9006] = "Linear_Foot_Indian"
  TCT[9007] = "Linear_Link"
  TCT[9008] = "Linear_Link_Benoit"
  TCT[9009] = "Linear_Link_Sears"
  TCT[9010] = "Linear_Chain_Benoit"
  TCT[9011] = "Linear_Chain_Sears"
  TCT[9012] = "Linear_Yard_Sears"
  TCT[9013] = "Linear_Yard_Indian"
  TCT[9014] = "Linear_Fathom"
  TCT[9015] = "Linear_Mile_International_Nautical"
  TCT[9101] = "Angular_Radian"
  TCT[9102] = "Angular_Degree"
  TCT[9103] = "Angular_Arc_Minute"
  TCT[9104] = "Angular_Arc_Second"
  TCT[9105] = "Angular_Grad"
  TCT[9106] = "Angular_Gon"
  TCT[9107] = "Angular_DMS"
  TCT[9108] = "Angular_DMS_Hemisphere"
  TCT[4201] = "GCS_Adindan"
  TCT[4202] = "GCS_AGD66"
  TCT[4203] = "GCS_AGD84"
  TCT[4204] = "GCS_Ain_el_Abd"
  TCT[4205] = "GCS_Afgooye"
  TCT[4206] = "GCS_Agadez"
  TCT[4207] = "GCS_Lisbon"
  TCT[4208] = "GCS_Aratu"
  TCT[4209] = "GCS_Arc_1950"
  TCT[4210] = "GCS_Arc_1960"
  TCT[4211] = "GCS_Batavia"
  TCT[4212] = "GCS_Barbados"
  TCT[4213] = "GCS_Beduaram"
  TCT[4214] = "GCS_Beijing_1954"
  TCT[4215] = "GCS_Belge_1950"
  TCT[4216] = "GCS_Bermuda_1957"
  TCT[4217] = "GCS_Bern_1898"
  TCT[4218] = "GCS_Bogota"
  TCT[4219] = "GCS_Bukit_Rimpah"
  TCT[4220] = "GCS_Camacupa"
  TCT[4221] = "GCS_Campo_Inchauspe"
  TCT[4222] = "GCS_Cape"
  TCT[4223] = "GCS_Carthage"
  TCT[4224] = "GCS_Chua"
  TCT[4225] = "GCS_Corrego_Alegre"
  TCT[4226] = "GCS_Cote_d_Ivoire"
  TCT[4227] = "GCS_Deir_ez_Zor"
  TCT[4228] = "GCS_Douala"
  TCT[4229] = "GCS_Egypt_1907"
  TCT[4230] = "GCS_ED50"
  TCT[4231] = "GCS_ED87"
  TCT[4232] = "GCS_Fahud"
  TCT[4233] = "GCS_Gandajika_1970"
  TCT[4234] = "GCS_Garoua"
  TCT[4235] = "GCS_Guyane_Francaise"
  TCT[4236] = "GCS_Hu_Tzu_Shan"
  TCT[4237] = "GCS_HD72"
  TCT[4238] = "GCS_ID74"
  TCT[4239] = "GCS_Indian_1954"
  TCT[4240] = "GCS_Indian_1975"
  TCT[4241] = "GCS_Jamaica_1875"
  TCT[4242] = "GCS_JAD69"
  TCT[4243] = "GCS_Kalianpur"
  TCT[4244] = "GCS_Kandawala"
  TCT[4245] = "GCS_Kertau"
  TCT[4246] = "GCS_KOC"
  TCT[4247] = "GCS_La_Canoa"
  TCT[4248] = "GCS_PSAD56"
  TCT[4249] = "GCS_Lake"
  TCT[4250] = "GCS_Leigon"
  TCT[4251] = "GCS_Liberia_1964"
  TCT[4252] = "GCS_Lome"
  TCT[4253] = "GCS_Luzon_1911"
  TCT[4254] = "GCS_Hito_XVIII_1963"
  TCT[4255] = "GCS_Herat_North"
  TCT[4256] = "GCS_Mahe_1971"
  TCT[4257] = "GCS_Makassar"
  TCT[4258] = "GCS_EUREF89"
  TCT[4259] = "GCS_Malongo_1987"
  TCT[4260] = "GCS_Manoca"
  TCT[4261] = "GCS_Merchich"
  TCT[4262] = "GCS_Massawa"
  TCT[4263] = "GCS_Minna"
  TCT[4264] = "GCS_Mhast"
  TCT[4265] = "GCS_Monte_Mario"
  TCT[4266] = "GCS_M_poraloko"
  TCT[4267] = "GCS_NAD27"
  TCT[4268] = "GCS_NAD_Michigan"
  TCT[4269] = "GCS_NAD83"
  TCT[4270] = "GCS_Nahrwan_1967"
  TCT[4271] = "GCS_Naparima_1972"
  TCT[4272] = "GCS_GD49"
  TCT[4273] = "GCS_NGO_1948"
  TCT[4274] = "GCS_Datum_73"
  TCT[4275] = "GCS_NTF"
  TCT[4276] = "GCS_NSWC_9Z_2"
  TCT[4277] = "GCS_OSGB_1936"
  TCT[4278] = "GCS_OSGB70"
  TCT[4279] = "GCS_OS_SN80"
  TCT[4280] = "GCS_Padang"
  TCT[4281] = "GCS_Palestine_1923"
  TCT[4282] = "GCS_Pointe_Noire"
  TCT[4283] = "GCS_GDA94"
  TCT[4284] = "GCS_Pulkovo_1942"
  TCT[4285] = "GCS_Qatar"
  TCT[4286] = "GCS_Qatar_1948"
  TCT[4287] = "GCS_Qornoq"
  TCT[4288] = "GCS_Loma_Quintana"
  TCT[4289] = "GCS_Amersfoort"
  TCT[4290] = "GCS_RT38"
  TCT[4291] = "GCS_SAD69"
  TCT[4292] = "GCS_Sapper_Hill_1943"
  TCT[4293] = "GCS_Schwarzeck"
  TCT[4294] = "GCS_Segora"
  TCT[4295] = "GCS_Serindung"
  TCT[4296] = "GCS_Sudan"
  TCT[4297] = "GCS_Tananarive"
  TCT[4298] = "GCS_Timbalai_1948"
  TCT[4299] = "GCS_TM65"
  TCT[4300] = "GCS_TM75"
  TCT[4301] = "GCS_Tokyo"
  TCT[4302] = "GCS_Trinidad_1903"
  TCT[4303] = "GCS_TC_1948"
  TCT[4304] = "GCS_Voirol_1875"
  TCT[4305] = "GCS_Voirol_Unifie"
  TCT[4306] = "GCS_Bern_1938"
  TCT[4307] = "GCS_Nord_Sahara_1959"
  TCT[4308] = "GCS_Stockholm_1938"
  TCT[4309] = "GCS_Yacare"
  TCT[4310] = "GCS_Yoff"
  TCT[4311] = "GCS_Zanderij"
  TCT[4312] = "GCS_MGI"
  TCT[4313] = "GCS_Belge_1972"
  TCT[4314] = "GCS_DHDN"
  TCT[4315] = "GCS_Conakry_1905"
  TCT[4322] = "GCS_WGS_72"
  TCT[4324] = "GCS_WGS_72BE"
  TCT[4326] = "GCS_WGS_84"
  TCT[4801] = "GCS_Bern_1898_Bern"
  TCT[4802] = "GCS_Bogota_Bogota"
  TCT[4803] = "GCS_Lisbon_Lisbon"
  TCT[4804] = "GCS_Makassar_Jakarta"
  TCT[4805] = "GCS_MGI_Ferro"
  TCT[4806] = "GCS_Monte_Mario_Rome"
  TCT[4807] = "GCS_NTF_Paris"
  TCT[4808] = "GCS_Padang_Jakarta"
  TCT[4809] = "GCS_Belge_1950_Brussels"
  TCT[4810] = "GCS_Tananarive_Paris"
  TCT[4811] = "GCS_Voirol_1875_Paris"
  TCT[4812] = "GCS_Voirol_Unifie_Paris"
  TCT[4813] = "GCS_Batavia_Jakarta"
  TCT[4901] = "GCS_ATF_Paris"
  TCT[4902] = "GCS_NDG_Paris"
  TCT[4001] = "GCSE_Airy1830"
  TCT[4002] = "GCSE_AiryModified1849"
  TCT[4003] = "GCSE_AustralianNationalSpheroid"
  TCT[4004] = "GCSE_Bessel1841"
  TCT[4005] = "GCSE_BesselModified"
  TCT[4006] = "GCSE_BesselNamibia"
  TCT[4007] = "GCSE_Clarke1858"
  TCT[4008] = "GCSE_Clarke1866"
  TCT[4009] = "GCSE_Clarke1866Michigan"
  TCT[4010] = "GCSE_Clarke1880_Benoit"
  TCT[4011] = "GCSE_Clarke1880_IGN"
  TCT[4012] = "GCSE_Clarke1880_RGS"
  TCT[4013] = "GCSE_Clarke1880_Arc"
  TCT[4014] = "GCSE_Clarke1880_SGA1922"
  TCT[4015] = "GCSE_Everest1830_1937Adjustment"
  TCT[4016] = "GCSE_Everest1830_1967Definition"
  TCT[4017] = "GCSE_Everest1830_1975Definition"
  TCT[4018] = "GCSE_Everest1830Modified"
  TCT[4019] = "GCSE_GRS1980"
  TCT[4020] = "GCSE_Helmert1906"
  TCT[4021] = "GCSE_IndonesianNationalSpheroid"
  TCT[4022] = "GCSE_International1924"
  TCT[4023] = "GCSE_International1967"
  TCT[4024] = "GCSE_Krassowsky1940"
  TCT[4025] = "GCSE_NWL9D"
  TCT[4026] = "GCSE_NWL10D"
  TCT[4027] = "GCSE_Plessis1817"
  TCT[4028] = "GCSE_Struve1860"
  TCT[4029] = "GCSE_WarOffice"
  TCT[4030] = "GCSE_WGS84"
  TCT[4031] = "GCSE_GEM10C"
  TCT[4032] = "GCSE_OSU86F"
  TCT[4033] = "GCSE_OSU91A"
  TCT[4034] = "GCSE_Clarke1880"
  TCT[4035] = "GCSE_Sphere"
  TCT[6201] = "Datum_Adindan"
  TCT[6202] = "Datum_Australian_Geodetic_Datum_1966"
  TCT[6203] = "Datum_Australian_Geodetic_Datum_1984"
  TCT[6204] = "Datum_Ain_el_Abd_1970"
  TCT[6205] = "Datum_Afgooye"
  TCT[6206] = "Datum_Agadez"
  TCT[6207] = "Datum_Lisbon"
  TCT[6208] = "Datum_Aratu"
  TCT[6209] = "Datum_Arc_1950"
  TCT[6210] = "Datum_Arc_1960"
  TCT[6211] = "Datum_Batavia"
  TCT[6212] = "Datum_Barbados"
  TCT[6213] = "Datum_Beduaram"
  TCT[6214] = "Datum_Beijing_1954"
  TCT[6215] = "Datum_Reseau_National_Belge_1950"
  TCT[6216] = "Datum_Bermuda_1957"
  TCT[6217] = "Datum_Bern_1898"
  TCT[6218] = "Datum_Bogota"
  TCT[6219] = "Datum_Bukit_Rimpah"
  TCT[6220] = "Datum_Camacupa"
  TCT[6221] = "Datum_Campo_Inchauspe"
  TCT[6222] = "Datum_Cape"
  TCT[6223] = "Datum_Carthage"
  TCT[6224] = "Datum_Chua"
  TCT[6225] = "Datum_Corrego_Alegre"
  TCT[6226] = "Datum_Cote_d_Ivoire"
  TCT[6227] = "Datum_Deir_ez_Zor"
  TCT[6228] = "Datum_Douala"
  TCT[6229] = "Datum_Egypt_1907"
  TCT[6230] = "Datum_European_Datum_1950"
  TCT[6231] = "Datum_European_Datum_1987"
  TCT[6232] = "Datum_Fahud"
  TCT[6233] = "Datum_Gandajika_1970"
  TCT[6234] = "Datum_Garoua"
  TCT[6235] = "Datum_Guyane_Francaise"
  TCT[6236] = "Datum_Hu_Tzu_Shan"
  TCT[6237] = "Datum_Hungarian_Datum_1972"
  TCT[6238] = "Datum_Indonesian_Datum_1974"
  TCT[6239] = "Datum_Indian_1954"
  TCT[6240] = "Datum_Indian_1975"
  TCT[6241] = "Datum_Jamaica_1875"
  TCT[6242] = "Datum_Jamaica_1969"
  TCT[6243] = "Datum_Kalianpur"
  TCT[6244] = "Datum_Kandawala"
  TCT[6245] = "Datum_Kertau"
  TCT[6246] = "Datum_Kuwait_Oil_Company"
  TCT[6247] = "Datum_La_Canoa"
  TCT[6248] = "Datum_Provisional_S_American_Datum_1956"
  TCT[6249] = "Datum_Lake"
  TCT[6250] = "Datum_Leigon"
  TCT[6251] = "Datum_Liberia_1964"
  TCT[6252] = "Datum_Lome"
  TCT[6253] = "Datum_Luzon_1911"
  TCT[6254] = "Datum_Hito_XVIII_1963"
  TCT[6255] = "Datum_Herat_North"
  TCT[6256] = "Datum_Mahe_1971"
  TCT[6257] = "Datum_Makassar"
  TCT[6258] = "Datum_European_Reference_System_1989"
  TCT[6259] = "Datum_Malongo_1987"
  TCT[6260] = "Datum_Manoca"
  TCT[6261] = "Datum_Merchich"
  TCT[6262] = "Datum_Massawa"
  TCT[6263] = "Datum_Minna"
  TCT[6264] = "Datum_Mhast"
  TCT[6265] = "Datum_Monte_Mario"
  TCT[6266] = "Datum_M_poraloko"
  TCT[6267] = "Datum_North_American_Datum_1927"
  TCT[6268] = "Datum_NAD_Michigan"
  TCT[6269] = "Datum_North_American_Datum_1983"
  TCT[6270] = "Datum_Nahrwan_1967"
  TCT[6271] = "Datum_Naparima_1972"
  TCT[6272] = "Datum_New_Zealand_Geodetic_Datum_1949"
  TCT[6273] = "Datum_NGO_1948"
  TCT[6274] = "Datum_Datum_73"
  TCT[6275] = "Datum_Nouvelle_Triangulation_Francaise"
  TCT[6276] = "Datum_NSWC_9Z_2"
  TCT[6277] = "Datum_OSGB_1936"
  TCT[6278] = "Datum_OSGB_1970_SN"
  TCT[6279] = "Datum_OS_SN_1980"
  TCT[6280] = "Datum_Padang_1884"
  TCT[6281] = "Datum_Palestine_1923"
  TCT[6282] = "Datum_Pointe_Noire"
  TCT[6283] = "Datum_Geocentric_Datum_of_Australia_1994"
  TCT[6284] = "Datum_Pulkovo_1942"
  TCT[6285] = "Datum_Qatar"
  TCT[6286] = "Datum_Qatar_1948"
  TCT[6287] = "Datum_Qornoq"
  TCT[6288] = "Datum_Loma_Quintana"
  TCT[6289] = "Datum_Amersfoort"
  TCT[6290] = "Datum_RT38"
  TCT[6291] = "Datum_South_American_Datum_1969"
  TCT[6292] = "Datum_Sapper_Hill_1943"
  TCT[6293] = "Datum_Schwarzeck"
  TCT[6294] = "Datum_Segora"
  TCT[6295] = "Datum_Serindung"
  TCT[6296] = "Datum_Sudan"
  TCT[6297] = "Datum_Tananarive_1925"
  TCT[6298] = "Datum_Timbalai_1948"
  TCT[6299] = "Datum_TM65"
  TCT[6300] = "Datum_TM75"
  TCT[6301] = "Datum_Tokyo"
  TCT[6302] = "Datum_Trinidad_1903"
  TCT[6303] = "Datum_Trucial_Coast_1948"
  TCT[6304] = "Datum_Voirol_1875"
  TCT[6305] = "Datum_Voirol_Unifie_1960"
  TCT[6306] = "Datum_Bern_1938"
  TCT[6307] = "Datum_Nord_Sahara_1959"
  TCT[6308] = "Datum_Stockholm_1938"
  TCT[6309] = "Datum_Yacare"
  TCT[6310] = "Datum_Yoff"
  TCT[6311] = "Datum_Zanderij"
  TCT[6312] = "Datum_Militar_Geographische_Institut"
  TCT[6313] = "Datum_Reseau_National_Belge_1972"
  TCT[6314] = "Datum_Deutsche_Hauptdreiecksnetz"
  TCT[6315] = "Datum_Conakry_1905"
  TCT[6322] = "Datum_WGS72"
  TCT[6324] = "Datum_WGS72_Transit_Broadcast_Ephemeris"
  TCT[6326] = "Datum_WGS84"
  TCT[6901] = "Datum_Ancienne_Triangulation_Francaise"
  TCT[6902] = "Datum_Nord_de_Guerre"
  TCT[6001] = "DatumE_Airy1830"
  TCT[6002] = "DatumE_AiryModified1849"
  TCT[6003] = "DatumE_AustralianNationalSpheroid"
  TCT[6004] = "DatumE_Bessel1841"
  TCT[6005] = "DatumE_BesselModified"
  TCT[6006] = "DatumE_BesselNamibia"
  TCT[6007] = "DatumE_Clarke1858"
  TCT[6008] = "DatumE_Clarke1866"
  TCT[6009] = "DatumE_Clarke1866Michigan"
  TCT[6010] = "DatumE_Clarke1880_Benoit"
  TCT[6011] = "DatumE_Clarke1880_IGN"
  TCT[6012] = "DatumE_Clarke1880_RGS"
  TCT[6013] = "DatumE_Clarke1880_Arc"
  TCT[6014] = "DatumE_Clarke1880_SGA1922"
  TCT[6015] = "DatumE_Everest1830_1937Adjustment"
  TCT[6016] = "DatumE_Everest1830_1967Definition"
  TCT[6017] = "DatumE_Everest1830_1975Definition"
  TCT[6018] = "DatumE_Everest1830Modified"
  TCT[6019] = "DatumE_GRS1980"
  TCT[6020] = "DatumE_Helmert1906"
  TCT[6021] = "DatumE_IndonesianNationalSpheroid"
  TCT[6022] = "DatumE_International1924"
  TCT[6023] = "DatumE_International1967"
  TCT[6024] = "DatumE_Krassowsky1960"
  TCT[6025] = "DatumE_NWL9D"
  TCT[6026] = "DatumE_NWL10D"
  TCT[6027] = "DatumE_Plessis1817"
  TCT[6028] = "DatumE_Struve1860"
  TCT[6029] = "DatumE_WarOffice"
  TCT[6030] = "DatumE_WGS84"
  TCT[6031] = "DatumE_GEM10C"
  TCT[6032] = "DatumE_OSU86F"
  TCT[6033] = "DatumE_OSU91A"
  TCT[6034] = "DatumE_Clarke1880"
  TCT[6035] = "DatumE_Sphere"
  TCT[7001] = "Ellipse_Airy_1830"
  TCT[7002] = "Ellipse_Airy_Modified_1849"
  TCT[7003] = "Ellipse_Australian_National_Spheroid"
  TCT[7004] = "Ellipse_Bessel_1841"
  TCT[7005] = "Ellipse_Bessel_Modified"
  TCT[7006] = "Ellipse_Bessel_Namibia"
  TCT[7007] = "Ellipse_Clarke_1858"
  TCT[7008] = "Ellipse_Clarke_1866"
  TCT[7009] = "Ellipse_Clarke_1866_Michigan"
  TCT[7010] = "Ellipse_Clarke_1880_Benoit"
  TCT[7011] = "Ellipse_Clarke_1880_IGN"
  TCT[7012] = "Ellipse_Clarke_1880_RGS"
  TCT[7013] = "Ellipse_Clarke_1880_Arc"
  TCT[7014] = "Ellipse_Clarke_1880_SGA_1922"
  TCT[7015] = "Ellipse_Everest_1830_1937_Adjustment"
  TCT[7016] = "Ellipse_Everest_1830_1967_Definition"
  TCT[7017] = "Ellipse_Everest_1830_1975_Definition"
  TCT[7018] = "Ellipse_Everest_1830_Modified"
  TCT[7019] = "Ellipse_GRS_1980"
  TCT[7020] = "Ellipse_Helmert_1906"
  TCT[7021] = "Ellipse_Indonesian_National_Spheroid"
  TCT[7022] = "Ellipse_International_1924"
  TCT[7023] = "Ellipse_International_1967"
  TCT[7024] = "Ellipse_Krassowsky_1940"
  TCT[7025] = "Ellipse_NWL_9D"
  TCT[7026] = "Ellipse_NWL_10D"
  TCT[7027] = "Ellipse_Plessis_1817"
  TCT[7028] = "Ellipse_Struve_1860"
  TCT[7029] = "Ellipse_War_Office"
  TCT[7030] = "Ellipse_WGS_84"
  TCT[7031] = "Ellipse_GEM_10C"
  TCT[7032] = "Ellipse_OSU86F"
  TCT[7033] = "Ellipse_OSU91A"
  TCT[7034] = "Ellipse_Clarke_1880"
  TCT[7035] = "Ellipse_Sphere"
  TCT[8901] = "PM_Greenwich"
  TCT[8902] = "PM_Lisbon"
  TCT[8903] = "PM_Paris"
  TCT[8904] = "PM_Bogota"
  TCT[8905] = "PM_Madrid"
  TCT[8906] = "PM_Rome"
  TCT[8907] = "PM_Bern"
  TCT[8908] = "PM_Jakarta"
  TCT[8909] = "PM_Ferro"
  TCT[8910] = "PM_Brussels"
  TCT[8911] = "PM_Stockholm"
  TCT[20137] = "PCS_Adindan_UTM_zone_37N"
  TCT[20138] = "PCS_Adindan_UTM_zone_38N"
  TCT[20248] = "PCS_AGD66_AMG_zone_48"
  TCT[20249] = "PCS_AGD66_AMG_zone_49"
  TCT[20250] = "PCS_AGD66_AMG_zone_50"
  TCT[20251] = "PCS_AGD66_AMG_zone_51"
  TCT[20252] = "PCS_AGD66_AMG_zone_52"
  TCT[20253] = "PCS_AGD66_AMG_zone_53"
  TCT[20254] = "PCS_AGD66_AMG_zone_54"
  TCT[20255] = "PCS_AGD66_AMG_zone_55"
  TCT[20256] = "PCS_AGD66_AMG_zone_56"
  TCT[20257] = "PCS_AGD66_AMG_zone_57"
  TCT[20258] = "PCS_AGD66_AMG_zone_58"
  TCT[20348] = "PCS_AGD84_AMG_zone_48"
  TCT[20349] = "PCS_AGD84_AMG_zone_49"
  TCT[20350] = "PCS_AGD84_AMG_zone_50"
  TCT[20351] = "PCS_AGD84_AMG_zone_51"
  TCT[20352] = "PCS_AGD84_AMG_zone_52"
  TCT[20353] = "PCS_AGD84_AMG_zone_53"
  TCT[20354] = "PCS_AGD84_AMG_zone_54"
  TCT[20355] = "PCS_AGD84_AMG_zone_55"
  TCT[20356] = "PCS_AGD84_AMG_zone_56"
  TCT[20357] = "PCS_AGD84_AMG_zone_57"
  TCT[20358] = "PCS_AGD84_AMG_zone_58"
  TCT[20437] = "PCS_Ain_el_Abd_UTM_zone_37N"
  TCT[20438] = "PCS_Ain_el_Abd_UTM_zone_38N"
  TCT[20439] = "PCS_Ain_el_Abd_UTM_zone_39N"
  TCT[20499] = "PCS_Ain_el_Abd_Bahrain_Grid"
  TCT[20538] = "PCS_Afgooye_UTM_zone_38N"
  TCT[20539] = "PCS_Afgooye_UTM_zone_39N"
  TCT[20700] = "PCS_Lisbon_Portugese_Grid"
  TCT[20822] = "PCS_Aratu_UTM_zone_22S"
  TCT[20823] = "PCS_Aratu_UTM_zone_23S"
  TCT[20824] = "PCS_Aratu_UTM_zone_24S"
  TCT[20973] = "PCS_Arc_1950_Lo13"
  TCT[20975] = "PCS_Arc_1950_Lo15"
  TCT[20977] = "PCS_Arc_1950_Lo17"
  TCT[20979] = "PCS_Arc_1950_Lo19"
  TCT[20981] = "PCS_Arc_1950_Lo21"
  TCT[20983] = "PCS_Arc_1950_Lo23"
  TCT[20985] = "PCS_Arc_1950_Lo25"
  TCT[20987] = "PCS_Arc_1950_Lo27"
  TCT[20989] = "PCS_Arc_1950_Lo29"
  TCT[20991] = "PCS_Arc_1950_Lo31"
  TCT[20993] = "PCS_Arc_1950_Lo33"
  TCT[20995] = "PCS_Arc_1950_Lo35"
  TCT[21100] = "PCS_Batavia_NEIEZ"
  TCT[21148] = "PCS_Batavia_UTM_zone_48S"
  TCT[21149] = "PCS_Batavia_UTM_zone_49S"
  TCT[21150] = "PCS_Batavia_UTM_zone_50S"
  TCT[21413] = "PCS_Beijing_Gauss_zone_13"
  TCT[21414] = "PCS_Beijing_Gauss_zone_14"
  TCT[21415] = "PCS_Beijing_Gauss_zone_15"
  TCT[21416] = "PCS_Beijing_Gauss_zone_16"
  TCT[21417] = "PCS_Beijing_Gauss_zone_17"
  TCT[21418] = "PCS_Beijing_Gauss_zone_18"
  TCT[21419] = "PCS_Beijing_Gauss_zone_19"
  TCT[21420] = "PCS_Beijing_Gauss_zone_20"
  TCT[21421] = "PCS_Beijing_Gauss_zone_21"
  TCT[21422] = "PCS_Beijing_Gauss_zone_22"
  TCT[21423] = "PCS_Beijing_Gauss_zone_23"
  TCT[21473] = "PCS_Beijing_Gauss_13N"
  TCT[21474] = "PCS_Beijing_Gauss_14N"
  TCT[21475] = "PCS_Beijing_Gauss_15N"
  TCT[21476] = "PCS_Beijing_Gauss_16N"
  TCT[21477] = "PCS_Beijing_Gauss_17N"
  TCT[21478] = "PCS_Beijing_Gauss_18N"
  TCT[21479] = "PCS_Beijing_Gauss_19N"
  TCT[21480] = "PCS_Beijing_Gauss_20N"
  TCT[21481] = "PCS_Beijing_Gauss_21N"
  TCT[21482] = "PCS_Beijing_Gauss_22N"
  TCT[21483] = "PCS_Beijing_Gauss_23N"
  TCT[21500] = "PCS_Belge_Lambert_50"
  TCT[21790] = "PCS_Bern_1898_Swiss_Old"
  TCT[21817] = "PCS_Bogota_UTM_zone_17N"
  TCT[21818] = "PCS_Bogota_UTM_zone_18N"
  TCT[21891] = "PCS_Bogota_Colombia_3W"
  TCT[21892] = "PCS_Bogota_Colombia_Bogota"
  TCT[21893] = "PCS_Bogota_Colombia_3E"
  TCT[21894] = "PCS_Bogota_Colombia_6E"
  TCT[22032] = "PCS_Camacupa_UTM_32S"
  TCT[22033] = "PCS_Camacupa_UTM_33S"
  TCT[22191] = "PCS_C_Inchauspe_Argentina_1"
  TCT[22192] = "PCS_C_Inchauspe_Argentina_2"
  TCT[22193] = "PCS_C_Inchauspe_Argentina_3"
  TCT[22194] = "PCS_C_Inchauspe_Argentina_4"
  TCT[22195] = "PCS_C_Inchauspe_Argentina_5"
  TCT[22196] = "PCS_C_Inchauspe_Argentina_6"
  TCT[22197] = "PCS_C_Inchauspe_Argentina_7"
  TCT[22332] = "PCS_Carthage_UTM_zone_32N"
  TCT[22391] = "PCS_Carthage_Nord_Tunisie"
  TCT[22392] = "PCS_Carthage_Sud_Tunisie"
  TCT[22523] = "PCS_Corrego_Alegre_UTM_23S"
  TCT[22524] = "PCS_Corrego_Alegre_UTM_24S"
  TCT[22832] = "PCS_Douala_UTM_zone_32N"
  TCT[22992] = "PCS_Egypt_1907_Red_Belt"
  TCT[22993] = "PCS_Egypt_1907_Purple_Belt"
  TCT[22994] = "PCS_Egypt_1907_Ext_Purple"
  TCT[23028] = "PCS_ED50_UTM_zone_28N"
  TCT[23029] = "PCS_ED50_UTM_zone_29N"
  TCT[23030] = "PCS_ED50_UTM_zone_30N"
  TCT[23031] = "PCS_ED50_UTM_zone_31N"
  TCT[23032] = "PCS_ED50_UTM_zone_32N"
  TCT[23033] = "PCS_ED50_UTM_zone_33N"
  TCT[23034] = "PCS_ED50_UTM_zone_34N"
  TCT[23035] = "PCS_ED50_UTM_zone_35N"
  TCT[23036] = "PCS_ED50_UTM_zone_36N"
  TCT[23037] = "PCS_ED50_UTM_zone_37N"
  TCT[23038] = "PCS_ED50_UTM_zone_38N"
  TCT[23239] = "PCS_Fahud_UTM_zone_39N"
  TCT[23240] = "PCS_Fahud_UTM_zone_40N"
  TCT[23433] = "PCS_Garoua_UTM_zone_33N"
  TCT[23846] = "PCS_ID74_UTM_zone_46N"
  TCT[23847] = "PCS_ID74_UTM_zone_47N"
  TCT[23848] = "PCS_ID74_UTM_zone_48N"
  TCT[23849] = "PCS_ID74_UTM_zone_49N"
  TCT[23850] = "PCS_ID74_UTM_zone_50N"
  TCT[23851] = "PCS_ID74_UTM_zone_51N"
  TCT[23852] = "PCS_ID74_UTM_zone_52N"
  TCT[23853] = "PCS_ID74_UTM_zone_53N"
  TCT[23886] = "PCS_ID74_UTM_zone_46S"
  TCT[23887] = "PCS_ID74_UTM_zone_47S"
  TCT[23888] = "PCS_ID74_UTM_zone_48S"
  TCT[23889] = "PCS_ID74_UTM_zone_49S"
  TCT[23890] = "PCS_ID74_UTM_zone_50S"
  TCT[23891] = "PCS_ID74_UTM_zone_51S"
  TCT[23892] = "PCS_ID74_UTM_zone_52S"
  TCT[23893] = "PCS_ID74_UTM_zone_53S"
  TCT[23894] = "PCS_ID74_UTM_zone_54S"
  TCT[23947] = "PCS_Indian_1954_UTM_47N"
  TCT[23948] = "PCS_Indian_1954_UTM_48N"
  TCT[24047] = "PCS_Indian_1975_UTM_47N"
  TCT[24048] = "PCS_Indian_1975_UTM_48N"
  TCT[24100] = "PCS_Jamaica_1875_Old_Grid"
  TCT[24200] = "PCS_JAD69_Jamaica_Grid"
  TCT[24370] = "PCS_Kalianpur_India_0"
  TCT[24371] = "PCS_Kalianpur_India_I"
  TCT[24372] = "PCS_Kalianpur_India_IIa"
  TCT[24373] = "PCS_Kalianpur_India_IIIa"
  TCT[24374] = "PCS_Kalianpur_India_IVa"
  TCT[24382] = "PCS_Kalianpur_India_IIb"
  TCT[24383] = "PCS_Kalianpur_India_IIIb"
  TCT[24384] = "PCS_Kalianpur_India_IVb"
  TCT[24500] = "PCS_Kertau_Singapore_Grid"
  TCT[24547] = "PCS_Kertau_UTM_zone_47N"
  TCT[24548] = "PCS_Kertau_UTM_zone_48N"
  TCT[24720] = "PCS_La_Canoa_UTM_zone_20N"
  TCT[24721] = "PCS_La_Canoa_UTM_zone_21N"
  TCT[24818] = "PCS_PSAD56_UTM_zone_18N"
  TCT[24819] = "PCS_PSAD56_UTM_zone_19N"
  TCT[24820] = "PCS_PSAD56_UTM_zone_20N"
  TCT[24821] = "PCS_PSAD56_UTM_zone_21N"
  TCT[24877] = "PCS_PSAD56_UTM_zone_17S"
  TCT[24878] = "PCS_PSAD56_UTM_zone_18S"
  TCT[24879] = "PCS_PSAD56_UTM_zone_19S"
  TCT[24880] = "PCS_PSAD56_UTM_zone_20S"
  TCT[24891] = "PCS_PSAD56_Peru_west_zone"
  TCT[24892] = "PCS_PSAD56_Peru_central"
  TCT[24893] = "PCS_PSAD56_Peru_east_zone"
  TCT[25000] = "PCS_Leigon_Ghana_Grid"
  TCT[25231] = "PCS_Lome_UTM_zone_31N"
  TCT[25391] = "PCS_Luzon_Philippines_I"
  TCT[25392] = "PCS_Luzon_Philippines_II"
  TCT[25393] = "PCS_Luzon_Philippines_III"
  TCT[25394] = "PCS_Luzon_Philippines_IV"
  TCT[25395] = "PCS_Luzon_Philippines_V"
  TCT[25700] = "PCS_Makassar_NEIEZ"
  TCT[25932] = "PCS_Malongo_1987_UTM_32S"
  TCT[26191] = "PCS_Merchich_Nord_Maroc"
  TCT[26192] = "PCS_Merchich_Sud_Maroc"
  TCT[26193] = "PCS_Merchich_Sahara"
  TCT[26237] = "PCS_Massawa_UTM_zone_37N"
  TCT[26331] = "PCS_Minna_UTM_zone_31N"
  TCT[26332] = "PCS_Minna_UTM_zone_32N"
  TCT[26391] = "PCS_Minna_Nigeria_West"
  TCT[26392] = "PCS_Minna_Nigeria_Mid_Belt"
  TCT[26393] = "PCS_Minna_Nigeria_East"
  TCT[26432] = "PCS_Mhast_UTM_zone_32S"
  TCT[26591] = "PCS_Monte_Mario_Italy_1"
  TCT[26592] = "PCS_Monte_Mario_Italy_2"
  TCT[26632] = "PCS_M_poraloko_UTM_32N"
  TCT[26692] = "PCS_M_poraloko_UTM_32S"
  TCT[26703] = "PCS_NAD27_UTM_zone_3N"
  TCT[26704] = "PCS_NAD27_UTM_zone_4N"
  TCT[26705] = "PCS_NAD27_UTM_zone_5N"
  TCT[26706] = "PCS_NAD27_UTM_zone_6N"
  TCT[26707] = "PCS_NAD27_UTM_zone_7N"
  TCT[26708] = "PCS_NAD27_UTM_zone_8N"
  TCT[26709] = "PCS_NAD27_UTM_zone_9N"
  TCT[26710] = "PCS_NAD27_UTM_zone_10N"
  TCT[26711] = "PCS_NAD27_UTM_zone_11N"
  TCT[26712] = "PCS_NAD27_UTM_zone_12N"
  TCT[26713] = "PCS_NAD27_UTM_zone_13N"
  TCT[26714] = "PCS_NAD27_UTM_zone_14N"
  TCT[26715] = "PCS_NAD27_UTM_zone_15N"
  TCT[26716] = "PCS_NAD27_UTM_zone_16N"
  TCT[26717] = "PCS_NAD27_UTM_zone_17N"
  TCT[26718] = "PCS_NAD27_UTM_zone_18N"
  TCT[26719] = "PCS_NAD27_UTM_zone_19N"
  TCT[26720] = "PCS_NAD27_UTM_zone_20N"
  TCT[26721] = "PCS_NAD27_UTM_zone_21N"
  TCT[26722] = "PCS_NAD27_UTM_zone_22N"
  TCT[26729] = "PCS_NAD27_Alabama_East"
  TCT[26730] = "PCS_NAD27_Alabama_West"
  TCT[26731] = "PCS_NAD27_Alaska_zone_1"
  TCT[26732] = "PCS_NAD27_Alaska_zone_2"
  TCT[26733] = "PCS_NAD27_Alaska_zone_3"
  TCT[26734] = "PCS_NAD27_Alaska_zone_4"
  TCT[26735] = "PCS_NAD27_Alaska_zone_5"
  TCT[26736] = "PCS_NAD27_Alaska_zone_6"
  TCT[26737] = "PCS_NAD27_Alaska_zone_7"
  TCT[26738] = "PCS_NAD27_Alaska_zone_8"
  TCT[26739] = "PCS_NAD27_Alaska_zone_9"
  TCT[26740] = "PCS_NAD27_Alaska_zone_10"
  TCT[26741] = "PCS_NAD27_California_I"
  TCT[26742] = "PCS_NAD27_California_II"
  TCT[26743] = "PCS_NAD27_California_III"
  TCT[26744] = "PCS_NAD27_California_IV"
  TCT[26745] = "PCS_NAD27_California_V"
  TCT[26746] = "PCS_NAD27_California_VI"
  TCT[26747] = "PCS_NAD27_California_VII"
  TCT[26748] = "PCS_NAD27_Arizona_East"
  TCT[26749] = "PCS_NAD27_Arizona_Central"
  TCT[26750] = "PCS_NAD27_Arizona_West"
  TCT[26751] = "PCS_NAD27_Arkansas_North"
  TCT[26752] = "PCS_NAD27_Arkansas_South"
  TCT[26753] = "PCS_NAD27_Colorado_North"
  TCT[26754] = "PCS_NAD27_Colorado_Central"
  TCT[26755] = "PCS_NAD27_Colorado_South"
  TCT[26756] = "PCS_NAD27_Connecticut"
  TCT[26757] = "PCS_NAD27_Delaware"
  TCT[26758] = "PCS_NAD27_Florida_East"
  TCT[26759] = "PCS_NAD27_Florida_West"
  TCT[26760] = "PCS_NAD27_Florida_North"
  TCT[26761] = "PCS_NAD27_Hawaii_zone_1"
  TCT[26762] = "PCS_NAD27_Hawaii_zone_2"
  TCT[26763] = "PCS_NAD27_Hawaii_zone_3"
  TCT[26764] = "PCS_NAD27_Hawaii_zone_4"
  TCT[26765] = "PCS_NAD27_Hawaii_zone_5"
  TCT[26766] = "PCS_NAD27_Georgia_East"
  TCT[26767] = "PCS_NAD27_Georgia_West"
  TCT[26768] = "PCS_NAD27_Idaho_East"
  TCT[26769] = "PCS_NAD27_Idaho_Central"
  TCT[26770] = "PCS_NAD27_Idaho_West"
  TCT[26771] = "PCS_NAD27_Illinois_East"
  TCT[26772] = "PCS_NAD27_Illinois_West"
  TCT[26773] = "PCS_NAD27_Indiana_East"
  TCT[26774] = "PCS_NAD27_BLM_14N_feet"
  TCT[26774] = "PCS_NAD27_Indiana_West"
  TCT[26775] = "PCS_NAD27_BLM_15N_feet"
  TCT[26775] = "PCS_NAD27_Iowa_North"
  TCT[26776] = "PCS_NAD27_BLM_16N_feet"
  TCT[26776] = "PCS_NAD27_Iowa_South"
  TCT[26777] = "PCS_NAD27_BLM_17N_feet"
  TCT[26777] = "PCS_NAD27_Kansas_North"
  TCT[26778] = "PCS_NAD27_Kansas_South"
  TCT[26779] = "PCS_NAD27_Kentucky_North"
  TCT[26780] = "PCS_NAD27_Kentucky_South"
  TCT[26781] = "PCS_NAD27_Louisiana_North"
  TCT[26782] = "PCS_NAD27_Louisiana_South"
  TCT[26783] = "PCS_NAD27_Maine_East"
  TCT[26784] = "PCS_NAD27_Maine_West"
  TCT[26785] = "PCS_NAD27_Maryland"
  TCT[26786] = "PCS_NAD27_Massachusetts"
  TCT[26787] = "PCS_NAD27_Massachusetts_Is"
  TCT[26788] = "PCS_NAD27_Michigan_North"
  TCT[26789] = "PCS_NAD27_Michigan_Central"
  TCT[26790] = "PCS_NAD27_Michigan_South"
  TCT[26791] = "PCS_NAD27_Minnesota_North"
  TCT[26792] = "PCS_NAD27_Minnesota_Cent"
  TCT[26793] = "PCS_NAD27_Minnesota_South"
  TCT[26794] = "PCS_NAD27_Mississippi_East"
  TCT[26795] = "PCS_NAD27_Mississippi_West"
  TCT[26796] = "PCS_NAD27_Missouri_East"
  TCT[26797] = "PCS_NAD27_Missouri_Central"
  TCT[26798] = "PCS_NAD27_Missouri_West"
  TCT[26801] = "PCS_NAD_Michigan_Michigan_East"
  TCT[26802] = "PCS_NAD_Michigan_Michigan_Old_Central"
  TCT[26803] = "PCS_NAD_Michigan_Michigan_West"
  TCT[26903] = "PCS_NAD83_UTM_zone_3N"
  TCT[26904] = "PCS_NAD83_UTM_zone_4N"
  TCT[26905] = "PCS_NAD83_UTM_zone_5N"
  TCT[26906] = "PCS_NAD83_UTM_zone_6N"
  TCT[26907] = "PCS_NAD83_UTM_zone_7N"
  TCT[26908] = "PCS_NAD83_UTM_zone_8N"
  TCT[26909] = "PCS_NAD83_UTM_zone_9N"
  TCT[26910] = "PCS_NAD83_UTM_zone_10N"
  TCT[26911] = "PCS_NAD83_UTM_zone_11N"
  TCT[26912] = "PCS_NAD83_UTM_zone_12N"
  TCT[26913] = "PCS_NAD83_UTM_zone_13N"
  TCT[26914] = "PCS_NAD83_UTM_zone_14N"
  TCT[26915] = "PCS_NAD83_UTM_zone_15N"
  TCT[26916] = "PCS_NAD83_UTM_zone_16N"
  TCT[26917] = "PCS_NAD83_UTM_zone_17N"
  TCT[26918] = "PCS_NAD83_UTM_zone_18N"
  TCT[26919] = "PCS_NAD83_UTM_zone_19N"
  TCT[26920] = "PCS_NAD83_UTM_zone_20N"
  TCT[26921] = "PCS_NAD83_UTM_zone_21N"
  TCT[26922] = "PCS_NAD83_UTM_zone_22N"
  TCT[26923] = "PCS_NAD83_UTM_zone_23N"
  TCT[26929] = "PCS_NAD83_Alabama_East"
  TCT[26930] = "PCS_NAD83_Alabama_West"
  TCT[26931] = "PCS_NAD83_Alaska_zone_1"
  TCT[26932] = "PCS_NAD83_Alaska_zone_2"
  TCT[26933] = "PCS_NAD83_Alaska_zone_3"
  TCT[26934] = "PCS_NAD83_Alaska_zone_4"
  TCT[26935] = "PCS_NAD83_Alaska_zone_5"
  TCT[26936] = "PCS_NAD83_Alaska_zone_6"
  TCT[26937] = "PCS_NAD83_Alaska_zone_7"
  TCT[26938] = "PCS_NAD83_Alaska_zone_8"
  TCT[26939] = "PCS_NAD83_Alaska_zone_9"
  TCT[26940] = "PCS_NAD83_Alaska_zone_10"
  TCT[26941] = "PCS_NAD83_California_1"
  TCT[26942] = "PCS_NAD83_California_2"
  TCT[26943] = "PCS_NAD83_California_3"
  TCT[26944] = "PCS_NAD83_California_4"
  TCT[26945] = "PCS_NAD83_California_5"
  TCT[26946] = "PCS_NAD83_California_6"
  TCT[26948] = "PCS_NAD83_Arizona_East"
  TCT[26949] = "PCS_NAD83_Arizona_Central"
  TCT[26950] = "PCS_NAD83_Arizona_West"
  TCT[26951] = "PCS_NAD83_Arkansas_North"
  TCT[26952] = "PCS_NAD83_Arkansas_South"
  TCT[26953] = "PCS_NAD83_Colorado_North"
  TCT[26954] = "PCS_NAD83_Colorado_Central"
  TCT[26955] = "PCS_NAD83_Colorado_South"
  TCT[26956] = "PCS_NAD83_Connecticut"
  TCT[26957] = "PCS_NAD83_Delaware"
  TCT[26958] = "PCS_NAD83_Florida_East"
  TCT[26959] = "PCS_NAD83_Florida_West"
  TCT[26960] = "PCS_NAD83_Florida_North"
  TCT[26961] = "PCS_NAD83_Hawaii_zone_1"
  TCT[26962] = "PCS_NAD83_Hawaii_zone_2"
  TCT[26963] = "PCS_NAD83_Hawaii_zone_3"
  TCT[26964] = "PCS_NAD83_Hawaii_zone_4"
  TCT[26965] = "PCS_NAD83_Hawaii_zone_5"
  TCT[26966] = "PCS_NAD83_Georgia_East"
  TCT[26967] = "PCS_NAD83_Georgia_West"
  TCT[26968] = "PCS_NAD83_Idaho_East"
  TCT[26969] = "PCS_NAD83_Idaho_Central"
  TCT[26970] = "PCS_NAD83_Idaho_West"
  TCT[26971] = "PCS_NAD83_Illinois_East"
  TCT[26972] = "PCS_NAD83_Illinois_West"
  TCT[26973] = "PCS_NAD83_Indiana_East"
  TCT[26974] = "PCS_NAD83_Indiana_West"
  TCT[26975] = "PCS_NAD83_Iowa_North"
  TCT[26976] = "PCS_NAD83_Iowa_South"
  TCT[26977] = "PCS_NAD83_Kansas_North"
  TCT[26978] = "PCS_NAD83_Kansas_South"
  TCT[26979] = "PCS_NAD83_Kentucky_North"
  TCT[26980] = "PCS_NAD83_Kentucky_South"
  TCT[26981] = "PCS_NAD83_Louisiana_North"
  TCT[26982] = "PCS_NAD83_Louisiana_South"
  TCT[26983] = "PCS_NAD83_Maine_East"
  TCT[26984] = "PCS_NAD83_Maine_West"
  TCT[26985] = "PCS_NAD83_Maryland"
  TCT[26986] = "PCS_NAD83_Massachusetts"
  TCT[26987] = "PCS_NAD83_Massachusetts_Is"
  TCT[26988] = "PCS_NAD83_Michigan_North"
  TCT[26989] = "PCS_NAD83_Michigan_Central"
  TCT[26990] = "PCS_NAD83_Michigan_South"
  TCT[26991] = "PCS_NAD83_Minnesota_North"
  TCT[26992] = "PCS_NAD83_Minnesota_Cent"
  TCT[26993] = "PCS_NAD83_Minnesota_South"
  TCT[26994] = "PCS_NAD83_Mississippi_East"
  TCT[26995] = "PCS_NAD83_Mississippi_West"
  TCT[26996] = "PCS_NAD83_Missouri_East"
  TCT[26997] = "PCS_NAD83_Missouri_Central"
  TCT[26998] = "PCS_NAD83_Missouri_West"
  TCT[27038] = "PCS_Nahrwan_1967_UTM_38N"
  TCT[27039] = "PCS_Nahrwan_1967_UTM_39N"
  TCT[27040] = "PCS_Nahrwan_1967_UTM_40N"
  TCT[27120] = "PCS_Naparima_UTM_20N"
  TCT[27200] = "PCS_GD49_NZ_Map_Grid"
  TCT[27291] = "PCS_GD49_North_Island_Grid"
  TCT[27292] = "PCS_GD49_South_Island_Grid"
  TCT[27429] = "PCS_Datum_73_UTM_zone_29N"
  TCT[27500] = "PCS_ATF_Nord_de_Guerre"
  TCT[27581] = "PCS_NTF_France_I"
  TCT[27582] = "PCS_NTF_France_II"
  TCT[27583] = "PCS_NTF_France_III"
  TCT[27591] = "PCS_NTF_Nord_France"
  TCT[27592] = "PCS_NTF_Centre_France"
  TCT[27593] = "PCS_NTF_Sud_France"
  TCT[27700] = "PCS_British_National_Grid"
  TCT[28232] = "PCS_Point_Noire_UTM_32S"
  TCT[28348] = "PCS_GDA94_MGA_zone_48"
  TCT[28349] = "PCS_GDA94_MGA_zone_49"
  TCT[28350] = "PCS_GDA94_MGA_zone_50"
  TCT[28351] = "PCS_GDA94_MGA_zone_51"
  TCT[28352] = "PCS_GDA94_MGA_zone_52"
  TCT[28353] = "PCS_GDA94_MGA_zone_53"
  TCT[28354] = "PCS_GDA94_MGA_zone_54"
  TCT[28355] = "PCS_GDA94_MGA_zone_55"
  TCT[28356] = "PCS_GDA94_MGA_zone_56"
  TCT[28357] = "PCS_GDA94_MGA_zone_57"
  TCT[28358] = "PCS_GDA94_MGA_zone_58"
  TCT[28404] = "PCS_Pulkovo_Gauss_zone_4"
  TCT[28405] = "PCS_Pulkovo_Gauss_zone_5"
  TCT[28406] = "PCS_Pulkovo_Gauss_zone_6"
  TCT[28407] = "PCS_Pulkovo_Gauss_zone_7"
  TCT[28408] = "PCS_Pulkovo_Gauss_zone_8"
  TCT[28409] = "PCS_Pulkovo_Gauss_zone_9"
  TCT[28410] = "PCS_Pulkovo_Gauss_zone_10"
  TCT[28411] = "PCS_Pulkovo_Gauss_zone_11"
  TCT[28412] = "PCS_Pulkovo_Gauss_zone_12"
  TCT[28413] = "PCS_Pulkovo_Gauss_zone_13"
  TCT[28414] = "PCS_Pulkovo_Gauss_zone_14"
  TCT[28415] = "PCS_Pulkovo_Gauss_zone_15"
  TCT[28416] = "PCS_Pulkovo_Gauss_zone_16"
  TCT[28417] = "PCS_Pulkovo_Gauss_zone_17"
  TCT[28418] = "PCS_Pulkovo_Gauss_zone_18"
  TCT[28419] = "PCS_Pulkovo_Gauss_zone_19"
  TCT[28420] = "PCS_Pulkovo_Gauss_zone_20"
  TCT[28421] = "PCS_Pulkovo_Gauss_zone_21"
  TCT[28422] = "PCS_Pulkovo_Gauss_zone_22"
  TCT[28423] = "PCS_Pulkovo_Gauss_zone_23"
  TCT[28424] = "PCS_Pulkovo_Gauss_zone_24"
  TCT[28425] = "PCS_Pulkovo_Gauss_zone_25"
  TCT[28426] = "PCS_Pulkovo_Gauss_zone_26"
  TCT[28427] = "PCS_Pulkovo_Gauss_zone_27"
  TCT[28428] = "PCS_Pulkovo_Gauss_zone_28"
  TCT[28429] = "PCS_Pulkovo_Gauss_zone_29"
  TCT[28430] = "PCS_Pulkovo_Gauss_zone_30"
  TCT[28431] = "PCS_Pulkovo_Gauss_zone_31"
  TCT[28432] = "PCS_Pulkovo_Gauss_zone_32"
  TCT[28464] = "PCS_Pulkovo_Gauss_4N"
  TCT[28465] = "PCS_Pulkovo_Gauss_5N"
  TCT[28466] = "PCS_Pulkovo_Gauss_6N"
  TCT[28467] = "PCS_Pulkovo_Gauss_7N"
  TCT[28468] = "PCS_Pulkovo_Gauss_8N"
  TCT[28469] = "PCS_Pulkovo_Gauss_9N"
  TCT[28470] = "PCS_Pulkovo_Gauss_10N"
  TCT[28471] = "PCS_Pulkovo_Gauss_11N"
  TCT[28472] = "PCS_Pulkovo_Gauss_12N"
  TCT[28473] = "PCS_Pulkovo_Gauss_13N"
  TCT[28474] = "PCS_Pulkovo_Gauss_14N"
  TCT[28475] = "PCS_Pulkovo_Gauss_15N"
  TCT[28476] = "PCS_Pulkovo_Gauss_16N"
  TCT[28477] = "PCS_Pulkovo_Gauss_17N"
  TCT[28478] = "PCS_Pulkovo_Gauss_18N"
  TCT[28479] = "PCS_Pulkovo_Gauss_19N"
  TCT[28480] = "PCS_Pulkovo_Gauss_20N"
  TCT[28481] = "PCS_Pulkovo_Gauss_21N"
  TCT[28482] = "PCS_Pulkovo_Gauss_22N"
  TCT[28483] = "PCS_Pulkovo_Gauss_23N"
  TCT[28484] = "PCS_Pulkovo_Gauss_24N"
  TCT[28485] = "PCS_Pulkovo_Gauss_25N"
  TCT[28486] = "PCS_Pulkovo_Gauss_26N"
  TCT[28487] = "PCS_Pulkovo_Gauss_27N"
  TCT[28488] = "PCS_Pulkovo_Gauss_28N"
  TCT[28489] = "PCS_Pulkovo_Gauss_29N"
  TCT[28490] = "PCS_Pulkovo_Gauss_30N"
  TCT[28491] = "PCS_Pulkovo_Gauss_31N"
  TCT[28492] = "PCS_Pulkovo_Gauss_32N"
  TCT[28600] = "PCS_Qatar_National_Grid"
  TCT[28991] = "PCS_RD_Netherlands_Old"
  TCT[28992] = "PCS_RD_Netherlands_New"
  TCT[29118] = "PCS_SAD69_UTM_zone_18N"
  TCT[29119] = "PCS_SAD69_UTM_zone_19N"
  TCT[29120] = "PCS_SAD69_UTM_zone_20N"
  TCT[29121] = "PCS_SAD69_UTM_zone_21N"
  TCT[29122] = "PCS_SAD69_UTM_zone_22N"
  TCT[29177] = "PCS_SAD69_UTM_zone_17S"
  TCT[29178] = "PCS_SAD69_UTM_zone_18S"
  TCT[29179] = "PCS_SAD69_UTM_zone_19S"
  TCT[29180] = "PCS_SAD69_UTM_zone_20S"
  TCT[29181] = "PCS_SAD69_UTM_zone_21S"
  TCT[29182] = "PCS_SAD69_UTM_zone_22S"
  TCT[29183] = "PCS_SAD69_UTM_zone_23S"
  TCT[29184] = "PCS_SAD69_UTM_zone_24S"
  TCT[29185] = "PCS_SAD69_UTM_zone_25S"
  TCT[29220] = "PCS_Sapper_Hill_UTM_20S"
  TCT[29221] = "PCS_Sapper_Hill_UTM_21S"
  TCT[29333] = "PCS_Schwarzeck_UTM_33S"
  TCT[29635] = "PCS_Sudan_UTM_zone_35N"
  TCT[29636] = "PCS_Sudan_UTM_zone_36N"
  TCT[29700] = "PCS_Tananarive_Laborde"
  TCT[29738] = "PCS_Tananarive_UTM_38S"
  TCT[29739] = "PCS_Tananarive_UTM_39S"
  TCT[29800] = "PCS_Timbalai_1948_Borneo"
  TCT[29849] = "PCS_Timbalai_1948_UTM_49N"
  TCT[29850] = "PCS_Timbalai_1948_UTM_50N"
  TCT[29900] = "PCS_TM65_Irish_Nat_Grid"
  TCT[30200] = "PCS_Trinidad_1903_Trinidad"
  TCT[30339] = "PCS_TC_1948_UTM_zone_39N"
  TCT[30340] = "PCS_TC_1948_UTM_zone_40N"
  TCT[30491] = "PCS_Voirol_N_Algerie_ancien"
  TCT[30492] = "PCS_Voirol_S_Algerie_ancien"
  TCT[30591] = "PCS_Voirol_Unifie_N_Algerie"
  TCT[30592] = "PCS_Voirol_Unifie_S_Algerie"
  TCT[30600] = "PCS_Bern_1938_Swiss_New"
  TCT[30729] = "PCS_Nord_Sahara_UTM_29N"
  TCT[30730] = "PCS_Nord_Sahara_UTM_30N"
  TCT[30731] = "PCS_Nord_Sahara_UTM_31N"
  TCT[30732] = "PCS_Nord_Sahara_UTM_32N"
  TCT[31028] = "PCS_Yoff_UTM_zone_28N"
  TCT[31121] = "PCS_Zanderij_UTM_zone_21N"
  TCT[31291] = "PCS_MGI_Austria_West"
  TCT[31292] = "PCS_MGI_Austria_Central"
  TCT[31293] = "PCS_MGI_Austria_East"
  TCT[31300] = "PCS_Belge_Lambert_72"
  TCT[31491] = "PCS_DHDN_Germany_zone_1"
  TCT[31492] = "PCS_DHDN_Germany_zone_2"
  TCT[31493] = "PCS_DHDN_Germany_zone_3"
  TCT[31494] = "PCS_DHDN_Germany_zone_4"
  TCT[31495] = "PCS_DHDN_Germany_zone_5"
  TCT[32001] = "PCS_NAD27_Montana_North"
  TCT[32002] = "PCS_NAD27_Montana_Central"
  TCT[32003] = "PCS_NAD27_Montana_South"
  TCT[32005] = "PCS_NAD27_Nebraska_North"
  TCT[32006] = "PCS_NAD27_Nebraska_South"
  TCT[32007] = "PCS_NAD27_Nevada_East"
  TCT[32008] = "PCS_NAD27_Nevada_Central"
  TCT[32009] = "PCS_NAD27_Nevada_West"
  TCT[32010] = "PCS_NAD27_New_Hampshire"
  TCT[32011] = "PCS_NAD27_New_Jersey"
  TCT[32012] = "PCS_NAD27_New_Mexico_East"
  TCT[32013] = "PCS_NAD27_New_Mexico_Cent"
  TCT[32014] = "PCS_NAD27_New_Mexico_West"
  TCT[32015] = "PCS_NAD27_New_York_East"
  TCT[32016] = "PCS_NAD27_New_York_Central"
  TCT[32017] = "PCS_NAD27_New_York_West"
  TCT[32018] = "PCS_NAD27_New_York_Long_Is"
  TCT[32019] = "PCS_NAD27_North_Carolina"
  TCT[32020] = "PCS_NAD27_North_Dakota_N"
  TCT[32021] = "PCS_NAD27_North_Dakota_S"
  TCT[32022] = "PCS_NAD27_Ohio_North"
  TCT[32023] = "PCS_NAD27_Ohio_South"
  TCT[32024] = "PCS_NAD27_Oklahoma_North"
  TCT[32025] = "PCS_NAD27_Oklahoma_South"
  TCT[32026] = "PCS_NAD27_Oregon_North"
  TCT[32027] = "PCS_NAD27_Oregon_South"
  TCT[32028] = "PCS_NAD27_Pennsylvania_N"
  TCT[32029] = "PCS_NAD27_Pennsylvania_S"
  TCT[32030] = "PCS_NAD27_Rhode_Island"
  TCT[32031] = "PCS_NAD27_South_Carolina_N"
  TCT[32033] = "PCS_NAD27_South_Carolina_S"
  TCT[32034] = "PCS_NAD27_South_Dakota_N"
  TCT[32035] = "PCS_NAD27_South_Dakota_S"
  TCT[32036] = "PCS_NAD27_Tennessee"
  TCT[32037] = "PCS_NAD27_Texas_North"
  TCT[32038] = "PCS_NAD27_Texas_North_Cen"
  TCT[32039] = "PCS_NAD27_Texas_Central"
  TCT[32040] = "PCS_NAD27_Texas_South_Cen"
  TCT[32041] = "PCS_NAD27_Texas_South"
  TCT[32042] = "PCS_NAD27_Utah_North"
  TCT[32043] = "PCS_NAD27_Utah_Central"
  TCT[32044] = "PCS_NAD27_Utah_South"
  TCT[32045] = "PCS_NAD27_Vermont"
  TCT[32046] = "PCS_NAD27_Virginia_North"
  TCT[32047] = "PCS_NAD27_Virginia_South"
  TCT[32048] = "PCS_NAD27_Washington_North"
  TCT[32049] = "PCS_NAD27_Washington_South"
  TCT[32050] = "PCS_NAD27_West_Virginia_N"
  TCT[32051] = "PCS_NAD27_West_Virginia_S"
  TCT[32052] = "PCS_NAD27_Wisconsin_North"
  TCT[32053] = "PCS_NAD27_Wisconsin_Cen"
  TCT[32054] = "PCS_NAD27_Wisconsin_South"
  TCT[32055] = "PCS_NAD27_Wyoming_East"
  TCT[32056] = "PCS_NAD27_Wyoming_E_Cen"
  TCT[32057] = "PCS_NAD27_Wyoming_W_Cen"
  TCT[32058] = "PCS_NAD27_Wyoming_West"
  TCT[32059] = "PCS_NAD27_Puerto_Rico"
  TCT[32060] = "PCS_NAD27_St_Croix"
  TCT[32100] = "PCS_NAD83_Montana"
  TCT[32104] = "PCS_NAD83_Nebraska"
  TCT[32107] = "PCS_NAD83_Nevada_East"
  TCT[32108] = "PCS_NAD83_Nevada_Central"
  TCT[32109] = "PCS_NAD83_Nevada_West"
  TCT[32110] = "PCS_NAD83_New_Hampshire"
  TCT[32111] = "PCS_NAD83_New_Jersey"
  TCT[32112] = "PCS_NAD83_New_Mexico_East"
  TCT[32113] = "PCS_NAD83_New_Mexico_Cent"
  TCT[32114] = "PCS_NAD83_New_Mexico_West"
  TCT[32115] = "PCS_NAD83_New_York_East"
  TCT[32116] = "PCS_NAD83_New_York_Central"
  TCT[32117] = "PCS_NAD83_New_York_West"
  TCT[32118] = "PCS_NAD83_New_York_Long_Is"
  TCT[32119] = "PCS_NAD83_North_Carolina"
  TCT[32120] = "PCS_NAD83_North_Dakota_N"
  TCT[32121] = "PCS_NAD83_North_Dakota_S"
  TCT[32122] = "PCS_NAD83_Ohio_North"
  TCT[32123] = "PCS_NAD83_Ohio_South"
  TCT[32124] = "PCS_NAD83_Oklahoma_North"
  TCT[32125] = "PCS_NAD83_Oklahoma_South"
  TCT[32126] = "PCS_NAD83_Oregon_North"
  TCT[32127] = "PCS_NAD83_Oregon_South"
  TCT[32128] = "PCS_NAD83_Pennsylvania_N"
  TCT[32129] = "PCS_NAD83_Pennsylvania_S"
  TCT[32130] = "PCS_NAD83_Rhode_Island"
  TCT[32133] = "PCS_NAD83_South_Carolina"
  TCT[32134] = "PCS_NAD83_South_Dakota_N"
  TCT[32135] = "PCS_NAD83_South_Dakota_S"
  TCT[32136] = "PCS_NAD83_Tennessee"
  TCT[32137] = "PCS_NAD83_Texas_North"
  TCT[32138] = "PCS_NAD83_Texas_North_Cen"
  TCT[32139] = "PCS_NAD83_Texas_Central"
  TCT[32140] = "PCS_NAD83_Texas_South_Cen"
  TCT[32141] = "PCS_NAD83_Texas_South"
  TCT[32142] = "PCS_NAD83_Utah_North"
  TCT[32143] = "PCS_NAD83_Utah_Central"
  TCT[32144] = "PCS_NAD83_Utah_South"
  TCT[32145] = "PCS_NAD83_Vermont"
  TCT[32146] = "PCS_NAD83_Virginia_North"
  TCT[32147] = "PCS_NAD83_Virginia_South"
  TCT[32148] = "PCS_NAD83_Washington_North"
  TCT[32149] = "PCS_NAD83_Washington_South"
  TCT[32150] = "PCS_NAD83_West_Virginia_N"
  TCT[32151] = "PCS_NAD83_West_Virginia_S"
  TCT[32152] = "PCS_NAD83_Wisconsin_North"
  TCT[32153] = "PCS_NAD83_Wisconsin_Cen"
  TCT[32154] = "PCS_NAD83_Wisconsin_South"
  TCT[32155] = "PCS_NAD83_Wyoming_East"
  TCT[32156] = "PCS_NAD83_Wyoming_E_Cen"
  TCT[32157] = "PCS_NAD83_Wyoming_W_Cen"
  TCT[32158] = "PCS_NAD83_Wyoming_West"
  TCT[32161] = "PCS_NAD83_Puerto_Rico_Virgin_Is"
  TCT[32201] = "PCS_WGS72_UTM_zone_1N"
  TCT[32202] = "PCS_WGS72_UTM_zone_2N"
  TCT[32203] = "PCS_WGS72_UTM_zone_3N"
  TCT[32204] = "PCS_WGS72_UTM_zone_4N"
  TCT[32205] = "PCS_WGS72_UTM_zone_5N"
  TCT[32206] = "PCS_WGS72_UTM_zone_6N"
  TCT[32207] = "PCS_WGS72_UTM_zone_7N"
  TCT[32208] = "PCS_WGS72_UTM_zone_8N"
  TCT[32209] = "PCS_WGS72_UTM_zone_9N"
  TCT[32210] = "PCS_WGS72_UTM_zone_10N"
  TCT[32211] = "PCS_WGS72_UTM_zone_11N"
  TCT[32212] = "PCS_WGS72_UTM_zone_12N"
  TCT[32213] = "PCS_WGS72_UTM_zone_13N"
  TCT[32214] = "PCS_WGS72_UTM_zone_14N"
  TCT[32215] = "PCS_WGS72_UTM_zone_15N"
  TCT[32216] = "PCS_WGS72_UTM_zone_16N"
  TCT[32217] = "PCS_WGS72_UTM_zone_17N"
  TCT[32218] = "PCS_WGS72_UTM_zone_18N"
  TCT[32219] = "PCS_WGS72_UTM_zone_19N"
  TCT[32220] = "PCS_WGS72_UTM_zone_20N"
  TCT[32221] = "PCS_WGS72_UTM_zone_21N"
  TCT[32222] = "PCS_WGS72_UTM_zone_22N"
  TCT[32223] = "PCS_WGS72_UTM_zone_23N"
  TCT[32224] = "PCS_WGS72_UTM_zone_24N"
  TCT[32225] = "PCS_WGS72_UTM_zone_25N"
  TCT[32226] = "PCS_WGS72_UTM_zone_26N"
  TCT[32227] = "PCS_WGS72_UTM_zone_27N"
  TCT[32228] = "PCS_WGS72_UTM_zone_28N"
  TCT[32229] = "PCS_WGS72_UTM_zone_29N"
  TCT[32230] = "PCS_WGS72_UTM_zone_30N"
  TCT[32231] = "PCS_WGS72_UTM_zone_31N"
  TCT[32232] = "PCS_WGS72_UTM_zone_32N"
  TCT[32233] = "PCS_WGS72_UTM_zone_33N"
  TCT[32234] = "PCS_WGS72_UTM_zone_34N"
  TCT[32235] = "PCS_WGS72_UTM_zone_35N"
  TCT[32236] = "PCS_WGS72_UTM_zone_36N"
  TCT[32237] = "PCS_WGS72_UTM_zone_37N"
  TCT[32238] = "PCS_WGS72_UTM_zone_38N"
  TCT[32239] = "PCS_WGS72_UTM_zone_39N"
  TCT[32240] = "PCS_WGS72_UTM_zone_40N"
  TCT[32241] = "PCS_WGS72_UTM_zone_41N"
  TCT[32242] = "PCS_WGS72_UTM_zone_42N"
  TCT[32243] = "PCS_WGS72_UTM_zone_43N"
  TCT[32244] = "PCS_WGS72_UTM_zone_44N"
  TCT[32245] = "PCS_WGS72_UTM_zone_45N"
  TCT[32246] = "PCS_WGS72_UTM_zone_46N"
  TCT[32247] = "PCS_WGS72_UTM_zone_47N"
  TCT[32248] = "PCS_WGS72_UTM_zone_48N"
  TCT[32249] = "PCS_WGS72_UTM_zone_49N"
  TCT[32250] = "PCS_WGS72_UTM_zone_50N"
  TCT[32251] = "PCS_WGS72_UTM_zone_51N"
  TCT[32252] = "PCS_WGS72_UTM_zone_52N"
  TCT[32253] = "PCS_WGS72_UTM_zone_53N"
  TCT[32254] = "PCS_WGS72_UTM_zone_54N"
  TCT[32255] = "PCS_WGS72_UTM_zone_55N"
  TCT[32256] = "PCS_WGS72_UTM_zone_56N"
  TCT[32257] = "PCS_WGS72_UTM_zone_57N"
  TCT[32258] = "PCS_WGS72_UTM_zone_58N"
  TCT[32259] = "PCS_WGS72_UTM_zone_59N"
  TCT[32260] = "PCS_WGS72_UTM_zone_60N"
  TCT[32301] = "PCS_WGS72_UTM_zone_1S"
  TCT[32302] = "PCS_WGS72_UTM_zone_2S"
  TCT[32303] = "PCS_WGS72_UTM_zone_3S"
  TCT[32304] = "PCS_WGS72_UTM_zone_4S"
  TCT[32305] = "PCS_WGS72_UTM_zone_5S"
  TCT[32306] = "PCS_WGS72_UTM_zone_6S"
  TCT[32307] = "PCS_WGS72_UTM_zone_7S"
  TCT[32308] = "PCS_WGS72_UTM_zone_8S"
  TCT[32309] = "PCS_WGS72_UTM_zone_9S"
  TCT[32310] = "PCS_WGS72_UTM_zone_10S"
  TCT[32311] = "PCS_WGS72_UTM_zone_11S"
  TCT[32312] = "PCS_WGS72_UTM_zone_12S"
  TCT[32313] = "PCS_WGS72_UTM_zone_13S"
  TCT[32314] = "PCS_WGS72_UTM_zone_14S"
  TCT[32315] = "PCS_WGS72_UTM_zone_15S"
  TCT[32316] = "PCS_WGS72_UTM_zone_16S"
  TCT[32317] = "PCS_WGS72_UTM_zone_17S"
  TCT[32318] = "PCS_WGS72_UTM_zone_18S"
  TCT[32319] = "PCS_WGS72_UTM_zone_19S"
  TCT[32320] = "PCS_WGS72_UTM_zone_20S"
  TCT[32321] = "PCS_WGS72_UTM_zone_21S"
  TCT[32322] = "PCS_WGS72_UTM_zone_22S"
  TCT[32323] = "PCS_WGS72_UTM_zone_23S"
  TCT[32324] = "PCS_WGS72_UTM_zone_24S"
  TCT[32325] = "PCS_WGS72_UTM_zone_25S"
  TCT[32326] = "PCS_WGS72_UTM_zone_26S"
  TCT[32327] = "PCS_WGS72_UTM_zone_27S"
  TCT[32328] = "PCS_WGS72_UTM_zone_28S"
  TCT[32329] = "PCS_WGS72_UTM_zone_29S"
  TCT[32330] = "PCS_WGS72_UTM_zone_30S"
  TCT[32331] = "PCS_WGS72_UTM_zone_31S"
  TCT[32332] = "PCS_WGS72_UTM_zone_32S"
  TCT[32333] = "PCS_WGS72_UTM_zone_33S"
  TCT[32334] = "PCS_WGS72_UTM_zone_34S"
  TCT[32335] = "PCS_WGS72_UTM_zone_35S"
  TCT[32336] = "PCS_WGS72_UTM_zone_36S"
  TCT[32337] = "PCS_WGS72_UTM_zone_37S"
  TCT[32338] = "PCS_WGS72_UTM_zone_38S"
  TCT[32339] = "PCS_WGS72_UTM_zone_39S"
  TCT[32340] = "PCS_WGS72_UTM_zone_40S"
  TCT[32341] = "PCS_WGS72_UTM_zone_41S"
  TCT[32342] = "PCS_WGS72_UTM_zone_42S"
  TCT[32343] = "PCS_WGS72_UTM_zone_43S"
  TCT[32344] = "PCS_WGS72_UTM_zone_44S"
  TCT[32345] = "PCS_WGS72_UTM_zone_45S"
  TCT[32346] = "PCS_WGS72_UTM_zone_46S"
  TCT[32347] = "PCS_WGS72_UTM_zone_47S"
  TCT[32348] = "PCS_WGS72_UTM_zone_48S"
  TCT[32349] = "PCS_WGS72_UTM_zone_49S"
  TCT[32350] = "PCS_WGS72_UTM_zone_50S"
  TCT[32351] = "PCS_WGS72_UTM_zone_51S"
  TCT[32352] = "PCS_WGS72_UTM_zone_52S"
  TCT[32353] = "PCS_WGS72_UTM_zone_53S"
  TCT[32354] = "PCS_WGS72_UTM_zone_54S"
  TCT[32355] = "PCS_WGS72_UTM_zone_55S"
  TCT[32356] = "PCS_WGS72_UTM_zone_56S"
  TCT[32357] = "PCS_WGS72_UTM_zone_57S"
  TCT[32358] = "PCS_WGS72_UTM_zone_58S"
  TCT[32359] = "PCS_WGS72_UTM_zone_59S"
  TCT[32360] = "PCS_WGS72_UTM_zone_60S"
  TCT[32401] = "PCS_WGS72BE_UTM_zone_1N"
  TCT[32402] = "PCS_WGS72BE_UTM_zone_2N"
  TCT[32403] = "PCS_WGS72BE_UTM_zone_3N"
  TCT[32404] = "PCS_WGS72BE_UTM_zone_4N"
  TCT[32405] = "PCS_WGS72BE_UTM_zone_5N"
  TCT[32406] = "PCS_WGS72BE_UTM_zone_6N"
  TCT[32407] = "PCS_WGS72BE_UTM_zone_7N"
  TCT[32408] = "PCS_WGS72BE_UTM_zone_8N"
  TCT[32409] = "PCS_WGS72BE_UTM_zone_9N"
  TCT[32410] = "PCS_WGS72BE_UTM_zone_10N"
  TCT[32411] = "PCS_WGS72BE_UTM_zone_11N"
  TCT[32412] = "PCS_WGS72BE_UTM_zone_12N"
  TCT[32413] = "PCS_WGS72BE_UTM_zone_13N"
  TCT[32414] = "PCS_WGS72BE_UTM_zone_14N"
  TCT[32415] = "PCS_WGS72BE_UTM_zone_15N"
  TCT[32416] = "PCS_WGS72BE_UTM_zone_16N"
  TCT[32417] = "PCS_WGS72BE_UTM_zone_17N"
  TCT[32418] = "PCS_WGS72BE_UTM_zone_18N"
  TCT[32419] = "PCS_WGS72BE_UTM_zone_19N"
  TCT[32420] = "PCS_WGS72BE_UTM_zone_20N"
  TCT[32421] = "PCS_WGS72BE_UTM_zone_21N"
  TCT[32422] = "PCS_WGS72BE_UTM_zone_22N"
  TCT[32423] = "PCS_WGS72BE_UTM_zone_23N"
  TCT[32424] = "PCS_WGS72BE_UTM_zone_24N"
  TCT[32425] = "PCS_WGS72BE_UTM_zone_25N"
  TCT[32426] = "PCS_WGS72BE_UTM_zone_26N"
  TCT[32427] = "PCS_WGS72BE_UTM_zone_27N"
  TCT[32428] = "PCS_WGS72BE_UTM_zone_28N"
  TCT[32429] = "PCS_WGS72BE_UTM_zone_29N"
  TCT[32430] = "PCS_WGS72BE_UTM_zone_30N"
  TCT[32431] = "PCS_WGS72BE_UTM_zone_31N"
  TCT[32432] = "PCS_WGS72BE_UTM_zone_32N"
  TCT[32433] = "PCS_WGS72BE_UTM_zone_33N"
  TCT[32434] = "PCS_WGS72BE_UTM_zone_34N"
  TCT[32435] = "PCS_WGS72BE_UTM_zone_35N"
  TCT[32436] = "PCS_WGS72BE_UTM_zone_36N"
  TCT[32437] = "PCS_WGS72BE_UTM_zone_37N"
  TCT[32438] = "PCS_WGS72BE_UTM_zone_38N"
  TCT[32439] = "PCS_WGS72BE_UTM_zone_39N"
  TCT[32440] = "PCS_WGS72BE_UTM_zone_40N"
  TCT[32441] = "PCS_WGS72BE_UTM_zone_41N"
  TCT[32442] = "PCS_WGS72BE_UTM_zone_42N"
  TCT[32443] = "PCS_WGS72BE_UTM_zone_43N"
  TCT[32444] = "PCS_WGS72BE_UTM_zone_44N"
  TCT[32445] = "PCS_WGS72BE_UTM_zone_45N"
  TCT[32446] = "PCS_WGS72BE_UTM_zone_46N"
  TCT[32447] = "PCS_WGS72BE_UTM_zone_47N"
  TCT[32448] = "PCS_WGS72BE_UTM_zone_48N"
  TCT[32449] = "PCS_WGS72BE_UTM_zone_49N"
  TCT[32450] = "PCS_WGS72BE_UTM_zone_50N"
  TCT[32451] = "PCS_WGS72BE_UTM_zone_51N"
  TCT[32452] = "PCS_WGS72BE_UTM_zone_52N"
  TCT[32453] = "PCS_WGS72BE_UTM_zone_53N"
  TCT[32454] = "PCS_WGS72BE_UTM_zone_54N"
  TCT[32455] = "PCS_WGS72BE_UTM_zone_55N"
  TCT[32456] = "PCS_WGS72BE_UTM_zone_56N"
  TCT[32457] = "PCS_WGS72BE_UTM_zone_57N"
  TCT[32458] = "PCS_WGS72BE_UTM_zone_58N"
  TCT[32459] = "PCS_WGS72BE_UTM_zone_59N"
  TCT[32460] = "PCS_WGS72BE_UTM_zone_60N"
  TCT[32501] = "PCS_WGS72BE_UTM_zone_1S"
  TCT[32502] = "PCS_WGS72BE_UTM_zone_2S"
  TCT[32503] = "PCS_WGS72BE_UTM_zone_3S"
  TCT[32504] = "PCS_WGS72BE_UTM_zone_4S"
  TCT[32505] = "PCS_WGS72BE_UTM_zone_5S"
  TCT[32506] = "PCS_WGS72BE_UTM_zone_6S"
  TCT[32507] = "PCS_WGS72BE_UTM_zone_7S"
  TCT[32508] = "PCS_WGS72BE_UTM_zone_8S"
  TCT[32509] = "PCS_WGS72BE_UTM_zone_9S"
  TCT[32510] = "PCS_WGS72BE_UTM_zone_10S"
  TCT[32511] = "PCS_WGS72BE_UTM_zone_11S"
  TCT[32512] = "PCS_WGS72BE_UTM_zone_12S"
  TCT[32513] = "PCS_WGS72BE_UTM_zone_13S"
  TCT[32514] = "PCS_WGS72BE_UTM_zone_14S"
  TCT[32515] = "PCS_WGS72BE_UTM_zone_15S"
  TCT[32516] = "PCS_WGS72BE_UTM_zone_16S"
  TCT[32517] = "PCS_WGS72BE_UTM_zone_17S"
  TCT[32518] = "PCS_WGS72BE_UTM_zone_18S"
  TCT[32519] = "PCS_WGS72BE_UTM_zone_19S"
  TCT[32520] = "PCS_WGS72BE_UTM_zone_20S"
  TCT[32521] = "PCS_WGS72BE_UTM_zone_21S"
  TCT[32522] = "PCS_WGS72BE_UTM_zone_22S"
  TCT[32523] = "PCS_WGS72BE_UTM_zone_23S"
  TCT[32524] = "PCS_WGS72BE_UTM_zone_24S"
  TCT[32525] = "PCS_WGS72BE_UTM_zone_25S"
  TCT[32526] = "PCS_WGS72BE_UTM_zone_26S"
  TCT[32527] = "PCS_WGS72BE_UTM_zone_27S"
  TCT[32528] = "PCS_WGS72BE_UTM_zone_28S"
  TCT[32529] = "PCS_WGS72BE_UTM_zone_29S"
  TCT[32530] = "PCS_WGS72BE_UTM_zone_30S"
  TCT[32531] = "PCS_WGS72BE_UTM_zone_31S"
  TCT[32532] = "PCS_WGS72BE_UTM_zone_32S"
  TCT[32533] = "PCS_WGS72BE_UTM_zone_33S"
  TCT[32534] = "PCS_WGS72BE_UTM_zone_34S"
  TCT[32535] = "PCS_WGS72BE_UTM_zone_35S"
  TCT[32536] = "PCS_WGS72BE_UTM_zone_36S"
  TCT[32537] = "PCS_WGS72BE_UTM_zone_37S"
  TCT[32538] = "PCS_WGS72BE_UTM_zone_38S"
  TCT[32539] = "PCS_WGS72BE_UTM_zone_39S"
  TCT[32540] = "PCS_WGS72BE_UTM_zone_40S"
  TCT[32541] = "PCS_WGS72BE_UTM_zone_41S"
  TCT[32542] = "PCS_WGS72BE_UTM_zone_42S"
  TCT[32543] = "PCS_WGS72BE_UTM_zone_43S"
  TCT[32544] = "PCS_WGS72BE_UTM_zone_44S"
  TCT[32545] = "PCS_WGS72BE_UTM_zone_45S"
  TCT[32546] = "PCS_WGS72BE_UTM_zone_46S"
  TCT[32547] = "PCS_WGS72BE_UTM_zone_47S"
  TCT[32548] = "PCS_WGS72BE_UTM_zone_48S"
  TCT[32549] = "PCS_WGS72BE_UTM_zone_49S"
  TCT[32550] = "PCS_WGS72BE_UTM_zone_50S"
  TCT[32551] = "PCS_WGS72BE_UTM_zone_51S"
  TCT[32552] = "PCS_WGS72BE_UTM_zone_52S"
  TCT[32553] = "PCS_WGS72BE_UTM_zone_53S"
  TCT[32554] = "PCS_WGS72BE_UTM_zone_54S"
  TCT[32555] = "PCS_WGS72BE_UTM_zone_55S"
  TCT[32556] = "PCS_WGS72BE_UTM_zone_56S"
  TCT[32557] = "PCS_WGS72BE_UTM_zone_57S"
  TCT[32558] = "PCS_WGS72BE_UTM_zone_58S"
  TCT[32559] = "PCS_WGS72BE_UTM_zone_59S"
  TCT[32560] = "PCS_WGS72BE_UTM_zone_60S"
  TCT[32601] = "PCS_WGS84_UTM_zone_1N"
  TCT[32602] = "PCS_WGS84_UTM_zone_2N"
  TCT[32603] = "PCS_WGS84_UTM_zone_3N"
  TCT[32604] = "PCS_WGS84_UTM_zone_4N"
  TCT[32605] = "PCS_WGS84_UTM_zone_5N"
  TCT[32606] = "PCS_WGS84_UTM_zone_6N"
  TCT[32607] = "PCS_WGS84_UTM_zone_7N"
  TCT[32608] = "PCS_WGS84_UTM_zone_8N"
  TCT[32609] = "PCS_WGS84_UTM_zone_9N"
  TCT[32610] = "PCS_WGS84_UTM_zone_10N"
  TCT[32611] = "PCS_WGS84_UTM_zone_11N"
  TCT[32612] = "PCS_WGS84_UTM_zone_12N"
  TCT[32613] = "PCS_WGS84_UTM_zone_13N"
  TCT[32614] = "PCS_WGS84_UTM_zone_14N"
  TCT[32615] = "PCS_WGS84_UTM_zone_15N"
  TCT[32616] = "PCS_WGS84_UTM_zone_16N"
  TCT[32617] = "PCS_WGS84_UTM_zone_17N"
  TCT[32618] = "PCS_WGS84_UTM_zone_18N"
  TCT[32619] = "PCS_WGS84_UTM_zone_19N"
  TCT[32620] = "PCS_WGS84_UTM_zone_20N"
  TCT[32621] = "PCS_WGS84_UTM_zone_21N"
  TCT[32622] = "PCS_WGS84_UTM_zone_22N"
  TCT[32623] = "PCS_WGS84_UTM_zone_23N"
  TCT[32624] = "PCS_WGS84_UTM_zone_24N"
  TCT[32625] = "PCS_WGS84_UTM_zone_25N"
  TCT[32626] = "PCS_WGS84_UTM_zone_26N"
  TCT[32627] = "PCS_WGS84_UTM_zone_27N"
  TCT[32628] = "PCS_WGS84_UTM_zone_28N"
  TCT[32629] = "PCS_WGS84_UTM_zone_29N"
  TCT[32630] = "PCS_WGS84_UTM_zone_30N"
  TCT[32631] = "PCS_WGS84_UTM_zone_31N"
  TCT[32632] = "PCS_WGS84_UTM_zone_32N"
  TCT[32633] = "PCS_WGS84_UTM_zone_33N"
  TCT[32634] = "PCS_WGS84_UTM_zone_34N"
  TCT[32635] = "PCS_WGS84_UTM_zone_35N"
  TCT[32636] = "PCS_WGS84_UTM_zone_36N"
  TCT[32637] = "PCS_WGS84_UTM_zone_37N"
  TCT[32638] = "PCS_WGS84_UTM_zone_38N"
  TCT[32639] = "PCS_WGS84_UTM_zone_39N"
  TCT[32640] = "PCS_WGS84_UTM_zone_40N"
  TCT[32641] = "PCS_WGS84_UTM_zone_41N"
  TCT[32642] = "PCS_WGS84_UTM_zone_42N"
  TCT[32643] = "PCS_WGS84_UTM_zone_43N"
  TCT[32644] = "PCS_WGS84_UTM_zone_44N"
  TCT[32645] = "PCS_WGS84_UTM_zone_45N"
  TCT[32646] = "PCS_WGS84_UTM_zone_46N"
  TCT[32647] = "PCS_WGS84_UTM_zone_47N"
  TCT[32648] = "PCS_WGS84_UTM_zone_48N"
  TCT[32649] = "PCS_WGS84_UTM_zone_49N"
  TCT[32650] = "PCS_WGS84_UTM_zone_50N"
  TCT[32651] = "PCS_WGS84_UTM_zone_51N"
  TCT[32652] = "PCS_WGS84_UTM_zone_52N"
  TCT[32653] = "PCS_WGS84_UTM_zone_53N"
  TCT[32654] = "PCS_WGS84_UTM_zone_54N"
  TCT[32655] = "PCS_WGS84_UTM_zone_55N"
  TCT[32656] = "PCS_WGS84_UTM_zone_56N"
  TCT[32657] = "PCS_WGS84_UTM_zone_57N"
  TCT[32658] = "PCS_WGS84_UTM_zone_58N"
  TCT[32659] = "PCS_WGS84_UTM_zone_59N"
  TCT[32660] = "PCS_WGS84_UTM_zone_60N"
  TCT[32701] = "PCS_WGS84_UTM_zone_1S"
  TCT[32702] = "PCS_WGS84_UTM_zone_2S"
  TCT[32703] = "PCS_WGS84_UTM_zone_3S"
  TCT[32704] = "PCS_WGS84_UTM_zone_4S"
  TCT[32705] = "PCS_WGS84_UTM_zone_5S"
  TCT[32706] = "PCS_WGS84_UTM_zone_6S"
  TCT[32707] = "PCS_WGS84_UTM_zone_7S"
  TCT[32708] = "PCS_WGS84_UTM_zone_8S"
  TCT[32709] = "PCS_WGS84_UTM_zone_9S"
  TCT[32710] = "PCS_WGS84_UTM_zone_10S"
  TCT[32711] = "PCS_WGS84_UTM_zone_11S"
  TCT[32712] = "PCS_WGS84_UTM_zone_12S"
  TCT[32713] = "PCS_WGS84_UTM_zone_13S"
  TCT[32714] = "PCS_WGS84_UTM_zone_14S"
  TCT[32715] = "PCS_WGS84_UTM_zone_15S"
  TCT[32716] = "PCS_WGS84_UTM_zone_16S"
  TCT[32717] = "PCS_WGS84_UTM_zone_17S"
  TCT[32718] = "PCS_WGS84_UTM_zone_18S"
  TCT[32719] = "PCS_WGS84_UTM_zone_19S"
  TCT[32720] = "PCS_WGS84_UTM_zone_20S"
  TCT[32721] = "PCS_WGS84_UTM_zone_21S"
  TCT[32722] = "PCS_WGS84_UTM_zone_22S"
  TCT[32723] = "PCS_WGS84_UTM_zone_23S"
  TCT[32724] = "PCS_WGS84_UTM_zone_24S"
  TCT[32725] = "PCS_WGS84_UTM_zone_25S"
  TCT[32726] = "PCS_WGS84_UTM_zone_26S"
  TCT[32727] = "PCS_WGS84_UTM_zone_27S"
  TCT[32728] = "PCS_WGS84_UTM_zone_28S"
  TCT[32729] = "PCS_WGS84_UTM_zone_29S"
  TCT[32730] = "PCS_WGS84_UTM_zone_30S"
  TCT[32731] = "PCS_WGS84_UTM_zone_31S"
  TCT[32732] = "PCS_WGS84_UTM_zone_32S"
  TCT[32733] = "PCS_WGS84_UTM_zone_33S"
  TCT[32734] = "PCS_WGS84_UTM_zone_34S"
  TCT[32735] = "PCS_WGS84_UTM_zone_35S"
  TCT[32736] = "PCS_WGS84_UTM_zone_36S"
  TCT[32737] = "PCS_WGS84_UTM_zone_37S"
  TCT[32738] = "PCS_WGS84_UTM_zone_38S"
  TCT[32739] = "PCS_WGS84_UTM_zone_39S"
  TCT[32740] = "PCS_WGS84_UTM_zone_40S"
  TCT[32741] = "PCS_WGS84_UTM_zone_41S"
  TCT[32742] = "PCS_WGS84_UTM_zone_42S"
  TCT[32743] = "PCS_WGS84_UTM_zone_43S"
  TCT[32744] = "PCS_WGS84_UTM_zone_44S"
  TCT[32745] = "PCS_WGS84_UTM_zone_45S"
  TCT[32746] = "PCS_WGS84_UTM_zone_46S"
  TCT[32747] = "PCS_WGS84_UTM_zone_47S"
  TCT[32748] = "PCS_WGS84_UTM_zone_48S"
  TCT[32749] = "PCS_WGS84_UTM_zone_49S"
  TCT[32750] = "PCS_WGS84_UTM_zone_50S"
  TCT[32751] = "PCS_WGS84_UTM_zone_51S"
  TCT[32752] = "PCS_WGS84_UTM_zone_52S"
  TCT[32753] = "PCS_WGS84_UTM_zone_53S"
  TCT[32754] = "PCS_WGS84_UTM_zone_54S"
  TCT[32755] = "PCS_WGS84_UTM_zone_55S"
  TCT[32756] = "PCS_WGS84_UTM_zone_56S"
  TCT[32757] = "PCS_WGS84_UTM_zone_57S"
  TCT[32758] = "PCS_WGS84_UTM_zone_58S"
  TCT[32759] = "PCS_WGS84_UTM_zone_59S"
  TCT[32760] = "PCS_WGS84_UTM_zone_60S"
  TCT[10101] = "Proj_Alabama_CS27_East"
  TCT[10102] = "Proj_Alabama_CS27_West"
  TCT[10131] = "Proj_Alabama_CS83_East"
  TCT[10132] = "Proj_Alabama_CS83_West"
  TCT[10201] = "Proj_Arizona_Coordinate_System_east"
  TCT[10202] = "Proj_Arizona_Coordinate_System_Central"
  TCT[10203] = "Proj_Arizona_Coordinate_System_west"
  TCT[10231] = "Proj_Arizona_CS83_east"
  TCT[10232] = "Proj_Arizona_CS83_Central"
  TCT[10233] = "Proj_Arizona_CS83_west"
  TCT[10301] = "Proj_Arkansas_CS27_North"
  TCT[10302] = "Proj_Arkansas_CS27_South"
  TCT[10331] = "Proj_Arkansas_CS83_North"
  TCT[10332] = "Proj_Arkansas_CS83_South"
  TCT[10401] = "Proj_California_CS27_I"
  TCT[10402] = "Proj_California_CS27_II"
  TCT[10403] = "Proj_California_CS27_III"
  TCT[10404] = "Proj_California_CS27_IV"
  TCT[10405] = "Proj_California_CS27_V"
  TCT[10406] = "Proj_California_CS27_VI"
  TCT[10407] = "Proj_California_CS27_VII"
  TCT[10431] = "Proj_California_CS83_1"
  TCT[10432] = "Proj_California_CS83_2"
  TCT[10433] = "Proj_California_CS83_3"
  TCT[10434] = "Proj_California_CS83_4"
  TCT[10435] = "Proj_California_CS83_5"
  TCT[10436] = "Proj_California_CS83_6"
  TCT[10501] = "Proj_Colorado_CS27_North"
  TCT[10502] = "Proj_Colorado_CS27_Central"
  TCT[10503] = "Proj_Colorado_CS27_South"
  TCT[10531] = "Proj_Colorado_CS83_North"
  TCT[10532] = "Proj_Colorado_CS83_Central"
  TCT[10533] = "Proj_Colorado_CS83_South"
  TCT[10600] = "Proj_Connecticut_CS27"
  TCT[10630] = "Proj_Connecticut_CS83"
  TCT[10700] = "Proj_Delaware_CS27"
  TCT[10730] = "Proj_Delaware_CS83"
  TCT[10901] = "Proj_Florida_CS27_East"
  TCT[10902] = "Proj_Florida_CS27_West"
  TCT[10903] = "Proj_Florida_CS27_North"
  TCT[10931] = "Proj_Florida_CS83_East"
  TCT[10932] = "Proj_Florida_CS83_West"
  TCT[10933] = "Proj_Florida_CS83_North"
  TCT[11001] = "Proj_Georgia_CS27_East"
  TCT[11002] = "Proj_Georgia_CS27_West"
  TCT[11031] = "Proj_Georgia_CS83_East"
  TCT[11032] = "Proj_Georgia_CS83_West"
  TCT[11101] = "Proj_Idaho_CS27_East"
  TCT[11102] = "Proj_Idaho_CS27_Central"
  TCT[11103] = "Proj_Idaho_CS27_West"
  TCT[11131] = "Proj_Idaho_CS83_East"
  TCT[11132] = "Proj_Idaho_CS83_Central"
  TCT[11133] = "Proj_Idaho_CS83_West"
  TCT[11201] = "Proj_Illinois_CS27_East"
  TCT[11202] = "Proj_Illinois_CS27_West"
  TCT[11231] = "Proj_Illinois_CS83_East"
  TCT[11232] = "Proj_Illinois_CS83_West"
  TCT[11301] = "Proj_Indiana_CS27_East"
  TCT[11302] = "Proj_Indiana_CS27_West"
  TCT[11331] = "Proj_Indiana_CS83_East"
  TCT[11332] = "Proj_Indiana_CS83_West"
  TCT[11401] = "Proj_Iowa_CS27_North"
  TCT[11402] = "Proj_Iowa_CS27_South"
  TCT[11431] = "Proj_Iowa_CS83_North"
  TCT[11432] = "Proj_Iowa_CS83_South"
  TCT[11501] = "Proj_Kansas_CS27_North"
  TCT[11502] = "Proj_Kansas_CS27_South"
  TCT[11531] = "Proj_Kansas_CS83_North"
  TCT[11532] = "Proj_Kansas_CS83_South"
  TCT[11601] = "Proj_Kentucky_CS27_North"
  TCT[11602] = "Proj_Kentucky_CS27_South"
  TCT[11631] = "Proj_Kentucky_CS83_North"
  TCT[11632] = "Proj_Kentucky_CS83_South"
  TCT[11701] = "Proj_Louisiana_CS27_North"
  TCT[11702] = "Proj_Louisiana_CS27_South"
  TCT[11731] = "Proj_Louisiana_CS83_North"
  TCT[11732] = "Proj_Louisiana_CS83_South"
  TCT[11801] = "Proj_Maine_CS27_East"
  TCT[11802] = "Proj_Maine_CS27_West"
  TCT[11831] = "Proj_Maine_CS83_East"
  TCT[11832] = "Proj_Maine_CS83_West"
  TCT[11900] = "Proj_Maryland_CS27"
  TCT[11930] = "Proj_Maryland_CS83"
  TCT[12001] = "Proj_Massachusetts_CS27_Mainland"
  TCT[12002] = "Proj_Massachusetts_CS27_Island"
  TCT[12031] = "Proj_Massachusetts_CS83_Mainland"
  TCT[12032] = "Proj_Massachusetts_CS83_Island"
  TCT[12101] = "Proj_Michigan_State_Plane_East"
  TCT[12102] = "Proj_Michigan_State_Plane_Old_Central"
  TCT[12103] = "Proj_Michigan_State_Plane_West"
  TCT[12111] = "Proj_Michigan_CS27_North"
  TCT[12112] = "Proj_Michigan_CS27_Central"
  TCT[12113] = "Proj_Michigan_CS27_South"
  TCT[12141] = "Proj_Michigan_CS83_North"
  TCT[12142] = "Proj_Michigan_CS83_Central"
  TCT[12143] = "Proj_Michigan_CS83_South"
  TCT[12201] = "Proj_Minnesota_CS27_North"
  TCT[12202] = "Proj_Minnesota_CS27_Central"
  TCT[12203] = "Proj_Minnesota_CS27_South"
  TCT[12231] = "Proj_Minnesota_CS83_North"
  TCT[12232] = "Proj_Minnesota_CS83_Central"
  TCT[12233] = "Proj_Minnesota_CS83_South"
  TCT[12301] = "Proj_Mississippi_CS27_East"
  TCT[12302] = "Proj_Mississippi_CS27_West"
  TCT[12331] = "Proj_Mississippi_CS83_East"
  TCT[12332] = "Proj_Mississippi_CS83_West"
  TCT[12401] = "Proj_Missouri_CS27_East"
  TCT[12402] = "Proj_Missouri_CS27_Central"
  TCT[12403] = "Proj_Missouri_CS27_West"
  TCT[12431] = "Proj_Missouri_CS83_East"
  TCT[12432] = "Proj_Missouri_CS83_Central"
  TCT[12433] = "Proj_Missouri_CS83_West"
  TCT[12501] = "Proj_Montana_CS27_North"
  TCT[12502] = "Proj_Montana_CS27_Central"
  TCT[12503] = "Proj_Montana_CS27_South"
  TCT[12530] = "Proj_Montana_CS83"
  TCT[12601] = "Proj_Nebraska_CS27_North"
  TCT[12602] = "Proj_Nebraska_CS27_South"
  TCT[12630] = "Proj_Nebraska_CS83"
  TCT[12701] = "Proj_Nevada_CS27_East"
  TCT[12702] = "Proj_Nevada_CS27_Central"
  TCT[12703] = "Proj_Nevada_CS27_West"
  TCT[12731] = "Proj_Nevada_CS83_East"
  TCT[12732] = "Proj_Nevada_CS83_Central"
  TCT[12733] = "Proj_Nevada_CS83_West"
  TCT[12800] = "Proj_New_Hampshire_CS27"
  TCT[12830] = "Proj_New_Hampshire_CS83"
  TCT[12900] = "Proj_New_Jersey_CS27"
  TCT[12930] = "Proj_New_Jersey_CS83"
  TCT[13001] = "Proj_New_Mexico_CS27_East"
  TCT[13002] = "Proj_New_Mexico_CS27_Central"
  TCT[13003] = "Proj_New_Mexico_CS27_West"
  TCT[13031] = "Proj_New_Mexico_CS83_East"
  TCT[13032] = "Proj_New_Mexico_CS83_Central"
  TCT[13033] = "Proj_New_Mexico_CS83_West"
  TCT[13101] = "Proj_New_York_CS27_East"
  TCT[13102] = "Proj_New_York_CS27_Central"
  TCT[13103] = "Proj_New_York_CS27_West"
  TCT[13104] = "Proj_New_York_CS27_Long_Island"
  TCT[13131] = "Proj_New_York_CS83_East"
  TCT[13132] = "Proj_New_York_CS83_Central"
  TCT[13133] = "Proj_New_York_CS83_West"
  TCT[13134] = "Proj_New_York_CS83_Long_Island"
  TCT[13200] = "Proj_North_Carolina_CS27"
  TCT[13230] = "Proj_North_Carolina_CS83"
  TCT[13301] = "Proj_North_Dakota_CS27_North"
  TCT[13302] = "Proj_North_Dakota_CS27_South"
  TCT[13331] = "Proj_North_Dakota_CS83_North"
  TCT[13332] = "Proj_North_Dakota_CS83_South"
  TCT[13401] = "Proj_Ohio_CS27_North"
  TCT[13402] = "Proj_Ohio_CS27_South"
  TCT[13431] = "Proj_Ohio_CS83_North"
  TCT[13432] = "Proj_Ohio_CS83_South"
  TCT[13501] = "Proj_Oklahoma_CS27_North"
  TCT[13502] = "Proj_Oklahoma_CS27_South"
  TCT[13531] = "Proj_Oklahoma_CS83_North"
  TCT[13532] = "Proj_Oklahoma_CS83_South"
  TCT[13601] = "Proj_Oregon_CS27_North"
  TCT[13602] = "Proj_Oregon_CS27_South"
  TCT[13631] = "Proj_Oregon_CS83_North"
  TCT[13632] = "Proj_Oregon_CS83_South"
  TCT[13701] = "Proj_Pennsylvania_CS27_North"
  TCT[13702] = "Proj_Pennsylvania_CS27_South"
  TCT[13731] = "Proj_Pennsylvania_CS83_North"
  TCT[13732] = "Proj_Pennsylvania_CS83_South"
  TCT[13800] = "Proj_Rhode_Island_CS27"
  TCT[13830] = "Proj_Rhode_Island_CS83"
  TCT[13901] = "Proj_South_Carolina_CS27_North"
  TCT[13902] = "Proj_South_Carolina_CS27_South"
  TCT[13930] = "Proj_South_Carolina_CS83"
  TCT[14001] = "Proj_South_Dakota_CS27_North"
  TCT[14002] = "Proj_South_Dakota_CS27_South"
  TCT[14031] = "Proj_South_Dakota_CS83_North"
  TCT[14032] = "Proj_South_Dakota_CS83_South"
  TCT[14100] = "Proj_Tennessee_CS27"
  TCT[14130] = "Proj_Tennessee_CS83"
  TCT[14201] = "Proj_Texas_CS27_North"
  TCT[14202] = "Proj_Texas_CS27_North_Central"
  TCT[14203] = "Proj_Texas_CS27_Central"
  TCT[14204] = "Proj_Texas_CS27_South_Central"
  TCT[14205] = "Proj_Texas_CS27_South"
  TCT[14231] = "Proj_Texas_CS83_North"
  TCT[14232] = "Proj_Texas_CS83_North_Central"
  TCT[14233] = "Proj_Texas_CS83_Central"
  TCT[14234] = "Proj_Texas_CS83_South_Central"
  TCT[14235] = "Proj_Texas_CS83_South"
  TCT[14301] = "Proj_Utah_CS27_North"
  TCT[14302] = "Proj_Utah_CS27_Central"
  TCT[14303] = "Proj_Utah_CS27_South"
  TCT[14331] = "Proj_Utah_CS83_North"
  TCT[14332] = "Proj_Utah_CS83_Central"
  TCT[14333] = "Proj_Utah_CS83_South"
  TCT[14400] = "Proj_Vermont_CS27"
  TCT[14430] = "Proj_Vermont_CS83"
  TCT[14501] = "Proj_Virginia_CS27_North"
  TCT[14502] = "Proj_Virginia_CS27_South"
  TCT[14531] = "Proj_Virginia_CS83_North"
  TCT[14532] = "Proj_Virginia_CS83_South"
  TCT[14601] = "Proj_Washington_CS27_North"
  TCT[14602] = "Proj_Washington_CS27_South"
  TCT[14631] = "Proj_Washington_CS83_North"
  TCT[14632] = "Proj_Washington_CS83_South"
  TCT[14701] = "Proj_West_Virginia_CS27_North"
  TCT[14702] = "Proj_West_Virginia_CS27_South"
  TCT[14731] = "Proj_West_Virginia_CS83_North"
  TCT[14732] = "Proj_West_Virginia_CS83_South"
  TCT[14801] = "Proj_Wisconsin_CS27_North"
  TCT[14802] = "Proj_Wisconsin_CS27_Central"
  TCT[14803] = "Proj_Wisconsin_CS27_South"
  TCT[14831] = "Proj_Wisconsin_CS83_North"
  TCT[14832] = "Proj_Wisconsin_CS83_Central"
  TCT[14833] = "Proj_Wisconsin_CS83_South"
  TCT[14901] = "Proj_Wyoming_CS27_East"
  TCT[14902] = "Proj_Wyoming_CS27_East_Central"
  TCT[14903] = "Proj_Wyoming_CS27_West_Central"
  TCT[14904] = "Proj_Wyoming_CS27_West"
  TCT[14931] = "Proj_Wyoming_CS83_East"
  TCT[14932] = "Proj_Wyoming_CS83_East_Central"
  TCT[14933] = "Proj_Wyoming_CS83_West_Central"
  TCT[14934] = "Proj_Wyoming_CS83_West"
  TCT[15001] = "Proj_Alaska_CS27_1"
  TCT[15002] = "Proj_Alaska_CS27_2"
  TCT[15003] = "Proj_Alaska_CS27_3"
  TCT[15004] = "Proj_Alaska_CS27_4"
  TCT[15005] = "Proj_Alaska_CS27_5"
  TCT[15006] = "Proj_Alaska_CS27_6"
  TCT[15007] = "Proj_Alaska_CS27_7"
  TCT[15008] = "Proj_Alaska_CS27_8"
  TCT[15009] = "Proj_Alaska_CS27_9"
  TCT[15010] = "Proj_Alaska_CS27_10"
  TCT[15031] = "Proj_Alaska_CS83_1"
  TCT[15032] = "Proj_Alaska_CS83_2"
  TCT[15033] = "Proj_Alaska_CS83_3"
  TCT[15034] = "Proj_Alaska_CS83_4"
  TCT[15035] = "Proj_Alaska_CS83_5"
  TCT[15036] = "Proj_Alaska_CS83_6"
  TCT[15037] = "Proj_Alaska_CS83_7"
  TCT[15038] = "Proj_Alaska_CS83_8"
  TCT[15039] = "Proj_Alaska_CS83_9"
  TCT[15040] = "Proj_Alaska_CS83_10"
  TCT[15101] = "Proj_Hawaii_CS27_1"
  TCT[15102] = "Proj_Hawaii_CS27_2"
  TCT[15103] = "Proj_Hawaii_CS27_3"
  TCT[15104] = "Proj_Hawaii_CS27_4"
  TCT[15105] = "Proj_Hawaii_CS27_5"
  TCT[15131] = "Proj_Hawaii_CS83_1"
  TCT[15132] = "Proj_Hawaii_CS83_2"
  TCT[15133] = "Proj_Hawaii_CS83_3"
  TCT[15134] = "Proj_Hawaii_CS83_4"
  TCT[15135] = "Proj_Hawaii_CS83_5"
  TCT[15201] = "Proj_Puerto_Rico_CS27"
  TCT[15202] = "Proj_St_Croix"
  TCT[15230] = "Proj_Puerto_Rico_Virgin_Is"
  TCT[15914] = "Proj_BLM_14N_feet"
  TCT[15915] = "Proj_BLM_15N_feet"
  TCT[15916] = "Proj_BLM_16N_feet"
  TCT[15917] = "Proj_BLM_17N_feet"
  TCT[17348] = "Proj_Map_Grid_of_Australia_48"
  TCT[17349] = "Proj_Map_Grid_of_Australia_49"
  TCT[17350] = "Proj_Map_Grid_of_Australia_50"
  TCT[17351] = "Proj_Map_Grid_of_Australia_51"
  TCT[17352] = "Proj_Map_Grid_of_Australia_52"
  TCT[17353] = "Proj_Map_Grid_of_Australia_53"
  TCT[17354] = "Proj_Map_Grid_of_Australia_54"
  TCT[17355] = "Proj_Map_Grid_of_Australia_55"
  TCT[17356] = "Proj_Map_Grid_of_Australia_56"
  TCT[17357] = "Proj_Map_Grid_of_Australia_57"
  TCT[17358] = "Proj_Map_Grid_of_Australia_58"
  TCT[17448] = "Proj_Australian_Map_Grid_48"
  TCT[17449] = "Proj_Australian_Map_Grid_49"
  TCT[17450] = "Proj_Australian_Map_Grid_50"
  TCT[17451] = "Proj_Australian_Map_Grid_51"
  TCT[17452] = "Proj_Australian_Map_Grid_52"
  TCT[17453] = "Proj_Australian_Map_Grid_53"
  TCT[17454] = "Proj_Australian_Map_Grid_54"
  TCT[17455] = "Proj_Australian_Map_Grid_55"
  TCT[17456] = "Proj_Australian_Map_Grid_56"
  TCT[17457] = "Proj_Australian_Map_Grid_57"
  TCT[17458] = "Proj_Australian_Map_Grid_58"
  TCT[18031] = "Proj_Argentina_1"
  TCT[18032] = "Proj_Argentina_2"
  TCT[18033] = "Proj_Argentina_3"
  TCT[18034] = "Proj_Argentina_4"
  TCT[18035] = "Proj_Argentina_5"
  TCT[18036] = "Proj_Argentina_6"
  TCT[18037] = "Proj_Argentina_7"
  TCT[18051] = "Proj_Colombia_3W"
  TCT[18052] = "Proj_Colombia_Bogota"
  TCT[18053] = "Proj_Colombia_3E"
  TCT[18054] = "Proj_Colombia_6E"
  TCT[18072] = "Proj_Egypt_Red_Belt"
  TCT[18073] = "Proj_Egypt_Purple_Belt"
  TCT[18074] = "Proj_Extended_Purple_Belt"
  TCT[18141] = "Proj_New_Zealand_North_Island_Nat_Grid"
  TCT[18142] = "Proj_New_Zealand_South_Island_Nat_Grid"
  TCT[19900] = "Proj_Bahrain_Grid"
  TCT[19905] = "Proj_Netherlands_E_Indies_Equatorial"
  TCT[19912] = "Proj_RSO_Borneo"
  TCT[1] = "CT_TransverseMercator"
  TCT[2] = "CT_TransvMercator_Modified_Alaska"
  TCT[3] = "CT_ObliqueMercator"
  TCT[4] = "CT_ObliqueMercator_Laborde"
  TCT[5] = "CT_ObliqueMercator_Rosenmund"
  TCT[6] = "CT_ObliqueMercator_Spherical"
  TCT[7] = "CT_Mercator"
  TCT[8] = "CT_LambertConfConic_2SP"
  TCT[9] = "CT_LambertConfConic_Helmert"
  TCT[10] = "CT_LambertAzimEqualArea"
  TCT[11] = "CT_AlbersEqualArea"
  TCT[12] = "CT_AzimuthalEquidistant"
  TCT[13] = "CT_EquidistantConic"
  TCT[14] = "CT_Stereographic"
  TCT[15] = "CT_PolarStereographic"
  TCT[16] = "CT_ObliqueStereographic"
  TCT[17] = "CT_Equirectangular"
  TCT[18] = "CT_CassiniSoldner"
  TCT[19] = "CT_Gnomonic"
  TCT[20] = "CT_MillerCylindrical"
  TCT[21] = "CT_Orthographic"
  TCT[22] = "CT_Polyconic"
  TCT[23] = "CT_Robinson"
  TCT[24] = "CT_Sinusoidal"
  TCT[25] = "CT_VanDerGrinten"
  TCT[26] = "CT_NewZealandMapGrid"
  TCT[5001] = "VertCS_Airy_1830_ellipsoid"
  TCT[5002] = "VertCS_Airy_Modified_1849_ellipsoid"
  TCT[5003] = "VertCS_ANS_ellipsoid"
  TCT[5004] = "VertCS_Bessel_1841_ellipsoid"
  TCT[5005] = "VertCS_Bessel_Modified_ellipsoid"
  TCT[5006] = "VertCS_Bessel_Namibia_ellipsoid"
  TCT[5007] = "VertCS_Clarke_1858_ellipsoid"
  TCT[5008] = "VertCS_Clarke_1866_ellipsoid"
  TCT[5010] = "VertCS_Clarke_1880_Benoit_ellipsoid"
  TCT[5011] = "VertCS_Clarke_1880_IGN_ellipsoid"
  TCT[5012] = "VertCS_Clarke_1880_RGS_ellipsoid"
  TCT[5013] = "VertCS_Clarke_1880_Arc_ellipsoid"
  TCT[5014] = "VertCS_Clarke_1880_SGA_1922_ellipsoid"
  TCT[5015] = "VertCS_Everest_1830_1937_Adjustment_ellipsoid"
  TCT[5016] = "VertCS_Everest_1830_1967_Definition_ellipsoid"
  TCT[5017] = "VertCS_Everest_1830_1975_Definition_ellipsoid"
  TCT[5018] = "VertCS_Everest_1830_Modified_ellipsoid"
  TCT[5019] = "VertCS_GRS_1980_ellipsoid"
  TCT[5020] = "VertCS_Helmert_1906_ellipsoid"
  TCT[5021] = "VertCS_INS_ellipsoid"
  TCT[5022] = "VertCS_International_1924_ellipsoid"
  TCT[5023] = "VertCS_International_1967_ellipsoid"
  TCT[5024] = "VertCS_Krassowsky_1940_ellipsoid"
  TCT[5025] = "VertCS_NWL_9D_ellipsoid"
  TCT[5026] = "VertCS_NWL_10D_ellipsoid"
  TCT[5027] = "VertCS_Plessis_1817_ellipsoid"
  TCT[5028] = "VertCS_Struve_1860_ellipsoid"
  TCT[5029] = "VertCS_War_Office_ellipsoid"
  TCT[5030] = "VertCS_WGS_84_ellipsoid"
  TCT[5031] = "VertCS_GEM_10C_ellipsoid"
  TCT[5032] = "VertCS_OSU86F_ellipsoid"
  TCT[5033] = "VertCS_OSU91A_ellipsoid"
  TCT[5101] = "VertCS_Newlyn"
  TCT[5102] = "VertCS_North_American_Vertical_Datum_1929"
  TCT[5103] = "VertCS_North_American_Vertical_Datum_1988"
  TCT[5104] = "VertCS_Yellow_Sea_1956"
  TCT[5105] = "VertCS_Baltic_Sea"
  TCT[5106] = "VertCS_Caspian_Sea"

  if n_elements(filters) gt 0 then begin
    regex = '^(' + strjoin(filters.toArray(), '|')+')_'

    filteredTCT = Hash()
    foreach key, TCT.keys() do begin
      if stregex(TCT[key], regex, /BOOLEAN) then begin
        filteredTCT[key] = TCT[key]
      endif
    endforeach
    TCT = filteredTCT
  endif

  return,TCT
end


;+
; :Description:
;    Returns an empty map info struct. The struct can be used to collect the required map info values. 
;-
function hubGeoHelper::getEmptyMapInfo 
  return,  hubIOImgMeta_mapInfo.createEmptyMapInfoStruct()
end

;+
; :Description:
;    This function returns the projected coordinate related to an pixel image coordinate.
;
; :Params:
;    pxX: in, required, type = integer
;     This is the pixels X, column or sample index.
;    pxY: in, required, type = integer
;     This is the pixels Y, row or line index.
;
; :Keywords:
;    Truncate: in, optional, type = int
;     Set this keyword to hande index out of bound errors as followed::
;       type = 0 | (default) make a normal coordinate conversion and throw an error in case of invalide pixel indices
;       type = 1 | remove outliers and return coordinate covered by the image only
;       type = 2 | set outliers to the nearest boder coordinate
;       type = 3 | change nothing, return coordinate out of the image boundaries too.
;-
function hubGeoHelper::convertPixelIndex2Coordinate, pxX, pxY, Truncate=Truncate 
  self._checkInitialization
  
  if ~isa(pxY) then begin
    ;conver 1D indices to 2D indices
    temp = ARRAY_INDICES([self.nSamples, self.nLines], pxX, /DIMENSIONS)
    pixelIndicesX = temp[0,*]
    pixelIndicesY = temp[1,*]
    
  endif else begin
    
    pixelIndicesX = pxX
    pixelIndicesY = pxY
  
  endelse 
  
  
  ;handle outliers 1
  if ~keyword_set(Truncate) then begin
    iX = where(pixelIndicesX lt 0 or pixelIndicesX gt self.nSamples - 1, /NULL)
    iY = where(pixelIndicesY lt 0 or pixelIndicesY gt self.nLines - 1, /NULL)
    if isa(iX) then message, 'X-Pixel Indices out of image boundaries'
    if isa(iY) then message, 'Y-Pixel Indices out of image boundaries'
  endif
  
  ;interpolate pixel coordinates
  eastValues   = interpol([self.ul.x, self.lr.x], [0, self.nSamples], pixelIndicesX) + (*self.mapInfo).sizeX*0.5
  northValues  = interpol([self.ul.y, self.lr.y], [0, self.nLines], pixelIndicesY) - (*self.mapInfo).sizeY*0.5
  
  ;handle outliers 2: truncation modes
  if keyword_set(Truncate) then begin
    case Truncate of 
      1: begin  ;truncate by removing outliers
         iPx = where(eastValues  ge self.ul.x and eastValues  le self.lr.x and $
                     northValues le self.ul.y and northValues ge self.lr.y , /NULL)
         if ~isa(iPx) then begin
            return, !NULL
         endif else begin
            pixelIndicesX = pixelIndicesX[iPx] 
            pixelIndicesY = pixelIndicesY[iPx]
         endelse
        end
      2: begin  ;truncate by setting outliers to exisiting range
          eastValues[where(eastValues lt self.ul.x, /NULL)] = self.ul.x
          eastValues[where(eastValues gt self.lr.x, /NULL)] = self.lr.x
          northValues[where(northValues gt self.ul.y, /NULL)] = self.ul.y
          northValues[where(northValues lt self.lr.y, /NULL)] = self.lr.y
         end
         
      3: ;do not change anything, allow out-of-range values 
      else : message, 'undefined value for option "truncate"'
    endcase
  
  endif
  
  return, self.createGeometry(eastValues, northValues)
end

;+
; :Description:
;    Converts a world coordinate to x and y pixel index values. 
;
; :Params:
;    geometry: in, required, type=geometry structure
;
; :Keywords:
;    Truncate: in, optional, type=boolean
;     Set this to return valid image pixel indices only.
;-
function hubGeoHelper::convertCoordinate2PixelIndex, geometry, Truncate=Truncate
  self._checkInitialization
  
  ;handle outliers 1
  if ~keyword_set(Truncate) then begin
    iX = where(geometry.x lt self.UL.x or geometry.x gt self.LR.x, /NULL)
    iY = where(geometry.y gt self.UL.y or geometry.y lt self.LR.y, /NULL)
    if isa(iX) then message, 'X-Coordinates out of image boundaries'
    if isa(iY) then message, 'Y-Coordinates out of image boundaries'
  endif
  
  
  ;interpolate pixel indices
  pixelIndicesX = interpol([0d, self.nSamples], [self.UL.x, self.LR.x], geometry.x)
  pixelIndicesY = interpol([0d, self.nLines]  , [self.UL.y, self.LR.y], geometry.y)
  
  ;handle outliers 2: truncation modes
  if keyword_set(Truncate) then begin
    case Truncate of
      1: begin  ;truncate by removing outliers
          iPx = where(pixelIndicesX ge 0 and pixelIndicesX lt self.nSamples and $
                      pixelIndicesY ge 0 and pixelIndicesY lt self.nLines, /NULL)
          if ~isa(iPx) then begin
            return, !NULL
          endif else begin
            pixelIndicesX = pixelIndicesX[iPx] 
            pixelIndicesY = pixelIndicesY[iPx]
          endelse
         end
      2: begin  ;truncate by setting outliers to [0, nSamples-1 or nLines-1]
          pixelIndicesX[where(pixelIndicesX lt 0, /NULL)] = 0
          pixelIndicesX[where(pixelIndicesX ge self.nSamples, /NULL)] = self.nSamples-1
          pixelIndicesY[where(pixelIndicesY lt 0, /NULL)] = 0
          pixelIndicesY[where(pixelIndicesY ge self.nLines, /NULL)] = self.nLines-1
         end
      3: ;do not change anything, allow out-of-range values 
      else : message, 'undefined value for option "truncate"'
    endcase 
  endif 
   
  pixelIndicesX = fix(pixelIndicesX) < (self.nSamples - 1)
  pixelIndicesY = fix(pixelIndicesY) < (self.nLines - 1)
  
  return, {x:fix(pixelIndicesX), y:fix(pixelIndicesY) $
          , pixelIndices: self.nSamples * pixelIndicesY + pixelIndicesX}
end

;+
; :Hidden:
;-
pro hubGeoHelper::_checkInitialization
  if ~self.isInitialized then message, 'geoHelper must get initialized to a map info'

end

;+
; :Description:
;    Returns true if the geoHelper was initialized to an image 
;
;-
function hubGeoHelper::isInitialized
  return, self.isInitialized
end


;+
; :Description:
;   Returns the mapInfo structure this geoHelper is related to.
;-
function hubGeoHelper::getMapInfo
  self._checkInitialization
  return, *self.mapInfo
end



;+
; :Hidden:
;-
pro hubGeoHelper__define
  struct = {hubGeoHelper $
    , mapInfo : ptr_new() $
    , boundaries : ptr_new() $
    , nLines : 0l $
    , nSamples : 0l $
    , UL:{POINT,x:0d, y:0d} $ ;upper left pixel edge coordinate
    , LR:{POINT,x:0d, y:0d} $ ;lower right pixel edge coordinate
    , isInitialized:0b $
    , inherits IDL_Object $
    } 
    ;, hasMapInfo : 0b $
  ;}
end

;+
; :Hidden:
;-
pro test_hubGeoHelper
  path =enmapBox_getTestImage('AF_Image')
  img = hubIOImgInputImage(path)
  gh = hubGeoHelper(img)
  geo_mbb = gh.getImageBoundaries()
  px_mbb  = gh.convertCoordinate2PixelIndex(geo_mbb)
  
  geo_mbb2 = gh.convertPixelIndex2Coordinate(px_mbb.x, px_mbb.y)
  
  mapInfo = img.getMeta('map info')
  nLines   = img.getMeta('lines')
  nSamples = img.getMeta('samples')
  div = img.getMeta('data ignore value')
  help, div
  print, div
  
  h  = hubGeoHelper(img)
  bounds = h.getImageBoundaries()
  print, h.getMapInfo()
  

  
  px  = h.convertCoordinate2PixelIndex(bounds)
  print, bounds.x, px.x
  
  bounds2 = bounds
  bounds2.x[0] = bounds2.x[0] + mapInfo.sizeX * 0.99
  
  px2 = h.convertCoordinate2PixelIndex(bounds2)
  print, bounds2.x, px2.x
  stop
end