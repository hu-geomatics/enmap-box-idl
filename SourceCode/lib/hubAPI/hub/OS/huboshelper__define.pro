;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Hidden::
;-


;überführe hierher:
;hubHelper::filepath
;hubHelper::openFile
;hubHelper::openHelpFile
;hubHelper::getByteOrder

pro hubOSHelper__define
  struct = {hubOSHelper $
    ,inherits IDL_Object $
  }
end