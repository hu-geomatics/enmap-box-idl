
;+
; :Description:
;    Initializes the Python Bridge
;
;
;
; :Keywords:
;    path_python: in, optional
;     Directory that contains the python executable. will be added to the PATH variable
;
;    pythonhome: in, optional
;     Directory where python searches for python libraries.
;     Will be set as PYTHONHOME environmental variable (if not already done).
;
;    verbose: in, optional, type=string, default=false
;       set this to get some more printouts on the python distribution.
;
; :Author: geo_beja
;-

function hubpython::_clean_env_paths, pathes
  compile_opt static, strictarr
  sep = PATH_SEP(/SEARCH_PATH)
  
  new_list = list()
  ;remove wrong directory endings, like '\' or ';'
  
    foreach path, pathes do begin
      if path eq '' then continue
      
      
      path2 = filepath(FILE_BASENAME(path), ROOT_DIR=FILE_DIRNAME(path))
      foreach c, [sep, '\','/'] do begin
        if path2.endswith(c) then begin
          path2 = strmid(path, 0, strlen(path)-strlen(c))
        endif  
      endforeach
      if isa(path2) && strlen(path2) > 0 $
        && ~isa(new_list.where(path2)) $
        && file_test(path, /Directory) then begin
        new_list.add, path2
      endif
  endforeach
  
  return, new_list
  
end

function hubpython::initPythonBridge $
  , DIR_PYTHON_EXE=DIR_PYTHON_EXE $
  , DIR_PYTHON_LIB=DIR_PYTHON_LIB $
  , verbose=verbose $
  , raise_errors=raise_errors
  
  compile_opt static, strictarr
  if ~isa(raise_errors) then raise_errors = 1b
  result = 0b

  hubAPIKey = 'hub.Python'

  if not raise_errors then begin
    CATCH, Error_status
    if Error_status ne 0 then begin
      CATCH, /CANCEL
      return, 0b
    endif

    result = hubPython.initPythonBridge(DIR_PYTHON_EXE=DIR_PYTHON_EXE $
      , DIR_PYTHON_LIB=DIR_PYTHON_LIB $
      , verbose=verbose $
      , raise_errors=1b)

  endif else begin


    if ~isa(verbose) then verbose = 0b

    ;cascaded variables
    hubsettings = hub_getSettings()


    if ~isa(DIR_PYTHON_EXE) then DIR_PYTHON_EXE = hubsettings.hubGetValue('dir_python_exe')

    if ~isa(DIR_PYTHON_LIB) then DIR_PYTHON_LIB = hubSettings.hubGetValue('dir_python_lib', default=getenv('PYTHONHOME'))
    if DIR_PYTHON_LIB eq '' then DIR_PYTHON_LIB = !NULL
    

    ;try to find folder with python library ("PYTHONHOME")
    if ~isa(DIR_PYTHON_LIB) then begin
      case 1b of 
        FILE_TEST(getenv('PYTHONHOME'), /DIRECTORY): begin
          DIR_PYTHON_LIB = getenv('PYTHONHOME')
          end
        isa(DIR_PYTHON_EXE) && FILE_TEST(DIR_PYTHON_EXE, /DIRECTORY): begin
          DIR_PYTHON_LIB = DIR_PYTHON_EXE
        end
        ELSE: ;nothing
      endcase
    endif
    
    ;try to find python.exe
    if ~isa(DIR_PYTHON_EXE) then begin 
      if isa(DIR_PYTHON_LIB) then begin
        p = FILEPATH('python', ROOT_DIR=DIR_PYTHON_LIB)
        if FILE_TEST(p) || FILE_TEST(p+'.exe') then begin
          DIR_PYTHON_EXE = DIR_PYTHON_LIB
        endif
      endif
      ;todo: try to find python installation from command line
    endif

    hubAssert, isa(DIR_PYTHON_EXE) && FILE_TEST(DIR_PYTHON_EXE, /DIRECTORY), 'Please specify DIR_PYTHON_EXE directory'
    hubAssert, isa(DIR_PYTHON_LIB) && FILE_TEST(DIR_PYTHON_LIB, /DIRECTORY), 'Please specify DIR_PYTHON_LIB directory'
    p = FILEPATH('python', ROOT_DIR=DIR_PYTHON_EXE) 
    hubAssert, FILE_TEST(p, /EXECUTABLE) || FILE_TEST(p+'.exe', /EXECUTABLE), 'Can not find python executable in DIR_PYTHON_EXE = '+DIR_PYTHON_EXE  

    

    sep = PATH_SEP(/SEARCH_PATH)
    ;remove wrong directory endings, like '\' or ';'
    DIR_PYTHON_LIB = (hubPython._clean_env_paths([DIR_PYTHON_LIB]))[0]


    ;read required env. variables
    PATH = list((getenv('PATH')).split(sep), /EXTRACT)
    ;add to env. variables, if not already done
    if ~isa(PATH.Where(DIR_PYTHON_EXE)) then PATH.add, DIR_PYTHON_EXE, 0

    
    PYTHONPATH = list((getenv('PYTHONPATH')).split(sep), /EXTRACT)

    IDLDirs = [filepath('', SUBDIRECTORY=['lib','bridges']) $
      ,filepath('', SUBDIRECTORY=['bin','bin.x86_64']) $
      ]

    foreach dir, idlDirs do begin
      hubAssert, file_test(dir, /DIRECTORY)
      if ~isa(PYTHONPATH.Where(dir)) then PYTHONPATH.add, dir, 0
    endforeach
    PYTHONPATH.add, DIR_PYTHON_LIB, 0
    
     
    
    DIR_HUBPYTHON = filepath('' $
                    , ROOT_DIR = hub_getDirname() $
                    , SUBDIRECTORY=['resource','python'])
    hubAssert, file_test(DIR_HUBPYTHON, /DIRECTORY)
    PYTHONPATH.add, DIR_HUBPYTHON, 0
    
    PATH.add, DIR_PYTHON_LIB, 0
    PATH.add, FILEPATH('Scripts', ROOT_DIR=DIR_PYTHON_LIB),0
    PATH = hubPython._clean_env_paths(PATH)
    PYTHONPATH = hubPython._clean_env_paths(PYTHONPATH)

    


    tested_DIR_PYTHON_LIB = hub_getAppState(hubAPIKey,'DIR_PYTHON_LIB')
    tested_DIR_PYTHON_EXE = hub_getAppState(hubAPIKey,'DIR_PYTHON_EXE')


    if DIR_PYTHON_EXE EQ tested_DIR_PYTHON_EXE and $
      DIR_PYTHON_LIB EQ tested_DIR_PYTHON_LIB then begin

      result = hub_getAppState(hubAPIKey ,'IS_INITIALIZED', default=0b)
    endif else  begin
      ;define environmental variables
      
      if 1b || verbose then begin
        print, 'PATH=',PATH
        print, 'PYTHONHOME=', DIR_PYTHON_LIB
        print, 'PYTHONPATH=', PYTHONPATH
      endif
      
      PATH = 'PATH=' + strjoin(PATH.toArray(),sep)
      PYTHONHOME = 'PYTHONHOME=' + DIR_PYTHON_LIB
      PYTHONPATH = 'PYTHONPATH=' + strjoin(PYTHONPATH.toArray(),sep)
      
      setenv, PATH
      setenv, PYTHONHOME
      setenv, PYTHONPATH

     

      hub_setAppState, hubAPIKey ,'DIR_PYTHON_LIB', !NULL
      hub_setAppState, hubAPIKey ,'DIR_PYTHON_EXE', !NULL

      pathCheck = filepath('hubPython.py', ROOT_DIR = DIR_HUBPYTHON)

      hubAssert, file_test(pathCheck), 'Missing file: '+pathCheck

      spawn, 'python "' + pathCheck + '" -B', res, err, EXIT_STATUS=EXIT_STATUS
      print, res
      if EXIT_STATUS ne 0 then begin
        message, strjoin(err,string(13b))
      endif

      sys = Python.Import('sys')
      print, 'IDL_Python Bridge initialized'
      print, sys.executable
      print, sys.version
      
      void = Python.Import('hubPython')

      hub_setAppState, hubAPIKey ,'DIR_PYTHON_LIB', DIR_PYTHON_LIB
      hub_setAppState, hubAPIKey ,'DIR_PYTHON_EXE', DIR_PYTHON_EXE
      hub_setAppState, hubAPIKey ,'IS_INITIALIZED', 1b
      result = 1b

    endelse
  endelse

  return, result
end


pro hubpython::initPythonBridge, DIR_PYTHON_EXE=DIR_PYTHON_EXE, DIR_PYTHON_LIB=DIR_PYTHON_LIB, verbose=verbose, raise_errors=raise_errors
  compile_opt static, strictarr
  !NULL = hubpython.initPythonBridge(DIR_PYTHON_EXE=DIR_PYTHON_EXE, DIR_PYTHON_LIB=DIR_PYTHON_LIB, verbose=verbose, raise_errors=raise_errors)
end


function hubPython::useGDAL_API
  compile_opt static, strictarr
  key = 'use_gdal_api'
  hasBridge = hubPython.initPythonBridge(raise_errors=0b)
  settings = hubSettings_getSettings()
  ;check if gdal should and can be used
  return, hasBridge && settings.hubKeywordSet(key)
  
end

pro hubPython__define
  compile_opt static, strictarr
  struct = {hubPython $
    ,inherits IDL_Object $
  }
end


pro test_hubpython
  print, hubpython.initPythonBridge(raise_errors=1b)
end