pro hubPython_showErrorReport, spawnResult, spawnError, Title=title
  if ~isa(title) then begin
    title = 'Python'
  endif
  hubExternal_showErrorReport, spawnResult, spawnError, Title=title
end
