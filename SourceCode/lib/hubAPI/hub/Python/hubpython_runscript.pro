;+
; :Description:
;    Use this function to run a Python script. Returns a result hash specified in Python.
;
;    For usage details also see
;    `Running R Scripts and Code <./_IDLDOC/rscripts>`
;
; :Params:
;    scriptFilename: in, required, type=string
;      Path to Python script.
;
;    parameters: in, optional, type=hash
;      Specify a hash of input parameters that is imported into R.
;      Note that data is passed via ASCII file I/O, which can be a bottle kneck.
;
;    spawnResult: out, optional, type=string[]
;      Standard output produced by Python.
;
;    spawnError: out, optional, type=string[]
;      Error output produced by Python.
;
; :Keywords:
;    GroupLeader: in, optional, type=widgetID
;      Group leader for progress bar.
;
;    Title: in, optional, type=widgetID, default='Python Script Output'
;      Report title.
;
;    NoReport: in, optional, type=boolean, default=0b
;      Set to suppress the HTML report that is showing the `spawnResult`  and `spawnError` information.
;
; :Author: Andreas Rabe
;-
function hubPython_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, PathPython=pathPython

  ; get python executable
  if ~isa(pathPython) then begin
    case !VERSION.OS_FAMILY of
      'Windows' :   begin
        ;is_init = hubPython.initPythonBridge()
        dir_python = (hub_getSettings()).hubGetValue('dir_python_exe')
        pathPython = FILEPATH('python.exe',ROOT_DIR=dir_python)
        if ~isa(dir_python) || ~file_test(pathPython) then begin
          ok = dialog_message(['Path to Python installation is not set correctly.', 'Use "dir_python_exe" to specify the folder with your python executable.'])
          hubSettings_editor, GroupLeader=groupLeader, Title=title
          message, 'huberror-ignore', /NONAME
        endif
        
      end
      'unix' : begin
        spawn, 'PATH=$PATH:/usr/bin; which python', spawnResult
        if spawnResult[0] eq '' then begin
          ok = dialog_message("The program 'Python' is currently not installed.")
          message, 'huberror-ignore', /NONAME        
        endif
      end
    endcase
  endif

  ; write inputs
  inputFilename = filepath(/TMP, 'hubPython_input.txt')
  outputFilename = filepath(/TMP, 'hubPython_output.txt')
  hubExternal_writeJSONFile, parameters, inputFilename, outputFilename

  ; run python script
  case !VERSION.OS_FAMILY of
    'Windows' : begin
      cmd = pathPython + ' "' + scriptFilename +'" "'+ inputFilename +'"'
      cd, dir_python, CURRENT=currentDirectory
    end
    'unix' : begin
      cmd = 'PATH=$PATH:/usr/bin; unset LD_LIBRARY_PATH; python '+scriptFilename +' '+ inputFilename
      cd, CURRENT=currentDirectory
    end
  endcase
  progressBar = hubProgressBar(Cancel=0, GroupLeader=groupLeader, Info='Running Python Script: '+file_basename(scriptFilename), Title=title)
  spawn, cmd, spawnResult, spawnError
  obj_destroy, progressBar
  cd, currentDirectory

  ; show spawn output in HTML report
  if ~keyword_set(noReport) then begin
    hubPython_showErrorReport, spawnResult, spawnError, Title=title
  endif

  ; read outputs
  result = hubExternal_readJSONFile(outputFilename)

  ; cleanup
  file_delete, inputFilename, outputFilename, /ALLOW_NONEXISTENT, /NOEXPAND_PATH, /QUIET 
  return, result
end

;+
; :Description:
;    Behaves like `hubPython_runScript` function, simply the result is skipped.
;
; :Author: Andreas Rabe
;-
pro hubPython_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, PathPython=pathPython
  !null = hubPython_runScript(scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, PathPython=pathPython)
end

;+
; :hidden:
;-
pro test_hubPython_runScript
;  pathPython = 'C:\Python27\ArcGISx6410.2\'
  scriptFilename = filepath('script.py', ROOT_DIR=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY=['TEMPLATE_PYTHON','_lib'])
  parameters = hash('inputText', 'Hello World from IDL')
  scriptResult = hubPython_runScript(scriptFilename, parameters, NoReport=0, PathPython=pathPython)
  print, scriptResult['outputText']
end