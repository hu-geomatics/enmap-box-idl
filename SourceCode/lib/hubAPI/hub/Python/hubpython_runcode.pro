;+
; :Description:
;    Use this function to run Python code. Returns a result hash specified in Python.
;
;    For usage details see
;    `Running R Scripts and Code <./_IDLDOC/rscripts>`
;
; :Params:
;    code: in, required, type=string[]
;      Specify Python code.
;
;    parameters: in, optional, type=hash
;      See `hubPython_runScript`.
;
;    spawnResult: out, optional, type=string[]
;      See `hubPython_runScript`.
;
;    spawnError: out, optional, type=string[]
;      See `hubPython_runScript`.
;
; :Keywords:
;    GroupLeader: in, optional, type=widgetID
;      See `hubPython_runScript`.
;
;    Title: in, optional, type=widgetID, default='R Script Output'
;      See `hubPython_runScript`.
;
;    NoReport: in, optional, type=boolean, default=0b
;      See `hubPython_runScript`.
;
;    ShowScript: in, optional, type=boolean, default=0b
;      Use to open the script that is created in the background.
;
; :Author: Andreas
;-
function hubPython_runCode, code, parameters, spawnResult, spawnError, File=file, GroupLeader=groupLeader, Title=title, NoReport=noReport, ShowScript=showScript, PathPython=pathPython

  script = list()
  script.add, [$
    '# import packages',$
    'import sys',$
    'import json',$
    '',$
    '# read input parameters',$
    'jsonFile = sys.argv[1]',$
    "fileObject = open(jsonFile, 'r')",$
    'jsonString = fileObject.read()',$
    'p = json.loads(jsonString) # p stands for input parameters',$
    'r = {}                     # r stands for result',$
    '',$
    '### user code ###',$
    ''], /Extract
  
  if keyword_set(file) then begin
    script.add, hubIOASCIIHelper.readFile(code), /Extract
  endif else begin
    script.add, code, /Extract    
  endelse
  script.add, [$
    '',$
    '#################',$
    '',$
    
    '# write output parameters',$
    "fileConnection = open(p['scriptOutputFilename'], 'w')",$
    'fileConnection.write(json.dumps(r))',$
    'fileConnection.close()'], /Extract
  scriptCode = script.toArray()
  scriptFilename = hub_getUserTemp('script.py')
  hubIOASCIIHelper.writeFile, scriptFilename, scriptCode 
  if keyword_set(showScript) then begin
    xdisplayfile, TEXT=scriptCode, FONT='COURIER'
    result = scriptCode
  endif else begin
    result = hubPython_runScript(scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, PathPython=pathPython)
    file_delete, scriptFilename, /ALLOW_NONEXISTENT, /NOEXPAND_PATH, /QUIET 
  endelse

  return, result
end

;+
; :Description:
;    Behaves like `hubPython_runCode` function, simply the result is skipped.
;
; :Author: Andreas Rabe
;-
pro hubPython_runCode, code, parameters, spawnResult, spawnError, _EXTRA=_extra
  !null =  hubPython_runCode(code, parameters, spawnResult, spawnError, _EXTRA=_extra)
end

;+
; :hidden:
;-
pro test_hubPython_runCode
  a = 1
  b = 2
  parameters = hash('a', a, 'b', b)
  code = "r['c'] = p['a']+p['b']"
  result = hubPython_runCode(code, parameters, NoReport=0)
  print, strtrim(a,2)+'+'+strtrim(b,2)+' = ',strtrim(result['c'],2)
end

;+
; :hidden:
;-
pro test_hubPython_runCode2

  plotFilename = filepath(/TMP, 'plot.png')
  x = [-2*!PI : +2*!PI : 0.1]
  y = cos(x)*x
  parameters = hash('x', x, 'y', y, 'plotFilename', plotFilename)
  parameters['title'] = 'Plotting Example'
  parameters['xlabel'] = 'x'
  parameters['ylabel'] = 'cos(x) x'
  parameters['format']  = 'b-'

  code = [$
    "import matplotlib.pyplot as pyplot",$
    "figure = pyplot.figure()",$
    "pyplot.plot(p['x'], p['y'], p['format'])",$
    "pyplot.title(p['title'])",$
    "pyplot.xlabel(p['xlabel'])",$
    "pyplot.ylabel(p['ylabel'])",$
    "figure.savefig(p['plotFilename'])"]
  hubPython_runCode, code, parameters, NoReport=0
  hubHelper.openFile, plotFilename
end

;+
; :hidden:
;-
pro test_hubhubPython_runCode3

  plotFilename = filepath(/TMP, 'plot.png')
  x = [-2*!PI : +2*!PI : 0.1]
  y = cos(x)*x
  parameters = hash('x', x, 'y', y, 'plotFilename', plotFilename)

  code = [$
    "import matplotlib.pyplot as pyplot",$
    "figure = pyplot.figure()",$
    "pyplot.plot(p['x'], p['y'], 'b-')",$
    "pyplot.title('Plotting Example')",$
    "pyplot.xlabel('x')",$
    "pyplot.ylabel('cos(x) x')",$
    "figure.savefig(p['plotFilename'])"]
  hubPython_runCode, code, parameters, NoReport=0
  hubHelper.openFile, plotFilename
end

