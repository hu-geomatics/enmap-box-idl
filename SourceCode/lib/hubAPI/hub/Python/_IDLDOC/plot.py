# import packages
import sys
import json

# read input parameters
jsonFile = sys.argv[1]
fileObject = open(jsonFile, 'r')
jsonString = fileObject.read()
p = json.loads(jsonString)
r = {}

### plot and save as png file ###

import matplotlib.pyplot as pyplot
figure = pyplot.figure()
pyplot.plot(p['x'], p['y'], p['format'])
pyplot.title(p['title'])
pyplot.xlabel(p['xlabel'])
pyplot.ylabel(p['ylabel'])
figure.savefig(p['plotFilename'])
