function hub_fix1d, array
  if isa(array) then begin
    !null = reform(/OVERWRITE, array, n_elements(array))
  endif
  return, array
end