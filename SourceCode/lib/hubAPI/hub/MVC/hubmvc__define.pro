function hubMVC::init, controller, model
  if ~isa(controller, 'hubMVCController') then message, 'Wrong argument.'
  self.initMVC, controller, model
  return, 1b
end

pro hubMVC::initMVC, controller, model
  self.controller = controller           ; application knows controller
  self.controller.setModel, model        ; controller knows model
  self.controller.setMVC, self        ; weak link to MVC object
end

pro hubMVC::subscribeView, view
  self.controller.subscribeView, view
end

pro hubMVC::unsubscribeView, view
  self.controller.unsubscribeView, view
end

function hubMVC::newView
  return, self.controller.newView()
end

pro hubMVC::getProperty, controller=controller
  if arg_present(controller) then controller = self.controller
end

pro hubMVC__define

  define = {hubMVC, inherits IDL_Object, $ 
    controller : obj_new() $
  }
  
end