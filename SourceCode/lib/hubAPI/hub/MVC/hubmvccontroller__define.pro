pro hubMVCController::setModel, model
  if ~isa(model, 'hubMVCModel') then message, 'Wrong argument.'
  self.model = model
  model.controller = self
end

pro hubMVCController::setMVC, mvc
  if ~isa(mvc, 'hubMVC') then message, 'Wrong argument.'
  self.mvc = mvc
end

pro hubMVCController::subscribeView, view
  if ~isa(view, 'hubMVCView') then message, 'Wrong argument.'
  if ~isa(self.views, 'list') then self.views = list()
  if self.views.hubIsElement(view) then message, 'View already subcribed.'
  self.views.add, view
  view.controller = self
end

pro hubMVCController::unsubscribeView, view
  if ~isa(view, 'hubMVCView') then message, 'Wrong argument.'
  if ~self.views.hubIsElement(view, Index=index) then message, 'View is not subcribed.'
  self.views.remove, index
end

function hubMVCController::newView
  message, 'abstract methode, return a newly created MVCView object'
end

pro hubMVCController::handleViewMessage, message
  message, 'abstract methode'
end

pro hubMVCController::handleModelMessage, message
  message, 'abstract methode'
end

pro hubMVCController::getProperty, model=model, views=views, mvc=mvc
  if arg_present(model) then model = self.model
  if arg_present(views) then views = self.views
  if arg_present(mvc) then mvc = self.mvc
end

pro hubMVCController::finish

  foreach view, self.views do begin
    view.finish
  endforeach
  self.model.finish
  obj_destroy, self.mvc
  
end

pro hubMVCController__define
  define = {hubMVCController, inherits IDL_Object, $
    mvc : obj_new(),$
    model : obj_new(),$
    views : list()}
end