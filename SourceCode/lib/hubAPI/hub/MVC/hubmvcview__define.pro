;function hubMVCView::newMessage, name
;  return, {name : name}
;end
;
;pro hubMVCView::sendMessage, message
;  self.controller.handleViewMessage, message
;end

pro hubMVCView::setProperty, controller=controller
  if isa(controller) then self.controller = controller
end

pro hubMVCView::getProperty, controller=controller
  if arg_present(controller) then controller = self.controller
end

pro hubMVCView::finish
  ; do nothing  
end

pro hubMVCView__define

  define = {hubMVCView, inherits IDL_Object,$
    controller : obj_new() $
  }
  
end