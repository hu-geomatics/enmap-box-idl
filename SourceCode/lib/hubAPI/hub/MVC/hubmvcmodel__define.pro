pro hubMVCModel::finish
  ; do nothing  
end

pro hubMVCModel::setProperty, controller=controller
  if isa(controller) then self.controller = controller
end

pro hubMVCModel::getProperty, controller=controller
  if arg_present(controller) then controller = self.controller
end

pro hubMVCModel__define
  define = {hubMVCModel, inherits IDL_Object,$
    controller:obj_new()}
end