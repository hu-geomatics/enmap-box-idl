;+
; This is a error handler batch that can be included into a procedure by typing
; `@huberrorcatch`. It catches any error, print the error message to the console, displayes
; the message to the screen and returns.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;        
; :Examples:
;    Use the `huberrorcatch` batch inside a procedure. Compile the following procedure definition::
;      pro myProcedure
;      
;        ; use error handler batch file
;        @huberrorcatch
;        
;        ; throw an error
;        message, 'Hello Error Message!'
;      
;      end
;    Run the procedure::
;      myProcedure
;    IDl prints::
;      % MYPROCEDURE: Hello Error Message!
;      % Execution halted at:  MYPROCEDURE         7 T:\myprocedure.pro
;      %                       $MAIN$          
;    
;-

  catch, error_status
  if (error_status ne 0) then begin
    catch, /CANCEL

    ; handle special huberrors
    case !error_state.msg of 
      'huberror-ignore' : ; do nothing  
      else : begin
        help, /LAST_MESSAGE
        help, /LAST_MESSAGE, OUTPUT=output
        !null = dialog_message(output, /ERROR)
      end
    endcase
    return
  endif 
 

  