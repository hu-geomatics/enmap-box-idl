function hub_fix3d, array
  if isa(array) then begin
    !null = reform(/OVERWRITE, array, n_elements(array[*,0,0]), n_elements(array[0,*,0]), n_elements(array[0,0,*]))
  endif
  return, array
end