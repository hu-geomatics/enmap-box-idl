;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns hubAPI test speclib filenames.
;    
; :Params:
;    basename: in, required, type=file basename
;    
; :Examples:
;    Print all hubAPI test image filenames::
;    
;      print, hub_getTestSpeclib()
;      
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\speclib\ClassificationLabels
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\speclib\ClassificationSpeclib
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\speclib\RegressionLabels
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\speclib\RegressionSpeclib  
;        
;    Print a specific filename::
;    
;      print, hub_getTestSpeclib('ClassificationSpeclib')
;      
;    IDL prints something like::
;    
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\speclib\ClassificationSpeclib
;    
;-
function hub_getTestSpeclib, basename, All=all

  getAll = ~isa(basename)
  root = filepath('', ROOT=hub_getDirname(), SUBDIR=['resource','testData','speclib'])
  if getAll then begin
    result = list(file_search(root, '*.hdr'), /EXTRACT)
    foreach filename, result, i do begin
      if strcmp(file_basename(filename), 'pyramid.', 8) then begin
        result[i] = ''
      endif else begin
        result[i] = hubHelper.removeFileExtension(filename)
      endelse
    endforeach
    removeIndex = result.where('')
    if isa(removeIndex) then result.remove, removeIndex
  endif else begin
    result = filepath(basename, ROOT=root)
    if ~(hubIOHelper()).fileReadable(result) then begin
      message, 'File is not part of hubAPI test speclib set: '+result
    endif
  endelse
  return, result

end

;+
; :Hidden:
;-
pro test_hub_getTestSpeclib
  
  print, hub_getTestSpeclib(/All)
  print, hub_getTestSpeclib('ClassificationSpeclib')
end
