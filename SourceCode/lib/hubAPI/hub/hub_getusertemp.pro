;+
; :Description:
;    Returns the user-specific temp directory or, if `basename` is defined, a temp filename.
;
; :Params:
;    basename: in, optional, type=string
;      Basename of the temp file.
;
; :Keywords:
;    SubDirectory: in, optional, type={string | string[]}
;      Specifiy subdirectories. 
;
; :Author: Andreas Rabe
;-
function hub_getUserTemp, basename, SubDirectory=subDirectory
  if ~isa(basename) then begin
    basename = ''
  endif
  
  ; default temp directory  
  case !VERSION.OS of
    'Win32' : tempdirDefault = filepath('', /TMP)
    'linux' : tempdirDefault = filepath('', ROOT_DIR='/tmp/', SUBDIRECTORY=(get_login_info()).user_name)
    'darwin' : tempdirDefault = filepath('', ROOT_DIR='/Users/Shared/', SUBDIRECTORY=(get_login_info()).user_name)
  endcase
  
  ; use temp directory specified inside settings file if defined
  hubSettings = hub_getSettings()
  tempdirSettings = hubSettings.hubGetValue('dir_tmp', Default='')

  ; use temp directory specified inside settings file if defined  
  tempdir = (tempdirSettings eq '') ? tempdirDefault : tempdirSettings

  ; insert basename and subdirectories
  result = filepath(basename, ROOT_DIR=tempdir, SUBDIRECTORY=subDirectory)
  
  ; create temp directory
  file_mkdir, file_dirname(result), /NOEXPAND_PATH  
  
  return, result
end