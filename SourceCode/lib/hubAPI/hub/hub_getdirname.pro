;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the hubAPI directory. Optionally, set `SourceCode` to return the associated source code directory.
;    
; :Keywords:
;    SourceCode:   in, optional, type=boolean
;      Set this keyword to return the associated source code folder.
;
; :Examples:
;    Print hubAPI directory and associated source code directory::
;      print, hub_getDirname()
;      print, hub_getDirname(/SourceCode)
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\lib\hubAPI
;      D:\EnMAP-Box\SourceCode\lib\hubAPI_v1.1
;-
function hub_getDirname, SourceCode=sourceCode
  targetDir = 'hubAPI'
  result = filepath(targetDir, ROOT=enmapBox_getDirname(/Lib, SourceCode=sourceCode))
  return, result
end

;+
; :Hidden:
;-
pro test_hub_getDirname

  print, hub_getDirname()
  print, hub_getDirname(/SourceCode)

end
