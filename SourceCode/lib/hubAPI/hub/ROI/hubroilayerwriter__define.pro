pro hubROILayerWriter::writeROIFile, filename, roiLayer
  save, FILENAME=filename, roiLayer
  hubProEnvHelper.openFile, filename
end

pro hubROILayerWriter::writeShapefile, pathShapefile, roiLayer $
    , coordinateSystemString=coordinateSystemString $
    , referenceFile=referenceFile $
    , mapInfo=mapInfo 
  
  if isa(referenceFile) then begin
    img = hubIOImgInputImage(referenceFile)
    mapInfo = img.getMeta('map info')
    coordinateSystemString = img.getMeta('coordinate system string')
  endif
  
  if ~isa(mapInfo) then begin
    ;mapInfo = hubIOImgHeader_parseHeader(()
    message, 'map info is not specified but required to transform pixel coordinates into SRS coordinates'
  endif
  
  nGeom = roiLayer.getNumberOfGeometries()
  nAttr = roiLayer.getNumberOfAttributes()
  
  if nGeom le 0 then message, 'There are no ROIs to export' 
  
  
  shpWriter = hubIOShapeFileWriter(pathShapefile, 'POINT', SPATIALREFERENCESYSTEM=coordinateSystemString)
  
  ;add attributes
  
  layerSize = roiLayer.getLayerSize()
  geoHelper = hubGeoHelper()
  geoHelper.initByMapInfo, mapInfo, layerSize[0], layerSize[1]
  
  
  
  attributeNames = roiLayer.getAttributeNames()
  attributeTypes = roiLayer.getAttributeTypes()
  attributeFormats       = roiLayer.getAttributeFormats()
  attributeFormatStrings = roiLayer.getAttributeFormats(/IDLFormatString)
  
  attributeTable = roiLayer.getattributeTable()
  
  attributesToAdd = Hash()
  foreach attributeName, attributeNames, iA do begin
    attributesToAdd[attributeName] = !NULL
    aValues = attributeTable[iA]
    atype   = attributeTypes[iA] 
    precision = !NULL
    case attributeFormats[iA] of
      'string'  : maxLength = max(strlen(avalues.toArray(missing='       ')))
                  
      'integer' : maxLength = ceil(max(alog10(abs(avalues.toArray(missing=0)))))+1
                  
      'decimal' : begin
                    maxLength = ceil(max(alog10(abs(avalues.toArray(missing=0)))))+1
                    format = attributeFormatStrings[iA]
                    precision = STRMID(format,4, strlen(format) - 5)
                    maxLength += precision + 1
                  end
      else : message, 'unknown roi data type: '+ attributeFormats[iA]
    endcase
    ;calculate fitting shapefile precission
    
    shpWriter.addAttribute, attributeName, atype, maxLength, precision=precision
  endforeach
  
  
  
  foreach pixel1DIndices, roiLayer.getGeometries(), iGeom do begin
     foreach attributeName, attributenames, iA do begin
      attributesToAdd[attributename] = (attributeTable[iA])[iGeom] 
     endforeach
     
     coordinates = geoHelper.convertPixelIndex2Coordinate(pixel1DIndices, truncate=3)
     for iCoord = 0, n_elements(coordinates.x)-1 do begin
      
      shpWriter.writeData, coordinates.x[iCoord], coordinates.y[iCoord] , attributesToAdd
     endfor
  endforeach
  
  hubProEnvHelper.openFile, pathShapefile
end

pro hubROILayerWriter::writeClassification, filename, roiLayer, classIDAttributeIndex, classNameAttributeIndex, MapInfo=mapInfo, CoordinateSystemString=coordinateSystemString
  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    help, /LAST_MESSAGE
    stop
  endif

  layerSize = roiLayer.getLayerSize()
  geometries = roiLayer.getGeometries()
  colors = roiLayer.getColors()
  attributes = roiLayer.getAttributeTable()
  ids = (attributes[classIDAttributeIndex]).toArray(Missing=0, Type='int')
  if isa(classNameAttributeIndex) then begin
    names = attributes[classNameAttributeIndex]
  endif
  
  writerSettings = hash($
    'samples', layerSize[0], $
    'lines', layerSize[1], $
    'bands', 1, $
    'tileProcessing', 0b, $
    'dataFormat', 'profiles', $
    'data type', 2)
  
  outputImage = hubIOImgOutputImage(filename)
  outputImage.initWriter, writerSettings
  classes = max(ids)+1
  classNames = 'Class '+strtrim(ulindgen(classes), 2)
  classLookup = bytarr(3,classes)
  foreach id, ids, i do begin
    indices = geometries[i]
    if isa(indices) then begin
      data = replicate(id, 1, n_elements(indices))
      outputImage.writeData, data, indices
    endif
    if isa(classNameAttributeIndex) then begin
      className = names[i]
      if isa(className) then begin
        classNames[id] = strtrim(className,2)
      endif
    endif
    classLookup[*,id] = colors[i]
  endforeach
  classLookup[*,0] = 0
  classNames[0] = 'Undefined'

  outputImage.setMeta, 'file type', 'envi classification'
  outputImage.setMeta, 'classes', classes
  outputImage.setMeta, 'class names', classNames
  outputImage.setMeta, 'class lookup', classLookup
  outputImage.setMeta, 'map info', mapInfo
  outputImage.setMeta, 'coordinate system string', coordinateSystemString
  outputImage.finishWriter  
end

pro hubROILayerWriter::writeRaster, filename, roiLayer, attributeIndices, dataType, dataIgnoreValue, MapInfo=mapInfo, CoordinateSystemString=coordinateSystemString

  layerSize = roiLayer.getLayerSize()
  geometries = roiLayer.getGeometries()
  attributes = (roiLayer.getAttributeTable())[attributeIndices]
  names = (roiLayer.getAttributeNames())[attributeIndices]
  bands = n_elements(attributeIndices)
  writerSettings = hash($
    'samples', layerSize[0], $
    'lines', layerSize[1], $
    'bands', bands, $
    'tileProcessing', 0b, $
    'dataFormat', 'profiles', $
    'data type', dataType)

  outputImage = hubIOImgOutputImage(filename)
  outputImage.setMeta, 'data ignore value', dataIgnoreValue
  outputImage.initWriter, writerSettings
  foreach indices, geometries, geometryIndex do begin
    if ~isa(indices) then continue
    profile = hub_fix2d(make_array(bands, 1, TYPE=outputImage.getMeta('data type')))
    for band=0,bands-1 do begin
      bandValue = (attributes[band])[geometryIndex]
      profile[band] = isa(bandValue) ? bandValue : dataIgnoreValue
    endfor
    data = rebin(/SAMPLE, profile, bands, n_elements(indices))
    outputImage.writeData, hub_fix2d(data), indices
  endforeach
  outputImage.setMeta, 'band names', names.toArray()
  outputImage.setMeta, 'map info', mapInfo
  outputImage.setMeta, 'coordinate system string', coordinateSystemString
  outputImage.finishWriter
end

pro hubROILayerWriter__define
  struct = {hubROILayerWriter, inherits IDL_Object}
end

pro test_hubROILayerWriter__writeShapefile
  reader = hubROILayerReader()
  refFile = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  layerA = reader.importRaster(refFile)
  
  shapeFileName = 'D:\SVNCheckouts\EnMAP-Box\SourceCode\lib\hubAPI\_resource\testData\vector\testshape'
  writer = hubROILayerWriter()
  writer.writeShapefile, shapeFileName, layerA, referenceFile=refFile


  filename = filepath('testROI.enmapROI', /TMP)
  writer.writeROIFile, filename, layerA
  layerB = reader.readROIFile(filename)
  layerB.print
  
end

pro test_hubROILayerWriter__writeClassification
  reader = hubROILayerReader()
  roiLayer = reader.importRaster(hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample'))
  filename = filepath('classification2', /TMP)
  writer = hubROILayerWriter()
  writer.writeClassification, filename, roiLayer, 0, 1
end

pro test_hubROILayerWriter__writeRaster
  reader = hubROILayerReader()
  roiLayer = reader.importRaster('D:\Users\Andreas\Desktop\svcEstimationClassProbabilities')
  filename = filepath('raster', /TMP)
  writer = hubROILayerWriter()
  writer.writeRaster, filename, roiLayer, indgen(5), 'float', -1
end

pro test_bug1

  reader = hubROILayerReader()
  roiLayer = reader.readROIFile('G:\temp\temp_ar\roi.enmapROI')
  roiLayer.print
  imageFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  writer = hubROILayerWriter()
  shapefileFilename = filepath(/TMP, 'shapefile.shp')
  writer.writeShapeFile, shapefileFilename, roiLayer, referenceFile=imageFilename
  
end


