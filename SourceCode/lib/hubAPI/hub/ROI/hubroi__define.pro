function hubROI::init, roiLayer, geometry, oModel, Color=color

  if ~isa(roiLayer, 'hubROILayer') then begin
    message, 'Wrong argument.'
  endif
  self.roiLayer = roiLayer
  color = isa(color) ? color : randomu(seed, 3, /LONG) mod 256 ;[255,0,0]
  self.oSymbolData = IDLgrPolygon([0,1,1,0],-[0,0,1,1], ALPHA_CHANNEL=1, Color=color)
  self.oSymbol = IDLgrSymbol(self.oSymbolData)
  self.oROI = IDLgrROI(STYLE=0, Symbol=self.oSymbol, LINESTYLE=6)
  self.zvalue = 0.1
  oModel.add, self.oROI
  self.setColor, color
  self.setGeometry, geometry
  return, 1b
end

pro hubROI::setColor, color
  self.oSymbolData.setProperty, Color=color
end

function hubROI::getColor
  self.oSymbolData.getProperty, Color=color
  return, color
end

pro hubROI::setGeometry, geometry
  self.geometry = ptr_new(geometry)
  self.setPlotData
end

pro hubROI::setPlotData
  geometry = self.getGeometry()
  if isa(geometry) then begin
    indices2D = self.convertGeometryTo2D(geometry)
    xy = self.flipY(indices2D)
    xyz = [xy,replicate(self.zvalue,1,n_elements(geometry))]
    self.oROI.setProperty, DATA=xyz
  endif else begin
    self.oROI.setProperty, DATA=[]
  endelse
end

function hubROI::getGeometry
  geometry = *self.geometry
  return, geometry
end

function hubROI::getSize
  return, n_elements(*self.geometry)
end

function hubROI::flipY, xy
  layerSize = self.roiLayer.getLayerSize()
  xyFlipped = xy
  xyFlipped[1,*] = layerSize[1]-xyFlipped[1,*]
  return, xyFlipped
end

function hubROI::convertGeometryTo2D, indices1D
  layerSize = self.roiLayer.getLayerSize()
  indices2D = array_indices(layerSize, indices1D, /DIMENSIONS)
  return, indices2D
end

pro hubROI::addPixel, pixel
  geometry = self.getGeometry()
  if ~isa(where(/NULL, geometry eq pixel)) then begin
    *self.geometry = [*self.geometry, pixel]
    index2D = self.convertGeometryTo2D(pixel)
    xy = self.flipY(index2D)
    self.oROI.AppendData, xy[0], xy[1], self.zvalue
  endif
end

pro hubROI::removePixel, pixel
  geometry = self.getGeometry()
  if isa((index=where(/NULL, geometry eq pixel, COMPLEMENT=complement))) then begin
    *self.geometry = (*self.geometry)[complement]
    self.oROI.RemoveData, START=index
  endif
end

pro hubROI::decreaseIndices, indexStart
  geometry = self.getGeometry()
  if ~isa(geometry) then return
  indices = where(/NULL, geometry gt hub_fix0d(indexStart))
  if ~isa(indices) then return
  geometry[indices] -= 1
  self.setGeometry, geometry
end

pro hubROI::delete
  obj_destroy, self.oROI
end

pro hubROI::empty
  self.setGeometry, [0]
  self.setPlotData
  self.removePixel, 0
end

pro hubROI__define
  struct = {hubROI, inherits IDL_Object,$
    roiLayer:obj_new(), geometry:ptr_new(),$
    oSymbolData:obj_new(), oSymbol:obj_new(),oROI:obj_new(),$
    zvalue:0.}
end
