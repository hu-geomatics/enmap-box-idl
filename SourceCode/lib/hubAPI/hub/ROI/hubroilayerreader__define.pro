function hubROILayerReader::readROIFile, filename
  compile_opt static, strictarr
  restore, FILENAME=filename
  return, roiLayer
end

function hubROILayerReader::importShapefile, pathShapefile, pathReferenceRaster
  compile_opt static, strictarr
  refImg = hubIOImgInputImage(pathReferenceRaster)
  geoHelper = hubGeoHelper(refImg)
  shpReader = hubIOShapeFileReader(pathShapefile)
  
  shapeType = shpReader.getShapeType()
  if total(shapeType eq ['POINT','POLYGON']) eq 0 then message, 'Shape file with POINT or POLYGON data required'
  attributeFormats = shpReader.getAttributeTypes(/Typecodes)
  attributeNames = shpReader.getAttributeNames()
  roiGeometries = list()
  
  entitiesPerStep = 200
  nEntities = shpReader.getNumberOfEntities()
  if nEntities eq 0 then message, 'Shape file is empty'
  
  layerSize = refImg.getSpatialSize()
  data = shpReader.getData()
  
  
  ;geometry
  foreach geom, data.geometries do begin
    pixelIndices = !NULL
    case geom.type of
      'point'   : begin
        if geoHelper.imageContains(geom) then begin
          pixelCoord = geoHelper.convertCoordinate2PixelIndex(geom)
          pixelIndices = pixelCoord.pixelIndices
          if n_elements(pixelIndices) gt 1 then stop
          
        endif
      end
      'polygon' : begin
        foreach part, geom.parts do begin
          if geoHelper.imageContains(part) then begin
            pixelCoord = geoHelper.convertCoordinate2PixelIndex(part)
            temp = polyfillv(pixelCoord.x, pixelCoord.y, nSamples, nLines)
            if temp[0] eq -1 then begin
              ;vertex coordinate too close to enclose other pixel
              ;add as single pixels?
              ;print, 'Polygon too small'
              temp = !NULL
            endif
            pixelIndices = [pixelIndices, temp]
          endif
        endforeach
      end
      else : message, 'Unknown type: '+geom.type
    endcase
    
    if isa(pixelIndices) then roiGeometries.add , pixelIndices, /EXTRACT, /NO_COPY
    
  endforeach
  
  attributeTable = list()
  for i=0, data.numberOfAttributes-1 do begin
    attributeTable.add, list(data.attributes.(i), /Extract)
  endfor
  
  attributeFormats = self._mapDataTypeToAttributeType(attributeFormats)
  
  roiLayer = hubROILayer(layerSize, roiGeometries $
                  , list(attributeNames, /EXTRACT) $
                  , list(attributeFormats, /EXTRACT) $
                  , attributeTable, Colors=colors)
  
  return, roiLayer
  
  
  
end

function hubROILayerReader::_mapDataTypeToAttributeType, idlDataTypeCode
  compile_opt static, strictarr
  if ISA(idlDataTypeCode, /ARRAY) then begin
    result = list()
    foreach t, idlDataTypeCode do begin
      result.add, self._mapDataTypeToAttributeType(t)
    endforeach
    result = result.toArray()
  endif else begin
    switch idlDataTypeCode of
      0: 
      7: begin
          result = 'string'
          break
         end
      4:
      5: begin
          result = 'decimal'
          break
         end
      else: result = 'integer'
    endswitch
  endelse
  
  return, result
end

function hubROILayerReader::importRaster, filename, PrepareRelabeling=prepareRelabeling

  compile_opt static, strictarr
  
  image = hubIOImgInputImage(filename)
  
  filenameFeatures = filename
  bands = image.getMeta('bands')
  if bands eq 1 then begin
    filenameLabels = filename 
  endif else begin
    filenameLabels = filepath('mask', /TMP)
    parameters = hash('inputFilename', filename, 'outputFilename', filenameLabels)
    hubApp_extractMask_processing, parameters
  endelse
  sampleSet = hubIOImg_readSample(filenameFeatures, filenameLabels)

  if image.isClassification() then begin
    classes = image.getMeta('classes')
    lookup = image.getMeta('class lookup')
    classNames = list(/EXTRACT, (image.getMeta('class names'))[1:*])
    histogramResult = hubMathHistogram(sampleSet.labels, [1,classes-1], /Integer, /GetReverseIndices)
    classIDs = list(histogramResult['binStart'], /EXTRACT)
    geometries = list()
    positions = sampleSet.positions
    foreach inverseIndices, histogramResult['reverseIndices'] do begin
      geometries.add, positions[inverseIndices]
    endforeach
    attributeNames = list('classID','className')
    attributeFormats = list('integer','string')
;    attributeFormats = list(['decimal','3'],'string')
    
    attributeTable = list(classIDs, classNames)
    colors = list()
    for i=1,classes-1 do colors.add,  lookup[*,i]
    
    if keyword_set(prepareRelabeling) then begin
      allGeometries = list()
      foreach geometry,geometries,i do begin
        allGeometries.add, geometry, /EXTRACT
        geometries[i] = []
      endforeach
      geometries.add, allGeometries.toArray(), 0
      (attributeTable[0]).add, 0, 0
      (attributeTable[1]).add, 'Unclassified', 0
      colors.add, [255,0,255], 0
    endif
    
  endif else begin
    geometries = list()
    foreach position, sampleSet.positions do begin
      geometries.add, [position]
    endforeach
    attributeNames = list(image.getMeta('band names'), /EXTRACT)
    attributeFormats = list(['decimal','4'], LENGTH=bands)
    attributeTable = list()
    for i=0,bands-1 do begin
      attributeTable.add, list((sampleSet.features)[i,*], /EXTRACT)
    endfor
    colors = [0,0,0]
  endelse
  
  layerSize = image.getSpatialSize()
  roiLayer = hubROILayer(layerSize, geometries, attributeNames, attributeFormats, attributeTable, Colors=colors)
  return, roiLayer
end

pro hubROILayerReader__define
  struct = {hubROILayerReader, inherits IDL_Object}
end

pro test_hubROILayerReader__importRaster
  reader = hubROILayerReader()
  ;roiLayer = reader.importClassification(hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample'))
  ;roiLayer = reader.importRaster(hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample'))
  
  roiLayer = reader.importRaster('C:\Users\janzandr\Desktop\svcEstimationClassProbabilities')
  roiLayer.print
end

pro test_hubROILayerReader_importShapefile
  pathSHP = 'D:\SVNCheckouts\EnMAP-Box\SourceCode\lib\hubAPI\_resource\testData\vector\Berlin-A_Point'
  pathRefRaster = hub_getTestImage('Hymap_Berlin-A_Image')
  roiLayer = hubROILayerReader.importShapefile(pathShp, pathRefRaster)
  roiLayer.print
end

pro test_hubROILayerReader_importRaster
  roiLayer = hubROILayerReader.importRaster(hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample'))
  roiLayer.print
end