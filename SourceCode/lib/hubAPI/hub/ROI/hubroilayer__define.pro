function hubROILayer::init, layerSize, geometries, attributeNames, attributeFormats, attributeTable, Colors=colors
  self.layerSize = layerSize

  self.attributeNames = isa(attributeNames) ? attributeNames : list('Attribute1')
  self.attributeFormats = isa(attributeFormats) ? attributeFormats : list('string')
  geometries = isa(geometries) ? geometries : list()

  ; store geometries inside rois
  self.oModel = IDLgrModel()
  self.rois = list()
  
  foreach geometry,geometries,i do begin
    case 1b of
      isa(colors, 'list') : color = colors[i]
      isa(colors, /ARRAY) : color = colors
      else : color = !null
    endcase
    self.rois.add, hubROI(self, geometry, self.oModel, Color=color)
  endforeach  

  ; create attribute table
  if isa(attributeTable) then begin
    self.attributeTable = attributeTable
  endif else begin
    self.attributeTable = list()
    foreach attributeName, self.attributeNames do begin
      emptyValueVector = list(LENGTH=n_elements(geometries))
      self.attributeTable.add, emptyValueVector
    endforeach
  endelse
  
  self.checkConsistency
  return, 1b
end

pro hubROILayer::checkConsistency

  if self.attributeNames.count() ne self.attributeTable.count() then begin
    message, 'Attribute names and attribute table do not match.'
  endif
  if self.attributeNames.count() ne self.attributeFormats.count() then begin
    message, 'Attribute names and attribute formats do not match.'
  endif

  numberOfGeometries = self.getNumberOfGeometries()
  foreach attributeList,self.attributeTable do begin
    if numberOfGeometries ne attributeList.count() then begin
      message, 'Number of geometries and attribute values do not match.'
    endif
  endforeach
end

pro hubROILayer::initGraphic, oParent
  oParent.add, self.oModel
  self.graphicInitialized = 1b
end

pro hubROILayer::finishGraphic
  self.oModel.getProperty, Parent=oParent 
  if obj_valid(oParent) then oParent.remove, self.oModel
  self.graphicInitialized = 0b
end

function hubROILayer::getModel
  if ~self.graphicInitialized then begin
    message, 'Graphic not initialized.'
  endif
  return, self.oModel
end

pro hubROILayer::setLayerSize, layerSize
  self.layerSize = layerSize
end

function hubROILayer::getLayerSize
  return, self.layerSize
end

function hubROILayer::getNumberOfAttributes
  return, self.attributeNames.count()
end

function hubROILayer::getNumberOfGeometries
  return, self.rois.count()
end

function hubROILayer::getAttributeTable, Text=text
  if keyword_set(text) then begin
    if self.getNumberOfAttributes() eq 0 or self.getNumberOfGeometries() eq 0 then begin
      return, !null
    endif
    result = strarr(self.getNumberOfAttributes(), self.getNumberOfGeometries())
    formats = self.getAttributeFormats(/IDLFORMATSTRING)
    missingValues = self.getAttributeNulls()
    types = self.getAttributeTypes()
    
    foreach attribute, self.attributeTable, i do begin
      if (attribute.count() eq 1) && ~isa(attribute[0]) then begin ; handle bug with single element lists
        result[i,*] = [' ']
      endif else begin
        attributeValues = attribute.toArray(Type=types[i], Missing=missingValues[i])
        attributeValues = string(attributeValues, FORMAT=formats[i])
        attributeValues[where(/NULL, attribute eq !null)] = ' '
        result[i,*] = attributeValues
      endelse
    endforeach
  endif else begin
    result = self.attributeTable
  endelse
  return, result
end

function hubROILayer::getAttributeNames
  return, self.attributeNames
end

function hubROILayer::getAttributeFormats, IDLFormatString=IDLFormatString
  if keyword_set(IDLFormatString) then begin
    result = list()
    foreach attributeFormat, self.attributeFormats do begin
      case attributeFormat[0] of
        'string'  : format = '(a)'
        'integer' : format = '(i0)'
        'decimal' : format = '(d0.'+attributeFormat[1]+')'
      endcase
      result.add, format
    endforeach
  endif else begin
    result = self.attributeFormats
  endelse
  return, result
end

function hubROILayer::getAttributeNulls
  result = list()
  foreach attributeFormat, self.attributeFormats do begin
    case attributeFormat[0] of
      'string'  : null = ' '
      'integer' : null = 0ll
      'decimal' : null = 0.0d
    endcase
    result.add, null
  endforeach
  return, result
end

function hubROILayer::getAttributeTypes
  nulls = self.getAttributeNulls()
  result = list()
  foreach null,nulls do begin
    result.add, size(/TYPE, null)
  endforeach
  return, result
end

function hubROILayer::getFormattedAttributeValue, value, attributeIndex
  format = (self.getAttributeFormats(/IDLFORMATSTRING))[attributeIndex]
  on_ioerror, errorLabel
  result = string(value, FORMAT=format)
  return, result
  
  errorLabel:
  return, '' 
end

pro hubROILayer::setAttributeValue, attributeIndex, geometryIndex, newValue
  attributeValues = (self.attributeTable)[attributeIndex]
  if strtrim(newValue,2) eq '' then begin
    value = !null
  endif else begin
    value = newValue
  endelse
  attributeValues[geometryIndex] = value
end

function hubROILayer::getAttributeValue, attributeIndex, geometryIndex
  attributeValues = (self.attributeTable)[attributeIndex]
  value = attributeValues[geometryIndex]
  return, value
end

pro hubROILayer::setGeometry, roiIndex, geometry
  (self.rois[roiIndex]).setGeometry, geometry
end

function hubROILayer::getGeometry, roiIndex
  return, (self.rois[roiIndex]).getGeometry()
end

function hubROILayer::getGeometries
  geometries = list()
  foreach roi, self.rois do begin
    geometries.add, roi.getGeometry()
  endforeach
  return, geometries
end

function hubROILayer::getGeometryDuplicates
  pixelCounts = hash()
  foreach roi, self.rois do begin
    geometry = roi.getGeometry()
    if isa(geometry) then begin
      pixelCounts[geometry] = (pixelCounts.hubGetValues(geometry, Default=0)).toArray()+1
    endif
  endforeach
  pixel =  (pixelCounts.keys()).toArray()
  counts = (pixelCounts.values()).toArray()
  pixelDuplicates = pixel[where(/NULL, counts gt 1)]
  return, pixelDuplicates
end

function hubROILayer::getColors
  colors = list()
  foreach roi, self.rois do begin
    colors.add, roi.getColor()
  endforeach
  return, colors
end

function hubROILayer::getPixel, roiIndex, pixelIndex
  roi = self.rois[roiIndex]
  geometry = roi.getGeometry()
  if isa(geometry) then begin
    pixel = geometry[pixelIndex mod n_elements(geometry)]
  endif else begin
    pixel = !null
  endelse
  return, pixel
end

pro hubROILayer::addPixel, roiIndex, pixel
  (self.rois[roiIndex]).addPixel, pixel
end

pro hubROILayer::removePixel, roiIndex, pixel
  (self.rois[roiIndex]).removePixel, pixel
end

pro hubROILayer::removePixelFromLayer, pixel
  foreach roi,self.rois do begin
    roi.removePixel, pixel
  endforeach
end

; only needed for SOIs
pro hubROILayer::decreaseIndices, indexStart
  foreach roi, self.rois do begin
    roi.decreaseIndices, indexStart
  endforeach
end

pro hubROILayer::deleteROI, roiIndex
  roi = (self.rois)[roiIndex]
  roi.delete
  self.rois.remove, roiIndex
  foreach attribute, self.attributeTable do begin
    attribute.remove, roiIndex
  endforeach
end

pro hubROILayer::emptyROI, roiIndex
  roi = (self.rois)[roiIndex]
  roi.empty
end

pro hubROILayer::addROI, geometry, attributes, color
  self.rois.add, hubROI(self, geometry, self.oModel, Color=color)
  attributes = isa(attributes) ? attributes : list(LENGTH=self.getNumberOfAttributes())
  foreach attribute, self.attributeTable, i do begin
    attribute.add, attributes[i]
  endforeach
end

function hubROILayer::getROIImageData, roiIndex, filename
  pixelPositions = self.getGeometry(roiIndex)
  profiles = hubIOImg_readProfiles(filename, pixelPositions)
  return,  profiles 
end

function hubROILayer::getROISizes
  sizes = list()
  foreach roi, self.rois do begin
    sizes.add, roi.getSize()
  endforeach
  return, sizes
end

pro hubROILayer::addAttribute, attributeName, attributeFormat
  self.attributeNames.add, attributeName
  self.attributeFormats.add, attributeFormat
  self.attributeTable.add, list(LENGTH=self.getNumberOfGeometries())
end

pro hubROILayer::editAttribute, attributeName, attributeFormat, attributeIndex
  (self.attributeNames)[attributeIndex] = attributeName
  (self.attributeFormats)[attributeIndex] = attributeFormat
end

pro hubROILayer::deleteAttribute, attributeIndex
  self.attributeNames.remove, attributeIndex
  self.attributeFormats.remove, attributeIndex
  self.attributeTable.remove, attributeIndex
end

pro hubROILayer::calculateAttribute,  targetIndex, inputIndices, operator
  targetAttribute = (self.attributeTable)[targetIndex]
  for i=0,self.getNumberOfGeometries()-1 do begin
    values = list()
    foreach inputIndex,inputIndices do begin
      value = ((self.attributeTable)[inputIndex])[i]
      if isa(value) then values.add, value
    endforeach
    if values.isEmpty() then begin
      targetValue = !null  
    endif else begin
      case operator of
        'first'  : targetValue = values[0]
        'last'   : targetValue = values[-1]
        'concat' : begin
          targetValue = ''
          foreach value,values.toArray(Type='string') do targetValue += value
        end
        'sum'    : targetValue = total(values.toArray(Type='double'))
        'mean'   : targetValue = mean(values.toArray(Type='double'))
        'min'    : targetValue = min(values.toArray(Type='double'))
        'max'    : targetValue = max(values.toArray(Type='double'))
        'range'  : targetValue = max(values.toArray(Type='double'))-min(values.toArray(Type='double'))
        'stdDev' : targetValue = stddev(values.toArray(Type='double'))
      endcase
    endelse
    targetAttribute[i] = targetValue
  endfor
end

pro hubROILayer::setROIColor, roiIndex, color
  roi = (self.rois)[roiIndex]
  roi.setColor, color
end

pro hubROILayer::showROIImageStatisticsReport, roiIndex, filename

  image = hubIOImgInputImage(filename)
  wavelength = image.getMeta('wavelength', Default=lindgen(image.getMeta('bands'))+1)
  profiles = hub_fix2d(self.getROIImageData(roiIndex, filename))
  
  if ~isa(profiles) then begin
    ok = dialog_message(/INFORMATION, 'ROI is empty.')
    return
  endif
  
  numberOfProfiles = n_elements(profiles[0,*])
  meanProfile = mean(profiles, DIMENSION=2)
  if numberOfProfiles gt 1 then begin
    stddevProfile = stddev(profiles,DIMENSION=2)
  endif
  maxProfile = max(profiles,DIMENSION=2)
  minProfile = min(profiles,DIMENSION=2)

  ROIMeanPlot = plot(wavelength, meanProfile $
    ,YTITLE='Value' $
    ,XTITLE='Wavelength' $
    ,NAME='Mean' $
    ,TITLE='ROI Statistics of Region #'+STRCOMPRESS(/REMOVE_ALL,roiIndex+1) $
;    +', '+STRCOMPRESS(/REMOVE_ALL,((self.getAttributeTable())[1])[roiIndex]) $
    +', '+STRCOMPRESS(/REMOVE_ALL,(self.getROISizes())[roiIndex])+' Points'+'!C'+file_basename(filename) $
    ,/BUFFER, XSTYLE=1, YSTYLE=1)
  if numberOfProfiles gt 1 then begin
    ROIStdDevPlusPlot = plot(wavelength, meanProfile+stddevProfile $
      ,NAME='+StdDev' $
      ,COLOR='green',/OVERPLOT)
    ROIStdDevMinusPlot = plot(wavelength, meanProfile-stddevProfile $
      ,NAME='-StdDev' $
      ,COLOR='green',/OVERPLOT)
    ROIMaxPlot = plot(wavelength, maxProfile $
      ,NAME='Max' $
      ,COLOR='red',/OVERPLOT)
    ROIMinPlot = plot(wavelength, minProfile $
      ,NAME='Min' $
      ,COLOR='red',/OVERPLOT)
    targets=[ROIMaxPlot,ROIStdDevPlusPlot,ROIMeanPlot,ROIStdDevMinusPlot,ROIMinPlot]
  endif else begin
    targets = [ROIMeanPlot]
  endelse
  leg = LEGEND(TARGET=targets, POSITION=[wavelength[-1],max(maxProfile)], /DATA)
  ROIStatsReport = hubReport(Title='ROI Statistics')
  imageContent = transpose(ROIMeanPlot.CopyWindow(BORDER=0), [1,2,0])
  ROIMeanPlot.Close
  ROIStatsReport.addImage, imageContent
 
  ROIStatsTable = hubReportTable()
  bandList = list()
  minList=list()
  maxList=list()
  meanList=list()
  stdList = list()
  for i=0, n_elements(meanProfile)-1 do begin
    bandList.add, 'Band '+ strcompress(/Remove_all,i+1)
    minList.add, minProfile[i]
    maxList.add, maxProfile[i]
    meanList.add, meanProfile[i]
    stdList.add, stddevProfile[i]
  endfor
  ROIStatsTable.addColumn, bandList,NAME='Statistics'
  ROIStatsTable.addColumn, minList,NAME='Min'
  ROIStatsTable.addColumn, maxList,NAME='Max'
  ROIStatsTable.addColumn, meanList,NAME='Mean'
  ROIStatsTable.addColumn, stdList,NAME='Stdev'
  ROIStatsReport.addHTML, ROIStatsTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
  ROIStatsReport.saveHTML,/SHOW

end

pro hubROILayer::print
  attributeTable = self.getAttributeTable(/Text)
  foreach roi, self.rois, i do begin
    geometry = roi.getGeometry()
    color = roi.getColor()
    print, 'ROI #'+strtrim(i+1, 2)
    print, 'Color:', color
    print, 'Attributes:', attributeTable[*,i]
    print, 'Pixels:', geometry
  endforeach
end

pro hubROILayer__define
  struct = {hubROILayer, inherits IDL_Object,$
    graphicInitialized:0b, rois:list(), oModel:obj_new(),  $
    layerSize:[0l,0l], attributeNames:list(), attributeFormats:list(), attributeTable:list()}
end

pro test_hubROILayer

  roiLayer = hubROILayer([300,300]); _getTestROILayer()
  
  ; visualize
  size = roiLayer.getLayerSize()
  tlb = widget_base()
  widget_control, tlb, /REALIZE
  windowID = widget_draw(tlb, XSIZE=size[0], YSIZE=size[1], GRAPHICS_LEVEL=2 )
  widget_control, windowID, GET_VALUE=oWindow
  
  oView = IDLgrView(VIEWPLANE_RECT=[0,0,size], DIMENSIONS=size, LOCATION=[0,0])
  roiLayer.initGraphic, oView
;  oView.add, roiLayer.getModel()
  oWindow.draw, oView

end

pro test_hubROILayer__showROIImageStatisticsReport
  reader = hubROILayerReader()
  roiLayer = reader.importRaster(hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample'))
  filename = hub_getTestImage('Hymap_Berlin-A_Image')
  roiLayer.showROIImageStatisticsReport, 0, filename
end