pro hubROILayerProcessor::checkLayerSizes, layer1, layer2
  compile_opt static, strictarr
  if product(layer1.getLayerSize() eq layer2.getLayerSize()) ne 1 then begin
    message, 'Layer sizes do not match.'
  endif
end

function hubROILayerProcessor::join, layer1, layer2
  compile_opt static, strictarr

  hubROILayerProcessor.checkLayerSizes, layer1, layer2
  geometries1 = layer1.getGeometries()
  geometries2 = layer2.getGeometries()
  geometries = geometries1 + geometries2
  colors = layer1.getColors() + layer2.getColors()
  attributeNames1 = layer1.getAttributeNames()
  attributeNames2 = layer2.getAttributeNames()
  attributeFormats1 = layer1.getAttributeFormats()
  attributeFormats2 = layer2.getAttributeFormats()
  attributeTable1 = layer1.getAttributeTable()
  attributeTable2 = layer2.getAttributeTable()
  attributeNames = list()
  attributeFormats = list()
  attributeTable = list()
  numberOfGeometries2 = geometries.count()

  ; copy first attribute table 
  for i1=0,layer1.getNumberOfAttributes()-1 do begin
    attributeName = attributeNames1[i1]
    attributeFormat = attributeFormats1[i1]
    ; if attribute is in both attribute tables merge values, else create !null values
    equalNames = attributeNames2.hubIsElement(attributeName, Index=i2)
    if equalNames then begin
      attributeFormat2 = attributeFormats2[i2]
      compatibleFormats = attributeFormat[0] eq attributeFormat2[0]
      if compatibleFormats && attributeFormat[0] eq 'decimal' then attributeFormat[1] = max([attributeFormat[1],attributeFormat2[1]])
    endif else begin
      compatibleFormats = 0b
    endelse
    if compatibleFormats then begin
      attributeNames2.remove, i2
      attributeFormats2.remove, i2
      attributeValues2 = attributeTable2.remove(i2)
    endif else begin
      attributeValues2 = list(LENGTH=layer2.getNumberOfGeometries())
    endelse
    attributeNames.add, attributeName
    attributeFormats.add, attributeFormat
    attributeTable.add, attributeTable1[i1]+attributeValues2
  endfor

  ; copy second attribute table
  for i2=0,layer2.getNumberOfAttributes()-1 do begin
    attributeNames.add, attributeNames2[i2]
    attributeFormats.add, attributeFormats2[i2]
    attributeValues1 = list(LENGTH=layer1.getNumberOfGeometries())
    attributeTable.add, attributeValues1 + attributeTable2[i2]
  endfor
  ; create new layer
  joinedLayer = hubROILayer(layer1.getLayerSize(), geometries, attributeNames, attributeFormats, attributeTable, Colors=colors)
  return, joinedLayer
end

function hubROILayerProcessor::dissolve, layer, targetAttributeIndex, operators
  compile_opt static, strictarr
  geometries = layer.getGeometries()
  attributes = layer.getAttributeTable()
  colors = layer.getColors()
  targetAttributeValues = attributes[targetAttributeIndex]
  dissolvedGeometries = hash()
  dissolvedColors = hash()
  dissolvedAttributes = hash()
  
  ; collect infos for the uniq rois
  foreach targetAttributeValue, targetAttributeValues, roiIndex do begin
    formattedValue = layer.getFormattedAttributeValue(targetAttributeValue, targetAttributeIndex)
    if ~dissolvedGeometries.hasKey(formattedValue) then begin
      dissolvedGeometries[formattedValue] = []
      dissolvedColors[formattedValue] =  colors[roiIndex]
      dissolvedAttributes[formattedValue] = list()
      foreach attribute,attributes do begin
        (dissolvedAttributes[formattedValue]).add, list()
      endforeach
    endif
    dissolvedGeometries[formattedValue] = [dissolvedGeometries[formattedValue], geometries[roiIndex]]
    foreach attribute,attributes,i do begin
      attributeValue = attribute[roiIndex]
      if isa(attributeValue) then begin
        ((dissolvedAttributes[formattedValue])[i]).add, attributeValue
      endif
    endforeach
  endforeach
  
  ; calculate new geometry and attribute values
  
  newGeometries = dissolvedGeometries.Values()
  foreach geometry,newGeometries,i do begin
    newGeometries[i] = hubmathHelper.getSet(geometry)
  endforeach
  newAttributes = list()
  foreach attribute,attributes do begin
    newAttributes.add, list()
  endforeach
  
  attributesVectors = dissolvedAttributes.hubGetValues(dissolvedGeometries.keys())
  foreach attributeVector,attributesVectors,roiIndex do begin
    foreach valueVector,attributeVector,attributeIndex do begin
      ; first operator
      if valueVector.isEmpty() then begin
        newValue = !null
      endif else begin
        newValue = valueVector[0]
      endelse
      (newAttributes[attributeIndex]).add, newValue
    endforeach
  endforeach
  newColors = dissolvedColors.hubGetValues(dissolvedGeometries.keys())

  ; sort everythink according to the dissolved target attribute values
  newTargetAttributeValues = newAttributes[targetAttributeIndex]
  strValues = newTargetAttributeValues.toArray(Type='string', Missing=' ')
  sortedIndices = sort(strValues)
  
  ;sort
  newGeometries = newGeometries[sortedIndices]
  newColors = newColors[sortedIndices]
  foreach newAttribute,newAttributes,i do begin
    newAttributes[i] = newAttribute[sortedIndices] 
  endforeach

  ; create new layer
  dissolvedLayer = hubROILayer(layer.getLayerSize(), newGeometries, layer.getAttributeNames(), layer.getAttributeFormats(), newAttributes, Colors=newColors)
  return, dissolvedLayer
end

pro hubROILayerProcessor__define
  struct = {hubROILayerProcessor, inherits IDL_Object}
end

pro test_hubROILayerProcessor
  reader = hubROILayerReader()
  layer1 = reader.importRaster(hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample'))
  layer2 = reader.importRaster(hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample'))
  ;layer2 = reader.importRaster(hub_getTestImage('Hymap_Berlin-A_Classification-Validation-Sample'))
  
  
  processor = hubROILayerProcessor()
  joinedLayer = processor.join(layer1, layer2)
  dissolvedLayer = processor.dissolve(joinedLayer, 0, operators)
  dissolvedLayer.print

end