pro hubExternal_writeJSONFile, parameters, inputFilename, outputFilename
  if ~isa(parameters, 'hash') then begin
    message, 'Wrong argument.'    
  endif
  openw, lun, inputFilename, width=9999, /GET_LUN
  printf, lun, json_serialize(parameters+hash('scriptOutputFilename', outputFilename))
  close, lun
end
