pro hubExternal_showErrorReport, spawnResult, spawnError, spawnCmd, Title=title
  if ~isa(title) then begin
    title = 'Script Output'
  endif
  report = hubReport(Title=title+' Script Output')
  if isa(spawnCmd) then begin
    report.addHeading, 'Commando'
    report.addMonospace, spawnCmd
  endif
  report.addHeading, 'Standard Output '
  report.addMonospace, spawnResult
  report.addHeading, 'Error Output'
  report.addMonospace, spawnError
  report.saveHTML, /Show
end
