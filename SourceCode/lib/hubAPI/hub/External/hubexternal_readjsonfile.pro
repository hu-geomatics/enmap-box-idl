function hubExternal_readJSONFile, outputFilename
 if file_test(outputFilename) then begin
    text = hubIOASCIIHelper.readFile(outputFilename)
    result = json_parse(text)
  endif else begin
    result = hash()
  endelse
  return, result
end

pro test_hubExternal_readJSONFile
  parameters = hash('x', 1)
  inputFilename = filepath(/TMP, 'hubR_input.txt')
  outputFilename = inputFilename
  hubExternal_writeJSONFile, parameters, inputFilename, outputFilename
  print, hubExternal_readJSONFile(outputFilename)
end