function hubTableColumn::init, name, dataType, formatString=formatString, values=values
  self.name = name
  
  ;explicit definition of column data type
  if isa(dataType) then begin
    self.dataType=dataType
  endif 
  
  self.values=list()
  if isa(values) then begin
    self.addValues, values
  endif
  
  return, 1
end

function hubTableColumn::_castToColumnDataType, variable
  if ~isa(variable) then return, !NULL
  if TYPENAME(variable) eq 'LIST' then begin
    n = N_ELEMENTS(variable)
    result = list(LENGTH=n)
    for i=0, n-1 do begin
      result[i] = self._castToColumnDataType(variable[i])
    endfor
    return, result
  endif else begin
    case self.dataType of
      0         : message, 'Column data type is undefined'
      1         : return, byte(variable)
      2         : return, fix(variable)
      3         : return, long(variable)
      4         : return, float(variable)
      5         : return, double(variable)
      6         : return, complex(variable)
      7         : return, string(variable)
      9         : return, dcomplex(variable)
      12        : return, uint(variable)
      13        : return, ulong(variable)
      14        : return, long64(variable)
      15        : return, ulong64(variable)
      else : return, variable
    endcase  
  endelse
  return, !NULL
end


pro hubTableColumn::setLength, length
  cnt = self.values.count()
  diff = length - cnt
  if diff lt 0 then begin
    ;remove values
    self.values.remove, indgen(abs(diff))+length
  endif
  
  if diff gt 0 then begin
    self.values.add, list(length=diff), /EXTRACT
  endif
end

function hubTableColumn::removeValue, rowIndex
  self.values[rowIndex] = !NULL
end

pro hubTableColumn::setValue, value, rowIndex
  self.values[rowIndex] = self._castToColumnDataType(value)  
end

function hubTableColumn::getValue, rowIndex
  return, self.values[rowIndex]
  
end


pro hubTableColumn::addValues, values
  if self.dataType eq 0 && n_elements(values) ne 0 then self.dataType = size(values[0], /TYPE)
  
  toAdd = self.dataType ne 0 ? self._castToColumnDataType(values) : values
  
  self.values.add, toAdd, EXTRACT=isa(values)
end

pro hubTableColumn::setValues, values
  
  if self.dataType eq 0 && n_elements(values) ne 0 then self.dataType = size(values[0], /TYPE)
  if self.dataType ne 0 then begin
    values = self._castToColumnDataType(values)
  endif
  
  self.values= TYPENAME(values) eq 'LIST' ? values : list(values)
end


function hubTableColumn::getValues, string=string, formatString=formatString, missing=missing
  l = list()
  foreach v, self.values do begin
    l.add, v
  endforeach
  
  if keyword_set(string) then begin
    format = isa(formatString) ? formatString : self.getDefaultFormatString()
    default = isa(missing) ? missing : ''
    for i=0, self.getCount()-1 do begin
        l[i] = isa(l[i]) ? string(format=format, l[i]) : default
    endfor
  endif
  
  return, l
end


function hubTableColumn::getName
  return, self.name
end

function hubTableColumn::getCount
  return, self.values.count()
end

function hubTableColumn::getDataType
  return, self.dataType
end


function hubTableColumn::getDefaultFormatString
  case self.dataType of
    1:  formatString = '(%"%i")'
    2:  formatString = '(%"%i")'
    3:  formatString = '(%"%i")'
    4 : formatString = '(%"%0.2f")'
    5 : formatString = '(%"%0.2f")'
    7 : formatString = '(%"%s")'
    12: formatString = '(%"%i")'
    13: formatString = '(%"%i")'
    14: formatString = '(%"%i")'
    15: formatString = '(%"%i")'
    else : formatString = !NULL
  endcase
  return, formatString
end

function hubTableColumn::_overloadPrint
  
  formatString = self.getDefaultFormatString()
  
  cnt = self.getCount()
  if cnt eq 0 then return, !NULL
  str = STRARR(cnt)
  for i=0, cnt-1 do begin
    str[i] = isa(self.values[i]) ? string(format=formatString, self.values[i]) : ''
  endfor
  return, strtrim(str,2)
end

pro hubTableColumn__define
  struct = {hubTableColumn $
    , inherits IDL_Object $
    , name : '' $
    , datatype: 0b $
    , values:list() $
  }
end

