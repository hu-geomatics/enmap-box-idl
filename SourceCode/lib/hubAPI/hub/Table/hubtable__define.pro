;+
; :Description:
;    Constructor to create a hubTable-Object.
;
;-
function hubTable::init
  
  self.columns = list()
  return, 1
end

;+
; :Description:
;    Adds a new column.
;
; :Params:
;    name: in, required, type=String
;     The column name
;     
;    dataType: in, optional
;     Datatype of this columns. If not set, the first value added to this column will specifiy the columns data type.
;
; :Keywords:
;    values: in, optional, type = list or array
;     The column values
;
;
;-
pro hubTable::addColumn, name, dataType, values=values
  
  names = self.getColumnNames()
  if isa(names) && total(STRCMP(name, names)) ne 0 then begin
    message, 'Column '+name+' already exists' 
  endif

  self.columns.add, hubTableColumn(name, dataType, formatString=formatString, values=values)
  if isa(values) || self.getRowCount() gt  0 then self._reformTable
end

;+
; :Description:
;    Adds a row 
;
; :Params:
;    values: in, required, type=list
;     A list of values for each column. 
;
;-
pro hubTable::addRow, values
  if n_elements(values) gt self.getColumnCount() then begin
    message, 'number of columns does not match'
  endif
  
  for i=0, n_elements(values)-1 do begin
    self.columns[i].addValues, values[i]
  endfor
end

;+
; :Description:
;    Returns the column names.
;-
function hubTable::getColumnNames
  names = list()
  foreach column, self.columns do begin
    names.add, column.getName()
  endforeach
  return, names.toArray()
end


;+
; :Description:
;    Returns true if a column with specified `name` exists.
;
; :Params:
;    name: in, required, type=string
;
;-
function hubTable::hasColumn, name
  foreach column, self.columns do begin
    if strcmp(column.getName(), name) then return, 1b
  endforeach
  return, 0b
end



;+
; :Description:
;    Returns the hubTableColumn object refered by `column`.
;
; :Params:
;    column: in, required, type=string
;
;-
function hubTable::getColumn, column
  if TYPENAME(column) eq 'STRING' then begin
    i = where(self.getColumnNames() eq column, /NULL)
    if ~isa(i) then message, 'Column '+column+' does not exist'
    i = i[0]
  endif else begin
    i = column
  endelse
  return, (self.columns)[i]
end


;+
; :Description:
;    Returns the number of columns.
;-
function hubTable::getColumnCount
  return, self.columns.count()
end

;+
; :Description:
;    Returns the number of rows.
;-
function hubTable::getRowCount
  if self.getColumnCount() eq 0 then return, 0 ;no columns, no rows
  return, self.columns[0].getCount()
end


;+
; :Description:
;    Use this routine to specify the values of a column.
;
; :Params:
;    column: in, required, type=string
;     The column name.
;    values: in, required, type=list or array
;     The column values.
;-
pro hubTable::setColumnValues, column, values
  c = self.getColumn(column)
  if c.getCount() eq 0  then begin
    c.addValues, values
  endif else begin
    c.setValues, values
  endelse
  self._reformTable
end

;+
; :Description:
;    Returns a list of values with `column`
;
; :Params:
;    column: in, required, type=string
;      The column name.
;-
function hubTable::getColumnValues, column
  c = self.getColumn(column)
  return, c.getValues()
end

;+
; :Description:
;    Sets the value of a single cell in this table.
;
; :Params:
;    column: in, required, type=string
;     The column name
;    row: in, required, type=int
;     The row index.
;    value: in, required
;     The cell value that is to set.
;
;
;-
pro hubTable::setValue, column, row, value
  c = self.getColumn(column)
  c.setValue, row, value
end



;+
; :Description:
;    Returns a single cell value.
;
; :Params:
;    column: in, required, type=string
;     The column name.
;     
;    row: in, required, type=int
;     The row index.
;
;
;-
function hubTable::getValue, column, row
  c = self.getColumn(column)
  return, c.getValue(row)
end



;+
; :Description:
;    Returns all values of a certain row.
;
; :Params:
;    rowIndex: in, required, type=int
;     The row index.
;
;-
function hubTable::getRowValues, rowIndex
  
  rowValues = list(LENGTH=self.getColumnCount())
  foreach column, self.columns, i do begin
    rowValues[i] = column.getValue(rowIndex)
  endforeach
  return, rowValues
  
end


;+
; :Hidden:
;-
function hubTable::_overloadPrint
  nRows = self.getRowCount()
  nCols = self.getColumnCount()
  if nRows eq 0 then return, !NULL
  M = make_array(nCols, nRows+1, /STRING, value='')
  foreach column, self.columns, i do begin
    values = column._overloadPrint()
    values = [column.getName(), values]
    n = max(strlen(values))
    M[i,*] = string(format='(%"%'+strtrim(n,2)+'s")', values)
  endforeach
  return, transpose(strjoin(M, ' '))
end

;+
; :hidden:
;-
pro hubTable::_reformTable
  nRowsMax = 0
  nColumns = self.getColumnCount()
  
  foreach column, self.columns do begin
    nRowsMax = nRowsMax > column.getCount()
  endforeach
  
  foreach column, self.columns do begin
    column.setLength, nRowsMax
  endforeach
end

;+
; :Hidden:
;-
pro hubTable__define
  struct = {hubTable $
    , inherits IDL_Object $
    , name : '' $
    , columns: list() $
    , hasValues:0b $
  }

end


;+
; :Hidden:
;-
pro test_hubTable 
  table = hubTable()
  table.addColumn, 'A'
  table.addColumn, 'B'
  
  ;add rows, type of column 
  table.addRow, list(999,'B')
  table.addRow, list(999,56)
  help, table.getValue('A', 0)
  help, table.getValue('A', 1)
  help, table.getValue('B', 0)
  help, table.getValue('B', 1)
  
  ;add columns,
  table.addColumn, 'C', 4 ;float column
  table.addColumn, 'D', 5 ;double column
  
  print, 'Row values:'
  for i=0, table.getRowCount()-1 do begin
    print, table.getRow(i)
  endfor
  
;  table = hubTable()
;  table.addColumn, 'A', 2
;  table.addColumn, 'B', 2
;  table.addColumn, 'S'
;  table.addRow, list(0.23, 24, 'blub')
;  table.addRow, list(0.34544, 25, 'foo   ')
;  table.addRow, list(787767.23, 26, 'bar')
  print, table
end

;+
; :Hidden:
;-
pro test2_hubTable
  table = hubTable()
  table.addColumn, 'A'
  table.addColumn, 'B'
  
  ;add rows, type of column
  table.addRow, list(999,'B')
  table.addRow, list(999,56)
   
  ;add columns,
  table.addColumn, 'C', 4 ;float column
  table.addColumn, 'D', 5 ;double column
  
  print, table
end