;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    Returns a subhash consisting of all values given by the `keys` argument.
;
; :Params:
;    keys : in, required, type=string[]
;      Use this argument to specify the hash keys to be copied. 
;      If a key is not defined, then its value is set to !null. 
;
;-
function hash::hubGetSubhash, keys

  subhash = hash()
  foreach key, keys do begin
    subhash[key] = self.hubGetValue(key[0])
  endforeach
  return, subhash
  
end


pro test_hash__hubGetSubhash

  origHash = hash()
  origHash['a'] = 1
  origHash['b'] = 2
  origHash['c'] = 3

  print, 'Hash: '
  print, origHash
  print
  
  subHash = origHash.hubGetSubhash([])
  print, 'Subhash:'
  print, subHash
end