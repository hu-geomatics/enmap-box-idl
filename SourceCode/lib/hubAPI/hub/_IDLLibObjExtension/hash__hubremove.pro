;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro hash::hubRemove, keys 
  foreach key, keys, i do begin
    if self.hasKey(key) then self.remove, key 
  endforeach
end