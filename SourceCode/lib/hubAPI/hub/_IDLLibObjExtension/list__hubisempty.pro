;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Reverse engineered version of IDL `list::IsEmpty` method (IDL 8.2). 
;-
function list::hubIsEmpty 
  return, n_elements(self) eq 0
end