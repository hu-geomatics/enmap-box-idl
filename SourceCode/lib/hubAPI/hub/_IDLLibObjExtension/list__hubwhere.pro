;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Reverse engineered version of IDL `list::Where` method (IDL 8.2). 
;-
function list::hubWhere, value $
  , Complement=complement, Count=count, Ncomplement=ncomplement 
  
  print ,'!!! list::hubWhere - This method is depricated and will get removed soon!'
  
  return, self.where(value,Complement=complement, Count=count, Ncomplement=ncomplement)
  
  

end

pro test_hubWhere
  l = list(1,2,3,4,'a')
  print, l.hubWhere('a')
  print, l.hubWhere('b')
   print, l.hubWhere(2) 
end