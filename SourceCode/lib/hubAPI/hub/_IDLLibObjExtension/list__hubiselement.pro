function list::hubIsElement, value, Index=index
  index = self.where(value)
  if isa(index) then begin
    index = n_elements(index) eq 1 ? index[0] : index
    return, 1b
  endif else begin
    return, 0b
  endelse
end
