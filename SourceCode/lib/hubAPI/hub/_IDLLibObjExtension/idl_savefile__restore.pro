;+
; :Description:
;    Returns a single save file variable 
;
; :Params:
;    varName: in, required, type=string
;      the name of the save file variable
; :Keywords:
;    NULL: in, optional, type=boolean, default = false
;     Set this to return !NULL instead of an error, in case the variable `varName` 
;     is not stored in the save file.
;
; :Author: geo_beja
;-
function IDL_Savefile::restore, varName, NULL=NULL
  
  if keyword_set(null) && ~total(stregex(self.names(), varName,/FOLD_CASE, /BOOLEAN)) then begin
    return, !NULL
  endif
  self.Restore, varName
  return, SCOPE_VARFETCH(varName)
end
