;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function hash::hubIsa, keys $
  , EvaluateXOR=evaluateXOR, EvaluateAND=evaluateAND, EvaluateOR=evaluateOR $
  , IndicesTrue=indicesTrue, IndicesFalse=indicesFalse

  keysSet = make_array(n_elements(keys), value=0, /BYTE)
  foreach key, keys, i do begin
    if self.hasKey(key) then keysSet[i] = isa(self[key]) 
  endforeach
  
  IndicesTrue = where(keysSet eq 1, /NULL, complement=indicesFalse)
  
  if keyword_set(EvaluateXOR) then keysSet = hubMathHelper.getXOR(keysSet)
  if keyword_set(EvaluateAND) then keysSet = hubMathHelper.getAND(keysSet)
  if keyword_set(EvaluateOR)  then keysSet = hubMathHelper.getOR(keysSet)
  return, keysSet
  
end