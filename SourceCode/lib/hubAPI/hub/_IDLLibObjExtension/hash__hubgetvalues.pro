;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    This method returns a list of hash value given by the keys argument.
;    If a given key is not defined, the corresponding value is set to !null.
;
; :Params:
;    keys : in, required, type=string[]
;
; :Keywords:
;    Default : in, optional, type=any, default=Null
;      Use this keyword to specify a default value for missing keys.
;-
function hash::hubGetValues, keys, Default=default
  values = list()
  if isa(keys) then begin
    foreach key,keys do begin
      values.add, self.hubGetValue(key, Default=default)
    endforeach
  endif
  return, values
end

pro test_hash__hubGetValues

  myHash = hash()
  myHash['a'] = 1
  myHash['b'] = 2

  print, myHash.hubGetValues(['a','c'])

end