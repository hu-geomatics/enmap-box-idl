;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function hash::hubHasKeyValue, key, values $
  , EvaluateXOR=evaluateXOR, EvaluateAND=evaluateAND, EvaluateOR=evaluateOR $
  , IndicesTrue=indicesTrue, IndicesFalse=indicesFalse

  if self.hubIsa(key) then begin
    result = self[key] eq values 
  endif else begin
    result = make_array(n_elements(values), /BYTE, values=0b)  
  endelse
  
  if keyword_set(EvaluateXOR) then result = hubMathHelper.getXOR(result)
  if keyword_set(EvaluateAND) then result = hubMathHelper.getAND(result)
  if keyword_set(EvaluateOR)  then result = hubMathHelper.getOR(result)
  
  return, result
end

pro test_hash_hubHasKeyValue
  H = Hash('A',[1,2])
  print, H.hubHasKeyValue('A',[1,2])
  print, H.hubHasKeyValue('A',[2,3])
  
  print, H.hubHasKeyValue('A',[1,2], /EvaluateOR)
  print, H.hubHasKeyValue('A',[2,3], /EvaluateOR)
  
  print, H.hubHasKeyValue('A',[[1,2],[2,3],[3,4]])
  print, H.hubHasKeyValue('A',[1,2,3], /EvaluateAND)
  ;print, H.hubHasKeyValue('A',2)
  ;print, H.hubHasKeyValue('C','whatever')
  
  ;because this does not work
  ;H = Hash('A',[1,2])
  ;print, H.HasKey('B') and H['B'] eq 1 
end