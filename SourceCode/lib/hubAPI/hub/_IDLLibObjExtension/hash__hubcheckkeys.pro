function hash::hubCheckKeys, keys, Missing=missing
  return, self.hubIsa(keys, /EvaluateAND, IndicesFalse=missing)
end

pro hash::hubCheckKeys, keys
  on_error, 2
  if ~self.hubCheckKeys(keys, Missing=missing) then begin
    message, 'Incomplete parameter hash. Missing values for keys: '+strjoin(keys[missing],', ')
  endif

end