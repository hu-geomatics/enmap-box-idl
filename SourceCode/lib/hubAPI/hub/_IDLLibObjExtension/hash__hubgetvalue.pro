;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    This method returns a single hash value given by the key argument.
;    If the given key is not defined, !null is returned.
;
; :Params:
;    key : in, required, type=string
;
; :Keywords:
;    Default:in, optional, default=!NULL
;      Use this keyword to specify a default value for missing keys.
;
;-
function hash::hubGetValue, key, Default=default

  if ~isa(key, /SCALAR) then begin
    message, 'Argument key must be a scalar.'
  endif
  
  value = self.hubIsa(key) ? self[key] : !NULL
  
  if ~isa(value) and isa(default) then value = default
  
  return, value

end

pro test_hash__hubGetValue

  myHash = hash()
  myHash['a'] = 1
  myHash['b'] = 2

  print, 'a: ', myHash.hubGetValue('a')
  print, 'c: ', myHash.hubGetValue('c')

end