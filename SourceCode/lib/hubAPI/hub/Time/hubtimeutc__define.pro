function hubTimeUTC::init, timestamps
  if ~isa(timestamps) then begin
    timestamps = timestamp()
  endif
  self.setTimestamps, timestamps
  return, 1b
end

pro hubTimeUTC::setTimestamps, timestamps
  ; convert to UTC
  timestamptovalues, timestamps, YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=second, OFFSET=offset
  timestampsUTC =    timestamp( YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=round(second), OFFSET=offset, /UTC)
  ; save UTC infos
  timestamptovalues, timestampsUTC, YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=second
  self.timestamps = ptr_new(timestampsUTC)
  julian  = julday(month, day, year, hour, minute, second)
  julian0 = julday(1, 1, year, 0, 0, 0)
  doy = long(julian-julian0)+1
  keys = ['julian','doy','year','month','day','hour','minute','second','sdoy','syear','smonth','sday','shour','sminute','ssecond']
  values = list(julian,doy,year,month,day,hour,minute,second,string(doy,format='(i3.3)'),string(year,format='(i4.4)'),string(month,format='(i2.2)'),string(day,format='(i2.2)'),string(hour,format='(i2.2)'),string(minute,format='(i2.2)'),string(second,format='(i2.2)'))
  self.info = dictionary(keys,values)
  self.info.count = n_elements(*self.timestamps)
end

pro hubTimeUTC::getProperty, YEAR=year,   MONTH=month,   DAY=day,   HOUR=hour,   MINUTE=minute,   SECOND=second,   DOY=doy, JULIAN=julian, STAMP=stamp,$
                             SYEAR=syear, SMONTH=smonth, SDAY=sday, SHOUR=shour, SMINUTE=sminute, SSECOND=ssecond, SDOY=sdoy, YDOY=ydoy, SYDOY=sydoy,$
                             DYEAR=dyear, SDYEAR=sdyear,$
                             Count=count

  if arg_present(julian) then  julian = self.info.julian
  if arg_present(year) then    year = self.info.year
  if arg_present(month) then   month = self.info.month
  if arg_present(day) then     day = self.info.day
  if arg_present(hour) then    hour = self.info.hour
  if arg_present(minute) then  minute = self.info.minute
  if arg_present(second) then  second = self.info.second
  if arg_present(doy) then     doy = self.info.doy
  if arg_present(dyear) or arg_present(sdyear) then begin
    ndays = julday(1, 1, self.info.year+1, 0, 0, 0) - julday(1, 1, self.info.year, 0, 0, 0)
    dyear = self.info.year + (self.info.doy-1)/(ndays-1)
    sdyear = strtrim(dyear,2)
  endif

  if arg_present(syear) then   syear = self.info.syear
  if arg_present(smonth) then  smonth = self.info.smonth
  if arg_present(sday) then    sday = self.info.sday
  if arg_present(shour) then   shour = self.info.shour
  if arg_present(sminute) then sminute = self.info.sminute
  if arg_present(ssecond) then ssecond = self.info.ssecond
  if arg_present(sdoy) then    sdoy = self.info.sdoy
  
  
  if arg_present(count) then   count = self.info.count
  if arg_present(stamp) then   stamp = (*self.timestamps)[*]

  if arg_present(ydoy) or arg_present(sydoy) then begin
    sydoy = self.info.syear+self.info.sdoy
    ydoy = long(sydoy) 
  endif
end

function hubTimeUTC::print, indices, Format=format
  if isa(format) then begin
    hubTimeParseFormat, format, placeholders, defaults, starts, lengths
    result = replicate(format, n_elements(*self.timestamps))
    keyMap = dictionary(['y','m','d','h','i','s'], ['year','month','day','hour','minute','second'])
    infoKeys = ((keyMap[starts.keys()]).values()).toArray()
    values = dictionary(starts.keys(), ((self.info)[infoKeys]).values())
    foreach placeholder, starts.keys() do begin
      for i=0,n_elements(result)-1 do begin
        resultI = result[i]
        strput, resultI, string((values[placeholder])[i], FORMAT='(i'+strtrim(lengths[placeholder],2)+'.'+strtrim(lengths[placeholder],2)+')'), starts[placeholder]
        result[i] = resultI
      endfor
    endforeach
  endif else begin
    result = *self.timestamps
  endelse
  if isa(indices) then begin
    result = result[indices]
  endif
  return, transpose(result[*])
end

pro hubTimeUTC::print, Format=format
  print, self.print(Format=format)
end

function hubTimeUTC::_overloadPrint
  return, self.print()
end

function hubTimeUTC::_overloadPlus, Left, Right
  if isa(left, 'hubTimeUTC')  then return, self.print()+right
  if isa(right, 'hubTimeUTC') then return, left+self.print()
end

function hubTimeUTC::wait, timespan, units
  julianDay = self.info.julian+hubTimeUTC.convertTimespan(timespan, units, 'days')
  caldat, julianDay, month, day, year, hour, minute, second
  timestampsUTC = timestamp(YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=round(second))  
  return, hubTimeUTC(timestampsUTC)
end

function hubTimeUTC::elapsed, units, Report=report
  now = hubTimeUTC()
  self.getProperty, Julian=julianSelf
  now.getProperty, Julian=julianNow
  elapsedDays = julianNow - julianSelf

  if ~isa(units) then begin
    elapsedSeconds = elapsedDays*60.*60.*24.
    case 1b of
      elapsedSeconds ge (n=60.*60*24)  : units = 'days'
      elapsedSeconds ge (n=60.*60)     : units = 'hours'
      elapsedSeconds ge (n=60.)        : units = 'minutes'
      else :                             units = 'seconds'
    endcase
  endif
  
  elapsed = hubTimeUTC.convertTimespan(elapsedDays, 'days', units)
  if keyword_set(report) then begin
    if units eq 'seconds' then begin
      return, string(elapsed, Format='(i0)')+' '+units
    endif else begin
      return, string(elapsed, Format='(f0.2)')+' '+units
    endelse
  endif else begin
    return, elapsed
  endelse
end

function hubTimeUTC::convertTimespan, timespan, fromUnits, toUnits
  compile_opt static
  
  ; convert from source unit to days
  case strlowcase(fromUnits) of
    'years'   : days = timespan*365d
    'months'  : days = timespan*30d
    'days'    : days = timespan*1d
    'hours'   : days = timespan/24d
    'minutes' : days = timespan/24d/60d
    'seconds' : days = timespan/24d/60d/60d
  endcase

  ; convert from days to target unit
  case strlowcase(toUnits) of
    'years'   : result = days/365d
    'months'  : result = days/30d
    'days'    : result = days*1d
    'hours'   : result = days*24d
    'minutes' : result = days*24d*60d
    'seconds' : result = days*24d*60d*60d
  endcase
  
  return, result
end

function hubTimeUTC::plus, timespans, units
  
  ; returns a matrix with n_element(timespans) x n_elements(this.timestamps)

  self.getProperty, Julian=julians
  timespanDays = hubTimeUTC.convertTimespan(timespans, units, 'days')
  
  ntimespans = n_elements(timespanDays)
  njulians = n_elements(julians)
  newJulians = rebin(transpose([julians]),ntimespans,njulians) + rebin([timespanDays],ntimespans,njulians)
  caldat, newJulians, month, day, year, hour, minute, second
  timestamps = timestamp(YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=second)
  times = hubTime(timestamps, /ISO)
  return, times
end

function hubTimeUTC::subset, first, last, Where=where
  if isa(first) and isa(last) then begin
    indices = where(/NULL, self.info.julian ge first.julian and self.info.julian le last.julian, count)
  endif else begin
    indices = lindgen(n_elements(*self.timestamps))
  endelse
  if keyword_set(where) then begin
    return, indices
  endif
  timestamps = (*self.timestamps)[indices]
  times = hubTime(timestamps, /ISO)
  return, times
end

function hubTimeUTC::locate, time, Distance=distance
  selfJulian = rebin(self.info.julian, self.info.count, time.count)
  targetJulian = rebin(transpose([time.julian]), self.info.count, time.count)
  distance = min(abs(selfJulian - targetJulian), DIMENSION=1, indices)
  indices = indices mod self.info.count
  return, indices
end

pro hubTimeUTC__define
  struct = {hubTimeUTC, inherits IDL_Object, timestamps:ptr_new(), info:dictionary()}
end

pro test_hubTimeUTC
  timestamps = ['2014-09-10T11:14:08.2123Z', '2014-09-10T13:14:08+02:00']
  time = hubTimeUTC(timestamps)

  time.print, FORMAT='yyyy-mm-dd'
  
  time = hubTime([2012001:2012010])
  print, time.locate(hubTime([2012001:2012002]))
end