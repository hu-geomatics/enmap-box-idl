function hubTimeSeries::init, timeStart, timeEnd, step, units
  self.timeStart = timeStart
  self.timeEnd = timeEnd
  timespan = timeEnd.julian - timeStart.julian
  times = timeStart.plus([0:timespan:8],units)
  !null = self.hubTimeUTC::init(times.print())
  return, 1b
end

pro hubTimeSeries__define
  struct = {hubTimeSeries, inherits hubTimeUTC, timeStart:obj_new(), timeEnd:obj_new()}
end

pro test_hubTimeSeries
  timeseries = hubTimeSeries(hubTime(2000001, /DOY), hubTime(2001365, /DOY), 8, 'days')
  print, timeseries
end