function hubTimeISO, timestamps
  return, hubTimeUTC(timestamps)
end

pro test_hubTimeISO
  timestamps = ['2014-09-10T11:14:08.2123Z', '2014-09-10T13:14:08+02:00']
  time = hubTimeISO(timestamps)
  time.print
end