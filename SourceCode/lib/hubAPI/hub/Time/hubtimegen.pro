function hubTimeGen, _extra=_extra
  julianDay = timegen(_extra=_extra)
  caldat, julianDay, month, day, year, hour, minute, second
  timestampsUTC = timestamp(YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=round(second))
  return, hubTimeUTC(timestampsUTC)
end

pro test_hubTimeGen
  times = hubTimeGen(UNITS="Days", HOURS=10, STEP_SIZE=8, START=julday(1,1-8,2000), FINAL=julday(1,365,2000))
  times.print
  print, times.sdoy
end