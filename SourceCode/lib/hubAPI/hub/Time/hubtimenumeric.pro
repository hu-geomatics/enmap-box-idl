function hubTimeNumeric, value

  dvalue = double(value)
  ipart = long64(dvalue)
  dpart = dvalue mod 1d

  svalue = strtrim(ipart,2)
  lengths = strlen(svalue)
  
  expandedValue = ipart*10ll^(14-lengths-(lengths mod 2))  ; tricky handling to consider DOY and MONTHDAY at the same time
  
  second = expandedValue mod 100 & expandedValue /= 100
  minute = expandedValue mod 100 & expandedValue /= 100
  hour   = expandedValue mod 100 & expandedValue /= 100
  year   = expandedValue / 10ll^(4-(lengths mod 2))        ; tricky handling to consider DOY and MONTHDAY at the same time

  ; convert DOY to MONTHYEAR 
  whereDoy = where(/NULL, lengths mod 2, COMPLEMENT=whereDay)
  if isa(whereDoy) then begin
    doy = expandedValue[whereDoy] mod 1000
    julianDay = julday(1, 1, year[whereDoy], 0, 0, 0)+doy-1
    caldat, julianDay, month_, day_, year_
    expandedValue[whereDoy] = year_*10000+month_*100+day_
  endif
  
  day = expandedValue mod 100 & expandedValue /= 100
  month = expandedValue  mod 100
  
  ; if month or day is not defined, set both to 1
  day += lengths le 6   ; yyyy or yyyymm format
  month += lengths le 4 ; yyyy format

  ; handle decimal part
  totalDays = list((julday(1, 1, year+1, 0, 0, 0)-julday(1, 1, year, 0, 0, 0)),$         ; days in current year
               (julday(month+1, 1, year, 0, 0, 0)-julday(month, 1, year, 0, 0, 0)),$     ; days in current month
               1d, 1d/24d, 1d/24d/60d, 1d/24d/60d/60d)                                   ; days in days, hour, minute, second 
  formatLengths = [4,6,8,10,12,14]
  fractionDays = dblarr(n_elements(value))
  foreach totalDaysI,totalDays,i do begin
    fractionDays += dpart*(totalDaysI) * (lengths eq (formatLengths[i] - (lengths mod 2)))
  endforeach
  julianDayIPart = julday(month, day, year, hour, minute, second)
  julianDay = julianDayIPart + fractionDays
  caldat, julianDay, month, day, year, hour, minute, second

  timestamp = timestamp(YEAR=year, MONTH=month, DAY=day, HOUR=hour, MINUTE=minute, SECOND=round(second<59))
  return, hubTimeUTC(timestamp)
end

;dyear = self.info.year + (self.info.doy-1)/(ndays-1)
    
pro test_hubTimeNumeric
  print, 'Years 2001-2005:        ', hubTimeNumeric([2001:2005], /Year)
  print, 'DOYs  16 day steps:     ', hubTimeNumeric([2014001:2014070:16], /Doy)
  print, 'Month 1-5 2014:         ', hubTimeNumeric([201401:201405], /Month)
  print, 'Days  7 day step        ', hubTimeNumeric([20140101:20140131:7], /Day)
  print, 'Decimal Year 0.25 steps:', hubTimeNumeric([2000:2001:0.25], /DecimalYear)
end
