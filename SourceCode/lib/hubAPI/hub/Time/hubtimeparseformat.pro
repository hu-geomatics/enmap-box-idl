pro hubTimeParseFormat, format, placeholders, defaults, starts, lengths

  placeholders = ['y','m','d','h','i','s']
  defaults = dictionary(placeholders, [1,1,1,0,0,0])

  ; parse format string
  starts = dictionary()
  lengths = dictionary()
  foreach placeholder,placeholders do begin
    start = strpos(format, placeholder)
    if start eq -1 then continue
    starts[placeholder]  = start
    lengths[placeholder] = strpos(format, placeholder, /REVERSE_SEARCH)-start+1
  endforeach
end