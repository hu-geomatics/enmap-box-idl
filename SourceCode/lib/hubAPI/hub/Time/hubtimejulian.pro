function hubTimeJulian, julian
  caldat, Julian, Month, Day, Year, Hour, Minute, Second
  timestamp = timestamp(DAY=day, HOUR=hour, MINUTE=minute, MONTH=month, SECOND=second, YEAR=year)
  return, hubTimeUTC(timestamp)
end

pro test_hubTimeJulian
  julian = systime(/JULIAN)
  time = hubTimeJulian(julian)
  time.print
end