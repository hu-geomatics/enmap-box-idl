;+
; :Description:
;    Returns UTC timestamps from formatted timestamps .
;
; :Params:
;    timestamps: in, required, type=string[]
;      Timestamps.
;      
;    format: in, required, type=string
;      Format string with fixed ranges for years yyyy, month mm, days dd, hours hh, minutes ii and seconds ss.
;      If 
;  
; :Examples:
; 
;    Parsing timestamps in yyyy-mm-dd format::
;    
;      timestamp = '2014-12-31' 
;      format = 'yyyy-mm-dd'
;      timeUTC = hubTimeGeneric(timestamp, format)
;      timeUTC.print
;      
;    IDL prints::
;    
;      2014-12-31T00:00:00Z
;
;
; :Author: Andreas Rabe
;-
function hubTimeGeneric, timestamps, format
  
  hubTimeParseFormat, format, placeholders, defaults, starts, lengths

  ; parse timestamps
  values = dictionary(placeholders, [list(),list(),list(),list(),list(),list()])
  foreach timestamp,timestamps,t do begin
    foreach placeholder,placeholders do begin
      if starts.hasKey(placeholder) then begin
        value = strmid(timestamp, starts[placeholder], lengths[placeholder])
        if value eq '' then value = defaults[placeholder]
      endif else begin
        value = defaults[placeholder]
      endelse
      (values[placeholder]).add, fix(value)
    endforeach
  endforeach
  
  ; special handling for 2-digit year format (yy instead of yyyy)
  if lengths.y eq 2 then begin
    values_y = values.y.toArray()
    indices2000 = where(/NULL, values_y lt 70, COMPLEMENT=indices1900)
    if isa(indices1900) then values_y[indices1900] += 1900
    if isa(indices2000) then values_y[indices2000] += 2000
    values.y = list(values_y, /EXTRACT)
  endif
  
  timestampsUTC = timestamp(YEAR=values.y.toArray(), MONTH=values.m.toArray(), DAY=values.d.toArray(), HOUR=values.h.toArray(), MINUTE=values.i.toArray(), SECOND=values.s.toArray())
  return, hubTimeUTC(timestampsUTC)
end

pro test_hubTimeGeneric
  format =      'yyyy-mm-dd hh-ii-ss'
  timestamps = ['2010-12-20 16-50-55',$
                '2001-11-03 01-32-12']

  format =      'yy-mm'
  timestamps = ['70','10']

  
  times = hubTimeGeneric(timestamps, format)
  times.print
  print, times.sdoy
end