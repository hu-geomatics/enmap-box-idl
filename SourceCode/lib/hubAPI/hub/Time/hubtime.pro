;+
; :Description:
;    This function can be used to initialize a hubTimeUTC object for managing date/time strings in ISO-8601 format.
;    Timestamps can be initialized with different inputs::
;    
;      a) ISO-8601 timestamps::
;      
;        timestamps = ['2014-09-10T11:14:08.2123Z', '2014-09-10T13:14:08+02:00']
;        time = hubTime(timestamps, /ISO)
;        time.print
;        
;      b) Standard form ASCII date strings::
;
;        timestamps = ['WED APR 03 04:04:04 2013', systime()]
;        time = hubTime(timestamps, /ASCII)
;        time.print
;        
;      c) Generic user-defined format strings::
;      
;        format = 'yyyy-mm-dd hh-ii-ss'
;        timestamps = ['2010-12-20 16-50-55', '2001-11-03 01-32-12']
;        time = hubTime(timestamps, Generic=format)
;        time.print
;
;      d) Numerically coded dates::
;      
;        print, 'Years 2001-2005:        ', hubTime([2001:2005], /Year)
;        print, 'DOYs  16 day steps:     ', hubTime([2014001:2014070:16], /Doy)
;        print, 'Month 1-5 2014:         ', hubTime([201401:201405], /Month)
;        print, 'Days  7 day step        ', hubTime([20140101:20140131:7], /Day)
;        print, 'Decimal Year 0.25 steps:', hubTime([2000:2001:0.25], /DecimalYear)
;        
;      e) Current system time::
;      
;        print, hubTime()
;        
;      The returned hubTimeUTC object can be used to query different informations, see `hubTimeUTC`.
;        
;        time = hubTime()
;        
;      
;        
;
; :Params:
;    timestamp
;      Format depends on the usage mode, see `ISO`, `ASCII` and `Generic` for details. 
;      In numeric mode (Year, Doy, Month, Day or DecimalYear keywords are used), 
;      the timestamp argument is a number or array of numbers.  
;
; :Keywords:
;    ISO: in, optional, type=boolean
;      See `hubTimeISO` for details.
;      
;    ASCII: in, optional, type=boolean
;      See `hubTimeASCII` for details.
;      
;    Generic: in, optional, type=format string
;      E.g. 'yyyy-mm-dd hh-ii-ss' or 'mm dd hh:ii:ss yyyy'.
;      See `hubTimeGeneric` for details.
;    
;    Year: in, optional, type=integer[]
;      Numerically coded years.
;      
;    Doy: in, optional, type=integer[]
;      Numerically coded days of yeas.
;      
;    Month: in, optional, type=integer[]
;      Numerically coded months.
;      
;    Day: in, optional, type=integer[]
;      Numerically coded days.
;      
;    DecimalYear: in, optional, type=integer[]
;      Numerically coded decimal years. E.g. 2014.5 mapped to 2014-07-02T12:00:00Z.
;
; :Author: Andreas Rabe
;-
function hubTime, timestamp, ISO=iso, Ascii=ascii, Generic=generic, $
  Year=year, Doy=doy, Month=month, Day=day, DecimalYear=decimalYear

  if ~isa(timestamp) then return, hubTimeUTC()
  if keyword_set(iso) then return, hubTimeUTC(timestamp)
  if keyword_set(ascii) then return, hubTimeASCII(timestamp)
  if isa(generic) then return, hubTimeGeneric(timestamp, generic)
  return, hubTimeNumeric(timestamp)
end

pro test_hubTime
  ; ISO-8601
  timestamps = ['2014-09-10T11:14:08.2123Z', '2014-09-11T13:14:08+02:00']
  time = hubTimeUTC(timestamps)
  time.print
stop
  ; ascii
  timestamps = ['WED APR 03 04:04:04 2013', systime()]
  time = hubTime(timestamps, /ASCIITimes)
  time.print
  
  ; generic
  format = 'yyyy-mm-dd hh-ii-ss'
  timestamps = ['2010-12-20 16-50-55', '2001-11-03 01-32-12']
  time = hubTime(timestamps, format)
  time.print

  ; queries
  time = hubTime()
  
  ; subset
  time = hubTime([2000001:2000365:10], /DOY)
  print, time.subset(hubTime(2000100,/Doy), hubTime(2000200,/Doy))


end