function hubTimeASCII, asciiTimes

  timestampsUTC = list()
  foreach asciiTime,asciiTimes do begin
    binDate = bin_date(asciiTime)
    timestampUTC = timestamp(YEAR=binDate[0], MONTH=binDate[1], DAY=binDate[2], HOUR=binDate[3], MINUTE=binDate[4], SECOND=round(binDate[5]))
    timestampsUTC.add, timestampUTC
  endforeach
  timestampsUTC = timestampsUTC.toArray()
  return, hubTimeUTC(timestampsUTC)
end

pro test_hubTimeASCII
  asciiTimes = [systime(0, randomu(seed, 1, /LONG)), systime(0, randomu(seed, 1, /LONG)), systime(0, randomu(seed, 1, /LONG))]
  times = hubTimeASCII(asciiTimes)
  times.print
  print, times.sdoy
end