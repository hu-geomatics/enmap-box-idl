;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro hubGUIHelper::setFixedWidgetFont

  if !VERSION.OS_FAMILY eq 'Windows' then begin
    widget_control,Default_font='ZAPFCHANCERY*14*PROOF*FIXED'
  endif

end

pro hubGUIHelper::centerTLB, tlb, SHIFT=shift
  COMPILE_OPT static
  scr_size = get_screen_size()
  geometry = widget_info(tlb,/GEOMETRY)
  xoffset = (scr_size[0]/2)-(geometry.scr_xsize/2)
  yoffset = (scr_size[1]/2)-(geometry.scr_ysize/2)
  if isa(shift) then begin
    xoffset += shift[0]
    yoffset += shift[1]
  endif
  widget_control, tlb, XOFFSET=xoffset, YOFFSET=yoffset

end

pro hubGUIHelper__define
  struct = {hubGUIHelper $
    ,inherits IDL_Object $
  }
end