;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-


pro test_hubGUIMenu_parseMenuFile

  ;menuFilename = 'D:\EnMAP-Box_resources\installer\IDL80\products\enmapBox\enmap.men'
    menuFilename = 'D:\EnMAP-Box_resources\source\prototyp\user.men'
  hubGUIMenu = hubGUIMenu()
  hubGUIMenu.parseMenuFile, menuFilename
  print, hubGUIMenu
 ; hubGUIMenu.printInfo
end

pro test_hubGUIMenu_extendMenu

  coreMenuFilename = 'D:\EnMAP-Box_resources\installer\IDL80\products\enmapBox\enmap.men'
  hubGUIMenu = hubGUIMenu()
  hubGUIMenu.parseMenuFile, coreMenuFilename
  
  userMenuFilename = 'D:\EnMAP-Box_resources\source\prototyp\user.men'
  hubGUIMenu.extendMenu, userMenuFilename
  
  mergedMenuFilename = 't:\menu.men'
  hubGUIMenu.writeMenuFile, mergedMenuFilename
  hubHelper.openFile, mergedMenuFilename

;print,hubGUIMenu
 ; hubGUIMenu.printInfo
end

pro test_hubGUIMenu_createMenuTree
  hubGUIMenu = hubGUIMenu()
  menuFilename = filepath('enmapMain.men', ROOT=enmapBox_getDirname(/ENMAPBOX), SUBDIR='resource')
  hubGUIMenu.extendMenu, menuFilename
  base = widget_base()
  tree = widget_tree(base)
  widget_control, base, /REALIZE
  hubGUIMenu.createMenuTree, tree, UVALUE=''
  
end

pro test_hubGUIMenu_createMenuBar
  hubGUIMenu = hubGUIMenu()
  menuFilename = filepath('enmapMain.men', ROOT=enmapBox_getDirname(/ENMAPBOX), SUBDIR='resource')
  hubGUIMenu.extendMenu, menuFilename
  base = widget_base(MBAR=menuBase)
  widget_control, base, /REALIZE
  hubGUIMenu.createMenuBar, menuBase, UVALUE=''

end