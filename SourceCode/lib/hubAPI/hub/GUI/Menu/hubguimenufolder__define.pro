;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-


function hubGUIMenuFolder::init $
  , level $
  , name $
  , parent $
  , Separator=separator $
  , Root=root
  
  self.children = list()
  if keyword_set(root) then begin
    self.type = 'root'
    self.level = -1
    self.name = 'hubGUIMenuRoot'
  endif else begin
    self.type = 'folder'
    !null = self->hubGUIMenuElement::init(level, name, parent, Separator=separator)
  endelse
  return, 1b

end

pro hubGUIMenuFolder::addChild $
  , child
  
  self.children.add, child
  
end

function hubGUIMenuFolder::getChildren
  return, self.children
end

function hubGUIMenuFolder::isFolder
  return, 1b
end

pro hubGUIMenuFolder::mergeFolder $
  , newFolder

  ; if folders have different names, than insert the new folder at the current menu level
  
  for iNew=0, n_elements(newFolder.getChildren())-1 do begin
    childNew = (newFolder.getChildren())[iNew]
;    print,childNew.getName()
;    stop
;    if ~childNew.isFolder() then continue

    merged =0b
    for iSelf=0, n_elements(self.children)-1 do begin
      childSelf = (self.children)[iSelf]
      if childSelf.isFolder() and strcmp(childSelf.getName(), childNew.getName()) then begin
        childSelf.mergeFolder, childNew
        merged = 1b
        break
      endif
    endfor

    if ~merged then begin 

      ; this folder is new, copy it to the current menu level
      self.addChild, childNew
    endif

  endfor
  
end

pro hubGUIMenuFolder::createMenuTree, root, _EXTRA=_extra
  if self.type eq 'root' then begin
    newRoot = root 
  endif else begin
    newRoot = widget_tree(root, VALUE=self.name, /FOLDER, UVALUE=_extra.uvalue)
  endelse
  
  for i=0,n_elements(self.children)-1 do begin
    child = (self.children)[i]
    child.createMenuTree, newRoot, _EXTRA=_extra
  endfor
end

pro hubGUIMenuFolder::createMenuBar, root, _EXTRA=_extra
  if self.type eq 'root' then begin
    newRoot = root 
  endif else begin
    newRoot = widget_button(root, VALUE=self.name, /MENU, SEPARATOR=self.separator, _EXTRA=_extra)
  endelse
  
  for i=0,n_elements(self.children)-1 do begin
    child = (self.children)[i]
    child.createMenuBar, newRoot, _EXTRA=_extra
  endfor
end

pro hubGUIMenuFolder::defineEnviMenu, buttonInfo, rootMenuName, refIndexHash

  isRoot = self.type eq 'root' 
  if isRoot then begin
    value = rootMenuName
    refValue = 'File'
    position = 'before'
    refIndexHash[rootMenuName] = 0
    refIndex = 0
  endif else begin
    value = self.name
    refValue = rootMenuName
    position = 'last'
    refIndexHash[value] = refIndexHash.hubGetValue(value, Default=-1)+1 
    refIndex = refIndexHash[refValue]
  endelse
  
  envi_define_menu_button, buttonInfo, VALUE=value, REF_VALUE=refValue, REF_INDEX=refIndex, /MENU, SIBLING=isRoot, POSITION=position, SEPARATOR=self.separator

  newRootMenuName = value
  for i=0,n_elements(self.children)-1 do begin
    child =  (self.children)[i]
    child.defineEnviMenu, buttonInfo, newRootMenuName, refIndexHash
  endfor
  
end

pro hubGUIMenuFolder::defineEnviToolbar, rootMenuName

  isRoot = self.type eq 'root'
  if isRoot then begin
    value = rootMenuName
    newRootMenuName = rootMenuName
  endif else begin
    value = self.name
    newRootMenuName = rootMenuName+'/'+value
  endelse

  for i=0,n_elements(self.children)-1 do begin
    child =  (self.children)[i]
    child.defineEnviToolbar, newRootMenuName
  endfor
end

function hubGUIMenuFolder::_overloadPrint

  if self.type eq 'root' then begin
    result = self.name
  endif else begin
    result = self->hubGUIMenuElement::_overloadPrint()
    result += self.separator ? '{separator}' : ''
  endelse

  for i=0,n_elements(self.children)-1 do begin
    child =  (self.children)[i] 
    result = [result, child._overloadPrint()]
  endfor

  return, result
  
end

pro hubGUIMenuFolder__define

  struct = {hubGUIMenuFolder $
    , inherits hubGUIMenuElement $
    , children : list() $
  }
  
end