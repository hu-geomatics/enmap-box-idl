;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function hubGUIMenu::init

  self.rootFolder = hubGUIMenuFolder(/Root)
  return, 1b

end

pro hubGUIMenu::addChild $
  , child
  
  self.rootFolder.addChild, child
  
end

pro hubGUIMenu::parseMenuFile $
  , filename

  ascii = (hubIOASCIIHelper()).readFile(filename)
  ;ascii = transpose(ascii[0:36])
  
  parentFolders = hash()
  parentFolders[-1] = self.rootFolder 
  
  foreach line,ascii,lineIndex do begin
    lineInfo = self.parseLine(line)
    if lineInfo.skipped then continue
    if lineInfo.error then begin
      message, 'Can not parse menu file, error occured at line '+strcompress(lineIndex+1)+' in file: '+filename
    endif

    ; get parentFolder
      parentFolder = parentFolders[lineInfo.level-1]

    ; create menu entry and update parentFolder hash
    if lineInfo.folder then begin
      menuFolder = hubGUIMenuFolder(lineInfo.level, lineInfo.name, parentFolder, Separator=lineInfo.separator)
      parentFolders[lineInfo.level] = menuFolder
    endif else begin
      !null = hubGUIMenuButton(lineInfo.level, lineInfo.name, lineInfo.handler, parentFolder, Separator=lineInfo.separator, Argument=lineInfo.argument)
    endelse
  endforeach
end

function hubGUIMenu::parseLine $
  , lineRaw

  ; init result structure
  result = { $
    error : 0b,$
    skipped : 0b,$
    folder : 0b,$
    level : 0,$
    name : '',$
    argument : '',$
    handler : '',$
    separator : 0b}

  ; delete leading white space
  line = strtrim(lineRaw, 2)
    
  ; skip comments and empty lines
  if strcmp(line,';',1) or strcmp(line,'') then begin
    result.skipped = 1b
    return, result
  endif

  ; split line into entries
  splittedLine = strsplit(line, '{', /EXTRACT)
  splittedLine = strtrim(splittedLine, 2)
  splittedLine = (strsplit(splittedLine, '}', /EXTRACT)).toArray(TYPE=7)
  splittedLine = strtrim(splittedLine, 2)
  numberOfSplits = n_elements(splittedLine)

  ; check for format errors
  if (numberOfSplits lt 2) or (numberOfSplits gt 5) then begin
    result.error = 1b
    return, result
  endif

  ; extract infos 
  result.level = splittedLine[0]
  result.name = splittedLine[1]
  case numberOfSplits of
    2 : begin
      result.folder = 1b
    end
    3 : begin
      result.folder = 1b
      result.separator = strcmp(splittedLine[2], 'separator', /FOLD_CASE)
    end
    4 : begin
      result.argument = splittedLine[2]
      result.handler = splittedLine[3]
    end
    5 : begin
      result.argument = splittedLine[2]
      result.handler = splittedLine[3]
      result.separator = strcmp(splittedLine[4], 'separator', /FOLD_CASE)
    end
  endcase
  
  return, result 
end

pro hubGUIMenu::extendMenu $
  , userMenuFilename
  
  userMenu = hubGUIMenu()
  userMenu.parseMenuFile, userMenuFilename
  self.rootFolder.mergeFolder, userMenu.getRootFolder()
  
end

pro hubGUIMenu::writeMenuFile $
  , filename
  
  text = self._overloadPrint()
  (hubIOASCIIHelper()).writeFile, filename, text[1:*]

end

pro hubGUIMenu::createMenuTree, root, _EXTRA=_extra
  self.rootFolder.createMenuTree, root, _EXTRA=_extra
end

pro hubGUIMenu::createMenuBar, root, _EXTRA=_extra
  self.rootFolder.createMenuBar, root, _EXTRA=_extra
end

pro hubGUIMenu::defineEnviMenu, buttonInfo, rootMenuName
  refIndexHash = hash()
  self.rootFolder.defineEnviMenu, buttonInfo, rootMenuName, refIndexHash
end

pro hubGUIMenu::defineEnviToolbar, rootMenuName
  self.rootFolder.defineEnviToolbar, rootMenuName
end

pro hubGUIMenu::defineEnmapMenu, buttonInfo, rootMenuName
  refIndexHash = hash()
  self.rootFolder.defineEnmapMenu, buttonInfo, rootMenuName, refIndexHash
end

function hubGUIMenu::_overloadPrint
  
  result = self.rootFolder._overloadPrint()
  return, transpose(result)
end

function hubGUIMenu::getRootFolder
  return, self.rootFolder
end

pro hubGUIMenu__define
  struct = {hubGUIMenu $
    , inherits IDL_Object $
    , rootFolder : obj_new() $
  }

end