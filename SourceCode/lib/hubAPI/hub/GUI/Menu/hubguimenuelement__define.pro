;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-


function hubGUIMenuElement::init $
  , level $
  , name $
  , parent $
  , Separator=separator
  
  self.level = level
  self.name = name
  self.separator = keyword_set(separator)
;  self.parent = parent
  parent.addChild, self
  return, 1b

end

function hubGUIMenuElement::getName
  return, self.name
end

function hubGUIMenuElement::_overloadPrint

  whiteSpace = strjoin(replicate(' ', ((self.level>0)+1)*3))
  result = whiteSpace+strcompress(self.level)+' {'+self.name+'} '
  return, result
  
end

pro hubGUIMenuElement__define
  struct = {hubGUIMenuElement $
    , inherits IDL_Object $
;    , parent : obj_new() $
    , type : '' $
    , level : 0l $
    , name : '' $
    , separator : 0b $ 
  }

end