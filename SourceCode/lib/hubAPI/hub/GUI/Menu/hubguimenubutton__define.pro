;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function hubGUIMenuButton::init $
  , level $
  , name $
  , handler $
  , parent $
  , Separator=separator $
  , Argument=argument
  
  self.type = 'button'
  self.handler = handler
  self.argument = isa(argument) ? argument : ''
  !null = self->hubGUIMenuElement::init(level, name, parent, Separator=separator)
  return, 1b

end

function hubGUIMenuButton::isFolder
  return, 0b
end

pro hubGUIMenuButton::createMenuTree, root, _EXTRA=_extra
  !null = widget_tree(root, VALUE=self.name, UNAME=self.handler, _EXTRA=_extra)
end

pro hubGUIMenuButton::createMenuBar, root, _EXTRA=_extra
  uvalue = hash()
  uvalue[0] = _extra.uvalue
  uvalue['name'] = self.name
  uvalue['argument'] = self.argument
  uvalue['handler'] = self.handler
  !null = widget_button(root, VALUE=self.name, UNAME=self.handler, SEPARATOR=self.separator, UVALUE=uvalue)
end

pro hubGUIMenuButton::defineEnviMenu, buttonInfo, rootMenuName, refIndexHash
  value = self.name
  refValue = rootMenuName
  refIndex = refIndexHash[refValue]
  envi_define_menu_button, buttonInfo, VALUE=value, REF_VALUE=refValue, REF_INDEX=refIndex, POSITION='last', UVALUE=self.argument ,EVENT_PRO=self.handler, SEPARATOR=self.separator
end

pro hubGUIMenuButton::defineEnviToolbar, rootMenuName
  forward_function envi
  envi = envi(/CURRENT)
  envi.AddExtension, self.name, self.handler, PATH=rootMenuName, UVALUE=self.argument, SEPARATOR=self.separator
  print,rootMenuName+'/'+self.name
end

function hubGUIMenuButton::_overloadPrint

  result = self->hubGUIMenuElement::_overloadPrint()
  result += '{'+self.argument+'} '
  result += '{'+self.handler+'} '
  result += self.separator ? '{separator}' : ''
  return, result
  
end

pro hubGUIMenuButton__define

  struct = {hubGUIMenuButton $
    , inherits hubGUIMenuElement $
    , handler : '' $
    , argument : '' $
  }
  
end