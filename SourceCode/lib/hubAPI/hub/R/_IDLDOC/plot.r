# import packages
require(rjson)

# read input parameters
jsonFile <- commandArgs(TRUE)
jsonString <- readLines(jsonFile)
p <- fromJSON(jsonString) # input parameters

# create plot and save to a tiff file
tiff(p$plotFilename, res=300, height=1500, width=1500) 
plot(x=p$x, y=p$y, type=p$type, col=p$col, main=p$main, sub=p$sub, xlab=p$xlab, ylab=p$ylab, pch=p$pch, lty=p$lty, lwd=p$lwd, cex=p$cex, xlim=p$xlim, ylim=p$ylim, bty=p$bty, xaxt=p$xaxt, font=p$font)
dev.off()

