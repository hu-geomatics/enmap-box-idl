;+
; :Description:
;    Use this function to run R code. Returns a result hash specified in R.
;
;    For usage details see
;    `Running R Scripts and Code <./_IDLDOC/rscripts>`
;
; :Params:
;    code: in, required, type=string[]
;      Specify R code.
;
;    parameters: in, optional, type=hash
;      See `hubR_runScript`.
;
;    spawnResult: out, optional, type=string[]
;      See `hubR_runScript`.
;
;    spawnError: out, optional, type=string[]
;      See `hubR_runScript`.
;
; :Keywords:
;    GroupLeader: in, optional, type=widgetID
;      See `hubR_runScript`.
;
;    Title: in, optional, type=widgetID, default='R Script Output'
;      See `hubR_runScript`.
;
;    NoReport: in, optional, type=boolean, default=0b
;      See `hubR_runScript`.
;
;    ShowScript: in, optional, type=boolean, default=0b
;      Use to open the script that is created in the background.
;
; :Author: Andreas
;-
function hubR_runCode, code, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, ShowScript=showScript

  script = list()
  script.add, [$
    '#import packages',$
    'require(rjson)',$
    '',$
    '# read input parameters',$
    'jsonFile <- commandArgs(TRUE)',$
    'jsonString <- readLines(jsonFile)',$
    'p <- fromJSON(jsonString) # p stands for input parameters',$
    'r <- list()               # r stands for result',$
    '',$
    '### user code ###',$
    ''], /Extract
  script.add, code, /Extract
  script.add, [$
    '',$
    '#################',$
    '',$
    'fileConnection<-file(p$scriptOutputFilename)',$
    'writeLines(toJSON(r), fileConnection)',$
    'close(fileConnection)'], /Extract
  scriptCode = script.toArray()
  scriptFilename = filepath(/TMP, 'script.r')
  hubIOASCIIHelper.writeFile, scriptFilename, scriptCode 
  if keyword_set(showScript) then begin
    xdisplayfile, TEXT=scriptCode, FONT='COURIER'
    result = scriptCode
  endif else begin
    result = hubR_runScript(scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport)
    file_delete, scriptFilename, /ALLOW_NONEXISTENT, /NOEXPAND_PATH, /QUIET 
  endelse

  return, result
end

;+
; :Description:
;    Behaves like `hubR_runCode` function, simply the result is skipped.
;
; :Author: Andreas Rabe
;-
pro hubR_runCode, code, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, ShowScript=showScript
  !null =  hubR_runCode(code, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport, ShowScript=showScript)
end

;+
; :hidden:
;-
pro test_hubR_runCode1
  code = ['print("Hello World")','print("Hello World")','r$number <- 123']
  parameters = hash()
  result = hubR_runCode(/SHOWSCRIPT,code, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport)
  print, result
end

;+
; :hidden:
;-
pro test_hubR_runCode2
  code = [$
    'tiff(p$plotFilename, res=300, height=1500, width=1500)',$
    'plot(x=p$x, y=p$y, type=p$type, col=p$col, main=p$main, sub=p$sub, xlab=p$xlab, ylab=p$ylab, pch=p$pch, lty=p$lty, lwd=p$lwd, cex=p$cex, xlim=p$xlim, ylim=p$ylim, bty=p$bty, xaxt=p$xaxt, font=p$font)',$
    'dev.off()']
  x=[-2*!PI : +2*!PI : 0.1]
  y = cos(x)*x
  plotFilename = filepath(/TMP, 'plot.tiff')
  parameters = hash('x', x, 'y', y, 'plotFilename', plotFilename)
  parameters['main'] = 'Plotting Example'
  parameters['col']  = 'blue'
  parameters['xlab'] = 'x'
  parameters['ylab'] = 'cos(x) x'
  parameters['type'] = 'l'
  !null = hubR_runCode(code, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport)
  hubHelper.openFile, plotFilename
end

;+
; :hidden:
;-
pro test_hubR_runCode3

  x=[-2*!PI : +2*!PI : 0.1]
  y = cos(x)*x
  plotFilename = filepath(/TMP, 'plot.tiff')
  parameters = hash('x', x, 'y', y, 'plotFilename', plotFilename)
  
  code = [$
    'tiff(p$plotFilename, res=300, height=1500, width=1500)',$
    'plot(x=p$x, y=p$y, type="l", col="blue", main="Plotting Example", xlab="x", ylab="cos(x) x")',$
    'dev.off()']
  
  !null = hubR_runCode(code, parameters, /NoReport)
  hubHelper.openFile, plotFilename
end

;+
; :hidden:
;-
pro test_hubR_runCode4
  a = 1
  b = 2
  code = 'r$c <- p$a+p$b'
  parameters = hash('a', a, 'b', b)
  result = hubR_runCode(code, parameters)
  print, strtrim(a,2)+'+'+strtrim(b,2)+' = ',strtrim(result['c'],2)
end

;+
; :hidden:
;-
pro test_hubR_runCode_exportImageData

  ; read sample set
  filenameFeatures = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  sample = hubIOImg_readSample(filenameFeatures, filenameLabels, OutNumberOfFeatures=numberOfFeatures, OutNumberOfSamples=numberOfSamples)+dictionary()
  help, sample
  
  ; draw 10 random samples without replacement
  subsample = dictionary()
  subsample.count = 10
  indices = hubRandom_sample(subsample.count, sample.count, Replace=0, /Sort)
  subsample.features = (sample.features)[*,indices]
  subsample.labels = fix((sample.labels)[indices])

  ; export to R
  code = ['sample = p$sample',$
          'sample']
  parameters = hash('sample', subsample)
  !null = hubR_runCode(code, parameters, spawnResult, spawnError, NoReport=noReport)

end