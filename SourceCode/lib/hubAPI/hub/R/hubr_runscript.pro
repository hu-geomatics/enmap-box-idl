;+
; :Description:
;    Use this function to run a R script. Returns a result hash specified in R.
;
;    For usage details also see
;    `Running R Scripts and Code <./_IDLDOC/rscripts>`
;
; :Params:
;    scriptFilename: in, required, type=string
;      Path to R script.
;
;    parameters: in, optional, type=hash
;      Specify a hash of input parameters that is imported into R.
;      Note that data is passed via ASCII file I/O, which can be a bottle kneck.
;
;    spawnResult: out, optional, type=string[]
;      Standard output produced by R.
;
;    spawnError: out, optional, type=string[]
;      Error output produced by R.
;
; :Keywords:
;    GroupLeader: in, optional, type=widgetID
;      Group leader for progress bar.
;
;    Title: in, optional, type=widgetID, default='R Script Output'
;      Report title.
;
;    NoReport: in, optional, type=boolean, default=0b
;      Set to suppress the HTML report that is showing the `spawnResult`  and `spawnError` information.
;
; :Author: Andreas Rabe
;-
function hubR_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport

  ; get Rscript executable
  case !VERSION.OS_FAMILY of
    'Windows' :   begin
      pathR = (hub_getSettings()).hubGetValue('dir_r')
      if ~isa(pathR) || ~file_test(pathR) then begin
        ok = dialog_message(['Path to R installation is not set correctly.', 'Use "dir_r = " to define it.'])
        hubSettings_editor, GroupLeader=groupLeader, Title=title
        message, 'huberror-ignore', /NONAME
      endif
    end
    'unix' : begin
      spawn, 'PATH=$PATH:/usr/bin; which Rscript', spawnResult
      if spawnResult[0] eq '' then begin
        ok = dialog_message("The program 'Rscript' is currently not installed.")
        message, 'huberror-ignore', /NONAME
      endif
    end
  endcase

  ; write inputs
  inputFilename = filepath(/TMP, 'hubR_input.txt')
  outputFilename = filepath(/TMP, 'hubR_output.txt')
  hubExternal_writeJSONFile, parameters, inputFilename, outputFilename

  ; run R script
  case !VERSION.OS_FAMILY of
    'Windows' : begin
      cmd = 'Rscript.exe "' + scriptFilename +'" "'+ inputFilename+'"'
      cd, filepath('bin', ROOT_DIR=pathR), CURRENT=currentDirectory
    end
    'unix' : begin
      cmd = 'PATH=$PATH:/usr/bin; unset LD_LIBRARY_PATH; Rscript '+scriptFilename +' '+ inputFilename
      cd, CURRENT=currentDirectory
    end
  endcase
  progressBar = hubProgressBar(Cancel=0, GroupLeader=groupLeader, Info='Running R Script: '+file_basename(scriptFilename), Title=title)
  spawn, cmd, spawnResult, spawnError
  obj_destroy, progressBar
  cd, currentDirectory

  ; show spawn output in HTML report
  if ~keyword_set(noReport) then begin
    hubR_showErrorReport, spawnResult, spawnError, Title=title
  endif

  ; read outputs
  result = hubExternal_readJSONFile(outputFilename)

  ; cleanup
  file_delete, inputFilename, outputFilename, /ALLOW_NONEXISTENT, /NOEXPAND_PATH, /QUIET

  return, result
end

;+
; :Description:
;    Behaves like `hubR_runScript` function, simply the result is skipped.
;
; :Author: Andreas Rabe
;-
pro hubR_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport
  !null = hubR_runScript(scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport)
end

;+
; :hidden:
;-
pro test_hubR_runScript
  scriptFilename = filepath('script.r', ROOT_DIR=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY=['TEMPLATE_R','_lib'])
  parameters = hash('inputText', 'Hello World from IDL')
  scriptResult = hubR_runScript(scriptFilename, parameters, NoReport=0)
  print, scriptResult['outputText']
end