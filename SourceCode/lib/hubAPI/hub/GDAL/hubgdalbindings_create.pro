pro hubGDALBindings_create
  
   dlm = mg_dlm(BASENAME='gdbBJ', prefix='bj_', description='IDL Bindings for GDAL', version='1.0', source='HUB')
   
   dirHdr = 'C:\OSGeo4W64\include'
   dlm.addInclude, 'gdal.h', HEADER_DIRECTORY= dirHdr
   
   dirLib = 'C:\OSGeo4W64\lib'
   dlm.addLibrary, 'gdal_i.lib', LIB_DIRECTORY=dirLib, static=0
   
   dirBHdr = 'D:\SVNCheckouts\EnMAP-Box\SourceCode\lib\hubAPI\hub\GDALBindings'
   dlm.addRoutinesFromHeaderFile, FILEPATH('bj_gdalbindings_hdr.h', root_dir=dirBHdr)
   
   dlm.write
   dlm.build
   
end