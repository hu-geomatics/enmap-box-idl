;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2014.
; 	
; :Description:
;   This wrapper makes use of the IDL `spawn` command to call the GDAL binaries of an local GDAL installation.
;   
;   See `http://www.gdal.org/gdal_utilities.html` for more details.
;   
;  :History:
;    
;    2015-05-07 : Search for GDAL binaries within PATH environmental variable. Removed hard-coded search routine. 
;    
;    2014 - automatic wrapper generation via `hubgdalwrapper_codegenerator_define.pro`
;     
;    year 2013 - first attempts to provide an IDL interface to the GDAL binaries
;-
;+
; :Description:
;    Constructor
;
; :Keywords:
;    dirGDAL: in, optional, type=string
;     Path of local directory where the GDAL binaries, e.g. gdalwarp.exe, are located.
;     
;    showCommands: in, optional, type=boolean
;     Set this to print the called GDAL command to the IDL command line.
;-
function hubGDALWrapper::init, dirGDAL=dirGDAL, showCommands=showCommands
  
  
  
  ;check enviroment variable GDAL_DATA
  _dirGDAL = isa(dirGDAL)? dirGDAL : self._searchGDALDirectory()
  if ~isa(_dirGDAL) or ~file_test(_dirGDAL, /DIRECTORY) then begin
    message, 'Directory with GDAL binaries is undefined. Please specify dirGDAL or set environmental variable PATH'
  endif
  self.dirGDAL = _dirGDAL
  
  self.dirGDAL_DATA = self.getGDAL_DATA() 

  
  
  self.ProjectionsTransformList = ptr_new()
  self.commands = Hash()
  
  ;check version
  versionRef  = '1.11.2'
  versionThis = stregex(self.version(), '[[:alnum:]]+\.[[:alnum:]]+\.[[:alnum:]]+', /EXTRACT)
  vnumRef  = fix(strjoin((versionRef.split('\.'))))
  vnumThis = fix(strjoin((versionThis.split('\.'))))
  if versionRef ne versionThis then begin
    info = list()
    info.add, 'Please note the version differences:'
    info.add, ' IDL code is based on GDAL version '+versionRef
    info.add, ' GDAL installation is GDAL version '+versionThis
    
    if vnumThis lt vnumRef then begin
      info.add, 'Your local GDAL Version is outdated'
    endif
    if vnumThis gt vnumRef then begin
      info.add, 'hubGDALWrapper-Source Code is older than your GDAL version.'
    endif    
    
    message, strjoin(info.toArray(), string(13b)),INFORMATIONAL=1 
  endif
  self.showCommands = keyword_set(showCommands)

  return, 1b
end

function hubGDALWrapper::array2string, array
  case TYPENAME(array) of
    'DOUBLE': result = strjoin(strtrim(string(array, format='(F)',2)), ' ')
    'FLOAT' : result = strjoin(strtrim(string(array, format='(F)',2)), ' ')
    else :result = strjoin(strtrim(array,2), ' ')  
  endcase
  return, result
end


function hubGDALWrapper::_runCommand, name $
  , arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10 $
  , skipCheck=SkipCheck $
  , spawnResult=spawnResult, spawnError=spawnError $
  , spawnCmd = spawnCmd $
  , passErrors=passErrors, _EXTRA=_EXTRA
  
  on_error, 2
  
  
  if ~(self.commands).hasKey(name) then begin
    
    message, 'GDAL application not found :'+name
  endif

  appInfo = (self.commands)[name]
  parInfos = appInfo['PARAMETERS']
  
  cmd = list(FILEPATH(appInfo['GDALNAME'], ROOT_DIR=self.dirGDAL))
  
  if isa(arg1) then cmd.add, self.array2string(arg1)
  if isa(arg2) then cmd.add, self.array2string(arg2)
  if isa(arg3) then cmd.add, self.array2string(arg3)
  if isa(arg4) then cmd.add, self.array2string(arg4)
  if isa(arg5) then cmd.add, self.array2string(arg5)
  if isa(arg6) then cmd.add, self.array2string(arg6)
  if isa(arg7) then cmd.add, self.array2string(arg7)
  if isa(arg8) then cmd.add, self.array2string(arg8)
  if isa(arg9) then cmd.add, self.array2string(arg9)
  if isa(arg10) then cmd.add, self.array2string(arg10)
  
  if isa(_EXTRA) then begin
    idlKeywords = STRLOWCASE(TAG_NAMES(_EXTRA))
    iGDALCommand = where(stregex(idlKeywords, 'gdalcmd', /FOLD_CASE, /BOOLEAN),/NULL)
    if isa(iGDALCommand) then begin
      cmd = list(cmd[0])
      cmd.add, _EXTRA.(iGDALCommand)
    endif else begin
    
      idlNames = list()
      i  = list()
      foreach parInfo, parInfos do begin
        foreach idlKW, idlKeywords, iTag do begin
          if STRCMP(parInfo.idlName, idlKW, /FOLD_CASE) then begin
            values = _extra.(iTag)
            
            case TYPENAME(values) of
              'DOUBLE': values = string(values, format='(F)')
              'FLOAT': values = string(values, format='(F)')
              'BYTE': values = strtrim(fix(values),2)
              else : values = strtrim(values,2)
            endcase
            values = strjoin(values,' ')
            cmdStr = parInfo.gdalName
            if ~parInfo.isbool then begin
              values = self.array2string(values)
              space = stregex(values, '^=', /BOOLEAN) ? '' : ' ' 
              cmdStr += space + values
              
            endif
            cmd.add, cmdStr
          endif
            
        endforeach
      endforeach
    endelse
  endif
  
  cmd  = strjoin(cmd.toArray(), ' ')
  if self.showCommands then print, cmd
  
  
  case STRUPCASE(!VERSION.OS_FAMILY) of
    'WINDOWS' : SPAWN,  cmd, spawnResult, spawnError, count=count, /HIDE, exit_status=exit_status, /NOSHELL
    'UNIX'    : SPAWN,  cmd, spawnResult, spawnError, count=count, exit_status=exit_status
    else : message, 'Unsupportet OS: ' + !VERSION.OS_FAMILY
  endcase
  
  if exit_status ne 0 || max(strlen(spawnError)) gt 0 then begin
    msg = list('GDAL Exit Status '+strtrim(exit_status,2))
    msg.add  , 'Command: '+cmd
    if spawnError[0] ne '' then begin
      msg.add, 'Error:   '
      msg.add, spawnError, /Extract
    endif
    if spawnResult[0] ne '' then begin
      msg.add, 'Result:   '
      msg.add, spawnResult, /Extract
    endif
    
    isInformational = stregex(spawnResult[0], '^Warning', /BOOLEAN) $
                   || stregex(spawnerror[0], '^Warning', /BOOLEAN) $
                   || stregex(spawnError[0], 'ERROR 6: WriteBlock', /BOOLEAN) $
                   || keyword_set(passErrors)
                  
                   
    msg = strjoin(msg.toArray(type='STRING'), string(13b))
    if isInformational then begin
      spawnError = ''
      print, msg
    endif else begin
      message, msg
    endelse
  endif

  if ~keyword_set(passErrors) then begin
    if max(strlen(spawnError)) gt 0 then begin
      message, strjoin(spawnError, string(10b))
    endif
    if total(stregex(spawnResult, '\(FATAL\)', /BOOLEAN)) then begin

      message, strjoin(spawnResult, string(10b))

    endif
  endif

  return, spawnResult
end

;+
; :Description:
;    Returns a list of possible GDAL Datatypes.
;-
function hubGDALWrapper::GDALDataTypes
  return, ['Byte','Int16','Int32','Float32','Float64','UInt16','UInt32']
end

;+
; :Description:
;    Returns information on the used GDAL version.
;
;-
function hubGDALWrapper::version
  return, self.GDALINFO(/version)
end


;+
; :Description:
;    Returns all licences that are provided with the GDAL.
;-
function hubGDALWrapper::license
  return, self.GDALINFO(/license)
end

;+
; :Description:
;    List all raster formats supported by this GDAL build (read-only and read-write) and exit. The format support is indicated as follows: 'ro' is read-only driver; 'rw' is read or write (ie. supports CreateCopy); 'rw+' is read, write and update (ie. supports Create). A 'v' is appended for formats supporting virtual IO (/vsimem, /vsigzip, /vsizip, etc). A 's' is appended for formats supporting subdatasets. Note: The valid formats for the output of gdalwarp are formats that support the Create() method (marked as rw+), not just the CreateCopy() method. 
;
;
;
; :Keywords:
;    STRUCTURE: in, optional
;     Set this to return a structure
;     
;    WRITE: in, optional, type=boolean
;     Set this to return writable formats only
;     
;    READ: in, optional, type=boolean
;     Set this to return readable formats only.
;     
;    UPDATE: in, optional, type=boolean
;     Set this to return formats that can get updated only.
;-
function hubGDALWrapper::formats, STRUCTURE=STRUCTURE, WRITE=WRITE, READ=READ, UPDATE=UPDATE
  result = self.GDALINFO(/formats)
  
  if keyword_set(STRUCTURE) then begin
    ;restructure the results
    result = result[where(stregex(result, 'Supported Formats', /BOOLEAN, /FOLD_CASE) ne 1, n, /NULL)]
    r = replicate({shortName:'' $
                  , support:{read:0b, write:0b, virtualIO:0b,update:0b,subdatasets:0b} $
                  , longName:'' $
                  },n)
                  
    for i=0, n-1 do begin
      entry = result[i]
      parts = strsplit(entry, ':', /EXTRACT, /REGEX)
      r[i].shortName = strtrim(stregex(parts[0], '^[^(]+', /EXTRACT),2)
      support    = strtrim(stregex(parts[0], '[^(]+$', /EXTRACT),2)
      r[i].support.read   = stregex(support, 'r', /Boolean)
      r[i].support.write  = stregex(support, 'w', /Boolean)
      r[i].support.update = stregex(support, '[+]', /Boolean)
      r[i].support.virtualIO   = stregex(support, 'v', /Boolean)
      r[i].support.subdatasets = stregex(support, 's', /Boolean)
      r[i].longName = strtrim(parts[1],2)
    endfor
    
    if keyword_set(update) then r = r[where(r.support.update eq 1)]
    if keyword_set(write) then r = r[where(r.support.write eq 1)]
    if keyword_set(read) then r = r[where(r.support.read eq 1)]
    
    result = r
  endif
  
  return, result
end

;+
; :Description:
;    List detailed information about a single format driver. The format should be the short name reported in the --formats list, such as GTiff.
;
; :Params:
;    format: in, required, type=string
;     Short driver/raster image format name you want to get information on. 
;-
function hubGDALWrapper::format, format
  commandList = list(self._getGDALBinaryPath('GDALINFO'))
  commandList.add, '--format '+strtrim(format,2)
  return, self._callGDAL(commandList)
end



;+
; :Description:
;    Control what debugging messages are emitted. A value of ON will enable all debug messages. A value of OFF will disable all debug messages. Another value will select only debug messages containing that string in the debug prefix code. 
;
; :Params:
;    value: in, required, type=string
;     
;     Can be `ON` or `OFF`
;
;
;-
pro hubGDALWrapper::debug, value
  self._runCommand, 'gdalinfo', debug=strtrim(value,2)
end

;+
; :Description:
;    Returns a general help as displayed by the GDAL binaries.
;-
function hubGDALWrapper::help_general
  return, self._runCommand('gdalinfo', /help_general)
end




;+
; :Hidden:
; :Description:
;   Adds GDAL command line parameters to a command list.
;-
pro hubGDALWrapper::_addParameters, commandList $
  , extraStruct=extraStruct $
  , nonBoolParameters=nonBoolParameters $
  , repeatableArgs=repeatableArgs $
  , lut=lut
  if ~isa(lut) then lut= hash()
  if ~isa(extraStruct) then return
  pNonBool  = isa(nonBoolParameters) ? list(STRLOWCASE(nonBoolParameters), /EXTRACT)     : list()
  pMulti    = isa(repeatableArgs) ? list(STRLOWCASE(repeatableArgs), /EXTRACT) : list()
  
  tagnames = tag_names(extraStruct)
  foreach tagname, tagnames, i do begin
    parName = STRLOWCASE(tagname)
    ;remove leading underscores
    len = strlen(stregex(parName, '^[_]+', /EXTRACT))
    parName = strmid(parName, len)
   
    if lut.haskey(parName) then parName = lut[parName]
    parValues = extraStruct.(i)
    
    ;non-boolean attribute
    match = pNonBool.where(parName)
    if isa(match) then begin
      space = stregex(parName, '=$', /BOOLEAN)? '': ' '
      cmd = string(format='(%"-%s%s%s")', parName, space, strjoin(strtrim(parValues,2), ' '))
      commandList.add, cmd
      continue
    endif
    
    ;non-boolean attribute that can be called multiple times, each with an extra keyword aka -par 'NAME=Value'
    match = pMulti.where(parName)
    if isa(match) then begin
      if TYPENAME(parValues) ne 'HASH' then begin
        foreach value, parValues do begin
          commandList.add, string(format='(%"-%s %s")', parName, strtrim(value,2))
        endforeach
        
      endif else begin
        foreach parKey, parValues.keys() do begin
          parValue = parValues[parKey]
          if TYPENAME(parValue) ne 'STRING' &&  parValue eq 0 then continue
          
          commandList.add, string(format='(%"-%s \"%s=%s\"")' $
            , parName, strtrim(parKey,2),strtrim(parValue,2))
          
        endforeach
      endelse
      
      continue
    endif
    
    ;boolean attribute
    commandList.add, '-'+parName
  endforeach
  
end



function hubGDALWrapper::getGDAL_DATA
  dir = getenv('GDAL_DATA')
  if ~FILE_TEST(dir, /DIRECTORY) then begin
    hubAPISettings =  hubSettings_getSettings()
    if hubAPISettings.hubIn('dir_gdal_data') then begin
      dir = hubAPISettings['dir_gdal_data']
      SETENV, 'GDAL_DATA='+dir
    endif
    dir = getenv('GDAL_DATA')
    if ~FILE_TEST(dir, /DIRECTORY) then message, 'Environmental variable GDAL_DATA is undefined'
  endif
  return, dir
end

function hubGDALWrapper::_searchGDALDirectory
  searchDirs = getenv('PATH')
  
  case STRUPCASE(!VERSION.OS_FAMILY) of
    'WINDOWS' : begin
      searchDirs = searchDirs.split(';')
      refBin = 'gdalinfo.exe'
    end
    'UNIX'    : begin
      searchDirs = searchDirs.split(':')
      refBin = 'gdalinfo'
    end
    else : message, 'Unsupportet OS: ' + !VERSION.OS_FAMILY
  endcase


  ;check for global/local hubAPI settings.
  hubAPISettings =  hubSettings_getSettings()
  if hubAPISettings.hasKey('dir_gdal_bin') then begin
    searchDirs = [hubAPISettings['dir_gdal_bin'], searchDirs]
  endif
  
  foreach dir, searchDirs do begin
    if FILE_TEST(FILEPATH(refBin, ROOT_DIR=dir)) then begin
      print, 'Use gdal binaries in directory: '+dir
      return, dir
    endif
  endforeach
  message, 'Unable to locate directory with GDAL Binaries. Please specify dirGDAL when initializing the hubGDALWrapper.'
end


function hubGDALWrapper::getSpatialReferenceSystems
  if ~isa(self.SRSDefinitions) then begin
    gdal_data = self.dirGDAL_DATA
    SRSTypes = ['pcs', 'gcs']
    
    SRSInfoCollection = Dictionary()
    foreach SRSType, SRSTypes do begin
      
      columns = !NULL
      files = FILE_SEARCH(self.dirGDAL_DATA, SRSType+'*', /FOLD_CASE, count=cnt)
      if cnt eq 0 then CONTINUE
      
      ;ensure pcs.csv comming before pcs.override.csv
      files = files[sort(strlen(files))]
      
      srsLines = list()

      foreach file, files do begin
        if ~FILE_TEST(file) then CONTINUE
        nLines = FILE_LINES(file)
        lineData = make_array(nLines, /STRING)
        OPENR, lun, file, /GET_LUN
        READF, lun, lineData
        FREE_LUN, lun, /FORCE
        
        if ~isa(columns) then begin
          columns = strsplit(lineData[0], '[",]',/REGEX, /EXTRACT)
        endif 
        if ~product(strcmp(columns, strsplit(lineData[0], '[",]',/REGEX, /EXTRACT), /FOLD_CASE)) then message, 'Wrong header lines'
        
          
        lineData = lineData[where(stregex(lineData, '^[[:digit:]]+,.*', /BOOLEAN), /NULL)]
        if ~isa(lineData) then continue
        srsLines.add, strsplit(lineData, ',', /PRESERVE_NULL, /EXTRACT), /EXTRACT
        ;PRINT, lineData[1058]
        
      endforeach
      
      
      ;correct for wrong splitted lines
      for i = 0, n_elements(srsLines)-1 do begin
        srsLine = srsLines[i]
        if~strcmp(srsLine[1], '"', 1) then begin
          srsLine[1] = '"'+srsLine[1]+'"'
          srsLines[i] = srsLine 
        endif
        
        if stregex(srsLine[1], '[^"]$', /BOOLEAN) then begin
          iEnd = where(stregex(srsLine,'"$', /BOOLEAN))
          srsLines[i] = [srsLine[0], strjoin(srsLine[1:iEnd], ','), srsLine[iEnd+1:*]]
        endif

      endfor
      
      refData = list()
      foreach column, columns do begin
        case 1b of
          stregex(column, 'CODE', /BOOLEAN, /FOLD_CASE) : refData.add, 0ul
          stregex(column, 'NAME', /BOOLEAN, /FOLD_CASE) : refData.add, ''
          stregex(column, 'SHOW', /BOOLEAN, /FOLD_CASE) : refData.add, 0b
          stregex(column, 'VALUE', /BOOLEAN, /FOLD_CASE) : refData.add, 0d
          else : refData.add, 0d
        endcase
      endforeach
      
      tmp = Hash(columns, refData, /EXTRACT)
      tmp['EPSG_CODE'] = ''
      tmp = tmp.toStruct()
      SRSInfo = replicate(tmp, n_elements(srsLines))
      ;SRSInfo = Dictionary()
      srsLines = srsLines.toArray(/NO_COPY, /TRANSPOSE)
      tagNames = TAG_NAMES(tmp)
      foreach column, columns, iColumn do begin
        iTag = (where(strcmp(tagNames, column, /FOLD_CASE)))[0]
        
        columnData = srsLines[iColumn,*]
        
        case TYPENAME(refData[iColumn]) of
          'ULONG' : SRSInfo[*].(iTag) = ulong(columnData[*])
          'STRING': SRSInfo[*].(iTag) = string(columnData[*])
          'DOUBLE': SRSInfo[*].(iTag) = double(columnData[*])
          'BYTE'  : SRSInfo[*].(iTag) = reform(byte(columnData[*]))
          else:stop
        endcase
        
        
      endforeach
      
      ;set EPSG prefix
      SRSInfo[*].EPSG_CODE = 'EPSG:'+strtrim(SRSInfo[*].COORD_REF_SYS_CODE,2) 
      SRSInfoCollection[srstype] = SRSInfo
    endforeach
    
    self.SRSDefinitions = SRSInfoCollection
  endif
  
  return, self.SRSDefinitions

end

;+
; :Description:
;    Print info 
;-
function hubGDALWrapper::_overloadPrint
  info = list()
  info.add, 'hubGDALWrapper'
  info.add, '  GDAL Version: '+self.version()
  info.add, '  Path Utility Programs: '+self.dirGDAL
  info.add, '  Path GDAL_DATA: ' + self.dirGDAL_DATA 
  return, strjoin(info.toArray(), string(13b))
end

;+
; :Description:
;    Converts a pixel position into a spatial reference coordinate. 
;
; :Params:
;    pathImage: in, required, type=string
;     Path of georeferences image.
;     
;    iX: in, required, type=string
;     Pixel position X.
;     
;    iY: in, required, type=string
;     Pixel position Y.
;
;
;-
function hubGDALWrapper::pixel2Coordinate, pathImage, iX, iY
  info = self.gdalInfo(pathImage,STRUCTURE=1 )
  
  pxSize = info[where(stregex(info, '^Pixel Size = ', /BOOLEAN))]
  pxSize = double((STRSPLIT(pxSize, '(,)', /EXTRACT))[1:*])
  ul = info[where(stregex(info, '^Upper Left[ ]+\(  ', /BOOLEAN))]
  ul = double((STRSPLIT(ul, '(,)', /EXTRACT))[1:2])
  
  return, [ul[0] + pxSize[0]*iX $; X coordinate value
          ,ul[1] + pxSize[1]*iY $; Y coordinate value
          ];
end



;+
; :Description:
;    Converts a coordinate into the pixel position of a certain image. 
;
; :Params:
;    pathImage: in, required, type=string
;     Path of georeferences image.
;      
;    cX: in, required, type=double
;     Coordinate X value.
;     
;    cY: in, required, type=double
;     Coordinate Y value.
;
;
;-
function hubGDALWrapper::coordinate2pixel, pathImage, cX, cY
  info = self.gdalInfo(pathImage,STRUCTURE=1 )
  
  pxSize = info[where(stregex(info, '^Pixel Size = ', /BOOLEAN))]
  pxSize = double((STRSPLIT(pxSize, '(,)', /EXTRACT))[1:*])
  ul = info[where(stregex(info, '^Upper Left[ ]+\(  ', /BOOLEAN))]
  ul = double((STRSPLIT(ul, '(,)', /EXTRACT))[1:2])
  
  return, round([(cX - ul[0]) / pxSize[0] $ ; X pixel index
                ,(cY - ul[1]) / pxSize[1] $ ; Y pixel index
                ])
end


;+
; :Hidden:
;-
pro hubGDALWrapper__define
  struct = {hubGDALWrapper $
    , inherits IDL_Object $
    , dirGDAL : '' $
    , dirGDAL_DATA :'' $
    , SRSDefinitions : obj_new() $
    , ProjectionsTransformList : ptr_new() $
    , showCommands : 0b $
    , commands:Hash() $
  }
end

pro example_hubGDALWrapper_warpImages
  
  pathSrc = 'X:\LandsatData\L8\LC82270652014207LGN00\LC82270652014207LGN00_TOA.bsq'
  pathOut = 'T:\temp\LC82270652014207LGN00_TOA_SAD69.bsq'
  gdal = hubGDALWrapper(SHOWCOMMANDS=1)
  
  ;target spatial reference system = South America Albers Equal Area Conic
  ;http://spatialreference.org/ref/esri/south-america-albers-equal-area-conic/
  t_srs = 'EPSG:102033'
  
  ;call gdalwarp -> http://www.gdal.org/gdalwarp.html
  gdal.gdalwarp, pathSrc, pathOut $
        , OF_ = 'ENVI'    $ ;output format = ENVI bsq
        , t_srs = t_srs   $ ;target spatial reference system
        , r1 ='cubicspline' ;resampling method: cubic spline
        
  print, 'Done!'
end

pro test_hubGDALWrapper
  ;specify directory with GDAL utility programms (e.g. gdalwarp.exe)
  ;gdalDir  = 'C:\Program Files (x86)\Quantum GIS Lisboa\bin'
  
  pathSrc  = 'D:\temp\Compositing\pbcInput_Untransformed\008060_861003_TestProject_SR.bsq'
  pathDst = 'D:\temp\Compositing\Transformations\testGDAL.bsq'
  
  GDALWrapper = hubGDALWrapper(/SHOWCOMMANDS)
  SRSLists = GDALWrapper.getSpatialReferenceSystems()
  
  
  pathIn = (hub_getTestImage())[0]
  ;GDAL INFO TEST
  info = GDALWrapper.gdalinfo(pathIn)
  
  hubAMW_program,TITLE='Select your SRS' 
  hubAMW_treelist, 'index', List=input[0:1,*], Title='My TreeList', Value=1, XSIZE=500, YSIZE=300, Extract=extract, /ShowFilter
  result = hubAMW_manage()
  if result['accept'] then begin
    srS = input[*,result['index']]
  endif
  stop
  pathIn = 'H:\bj_temp\HDFExtract\TestResults\lndcal.LE72260662010101EDC00_band_1_2_3_4_5_6'
  pathOut= 'C:\Users\geo_beja\Documents\temp\testGDALWarpRCP'

  ;GDAL INFO TEST
  info = GDALWrapper.gdalinfo(pathIn) 
  
  
;  ;GDAL WARP TEST
  GDALWrapper.gdalwarp, pathIn, pathOut $
           , t_srs='EPSG:102033', OF='GTiff' $
           , tr=[30,30], r='near'
;  
  stop
  ;print, GDALWrapper.gdalSRSInfo('EPSG:32717')
  ;testCMD  = '/Library/Frameworks/GDAL.framework/Versions/1.10/Programs/gdaltransform -s_srs EPSG:28992 -t_srs EPSG:31370'
  
  ;print, GDALWrapper.gdaltransform(s_srs='EPSG:28992', t_srs='EPSG:31370')
  
  ;GDALWrapper.gdal_proximity, enmapBox_getTestImage('AF_LC'), pathDst, values=[1,3]
  
  ;print, GDALWrapper.gdal_config() ;UNIX Systems only!
  ;print, GDALWrapper.generic(/VERSION)
  ;print, GDALWrapper.version()
  formats = GDALWrapper.formats(/STRUCTURE)
  ;print writable formats
  print, 'Writable formats:'
  foreach i, where(formats.support.write eq 1) do begin
    print, formats[i].shortName,  formats[i].longName
  endforeach
  print, GDALWrapper.formats(/STRUCTURE)
  ;print, GDALWrapper.gdalSRSInfo('EPSG:29101')
  print, GDALWrapper.gdalSRSInfo('EPSG:5530') ; 
  print, GDALWrapper.gdalSRSInfo('EPSG:5880') ; 
  print, GDALWrapper.gdalSRSInfo('EPSG:31370')
  ;print, GDALWrapper.gdalSRSInfo(enmapBox_getTestImage('AF_Image'))
  return
  ;warp the image
  ;see http://www.gdal.org/gdalwarp.html for GDAL parameter details 
  tic
  GDALWrapper.gdalWarp, pathSrc, pathDst, /OVERWRITE $
                      , T_SRS = 'EPSG:32717' $ ;output CRS -> UTM 17 South
                      , OF='ENVI'            $ ;write as ENVI Data
                      , dstnodata = -9999    $ ;output no data value 
                      , tr = [30.,30.]       $ ;output pixel resolution
                      , order = 1            $ ;oder of polynomial used for warping
                      , r='near'               ;nearest neighbour resampling
                      
  print, 'required time:', toc()
  
  ;compare coordinate systems
  print, 'Input  CRS:', GDALWrapper.gdalSRSInfo(pathSrc, o='wkt_simple')
  print, 'Output CRS:', GDALWrapper.gdalSRSInfo(pathDst, o='wkt_simple')
  
  print, 'Tests done!'
end

