;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2015.
;-


pro hubGDALTools::gdal_translate, pathSrc, pathDst, drvDst
  compile_opt static, strictarr
  hubpython.initPythonBridge, /raise_errors
  raster_formats = hubGDALTools.raster_formats()
  hubAssert, raster_formats.hasKey(drvDst), 'Can not load image driver '+drvDst
  void = Python.Import('gdal')
  ;hubApp_example_gdal
  Python.drvDst = drvDst
  Python.pathSrc = pathSrc
  Python.pathDst = pathDst

  ;gdt = Python.Import('hubGDALTools')
  print, 'Run python code...'
  Python.run, 'drv = gdal.GetDriverByName(drvdst)'
  Python.run, 'dsSrc = gdal.Open(pathsrc)
  Python.run, 'dsDst = drv.CreateCopy(pathdst, dsSrc)'
  ;Python.run, 'if dsDst is None: return None'
  Python.run, 'dsDst.SetProjection(dsSrc.GetProjection())
  Python.run, 'dsDst.SetGeoTransform(dsSrc.GetGeoTransform())
  Python.run, 'dsDst.FlushCache()
  Python.run, 'dsDst = None
  print, 'Image conversion done.'
  
end




function hubGDALTools::raster_formats, can_read=can_read, can_write=can_write, can_copy=can_copy
  compile_opt static, strictarr
  
  ;if ~isa(can_read) then can_read=0b
  ;if ~isa(can_write) then can_write=0b
  ;if ~isa(can_copy) then can_copy=0b
  
  
  ;default set of drivers. should be available in every GDAL version
  drvs = ORDEREDHASH()
  drvs['ENVI'] = ['img','ENVI .hdr Labelled Raster (.img)']
  drvs['GTiff'] = ['tif','TIFF / BigTIFF / GeoTIFF (.tif)']
  drvs['HFA']  = ['img','Erdas Imagine (.img)']
  drvs['VRT'] = ['vrt','GDAL Virtual (.vrt)']
  
  if hubPython.initPythonBridge() then begin
    drvs = ORDEREDHASH()
    Python.Run,'from osgeo import gdal'
    Python.Run,'import numpy as np'
    Python.Run, 'drivers = [gdal.GetDriver(i) for i in range(gdal.GetDriverCount())]'
    n_drv = Python.Run('len(drivers)')
    
    void = Python.Run('snames = [d.ShortName for d in drivers]')
    void = Python.Run('lnames = [d.LongName for d in drivers]')
    void = Python.Run("exts = [d.GetMetadataItem('DMD_EXTENSION') for d in drivers]")
    void = Python.Run("exts = [e or '' for e in exts]")
    void = Python.Run("can_read = [d.GetMetadataItem('DCAP_OPEN') == 'YES' for d in drivers]")
    void = Python.Run("can_write = [d.GetMetadataItem('DCAP_CREATE') == 'YES' for d in drivers]")
    void = Python.Run("can_copy = [d.GetMetadataItem('DCAP_CREATECOPY') == 'YES' for d in drivers]")
    
    snames = Python.snames
    lnames = Python.lnames
    extensions = Python.exts
     
    drv_can_read = Python.can_read
    drv_can_write = Python.can_write
    drv_can_copy = Python.can_copy
    
    mask = MAKE_ARRAY(n_drv, /BOOLEAN, VALUE=1b)
    if keyword_set(can_read) then mask *= drv_can_read EQ 1b
    if keyword_set(can_write) then mask *= drv_can_write EQ 1b
    if keyword_set(can_copy) then mask *= drv_can_copy eq 1b
    
    selection = where(mask)
    
    foreach i, selection do begin
      sn = snames[i]
      drvs[sn] = [extensions[i], lnames[i]]
    endforeach
    
  endif
  return, drvs
end

pro hubGDALTools::vector2raster, srcShp, dstImg, refImg=refImg,te=te, tr=tr, ts=ts, a=a $
  , nodata=nodata, ot=ot, l $
  , overwrite=overwrite
  
  compile_opt static, strictarr
  
  gdal = hubGDALWrapper(SHOWCOMMANDS=1)
  if isa(refImg) then begin
    gridInfo = gdal.gdalinfo(refImg)
    
    if ~isa(te) then begin
      gridTE = gridInfo[where(stregex(gridInfo, '^(Upper|Lower) (Left|Right)', /BOOLEAN))]
      gridTE = stregex(gridTE, '\([^(]+\)',/EXTRACT)
      gridTE = double((STRSPLIT(gridTE, '(,)', /EXTRACT)).toArray())
      xmin = min(gridTE[*,0], MAX=xmax)
      ymin = min(gridTE[*,1], MAX=ymax)
      te = [xmin, ymin, xmax, ymax]
    endif
    if ~isa(tr) then begin
      gridTR = gridInfo[where(stregex(gridInfo, '^Pixel Size = ', /BOOLEAN))]
      tr = double((STRSPLIT(gridTR, '(,)', /EXTRACT))[1:*])
    endif 
  endif
  
  if file_test(dstImg) then begin
    if keyword_set(overwrite) then begin
      gdal.gdalmanage,'delete', dstImg
    endif else begin
      te = !NULL
      ;tr = !NULL
      ;ts = !NULL
      of_ = !NULL
    endelse
  endif else begin
    if ~(isa(ts) or isa(tr)) then message, 'Please specify either ts (target size in samples and lines) or tr (resolution).
    if ~isa(of_) then of_ = 'ENVI'
  endelse
  
  FILE_MKDIR, FILE_DIRNAME(dstImg)
;  -te xmin ymin xmax ymax :
;  (GDAL >= 1.8.0) set georeferenced extents. The values must be expressed in georeferenced units. If not specified, the extent of the output file will be the extent of the vector layers.
;  -tr xres yres :
;  (GDAL >= 1.8.0) set target resolution. The values must be expressed in georeferenced units. Both must be positive values.
;  -tap:
;  (GDAL >= 1.8.0) (target aligned pixels) align the coordinates of the extent of the output file to the values of the -tr, such that the aligned extent includes the minimum extent.
;  -ts width height:
;  (GDAL >= 1.8.0) set output file size in pixels and lines. Note that -ts cannot be used with -tr
  
  if ~isa(a) then a = 'fid'
  if ~isa(l) then l = FILE_BASENAME(srcShp, '.shp',FOLD_CASE=1)
  gdal.gdal_rasterize, srcShp, dstImg, te=te, ts=ts, OF_=of_, tr=tr,l=l, A1=a, a_nodata=nodata, OT=OT
end

pro hubGDALTools::GPSPhotos2Shapefile, inPictures=inPictures, outShp=outShp $
  , verbose=verbose, pathEXIFTool=pathEXIFTool
  compile_opt static, strictarr
  
  isVerbose = keyword_set(verbose)
  
  gdal = hubGDALWrapper(SHOWCOMMANDS=0)
  
  fail = where(~FILE_TEST(inPictures), /NULL, nFail)
  if isa(fail) then begin
    message, 'Failed to open '+strtrim(nFail,2)+' files:' $
      + strjoin(inPictures[fail[0,10 < (nFail-1)]],',')+'...'
  endif
  
  ;read exif data 
  i=0
  n = N_ELEMENTS(inPictures)
  print, 'Read EXIF data of '+strtrim(n,2)+' images...'
  exifInfos = hash()
  requiredTags = ['DateTime','GPS'+['Longitude','Latitude','LongitudeRef','LatitudeRef']]
  
  ;if possible, use the EXIF Tools 
  if isa(pathEXIFTool) then begin
    lMax = 2048
    ;split file names into portions
    cmds = list()
    cmd = !NULL
    for i=0, N_ELEMENTS(inpictures)-1 do begin
      if ~isa(cmd) then begin
        ;cmd = string(format='(%"\"%s\" -c \"\%.6f degrees\" %s")', pathEXIFTool, dir)
        cmd = string(format='(%"\"%s\" -c \"\%.6f degrees\" ")', pathEXIFTool)
      endif
      if strlen(cmd) + strlen(inPictures[i]) +3 lt lMax then begin
        cmd += '"'+inPictures[i] + '" '
      endif else begin
        cmds.add, cmd
        cmd = !NULL
      endelse
    endfor
    
    missing = list()
    foreach cmd, cmds do begin
      
      if isVerbose then print, cmd
      spawn, cmd, results, error,NOSHELL=1 
      
      if error ne ''  then message, error
      
      iStart = where(stregex(results, '^===', /BOOLEAN), /NULL)
      
      foreach i0, iStart, i do begin
        ie =  i lt N_ELEMENTS(iStart)-1? iStart[i+1]-1 : n_elements(results)-1
        exif = results[i0+1:ie]
        path = strmid(stregex(results[i0], '= .+$', /EXTRACT),2)
        for j=0, N_ELEMENTS(exif)-1 do begin
          tagName = stregex(exif[j], '^[^:]+:', /EXTRACT)
          tagDef = strtrim(strmid(exif[j], strlen(tagName)),2)
          tagName = strjoin(strsplit(tagname, '(/:) ', /EXTRACT),'')
          exif[j] = tagName+'='+tagDef 
        endfor
        
        missing.remove, /ALL
        foreach tag, requiredTags do begin
          if ~total(stregex(exif, '^'+tag, /BOOLEAN)) then missing.add, tag
        endforeach
        if N_ELEMENTS(missing) gt 0 then begin
          print, format='(%"File %s misses EXIF tags: %s")', path, strjoin(missing.toArray(),',')
        endif else begin
          exifInfos[path] = exif
        endelse
      endforeach
      
    endforeach
    
    
  endif else begin
    missing = list()
    foreach inImg, inPictures do begin
      if isVerbose then print, format='(%"read %s (%5.2f\%)")',inImg, 100. * i++/n
      info = gdal.gdalinfo(inImg, mdd='EXIF', /NORAR, /NOCT, /NOGCP)
      exif = strtrim(info[where(stregex(info, 'EXIF_', /BOOLEAN), /NULL)],2)
      if isa(exif) then begin
        exif = strmid(exif, 5)
        
        missing.remove, /ALL
        foreach tag, requiredTags do begin
          if ~total(stregex(exif, '^'+tag, /BOOLEAN)) then missing.add, tag
        endforeach
        if N_ELEMENTS(missing) gt 0 then begin
          print, format='(%"File %s misses EXIF tags: %s")', FILE_BASENAME(imgName), strjoin(missing.toArray(),',')
        endif else begin
          exifInfos[inImg] = exif
        endelse
        
      endif
    endforeach
  endelse
  ;
  
  ;write shapefile
  srs = gdal.gdalsrsinfo('EPSG:4326', /WKT_ESRI)
  srs = srs[where(stregex(srs, 'OGC WKT :', /BOOLEAN),/NULL)+1:*]
  srs = strjoin(strtrim(srs,2),'')
  
  FILE_MKDIR, FILE_DIRNAME(outShp)
  
  shp = hubIOShapeFileWriter(outShp,'POINT', SPATIALREFERENCESYSTEM=srs)
  shp.addAttribute, 'name', 'string', 255
  shp.addAttribute, 'date', 'string', 255
  shp.addAttribute, 'time', 'string', 255
  shp.addAttribute, 'date_gps', 'string', 255
  shp.addAttribute, 'time_gps', 'string', 255
  shp.addAttribute, 'lat', 5, 14,PRECISION=10
  shp.addAttribute, 'lon', 5, 14,PRECISION= 10
  shp.addAttribute, 'alt', 5, 14,PRECISION= 10
  shp.addAttribute, 'dir', 5, 14,PRECISION= 10
  shp.addAttribute, 'path', 'string', 255
  
  regexDate = '[0-9]{4}:[0-9]{2}:[0-9]{2}'
  regexTime = '[0-9]{2}:[0-9]{2}:[0-9]{2}'
  regexFloat ='[-+]?[0-9]*\.?[0-9]+.' 
  regexFloat2 ='[-+]?[0-9]+\.[0-9]+.' 
  id = 1
  
  print, format='(%"Write shapefile for %i images: %s")', N_ELEMENTS(exifinfos), outShp
  
  foreach EXIF, EXIFINFOS, imgName do begin
    lon = 0
    lat = 0
    
    attr = OrderedHash('name', FILE_BASENAME(imgName), 'path', imgName)
    
    iDateTime =where(stregex(exif, '^DateTime(Original)?=', /BOOLEAN), /NULL)
    iGPSDate = where(stregex(exif, '^GPSDateStamp=', /BOOLEAN), /NULL)
    iGPSTime = where(stregex(exif, '^GPSTimeStamp=', /BOOLEAN), /NULL)
    iGPSLon = where(stregex(exif, '^GPSLongitude=', /BOOLEAN), /NULL)
    iGPSLat = where(stregex(exif, '^GPSLatitude=', /BOOLEAN), /NULL)
    iGPSAlt = where(stregex(exif, '^GPSAltitude=', /BOOLEAN), /NULL)
    iGPSDir = where(stregex(exif, '^GPSImgDirection=', /BOOLEAN), /NULL)

    mulLat = total(stregex(exif, '^GPSLatitudeRef=S', /BOOLEAN)) ? -1:1
    mulLon = total(stregex(exif, '^GPSLongitudeRef=W', /BOOLEAN)) ? -1:1
    
    ;required attributes
    attr['date'] = stregex(exif[iDateTime[0]],regexDate, /EXTRACT)
    attr['time'] = stregex(exif[iDateTime[0]],regexTime, /EXTRACT)
    
    
    attr['lat'] = double(stregex(exif[iGPSLat], regexFloat2, /EXTRACT)) * mulLat
    attr['lon'] = double(stregex(exif[iGPSLon], regexFloat2, /EXTRACT)) * mulLon

    ;optional attributes
    if isa(iGPSDate) then begin
      attr['date_gps'] = stregex(exif[iGPSDate],regexDate, /EXTRACT)
    endif
    if isa(iGPSTime) then begin
      gpstime = stregex(exif[iGPSTime],regexTime, /EXTRACT)
      if gpsTime eq '' then begin
        gpsTime = strjoin(strsplit(exif[iGPSTime], '() ', /EXTRACT),':')
        gpstime = stregex(gpsTime,regexTime, /EXTRACT)
      endif
      
      attr['time_gps'] = gpstime 
    endif 
    if isa(iGPSAlt) then begin
      asl = stregex(exif[iGPSAlt],regexFloat +'( m|\))',/EXTRACT)
      asl = stregex(asl,regexFloat,/EXTRACT)
      attr['alt'] = double(asl)
    endif
    if isa(iGPSDir) then begin
      attr['dir'] = double(stregex(exif[iGPSDir], regexFloat, /EXTRACT))
    endif
    
    shp.writeData, attr['lon'],  attr['lat'],  attr
  endforeach
  
end

pro example_hubGDALTools_GPSPhoto2Shapefile
  
;  inDir = 'Z:\Data\FieldTrip2014\08_Pictures\'
;  inPictures = file_search_fast(inDir, '*.jpg', /FULLNAME, /RECURSIVE)
;  inPictures = inPictures[where(stregex(inpictures, '.jpg$', /BOOLEAN, /FOLD_CASE))]
;  outShp = 'F:\SenseCarbonProcessing\99_temp\picturesFiledTrip2014.shp'
;  ;inPictures = inpictures[0:2]
;  pathEXIFTool = 'C:\Users\geo_beja\Downloads\exiftool-9.74\exiftool.exe'
;  hubgdalTools.GPSPhotos2Shapefile, inPictures=inPictures, outShp=outShp, /Verbose, pathEXIFTool=pathEXIFTool
;  print, 'Done'
end

pro test_hubGDALTools_vector2raster
  root = 'F:\SenseCarbonProcessing\BJ_Segmentation\01_Edge\meanshift\'
  
  dirShp = 'F:\SenseCarbonProcessing\BJ_Segmentation\01_Edge\meanshift\Vector\LC82270652014175LGN00\'
  dirImg = 'F:\SenseCarbonProcessing\BJ_Segmentation\01_Edge\meanshift\Raster\
  srcShps = FILE_SEARCH(dirShp, '*.shp')
  foreach shp, srcShps do begin
    bn = FILE_BASENAME(shp, '.shp', /FOLD_CASE)
    dstImg = FILEPATH(bn+'.bsq', root_dir=dirImg)
    noDatA = -9999
    hubGDALTools.vector2raster, shp, dstImg, te=te, tr=[30.,30.],a='FID' $
      ,overwrite=0, noData=noData, ot='Int16'
      print, dstImg
      break
  endforeach
  
  files = file_search('F:\SenseCarbonProcessing\BJ_ML\02_LandsatData\227_065', '*SR.bsq')
  ts = hubRSTimeseries(files)
  dstImg = ''
end


pro hubGDALTools::clipRasterByVector, srcImg, srcShp, dstImg, sql=sql, featureID=featureID, outputFormat=outputFormat
  compile_opt static, strictarr
  if ~isa(dstImg) then stop
  gdal = hubGDALWrapper(SHOWCOMMANDS=1)
  imgInfo = gdal.gdalInfo(srcImg, /noGCP, /nomd, /norat, /noct)
  shpInfo = gdal.ogrinfo(srcShp, sql=sql, /SO, /AL, fid=featureID)
  te = shpInfo[where(stregex(shpInfo, '^Extent:', /BOOLEAN), /NULL)]
  te = strsplit(te, '(),', /EXTRACT)
  te = reform(double(te[[1,2,4,5]]),2,2)
  projwin = [min(te[0,*], maX=maxX), max(te[1,*], min=minY), maxX, minY]
  
  gdal.gdal_translate, srcImg, dstImg , projwin=projwin $
    , OF_=isa(outputFormat)? outputFormat : 'ENVI'

end

pro test_hubGDALTools_clipRasterByVector
  srcImg ='Y:\LandsatData\L8\LC82270652014175LGN00\LC82270652014175LGN00_TOA.bsq'
  root = 'F:\SenseCarbonProcessing\BJ_Segmentation\Rasterization'
  srcShp = filepath('edison_spatialr110_ranger0.3000000_minsize10_sgmts.shp', ROOT_DIR=root)
  
  dstImg = 'F:\SenseCarbonProcessing\BJ_Segmentation\Rasterization\LS8_Clip.tif'
  ;featureID=''
  outputFormat= 'GTiff'
  hubGDALTools.clipRasterByVector, srcImg, srcShp, dstImg $
      , featureID=featureID $
      , outputFormat=outputFormat
end

;+
; :Description:
;    
;
; :Params:
;    srcImg: in, required, type=string
;     Filepath of source image.
;     
;    gridImg: in, required, type=string
;     Filepath of grid image (= a raster image with the grid (=spatial reference + pixel size) of choice.
;     
;    dstImg: in, required, type=string
;     Filepath of output image.
;
; :Keywords:
;    resampling: in, optional, type=string
;     Any of the following values: ['near','bilinear','cubic','cubicspline','lanczos','average','mode']
;     (see `http://www.gdal.org/gdalwarp.html` for details.)
;     
;    t_srs: in, optional, type=string
;     Set this to specify the target SRS.
;-
pro hubGDALTools::map2rastergrid, srcImg, gridImg, dstImg, resampling=resampling, t_srs=t_srs
  compile_opt static, strictarr
  gdal = hubGDALWrapper(SHOWCOMMANDS=1)
  gridInfo = gdal.gdalInfo(gridImg, /noGCP, /nomd, /norat, /noct)
  
  outputFormat= 'ENVI'
  if ~isa(t_srs) then begin
    t_srs = reform(gdal.gdalSRSInfo(gridImg, o1='proj4'))
    t_srs = STRSPLIT(t_srs, "'", /EXTRACT)
    t_srs = '"'+strjoin(strtrim(t_srs,2),'')+'"' 
  endif
  
  gridTR = gridInfo[where(stregex(gridInfo, '^Pixel Size = ', /BOOLEAN))]
  gridTR = double((STRSPLIT(gridTR, '(,)', /EXTRACT))[1:*])
  
  gridTE = gridInfo[where(stregex(gridInfo, '^(Upper|Lower) (Left|Right)', /BOOLEAN))]
  gridTE = stregex(gridTE, '\([^(]+\)',/EXTRACT)
  gridTE = double((STRSPLIT(gridTE, '(,)', /EXTRACT)).toArray())
  xmin = min(gridTE[*,0], MAX=xmax)
  ymin = min(gridTE[*,1], MAX=ymax)
  te = [xmin, ymin, xmax, ymax]
  
  gdal.gdalwarp, srcImg, dstImg, s_srs = s_srs, t_srs = t_srs $
      , te=te, tr = gridTR $  $
      , r1=resampling, of_=outputFormat $
      , /overwrite
  
end

;+
; :Hidden:
;-
pro hubGDALTools__define
  struct = {hubGDALTools $
    , inherits IDL_Object $,
  }
end

pro example_convertFileFormat
  dirIn = 'F:\SenseCarbonProcessing\BJ_Segmentation\00_L8Data'
  dirOut = dirIn
  
  inFiles = FILE_SEARCH(dirIn, 'sub*.bsq')
  
  gdal = hubGDALWrapper()
  foreach inFile, inFiles do begin
    bn = FILE_BASENAME(inFile, '.bsq', /FOLD_CASE)
    outFile = FILEPATH(bn+'.tif', ROOT_DIR=dirOut)
    print, 'Convert '+inFile+'...'
    gdal.gdal_translate, infile, outFile, of1='GTIFF'
    
  endforeach
  print, 'Done'
end
pro test_hubGDALTools_map2rastergrid
  
 
  
  srcImg = hub_getTestImage('Spot_Berlin') 
  gridImg = hub_getTestImage('Hymap_Berlin-A_Image')
  

  tempDir = 'D:\temp\Examples'
  
  
  resamplings = ['near','bilinear','cubic','cubicspline','lanczos','average','mode']
  foreach resampling, resamplings do begin
    dstImg = FILEPATH('HyMap2SPOT_'+resampling, ROOT_DIR=tempDir)
    hubgdalTools.map2rastergrid, srcImg, gridImg, dstImg, RESAMPLING=resampling
    print, 'Done with '+dstImg
  endforeach
  
  enmapBox_openFile,  dstImg
  print, 'Finished'
end

pro test_hubGDAL_Tools
  print,  hubgdalTools.raster_formats()
  
  pathSrc = hub_getTestImage('Hymap_Berlin-B_Image')
  pathDst = FILEPATH('tmp.png', /TMP)
  hubgdalTools.gdal_translate, pathSrc, pathDst, 'PNG'

end