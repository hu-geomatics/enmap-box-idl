function VRTBuilder::init,dirGDAL=dirGDAL, showCommands=showCommands

  self.gdal = hubGDALWrapper(dirGDAL=dirGDAL, showCommands=showCommands)
  self.vrtRasterBands = list()
  self.metaData = hash()
  return, 1b
end

pro VRTBuilder::setMaskBand, maskImages, maskImageBandIndices
  self.maskImages = list(maskImages, /EXTRACT)
  n = N_ELEMENTS(self.maskImages)
  if isa(maskImageBandIndices) then begin
    self.maskImageBandIndices = list(maskImageBandIndices, /EXTRACT)
  endif else begin
    self.maskImageBandIndices = list(make_array(n, value=0), /EXTRACT)
  endelse
  
end

pro VRTBuilder::setMetaData, key, value
  (self.metaData)[strtrim(key,2)]= strtrim(value,2)
end

function VRTBuilder::getMetaData, key, value
  return, (self.metaData)[key]
end


pro VRTBuilder::addVRTRasterBand, srcImages, srcImageBandIndices $
  , description=description $
  , NoDataValue=NoDataValue $
  , HideNoDataValue=HideNoDataValue $
  , Offset=offset $
  , Scale=scale $
  , CategoryNames=CategoryNames $
  , SimpleSource=SimpleSource $
  , AveragedSource=AveragedSource $
  , ComplexSource=ComplexSource $
  , metaData=metaData $ 
  , maskImages=maskImages $
  , maskImageBandIndices = maskImageBandIndices  
  
  n = n_elements(srcImages)
  
  if isa(srcImageBandIndices) then begin
    nSrcBands = N_ELEMENTS(srcImageBandIndices)
    if nSrcBands ne 1 and nSrcBands ne n then message, 'Source band indices must be defined for all source images'  
    iSrcBands = nSrcBands eq 1 ? make_array(n, value=srcImageBandIndices) : srcImageBandIndices    
  endif else begin
    iSrcBands = make_array(n, value=0)
  endelse
  
  if ~ (keyword_set(simpleSource) || keyword_set(AveragedSource) || keyword_set(ComplexSource)) then begin
    simpleSource = 1b
    ;message, 'Set one of the following keywords: SimpleSource | AveragedSource | ComplexSource 
  endif
  
  bandID = n_elements(self.vrtRasterBands)+1
  
  vrtBand = dictionary()
  vrtBand.description = isa(description) ? description : string(format='(%"Band %i")', bandID)
  vrtBand.srcImages = srcImages
  if isa(maskImages) then begin
    if N_ELEMENTS(maskImages) ne n then message, 'Please define as many mask images as input images'
    vrtBand.maskImages = maskImages
    vrtBand.maskImageBandIndices = make_array(n, value=0)
    ;TODO
  endif
  
  vrtBand.iSrcBands = iSrcBands
  if isa(noDataValue) then begin
    vrtBand.noDataValue = noDataValue
    vrtBand.hideNoDataValue =  keyword_set(hideNoDataValue)
  endif
  if isa(offset) then vrtBand.offset = offset
  if isa(scale) then vrtBand.scale = scale
  if isa(categoryNames) then vrtBand.categoryNames = categoryNames
  vrtBand.metaData = isa(metaData) ? metaData : Hash() 
  self.vrtRasterBands.add, vrtBand
  
end

function VRTBuilder::_overloadPrint
  info = list('VRTBuilder')
  foreach vrtBand, self.vrtRasterBands, i do begin
    
    srcFileInfo = strjoin(string(vrtBand.srcImages)+','+strtrim(vrtBand.iSrcBands,2), ';')
    if strlen(srcFileInfo) gt 100 then begin
      srcFileInfo = string(format='(%"%-100s...")', srcFileInfo)
    endif
    info.add, string(format='(%"  Band %i \"%s\" {%s}")', i, vrtBand.description, srcFileInfo)
  endforeach
  
  return, strjoin(info.toArray(), string(13b))
end 


;+
; :Description:
;    Returns the relative path from fileRoot to fileTarget
;
; :Params:
;    fileRoot: in, required, type=string
;     Absolute filepath of the file where the relative path starts
;    fileTarget: in, required, type=string
;     Absolute filepath of the target file.
;
;
;-
function VRTBuilder::_createRelativePath, fileRoot, fileTarget, sep=sep

  ;get the current systems path separator
  if ~isa(sep) then sep = STRMID(FILEPATH('A',ROOT_DIR='B'),1,1)
  tFileName = FILE_BASENAME(fileTarget)

  rFolders = STRSPLIT(FILE_DIRNAME(fileRoot), sep, /EXTRACT, COUNT=nrFolders)
  tFolders = STRSPLIT(FILE_DIRNAME(fileTarget), sep, /EXTRACT, COUNT=ntFolders)

  ;skip the folders both path's have in common
  iMax = (nrFolders < ntFolders)-1
  i = 0
  while i le iMax do begin
    if ~strcmp(rFolders[i],tFolders[i]) then break
    i++
  endwhile
  
  if i eq 0 then message, 'A relative path can not span different drives'

  rFolders = i eq nrFolders ? !NULL : rFolders[i:*]
  tFolders = i eq ntFolders ? !NULL : tfolders[i:*]
  relPath = ''

  nrFolders = n_elements(rFolders)
  ntFolders = n_elements(tfolders)

  ;go nrFolders back
  relPath = nrFolders gt 0 ? strjoin(make_array(nrFolders, value='..'),sep) : ''

  ;go ntFolders to the target folder
  if ntFolders gt 0 then relPath += strjoin(tFolders, sep) + sep

  ;finally add the target file
  relPath += tFileName

  return, relPath
end

function VRTBuilder::_readASCIILines, pathASCIIFile
  lines = strarr(file_lines(pathASCIIFile))
  openr, un, pathASCIIFile, /get_lun
  readf, un, lines
  close, un,FORCE=1
  free_lun, un,FORCE=1
  return, lines
end

pro VRTBuilder::_writeASCIILines, pathASCIIFile, lines
  openw, un, pathASCIIFile, /get_lun
  foreach line, lines do begin
    printf, un, line
  endforeach
  close, un,FORCE=1
  free_lun, un,FORCE=1
end

function VRTBuilder::_getXMLTags, lines, xmlTag
  tags = list()
  regexStart = '<'+xmlTag+'[ >]'
  regexEnd   = '(</'+xmlTag+'>|<'+xmlTag+'[^/>]*/>)'
  
  nLines = N_ELEMENTS(lines)
  i = 0
  while i lt nLines do begin
    if STREGEX(lines[i], regexStart, /BOOLEAN) then begin
      j = i
      while j lt nLines-1 && ~STREGEX(lines[j], regexEnd, /BOOLEAN) do begin
        j++
      endwhile
      tags.add, lines[i:j]
      i = j+1
    endif else begin
      i++
    endelse
  endwhile 
  
  return, tags
end

function VRTBuilder::_createRasterSourceXML, pathRasterImage, bandIndex $
  , nodata=nodata $
  , isMask=isMask 
  ;TODO add more
   
  srcType = 'SimpleSource'
  if isa(noData) then begin
    srcType = 'ComplexSource'
  endif
  
  fileInfo = self.vrtInputFileInfos[pathRasterImage[0]]
  xml = list()
  
  xml.add, '<'+srcType+'>'
  xml.add, '  '+self._fileInfo2XML(fileinfo.SourceFilename)  
  xml.add, '  '+self._fileInfo2XML(fileinfo.SourceBand, value= (keyword_set(isMask) ? 'mask,' : '')+strtrim(bandIndex+1,2))
  xml.add, '  '+self._fileInfo2XML(fileinfo.SourceProperties)
  xml.add, '  '+self._fileInfo2XML(fileinfo.SrcRect)
  xml.add, '  '+self._fileInfo2XML(fileinfo.DstRect)
  if isa(noData) then begin
    xml.add, '  <NODATA>'+strtrim(nodata,2)+'</NODATA>'
  endif
  ;todo
  xml.add, '</'+srcType+'>'
  return, xml 
end

function VRTBuilder::_fileInfo2XML, fileInfo, attr=attr, value=value
  _name = fileInfo.name
  _attr = isa(attr)? attr: (fileInfo.hasKey('attr') ? fileInfo.attr : !NULL)
  _value = isa(value) ? value : (fileInfo.hasKey('value') ? fileInfo.value : !NULL) 
  
  result = '<'+_name
  
  if isa(_attr) then result += ' '+_attr
  if isa(_value) then begin
    result += '>'+strtrim(_value,2)+'</'+_name+'>'
  endif else begin
    result += '/>'
  endelse
  return, result
end

function VRTBuilder::_createMaskBandXML, maskImages, bandIndices
  
  n = N_ELEMENTS(maskImages)
  if isa(bandIndices) then begin
    ;TODO
  endif
  
  xml = list()
  n= 0
  xml.add, '<MaskBand>
  xml.add, '  <VRTRasterBand dataType="Byte">
  foreach pathMaskImage, maskImages, maskImg do begin
    if isa(pathMaskimage) then begin
      n++
      srcXML = self._createRasterSourceXML(pathMaskImage,0, /isMask)
      self._appendList, srcXML, xml, 4
    endif
  endforeach
  xml.add, '  </VRTRasterBand>
  xml.add, '</MaskBand>
  
  return, n gt 0 ? xml : ''
end

function VRTBuilder::_createMetaDataXML, hash
  xml = list()
  xml.add, '<Metadata>'
  foreach value, hash, key do begin
    xml.add, string(format='(%"  <MDI key=\"%s\">%s</MDI>")', key, value)
  endforeach
  xml.add, '</Metadata>'
  return, xml
end


;+
; :Description:
;    Reads the XML definitions of all files 
;
;-
function VRTBuilder::_readInputFileInfoXML, xmlLines
  fileInfos = Hash()
  xmlTags = self._getXMLTags(xmlLines, '(Simple|Averaged|Complex)Source')
  ;read all 
  foreach xmlTag, xmlTags do begin
    fileInfo = Dictionary()
    foreach tagName, ['SourceFilename','SourceBand','SourceProperties','SrcRect','DstRect'] do begin
      fileInfo[tagName] = self._getTagInfo(xmlTag, tagName)
    endforeach
    filename = fileInfo.SourceFilename.value
    if ~fileInfos.HasKey(filename) then begin
      fileInfos[filename] = fileinfo
    endif
  endforeach
  
  
  return, fileInfos
end


pro VRTBuilder::_appendList, src, dst, offset
  _str = isa(offset) ? strjoin(make_array(offset, value=' '),'') : '';
  foreach line, src do dst.add, _str+line 
end

function VRTBuilder::_getTagInfo, xmlLines, tagName
  ;get tag start
  regexStart = '<'+tagName+'[ >]'
  
  regexEnd   = '(</'+tagName+'>|<'+tagName+'[^/>]*/>)'
  
  tagDef = Dictionary('name',tagName)
  
  tagLines = !NULL
  nLines = N_ELEMENTS(xmlLines)
  i = where(STREGEX(xmlLines, regexStart, /BOOLEAN), /NULL)
  j = where(STREGEX(xmlLines, regexEnd, /BOOLEAN), /NULL)
  if ~isa(i) then return, !NULL
  if ~isa(j) then message, 'Tag is not finished properly: '+ tagName
  
  tagLines = strtrim(strjoin(xmlLines[i[0]:j[0]],''),2)
  ;get attribute string
  head = stregex(tagLines, '<'+tagName+'[^>]*>', /EXTRACT)
  lHead = strlen(head)
  hasValues = ~stregex(head, '/>$', /BOOLEAN) 
  attr = strtrim(strmid(stregex(head, '[^</>]+', /EXTRACT), strlen(tagName)+1),2)
  if strlen(attr) gt 0 then tagDef.attr = attr
  
  ;get value string, if exists
  if hasValues then begin
    tagDef.value = strmid(tagLines, lHead, stregex(tagLines, '</'+tagName)-lHead)
  endif
    
  return, tagDef

  
end


pro VRTBuilder::writeVRT, pathVRT, relativePaths=relativePaths
  
  if ~isa(pathVRT) then message, 'Please specify pathVRT'
  
  ;1. get "hull of images"
  inputfiles = list()
  lam = lambda(x:isa(x))
  if isa(self.maskImages) then begin
    
    inputFiles.add, self.maskImages.Filter(lam), /EXTRACT
  endif
  foreach vrtBand, self.vrtRasterBands do begin
    inputfiles.add, vrtBand.srcImages, /EXTRACT
    if vrtBand.hasKey('maskImages') then begin
      inputfiles.add, vrtBand.maskImages.filter(lam), /EXTRACT
    endif
  endforeach
  
  inputfiles = hubMathHelper.getSet(inputfiles)
  
 
  
  ;2. create temp. VRT image
  myID = strjoin(strsplit(strtrim(systime(0),2), ' :', /EXTRACT),'')
  pathTmpVRT = FILEPATH('tmpVRT'+myID+'.vrt', /TMP)
  pathTmpVRTInput = FILEPATH('tmpVRT'+myID+'input.txt', /TMP)
  
  self._writeASCIILines, pathTmpVRTInput, inputfiles
  self.gdal.gdalbuildvrt, pathTmpVRT,  input_file_list = pathTmpVRTInput $
    ,/separate , /overwrite ;b=1 ;<- but gets GDAL error :(  
  
  
  tmpVRTLines = self._readASCIILines(pathTmpVRT)
  FILE_DELETE, pathTMPVRT, pathTmpVRTInput
  
  
  
  ;3. parse VRT 
  self.vrtInputFileInfos = self._readInputFileInfoXML(tmpVRTLines)
  ;test 
  missing = hubMathHelper.getDifference(inputfiles, self.vrtInputFileInfos.keys())
  if isa(missing) then message, 'lost files'
  
  
  xmlSRS = self._getXMLTags(tmpVRTLines, 'SRS')
  xmlGeoTrans = self._getXMLTags(tmpVRTLines, 'GeoTransform')
  
  ;4. create VRT XML 
  vrtXML = list()
  vrtXML.add, tmpVRTLines[0] ;add the VRTDataset line
  vrtXML.add, xmlSRS, /EXTRACT
  vrtXML.add, xmlGeoTrans, /EXTRACT
  if isa(self.maskImages) && N_ELEMENTS(self.maskImages) gt 0 then begin
    maskXML = self._createMaskBandXML(self.maskImages, self.maskImageBandIndices)
    self._appendList, maskXML, vrtXML, 2
  endif
  
  if n_elements(self.metadata) gt 0 then begin
    metaDataXML = self._createMetaDataXML(self.metaData)
    foreach line, metaDataXML do vrtXML.add, '  '+line
  endif
  
  ;add virtual bands, use XML information as described in the temporary VRT file
  flagRelativePaths = KEYWORD_SET(relativepaths)
  
  foreach vrtBand, self.vrtRasterBands, iBand do begin
    pathSrcImages = vrtBand.srcImages
    
    vrtBandXML = list()
    ;specify data type of VRT band
    vrtBandType = vrtBand.haskey('datatype') ? 'DataType="'+vrtBandType+'"' : !NULL
    if ~isa(vrtBandType) then begin
      ;if not specified, use first source files data type
      srcPropertiesAttributes = (self.vrtInputFileInfos[pathSrcImages[0]]).sourceProperties.attr
      vrtBandType = stregex(srcPropertiesAttributes, 'DataType=[^ ]+', /EXTRACT)
    endif
    
    vrtBandXML.add, string(format='(%"<VRTRasterBand %s band=\"%i\">")',vrtBandType, iBand+1)
    vrtBandXML.add, string(format='(%"  <Description>%s</Description>")', vrtBand.description)
    
    div = !NULL
    if vrtBand.hasKey('nodatavalue') then begin
      vrtBandXML.add, string(format='(%"  <NoDataValue>%f</NoDataValue>")', vrtBand.nodatavalue)
      vrtBandXML.add, string(format='(%"  <HideNoDataValue>%i</HideNoDataValue>")', vrtBand.HideNoDataValue)
      div = vrtBand.nodatavalue
    endif
    
    if vrtBand.hasKey('offset') then begin
      vrtBandXML.add, string(format='(%"  <Offset>%f</Offset>")', vrtBand.Offset)
    endif

    if vrtBand.hasKey('scale') then begin
      vrtBandXML.add, string(format='(%"  <Scale>%f</Scale>")', vrtBand.Scale)
    endif
    

    if vrtBand.hasKey('CategoryNames') then begin
      vrtBandXML.add, '  <CategoryNames>' 
      foreach categoryName, vrtBand.CategoryNames do begin
        vrtBandXML.add, string(format='(%"      <CategoryName>%s</CategoryName>")', categoryName)  
      endforeach
      vrtBandXML.add, '  </CategoryNames>'
    endif
    
    ;add mask image informations
    if vrtBand.hasKey('maskImages') && isa(vrtBand.maskImages) then begin
      maskBandXML = self._createMaskBandXML(vrtBand.maskImages, vrtBand.maskImageBandIndices)
      self._appendList, maskBandXML, vrtBandXML, 2
    endif
    
    ;add source image information
    foreach pathSrcImage, pathSrcImages, iSrcImg do begin
      NODATA = !NULL
      if vrtBand.hasKey('NoDataValue') then begin
        NODATA = vrtBand.noDataValue
      endif
      srcXML = self._createRasterSourceXML(pathSrcImage, (vrtBand.iSrcBands)[iSrcImg], nodata=nodata)
      self._appendList, srcXML, vrtBandXML, 2
    endforeach
    
    vrtBandXML.add, '</VRTRasterBand>'
    self._appendList, vrtBandXML, vrtXML, 2
    
  endforeach
  
  vrtXML.add, '</VRTDataset>'
  
  ;5. write VRT XML
  self._writeASCIILines, pathvrt, vrtXML
  
end


pro VRTBuilder__define
  struct = {VRTBuilder $
    , inherits IDL_Object $,
    , gdal:obj_new() $
    , vrtRasterBands:list() $
    , vrtInputFileInfos:hash() $
    , metaData:hash() $
    , maskImages:list() $
    , maskImageBandIndices:list() $
  }
end

pro VRTBuilderExample_createStack

end
pro VRTBuilderExample_createMosaic
  dir = 'X:\LandsatData\BR163Buffer50k'
  files = file_search_fast(dir, 'L*_TOA.bsq', /FULLNAME, /FILES)
  files = files[0:10]
  pathVRT = 'X:\LandsatData\BR163Buffer50k.vrt'

  vb = vrtBuilder(SHOWCOMMANDS=1)
  for iBand = 0, 5 do begin
    vb.addVRTRasterBand, files, iBand, NODATAVALUE=0
  endfor
  print, vb
  vb.writeVRT, pathVRT,RELATIVEPATHS=0
  hubHelper.openFile, pathVRT, /ASCII
end

pro VRTBuilderExample_createMaskedImage
  pathVRT = 'X:\LandsatData\VRTStackExample.vrt'
  pathSrc =  enmapBox_getTestImage('AF_Image')
  pathMask = enmapBox_getTestImage('AF_Mask')
  vb = vrtBuilder(SHOWCOMMANDS=1)
  vb.setMetaData, 'meta1',23
  vb.setMetaData, 'meta2','foobar'
  vb.setMetaData, 'meta3',42.12
  vb.setMaskBand, enmapBox_getTestImage('AF_MaskVegetation'), 0

  for iBand=0, 9 do begin
    vb.addVRTRasterBand, pathSrc,iBand, NODATAVALUE=-1
    print, iBand
  endfor
  vb.writeVRT, pathVRT
end


pro VRTBuilderExample_createStackedMosaic
  ;write time stack of footprint 227/065 (Novo Progresso) + 227/066
  ;one image per year and footprint
  
  dir = 'Y:\01_InputData'
  bandOfInterest = 5
  yStart = 2000
  yEnd = 2005

  pathVRT = 'X:\LandsatData\TimeStack_227_065_B'+strtrim(bandOfInterest,2)+'.vrt'
  pathVRT = 'X:\LandsatData\TimeStackExample.vrt'

  srfiles = list()
  srfiles.add, file_search_fast(dir, '227065_*BLA_SR.bsq', /FULLNAME, /FILES), /EXTRACT
  srfiles.add, file_search_fast(dir, '227066_*BLA_SR.bsq', /FULLNAME, /FILES), /EXTRACT

  ;read MTL file info to get approx. of cloud cover
  mtlInfos = list()
  n = N_ELEMENTS(srFiles)
  foreach srFile, srFiles, i do begin
    pathMTL = FILEPATH(FILE_BASENAME(srFile,'.bsq')+'_MTL.txt', root_dir=dir)
    print, format='(%"Read %s: %5.2f...")', FILE_BASENAME(pathMTL), 100.*i/n
    mtlInfos.add, landsatTools.readMTLFile(pathMTL, tagsOfInterest = ['DATE_ACQUIRED','CLOUD_COVER','WRS_PATH','WRS_ROW'])
  endforeach

  mtlMeta = replicate({year:0, pathRow:'', cc:0.}, n)
  foreach mtlInfo, mtlInfos, i do begin
    mtlMeta[i].year = fix(strmid(mtlInfo.DATE_ACQUIRED,0,4))
    mtlMeta[i].pathRow = mtlInfo.WRS_Path+'_'+mtlInfo.WRS_Row
    mtlMeta[i].cc = float(mtlInfo.Cloud_Cover)
  endforeach
  
  
  
  ;
  pathRow = hubMathHelper.getSet(mtlMeta.pathRow)

  ;build the VRT
  vb = vrtBuilder(SHOWCOMMANDS=1)
  for y = yStart, yEnd do begin
    bestObservationFiles = list()
    foreach pr, pathRow do begin
      i = where(mtlMeta.pathRow eq pr and mtlMeta.year eq y, /NULL)
      if isa(i) then begin
        ;get the image of 
        !NULL = min(mtlMeta[i].cc, ii)
        bestObservationFiles.add, srFiles[i[ii[0]]]
      endif
    endforeach
    if N_ELEMENTS(bestObservationFiles) gt 0 then begin
      vb.addVRTRasterBand, bestObservationFiles, bandOfInterest-1 $
        , NODATAVALUE=-9999 $
        , DESCRIPTION= string(format='(%"%i_B%i")', y , bandOfInterest)
    endif
  endfor

    vb.writeVRT, pathVRT,RELATIVEPATHS=0
    hubHelper.openFile, pathVRT, /ASCII
end

pro example_vrtBuilderNDVIStack
  timeseries = hubRSTimeseriesLandsat('G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014')
  timeseries = timeseries.subset(hubTime(2001),hubTime(2001100))
  filenames = timeseries.scenes.path.fmask
  pathVRT = 'myNDVI.vrt' 
  vb = VRTBuilder(SHOWCOMMANDS=1)
  foreach path, filenames do begin
    vb.addVRTRasterBand, path, 0, DESCRIPTION=FILE_BASENAME(path)
  endforeach
  vb.writeVRT, pathVRT
  hubHelper.openFile, pathVRT, /ASCII
  
  stop
end

pro test_vrtBuilder
  if 0b then VRTBuilderExample_createMaskedImage
  if 0b then VRTBuilderExample_createMosaic
  if 1b then VRTBuilderExample_createStackedMosaic  
   
  if 0b then begin
    vb = vrtBuilder(SHOWCOMMANDS=1)
    print, vb._createRelativePath('AA\BB\rfile', 'AA\BB\tfile')
    print, vb._createRelativePath('AA\BB\rfile', 'AA\BB\CC\DD\tfile')
    print, vb._createRelativePath('AA\BB\CC\DD\rfile', 'AA\BB\tfile')
    print, vb._createRelativePath('AA\BB\CC\DD\rfile', 'AA\BB\C2\tfile')
    stop
  endif

end
  
  
