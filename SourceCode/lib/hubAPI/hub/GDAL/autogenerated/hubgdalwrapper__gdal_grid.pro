function hubGDALWrapper::gdal_grid $
   ,z_increase=z_increase   $
   ,l=l   $
   ,src_datasource $
   ,spat=spat   $
   ,dst_filename $
   ,where=where   $
   ,a_srs=a_srs   $
   ,clipsrclayer=clipsrclayer   $
   ,co1=co1   $
   ,ot=ot   $
   ,outsize=outsize   $
   ,clipsrc1=clipsrc1   $
   ,of_=of_   $
   ,tye=tye   $
   ,q=q   $
   ,sql=sql   $
   ,clipsrcsql=clipsrcsql   $
   ,clipsrcwhere=clipsrcwhere   $
   ,zfield=zfield   $
   ,z_multiply=z_multiply   $
   ,a1=a1   $
   ,txe=txe   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_grid'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_grid","IDLNAME":"gdal_grid","DESCRIPTION":["creates regular grid from the scattered data","This program creates regular grid (raster) from the scattered data read from the OGR datasource. Input data will be interpolated to fill grid nodes with values, you can choose from various interpolation methods.","There are number of interpolation algorithms to choose from.","Inverse distance to a power. This is default algorithm. It has following parameters:","Moving average algorithm. It has following parameters:","Note, that it is essential to set search ellipse for moving average method. It is a window that will be averaged when computing grid nodes values.","Nearest neighbor algorithm. It has following parameters:","All the metrics have the same set of options:","If your CSV file does not contain column headers then it can be handled in the following way:"],"PARAMETERS":[{"GDALNAME":"-z_increase","GDALVALUE":"increase_value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"z_increase"},{"GDALNAME":"-l","GDALVALUE":"layername]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"l"},{"GDALNAME":"src_datasource","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"src_datasource"},{"GDALNAME":"-spat","GDALVALUE":"xmin ymin xmax ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"spat"},{"GDALNAME":"dst_filename","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dst_filename"},{"GDALNAME":"-where","GDALVALUE":"expression","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"where"},{"GDALNAME":"-a_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_srs"},{"GDALNAME":"-clipsrclayer","GDALVALUE":"layer","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrclayer"},{"GDALNAME":"-co","GDALVALUE":"\"NAME=VALUE\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"-ot","GDALVALUE":"{Byte/Int16/UInt16/UInt32/Int32/Float32/Float64/ CInt16/CInt32/CFloat32/CFloat64}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-outsize","GDALVALUE":"xsize ysize","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"outsize"},{"GDALNAME":"-clipsrc","GDALVALUE":"xmin ymin xmax ymax|WKT|datasource|spat_extent","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrc1"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"-tye","GDALVALUE":"ymin ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tye"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-sql","GDALVALUE":"select_statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sql"},{"GDALNAME":"-clipsrcsql","GDALVALUE":"sql_statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrcsql"},{"GDALNAME":"-clipsrcwhere","GDALVALUE":"expression","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrcwhere"},{"GDALNAME":"-zfield","GDALVALUE":"field_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"zfield"},{"GDALNAME":"-z_multiply","GDALVALUE":"multiply_value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"z_multiply"},{"GDALNAME":"-a","GDALVALUE":"algorithm[:parameter1=value1]*","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a1"},{"GDALNAME":"-txe","GDALVALUE":"xmin xmax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"txe"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_grid' $
      , z_increase=z_increase   $
      , l=l   $
      , src_datasource $
      , spat=spat   $
      , dst_filename $
      , where=where   $
      , a_srs=a_srs   $
      , clipsrclayer=clipsrclayer   $
      , co1=co1   $
      , ot=ot   $
      , outsize=outsize   $
      , clipsrc1=clipsrc1   $
      , of_=of_   $
      , tye=tye   $
      , q=q   $
      , sql=sql   $
      , clipsrcsql=clipsrcsql   $
      , clipsrcwhere=clipsrcwhere   $
      , zfield=zfield   $
      , z_multiply=z_multiply   $
      , a1=a1   $
      , txe=txe   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_grid $
   ,z_increase=z_increase   $
   ,l=l   $
   ,src_datasource $
   ,spat=spat   $
   ,dst_filename $
   ,where=where   $
   ,a_srs=a_srs   $
   ,clipsrclayer=clipsrclayer   $
   ,co1=co1   $
   ,ot=ot   $
   ,outsize=outsize   $
   ,clipsrc1=clipsrc1   $
   ,of_=of_   $
   ,tye=tye   $
   ,q=q   $
   ,sql=sql   $
   ,clipsrcsql=clipsrcsql   $
   ,clipsrcwhere=clipsrcwhere   $
   ,zfield=zfield   $
   ,z_multiply=z_multiply   $
   ,a1=a1   $
   ,txe=txe   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_grid($
   z_increase=z_increase   $
   ,l=l   $
   ,src_datasource $
   ,spat=spat   $
   ,dst_filename $
   ,where=where   $
   ,a_srs=a_srs   $
   ,clipsrclayer=clipsrclayer   $
   ,co1=co1   $
   ,ot=ot   $
   ,outsize=outsize   $
   ,clipsrc1=clipsrc1   $
   ,of_=of_   $
   ,tye=tye   $
   ,q=q   $
   ,sql=sql   $
   ,clipsrcsql=clipsrcsql   $
   ,clipsrcwhere=clipsrcwhere   $
   ,zfield=zfield   $
   ,z_multiply=z_multiply   $
   ,a1=a1   $
   ,txe=txe   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
