function hubGDALWrapper::gdaltindex $
   ,tileindex=tileindex   $
   ,skip_different_projection=skip_different_projection   $
   ,t_srs=t_srs   $
   ,src_srs_format=src_srs_format   $
   ,src_srs_name=src_srs_name   $
   ,write_absolute_path=write_absolute_path   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdaltindex'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdaltindex","IDLNAME":"gdaltindex","DESCRIPTION":["Builds a shapefile as a raster tileindex"],"PARAMETERS":[{"GDALNAME":"-tileindex","GDALVALUE":"field_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tileindex"},{"GDALNAME":"-skip_different_projection","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"skip_different_projection"},{"GDALNAME":"-t_srs","GDALVALUE":"target_srs","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"t_srs"},{"GDALNAME":"-src_srs_format","GDALVALUE":"[AUTO|WKT|EPSG|PROJ] [-lyr_name name] index_file [gdal_file]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"src_srs_format"},{"GDALNAME":"-src_srs_name","GDALVALUE":"field_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"src_srs_name"},{"GDALNAME":"-write_absolute_path","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"write_absolute_path"},{"GDALNAME":"-f","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"f1"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdaltindex' $
      , tileindex=tileindex   $
      , skip_different_projection=skip_different_projection   $
      , t_srs=t_srs   $
      , src_srs_format=src_srs_format   $
      , src_srs_name=src_srs_name   $
      , write_absolute_path=write_absolute_path   $
      , f1=f1   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdaltindex $
   ,tileindex=tileindex   $
   ,skip_different_projection=skip_different_projection   $
   ,t_srs=t_srs   $
   ,src_srs_format=src_srs_format   $
   ,src_srs_name=src_srs_name   $
   ,write_absolute_path=write_absolute_path   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdaltindex($
   tileindex=tileindex   $
   ,skip_different_projection=skip_different_projection   $
   ,t_srs=t_srs   $
   ,src_srs_format=src_srs_format   $
   ,src_srs_name=src_srs_name   $
   ,write_absolute_path=write_absolute_path   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
