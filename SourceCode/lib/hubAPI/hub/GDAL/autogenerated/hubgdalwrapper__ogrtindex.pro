function hubGDALWrapper::ogrtindex $
   ,write_absolute_path=write_absolute_path   $
   ,skip_different_projection=skip_different_projection   $
   ,src_dataset $
   ,output_dataset $
   ,lnum=lnum   $
   ,lname=lname   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'ogrtindex'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"ogrtindex","IDLNAME":"ogrtindex","DESCRIPTION":["creates a tileindex","If no -lnum or -lname arguments are given it is assumed that all layers in source datasets should be added to the tile index as independent records.","If the tile index already exists it will be appended to, otherwise it will be created.","It is a flaw of the current ogrtindex program that no attempt is made to copy the coordinate system definition from the source datasets to the tile index (as is expected by MapServer when PROJECTION AUTO is in use).","This example would create a shapefile (tindex.shp) containing a tile index of the BL2000_LINK layers in all the NTF files in the wrk directory: "],"PARAMETERS":[{"GDALNAME":"-write_absolute_path","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"write_absolute_path"},{"GDALNAME":"-skip_different_projection","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"skip_different_projection"},{"GDALNAME":"src_dataset","GDALVALUE":"...","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"src_dataset"},{"GDALNAME":"output_dataset","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_dataset"},{"GDALNAME":"-lnum","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"lnum"},{"GDALNAME":"-lname","GDALVALUE":"name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"lname"},{"GDALNAME":"-f","GDALVALUE":"output_format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"f1"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('ogrtindex' $
      , write_absolute_path=write_absolute_path   $
      , skip_different_projection=skip_different_projection   $
      , src_dataset $
      , output_dataset $
      , lnum=lnum   $
      , lname=lname   $
      , f1=f1   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::ogrtindex $
   ,write_absolute_path=write_absolute_path   $
   ,skip_different_projection=skip_different_projection   $
   ,src_dataset $
   ,output_dataset $
   ,lnum=lnum   $
   ,lname=lname   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.ogrtindex($
   write_absolute_path=write_absolute_path   $
   ,skip_different_projection=skip_different_projection   $
   ,src_dataset $
   ,output_dataset $
   ,lnum=lnum   $
   ,lname=lname   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
