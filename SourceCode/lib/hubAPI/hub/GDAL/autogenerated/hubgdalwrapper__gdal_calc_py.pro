function hubGDALWrapper::gdal_calc_py $
   ,debug=debug   $
   ,calc=calc   $
   ,NoDataValue=NoDataValue   $
   ,A1=A1   $
   ,creation_option=creation_option   $
   ,A_band=A_band   $
   ,type=type   $
   ,help1=help1   $
   ,h1=h1   $
   ,co1=co1   $
   ,overwrite=overwrite   $
   ,outfile=outfile   $
   ,format1=format1   $
   ,allBands=allBands   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_calc.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_calc.py","IDLNAME":"gdal_calc_py","DESCRIPTION":["Command line raster calculator with numpy syntax","Command line raster calculator with numpy syntax. Use any basic arithmetic supported by numpy arrays such as +-*\\ along with logical operators such as &gt;. Note that all files must have the same dimensions, but no projection checking is performed.","add two files together ","average of two layers ","set values of zero and below to null "],"PARAMETERS":[{"GDALNAME":"--debug","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--calc","GDALVALUE":"CALC","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"calc"},{"GDALNAME":"--NoDataValue","GDALVALUE":"NODATAVALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"NoDataValue"},{"GDALNAME":"-A","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"A1"},{"GDALNAME":"--creation-option","GDALVALUE":"CREATION_OPTIONS","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"creation_option"},{"GDALNAME":"--A_band","GDALVALUE":"A_BAND","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"A_band"},{"GDALNAME":"--type","GDALVALUE":"TYPE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"type"},{"GDALNAME":"--help","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help1"},{"GDALNAME":"-h","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"h1"},{"GDALNAME":"--co","GDALVALUE":"CREATION_OPTIONS","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"--overwrite","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"overwrite"},{"GDALNAME":"--outfile","GDALVALUE":"OUTF","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"outfile"},{"GDALNAME":"--format","GDALVALUE":"FORMAT","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"format1"},{"GDALNAME":"--allBands","GDALVALUE":"ALLBANDS","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"allBands"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_calc.py' $
      , debug=debug   $
      , calc=calc   $
      , NoDataValue=NoDataValue   $
      , A1=A1   $
      , creation_option=creation_option   $
      , A_band=A_band   $
      , type=type   $
      , help1=help1   $
      , h1=h1   $
      , co1=co1   $
      , overwrite=overwrite   $
      , outfile=outfile   $
      , format1=format1   $
      , allBands=allBands   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_calc_py $
   ,debug=debug   $
   ,calc=calc   $
   ,NoDataValue=NoDataValue   $
   ,A1=A1   $
   ,creation_option=creation_option   $
   ,A_band=A_band   $
   ,type=type   $
   ,help1=help1   $
   ,h1=h1   $
   ,co1=co1   $
   ,overwrite=overwrite   $
   ,outfile=outfile   $
   ,format1=format1   $
   ,allBands=allBands   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_calc_py($
   debug=debug   $
   ,calc=calc   $
   ,NoDataValue=NoDataValue   $
   ,A1=A1   $
   ,creation_option=creation_option   $
   ,A_band=A_band   $
   ,type=type   $
   ,help1=help1   $
   ,h1=h1   $
   ,co1=co1   $
   ,overwrite=overwrite   $
   ,outfile=outfile   $
   ,format1=format1   $
   ,allBands=allBands   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
