function hubGDALWrapper::gdal_edit_py $
   ,unsetgt=unsetgt   $
   ,tr=tr   $
   ,ro=ro   $
   ,a_srs=a_srs   $
   ,oo=oo   $
   ,datasetname $
   ,stats=stats   $
   ,mo=mo   $
   ,a_nodata=a_nodata   $
   ,unsetstats=unsetstats   $
   ,unsetmd=unsetmd   $
   ,a_ullr=a_ullr   $
   ,gdal_edit $
   ,gcp=gcp   $
   ,help_general=help_general   $
   ,approx_stats=approx_stats   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_edit.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_edit.py","IDLNAME":"gdal_edit_py","DESCRIPTION":["Edit in place various information of an existing GDAL dataset","The gdal_edit.py script can be used to edit in place various information of an existing GDAL dataset (projection, geotransform, nodata, metadata).","It works only with raster formats that support update access to existing datasets. ","Defines the target coordinate system. This coordinate system will be written to the dataset.","-a_ullr, -tr and -unsetgt options are exclusive.","-unsetstats and either -stats or -approx_stats options are exclusive."],"PARAMETERS":[{"GDALNAME":"-unsetgt","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unsetgt"},{"GDALNAME":"-tr","GDALVALUE":"xres yres","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tr"},{"GDALNAME":"-ro","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ro"},{"GDALNAME":"-a_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_srs"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"oo"},{"GDALNAME":"datasetname","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"datasetname"},{"GDALNAME":"-stats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"stats"},{"GDALNAME":"-mo","GDALVALUE":"\"META-TAG=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mo"},{"GDALNAME":"-a_nodata","GDALVALUE":"value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_nodata"},{"GDALNAME":"-unsetstats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unsetstats"},{"GDALNAME":"-unsetmd","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unsetmd"},{"GDALNAME":"-a_ullr","GDALVALUE":"ulx uly lrx lry","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_ullr"},{"GDALNAME":"gdal_edit","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"gdal_edit"},{"GDALNAME":"-gcp","GDALVALUE":"pixel line easting northing [elevation]]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"gcp"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-approx_stats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"approx_stats"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_edit.py' $
      , unsetgt=unsetgt   $
      , tr=tr   $
      , ro=ro   $
      , a_srs=a_srs   $
      , oo=oo   $
      , datasetname $
      , stats=stats   $
      , mo=mo   $
      , a_nodata=a_nodata   $
      , unsetstats=unsetstats   $
      , unsetmd=unsetmd   $
      , a_ullr=a_ullr   $
      , gdal_edit $
      , gcp=gcp   $
      , help_general=help_general   $
      , approx_stats=approx_stats   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_edit_py $
   ,unsetgt=unsetgt   $
   ,tr=tr   $
   ,ro=ro   $
   ,a_srs=a_srs   $
   ,oo=oo   $
   ,datasetname $
   ,stats=stats   $
   ,mo=mo   $
   ,a_nodata=a_nodata   $
   ,unsetstats=unsetstats   $
   ,unsetmd=unsetmd   $
   ,a_ullr=a_ullr   $
   ,gdal_edit $
   ,gcp=gcp   $
   ,help_general=help_general   $
   ,approx_stats=approx_stats   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_edit_py($
   unsetgt=unsetgt   $
   ,tr=tr   $
   ,ro=ro   $
   ,a_srs=a_srs   $
   ,oo=oo   $
   ,datasetname $
   ,stats=stats   $
   ,mo=mo   $
   ,a_nodata=a_nodata   $
   ,unsetstats=unsetstats   $
   ,unsetmd=unsetmd   $
   ,a_ullr=a_ullr   $
   ,gdal_edit $
   ,gcp=gcp   $
   ,help_general=help_general   $
   ,approx_stats=approx_stats   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
