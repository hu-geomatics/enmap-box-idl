function hubGDALWrapper::pct2rgb_py $
   ,dest_file $
   ,of_=of_   $
   ,source_file $
   ,rgba=rgba   $
   ,b=b   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'pct2rgb.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"pct2rgb.py","IDLNAME":"pct2rgb_py","DESCRIPTION":["Convert an 8bit paletted image to 24bit RGB","This utility will convert a pseudocolor band on the input file into an output RGB file of the desired format.","NOTE: pct2rgb.py is a Python script, and will only work if GDAL was built with Python support.","The new `-expand rgb|rgba` option of gdal_translate obsoletes that utility. "],"PARAMETERS":[{"GDALNAME":"dest_file","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dest_file"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"source_file","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"source_file"},{"GDALNAME":"-rgba","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"rgba"},{"GDALNAME":"-b","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('pct2rgb.py' $
      , dest_file $
      , of_=of_   $
      , source_file $
      , rgba=rgba   $
      , b=b   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::pct2rgb_py $
   ,dest_file $
   ,of_=of_   $
   ,source_file $
   ,rgba=rgba   $
   ,b=b   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.pct2rgb_py($
   dest_file $
   ,of_=of_   $
   ,source_file $
   ,rgba=rgba   $
   ,b=b   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
