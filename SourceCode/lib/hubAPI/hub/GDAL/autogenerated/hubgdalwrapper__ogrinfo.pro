function hubGDALWrapper::ogrinfo $
   ,so=so   $
   ,ro=ro   $
   ,spat=spat   $
   ,where=where   $
   ,oo=oo   $
   ,fields=fields   $
   ,geom1=geom1   $
   ,geomfield=geomfield   $
   ,q=q   $
   ,dialect=dialect   $
   ,fid=fid   $
   ,sql=sql   $
   ,al=al   $
   ,layer $
   ,formats=formats   $
   ,help_general=help_general   $
   ,datasource_name $
   ,version=version   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'ogrinfo'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"ogrinfo","IDLNAME":"ogrinfo","DESCRIPTION":["lists information about an OGR supported data source","The ogrinfo program lists various information about an OGR supported data source to stdout (the terminal).","If no layer names are passed then ogrinfo will report a list of available layers (and their layerwide geometry type). If layer name(s) are given then their extents, coordinate system, feature count, geometry type, schema and all features matching query parameters will be reported to the terminal. If no query parameters are provided, all features are reported.","Geometries are reported in OGC WKT format."],"PARAMETERS":[{"GDALNAME":"-so","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"so"},{"GDALNAME":"-ro","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ro"},{"GDALNAME":"-spat","GDALVALUE":"xmin ymin xmax ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"spat"},{"GDALNAME":"-where","GDALVALUE":"restricted_where","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"where"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"oo"},{"GDALNAME":"-fields","GDALVALUE":"{YES/NO}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fields"},{"GDALNAME":"-geom","GDALVALUE":"{YES/NO/SUMMARY}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"geom1"},{"GDALNAME":"-geomfield","GDALVALUE":"field","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"geomfield"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-dialect","GDALVALUE":"dialect","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"dialect"},{"GDALNAME":"-fid","GDALVALUE":"fid","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fid"},{"GDALNAME":"-sql","GDALVALUE":"statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sql"},{"GDALNAME":"-al","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"al"},{"GDALNAME":"layer","GDALVALUE":"[layer ...]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":1,"IDLNAME":"layer"},{"GDALNAME":"-formats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"datasource_name","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"datasource_name"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('ogrinfo' $
      , so=so   $
      , ro=ro   $
      , spat=spat   $
      , where=where   $
      , oo=oo   $
      , fields=fields   $
      , geom1=geom1   $
      , geomfield=geomfield   $
      , q=q   $
      , dialect=dialect   $
      , fid=fid   $
      , sql=sql   $
      , al=al   $
      , layer $
      , formats=formats   $
      , help_general=help_general   $
      , datasource_name $
      , version=version   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::ogrinfo $
   ,so=so   $
   ,ro=ro   $
   ,spat=spat   $
   ,where=where   $
   ,oo=oo   $
   ,fields=fields   $
   ,geom1=geom1   $
   ,geomfield=geomfield   $
   ,q=q   $
   ,dialect=dialect   $
   ,fid=fid   $
   ,sql=sql   $
   ,al=al   $
   ,layer $
   ,formats=formats   $
   ,help_general=help_general   $
   ,datasource_name $
   ,version=version   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.ogrinfo($
   so=so   $
   ,ro=ro   $
   ,spat=spat   $
   ,where=where   $
   ,oo=oo   $
   ,fields=fields   $
   ,geom1=geom1   $
   ,geomfield=geomfield   $
   ,q=q   $
   ,dialect=dialect   $
   ,fid=fid   $
   ,sql=sql   $
   ,al=al   $
   ,layer $
   ,formats=formats   $
   ,help_general=help_general   $
   ,datasource_name $
   ,version=version   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
