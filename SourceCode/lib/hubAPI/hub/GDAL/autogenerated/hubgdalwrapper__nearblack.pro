function hubGDALWrapper::nearblack $
   ,q=q   $
   ,of_=of_   $
   ,infile $
   ,setalpha=setalpha   $
   ,nb=nb   $
   ,near=near   $
   ,setmask=setmask   $
   ,o1=o1   $
   ,white=white   $
   ,co1=co1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'nearblack'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"nearblack","IDLNAME":"nearblack","DESCRIPTION":["convert nearly black/white borders to black","This utility will scan an image and try to set all pixels that are nearly or exactly black, white or one or more custom colors around the collar to black or white. This is often used to \"fix up\" lossy compressed airphotos so that color pixels can be treated as transparent when mosaicking.","The algorithm processes the image one scanline at a time. A scan \"in\" is done from either end setting pixels to black or white until at least \"non_black_pixels\" pixels that are more than \"dist\" gray levels away from black, white or custom colors have been encountered at which point the scan stops. The nearly black, white or custom color pixels are set to black or white. The algorithm also scans from top to bottom and from bottom to top to identify indentations in the top or bottom.","The processing is all done in 8bit (Bytes).","If the output file is omitted, the processed results will be written back to the input file - which must support update. "],"PARAMETERS":[{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"infile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"infile"},{"GDALNAME":"-setalpha","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"setalpha"},{"GDALNAME":"-nb","GDALVALUE":"non_black_pixels","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nb"},{"GDALNAME":"-near","GDALVALUE":"dist","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"near"},{"GDALNAME":"-setmask","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"setmask"},{"GDALNAME":"-o","GDALVALUE":"outfile","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"o1"},{"GDALNAME":"-white","GDALVALUE":"| [-color c1,c2,c3...cn]*","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"white"},{"GDALNAME":"-co","GDALVALUE":"\"NAME=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('nearblack' $
      , q=q   $
      , of_=of_   $
      , infile $
      , setalpha=setalpha   $
      , nb=nb   $
      , near=near   $
      , setmask=setmask   $
      , o1=o1   $
      , white=white   $
      , co1=co1   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::nearblack $
   ,q=q   $
   ,of_=of_   $
   ,infile $
   ,setalpha=setalpha   $
   ,nb=nb   $
   ,near=near   $
   ,setmask=setmask   $
   ,o1=o1   $
   ,white=white   $
   ,co1=co1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.nearblack($
   q=q   $
   ,of_=of_   $
   ,infile $
   ,setalpha=setalpha   $
   ,nb=nb   $
   ,near=near   $
   ,setmask=setmask   $
   ,o1=o1   $
   ,white=white   $
   ,co1=co1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
