function hubGDALWrapper::gdal2tiles_py $
   ,u=u   $
   ,s=s   $
   ,output_dir $
   ,e=e   $
   ,n=n   $
   ,g=g   $
   ,z=z   $
   ,w=w   $
   ,k=k   $
   ,h1=h1   $
   ,p=p   $
   ,v1=v1   $
   ,b=b   $
   ,input_file $
   ,r=r   $
   ,a=a   $
   ,c1=c1   $
   ,t=t   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal2tiles.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal2tiles.py","IDLNAME":"gdal2tiles_py","DESCRIPTION":["generates directory with TMS tiles, KMLs and simple web viewers","This utility generates a directory with small tiles and metadata, following the OSGeo Tile Map Service Specification. Simple web pages with viewers based on Google Maps and OpenLayers are generated as well - so anybody can comfortably explore your maps on-line and you do not need to install or configure any special software (like MapServer) and the map displays very fast in the web browser. You only need to upload the generated directory onto a web server.","GDAL2Tiles also creates the necessary metadata for Google Earth (KML SuperOverlay), in case the supplied map uses EPSG:4326 projection.","World files and embedded georeferencing is used during tile generation, but you can publish a picture without proper georeferencing too.","Options for generated Google Earth SuperOverlay metadata ","Options for generated HTML viewers a la Google Maps ","NOTE: gdal2tiles.py is a Python script that needs to be run against \"new generation\" Python GDAL binding. "],"PARAMETERS":[{"GDALNAME":"-u","GDALVALUE":"url","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"u"},{"GDALNAME":"-s","GDALVALUE":"srs","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"s"},{"GDALNAME":"output_dir","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_dir"},{"GDALNAME":"-e","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"e"},{"GDALNAME":"-n","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"n"},{"GDALNAME":"-g","GDALVALUE":"googlekey","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"g"},{"GDALNAME":"-z","GDALVALUE":"zoom","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"z"},{"GDALNAME":"-w","GDALVALUE":"webviewer","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"w"},{"GDALNAME":"-k","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"k"},{"GDALNAME":"-h","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"h1"},{"GDALNAME":"-p","GDALVALUE":"profile","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"p"},{"GDALNAME":"-v","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"v1"},{"GDALNAME":"-b","GDALVALUE":"bingkey","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"input_file","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"input_file"},{"GDALNAME":"-r","GDALVALUE":"resampling","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"r"},{"GDALNAME":"-a","GDALVALUE":"nodata","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a"},{"GDALNAME":"-c","GDALVALUE":"copyright","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"c1"},{"GDALNAME":"-t","GDALVALUE":"title","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"t"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal2tiles.py' $
      , u=u   $
      , s=s   $
      , output_dir $
      , e=e   $
      , n=n   $
      , g=g   $
      , z=z   $
      , w=w   $
      , k=k   $
      , h1=h1   $
      , p=p   $
      , v1=v1   $
      , b=b   $
      , input_file $
      , r=r   $
      , a=a   $
      , c1=c1   $
      , t=t   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal2tiles_py $
   ,u=u   $
   ,s=s   $
   ,output_dir $
   ,e=e   $
   ,n=n   $
   ,g=g   $
   ,z=z   $
   ,w=w   $
   ,k=k   $
   ,h1=h1   $
   ,p=p   $
   ,v1=v1   $
   ,b=b   $
   ,input_file $
   ,r=r   $
   ,a=a   $
   ,c1=c1   $
   ,t=t   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal2tiles_py($
   u=u   $
   ,s=s   $
   ,output_dir $
   ,e=e   $
   ,n=n   $
   ,g=g   $
   ,z=z   $
   ,w=w   $
   ,k=k   $
   ,h1=h1   $
   ,p=p   $
   ,v1=v1   $
   ,b=b   $
   ,input_file $
   ,r=r   $
   ,a=a   $
   ,c1=c1   $
   ,t=t   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
