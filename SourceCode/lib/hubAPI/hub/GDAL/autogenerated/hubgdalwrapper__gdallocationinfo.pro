function hubGDALWrapper::gdallocationinfo $
   ,overview=overview   $
   ,xml=xml   $
   ,x1 $
   ,srcfile $
   ,b=b   $
   ,geoloc=geoloc   $
   ,l_srs=l_srs   $
   ,wgs84=wgs84   $
   ,help_general=help_general   $
   ,lifonly=lifonly   $
   ,valonly=valonly   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdallocationinfo'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdallocationinfo","IDLNAME":"gdallocationinfo","DESCRIPTION":["raster query tool","The gdallocationinfo utility provide a mechanism to query information about a pixel given it`s location in one of a variety of coordinate systems. Several reporting options are provided.","This utility is intended to provide a variety of information about a pixel. Currently it reports three things:","The pixel selected is requested by x/y coordinate on the commandline, or read from stdin. More than one coordinate pair can be supplied when reading coordinatesis from stdin. By default pixel/line coordinates are expected. However with use of the -geoloc, -wgs84, or -l_srs switches it is possible to specify the location in other coordinate systems.","The default report is in a human readable text format. It is possible to instead request xml output with the -xml switch.","For scripting purposes, the -valonly and -lifonly switches are provided to restrict output to the actual pixel values, or the LocationInfo files identified for the pixel.","It is anticipated that additional reporting capabilities will be added to gdallocationinfo in the future.","Simple example reporting on pixel (256,256) on the file utm.tif.","Query a VRT file providing the location in WGS84, and getting the result in xml."],"PARAMETERS":[{"GDALNAME":"-overview","GDALVALUE":"overview_level","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"overview"},{"GDALNAME":"-xml","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"xml"},{"GDALNAME":"x","GDALVALUE":"y","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"x1"},{"GDALNAME":"srcfile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"srcfile"},{"GDALNAME":"-b","GDALVALUE":"band]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"-geoloc","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"geoloc"},{"GDALNAME":"-l_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"l_srs"},{"GDALNAME":"-wgs84","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"wgs84"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-lifonly","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"lifonly"},{"GDALNAME":"-valonly","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"valonly"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdallocationinfo' $
      , overview=overview   $
      , xml=xml   $
      , x1 $
      , srcfile $
      , b=b   $
      , geoloc=geoloc   $
      , l_srs=l_srs   $
      , wgs84=wgs84   $
      , help_general=help_general   $
      , lifonly=lifonly   $
      , valonly=valonly   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdallocationinfo $
   ,overview=overview   $
   ,xml=xml   $
   ,x1 $
   ,srcfile $
   ,b=b   $
   ,geoloc=geoloc   $
   ,l_srs=l_srs   $
   ,wgs84=wgs84   $
   ,help_general=help_general   $
   ,lifonly=lifonly   $
   ,valonly=valonly   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdallocationinfo($
   overview=overview   $
   ,xml=xml   $
   ,x1 $
   ,srcfile $
   ,b=b   $
   ,geoloc=geoloc   $
   ,l_srs=l_srs   $
   ,wgs84=wgs84   $
   ,help_general=help_general   $
   ,lifonly=lifonly   $
   ,valonly=valonly   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
