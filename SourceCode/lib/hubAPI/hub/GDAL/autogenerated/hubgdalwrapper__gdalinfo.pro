function hubGDALWrapper::gdalinfo $
   ,norat=norat   $
   ,mdd=mdd   $
   ,noct=noct   $
   ,nogcp=nogcp   $
   ,proj4=proj4   $
   ,oo=oo   $
   ,stats=stats   $
   ,datasetname $
   ,nofl=nofl   $
   ,hist=hist   $
   ,checksum=checksum   $
   ,mm=mm   $
   ,listmdd=listmdd   $
   ,sd=sd   $
   ,help_general=help_general   $
   ,nomd=nomd   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdalinfo'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdalinfo","IDLNAME":"gdalinfo","DESCRIPTION":["lists information about a raster dataset","The gdalinfo program lists various information about a GDAL supported raster dataset. ","The gdalinfo will report all of the following (if known):"],"PARAMETERS":[{"GDALNAME":"-norat","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"norat"},{"GDALNAME":"-mdd","GDALVALUE":"domain|`all`]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mdd"},{"GDALNAME":"-noct","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"noct"},{"GDALNAME":"-nogcp","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nogcp"},{"GDALNAME":"-proj4","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"proj4"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"oo"},{"GDALNAME":"-stats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"stats"},{"GDALNAME":"datasetname","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"datasetname"},{"GDALNAME":"-nofl","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nofl"},{"GDALNAME":"-hist","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"hist"},{"GDALNAME":"-checksum","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"checksum"},{"GDALNAME":"-mm","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mm"},{"GDALNAME":"-listmdd","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"listmdd"},{"GDALNAME":"-sd","GDALVALUE":"subdataset","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sd"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-nomd","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nomd"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdalinfo' $
      , norat=norat   $
      , mdd=mdd   $
      , noct=noct   $
      , nogcp=nogcp   $
      , proj4=proj4   $
      , oo=oo   $
      , stats=stats   $
      , datasetname $
      , nofl=nofl   $
      , hist=hist   $
      , checksum=checksum   $
      , mm=mm   $
      , listmdd=listmdd   $
      , sd=sd   $
      , help_general=help_general   $
      , nomd=nomd   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdalinfo $
   ,norat=norat   $
   ,mdd=mdd   $
   ,noct=noct   $
   ,nogcp=nogcp   $
   ,proj4=proj4   $
   ,oo=oo   $
   ,stats=stats   $
   ,datasetname $
   ,nofl=nofl   $
   ,hist=hist   $
   ,checksum=checksum   $
   ,mm=mm   $
   ,listmdd=listmdd   $
   ,sd=sd   $
   ,help_general=help_general   $
   ,nomd=nomd   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdalinfo($
   norat=norat   $
   ,mdd=mdd   $
   ,noct=noct   $
   ,nogcp=nogcp   $
   ,proj4=proj4   $
   ,oo=oo   $
   ,stats=stats   $
   ,datasetname $
   ,nofl=nofl   $
   ,hist=hist   $
   ,checksum=checksum   $
   ,mm=mm   $
   ,listmdd=listmdd   $
   ,sd=sd   $
   ,help_general=help_general   $
   ,nomd=nomd   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
