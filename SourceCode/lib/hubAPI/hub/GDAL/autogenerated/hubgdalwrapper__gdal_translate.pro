function hubGDALWrapper::gdal_translate $
   ,projwin=projwin   $
   ,epo=epo   $
   ,src_dataset $
   ,oo=oo   $
   ,mask=mask   $
   ,outsize=outsize   $
   ,mo=mo   $
   ,ot=ot   $
   ,exponent=exponent   $
   ,q=q   $
   ,of_=of_   $
   ,dst_dataset $
   ,scale=scale   $
   ,b=b   $
   ,unscale=unscale   $
   ,r=r   $
   ,eco=eco   $
   ,gcp=gcp   $
   ,help_general=help_general   $
   ,norat=norat   $
   ,strict=strict   $
   ,tr=tr   $
   ,a_srs=a_srs   $
   ,stats=stats   $
   ,co1=co1   $
   ,expand=expand   $
   ,a_nodata=a_nodata   $
   ,sds=sds   $
   ,a_ullr=a_ullr   $
   ,srcwin=srcwin   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_translate'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_translate","IDLNAME":"gdal_translate","DESCRIPTION":["converts raster data between different formats","The gdal_translate utility can be used to convert raster data between different formats, potentially performing some operations like subsettings, resampling, and rescaling pixels in the process.","Starting with GDAL 1.8.0, to create a JPEG-compressed TIFF with internal mask from a RGBA dataset : ","Starting with GDAL 1.8.0, to create a RGBA dataset from a RGB dataset with a mask : "],"PARAMETERS":[{"GDALNAME":"-projwin","GDALVALUE":"ulx uly lrx lry","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"projwin"},{"GDALNAME":"-epo","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"epo"},{"GDALNAME":"src_dataset","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"src_dataset"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"oo"},{"GDALNAME":"-mask","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mask"},{"GDALNAME":"-outsize","GDALVALUE":"xsize[%] ysize[%]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"outsize"},{"GDALNAME":"-mo","GDALVALUE":"\"META-TAG=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mo"},{"GDALNAME":"-ot","GDALVALUE":"{Byte/Int16/UInt16/UInt32/Int32/Float32/Float64/ CInt16/CInt32/CFloat32/CFloat64}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-exponent","GDALVALUE":"[_bn] exp_val]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"exponent"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"dst_dataset","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dst_dataset"},{"GDALNAME":"-scale","GDALVALUE":"[_bn] [src_min src_max [dst_min dst_max]]]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"scale"},{"GDALNAME":"-b","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"-unscale","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unscale"},{"GDALNAME":"-r","GDALVALUE":"{nearest,bilinear,cubic,cubicspline,lanczos,average,mode}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"r"},{"GDALNAME":"-eco","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"eco"},{"GDALNAME":"-gcp","GDALVALUE":"pixel line easting northing [elevation]]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"gcp"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-norat","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"norat"},{"GDALNAME":"-strict","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"strict"},{"GDALNAME":"-tr","GDALVALUE":"xres yres","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tr"},{"GDALNAME":"-a_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_srs"},{"GDALNAME":"-stats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"stats"},{"GDALNAME":"-co","GDALVALUE":"\"NAME=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"-expand","GDALVALUE":"{gray|rgb|rgba}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"expand"},{"GDALNAME":"-a_nodata","GDALVALUE":"value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_nodata"},{"GDALNAME":"-sds","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sds"},{"GDALNAME":"-a_ullr","GDALVALUE":"ulx uly lrx lry","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_ullr"},{"GDALNAME":"-srcwin","GDALVALUE":"xoff yoff xsize ysize","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"srcwin"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_translate' $
      , projwin=projwin   $
      , epo=epo   $
      , src_dataset $
      , oo=oo   $
      , mask=mask   $
      , outsize=outsize   $
      , mo=mo   $
      , ot=ot   $
      , exponent=exponent   $
      , q=q   $
      , of_=of_   $
      , dst_dataset $
      , scale=scale   $
      , b=b   $
      , unscale=unscale   $
      , r=r   $
      , eco=eco   $
      , gcp=gcp   $
      , help_general=help_general   $
      , norat=norat   $
      , strict=strict   $
      , tr=tr   $
      , a_srs=a_srs   $
      , stats=stats   $
      , co1=co1   $
      , expand=expand   $
      , a_nodata=a_nodata   $
      , sds=sds   $
      , a_ullr=a_ullr   $
      , srcwin=srcwin   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_translate $
   ,projwin=projwin   $
   ,epo=epo   $
   ,src_dataset $
   ,oo=oo   $
   ,mask=mask   $
   ,outsize=outsize   $
   ,mo=mo   $
   ,ot=ot   $
   ,exponent=exponent   $
   ,q=q   $
   ,of_=of_   $
   ,dst_dataset $
   ,scale=scale   $
   ,b=b   $
   ,unscale=unscale   $
   ,r=r   $
   ,eco=eco   $
   ,gcp=gcp   $
   ,help_general=help_general   $
   ,norat=norat   $
   ,strict=strict   $
   ,tr=tr   $
   ,a_srs=a_srs   $
   ,stats=stats   $
   ,co1=co1   $
   ,expand=expand   $
   ,a_nodata=a_nodata   $
   ,sds=sds   $
   ,a_ullr=a_ullr   $
   ,srcwin=srcwin   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_translate($
   projwin=projwin   $
   ,epo=epo   $
   ,src_dataset $
   ,oo=oo   $
   ,mask=mask   $
   ,outsize=outsize   $
   ,mo=mo   $
   ,ot=ot   $
   ,exponent=exponent   $
   ,q=q   $
   ,of_=of_   $
   ,dst_dataset $
   ,scale=scale   $
   ,b=b   $
   ,unscale=unscale   $
   ,r=r   $
   ,eco=eco   $
   ,gcp=gcp   $
   ,help_general=help_general   $
   ,norat=norat   $
   ,strict=strict   $
   ,tr=tr   $
   ,a_srs=a_srs   $
   ,stats=stats   $
   ,co1=co1   $
   ,expand=expand   $
   ,a_nodata=a_nodata   $
   ,sds=sds   $
   ,a_ullr=a_ullr   $
   ,srcwin=srcwin   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
