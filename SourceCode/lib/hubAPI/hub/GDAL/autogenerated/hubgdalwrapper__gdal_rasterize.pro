function hubGDALWrapper::gdal_rasterize $
   ,l=l   $
   ,src_datasource $
   ,tr=tr   $
   ,dst_filename $
   ,where=where   $
   ,at=at   $
   ,a_srs=a_srs   $
   ,_3d=_3d   $
   ,i1=i1   $
   ,co1=co1   $
   ,ot=ot   $
   ,init=init   $
   ,a_nodata=a_nodata   $
   ,of_=of_   $
   ,q=q   $
   ,te=te   $
   ,b1=b1   $
   ,sql=sql   $
   ,a1=a1   $
   ,burn=burn   $
   ,tap=tap   $
   ,ts=ts   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_rasterize'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_rasterize","IDLNAME":"gdal_rasterize","DESCRIPTION":["burns vector geometries into a raster","This program burns vector geometries (points, lines, and polygons) into the raster band(s) of a raster image. Vectors are read from OGR supported vector formats.","Note that the vector data must be in the same coordinate system as the raster data; on the fly reprojection is not provided.","Since GDAL 1.8.0, the target GDAL file can be created by gdal_rasterize. Either the -tr or -ts option must be used in that case.","The following would burn all polygons from mask.shp into the RGB TIFF file work.tif with the color red (RGB = 255,0,0).","The following would burn all \"class A\" buildings into the output elevation file, pulling the top elevation from the ROOF_H attribute."],"PARAMETERS":[{"GDALNAME":"-l","GDALVALUE":"layername]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"l"},{"GDALNAME":"src_datasource","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"src_datasource"},{"GDALNAME":"-tr","GDALVALUE":"xres yres","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tr"},{"GDALNAME":"dst_filename","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dst_filename"},{"GDALNAME":"-where","GDALVALUE":"expression","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"where"},{"GDALNAME":"-at","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"at"},{"GDALNAME":"-a_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_srs"},{"GDALNAME":"-3d","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"_3d"},{"GDALNAME":"-i","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"i1"},{"GDALNAME":"-co","GDALVALUE":"\"NAME=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"-ot","GDALVALUE":"{Byte/Int16/UInt16/UInt32/Int32/Float32/Float64/ CInt16/CInt32/CFloat32/CFloat64}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-init","GDALVALUE":"value]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"init"},{"GDALNAME":"-a_nodata","GDALVALUE":"value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_nodata"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-te","GDALVALUE":"xmin ymin xmax ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"te"},{"GDALNAME":"-b","GDALVALUE":"band]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b1"},{"GDALNAME":"-sql","GDALVALUE":"select_statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sql"},{"GDALNAME":"-a","GDALVALUE":"attribute_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a1"},{"GDALNAME":"-burn","GDALVALUE":"value]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"burn"},{"GDALNAME":"-tap","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tap"},{"GDALNAME":"-ts","GDALVALUE":"width height","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ts"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_rasterize' $
      , l=l   $
      , src_datasource $
      , tr=tr   $
      , dst_filename $
      , where=where   $
      , at=at   $
      , a_srs=a_srs   $
      , _3d=_3d   $
      , i1=i1   $
      , co1=co1   $
      , ot=ot   $
      , init=init   $
      , a_nodata=a_nodata   $
      , of_=of_   $
      , q=q   $
      , te=te   $
      , b1=b1   $
      , sql=sql   $
      , a1=a1   $
      , burn=burn   $
      , tap=tap   $
      , ts=ts   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_rasterize $
   ,l=l   $
   ,src_datasource $
   ,tr=tr   $
   ,dst_filename $
   ,where=where   $
   ,at=at   $
   ,a_srs=a_srs   $
   ,_3d=_3d   $
   ,i1=i1   $
   ,co1=co1   $
   ,ot=ot   $
   ,init=init   $
   ,a_nodata=a_nodata   $
   ,of_=of_   $
   ,q=q   $
   ,te=te   $
   ,b1=b1   $
   ,sql=sql   $
   ,a1=a1   $
   ,burn=burn   $
   ,tap=tap   $
   ,ts=ts   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_rasterize($
   l=l   $
   ,src_datasource $
   ,tr=tr   $
   ,dst_filename $
   ,where=where   $
   ,at=at   $
   ,a_srs=a_srs   $
   ,_3d=_3d   $
   ,i1=i1   $
   ,co1=co1   $
   ,ot=ot   $
   ,init=init   $
   ,a_nodata=a_nodata   $
   ,of_=of_   $
   ,q=q   $
   ,te=te   $
   ,b1=b1   $
   ,sql=sql   $
   ,a1=a1   $
   ,burn=burn   $
   ,tap=tap   $
   ,ts=ts   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
