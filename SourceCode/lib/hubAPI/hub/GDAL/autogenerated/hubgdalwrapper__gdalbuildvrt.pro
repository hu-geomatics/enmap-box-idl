function hubGDALWrapper::gdalbuildvrt $
   ,overwrite=overwrite   $
   ,gdalfile $
   ,tr=tr   $
   ,allow_projection_difference=allow_projection_difference   $
   ,a_srs=a_srs   $
   ,hidenodata=hidenodata   $
   ,srcnodata=srcnodata   $
   ,q=q   $
   ,vrtnodata=vrtnodata   $
   ,input_file_list=input_file_list   $
   ,te=te   $
   ,b=b   $
   ,tileindex=tileindex   $
   ,resolution=resolution   $
   ,separate=separate   $
   ,tap=tap   $
   ,sd=sd   $
   ,output $
   ,addalpha=addalpha   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdalbuildvrt'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdalbuildvrt","IDLNAME":"gdalbuildvrt","DESCRIPTION":["Builds a VRT from a list of datasets. (compiled by default since GDAL 1.6.1)","If one GDAL dataset is made of several subdatasets and has 0 raster bands, all the subdatasets will be added to the VRT rather than the dataset itself.","gdalbuildvrt does some amount of checks to assure that all files that will be put in the resulting VRT have similar characteristics : number of bands, projection, color interpretation... If not, files that do not match the common characteristics will be skipped. (This is only true in the default mode, and not when using the -separate option)","If there is some amount of spatial overlapping between files, the order may depend on the order they are inserted in the VRT file, but this behaviour should not be relied on.","This utility is somehow equivalent to the gdal_vrtmerge.py utility and is build by default in GDAL 1.6.1.","Make a virtual mosaic from all TIFF files contained in a directory : ","Make a virtual mosaic from files whose name is specified in a text file : ","Make a RGB virtual mosaic from 3 single-band input files : ","Make a virtual mosaic with blue background colour (RGB: 0 0 255) : "],"PARAMETERS":[{"GDALNAME":"-overwrite","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"overwrite"},{"GDALNAME":"gdalfile","GDALVALUE":"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"gdalfile"},{"GDALNAME":"-tr","GDALVALUE":"xres yres","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tr"},{"GDALNAME":"-allow_projection_difference","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"allow_projection_difference"},{"GDALNAME":"-a_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_srs"},{"GDALNAME":"-hidenodata","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"hidenodata"},{"GDALNAME":"-srcnodata","GDALVALUE":"\"value [value...]\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"srcnodata"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-vrtnodata","GDALVALUE":"\"value [value...]\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"vrtnodata"},{"GDALNAME":"-input_file_list","GDALVALUE":"my_liste.txt","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"input_file_list"},{"GDALNAME":"-te","GDALVALUE":"xmin ymin xmax ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"te"},{"GDALNAME":"-b","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"-tileindex","GDALVALUE":"field_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tileindex"},{"GDALNAME":"-resolution","GDALVALUE":"{highest|lowest|average|user}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"resolution"},{"GDALNAME":"-separate","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"separate"},{"GDALNAME":"-tap","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tap"},{"GDALNAME":"-sd","GDALVALUE":"subdataset","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sd"},{"GDALNAME":"output","GDALVALUE":".vrt","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output"},{"GDALNAME":"-addalpha","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"addalpha"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdalbuildvrt' $
      , overwrite=overwrite   $
      , gdalfile $
      , tr=tr   $
      , allow_projection_difference=allow_projection_difference   $
      , a_srs=a_srs   $
      , hidenodata=hidenodata   $
      , srcnodata=srcnodata   $
      , q=q   $
      , vrtnodata=vrtnodata   $
      , input_file_list=input_file_list   $
      , te=te   $
      , b=b   $
      , tileindex=tileindex   $
      , resolution=resolution   $
      , separate=separate   $
      , tap=tap   $
      , sd=sd   $
      , output $
      , addalpha=addalpha   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdalbuildvrt $
   ,overwrite=overwrite   $
   ,gdalfile $
   ,tr=tr   $
   ,allow_projection_difference=allow_projection_difference   $
   ,a_srs=a_srs   $
   ,hidenodata=hidenodata   $
   ,srcnodata=srcnodata   $
   ,q=q   $
   ,vrtnodata=vrtnodata   $
   ,input_file_list=input_file_list   $
   ,te=te   $
   ,b=b   $
   ,tileindex=tileindex   $
   ,resolution=resolution   $
   ,separate=separate   $
   ,tap=tap   $
   ,sd=sd   $
   ,output $
   ,addalpha=addalpha   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdalbuildvrt($
   overwrite=overwrite   $
   ,gdalfile $
   ,tr=tr   $
   ,allow_projection_difference=allow_projection_difference   $
   ,a_srs=a_srs   $
   ,hidenodata=hidenodata   $
   ,srcnodata=srcnodata   $
   ,q=q   $
   ,vrtnodata=vrtnodata   $
   ,input_file_list=input_file_list   $
   ,te=te   $
   ,b=b   $
   ,tileindex=tileindex   $
   ,resolution=resolution   $
   ,separate=separate   $
   ,tap=tap   $
   ,sd=sd   $
   ,output $
   ,addalpha=addalpha   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
