function hubGDALWrapper::gdal_merge_py $
   ,input_files $
   ,ps=ps   $
   ,n=n   $
   ,co1=co1   $
   ,ot=ot   $
   ,init=init   $
   ,of_=of_   $
   ,ul_lr=ul_lr   $
   ,createonly=createonly   $
   ,v1=v1   $
   ,pct=pct   $
   ,separate=separate   $
   ,o1=o1   $
   ,tap=tap   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_merge.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_merge.py","IDLNAME":"gdal_merge_py","DESCRIPTION":["mosaics a set of images","This utility will automatically mosaic a set of images. All the images must be in the same coordinate system and have a matching number of bands, but they may be overlapping, and at different resolutions. In areas of overlap, the last image will be copied over earlier ones.","NOTE: gdal_merge.py is a Python script, and will only work if GDAL was built with Python support.","Create an image with the pixels in all bands initialized to 255.","Create an RGB image that shows blue in pixels with no data. The first two bands will be initialized to 0 and the third band will be initialized to 255."],"PARAMETERS":[{"GDALNAME":"input_files","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"input_files"},{"GDALNAME":"-ps","GDALVALUE":"pixelsize_x pixelsize_y","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ps"},{"GDALNAME":"-n","GDALVALUE":"nodata_value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"n"},{"GDALNAME":"-co","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"-ot","GDALVALUE":"datatype","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-init","GDALVALUE":"\"value [value...]\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"init"},{"GDALNAME":"-of","GDALVALUE":"out_format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"-ul_lr","GDALVALUE":"ulx uly lrx lry","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ul_lr"},{"GDALNAME":"-createonly","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"createonly"},{"GDALNAME":"-v","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"v1"},{"GDALNAME":"-pct","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"pct"},{"GDALNAME":"-separate","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"separate"},{"GDALNAME":"-o","GDALVALUE":"out_filename","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"o1"},{"GDALNAME":"-tap","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tap"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_merge.py' $
      , input_files $
      , ps=ps   $
      , n=n   $
      , co1=co1   $
      , ot=ot   $
      , init=init   $
      , of_=of_   $
      , ul_lr=ul_lr   $
      , createonly=createonly   $
      , v1=v1   $
      , pct=pct   $
      , separate=separate   $
      , o1=o1   $
      , tap=tap   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_merge_py $
   ,input_files $
   ,ps=ps   $
   ,n=n   $
   ,co1=co1   $
   ,ot=ot   $
   ,init=init   $
   ,of_=of_   $
   ,ul_lr=ul_lr   $
   ,createonly=createonly   $
   ,v1=v1   $
   ,pct=pct   $
   ,separate=separate   $
   ,o1=o1   $
   ,tap=tap   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_merge_py($
   input_files $
   ,ps=ps   $
   ,n=n   $
   ,co1=co1   $
   ,ot=ot   $
   ,init=init   $
   ,of_=of_   $
   ,ul_lr=ul_lr   $
   ,createonly=createonly   $
   ,v1=v1   $
   ,pct=pct   $
   ,separate=separate   $
   ,o1=o1   $
   ,tap=tap   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
