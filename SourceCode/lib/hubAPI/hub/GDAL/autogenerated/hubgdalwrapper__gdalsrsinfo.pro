function hubGDALWrapper::gdalsrsinfo $
   ,h1=h1   $
   ,o1=o1   $
   ,V1=V1   $
   ,srs_def $
   ,help_general=help_general   $
   ,p=p   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdalsrsinfo'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdalsrsinfo","IDLNAME":"gdalsrsinfo","DESCRIPTION":["lists info about a given SRS in number of formats (WKT, PROJ.4, etc.)","The gdalsrsinfo utility reports information about a given SRS from one of the following:"],"PARAMETERS":[{"GDALNAME":"-h","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"h1"},{"GDALNAME":"-o","GDALVALUE":"out_type={ default, all, wkt_all, proj4, wkt, wkt_simple, wkt_noct, wkt_esri, mapinfo, xml","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"o1"},{"GDALNAME":"-V","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"V1"},{"GDALNAME":"srs_def","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"srs_def"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-p","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"p"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdalsrsinfo' $
      , h1=h1   $
      , o1=o1   $
      , V1=V1   $
      , srs_def $
      , help_general=help_general   $
      , p=p   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdalsrsinfo $
   ,h1=h1   $
   ,o1=o1   $
   ,V1=V1   $
   ,srs_def $
   ,help_general=help_general   $
   ,p=p   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdalsrsinfo($
   h1=h1   $
   ,o1=o1   $
   ,V1=V1   $
   ,srs_def $
   ,help_general=help_general   $
   ,p=p   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
