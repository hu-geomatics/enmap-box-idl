function hubGDALWrapper::gdaldem $
   ,input_dem $
   ,compute_edges=compute_edges   $
   ,trigonometric=trigonometric   $
   ,alpha=alpha   $
   ,output_color_relief_map $
   ,hillshade $
   ,output_hillshade $
   ,q=q   $
   ,of_=of_   $
   ,output_aspect_map $
   ,az=az   $
   ,b=b   $
   ,TPI $
   ,combined=combined   $
   ,slope $
   ,alg=alg   $
   ,roughness $
   ,s1=s1   $
   ,alt=alt   $
   ,output_TRI_map $
   ,z1=z1   $
   ,output_slope_map $
   ,zero_for_flat=zero_for_flat   $
   ,co1=co1   $
   ,color_relief $
   ,exact_color_entry=exact_color_entry   $
   ,aspect $
   ,p=p   $
   ,output_roughness_map $
   ,color_text_file $
   ,TRI1 $
   ,output_TPI_map $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdaldem'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdaldem","IDLNAME":"gdaldem","DESCRIPTION":["Tools to analyze and visualize DEMs. (since GDAL 1.7.0)","This utility has 7 different modes : ","The following general options are available : ","For all algorithms, except color-relief, a nodata value in the target dataset will be emitted if at least one pixel set to the nodata value is found in the 3x3 window centered around each source pixel. The consequence is that there will be a 1-pixel border around each image set with nodata value. From GDAL 1.8.0, if -compute_edges is specified, gdaldem will compute values at image edges or if a nodata value is found in the 3x3 window, by interpolating missing values.","This command outputs an 8-bit raster with a nice shaded relief effect. It’s very useful for visualizing the terrain. You can optionally specify the azimuth and altitude of the light source, a vertical exaggeration factor and a scaling factor to account for differences between vertical and horizontal units.","The value 0 is used as the output nodata value.","The following specific options are available : ","This command will take a DEM raster and output a 32-bit float raster with slope values. You have the option of specifying the type of slope value you want: degrees or percent slope. In cases where the horizontal units differ from the vertical units, you can also supply a scaling factor.","The value -9999 is used as the output nodata value.","The following specific options are available : ","This command outputs a 32-bit float raster with values between 0° and 360° representing the azimuth that slopes are facing. The definition of the azimuth is such that : 0° means that the slope is facing the North, 90° it`s facing the East, 180° it`s facing the South and 270° it`s facing the West (provided that the top of your input raster is north oriented). The aspect value -9999 is used as the nodata value to indicate undefined aspect in flat areas with slope=0.","The following specifics options are available : ","By using those 2 options, the aspect returned by gdaldem aspect should be identical to the one of GRASS r.slope.aspect. Otherwise, it`s identical to the one of Matthew Perry`s aspect.cpp utility.","This command outputs a 3-band (RGB) or 4-band (RGBA) raster with values are computed from the elevation and a text-based color configuration file, containing the association between various elevation values and the corresponding wished color. By default, the colors between the given elevation values are blended smoothly and the result is a nice colorized DEM. The -exact_color_entry or -nearest_color_entry options can be used to avoid that linear interpolation for values that don`t match an index of the color configuration file.","The following specifics options are available : ","The color-relief mode is the only mode that supports VRT as output format. In that case, it will translate the color configuration file into appropriate LUT elements. Note that elevations specified as percentage will be translated as absolute values, which must be taken into account when the statistics of the source raster differ from the one that was used when building the VRT.","An extra column can be optionally added for the alpha component. If it is not specified, full opacity (255) is assumed.","Various field separators are accepted : comma, tabulation, spaces, `:`.","Common colors used by GRASS can also be specified by using their name, instead of the RGB triplet. The supported list is : white, black, red, green, blue, yellow, magenta, cyan, aqua, grey/gray, orange, brown, purple/violet and indigo.","Since GDAL 1.8.0, GMT .cpt palette files are also supported (COLOR_MODEL = RGB only).","Note: the syntax of the color configuration file is derived from the one supported by GRASS r.colors utility. ESRI HDR color table files (.clr) also match that syntax. The alpha component and the support of tab and comma as separators are GDAL specific extensions.","For example : ","This command outputs a single-band raster with values computed from the elevation. TRI stands for Terrain Ruggedness Index, which is defined as the mean difference between a central pixel and its surrounding cells (see Wilson et al 2007, Marine Geodesy 30:3-35).","The value -9999 is used as the output nodata value.","There are no specific options.","This command outputs a single-band raster with values computed from the elevation. TPI stands for Topographic Position Index, which is defined as the difference between a central pixel and the mean of its surrounding cells (see Wilson et al 2007, Marine Geodesy 30:3-35).","The value -9999 is used as the output nodata value.","There are no specific options.","This command outputs a single-band raster with values computed from the elevation. Roughness is the largest inter-cell difference of a central pixel and its surrounding cell, as defined in Wilson et al (2007, Marine Geodesy 30:3-35).","The value -9999 is used as the output nodata value.","There are no specific options.","Derived from code by Michael Shapiro, Olga Waupotitsch, Marjorie Larson, Jim Westervelt : U.S. Army CERL, 1993. GRASS 4.1 Reference Manual. U.S. Army Corps of Engineers, Construction Engineering Research Laboratories, Champaign, Illinois, 1-425.","Documentation of related GRASS utilities :"],"PARAMETERS":[{"GDALNAME":"input_dem","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"input_dem"},{"GDALNAME":"-compute_edges","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"compute_edges"},{"GDALNAME":"-trigonometric","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"trigonometric"},{"GDALNAME":"-alpha","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"alpha"},{"GDALNAME":"output_color_relief_map","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_color_relief_map"},{"GDALNAME":"hillshade","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"hillshade"},{"GDALNAME":"output_hillshade","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_hillshade"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"output_aspect_map","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_aspect_map"},{"GDALNAME":"-az","GDALVALUE":"Azimuth (default=315)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"az"},{"GDALNAME":"-b","GDALVALUE":"Band (default=1)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"TPI","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"TPI"},{"GDALNAME":"-combined","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"combined"},{"GDALNAME":"slope","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"slope"},{"GDALNAME":"-alg","GDALVALUE":"ZevenbergenThorne","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"alg"},{"GDALNAME":"roughness","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"roughness"},{"GDALNAME":"-s","GDALVALUE":"scale* (default=1)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"s1"},{"GDALNAME":"-alt","GDALVALUE":"Altitude (default=45)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"alt"},{"GDALNAME":"output_TRI_map","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_TRI_map"},{"GDALNAME":"-z","GDALVALUE":"ZFactor (default=1)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"z1"},{"GDALNAME":"output_slope_map","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_slope_map"},{"GDALNAME":"-zero_for_flat","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"zero_for_flat"},{"GDALNAME":"-co","GDALVALUE":"\"NAME=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"color-relief","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"color_relief"},{"GDALNAME":"-exact_color_entry","GDALVALUE":"| -nearest_color_entry","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"exact_color_entry"},{"GDALNAME":"aspect","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"aspect"},{"GDALNAME":"-p","GDALVALUE":"use percent slope (default=degrees)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"p"},{"GDALNAME":"output_roughness_map","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_roughness_map"},{"GDALNAME":"color_text_file","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"color_text_file"},{"GDALNAME":"TRI","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"TRI1"},{"GDALNAME":"output_TPI_map","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"output_TPI_map"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdaldem' $
      , input_dem $
      , compute_edges=compute_edges   $
      , trigonometric=trigonometric   $
      , alpha=alpha   $
      , output_color_relief_map $
      , hillshade $
      , output_hillshade $
      , q=q   $
      , of_=of_   $
      , output_aspect_map $
      , az=az   $
      , b=b   $
      , TPI $
      , combined=combined   $
      , slope $
      , alg=alg   $
      , roughness $
      , s1=s1   $
      , alt=alt   $
      , output_TRI_map $
      , z1=z1   $
      , output_slope_map $
      , zero_for_flat=zero_for_flat   $
      , co1=co1   $
      , color_relief $
      , exact_color_entry=exact_color_entry   $
      , aspect $
      , p=p   $
      , output_roughness_map $
      , color_text_file $
      , TRI1 $
      , output_TPI_map $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdaldem $
   ,input_dem $
   ,compute_edges=compute_edges   $
   ,trigonometric=trigonometric   $
   ,alpha=alpha   $
   ,output_color_relief_map $
   ,hillshade $
   ,output_hillshade $
   ,q=q   $
   ,of_=of_   $
   ,output_aspect_map $
   ,az=az   $
   ,b=b   $
   ,TPI $
   ,combined=combined   $
   ,slope $
   ,alg=alg   $
   ,roughness $
   ,s1=s1   $
   ,alt=alt   $
   ,output_TRI_map $
   ,z1=z1   $
   ,output_slope_map $
   ,zero_for_flat=zero_for_flat   $
   ,co1=co1   $
   ,color_relief $
   ,exact_color_entry=exact_color_entry   $
   ,aspect $
   ,p=p   $
   ,output_roughness_map $
   ,color_text_file $
   ,TRI1 $
   ,output_TPI_map $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdaldem($
   input_dem $
   ,compute_edges=compute_edges   $
   ,trigonometric=trigonometric   $
   ,alpha=alpha   $
   ,output_color_relief_map $
   ,hillshade $
   ,output_hillshade $
   ,q=q   $
   ,of_=of_   $
   ,output_aspect_map $
   ,az=az   $
   ,b=b   $
   ,TPI $
   ,combined=combined   $
   ,slope $
   ,alg=alg   $
   ,roughness $
   ,s1=s1   $
   ,alt=alt   $
   ,output_TRI_map $
   ,z1=z1   $
   ,output_slope_map $
   ,zero_for_flat=zero_for_flat   $
   ,co1=co1   $
   ,color_relief $
   ,exact_color_entry=exact_color_entry   $
   ,aspect $
   ,p=p   $
   ,output_roughness_map $
   ,color_text_file $
   ,TRI1 $
   ,output_TPI_map $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
