function hubGDALWrapper::gdal_proximity_py $
   ,ot=ot   $
   ,fixed_buf_val=fixed_buf_val   $
   ,dstfile $
   ,of_=of_   $
   ,values=values   $
   ,srcfile $
   ,dstband=dstband   $
   ,srcband=srcband   $
   ,distunits=distunits   $
   ,maxdist=maxdist   $
   ,nodata=nodata   $
   ,co1=co1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_proximity.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_proximity.py","IDLNAME":"gdal_proximity_py","DESCRIPTION":["produces a raster proximity map","The gdal_proximity.py script generates a raster proximity map indicating the distance from the center of each pixel to the center of the nearest pixel identified as a target pixel. Target pixels are those in the source raster for which the raster pixel value is in the set of target pixel values."],"PARAMETERS":[{"GDALNAME":"-ot","GDALVALUE":"Byte/Int16/Int32/Float32/etc","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-fixed-buf-val","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fixed_buf_val"},{"GDALNAME":"dstfile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dstfile"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"-values","GDALVALUE":"n,n,n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"values"},{"GDALNAME":"srcfile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"srcfile"},{"GDALNAME":"-dstband","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"dstband"},{"GDALNAME":"-srcband","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"srcband"},{"GDALNAME":"-distunits","GDALVALUE":"PIXEL/GEO","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"distunits"},{"GDALNAME":"-maxdist","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"maxdist"},{"GDALNAME":"-nodata","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nodata"},{"GDALNAME":"-co","GDALVALUE":"name=value]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_proximity.py' $
      , ot=ot   $
      , fixed_buf_val=fixed_buf_val   $
      , dstfile $
      , of_=of_   $
      , values=values   $
      , srcfile $
      , dstband=dstband   $
      , srcband=srcband   $
      , distunits=distunits   $
      , maxdist=maxdist   $
      , nodata=nodata   $
      , co1=co1   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_proximity_py $
   ,ot=ot   $
   ,fixed_buf_val=fixed_buf_val   $
   ,dstfile $
   ,of_=of_   $
   ,values=values   $
   ,srcfile $
   ,dstband=dstband   $
   ,srcband=srcband   $
   ,distunits=distunits   $
   ,maxdist=maxdist   $
   ,nodata=nodata   $
   ,co1=co1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_proximity_py($
   ot=ot   $
   ,fixed_buf_val=fixed_buf_val   $
   ,dstfile $
   ,of_=of_   $
   ,values=values   $
   ,srcfile $
   ,dstband=dstband   $
   ,srcband=srcband   $
   ,distunits=distunits   $
   ,maxdist=maxdist   $
   ,nodata=nodata   $
   ,co1=co1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
