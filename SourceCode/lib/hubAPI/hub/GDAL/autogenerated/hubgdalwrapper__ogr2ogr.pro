function hubGDALWrapper::ogr2ogr $
   ,fieldTypeToString=fieldTypeToString   $
   ,fieldmap=fieldmap   $
   ,skipfailures=skipfailures   $
   ,spat=spat   $
   ,nlt=nlt   $
   ,clipdstsql=clipdstsql   $
   ,s_srs=s_srs   $
   ,progress=progress   $
   ,geomfield=geomfield   $
   ,nln=nln   $
   ,doo=doo   $
   ,clipsrc1=clipsrc1   $
   ,splitlistfields=splitlistfields   $
   ,mapFieldType=mapFieldType   $
   ,fid=fid   $
   ,sql=sql   $
   ,t_srs=t_srs   $
   ,layer $
   ,clipsrcsql=clipsrcsql   $
   ,clipsrcwhere=clipsrcwhere   $
   ,help_general=help_general   $
   ,datelineoffset=datelineoffset   $
   ,gcp=gcp   $
   ,addfields=addfields   $
   ,src_datasource_name $
   ,dsco=dsco   $
   ,lco=lco   $
   ,explodecollections=explodecollections   $
   ,clipdstlayer=clipdstlayer   $
   ,dialect=dialect   $
   ,wrapdateline=wrapdateline   $
   ,simplify=simplify   $
   ,zfield=zfield   $
   ,f1=f1   $
   ,relaxedFieldNameMatch=relaxedFieldNameMatch   $
   ,append=append   $
   ,where=where   $
   ,dim=dim   $
   ,oo=oo   $
   ,clipdstwhere=clipdstwhere   $
   ,maxsubfields=maxsubfields   $
   ,update=update   $
   ,clipdst1=clipdst1   $
   ,forceNullable=forceNullable   $
   ,overwrite=overwrite   $
   ,a_srs=a_srs   $
   ,clipsrclayer=clipsrclayer   $
   ,gt_=gt_   $
   ,unsetFid=unsetFid   $
   ,select=select   $
   ,unsetFieldWidth=unsetFieldWidth   $
   ,dst_datasource_name $
   ,unsetDefault=unsetDefault   $
   ,order=order   $
   ,preserve_fid=preserve_fid   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'ogr2ogr'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"ogr2ogr","IDLNAME":"ogr2ogr","DESCRIPTION":["converts simple features data between file formats","This program can be used to convert simple features data between file formats performing various operations during the process such as spatial or attribute selections, reducing the set of attributes, setting the output coordinate system or even reprojecting the features during translation.","Srs_def can be a full WKT definition (hard to escape properly), or a well known definition (ie. EPSG:4326) or a file with a WKT definition.","Advanced options :","For PostgreSQL, the PG_USE_COPY config option can be set to YES for significantly insertion performance boot. See the PG driver documentation page.","More generally, consult the documentation page of the input and output drivers for performance hints.","More examples are given in the individual format pages. "],"PARAMETERS":[{"GDALNAME":"-fieldTypeToString","GDALVALUE":"All|(type1[,type2]*)","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fieldTypeToString"},{"GDALNAME":"-fieldmap","GDALVALUE":"identity | index1[,index2]*","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fieldmap"},{"GDALNAME":"-skipfailures","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"skipfailures"},{"GDALNAME":"-spat","GDALVALUE":"xmin ymin xmax ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"spat"},{"GDALNAME":"-nlt","GDALVALUE":"type|PROMOTE_TO_MULTI|CONVERT_TO_LINEAR","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nlt"},{"GDALNAME":"-clipdstsql","GDALVALUE":"sql_statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipdstsql"},{"GDALNAME":"-s_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"s_srs"},{"GDALNAME":"-progress","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"progress"},{"GDALNAME":"-geomfield","GDALVALUE":"field","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"geomfield"},{"GDALNAME":"-nln","GDALVALUE":"name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nln"},{"GDALNAME":"-doo","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"doo"},{"GDALNAME":"-clipsrc","GDALVALUE":"[xmin ymin xmax ymax]|WKT|datasource|spat_extent","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrc1"},{"GDALNAME":"-splitlistfields","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"splitlistfields"},{"GDALNAME":"-mapFieldType","GDALVALUE":"type1|All=type2[,type3=type4]*","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mapFieldType"},{"GDALNAME":"-fid","GDALVALUE":"FID","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fid"},{"GDALNAME":"-sql","GDALVALUE":"sql statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"sql"},{"GDALNAME":"-t_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"t_srs"},{"GDALNAME":"layer","GDALVALUE":"[layer ...]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":1,"IDLNAME":"layer"},{"GDALNAME":"-clipsrcsql","GDALVALUE":"sql_statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrcsql"},{"GDALNAME":"-clipsrcwhere","GDALVALUE":"expression","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrcwhere"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-datelineoffset","GDALVALUE":"val","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"datelineoffset"},{"GDALNAME":"-gcp","GDALVALUE":"pixel line easting northing [elevation]]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"gcp"},{"GDALNAME":"-addfields","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"addfields"},{"GDALNAME":"src_datasource_name","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"src_datasource_name"},{"GDALNAME":"-dsco","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"dsco"},{"GDALNAME":"-lco","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"lco"},{"GDALNAME":"-explodecollections","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"explodecollections"},{"GDALNAME":"-clipdstlayer","GDALVALUE":"layer","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipdstlayer"},{"GDALNAME":"-dialect","GDALVALUE":"dialect","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"dialect"},{"GDALNAME":"-wrapdateline","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"wrapdateline"},{"GDALNAME":"-simplify","GDALVALUE":"tolerance","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"simplify"},{"GDALNAME":"-zfield","GDALVALUE":"field_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"zfield"},{"GDALNAME":"-f","GDALVALUE":"format_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"f1"},{"GDALNAME":"-relaxedFieldNameMatch","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"relaxedFieldNameMatch"},{"GDALNAME":"-append","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"append"},{"GDALNAME":"-where","GDALVALUE":"restricted_where","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"where"},{"GDALNAME":"-dim","GDALVALUE":"2|3|layer_dim","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"dim"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"oo"},{"GDALNAME":"-clipdstwhere","GDALVALUE":"expression","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipdstwhere"},{"GDALNAME":"-maxsubfields","GDALVALUE":"val","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"maxsubfields"},{"GDALNAME":"-update","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"update"},{"GDALNAME":"-clipdst","GDALVALUE":"[xmin ymin xmax ymax]|WKT|datasource","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipdst1"},{"GDALNAME":"-forceNullable","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"forceNullable"},{"GDALNAME":"-overwrite","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"overwrite"},{"GDALNAME":"-a_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a_srs"},{"GDALNAME":"-clipsrclayer","GDALVALUE":"layer","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clipsrclayer"},{"GDALNAME":"-gt","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"gt_"},{"GDALNAME":"-unsetFid","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unsetFid"},{"GDALNAME":"-select","GDALVALUE":"field_list","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"select"},{"GDALNAME":"-unsetFieldWidth","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unsetFieldWidth"},{"GDALNAME":"dst_datasource_name","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dst_datasource_name"},{"GDALNAME":"-unsetDefault","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"unsetDefault"},{"GDALNAME":"-order","GDALVALUE":"n | -tps","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"order"},{"GDALNAME":"-preserve_fid","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"preserve_fid"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('ogr2ogr' $
      , fieldTypeToString=fieldTypeToString   $
      , fieldmap=fieldmap   $
      , skipfailures=skipfailures   $
      , spat=spat   $
      , nlt=nlt   $
      , clipdstsql=clipdstsql   $
      , s_srs=s_srs   $
      , progress=progress   $
      , geomfield=geomfield   $
      , nln=nln   $
      , doo=doo   $
      , clipsrc1=clipsrc1   $
      , splitlistfields=splitlistfields   $
      , mapFieldType=mapFieldType   $
      , fid=fid   $
      , sql=sql   $
      , t_srs=t_srs   $
      , layer $
      , clipsrcsql=clipsrcsql   $
      , clipsrcwhere=clipsrcwhere   $
      , help_general=help_general   $
      , datelineoffset=datelineoffset   $
      , gcp=gcp   $
      , addfields=addfields   $
      , src_datasource_name $
      , dsco=dsco   $
      , lco=lco   $
      , explodecollections=explodecollections   $
      , clipdstlayer=clipdstlayer   $
      , dialect=dialect   $
      , wrapdateline=wrapdateline   $
      , simplify=simplify   $
      , zfield=zfield   $
      , f1=f1   $
      , relaxedFieldNameMatch=relaxedFieldNameMatch   $
      , append=append   $
      , where=where   $
      , dim=dim   $
      , oo=oo   $
      , clipdstwhere=clipdstwhere   $
      , maxsubfields=maxsubfields   $
      , update=update   $
      , clipdst1=clipdst1   $
      , forceNullable=forceNullable   $
      , overwrite=overwrite   $
      , a_srs=a_srs   $
      , clipsrclayer=clipsrclayer   $
      , gt_=gt_   $
      , unsetFid=unsetFid   $
      , select=select   $
      , unsetFieldWidth=unsetFieldWidth   $
      , dst_datasource_name $
      , unsetDefault=unsetDefault   $
      , order=order   $
      , preserve_fid=preserve_fid   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::ogr2ogr $
   ,fieldTypeToString=fieldTypeToString   $
   ,fieldmap=fieldmap   $
   ,skipfailures=skipfailures   $
   ,spat=spat   $
   ,nlt=nlt   $
   ,clipdstsql=clipdstsql   $
   ,s_srs=s_srs   $
   ,progress=progress   $
   ,geomfield=geomfield   $
   ,nln=nln   $
   ,doo=doo   $
   ,clipsrc1=clipsrc1   $
   ,splitlistfields=splitlistfields   $
   ,mapFieldType=mapFieldType   $
   ,fid=fid   $
   ,sql=sql   $
   ,t_srs=t_srs   $
   ,layer $
   ,clipsrcsql=clipsrcsql   $
   ,clipsrcwhere=clipsrcwhere   $
   ,help_general=help_general   $
   ,datelineoffset=datelineoffset   $
   ,gcp=gcp   $
   ,addfields=addfields   $
   ,src_datasource_name $
   ,dsco=dsco   $
   ,lco=lco   $
   ,explodecollections=explodecollections   $
   ,clipdstlayer=clipdstlayer   $
   ,dialect=dialect   $
   ,wrapdateline=wrapdateline   $
   ,simplify=simplify   $
   ,zfield=zfield   $
   ,f1=f1   $
   ,relaxedFieldNameMatch=relaxedFieldNameMatch   $
   ,append=append   $
   ,where=where   $
   ,dim=dim   $
   ,oo=oo   $
   ,clipdstwhere=clipdstwhere   $
   ,maxsubfields=maxsubfields   $
   ,update=update   $
   ,clipdst1=clipdst1   $
   ,forceNullable=forceNullable   $
   ,overwrite=overwrite   $
   ,a_srs=a_srs   $
   ,clipsrclayer=clipsrclayer   $
   ,gt_=gt_   $
   ,unsetFid=unsetFid   $
   ,select=select   $
   ,unsetFieldWidth=unsetFieldWidth   $
   ,dst_datasource_name $
   ,unsetDefault=unsetDefault   $
   ,order=order   $
   ,preserve_fid=preserve_fid   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.ogr2ogr($
   fieldTypeToString=fieldTypeToString   $
   ,fieldmap=fieldmap   $
   ,skipfailures=skipfailures   $
   ,spat=spat   $
   ,nlt=nlt   $
   ,clipdstsql=clipdstsql   $
   ,s_srs=s_srs   $
   ,progress=progress   $
   ,geomfield=geomfield   $
   ,nln=nln   $
   ,doo=doo   $
   ,clipsrc1=clipsrc1   $
   ,splitlistfields=splitlistfields   $
   ,mapFieldType=mapFieldType   $
   ,fid=fid   $
   ,sql=sql   $
   ,t_srs=t_srs   $
   ,layer $
   ,clipsrcsql=clipsrcsql   $
   ,clipsrcwhere=clipsrcwhere   $
   ,help_general=help_general   $
   ,datelineoffset=datelineoffset   $
   ,gcp=gcp   $
   ,addfields=addfields   $
   ,src_datasource_name $
   ,dsco=dsco   $
   ,lco=lco   $
   ,explodecollections=explodecollections   $
   ,clipdstlayer=clipdstlayer   $
   ,dialect=dialect   $
   ,wrapdateline=wrapdateline   $
   ,simplify=simplify   $
   ,zfield=zfield   $
   ,f1=f1   $
   ,relaxedFieldNameMatch=relaxedFieldNameMatch   $
   ,append=append   $
   ,where=where   $
   ,dim=dim   $
   ,oo=oo   $
   ,clipdstwhere=clipdstwhere   $
   ,maxsubfields=maxsubfields   $
   ,update=update   $
   ,clipdst1=clipdst1   $
   ,forceNullable=forceNullable   $
   ,overwrite=overwrite   $
   ,a_srs=a_srs   $
   ,clipsrclayer=clipsrclayer   $
   ,gt_=gt_   $
   ,unsetFid=unsetFid   $
   ,select=select   $
   ,unsetFieldWidth=unsetFieldWidth   $
   ,dst_datasource_name $
   ,unsetDefault=unsetDefault   $
   ,order=order   $
   ,preserve_fid=preserve_fid   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
