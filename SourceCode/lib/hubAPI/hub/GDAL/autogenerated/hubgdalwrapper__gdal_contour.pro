function hubGDALWrapper::gdal_contour $
   ,inodata=inodata   $
   ,dsco=dsco   $
   ,dst_filename $
   ,lco=lco   $
   ,fl=fl   $
   ,src_filename $
   ,_3d=_3d   $
   ,i1=i1   $
   ,nln=nln   $
   ,snodata=snodata   $
   ,off=off   $
   ,b=b   $
   ,a=a   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_contour'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_contour","IDLNAME":"gdal_contour","DESCRIPTION":["builds vector contour lines from a raster elevation model","This program generates a vector contour file from the input raster elevation model (DEM).","Starting from version 1.7 the contour line-strings will be oriented consistently. The high side will be on the right, i.e. a line string goes clockwise around a top.","This would create 10meter contours from the DEM data in dem.tif and produce a shapefile in contour.shp/shx/dbf with the contour elevations in the \"elev\" attribute."],"PARAMETERS":[{"GDALNAME":"-inodata","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"inodata"},{"GDALNAME":"-dsco","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"dsco"},{"GDALNAME":"dst_filename","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dst_filename"},{"GDALNAME":"-lco","GDALVALUE":"NAME=VALUE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":1,"IDLNAME":"lco"},{"GDALNAME":"-fl","GDALVALUE":"level level...","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"fl"},{"GDALNAME":"src_filename","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"src_filename"},{"GDALNAME":"-3d","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"_3d"},{"GDALNAME":"-i","GDALVALUE":"interval","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"i1"},{"GDALNAME":"-nln","GDALVALUE":"outlayername","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nln"},{"GDALNAME":"-snodata","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"snodata"},{"GDALNAME":"-off","GDALVALUE":"offset","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"off"},{"GDALNAME":"-b","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"-a","GDALVALUE":"attribute_name","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"a"},{"GDALNAME":"-f","GDALVALUE":"formatname","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"f1"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_contour' $
      , inodata=inodata   $
      , dsco=dsco   $
      , dst_filename $
      , lco=lco   $
      , fl=fl   $
      , src_filename $
      , _3d=_3d   $
      , i1=i1   $
      , nln=nln   $
      , snodata=snodata   $
      , off=off   $
      , b=b   $
      , a=a   $
      , f1=f1   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_contour $
   ,inodata=inodata   $
   ,dsco=dsco   $
   ,dst_filename $
   ,lco=lco   $
   ,fl=fl   $
   ,src_filename $
   ,_3d=_3d   $
   ,i1=i1   $
   ,nln=nln   $
   ,snodata=snodata   $
   ,off=off   $
   ,b=b   $
   ,a=a   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_contour($
   inodata=inodata   $
   ,dsco=dsco   $
   ,dst_filename $
   ,lco=lco   $
   ,fl=fl   $
   ,src_filename $
   ,_3d=_3d   $
   ,i1=i1   $
   ,nln=nln   $
   ,snodata=snodata   $
   ,off=off   $
   ,b=b   $
   ,a=a   $
   ,f1=f1   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
