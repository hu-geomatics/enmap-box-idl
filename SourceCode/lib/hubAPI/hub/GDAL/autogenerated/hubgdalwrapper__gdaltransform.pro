function hubGDALWrapper::gdaltransform $
   ,s_srs=s_srs   $
   ,srcfile $
   ,t_srs=t_srs   $
   ,geoloc=geoloc   $
   ,order=order   $
   ,rpc=rpc   $
   ,gcp=gcp   $
   ,tps=tps   $
   ,help_general=help_general   $
   ,to=to   $
   ,i=i   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdaltransform'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdaltransform","IDLNAME":"gdaltransform","DESCRIPTION":["transforms coordinates","The gdaltransform utility reprojects a list of coordinates into any supported projection,including GCP-based transformations.","Coordinates are read as pairs (or triples) of numbers per line from standard input, transformed, and written out to standard output in the same way. All transformations offered by gdalwarp are handled, including gcp-based ones.","Note that input and output must always be in decimal form. There is currently no support for DMS input or output.","If an input image file is provided, input is in pixel/line coordinates on that image. If an output file is provided, output is in pixel/line coordinates on that image.","Simple reprojection from one projected coordinate system to another:","Produces the following output in meters in the \"Belge 1972 / Belgian Lambert 72\" projection:","The following command requests an RPC based transformation using the RPC model associated with the named file. Because the -i (inverse) flag is used, the transformation is from output georeferenced (WGS84) coordinates back to image coordinates.","Produces this output measured in pixels and lines on the image: "],"PARAMETERS":[{"GDALNAME":"-s_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"s_srs"},{"GDALNAME":"srcfile","GDALVALUE":"[dstfile]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":1,"IDLNAME":"srcfile"},{"GDALNAME":"-t_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"t_srs"},{"GDALNAME":"-geoloc","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"geoloc"},{"GDALNAME":"-order","GDALVALUE":"n","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"order"},{"GDALNAME":"-rpc","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"rpc"},{"GDALNAME":"-gcp","GDALVALUE":"pixel line easting northing [elevation]]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"gcp"},{"GDALNAME":"-tps","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tps"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-to","GDALVALUE":"\"NAME=VALUE\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"to"},{"GDALNAME":"-i","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"i"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdaltransform' $
      , s_srs=s_srs   $
      , srcfile $
      , t_srs=t_srs   $
      , geoloc=geoloc   $
      , order=order   $
      , rpc=rpc   $
      , gcp=gcp   $
      , tps=tps   $
      , help_general=help_general   $
      , to=to   $
      , i=i   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdaltransform $
   ,s_srs=s_srs   $
   ,srcfile $
   ,t_srs=t_srs   $
   ,geoloc=geoloc   $
   ,order=order   $
   ,rpc=rpc   $
   ,gcp=gcp   $
   ,tps=tps   $
   ,help_general=help_general   $
   ,to=to   $
   ,i=i   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdaltransform($
   s_srs=s_srs   $
   ,srcfile $
   ,t_srs=t_srs   $
   ,geoloc=geoloc   $
   ,order=order   $
   ,rpc=rpc   $
   ,gcp=gcp   $
   ,tps=tps   $
   ,help_general=help_general   $
   ,to=to   $
   ,i=i   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
