function hubGDALWrapper::gdaladdo $
   ,levels $
   ,ro=ro   $
   ,b=b   $
   ,r1=r1   $
   ,oo=oo   $
   ,filename $
   ,help_general=help_general   $
   ,clean=clean   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdaladdo'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdaladdo","IDLNAME":"gdaladdo","DESCRIPTION":["builds or rebuilds overview images","The gdaladdo utility can be used to build or rebuild overview images for most supported file formats with one of several downsampling algorithms.","gdaladdo will honour properly NODATA_VALUES tuples (special dataset metadata) so that only a given RGB triplet (in case of a RGB image) will be considered as the nodata value and not each value of the triplet independently per band.","For internal GeoTIFF overviews (or external overviews in GeoTIFF format), note that -clean does not shrink the file. A later run of gdaladdo with overview levels will cause the file to be expanded, rather than reusing the space of the previously deleted overviews. If you just want to change the resampling method on a file that already has overviews computed, you don`t need to clean the existing overviews.","Some format drivers do not support overviews at all. Many format drivers store overviews in a secondary file with the extension .ovr that is actually in TIFF format. By default, the GeoTIFF driver stores overviews internally to the file operated on (if it is writeable), unless the -ro flag is specified.","Most drivers also support an alternate overview format using Erdas Imagine format. To trigger this use the USE_RRD=YES configuration option. This will place the overviews in an associated .aux file suitable for direct use with Imagine or ArcGIS as well as GDAL applications. (eg --config USE_RRD YES)","External overviews created in TIFF format may be compressed using the COMPRESS_OVERVIEW configuration option. All compression methods, supported by the GeoTIFF driver, are available here. (eg --config COMPRESS_OVERVIEW DEFLATE). The photometric interpretation can be set with --config PHOTOMETRIC_OVERVIEW {RGB,YCBCR,...}, and the interleaving with --config INTERLEAVE_OVERVIEW {PIXEL|BAND}.","For JPEG compressed external overviews, the JPEG quality can be set with \"--config JPEG_QUALITY_OVERVIEW value\" (GDAL 1.7.0 or later).","For LZW or DEFLATE compressed external overviews, the predictor value can be set with \"--config PREDICTOR_OVERVIEW 1|2|3\" (GDAL 1.8.0 or later).","To produce the smallest possible JPEG-In-TIFF overviews, you should use : ","Starting with GDAL 1.7.0, external overviews can be created in the BigTIFF format by using the BIGTIFF_OVERVIEW configuration option : --config BIGTIFF_OVERVIEW {IF_NEEDED|IF_SAFER|YES|NO}. The default value is IF_NEEDED. The behaviour of this option is exactly the same as the BIGTIFF creation option documented in the GeoTIFF driver documentation. ","See the documentation of the GeoTIFF driver for further explanations on all those options.","Create an external JPEG-compressed GeoTIFF overview file from a 3-band RGB dataset (if the dataset is a writeable GeoTIFF, you also need to add the -ro option to force the generation of external overview):"],"PARAMETERS":[{"GDALNAME":"levels","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"levels"},{"GDALNAME":"-ro","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ro"},{"GDALNAME":"-b","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"-r","GDALVALUE":"{nearest,average,gauss,cubic,cubicspline,lanczos,average_mp,average_magphase,mode}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"r1"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"oo"},{"GDALNAME":"filename","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"filename"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-clean","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"clean"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdaladdo' $
      , levels $
      , ro=ro   $
      , b=b   $
      , r1=r1   $
      , oo=oo   $
      , filename $
      , help_general=help_general   $
      , clean=clean   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdaladdo $
   ,levels $
   ,ro=ro   $
   ,b=b   $
   ,r1=r1   $
   ,oo=oo   $
   ,filename $
   ,help_general=help_general   $
   ,clean=clean   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdaladdo($
   levels $
   ,ro=ro   $
   ,b=b   $
   ,r1=r1   $
   ,oo=oo   $
   ,filename $
   ,help_general=help_general   $
   ,clean=clean   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
