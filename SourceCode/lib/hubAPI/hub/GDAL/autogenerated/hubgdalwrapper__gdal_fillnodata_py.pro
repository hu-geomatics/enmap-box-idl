function hubGDALWrapper::gdal_fillnodata_py $
   ,md=md   $
   ,q=q   $
   ,of_=of_   $
   ,dstfile $
   ,srcfile $
   ,b=b   $
   ,nomask=nomask   $
   ,o1=o1   $
   ,si=si   $
   ,mask=mask   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_fillnodata.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_fillnodata.py","IDLNAME":"gdal_fillnodata_py","DESCRIPTION":["fill raster regions by interpolation from edges","The gdal_fillnodata.py script fills selection regions (usually nodata areas) by interpolating from valid pixels around the edges of the area."],"PARAMETERS":[{"GDALNAME":"-md","GDALVALUE":"max_distance","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"md"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"dstfile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dstfile"},{"GDALNAME":"srcfile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"srcfile"},{"GDALNAME":"-b","GDALVALUE":"band","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"b"},{"GDALNAME":"-nomask","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nomask"},{"GDALNAME":"-o","GDALVALUE":"name=value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"o1"},{"GDALNAME":"-si","GDALVALUE":"smooth_iterations","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"si"},{"GDALNAME":"-mask","GDALVALUE":"filename","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"mask"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_fillnodata.py' $
      , md=md   $
      , q=q   $
      , of_=of_   $
      , dstfile $
      , srcfile $
      , b=b   $
      , nomask=nomask   $
      , o1=o1   $
      , si=si   $
      , mask=mask   $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_fillnodata_py $
   ,md=md   $
   ,q=q   $
   ,of_=of_   $
   ,dstfile $
   ,srcfile $
   ,b=b   $
   ,nomask=nomask   $
   ,o1=o1   $
   ,si=si   $
   ,mask=mask   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_fillnodata_py($
   md=md   $
   ,q=q   $
   ,of_=of_   $
   ,dstfile $
   ,srcfile $
   ,b=b   $
   ,nomask=nomask   $
   ,o1=o1   $
   ,si=si   $
   ,mask=mask   $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
