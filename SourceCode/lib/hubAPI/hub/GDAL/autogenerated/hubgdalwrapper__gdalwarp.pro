function hubGDALWrapper::gdalwarp $
   ,ovr=ovr   $
   ,formats=formats   $
   ,cutline=cutline   $
   ,refine_gcps=refine_gcps   $
   ,crop_to_cutline=crop_to_cutline   $
   ,oo=oo   $
   ,s_srs=s_srs   $
   ,setci=setci   $
   ,ot=ot   $
   ,et=et   $
   ,wo=wo   $
   ,q=q   $
   ,csql=csql   $
   ,of_=of_   $
   ,te=te   $
   ,t_srs=t_srs   $
   ,ts=ts   $
   ,r1=r1   $
   ,cl=cl   $
   ,tap=tap   $
   ,cvmd=cvmd   $
   ,help_general=help_general   $
   ,overwrite=overwrite   $
   ,dstnodata=dstnodata   $
   ,dstfile $
   ,tr=tr   $
   ,wt=wt   $
   ,wm=wm   $
   ,srcfile $
   ,to=to   $
   ,co1=co1   $
   ,srcnodata=srcnodata   $
   ,multi=multi   $
   ,cwhere=cwhere   $
   ,cblend=cblend   $
   ,dstalpha=dstalpha   $
   ,order=order   $
   ,nomd=nomd   $
   ,version=version   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdalwarp'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdalwarp","IDLNAME":"gdalwarp","DESCRIPTION":["image reprojection and warping utility","The gdalwarp utility is an image mosaicing, reprojection and warping utility. The program can reproject to any supported projection, and can also apply GCPs stored with the image if the image is \"raw\" with control information.","Mosaicing into an existing output file is supported if the output file already exists. The spatial extent of the existing file will not be modified to accommodate new data, so you may have to remove it in that case, or use the -overwrite option.","Polygon cutlines may be used as a mask to restrict the area of the destination file that may be updated, including blending. If the OGR layer containing the cutline features has no explicit SRS, the cutline features must be in the SRS of the destination file. When outputing to a not yet existing target dataset, its extent will be the one of the original raster unless -te or -crop_to_cutline are specified.","For instance, an eight bit spot scene stored in GeoTIFF with control points mapping the corners to lat/long could be warped to a UTM projection with a command like this:","For instance, the second channel of an ASTER image stored in HDF with control points mapping the corners to lat/long could be warped to a UTM projection with a command like this:"],"PARAMETERS":[{"GDALNAME":"-ovr","GDALVALUE":"level|AUTO|AUTO-n|NONE","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ovr"},{"GDALNAME":"--formats","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"-cutline","GDALVALUE":"datasource","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"cutline"},{"GDALNAME":"-refine_gcps","GDALVALUE":"tolerance [minimum_gcps]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"refine_gcps"},{"GDALNAME":"-crop_to_cutline","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"crop_to_cutline"},{"GDALNAME":"-oo","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"oo"},{"GDALNAME":"-s_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"s_srs"},{"GDALNAME":"-setci","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"setci"},{"GDALNAME":"-ot","GDALVALUE":"Byte/Int16/...","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-et","GDALVALUE":"err_threshold","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"et"},{"GDALNAME":"-wo","GDALVALUE":"\"NAME=VALUE\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"wo"},{"GDALNAME":"-q","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"q"},{"GDALNAME":"-csql","GDALVALUE":"statement","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"csql"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"-te","GDALVALUE":"xmin ymin xmax ymax","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"te"},{"GDALNAME":"-t_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"t_srs"},{"GDALNAME":"-ts","GDALVALUE":"width height","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ts"},{"GDALNAME":"-r","GDALVALUE":"resampling_method","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"r1"},{"GDALNAME":"-cl","GDALVALUE":"layer","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"cl"},{"GDALNAME":"-tap","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tap"},{"GDALNAME":"-cvmd","GDALVALUE":"meta_conflict_value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"cvmd"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"},{"GDALNAME":"-overwrite","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"overwrite"},{"GDALNAME":"-dstnodata","GDALVALUE":"\"value [value...]\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"dstnodata"},{"GDALNAME":"dstfile","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dstfile"},{"GDALNAME":"-tr","GDALVALUE":"xres yres","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tr"},{"GDALNAME":"-wt","GDALVALUE":"Byte/Int16","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"wt"},{"GDALNAME":"-wm","GDALVALUE":"memory_in_mb","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"wm"},{"GDALNAME":"srcfile","GDALVALUE":"*","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"srcfile"},{"GDALNAME":"-to","GDALVALUE":"\"NAME=VALUE\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"to"},{"GDALNAME":"-co","GDALVALUE":"\"NAME=VALUE\"]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"-srcnodata","GDALVALUE":"\"value [value...]\"","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"srcnodata"},{"GDALNAME":"-multi","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"multi"},{"GDALNAME":"-cwhere","GDALVALUE":"expression","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"cwhere"},{"GDALNAME":"-cblend","GDALVALUE":"dist_in_pixels","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"cblend"},{"GDALNAME":"-dstalpha","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"dstalpha"},{"GDALNAME":"-order","GDALVALUE":"n | -tps | -rpc | -geoloc","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"order"},{"GDALNAME":"-nomd","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"nomd"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdalwarp' $
      , ovr=ovr   $
      , formats=formats   $
      , cutline=cutline   $
      , refine_gcps=refine_gcps   $
      , crop_to_cutline=crop_to_cutline   $
      , oo=oo   $
      , s_srs=s_srs   $
      , setci=setci   $
      , ot=ot   $
      , et=et   $
      , wo=wo   $
      , q=q   $
      , csql=csql   $
      , of_=of_   $
      , te=te   $
      , t_srs=t_srs   $
      , ts=ts   $
      , r1=r1   $
      , cl=cl   $
      , tap=tap   $
      , cvmd=cvmd   $
      , help_general=help_general   $
      , overwrite=overwrite   $
      , dstnodata=dstnodata   $
      , dstfile $
      , tr=tr   $
      , wt=wt   $
      , wm=wm   $
      , srcfile $
      , to=to   $
      , co1=co1   $
      , srcnodata=srcnodata   $
      , multi=multi   $
      , cwhere=cwhere   $
      , cblend=cblend   $
      , dstalpha=dstalpha   $
      , order=order   $
      , nomd=nomd   $
      , version=version   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdalwarp $
   ,ovr=ovr   $
   ,formats=formats   $
   ,cutline=cutline   $
   ,refine_gcps=refine_gcps   $
   ,crop_to_cutline=crop_to_cutline   $
   ,oo=oo   $
   ,s_srs=s_srs   $
   ,setci=setci   $
   ,ot=ot   $
   ,et=et   $
   ,wo=wo   $
   ,q=q   $
   ,csql=csql   $
   ,of_=of_   $
   ,te=te   $
   ,t_srs=t_srs   $
   ,ts=ts   $
   ,r1=r1   $
   ,cl=cl   $
   ,tap=tap   $
   ,cvmd=cvmd   $
   ,help_general=help_general   $
   ,overwrite=overwrite   $
   ,dstnodata=dstnodata   $
   ,dstfile $
   ,tr=tr   $
   ,wt=wt   $
   ,wm=wm   $
   ,srcfile $
   ,to=to   $
   ,co1=co1   $
   ,srcnodata=srcnodata   $
   ,multi=multi   $
   ,cwhere=cwhere   $
   ,cblend=cblend   $
   ,dstalpha=dstalpha   $
   ,order=order   $
   ,nomd=nomd   $
   ,version=version   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdalwarp($
   ovr=ovr   $
   ,formats=formats   $
   ,cutline=cutline   $
   ,refine_gcps=refine_gcps   $
   ,crop_to_cutline=crop_to_cutline   $
   ,oo=oo   $
   ,s_srs=s_srs   $
   ,setci=setci   $
   ,ot=ot   $
   ,et=et   $
   ,wo=wo   $
   ,q=q   $
   ,csql=csql   $
   ,of_=of_   $
   ,te=te   $
   ,t_srs=t_srs   $
   ,ts=ts   $
   ,r1=r1   $
   ,cl=cl   $
   ,tap=tap   $
   ,cvmd=cvmd   $
   ,help_general=help_general   $
   ,overwrite=overwrite   $
   ,dstnodata=dstnodata   $
   ,dstfile $
   ,tr=tr   $
   ,wt=wt   $
   ,wm=wm   $
   ,srcfile $
   ,to=to   $
   ,co1=co1   $
   ,srcnodata=srcnodata   $
   ,multi=multi   $
   ,cwhere=cwhere   $
   ,cblend=cblend   $
   ,dstalpha=dstalpha   $
   ,order=order   $
   ,nomd=nomd   $
   ,version=version   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,_EXTRA = _EXTRA)
end
