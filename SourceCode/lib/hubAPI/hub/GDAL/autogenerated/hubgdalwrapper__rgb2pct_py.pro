function hubGDALWrapper::rgb2pct_py $
   ,n=n   $
   ,dest_file $
   ,of_=of_   $
   ,source_file $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'rgb2pct.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"rgb2pct.py","IDLNAME":"rgb2pct_py","DESCRIPTION":["Convert a 24bit RGB image to 8bit paletted","This utility will compute an optimal pseudo-color table for a given RGB image using a median cut algorithm on a downsampled RGB histogram. Then it converts the image into a pseudo-colored image using the color table. This conversion utilizes Floyd-Steinberg dithering (error diffusion) to maximize output image visual quality.","NOTE: rgb2pct.py is a Python script, and will only work if GDAL was built with Python support.","If it is desired to hand create the palette, likely the simplest text format is the GDAL VRT format. In the following example a VRT was created in a text editor with a small 4 color palette with the RGBA colors 238/238/238/255, 237/237/237/255, 236/236/236/255 and 229/229/229/255."],"PARAMETERS":[{"GDALNAME":"-n","GDALVALUE":"colors | -pct palette_file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"n"},{"GDALNAME":"dest_file","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"dest_file"},{"GDALNAME":"-of","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"source_file","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"source_file"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('rgb2pct.py' $
      , n=n   $
      , dest_file $
      , of_=of_   $
      , source_file $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::rgb2pct_py $
   ,n=n   $
   ,dest_file $
   ,of_=of_   $
   ,source_file $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.rgb2pct_py($
   n=n   $
   ,dest_file $
   ,of_=of_   $
   ,source_file $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
