function hubGDALWrapper::gdal_retile_py $
   ,pyramidOnly=pyramidOnly   $
   ,input_files $
   ,ps=ps   $
   ,targetDir=targetDir   $
   ,tileIndex=tileIndex   $
   ,s_srs=s_srs   $
   ,numberoflevels $
   ,ot=ot   $
   ,co1=co1   $
   ,levels=levels   $
   ,of_=of_   $
   ,csv=csv   $
   ,v1=v1   $
   ,useDirForEachRow=useDirForEachRow   $
   ,r=r   $
   ,TileDirectory $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA

  ;add GDAL application info (contains parameter definitions, descriptions, etc.)
  appGDALName = 'gdal_retile.py'
  registerOnly = keyword_set(_registerAppInfo) 
  registerOnly or= isa(_EXTRA) && total(strcmp(tag_names(_EXTRA),'_registerAppInfo', /FOLD_CASE)) 
  if ~(self.commands).hasKey(appGDALName) or registerOnly then begin 
     appInfo = '{"GDALNAME":"gdal_retile.py","IDLNAME":"gdal_retile_py","DESCRIPTION":["gdal_retile.py retiles a set of tiles and/or build tiled pyramid levels","This utility will retile a set of input tile(s). All the input tile(s) must be georeferenced in the same coordinate system and have a matching number of bands. Optionally pyramid levels are generated. It is possible to generate shape file(s) for the tiled output.","If your number of input tiles exhausts the command line buffer, use the general --optfile option","NOTE: gdal_retile.py is a Python script, and will only work if GDAL was built with Python support. "],"PARAMETERS":[{"GDALNAME":"-pyramidOnly","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"pyramidOnly"},{"GDALNAME":"input_files","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"input_files"},{"GDALNAME":"-ps","GDALVALUE":"pixelWidth pixelHeight","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ps"},{"GDALNAME":"-targetDir","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"targetDir"},{"GDALNAME":"-tileIndex","GDALVALUE":"tileIndexName [-tileIndexField tileIndexFieldName]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"tileIndex"},{"GDALNAME":"-s_srs","GDALVALUE":"srs_def","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"s_srs"},{"GDALNAME":"numberoflevels","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"numberoflevels"},{"GDALNAME":"-ot","GDALVALUE":"{Byte/Int16/UInt16/UInt32/Int32/Float32/Float64/ CInt16/CInt32/CFloat32/CFloat64}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"ot"},{"GDALNAME":"-co","GDALVALUE":"NAME=VALUE]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"co1"},{"GDALNAME":"-levels","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"levels"},{"GDALNAME":"-of","GDALVALUE":"out_format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"of_"},{"GDALNAME":"-csv","GDALVALUE":"fileName [-csvDelim delimiter]","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"csv"},{"GDALNAME":"-v","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"v1"},{"GDALNAME":"-useDirForEachRow","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"useDirForEachRow"},{"GDALNAME":"-r","GDALVALUE":"{near/bilinear/cubic/cubicspline/lanczos}","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"r"},{"GDALNAME":"TileDirectory","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":0,"ISREPEATABLE":0,"IDLNAME":"TileDirectory"},{"GDALNAME":"--version","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"version"},{"GDALNAME":"--formats","GDALVALUE":"format","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"formats"},{"GDALNAME":"--optfile","GDALVALUE":"file","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"optfile"},{"GDALNAME":"--config","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"config"},{"GDALNAME":"--debug","GDALVALUE":"key value","GDALDESCRIPTION":"","ISBOOL":0,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"debug"},{"GDALNAME":"--help-general","GDALVALUE":"","GDALDESCRIPTION":"","ISBOOL":1,"ISKEYWORD":1,"ISREPEATABLE":0,"IDLNAME":"help_general"}]}'
     (self.commands)[appGDALName] = JSON_PARSE(appInfo, /DICTIONARY) 
     if registerOnly then return, !NULL  
  endif

return, self._runCommand('gdal_retile.py' $
      , pyramidOnly=pyramidOnly   $
      , input_files $
      , ps=ps   $
      , targetDir=targetDir   $
      , tileIndex=tileIndex   $
      , s_srs=s_srs   $
      , numberoflevels $
      , ot=ot   $
      , co1=co1   $
      , levels=levels   $
      , of_=of_   $
      , csv=csv   $
      , v1=v1   $
      , useDirForEachRow=useDirForEachRow   $
      , r=r   $
      , TileDirectory $
      , version=version   $
      , formats=formats   $
      , optfile=optfile   $
      , config=config   $
      , debug=debug   $
      , help_general=help_general   $
      , _EXTRA = _EXTRA)
end
 
pro hubGDALWrapper::gdal_retile_py $
   ,pyramidOnly=pyramidOnly   $
   ,input_files $
   ,ps=ps   $
   ,targetDir=targetDir   $
   ,tileIndex=tileIndex   $
   ,s_srs=s_srs   $
   ,numberoflevels $
   ,ot=ot   $
   ,co1=co1   $
   ,levels=levels   $
   ,of_=of_   $
   ,csv=csv   $
   ,v1=v1   $
   ,useDirForEachRow=useDirForEachRow   $
   ,r=r   $
   ,TileDirectory $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA
 
!NULL = self.gdal_retile_py($
   pyramidOnly=pyramidOnly   $
   ,input_files $
   ,ps=ps   $
   ,targetDir=targetDir   $
   ,tileIndex=tileIndex   $
   ,s_srs=s_srs   $
   ,numberoflevels $
   ,ot=ot   $
   ,co1=co1   $
   ,levels=levels   $
   ,of_=of_   $
   ,csv=csv   $
   ,v1=v1   $
   ,useDirForEachRow=useDirForEachRow   $
   ,r=r   $
   ,TileDirectory $
   ,version=version   $
   ,formats=formats   $
   ,optfile=optfile   $
   ,config=config   $
   ,debug=debug   $
   ,help_general=help_general   $
   ,_EXTRA = _EXTRA)
end
