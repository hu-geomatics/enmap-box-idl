;+
; :Description:
;    Returns an image path created from given dirname and basename arguments. Throws an error if image does not exist. 
;
; :Params:
;    dirname : in, required, type=dirname
;      Path to directory.
;      
;    basename: in, optional, type=basename
;      File basename. If skipped, all image files in directory are returned.
;
; :Author: Andreas Rabe
;-
function hub_getDirectoryImageFilename, dirname, basename
  if isa(basename) then begin
    filename = filepath(basename, ROOT=dirname)
    if ~file_test(/NOEXPAND_PATH, filename) then begin
      message, 'File not found: '+filename
    endif
    return, filename
  endif else begin
    filenames = hub_getDirectoryImageFilenames(dirname)
    return, filenames
  endelse
end

pro test_hub_getDirectoryImageFilename
  dirname = filepath('', ROOT=hub_getDirname(), SUBDIR=['resource','testData','image'])
  basename = 'Hymap_Berlin-A_Image'
  print, hub_getDirectoryImageFilename(dirname, basename)
end