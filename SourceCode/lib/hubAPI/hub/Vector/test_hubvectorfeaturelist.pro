function test_hubVectorFeatureList
  ; create a feature list with a single feature representing three point and three polygon geometries
  featureList = hubVectorFeatureList()
  featureList.setAttributeNames, ['Name', 'Color']

  feature = featureList.newFeature()

  feature.setAttribute, 'Name', '3 points and 3 polygons'
  xs = [0.5,1.5,2.5]
  ys = [0.5,1.5,2.5]
  for i=0, n_elements(xs)-1 do begin
    feature.add, /Point, xs[i], ys[i]
  endfor

  xRef = [0,1,1,0]
  yRef = [0,0,1,1]
  xs = list(xRef+5,xRef+6,xRef+7)
  ys = list(yRef+5,yRef+6,yRef+7)
  for i=0, n_elements(xs)-1 do begin
    feature.add, /Polygon, xs[i], ys[i]
  endfor
  
  for i=1,100 do feature.add, /Polygon, (xRef+1)*i, (yRef+1)*i

 return, featureList
end

;function test_hubVectorFeatureList
;  ; create a feature list with a single multi point feature representing three points
;  featureList = hubVectorFeatureList()
;  pointFeature = hubVectorFeaturePoint()
;  pointMultiGeometry = hubVectorMultiGeometryPoint()
;  
;  featureList.setAttributeNames, ['Name', 'Color']
;  featureList.addFeature, pointFeature
;  pointFeature.setGeometry, pointMultiGeometry
;  pointFeature.setAttribute, 'Name', '3 point feature'
;  
;  xs = [0.5,1.5,2.5]
;  ys = [0.5,1.5,2.5]
;  for i=0, n_elements(xs)-1 do begin
;    pointGeometry = hubVectorGeometryPoint()
;    pointGeometry.setData, xs[i], ys[i]
;    pointMultiGeometry.addGeometry, pointGeometry
;  endfor
;  
;  return, featureList
;end

pro test_hubVectorFeatureList
  
  featureList = test_hubVectorFeatureList()
  at = featureList.getAttributeTableAsStringArray()
  help,at
  featureList.print
  help, featureList.createROIMultiGroupList()
  
end
