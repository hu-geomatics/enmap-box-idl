function hubVectorGeometry::init, x, y
  self.setData, x, y
  return, 1b
end

pro hubVectorGeometry::setData, x, y
  if n_elements(x) ne n_elements(y) then begin
    message, 'Arguments size do not match.'
  endif
  self.x = ptr_new(x)
  self.y = ptr_new(y)
end

pro hubVectorGeometry::print
  print, 'X:',*self.x
  print, 'Y:',*self.y
end

pro hubVectorGeometry::getProperty, xData=xData, yData=yData
  if arg_present(xData) then xData = *self.x
  if arg_present(yData) then yData = *self.y
end

pro hubVectorGeometry__define
  struct = {hubVectorGeometry, inherits IDL_Object,$
    x:ptr_new(), y:ptr_new()}
end
