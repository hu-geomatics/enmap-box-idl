pro hubVectorGeometryPolygon::setData, x, y
  if n_elements(x) lt 3 then begin
    message, 'Number of points must be greater or equal to three.'
  endif
  self.hubVectorGeometry::setData, x, y
end

function hubVectorGeometryPolygon::getStyle
  return, 2
end

pro hubVectorGeometryPolygon__define
  struct = {hubVectorGeometryPolygon, inherits hubVectorGeometry}
end

pro test_hubVectorGeometryPolygon
  x = [0.5,1.5,1.0]
  y = [0.0,0.0,1.0]
  polygon = hubVectorGeometryPolygon(x,y)
  polygon.print

end