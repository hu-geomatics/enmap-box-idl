function hubVectorFeature::init
  self.multiGeometry = hubVectorMultiGeometry()
  self.attributes = hash()
  return, 1b
end

pro hubVectorFeature::setAttribute, attributeName, value
  (self.attributes)[attributeName] = value
end

function hubVectorFeature::getAttributes, attributeNames
  result = list()
  foreach attributeName,attributeNames do begin
    result.add, self.attributes.hubGetValue(attributeName)
  endforeach
  return, result
end

pro hubVectorFeature::add, x, y, Point=point, Polygon=polygon
  if keyword_set(point) then geometry = hubVectorGeometryPoint(x,y)
  if keyword_set(polygon) then geometry = hubVectorGeometryPolygon(x,y)
  if ~isa(geometry) then message, 'Set a keyword.'
  self.multiGeometry.addGeometry, geometry
end

function hubVectorFeature::createROIGroup
  return, self.multiGeometry.createROIGroup()
end

pro hubVectorFeature::getProperty, multiGeometry=multiGeometry
  if arg_present(multiGeometry) then multiGeometry = self.multiGeometry
end

pro hubVectorFeature__define
  struct = {hubVectorFeature, inherits IDL_Object,$
    multiGeometry:obj_new(), attributes:hash()}
end
