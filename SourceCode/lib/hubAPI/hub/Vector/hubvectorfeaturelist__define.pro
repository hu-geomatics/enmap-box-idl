function hubVectorFeatureList::init
  self.features = list()
  return, 1b
end

pro hubVectorFeatureList::addFeature, feature
  if ~isa(feature, 'hubVectorFeature') then begin
    message, 'Wrong argument.'
  endif
  self.features.add, feature
end

function hubVectorFeatureList::newFeature
  feature = hubVectorFeature()
  self.addFeature, feature
  return, feature
end

function hubVectorFeatureList::createROIGroupList
  ROIGroupList = hubVectorGraphicROIGroupList()
  foreach feature, self.features do begin
    ROIGroupList.add, feature.createROIGroup()
  endforeach
  return, ROIGroupList
end

pro hubVectorFeatureList::setAttributeNames, attributeNames
  self.attributeNames = ptr_new(attributeNames)
end

function hubVectorFeatureList::getAttributeNames
  return, *self.attributeNames
end

function hubVectorFeatureList::getAttributeTable
  result = list()
  foreach feature, self.features, i do begin
    result.add, feature.getAttributes(*self.attributeNames)
  endforeach
  return, result
end

function hubVectorFeatureList::getAttributeTableAsStringArray
  attributeTable = self.getAttributeTable()
  foreach featureAttributes, attributeTable, i do begin
    attributeTable[i] = featureAttributes.toArray(MISSING='null', TYPE='string')
  endforeach
  result = attributeTable.toArray(TYPE='string', /TRANSPOSE)
  return, result
end

function hubVectorFeatureList::getNumberOfFeatures
  return, self.features.count()
end

function hubVectorFeatureList::getGeometries
  result = list()
  foreach feature, self.features, i do begin
    result.add, feature.multiGeometry
  endforeach
  return, result
end

pro hubVectorFeatureList::print
  print, 'Attribute Names:', '"'+*self.attributeNames+'"'
  foreach feature, self.features, i do begin
    attributeValues = feature.getAttributes(*self.attributeNames)
    print, '-----------'
    print, 'Feature '+strcompress(/REMOVE_ALL, i+1)
    foreach attributeName, *self.attributeNames, k do begin
      print, '"'+attributeName+'": ', attributeValues[k]  
    endforeach
  endforeach
end

pro hubVectorFeatureList::getProperty, features=features
  if arg_present(features) then features = self.features
end

pro hubVectorFeatureList__define
  struct = {hubVectorFeatureList, inherits IDL_Object,$
    attributeNames:ptr_new(),$
    features:list()}
end
