function hubVectorGeometryPoint::init, x, y
  self.setData, x, y
  return, 1b
end

pro hubVectorGeometryPoint::setData, x, y
  if isa(x) && n_elements(x) ne 1 then begin
    message, 'Arguments x and y must be scalars.'
  endif
  self.hubVectorGeometry::setData, x, y
end

function hubVectorGeometryPoint::getStyle
  return, 0
end

pro hubVectorGeometryPoint__define
  struct = {hubVectorGeometryPoint, inherits hubVectorGeometry}
end

pro test_hubVectorGeometryPoint

  point = hubVectorGeometryPoint(0.5, 0.5)
  point.print

end