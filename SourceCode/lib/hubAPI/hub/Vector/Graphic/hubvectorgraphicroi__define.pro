function hubVectorGraphicROI::init, x, y, Style=style, _EXTRA=_extra
  z = x*0+1
  color = [0,0,255]
  symbol = IDLgrSymbol(1, SIZE=0.5)
;  fillPattern = IDLgrPattern(1, ORIENTATION=30,SPACING=5)
  fillPattern = IDLgrPattern(0, ORIENTATION=30,SPACING=5)
  
  case style of
    0 : begin
      self.graphicAtom = IDLgrROI(x, y, z, Style=style, _EXTRA=_extra, Color=color, Symbol=symbol)
      self.graphicROI = IDLgrROI(x, y, z, Style=style, _EXTRA=_extra, Color=color)
    end
    1 : begin
      self.graphicAtom = IDLgrROI(x, y, z, Style=style, _EXTRA=_extra, Color=color)
      self.graphicROI = IDLgrROI(x, y, z, Style=style, _EXTRA=_extra, Color=color)
    end
    2 : begin
      self.graphicAtom = IDLgrPolygon(x, y, z, _EXTRA=_extra, Color=color, FILL_PATTERN=fillPattern, ALPHA_CHANNEL=0.2)
      self.graphicROI = IDLgrROI(x, y, z,_EXTRA=_extra, Style=style, Color=color)
    end
  endcase
  return, 1b
end

pro hubVectorGraphicROI::addToModel, model
  model.add, self.graphicAtom, POSITION=0
  model.add, self.graphicROI, POSITION=0
end

function hubVectorGraphicROI::getStyle
  self.graphicAtom.getProperty, STYLE=style
  return, style
end

function hubVectorGraphicROI::getGraphicAtom
  return, self.graphicAtom
end

function hubVectorGraphicROI::getGraphicROI
  return, self.graphicROI
end

pro hubVectorGraphicROI__define
  struct = {hubVectorGraphicROI, inherits IDL_Object,$
    graphicAtom:obj_new(),$
    graphicROI:obj_new()}
end
