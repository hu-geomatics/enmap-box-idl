function hubVectorGraphicROIGroup::init
  self.model = IDLgrModel()
  self.ROIs = list()
  return, 1b
end

pro hubVectorGraphicROIGroup::add, ROI
  if ~isa('hubVectorGraphicROI') then begin 
    message, 'Wrong argument.'
  endif
  ROI.addToModel, self.model
  self.ROIs.add, ROI
end

function hubVectorGraphicROIGroup::getModel
  return, self.model
end

pro hubVectorGraphicROIGroup__define
  struct = {hubVectorGraphicROIGroup, inherits IDL_Object,$
    model:obj_new(),$
    ROIs:list()}
end
