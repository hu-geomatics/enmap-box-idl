function hubVectorGraphicROIGroupList::init
  self.model = IDLgrModel()
  self.ROIGroups = list()
  return, 1b
end

pro hubVectorGraphicROIGroupList::add, ROIGroup
  self.model.add, ROIGroup.getModel()
  self.ROIGroups.add, ROIGroup
end

pro hubVectorGraphicROIGroupList::setSelection, selectedROIGroup, Index=index
  if ~isa(selectedROIGroup) then begin
    selectedROIGroup = (self.ROIGroups)[index]
  endif
  
  foreach ROIGroup, self.ROIGroups do begin
    if ROIGroup eq selectedROIGroup then begin
      ; todo roi group selektieren durch ändern des THICK keyword
;      ROIGroup.setSelection
    endif
  endforeach
end

function hubVectorGraphicROIGroupList::getROIGroupSelection
  return, self.ROIGroupSelection
end

function hubVectorGraphicROIGroupList::getModel
  return, self.model
end

pro hubVectorGraphicROIGroupList__define
  struct = {hubVectorGraphicROIGroupList, inherits IDL_Object,$
    model:obj_new(),$
    ROIGroups:list(),$
    ROIGroupSelection:obj_new() $
    }
end
