function hubVectorMultiGeometry::init
  self.geometries = list()
  return, 1b
end

pro hubVectorMultiGeometry::addGeometry, geometry
  if ~isa(geometry, 'hubVectorGeometry') then begin
    message, 'Wrong argument.'
  endif
  self.geometries.add, geometry
end

function hubVectorMultiGeometry::createROIGroup
  ROIGroup = hubVectorGraphicROIGroup()
  foreach geometry, self.geometries do begin
    ROIGroup.add, hubVectorGraphicROI(geometry.xData, geometry.yData, STYLE=geometry.getStyle())
  endforeach
  return, ROIGroup
end

pro hubVectorMultiGeometry::print
  foreach geometry, self.geometries, i do begin
    print, 'ID:',i
    geometry.print
  endforeach
end

pro hubVectorMultiGeometry::getProperty, geometries=geometries
  if arg_present(geometries) then geometries = self.geometries
end

pro hubVectorMultiGeometry__define
  struct = {hubVectorMultiGeometry, inherits IDL_Object,$
    geometries:list()}
end
