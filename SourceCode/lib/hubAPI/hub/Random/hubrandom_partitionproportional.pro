;+
; :Author: Andreas Rabe
;-

;+
; :Description:
;    Returns a random partition of given size.
;
; :Params:
;    partitionProportions : in, required, type=number[]
;      Array of partition proportions sizes. Must not necessarily sum up to one, in that case will be normalized internally.
;
;    setSize : in, required, type=number
;      Overall set size.
;
; :Keywords:
;    Seed :  in, optional, type=any
;      Random number seed.
;      
;    ToList : in, optional, type=boolean
;      Set to return a list with the individual partition indices. 
;
;-
function hubRandom_partitionProportional, partitionProportions, setSize, Seed=seed, ToList=toList
  
  normalizedProportions = partitionProportions/total(partitionProportions)
  partitionSizes = round(setSize*normalizedProportions) > 1
  partitionSizes[-1] = setSize-total(partitionSizes[0:-2]) > 1
  if total(partitionSizes) ne setSize then begin
    message, 'Set is too small.' 
  endif
  return, hubRandom_partition(partitionSizes, Seed=seed, ToList=toList)
end

pro test_hubRandom_partitionProportional
  ; partition into 3 folds with proportional [0.2,0.2,0.4] and overall size of 10
  print, 'partition:',hubRandom_partitionProportional([0.2,0.2,0.6], 10)
  
  ; return a list of indices for each partition fold
  print, 'partition:'
  foreach indices,hubRandom_partitionProportional([0.2,0.2,0.6], 10, /ToList),i do begin
    print, '#'+strtrim(i,2), indices
  endforeach  
end