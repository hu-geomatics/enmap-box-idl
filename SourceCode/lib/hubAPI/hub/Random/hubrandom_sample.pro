;+
; :Author: Andreas Rabe
;-

;+
; :Description:
;    Returns a random subset indices or values.
;
; :Params:
;    subsetSize : in, required, type=number
;      Number of samples to draw.
;
;    setSize : in, optional, type=number
;      Number of samples to draw from. Can be omitted if the Value keyword is used.
;
; :Keywords:
;    Replace : in, optional, type=boolean
;      Set to draw with replacement.
;      
;    Seed :  in, optional, type=any
;      Random number seed.
;      
;    Values : in, optional, type={array | list}
;      Per default drawn indices are returned. Use this argument to specify an array of values to be returned.
;
;    Sort : in, optional, type=boolean
;      Set to sort the drawn indices. 
;
;    Complement : out, optional
;      Use to specify the sample complement.
;
;-
function hubRandom_sample, subsetSize, setSize, Replace=replace, Seed=seed, Values=values, Sort=sort, Complement=complement
  if keyword_set(replace) then begin
    return, hubRandom_sampleWithReplacement(subsetSize, setSize, Seed=seed, Values=values, Sort=sort, Complement=complement)
  endif else begin
    return, hubRandom_sampleWithoutReplacement(subsetSize, setSize, Seed=seed, Values=values, Sort=sort, Complement=complement)
  endelse
end

pro test_hubRandom_sample
  ; draw 7 out of 10 with replacement
  print, 'subset:    ', hubRandom_sample(7, 10, /Replace, /Sort, Complement=complement)
  print, 'complement:', complement

  ; draw from an string array without replacement
  print, 'subset:    ',hubRandom_sample(3, Values=['a','b','c','d','e'], /Sort, Complement=complement)
  print, 'complement:', complement
  
  
end