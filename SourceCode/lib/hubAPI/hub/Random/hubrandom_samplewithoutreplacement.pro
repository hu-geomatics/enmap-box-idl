function hubRandom_sampleWithoutReplacement, subsetSize, setSize,$
  Seed=seed, Values=values, Complement=complement, Sort=sort

  if isa(values) then begin
    if isa(setSize) then message, 'SetSize argument not allowed when Value keyword is used.'
    setSize = n_elements(values)
  endif

  currentIndices = []
  currentComplementIndices = indgen(setSize)
  currentSubsetSize = 0ll
  
  while currentSubsetSize ne subsetSize do begin
    newIndicesWithReplacement = hubRandom_sampleWithReplacement(subsetSize-currentSubsetSize, Seed=seed, VALUES=currentComplementIndices, COMPLEMENT=newComplementIndices)
    newIndices = hubmathHelper.getSet(newIndicesWithReplacement)
    currentIndices = [currentIndices, newIndices]
    currentComplementIndices = newComplementIndices
    currentSubsetSize += n_elements(newIndices)
  endwhile

  ; sort indices if needed
  if keyword_set(sort) then begin
    if isa(currentIndices) then currentIndices = currentIndices[sort(currentIndices)]
    if isa(currentComplementIndices) then currentComplementIndices = currentComplementIndices[sort(currentComplementIndices)]
  endif

  ; prepare result, return either sample indices or sample values
  if isa(values) then begin
    if arg_present(complement) then complement = values[currentComplementIndices]
    result = values[currentIndices]
  endif else begin
    if arg_present(complement) then complement = currentComplementIndices
    result = currentIndices
  endelse

  return, result
end