function hubRandom_sampleWithReplacement, subsetSize, setSize,$
  Seed=seed, Values=values, Complement=complement, Sort=sort
  
  ; get set size from value array if provided
  if isa(values) then begin
    if isa(setSize) then message, 'SetSize argument not allowed when Value keyword is used.'
    setSize = n_elements(values)
  endif
  
  ; draw random sample
  indices = randomu(seed, subsetSize, /LONG) mod setSize
  
  ; calculate complement if needed
  if arg_present(complement) then begin
    indexFlags = bytarr(setSize)
    indexFlags[indices] = 1b
    complementIndices = where(/NULL, ~indexFlags)
  endif
  
  ; sort indices if needed
  if keyword_set(sort) then begin
    if isa(indices) then indices = indices[sort(indices)]
    if isa(complementIndices) then complementIndices = complementIndices[sort(complementIndices)]
  endif
  
  ; prepare result, return either sample indices or sample values
  if isa(values) then begin
    if arg_present(complement) then complement = values[complementIndices]
    result = values[indices]
  endif else begin
    if arg_present(complement) then complement = complementIndices
    result = indices
  endelse
  
  return, result
end