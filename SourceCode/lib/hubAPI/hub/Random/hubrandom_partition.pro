;+
; :Author: Andreas Rabe
;-

;+
; :Description:
;    Returns a random partition of given size.
;
; :Params:
;    partitionSizes : in, required, type=number[]
;      Array of partition sizes.
;
; :Keywords:
;    Seed :  in, optional, type=any
;      Random number seed.
;      
;    ToList : in, optional, type=boolean
;      Set to return a list with the individual partition indices. 
;
;-
function hubRandom_partition, partitionSizes, Seed=seed, ToList=toList
  setSize = total(partitionSizes, /INTEGER)
  numberOfPartitions = n_elements(partitionSizes)
  partition = intarr(setSize)
  complement = indgen(setSize)
  
  for i=1,numberOfPartitions-1 do begin
    indices = hubRandom_sample(partitionSizes[i], Values=complement, Complement=newComplement, Seed=seed)
    complement = newComplement
    partition[indices] = i
  endfor
  
  if keyword_set(toList) then begin
    result = list()
    histo = histogram(partition, BINSIZE=1, MIN=0, MAX=numberOfPartitions-1, REVERSE_INDICES=reverseIndices, LOCATIONS=locations)
    result = hubMathHelper.reverseIndicesToList(reverseIndices, locations)
  endif else begin
    result = temporary(partition)
  endelse
  return, result
end

pro test_hubRandom_partition
  ; partition into 3 folds of sizes [4,3,2]
  print, 'partition:',hubRandom_partition([4,3,2])
  
  ; return a list of indices for each partition fold
  print, 'partition:'
  foreach indices,hubRandom_partition([4,3,2], /ToList),i do begin
    print, '#'+strtrim(i,2), indices
  endforeach  
end