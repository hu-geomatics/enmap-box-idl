;+
; :Description:
;    Returns continuum removed spectrum and optionally the convex hull.
;
; :Params:
;    spectrum: in, required, type=number[]
;      Use this argument to specify the spectrum in form of an one-dimensional array of numbers. Note that values below zero are set to zero.
;      
;    wavelength: in, optional, type=number[], default=[0...bands-1]
;      Use this argument to specify an the corresponding wavelength. 
;      If this argument is omitted, the band indices [0, 1, ...] are used.
;      
; :Keywords:
;    HullIndices: out, optional, type=number[]
;      A named variable that, on exit, contains the array of convex hull indices 
;
; :Examples:
;    Calculate continuum-removed spectrum::
;    
;      ; create test data
;      
;      wavelength = [0.45, 0.46, 0.48, 0.50, 0.51, 0.53, 0.54, 0.56, 0.57, 0.59, 0.60, 0.62, 0.63, 0.65, 0.66, 0.68, 0.69, 0.71, 0.72, 0.74, 0.76, 0.77, 0.79, 0.80, 0.82, 0.83, 0.85, 0.86, 0.88, 0.89, 0.89, 0.91, 0.93, 0.94, 0.96, 0.97, 0.99, 1.01, 1.02, 1.04, 1.05, 1.07, 1.08, 1.10, 1.11, 1.13, 1.14, 1.16, 1.17, 1.19, 1.20, 1.21, 1.23, 1.24, 1.26, 1.27, 1.29, 1.30, 1.31, 1.33, 1.34, 1.51, 1.52, 1.53, 1.55, 1.56, 1.57, 1.59, 1.60, 1.61, 1.63, 1.64, 1.65, 1.67, 1.68, 1.69, 1.70, 1.72, 1.73, 1.74, 1.75, 1.76, 1.78, 1.79, 1.80, 1.81, 1.99, 2.01, 2.03, 2.05, 2.07, 2.08, 2.10, 2.12, 2.14, 2.16, 2.17, 2.19, 2.21, 2.23, 2.24, 2.26, 2.28, 2.29, 2.31, 2.33, 2.34, 2.36, 2.38, 2.39, 2.41, 2.42, 2.44, 2.45]
;      spectrum = [180, 180, 184, 198, 268, 371, 468, 509, 452, 379, 363, 327, 308, 274, 236, 244, 928, 1611, 2282, 2951, 3240, 3525, 3807, 3847, 3889, 3930, 3972, 4021, 4068, 4119, 4104, 4127, 4111, 4014, 3911, 3876, 3957, 4039, 4110, 4214, 4267, 4281, 4233, 4187, 4116, 3937, 3671, 3457, 3425, 3393, 3369, 3406, 3477, 3573, 3625, 3623, 3573, 3447, 3302, 3050, 2554, 819, 922, 1038, 1127, 1234, 1320, 1403, 1479, 1560, 1607, 1655, 1693, 1661, 1672, 1650, 1598, 1560, 1511, 1460, 1396, 1353, 1194, 1141, 1078, 974, 273, 300, 343, 369, 424, 455, 508, 532, 581, 583, 613, 630, 662, 670, 597, 528, 491, 444, 410, 362, 333, 314, 287, 250, 214, 217, 207, 190]
;    
;      ; calculate result
;    
;      continuumRemovedSpectrum = hubContinuumRemoval(spectrum, wavelength, HullIndices=hullIndices)
;        
;      ; plot result
;    
;      !null = plot(wavelength, spectrum, WINDOW_TITLE='Spectrum with Convex Hull')
;      !null = plot(wavelength[hullIndices], spectrum[hullIndices], /Overplot)
;      !null = plot(wavelength, continuumRemovedSpectrum, WINDOW_TITLE='Continuum-Removed Spectrum')
;      
;-
function hubContinuumRemoval, spectrum, wavelength, HullIndices=hullIndices, HullSpectrum=hullSpectrum

  if ~isa(spectrum) then return, !null

  ; prepare data

  bands = n_elements(spectrum)
  wavelength = isa(wavelength) ? wavelength : findgen(bands)
  x = [wavelength[0], wavelength, wavelength[-1]]
  x = float(x)
  y = spectrum > 0
  y = [-1, y, -1]

  ; calculate convex hull, 
  ; try to catch colinearity error, return constant spectrum in this case
  
  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    if !ERROR_STATE.MSG eq 'TRIANGULATE: Not enough valid and unique points specified.' then begin
      result = make_array(bands, VALUE=1.)
      hullIndices = [0, bands]
      return, result
    endif else begin
      message, !ERROR_STATE.MSG
    endelse
  endif

  triangulate, x, y, triangles, hullIndices
  catch, /CANCEL
  hullIndices = reverse(hullIndices[2:*])-1
  
  ; interpolate hull for the whole profile

  hullSpectrum = interpol(spectrum[hullIndices], wavelength[hullIndices], wavelength)
  
  ; remove continuum and return result
  ; (handle mathErrors that may occur when spectrum and hull value are both zero, set result to one in this case)
  
  hubMath_mathErrorsOff, state
  result = spectrum/hullSpectrum
  !null = check_math()
  hubMath_mathErrorsOn, state
  result[where(/NULL, ~finite(result))] = 1

  return, result
  
end