;+
; :Description:
;    This function returns values that have been stored with `hub_getAppState` before.
;    This allows to re-use values even after application that stored the values has been finished.
;
; :Params:
;    application: in, required, type = string
;     Name of your application.
;     
;    appStateKey: in, optional, type = string
;     The key that identifies the value you want to get.
;     
;  :Examples:
;      This example shows you how to use `hub_setAppState` and `hub_getAppState`. 
;      
;      First we define a simple application. Its task to print random number which is generated
;      the first time the application is called and re-used all other times::
;      
;        pro sampleApplication
;          print, 'Start Application...'
;        
;          ;Try to restore the random number n
;          n = hub_getappState('MyApp', 'myRandomNumber')
;        
;          ;if not restored, generate the number
;          if ~isa(n) then begin
;            n = randomu(systime(1))
;            print, 'Random number generated'
;          endif else begin
;            print, 'Random number restored'
;          endelse
;        
;          ;save state of this application
;          hub_setAppState, 'MyApp', 'myRandomNumber', n
;          print, 'Application finished!'
;          print, ''
;        end
;    
;    Now we create a program that simulates the runtime of an IDL session where out application 
;    is called several times::
;    
;          pro sampleRuntime
;            ;2x calls of the same application
;            sampleApplication
;          
;            sampleApplication
;          end
;     
;    Running `sampleRuntime` the first time should print:: 
;    
;          Start Application...
;          Random number generated
;          Application finished!
;          
;          Start Application...
;          Random number restored
;          Application finished!
;
;    
;    Without a reset of IDL, running `sampleApplication` again and again will always print that the number
;    was restored. 
;    
;-
function hub_getAppState, application, appStateKey, Default=default, All=all
  defsysv, '!hubAppStates', EXISTS = exists
  
  default = isa(default)? default : !NULL
  
  if ~exists then begin
    if keyword_set(all) then begin
      return, hash()
    endif else begin
      return, default
    endelse
  endif
  appStates = !hubAppStates.appStates
  
  if keyword_set(all) then begin
    return, appStates
  endif
  
  ;check input parameters
  if isa(application) && ~appStates.hasKey(application) then begin
    return, default
  endif
  
  if isa(appStateKey) && ~(appStates[application]).hasKey(appStateKey) then begin
    return, default
  endif
  
  ;return requested values
  if isa(appStateKey) then return, (appStates[application])[appStateKey]
  if isa(application) then return, appStates[application]
  return, appStates
end

;+
; :Description:
;    Describe the procedure.
;
;
;
;
;
;-
pro test_hub_getAppState
  
  print, hub_getappstate() ;all
  print, hub_getAppState('myApp') ;single app
  print, hub_getAppState('myApp', 'myKey') ;single app key
  print, hub_getAppState('myApp', 'myKey', default=24) ;single app key
  
  ;add state
  ;hub_setAppState, 'myApp', appStateHash=Hash('myKey','key value string')
  hub_setAppState, 'myAppB', 'keyB', 'keyB value'
  ;
  print,'Single:', hub_getAppState('myApp', 'myKey') ;single app key
  print,'App   :', hub_getAppState('myApp') ;single app
  print,'ALL   :', hub_getAppState() ;all
end