;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This function returns a routine name from the IDL calling stack level specified by the `level` argument.  
;
; :Params:
;    level : in, optional, type=int, default=0
;      Use this keyword to specify the stack level from which the routine name should be returned. 
;      Setting `level = 0` (default) returns the current routine name, `level = 1` returns the callers routine
;      , `level = 2` returns the callers caller routine name, and so on.
;      
; :Author: Andreas
;-
function hubTraceback_getCaller, level

  level_ = isa(level) ? level : 0
  level_ += 1

  ; extract the routine name from the IDL calling stack
  callStack = hubTraceback_getStack()
  ; handle: special case for $MAIN$ routine
  if level_ gt n_elements(callStack)-1 then begin
    message,'$MAIN$ has no caller routine.' 
  endif
  
  routineName = callStack[level_]
  return,routineName

end
