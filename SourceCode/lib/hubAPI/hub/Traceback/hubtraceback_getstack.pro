;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This function returns the IDL calling stack. 
;-
function hubTraceback_getStack
  help, CALLS=callStack
  callStack = strupcase(callStack[1:*])
  for routineIndex=0,n_elements(callStack)-1 do begin
    callStack[routineIndex] = (strsplit(callStack[routineIndex],' ',/EXTRACT))[0]
  endfor
  return, callStack
end
