;+
; :Description:
;    Turns math error checking on.
;    
; :Params:
;    state: in, optional, type={0 | 1 | 2}, default=2
;      Use this keyword to set the value of !EXCEPT.
;-
pro hubMath_mathErrorsOn, state
  !null = check_math()
  !EXCEPT = isa(state) ? state : 2
end