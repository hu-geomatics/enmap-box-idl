;+
; :Description:
;    Returns the data span (max-min).
;
; :Params:
;    x: in, required, type = numeric[]
;     Data values.
;     
;-
function hubMathSpan, x
  min = min(x, MAX=max)
  result = max-min
  return, result
end

;+
; :Hidden:
;-
pro test_hubMathSpan
  result = hubMathSpan([1,2,3,4,5])
  print, result
end
