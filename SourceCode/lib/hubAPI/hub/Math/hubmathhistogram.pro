;+
; :Author: Andreas
;-

;+
; :Description:
;    Wrapper for `hubMathIncHistogram`
;
; :Params:
;    x
;    numberOfBins
;
; :Keywords:
;    Integer
;    InPercentileRanks
;    OutPercentiles
;-
function hubMathHistogram, x, range, numberOfBins, Integer=integer, $
  InPercentileRanks=inPercentileRanks, OutPercentiles=outPercentiles, GetReverseIndices=getReverseIndices

  incHistogram = hubMathIncHistogram(range, numberOfBins, Integer=integer, GetReverseIndices=getReverseIndices)
  incHistogram.addData, x
  histogram = incHistogram.getResult()
  if isa(inPercentileRanks) then begin
    outPercentiles = incHistogram.getPercentiles(inPercentileRanks)
  endif
  return, histogram
end

pro test_hubMathHistogram
  data = randomn(seed, 1000)
  histogram = hubMathHistogram(data, [min(data),max(data)], 100, InPercentileRanks=findgen(101), OutPercentiles=outPercentiles)
  plot, histogram['histogram'], PSYM=10
  plot, outPercentiles
  
end