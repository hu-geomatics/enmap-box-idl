;+
; 
; A hubMathIncVariance can be used to calculate the variance of differenct data values incrementally.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Constructor to initialize the `hubMathIncVariance` object.
;
; :Params:
;    mean: in, required, type = numeric
;     The mean of all data values that will be added incrementally using `hubMathIncVariance::addData`.
;
;
;-
function hubMathIncVariance::init $
  , mean

  self.mean = mean
  return, 1b

end

;+
; :Description:
;    Adds data values to calculate the variance from.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;-
pro hubMathIncVariance::addData $
  , x

  self.hubMathInc::addData, x
  if ~isa(x) then return
  self.sum += total((x-self.mean)^2, /Double)

end


;+
; :Description:
;    Returns the squared sum without dividing it by the number of elements.
;-
function hubMathIncVariance::getSum

  return, self.sum
  
end

;+
; :Description:
;    Returns the variance of data values added with `hubMathIncVariance.addData`.
;-
function hubMathIncVariance::getResult
  if self.numberOfSamples eq 0 then begin
    result = !VALUES.D_NAN
  endif else begin
    result = self.sum / (self.numberOfSamples)
  endelse

  return, result

end

;+
; :Hidden:
;-
pro hubMathIncVariance__define
  
  struct = {hubMathIncVariance $
    , inherits hubMathInc $
    , mean : 0d $ 
    , sum : 0d $
  }
end

;+
; :Hidden:
;-
pro test_hubMathIncVariance
  
  
  incVariance = hubMathIncVariance(0)
  print,incVariance.getResult()
return
  
  ; test data 
  
  seed=1
  x1 = randomu(seed, 100)
  x2 = randomu(seed, 100)
  x3 = randomu(seed, 100)

  ; IDL variance
  
  print, stddev([x1,x2,x3])^2
  print, total( ([x1,x2,x3] - mean([x1,x2,x3]))^2 ) 
  ; incremental variance
  
  incMean = hubMathIncMean()
  incMean.addData, x1
  incMean.addData, x2
  incMean.addData, x3
  incVariance = hubMathIncVariance(incMean.getResult())
  incVariance.addData, x1
  incVariance.addData, x2
  incVariance.addData, x3
  print,incVariance.getResult()
  
end