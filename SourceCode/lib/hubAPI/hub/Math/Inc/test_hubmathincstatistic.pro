;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
; :hidden:
;-

pro test_hubMathIncStatistic

  ; test data 
  
  
  
  x = list()
  y = list()
  xAll = []
  yAll = []
  numberOfTiles = 5
  seed = 1
  for i=0,numberOfTiles-1 do begin
    x.add, randomu(seed, 100, /DOUBLE)
    y.add, randomu(seed, 100, /DOUBLE)
    xAll = [xAll, x[i]]
    yAll = [yAll, y[i]]
  endfor
  
  x.add, [] ; add some empty data
  y.add, [] ; add some empty data
  
  
  ; MIN, MAX and MEAN incremental stats
  
  incMeanX = hubMathIncMean()
  incRangeX = hubMathIncRange()
  incMeanY = hubMathIncMean()
  incRangeY = hubMathIncRange()
  
  for i=0,numberOfTiles-1 do begin
    incMeanX.addData, x[i]
    incRangeX.addData, x[i]
    incMeanY.addData, y[i]
    incRangeY.addData, y[i]
  endfor
  
  ; VAR, COVAR and CORR incremental stats
  
  incVarX = hubMathIncVariance(incMeanX.getResult())
  incVarY = hubMathIncVariance(incMeanY.getResult())
  incCovar = hubMathIncCovariance(incMeanX.getResult(), incMeanY.getResult())
  incCorr = hubMathIncCorrelation(incMeanX.getResult(), incMeanY.getResult())

  for i=0,numberOfTiles-1 do begin
    incVarX.addData, x[i]
    incVarY.addData, y[i]
    incCovar.addData, x[i], y[i]
    incCorr.addData, x[i], y[i]
  endfor

  print, '            min(x)       max(x)      mean(x)    var(x)     covar(x,y)    corr(x,y)'
  print, 'hub:',[incRangeX.getResult(), incMeanX.getResult(), incVarX.getResult(), incCovar.getResult(),              incCorr.getResult()]
  print, 'idl:',[min(xAll),max(xAll),   mean(xAll),           stddev(xAll)^2,      correlate(xAll,yAll, /COVARIANCE), correlate(xAll,yAll)]
  print

end