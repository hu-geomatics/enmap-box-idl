;+
;  A hubMathIncHistogram object can be used to calculate a 1D histogram incrementally.
;
; :Author: 
;   Andreas Rabe (andreas.rabe@geo.hu-berlin.de) 
;   Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;   
; :Examples:
;   This example shows how to use the hubMathIncHistogram object, add data values incrementally and
;   to return the histogram information::
;   
;      range = [0,1] 
;      nBins  = 10  ;number of bins
;      incHist = hubMathIncHistogram(range, nBins)
;      
;      ;this loop could be an image tile reading loop
;      seed = systime(1)
;      for loop = 1, 20 do begin
;        data = randomn(seed, 100)
;        ;add the data values incrementally
;        incHist.addData, data
;      endfor
;      
;      hist = incHist.getResult()
;      print, hist  
;      print, hist['histogram']
;   
; 
;-

;+
; :Description:
;    Constructor of a new `hubMathIncHistogram` object. 
;
; :Params:
;    range: in, required, type = numeric[2]
;     The input data range of all data values. Values out of this range won't be counted in histogram. 
;      
;    numberOfBins: in, required, type = int, default = 256
;     Final number of bins to calculate for data values.
;     
;     
; :Keywords:
;    Integer: in, optional, type = bool
;     Set this to use integer/natural numbers as bin boundaries. 
;
;-
function hubMathIncHistogram::init $
  , range $
  , numberOfBins $
  , Integer=integer $
  , GetReverseIndices=getReverseIndices
  
  if total(FINITE(range)) ne 2 then message, 'Parameter range must contain two finite values', /INFORMATIONAL
  
  
  self.numberOfBins = isa(numberOfBins) ? numberOfBins : 256u
  self.min = min(range)
  self.integer = keyword_set(integer)
  self.getReverseIndices = keyword_set(getReverseIndices)
  if self.integer then begin
    self.max = max(range)+1
    self.numberOfBins <= max(range[1]) - min(range) +1
  endif else begin
    self.max = self.getNextNumber(range[1], TYPESPECIFIC=0)
    ;range[1]+(machar(/Double)).eps
  endelse
  
  self.span = self.max - self.min
  if self.getReverseIndices then begin
    self.reverseIndices = list(Length=self.numberOfBins)
  endif
  self.histogram = ptr_new(make_array(self.numberOfBins, /UL64))
  return, 1b

end


;+
; :private:
; :Description:
;    Recursion to find the successor of a floating point number.
;    
;
; :Params:
;    num: in, required, type=float/double
;     The number to find the successor value for.
;     
;    eps: in, required, type=float/double
;      The machine dependent floating point precission from the machar struct.
;      See IDL help 'machar'.
;      
;    factorLeft: in, required, type=unsigned long integer
;      Multiplicator for eps to define the left boundary by 
;      `left = num + eps * factorLeft`
;       
;    factorRight: in, required, type=unsigned long integer
;      Multiplicator for eps to define the right boundary by 
;      `right = num + eps * factorRight`
;
; :Keywords:
;    iteration: 
;
; :Author: geo_beja
;-
function hubMathIncHistogram::getNextNumberRecursion, num, eps, factorLeft, factorRight, iteration=iteration
  iteration++
  ;check for errors
  if iteration ge 100 then message, 'too many recursion steps required to get the next number'
  if factorLeft ge factorRight then message, 'factorLefteft < factorRightight'
  if num ge num + eps * factorRight then message, 'num >= num + eps * factorRightight'
  if num lt num + eps * factorLeft then message, 'num < num + eps * factorLefteft'
  
  
  ;stop criterion 1: factor * right border is close enough 
  if num ge (num +   factorRight   * eps) then begin
    return, num + (factorRight+1ul) * eps
  endif
  ;stop criterion 2: factor * left border is close enough
  if num ge (num + factorLeft * eps) && (num lt (num + (factorLeft+1ul) * eps)) then begin
    return, num + (factorLeft+1ul) * eps
  endif 
  
  ;define middle (fM) between left and right border
  factorMiddle = (factorLeft + factorRight) / 2ul
  newNum = num + factorMiddle * eps 
  
  if newNum gt num then begin
    return, self.getNextNumberRecursion(num, eps, factorLeft, factorMiddle, iteration=iteration)
  endif else begin
    return, self.getNextNumberRecursion(num, eps, factorMiddle, factorRight, iteration=iteration)
  endelse
  
end


;+
; :private:
; :Description:
;    Wrapper routine to find the successor of a number.
;    
;
; :Params:
;    number: in, required, type=numberic
;     The number to return the successor for.
;
; :Keywords:
;    typeSpecific: in, optional, type=bool, default=false
;     Set this keyword to return the type-specific successor. For all integer values this is
;     number + 1. For floating point values this is the smallest number number2 with number < number2. 
;
; :Author: geo_beja
;-
function hubMathIncHistogram::getNextNumber, number, typeSpecific=TypeSpecific
  if ~FINITE(number) then begin
    message, 'Number can not be NaN or Inf', /INFORMATIONAL
    return, number
  endif
  
  ;get next number as possible for the specific number type used
  ;this might be useful when hubMathIncHistogram can work in pure integer mode
  ; (/Integer -> pure integer input, boundaries returned as pure integer)
  ; 
  if keyword_set(typeSpecific) then begin
     
     type = TYPENAME(typespecific) eq 'STRING' ? typespecific :typename(number)
      switch STRUPCASE(type) of
        'BYTE' :
        'INT'  :
        'UINT' :
        'LONG' :
        'ULONG':
        'LONG64':
        'ULONG64':
        'ULONG64': begin
                      return, number+1
                      break
                   end 
        'FLOAT':  begin
                    eps = (machar()).eps
                    break
                  end
        'DOUBLE': begin
                    eps = (machar(/DOUBLE)).eps
                    break
                  end
        else: message, 'can not find successor of data type '+type
      endswitch
  endif else begin
    ;always compare with double bin-boundaries 
    eps = (machar(/DOUBLE)).eps
  endelse
  
  if number + eps gt number then return, number + eps
  factor = 1ul
  while ~(number lt number + factor *eps) do begin
    factor *= 10ul 
  endwhile
  
  iteration = 0ul
  return, self.getNextNumberRecursion(number, eps, factor/10ul, factor, iteration=iteration)
end

;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    percentileRanks: in, required, type= {int | int[]}
;     Scalar or array with percentile ranks that are to calculate.
;     Example::
;       percentileRank=10 ;to get the 10 percent percentile
;       percentileRank = [5,95] ;to get the 5 and 95% percentile 
;
;-
function hubMathIncHistogram::getPercentiles, percentileRanks
  iFalse = where(percentileRanks lt 0 or percentileRanks gt 100, /NULL)
  if isa(iFalse) then begin
    message, 'Percentile Rank must be within range: 0 <= rank <= 100 
  endif
  
  hist = self.getResult()
  
  cumDist = hist['cumulativeDistribution']*100
  validHisto = total(finite(cumDist)) ne 0
  percentiles = make_array(n_elements(percentileRanks), value=0, /DOUBLE)
  if validHisto then begin
    foreach percentileRank, percentileRanks, i do begin
      iBin = (where(cumDist ge percentileRank, /NULL))[0]
      
      ;the value of right and left bin-border
      percentileA = (hist['binStart'])[iBin]
      percentileB = (hist['binEnd'])[iBin]
      
      ;the rank of each border, given by its percentile [0,100]
      rankA = (iBin eq 0)? 0d : cumDist[iBin-1]
      rankB = cumDist[iBin]
      f = (rankB - percentileRank)/(rankB-rankA)
      percentiles[i] = percentileA * f + percentileB *(1-f)
  
    endforeach
  endif
  return, percentiles
end

;+
; :Description:
;    Adds data values to calculate the histogram from. 
;    Repeat this routine to add more values.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;-
pro hubMathIncHistogram::addData $
  , x
  
  if ~isa(x) then return
  
  ; exclude outliers
  
  validIndices = where(/NULL, (x ge self.min) and (x lt self.max))
  x_ = x[validIndices]

  if ~isa(x_) then return  
  
  self.hubMathInc::addData, x_
  
  ;stretch data to [0,numberOfBins-1] range

  xStreched = ((temporary(x_)-self.min)/self.span*self.numberOfBins)<(self.numberOfBins-1)
  if self.integer then begin
    xStreched = round(xStreched) ; rounding is required because of floating point precision issues 
  endif
  if self.numberOfBins le 256 then begin
    xStreched = byte(xStreched)
    min = 0b
    max = byte(self.numberOfBins-1)
    binsize = 1b
  endif else begin
    xStreched = uint(xStreched)
    min = 0u
    max = uint(self.numberOfBins-1)
    binsize = 1u
  endelse
  
  ;calculate histogram and update
  if self.getReverseIndices then begin
    histogram = histogram(xStreched, MIN=min, MAX=max ,BINSIZE=binsize, LOCATIONS=locations, REVERSE_INDICES=reverse_indices)
    reverseIndices = hubHelper.getHistogramLocationIndices(reverse_indices, self.numberOfBins)
    for i=0, self.numberOfBins-1 do begin
      if isa(reverseIndices[i]) then begin
        self.reverseIndices[i] = [self.reverseIndices[i], validIndices[reverseIndices[i]]+self.reverseIndicesOffset]
      endif
    endfor
    self.reverseIndicesOffset += n_elements(x)
  endif else begin
    histogram = histogram(xStreched, MIN=min, MAX=max ,BINSIZE=binsize)
  endelse
  *self.histogram += histogram
end

;+
; :Description:
;    Returns a hash containing the following values::
;    
;      nBX = number of bins for X data values
;      nbY = number of bins for Y data values
;      
;      key                    | type        | description
;     ------------------------+-------------+--------------
;      binStart               | double      | vector with bin minimum values (left boundaries) 
;      binEnd                 | double      | vector with bin maximum values (right boundaries)
;      binCenter              | double      | vector with bin center values
;      binSize                | double      | size of single bin 
;      numberOfBins           | uint        | number of bins
;      range                  | double      | value data range 
;      numberOfSamples        | long64      | total number of samples
;      hasSamples             | bool        | true if numberOfSamples > 0   
;      histogram              | ulong64[nB] | histogram (array with counts for each bin position)
;      cumulativeHistogram    | ulong64[nB] | cumulative histogram
;      probabilityDensity     | double[nB]  | probability of each bin
;      cumulativeDistribution | double[nB]  | cumulative probability of each bin
;      -----------------------+-------------+-----------------
;-
function hubMathIncHistogram::getResult
  
  binSize = self.span / self.numberOfBins
  binStart = self.min+dindgen(self.numberOfBins)*binSize
  binEnd = binStart+binSize
  
  binCenter = binStart+binSize/2d
  histogram = *self.histogram
  cumulativeHistogram = total(histogram, /PRESERVE_TYPE, /CUMULATIVE)
  probabilityDensity = 1d*histogram/cumulativeHistogram[-1]
  cumulativeDistribution = 1d*cumulativeHistogram/cumulativeHistogram[-1]

  result = Dictionary()
  result['binStart'] = binStart
  result['binEnd'] = binEnd
  result['binCenter'] = binCenter
  result['binSize'] = binSize
  result['numberOfSamples'] = self.getNumberOfSamples()
  result['hasSamples'] = result['numberOfSamples'] ne 0 
  result['numberOfBins'] = self.numberOfBins
  result['histogram'] = histogram
  result['cumulativeHistogram'] = cumulativeHistogram
  result['probabilityDensity'] = probabilityDensity
  result['cumulativeDistribution'] = cumulativeDistribution
  if self.getReverseIndices then begin
    result['reverseIndices'] = self.reverseIndices
  endif
  return, result

end

;+
; :hidden:
;-
pro hubMathIncHistogram__define
  
  struct = {hubMathIncHistogram $
    , inherits hubMathInc $
    , min : 0d $
    , max : 0d $ 
    , span : 0d $
    , histogram : ptr_new() $
    , numberOfBins : 0u $
    , integer : 0b $
    , getReverseIndices : 0b $
    , reverseIndices : obj_new() $
    , reverseIndicesOffset : 0ll $
  }
end

;+
; :hidden:
;-
pro test_hubMathIncHistogram
    ;incHist = hubMathIncHistogram([0,100])
    
    incHist = hubMathIncHistogram([!Values.D_Nan, !Values.D_Nan])
    print, incHist.getResult()
    
    stop
    
    path = hub_getTestImage('Hymap_Berlin-A_Image')
    img = hubIOImgInputImage(path)
    dataIgnoreValue = img.getMeta('data ignore value')
    
    bandIndex = 5 
    tileLines = 10
    incRange = hubMathIncRange()
    img.initReader, tileLines, /TILEPROCESSING, /Slice, SUBSETBANDPOSITIONS=bandIndex
    while ~img.tileProcessingDone() do begin
      data = img.getData() ;get you tile data
      
      ;exclude data values equal the data ignore value 
      if isa(dataIgnoreValue) then begin
        data = data[where(data ne dataIgnoreValue, /NULL)]
      endif
      incRange.addData, data
    endwhile
    img.finishReader
    
    range = incRange.getResult()
    print, 'Range min:', range[0], ' max:', range[1]
    
    ;create the histogram object (256 bins by default)
    incHistogram = hubMathIncHistogram(range)
    
    ;re-initialize the image reader & read all image values again
    img.initReader, tileLines, /TILEPROCESSING, /Slice, SUBSETBANDPOSITIONS=bandIndex
    while ~img.tileProcessingDone() do begin
      data = img.getData() ;get you tile data
      
      ;exclude data values equal the data ignore value 
      if isa(dataIgnoreValue) then begin
        data = data[where(data ne dataIgnoreValue, /NULL)]
      endif
      incHistogram.addData, data
    endwhile
    img.cleanup
      
    hist = incHistogram.getResult()
    
    ;print histrogram properties
    print, hist
    
    ;plot histogram 
    cgPlot, hist['binCenter'] $
          , hist['histogram'] , psym=10 $
          , XTitle = 'bin center value', YTitle='Counts' $
          , background = 'white' $
          , color='blue' $
          , Title = 'Histogram'
    
    
   stop
  
  ; test data 
  
  data = [0,0,0,0,0,0,0]
  data = randomn(seed, 100)
  range = [min(data), max(data)]
  incHist = hubMathIncHistogram(range,10)
  incHist.addData, data
  hist = incHist.getResult()
  foreach k, hist.keys() do begin
    ;print, k
    help,hist[k], OUTPUT= s
    print, k, s
  endforeach
  
  stop
  p10 = incHist.getPercentiles(50)

  
  

  ; incremental histogram
  
  range = [min([x1,x2,x3]),max([x1,x2,x3])]
  incHist = hubMathIncHistogram(range, 30, Integer=testInteger)
  incHist.addData, x1
  incHist.addData, x2
  incHist.addData, x3
  incHistResult = incHist.getResult()
  p10 = incHist.getPercentiles(10)
 ; print,incHistResult
  print,'     BinStart        Histo     CumHisto      Density      CumDist'
  print,transpose([[incHistResult['binStart']],[incHistResult['histogram']],[incHistResult['cumulativeHistogram']] $
    , [incHistResult['probabilityDensity']],[incHistResult['cumulativeDistribution']]])
  plot,incHistResult['binStart'],incHistResult['histogram'],PSYM=10
  
end

pro test_hubMathIncHistogram2
  incHist = hubMathIncHistogram([1,3], /Integer, /GetReverseIndices)
  incHist.addData, [1,2,2]
  incHist.addData, [1,5,6]
  incHist.addData, [1,8,9]
  incHistResult = incHist.getResult()
  print, incHistResult['binStart']
  print, incHistResult['reverseIndices']
stop
end