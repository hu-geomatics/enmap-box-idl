;+
; A hubMathIncHistogram2D object can be used to calculate a 2D histogram incrementally.
;
; :Author: 
;   Andreas Rabe (andreas.rabe@geo.hu-berlin.de) 
;   Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;   
; :Examples:
;   This example shows how to use the hubMathIncHistogram2D object, add data values incrementally and
;   to return the histogram information::
;   
;      rangeX = [0,1] ;range X values
;      rangeY = [0,1] ;range Y values
;      nBins  = 10 ;number of bins
;      incHist = hubMathIncHistogram2D(rangeX, rangeY, nBins)
;      
;      ;this loop could be an image tile reading loop
;      seed = systime(1)
;      for loop = 1, 20 do begin
;        dataX = randomn(seed, 100)
;        dataY = randomn(seed, 100)
;        ;add the data values incrementally
;        incHist.addData, dataX, dataY
;      endfor
;      
;      hist = incHist.getResult()
;      print, hist  
;      print, hist['histogram2D']
;   
; 
;-

;+
; :Description:
;    Constructor of a new `hubMathIncHistogram2D` object. 
;
; :Params:
;    rangeX: in, required, type = numeric[2]
;     The input data range of all X values. Values out of this range won't be counted in histogram. 
;      
;    rangeY: in, required, type = numeric[2]
;     The input data range of all Y values. Values out of this range won't be counted in histogram.
;    
;    numberOfBinsX: in, required, type = int, default = 256
;     Final number of bins to calculate for X data values.
;     
;    numberOfBinsY: in, required, type = int, default = `numberOfBinsX`
;     Final number of bins to calculate for Y data values.
;     
; :Keywords:
;    Integer: in, optional, type = bool
;     Set this to use integer/natural numbers as bin boundaries. 
;
;-
function hubMathIncHistogram2D::init $
  , rangeX $
  , rangeY $
  , numberOfBinsX $
  , numberOfBinsY $
  , Integer=integer
  
  if total(finite(rangeX)) ne 2 then message, 'Parameter rangeX must contain finite values'
  if total(finite(rangeY)) ne 2 then message, 'Parameter rangeY must contain finite values'
  
  ;if set, the binsize will be a natural number 
  self.Integer = keyword_set(integer)
  
  self.numberOfBinsX = isa(numberOfBinsX) ? numberOfBinsX : 256u
  self.numberOfBinsY = isa(numberOfBinsY) ? numberOfBinsY : self.numberOfBinsX
    
  
  
  ;use exact integer borders
  if keyword_set(integer) then begin
    self.maxX = fix(rangeX[1]) ;+1
    self.maxY = fix(rangeY[1]) ;+1
    self.minX = fix(rangeX[0])
    self.minY = fix(rangeY[0])
    
    self.numberOfBinsX <= long(self.maxX - self.minX) +1
    self.numberOfBinsY <= long(self.maxY - self.minY) +1
    
    self.binSizeX = 1
    self.binSizeY = 1 
  endif else begin
  
    self.minX = rangeX[0]
    self.minY = rangeY[0]
    self.maxX = rangeX[1]
    self.maxY = rangeY[1]
  
    
    nextX = self.hubMathIncHistogram::getNextNumber(rangeX[1], type='double')
    nextY = self.hubMathIncHistogram::getNextNumber(rangeY[1], type='double')
    
    self.binSizeX = (nextX - self.minX) / self.numberOfBinsX
    self.binSizeY = (nextY - self.minY) / self.numberOfBinsY
  
  endelse
  
  self.histogram2D = ptr_new(make_array(self.numberOfBinsX, self.numberOfBinsY, value=0, /UL64))
  return, 1b

end



;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    percentileRanks: in, required, type= {int | int[]}
;     Scalar or array with percentile ranks that are to calculate.
;     Example::
;       percentileRank=10 ;to get the 10 percent percentile
;       percentileRank = [5,95] ;to get the 5 and 95% percentile 
;
;-
function hubMathIncHistogram2D::getPercentiles, percentileRanks
  return, !null
end

;+
; :Description:
;    Adds data values to calculate the 2D histogram from. 
;    Repeat this routine to add more values.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;    y: in, required, type = numeric
;     Scalar / Vector of data values. Must have same length as `x`.
;-
pro hubMathIncHistogram2D::addData $
  , x, y

  if ~isa(x) && ~isa(y) then return
  
  ; exclude outliers
  i = where(/NULL, (x ge self.minX) and (x le self.maxX) $
               and (y ge self.minY) and (y le self.maxY))
  if ~isa(i) then return  
  _x = x[i]
  _y = y[i]
  self.hubMathInc::addData, _x, _y
  
  
  ;calculate histogram and update
  
  hist = HIST_2D(_x, _y $
                  , bin1 = self.binSizeX, bin2 = self.binSizeY $
                  , min1= self.minX, max1=self.maxX $
                  , min2= self.minY, max2=self.maxY)
  
  ;if ~product(size(*self.histogram2D, /DIMENSIONS ) eq size(hist, /DIMENSIONS), /INTEGER) then begin
  ;  stop
  ;endif
  
  *self.histogram2D += hist[0:self.numberOfBinsX-1, 0:self.numberOfBinsY-1]

end


;+
; :Description:
;    Returns the 1D Histogram for all X values.
;-
function hubMathIncHistogram2D::getHistogramY
  return, self._getSingleHistogram('Y')
end

;+
; :Description:
;    Returns the 1D Histogram for all X values.
;-
function hubMathIncHistogram2D::getHistogramX
  return, self._getSingleHistogram('X')
end

;+
; :Description:
;    Returns the 1D Histogram for all X values.
;-
function hubMathIncHistogram2D::_getSingleHistogram, dimension
  if total(strcmp(dimension, ['X','Y'])) ne 1 then message,'Dimension must be X or Y'
  results2D = self.getResult()
  
  result = hash()
  toCopy = ['binStart','binEnd','binCenter','binSize','numberOfBins','range']
  foreach key, toCopy do begin
    result[key] = results2D[key+dimension]
  endforeach
  result['numberOfSamples'] = self.getNumberOfSamples()
  result['hasSamples'] = results2D['numberOfSamples'] gt 0
  
  case dimension of
     'X' : histogram = total(*self.histogram2D,2)
     'Y' : histogram = total(*self.histogram2D,1)
  endcase
  
  cumulativeHistogram = total(histogram, /PRESERVE_TYPE, /CUMULATIVE)
  probabilityDensity = 1d*histogram/cumulativeHistogram[-1]
  cumulativeDistribution = 1d*cumulativeHistogram/cumulativeHistogram[-1]
  
  result['histogram'] = histogram
  result['cumulativeHistogram'] = cumulativeHistogram
  result['probabilityDensity'] = probabilityDensity
  result['cumulativeDistribution'] = cumulativeDistribution
  
  return, result
  
end



;+
; :Description:
;    Returns a hash containing the 2D histogram and related descriptions.
;    Use the following keys to access the hash::
;    
;      nBX = number of bins for X data values
;      nbY = number of bins for Y data values
;      
;      key              | type              | description
;      -----------------+-------------------+--------------
;      binStartX        | double[nBX]       | vector with X bin minimum values (left boundaries) 
;      binStartY        | double[nBY]       | vector with Y bin minimum values (left boundaries)
;      binEndX          | double[nBX]       | vector with X bin maximum values (right boundaries)
;      binEndY          | double[nBY]       | vector with Y bin maximum values (right boundaries)
;      binCenterX       | double[nBX]       | vector with X bin center values
;      binCenterY       | double[nBY]       | vector with Y bin center values
;      binSizeX         | double            | size of single X bin 
;      binSizeY         | double            | size of single Y bin 
;      numberOfBinsX    | long              | number of X bins
;      numberOfBinsY    | long              | number of Y bins
;      rangeX           | double            | data range X values
;      rangeY           | double            | data range Y values
;      numberOfSamples  | long64            | total number of samples
;      hasSamples       | bool              | true if numberOfSamples > 0   
;      histogram2D      | long64[nBX][nBY]  | 2D array with counts for each X-Y bin position
;      -----------------+-------------------+-----------------
;-
function hubMathIncHistogram2D::getResult
  results = Hash()
  
  results['binStartX'] = indgen(self.numberOfBinsX) * self.binSizeX + self.minX
  results['binStartY'] = indgen(self.numberOfBinsY) * self.binSizeY + self.minY
  results['binEndX'] = results['binStartX'] + self.binSizeX 
  results['binEndY'] = results['binStartY'] + self.binSizeY
  results['binCenterX'] = (results['binStartX'] + results['binEndX']) / 2 
  results['binCenterY'] = (results['binStartY'] + results['binEndY']) / 2
  results['binSizeX'] = self.binSizeX
  results['binSizeY'] = self.binSizeY
  results['numberOfBinsX'] = self.numberOfBinsX
  results['numberOfBinsY'] = self.numberOfBinsY
  results['rangeX'] = [self.minX, self.maxX] 
  results['rangeY'] = [self.minY, self.maxY]
    
  results['numberOfSamples'] = self.hubMathInc::getNumberOfSamples()
  results['hasSamples'] = results['numberOfSamples'] gt 0  
  results['histogram2D'] = *self.histogram2D
  
  
  return, results

end

;+
; :hidden:
;-
pro hubMathIncHistogram2D__define
  
  struct = {hubMathIncHistogram2D $
    , inherits hubMathIncHistogram $
    , minX : 0d $
    , minY : 0d $
    , maxX : 0d $ 
    , maxY : 0d $ 
    , histogram2D : ptr_new() $
    , numberOfBinsX : 0u $
    , numberOfBinsY : 0u $
    , binSizeX : 0d $
    , binSizeY : 0d $
  }
end

;+
; :hidden:
;-
pro TEST_HUBMATHINCHISTOGRAM2D
  
      ;initialize the hubMathIncHistogram2D object
      rangeX = [0,1] ;range X values
      rangeY = [0,100] ;range Y values
      nBinsX  = 10 ;number of bins
      nBinsY  = 50 
      incHist = hubMathIncHistogram2D(rangeX, rangeY, nBinsX, nBinsY)
      
      ;this loop could be an image tile reading loop
;      seed = systime(1)
;      for loop = 1, 20 do begin
;        dataX = randomn(seed, 100)
;        dataY = randomn(seed, 100)*100
;        ;add the data values incrementally
;        incHist.addData, dataX, dataY
;      endfor
      
      print, incHist.getHistogramX()
      print, incHist.getHistogramY()
      hist = incHist.getResult()
      print, hist  
      print, hist['histogram2D']

end