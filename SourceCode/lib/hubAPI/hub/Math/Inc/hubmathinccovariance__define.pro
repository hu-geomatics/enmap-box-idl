;+
; A hubMathIncCovariance object can be used to calculate the co-variance between two data sets 
; X and Y incrementally.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Constructor.
;
; :Params:
;    meanX: in, required, type = numeric
;     Mean value of X data values.
;     
;    meanY: in, required, type = numeric
;     Mean value of Y data values.
;
;
;
;-
function hubMathIncCovariance::init $
  , meanX, meanY

  self.meanX = meanX
  self.meanY = meanY
  return, 1b

end

;+
; :Description:
;    Adds data values to calculate the co-variance from. 
;    Repeat this routine to add more values.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;    y: in, required, type = numeric
;     Scalar / Vector of data values. Must have the same length as `x`.
;-
pro hubMathIncCovariance::addData $
  , x, y

  if ~isa(x) then return
  
  self.hubMathInc::addData, x, y
  self.sum += total((x-self.meanX)*(y-self.meanY), /DOUBLE)

end

;+
; :Description:
;    Returns the co-variance between X and Y data values.
;-
function hubMathIncCovariance::getResult
  if self.numberOfSamples eq 0 then begin
    result = !VALUES.D_NAN
  endif else begin
    result = self.sum / self.numberOfSamples
  endelse
  return, result

end

;+
; :hidden:
;-
pro hubMathIncCovariance__define
  
  struct = {hubMathIncCovariance $
    , inherits hubMathInc $
    , meanX : 0d $
    , meanY : 0d $
    , sum : 0d $
  }
end

pro test_hubMathIncCovariance
  X = [65, 63, 67, 64, 68, 62, 70, 66, 68, 67, 69, 71]
  Y = [68, 66, 68, 65, 69, 66, 68, 65, 71, 67, 68, 70]
  data = TRANSPOSE([[X],[Y]])
  PRINT, CORRELATE(data, /COVARIANCE)
  
  
  incCovariance = hubMathIncCovariance(mean(x), mean(y))
  incCovariance.addData, x, y
  print, incCovariance.getResult()
  

  
end

