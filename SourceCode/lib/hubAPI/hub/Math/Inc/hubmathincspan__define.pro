;+
; 
; A hubMathIncSpan object can be used to calculate the span between the minimum and maximum values of 
; incrementally added data values.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; 
;-

;+
; :Description:
;    Returns the span between the minimum and the maximum data value.
;-
function hubMathIncSpan::getResult
  range = self.hubMathIncRange::getResult()
  return, range[1]-range[0]
end

;+
; :hidden:
;-
pro hubMathIncSpan__define
  
  struct = {hubMathIncSpan $
    , inherits hubMathIncRange}
end

pro test_hubMathIncSpan

  incSpan = hubMathIncSpan()
  print, incSpan.getResult()
  
end