;+
;
; A hubMathIncMean object can be used to calculate the mean of different data values incrementally.
;
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Adds data values to calculate the mean from. 
;    Repeat this routine to add further values.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;-
pro hubMathIncMean::addData $
  , x
  self.hubMathIncSum::addData, x
end

;+
; :Description:
;    Returns the mean of all added data values.
;-
function hubMathIncMean::getResult
  if self.numberOfSamples eq 0 then begin
    result = !VALUES.D_NAN
  endif else begin
    result = self.hubMathIncSum::getResult() / self.numberOfSamples
  endelse
  return, result
end

;+
; :hidden:
;-
pro hubMathIncMean__define
  
  struct = {hubMathIncMean $
    , inherits hubMathIncSum $
  }
end

;+
; :hidden:
;-
pro test_hubMathIncMean

  incMean = hubMathIncMean()
  incMean.addData, [1,1,1]
  incMean.addData, [3,3,3]
  print, incMean.getResult()
end
