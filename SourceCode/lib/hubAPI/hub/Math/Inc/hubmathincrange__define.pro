;+
; A hubMathIncRange object can be used to calculate the range of incrementally added data values.
; 
; 
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :examples:
;   This example shows how to add data values and retrieve the total range of it::
;     
;     incRange = hubMathIncRange()
;     
;     ;add data values incrementally / step by step
;     incRange.addData, [0,1,2,30] 
;     incRange.addData, [-2,5,2,1,4]
;     incRange.addData, [-3,9.4]
;     
;     ;print the total range
;     print, incRange.getResult()
; 
; 
;-

;+
; :hidden:
;-
function hubMathIncRange::init

  self.min = +!VALUES.d_infinity
  self.max = -!VALUES.d_infinity
  return, 1b

end

;+
; :Description:
;    Adds data values to calculate the range from.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;-
pro hubMathIncRange::addData $
  , x

  self.hubMathInc::addData, x

  if ~isa(x) then return

  minX = min(x, MAX=maxX)
  self.min <= minX 
  self.max >= maxX

end

;+
; :Description:
;    Returns the range as vector `[min, max]`.
;-
function hubMathIncRange::getResult

  result = [self.min, self.max]
  if ~finite(result[0]) or ~finite(result[1]) then begin
    result = [!VALUES.D_NAN, !VALUES.D_NAN]
  endif else begin
    result = [self.min, self.max]
  endelse
  return, result

end

;+
; :hidden:
;-
pro hubMathIncRange__define
  
  struct = {hubMathIncRange $
    , inherits hubMathInc $
    , min : 0d $
    , max : 0d $
  }
end

pro test_hubMathIncRange
  
  incRange = hubMathIncRange()
  ;add data values incrementally / step by step
;  incRange.addData, [0,1,2,30]
;  incRange.addData, [-3,9.4]
  print, incRange.getResult()

end