;+
; Use the hubMathInc class to describe new objects for incremental calculations.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This routine adds data values to calculate a value from.    
;
; :Params:
;    x: in, required, type=numeric[]
;     Use this argument to add a scalar or vector of data values that are to be evaluated.
;     
;    y: in, optional,  type=numeric[]
;     Use this argument to add a second scalar of vector of data values.
;     The number of values must be the same as in `x`.
;     
; :Keywords:
;    NumberOfSamples:
;     Returns the number of samples contained in `x`.
;
;-
pro hubMathInc::addData $
  , x, y $
  , NumberOfSamples=numberOfSamples
  
  if n_params() eq 2 then begin
    if n_elements(x) ne n_elements(y) then begin
      message, 'Number of elements in x and y must be the same.'
    endif  
  endif

  numberOfSamples = n_elements(x)
  self.numberOfSamples += numberOfSamples

end



;+
; :Description:
;    Returns the number of data samples added to this objects.
;-
function hubMathInc::getNumberOfSamples
  
  return, self.numberOfSamples

end

;
pro hubMathInc__define
  
  struct = {hubMathInc $
    , inherits IDL_Object $
    , numberOfSamples : 0ll $
  }
end

;+
; :hidden:
;-
pro test_hubMathInc
  
  ; test data 
  
  seed=1
  x1 = randomu(seed, 100)
  x2 = randomu(seed, 100)
  x3 = randomu(seed, 100)
  
  ; incremental object
  
  incObj = hubMathInc()
  incObj.addData, x1
  incObj.addData, x2
  incObj.addData, x3
  print,incObj.getNumberOfSamples()
  
end