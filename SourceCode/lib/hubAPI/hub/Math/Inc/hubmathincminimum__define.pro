;+
; A hubMathIncMinimum object can be used to calculate the minimum of different 
; data values incrementally.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-


;+
; :Description:
;    Returns the minimum of all added data values.
;-
function hubMathIncMinimum::getResult
  range = self.hubMathIncRange::getResult()
  if isa(range) then begin
    result = range[0]
  endif else begin
    result = !null
  endelse
  return, result
end

;+
; :hidden:
;-
pro hubMathIncMinimum__define
  
  struct = {hubMathIncMinimum $
    , inherits hubMathIncRange}
end

;+
; :hidden:
;-
pro test_hubMathIncMinimum
  m = hubMathIncMinimum()
  print, m.getResult()
end