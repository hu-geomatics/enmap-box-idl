;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
; :hidden:
;-

pro test_hubMathIncPerformance

  ; test data 
  
  ref = list([1,2],[],[3,4],[5])
  est = list([1,2],[],[3,4],[5])
  numberOfTiles = n_elements(ref)
  numberOfClasses = 5

  ; incremental performance

  incClassPerf = hubMathIncClassificationPerformance(numberOfClasses)
  incRegrPerf = hubMathIncRegressionPerformance(mean(3), mean(3))
  for i=0,numberOfTiles-1 do begin
    incClassPerf.addData, ref[i], est[i]
    incRegrPerf.addData,  ref[i], est[i]
  endfor
  
  print, 'Classification Performance:'
  print, (incClassPerf.getResult())
  print
  
  print, 'Regression Performance:'
  print, (incRegrPerf.getResult())
  print
  
  
end

pro test_hubMathIncPerformance_AdjustClassProbs
  
  ; siehe daten unter Z:\zusammenarbeit\Tobias_Kuemmerle\adj-error-mtx_andreas-beispiel.xls
  
  numberOfClasses = 10
  confusionMatrix = float([ $
    58, 0, 0, 0, 12, 2, 2, 0, 0, 12 $
   , 0, 99, 0, 0, 0, 0, 0, 0, 0, 0 $
   , 0, 0, 100, 0, 1, 0, 0, 3, 0, 0 $
   , 0, 0, 0, 96, 1, 0, 0, 0, 0, 0 $
   , 7, 0, 0, 0, 9, 6, 6, 0, 10, 10 $
   , 2, 0, 0, 2, 17, 73, 17, 0, 4, 10 $
   , 7, 0, 0, 2, 31, 18, 60, 0, 21, 7 $
   , 0, 0, 0, 0, 2, 0, 0, 97, 6, 0 $
   , 7, 0, 0, 0, 23, 1, 12, 0, 59, 13 $
   , 19, 1, 0, 0, 4, 0, 3, 0, 0, 48 $
  ])
  
  mapProbs = [0.037769,0.060845,0.108264,0.334532,0.108187,0.025334,0.02887,0.217222,0.051906,0.027071]
  
  incClassPerf = hubMathIncClassificationPerformance(numberOfClasses, confusionMatrix)
  print,incClassPerf.getResult(mapProbs, /Adjust)

end