;+
; A hubMathIncCorrelation object can be used to calculate the pearson correlation
; between two data sets X and Y incrementally.
; 
; The pearson corellation is defined as::
;     
;                 covariance(X,Y)
;   r = ------------------------------------
;       square root(variance(X)*variance(Y))
;    
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Constructor. Requries to know the mean of X and Y.
;
; :Params:
;    meanX: in, required, type = numeric
;     Mean of all data values in data set X.
;    meanY: in, required, type = numeric
;     Mean of all data values in data set Y.
;
;
;-
function hubMathIncCorrelation::init $
  , meanX, meanY

  self.incCovariance = hubMathIncCovariance(meanX, meanY)
  self.incVarianceX = hubMathIncVariance(meanX)
  self.incVarianceY = hubMathIncVariance(meanY)
  return, 1b

end

;+
; :Description:
;    Adds data values to calculate the pearson correlation from. 
;    Repeat this routine to add more values.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;    y: in, required, type = numeric
;     Scalar / Vector of data values. Must have same length as `x`.
;-
pro hubMathIncCorrelation::addData $
  , x, y
  self.hubMathInc::addData, x, y
  self.incCovariance.addData, x, y
  self.incVarianceX.addData, x
  self.incVarianceY.addData, y

end

;+
; :Description:
;    Returns the number of samples added to this object.
;-
function hubMathIncCorrelation::getNumberOfSamples
  
  ;use the number of samples of the covariance object
  return, self.incCovariance.getNumberOfSamples()

end


;+
; :Description:
;    Returns the pearson corellation coefficient between the added data values in X and Y.
;-
function hubMathIncCorrelation::getResult
  if self.numberOfSamples eq 0 then begin
    result = !VALUES.D_NAN
  endif else begin
    hubMath_mathErrorsOff
    result = self.incCovariance.getResult() / sqrt(self.incVarianceX.getResult() * self.incVarianceY.getResult())
    hubMath_mathErrorsOn
    if ~finite(result) then begin
      result=1d
    endif
  endelse
  return, result

end

;+
; :Description:
;    Returns the variance of data set X.
;-
function hubMathIncCorrelation::getVarianceX
  return, self.incVarianceX.getResult()
end


;+
; :Description:
;    Returns the variance of data set Y.
;-
function hubMathIncCorrelation::getVarianceY
  return, self.incVarianceY.getResult()
end
;+
; :hidden:
;-
pro hubMathIncCorrelation__define
  
  struct = {hubMathIncCorrelation $
    , inherits hubMathInc $
    , incCovariance : obj_new() $
    , incVarianceX  : obj_new() $
    , incVarianceY  : obj_new() $
  }
end


;+
; :hidden:
;-
pro test_hubMathIncCorrelation
    path = hub_getTestImage('Hymap_Berlin-A_Image')
    img = hubIOImgInputImage(path)
    dataIgnoreValue = img.getMeta('data ignore value')
    
    bandX = 5   ;index band X
    bandY = 25  ;index band Y
    tileLines = 10 ;number of image lines to read in one loop 
    
    ;create objects for calculating the band mean values 
    incMeanX = hubMathIncMean()
    incMeanY = hubMathIncMean()
    
    ;read image bands 
    img.initReader, tileLines, /TileProcessing, /Slice, SubsetBandPositions = [bandX, bandY]
    while ~img.tileProcessingDone() do begin
      data = img.getData() ;get you tile data
      
      ;exclude data values equal to the data ignore value 
      validPixels = make_array(n_elements(data[0,*]), /Byte, value = 1b)
      if isa(dataIgnoreValue) then begin
        validPixels and= data[0,*] ne dataIgnoreValue
        validPixels and= data[1,*] ne dataIgnoreValue
      endif 
      iValid = where(validPixels ne 0, /NULL)
      if ~isa(iValid) then stop
      incMeanX.addData, data[0, iValid]
      incMeanY.addData, data[1, iValid]
    endwhile
    img.finishReader
    
    meanX = incMeanX.getResult()
    meanY = incMeanY.getResult()
    print, 'Mean X:', meanX, ' Mean Y:', meanY
    
    ;create incremental correlation object
    incCorrelation = hubMathIncCorrelation(meanX, meanY)
    
    ;re-initialize the image reader & read all image band values again
    img.initReader, tileLines, /TileProcessing, /Slice, SubsetBandPositions = [bandX, bandY]
    
    while ~img.tileProcessingDone() do begin
      data = img.getData() ;get you tile data
      
      ;exclude data values equal to the data ignore value 
      validPixels = make_array(n_elements(data[0,*]), /Byte, value = 1b)
      if isa(dataIgnoreValue) then begin
        validPixels and= data[0,*] ne dataIgnoreValue
        validPixels and= data[1,*] ne dataIgnoreValue
      endif 
      iValid = where(validPixels ne 0, /NULL)
      nValid += n_elements(iValid)
      if ~isa(iValid) then stop
      ;add valid data pairs of X and Y 
      incCorrelation.addData, data[0, iValid], data[1, iValid]  
    endwhile
    img.cleanup
    
    ;print the correlation coefficient + number of samples
    print, 'Pearson Correlation: ', incCorrelation.getResult() $
         , '#Samples: ', incCorrelation.getNumberOfSamples()
    
end
