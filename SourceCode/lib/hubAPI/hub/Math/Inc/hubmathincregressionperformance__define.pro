;+
;
; A hubMathIncRegressionPerformance object can be used to calculate the 
; performance of a regression estimator incrementally.   
;
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Examples:
;    This example shows how to use the hubMathIncRegressionPerformance object.
;    The for-loop represents an incremental adding of data values::
;    
;      referenceData = [1, 2,3,4,5,6]
;      estimatedData = [2, 2,3,4,5,7]
;    
;      incRegressionPerformance = hubMathIncRegressionPerformance(mean(ref), mean(est))
;      
;      ;incremental regression performance
;      for i=0, n_elements(referenceData)-1 do begin
;        ;add estimated & reference values incrementally
;        incRegressionPerformance.addData, referenceData[i], estimatedData[i]
;      endfor
;      
;      print, incRegressionPerformance.getResult()
;-

;+
; :Description:
;    Constructor to create a new `hubMathIncRegressionPerformance` object.
;
; :Params:
;    meanReference: in, required, type = numeric
;     Mean value of reference data values.
;     
;    meanEstimation: in, required, type = numeric
;     Mean value of estimated data values.
;
;-
function hubMathIncRegressionPerformance::init $
  , meanReference, meanEstimation

  self.incMeanSquaredError = hubMathIncMean()
  self.incMeanAbsoluteError = hubMathIncMean()
  self.incCorrelation = hubMathIncCorrelation(meanReference, meanEstimation)
  return, 1b

end

;+
; :Description:
;    Adds data values to calculate the regression performance from.
;
; :Params:
;    reference: in, required, type = numeric
;     Scalar / Vector with reference data values.
;     
;    estimation: in, required, type = numeric
;     Scalar / Vector with estimated data values. Must have the same length as `reference`. 
;-
pro hubMathIncRegressionPerformance::addData $
  , reference, estimation

  self.hubMathInc::addData, reference, estimation
  absoluteResiduals = abs(reference-estimation)
  self.incMeanSquaredError.addData, absoluteResiduals^2
  self.incMeanAbsoluteError.addData, absoluteResiduals
  self.incCorrelation.addData, reference, estimation
end

;+
; :Description:
;    Returns a hash containing the following regression performance measures::
;
;     key                       | type    | description
;     --------------------------+---------+----------
;     meanSquaredError          | double  | mean squared error (MSE)
;     rootMeanSquaredError      | double  | root mean squared error (RMSE)
;     meanAbsoluteError         | double  | mean absolute error (MAE)
;     pearsonCorrelation        | double  | person correlation  (r)
;     squaredPearsonCorrelation | double  | squared pearson correlation (r^2) 
;     nashSutcliffeEfficiency   | double  | Nash-Sutcliffe model efficiency coefficient (NSE) 
;     numberOfSamples           | long64  | number of added estimaten/validation values
;     --------------------------+---------+-----------------------------
;
;
;-
function hubMathIncRegressionPerformance::getResult
  
  result = hash()
  
  result['meanSquaredError'] = self.incMeanSquaredError.getResult()
  result['rootMeanSquaredError'] = sqrt(result['meanSquaredError'])
  result['meanAbsoluteError'] = self.incMeanAbsoluteError.getResult()
  result['pearsonCorrelation'] = self.incCorrelation.getResult()
  result['squaredPearsonCorrelation'] = result['pearsonCorrelation']^2
  result['numberOfSamples'] = self.incMeanSquaredError.getNumberOfSamples()
     
  result['nashSutcliffeEfficiency'] = 1. -  self.incMeanSquaredError.getResult() $
                                          / self.incCorrelation.getVarianceX()
  return, result
  
end

;+
; :hidden:
;-
pro hubMathIncRegressionPerformance__define
  
  struct = {hubMathIncRegressionPerformance $
    , inherits hubMathInc $
    , incMeanSquaredError  : obj_new() $
    , incMeanAbsoluteError : obj_new() $
    , incCorrelation       : obj_new() $
  }
end

;+
; :hidden:
;-
pro test_hubMathIncRegressionPerformance
  
  ; test data 
  referenceData = [1., 2,3,4,5,6]
  estimatedData = [2., 2,3,4,5,7]


  incRegressionPerformance = hubMathIncRegressionPerformance(mean(referenceData), mean(estimatedData))
  
  ;incremental regression performance
  for i=0, n_elements(referenceData)-1 do begin
    incRegressionPerformance.addData, referenceData[i], estimatedData[i]
  endfor
  
  print, incRegressionPerformance.getResult()
  
  print, 1. - (total((referenceData - estimatedData)^2) $
             / total((referenceData - mean(referenceData))^2)) 
end