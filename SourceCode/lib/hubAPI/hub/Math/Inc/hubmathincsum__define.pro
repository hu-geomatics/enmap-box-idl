;+
; A hubMathIncSum object can be used to calculate a sum of incrementally added data values. 
; 
; :Examples:
;   This example shows how to calculate the sum of all non-masked image pixel values::
;     
;      path = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
;      img = hubIOImgInputImage(path)
;      dataIgnoreValue = img.getMeta('data ignore value')
;      
;      incSum = hubMathIncSum()
;      
;      img.initReader, 10, /TILEPROCESSING, /Slice
;      while ~img.tileProcessingDone() do begin
;        data = img.getData()
;        incSum.addData, data[where(data ne dataIgnoreValue, /NULL)]
;      endwhile
;      img.cleanup
;      
;      ;print the calculated sum
;      print, 'Sum:', incSum.getResult()
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; 
; 
; 
;-

;+
; :Description:
;    Adds data values to calculate the sum from.
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;-
pro hubMathIncSum::addData $
  , x

  self.hubMathInc::addData, x
  if ~isa(x) then return 
  self.sum += total(x, /Double)

end

;+
; :Description:
;    Returns the sum of all values added with `hubMathIncSum::addData`.
;-
function hubMathIncSum::getResult
  
  if self.numberOfSamples eq 0 then begin
    result = !VALUES.D_NAN
  endif else begin
    result = self.sum
  endelse
  return, result

end

;+
; :hidden:
;-
pro hubMathIncSum__define
  
  struct = {hubMathIncSum $
    , inherits hubMathInc $
    , sum : 0d $
  }
end

pro test_hubMathIncSum
  incSum = hubMathIncSum()
  print, incSum.getResult()
end