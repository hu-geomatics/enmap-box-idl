 ;+
; A hubMathIncClassificationPerformance object can be used to calculate the 
; performance of an classifier by comparing it's estimates with valid reference data.
;
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Constructor of `hubMathIncClassificationPerformance` object.
;
; :Params:
;    numberOfClasses: in, required, type = int
;     The maximum number of classes. Must be greater than 0.
;     
;    initialConfusionMatrix: in, optional, type = numeric[nC][nC], hidden
;     Use this to provide an initial confusion matrix used to add further classification results.
;     Must have nC = number of classes. This means
;     
;-
function hubMathIncClassificationPerformance::init, $
  numberOfClasses, initialConfusionMatrix, initialAdjustedConfusionMatrix, $
  AdjustSamplingBias=adjustSamplingBias, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions, StrataNames=strataNames, $
  AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions=mapClassProportions,$
  ClassNames=classNames, ClassColors=classColors
  
  self.numberOfClasses = numberOfClasses
  self.confusionMatrix = ptr_new(make_array(numberOfClasses, numberOfClasses, /Double ))
  self.adjustedConfusionMatrix = ptr_new(make_array(numberOfClasses, numberOfClasses, /Double ))

  if isa(initialConfusionMatrix) then begin
    (*self.confusionMatrix)[*] = initialConfusionMatrix[*]
    (*self.adjustedConfusionMatrix)[*] = initialConfusionMatrix[*] 
    self.numberOfSamples = total(initialConfusionMatrix)
  endif
  
  self.adjustSamplingBias = keyword_set(adjustSamplingBias)
  if self.adjustSamplingBias then begin
    self.strataProportions = ptr_new(double(strataProportions) / total(strataProportions,/Double)) ;normalized to 1
    self.strataSampleProportions = ptr_new(double(strataSampleProportions) / total(strataSampleProportions,/Double) )
    self.strataNames = ptr_new(isa(strataNames) ? strataNames  : 'class '+strtrim(indgen(n_elements(strataProportions))+1,2))
    if isa(initialConfusionMatrix) then begin
      (*self.adjustedConfusionMatrix)[*] = initialAdjustedConfusionMatrix[*]      
    endif
  endif else begin
    self.strataProportions = ptr_new([1d])
    self.strataSampleProportions = ptr_new([1d])
  endelse
  self.numberOfStrata = n_elements(*self.strataProportions)
  
  self.adjustAreaEstimation = keyword_set(adjustAreaEstimation)
  if self.adjustAreaEstimation then begin
    self.mapClassProportions = ptr_new(double(mapClassProportions))
  endif
  
  self.strataWeights = ptr_new(*self.strataProportions / *self.strataSampleProportions)
  self.classColors = ptr_new(isa(classColors)? classColors : randomu(SEED , 3, numberOfClasses, /LONG) mod 256)
  self.classNames  = ptr_new(isa(classNames) ? classNames  : 'class '+strtrim(indgen(numberOfClasses)+1,2))
  return, 1b
end

;+
; :Description:
;    Adds classification data to the `hubMathIncClassificationPerformance` object.
;
; :Params:
;    reference: in, required, type = numeric
;     Reference data values. Must be in range [0, number of samples].
;    estimation: in, required, type = numeric
;     Estimated data values. Must be in range [0, number of samples].
;
;
;
;-
pro hubMathIncClassificationPerformance::addData $
  , reference, estimation, strataLabel
  
  self.hubMathInc::addData, reference, estimation, NumberOfSamples=numberOfSamples

  if numberOfSamples eq 0 then begin
    return
  endif
  
  ; check for input
  
  if isa(where(/NULL, (estimation lt 1) or (estimation gt self.numberOfClasses))) then begin
    message, string(format='(%"Estimation data outside valid range: [1,%i].")', self.numberOfClasses)
  endif
  
  if isa(where(/NULL, (reference lt 1) or (reference gt self.numberOfClasses))) then begin
    message, string(format='(%"Reference data outside valid range [1,%i].")', self.numberOfClasses)
  endif

  if self.adjustSamplingBias then begin
    if isa(where(/NULL, (strataLabel lt 1) or (strataLabel gt self.numberOfStrata))) then begin
      message, string(format='(%"Strata data outside valid range: [1,%i]")', self.numberOfStrata)
    endif
  endif

  
  if self.adjustSamplingBias then begin
    if n_elements(strataLabel) ne numberOfSamples then begin
      message, 'Size of strataLabel must match size of reference and estimation.'
    endif
  endif else begin
    strataLabel = replicate(1, numberOfSamples)
  endelse
  
  

  ; calculate confusion matrix (sampling bias is removed by using the strata weights, if provided)
  confusionMatrix = make_array(self.numberOfClasses, self.numberOfClasses, /Double)
  adjustedConfusionMatrix = make_array(self.numberOfClasses, self.numberOfClasses, /Double)
  
  for i=1,self.numberOfClasses do begin
    for j=1,self.numberOfClasses do begin
      matchMask = ((reference eq i) * (estimation eq j))
      confusionMatrix[i-1,j-1] = total(matchMask, /DOUBLE)
      adjustedConfusionMatrix[i-1,j-1] = total(matchMask* (*self.strataWeights)[strataLabel-1], /DOUBLE)
    endfor
  endfor
  *self.confusionMatrix += confusionMatrix
  *self.adjustedConfusionMatrix += adjustedConfusionMatrix
end


;+
; :Description:
;    Returns the classification performance as hash containing the following values
;    (nC = number of classes) ::
;    
;     key                      | type           | description
;     -------------------------+----------------+--------------------
;     confusionMatrix          | long64[nC][nC] | confusion matrix
;     sumReference             | double[nC]     | sumReference
;     sumEstimation            | double[nC]     | sumEstimation
;     overallAccuracy          | double         | overallAccuracy
;     userAccuracy             | double[nC]     | userAccuracy
;     producerAccuracy         | double[nC]     | producerAccuracy
;     f1Accuracy               | double[nC]     | f1Accuracy
;     averageUserAccuracy      | double         | averageUserAccuracy
;     averageProducerAccuracy  | double         | averageProducerAccuracy
;     averageF1Accuracy        | double         | averageF1Accuracy
;     kappaAccuracy            | double         | kappa value
;     numberOfSamples          | long64         | total number of samples  
;     numberOfClasses          | int            | number of classes
;     errorOfCommission        | double[nC]     | error of commission
;     errorOfOmission          | double[nC]     | error of omission
;     -------------------------+----------------+--------------------
;     
;-
function hubMathIncClassificationPerformance::getResult, TotalArea=totalArea

  numberOfClasses = self.numberOfClasses
  
  alphas = reverse(double([0.1, 0.05, 0.01])) ;alphas for confidence intervals
  
  ; mij is the confusion matrix that is adjusted for sampling bias if strata information was provided,
  ; otherwise it is assumed that the reference sample has no bias (which is true for e.g. simple random or proportional stratified)
  mij = *self.adjustedConfusionMatrix
  mi_ = total(mij, 1, /DOUBLE) ; class-wise sum over estimations
  m_j = total(mij, 2, /DOUBLE) ; class-wise sum over references 
  mii = mij[indgen(numberOfClasses),indgen(numberOfClasses)]
  m = total(mij, /DOUBLE)

  ; mapped class proportions Wi are provided or are substituted by the map estimates corresponding to the reference sample 
  ; (this is a suitable estimate, because the sampling bias is already removed)
  ; *** 
  ;     Should we use Wi at all for the calculation of pij? mij is already unbiased!
  ;     It is comparable to the case when using the Olofsson 2013 formular with an unbiased reference sample, correct? 
  ;     We could simply always use pij = mij/numberOfSamples.
  ; ***   
  if self.adjustAreaEstimation then begin
    Wi = *self.mapClassProportions
  endif else begin
    Wi = mi_/m ; note that in this case pij is reduced to pij=mij/m
  endelse
  
  ; pij is the proportion of area that is adjusted by using the mapped class proportions Wi
  ; pij = Wi*mij/mi_
  if self.adjustAreaEstimation then begin
    pij = make_array(numberOfClasses,numberOfClasses)
    for i=0,numberOfClasses-1 do for j=0,numberOfClasses-1 do begin
      pij[i,j] = Wi[i]*mij[i,j]/mi_[i] 
    endfor
  endif else begin
    pij = mij/m
  endelse
  
  pi_ = total(pij, 1, /DOUBLE)
  p_j = total(pij, 2, /DOUBLE)
  pii = pij[indgen(numberOfClasses),indgen(numberOfClasses)]

  ; estimate performance measures and variances
  hubMath_mathErrorsOff, oldState
  
  overallAccuracy = total(mii, /DOUBLE)/m

  kappaAccuracy = (m*total(mii) - total(mi_*m_j)) / (m^2 - total(mi_*m_j))
  kappaAccuracy = finite(kappaAccuracy) ? kappaAccuracy : 0d
  
  userAccuracy = mii/mi_
  userAccuracy[where(/NULL, ~finite(userAccuracy))] = 0d
  averageUserAccuracy = mean(userAccuracy, /DOUBLE)
  
  producerAccuracy = mii/m_j
  producerAccuracy[where(/NULL, ~finite(producerAccuracy))] = 0d
  averageProducerAccuracy = mean(producerAccuracy, /DOUBLE)
  
  f1Accuracy = 2d*userAccuracy*producerAccuracy/(userAccuracy+producerAccuracy)
  f1Accuracy[where(/NULL, ~finite(f1Accuracy))] = 0d
  averageF1Accuracy = mean(f1Accuracy)

  conditionalKappaAccuracy = (m*mii-mi_*m_j)/(m*mi_-mi_*m_j)
  conditionalKappaAccuracy[where(/NULL, ~finite(conditionalKappaAccuracy))] = 0d

  overallAccuracySquaredStandardError = 0d
  for i=0,numberOfClasses-1 do begin
    overallAccuracySquaredStandardError += pij[i,i]*(Wi[i]-pij[i,i])/(Wi[i]*m)
  endfor
  
  a1 = total(mii, /DOUBLE)/m
  a2 = total(mi_*m_j, /DOUBLE)/m^2
  a3 = total(mii*(mi_+m_j), /DOUBLE)/m^2
  a4 = 0d
  for i=0,numberOfClasses-1 do for j=0,numberOfClasses-1 do a4 += mij[i,j]*(mi_[j]+m_j[i])^2
  a4 /= m^3
  b1 = a1*(1-a1)/(1-a2)^2
  b2 = 2*(1-a1)*(2*a1*a2-a3)/(1-a2)^3
  b3 = (1-a1)^2*(a4-4*a2^2)/(1-a2)^4
  kappaAccuracySquaredStandardError = (b1+b2+b3)/m
  
  producerAccuracySquaredStandardError = make_array(numberOfClasses, /DOUBLE)
  for i=0,numberOfClasses-1 do begin
    sum = 0d
    for j=0,numberOfClasses-1 do begin
      if i eq j then continue
      sum += pij[i,j]*(Wi[j]-pij[i,j])/(Wi[j]*m)
    endfor
    producerAccuracySquaredStandardError[i] = pij[i,i]*p_j[i]^(-4) * (pij[i,i]*sum + (Wi[i]-pij[i,i])*(p_j[i]-pij[i,i])^2/(Wi[i]*m))
  endfor

  userAccuracySquaredStandardError = make_array(numberOfClasses, /DOUBLE)
  for i=0,numberOfClasses-1 do begin
    userAccuracySquaredStandardError[i] = pij[i ,i]*(Wi[i]-pij[i,i])/(Wi[i]^2*m)
  endfor

  conditionalKappaAccuracySquaredStandardError = m*(mi_-mii) / (mi_*(m-m_j))^3 * ( (mi_-mii)*(mi_*m_j-m*mii)+m*mii*(m-mi_-m_j+mii) )

  ; estimate mapped class proportions/areas and standard errors
  classProportion = m_j/m
  classProportionSquaredStandardError = dblarr(self.numberOfClasses)
  for j=0,self.numberOfClasses-1 do begin
    for i=0,self.numberOfClasses-1 do begin
      classProportionSquaredStandardError[j] += Wi[i]^2 * ( (mij[i,j]/mi_[i])*(1-mij[i,j]/mi_[i]) )/(mi_[i]-1)
    endfor 
  endfor
  
  ;avoid negative standard errors related to floating point errors
  overallAccuracySquaredStandardError >= 0
  producerAccuracySquaredStandardError >= 0 
  useraccuracysquaredstandarderror >= 0
  conditionalKappaAccuracySquaredStandardError >= 0
  classProportionSquaredStandardError >= 0
  
  hubMath_mathErrorsOn, oldState

  ; create result hash
  result = Dictionary()
  nij = *self.confusionMatrix
  ni_ = total(nij, 1, /DOUBLE) ; class-wise sum over estimations
  n_j = total(nij, 2, /DOUBLE) ; class-wise sum over references 

  result['confusionMatrix'] = nij
  result['sumReference'] = n_j
  result['sumEstimation'] = ni_
  result['numberOfSamples'] = self.numberOfSamples
    
  result['adjustedConfusionMatrix'] = mij
  result['adjustedSumReference'] = m_j
  result['adjustedSumEstimation'] = mi_
  result['adjustedNumberOfSamples'] = m

  result['confidenceIntervals'] = hash('significanceLevels', alphas)
  self.addResult, 'overallAccuracy',          overallAccuracy,           overallAccuracySquaredStandardError,         Alphas=alphas, Result=result, /Percent
  self.addResult, 'kappaAccuracy',            kappaAccuracy,            kappaAccuracySquaredStandardError,            Alphas=alphas, Result=result, /Percent
  self.addResult, 'userAccuracy',             userAccuracy,             userAccuracySquaredStandardError,             Alphas=alphas, Result=result, /Percent
  self.addResult, 'producerAccuracy',         producerAccuracy,         producerAccuracySquaredStandardError,         Alphas=alphas, Result=result, /Percent
  self.addResult, 'conditionalKappaAccuracy', conditionalkappaAccuracy, conditionalKappaAccuracySquaredStandardError, Alphas=alphas, Result=result, /Percent
  self.addResult, 'errorOfOmission',          1.-producerAccuracy,      producerAccuracySquaredStandardError,         Alphas=alphas, Result=result, /Percent
  self.addResult, 'errorOfCommission',        1.-userAccuracy,          userAccuracySquaredStandardError,             Alphas=alphas, Result=result, /Percent
  self.addResult, 'f1Accuracy',               f1Accuracy,                                                                            Result=result, /Percent
  self.addResult, 'averageUserAccuracy',      averageUserAccuracy,                                                                   Result=result, /Percent
  self.addResult, 'averageProducerAccuracy',  averageProducerAccuracy,                                                               Result=result, /Percent
  self.addResult, 'averageF1Accuracy',        averageF1Accuracy,                                                                     Result=result, /Percent
  self.addResult, 'classProportion',          classProportion,          classProportionSquaredStandardError,          Alphas=alphas, Result=result, /Percent
  
  result['adjustedAreaEstimation'] = self.adjustAreaEstimation
  result['adjustedSamplingBias']   = self.adjustSamplingBias
  
  if isa(totalArea) then begin
    classArea = classProportion*totalArea
    classAreaSquaredStandardError = (totalArea * sqrt(classProportionSquaredStandardError))^2
    result['totalArea'] = totalArea
    self.addResult, 'classArea', classArea, classAreaSquaredStandardError, Alphas=alphas, Result=result
  endif
  
  if self.adjustSamplingBias then begin
    result['strataProportions']       = *self.strataProportions
    result['strataSampleProportions'] = *self.strataSampleProportions
    result['strataWeights']           = *self.strataWeights
    result['strataNames']             = *self.strataNames
  endif
   
  result['numberOfClasses'] = numberOfClasses
  result['classColors'] = *self.classColors
  result['classNames'] = *self.classNames
  return, result
  
end

pro hubMathIncClassificationPerformance::addResult, name, mean, squaredStandardError, Alphas=alphas, Result=result, Percent=percent
  result[name] = mean * (keyword_set(percent)?100d:1d)
  if ~isa(squaredStandardError) then return
  standardError = sqrt(squaredStandardError > 0) ;avoid negative squared SEs
  result[name+'StandardError'] = standardError * (keyword_set(percent)?100d:1d)
  confidenceIntervals = orderedhash()
  
  ;Add confidence intervalls
  foreach alpha, alphas do begin
    z = gauss_cvf(alpha/2.)
    confidenceIntervals[alpha] = {intervalStart:result[name]-z*result[name+'StandardError'], intervalEnd:result[name]+z*result[name+'StandardError']}
  endforeach
  (result['confidenceIntervals'])[name] = confidenceIntervals
end

;function hubMathIncClassificationPerformance::getConficenceIntervalls, result, alpha, numberOfSamples
;  ciResults = Hash()
;  
;  ciResults['overallAccuracy'] = self.getConfidenceIntervall(result['overallAccuracy'], result['overallAccuracyStandardError']^2, alpha, numberOfSamples)
;  ciResults['kappaAccuracy'] = self.getConfidenceIntervall(result['kappaAccuracy'], result['kappaAccuracyStandardError']^2, alpha, numberOfSamples)
;  ciResults['userAccuracy'] = self.getConfidenceIntervall(result['userAccuracy'], result['userAccuracyStandardError']^2, alpha, numberOfSamples)
;  ciResults['producerAccuracy'] = self.getConfidenceIntervall(result['producerAccuracy'], result['producerAccuracyStandardError']^2, alpha, numberOfSamples)
;  ciResults['conditionalKappaAccuracy'] = self.getConfidenceIntervall(result['conditionalKappaAccuracy'], result['conditionalKappaAccuracyStandardError']^2, alpha, numberOfSamples)
;  ciResults['errorOfOmission'] = self.getConfidenceIntervall(result['errorOfOmission'], result['producerAccuracyStandardError']^2, alpha, numberOfSamples)
;  ciResults['errorOfCommission'] = self.getConfidenceIntervall(result['errorOfCommission'], result['userAccuracyStandardError']^2, alpha, numberOfSamples)
;  return, ciResults
;end

;function hubMathIncClassificationPerformance::getConfidenceIntervall, mean, squaredStandardError, alpha, numberOfSamples
;  z = gauss_cvf(alpha/2.)
;  standardDeviation = sqrt(squaredStandardError)
;  standardError = standardDeviation/sqrt(numberOfSamples)
;  ci = transpose([[mean-z*sqrt(squaredStandardError)],[mean+z*sqrt(squaredStandardError)]])
;  return, ci
;end

pro hubMathIncClassificationPerformance__define
  
  struct = {hubMathIncClassificationPerformance $
    , inherits hubMathInc $
    , numberOfClasses : 0 $
    , confusionMatrix : ptr_new() $
    , adjustedConfusionMatrix : ptr_new() $
    , adjustSamplingBias : 0b $
    , adjustAreaEstimation : 0b $
    , strataProportions : ptr_new() $
    , strataSampleProportions : ptr_new() $
    , strataWeights : ptr_new() $
    , strataNames: ptr_new() $
    , numberOfStrata : 0 $
    , mapClassProportions : ptr_new() $
    , classColors: ptr_new() $
    , classNames: ptr_new() $
    }
end

;+
; :hidden:
;-
pro test_hubMathIncClassificationPerformance
  h = hubMathIncClassificationPerformance(5)
  h.addData, [1,2,3,4,5,1,2,3,4,5], [1,2,3,4,5,1,2,3,4,5]
  print,h.getNumberOfSamples()
  res = h.getResult()
  print, res['confusionMatrix']
  print, res['overallAccuracy']
  print
  print, res['userAccuracy']
  print, res['producerAccuracy']
  print, res
  
end