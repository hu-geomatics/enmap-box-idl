;+
; A hubMathIncProduct object can be used to calculate the product of different data values incrementally.
;
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Adds data values to calculate the product from. 
;
; :Params:
;    x: in, required, type = numeric
;     Scalar or vector with data values.
;
;-
pro hubMathIncProduct::addData $
  , x

  self.hubMathInc::addData, x
  if ~isa(x) then return 
  self.product *= product(x, /Double)

end

;+
; :Description:
;    Returns the product of all added data values.
;-
function hubMathIncProduct::getResult
  if self.numberOfSamples eq 0 then begin
    result = !VALUES.D_NAN    
  endif else begin
    result = self.product  
  endelse
  return, result
end

;+
; :hidden:
;-
pro hubMathIncProduct__define
  
  struct = {hubMathIncProduct $
    , inherits hubMathInc $
    , product : 0d $
  }
end

pro test_hubMathIncProduct

  incProduct = hubMathIncProduct()
  print, incProduct.getResult()
  
end