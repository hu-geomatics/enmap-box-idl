;+
; A hubMathIncMaximum object can be used to calculate the maximum of different data values incrementally.
;
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the maximum of all added data values.
;-
function hubMathIncMaximum::getResult
  range = self.hubMathIncRange::getResult()
  if isa(range) then begin
    result = range[1]
  endif else begin
    result = !null
  endelse
  return, result
end


;+
; :hidden:
;-
pro hubMathIncMaximum__define
  
  struct = {hubMathIncMaximum $
    , inherits hubMathIncRange}
end

pro test_hubMathIncMaximum

  incMaximum = hubMathIncMaximum()
  print, incMaximum.getResult()
  
end

