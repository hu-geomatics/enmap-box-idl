;+
; :Description:
;    Wrapper for `hubMathIncClassificationPerformance`
;
; :Params:
;    reference
;    estimation
;    numberOfClasses
;
; :Keywords:
;    AdjustSamplingBias
;    StrataLabel
;    StrataProportions
;    StrataSampleProportions
;    AdjustAreaEstimation
;    MapClassProportions
;    TotalArea
;    classNames
;    classColors
;    mapInfo : in, optional, type = map info dictionary
;    
;     A dictionary that contains the following values::
;       key   | type    | description
;       ------+---------+------------------------------
;       sizeX | numeric | pixel size in x direction
;       sizeY | numeric | pixel size in y direction
;       units | string  | length unit, e.g. km, m, feet
;       
;     You might create an empty map info dictionary using the command: 'hubIOImgMeta_mapInfo.createEmptyMapInfoStruct()'
;       
;-
function hubMathClassificationPerformance, reference, estimation, numberOfClasses, $
  AdjustSamplingBias=adjustSamplingBias, StrataLabel=strataLabel, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions,$
  AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions=mapClassProportions,$
  TotalArea=totalArea, classNames=classNames, classColors=classColors $
  , mapInfo=mapInfo
  
  inc = hubMathIncClassificationPerformance(numberOfClasses, $
          AdjustSamplingBias=adjustSamplingBias, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions, $
          AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions=mapClassProportions,$
          ClassNames=classNames, ClassColors=classColors)
          
  inc.addData, reference, estimation, strataLabel
  performance = inc.getResult(TotalArea=totalArea) 
  if isa(mapInfo) then performance['map info'] = mapInfo
  return, performance+hash('accuracyType', 'classification')
end

pro test_hubMathClassificationPerformance
  numberOfClasses = 4
  reference = [1,2,3,4]
  estimation = [1,2,3,4]
  strataLabel = [1,1,2,2]
  strataProportions = [0.75, 0.25]
  strataSampleProportions = [0.5, 0.5]
  mapClassProportions = [0.25, 0.25, 0.25, 0.25]
  totalArea = 10000
  mapInfo = hubIOImgMeta_mapInfo.createEmptyMapInfoStruct()
  mapInfo.sizeX = 30
  mapInfo.sizeY  = 30
  mapInfo.units = 'meters'
  performance = hubMathClassificationPerformance(reference, estimation, numberOfClasses, $
    /AdjustSamplingBias, StrataLabel=strataLabel, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions,$
    /AdjustAreaEstimation, MapClassProportions=mapClassProportions $
    , mapInfo = mapInfo $
    , TotalArea=totalArea, ClassNames=classNames, ClassColors=classColors)

  report = hubApp_accuracyAssessment_getReport_Classification(performance)
  report.SaveHTML, /show
end