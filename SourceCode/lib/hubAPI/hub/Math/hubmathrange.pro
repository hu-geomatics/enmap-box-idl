function hubMathRange, array
  if ~isa(array) then return, !NULL
  range = [min(array, /NAN), max(array, /NAN)]
  return, range
end