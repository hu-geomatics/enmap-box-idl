;+
; :Description:
;    Wrapper for `hubMathIncRegressionPerformance`
;
; :Params:
;    reference
;    estimation
;-
function hubMathRegressionPerformance, reference, estimation
  inc = hubMathIncRegressionPerformance(mean(reference), mean(estimation))
  inc.addData, reference, estimation
  return, inc.getResult()
end

pro test_hubMathRegressionPerformance
  result = hubMathRegressionPerformance([1,2,3,4,5],[1,2,3,4,5])
  print, result
end