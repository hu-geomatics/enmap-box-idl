;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Evaluates logical `xor`. Returns 1 if exactly one element of `array` is true and 0 otherwise.
;
; :Params:
;    array: in, required, type=byte[]
;      Array of zeroes and ones.
;
; :Examples:
;    Some examples::
;      print, hubMathHelper.getXOR([0,1,0])
;      print, hubMathHelper.getXOR([0,1,1])
;      print, hubMathHelper.getXOR([0,0,0])
;    IDL prints::
;      1
;      0
;      0
;-
function hubMathHelper::getXOR, array
  compile_opt static, strictarr
  return, total(array) eq 1 
  
end

;+
; :Description:
;    Evaluates logical `or`. Returns 1 if at least one element of `array` is true and 0 otherwise.
;
; :Params:
;    array: in, required, type=byte[]
;      Array of zeroes and ones.
;
; :Examples:
;    Some examples::
;      print, hubMathHelper.getOR([0,1,0])
;      print, hubMathHelper.getOR([0,1,1])
;      print, hubMathHelper.getOR([0,0,0])
;    IDL prints::
;      1
;      1
;      0
;-
function hubMathHelper::getOR, array
  compile_opt static, strictarr
  return, total(array) ge 1 
  
end

;+
; :Description:
;    Evaluates logical `and`. Returns 1 if all element of `array` are true and 0 otherwise.
;
; :Params:
;    array: in, required, type=byte[]
;      Array of zeroes and ones.
;
; :Examples:
;    Some examples::
;      print, hubMathHelper.getAND([0,1,0])
;      print, hubMathHelper.getAND([1,1,1])
;      print, hubMathHelper.getAND([0,0,0])
;    IDL prints::
;      0
;      1
;      0
;-
function hubMathHelper::getAND, array
  compile_opt static, strictarr
  return, product(array) ne 0 
  
end



;+
; :Description:
;    Returns the set with distinct and ordered values of a given input array
;
; :Params:
;    values: in, required
;     The input array with values you want to get the set from.
;-
function hubMathHelper::getSet, values
  compile_opt static

  if isa(values, 'list') then begin
    return, hubMathHelper.getSet(values.toArray())
  endif
  if ~isa(values) || n_elements(values) eq 0 then return, []
  setValues = values[sort(values)]
  return, setValues[uniq(setValues)]
end

;+
; :Description:
;    Returns the union of set A and B.
;
; :Params:
;    A: in, required, type = numeric[]
;    B: in, required, type = numeric[]
;
; :Author: benjaminjakimow
;-
function hubMathHelper::getUnion, A, B
  compile_opt static, strictarr
  if ~isa(A) then return, hubMathHelper.getSet(B)
  if ~isa(B) then return, hubMathHelper.getSet(A)
  return, hubMathHelper.getSet([A, B])
end

;+
; :Description:
;    Returns the intersection of A and B
;
; :Params:
;    A: in, required
;     Array with values of set A 
;    B: in, required
;     Array with values of set B
;-
function hubMathHelper::getIntersection, A, B
  compile_opt static, strictarr
  if ~isa(A) || ~isa(B) then return, !NULL
  
  setValues = list()
  foreach v, A do begin
    if total(v eq B) gt 0 then begin
      setValues.add, v
    endif
  endforeach
  return, hubMathHelper.getSet(setValues.toArray())
end

;+
; :Description:
;    Returns the relative complement of A in B or the difference between A, respectively. 
;    Returns all values from B that are not in A.
;
; :Params:
;    B: in, required
;     Array with values of you want to choose from.
;    A: in, required
;     Array with values of you don't want to have in your results. 

;-
function hubMathHelper::getDifference, B, A
  compile_opt static, strictarr
  if ~isa(A) then return, hubMathHelper.getSet(B)
  
  _B = hubMathHelper.getSet(B)
  _A = hubMathHelper.getset(A)
  
  setValues = list()
  
  foreach v, _B do begin
    if TYPENAME(v) eq 'STRING' then begin
      if total(STRCMP(v, _A)) eq 0 then begin
        setValues.add, v
      endif
    endif else begin
      if total(v eq _A) eq 0 then begin
        setValues.add, v
      endif
    endelse
    
  endforeach
  return, hubMathHelper.getSet(setValues)
end

function hubMathHelper::getSymetricDiff, A, B
  compile_opt static, strictarr
  return, hubMathHelper.getUnion(hubMathHelper.getDifference(A, B), hubMathHelper.getDifference(B, A))
end


;+
; :Hidden:
; :Description:
;    Describe how to use the set functions
;-
pro test_hubMathHelper_setOperations
  
  A = hubMathHelper.getSet([3,2,1,2,1,1])
  B = hubMathHelper.getSet([3,4])
  print, A, B
  print, 'Diff B-A:', hubMathHelper.getDifference(B,A)
  print, 'Diff A-B:', hubMathHelper.getDifference(A,B)
  print, 'Intersection:', hubMathHelper.getIntersection(A,B)
  print, 'Union:', hubMathHelper.getUnion(A,B)
  print, 'Sym.Diff:', hubMathHelper.getSymetricDiff(A,B)
end
;+
; :Description:
;    Evaluates mathematical +,-,/ and * operator. Behaves like the normal IDL operators, 
;    but can handle !null or undefined operands. If one or both operands are !null or
;    undefined, !null is returned.  
;
; :Params:
;    operator: in, required, type={'+' | '-' | '/' | '*'}
;      IDL operator provided as a string.
;    a: in, required, type=numerical
;      First operand.
;    b: in, required, type=numerical
;      Second operand.
;
; :Examples:
;    Some examples::
;      print, hubMathHelper.evalOp('+',1,2)
;      print, hubMathHelper.evalOp('+',1,notDefined)
;      print, hubMathHelper.evalOp('+',!null,2)
;    IDL prints::
;      3
;      !NULL
;      !NULL
;    The normal IDL + operator would throw an error::
;      print, 1+notDefined
;    IDL prints::
;      % Variable is undefined: NOTDEFINED.
;      % Error occurred at: $MAIN$          
;      % Execution halted at: $MAIN$ 
;-
function hubMathHelper::evalOp, operator, a, b
  compile_opt static, strictarr
  
  if ~isa(a) or ~isa(b) then return, !null

  case operator of
    '+'  : result = a + b
    '-'  : result = a - b
    '/'  : result = a / b
    '*'  : result = a * b
  endcase
  
  return, result
  
end

function hubMathHelper::resolveDataTypeCode, dataTypeCodes
  compile_opt static, strictarr
  
  dataTypeNames = list()
  foreach dataTypeCode, dataTypeCodes do begin
    case dataTypeCode of
      1:         dataTypeNames.add, 'byte'
      2:         dataTypeNames.add, 'int'
      3:         dataTypeNames.add, 'long'
      4:         dataTypeNames.add, 'float'
      5:         dataTypeNames.add, 'double'
      6:         dataTypeNames.add, 'complex'
      7:         dataTypeNames.add, 'string'
      8:         dataTypeNames.add, 'anonymous'
      9:         dataTypeNames.add, 'dcomplex'
      10:        dataTypeNames.add, 'pointer'
      11:        dataTypeNames.add, 'object'
      12:        dataTypeNames.add, 'uint'
      13:        dataTypeNames.add, 'ulong'
      14:        dataTypeNames.add, 'long64'
      15:        dataTypeNames.add, 'ulong64'
      else : message, 'unknown type code:'+strtrim(dataTypeCode, 2)
    end
  endforeach
  return, dataTypeNames.toArray()
end

function hubMathHelper::resolveDataTypeAlias, dataTypeAliases
  compile_opt static, strictarr

  dataTypes = list()
  foreach dataTypeAlias, dataTypeAliases do begin
    if typename(dataTypeAlias) eq 'BYTE' then begin
      s = strtrim(fix(dataTypeAlias),2)
    endif else begin
      s = strtrim(dataTypeAlias,2)
    endelse
    s = strlowcase(s)
    
    case s of
      'byte':      dataTypes.add, 1
      'int':       dataTypes.add, 2
      'long':      dataTypes.add, 3
      'float':     dataTypes.add, 4
      'double':    dataTypes.add, 5
      'complex':   dataTypes.add, 6
      'string':    dataTypes.add, 7
      'structure': dataTypes.add, 8
      'dcomplex':  dataTypes.add, 9
      'pointer':   dataTypes.add, 10
      'object':    dataTypes.add, 11
      'uint':      dataTypes.add, 12
      'ulong':     dataTypes.add, 13
      'long64':    dataTypes.add, 14
      'ulong64':   dataTypes.add, 15
      '1':         dataTypes.add, 1
      '2':         dataTypes.add, 2
      '3':         dataTypes.add, 3
      '4':         dataTypes.add, 4
      '5':         dataTypes.add, 5
      '6':         dataTypes.add, 6
      '7':         dataTypes.add, 7
      '8':         dataTypes.add, 8
      '9':         dataTypes.add, 9
      '10':        dataTypes.add, 10
      '11':        dataTypes.add, 11
      '12':        dataTypes.add, 12
      '13':        dataTypes.add, 13
      '14':        dataTypes.add, 14
      '15':        dataTypes.add, 15
      else: message, 'Not a valid data type 
    endcase 
     
  endforeach
  return, dataTypes.toArray()
end
  
function hubMathHelper::isConvertableData, data, dataTypeAlias
  compile_opt static, strictarr
  
  if isa(data) then begin
    dataType = hubMathHelper.resolveDataTypeAlias(dataTypeAlias)
    data0 = data[0]
    
    on_ioerror, badIO
    catch, errorStatus
    if (errorStatus ne 0) then begin
      catch, /CANCEL
      badIO:
      return, 0b
    endif
  
    ; try conversion
    !null = fix(data0, TYPE=dataType>2)
    catch, /CANCEL
  endif

  return, 1b

end

;+
; :Description:
;    Converts `data` to a specific type, given by `dataType`, and returns the result.
;
; :Params:
;    data: in, required, type=numerical
;      Data to be converted.
;      
;    dataType: in, required, type=type ID or string
;      Target data type like::
;        string    | type ID
;        ----------|---------       
;        'byte'    | 1 
;        'int'     | 2
;        'long'    | 3
;        'float'   | 4
;        'double'  | 5
;        'uint'    | 12
;        'ulong'   | 13
;        'long64'  | 14
;        'ulong64' | 15
;        
; :Keywords:
;    NoCopy: in, optional, type=booloen
;      `data` variable is freed.
;
; :Examples:
;    Convert data to float::
;      print, hubMathHelper.convertData([1,2,3],4)
;      print, hubMathHelper.convertData([1,2,3],'float')
;    IDL prints::
;            1.00000      2.00000      3.00000
;            1.00000      2.00000      3.00000
;
;-
function hubMathHelper::convertData $
  ,data $
  ,dataType $
  ,NoCopy=noCopy
  
  compile_opt static, strictarr
  ; prepare result variable
  
  if keyword_set(noCopy) then begin
    convertedData = temporary(data)
  endif else begin
    convertedData = data
  endelse
  
  ; replace alias
  
  type = hubMathHelper.resolveDataTypeAlias(dataType)
  
  ; check if current data type is equal to target data type
  
  currentDataType = size(/Type, convertedData)
  currentDataTypeEqualToTargetDataType = type eq currentDataType

  ; convert data

  if ~currentDataTypeEqualToTargetDataType then begin
  
    if isa(convertedData) then begin

      ; handle special case: from string to byte
      if (type eq 1) and (currentDataType eq 7) then begin
        convertedData = fix(convertedData)
      endif
      ; handle special case: from byte to string
      if (type eq 7) and (currentDataType eq 1) then begin
        convertedData = fix(convertedData)
      endif
      
      convertedData = fix(convertedData, TYPE=type)

    endif else begin

      convertedData = !null

    endelse

  endif

  return,convertedData

end

;+
; :Description:
;    Reformats the IDL `histogram` function REVERSE_INDICES information. It returns a 
;    list of arrays, the i-th array is holding the indices for the i-th bin. 
;
; :Params:
;    reverseIndices: in, required, type=see `REVERSE_INDICES` keyword of the `histogram` function
;    locations: in, required, type=see `LOCATIONS` keyword of the `histogram` function
;
; :Examples:
;    Generate some data, calculate the histogram, plot it and print all indices for each bin::
;      data = fix(randomu(seed, 25)*9)
;      histo = histogram(data,REVERSE_INDICES=reverse_indices,LOCATIONS=locations)
;      plot, locations, histo,PSYM=10
;      reverseIndicesList = hubMathHelper.reverseIndicesToList(reverse_indices,locations)
;      for i=0,n_elements(histo)-1 do begin &$
;        print, string(i+1)+'th bin indices: '+strjoin(reverseIndicesList[i],', ') &$
;      endfor
;    IDL prints something like::
;             1th bin indices:            1,           12
;             2th bin indices:            5
;             3th bin indices:           21,           22
;             4th bin indices:            9,           10,           14,           17,           23,           24
;             5th bin indices:           16
;             6th bin indices:            2,           11,           19
;             7th bin indices:            0,            4,           13,           18
;             8th bin indices:            3,            7
;             9th bin indices:            8,           15,           20
;-
function hubMathHelper::reverseIndicesToList $
  ,reverseIndices $
  ,locations
  
  compile_opt static, strictarr
  
  indicesList = list()

  foreach location,locations,bin do begin
  
    ; get indices for the current bin

    binIsNotEmpty = reverseIndices[bin] ne reverseIndices[bin+1]
    if binIsNotEmpty then begin 
      indices = reverseIndices[reverseIndices[bin] : reverseIndices[bin+1]-1]
    endif else begin
      indices = !null
    endelse
    
    ; save indices for the current bin

    indicesList.add, indices
    
  endforeach
  
  return, indicesList

end

;+
; :Hidden:
;-
pro hubMathHelper__define
  struct = {hubMathHelper $
    ,inherits IDL_Object $
  }
end

