;+
; :Description:
;    Returns the set difference of set A and B. Inputs and outputs are arrays.
;
; :Params:
;    A
;    B
;-
function hubMathSetDifference, A, B

  isElementOfB = bytarr(n_elements(A))
  foreach elemA, A, i do begin
    isElementOfB[i] = isa(where(/NULL, B eq elemA))
  endforeach
  result = A[where(/NULL, ~isElementOfB)]
  return, result
end

pro test_hubMathSetDifference
  print, hubMathSetDifference([1,2,3], [2,3,4]) ; should be [1]
end
