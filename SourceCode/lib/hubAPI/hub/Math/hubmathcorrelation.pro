;+
; :Description:
;    Wrapper for `hubMathIncClassificationPerformance`
;
; :Params:
;    x: in, required, type = numeric
;     Scalar / Vector of data values.
;     
;    y: in, required, type = numeric
;     Scalar / Vector of data values. Must have same length as `x`.
;
;-
function hubMathCorrelation, x, y
  inc = hubMathIncCorrelation(mean(x), mean(y))
  inc.addData, x, y
  return, inc.getResult()
end


;+
; :Hidden:
;-
pro test_hubMathCorrelation
  result = hubMathCorrelation([1,2,3,4,5], [1,1,1,1,1])
  print, result
end
