;+
; :Description:
;    Turns math error checking off.
;
; :Params:
;    state: out, optional, type={1 | 2 | 3}
;      Use this keyword to return the current value of !EXCEPT.
;-
pro hubMath_mathErrorsOff, state
  state = !EXCEPT
  !EXCEPT=0
end