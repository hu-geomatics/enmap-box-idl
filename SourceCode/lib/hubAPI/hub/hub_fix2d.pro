function hub_fix2d, array
  if isa(array) then begin
    !null = reform(/OVERWRITE, array, n_elements(array[*,0]), n_elements(array[0,*]))
  endif
  return, array
end