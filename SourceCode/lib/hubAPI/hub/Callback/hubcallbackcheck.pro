;+
; Use this batch to force IDL to check if a callback (widget events)  occurs.   
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;
;-
  if isa(hubCallBackCheckID) && widget_info(/VALID_ID, hubCallBackCheckID) then begin
    !null = widget_event(hubCallBackCheckID, /NOWAIT, /SAVE_HOURGLASS)
  endif else begin
    !null = widget_event(/NOWAIT, /SAVE_HOURGLASS)
  endelse
  
  