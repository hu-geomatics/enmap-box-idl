;+
; :Author: geo_beja
;-
;+
; :Description:
;    This method is called when creating the object with hubIOSLOutputSpeclib(). 
;
; :Params:
;    filename: in, required, type=file path
;     Specifies the file name of the ENVI Spectral Library.
;     
; :Keywords:
;    numberOfBands: in, required, type = int
;     Use this keyword to specify the number of bands each spectrum must be defined for.
;     By default this value will be set to the number of bands of the first written spectrum. 
;     
;    dataType: in, optional, type = IDL data type code
;     Use this keyword to specify the numeric IDL data type which is used to store the 
;     spectral values. 
;      
;    NoOpen: in, optional, type = boolean
;     Set this keyword to avoid opening the new spectral library in the EnMAP-Box.  
;
;-
function hubIOSLOutputSpeclib::init $
  , filename $
  , numberOfBands = numberOfBands $
  , numberOfProfiles = numberOfProfiles $
  , dataType = dataType $
  , NoOpen = NoOpen
  
  
  ;get file access
  !null = self->hubIOImgImage::init(filename)
  self.filenameData = filename
  self.filenameHeader = self._createfilenameHeader(self.filenameData)
  self.header.setMeta, 'filename header', self.filenameHeader
  self.openFile = ~keyword_set(noOpen)
  
  ;get logical file unit
  openw, logicalUnitNumber, self.filenameData ,/GET_LUN
  self.logicalUnitNumber = logicalUnitNumber
  
  ;spectra data type
  if isa(dataType) then begin 
    self.binaryIO_type = dataType
    typeInfo = hubHelper.getTypeInfo(self.binaryIO_type)
    self.binaryIO_typeSize = typeInfo.size
  endif else begin
    self.binaryIO_type = -1
  endelse
  
  self.numberOfBands = isa(numberOfBands)? numberOfBands : -1
  self.numberOfProfiles = 0
  self.profileNames = list()
  
  
  return,1b

end

function hubIOSLOutputSpeclib::_createfilenameHeader, filenameData

  dn = file_dirname(filenameData, /MARK_DIRECTORY)
  bn = file_basename(filenameData)

  if stregex(bn, '\.(bsq|bil|bip|esl|sli)$', /FOLD_CASE, /BOOLEAN) then begin
    bn = STRMID(bn, 0, strlen(bn)-4)
  endif
  return, dn + bn + '.hdr'
end

;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    metaName: in, required, type=string
;     Use this argument to specify the metadata name.
;     
; :Keywords:
;    Default: in, optional
;     Set this keyword to define the default values. 
;     It is used in case the meta value is not set explicitely in the file header.
;
;-
function hubIOSLOutputSpeclib::getMeta $
  ,metaName, Default=Default 
  return, self.hubIOImgImage::getMeta(metaName, Default=Default)
end



;+
; :Description:
;    This method is used to set a single metadata value of the output spectral library.
;
; :Params:
;    metaName: in, required, type=string
;     Use this argument to specify the metadata name.
;     
;    metaValue: in, required, type=string
;      Use this argument to specify the metadata value.
;
;
;
;-
pro hubIOSLOutputSpeclib::setMeta $
  ,metaName $
  ,metaValue  
  
  self.header.setMeta, metaName, metaValue
end

;+
; :Description:
;    This method is used to copy metadata values from an hubIOImgImage Object.
;    
;    For the complete list of metadata see `Supported Metadata <./metadata.html>`.
;
; :Params:
;    hubIOImgImage: in, required, type = {hubIOImgInputImage | hubIOImgOutputImage | hubIOSLOutputSpeclib}
;     Use this argument to specify the source image or spectral library from which the metadata is copied.
;    
;    metaNames: in, required, type=string[]
;     Use this argument to specify an array of metadata name, to be copied.
;
;
;-
pro hubIOSLOutputSpeclib::copyMeta $
  , hubIOImgImage $
  , metaNames 
  
  for i=0, n_elements(metaNames)-1 do begin
    if hubIOImgImage.hasMeta(metaName[i]) then begin
      self.setMeta, metaName[i], hubIOImgImage.getMeta(metaName[i]) 
    endif
  endfor
end


;+
; :Description:
;    Use this function to check whether a specific meta value is defined. 
;
; :Params:
;    metaNames: in, required, type = {string | string[]}
;
;
;
;-
function hubIOSLOutputSpeclib::hasMeta $
  , metaNames
  return, self.hubIOImgImage::hasMeta(metaNames)
end


;+
; :Description:
;    Use this routine to finish writing and creat the header file that acompanies the 
;    binary file which containes the spectral values.
;    
;-
pro hubIOSLOutputSpeclib::finishWriter
  
  free_lun, self.logicalUnitNumber
  
  if self.numberOfProfiles eq 0 then begin
    FILE_DELETE, self.filenameData
  endif else begin
    self.setMeta, 'file type', 'ENVI Spectral Library
    self.setMeta, 'bands', 1
    self.setMeta, 'lines', self.numberOfProfiles
    self.setMeta, 'samples', self.numberOfBands
    self.setMeta, 'interleave', 'bsq'
    self.setMeta, 'data type', self.binaryIO_type
    self.setMeta, 'byte order', hubHelper.getByteOrder()
    if n_elements(self.profileNames) gt 0 then begin
      self.setMeta, 'spectra names', self.profileNames.toArray()
    endif
    ;write header data    
    self.header.writeFile,self.filenameHeader
    
    ;open file in EnMAP-Box
    if self.openFile then begin
      hubProEnvHelper.openImage, self.filenameData
    endif
  endelse
end

;+
; :Description:
;    Use this methode to write spectral data.
;
; :Params:
;    profiles: in, required, type=numeric[bands][spectra]
;      Use this parameter to write profiles 
;
;
;-
pro hubIOSLOutputSpeclib::writeData, profiles
  nBands = n_elements(profiles[*,0])
  nProfiles = n_elements(profiles[0,*])
  
  if self.binaryIO_type eq -1 then begin
    self.binaryIO_type = size(profiles, /TYPE)
    typeInfo = hubHelper.getTypeInfo(self.binaryIO_type)
    self.binaryIO_typeSize = typeInfo.size
  endif
  
  if self.numberOfBands eq -1 then self.numberOfBands = nBands 
  if nBands ne self.numberOfBands then begin
    message, string(format='(%"Each spectrum must have %i band values")', self.numberOfBands)
  endif

  ; write data
  ; cast to output data type
  profiles = hubMathHelper.convertData(profiles, self.binaryIO_type, /NoCopy)

  ;write to file
  writeu, self.logicalUnitNumber, profiles
  
  self.numberOfProfiles += nProfiles
end


;+
; :Description:
;    This method is used to performs all cleanup on the object and 
;    calls the hubIOSLOutputSpeclib::finishWriter method.
;-
pro hubIOSLOutputSpeclib::cleanup
  self.finishWriter
end

;+
; :Description:
;    This function returns the number already written spectra.
;-
function hubIOSLOutputSpeclib::numberOfProfiles
  return, self.numberOfProfiles
end

;+
; :Hidden:
;-
pro hubIOSLOutputSpeclib__define
  struct = {hubIOSLOutputSpeclib $
    , inherits hubIOImgImage $
    , binaryIO_type : 0 $
    , binaryIO_typeSize : 0 $
    , openFile : 0b $
    , fileName : 0b $
    , numberOfBands:0 $
    , numberOfProfiles:0l $
    , profileNames:list() $
  }
  
end


;+
; :hidden:
;-
pro test_hubIOSLOutputSpeclib
 imgPath = hub_getTestImage('Hymap_Berlin-A_Image')
 eslPath = 'D:\testESL'
 n = 50 ;300l * 300l
 tileLines = 2
 
 inputImage = hubIOImgInputImage(imgPath)
 eslWriter = hubIOSLOutputSpeclib(eslPath)
 
 inputImage.initReader, tileLines, /Slice, /TileProcessing
 while ~inputImage.tileProcessingDone() && eslWriter.numberOfProfiles() le n do begin
  data = inputImage.getData()
  ;add profiles
  ;eslWriter.writeData, data
  
  ;add single spectrum
  eslWriter.writeData, data[*,1:5]
  
  ;add many spectra
  ;eslWriter.writeData, data[*,1:10]
  
  ;add single spectrum with name
  eslWriter.writeData, data[*,1:3]
 
 endwhile
 inputImage.cleanup
 eslWriter.cleanup ;calls finishWriter implicitely
 print, 'done!'
end