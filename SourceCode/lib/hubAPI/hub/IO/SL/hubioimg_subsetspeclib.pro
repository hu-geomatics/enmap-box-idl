pro hubIOImg_subsetSpeclib, inFilename, outFilename, indices
 
  inputImage = hubIOImgInputImage(inFilename)
  outputImage = hubIOImgOutputImage(outFilename)
  
  inputImage.initReader, /Cube
  data = inputImage.getData()
  
  writerSettings = inputImage.getWriterSettings(SETLINES=n_elements(indices))

  outputImage.initWriter, writerSettings
  outputImage.setmeta, 'wavelength', inputImage.getmeta('wavelength')
  outputImage.setmeta, 'wavelength units', inputImage.getmeta('wavelength units')
  outputImage.setmeta, 'spectra names', (inputImage.getmeta('spectra names'))[indices]
  outputImage.setmeta, 'file type', inputImage.getmeta('file type')
 
  outputImage.writeData, transpose(data[*, indices, *],[2,1,0])
  
  outputImage.cleanup
  inputImage.cleanup

end

pro test_hubIOImg_subsetSpeclib

  inFilename = 'C:\Program Files\Exelis\ENVI53\resource\filterfuncs\landsat8_oli.sli'
  outFilename = filepath(/TMP,'l8_reduced_')
  
  hubIOImg_subsetSpeclib, inFilename, outFilename, [1:3]
end