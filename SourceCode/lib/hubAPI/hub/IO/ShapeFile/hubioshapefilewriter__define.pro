function hubIOShapeFileWriter::init, filename, shapeType, append=append, spatialReferenceSystem=spatialReferenceSystem
  
  
  
  
  isInit = self.hubIOShapeFile::init(filename)
  
  ;create the spatial reference file, if defined
  self.spatialReferenceSystem = isa(spatialReferenceSystem) ? spatialReferenceSystem : ''
  if self.spatialReferenceSystem ne '' then begin
    fn = self.getFilename()
    dn = FILE_DIRNAME(fn)
    bn = FILE_BASENAME(fn, '.shp', /FOLD_CASE)
    
    projFile = FILEPATH(bn+'.prj', ROOT_DIR=dn)
    
    openw, lun, projFile, /GET_LUN
    printf, lun, self.spatialReferenceSystem
    FREE_LUN, lun
    
  endif
  
  self.shapeType = self.hubIOShapeFile::_getShapeTypeCode(shapeType)
  
  
  ;check supported types
  case self.shapeType of
    1 : ;POINT supported 
    3 : ;POLYLINE supported 
    5 : ;POLYGONE supported
    8 :  message, 'MULTIPOINT not supported yet'
   11 :  message, 'POINTZ not supported yet'
   13 :  message, 'POLYLINEZ not supported yet'
   15 :  message, 'POLYGONZ not supported yet'
   18 :  message, 'MULTIPOINTZ not supported yet'
   21 :  message, 'POINTM not supported yet'
   23 :  message, 'POLYLINEM not supported yet'
   25 :  message, 'POLYGONM not supported yet'
   28 :  message, 'MULTIPOINTM not supported yet'
   31 :  message, 'MULTIPATCH not supported yet'
   else : message, 'unknown entity/geometry type'
  endcase
  
  isOpen = (self.shape).open(filename, entity_type = self.shapeType, /Update)
  if ~isOpen then message, 'Can not create new shape file: '+filename
  
  
  if ~keyword_set(append) then begin
    self.AddAttribute,'id',3,10, Precision=0
  endif 
  
  return, 1
end

pro hubIOShapeFileWriter::addAttribute, name, dataType, maxLength, precision=precision
  
  if strlen(name) gt 11 then MESSAGE, /CONTINUE, 'Shapefile attribute name '+name+' is to longan will get runcated. Use 11 characters at maximum'
  typeCode = hubMathHelper.resolveDataTypeAlias(dataType)
  typeCode = typeCode[0]
  idlTypeCodes   = [1,2,3,4,5,7,12,13,14,15]
  shapeTypeCodes = [3,3,3,5,5,7,3 ,3 ,3 ,3 ]
  ;3 = Long int
  ;5 = Double
  ;7 = string
  iShapeTypeCode = where(IDLTypeCodes eq typeCode, /NULL)
  if isa(iShapeTypeCode) then begin
    shapeTypeCode = ShapeTypeCodes[iShapeTypeCode] 
  endif else begin
     message, 'idl type can not be stored in a shape file'
  endelse
  
  (self.shape).AddAttribute, name, shapeTypeCode[0], maxLength, PRECISION=precision
  (self.attributeNames).add, name
end 

pro hubIOShapeFileWriter::writeData, x, y, attributeHash
  
  case self.shapeType of
    1 : self._writePoint  , x, y, attributeHash    ;POINT
    3 : self._writeLine   , x, y, attributeHash    ;POLYLINE / LINE 
    5 : self._writePolygon, x, y, attributeHash ;POLYGON
    else : message, 'Entity Type is not supported yet'
  endcase
  
end

pro hubIOShapeFileWriter::_writePolygon, x, y, attributeHash
   nVertices = n_elements(x) 
   
   vertices = make_array(2, nVertices, /Double)
   vertices[0,*] = x 
   vertices[1,*] = y
   
   shape = self.shape

   ; Create structure for new entity.
   entity = {IDL_SHAPE_ENTITY}
   entity.shape_type = self.shapeType
   entity.bounds[0] = min(x)
   entity.bounds[1] = min(y)
   
   entity.bounds[4] = max(x)
   entity.bounds[5] = max(y)
   
   entity.n_vertices = nVertices
   entity.vertices = ptr_new(vertices)
   
   self._writeEntity, entity, self._createAttributeStruct(attributeHash)
end


pro hubIOShapeFileWriter::_writeLine, x, y, attributeHash
   
   nVertices = n_elements(x) 
   if nVertices le 1 then message, 'A line must have at least two vertices'
   if n_elements(y) ne nVertices then message, 'x and y do not have the same number of elements'
         
   vertices = make_array(2, nVertices, /Double)
   vertices[0,*] = x 
   vertices[1,*] = y
   
   
   shape = self.shape

   ; Create structure for new entity.
   entity = {IDL_SHAPE_ENTITY}
   entity.shape_type = self.shapeType
   entity.bounds[0] = min(x)
   entity.bounds[1] = min(y)
   
   entity.bounds[4] = max(x)
   entity.bounds[5] = max(y)
   
   entity.n_vertices = nVertices
   entity.vertices = ptr_new(vertices)
   
   self._writeEntity, entity, self._createAttributeStruct(attributeHash)
   
end
   
pro hubIOShapeFileWriter::_writePoint, x, y, attributeHash
      
      ; Create structure for new entity.
      entity = {IDL_SHAPE_ENTITY}
      entity.shape_type = self.shapeType
      entity.bounds[0] = double(x)
      entity.bounds[1] = double(y)
      entity.bounds[4] = entity.bounds[0]  ;
      entity.bounds[5] = entity.bounds[1]  ;
      entity.n_vertices = 1
          
      
      self._writeEntity, entity, self._createAttributeStruct(attributeHash)

end


function hubIOShapeFileWriter::_createAttributeStruct, attributeHash
      
      attributes = (self.shape).GetAttributes(/ATTRIBUTE_STRUCTURE)
      
      ;add standard attribute values
      attributes.(0) = self.entityCount  ;add id
      
      
      if isa(attributeHash) then begin
        foreach attributeName, attributeHash.keys() do begin
          
          if attributeName eq 'id' then message, 'attribute name "id" is already reserved for internal use'
          
          iName = (self.attributeNames).where(attributeName)
          if ~isa(iName) then message, '"' + attributeName + '" is not an attribute of this shape file'
        
          attributes.(iName) = attributeHash[attributeName]
        
        endforeach
      endif
      
      return, attributes
end

pro hubIOShapeFileWriter::_writeEntity, entity, attributes
      shape = self.shape
      shape.PutEntity, entity
      if isa(attributes) then begin
        shape.SetAttributes, self.entityCount, attributes
      endif
      self.entityCount++
end

pro hubIOShapeFileWriter::cleanup
  self.hubIOShapefile::Cleanup
end

;+
; :Hidden:
;-
pro hubIOShapeFileWriter__define
  struct = {hubIOShapeFileWriter $
    ,inherits hubIOShapeFile $
    ,entityCount : 0l $
  }
end

;+
; :Hidden:
;-
pro test_hubIOShapeFileWriter_Polygons
  
  rootDir = '/Users/benjaminjakimow/Documents/SVNCheckouts/Berlin-II/
  pathShape = FILEPATH('testShapeFileWriterPOLYGONS.shp', ROOT_DIR=rootDir,SUBDIRECTORY='shapes' )
  FILE_MKDIR, FILE_DIRNAME(pathShape)
  shape = hubIOShapeFileWriter(pathShape, 'POLYGON')
  shape.AddAttribute, 'name', 'string', 50
  shape.AddAttribute, 'number', 'int', 10, precision=0
  pointValues1 = Hash('name','point1', 'number',99)
  pointValues2 = Hash('name','point2', 'number',-99)
  shape.writeData, [1,2,1],  [1,2,3],  pointValues1
  shape.writeData, [3,2,3],  [3,2,1], pointValues2
  shape.cleanup
  
  print, 'done'
end

;+
; :Hidden:
;-
pro test_hubIOShapeFileWriter_Lines
  
  rootDir = '/Users/benjaminjakimow/Documents/SVNCheckouts/Berlin-II/
  pathShape = FILEPATH('testShapeFileWriterLINE.shp', ROOT_DIR=rootDir,SUBDIRECTORY='shapes' )
  FILE_MKDIR, FILE_DIRNAME(pathShape)
  shape = hubIOShapeFileWriter(pathShape, 'POLYLINE')
  shape.AddAttribute, 'name', 'string', 50
  shape.AddAttribute, 'number', 'int', 10, precision=0
  pointValues1 = Hash('name','point1', 'number',99)
  pointValues2 = Hash('name','point2', 'number',-99)
  shape.writeData, [1,2],  [1,2],  pointValues1
  shape.writeData, [1,2],  [2,1], pointValues2
  shape.cleanup
  
  print, 'done'
end



;+
; :Hidden:
;-
pro test_hubIOShapeFileWriter_Point
  
  rootDir = '/Users/benjaminjakimow/Documents/SVNCheckouts/Berlin-II/
  rootDir = 'D:\Berlin_II\Berlin-II-IDLCode\'
  pathShape = FILEPATH('testShapeFileWriter2.shp', ROOT_DIR=rootDir,SUBDIRECTORY='output' )
  FILE_MKDIR, FILE_DIRNAME(pathShape)
  shape = hubIOShapeFileWriter(pathShape, 'POINT')
  shape.AddAttribute, 'name', 'string', 50
  shape.AddAttribute, 'number', 'int', 10, precision=0
  pointValues1 = Hash('name','point1', 'number',99)
  pointValues2 = Hash('name','point2', 'number',-99)
  
  shape.writeData, 1,  1,  pointValues1
  shape.writeData, 2,  2, pointValues2
  
  
  ;shape._writePoint, 383639.17,  5819981.15,  pointValues1
  ;shape._writePoint, 385435.57,  5819981.15, pointValues2
  shape.cleanup
  
  print, 'done'
end