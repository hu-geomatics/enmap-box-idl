;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    Constuctor of the `hubIOShapeFileReader` object.
;
; :Params:
;    filename: in, required, type = file path
;     The shape file (*.shp) file path 
;
; :Examples:
;
;-
function hubIOShapeFileReader::init, filename, Test=Test, _UPDATE=_UPDATE
  isInit = self.hubIOShapeFile::init(filename)
  
  isOpen = (self.shape).open(filename, UPdate=keyword_set(_UPDATE))
  ;(self.shape).getProperty, is_open=isOpen
  if ~isOpen then begin
    if keyword_set(test) then return, 0
    message, 'Can not read shape file: '+filename
  endif 
  
  self.nextEntityIndex = 0
  shape = self.shape
  shape.getProperty, attribute_info=attribute_info $
                   , attribute_names=attributeNames $
                   , entity_type=shapeType
               
  
  ;check supported types
  case shapeType of
    1 : ;POINT supported 
    3 : ;POLYLINE supported 
    5 : ;POLYGONE supported
    8 :  message, 'MULTIPOINT not supported yet'
   11 :  message, 'POINTZ not supported yet'
   13 :  message, 'POLYLINEZ not supported yet'
   15 :  message, 'POLYGONZ not supported yet'
   18 :  message, 'MULTIPOINTZ not supported yet'
   21 :  message, 'POINTM not supported yet'
   23 :  message, 'POLYLINEM not supported yet'
   25 :  message, 'POLYGONM not supported yet'
   28 :  message, 'MULTIPOINTM not supported yet'
   31 :  message, 'MULTIPATCH not supported yet'
   else : message, 'unknown shape type'
  endcase
  self.shapeType = shapeType
  (self.attributeNames).add, attributeNames, /Extract
  
  ;find projection file and read spatial reference system definition
  bd = FILE_DIRNAME(self.filename)
  bn = FILE_BASENAME(self.filename,'.shp',/FOLD_CASE)
  extensions = '.'+['prj','qpj']
  projectionFile = !NULL
  foreach ext, extensions do begin
    pFile = FILEPATH(bn+ext, ROOT_DIR=bd)
    if FILE_TEST(pFile) then begin
      projectionFile = pFile
      break
    endif
  endforeach
  
  if isa(projectionFile) then begin
    projectionString = ''
    line = ''
    openr, lun, projectionFile, /GET_LUN
    while not eof(lun) do begin
      readf, lun, line
      projectionString += line
    endwhile
    FREE_LUN, lun
    
    projectionString = STRCOMPRESS(projectionString, /REMOVE_ALL)
    if STRLEN(projectionString) gt 0 then self.spatialReferenceSystem = projectionString
  endif
  
  self.initReader, self.getNumberOfEntities()
  return, 1
end


;+
; :Description:
;    Initializes a reading sequence of this shape file
;
; :Params:
;    entitiesPerStep: in, optional, type = integer, default = 100
;      The number of entities to read in one reading step when calling `hubIOShapeFileReader::getData`.
;
;
; :Examples:
;
;-
pro hubIOShapeFileReader::initReader, entitiesPerStep
  
  self.entitiesPerStep = isa(entitiesPerStep)? entitiesPerStep : 100
  self.nextEntityIndex = 0

end

;+
; :Description:
;    Is true when the reading has been finished.
;
;-
function hubIOShapeFileReader::readingDone
  return, self.nextEntityIndex ge self.getNumberOfEntities()
end


function hubIOShapeFileReader::getGeometryData, entityIndices
      ;check supported types
        case self.shapeType of
          1 : geometries = self._getGeometryPoint(entityIndices)
          3 : geometries = self._getGeometryPolyLine(entityIndices);POLYLINE supported 
          5 : geometries = self._getGeometryPolygon(entityIndices)
      ;    8 :  message, 'MULTIPOINT not supported yet'
      ;   11 :  message, 'POINTZ not supported yet'
      ;   13 :  message, 'POLYLINEZ not supported yet'
      ;   15 :  message, 'POLYGONZ not supported yet'
      ;   18 :  message, 'MULTIPOINTS not supported yet'
      ;   21 :  message, 'POINTM not supported yet'
      ;   23 :  message, 'POLYLINE not supported yet'
      ;   25 :  message, 'POLYGONM not supported yet'
      ;   28 :  message, 'MULTIPOINTM not supported yet'
      ;   31 :  message, 'MULTIPATCH not supported yet'
         else : message, 'geometry type of shapefile is not supported'
        endcase
    return, geometries
end

function hubIOShapeFileReader::getAttributeData, entityIndices, asDictionary=asDictionary
  result = (self.shape).getAttributes(entityIndices, All = ~isa(entityIndices))
  if keyword_set(asDictionary) then begin
    l = list()
    
    
  endif
  return, result 
end
;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    entityIndices: in, optional, type = integer[]
;     An array with indices to specifiy the entities to get the data from.
;     
;     Using this keyword will not change internal reading settings.
;      
;
;
;
;-
function hubIOShapeFileReader::getData, entityIndices, asDictionary=asDictionary
  
  
  predefined = isa(entityIndices) 
  
  if predefined then begin
    iEntities = entityIndices
  endif else begin
    nEntities = self.entitiesPerStep < (self.getNumberOfEntities() - self.nextEntityIndex)
    iEntities = indgen(nEntities) + self.nextEntityIndex
  endelse
  
  shape = (self.shape)
  
  geometries = self.getGeometryData(iEntities)
  attributes = self.getAttributeData(iEntities, asDictionary=asDictionary)
  
  attributeNames = self.getAttributeNames()
  result = {geometries:geometries, attributes:attributes $
           , numberOfEntities:n_elements(geometries) $
           , numberOfAttributes:n_elements(attributeNames) $
           , attributeNames:attributeNames }
  
  if ~predefined then begin
    self.nextEntityIndex = iEntities[-1]+1
  endif 
  return, result
end

;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    entityIndices: in, optional, type = integer[]
;     
;
;
;
;-
function hubIOShapeFileReader::_getGeometryPoint, entityIndices
  
  shape = (self.shape)
  
  
  entities = shape.getEntity(entityIndices)
  nEntities = n_elements(entities)
  results = replicate({x:0d, y:0d, type:'point'}, nEntities) 
  
  for i = 0, nEntities -1 do begin
    results[i].x = entities[i].bounds[0]
    results[i].y = entities[i].bounds[1]
  endfor
    
  
  
  return, results
  
end

;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    entityIndices: in, optional, type = integer[]
;     
;
;
;
;-
function hubIOShapeFileReader::_getGeometryPolygon, entityIndices
  
  shape = (self.shape)
  
  
  entities = shape.getEntity(entityIndices)
  nEntities = n_elements(entities)
  
  results = List()
  
  if total(entities[*].n_parts gt 1) ne 0 then begin
    print, 'WARNING: Multi part polygons'
  endif
  
  for i = 0, nEntities -1 do begin
    geometryStruct = {parts:list(), type:'polygon', numberOfParts:0}
    ent = entities[i]
    vertices = (*ent.vertices)
    nVertices = n_elements(vertices[0,*])
    nParts = ent.n_parts
    for iPart = 0, nParts - 1 do begin
      s = (*ent.parts)[iPart]
      e = (iPart lt nParts - 1) ? (*ent.parts)[iPart + 1] - 1 : nVertices - 1
      x = vertices[0,s:e]
      y = vertices[1,s:e]
      (geometryStruct.parts).add, {x:vertices[0,s:e], y:vertices[1,s:e]}
      geometryStruct.numberOfParts++
    endfor
    results.add, geometryStruct
  endfor
    
  
  
  return, results
  
end

function hubIOShapeFileReader::_getGeometryPolyLine, entityIndices

  shape = (self.shape)


  entities = shape.getEntity(entityIndices)
  nEntities = n_elements(entities)

  results = List()

  if total(entities[*].n_parts gt 1) ne 0 then begin
    print, 'WARNING: Multi part polylines'
  endif

  for i = 0, nEntities -1 do begin
    geometryStruct = {parts:list(), type:'polyline', numberOfParts:0}
    ent = entities[i]
    vertices = (*ent.vertices)
    nVertices = n_elements(vertices[0,*])
    nParts = ent.n_parts
    for iPart = 0, nParts - 1 do begin
      s = (*ent.parts)[iPart]
      e = (iPart lt nParts - 1) ? (*ent.parts)[iPart + 1] - 1 : nVertices - 1
      x = vertices[0,s:e]
      y = vertices[1,s:e]
      (geometryStruct.parts).add, {x:vertices[0,s:e], y:vertices[1,s:e]}
      geometryStruct.numberOfParts++
    endfor
    results.add, geometryStruct
  endfor



  return, results

end


;+
; :Hidden:
;-
pro hubIOShapeFileReader__define
  struct = {hubIOShapeFileReader $
    ,inherits hubIOShapefile $
    ,nextEntityIndex:0l $
    ,entitiesPerStep : 0l $
  }
end


;+
; :hidden:
;-
pro test_hubIOShapeFileReader
  path = '/Users/benjaminjakimow/Documents/SVNCheckouts/Berlin-II/testInput/testROI.shp'
  path = 'D:\Berlin_II\Berlin-II-IDLCode\output\testShapeFromASCII_NEW.shp'
  path = 'G:\temp\temp_akpona\4BJ\'
  path = 'G:\temp\temp_akpona\4BJ\Reference_LC_2005_new.shp'
  path = 'G:\temp\temp_bj\TestShpEnMAP-Box\LC_Train_Points.shp'
  shapeReader = hubIOShapeFileReader(path)
 
  print, shapeReader.getBoundingBox()
  data = shapeReader.getData([1,2,3])
  name1 = shapeReader.getAttributeNames()
  shapeReader.initReader, 10
  data = shapeReader.getData()
  print, 'done'

end