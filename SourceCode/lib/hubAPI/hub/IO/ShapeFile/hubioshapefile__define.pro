function hubIOShapeFile::init, filename
   self.filename = filename
   self.shape = IDLffShape()
   self.shapeType = 0
   self.attributeNames = List()
   
   
   return, 1
end

;+
; :Description:
;    Returns the number of entities stored in the shape file.
;-
function hubIOShapeFile::getNumberOfEntities
  (self.shape).getProperty, n_entities = n_entities
  return, n_entities
end

function hubIOShapeFile::getSpatialReferenceSystem
  srs = self.spatialReferenceSystem
  if srs eq '' then srs = !NULL 
  return, srs
  
end


;+
; :Description:
;    Returns the number of attributes used to describe a shape.
;-
function hubIOShapeFile::getNumberOfAttributes
  (self.shape).getProperty, n_attributes = n_attributes
  return, n_attributes
end

;+
; :Description:
;    Returns the path of the shape file
;-
function hubIOShapeFile::getFileName
  ;(self.shape).getProperty, filename = filename
  return, self.filename
end


;+
; :Description:
;    Returns an array of IDL types, each corresponding to a shape fiel column.
;
;
;
; :Keywords:
;    TypeNames
;    TypeCodes
;
; :Examples:
;
;-
function hubIOShapeFile::getAttributeTypes, TypeNames=TypeNames, TypeCodes=TypeCodes
  if ~keyword_Set(TypeNames) && ~keyword_set(TypeCodes) then message, 'Set either keyword ''TypeNames'' or ''TypeCodes''
  (self.shape).getProperty, attribute_info = attr_info, n_attributes=numberOfAttributes
  
  tcodes = make_array(numberOfAttributes, /Integer)
  for i=0, numberOfAttributes - 1 do begin
    tcodes[i] = attr_info[i].type
  endfor
  
  if keyword_set(TypeNames) then return, hubMathHelper.resolveDataTypecode(tcodes)
  if keyword_set(Typecodes) then return, TCodes
  return, tnames
end

function hubIOShapeFile::getBoundingBox
  shape = self.shape
  
  geoHelper = hubGeoHelper()
  blockSize = 100
  eDone = 0
  nEntitiesTotal = self.getNumberOfEntities()
  if nEntitiesTotal eq 0 then return, !NULL
  
  iNextEntity = 0
  
  minX = !NULL
  maxX = !NULL
  minY = !NULL
  maxY = !NULL
  
  while iNextEntity lt nEntitiesTotal do begin
    nEntities = blockSize < (nEntitiesTotal - iNextEntity)  
    iEntities = indgen(nEntities) + iNextEntity
    entities = shape.getEntity(iEntities)
    minX = min([minX, entities[*].bounds[0]])
    maxX = max([maxX, entities[*].bounds[4]])
    minY = min([minY, entities[*].bounds[1]])
    maxY = max([maxY, entities[*].bounds[5]])
    iNextEntity += nEntities
  endwhile
  
  geom = geoHelper.createGeometry([minX,maxX], [minY,maxY])
  return, geoHelper.getBoundingBox(geom)
end


;+
; :Description:
;    Returns the geometric type name of the entities stored in the shape files.
;   
;    Possible values are:
;          Code | Shape Type Name
;          -----+------------------
;            0  | Null Shape
;            1  | Point
;            3  | PolyLine
;            5  | Polygon
;            8  | MultiPoint
;            11 | PointZ
;            13 | PolyLineZ
;            15 | PolygonZ
;            18 | MultiPointZ
;            21 | PointM
;            23 | PolyLineM
;            25 | PolygonM
;            28 | MultiPointM
;            31 | MultiPatch
;          -----+------------------
;          
;    (see http://www.esri.com/library/whitepapers/pdfs/shapefile.pdf);
;    
; :Keywords:
;    typeCode: in, optional, type = boolean
;     Set this to return the type code instead the shape type name.
;
; :Examples:
;
;-
function hubIOShapeFile::getShapeType, typeCode=typeCode
  return, keyword_set(typeCode) ? self.shapeType : self._getShapeTypeString(self.shapeType)
end

;+
; :Description:
;    Returns the attributes / column names used to describe the shape files entities.
;    
;-
function hubIOShapeFile::getAttributeNames
  return, (self.attributeNames).toArray()
end

;+
; :Description:
;    Returns the first tag name index of a specific attribute
;
;-
function hubIOShapeFile::getAttributeIndex, name, fold_case = fold_case
  foreach aname, self.attributeNames, i do begin
    if STRCMP(name, aname, FOLD_CASE=FOLD_CASE) then return, i 
  endforeach
  
  return, !NULL ;name does not exist
end


function hubIOShapeFile::_getShapeTypeCode, shapeTypeString
  
    if typeName(shapeTypeString) ne 'STRING' then begin
      shapeTypeCode = self._getShapeTypeCode(shapeTypeString)
    endif else begin
      case strtrim(STRUPCASE(shapeTypeString),2) of
        'POINT'       : shapeTypeCode = 1
        'POLYLINE'    : shapeTypeCode = 3
        'LINE'        : shapeTypeCode = 3 ;this is not defined in ShapeFile Standard
        'POLYGON'     : shapeTypeCode = 5
        'MULTIPOINT'  : shapeTypeCode = 8
        'POINTZ'      : shapeTypeCode = 11
        'POLYLINEZ'   : shapeTypeCode = 13
        'POLYGONZ'    : shapeTypeCode = 15
        'MULTIPOINTZ' : shapeTypeCode = 18
        'POINTM'      : shapeTypeCode = 21
        'POLYLINEM'   : shapeTypeCode = 23
        'POLYGONM'    : shapeTypeCode = 25
        'MULTIPOINTM' : shapeTypeCode = 28
        'MULTIPATCH'  : shapeTypeCode = 31
        else : message, "Unknown geometry/entity type string'"+ string(shapeTypeString) + "'"
      endcase
    endelse
  return, shapeTypeCode
end

function hubIOShapeFile::_getShapeTypeString, entityCode
    case entityCode of
        1 : entityString = 'POINT'       
        3 : entityString = 'POLYLINE'    
        5 : entityString = 'POLYGON'     
        8 : entityString = 'MULTIPOINT'  
        11: entityString = 'POINTZ'      
        13: entityString = 'POLYLINEZ'   
        15: entityString = 'POLYGONZ'    
        18: entityString = 'MULTIPOINTZ' 
        21: entityString = 'POINTM'      
        23: entityString = 'POLYLINEM'   
        25: entityString = 'POLYGONM'    
        28: entityString = 'MULTIPOINTM' 
        31: entityString = 'MULTIPATCH'  
        else : message, "Unknown geometry/entity type code'"+ string(entityCode) + "'"
    
    endcase
  
  return, entityString
end

;+
; :Description:
;    Print info 
;-
function hubIOShapeFile::_overloadPrint
  info = list()
  info.add, 'Shapefile: '
  info.add, '  Path:     ' + self.filename
  info.add, '  Geometry: ' + self.getShapeType() 
  return, strjoin(info.toArray(), string(13b))
end


function hubIOShapeFile::_getIDLffShapeObj
  return, self.shape
end

pro hubIOShapeFile::cleanup
  (self.shape).close
  (self.shape).cleanup
end
;+
; :Hidden:
;-
pro hubIOShapeFile__define
  struct = {hubIOShapeFile $
    ,inherits IDL_Object $
    ,fileName:'' $
    ,spatialReferenceSystem:'' $
    ,shapeType:0 $
    ,shape:obj_new() $
    ,attributeNames:obj_new() $
  }
end
