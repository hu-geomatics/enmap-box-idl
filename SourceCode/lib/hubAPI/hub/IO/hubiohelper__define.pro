;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-



function hubIOHelper::fileReadable $
  ,fname

  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    return, 0
  endif
 
  return,file_test(fname,/NOEXPAND_PATH)

end

pro hubIOHelper__define
  struct = {hubIOHelper $
    ,inherits IDL_Object $
  }
end