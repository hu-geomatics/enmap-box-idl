;+
;  An hubIOImgSampleSetForRegression object can be used to access regression sample set data.
;
;  For usage details also see `Image Data Input/Output API <./image-data-input-output.html>` and
;  `Accessing Sample Set Data <./sample-set.html>`.
;-  

;+
; :Description:
;    This method is called indirectly when calling the object constructor hubIOImgSampleSetForClassification().
;    The method return value is an object reference to the newly-created object.
;
; :Params:
;    filenameFeatures : in, required, type=string
;      Use this argument to specify the full path name of the feature image data file.
;      The feature image spatial size must match the label image spatial size.
;      
;    filenameLabels : in, required, type=string
;      Use this argument to specify the full path name of the label image data file.
;      The label image must be a regression image (`file type = EnMAP-Box Regression`) and its 
;      spatial size must match the feature image spatial size.
;
;-
function hubIOImgSampleSetForRegression::init $
  , filenameFeatures $
  , filenameLabels

  !null = self->hubIOImgSampleSet::init(filenameFeatures, filenameLabels)
  
  if ~self.imageLabels.isRegression() then begin
    message,'Label image must be a one-band image with specified data ignore value.'
  endif
  
  return,1b

end

;+
; :Hidden:
;-
pro hubIOImgSampleSetForRegression__define
  struct = {hubIOImgSampleSetForRegression $
    , inherits hubIOImgSampleSet $
  }
end

;+
; :Hidden:
;-
pro test_hubIOImgSampleSetForRegression


  filenameFeatures = hub_getTestImage('Hymap_Berlin-B_Image')
  filenameLabels = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')

  sampleSet = hubIOImgSampleSetForRegression(filenameFeatures, filenameLabels)
  indices = sampleSet.getIndices()
  labels = sampleSet.getLabels(indices)
  features = sampleSet.getFeatures(indices)

  help, indices, features
plot, histogram(labels)
return
  for i=0,n_elements(features[0, *])-1 do begin
    plot, features[*, i]
    wait,0.1
  endfor

end