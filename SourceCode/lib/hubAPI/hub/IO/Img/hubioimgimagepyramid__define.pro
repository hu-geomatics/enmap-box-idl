function hubIOImgImagePyramid::init, filename, tileSize
  
  self.filename = filename
  self.directory = hubIOImgImagePyramid_getDirectory(self.filename)

  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    self.directory = filepath('pyramids', ROOT_DIR=hubSettingsLocal_getDirname())
    file_mkdir, self.directory ,/NOEXPAND_PATH
  endif
  file_mkdir, self.directory ,/NOEXPAND_PATH
  catch, /CANCEL

  self.image = hubIOImgInputImage(filename)
  self.tileSize = tileSize
  self.bigFileThreshold = 5000ll^2 ;pixel
  self.buildPyramid = ~self.bigFileThresholdReached()
  self.overviewMaxSize = [300,300] ;pixel
  return, 1b
end

function hubIOImgImagePyramid::getFilename, band, level
  return, filepath('pyramid.band'+strcompress(/REMOVE_ALL, band)+'.level'+strcompress(/REMOVE_ALL, level), ROOT_DIR=self.directory)
end

function hubIOImgImagePyramid::getOverviewFilename, band
  return, filepath('pyramid.band'+strcompress(/REMOVE_ALL, band)+'.overview', ROOT_DIR=self.directory) 
end

function hubIOImgImagePyramid::getStatisticsFilename, band, Approximated=approximated
  basename = 'pyramid.band'+strcompress(/REMOVE_ALL, band)
  basename += keyword_set(approximated) ? '.approximatedStatistics.sav' : '.statistics.sav'
  filename = filepath(basename, ROOT_DIR=self.directory)
  return, filename 
end

function hubIOImgImagePyramid::isWritten, band, Approximated=approximated
  filename = self.getStatisticsFilename(band, Approximated=approximated)
  if file_test(filename) then begin
    pyramidOutdated = (file_info(filename)).mtime lt (file_info(self.filename)).mtime
  endif else begin
    pyramidOutdated = 1b
  endelse
  return, ~pyramidOutdated
end

pro hubIOImgImagePyramid::write, band

  pyramidOutdated = 1

  ; calculate approximation and return, if pyramid should not be build
  if ~self.buildPyramid then begin
    if ~self.isWritten(band, /Approximated) then begin
      self.writeApproximatedStatistics, band, Outdated=pyramidOutdated
      self.writeOverview, band
    endif
    return
  endif

  ; return, if pyramid already exists
  if self.isWritten(band) then begin
    return
  endif

  ; calculate number of pyramid levels
  imageSize = self.image.getSpatialSize()
  nxtiles = ceil(1.*imageSize[0]/self.tileSize[0])
  nytiles = ceil(1.*imageSize[1]/self.tileSize[1])
  nxlevels = ceil(alog(nxtiles)/alog(2))+1
  nylevels = ceil(alog(nytiles)/alog(2))+1
  numberOfLevels = max([nxlevels,nylevels])

  ; init reader
  numberOfTileLines = self.tileSize[1]
  self.image.initReader, numberOfTileLines, /Band, /TileProcessing, SubsetBandPositions=band

  ; init writer
  samples = imageSize[0]
  lines = imageSize[1]
  outputImages = list()
  for level=1,numberOfLevels-1 do begin ;Note: level 0 is not written!
    samples = ceil(samples/2.)
    lines = ceil(lines/2.)
    writerSettings = self.image.getWriterSettings(SetSamples=samples, SetLines=lines)
    outputImage = hubIOImgOutputImage(self.getFilename(band, level), /NoOpen)
    outputImage.initWriter, writerSettings
    outputImages.add, outputImage
  endfor

  ; write data and calculate data min/max
  dataIgnoreValue = self.image.getMeta('data ignore value')
  incRange = hubMathIncRange()
  while ~self.image.tileProcessingDone() do begin
    data = self.image.getData()

    if isa(dataIgnoreValue) then begin
      incRange.addData, data[where(data ne dataIgnoreValue and finite(data), /NULL)]
    endif else begin
      incRange.addData, data[where(finite(data), /NULL)]
    endelse

    foreach outputImage,outputImages do begin
      data = data[0:-1:2, 0:-1:2]
      outputImage.writeData, data
    endforeach
  endwhile
  self.image.finishReader
  range = incRange.getResult()
  if ~finite(range[0]) then begin
    if isa(dataIgnoreValue) then begin
      range = [dataIgnoreValue,dataIgnoreValue]
    endif else begin
      range = [0,0]
    endelse
  endif
  foreach outputImage, outputImages do begin
    obj_destroy, outputImage
  endforeach


  self.writeStatistics, band, range
  if ~file_test(self.getOverviewFilename(band)) then begin
    self.writeOverview, band
  endif

end


pro hubIOImgImagePyramid::writeStatistics, band, range
  
  ; calculate percentiles 
  incHistogram = hubMathIncHistogram(range)
  numberOfTileLines = self.tileSize[1]
  self.image.initReader, numberOfTileLines, /TileProcessing, /Value, SubsetBandPositions=band
  while ~self.image.tileProcessingDone() do begin
    data = self.image.getData()
    incHistogram.addData, data
  endwhile
  self.image.finishReader
  percentileRanks = indgen(101)
  percentiles = incHistogram.getPercentiles(percentileRanks)
  histogram = incHistogram.getResult()

  ; write band statistics
  file_mkdir, self.directory
  save, percentiles, histogram, FILENAME=self.getStatisticsFilename(band)

end

pro hubIOImgImagePyramid::writeApproximatedStatistics, band, Outdated=pyramidOutdated
  
  ; return, if approximated stats file for the band already exist and is not outdated  
  statsFileExists = file_test(self.getStatisticsFilename(band, /Approximated))
  if statsFileExists then begin
    pyramidOutdated = (file_info(self->getStatisticsFilename(band, /Approximated))).mtime lt (file_info(self.filename)).mtime
  endif else begin
    pyramidOutdated = 1b
  endelse
  if ~pyramidOutdated then return
  
  ; draw 1000 random values
  indices = randomu(/LONG, seed, 1000) mod product(self.image.getSpatialSize())
  self.image.initReader, /Profiles, SubsetBandPositions=band
  values = self.image.getData(indices)
  self.image.finishReader
  if self.image.hasMeta('data ignore value') then begin
    dataIgnoreValue = self.image.getMeta('data ignore value')
    values = values[where(/NULL,values ne dataIgnoreValue)]
  endif
  
  if isa(values) then begin
    histogram = hubMathHistogram(values, hubRange(values), InPercentileRanks=indgen(101), OutPercentiles=percentiles)
  endif else begin
    percentiles = findgen(101)*2.55
    histogram = intarr(101)+1
  endelse

  ; write band statistics
  file_mkdir, self.directory
  save, percentiles, histogram, FILENAME=self.getStatisticsFilename(band, /Approximated)

end

pro hubIOImgImagePyramid::writeOverview, band
  
  ; calculate requiered lines and samples
  imageSize = self.getImageSize()
  overviewSize = self.getOverviewSize()
  samples = overviewSize[0]
  lines = overviewSize[1]
  sampleIndices = congrid(lindgen(imageSize[0]), samples)
  lineIndices   = congrid(lindgen(imageSize[1]), lines)

  ; read/write data
  self.image.initReader, /Slice, SubsetBandPositions=band 
  writerSettings = self.image.getWriterSettings(SetSamples=samples, SetLines=lines)
  outputImage = hubIOImgOutputImage(self.getOverviewFilename(band), /NoOpen)
  outputImage.initWriter, writerSettings
  foreach lineIndex, lineIndices do begin
    data = self.image.getData(lineIndex)
    data = data[sampleIndices]
    outputImage.writeData, data
  endforeach
  self.image.finishReader
  obj_destroy, outputImage
end

function hubIOImgImagePyramid::getImageSize
  return, self.image.getSpatialSize()
end

function hubIOImgImagePyramid::getOverviewSize
  step = self.getOverviewResamplingStep()
  imageSize = self.getImageSize()
  samples = round(imageSize[0]/step)
  lines = round(imageSize[1]/step)
  return, [samples,lines] > 1
end

function hubIOImgImagePyramid::getOverviewResamplingStep
  return, max(self.getImageSize()/float(self.overviewMaxSize)) > 1
end

function hubIOImgImagePyramid::read, band, level, lineRange, sampleRange

  widget_control, /HOURGLASS
  if level eq 0 then begin
    filename = self.filename
    bandPositions = band
  endif else begin
    filename = self.getFilename(band, level)
    bandPositions = 0
  endelse
  image = hubIOImgInputImage(filename)
  image.initReader, /Band, SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetBandPositions=bandPositions
  data = image.getData(0)
  image.finishReader
  return, data
end

function hubIOImgImagePyramid::readOverview, band
  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    self.delete
    message, 'Error occured while reading image pyramid. Pyramid will be deleted and recalculated.'
  endif

  imageBand = hubIOImg_readBand(self.getOverviewFilename(band), 0)
  return, imageBand
end

pro hubIOImgImagePyramid::delete
  file_delete, self.directory, /RECURSIVE, /ALLOW_NONEXISTENT, /VERBOSE
end

function hubIOImgImagePyramid::getPercentiles, band, Histogram=histogram
  if file_test(self.getStatisticsFilename(band)) then begin
    restore, self.getStatisticsFilename(band)
  endif else begin
    restore, self.getStatisticsFilename(band, /Approximated)
  endelse
  return, percentiles
end

function hubIOImgImagePyramid::bigFileThresholdReached
  return, product(self.image.getSpatialSize()) gt self.bigFileThreshold
end

pro hubIOImgImagePyramid::getProperty, BuildPyramid=buildPyramid, BigFileThreshold=bigFileThreshold
  if arg_present(buildPyramid) then buildPyramid = self.buildPyramid
  if arg_present(bigFileThreshold) then bigFileThreshold = self.bigFileThreshold
end

pro hubIOImgImagePyramid::setProperty, BuildPyramid=buildPyramid
  self.buildPyramid = buildPyramid
end

pro hubIOImgImagePyramid__define
  define = {hubIOImgImagePyramid, inherits IDL_OBJECT, $
    filename:'',$
    directory:'',$
    image:obj_new(),$
    tileSize:[0l,0l],$
    bigFileThreshold:0ll,$
    buildPyramid:0b,$
    overviewMaxSize:[0l,0l], $
    overviewSize:[0l,0l] $
    }
end

pro test_hubIOImgImagePyramid
  filename = hub_getTestImage('Spot_Berlin')
  filename = hub_getTestImage('Hymap_Berlin-A_Image')
  filename = hub_getTestImage('AF_Image')
  tileSize = [64,64]
  pyramid = hubIOImgImagePyramid(filename, tileSize)
  pyramid.delete
 
  pyramid.write, 1, Outdated=outdated, Overwrite=0
  if ~keyword_set(outdated) then print, 'Pyramid already exist.'
;  !null=image(pyramid.read(1, 1), /ORDER)
  ;tvscl, pyramid.readOverview(1)

  
end