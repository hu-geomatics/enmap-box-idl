function hubIOImgImagePyramid_getDirectory, filename, Exists=exists
  directory = filepath('EnmapBoxPyramid.'+file_basename(filename), ROOT_DIR=file_dirname(filename))
  exists = file_test(directory)
  return, directory
end
