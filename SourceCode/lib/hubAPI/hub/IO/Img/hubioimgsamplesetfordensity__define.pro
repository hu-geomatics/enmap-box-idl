;+
;  An hubIOImgSampleSetForDensity object can be used to access density estimation sample set data.
;
;  For usage details also see `Image Data Input/Output API <./image-data-input-output.html>` and
;  `Accessing Sample Set Data <./sample-set.html>`.
;-  

;+
; :Description:
;    This method is called indirectly when calling the object constructor hubIOImgSampleSetForDensity().
;    The method return value is an object reference to the newly-created object.
;
; :Params:
;    filenameFeatures : in, required, type=string
;      Use this argument to specify the full path name of the feature image data file.
;      The feature image spatial size must match the label image spatial size.
;      
;    filenameLabels : in, required, type=string
;      Use this argument to specify the full path name of the label image data file.
;      The label image must be a single band image and its 
;      spatial size must match the feature image spatial size.
;      
;    presenceValues : in, optional, type=number[]
;      Use this argument to specify an array of values defining the occurrence pixel locations.
;      All values different from those values are treated as background. When this argument is
;      not specified, the background is defined by the label image `data ignore value`, 
;      or if not present, by zero. 
;
;-
function hubIOImgSampleSetForDensity::init $
  , filenameFeatures $
  , filenameLabels $
  , presenceValues

  !null = self->hubIOImgSampleSet::init(filenameFeatures, filenameLabels)

  ; default absenceValues is label image 'data ignore value' or 0

  self.presenceValues = list()
  if isa(presenceValues) then begin
    self.presenceValues.add, presenceValues, /EXTRACT
  endif
  
  return,1b

end

;+
; :Hidden:
;-
function hubIOImgSampleSetForDensity::_filterIndices $
  , labels

  if n_elements(self.presenceValues) eq 0 then begin
  
    filteredIndices = self->hubIOImgSampleSet::_filterIndices(labels)
  
  endif else begin
  
    ; exclude non presence values
    
    labelIsEqualToPresenceValue = bytarr(size(labels, /Dimensions))
    foreach presenceValue, self.presenceValues do begin
      labelIsEqualToPresenceValue or= labels eq presenceValue
    endforeach
    filteredIndices = where(/NULL, labelIsEqualToPresenceValue)
  
  endelse

  return, filteredIndices 

end

;+
; :Hidden:
;-
pro hubIOImgSampleSetForDensity__define
  struct = {hubIOImgSampleSetForDensity $
    , inherits hubIOImgSampleSet $
    , presenceValues : list() $
  }
end

;+
; :Hidden:
;-
pro test_hubIOImgSampleSetForDensity


  filenameFeatures = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  presenceValues = [1,2,5]
  sampleSet = hubIOImgSampleSetForDensity(filenameFeatures, filenameLabels, presenceValues)
  indices = sampleSet.getIndices()
  labels = sampleSet.getLabels(indices)

  print, labels
  help, labels

end