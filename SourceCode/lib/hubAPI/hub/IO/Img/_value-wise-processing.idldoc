.. title:: Value-Wise Tile Processing Routines

In a value-wise tile processing routine, data reading, processing and writing is performed on image value tiles:

.. image:: _IDLDOC/tileValue.gif

Note that the values are ordered differently in dependence to the image interleave format (BSQ, BIP or BIL).
It is crucial to understand, that there is never a need for knowing the specific order.

Note that you can always replace a value-wise processing routine by a 
spatial, spectral or combined spatial-spectral processing routine.
The key advantage of a value-wise processing routine is that image value tiles
are read in native image interleave format, which is faster than, for example,
reading image band tiles from an image with BIL or BIP interleave, 
or reading image slice tiles from an image with BSQ interleave.

Reader Initialization
---------------------

Call the `hubIOImgInputImage::initReader` method in conjunction with the 
`TileProcessing <./hubIOImgInputImage__define.html#hubIOImgInputImage::initReader:k:TileProcessing>`
and
`Value <./hubIOImgInputImage__define.html#hubIOImgInputImage::initReader:k:Value>`
keywords. 
Use the  
`numberOfTileLines <./hubIOImgInputImage__define.html#hubIOImgInputImage::initReader:p:numberOfTileLines>`
argument to specify the number of image lines included in each tile.

Value-Wise IDL Functions and Operators
--------------------------------------

Unary Operators: negation (-), increment (++), decrement (--)

Basic Arithmetic: addition (+), subtraction (-), multiplication (*), division (/)

Trigonometric Functions: sin(x), cos(x), tan(x), asin(x), acos(x), atan(x), sinh(x), cosh(x), tanh(x)

Relational and Logical Operators: LT, LE, EQ, NE, GE, GT, AND, OR, NOT, XOR, maximum (>), minimum (<)

Other Math Functions: exponent (^), alog(x), alog10(x), round(x), ceil(x), sqrt(x), abs(x)

...

Examples
--------

Inverting Image Values
~~~~~~~~~~~~~~~~~~~~~~

Invert all image values::

   ; create objects

  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  inputImage = hubIOImgInputImage(inputFilename)
  outputFilename = filepath('my_image', /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)
 
  ; copy some metadata
 
  outputImage.copyMeta, inputImage, ['wavelength', 'wavelength units']

  ; initialize for reading/writing image value tiles

  numberOfTileLines = 50
  inputImage.initReader, numberOfTileLines, /Value, /TileProcessing
  outputImage.initWriter, inputImage.getWriterSettings()
 
  ; perform processing

  & while ~inputImage.tileProcessingDone() do begin  $
  &   valueData = inputImage.getData()               $
  &   valueInverted = -valueData                     $
  &   outputImage.writeData, valueInverted           $
  & endwhile

  ; cleanup
 
  inputImage.cleanup
  outputImage.cleanup
  
  ; show result
 
  (hubIOImgInputImage(outputFilename)).quickLook, /ColoredInfrared                                    

.. image:: _IDLDOC/valueWiseTile.gif

Recode Classification Image Class Labels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Recode a classification image with classes 1 - vegetation, 2 - built-up, 3 - impervious, 4 - pervious and 5 - water. 
The classes 1 and 4 will be merged into the new class 1 - sealed (in yellow), 
the classes 2 and 3 will be merged into the new class 2 - unsealed (in green),
and the class 5 will be assigned as 0 - unclassified (in black):: 

  ; define recoding information
  ; - map original class labels 0, 1, 2, 3, 4, 5
  ;         to new class labels 0, 1, 2, 2, 1, 0 
 
   recodingVector = [0, 2, 1, 1, 2, 0]

   ; create objects

  inputFilename = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth')
  inputImage = hubIOImgInputImage(inputFilename)
  outputFilename = filepath('my_image', /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)

  ; setup metadata for recoded classification image

  outputImage.setMeta, 'file type', 'envi classification'
  outputImage.setMeta, 'classes', 3
  outputImage.setMeta, 'class names',  ['Unclassified', 'sealed',   'unsealed' ]
  outputImage.setMeta, 'class lookup', [0, 0, 0,        255, 255, 0,  255, 0, 0]

  ; initialize for reading/writing image value tiles

  numberOfTileLines = 50
  inputImage.initReader, numberOfTileLines, /Value, /TileProcessing
  outputImage.initWriter, inputImage.getWriterSettings()
 
  ; perform processing

  & while ~inputImage.tileProcessingDone() do begin  $
  &   valueData = inputImage.getData()               $
  &   valueRecoded = recodingVector[valueData]       $
  &   outputImage.writeData, valueRecoded           $
  & endwhile

  ; cleanup
 
  inputImage.cleanup
  outputImage.cleanup
  
  ; compare original and result images
 
  (hubIOImgInputImage(inputFilename)).quickLook                                 
  (hubIOImgInputImage(outputFilename)).quickLook

.. image:: _IDLDOC/recodeClassification.gif
