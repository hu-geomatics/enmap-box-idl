;+
;  An hubIOImgOutputImage object can be used to create new image files. 
;  
;  For usage details also see 
;  `Image Data Input/Output API <./image-data-input-output.html>` and
;  `Creating Output Images <./output-images.html>`.
;-

;+
; :Description:
;    This method is called indirectly when calling the object constructor hubIOImgOutputImage().
;    The method return value is an object reference to the newly-created object.
;    
; :Params:
;    filename : in, required, type=string
;      Use this argument to specify the full path name of the output image data file.
; 
; :Keywords:
;    NoOpen : in, optional, type=boolean, default=0
;      Set this keyword to not open the output image inside the EnMAP-Box, after calling the `hubIOImgOutputImage::finishWriter` method.
;      
; :Examples:
;    Creating an output image::
;    
;      outputFilename = filepath('my_image', /TMP)
;      outputImage = hubIOImgOutputImage(outputFilename)
;      help, outputImage
;      print, outputImage
;      outputImage.cleanup
;      
;    IDL prints::
;    
;      OUTPUTIMAGE     OBJREF    = <ObjHeapVar250530(hubIOImgOutputImage)>
;      filename header: C:\Users\janzandr\AppData\Local\Temp\my_image.hdr
;      filename data: C:\Users\janzandr\AppData\Local\Temp\my_image
;    
;-
function hubIOImgOutputImage::init $
  , filename $
  , NoOpen=noOpen
    
  on_error,2

  !null = self->hubIOImgImage::init(filename)
  self.header = hubIOImgHeader()
  self.header.setRequiredDefaults
  ;self.filenameHeader = self.filenameData+'.hdr'
  self.filenameHeader = self._createfilenameHeader(self.filenameData)
  self.header.setMeta, 'filename data', self.filenameData
  self.header.setMeta, 'filename header', self.filenameHeader
  self.openFile = ~keyword_set(noOpen)
  return,1b

end

function hubIOImgOutputImage::_createfilenameHeader, filenameData
  
  dn = file_dirname(filenameData, /MARK_DIRECTORY)
  bn = file_basename(filenameData)

  if stregex(bn, '\.(bsq|bil|bip)$', /FOLD_CASE, /BOOLEAN) then begin
    bn = STRMID(bn, 0, strlen(bn)-4)
  endif
  return, dn + bn + '.hdr'
end

;+
; :Description:
;    This method returns 1 (true) if the image metadata includes all metadata specified
;    by the `metaNames` argument, 0 (false) otherwise. 
;
; :Params:
;    metaNames : in, required, type=string[]
;      Use this argument to specify the metadata for the test.
;
;-
function hubIOImgOutputImage::hasMeta $
  , metaNames

  return, self.hubIOImgImage::hasMeta(metaNames)

end

;+
; :Description:
;    This method returns a single metadata value that is specified by the `metaName` argument. 
;    If the metadata is not available !NULL is returned.
;    
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
; 
; :Params:
;    metaName : in, required, type=string
;      Use this keyword to specify the metadata name.
;      
; :Examples:
;    See the example section of `hubIOImgInputImage::getMeta`.
;
function hubIOImgOutputImage::getMeta $
  ,metaName 

  return, self.hubIOImgImage::getMeta(metaName)

end

;+
; :Description:
;    This method is used to set a single metadata value of the output image.
;    Note that you can not set metadata after initializing the data writer (see `hubIOImgOutputImage::initWriter`).
;    
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
;
; :Params:
;    metaName : in, required, type=string
;      Use this argument to specify the metadata name.
;      
;    metaValue : in, required, type=depends on the metadata 
;      Use this argument to specify the metadata value.
;
; :Examples:
;    Set some metadata of the output image::
;
;      outputFilename = filepath('my_image', /TMP)
;      outputImage = hubIOImgOutputImage(outputFilename)
;      outputImage.setMeta, 'samples', 100
;      outputImage.setMeta, 'lines', 100
;      outputImage.setMeta, 'bands', 3
;      print, outputImage
;      outputImage.cleanup
;      
;    IDL prints::
;    
;      filename data: C:\Users\janzandr\AppData\Local\Temp\my_image
;      filename header: C:\Users\janzandr\AppData\Local\Temp\my_image.hdr
;      samples:      100
;      lines:      100
;      bands:        3
;      
;-
pro hubIOImgOutputImage::setMeta $
  ,metaName $
  ,metaValue
  
;  if self.writerInitialized then begin
;    message, 'Can not set metadata after calling hubIOImgOutputImage::initWriter.'
;  endif
  self.header.setMeta, metaName, metaValue 
end

;+
; :Description:
;    This method is used to copy metadata values from another image and set it to the output image.
;    Note that you can not copy metadata after initializing the data writer (see `hubIOImgOutputImage::initWriter`).
;    
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
;
; :Params:
;    image : in, required, type={hubIOImgInputImage | hubIOImgOutputImage}
;      Use this argument to specify the source image from which the metadata is copied.
;    
;    metaNames : in, required, type=string[]
;      Use this argument to specify an array of metadata name, to be copied. 
;      
; :Keywords:
;    CopyFileType : in, optional, type=boolean, default=0
;      Set this keyword to additionally copy` file type `metadata.
;      
;    CopyLabelInformation : in, optional, type=boolean, default=0
;      Set this keyword to additionally copy` classes`,` class lookup`,` class names `and` data ignore value `metadata.
;      
;    CopySpatialInformation : in, optional, type=boolean, default=0
;      Set this keyword to additionally copy` samples`,` lines `and` map info `metadata.
;      
;    CopySpectralInformation : in, optional, type=boolean, default=0
;      Set this keyword to additionally copy` bands`,` band names`,` data gain values`, 
;     ` data offset values`,` default bands`,` wavelength units`,` wavelength `and` fwhm `metadata.
;-
pro hubIOImgOutputImage::copyMeta $
  , image $
  , metaNames $
  , CopyFileType=copyFileType $
  , CopyLabelInformation=copyLabelInformation $
  , CopySpatialInformation=copySpatialInformation $
  , CopySpectralInformation=copySpectralInformation

  on_error,2
  
;  if self.writerInitialized then begin
;    message, 'Can not set metadata after calling hubIOImgOutputImage::initWriter.'
;  endif
  
  self.header.copyMeta, image.getHeader(), metaNames $
    , CopyFileType=copyFileType $
    , CopyLabelInformation=copyLabelInformation $
    , CopySpatialInformation=copySpatialInformation $
    , CopySpectralInformation=copySpectralInformation

end


;+
; :Description:
;    This method is used to initialize writing of image data or image tile data to a newly-created image file. 
;    All information required for initialization must be specified in the `writerSettings` argument of this method.
;    
;    Note that this method creates two new files: 
;    the image data file, whose filename was specified during object construction (see `hubIOImgOutputImage::init`),
;    and the image header file, whose filename is equal to the image data filename plus a '.hdr' extension.
;    
;    Note that this method opens the image data file and allocates a free IDL Logical Unit Number (LUN) for further writing operations.
;    After finishing all writing operations, it is strongly recommended to call the `hubIOImgOutputImage::finishWriter` method to free the LUN.
;    Alternatively, the `hubIOImgOutputImage::cleanup` method can be called, which implizitly calls the `hubIOImgOutputImage::finishWriter` method.
;
; :Params:
;    writerSettings : in, required, type=hash
;      Use this argument to specify all information required for initialization of the writer.
;      In general, an output image is the result of processing an input image, 
;      and therefor, it is almost always the simplest way to directly pass the result of the
;      input image `hubIOImgInputImage::getWriterSettings` method to this argument.
;      For detailed information on this argument also see `hubIOImgInputImage::getWriterSettings`.
;
;-
pro hubIOImgOutputImage::initWriter $
  , writerSettings

  keys = ['samples', 'bands', 'dataFormat', 'lines', 'data type']
  if isa(writerSettings, 'HASH') then begin
    correctWriterSettings = product(writerSettings.hasKey(keys)) eq 1
  endif else begin
    correctWriterSettings = 0
  endelse
  if ~correctWriterSettings then begin
    message, 'Argument writerSettings must be a hash variable with hash keys: '+strjoin(keys,', ')+'.'
  endif
  
  if ~writerSettings.hasKey('tileProcessing') then begin
    writerSettings['tileProcessing'] = 0
  endif

  if (writerSettings['dataFormat'] eq 'value') and ~writerSettings.hasKey('interleave') then begin
    message, "If writerSettings['dataFormat'] is equal to 'value', then 'interleave' key must be defined."
  endif

  self.writerSettings = writerSettings[*]

  if (self.writerSettings)['tileProcessing'] then begin
    self._initTileWriter
  endif else begin
    self._initNonTileWriter
  endelse

  ; set metadata
  
  self.setMeta, 'samples', (self.writerSettings)['samples']
  self.setMeta, 'lines', (self.writerSettings)['lines']
  self.setMeta, 'bands', (self.writerSettings)['bands']
  self.setMeta, 'data type', (self.writerSettings)['data type']
  
  
  compress = self.writerSettings.hubGetValue('file compression', default=0b)
  if compress  then self.setMeta, 'file compression', 1b
  
  ; cleanup existing files saved on the same location
  hubProEnvHelper.deleteImage, self.filenameData  
  
  ; open file for write access and save some information for faster binary data input
  if ~file_test(file_dirname(self.filenameData), /DIRECTORY) then begin
    file_mkdir, file_dirname(self.filenameData) ,/NOEXPAND_PATH
  endif 
  openw,logicalUnitNumber,self.filenameData,/GET_LUN, COMPRESS=compress
  self.logicalUnitNumber = logicalUnitNumber

  self.binaryIO_type = self.getMeta('data type')
  typeInfo = hubHelper.getTypeInfo(self.binaryIO_type)
  self.binaryIO_typeSize = typeInfo.size

  self.writerInitialized = 1b
  
  if writerSettings['dataFormat'] eq 'profiles' then begin
    self._writeBackgroundImage
  endif

end

;+
; :Description:
;    This method is used to finish writing of image data or image tile data.
;    Call this method after finishing all writing operations to free the LUN that was allocated by the `hubIOImgOutputImage::initWriter` methode.
;    It is also called implicitly when destroying the object via the `hubIOImgOutputImage::cleanup` method.
;-
pro hubIOImgOutputImage::finishWriter

  if self.writerInitialized then begin
    free_lun, self.logicalUnitNumber
    self.header.convertSpeclibBackward
    self.header.writeFile, self.filenameHeader
    self.fileCreated = 1b
  endif else begin
    self.fileCreated = 0b
  endelse

  if self.openFile and self.fileCreated  then begin
    hubProEnvHelper.openImage, self.filenameData
  endif
  
  self.writerInitialized = 0b

end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_initNonTileWriter

  case (self.writerSettings)['dataFormat'] of
    'band'  : interleave = 'bsq'
    'slice' : interleave = 'bip'
    'cube'  : interleave = 'bsq'
    'profiles' : interleave = 'bip'
  endcase
  self.setMeta, 'interleave', interleave

end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_initTileWriter
 
  ; save some writer settings
  neighborhoodHeight = self.writerSettings.hubGetValue('neighborhoodHeight', Default=1)
  (self.writerSettings)['spatialMargin'] = floor(neighborhoodHeight/2)
  
  if (self.writerSettings)['dataFormat'] eq 'band' then begin
    (self.writerSettings)['numberOfTilesPerBand'] = (self.writerSettings)['numberOfTiles'] / (self.writerSettings)['bands']
  endif; else begin
;    (self.writerSettings)['numberOfTilesPerBand'] = (self.writerSettings)['numberOfTiles']
;  endelse

  
  (self.writerSettings)['currentTile'] = 0ll
  
  ; set interleave
  
  case (self.writerSettings)['dataFormat'] of
    'band' : interleave = 'bsq'
    'slice' : interleave = 'bip'
    'cube' : interleave = 'bil'
    'value' : interleave = (self.writerSettings)['interleave']
  endcase
  self.setMeta, 'interleave', interleave
  
end

;+
; :Description:
;    This method is used to write image data or image tile data. 
;    In dependence to the writer initialization (see `hubIOImgOutputImage::initWriter`),
;    different types of data formats must be passed to it.
;
; :Params:
;    data : in, required, type=any
;    
;    indices : in, required, type=any
;
;-
pro hubIOImgOutputImage::writeData $
  , data $
  , indices

  if ~self.writerInitialized then begin
    message, 'Can not write data before calling hubIOImgOutputImage::initWriter
  endif

  if ~isa(data) then begin
    message, 'Missing argument: data.'
  endif

  if ((self.writerSettings)['dataFormat'] eq 'profiles') and ~isa(indices) then begin
    message, 'Missing argument: indices.'
  endif

  ; write data

  if (self.writerSettings)['tileProcessing'] then begin
    self._writeTileData, data
  endif else begin
  
    if (self.writerSettings)['dataFormat'] eq 'profiles' then begin
      self._writeProfiles, data, indices
    endif else begin
      self._writeBinary, data
    endelse
  endelse

end

pro hubIOImgOutputImage::writeImage, data, NoOpen=NoOpen
  meta = self.getMeta()
  hubIOImg_writeImage, data, self.getMeta('filename data'), MetaNames=meta.keys(), MetaValues=meta.values(), NoOpen=NoOpen
end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeTileData $
  , tileData
  
  case (self.writerSettings)['dataFormat'] of
    'band'  : self._writeBandTile, tileData
    'slice' : self._writeSliceTile, tileData
    'cube'  : self._writeCubeTile, tileData
    'value' : self._writeValueTile, tileData
  end
  
end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeBandTile $
  , tileData

  bandTile = tileData

  ; clip spatial margin from band tile
  
  spatialMargin = (self.writerSettings)['spatialMargin']
  if spatialMargin ge 1 then begin
    currentTile = (self.writerSettings)['currentTile']
    numberOfTilesPerBand = (self.writerSettings)['numberOfTilesPerBand']

    isFirstTileOfBand = ( currentTile mod numberOfTilesPerBand ) eq 0
    isLastTileOfBand = ( (currentTile+1) mod numberOfTilesPerBand ) eq 0
    isFirstAndLastTileOfBand = isFirstTileOfBand and isLastTileOfBand

    tileType = 'middle'
    if isFirstTileOfBand then tileType = 'first'
    if isLastTileOfBand then tileType = 'last'
    if isFirstTileOfBand and isLastTileOfBand then tileType = 'firstAndLast'
       
    case tileType of
      'firstAndLast' : ; do nothing
      'first' : bandTile = bandTile[*, 0:-1-spatialMargin]
      'last' : bandTile = bandTile[*, spatialMargin:-1]
      'middle' : bandTile = bandTile[*, spatialMargin:-1-spatialMargin]
    endcase
  endif
  
  ; write binary data
  
  self._writeBinary, bandTile
    
end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeSliceTile $
  , tileData

  self._writeBinary, tileData
    
end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeCubeTile $
  , tileData

  cubeTile = tileData
  
  ; clip spatial margin from cube tile

  spatialMargin = (self.writerSettings)['spatialMargin']
  if spatialMargin ge 1 then begin

    isFirstTile = (self.writerSettings)['currentTile'] eq 0
    isLastTile = (self.writerSettings)['currentTile']+1 eq (self.writerSettings)['numberOfTiles']
    isFirstAndLastTile = isFirstTile and isLastTile

    tileType = 'middle'
    if isFirstTile then tileType = 'first'
    if isLastTile then tileType = 'last'
    if isFirstTile and isLastTile then tileType = 'firstAndLast'

    case tileType of
      'firstAndLast' : ; do nothing
      'first' : cubeTile = cubeTile[*, 0:-1-spatialMargin, *]
      'last' : cubeTile = cubeTile[*, spatialMargin:-1, *]
      'middle' : cubeTile = cubeTile[*, spatialMargin:-1-spatialMargin, *]
    endcase
  endif

  ; repair dimensions
  cubeTileSize = size(/DIMENSIONS, cubeTile)
  case n_elements(cubeTileSize) of 
    3 : ;do nothing
    2 : !null = reform(/OVERWRITE, cubeTile, [cubeTileSize, 1])
    1 : !null = reform(/OVERWRITE, cubeTile, [cubeTileSize, 1, 1])
    else : message, 'Wrong data dimension.'
  endcase 
    
  ; transpose to BIL
  cubeTile = transpose(cubeTile, [0, 2, 1])
  
  ; write binary data
  
  self._writeBinary, cubeTile
    
end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeValueTile $
  , tileData
  
  self._writeBinary, tileData
    
end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeProfiles $
  , data $
  , indices

  offsetFactor = 1ll*(self.writerSettings)['bands']*self.binaryIO_typeSize

  foreach index, indices, i do begin
  
    ; jump to address
    
    offset = offsetFactor*index
    point_lun, self.logicalunitnumber, offset
    
    ; write profile
    
    self._writeBinary, data[*, i]
  
  endforeach

end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeBackgroundImage

  if isa(self.getMeta('data ignore value')) then begin
    backgroundValue = self.getMeta('data ignore value')
  endif else begin
    backgroundValue = 0
  endelse
  backgroundData = make_array((self.writerSettings)['samples']*(self.writerSettings)['bands'], VALUE=backgroundValue, TYPE=self.binaryIO_type)

  ;write to file

  for line=1,(self.writerSettings)['lines'] do begin
    writeu, self.logicalUnitNumber, backgroundData
  endfor

end

;+
; :Hidden:
;-
pro hubIOImgOutputImage::_writeBinary $
  ,data

  ; cast to output data type

  data = hubMathHelper.convertData(data, self.binaryIO_type, /NoCopy)

  ;write to file
  
  writeu, self.logicalUnitNumber, data

end

;+
; :Description:
;    This method is used to performs all cleanup on the object and calls the `hubIOImgOutputImage::finishWriter` method.
;
;-
pro hubIOImgOutputImage::cleanup

  self.finishWriter

end

;+
; :Hidden:
;-
pro hubIOImgOutputImage__define
  struct = {hubIOImgOutputImage $
    , inherits hubIOImgImage $
    , binaryIO_type : 0 $
    , binaryIO_typeSize : 0 $
    , writerSettings : hash() $
    , writerInitialized : 0b $
    , openFile : 0b $
    , fileCreated : 0b $
  }
  
end

pro test_hubIOImgOutputImage

  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  inputImage = hubIOImgInputImage(inputFilename)
  outputFilename = filepath('my_image.bil', /TMP)
  
  outputImage = hubIOImgOutputImage(outputFilename)
  
  print, outputImage
  return
  outputImage.copyMeta, inputImage, /CopySpectralInformation

  inputImage.initReader, /Cube
  writerSettings = inputImage.getWriterSettings()
  writerSettings['file compression'] = 1b
  outputImage.initWriter, writerSettings

  data = inputImage.getData()
  outputImage.writeData, data
  outputImage.writeData, data
  
  outputImage.setMeta, 'lines', inputImage.getMeta('lines')*2

  inputImage.cleanup
  outputImage.cleanup

  resultImage = hubIOImgInputImage(outputFilename)
  resultImage.quickLook, /Default
  ;print, resultImage
  resultImage.cleanup
  info = FILE_INFO(outputFilename)
  print, info.size
  stop
end
