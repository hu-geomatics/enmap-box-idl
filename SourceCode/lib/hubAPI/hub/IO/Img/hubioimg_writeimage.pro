;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    Use this procedure to create an image from a three dimensional data array.
;
; :Params:
;    data : in, required, type=number
;      Use this argument to specify the image cube. Data must be in BSQ order [samples,lines,bands].
;      
;    filename : in, required, type=string
;      Use this argument to specify the filename for writing the result image.
;
; :Keywords:
;    NoOpen : in, optional, type=boolean, default=0
;      Set this keyword to not open the output image inside the EnMAP-Box.
;
;-
pro hubIOImg_writeImage, data, filename, MetaNames=metaNames, MetaValues=metaValues, NoOpen=NoOpen
  
  size = size(/DIMENSIONS, hub_fix3d(data))
  writerSettings = hash()
  writerSettings['samples'] = size[0]
  writerSettings['lines'] = size[1]
  writerSettings['bands'] = size[2]
  writerSettings['data type'] = size(/TYPE, data)
  writerSettings['dataFormat'] = 'band'
  outputImage = hubIOImgOutputImage(filename, NoOpen=NoOpen)
  for i=0,n_elements(metaNames)-1 do begin
    outputImage.setMeta, metaNames[i], metaValues[i]
  endfor
  outputImage.initWriter, writerSettings
  outputImage.writeData, data
  outputImage.cleanup ;removed by BJ - cleanup get called by GC
 
end