;+
; :Description:
;    Is a procedural wrapper for `hubIOImgInputImage::getBand`.
;
;-
function hubIOImg_readBand, filename, bandPosition, Header=header, $ 
  SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle
  
  inputImage = hubIOImgInputImage(filename)
  header = inputImage.getHeader()
  result = inputImage.getBand(bandPosition, SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle)
  return, result
end

pro test_hubIOImg_readBand
;  filename = hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth')
;  filename = hub_getTestImage('Hymap_Berlin-A_Image')


  filename = 'E:\Andreas\Dropbox\images\bsq'
  filename = 'E:\Andreas\Dropbox\images\bil'
  filename = 'E:\Andreas\Dropbox\images\bip'
  

 ; filename = enmapbox_getTestImage('AF_LAI')
;  rectangle = (hubIOImgInputImage(filename)).getMapCoordinates(/BoundingBox)

  rectangle = (hubIOImgInputImage(filename)).getMapCoordinates([[-50,-50],[500,500]])
  rectangle = (hubIOImgInputImage(filename)).getMapCoordinates([[-50,-50],[-1,-1]])
  
  
  band = hubIOImg_readBand(filename, 0, SubsetRectangle=rectangle)
  window, /FREE, XSIZE=n_elements(band[*,0]), YSIZE=n_elements(band[0,*])
  tvscl, band, /Order
end