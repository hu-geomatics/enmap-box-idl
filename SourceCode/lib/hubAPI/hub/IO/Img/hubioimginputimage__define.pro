;+
;  An hubIOImgInputImage object can be used to access image data and metadata. 
;
;  For usage details also see 
;  `Image Data Input/Output API <./_image-data-input-output.html>` and
;  `Accessing Input Images <./_input-images.html>`.
;-  

;+
; :Description:
;    This method is called indirectly when calling the object constructor hubIOImgInputImage().
;    The method return value is an object reference to the newly-created object.
;
; :Params:
;   filename : in, required, type=string
;      Use this argument to specify the full path name of the input image data file.
;
; :Keywords:
;    Classification: in, optional, type=bool
;      Set this keyword to check that the image is a correct classification file.
;      
;    Regression: in, optional, type=bool
;      Set this keyword to check that the image is a correct regression file.
;      
;    Mask: in, optional, type=bool
;      Set this keyword to check that the image is a correct mask file.
;
;    SpectralLibrary: in, optional, type=bool
;      Set this keyword to check that the image is a correct spectral library file.
;
;-
function hubIOImgInputImage::init $
  , filename $
  , Classification=classification $
  , Regression=regression $
  , Mask=mask $
  , SpectralLibrary=spectralLibrary

  !null = self->hubIOImgImage::init(filename)
  
  ; set/check filenames
  
  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    hubProEnvHelper.closeFile, filename
    message, !error_state.msg
  endif
  self.filenameHeader = (hubProEnvEnviHelper()).findHeaderFile(self.filenameData)
  self._checkReadPermission
  catch, /CANCEL
  
  ; prepare header
  
  self.header.setMeta, 'filename header', self.filenameHeader
  self.header.setRequiredDefaults
  self.header.parseFile, self.filenameHeader
  self.header.checkRequiredMeta
  self.header.convertSpeclibForward ; tread speclibs like images
  self.header.setOptionalDefaults
  self.header.checkOptionalMeta
    
  ; set spatial/spectral subset to full extend
  
  self._defineSubset,/Default
  
  ; check image types
  
  if keyword_set(classification) then begin
    if ~self.isClassification(info=info) then begin
      message, strjoin(['File is not a valid classification image: '+self.filenameData+':', info], string(13b))
    endif
  endif

  if keyword_set(regression) then begin
    if ~self.isRegression(info=info) then begin
      message, strjoin(['File is not a valid regression image: '+self.filenameData+':', info], string(13b))
    endif
  endif

  if keyword_set(mask) then begin
    if ~self.isMask(info=info) then begin
      message, strjoin(['File is not a valid mask image: '+self.filenameData+':', info], string(13b))
    endif
  endif

  if keyword_set(spectralLibrary) then begin
    if ~self.isSpectralLibrary(info=info) then begin
      message, strjoin(['File is not a valid spectral library: '+self.filenameData+':', info], string(13b))
    endif
  endif

  return,1b

end

;+
; :Description:
;    This method returns a single metadata value that is specified by the `metaName` argument. 
;    If the metadata is not available inside the image file header (*.hdr file), !NULL is returned.
;    
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
; 
; :Params:
;    metaName : in, required, type=string
;      Use this argument to specify the metadata name.
;      
; :Keywords:
;    Default : in, optional
;      Use this keyword to specify a default value, which will be returned if the metadata value is undefined.   
;-
function hubIOImgInputImage::getMeta $
  , metaName $
  , Default=default

  return, self.hubIOImgImage::getMeta(metaName, Default=default)

end

;+
; :Description:
;    This method returns a list of metadata values that is specified by the `metaNames` argument.
;
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
;
; :Params:
;    metaNames : in, required, type=string[]
;      Use this argument to specify the metadata names.
;
; :Keywords:
;    Defaults : in, optional, type=list()
;      Use this keyword to specify default values, which will be returned if the metadata value is undefined.
;-
function hubIOImgInputImage::getMetaList $
  , metaNames $
  , Defaults=defaults
  
  result = list()
  foreach metaName, metaNames, i do begin
    default = isa(defaults) ? defaults[i] : !null
    result.add, self.hubIOImgImage::getMeta(metaName, Default=default)
  endforeach
  return, result
end

;+
; :Description:
;    This method returns a hash of metadata that is specified by the `metaNames` argument. 
;    Metadata values that are not available inside the image file header (*.hdr file) are set to !NULL.
;    
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
; 
; :Params:
;    metaNames : in, required, type=string[]
;      Use this argument to specify the metadata names.
;   
;-
function hubIOImgInputImage::getMetaHash $
  , metaNames

  return, self.hubIOImgImage::getMetaHash(metaNames)

end

;+
; :Hidden:
;-
function hubIOImgInputImage::getInfo $
  ,infoName 

  return, self.hubIOImgImage::getInfo(infoName)

end

;+
; :Hidden:
;-
function hubIOImgInputImage::getFilename
  return, self.filenameData
end

;+
; :Hidden:
;-
function hubIOImgInputImage::getFileSize
  return, (file_info(self.filenameData)).size
end

;+
; :Hidden:
; 
; :Description:
;    This method is used to define a rectangular spatial or arbitrary spectral image subset.
;    It is called inside the `hubIOImgInputImage::initReader` method.
;
; :Keywords:
;    BandPositions : in, optional, type=int[]
;      Use this keyword to specify an array with image band positions that are included in the image subset. This array must not necessarily be ordered.
;      
;    SampleRange : in, optional, type=int[2]
;      Use this keyword to specify the starting sample number (SampleRange[0]) and the ending sample number (SampleRange[1]). 
;     
;    LineRange : in, optional, type=int[2]
;      Use this keyword to specify the starting line number (LineRange[0]) and the ending line number (LineRange[1]).
;     
;    Default : in, optional, type=boolean
;      Set this keyword to set the image subset to the original spatial extend and band positions. 
;      
pro hubIOImgInputImage::_defineSubset $
  ,BandPositions=bandPositions $
  ,SampleRange=sampleRange, LineRange=lineRange $
  ,Default=default

  if keyword_set(default) then begin
    self._defineSubset,BandPositions=l64indgen(self.getMeta('bands')),SampleRange=[0,self.getMeta('samples')-1],LineRange=[0,self.getMeta('lines')-1]
    return
  endif

  if isa(bandPositions) then begin
    if total((bandPositions lt 0) or (bandPositions ge self.getMeta('bands'))) ne 0 then begin
      message,'Invalid band positions.'
    endif
    self.subset.bandPositions = ptr_new(bandPositions)
  endif

  if isa(sampleRange) then begin
    if n_elements(sampleRange) ne 2 then begin
      message,'SampleRange keyword must be a two elemental integer array.'
    endif
    if (sampleRange[0] lt 0) or (sampleRange[1] ge self.getMeta('samples')) then begin
     if ~keyword_set(self.allowOutOfRangeAccess) then begin
        message,'Invalid sample range.'
     endif
    endif
    self.subset.sampleRange = long64(sampleRange)
  endif
  if isa(lineRange) then begin
    if n_elements(lineRange) ne 2 then begin
      message,'LineRange keyword must be a two elemental integer array.'
    endif
    if (lineRange[0] lt 0) or (lineRange[1] ge self.getMeta('lines')) then begin
      if ~keyword_set(self.allowOutOfRangeAccess) then begin
        message,'Invalid line range.'
      endif
    endif
    self.subset.lineRange = long64(lineRange)
  endif

  numberOfSamples = self.subset.sampleRange[1] - self.subset.sampleRange[0] +1
  numberOfLines = self.subset.lineRange[1] - self.subset.lineRange[0] +1
  numberOfBands = n_elements(*self.subset.bandPositions)
  self.subset.dimensions = [numberOfSamples,numberOfLines,numberOfBands]
  
  self.validSubset.noOverlap = self.subset.sampleRange[0] gt (self.getMeta('samples')-1) || $
                               self.subset.sampleRange[1] lt 0  || $
                               self.subset.lineRange[0] gt (self.getMeta('lines')-1) || $
                               self.subset.lineRange[1] lt 0
 
  self.validSubset.sampleRange = (self.subset.sampleRange > 0) < (self.getMeta('samples')-1)
  self.validSubset.lineRange = (self.subset.lineRange > 0) < (self.getMeta('lines')-1)
  self.validSubset.dimensions = [hubmathspan(self.validSubset.sampleRange)+1, hubmathspan( self.validSubset.lineRange)+1, numberOfBands]
  self.validSubset.sampleOffset = (self.validSubset.sampleRange-self.subset.sampleRange)[0]
  self.validSubset.lineOffset = (self.validSubset.lineRange-self.subset.lineRange)[0]

end

;+
; :Description:
;    This method is used to initialize reading of image data or image tile data. 
;    Use the `hubIOImgInputImage::getData` method for subsequent data reading.
;    
;    To work on a rectangular spatial or arbitrary spectral image subset, 
;    use the `SubsetSampleRange`, `SubsetLineRange` and `SubsetBandPositions` keywords.
;    
;    Note that this method opens the image data file and allocates a free IDL Logical Unit Number (LUN) for further reading operations.
;    After finishing all reading operations, it is strongly recommended to call the 
;    `hubIOImgInputImage::finishReader` or `hubIOImgInputImage::cleanup` methods to free the LUN.
;
; :Params:
;    numberOfTileLines : in, optional, type=int
;      Use this argument to specify the number of image lines included in a single tile. 
;      Use it in conjunction with the `TileProcessing` keyword. 
;      
;    neighborhoodHeight : in, optional, type=int, default=1
;      Use this argument to specify the spatial operator size, that is to be applied to the image.
;      Each tile will be extended by neighborhoodHeight-1 extra lines that do overlap with the first lines 
;      of the next tile. Using this argument is crucial when applying spatial operators (e.g. convolution filters)
;      to band tiles or cube tiles to ensure correct results at the tile edges. 
;      
;      Use this argument in conjunction with the `TileProcessing` keyword, but only when reading band tiles or cube tiles, 
;      do not use it when reading slice tiles or value tiles.
;
; :Keywords:
;    TileProcessing : in, optional, type=boolean, default=0
;      Set this keyword to initialize tile data reading. For (non-tile) data reading, skip it or set it to zero.
;      
;    Band : in, optional, type=boolean, default=0
;      Set this keyword to read image bands. 
;      Set it in conjunction with the `TileProcessing` keyword to read image band tiles.
;       
;    Slice : in, optional, type=boolean, default=0
;      Set this keyword to read image slices. 
;      Set it in conjunction with the `TileProcessing` keyword to read image slice tiles.
;      
;    Cube : in, optional, type=boolean, default=0
;      Set this keyword to read the complete image cube. 
;      Set it in conjunction with the `TileProcessing` keyword to read image cube tiles.
;
;    Value : in, optional, type=boolean, default=0
;      Set this keyword in conjunction with the `TileProcessing` keyword to read image value tiles.
;      
;    Profiles : in, optional, type=boolean, default=0
;      Set this keyword to read image pixel profiles. 
;
;    DataType : in, optional, type={string | int}, default=image data type
;      Use this keyword to specify the output data type of the image data returned by the `hubIOImgInputImage::getData` methode.
;      By default, the data type is given by the image data type.
;      
;    SubsetBandPositions : in, optional, type=int[]
;      Use this keyword to specify an array with image band positions that are included in the image subset. This array must not necessarily be ordered.
;      
;    SubsetSampleRange : in, optional, type=int[2]
;      Use this keyword to specify the starting sample number (SubsetSampleRange[0]) and the ending sample number (SubsetSampleRange[1]). 
;     
;    SubsetLineRange : in, optional, type=int[2]
;      Use this keyword to specify the starting line number (SubsetLineRange[0]) and the ending line number (SubsetLineRange[1]).
;      
;    SubsetRectangle : in, optional, type=number[4]
;      Use this keyword to specify the spatial subset via a rectangle in map coordinates [upper left x, upper left y, lower right x, lower right y].
;      The given map coordinates are converted to image pixel coordinates using nearest neighbour resampling.
;      Resulting subset size in pixels calculated by: round( <rectangle size> / <pixel size> ).
;      Resulting line and sample ranges are returned to the `SubsetSampleRange` and `SubsetLineRange` keywords.
;
;    Mask : in, optional, type=boolean, default=0
;      Use this keyword to return mask data instead of image data. 
;      Mask data values are zero if the original image data is equal to the image background value, 
;      which is given by the image 'data ignore value' metadata. If the 'data ignore value' is undefined,
;      the background value is zero. All other mask data values are set to one, so to say the foreground.
;      
;    ForegroundMaskValue : in, optional, type=number
;      Use this keyword in conjunction with the `mask` keyword to explicitly define the mask foreground value.
;      Mask data values are set to one, if the original image data is equal to the foreground value. 
;      All other mask data values are set to zero, so to say the background.
;      
;    BackgroundMaskValue : in, optional, type=number
;      Use this keyword in conjunction with the `mask` keyword to explicitly define the mask background value.
;      Mask data values are set to zero, if the original image data is equal to the background value. 
;      All other mask data values are set to one, so to say the foreground.
;-
pro hubIOImgInputImage::initReader $
  , numberOfTileLines $
  , neighborhoodHeight $
  , Band=band $
  , Slice=slice $
  , Cube=cube $
  , Value=value $
  , Profiles=profiles $
  , DataType=dataType $
  , TileProcessing=tileProcessing $
  , SubsetBandPositions=bandPositions $
  , SubsetSampleRange=sampleRange $
  , SubsetLineRange=lineRange $
  , SubsetRectangle=rectangle $
  , Mask=mask $
  , ForegroundMaskValue=foregroundMaskValue $
  , BackgroundMaskValue=backgroundMaskValue
    
  ; define image subset

  if isa(rectangle) then begin
    
    mapInfo = self.getMeta('map info')
    pixelSize = [mapInfo.sizeX, mapInfo.sizeY]
    rectangleUL = rectangle[0:1]
    rectangleLR = rectangle[2:3]
    rectangleRange = [+1,-1]*(rectangleLR-rectangleUL)                      ; in map units
    targetSize = round(rectangleRange/pixelSize)                            ; in pixels
    targetOffset = self.locatePixel(rectangleUL+[+1,-1]*pixelSize/2.)       ; in pixels
    sampleRange = [targetOffset[0], targetOffset[0]+targetSize[0]-1]
    lineRange   = [targetOffset[1], targetOffset[1]+targetSize[1]-1]

;    imageSize = self.getSpatialSize()
;    sampleRangeWithoutBorder = (sampleRangeWithBorder > 0) < (imageSize[0]-1)
;    lineRangeWithoutBorder = (lineRangeWithBorder > 0) < (imageSize[1]-1)
;      
;    sampleBorder = sampleRange-sampleRangeWithBorder
;    lineBorder = lineRange-lineRangeWithBorder
    
    self.allowOutOfRangeAccess = 1b
  endif
  
  self._defineSubset, BandPositions=bandPositions, SampleRange=sampleRange, LineRange=lineRange

  ; check keywords and arguments

  dataType = isa(dataType) ? dataType : self.getMeta('data type')

  if keyword_set(tileProcessing) then begin
  
    conflictingKeywords = total([keyword_set(band), keyword_set(slice), keyword_set(cube), keyword_set(value)]) ne 1
    if conflictingKeywords then begin
      message, 'Set one of the exclusive keywords: Band, Slice, Cube or Value.'
    endif
  
    if ~isa(numberOfTileLines) then begin
      message, 'Argument numberOfTileLines is required when keyword TileProcessing is set.'  
    endif
    
    if ~(numberOfTileLines ge 1) then begin
      message, 'Argument numberOfTileLines must be an integer number greater or equal to one.'  
    endif
       
    if (keyword_set(slice) or keyword_set(value)) and isa(neighborhoodHeight) then begin
      message, 'Argument neighborhoodHeight is not allowed when keyword Slice or Value is set.'
    endif
    
    _neighborhoodHeight = isa(neighborhoodHeight) ? long64(neighborhoodHeight) : 1ll
    correctNeighborhoodHeight = (_neighborhoodHeight mod 2) and (_neighborhoodHeight ge 1)
    if _neighborhoodHeight lt 1 then begin
      message, 'Argument neighborhoodHeight must be greater equal 1.'
    endif

    if (_neighborhoodHeight mod 2) ne 1 then begin
      message, 'Argument neighborhoodHeight must be an odd number.'
    endif

    linesToRead = isa(lineRange) ? lineRange[1]-lineRange[0] : self.getMeta('lines')
    if _neighborhoodHeight gt linesToRead then begin
      message, 'Argument neighborhoodHeight must be lower than or equal to the number of image lines.'
    endif

    if ~( numberOfTileLines gt (_neighborhoodHeight-1) ) then begin
      message, 'Argument numberOfTileLines must be greater than argument neighborhoodHeight-1.'
    endif

;    if keyword_set(value) then begin
;      message, 'Keyword Profiles is not allowed when keyword TileProcessing is set.'  
;    endif
    
  endif else begin
    
    conflictingKeywords = total([keyword_set(band), keyword_set(slice), keyword_set(cube), keyword_set(profiles)]) ne 1
    if conflictingKeywords then begin
      message, 'Set one of the exclusive keywords: Band, Slice, Cube or Profiles.'
    endif
  
    if isa(_neighborhoodHeight) then begin
      message, 'Argument neighborhoodHeight is not allowed when keyword TileProcessing is not set.'  
    endif
    
    if keyword_set(value) then begin
      message, 'Keyword Value is not allowed when keyword TileProcessing is not set.'  
    endif
  
  endelse

  ; determine data format
  
  if keyword_set(band) then dataFormat = 'band'
  if keyword_set(slice) then dataFormat = 'slice'
  if keyword_set(cube) then dataFormat = 'cube'
  if keyword_set(value) then dataFormat = 'value'
  if keyword_set(profiles) then dataFormat = 'profiles'  

  ; save generell reader settings
  
  self.readerSettings = hash()
  (self.readerSettings)['dataFormat'] = dataFormat
  (self.readerSettings)['samples'] = self.subset.dimensions[0]
  (self.readerSettings)['lines'] = self.subset.dimensions[1]
  (self.readerSettings)['bands'] = self.subset.dimensions[2]
  (self.readerSettings)['data type'] = dataType
  (self.readerSettings)['tileProcessing'] = keyword_set(tileProcessing)
  (self.readerSettings)['useMask'] = keyword_set(mask)  
  
  if keyword_set(allowOutOfRangeAccess) then begin
    (self.readerSettings)['allowOutOfRangeAccess'] = 1b
    (self.readerSettings)['sampleBorder'] = sampleBorder
    (self.readerSettings)['lineBorder'] = lineBorder
  endif

  if keyword_set(mask) then begin

    if isa(foregroundMaskValue) and isa(backgroundMaskValue) then begin
      message, 'Conflicting keywords: foregroundMaskValue, backgroundMaskValue.'
    endif 

    (self.readerSettings)['maskingMode'] = 'background'

    if isa(foregroundMaskValue) then begin
      (self.readerSettings)['maskingMode'] = 'foreground'
    endif
    
    case (self.readerSettings)['maskingMode'] of
      'background' : begin
        if isa(backgroundMaskValue) then begin
          (self.readerSettings)['backgroundValue'] = backgroundMaskValue
        endif else begin
          (self.readerSettings)['backgroundValue'] = self.getMeta('data ignore value', Default=0)
        endelse
      end
      'foreground' : begin
        (self.readerSettings)['foregroundValue'] = foregroundMaskValue
      end
    endcase
  
  endif

  ; save tile processing reader settings and initialize tile reader
  
  if keyword_set(tileProcessing) then begin
    
    (self.readerSettings)['numberOfTileLines'] = long64(numberOfTileLines) < (self.readerSettings)['lines']
    (self.readerSettings)['neighborhoodHeight'] = long64(_neighborhoodHeight)  
    self._initTileReader

  endif
  
  ; open file for read access and save some information for faster binary data input 
  if self.readerInitialized then begin
    message, 'Reader already initialized. Use hubIOImgInputImage::finishReader before reader re-initialization.'
  endif
  
  isCompressed = self.getMeta('file compression')
  isCompressed = isa(isCompressed) && fix(isCompressed) ne 0

  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    hubProEnvHelper.closeFile, self.filenameData
    message, !error_state.msg
  endif
  openr, logicalUnitNumber, self.filenameData, /GET_LUN, COMPRESS=isCompressed
  catch, /CANCEL

  self.binaryIO.type = self.getMeta('data type')
  typeInfo = hubHelper.getTypeInfo(self.binaryIO.type)
  self.binaryIO.typeSize = typeInfo.size
  self.binaryIO.byteOrder = self.getMeta('byte order')
  self.binaryIO.byteOrderSystem = hubHelper.getByteOrder()
  self.logicalUnitNumber = logicalUnitNumber
  self.readerInitialized = 1b
  
end

;+
; :Hidden:
;-
pro hubIOImgInputImage::_initTileReader

  ; calculate tile information

  numberOfTiles = (self.readerSettings)['lines'] / (self.readerSettings)['numberOfTileLines']
  
  lineStart = l64indgen(numberOfTiles)*(self.readerSettings)['numberOfTileLines'] + self.subset.lineRange[0]
  lineEnd = lineStart + (self.readerSettings)['numberOfTileLines']-1 + (self.readerSettings)['neighborhoodHeight']-1
  lineEnd = lineEnd < self.subset.lineRange[1]

  ; special handling for the last tile lines
  
  remainingLines = self.subset.lineRange[1] - lineEnd[-1] 
  
  if remainingLines le (self.readerSettings)['neighborhoodHeight'] then begin
    lineEnd[-1] = self.subset.lineRange[1]
  endif else begin
    lineStart = [lineStart, lineStart[-1]+(self.readerSettings)['numberOfTileLines']]
    lineEnd = [lineEnd, self.subset.lineRange[1]]
    numberOfTiles += 1
  endelse
  
  if ((self.readerSettings)['dataFormat'] eq 'band') or $
    (((self.readerSettings)['dataFormat'] eq 'value') and (self.getMeta('interleave') eq 'bsq')) then begin
    
    bandIndex = l64indgen((self.readerSettings)['bands'])
    bandIndex = rebin(bandIndex, numberOfTiles*(self.readerSettings)['bands'], /SAMPLE)
    lineStart = (rebin(lineStart, numberOfTiles, (self.readerSettings)['bands'], /SAMPLE))[*] 
    lineEnd = (rebin(lineEnd, numberOfTiles, (self.readerSettings)['bands'], /SAMPLE))[*]
    numberOfTiles = numberOfTiles*(self.readerSettings)['bands']

  endif
  
  ; save tile processing reader settings
  
  (self.readerSettings)['numberOfTiles'] = numberOfTiles
  (self.readerSettings)['lineStart'] = lineStart
  (self.readerSettings)['lineEnd'] = lineEnd
  (self.readerSettings)['bandIndex'] = bandIndex
  (self.readerSettings)['currentTile'] = 0ll
  
end

;+
; :Description:
;    This method is used to finish reading of image data or image tile data.
;    Call this method after finishing all reading operations to free the LUN that was allocated by the `hubIOImgInputImage::initReader` methode.
;    It is also called implicitly when destroying the object via the `hubIOImgInputImage::cleanup` method.
;-
pro hubIOImgInputImage::finishReader

  if self.readerInitialized then begin
    free_lun, self.logicalUnitNumber
  endif
  self.readerInitialized = 0b
    
end

;+
; :Description:
;    This method returns a hash variable that can be used as input argument for the `hubIOImgOutputImage::initWriter` methode.
;    The hash contains generel information about how the data reader was initialized::
;    
;      hash key       | type                                               | description
;      ---------------|----------------------------------------------------|------------------------------
;      data type      | {'byte' | ... | 'ulong64'}                         | numerical IDL data type
;      dataFormat     | {'band' | 'slice' | 'cube'} | 'value' | profiles}  | data format
;      tileProcessing | {0 | 1}                                            | tile or non-tile processing
;      samples        | int                                                | number of samples
;      lines          | int                                                | number of lines
;      bands          | int                                                | number of bands
;      
;    In the tile processing case, the hash additionally contains information about::
;         
;      hash key           | type  | description
;      -------------------|-------|------------------------------
;      numberOfTiles      | int   | 
;      neighborhoodHeight | int   |                  neighborhood height
;
;    In some situations it is necessary to manipulate some of the following values:
;    `'samples'`, `'lines'`, `'bands'` or `'data type'` to correctly initialize the data writer. 
;    Therefor use the `Set*` keywords to change output image writer settings, that differ from the input image.
;
; :Keywords:
;    SetSamples : in, optional, type=int
;      Use this keyword to specify the number of samples inside the result hash. 
;      Only use it if the number of samples in the input and output images differ.
;       
;    SetLines : in, optional, type=int
;      Use this keyword to specify the number of lines inside the result hash. 
;      Only use it if the number of lines in the input and output images differ.
;      
;    SetBands : in, optional, type=int
;      Use this keyword to specify the number of bands inside the result hash. 
;      Only use it if the number of bands in the input and output images differ.
;      
;    SetDataType : in, optional, type={string | int}
;      Use this keyword to specify the data type inside the result hash. 
;      Only use it if the data type in the input and output images differ.
;      
;-
function hubIOImgInputImage::getWriterSettings $
  , SetSamples=setSamples $
  , SetLines=setLines $
  , SetBands=setBands $
  , SetDataType=setDataType

  if ~self.readerInitialized then begin
    message, 'Reader not initialized, call hubIOImgInputImage::initReader.'
  endif
  
  keys1 = ['dataFormat', 'samples', 'lines', 'bands', 'data type', 'tileProcessing'] 
  readerSettings = (self.readerSettings)[keys1]
  
  if (self.readerSettings)['tileProcessing'] then begin
 
    keys2 = ['neighborhoodHeight', 'numberOfTiles']
    readerSettings = readerSettings + (self.readerSettings)[keys2]
    
    if (self.readerSettings)['dataFormat'] eq 'value' then begin
      readerSettings['interleave'] = self.getMeta('interleave')
    endif
 
  endif
  
  ; change settings
  
  if isa(setSamples) then readerSettings['samples'] = setSamples
  if isa(setLines) then readerSettings['lines'] = setLines
  if isa(setBands) then readerSettings['bands'] = setBands
  if isa(setDataType) then readerSettings['data type'] = setDataType
  
  return, readerSettings

end

;+
; :Description:
;    This method returns image data or image tile data. In dependence to the reader initialization (see `hubIOImgInputImage::initReader`),
;    different types of data formats are returned::
;    
;      data format           | reader initialization    | result dimensions
;      ----------------------|--------------------------|------------------------------
;      image band            | /Band                    | 2D - [samples, lines]
;      image slice           | /Slice                   | 2D - [bands, samples]
;      image cube            | /Cube                    | 3D - [samples, lines, bands]
;      image pixel profiles  | /Profiles                | 2D - [bands, profiles]
;                            |                          |
;      image band tile       | /Band, /TileProcessing   | 2D - [samples, tileLines]
;      image slice tile      | /Slice, /TileProcessing  | 2D - [bands, samples*tileLines]
;      image cube tile       | /Cube, /TileProcessing   | 3D - [samples, tileLines, bands]
;      image value tile      | /Value, /TileProcessing  | 1D - [samples*tileLines*bands]
;
; :Params:
;    index : in, optional, type={int | int[] | int[2][]}
;      Use this argument to either specify a) a single image band index,
;      b) a single image line index, or c) an array of one- or two-dimensional image pixel indices.
;      Use it only in the case, when the reader was initialized for non-tile reading of image bands, image slices
;      or image pixel profiles. Note that indices are always zero-based.
;
function hubIOImgInputImage::getData $
  ,index

  if ~self.readerInitialized then begin
    message, 'Reader not initialized, call hubIOImgInputImage::initReader.'
  endif

  if (self.readerSettings)['tileProcessing'] then begin
  
    data = self._getTile()
    
  endif else begin
  
    case (self.readerSettings)['dataFormat'] of
      'band'     : data = self._getBand(index)
      'slice'    : data = self._getSlice(index)
      'cube'     : data = self._getCube()
      'profiles' : begin
      
        ; convert to 1D indices
        if isa(index) then begin
          is2DIndex = (size(/N_DIMENSIONS, index) eq 2)
          if is2DIndex then begin
            if   (total(index lt 0) ne 0) $
              or (total(index[0,*] ge self.getMeta('samples')) ne 0) $
              or (total(index[1,*] ge self.getMeta('lines')) ne 0) then begin
                return, !null ; pixel indices are of ouf image range
            endif 
            indices = index[0,*] + index[1,*]*self.subset.dimensions[0]
          endif else begin
            indices = index[*]
            if total(indices lt 0 or indices ge (self.getMeta('samples') * self.getMeta('lines'))) ne 0 then begin
              return, !null ; pixel indices are of ouf image range 
            endif 
          endelse
        endif else begin
          indices = !null
        endelse
        
        data = self._getProfiles(indices)

      end
    endcase
  endelse
  
  ; convert or mask data 
  
  if (self.readerSettings)['useMask'] then begin
    dataSize = size(/DIMENSIONS, data)
    
    case (self.readerSettings)['maskingMode'] of
      'background' : begin
          ;Extension to allow more than one background/foreground value
          bvmask = make_array(dataSize, /BYTE, value=1b)
          foreach bv, (self.readerSettings)['backgroundValue'] do begin
            bvmask and= data ne bv 
          endforeach
        end
      'foreground' : begin
          fvmask = make_array(dataSize, /BYTE, value=0b)
          foreach fv, (self.readerSettings)['foregroundValue'] do begin
            fvmask or= data eq fv
          endforeach
        end
        else: begin
            ;implicit background value = 0 or, if defined, the data ignore value
            div = self.getMeta('data ignore value')
            bvMask = isa(div) ? data ne div : data ne 0
        end   
    endcase
    data = temporary(bvMask)
    if isa(fvmask) then data or= temporary(fvMask)
    
    !null = reform(/OVERWRITE, data, dataSize)
  endif else begin
    data = hubMathHelper.convertData(data, (self.readerSettings)['data type'], /NoCopy)
  endelse
  return, data

end

function hubIOImgInputImage::getCube, Slice=slice, SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle, SubsetBandPositions=bandPositions, DataType=dataType
  self.initReader, /Cube, SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle, SubsetBandPositions=bandPositions, DataType=dataType
  result = self.getData()
  self.finishReader
  if keyword_set(slice) then begin
    result = hub_fix2d(reform(result, product(self.getSpatialSize()), self.getSpectralSize()))
  endif
  return, result
end

function hubIOImgInputImage::getBand, band, $
  SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle, DataType=dataType
  self.initReader, /Band, SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle, DataType=dataType
  imageBand = self.getData(band)
  self.finishReader
  return, imageBand
end

function hubIOImgInputImage::getProfile, pixelPositions, SubsetBandPositions=bandPositions
  self.initReader, /Profiles, SubsetBandPositions=bandPositions
  result = self.getData(pixelPositions)
  self.finishReader
  return, result
end

;+
; :Description:
;    This method returns 1 if all tiles of the image are read and 0 otherwise. 
;
;-
function hubIOImgInputImage::tileProcessingDone, Progress=progress
  
  if ~(self.readerSettings)['tileProcessing'] then begin
    message, 'Reader not initialized for tile processing.'
  endif
  tileProcessingDone = (self.readerSettings)['currentTile'] eq (self.readerSettings)['numberOfTiles']
  progress = 1.*(self.readerSettings)['currentTile'] / (self.readerSettings)['numberOfTiles']
  return, tileProcessingDone
end

;+
; :Hidden:
;-
function hubIOImgInputImage::_getBand $
  ,index

  if ~isa(index) then begin
    return, !null
  endif

  bandIndex = (*(self.subset.bandPositions))[index]
  samples = self.getMeta('samples')
  lines = self.getMeta('lines')
  bands = self.getMeta('bands')

  bandData = hub_fix2d(make_array(self.subset.dimensions[[0,1]], TYPE=self.binaryIO.type))

  if ~self.validSubset.noOverlap then begin 
  
    case self.getMeta('interleave') of
      'bsq' : $
        begin
          memoryEfficient = 0b
          if memoryEfficient then begin
            ; todo test
            stop
            offset = 1ll*bandIndex*samples*lines + self.subset.sampleRange[0]
            for line=0ll, self.subset.dimensions[1]-1 do begin
              dimensions = [self.subset.dimensions[0], 1]
              bandData[0,line] = self->_readBinary(offset, dimensions)            
              offset += samples
            endfor
          endif else begin
            offset = 1ll*bandIndex*samples*lines + samples*self.validSubset.lineRange[0]
            dimensions = [samples, self.validSubset.dimensions[1]]
            bandData[self.validSubset.sampleOffset,self.validSubset.lineOffset] = (self->_readBinary(offset, dimensions))[self.validSubset.sampleRange[0]:self.validSubset.sampleRange[1], *]
          endelse
        end
      'bil' : $
        begin
          dimensions = [self.validSubset.dimensions[0]]
          for line=self.validSubset.lineRange[0],self.validSubset.lineRange[1] do begin
            offset = 1ll*line*samples*bands + 1ll*bandIndex*samples + 1ll*self.validSubset.sampleRange[0]
            bandData[self.validSubset.sampleOffset, self.validSubset.lineOffset+line-self.validSubset.lineRange[0]] = self._readBinary(offset, dimensions)
          endfor
        end
      'bip' : $
        begin
          dimensions = [bands*self.validSubset.dimensions[0]]
          for line=self.validSubset.lineRange[0],self.validSubset.lineRange[1] do begin
            offset = 1ll*line*bands*samples + 1ll*bands*self.validSubset.sampleRange[0]
            lineBIP = self._readBinary(offset,dimensions)
            bandData[self.validSubset.sampleOffset, self.validSubset.lineOffset+line-self.validSubset.lineRange[0]] = lineBIP[bandIndex:*:bands]
          endfor
        end    
      
      else : message,'Wrong interleave type.'
    endcase
  endif
  return,bandData

end

;+
; :Hidden:
;-
function hubIOImgInputImage::_getSlice $
  ,index

  if ~isa(index) then begin
    return, !null
  endif

  ; save old line range and set line range to single line
  oldLineRange = self.subset.lineRange
  self._defineSubset, LineRange=oldLineRange[0]+[index, index]

  case strlowcase(self.getMeta('interleave')) of
    'bsq' : $
      begin
        sliceData = self._getCube()
        ; remove scalar dimension
        !null = reform(sliceData, self.subset.dimensions[[0,2]], /OVERWRITE)
        ; transpose to BIP and fix dimensions
        sliceData = transpose(sliceData)
        !null = reform(sliceData, self.subset.dimensions[[2,0]], /OVERWRITE)
      end
    'bil' : $
      begin
        sliceData = self._getCube(/NoTranspose)
        
        ; remove scalar dimension
        !null = reform(sliceData, self.subset.dimensions[[0,2]], /OVERWRITE)
        ; transpose to BIP and fix dimensions
        sliceData = transpose(sliceData)
        !null = reform(sliceData, self.subset.dimensions[[2,0]], /OVERWRITE)
      end
    'bip' : $
      begin
        sliceData = self._getCube(/NoTranspose)
        
        ; remove scalar dimension
        !null = reform(sliceData, self.subset.dimensions[[2,0]], /OVERWRITE)
        ; transpose to BIP
          ; nothing to do
      end    

    else : message,'Wrong interleave type.'
  endcase

  ; reset line range

  self._defineSubset, LineRange=oldLineRange

  return,sliceData

end

;+
; :Hidden:
;-
function hubIOImgInputImage::_getCube, NoTranspose=noTranspose

  samples = self.getMeta('samples')
  lines = self.getMeta('lines')
  bands = self.getMeta('bands')

  if ~self.validSubset.noOverlap then begin

    case strlowcase(self.getMeta('interleave')) of
      'bsq' : $
        begin
          cubeData = hub_fix3d(make_array(self.subset.dimensions[[0,1,2]], TYPE=self.binaryIO.type))
          for bandPositionIndex=0,n_elements(*self.subset.bandPositions)-1 do begin
            bandPosition = (*self.subset.bandPositions)[bandPositionIndex]
            cubeData[0, 0, bandPositionIndex] = self._getBand(bandPositionIndex)
          endfor
        end
      'bil' : $
        begin
          offset = 1ll*samples*bands*self.validSubset.lineRange[0]
          dimensions = [samples, bands, self.validSubset.dimensions[1]]
          
          ; read cube as BIL
          validCubeData = self._readBinary(offset, dimensions)

          ; subset cube
          validCubeData = hub_fix3d(validCubeData[self.validSubset.sampleRange[0]:self.validSubset.sampleRange[1], *self.subset.bandPositions, *])
  
          ; transpose cube to BSQ
          if ~keyword_set(noTranspose) then begin
            validCubeData = hub_fix3d(transpose(validCubeData,[0,2,1]))
          endif

          ; fill in valid data
          if keyword_set(noTranspose) then begin
            cubeData = hub_fix3d(make_array(self.subset.dimensions[[0,2,1]], TYPE=self.binaryIO.type))
            cubeData[self.validSubset.sampleOffset, 0, self.validSubset.lineOffset] = validCubeData
          endif else begin
            cubeData = hub_fix3d(make_array(self.subset.dimensions[[0,1,2]], TYPE=self.binaryIO.type))
            cubeData[self.validSubset.sampleOffset, self.validSubset.lineOffset,0 ] = validCubeData
          endelse
        end
      'bip' : $
        begin
          offset = 1ll*bands*samples*self.validSubset.lineRange[0]
          dimensions = [bands, samples, self.validSubset.dimensions[1]]
          
          ; read cube as BIP
          validCubeData = self._readBinary(offset, dimensions)
  
          ; subset cube
          validCubeData = hub_fix3d(validCubeData[*self.subset.bandPositions, self.validSubset.sampleRange[0]:self.validSubset.sampleRange[1], *])
  
          ; transpose cube to BSQ
          if ~keyword_set(noTranspose) then begin
            validCubeData = hub_fix3d(transpose(temporary(validCubeData),[1,2,0]))
          endif

          ; fill in valid data
          if keyword_set(noTranspose) then begin
            cubeData = make_array(self.subset.dimensions[[2,0,1]], TYPE=self.binaryIO.type)
            cubeData[0, self.validSubset.sampleOffset, self.validSubset.lineOffset] = validCubeData
          endif else begin          
            cubeData = make_array(self.subset.dimensions[[0,1,2]], TYPE=self.binaryIO.type)
            cubeData[self.validSubset.sampleOffset, self.validSubset.lineOffset, 0] = validCubeData
          endelse  
        end    
      
      else : message,'Wrong interleave type.'
    endcase
  endif else begin
    cubeData = hub_fix3d(make_array(self.subset.dimensions[[0,1,2]], TYPE=self.binaryIO.type))
  endelse
  return,cubeData

end

;+
; :Hidden:
;-
function hubIOImgInputImage::_getProfiles $
  , indices

  ; check argument
  
  if ~isa(indices) then begin
    return, !null
  endif
  
  ; sort indices 
  
  indicesSortedIndices = sort(indices)
  sortedIndices = indices[indicesSortedIndices]
  
  ; convert 1D to 2D indices

  sampleIndices = sortedIndices mod self.subset.dimensions[0]
  lineIndices = sortedIndices / self.subset.dimensions[0]
  
  ; find the first and last occurrence of samples in every line 
 
  lastOccurrence = uniq(lineIndices)
  firstOccurrence = shift(lastOccurrence,+1)+1
  firstOccurrence[0] = 0
  uniqLines = lineIndices[firstOccurrence] 

  ; prepare result variable

  profiles = make_array(self.subset.dimensions[2], n_elements(sortedIndices)$
    , TYPE=self.binaryIO.type)

  ; read each line (slice) that occurres and extract the specific samples

  foreach line,uniqLines,i do begin
    currentSlice = self._getSlice(line)
    currentSampleIndices = sampleIndices[firstOccurrence[i]:lastOccurrence[i]]
    currentProfiles = currentSlice[*, currentSampleIndices]
    profiles[*, firstOccurrence[i]:lastOccurrence[i]] = currentProfiles
  endforeach

  return, profiles[*, sort(indicesSortedIndices)]
 
end

;+
; :Hidden:
;-
function hubIOImgInputImage::_getTile

  ; save old line range and set line range to tile line range 

  oldLineRange = self.subset.lineRange
  self._defineSubset, LineRange=[((self.readerSettings)['lineStart'])[(self.readerSettings)['currentTile']], ((self.readerSettings)['lineEnd'])[(self.readerSettings)['currentTile']]]
  
  ; read tile
  
  case (self.readerSettings)['dataFormat'] of
    'band' : begin
      bandIndex = ((self.readerSettings)['bandIndex'])[(self.readerSettings)['currentTile']]
      tileData = self._getBand(bandIndex)
    end
    'slice' : begin
      tileData = self._getCube(/NoTranspose)
      case self.getMeta('interleave') of
        'bsq' : tileData = transpose(tileData, [2, 0, 1])
        'bil' : tileData = transpose(tileData, [1, 0, 2])
        'bip' : ; do nothing
      endcase
      !null = reform(tileData, self.subset.dimensions[2], self.subset.dimensions[0]*self.subset.dimensions[1], /OVERWRITE)
    end
    
    'cube' : begin
      tileData = self._getCube()
    end

    'value' : begin
      case self.getMeta('interleave') of
        'bsq' : begin
          tileData = self._getCube(/NoTranspose)
          bandIndex = ((self.readerSettings)['bandIndex'])[(self.readerSettings)['currentTile']]
          tileData = self._getBand(bandIndex)
        end
        'bil' : tileData = self._getCube(/NoTranspose)
        'bip' : tileData = self._getCube(/NoTranspose)
      endcase
      !null = reform(tileData, n_elements(tileData), /OVERWRITE)
    end
    
  endcase
  
  ; reset line range

  self._defineSubset, LineRange=oldLineRange

  ; increase tile counter
  
  (self.readerSettings)['currentTile'] = (self.readerSettings)['currentTile']+1

  ; return output
 
  return,tileData

end

;+
; :Hidden:
;-
function hubIOImgInputImage::_readBinary $
  , offset $
  , dimensions 

  ; read from file

  result = make_array(TYPE=self.binaryIO.type, dimensions, /NOZERO)
  point_lun, self.logicalUnitNumber, offset*self.binaryIO.typeSize
  readu, self.logicalUnitNumber, result

  ; swap endian if necessary
 
  if self.binaryIO.byteOrder ne self.binaryIO.byteOrderSystem then begin 
    swap_endian_inplace, result
  endif
  
  ; fix dimensions
  
  !null = reform(result, dimensions,/OVERWRITE)
  
  return, result

end

;+
; :Hidden:
;-
pro hubIOImgInputImage::_checkReadPermission
  if ~ (hubIOHelper()).fileReadable(self.filenameData) then begin
    message, 'Data file is not readable: '+self.filenameData
  endif
  if ~ (hubIOHelper()).fileReadable(self.filenameHeader) then begin
    message, 'Header file is not readable: '+self.filenameHeader
  endif

end

;+
; :Hidden:
;-
function hubIOImgInputImage::_overloadPrint

  return,self.header._overloadPrint()

end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid classification image, 0 (false) otherwise.
;
;
;
; :Keywords:
;    info: out, type=string
;      In case this image is not a classification image, this keyword tells you why. 
;
; 
;-
function hubIOImgInputImage::isClassification, info=info
  
  info = !NULL
  
  if self.getMeta('bands') gt 1 then begin
    info = [info, 'Image has more than one band']
  endif
  
  if ~strcmp(/FOLD_CASE, self.getMeta('file type'), 'ENVI Classification') then begin
    info = [info, 'Image file type is different to "ENVI Classificatinon".']
  endif
  
  return, ~isa(info)

end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid regression image (one band + specified data ignore value), 0 (false) otherwise.
;
;
;
; :Keywords:
;    info: out, type=string
;      In case this image is not a regression image, this keyword tells you why. 
;
;-
function hubIOImgInputImage::isRegression, info=info
  
  info = !Null
  if self.getMeta('bands') gt 1 then begin
    info = [info, 'Image has more than one band.']
  endif
  
  if ~isa(self.getMeta('data ignore value')) then begin
    info = [info, 'Data ignore value is not specified.']
  endif
  
  return, ~isa(info)

end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid mask image, 0 (false) otherwise.
;
; :Keywords:
;    info: out, type=string
;      In case this image is not a valid mask image, this keyword tells you why.
;-
function hubIOImgInputImage::isMask, info=info
  info = !NULL

  if self.getMeta('bands') ne 1 then begin
    info = [info, 'Image has more than one band.']
  endif
  return, ~isa(info)

end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid spectral library, 0 (false) otherwise.
;    
; :Keywords:
;    info: out, type=string
;      In case this dataset is not a spectral library, this keyword tells you why.
;-
function hubIOImgInputImage::isSpectralLibrary, info=info

  info = !NULL
  if ~strcmp(self.getMeta('file type'), 'ENVI Spectral Library', /Fold_Case) then begin
    info = [info, 'File type is not "ENVI Spectral Library".']
  endif
  
  return, ~isa(info)

end

;+
; :Description:
;    This method returns 1 (true) if the image spatial size is equal to the size specified 
;    by the `spatialSize` argument, 0 (false) otherwise.
;
; :Params:
;    spatialSize : in, required, type=number[2]
;      Use this argument to specify the spatial size for the test.
;
;-
function hubIOImgInputImage::isCorrectSpatialSize $
  , spatialSize

  isCorrectSpatialSize = 1
  selfSpatialSize = self.getSpatialSize()
  
  isCorrectSpatialSize and= selfSpatialSize[0] eq spatialSize[0]
  isCorrectSpatialSize and= selfSpatialSize[1] eq spatialSize[1]
  
  return, isCorrectSpatialSize

end

;+
; :Description:
;    This method returns 1 (true) if the image spectral size is equal to the size specified
;    by the `spectralSize` argument, 0 (false) otherwise.
;
; :Params:
;    spectralSize : in, required, type=number[2]
;      Use this argument to specify the spectral size for the test.
;
;-
function hubIOImgInputImage::isCorrectSpectralSize $
  , spectralSize
    
  isCorrectSpectralSize = self.getMeta('bands') eq spectralSize
  return, isCorrectSpectralSize

end

;+
; :Description:
;    This method returns the image spatialSize (result = [samples, lines]).
;-
function hubIOImgInputImage::getSpatialSize
  return, self.header.getSpatialSize()
end

;+
; :Description:
;    This method returns the image spectralSize (result = [bands]).
;-
function hubIOImgInputImage::getSpectralSize

  spectralSize = self.getMeta('bands')
  return, spectralSize

end

;+
; :Description:
;    This method returns 1 (true) if the image metadata includes all metadata specified
;    by the `metaNames` argument, 0 (false) otherwise. 
;
; :Params:
;    metaNames : in, required, type=string[]
;      Use this argument to specify the metadata for the test.
;
;-
function hubIOImgInputImage::hasMeta $
  , metaNames

  return, self.hubIOImgImage::hasMeta(metaNames)

end

;+
; :Description:
;    This method returns band positions of the wavelengths locations specified by the `wavelengths` argument.
;
; :Params:
;    wavelengths : in, optional, type=float[]
;      Use this argument to specify an array with wavelengths locations in nanometers.
;
; :Keywords:
;    TrueColor : in, optional, type=boolean, default=0
;      Set this keyword to return the band positions for true color (640, 550 and 460 nanometer).
;      
;    ColoredInfrared : in, optional, type=boolean, default=0
;      Set this keyword to return the band positions for colored infrared (860, 650 and 550 nanometer).
;      
;-
function hubIOImgInputImage::locateWavelength, wavelengths, TrueColor=trueColor, ColoredInfrared=coloredInfrared
  return, self.header.locateWavelength(wavelengths, TrueColor=trueColor, ColoredInfrared=coloredInfrared)
end


function hubIOImgInputImage::locatePixel, mapX, mapY
  pixelCoordinates = self.getPixelCoordinates(mapX, mapY)
  pixelIndices = floor(pixelCoordinates)
  return, pixelIndices
end

;+
; :Description:
;    This method returns map coordinates for given pixel coordinates.
;
; :Params:
;    pixelX: in, required, type={number[] | number[2][]}
;      Use this argument to specify a 1-dimensional array of pixel x coordinates (in this case also specify `pixelY`).
;      Alternatively, specify a 2-dimensional array of pixel xy coordinates (in this case `pixelY` is ignored).
;
;    pixelY: in, optional, type={number[] | number[2][]}
;      Use this argument to specify a 1-dimensional array of pixel y coordinates.
;
; :Keywords:
;    BoundingBox: in, optional, type=boolean
;      Set this keyword to return the image bounding box (`pixelX` and `pixelY` are ignored).
;
; :Author: Andreas Rabe
;-
function hubIOImgInputImage::getMapCoordinates, pixelX, pixelY, BoundingBox=boundingBox
  if keyword_set(boundingBox) then begin
    spatialSize = self.getSpatialSize()
    return, self.getMapCoordinates([0,spatialSize[0]], [0,spatialSize[1]])
  endif
  mapInfoObj = (self.getHeader()).getMetaObj('map info')
  return, mapInfoObj.getMapCoordinates(pixelX, pixelY)
end

;+
; :Description:
;    This method returns pixel coordinates for given map coordinates.
;
; :Params:
;    mapX: in, required, type={number[] | number[2][]}
;      Use this argument to specify a 1-dimensional array of map x coordinates (in this case also specify `mapY`).
;      Alternatively, specify a 2-dimensional array of map xy coordinates (in this case `mapY` is ignored).
;
;    mapY: in, optional, type={number[] | number[2][]}
;      Use this argument to specify a 1-dimensional array of map y coordinates.
;
; :Author: Andreas Rabe
;-
function hubIOImgInputImage::getPixelCoordinates, mapX, mapY
  if ~isa(mapX) then return, !null
  if ~isa(mapY) then begin
    mapCoordinates = mapX
  endif else begin
    pixelCoordinates = [transpose(mapX[*]),transpose(mapY[*])]
  endelse

  mapInfo = self.getMeta('map info')
  pixelSize = [mapInfo.sizeX, mapInfo.sizeY]
  tieMap = [mapInfo.easting, mapInfo.northing]
  tiePixel = [mapInfo.pixelX, mapInfo.pixelY]

  pixelCoordinates = dblarr(size(/DIMENSIONS, mapCoordinates))
  pixelCoordinates[0,*] = (mapCoordinates[0,*]-tieMap[0])/( pixelSize[0])+tiePixel[0]
  pixelCoordinates[1,*] = (mapCoordinates[1,*]-tieMap[1])/(-pixelSize[1])+tiePixel[1]
  return, pixelCoordinates

end

;+
; :Description:
;    This method is used to visualize a single image band or a RGB composition of three image bands.
;
; :Params:
;    bandPositions : in, optional, type={int | int[3]}, default=0
;      Use this argument to specify a single band position or an three-elemental array with image band positions
;      to visualize either a greyscale or a RGB composite image.
;
; :Keywords:
;    Stretch : in, optional, type={int | int[2]}, default=[2\, 98]
;      Use this keyword to specify the starting data range percentile (Stretch[0]) 
;      and the ending data range percentile (Stretch[1]). 
;      This is useful for contrast enhancement.
;      When specifying a scalar value p, Stretch is set to [p, 100-p].
;      
;    TrueColor : in, optional, type=boolean, default=0
;      Set this keyword to visualize true color composition.
;      
;    ColoredInfrared : in, optional, type=boolean, default=0
;      Set this keyword to visualize colored infrared composition.
;      
;    DefaultBands : in, optional, type=boolean, default=0
;      Set this keyword to visualize the default bands composition defined inside the image header file under 'default bands'.
;    _REF_EXTRA : in, optional
;      Extra keywords are passed to the `image` function.
;      
;-
pro hubIOImgInputImage::quickLook $
  , bandPositions $
  , Stretch=stretchingRange $
  , TrueColor=trueColor $
  , ColoredInfrared=coloredInfrared $
  , DefaultBands=defaultBands $
  , ImageReference=imageReference $
  , _REF_EXTRA=_ref_extra

  inputImage = hubIOImgInputImage(self.filenameData)

  ; find RGB or CIR or Default
  if keyword_set(trueColor) then begin
    bandPositions = inputImage.locateWavelength(/TrueColor)
  endif
  if keyword_set(coloredInfrared) then begin
    bandPositions = inputImage.locateWavelength(/ColoredInfrared)
  endif

  ; set default bands, if bandPositions argument not defined
  if keyword_set(defaultBands) then begin
    bandPositions = self.getMeta('default bands')
  endif

  ; set first band, if bandPositions argument still not defined
  if ~isa(bandPositions) then begin
    bandPositions = [0]
  endif
  
  ; set class colors
  if strlowcase(self.getMeta('file type')) eq 'envi classification' then begin
    rgb_table = bytarr(3, 256)
    rgb_table[0, 0] = self.getMeta('class lookup')
    noStretch = 1
  endif

  ; set data stretching range in %
  if ~isa(stretchingRange) then begin
    stretchingRange = [2,98]
  endif
  
  if n_elements(stretchingRange) eq 1 then begin
    stretchingRange = [stretchingRange, 100-stretchingRange]
  endif

  inputImage = hubIOImgInputImage(self.filenameData)
  inputImage._defineSubset, BandPositions=bandPositions
  inputImage.initReader, /Cube
  imageCube = inputImage.getData()
  obj_destroy, inputImage
  imageDimensions = size(/DIMENSIONS, imageCube)
  imageRGB = bytarr(imageDimensions)
  for i=0, n_elements(imageCube[0,0,*])-1 do begin
    
    if keyword_set(noStretch) then begin
      channel = byte(imageCube[*,*,i])
    endif else begin
      channel = imageCube[*,*,i]
      if isa(self.getMeta('data ignore value')) then begin
        channel = channel[where(/NULL, channel ne self.getMeta('data ignore value'))] 
      endif
      channel = bytscl(channel)
      channelCumulativeHistogram = total(histogram(channel, /L64), /CUMULATIVE, /PRESERVE_TYPE)
      channelDistribution = byte(100.*channelCumulativeHistogram/channelCumulativeHistogram[-1])
      stretchMin = (where(channelDistribution ge stretchingRange[0]))[0]
      stretchMax = (where(channelDistribution le stretchingRange[1]))[-1]
      channel = bytscl(bytscl(imageCube[*,*,i]) ,MIN=stretchMin, MAX=stretchMax)
    endelse
    imageRGB[0,0,i] = channel
  endfor

  image_dimensions = [self.getMeta('samples'), self.getMeta('lines')]
  dimensions = image_dimensions < get_screen_size()*0.75
  window_title = 'QuickLook - '+file_basename(self.filenameData)
  imageReference = image(imageRGB, /ORDER, DIMENSIONS=dimensions, WINDOW_TITLE=window_title, RGB_TABLE=rgb_table, _EXTRA=_ref_extra)
end

;+
; :Description:
;    Return the quick look image as a 3D byte array [samples,lines,3].
;    The Image is produced by wrapping the `hubIOImgInputImage::quickLook` method.
;
; :Params:
;    bandPositions
;      See `hubIOImgInputImage::quickLook`.
;
; :Keywords:
;    _REF_EXTRA
;      Extra keywords are passed to `hubIOImgInputImage::quickLook`.
;      
; :Examples:
;    Make a quick look::
;    
;      imageFilename = hub_getTestImage('Hymap_Berlin-A_Image')
;      imageBSQ = (hubIOImgInputImage(imageFilename)).quickLook(/TrueColor)
;      help, imageBSQ
;    
;    IDL prints::
;    
;      IMAGEBSQ        BYTE      = Array[300, 300, 3]  
;-
function hubIOImgInputImage::quickLook, bandPositions, _REF_EXTRA=_ref_extra

  self.quickLook, bandPositions, _EXTRA=_ref_extra, ImageReference=imageReference, /Buffer
  imageRGB = transpose(imageReference.copyWindow(), [1,2,0])
  imageReference.close
  return, imageRGB

end

;+
; :Description:
;    This method is used to plot a single image pixel profile.
;
; :Params:
;    sampleIndex : in, required, type=int
;      Use this keyword to specify the profile sample index.
;      
;    lineIndex : in, required, type=int
;      Use this keyword to specify the profile line index.
;
;-
pro hubIOImgInputImage::quickLookProfile $
  , sampleIndex $
  , lineIndex

  if ~isa(sampleIndex, /SCALAR) then begin
    message, 'Missing or wrong argument: sampleIndex'
  endif 

  if ~isa(lineIndex, /SCALAR) then begin
    message, 'Missing or wrong argument: lineIndex'
  endif 

  inputImage = hubIOImgInputImage(self.filenameData)
  inputImage.initReader, /Profiles
  profile = inputImage.getData([[sampleIndex],[lineIndex]])
  obj_destroy, inputImage

  window_title = 'QuickLookProfile - '+file_basename(self.filenameData)
  title = 'Pixel Profile of [Sample, Line] = ['+strcompress(/REMOVE_ALL, sampleIndex)+', '+strcompress(/REMOVE_ALL, lineIndex) +']'
  xtitle = 'band index'
  ytitle = 'value'
  !null = plot(profile, WINDOW_TITLE=window_title, TITLE=title, XTITLE=xtitle, YTITLE=ytitle)

end

pro hubIOImgInputImage::showHeader
  filename = self.getMeta('filename header')
  xdisplayfile, filename, /GROW_TO_SCREEN
end

;+
; :Description:
;    This method is used to performs all cleanup on the object and calls the `hubIOImgInputImage::finishReader` method.
;
;-
pro hubIOImgInputImage::cleanup

  self.finishReader

end

;+
; :Hidden:
;-
pro hubIOImgInputImage__define
  struct = {hubIOImgInputImage $
    , inherits hubIOImgImage $
    , subset      : {hubIOImgInputImageSubset,      sampleRange:[0ll,0ll],lineRange:[0ll,0ll],bandPositions:ptr_new(),dimensions:[0ll,0ll,0ll]} $
    , validSubset : {hubIOImgInputImageValidSubset, sampleRange:[0ll,0ll],lineRange:[0ll,0ll],dimensions:[0ll,0ll,0ll], sampleOffset:0ll, lineOffset:0ll, noOverlap:0b} $
    , binaryIO : {hubIOImgInputImageBinaryIO,type:0,typeSize:0ll,byteOrder:0,byteOrderSystem:0} $
    , readerSettings : hash() $
    , readerInitialized : 0b $
    , allowOutOfRangeAccess : 0b$
  }
end

pro test_hubIOImgInputImage

  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  inputImage = hubIOImgInputImage(inputFilename)
  boundingBox = inputImage.getMapCoordinates(/BOUNDINGBOX)
  print, boundingBox
  print, inputImage.getPixelCoordinates(boundingBox)

  
  
end
