function hubIOImgInputImageList::init
  self.images = hash()
  self.readerSettings = hash()
  return, 1b 
end

pro hubIOImgInputImageList::addImage, filename, key, ImageReference=imageReference $
    , Classification=classification, Regression=regression, SpectralLibrary=spectralLibrary $
    , DataType=dataType, Mask=mask, ForegroundMaskValue=foregroundMaskValue, BackgroundMaskValue=backgroundMaskValue, SubsetBandPositions=bandPositions

  imageReference = self.addImage(filename, key $
    , Classification=classification, Regression=regression, SpectralLibrary=spectralLibrary $ ;consistency checks
    , DataType=dataType, Mask=mask, ForegroundMaskValue=foregroundMaskValue, BackgroundMaskValue=backgroundMaskValue, SubsetBandPositions=bandPositions) 
end

function hubIOImgInputImageList::addImage, filename, key $
    , Classification=classification, Regression=regression, SpectralLibrary=spectralLibrary $
    , DataType=dataType, Mask=mask, ForegroundMaskValue=foregroundMaskValue, BackgroundMaskValue=backgroundMaskValue, SubsetBandPositions=bandPositions

  key = isa(key) ? key : self.images.count()
  readerSetting = hash('DataType',dataType, 'Mask',mask, 'ForegroundMaskValue',foregroundMaskValue, 'BackgroundMaskValue',backgroundMaskValue, 'SubsetBandPositions',bandPositions)
  (self.readerSettings)[key] = readerSetting
  ref = hubIOImgInputImage(filename, Classification=classification, Regression=regression, Mask=mask, SpectralLibrary=spectralLibrary)
  (self.images)[key] = ref
  return, ref
end

pro hubIOImgInputImageList::initReader, numberOfTileLines, neighborhoodHeight, _EXTRA=_extra
  self.checkConsistency
  ;if (hash(_extra)).hasKey('BAND') then message, 'Band Keyword not allowed.'
  
  foreach image,self.images,key do begin
    readerSetting = ( (self.readerSettings)[key] + _extra).toStruct()
    image.initReader, numberOfTileLines, neighborhoodHeight, /tileprocessing, _EXTRA=readerSetting
  endforeach
end

pro hubIOImgInputImageList::finishReader
  foreach image, self.images do begin
    image.finishReader
  endforeach
end

function hubIOImgInputImageList::getWriterSettings, SetBands=setBands, SetDataType=setDataType, _EXTRA=_extra
  if ~isa(setBands) then message, 'Undefined argument'
  if ~isa(setDataType) then message, 'Undefined argument'
  firstKey = (self.images.keys())[0]
  return, ((self.images)[firstKey]).getWriterSettings(SetBands=setBands, SetDataType=setDataType, _EXTRA=_extra)
end

pro hubIOImgInputImageList::checkConsistency
  if self.images.isEmpty() then message, 'Image list is empty.'
  firstKey = (self.images.keys())[0]
  referenceSize = ((self.images)[firstKey]).getSpatialSize()
  foreach image, self.images do begin
    if ~image.isCorrectSpatialSize(referenceSize) then begin
      message, 'Images have different spatial sizes.' 
    endif
  endforeach
end

function hubIOImgInputImageList::getData, index
  result = hash()
  foreach image, self.images, key do begin
    result[key] = image.getData(index)
  endforeach
  return, result
end

function hubIOImgInputImageList::ImageKeys
  return, (self.images).keys()
end

function hubIOImgInputImageList::hasImage, key
  return, (self.images).hasKey(key)
end

function hubIOImgInputImageList::getImage, key
  return, (self.images)[key]
end

function hubIOImgInputImageList::tileProcessingDone
  firstKey = (self.images.keys())[0]
  return, ((self.images)[firstKey]).tileProcessingDone()
end

function hubIOImgInputImageList::getMapCoordinatesBoundingBox
  boundingBox = [+1,-1,-1,+1]*!VALUES.D_INFINITY
  foreach image, self.images do begin
    boundingBoxImage = image.getMapCoordinates(/BoundingBox)
    boundingBox  = [min([boundingBoxImage[0],boundingBox[0]]),$
                    max([boundingBoxImage[1],boundingBox[1]]),$
                    max([boundingBoxImage[2],boundingBox[2]]),$
                    min([boundingBoxImage[3],boundingBox[3]])]
  endforeach
  return, boundingBox
end

pro hubIOImgInputImageList::cleanup
  foreach image, self.images do begin
    image.cleanup
  endforeach
end

pro hubIOImgInputImageList__define
  struct = {hubIOImgInputImageList, inherits IDL_Object,$
    images : hash(),$
    readerSettings : hash()}
end

pro test_hubIOImgInputImageList_masking

  imageList = hubIOImgInputImageList()
  imageReference = imageList.addImage(hub_getTestImage('Hymap_Berlin-A_Image'), 'image', DataType='float')
  imageList.addImage, hub_getTestImage('Hymap_Berlin-A_Mask'), 'mask', DataType='byte'

  outputFilename = hub_getUserTemp('my_image')
  outputImage = hubIOImgOutputImage(outputFilename)
  outputImage.copyMeta, imageReference, /CopySpatialInformation
 
  numberOfTileLines = hubIOImg_getTileLines(Image=imageReference)
  imageList.initReader, numberOfTileLines, /Cube, /TileProcessing
  bands = imageReference.getMeta('bands')
  writerSettings = imageList.getWriterSettings(SetBands=bands, SetDataType='float')
  outputImage.initWriter, writerSettings
  
  ; perform processing for each tile
  
  while ~imageList.tileProcessingDone() do begin                               
    tileDataHash = imageList.getData()
    maskedData = tileDataHash['image']
    for i=0,bands-1 do maskedData[*,*,i] *= tileDataHash['mask']                                  
    outputImage.writeData, maskedData       
    help, tileDataHash['image'], tileDataHash['mask']                              
  endwhile
   
  ; cleanup
  imageList.cleanup
  outputImage.cleanup
  
  ; show the result image
  
  (hubIOImgInputImage(outputFilename)).quickLook, imageReference.locateWavelength(/ColoredInfrared)
 
end

pro test_hubIOImgInputImageList_compositing

  defaultBands =  [24, 71, 13]

  imageList = hubIOImgInputImageList()
  imageList.addImage, hub_getTestImage('Hymap_Berlin-A_Image'), 'imageA', SubsetBandPositions=defaultBands
  imageList.addImage, hub_getTestImage('Hymap_Berlin-B_Image'), 'imageB', SubsetBandPositions=defaultBands
  boundingBox = imageList.getMapCoordinatesBoundingBox()

  outputFilename = hub_getUserTemp('my_image')
  outputImage = hubIOImgOutputImage(outputFilename)
  
  ; initialize for reading/writing image cube tiles
  numberOfTileLines = 50
  imageList.initReader, numberOfTileLines, /Cube, /TileProcessing, SubsetRectangle=boundingBox
  writerSettings = imageList.getWriterSettings(SetBands=3, SetDataType='int')
  outputImage.initWriter, writerSettings

  ; perform processing cube tile by cube tile
  while ~imageList.tileProcessingDone() do begin
    tileDataHash = imageList.getData()
    stop
    tileComposite = tileDataHash['image']
    for i=0,bands-1 do maskedData[*,*,i] *= tileDataHash['mask']
    outputImage.writeData, maskedData

    
    cubeTileData = inputImage.getData()
    outputImage.writeData, cubeTileData
  endwhile

  compositeSize = size(/DIMENSIONS, imageB)
  imageA = congrid(imageA, compositeSize[0], compositeSize[1], compositeSize[2])
  composite = imageA+imageB
  window, /FREE, XSIZE=compositeSize[0], YSIZE=compositeSize[1]
  tvscl, composite, /Order, True=3
end
