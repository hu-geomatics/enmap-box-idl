;+
;  An hubIOImgSampleSetForClassification object can be used to access classification sample set data.
;
;  For usage details also see `Image Data Input/Output API <./image-data-input-output.html>` and
;  `Accessing Sample Set Data <./sample-set.html>`.
;-  

;+
; :Description:
;    This method is called indirectly when calling the object constructor hubIOImgSampleSetForClassification().
;    The method return value is an object reference to the newly-created object.
;
; :Params:
;    filenameFeatures : in, required, type=string
;      Use this argument to specify the full path name of the feature image data file.
;      The feature image spatial size must match the label image spatial size.
;      
;    filenameLabels : in, required, type=string
;      Use this argument to specify the full path name of the label image data file.
;      The label image must be a classification image (`file type = ENVI Classification`) and its 
;      spatial size must match the feature image spatial size.
;
;-
function hubIOImgSampleSetForClassification::init $
  , filenameFeatures $
  , filenameLabels

  !null = self->hubIOImgSampleSet::init(filenameFeatures, filenameLabels)

  if ~self.imageLabels.isClassification() then begin
    message,'Label image must be a classification image.'
  endif
  
  *self.backgroundValue = 0
   
  return,1b

end

;+
; :Description:
;    This method returns the pixel locations of all labeled pixels ordered class-wise. 
;    It returns a list of arrays of one-dimensional indices, 
;    or two-dimensional indices (`result = list(classes)`).
;    The pixel locations of class k are given by result[k]. 
;    Note that the pixel locations of the unclassified class, so to say the background,
;    is not provided (`result[0]` is always the empty array []). 
;
; :Keywords:
;    TwoDimensional : in, optional, type=boolean, default=0
;      Set this keyword to return two-dimensional indices.
;-
function hubIOImgSampleSetForClassification::getClasswiseIndices $
  , TwoDimensional=twoDimensional

  indices = self.getIndices(/TwoDimensional)
  labels = byte(self.getLabels(indices))
  
  ; find class indices 
  
  !null = histogram(labels,REVERSE_INDICES=reverse_indices,MIN=1,MAX=self.imageLabels.getMeta('classes')-1,LOCATIONS=locations)
  classwiseIndices = list(!null) ; class unclassified is ignored 
  classwiseIndices = classwiseIndices + hubMathHelper.reverseIndicesToList(reverse_indices,locations)

  ; map label indices to spatial indices
  
  foreach classIndices,classwiseIndices,i do begin
    if classIndices ne !null then begin
      classwiseIndices[i] = indices[classIndices, *]
    endif
  endforeach

  return, classwiseIndices

end

pro hubIOImgSampleSetForClassification__define
  struct = {hubIOImgSampleSetForClassification $
    ,inherits hubIOImgSampleSet $
  }
end

;+
; :Hidden:
;-
pro test_hubIOImgSampleSetForClassification

  filenameFeatures = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')

  sampleSet = hubIOImgSampleSetForClassification(filenameFeatures,filenameLabels)
  indicesClasswise = sampleSet.getClasswiseIndices()
  indicesClasswise2D = sampleSet.getClasswiseIndices(/TwoDimensional)
help,indicesClasswise
  features = sampleSet.getFeatures(indicesClasswise2D[1])
  help,features

  for i=0,n_elements(features[0, *])-1 do begin
    plot, features[*, i]
    wait,0.1
  endfor

end