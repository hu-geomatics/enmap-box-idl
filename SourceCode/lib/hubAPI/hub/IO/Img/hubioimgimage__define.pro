;+
; This class is abstract, do not instantiate it. 
;   
;-

;+
;   :Hidden:
;-
function hubIOImgImage::init $
  , filename
  
  compile_opt static, strictarr
  
  if ~isa(filename, 'STRING', /SCALAR) then begin
    message, 'Argument filename must be a scalar string: '+string(filename)
  endif
  
  fileExtension = hubHelper.getFileExtension(filename) 
  if strcmp(fileExtension, 'hdr', /FOLD_CASE) then begin
    message, 'Header file selected. Please select image binary file.' 
  endif 
  
  self.filenameData = filename
  self.header = hubIOImgHeader()
  self.header.setMeta, 'filename data', self.filenameData

  return,1b

end

pro hubIOImgImage::checkFileSize

  on_error,2

  fileInfo = file_info(self.getMeta('filename data'))
  dataTypeSize = hubHelper.getDataTypeInfo(self.getMeta('data type'), /TypeSizes)

  specifiedSize = dataTypeSize*self.getMeta('samples')*self.getMeta('lines')*self.getMeta('bands')
  
  if specifiedSize ne fileInfo.size then  begin
    message,"File size and specified size do not match: specified("+strcompress(/Remove_all, specifiedSize)+") vs file("+strcompress(/Remove_all,fileInfo.size)+"), "+self.getMeta('filename data')
  endif  

end

;+
;   :Hidden:
;-
function hubIOImgImage::getMeta $
  , metaName $
  , Default=default

  result = self.header.getMeta(metaName, Default=default)
  return, result

end

;+
;   :Hidden:
;-
function hubIOImgImage::hasMeta $
  , metaNames

  allMetadataIncluded = 1
  foreach metaName, metaNames do begin
    allMetadataIncluded and= isa(self.getMeta(metaName))
  endforeach

  return, allMetadataIncluded

end

;+
; :Hidden:
;-
function hubIOImgImage::getMetaHash $
  , metaNames

  return, self.header.getMetaHash(metaNames)

end

;+
;   :Hidden:
;-
function hubIOImgImage::getInfo $
  ,infoName 

  return,self.header.getInfo(infoName)

end

;+
; :Description:
;    This method returns a hash variable that can be used as input argument for the `hubIOImgOutputImage::initWriter` methode.
;    The hash contains generel information about how the data reader was initialized::
;    
;      hash key       | type                                               | description
;      ---------------|----------------------------------------------------|------------------------------
;      data type      | {'byte' | ... | 'ulong64'}                         | numerical IDL data type
;      dataFormat     | {'band' | 'slice' | 'cube'} | 'value' | profiles}  | data format
;      tileProcessing | {0 | 1}                                            | tile or non-tile processing
;      samples        | int                                                | number of samples
;      lines          | int                                                | number of lines
;      bands          | int                                                | number of bands
;      
;    In the tile processing case, the hash additionally contains information about::
;         
;      hash key           | type  | description
;      -------------------|-------|------------------------------
;      numberOfTiles      | int   | 
;      neighborhoodHeight | int   |                  neighborhood height
;
;    In some situations it is necessary to manipulate some of the following values:
;    `'samples'`, `'lines'`, `'bands'` or `'data type'` to correctly initialize the data writer. 
;    Therefor use the `Set*` keywords to change output image writer settings, that differ from the input image.
;
; :Keywords:
;    SetSamples : in, optional, type=int
;      Use this keyword to specify the number of samples inside the result hash. 
;      Only use it if the number of samples in the input and output images differ.
;       
;    SetLines : in, optional, type=int
;      Use this keyword to specify the number of lines inside the result hash. 
;      Only use it if the number of lines in the input and output images differ.
;      
;    SetBands : in, optional, type=int
;      Use this keyword to specify the number of bands inside the result hash. 
;      Only use it if the number of bands in the input and output images differ.
;      
;    SetDataType : in, optional, type={string | int}
;      Use this keyword to specify the data type inside the result hash. 
;      Only use it if the data type in the input and output images differ.
;      
;-
function hubIOImgImage::getWriterSettings $
  , SetSamples=setSamples $
  , SetLines=setLines $
  , SetBands=setBands $
  , SetDataType=setDataType

  writerSettings = hash('samples', !null, 'lines', !null, 'bands', !null, 'data type', !null)
  
  ; change settings
  
  if isa(setSamples) then writerSettings['samples'] = setSamples
  if isa(setLines) then writerSettings['lines'] = setLines
  if isa(setBands) then writerSettings['bands'] = setBands
  if isa(setDataType) then writerSettings['data type'] = setDataType
  
  return, writerSettings

end

;+
;   :Hidden:
;-
function hubIOImgImage::getHeader

  return,self.header

end

;+
; :Description:
;    Static method to delete an image (binary file + header file).
;
; :Params:
;    path: in, required, type=string
;     Path to image.
;
;-
pro hubIOImgImage::deleteImage, path
  compile_opt static, strictarr
   
  img = hubIOImgInputImage(path)
  pathDat = img.getMeta('filename data')
  pathHdr = img.getMeta('filename header')
  img = !NULL
  FILE_DELETE, pathDat, pathHdr
end

;+
;   :Hidden:
;-
function hubIOImgImage::_overloadPrint

  return,self.header._overloadPrint()

end

pro hubIOImgImage__define
  struct = {hubIOImgImage $
    , inherits IDL_Object $
    , header : obj_new() $
    , filenameData : '' $
    , filenameHeader : '' $
    , logicalUnitNumber : 0l $
  }
end


