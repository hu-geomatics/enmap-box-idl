pro hubIOImg_test

;  spatialSubset = [385271.75, 5821155.8, 391799.25, 5818005.8]

  ; create objects
;  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  foreach inputFilename, 'E:\Andreas\Dropbox\images\'+['bsq','bil','bip'] do begin
    inputImage = hubIOImgInputImage(inputFilename)
    outputFilename = filepath('my_image', /TMP)
    outputImage = hubIOImgOutputImage(outputFilename)
    
    ; copy some metadata
    outputImage.copyMeta, inputImage, ['wavelength', 'wavelength units']
    
    ; initialize for reading/writing image cube tiles
    inputImage.initReader, /Cube, DataType='float', SubsetRectangle=spatialSubset;, SubsetBandPositions=inputImage.locateWavelength(/coloredInfrared)
    writerSettings = inputImage.getWriterSettings()
    outputImage.initWriter, writerSettings
    
    cubeData = inputImage.getData()                                 
    outputImage.writeData, cubeData                             
  
    ; cleanup
    inputImage.cleanup
    outputImage.cleanup
    
    ; show result
    (hubIOImgInputImage(outputFilename)).quickLook, /ColoredInfrared
  endforeach
end

pro hubIOImg_testTile

  spatialSubset = [385271.75, 5821155.8, 391799.25, 5818005.8]

  ; create objects
  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  
  inputImage = hubIOImgInputImage(inputFilename)
  outputFilename = filepath('my_image2', /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)

  ; copy some metadata
  outputImage.copyMeta, inputImage, ['wavelength', 'wavelength units']

  ; initialize for reading/writing image cube tiles
  numberOfTileLines = 50
  neighborhoodHeight = 5
  inputImage.initReader, numberOfTileLines, neighborhoodHeight, /Cube, /TileProcessing, DataType='float', SubsetRectangle=spatialSubset, SubsetBandPositions=spectralSubset
  writerSettings = inputImage.getWriterSettings()
  outputImage.initWriter, writerSettings
  
  ; perform processing cube tile by cube tile
  while ~inputImage.tileProcessingDone() do begin                       
    cubeTileData = inputImage.getData()                                 
    outputImage.writeData, cubeTileData                             
  endwhile

  ; cleanup
  inputImage.cleanup
  outputImage.cleanup

  ; show result
  (hubIOImgInputImage(outputFilename)).quickLook, /ColoredInfrared
end

pro hubIOImg_testReadCube
  spatialSubset = [665645.36699999997, 5304502.4919999996, 678635.36699999997, 5296492.4919999996]
  spectralSubset = [65,151,45]-1
  foreach inputFilename, 'E:\Andreas\Dropbox\images\'+['bsq','bil','bip'], i do begin
    print,inputFilename
    cube = hubIOImg_readImage(inputFilename, SubsetBandPositions=spectralSubset, SubsetRectangle=spatialSubset)
    window, i, XSIZE=n_elements(cube[*,0,0]), YSIZE=n_elements(cube[0,*,0])
    tvscl, cube, TRUE=3
    wshow, i
  endforeach
end

pro hubIOImg_testReadBand
  spatialSubset = [665645.36699999997, 5304502.4919999996, 678635.36699999997, 5296492.4919999996]
  spectralSubset = [65,151,45]-1
  foreach inputFilename, 'E:\Andreas\Dropbox\images\'+['bsq','bil','bip'], i do begin
    print,inputFilename
    bandR = hubIOImg_readBand(inputFilename, spectralSubset[0], SubsetRectangle=spatialSubset)
    bandG = hubIOImg_readBand(inputFilename, spectralSubset[1], SubsetRectangle=spatialSubset)
    bandB = hubIOImg_readBand(inputFilename, spectralSubset[2], SubsetRectangle=spatialSubset)
    cube = [[[bandR]],[[bandG]],[[bandB]]]
    window, i, XSIZE=n_elements(cube[*,0,0]), YSIZE=n_elements(cube[0,*,0])
    tvscl, cube, TRUE=3
    wshow, i
  endforeach
end

pro hubIOImg_testSubset

  spatialSubset = [385271.75, 5821155.8, 391799.25, 5818005.8]

  ; create objects
  inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')

  inputImage = hubIOImgInputImage(inputFilename)
  outputFilename = filepath('my_image2', /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)

  ; copy some metadata
  outputImage.copyMeta, inputImage, ['wavelength', 'wavelength units']

  ; initialize for reading/writing image cube tiles
  numberOfTileLines = 50
  neighborhoodHeight = 5
  inputImage.initReader, numberOfTileLines, neighborhoodHeight, /Cube, /TileProcessing, DataType='float', SubsetRectangle=spatialSubset, SubsetBandPositions=spectralSubset
  writerSettings = inputImage.getWriterSettings()
  outputImage.initWriter, writerSettings

  ; perform processing cube tile by cube tile
  while ~inputImage.tileProcessingDone() do begin
    cubeTileData = inputImage.getData()
    outputImage.writeData, cubeTileData
  endwhile

  ; cleanup
  inputImage.cleanup
  outputImage.cleanup

  ; show result
  (hubIOImgInputImage(outputFilename)).quickLook, /ColoredInfrared
end