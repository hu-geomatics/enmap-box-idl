;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    Returns an image meta information.
;
; :Params:
;    filename : in, required, type=string
;      Use this argument to specify the image filename.
;      
;    metaName : in, required, type={string | string[]}
;      Use this argument to specify the meta information to by queried.
;
;-
function hubIOImg_getMeta, filename, metaName
  inputImage = hubIOImgInputImage(filename)
  if ~isa(metaName) then begin
    metaValue = inputImage.getHeader()
  endif else begin
    if n_elements(metaName) gt 1 then begin
      metaValue = inputImage.getMetaList(metaName)
    endif else begin
      metaValue = inputImage.getMeta(metaName)
    endelse
  endelse
  return, metaValue
end