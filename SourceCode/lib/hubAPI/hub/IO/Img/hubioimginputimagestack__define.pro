;+
;  An hubIOImgInputImage object can be used to access image data and metadata.
;
;  For usage details also see
;  `Image Data Input/Output API <./_image-data-input-output.html>` and
;  `Accessing Input Images <./_input-images.html>`.
;-


function hubIOImgInputImageStack::init $ 
  , filenames $
  , StackMode=StackMode $
  , dataIgnoreValue=dataIgnoreValue
  
  if isa(stackMode) then begin
    case STRLOWCASE(stackmode) of
      'intersection' : ;ok
      'union'        : ;ok
      else : message, 'Unknown value for stackMode: '+stackmode 
    endcase
    self.stackMode = strlowcase(stackMode)
  endif else begin
    self.stackmode = 'intersection' ; default value 
  endelse
  
  self.imageInfoList = list()
  self.meta.bands = 0l
  self.meta.lines = 0l
  self.meta.samples = 0l
  self.meta.dataIgnoreValue = isa(dataIgnoreValue) ? dataIgnoreValue : 0 ;default value for filling gaps 
  
  img0 = !NULL
  mapInfo0 = !NULL
  foreach filename, filenames, i do begin
    self.addImage, filename

  endforeach
  
  self._refreshStackMeta

  return,1b
  
end



pro hubIOImgInputImageStack::addImage,filename, index

  img = hubIOImgInputImage(filename)
  imgMapInfo = img.getMeta('map info')
  
  ;first image initializations?
  
  if n_elements(self.imageInfoList) eq 0 then begin
    if isa(imgMapInfo) then begin
      self.useMapInfo = 1b
      self.meta.MapInfo = PTR_NEW(imgMapInfo)
      self.meta.BoundingBox = ptr_new(self._getBoundingBox(img))
      self.meta.samples = -1
      self.meta.lines = -1
    endif else begin
      self.useMapInfo = 0b
      self.meta.samples = img.getMeta('samples')
      self.meta.lines = img.getMeta('lines')
    endelse
  endif else begin
    
    ;check image for spatial consistency
    img0 = ((self.imageInfoList)[0]).inputImage
    if self.useMapInfo then begin
      if ~isa(imgMapInfo) then message, string(format='Image %s has no map info definition', img.getMeta('filename'))
      ;compare map infos
      mapInfo0 = *(self.meta.mapinfo)
      if ~strcmp(imgMapInfo.projection, mapInfo0.projection, /FOLD_CASE) then message, message, string(format='(%"Image %s has differing map info projection")', img.getMeta('filename header'))
      if imgMapInfo.sizeX ne mapInfo0.sizeX then message, string(format='(%"Image %s has differing map info pixel size (X direction)")', img.getMeta('filename header'))
      if imgMapInfo.sizeY ne mapInfo0.sizeY then message, string(format='(%"Image %s has differing map info pixel size (Y direction)")', img.getMeta('filename header'))
      
    endif else begin
      if ~img0.isCorrectSpatialSize(img.getSpatialSize()) then begin
        message, string(format='(%"Image \"%s\" has differing spatial size")', FILE_BASENAME(filename))
      endif
    endelse
  endelse
  
  ;add to list
  
  self.meta.bands += img.getMeta('bands')
  self.imageInfoList.add, self._createImageInfoStruct(img)
  self._refreshStackMeta
end



pro hubIOImgInputImageStack::_refreshStackMeta
  self.meta.bands = 0
  self.meta.datatype = 0
  
  bbList = list()
  foreach info, self.imageInfoList, i do begin
    img = info.inputImage
    self.meta.bands += img.getMeta('bands')
    self.meta.dataType = max([self.meta.bands, img.getMeta('data type')])
    bbList.add, info.boundingBox
  endforeach
  
  ;define stack reference system
  if self.useMapInfo then begin
    stackBB = !NULL
    case self.stackMode of
      'intersection' : stackBB = self._getBoundingBoxIntersection(bbList)
      'union' :stackBB = self._getBoundingBoxUnion(bbList)
    endcase
    if ~isa(stackBB) then begin
      message, 'Missing bounding box'
    endif else begin
      self.meta.boundingBox = ptr_new(stackBB)
    endelse 
  endif
  ;set stack map info to edge coordinate
  mi = *self.meta.mapInfo
  mi.pixelX = 0
  mi.pixelY = 0
  mi.easting = stackBB.x[0]
  mi.northing = stackBB.y[0]
  self.meta.MapInfo = ptr_new(mi)
  
end


function hubIOImgInputImageStack::_getBoundingBoxUnion, bbList
  bb = bbList[0]
  for i = 1, N_ELEMENTS(bbList)-1 do begin
    bb.x = [bbList[i].x[0] < bb.x[0], bbList[i].x[1] > bb.x[1]]
    bb.y = [bbList[i].y[0] > bb.x[0], bbList[i].y[1] < bb.y[1]]
  endfor
  return, bb
end


function hubIOImgInputImageStack::_getBoundingBoxIntersection, bbList
  bb = bbList[0]
  for i = 1, N_ELEMENTS(bbList)-1 do begin
    bbi = bbList[i]
    ;check for mssing overlapp
    if (bb.x[1] lt bbi.x[0]) or (bb.x[0] gt bbi.x[1]) then return, !NULL
    if (bb.y[1] gt bbi.y[0]) or (bb.y[0] lt bbi.y[1]) then return, !NULL
    
    ;set new bounding box
    bb.x = [bb.x[0] > bbi.x[0], bb.x[1] < bbi.x[1]]
    bb.y = [bb.y[0] < bbi.y[0], bb.y[1] > bbi.y[1]]
  endfor
  return, bb
end


function hubIOImgInputImageStack::_getBoundingBox, inputImage
  bb = {x:[0d,0d] $
       ,y:[0d,0d] $
       }
  if ~isa(inputImage) then return, bb 
 
  mapInfo = inputImage.getMeta('map info')
  
  ns = inputImage.getMeta('samples')
  nl = inputImage.getMeta('lines')
  
  ulX = mapInfo.easting - mapInfo.pixelX * mapInfo.sizeX
  ulY = mapInfo.northing + mapInfo.pixelY * mapInfo.sizeY 
  
  bb.x =[ulX, ulX + ns * mapInfo.sizeX ] 
  bb.y =[ulY, ulY - nl * mapInfo.sizeY ] 
        
  return, bb
end


function hubIOImgInputImageStack::_createImageInfoStruct, inputImage
  return, { $
          inputImage:inputImage $
          ,pxOffsetX:-9999l $
          ,pxOffsetY:-9999l $
          ,boundingBox:self._getBoundingBox(inputImage) $
          }

end


;+
; :Description:
;    This method returns a single metadata value that is specified by the `metaName` argument.
;    If the metadata is not available inside the image file header (*.hdr file), !NULL is returned.
;
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
;
; :Params:
;    metaName : in, required, type=string
;      Use this argument to specify the metadata name.
;
; :Keywords:
;    Default : in, optional
;      Use this keyword to specify a default value, which will be returned if the metadata value is undefined.
;-
function hubIOImgInputImageStack::getMeta $
  , metaName $
  , Default=default
  
  
  case metaName of
     'file type'         : return, 'ENVI Standard'
     'lines'             : return, self.nLines
     'samples'           : return, self.nSamples
     'bands'             : return, self.nBands
     'map info'          : return, self.mapInfo
     'coordinate system' : stop
     'data type'         : return, self.dataType 
     'band names'        : begin
        ml = self._getMetaList('band names')
        
        
       end       
     else : begin
       metaList = List()
       foreach info, self.imageInfoList do begin
         metaList.add, (info.inputImage).getMeta(metaName, default=default)
       endforeach
     endelse
  endcase
  

  
  
;  return, self.hubIOImgImage::getMeta(metaName, Default=default)
  
end

function hubIOImgInputImageStack::_getMetaList $
  , metaName $
  , Default=default
  
  metaList = List()
  foreach imageInfo, self.imageInfoList do begin
    metaList.add, imageInfo.inputImage.getMeta(metaName, default=default)
  endforeach
  return, metaList
end



;+
; :Description:
;    This method returns a hash of metadata that is specified by the `metaNames` argument.
;    Metadata values that are not available inside the image file header (*.hdr file) are set to !NULL.
;
;    For the complete list of metadata see `Supported Metadata <./_metadata.html>`.
;
; :Params:
;    metaNames : in, required, type=string[]
;      Use this argument to specify the metadata names.
;
;-
function hubIOImgInputImageStack::getMetaHash $
  , metaNames
  
;  return, self.hubIOImgImage::getMetaHash(metaNames)
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::getInfo $
  ,infoName
  
;  return, self.hubIOImgImage::getInfo(infoName)
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::getFilename
;  return, self.filenameData
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::getFileSize
;  return, (file_info(self.filenameData)).size
end

;+
; :Hidden:
;
; :Description:
;    This method is used to define a rectangular spatial or arbitrary spectral image subset.
;    It is called inside the `hubIOImgInputImageStack::initReader` method.
;
; :Keywords:
;    BandPositions : in, optional, type=int[]
;      Use this keyword to specify an array with image band positions that are included in the image subset. This array must not necessarily be ordered.
;
;    SampleRange : in, optional, type=int[2]
;      Use this keyword to specify the starting sample number (SampleRange[0]) and the ending sample number (SampleRange[1]).
;
;    LineRange : in, optional, type=int[2]
;      Use this keyword to specify the starting line number (LineRange[0]) and the ending line number (LineRange[1]).
;
;    Default : in, optional, type=boolean
;      Set this keyword to set the image subset to the original spatial extend and band positions.
;
pro hubIOImgInputImageStack::_defineSubset $
  ,BandPositions=bandPositions $
  ,SampleRange=sampleRange $
  ,LineRange=lineRange $
  ,Default=default

;  if keyword_set(default) then begin
;    self._defineSubset,BandPositions=l64indgen(self.getMeta('bands')),SampleRange=[0,self.getMeta('samples')-1],LineRange=[0,self.getMeta('lines')-1]
;    return
;  endif
;  
;  if isa(bandPositions) then begin
;    if total((bandPositions lt 0) or (bandPositions ge self.header.getMeta('bands'))) ne 0 then begin
;      message,'Invalid band positions.'
;    endif
;    self.subset.bandPositions = ptr_new(bandPositions)
;  endif
;  
;  if isa(sampleRange) then begin
;    if n_elements(sampleRange) ne 2 then begin
;      message,'SampleRange keyword must be a two elemental integer array.'
;    endif
;    if (sampleRange[0] lt 0) or (sampleRange[1] ge self.header.getMeta('samples')) then begin
;      message,'Invalid sample range.'
;    endif
;    self.subset.sampleRange = long64(sampleRange)
;  endif
;  if isa(lineRange) then begin
;    if n_elements(lineRange) ne 2 then begin
;      message,'LineRange keyword must be a two elemental integer array.'
;    endif
;    if (lineRange[0] lt 0) or (lineRange[1] ge self.header.getMeta('lines')) then begin
;      message,'Invalid line range.'
;    endif
;    self.subset.lineRange = long64(lineRange)
;  endif
;  
;  numberOfSamples = self.subset.sampleRange[1] - self.subset.sampleRange[0] +1
;  numberOfLines = self.subset.lineRange[1] - self.subset.lineRange[0] +1
;  numberOfBands = n_elements(*self.subset.bandPositions)
;  self.subset.dimensions = [numberOfSamples,numberOfLines,numberOfBands]
  
end

;+
; :Description:
;    This method is used to initialize reading of image data or image tile data.
;    Use the `hubIOImgInputImageStack::getData` method for subsequent data reading.
;
;    To work on a rectangular spatial or arbitrary spectral image subset,
;    use the `SubsetSampleRange`, `SubsetLineRange` and `SubsetBandPositions` keywords.
;
;    Note that this method opens the image data file and allocates a free IDL Logical Unit Number (LUN) for further reading operations.
;    After finishing all reading operations, it is strongly recommended to call the
;    `hubIOImgInputImageStack::finishReader` or `hubIOImgInputImageStack::cleanup` methods to free the LUN.
;
; :Params:
;    numberOfTileLines : in, optional, type=int
;      Use this argument to specify the number of image lines included in a single tile.
;      Use it in conjunction with the `TileProcessing` keyword.
;
;    neighborhoodHeight : in, optional, type=int, default=1
;      Use this argument to specify the spatial operator size, that is to be applied to the image.
;      Each tile will be extended by neighborhoodHeight-1 extra lines that do overlap with the first lines
;      of the next tile. Using this argument is crucial when applying spatial operators (e.g. convolution filters)
;      to band tiles or cube tiles to ensure correct results at the tile edges.
;
;      Use this argument in conjunction with the `TileProcessing` keyword, but only when reading band tiles or cube tiles,
;      do not use it when reading slice tiles or value tiles.
;
; :Keywords:
;    TileProcessing : in, optional, type=boolean, default=0
;      Set this keyword to initialize tile data reading. For (non-tile) data reading, skip it or set it to zero.
;
;    Band : in, optional, type=boolean, default=0
;      Set this keyword to read image bands.
;      Set it in conjunction with the `TileProcessing` keyword to read image band tiles.
;
;    Slice : in, optional, type=boolean, default=0
;      Set this keyword to read image slices.
;      Set it in conjunction with the `TileProcessing` keyword to read image slice tiles.
;
;    Cube : in, optional, type=boolean, default=0
;      Set this keyword to read the complete image cube.
;      Set it in conjunction with the `TileProcessing` keyword to read image cube tiles.
;
;    Value : in, optional, type=boolean, default=0
;      Set this keyword in conjunction with the `TileProcessing` keyword to read image value tiles.
;
;    Profiles : in, optional, type=boolean, default=0
;      Set this keyword to read image pixel profiles.
;
;    DataType : in, optional, type={string | int}, default=image data type
;      Use this keyword to specify the output data type of the image data returned by the `hubIOImgInputImageStack::getData` methode.
;      By default, the data type is given by the image data type.
;
;    SubsetBandPositions : in, optional, type=int[]
;      Use this keyword to specify an array with image band positions that are included in the image subset. This array must not necessarily be ordered.
;
;    SubsetSampleRange : in, optional, type=int[2]
;      Use this keyword to specify the starting sample number (SubsetSampleRange[0]) and the ending sample number (SubsetSampleRange[1]).
;
;    SubsetLineRange : in, optional, type=int[2]
;      Use this keyword to specify the starting line number (SubsetLineRange[0]) and the ending line number (SubsetLineRange[1]).
;
;    Mask : in, optional, type=boolean, default=0
;      Use this keyword to return mask data instead of image data.
;      Mask data values are zero if the original image data is equal to the image background value,
;      which is given by the image 'data ignore value' metadata. If the 'data ignore value' is undefined,
;      the background value is zero. All other mask data values are set to one, so to say the foreground.
;
;    ForegroundMaskValue : in, optional, type=number
;      Use this keyword in conjunction with the `mask` keyword to explicitly define the mask foreground value.
;      Mask data values are set to one, if the original image data is equal to the foreground value.
;      All other mask data values are set to zero, so to say the background.
;
;    BackgroundMaskValue : in, optional, type=number
;      Use this keyword in conjunction with the `mask` keyword to explicitly define the mask background value.
;      Mask data values are set to zero, if the original image data is equal to the background value.
;      All other mask data values are set to one, so to say the foreground.
;-
pro hubIOImgInputImageStack::initReader $
  , numberOfTileLines $
  , neighborhoodHeight $
  , Band=band $
  , Slice=slice $
  , Cube=cube $
  , Value=value $
  , Profiles=profiles $
  , DataType=dataType $
  , TileProcessing=tileProcessing $
  , SubsetBandPositions=bandPositions $
  , SubsetSampleRange=sampleRange $
  , SubsetLineRange=lineRange $
  , Mask=mask $
  , ForegroundMaskValue=foregroundMaskValue $
  , BackgroundMaskValue=backgroundMaskValue
  
  
  
  ; define image subset
  
;self._defineSubset, BandPositions=bandPositions, SampleRange=sampleRange, LineRange=lineRange
;  
;  ; check keywords and arguments
;  
;  dataType = isa(dataType) ? dataType : self.getMeta('data type')
;  
;  if keyword_set(tileProcessing) then begin
;  
;    conflictingKeywords = total([keyword_set(band), keyword_set(slice), keyword_set(cube), keyword_set(value)]) ne 1
;    if conflictingKeywords then begin
;      message, 'Set one of the exclusive keywords: Band, Slice, Cube or Value.'
;    endif
;    
;    if ~isa(numberOfTileLines) then begin
;      message, 'Argument numberOfTileLines is required when keyword TileProcessing is set.'
;    endif
;    
;    if ~(numberOfTileLines ge 1) then begin
;      message, 'Argument numberOfTileLines must be an integer number greater or equal to one.'
;    endif
;    
;    if (keyword_set(slice) or keyword_set(value)) and isa(neighborhoodHeight) then begin
;      message, 'Argument neighborhoodHeight is not allowed when keyword Slice or Value is set.'
;    endif
;    
;    neighborhoodHeight = isa(neighborhoodHeight) ? long64(neighborhoodHeight) : 1ll
;    correctNeighborhoodHeight = (neighborhoodHeight mod 2) and (neighborhoodHeight ge 1)
;    if neighborhoodHeight lt 1 then begin
;      message, 'Argument neighborhoodHeight must be greater equal 1.'
;    endif
;    
;    if (neighborhoodHeight mod 2) ne 1 then begin
;      message, 'Argument neighborhoodHeight must be an odd number.'
;    endif
;    
;    linesToRead = isa(lineRange) ? lineRange[1]-lineRange[0] : self.getMeta('lines')
;    if neighborhoodHeight gt linesToRead then begin
;      message, 'Argument neighborhoodHeight must be lower than or equal to the number of image lines.'
;    endif
;    
;    if ~( numberOfTileLines gt (neighborhoodHeight-1) ) then begin
;      message, 'Argument numberOfTileLines must be greater than argument neighborhoodHeight-1.'
;    endif
;    
;    ;    if keyword_set(value) then begin
;    ;      message, 'Keyword Profiles is not allowed when keyword TileProcessing is set.'
;    ;    endif
;    
;  endif else begin
;  
;    conflictingKeywords = total([keyword_set(band), keyword_set(slice), keyword_set(cube), keyword_set(profiles)]) ne 1
;    if conflictingKeywords then begin
;      message, 'Set one of the exclusive keywords: Band, Slice, Cube or Profiles.'
;    endif
;    
;    if isa(neighborhoodHeight) then begin
;      message, 'Argument neighborhoodHeight is not allowed when keyword TileProcessing is not set.'
;    endif
;    
;    if keyword_set(value) then begin
;      message, 'Keyword Value is not allowed when keyword TileProcessing is not set.'
;    endif
;    
;  endelse
;  
;  ; determine data format
;  
;  if keyword_set(band) then dataFormat = 'band'
;  if keyword_set(slice) then dataFormat = 'slice'
;  if keyword_set(cube) then dataFormat = 'cube'
;  if keyword_set(value) then dataFormat = 'value'
;  if keyword_set(profiles) then dataFormat = 'profiles'
;  
;  ; save generell reader settings
;  
;  self.readerSettings = hash()
;  (self.readerSettings)['dataFormat'] = dataFormat
;  (self.readerSettings)['samples'] = self.subset.dimensions[0]
;  (self.readerSettings)['lines'] = self.subset.dimensions[1]
;  (self.readerSettings)['bands'] = self.subset.dimensions[2]
;  (self.readerSettings)['data type'] = dataType
;  (self.readerSettings)['tileProcessing'] = keyword_set(tileProcessing)
;  (self.readerSettings)['useMask'] = keyword_set(mask)
;  if keyword_set(mask) then begin
;  
;    if isa(foregroundMaskValue) and isa(backgroundMaskValue) then begin
;      message, 'Conflicting keywords: foregroundMaskValue, backgroundMaskValue.'
;    endif
;    
;    (self.readerSettings)['maskingMode'] = 'background'
;    
;    if isa(foregroundMaskValue) then begin
;      (self.readerSettings)['maskingMode'] = 'foreground'
;    endif
;    
;    case (self.readerSettings)['maskingMode'] of
;      'background' : begin
;        if isa(backgroundMaskValue) then begin
;          (self.readerSettings)['backgroundValue'] = backgroundMaskValue
;        endif else begin
;          (self.readerSettings)['backgroundValue'] = self.getMeta('data ignore value', Default=0)
;        endelse
;      end
;      'foreground' : begin
;        (self.readerSettings)['foregroundValue'] = foregroundMaskValue
;      end
;    endcase
;    
;  endif
;  
;  ; save tile processing reader settings and initialize tile reader
;  
;  if keyword_set(tileProcessing) then begin
;  
;    (self.readerSettings)['numberOfTileLines'] = long64(numberOfTileLines) < (self.readerSettings)['lines']
;    (self.readerSettings)['neighborhoodHeight'] = long64(neighborhoodHeight)
;    self._initTileReader
;    
;  endif
;  
;  ; open file for read access and save some information for faster binary data input
;  if self.readerInitialized then begin
;    message, 'Reader already initialized. Use hubIOImgInputImageStack::finishReader before reader re-initialization.'
;  endif
;  
;  openr, logicalUnitNumber, self.filenameData, /GET_LUN
;  self.binaryIO.type = self.getMeta('data type')
;  typeInfo = hubHelper.getTypeInfo(self.binaryIO.type)
;  self.binaryIO.typeSize = typeInfo.size
;  self.binaryIO.byteOrder = self.getMeta('byte order')
;  self.binaryIO.byteOrderSystem = hubHelper.getByteOrder()
;  self.logicalUnitNumber = logicalUnitNumber
;  self.readerInitialized = 1b
  
end

;+
; :Hidden:
;-
pro hubIOImgInputImageStack::_initTileReader

  ; calculate tile information
;  
;  numberOfTiles = (self.readerSettings)['lines'] / (self.readerSettings)['numberOfTileLines']
;  
;  lineStart = l64indgen(numberOfTiles)*(self.readerSettings)['numberOfTileLines'] + self.subset.lineRange[0]
;  lineEnd = lineStart + (self.readerSettings)['numberOfTileLines']-1 + (self.readerSettings)['neighborhoodHeight']-1
;  lineEnd = lineEnd < self.subset.lineRange[1]
;  
;  ; special handling for the last tile lines
;  
;  remainingLines = self.subset.lineRange[1] - lineEnd[-1]
;  
;  if remainingLines le (self.readerSettings)['neighborhoodHeight'] then begin
;    lineEnd[-1] = self.subset.lineRange[1]
;  endif else begin
;    lineStart = [lineStart, lineStart[-1]+(self.readerSettings)['numberOfTileLines']]
;    lineEnd = [lineEnd, self.subset.lineRange[1]]
;    numberOfTiles += 1
;  endelse
;  
;  if ((self.readerSettings)['dataFormat'] eq 'band') or $
;    (((self.readerSettings)['dataFormat'] eq 'value') and (self.getMeta('interleave') eq 'bsq')) then begin
;    
;    bandIndex = l64indgen((self.readerSettings)['bands'])
;    bandIndex = rebin(bandIndex, numberOfTiles*(self.readerSettings)['bands'], /SAMPLE)
;    lineStart = (rebin(lineStart, numberOfTiles, (self.readerSettings)['bands'], /SAMPLE))[*]
;    lineEnd = (rebin(lineEnd, numberOfTiles, (self.readerSettings)['bands'], /SAMPLE))[*]
;    numberOfTiles = numberOfTiles*(self.readerSettings)['bands']
;    
;  endif
;  
;  ; save tile processing reader settings
;  
;  (self.readerSettings)['numberOfTiles'] = numberOfTiles
;  (self.readerSettings)['lineStart'] = lineStart
;  (self.readerSettings)['lineEnd'] = lineEnd
;  (self.readerSettings)['bandIndex'] = bandIndex
;  (self.readerSettings)['currentTile'] = 0ll
  
end

;+
; :Description:
;    This method is used to finish reading of image data or image tile data.
;    Call this method after finishing all reading operations to free the LUN that was allocated by the `hubIOImgInputImageStack::initReader` methode.
;    It is also called implicitly when destroying the object via the `hubIOImgInputImageStack::cleanup` method.
;-
pro hubIOImgInputImageStack::finishReader

;  if self.readerInitialized then begin
;    free_lun, self.logicalUnitNumber
;  endif
;  self.readerInitialized = 0b
  
end

;+
; :Description:
;    This method returns a hash variable that can be used as input argument for the `hubIOImgOutputImage::initWriter` methode.
;    The hash contains generel information about how the data reader was initialized::
;
;      hash key       | type                                               | description
;      ---------------|----------------------------------------------------|------------------------------
;      data type      | {'byte' | ... | 'ulong64'}                         | numerical IDL data type
;      dataFormat     | {'band' | 'slice' | 'cube'} | 'value' | profiles}  | data format
;      tileProcessing | {0 | 1}                                            | tile or non-tile processing
;      samples        | int                                                | number of samples
;      lines          | int                                                | number of lines
;      bands          | int                                                | number of bands
;
;    In the tile processing case, the hash additionally contains information about::
;
;      hash key           | type  | description
;      -------------------|-------|------------------------------
;      numberOfTiles      | int   |
;      neighborhoodHeight | int   |                  neighborhood height
;
;    In some situations it is necessary to manipulate some of the following values:
;    `'samples'`, `'lines'`, `'bands'` or `'data type'` to correctly initialize the data writer.
;    Therefor use the `Set*` keywords to change output image writer settings, that differ from the input image.
;
; :Keywords:
;    SetSamples : in, optional, type=int
;      Use this keyword to specify the number of samples inside the result hash.
;      Only use it if the number of samples in the input and output images differ.
;
;    SetLines : in, optional, type=int
;      Use this keyword to specify the number of lines inside the result hash.
;      Only use it if the number of lines in the input and output images differ.
;
;    SetBands : in, optional, type=int
;      Use this keyword to specify the number of bands inside the result hash.
;      Only use it if the number of bands in the input and output images differ.
;
;    SetDataType : in, optional, type={string | int}
;      Use this keyword to specify the data type inside the result hash.
;      Only use it if the data type in the input and output images differ.
;
;-
function hubIOImgInputImageStack::getWriterSettings $
  , SetSamples=setSamples $
  , SetLines=setLines $
  , SetBands=setBands $
  , SetDataType=setDataType
  
;  if ~self.readerInitialized then begin
;    message, 'Reader not initialized, call hubIOImgInputImageStack::initReader.'
;  endif
;  
;  keys1 = ['dataFormat', 'samples', 'lines', 'bands', 'data type', 'tileProcessing']
;  readerSettings = (self.readerSettings)[keys1]
;  
;  if (self.readerSettings)['tileProcessing'] then begin
;  
;    keys2 = ['neighborhoodHeight', 'numberOfTiles']
;    readerSettings = readerSettings + (self.readerSettings)[keys2]
;    
;    if (self.readerSettings)['dataFormat'] eq 'value' then begin
;      readerSettings['interleave'] = self.getMeta('interleave')
;    endif
;    
;  endif
;  
;  ; change settings
;  
;  if isa(setSamples) then readerSettings['samples'] = setSamples
;  if isa(setLines) then readerSettings['lines'] = setLines
;  if isa(setBands) then readerSettings['bands'] = setBands
;  if isa(setDataType) then readerSettings['data type'] = setDataType
;  
;  return, readerSettings
  
end

;+
; :Description:
;    This method returns image data or image tile data. In dependence to the reader initialization (see `hubIOImgInputImageStack::initReader`),
;    different types of data formats are returned::
;
;      data format           | reader initialization    | result dimensions
;      ----------------------|--------------------------|------------------------------
;      image band            | /Band                    | 2D - [samples, lines]
;      image slice           | /Slice                   | 2D - [bands, samples]
;      image cube            | /Cube                    | 3D - [samples, lines, bands]
;      image pixel profiles  | /Profiles                | 2D - [bands, profiles]
;                            |                          |
;      image band tile       | /Band, /TileProcessing   | 2D - [samples, tileLines]
;      image slice tile      | /Slice, /TileProcessing  | 2D - [bands, samples*tileLines]
;      image cube tile       | /Cube, /TileProcessing   | 3D - [samples, tileLines, bands]
;      image value tile      | /Value, /TileProcessing  | 1D - [samples*tileLines*bands]
;
; :Params:
;    index : in, optional, type={int | int[] | int[][2]}
;      Use this argument to either specify a) a single image band index,
;      b) a single image line index, or c) an array of one- or two-dimensional image pixel indices.
;      Use it only in the case, when the reader was initialized for non-tile reading of image bands, image slices
;      or image pixel profiles. Note that indices are always zero-based.
;
function hubIOImgInputImageStack::getData $
  ,index
  
;  if ~self.readerInitialized then begin
;    message, 'Reader not initialized, call hubIOImgInputImageStack::initReader.'
;  endif
;  
;  if (self.readerSettings)['tileProcessing'] then begin
;  
;    data = self._getTile()
;    
;  endif else begin
;  
;    case (self.readerSettings)['dataFormat'] of
;      'band'     : data = self._getBand(index)
;      'slice'    : data = self._getSlice(index)
;      'cube'     : data = self._getCube()
;      'profiles' : begin
;      
;        ; convert to 1D indices
;        if isa(index) then begin
;          is2DIndex = (size(/N_DIMENSIONS, index) eq 2)
;          if is2DIndex then begin
;            if   (total(index lt 0) ne 0) $
;              or (total(index[*,0] ge self.getMeta('samples')) ne 0) $
;              or (total(index[*,1] ge self.getMeta('lines')) ne 0) then begin
;              return, !null ; pixel indices are of ouf image range
;            endif
;            indices = index[*, 0] + index[*, 1]*self.subset.dimensions[0]
;          endif else begin
;            indices = index[*]
;            if total(indices lt 0 or indices ge (self.getMeta('samples') * self.getMeta('lines'))) ne 0 then begin
;              return, !null ; pixel indices are of ouf image range
;            endif
;          endelse
;        endif else begin
;          indices = !null
;        endelse
;        
;        data = self._getProfiles(indices)
;        
;      end
;    endcase
;  endelse
;  
;  ; convert or mask data
;  
;  if (self.readerSettings)['useMask'] then begin
;    dataSize = size(/DIMENSIONS, data)
;    case (self.readerSettings)['maskingMode'] of
;      'background' : data = data ne (self.readerSettings)['backgroundValue']
;      'foreground' : data = data eq (self.readerSettings)['foregroundValue']
;    endcase
;    !null = reform(/OVERWRITE, data, dataSize)
;  endif else begin
;    data = hubMathHelper.convertData(data, (self.readerSettings)['data type'], /NoCopy)
;  endelse
;  return, data
  
end

function hubIOImgInputImageStack::getCube
;  inputImage = hubIOImgInputImageStack(self.filenameData)
;  inputImage.initReader, /Cube
;  imageCube = inputImage.getData()
;  obj_destroy, inputImage
;  return, imageCube
end

function hubIOImgInputImageStack::getProfile, sample, line
;  self.initReader, /Profiles
;  imageProfile = self.getData([[sample], [line]])
;  self.finishReader
;  return, imageProfile
end

;+
; :Description:
;    This method returns 1 if all tiles of the image are read and 0 otherwise.
;
;-
function hubIOImgInputImageStack::tileProcessingDone

;  if ~(self.readerSettings)['tileProcessing'] then begin
;    message, 'Reader not initialized for tile processing.'
;  endif
;  
;  tileProcessingDone = (self.readerSettings)['currentTile'] eq (self.readerSettings)['numberOfTiles']
;  return, tileProcessingDone
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_getBand $
  ,index
  
;  if ~isa(index) then begin
;    return, !null
;  endif
;  
;  bandIndex = (*(self.subset.bandPositions))[index]
;  samples = self.getMeta('samples')
;  lines = self.getMeta('lines')
;  bands = self.getMeta('bands')
;  
;  bandData = make_array(self.subset.dimensions[[0,1]], TYPE=self.binaryIO.type, /NoZero)
;  !null = reform(bandData, self.subset.dimensions[[0,1]], /OVERWRITE)
;  
;  case self.getMeta('interleave') of
;    'bsq' : $
;      begin
;      offset = 1ll*bandIndex*samples*lines + samples*self.subset.lineRange[0]
;      dimensions = [samples, self.subset.dimensions[1]]
;      bandData[0,0] = (self->_readBinary(offset, dimensions))[self.subset.sampleRange[0]:self.subset.sampleRange[1], *]
;    end
;    'bil' : $
;      begin
;      dimensions = [self.subset.dimensions[0]]
;      for line=self.subset.lineRange[0],self.subset.lineRange[1] do begin
;        offset = 1ll*line*samples*bands + 1ll*bandIndex*samples + 1ll*self.subset.sampleRange[0]
;        bandData[0, line-self.subset.lineRange[0]] = self._readBinary(offset, dimensions)
;      endfor
;    end
;    'bip' : $
;      begin
;      dimensions = [bands*self.subset.dimensions[0]]
;      for line=self.subset.lineRange[0],self.subset.lineRange[1] do begin
;        offset = 1ll*line*bands*samples + 1ll*bands*self.subset.sampleRange[0]
;        lineBIP = self._readBinary(offset,dimensions)
;        bandData[0,line-self.subset.lineRange[0]] = lineBIP[bandIndex:*:bands]
;      endfor
;    end
;    
;    else : message,'Wrong interleave type.'
;  endcase
;  
;  return,bandData
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_getSlice $
  ,index
  
;  if ~isa(index) then begin
;    return, !null
;  endif
;  
;  ; save old line range and set line range to single line
;  
;  oldLineRange = self.subset.lineRange
;  self._defineSubset, LineRange=oldLineRange[0]+[index, index]
;  
;  case strlowcase(self.getMeta('interleave')) of
;    'bsq' : $
;      begin
;      sliceData = self._getCube()
;      ; remove scalar dimension
;      !null = reform(sliceData, self.subset.dimensions[[0,2]], /OVERWRITE)
;      ; transpose to BIP and fix dimensions
;      sliceData = transpose(sliceData)
;      !null = reform(sliceData, self.subset.dimensions[[2,0]], /OVERWRITE)
;    end
;    'bil' : $
;      begin
;      sliceData = self._getCube(/NoTranspose)
;      ; remove scalar dimension
;      !null = reform(sliceData, self.subset.dimensions[[0,2]], /OVERWRITE)
;      ; transpose to BIP and fix dimensions
;      sliceData = transpose(sliceData)
;      !null = reform(sliceData, self.subset.dimensions[[2,0]], /OVERWRITE)
;    end
;    'bip' : $
;      begin
;      sliceData = self._getCube(/NoTranspose)
;      ; remove scalar dimension
;      !null = reform(sliceData, self.subset.dimensions[[2,0]], /OVERWRITE)
;      ; transpose to BIP
;      ; nothing to do
;    end
;    
;    else : message,'Wrong interleave type.'
;  endcase
;  
;  ; reset line range
;  
;  self._defineSubset, LineRange=oldLineRange
;  
;  return,sliceData
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_getCube $
  , NoTranspose=noTranspose
  
;  samples = self.getMeta('samples')
;  lines = self.getMeta('lines')
;  bands = self.getMeta('bands')
;  
;  case strlowcase(self.getMeta('interleave')) of
;    'bsq' : $
;      begin
;      cubeData = make_array(self.subset.dimensions[[0,1,2]], TYPE=self.binaryIO.type, /NoZero)
;      !null = reform(cubeData, self.subset.dimensions[[0,1,2]], /OVERWRITE)
;      for bandPositionIndex=0,n_elements(*self.subset.bandPositions)-1 do begin
;        bandPosition = (*self.subset.bandPositions)[bandPositionIndex]
;        cubeData[0, 0, bandPositionIndex] = self._getBand(bandPositionIndex)
;      endfor
;    end
;    'bil' : $
;      begin
;      offset = 1ll*samples*bands*self.subset.lineRange[0]
;      dimensions = [samples, bands, self.subset.dimensions[1]]
;      cubeData = self._readBinary(offset, dimensions)
;      ; subset cube and fix dimensions
;      cubeData = cubeData[self.subset.sampleRange[0]:self.subset.sampleRange[1] $
;        , *self.subset.bandPositions $
;        , *]
;      ; fix dimensions
;      !null = reform(cubeData, self.subset.dimensions[[0,2,1]], /OVERWRITE)
;      ; transpose cube to BSQ and fix dimensions
;      if ~keyword_set(noTranspose) then begin
;        cubeData = transpose(temporary(cubeData),[0,2,1])
;        !null = reform(cubeData,self.subset.dimensions[[0,1,2]],/OVERWRITE)
;      endif
;    end
;    'bip' : $
;      begin
;      offset = 1ll*bands*samples*self.subset.lineRange[0]
;      dimensions = [bands, samples, self.subset.dimensions[1]]
;      ; read cube as BIP
;      cubeData = self._readBinary(offset, dimensions)
;      ; subset cube and fix dimensions
;      cubeData = cubeData[*self.subset.bandPositions $
;        , self.subset.sampleRange[0]:self.subset.sampleRange[1] $
;        , *]
;      ; fix dimensions
;      !null = reform(cubeData,self.subset.dimensions[[2,0,1]],/OVERWRITE)
;      ; transpose cube to BSQ and fix dimensions
;      if ~keyword_set(noTranspose) then begin
;        cubeData = transpose(temporary(cubeData),[1,2,0])
;        !null = reform(cubeData,self.subset.dimensions[[0,1,2]],/OVERWRITE)
;      endif
;    end
;    
;    else : message,'Wrong interleave type.'
;  endcase
;  
;  return,cubeData
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_getProfiles $
  , indices
  
;  ; check argument
;  
;  if ~isa(indices) then begin
;    return, !null
;  endif
;  
;  ; sort indices
;  
;  indicesSortedIndices = sort(indices)
;  sortedIndices = indices[indicesSortedIndices]
;  
;  ; convert 1D to 2D indices
;  
;  sampleIndices = sortedIndices mod self.subset.dimensions[0]
;  lineIndices = sortedIndices / self.subset.dimensions[0]
;  
;  ; find the first and last occurrence of samples in every line
;  
;  lastOccurrence = uniq(lineIndices)
;  firstOccurrence = shift(lastOccurrence,+1)+1
;  firstOccurrence[0] = 0
;  uniqLines = lineIndices[firstOccurrence]
;  
;  ; prepare result variable
;  
;  profiles = make_array(self.subset.dimensions[2], n_elements(sortedIndices)$
;    , TYPE=self.binaryIO.type)
;    
;  ; read each line (slice) that occurres and extract the specific samples
;  
;  foreach line,uniqLines,i do begin
;    currentSlice = self._getSlice(line)
;    currentSampleIndices = sampleIndices[firstOccurrence[i]:lastOccurrence[i]]
;    currentProfiles = currentSlice[*, currentSampleIndices]
;    profiles[*, firstOccurrence[i]:lastOccurrence[i]] = currentProfiles
;  endforeach
;  
;  return, profiles[*, sort(indicesSortedIndices)]
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_getTile

;  ; save old line range and set line range to tile line range
;  
;  oldLineRange = self.subset.lineRange
;  self._defineSubset, LineRange=[((self.readerSettings)['lineStart'])[(self.readerSettings)['currentTile']], ((self.readerSettings)['lineEnd'])[(self.readerSettings)['currentTile']]]
;  
;  ; read tile
;  
;  case (self.readerSettings)['dataFormat'] of
;    'band' : begin
;      bandIndex = ((self.readerSettings)['bandIndex'])[(self.readerSettings)['currentTile']]
;      tileData = self._getBand(bandIndex)
;    end
;    'slice' : begin
;      tileData = self._getCube(/NoTranspose)
;      case self.getMeta('interleave') of
;        'bsq' : tileData = transpose(tileData, [2, 0, 1])
;        'bil' : tileData = transpose(tileData, [1, 0, 2])
;        'bip' : ; do nothing
;      endcase
;      !null = reform(tileData, self.subset.dimensions[2], self.subset.dimensions[0]*self.subset.dimensions[1], /OVERWRITE)
;    end
;    
;    'cube' : begin
;      tileData = self._getCube()
;    end
;    
;    'value' : begin
;      case self.getMeta('interleave') of
;        'bsq' : begin
;          tileData = self._getCube(/NoTranspose)
;          bandIndex = ((self.readerSettings)['bandIndex'])[(self.readerSettings)['currentTile']]
;          tileData = self._getBand(bandIndex)
;        end
;        'bil' : tileData = self._getCube(/NoTranspose)
;        'bip' : tileData = self._getCube(/NoTranspose)
;      endcase
;      !null = reform(tileData, n_elements(tileData), /OVERWRITE)
;    end
;    
;  endcase
;  
;  ; reset line range
;  
;  self._defineSubset, LineRange=oldLineRange
;  
;  ; increase tile counter
;  
;  (self.readerSettings)['currentTile'] = (self.readerSettings)['currentTile']+1
;  
;  ; return output
;  
;  return,tileData
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_readBinary $
  , offset $
  , dimensions
  
  ; read from file
  
;  result = make_array(TYPE=self.binaryIO.type, dimensions, /NOZERO)
;  point_lun, self.logicalUnitNumber, offset*self.binaryIO.typeSize
;  readu, self.logicalUnitNumber, result
;  
;  ; swap endian if necessary
;  
;  if self.binaryIO.byteOrder ne self.binaryIO.byteOrderSystem then begin
;    swap_endian_inplace, result
;  endif
;  
;  ; fix dimensions
;  
;  !null = reform(result, dimensions,/OVERWRITE)
;  
;  return, result
  
end

;+
; :Hidden:
;-
pro hubIOImgInputImageStack::_checkReadPermission

;  if ~ (hubIOHelper()).fileReadable(self.filenameData) then begin
;    message, 'Data file is not readable: '+self.filenameData
;  endif
;  if ~ (hubIOHelper()).fileReadable(self.filenameHeader) then begin
;    message, 'Header file is not readable: '+self.filenameHeader
;  endif
  
end

;+
; :Hidden:
;-
function hubIOImgInputImageStack::_overloadPrint

;  return,self.header._overloadPrint()
  
end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid classification image, 0 (false) otherwise.
;-
function hubIOImgInputImageStack::isClassification

;  isClassification = 1
;  isClassification and= self.getMeta('bands') eq 1
;  isClassification and= strcmp(/FOLD_CASE, self.getMeta('file type'), 'ENVI Classification')
;  return, isClassification
  
end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid regression image, 0 (false) otherwise.
;-
function hubIOImgInputImageStack::isRegression

;  isRegression = 1
;  isRegression and= self.getMeta('bands') eq 1
;  ;  isRegression and= strcmp(/FOLD_CASE, self.getMeta('file type'), 'EnMAP-Box Regression')
;  isRegression and= isa(self.getMeta('data ignore value'))
;  return, isRegression
  
end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid mask image, 0 (false) otherwise.
;-
function hubIOImgInputImageStack::isMask

;  isMask = 1
;  isMask and= self.getMeta('bands') eq 1
;  return, isMask
  
end

;+
; :Description:
;    This method returns 1 (true) if the image is a valid spectral library, 0 (false) otherwise.
;-
function hubIOImgInputImageStack::isSpectralLibrary

;  isSpecLib = 1
;  isSpecLib and= strcmp(self.getMeta('file type'), 'ENVI Spectral Library', /Fold_Case)
;  return, isSpecLib
  
end

;+
; :Description:
;    This method returns 1 (true) if the image spatial size is equal to the size specified
;    by the `spatialSize` argument, 0 (false) otherwise.
;
; :Params:
;    spatialSize : in, required, type=number[2]
;      Use this argument to specify the spatial size for the test.
;
;-
function hubIOImgInputImageStack::isCorrectSpatialSize $
  , spatialSize
  
;  isCorrectSpatialSize = 1
;  selfSpatialSize = self.getSpatialSize()
;  
;  isCorrectSpatialSize and= selfSpatialSize[0] eq spatialSize[0]
;  isCorrectSpatialSize and= selfSpatialSize[1] eq spatialSize[1]
;  
;  return, isCorrectSpatialSize
  
end

;+
; :Description:
;    This method returns 1 (true) if the image spectral size is equal to the size specified
;    by the `spectralSize` argument, 0 (false) otherwise.
;
; :Params:
;    spectralSize : in, required, type=number[2]
;      Use this argument to specify the spectral size for the test.
;
;-
function hubIOImgInputImageStack::isCorrectSpectralSize $
  , spectralSize
  
  
;  isCorrectSpectralSize = 1
;  if strcmp(self.getMeta('file type'), 'envi spectral Library', /FOLD_CASE) then begin
;    isCorrectSpectralSize and= self.getMeta('samples') eq spectralSize
;  end else begin
;    isCorrectSpectralSize and= self.getMeta('bands') eq spectralSize
;  endelse
;  return, isCorrectSpectralSize
  
end

;+
; :Description:
;    This method returns the image spatialSize (result = [samples, lines]).
;-
function hubIOImgInputImageStack::getSpatialSize

;  spatialSize = [self.getMeta('samples'), self.getMeta('lines')]
;  return, spatialSize
  
end

;+
; :Description:
;    This method returns the image spectralSize (result = [bands]).
;-
function hubIOImgInputImageStack::getSpectralSize

;  spectralSize = self.getMeta('bands')
;  return, spectralSize
  
end

;+
; :Description:
;    This method returns 1 (true) if the image metadata includes all metadata specified
;    by the `metaNames` argument, 0 (false) otherwise.
;
; :Params:
;    metaNames : in, required, type=string[]
;      Use this argument to specify the metadata for the test.
;
;-
function hubIOImgInputImageStack::hasMeta $
  , metaNames
  
;  return, self.hubIOImgImage::hasMeta(metaNames)
  
end

;+
; :Description:
;    This method returns band positions of the wavelengths locations specified by the `wavelengths` argument.
;
; :Params:
;    wavelengths : in, optional, type=float[]
;      Use this argument to specify an array with wavelengths locations in nanometers.
;
; :Keywords:
;    TrueColor : in, optional, type=boolean, default=0
;      Set this keyword to return the band positions for true color (640, 550 and 460 nanometer).
;
;    ColoredInfrared : in, optional, type=boolean, default=0
;      Set this keyword to return the band positions for colored infrared (860, 650 and 550 nanometer).
;
;-
function hubIOImgInputImageStack::locateWavelength $
  , wavelengths $
  , TrueColor=trueColor $
  , ColoredInfrared=coloredInfrared
  
;  return, self.header.locateWavelength(wavelengths, TrueColor=trueColor, ColoredInfrared=coloredInfrared)
  
end

;+
; :Description:
;    This method is used to visualize a single image band or a RGB composition of three image bands.
;
; :Params:
;    bandPositions : in, optional, type={int | int[3]}, default=0
;      Use this argument to specify a single band position or an three-elemental array with image band positions
;      to visualize either a greyscale or a RGB composite image.
;
; :Keywords:
;    Stretch : in, optional, type={int | int[2]}, default=[2\, 98]
;      Use this keyword to specify the starting data range percentile (Stretch[0])
;      and the ending data range percentile (Stretch[1]).
;      This is useful for contrast enhancement.
;      When specifying a scalar value p, Stretch is set to [p, 100-p].
;
;    TrueColor : in, optional, type=boolean, default=0
;      Set this keyword to visualize true color composition.
;
;    ColoredInfrared : in, optional, type=boolean, default=0
;      Set this keyword to visualize colored infrared composition.
;
;    DefaultBands : in, optional, type=boolean, default=0
;      Set this keyword to visualize the default bands composition defined inside the image header file under 'default bands'.
;    _REF_EXTRA : in, optional
;      Extra keywords are passed to the `image` function.
;
;-
pro hubIOImgInputImageStack::quickLook $
  , bandPositions $
  , Stretch=stretchingRange $
  , TrueColor=trueColor $
  , ColoredInfrared=coloredInfrared $
  , DefaultBands=defaultBands $
  , ImageReference=imageReference $
  , _REF_EXTRA=_ref_extra
  
;  inputImage = hubIOImgInputImageStack(self.filenameData)
;  
;  ; find RGB or CIR or Default
;  if keyword_set(trueColor) then begin
;    bandPositions = inputImage.locateWavelength(/TrueColor)
;  endif
;  if keyword_set(coloredInfrared) then begin
;    bandPositions = inputImage.locateWavelength(/ColoredInfrared)
;  endif
;  
;  ; set default bands, if bandPositions argument not defined
;  if keyword_set(defaultBands) then begin
;    bandPositions = self.getMeta('default bands')
;  endif
;  
;  ; set first band, if bandPositions argument still not defined
;  if ~isa(bandPositions) then begin
;    bandPositions = [0]
;  endif
;  
;  ; set class colors
;  if strlowcase(self.getMeta('file type')) eq 'envi classification' then begin
;    rgb_table = bytarr(3, 256)
;    rgb_table[0, 0] = self.getMeta('class lookup')
;    noStretch = 1
;  endif
;  
;  
;  ; set data stretching range in %
;  if ~isa(stretchingRange) then begin
;    stretchingRange = [2,98]
;  endif
;  
;  if n_elements(stretchingRange) eq 1 then begin
;    stretchingRange = [stretchingRange, 100-stretchingRange]
;  endif
;  
;  inputImage = hubIOImgInputImageStack(self.filenameData)
;  inputImage._defineSubset, BandPositions=bandPositions
;  inputImage.initReader, /Cube
;  imageCube = inputImage.getData()
;  obj_destroy, inputImage
;  imageDimensions = size(/DIMENSIONS, imageCube)
;  imageRGB = bytarr(imageDimensions)
;  for i=0, imageDimensions[2]-1 do begin
;  
;    if keyword_set(noStretch) then begin
;      channel = byte(imageCube[*,*,i])
;    endif else begin
;      channel = imageCube[*,*,i]
;      if isa(self.getMeta('data ignore value')) then begin
;        channel = channel[where(/NULL, channel ne self.getMeta('data ignore value'))]
;      endif
;      channel = bytscl(channel)
;      channelCumulativeHistogram = total(histogram(channel, /L64), /CUMULATIVE, /PRESERVE_TYPE)
;      channelDistribution = byte(100.*channelCumulativeHistogram/channelCumulativeHistogram[-1])
;      stretchMin = (where(channelDistribution ge stretchingRange[0]))[0]
;      stretchMax = (where(channelDistribution le stretchingRange[1]))[-1]
;      channel = bytscl(bytscl(imageCube[*,*,i]) ,MIN=stretchMin, MAX=stretchMax)
;    endelse
;    imageRGB[0,0,i] = channel
;  endfor
;  
;  image_dimensions = [self.getMeta('samples'), self.getMeta('lines')]
;  dimensions = image_dimensions < get_screen_size()*0.75
;  window_title = 'QuickLook - '+file_basename(self.filenameData)
;  imageReference = image(imageRGB, /ORDER, DIMENSIONS=dimensions, WINDOW_TITLE=window_title, RGB_TABLE=rgb_table, _EXTRA=_ref_extra)
  
end

;+
; :Description:
;    Return the quick look image as a 3D byte array [samples,lines,3].
;    The Image is produced by wrapping the `hubIOImgInputImageStack::quickLook` method.
;
; :Params:
;    bandPositions
;      See `hubIOImgInputImageStack::quickLook`.
;
; :Keywords:
;    _REF_EXTRA
;      Extra keywords are passed to `hubIOImgInputImageStack::quickLook`.
;
; :Examples:
;    Make a quick look::
;
;      imageFilename = hub_getTestImage('Hymap_Berlin-A_Image')
;      imageBSQ = (hubIOImgInputImageStack(imageFilename)).quickLook(/TrueColor)
;      help, imageBSQ
;
;    IDL prints::
;
;      IMAGEBSQ        BYTE      = Array[300, 300, 3]
;-
function hubIOImgInputImageStack::quickLook, bandPositions, _REF_EXTRA=_ref_extra

;  self.quickLook, bandPositions, _EXTRA=_ref_extra, ImageReference=imageReference, /Buffer
;  imageRGB = transpose(imageReference.copyWindow(), [1,2,0])
;  imageReference.close
;  return, imageRGB
  
end

;+
; :Description:
;    This method is used to plot a single image pixel profile.
;
; :Params:
;    sampleIndex : in, required, type=int
;      Use this keyword to specify the profile sample index.
;
;    lineIndex : in, required, type=int
;      Use this keyword to specify the profile line index.
;
;-
pro hubIOImgInputImageStack::quickLookProfile $
  , sampleIndex $
  , lineIndex
  
;  if ~isa(sampleIndex, /SCALAR) then begin
;    message, 'Missing or wrong argument: sampleIndex'
;  endif
;  
;  if ~isa(lineIndex, /SCALAR) then begin
;    message, 'Missing or wrong argument: lineIndex'
;  endif
;  
;  inputImage = hubIOImgInputImageStack(self.filenameData)
;  inputImage.initReader, /Profiles
;  profile = inputImage.getData([[sampleIndex],[lineIndex]])
;  obj_destroy, inputImage
;  
;  window_title = 'QuickLookProfile - '+file_basename(self.filenameData)
;  title = 'Pixel Profile of [Sample, Line] = ['+strcompress(/REMOVE_ALL, sampleIndex)+', '+strcompress(/REMOVE_ALL, lineIndex) +']'
;  xtitle = 'band index'
;  ytitle = 'value'
;  !null = plot(profile, WINDOW_TITLE=window_title, TITLE=title, XTITLE=xtitle, YTITLE=ytitle)
;  
end

pro hubIOImgInputImageStack::showHeader
;  filename = self.getMeta('filename header')
;  xdisplayfile, filename, /GROW_TO_SCREEN
end

;+
; :Description:
;    This method is used to performs all cleanup on the object and calls the `hubIOImgInputImageStack::finishReader` method.
;
;-
pro hubIOImgInputImageStack::cleanup

  self.finishReader
  
end



;+
; :Hidden:
;-
pro hubIOImgInputImageStack__define
  struct = {hubIOImgInputImageStack $
    , inherits IDL_Object $
    , imageInfoList: obj_new() $
    , useMapInfo:0b $
    , stackMode:'' $
    , meta: {StackMetaData $
            , mapInfo: ptr_new() $
            , boundingBox : ptr_new() $
            , bands : 0u $
            , lines : 0u $
            , samples : 0u $
            , dataType : 0b $
            , dataIgnoreValue : 0d $
            } $
  }
   
end

pro test_hubIOImgInputImageStack

  imageNames = [ $;'D:\TestShrink\shrinkAF_LC_Training' $ 
               hub_getTestImage('Hymap_Berlin-A_Image') $
               ,hub_getTestImage('Hymap_Berlin-A_Classification-Estimation') $
               ,hub_getTestImage('Hymap_Berlin-A_Image') $
               ]
               
  testDir = 'Z:\BJ\TestData\'
  imageNames = [FILEPATH('test2006', ROOT_DIR=testDir) $
               ,FILEPATH('test2011', ROOT_DIR=testDir) $
               ]
  inputImageStack = hubIOImgInputImageStack(imageNames)
  print, 'Test done'
end
