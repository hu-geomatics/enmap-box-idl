function hubIOImgHeader_parseHeader, filename, Lines=lines, tolerant=tolerant
  
  if ~(hubIOHelper()).fileReadable(filename) then begin
    message,'Can not open header file: '+string(filename)
  endif
  ascii = (hubIOASCIIHelper()).readFile(filename, Lines=lines)

  ; parse header
  
  metaList = list()
  numberOfOpenBrackets = 0
  
  for lineIndex=0, n_elements(ascii)-1 do begin
   
    ; delete leading/ending blank space
    ascii[lineIndex] = strtrim(ascii[lineIndex],2)
    
    ; ignore empty lines
    if ascii[lineIndex] eq '' then continue
    
    ; ignore 'ENVI' line
    if strcmp(ascii[lineIndex],'envi',/FOLD_CASE) then continue
    
    ; delete line breaks
    case numberOfOpenBrackets of
      0 : begin
            if stregex(ascii[lineIndex], '^[a-zA-Z]+[^=]*=', /Boolean) then begin
              metaList.add, ascii[lineIndex]
            endif
          end
      1 : metaList[-1] = metaList[-1] + ascii[lineIndex]
      else : message,'can not parse header'
    endcase
    
    ; calculate number of brackets
    currentOpeningBrackets = n_elements(strsplit(ascii[lineIndex],'{',/PRESERVE_NULL))-1
    currentClosingBrackets = n_elements(strsplit(ascii[lineIndex],'}',/PRESERVE_NULL))-1
    if (currentOpeningBrackets gt 1) or (currentClosingBrackets gt 1) then begin
      if ~keyword_set(tolerant) then message,'can not parse header'
    endif else begin
      numberOfOpenBrackets += (currentOpeningBrackets-currentClosingBrackets)
    endelse
  endfor 

  ; extract names and values as strings
  names = list()
  values = list() 
  for metaIndex=0,n_elements(metaList)-1 do begin
    splittedLine = strsplit(metaList[metaIndex], '=', /EXTRACT)
    if (keyword_set(tolerant) && n_elements(splittedLine) ge 2) or $
       ~keyword_set(tolerant) then begin
      names.add, splittedLine[0]
      values.add, strjoin(splittedLine[1:*], '=')
    endif
  endfor 

  ; create list of meta information
  metas = list()
  for metaIndex=0,n_elements(metaList)-1 do begin
    metaName = names[metaIndex]
    value = values[metaIndex]
    meta = hubIOImgMeta_createFromEnviString(names[metaIndex], values[metaIndex])
    metas.add, meta
  endfor

  return, metas

end


pro test_hubIOImgHeader_parseHeader

  filename = hub_getTestImage('Hymap_Berlin-A_Image')+'.hdr'
  metas = hubIOImgHeader_parseHeader(filename, /Tolerant)
  foreach meta, metas do begin
    print, meta
  endforeach 

end