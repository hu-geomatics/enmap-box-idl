function hubIOImgMeta_wavelengthUnits::init
  self.setName, 'wavelength units'
  self.setEnumeration, ['micrometers', 'nanometers', 'wavenumber', 'gigahertz', 'megahertz', 'index'], $ 
                       ['mm',          'nm',         'wn',         'ghz',       'mhz',       'index']
     ValueDescriptions=['Micrometers', 'Nanometers', 'Wavenumber', 'Gigahertz', 'Megahertz', 'Index']
  self.setType, 'string'
  return, 1b
end

pro hubIOImgMeta_wavelengthUnits__define
  struct = {hubIOImgMeta_wavelengthUnits $
    , inherits hubIOImgMetaEnumeration}
end

