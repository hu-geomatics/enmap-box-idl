function hubIOImgMetaScalar::getValueFromENVIValue, stringArray
  value = self.hubIOImgMetaTyped::getValueFromENVIValue(stringArray[0])
  return, value
end

pro hubIOImgMetaScalar__define
  struct = {hubIOImgMetaScalar $
    , inherits hubIOImgMetaTyped $
  }
end
