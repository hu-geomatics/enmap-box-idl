function hubIOImgMetaEnumeration::castValue, value
  if isa(value) then begin
    value_ = strlowcase(hubMathHelper.convertData(value[0], 'string'))
    if ~self.values.hubIsElement(value_) then begin
      if self.alias.hubIsElement(value_, Index=aliasIndex) then begin
        if isa(aliasIndex) then begin
          value_ = (self.values)[aliasIndex]
        endif else begin
          value_ = !null
        endelse
      endif else begin
        value_ = !null
      endelse
    endif
  endif else begin
    value_ = !null
  endelse
  
  castedValue = self.hubIOImgMetaTyped::castValue(value_)
  return, castedValue
end

pro hubIOImgMetaEnumeration::setEnumeration, values, alias, ValueDescriptions=valueDescriptions

  self.values = list(values, /Extract)
  self.alias = list(alias, /Extract)
  self.valueDescriptions = list(valueDescriptions, /Extract)

end

function hubIOImgMetaEnumeration::getValueDescriptions
  return, self.valueDescriptions.ToArray()
end

function hubIOImgMetaEnumeration::isConsistentValue, value, Message=message
  message = 'Wrong enumeration value. Set one of the following: '+strjoin(strupcase(self.values.toArray()), ', ')
  if isa(value) then begin
    isConsistent = self.values.hubIsElement(strlowcase(value))
  endif else begin
    isConsistent = 1b
  endelse
  return, isConsistent
end

pro hubIOImgMetaEnumeration__define
  struct = {hubIOImgMetaEnumeration $
    , inherits hubIOImgMetaScalar $
    , values : list() $
    , alias : list() $
    , valueDescriptions : list() $
  }
end
