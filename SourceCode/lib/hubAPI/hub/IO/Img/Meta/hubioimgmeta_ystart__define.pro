function hubIOImgMeta_yStart::init
  self.setName, 'y start'
  self.setType, 'long64'
  self.isZeroBased = 1b
  return, 1b
end

pro hubIOImgMeta_yStart__define
  struct = {hubIOImgMeta_yStart $
    , inherits hubIOImgMetaScalar}
end
