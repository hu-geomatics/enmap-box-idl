;+
; :Hidden:
;-

function hubIOImgMeta_createFromValue, name, value

  meta = hubIOImgMeta_create(name)
  meta.setValue, value
  return, meta
  
end

pro test_hubIOImgMeta_createFromValue
 ; meta = hubIOImgMeta_createFromValue('data type', 'byte')
  meta = hubIOImgMeta_createFromValue('band names', ['a,1','b,2'])
  
  
  print, meta
  help, meta
end

