function hubIOImgMeta_byteOrder::init
  self.setName, 'byte order'
  self.setEnumeration, [ 0,        1], $
                       ['little', 'big'], $
     ValueDescriptions=['little endian', 'big endian']
  self.setType, 'int'
  return, 1b
end

pro hubIOImgMeta_byteOrder__define
  struct = {hubIOImgMeta_byteOrder $
    , inherits hubIOImgMetaEnumeration}
end

