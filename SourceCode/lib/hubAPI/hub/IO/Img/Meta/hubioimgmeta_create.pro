;+
; :Hidden:
;-

function hubIOImgMeta_create, name

  name_ = strcompress(/REMOVE_ALL, name)
  object_structure = 'hubIOImgMeta_'+name_+'__define'
  if (hubRoutineInfo(object_structure)).missing then begin
    meta = obj_new('hubIOImgMeta')
    meta.setName, name
  endif else begin
    meta = obj_new('hubIOImgMeta_'+name_)
  endelse
  return, meta
end

pro test_hubIOImgMeta_create
  
  meta1 = hubIOImgMeta_create('file type')
  meta2 = hubIOImgMeta_create('new meta')

  print, meta1
  print, meta2
  help, meta1, meta2
  
end

