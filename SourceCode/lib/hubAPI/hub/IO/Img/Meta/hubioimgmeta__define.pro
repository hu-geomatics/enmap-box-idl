;+
; :Hidden:
;-

pro hubIOImgMeta::setName, name
  self.name = strlowcase(strtrim(name, 2))
end

function hubIOImgMeta::getName
  return, self.name
end

pro hubIOImgMeta::setValue, value
  if isa(value) then begin
    if ~self.isConsistentValue(value, Message=message) then begin
      message, strjoin(message, string(13b))
    endif
    value_ = value
    if isa(value_, 'string') && n_elements(value_) gt 1 then begin
      foreach element, value_, i do begin
        bytes = byte(element)
        bytes[where(/NULL, bytes eq hub_fix0d(byte(',')))] = byte(';')
        value_[i] = string(bytes)
      endforeach
    endif
  endif else begin
    value_ = !null 
  endelse
  self.value = ptr_new(value_)
end

function hubIOImgMeta::getValue
  if ptr_valid(self.value) then begin
    result = *(self.value)
  endif else begin
    result = !null
  endelse
    
  return, result
end

pro hubIOImgMeta::setValueFromENVIString, ENVIString
  
  ;is this a collection enclosed by {}?
  if stregex(ENVIString, '{[^{]+}', /BOOLEAN) then begin
    ENVIValue = strtrim(strsplit(strtrim(ENVIString, 2), '{},', /EXTRACT), 2)
  endif else begin
    ENVIValue = strtrim(ENVIString,2)
  endelse
  
  
  if ~self.isConsistentENVIValue(ENVIValue, Message=message) then begin
    message, messageString
  endif
  value = self.getValueFromENVIValue(ENVIValue)
  if n_elements(value) eq 1 then begin
    value = value[0]
  endif
  self.setValue, value
end

function hubIOImgMeta::getValueAsENVIString
  result = list()
  if self.isDefined() then begin
    value = self.getValueAsENVIValue()
    if n_elements(value) ge 2 then begin
      result.add, '{'
      result.add, [value[0:-2]+',', value[-1]], /EXTRACT
      result.add, '}' 
    endif else begin
      result.add, value
    endelse
  endif
  result = result.toArray()
  return, result
end

function hubIOImgMeta::getValueAsENVIValue
  value = self.getValue()
  if self.isZeroBased then begin
    value += 1
  endif
  if typename(value) eq 'BYTE' then begin
    value = fix(value)
  endif
  ENVIValue = strtrim(value, 2) 
  return, ENVIValue
end

function hubIOImgMeta::isConsistentValue, value, Message=message
  return, 1b
end

function hubIOImgMeta::isConsistentENVIValue, ENVIValue, Message=message
  return, 1b
end

function hubIOImgMeta::getValueFromENVIValue, ENVIValue
  value = ENVIValue
  if self.isZeroBased then begin
    value -= 1
  endif
  return, value
end

function hubIOImgMeta::isEqual, meta

  if typename(self) ne typename(meta) then begin
    result = 1b
  endif else begin
    result = hubEQ(self.getValue(), meta.getValue(), /Scalar)    
  endelse

  return, result
end

function hubIOImgMeta::isDefined
  return, isa(self.getValue())
end

function hubIOImgMeta::_overloadPrint

  value = self.getValue()
  result = self.getName()+' = '
  enviString = self.getValueAsENVIString()
  result += isa(enviString) ? strjoin(enviString) : '!NULL' 
  result += ' ('+typename(value)
  dims = size(/DIMENSIONS, value)
  if n_elements(value) ge 2 then begin
    result += strjoin('['+strcompress(/REMOVE_ALL, dims)+']')
  endif
  result += ')'
  return, result
end

pro hubIOImgMeta__define
  struct = {hubIOImgMeta $
    , inherits IDL_Object $
    , name : '' $
    , value : ptr_new() $
    , isZeroBased : 0b $
  }
end

pro test_hubIOImgMeta
  meta = hubIOImgMeta_bands()
  meta.setValue, 'x23y'
  print, meta
end