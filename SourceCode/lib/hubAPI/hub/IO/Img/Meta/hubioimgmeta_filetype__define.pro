function hubIOImgMeta_fileType::init
  self.setName, 'file type'
  self.setEnumeration, ['envi standard', 'envi classification', 'envi spectral library', 'enmap-box regression'], $
      ValueDescription=['ENVI Standard', 'ENVI Classification', 'ENVI Spectral Library', 'EnMAP-Box Regression']
  self.setType, 'string'
  return, 1b
end

pro hubIOImgMeta_filetype__define
  struct = {hubIOImgMeta_fileType $
    , inherits hubIOImgMetaEnumeration}
end
