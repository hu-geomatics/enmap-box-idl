function hubIOImgMeta_dataOffsetValues::init
  self.setName, 'data gain values'
  self.setType, 'double'
  return, 1b
end

pro hubIOImgMeta_dataOffsetValues__define
  struct = {hubIOImgMeta_dataOffsetValues $
    , inherits hubIOImgMetaArray}
end

