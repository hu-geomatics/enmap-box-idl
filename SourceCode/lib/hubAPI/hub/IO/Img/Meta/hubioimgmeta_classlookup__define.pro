function hubIOImgMeta_classLookup::init
  self.setName, 'class lookup'
  self.setType, 'long64'
  return, 1b
end

function hubIOImgMeta_classLookup::getValue

  value = self.hubIOImgMetaArray::getValue()
  if isa(value) then begin
    result = reform(/OVERWRITE, value, 3, n_elements(value)/3)
  endif else begin
    result = !null
  endelse
  return, result
  
end

pro hubIOImgMeta_classLookup__define
  struct = {hubIOImgMeta_classLookup $
    , inherits hubIOImgMetaArray}
end

pro test_hubIOImgMeta_classLookup 

  meta = hubIOImgMeta_classLookup()
  meta.setValueFromENVIString, '{0,   0,   0,   0, 255,   0, 255,   0,   0, 255, 255,   0,   0, 255, 255,   0,   0, 255}'
  print, meta

end