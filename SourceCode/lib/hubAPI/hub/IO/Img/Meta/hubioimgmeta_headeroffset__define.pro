function hubIOImgMeta_headerOffset::init
  self.setName, 'header offset'
  self.setType, 'long64'
  return, 1b
end

pro hubIOImgMeta_headerOffset__define
  struct = {hubIOImgMeta_headerOffset $
    , inherits hubIOImgMetaScalar}
end

