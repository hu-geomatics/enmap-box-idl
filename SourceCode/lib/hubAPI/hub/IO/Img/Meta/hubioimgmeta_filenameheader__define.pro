function hubIOImgMeta_filenameHeader::init
  self.setName, 'filename header'
  self.setType, 'string'
  return, 1b
end

pro hubIOImgMeta_filenameHeader__define
  struct = {hubIOImgMeta_filenameHeader $
    , inherits hubIOImgMetaScalar}
end

