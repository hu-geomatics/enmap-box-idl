function hubIOImgMeta_samples::init
  self.setName, 'samples'
  self.setType, 'long64'
  return, 1b
end

pro hubIOImgMeta_samples__define
  struct = {hubIOImgMeta_samples $
    , inherits hubIOImgMetaScalar}
end
