;+
; :hidden:
;    Constructor
;-       hubIOImgMeta_coordinatesystemstring__define
function hubIOImgMeta_coordinateSystemString::init
  self.setName, 'coordinate system string'
  return, 1b
end

function hubIOImgMeta_coordinateSystemString::isConsistentValue, value, Message=message
  ;TODO count braces
  

  return, 1b
end


function hubIOImgMeta_coordinateSystemString::isConsistentENVIValue, ENVIValue, Message=message
  
  isValid = 1b
  msg = !NULL
  
  ;TODO count braces, check WKT Syntax
  ;see http://www.geoapi.org/3.0/javadoc/org/opengis/referencing/doc-files/WKT.html

  message = msg
  return, isValid
end

pro hubIOImgMeta_coordinateSystemString::setValueFromENVIString, ENVIString

  ENVIValue = strjoin(strsplit(strtrim(ENVIString, 2), '{} ', /EXTRACT),'')
  

  if ~self.isConsistentENVIValue(ENVIValue, Message=message) then begin
    message, messageString
  endif
  value = self.getValueFromENVIValue(ENVIValue)
  if n_elements(value) eq 1 then begin
    value = value[0]
  endif
  self.setValue, value
end

function hubIOImgMeta_coordinateSystemString::getValueFromENVIValue, ENVIValue
 

  return, ENVIValue
end

;+
; :Description:
;    Creates the ENVI Header Value String.
;-
function hubIOImgMeta_coordinateSystemString::getValueAsENVIValue
  
  return, '{'+self.getValue() + '}'
end



;+
; :hidden:
;-
pro hubIOImgMeta_coordinateSystemString__define
  struct = {hubIOImgMeta_coordinateSystemString $
    , inherits hubIOImgMeta}
end

;+
; :hidden:
;-
pro test_hubIOImgMeta_coordinateSystemString
  path = 'H:\bj_temp\S2Workshop\04_StackSubsets\226_065\EVI.bsq'
  img = hubIOImgInputImage(path)
  ms = img.getMeta('coordinate system string')
  mo = (img.getHeader()).getMetaObj('coordinate system string')

  stop
end

