function hubIOImgMetaArray::getValueFromENVIValue, stringArray
  value = self.hubIOImgMetaTyped::getValueFromENVIValue([stringArray])
  return, value
end

pro hubIOImgMetaArray::replicateIfScalar, size
  if n_elements(self.getValue()) eq 1 then begin
    self.setValue, replicate((self.getValue())[0], size) 
  endif
end

function hubIOImgMetaArray::count
  return, n_elements(self.getValue())
end

pro hubIOImgMetaArray__define
  struct = {hubIOImgMetaArray $
    , inherits hubIOImgMetaTyped $
  }
end
