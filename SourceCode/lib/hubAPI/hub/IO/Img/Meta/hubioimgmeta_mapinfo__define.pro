;+
; :hidden:
;    Constructor
;-
function hubIOImgMeta_mapInfo::init
  self.setName, 'map info' 
  return, 1b
end

;function hubIOImgMeta_mapInfo::getValue
;  if (randomu(seed,1, /LONG) mod 10) eq 1 then  print, 'RANDOM WARNING: delete quick fix in hubIOImgMeta_mapInfo::getValue !!!'
;  value = json_parse(/DICTIONARY,'{"NORTHING":3335851.5589999999,"PIXELY":0.0,"PIXELX":0.0,"PROJECTION":"Sinusoidal","SIZEX":231.65636000000001,"SIZEY":231.65636000000001,"units":null,"EASTING":8895604.1573329996,"datum":null}')
;  return, value
;end

;+
; :Description:
;    Creates an empty map info structure that descibes the 
;    tie point/reference pixel by using the following tags::
;      
;       tag        | type    | description
;       -----------+---------+-----------------
;       projection | string  | e.g. 'UTM'
;       pixelX     | double  | tie point's image x coordinate (zero based)
;       pixelY     | double  | tie point's image y coordinate (zero based)
;       easting    | double  | tie point's real world x coordinate 
;       northing   | double  | tie point's real world y coordinate
;       sizeX      | double  | a pixels real world x size
;       sizeY      | double  | a pixels real world y size
;       isMapBased | bool    | 0 = pixel index based 
;                  |         | 1 = map based with defined projection
;        /projection specific values/
;       datum      | string  | datum of used coordinate system
;       zone       | integer | e.g. 41 or 42 for germany 
;       NorthSouth | string  | can be 'north' or 'south'
;       units      | string  | 'meters', 'kilometers', 'yard', 'feet', 'miles'
;       otherInfos | string  | further descriptions
;       -----------+---------+-----------------
;
;
;
;-
function hubIOImgMeta_mapInfo::createEmptyMapInfoStruct
  compile_opt static, strictarr
  ;['Arbitrary', '1.0000', '1.0000', '0.0000', '0.0000', '1.0000000000e+000', '1.0000000000e+000', '0', 'units=Meters']
  d = dictionary()
  foreach key, hubIOImgMeta_mapInfo._requiredKeys() do begin
    d[key] = !NULL
  endforeach
  ;add additional keys
  foreach key, ['datum','units'] do begin
    d[key] = !NULL
  endforeach
  return, d
end

;+
; :Description:
;    returns the keys that have to be defined in each map info structure
;-
function hubIOImgMeta_mapInfo::_requiredKeys
  compile_opt static, strictarr
  return, ['projection', 'pixelX', 'pixelY', 'easting','northing','sizeX','sizeY']
end

;+
; :Description:
;    Checks whether a hubIOImgMeta_mapInfo object is valid.
;
; :Params:
;    value: in, required, type = hubIOImgMeta_mapInfo
;     hubIOImgMeta_mapInfo object to check for consistency.
;
; :Keywords:
;    Message: out, optional, type = string
;     Message thrown in case the hubIOImgMeta_mapInfo object is not valid.
;
function hubIOImgMeta_mapInfo::isConsistentValue, value, Message=message

  emptyMapInfo = self.createEmptyMapInfoStruct()
  message = !NULL
  if typename(value) ne 'DICTIONARY' then begin
     message = 'value type is not of type DICTIONARY'
     return, 0b 
  endif 
  
  foreach key, hubIOImgMeta_mapInfo._requiredKeys() do begin
    if ~value.hasKey(key) then begin
      message = [message, 'missing map info key: '+key]
      continue
    endif 
    
    if ~isa(value[key]) then begin
      message = [message, 'key is !NULL: '+key]
      continue      
    endif
  endforeach
  
  if isa(message) then return, 0b
  
  ;check projection specific properties
  projection = value.Projection
  IF ~isa(projection) then return, 0b
  projection = STRTRIM(STRUPCASE(projection),2)
  case projection of
    ;projection specific requirements
    'UTM': begin
      if ~value.hasKey('zone') then begin
        message = 'Missing UTM zone'
        return, 0b
      endif
      if value.zone lt 1 or $
        value.zone gt 60 then begin
        message = 'UTM Zone must be between 1 and 60 ('+strtrim(value.zone)+')'
        return, 0b
      endif

    end
    else  : ;nothing
  endcase
    
  
  return, 1b
end

;+
; :Description:
;    Checks whether a map info string is valid.
;
; :Params:
;    ENVIValue: in, required, type = string
;     ENVI header map info string.
;
; :Keywords:
;    Message: out, optional, type = string
;     Message thrown in case the map info string is not valid.
;
;-
function hubIOImgMeta_mapInfo::isConsistentENVIValue, ENVIValue, Message=message
  isValid = 1b
  msg = !NULL
  
  
;  Projection name
;  Reference (tie point) pixel x location (in file coordinates)
;  Reference (tie point) pixel y location (in file coordinates)
;  Pixel easting
;  Pixel northing
;  x pixel size
;  y pixel size
;  Projection zone (UTM only)
;  North or South (UTM only)
;  Datum
;  Units

  
  if n_elements(ENVIValue) lt 7 then begin
    isValid *= 0b
    msg = 'not enough map info elements'
  endif
  helper = hubHelper()
  ;check to parse pixel locations + dimensions
  for i=1,6 do begin
    if ~helper.canConvert(ENVIValue[i], 'float') then begin
      isValid *= 0b
      msg = [msg, 'can not convert "'+ENVIValue[i]+'" to float']
      
      break;
    endif
  endfor
  
  message = msg
  return, isValid
end

;+
; :Description:
;    Returns a hubIOImgMeta_mapInfo object from reading a ENVI header map info string.
;
; :Params:
;    ENVIValue: in, required, type = string
;     ENIV header map info string
;
;
;
;-
function hubIOImgMeta_mapInfo::getValueFromENVIValue, ENVIValue
  mapInfo = self.createEmptyMapInfoStruct()
  
  n = n_elements(ENVIValue)
  if n lt 7 then message, 'An ENVI MAP INFO definition must contain at least 7 separated values'
  ;  0 Projection name
  ;  1 Reference (tie point) pixel x location (in file coordinates)
  ;  2 Reference (tie point) pixel y location (in file coordinates)
  ;  3 Pixel easting
  ;  4 Pixel northing
  ;  5 x pixel size
  ;  6 y pixel size
  ;  7 Projection zone (UTM only)
  ;  8 North or South (UTM only)
  ;  9 Datum
  ;  10 Units
  mapInfo.Projection = ENVIValue[0]
  mapInfo.pixelX   = double(ENVIValue[1])-1 ; zero based
  mapInfo.pixelY   = double(ENVIValue[2])-1 ; zero based
  mapInfo.easting  = double(ENVIValue[3])
  mapInfo.northing = double(ENVIValue[4])
  mapInfo.sizeX    = double(ENVIValue[5])
  mapInfo.sizeY    = double(ENVIValue[6])
    
  ;catch the units element wherever it is
  for i=0, n_elements(ENVIValue)-1 do begin
    if stregex(ENVIValue[i], 'units=', /BOOLEAN, /FOLD_CASE ) then begin
      mapInfo.units = strlowcase(stregex(ENVIValue[i], '[^=]+$', /Extract))
      break
    endif
  endfor
  
  if ~isa(mapInfo.units) then begin
    case 1b of 
      stregex(mapInfo.projection, '(UTM|Albers Conical Equal)', /BOOLEAN, /FOLD_CASE) : $
          mapInfo.units = 'meters'
      ;TODO
      else:; print, 'hubIOImgMeta_mapInfo: unknown length unit'
    endcase
  endif
  
  ;add projection specific values
  case STRUPCASE(mapInfo.projection) of
    'UTM': begin
            ;catch the units element
            
            if n ge 8 then mapInfo['zone'] = fix(ENVIValue[7])
            if n ge 9 then mapInfo['NorthSouth'] = ENVIValue[8]
            if n ge 10 then mapInfo['Datum'] = ENVIValue[9]
      
           end
     else : begin
        ;try to get the datum from fore-last item
        if ~stregex(ENVIValue[-2], 'units=', /FOLD_CASE, /BOOLEAN) && $
            stregex(ENVIValue[-2], '[:alpha:]+', /BOOLEAN) then begin
          mapInfo['datum'] = ENVIValue[-2]
        endif
        
      end
  endcase
  
  return, mapInfo
end

;+
; :Description:
;    Creates the ENVI Header Value String.
;-
function hubIOImgMeta_mapInfo::getValueAsENVIValue
  
  ;ENVIValue = ['Arbitrary', '1.0000', '1.0000', '0.0000', '0.0000', '1.0000000000e+000', '1.0000000000e+000', '0', 'units=Meters']
  result = list()
  if self.isDefined() then begin
    ;['Arbitrary', '1.0000', '1.0000', '0.0000', '0.0000', '1.0000000000e+000', '1.0000000000e+000', '0', 'units=Meters']
    mapInfo = self.getValue()
    ;add required values
    result.add, mapInfo.Projection
    result.add, string(format='(F)', mapInfo.PixelX+1) ; pixel index plus 1
    result.add, string(format='(F)', mapInfo.PixelY+1) ; pixel index plus 1
    result.add, strtrim(string(format='(F25.10)', mapInfo.Easting),2)
    result.add, strtrim(string(format='(F25.10)', mapInfo.Northing),2)
    result.add, strtrim(string(mapInfo.sizeX),2)
    result.add, strtrim(string(mapInfo.sizeY),2)
    
    ;add UTM specific values
    if STRCMP(mapInfo.projection, 'UTM', /FOLD_CASE) then begin
      result.add, strtrim(mapInfo.zone)
      result.add, strtrim(mapInfo.hubGetValue('northSouth', default='North'))
    endif
    
    ;add optionals
    optional = ['datum']
    foreach key, optional do begin
      if mapInfo.hubIsa(key) then result.add, mapInfo[key]
    endforeach

    if mapInfo.hubIsa('units') then result.add, 'units='+mapInfo.units
  endif
  return, result.toArray()
end

;+
; :hidden:
;-
function hubIOImgMeta_mapInfo::isEqual, meta
  mStruct1 = self.getValue()
  if isa(mStruct1) ne isa(meta) then return, 0b
  
  mStruct2 = !NULL
  case typename(meta) of
    'MAPINFOSTRUCT'        : mStruct2 = meta
    'HUBIOIMGMETA_MAPINFO' : mStruct2 = meta.getValue()
    else : return, 0
  endcase
  
  ;compare structs and its content
  tn1 = TAG_NAMES(mStruct1)
  tn2 = TAG_NAMES(mStruct2)
  if n_elements(tn1) ne n_elements(tn2) then return, 0b
  for i=0, n_elements(tn1)-1 do begin
    if tn1[i] ne tn2[i] then return, 0b
    if mStruct1.(i) ne mStruct2.(i) then return, 0b
  endfor

  return, 1b
end

function hubIOImgMeta_mapInfo::getMapCoordinates, pixelX, pixelY
  if ~isa(pixelX) then return, !null
  if ~isa(pixelY) then begin
    pixelCoordinates = pixelX
  endif else begin
    pixelCoordinates = [transpose(pixelX[*]),transpose(pixelY[*])]
  endelse
  mapInfo = self.getValue()
;  pixelSize = [mapInfo.sizeX, mapInfo.sizeY]
;  tieMap = [mapInfo.easting, mapInfo.northing]
;  tiePixel = [mapInfo.pixelX, mapInfo.pixelY]
;  mapCoordinates = dblarr(size(/DIMENSIONS, pixelCoordinates))
;  mapCoordinates[0,*] = tieMap[0]+pixelSize[0]*(pixelCoordinates[0,*]-tiePixel[0])
;  mapCoordinates[1,*] = tieMap[1]-pixelSize[1]*(pixelCoordinates[1,*]-tiePixel[1])
  mapCoordinates = dblarr(size(/DIMENSIONS, pixelCoordinates))
  mapCoordinates[0,*] = mapInfo.easting+mapInfo.sizeX*(pixelCoordinates[0,*]-mapInfo.pixelX)
  mapCoordinates[1,*] = mapInfo.northing-mapInfo.sizeX*(pixelCoordinates[1,*]-mapInfo.pixelY)
  return, mapCoordinates
end

function hubIOImgMeta_mapInfo::getPixelCoordinates, mapX, mapY
  if ~isa(mapX) then return, !null
  if ~isa(mapY) then begin
    mapCoordinates = mapX
  endif else begin
    pixelCoordinates = [transpose(mapX[*]),transpose(mapY[*])]
  endelse
  
  mapInfo = self.getValue()
  pixelCoordinates = dblarr(size(/DIMENSIONS, mapCoordinates))
  pixelCoordinates[0,*] = (mapCoordinates[0,*]-mapInfo.easting) /( mapInfo.sizeX)+mapInfo.pixelX
  pixelCoordinates[1,*] = (mapCoordinates[1,*]-mapInfo.northing)/(-mapInfo.sizeY)+mapInfo.pixelY
  return, pixelCoordinates

end

;+
; :hidden:
;-
pro hubIOImgMeta_mapInfo__define
  struct = {hubIOImgMeta_mapInfo $
    , inherits hubIOImgMeta}
end

;+
; :hidden:
;-
pro test_hubIOImgMeta_mapInfo
  pathNoMapInfo = 'D:\TestShrink\shrinkAF_LC_Training'
  
  
  path = hub_getTestImage('Hymap_Berlin-A_Image')
  img1 = hubIOImgInputImage(path)
  h1 = img1.getHeader()
  mi1 = img1.getMeta('map info')
  meta1 = h1.getMetaObj('map info')
  meta2 = h1.getMetaObj('xyz')
  print, meta1.getValueAsENVIValue()
 ; print, meta1
 ; print, meta2
  stop
  img3 = hubIOImgInputImage(pathNoMapInfo)
  h3 = img3.getHeader()
  mi3 = img3.getMeta('map info')
  meta3 = h3.getMetaObj('map info')  
  stop
 print, meta3
  
  
  img2 = hubIOImgInputImage(path)
  
  
  h2 = img2.getHeader()
  
  
  meta2 = h2.getMetaObj('map info')
  
  print, meta1 eq meta2      ; gibt 0 zurück -> eq könnte überladen werden
  print, meta1.isEqual(meta2); gibt 1 zurück
  
  
end

pro debug_hubIOImgMeta_mapInfo
  inputImage = hubIOImgInputImage(hub_getTestImage('Hymap_Berlin-A_Image'))
end

