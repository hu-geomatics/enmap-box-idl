function hubIOImgMeta_spectraNames::init
  self.setName, 'spectra names'
  self.setType, 'string'
  return, 1b
end

pro hubIOImgMeta_spectraNames__define
  struct = {hubIOImgMeta_spectraNames $
    , inherits hubIOImgMetaArray}
end

