function hubIOImgMeta_filecompression::init
  self.setName, 'file compression'
  self.setType, 'byte'
  return, 1b
end

pro hubIOImgMeta_filecompression__define
  struct = {hubIOImgMeta_filecompression $
    , inherits hubIOImgMetaScalar}
end

pro debug_hubIOImgMeta_filecompression
  di = hubIOImgMeta_filecompression()
  di.setType, 1
  di.setValue, 0
  print,di
end