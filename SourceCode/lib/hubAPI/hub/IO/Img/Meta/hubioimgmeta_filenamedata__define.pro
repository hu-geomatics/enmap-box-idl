function hubIOImgMeta_filenameData::init
  self.setName, 'filename data'
  self.setType, 'string'
  return, 1b
end

pro hubIOImgMeta_filenameData__define
  struct = {hubIOImgMeta_filenameData $
    , inherits hubIOImgMetaScalar}
end

