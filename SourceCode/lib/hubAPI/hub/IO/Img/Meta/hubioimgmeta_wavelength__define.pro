function hubIOImgMeta_wavelength::init
  self.setName, 'wavelength'
  self.setType, 'double'
  return, 1b
end

pro hubIOImgMeta_wavelength__define
  struct = {hubIOImgMeta_wavelength $
    , inherits hubIOImgMetaArray}
end

