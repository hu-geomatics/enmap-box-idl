function hubIOImgMeta_xStart::init
  self.setName, 'x start'
  self.setType, 'long64'
  self.isZeroBased = 1b
  return, 1b
end

pro hubIOImgMeta_xStart__define
  struct = {hubIOImgMeta_xStart $
    , inherits hubIOImgMetaScalar}
end
