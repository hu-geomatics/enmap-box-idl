function hubIOImgMeta_dataGainValues::init
  self.setName, 'data gain values'
  self.setType, 'double'
  return, 1b
end

pro hubIOImgMeta_dataGainValues__define
  struct = {hubIOImgMeta_dataGainValues $
    , inherits hubIOImgMetaArray}
end

