function hubIOImgMeta_dataIgnoreValue::init
  self.setName, 'data ignore value'
  self.setType, 'double'
  return, 1b
end

pro hubIOImgMeta_dataIgnoreValue::setType, type
  self.type = ptr_new(type)
end

pro hubIOImgMeta_dataIgnoreValue__define
  struct = {hubIOImgMeta_dataIgnoreValue $
    , inherits hubIOImgMetaScalar}
end

pro debug
  di = hubIOImgMeta_dataIgnoreValue()
  di.setType, 1
  di.setValue, -1
  print,di
end