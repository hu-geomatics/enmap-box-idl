function hubIOImgMetaTyped::init
  self.type = ptr_new('string')
  return, 1b
end

pro hubIOImgMetaTyped::setValue, value
  castedValue = self.castValue(value)
  self.hubIOImgMeta::setValue, castedValue
end

function hubIOImgMetaTyped::isConsistentValue, value, Message=message
  message = 'Can not cast '+strupcase(self.getName())+' metadata value.' 
  isConsistent = hubMathHelper.isConvertableData(value, self.getType())
  return, isConsistent
end

function hubIOImgMetaTyped::castValue, value
  castedValue = hubMathHelper.convertData(value, self.getType())
  return, castedValue
end

pro hubIOImgMetaTyped::setType, type
  self.type = ptr_new(type)
end

function hubIOImgMetaTyped::getType
  return, *(self.type)
end

pro hubIOImgMetaTyped__define
  struct = {hubIOImgMetaTyped $
    , inherits hubIOImgMeta $
    , type : ptr_new() $
  }
end
