;+
; :Hidden:
;-

function hubIOImgHeader::init
  self.metas = hash()
  return,1
end

function hubIOImgHeader::hasMeta, metaName
  return, self.metas.hasKey(strlowcase(metaName))
end

pro hubIOImgHeader::setMeta, metaName, value
  meta = hubIOImgMeta_createFromValue(metaName, value)
  self.setMetaObj, meta

end

pro hubIOImgHeader::setMetaIfUndefined, metaName, value
  if ~self.metaDefined(metaName) then begin
    self.setMeta, metaName, value
  endif
end

pro hubIOImgHeader::setMetaList, metaNames, values
  foreach metaName, metaNames, i do begin
    self.setMeta, metaName, values[i]
  endforeach
end

function hubIOImgHeader::getMeta, metaName, Default=default
  if ~isa(metaName) then begin
    value = Hash()
    metaNames  = self.metas.keys()
    foreach metaName, metaNames do begin
      value[metaName] = self.getMeta(metaName)
    endforeach
  endif else begin
    meta = self.getMetaObj(metaName)
    value = meta.getValue()
  endelse
  
  if ~isa(value) and isa(default) then begin
    value = default
  endif
  
  return, value
end

function hubIOImgHeader::getMetaHash, metaNames
  
  if ~isa(metaNames) then begin
    metaHash = self.metas
  endif else begin
    metaHash = Hash()
    foreach metaName, metaNames do begin
      if self.hasMeta(metaName) then metaHash[metaName] = self.getMeta(metaName)
    endforeach
  endelse
  return, metaHash
end

function hubIOImgHeader::getMetaNames
  metaNames = self.metas.keys()
  indices = [metaNames.where('filename data'),metaNames.where('filename header')]
  if isa(indices) then begin
    metaNames.remove, indices
  endif
  return, metaNames.toArray()
end

function hubIOImgHeader::metaDefined, metaName
  meta = self.getMetaObj(metaName)
  return, meta.isDefined()
end

pro hubIOImgHeader::setMetaObj, meta
  (self.metas)[meta.getName()] = meta
end

function hubIOImgHeader::getMetaObj, metaName

  meta = self.metas.hubGetValue(strlowcase(metaName))
  if ~isa(meta) then begin
    meta = hubIOImgMeta_createFromValue(metaName, !null)
  endif
  return, meta
end

pro hubIOImgHeader::setMetaObjHash, metaHash
  self.metas = self.metas + metaHash
end

function hubIOImgHeader::getMetaObjHash, metaNames
  if isa(metaNames) then begin
    metasSub = self.metas.hubGetSubhash(strlowcase(metaNames))
  endif else begin
    metasSub = hash()
  endelse
  foreach value, metasSub, metaName do begin
    if ~isa(value) then begin
      metasSub[metaName] = hubIOImgMeta_createFromValue(metaName, !null)
    endif
  endforeach
  return, metasSub
end

pro hubIOImgHeader::copyMeta $
  , header $
  , metaNames $
  , CopyFileType=copyFileType $
  , CopyLabelInformation=copyLabelInformation $
  , CopySpatialInformation=copySpatialInformation $
  , CopySpectralInformation=copySpectralInformation
  
  if isa(metaNames) then begin
    metaNamesAll = metaNames
  endif else begin
    metaNamesAll = []
  endelse

  if keyword_set(copyFileType) then begin
    metaNamesAll = [metaNamesAll, 'file type']
  endif
  
  if keyword_set(copyLabelInformation) then begin
    metaNamesAll = [metaNamesAll, 'classes', 'class lookup', 'class names', 'data ignore value']
  endif
  
  if keyword_set(copySpatialInformation) then begin
    metaNamesAll = [metaNamesAll, 'samples', 'lines', 'map info', 'coordinate system string', 'map info', 'x start', 'y start']
  endif

  if keyword_set(copySpectralInformation) then begin
    metaNamesAll = [metaNamesAll, 'bands', 'band names', 'data gain values', 'data offset values' $
      , 'default bands', 'wavelength units', 'wavelength', 'fwhm']
  endif

  self.setMetaObjHash, header.getMetaObjHash(metaNamesAll)
  
end

function hubIOImgHeader::getSpatialSize
  spatialSize = [self.getMeta('samples'), self.getMeta('lines')]
  return, spatialSize
end


pro hubIOImgHeader::parseFile, filename, Lines=lines
  metas = hubIOImgHeader_parseHeader(filename, Lines=lines)
  foreach meta, metas do begin
    self.setMetaObj, meta
  endforeach
end

pro hubIOImgHeader::writeFile, filename

  enviHeader = list()

  ignoredMetanames = ['filename header', 'filename data'] 
  sortedMetaNames = ['description', 'samples', 'lines', 'bands', 'data type', 'interleave', 'file type', 'byte order', 'x start', 'y start', 'default bands', 'wavelength units']

  ; sorted metadata first

  enviHeader.add, list('ENVI')
  sortedMetas = self.getMetaObjHash(sortedMetaNames)
  foreach metaName, sortedMetaNames do begin
    meta = self.getMetaObj(metaName)
    if meta.isDefined() then begin
      ENVIString = meta.getValueAsENVIString()
      if n_elements(ENVIString) eq 1 then begin
        enviHeader.add, meta.getName()+' = '+ENVIString[0]
      endif else begin
        enviHeader.add, [meta.getName()+' = ', ENVIString]
      endelse
    endif
  endforeach

  ; than the rest
  
  foreach meta, self.metas do begin
    if isa(where(/NULL, meta.getName() eq [sortedMetaNames, ignoredMetanames])) then begin
      continue
    endif
    if meta.isDefined() then begin
      ENVIString = meta.getValueAsENVIString()
      if n_elements(ENVIString) eq 1 then begin
        enviHeader.add, meta.getName()+' = '+ENVIString[0]
      endif else begin
        enviHeader.add, [meta.getName()+' = ', ENVIString]
      endelse
    endif
  endforeach

  ; write to file

  openw, unit, filename, /GET_LUN
  foreach enviString, enviHeader do begin
    printf, unit, enviString
  endforeach
  free_lun,unit

end

pro hubIOImgHeader::setRequiredDefaults

  self.setMetaIfUndefined, 'file type', 'envi standard'
  self.setMetaIfUndefined, 'byte order', hubHelper.getByteOrder()
  
end

pro hubIOImgHeader::checkRequiredMeta

  foreach metaName, ['samples','lines','bands','data type','interleave','byte order','file type'] do begin
    if ~self.metaDefined(metaName) then begin
      message, 'Required meta information is not defined: '+metaName
    endif
  endforeach

;  if self.getMeta('file type') eq 'envi classification' then begin
  if self.getMeta('file type') eq 'envi classification' then begin
    foreach metaName, ['classes'] do begin
      if ~self.metaDefined(metaName) then begin
        message, 'Required meta information is not defined: '+metaName
      endif
    endforeach
  endif

end

pro hubIOImgHeader::setOptionalDefaults
  
  self.setMetaIfUndefined, 'band names', 'Band '+strcompress(/REMOVE_ALL,l64indgen(self.getMeta('bands'))+1)
  
  
  ;defaultMapInfo = (hubIOImgMeta_mapInfo()).createEmptyMapInfoStruct()
  ;self.setMetaIfUndefined, 'map info', defaultMapInfo  ; this is ['Arbitrary', '1.0000', '1.0000', '0.0000', '0.0000', '1.0000000000e+000', '1.0000000000e+000', '0', 'units=Meters']
  
  
  if self.hasMeta('data gain values') then begin
    dataGainValues = self.getMetaObj('data gain values')
    dataGainValues.replicateIfScalar, self.getMeta('bands')
  endif

  if self.hasMeta('data offset values') then begin
    dataOffsetValues = self.getMetaObj('data offset values')
    dataOffsetValues.replicateIfScalar, self.getMeta('bands')
  endif
  
  if self.getMeta('file type') eq 'envi classification' then begin
    self.setMeta,'data ignore value', fix(0, Type=self.getMeta('data type')) ; always set value to 0
    classLookup = hubColor_getENVIColors(lindgen(self.getMeta('classes')))
    self.setMetaIfUndefined, 'class lookup', classLookup
    classNames =  'Class '+strtrim(ulindgen(self.getMeta('classes')),2)
    classNames[0] = 'Undefined'
    self.setMetaIfUndefined, 'class names', classNames
  endif
  
  if self.getMeta('file type') eq 'envi spectral library' then begin
    self.setMetaIfUndefined,'spectra names', 'Profile '+strcompress(/REMOVE_ALL, l64indgen(self.getMeta('lines'))+1)
  endif
  
  if self.hasMeta('data ignore value') then begin
    dataIgnoreValue = self.getMetaObj('data ignore value')
    dataIgnoreValue.setType, self.getMeta('data type')
    dataIgnoreValue.setValue, dataIgnoreValue.getValue()
  endif
end

pro hubIOImgHeader::checkOptionalMeta

  metaNames = ['band names','data gain values','data offset values','wavelength','fwhm']
  foreach metaName, metaNames do begin
    self.checkMetaSize, metaName, self.getMeta('bands')
  endforeach
  
  if self.getMeta('file type') eq 'envi classification' then begin
    self.checkMetaSize, 'class lookup', self.getMeta('classes')*3
    self.checkMetaSize, 'class names', self.getMeta('classes')
  endif
  
  if self.getMeta('file type') eq 'envi spectral library' then begin
    self.checkMetaSize, 'spectra names', self.getMeta('lines')
  endif


end

pro hubIOImgHeader::checkMetaSize, metaName, size
  if self.metaDefined(metaName) then begin
    if n_elements(self.getMeta(metaName)) ne size then begin
      filename = ''
      if self.hasMeta('filename header') then filename = '. File: '+self.getMeta('filename header')
      message, 'Inconsistent number of elements: '+metaName+filename
    endif
  endif
end

pro hubIOImgHeader::convertSpeclibForward
  if strlowcase(self.getMeta('file type')) eq 'envi spectral library' then begin
    self.setMeta, 'bands', self.getMeta('samples')
    self.setMeta, 'samples', 1
    self.setMeta, 'interleave', 'bip'
    self.setMeta, 'band names', !null
  endif
end

pro hubIOImgHeader::convertSpeclibBackward
  if strlowcase(self.getMeta('file type')) eq 'envi spectral library' then begin
    self.setMeta, 'samples', self.getMeta('bands')
    self.setMeta, 'bands', 1
    self.setMeta, 'interleave', 'bsq'
    self.setMeta, 'band names', !null
  endif
end

function hubIOImgHeader::locateWavelength $
  , wavelengths $
  , TrueColor=trueColor $
  , ColoredInfrared=coloredInfrared
  
  if keyword_set(trueColor) then begin
    wavelengths = [640, 550, 460]
  endif
  
  if keyword_set(coloredInfraRed) then begin
    wavelengths = [860, 650, 550]
  endif

  if ~isa(wavelengths) then begin
    message, 'Missing argument: wavelengths.'
  endif
  
  imageWavelengths = self.getMeta('wavelength')
  imageWavelengthUnit = self.getMeta('wavelength units')
  if ~isa(imageWavelengths) or ~isa(imageWavelengthUnit) then begin
    message, "Missing image metadata: 'wavelength' or 'wavelength units'."
  endif
  
  imageWavelengths = double(imageWavelengths)
  if strlowcase(imageWavelengthUnit) eq 'micrometers' then begin
    imageWavelengths *= 1000
  endif
  
  bandPositions = lon64arr(n_elements(wavelengths))
  foreach wavelength, wavelengths, i do begin
    distance = abs(imageWavelengths - wavelength)
    bandPositions[i] = (where(distance eq min(distance)))[0]
  endforeach
  
  return, bandPositions

end

function hubIOImgHeader::_overloadPrint

  result = list()
  
  foreach meta, self.metas do begin
    result.add, meta._overloadPrint()
  endforeach 
   
  result = transpose(result.toArray())
  return, result

end

pro hubIOImgHeader__define
  struct = {hubIOImgHeader $
    , inherits IDL_Object $
    , metas : hash() $
  }
end

pro test_hubIOImgHeader

  header = hubIOImgHeader()
  header.parseFile, hub_getTestImage('Hymap_Berlin-B_Image')+'.hdr'
 
  
  print,header
  header.setMeta, 'data ignore value', -1234
  header.writeFile, 'd:\tempHeader.hdr'
  hubHelper.openFile, 'd:\tempHeader.hdr'
  
end