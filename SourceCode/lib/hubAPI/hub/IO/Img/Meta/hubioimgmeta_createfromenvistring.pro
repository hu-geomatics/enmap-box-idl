;+
; :Hidden:
;-

function hubIOImgMeta_createFromENVIString, name, ENVIString
  meta = hubIOImgMeta_create(name)
  meta.setValueFromENVIString, ENVIString   
  return, meta
end

pro test_hubIOImgMeta_createFromENVIString
  meta = hubIOImgMeta_createFromENVIString('band names', '{band 1, band 2, band 3}')
  print, meta
  help, meta
end

