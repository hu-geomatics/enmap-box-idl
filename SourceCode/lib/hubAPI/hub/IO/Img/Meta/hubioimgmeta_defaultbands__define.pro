function hubIOImgMeta_defaultBands::init
  self.setName, 'default bands'
  self.setType, 'long64'
  self.isZeroBased = 1b
  return, 1b
end

pro hubIOImgMeta_defaultBands::setValue, value, Format=format

  if isa(value, /SCALAR) then begin
    value_ = [value, value, value]
  endif else begin
    value_ = value
  endelse
  
  self.hubIOImgMetaArray::setValue, value_
  
end

pro hubIOImgMeta_defaultBands__define
  struct = {hubIOImgMeta_defaultBands $
    , inherits hubIOImgMetaArray}
end

