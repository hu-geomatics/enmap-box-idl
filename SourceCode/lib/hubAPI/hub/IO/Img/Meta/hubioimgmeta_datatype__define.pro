function hubIOImgMeta_dataType::init
  self.setName, 'data type'
  self.setEnumeration, [ 1,      2,     3,       4,      5,        12,     13,      14,       15] $
                     , ['byte', 'int', 'long', 'float', 'double', 'uint', 'ulong', 'long64', 'ulong64'] $
                     , ValueDescription=[ 'Byte (8 bits)' $
                                         ,'Integer (16 bits)' $
                                         ,'Long integer (32 bits)' $
                                         ,'Floating-point (32 bits)' $
                                         ,'Double-precision floating-point (64 bits)' $
                                         ,'Unsigned integer (16 bits)' $
                                         ,'Unsigned long integer (32 bits)' $
                                         ,'Long 64-bit integer' $
                                         ,'Unsigned long 64-bit integer']
  self.setType, 'int'
;  self.setValue, !null
  return, 1b
end

pro hubIOImgMeta_dataType__define
  struct = {hubIOImgMeta_dataType $
    , inherits hubIOImgMetaEnumeration}
end

