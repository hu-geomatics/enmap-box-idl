function hubIOImgMeta_interleave::init
  self.setName, 'interleave'
  self.setEnumeration, ['bsq', 'bil', 'bip'], $
      ValueDescription=['Band Sequential (bsq)', $
                        'Band Interleaved by Line (bil)', $
                        'Band Interleaved by Pixel (bip)']
  self.setType, 'string'
  return, 1b
end

pro hubIOImgMeta_interleave__define
  struct = {hubIOImgMeta_interleave $
    , inherits hubIOImgMetaEnumeration}
end

