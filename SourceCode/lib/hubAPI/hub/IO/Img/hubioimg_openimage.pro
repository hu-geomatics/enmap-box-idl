;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    Use this procedure to open an image inside the current processing environment (e.g. ENVI or EnMAP-Box).
;
; :Params:
;    filename : in, required, type=string
;      Image filename.
;
;-
pro hubIOImg_openImage, filename
  hubProEnvHelper.openImage, filename
end