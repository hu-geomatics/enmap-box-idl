;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    Set image meta information.
;
; :Params:
;    filename : in, required, type=string
;      Use this argument to specify the image filename.
;      
;    metaNames : in, required, type={string | string[]}
;      Use this argument to specify the meta information to by defined.
;      
;    metaValues : in, required, type=list[]
;      Use this argument to specify the meta values. 
;
;
;-
pro hubIOImg_setMeta, filename, metaNames, metaValues
  image = hubIOImgInputImage(filename)
  header = image.getHeader()
  header.setMeta, metaNames, metaValues
  header.writeFile, header.getMeta('filename header')
end