;+
; :Description:
;    Is a procedural wrapper for `hubIOImgInputImage::getCube`.
;
;-
function hubIOImg_readImage, filename, Header=header, Slice=slice, $
  SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle, $
  SubsetBandPositions=bandPositions

  inputImage = hubIOImgInputImage(filename)
  header = inputImage.getHeader()
  image = hubIOImgInputImage(filename)
  result = image.getCube(Slice=slice, SubsetSampleRange=sampleRange, SubsetLineRange=lineRange, SubsetRectangle=rectangle, SubsetBandPositions=bandPositions)
  return, result
end

pro test_hubIOImg_readImage
  filename = hub_getTestImage('Hymap_Berlin-A_Image')

  filename = 'E:\Andreas\Dropbox\images\bsq'
;  filename = 'E:\Andreas\Dropbox\images\bil'
;  filename = 'E:\Andreas\Dropbox\images\bip'

  rectangle = (hubIOImgInputImage(filename)).getMapCoordinates([[-150,-50],[500,500]])
  rectangle = (hubIOImgInputImage(filename)).getMapCoordinates([[-150,-50],[-1,-1]])
  
  
  cirBands =  (hubIOImgInputImage(filename)).locateWavelength(/ColoredInfrared)
 
  image = hubIOImg_readImage(filename, SubsetRectangle=rectangle, SubsetBandPositions=cirBands)
  window, /FREE, XSIZE=n_elements(image[*,0,0]), YSIZE=n_elements(image[0,*,0])
  tvscl, image, /Order, True=3
end

pro test_hubIOImg_readImage2

  filenameA = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameB = hub_getTestImage('Hymap_Berlin-B_Image')

  boundingBoxA = (hubIOImgInputImage(filenameA)).getMapCoordinates(/BoundingBox)
  boundingBoxB = (hubIOImgInputImage(filenameB)).getMapCoordinates(/BoundingBox)

  boundingBox  = [min([boundingBoxA[0],boundingBoxB[0]]),$
                  max([boundingBoxA[1],boundingBoxB[1]]),$
                  max([boundingBoxA[2],boundingBoxB[2]]),$
                  min([boundingBoxA[3],boundingBoxB[3]])]

  cirBands = (hubIOImgInputImage(filenameA)).locateWavelength(/ColoredInfrared)
  mapInfoB = (hubIOImgInputImage(filenameB)).getMeta('map info')
  pixelSize = [mapInfoB.sizeX, mapInfoB.sizeY]

  stop

  imageA = hubIOImg_readImage(filenameA, SubsetRectangle=boundingBox, PixelSize=pixelSize, SubsetBandPositions=cirBands)
  imageB = hubIOImg_readImage(filenameB, SubsetRectangle=boundingBox, PixelSize=pixelSize, SubsetBandPositions=cirBands)

  compositeSize = size(/DIMENSIONS, imageB)
  imageA = congrid(imageA, compositeSize[0], compositeSize[1], compositeSize[2])
  composite = imageA+imageB
  window, /FREE, XSIZE=compositeSize[0], YSIZE=compositeSize[1]
  tvscl, composite, /Order, True=3

end

pro test_hubIOImg_readAndWrite

  filenameA = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameB = hub_getTestImage('Hymap_Berlin-B_Image')

  boundingBoxA = (hubIOImgInputImage(filenameA)).getMapCoordinates(/BoundingBox)
  boundingBoxB = (hubIOImgInputImage(filenameB)).getMapCoordinates(/BoundingBox)

  boundingBox  = [min([boundingBoxA[0],boundingBoxB[0]]),$
    max([boundingBoxA[1],boundingBoxB[1]]),$
    max([boundingBoxA[2],boundingBoxB[2]]),$
    min([boundingBoxA[3],boundingBoxB[3]])]

  cirBands =  (hubIOImgInputImage(filenameA)).locateWavelength(/ColoredInfrared)

  imageA = hubIOImg_readImage(filenameA, SubsetRectangle=boundingBox, SubsetBandPositions=cirBands)
  imageB = hubIOImg_readImage(filenameB, SubsetRectangle=boundingBox, SubsetBandPositions=cirBands)

  compositeSize = size(/DIMENSIONS, imageB)
  imageA = congrid(imageA, compositeSize[0], compositeSize[1], compositeSize[2])
  composite = imageA+imageB
  window, /FREE, XSIZE=compositeSize[0], YSIZE=compositeSize[1]
  tvscl, composite, /Order, True=3

end


