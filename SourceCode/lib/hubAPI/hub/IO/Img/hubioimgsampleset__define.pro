;+
;  An hubIOImgSampleSet object can be used to access sample set data.
;  Sample set data is stored in two separate image files: 
;  the feature image file and the label image file. 
;
;  For usage details also see `Image Data Input/Output API <./image-data-input-output.html>` and
;  `Accessing Sample Set Data <./sample-set.html>`.
;-  

;+
; :Description:
;    This method is called indirectly when calling the object constructor hubIOImgSampleSet().
;    The method return value is an object reference to the newly-created object.
;
; :Params:
;    filenameFeatures : in, required, type=string
;      Use this argument to specify the full path name of the feature image data file.
;      The feature image spatial size must match the label image spatial size.
;      
;    filenameLabels : in, required, type=string
;      Use this argument to specify the full path name of the label image data file.
;      The label image must be a single band image and its 
;      spatial size must match the feature image spatial size.
;
;-
function hubIOImgSampleSet::init $
  , filenameFeatures $
  , filenameLabels

  self.imageFeatures = hubIOImgInputImage(filenameFeatures)
  self.imageLabels = hubIOImgInputImage(filenameLabels)
  
  if ~self.imageLabels.isCorrectSpectralSize(1) then begin
    message,'Label image must be a single band image.'
  endif

  if ~self.imageFeatures.isCorrectSpatialSize(self.imageLabels.getSpatialSize()) then begin
    message,'Label image and feature image must have the same spatial size.'
  endif
  
  self.backgroundValue = ptr_new(self.imageLabels.getMeta('data ignore value'))
  
  return, 1b

end

;+
; :Description:
;    This method returns the pixel locations of all labeled pixels. 
;    It returns an array of one-dimensional indices (`result = int[]`), or two-dimensional indices (`result = int[][2]`).
;
; :Keywords:
;    TwoDimensional : in, optional, type=boolean, default=0
;      Set this keyword to return two-dimensional indices.
;-
function hubIOImgSampleSet::getIndices $
  , TwoDimensional=twoDimensional

  numberOfSamples = self.imageLabels.getMeta('samples')
  numberOfLines = self.imageLabels.getMeta('lines')
  self.imageLabels.initReader, /Slice
  indices = []
  for sliceIndex=0ll,numberOfLines-1 do begin
    currentLabels = self.imageLabels.getData(sliceIndex)
    currentIndices = self._filterIndices(currentLabels)
    if isa(currentIndices) then begin
      currentIndices += sliceIndex*numberOfSamples
      indices = [indices, currentIndices]
    endif
  endfor
  self.imageLabels.finishReader

  ; convert to 2D indices
  
  if keyword_set(twoDimensional) then begin
    if isa(indices) then begin
      indices = array_indices([numberOfSamples, numberOfLines],indices , /DIMENSIONS)
      indices = transpose(indices)
    endif
  endif

  return, indices

end

;+
; :Hidden:
;-
function hubIOImgSampleSet::_filterIndices $
  , labels

  filteredIndices = where(/NULL, labels ne *self.backgroundValue)
  return, filteredIndices 

end

;+
; :Description:
;    This method returns pixel profiles (`result = number[bands][]`) for the specified pixel locations.
;
; :Params:
;    indices : in, required, type={int[] | int[][2]}
;      Use this argument to specify pixel locations. 
;      Specify either an array of one- or two-dimensional indices.
;
; :Keywords:
;    DataType : in, optional, type={string | int}, default=feature image data type
;      Use this keyword to specify the result data type.
;      
;    SubsetBandPositions : in, optional, type=int[], default=all bands
;      Use this keyword to specify an array with image band positions that are included in the pixel profiles. 
;      This array must not necessarily be ordered.
;-
function hubIOImgSampleSet::getFeatures $
  , indices $
  , DataType=dataType $
  , SubsetBandPositions=subsetBandPositions

  self.imageFeatures.initReader, /Profiles, DataType=dataType, SubsetBandPositions=subsetBandPositions
  features = self.imageFeatures.getData(indices)
  self.imageFeatures.finishReader

  return, features

end

;+
; :Description:
;    This method returns pixel labels (`result = number[]`) for the specified pixel locations.
;
; :Params:
;    indices : in, required, type={int[] | int[][2]}
;      Use this argument to specify pixel locations. 
;      Specify either an array of one- or two-dimensional indices.
;
; :Keywords:
;    DataType : in, optional, type={string | int}, default=label image data type
;      Use this keyword to specify the result data type.
;      
;-
function hubIOImgSampleSet::getLabels $
  , indices $
  , DataType=dataType
  
  if isa(indices) then begin
    self.imageLabels.initReader, /Profiles, DataType=dataType
    labels = reform(self.imageLabels.getData(indices))
    self.imageLabels.finishReader
  endif else begin
    labels = !NULL
  endelse
  
  return, labels

end

;+
; :Description:
;    This method returns feature image meta data. For details see `hubIOImgInputImage::getMeta`. 
;-
function hubIOImgSampleSet::getFeatureMeta $
  , metaName

  return, self.imageFeatures.getMeta(metaName)

end

;+
; :Description:
;    This method returns label image meta data. For details see `hubIOImgInputImage::getMeta`.
;-
function hubIOImgSampleSet::getLabelMeta, metaName, Default=default
  return, self.imageLabels.getMeta(metaName, Default=default)
end

;+
; :Description:
;    This method returns label image meta data. For details see `hubIOImgInputImage::getMeta`.
;-
function hubIOImgSampleSet::getBackgroundValue
  return, *self.backgroundValue
end

function hubIOImgSampleSet::getSample

  bands = self.getFeatureMeta('bands')
  samples = self.getFeatureMeta('samples')
  lines   = self.getFeatureMeta('lines')
  
  dataIgnoreValue = self.getLabelMeta('data ignore value', Default=0)
  
  tileLines = hubIOImg_getTileLines(image=self.imageFeatures)
  self.imageFeatures.initReader, tileLines, /TileProcessing, /Slice
  self.imageLabels.initReader, tileLines, /TileProcessing, /Slice
  
  featuresList  = list()
  labelsList    = list()
  positionsList = list() 
  numberOfLabeledPixels = 0ll

  positionsOffset = 0ul
  while ~self.imageFeatures.tileProcessingDone() do begin
    featureData = self.imageFeatures.getData()
    labelData   = (self.imageLabels.getData())[*]
    tileSize = n_elements(labelData)
    
    indices = where(/NULL, labelData ne dataIgnoreValue, numberOfLabeledPixelsInTile)
    
    if isa(indices) then begin
      numberOfLabeledPixels += numberOfLabeledPixelsInTile
      featuresList.add, /NO_COPY, featureData[*,indices]
      labelsList.add, /NO_COPY, labelData[indices]
      positionsList.add, /NO_COPY, indices+positionsOffset
    endif
    positionsOffset += tileSize    
  endwhile
  
  self.imageFeatures.finishReader
  self.imageLabels.finishReader
  if numberOfLabeledPixels eq 0 then begin
    features = !null
    labels = !null
    positions = !null
  endif else begin
    features = make_array(bands, numberOfLabeledPixels, TYPE=self.getFeatureMeta('data type'))
    labels = make_array(numberOfLabeledPixels, TYPE=self.getlabelMeta('data type'))
    positions = make_array(numberOfLabeledPixels, TYPE=13) ; ulong
    offset = 0ll
    for i=0,n_elements(labelsList)-1 do begin
      labelsInTile = labelsList.remove(0)
      featuresInTile = featuresList.remove(0)
      positionsInTile = positionsList.remove(0) 
      if isa(featuresInTile) then features[0,offset] = featuresInTile
      if isa(labelsInTile) then labels[offset] = labelsInTile
      if isa(positionsInTile) then positions[offset] = positionsInTile
      offset += n_elements(positionsInTile)
    endfor
    features = hub_fix2d(features)
  endelse
  
  return, {features:features, labels:labels, positions:positions, count:numberOfLabeledPixels}
end

;+
; :Description:
;    This method is used to write a new label image with a sample subset of the original label image.
;
; :Params:
;    filename : in, required, type=string
;      Use this argument to specify the full path name of the created label image data file.
;      
;    indices : in, required, type={int[] | int[][2]}
;      Use this argument to specify pixel locations included inside the sample subset. 
;      Specify either an array of one- or two-dimensional indices.
;-
pro hubIOImgSampleSet::writeLabelImage $
  , filename $
  , indices

  if ~arg_present(indices) then begin
    message, 'Missing argument: indices.'
  endif

  if isa(indices) then begin
  
    ; convert 1D to 2D indices
    
    is2DIndex = (size(/N_DIMENSIONS, indices) eq 2)
    if is2DIndex then begin
    
      sampleIndices = indices[*, 0] 
      lineIndices = indices[*, 1]
        
    endif else begin
    
      sampleIndices = indices mod self.imageLabels.getMeta('samples') 
      lineIndices = indices / self.imageLabels.getMeta('samples')
      
    endelse
    
    ; sort indices
    
    for i=0, n_elements(lineIndices)-2 do begin
      notSorted = lineIndices[i] gt lineIndices[i+1] 
      if notSorted then begin
        sortedIndices = sort(lineIndices)
        sampleIndices = sampleIndices(sortedIndices)
        lineIndices = lineIndices(sortedIndices)
        break
      endif
    endfor
    
    ; find the first and last occurrence of samples in every line 
   
    lastOccurrence = uniq(lineIndices)
    firstOccurrence = shift(lastOccurrence,+1)+1
    firstOccurrence[0] = 0
    uniqLines = list(lineIndices[firstOccurrence], /EXTRACT)
    uniqLines.add, -1
  
  endif else begin
    uniqLines = list()
  endelse
  
  uniqLines.add, -1

  ; create output image and copy metadata
  
  outputImage = hubIOImgOutputImage(filename)
  outputImage.copyMeta, self.imageLabels, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation

  ; initialize reader and writer

  self.imageLabels.initReader, /Slice
  outputImage.initWriter, self.imageLabels.getWriterSettings()

  ; write subsetted label image

  lineBackground = replicate(*self.backgroundValue, outputImage.getMeta('samples'))
  uniqLineIndex = 0ll
  for line=0,outputImage.getMeta('lines')-1 do begin
  
    newLineData = lineBackground 
    
    if line eq uniqLines[uniqLineIndex] then begin
      origLineData = self.imageLabels.getData(line)
      currentSampleIndices = sampleIndices[firstOccurrence[uniqLineIndex]:lastOccurrence[uniqLineIndex]]
      newLineData[currentSampleIndices] = origLineData[currentSampleIndices]
      uniqLineIndex++
    endif 
    
    outputImage.writeData, newLineData   
  endfor
  
  ; finish
  
  self.imageLabels.finishReader
  outputImage.finishWriter

end

function hubIOImgSampleSet::_overloadPrint

  result = transpose(['Feature filename: '+self.imageFeatures.getMeta('filename data'), $
                      'Label filename:   '+self.imageLabels.getMeta('filename data')])
  return, result

end

;+
; :Description:
;    This method is used to perform all cleanup on the object.
;
;-
pro hubIOImgSampleSet::cleanup

  obj_destroy, self.imageFeatures
  obj_destroy, self.imageLabels

end

;+
; :Hidden:
;-
pro hubIOImgSampleSet__define
  struct = {hubIOImgSampleSet $
    , inherits IDL_Object $
    , imageFeatures : obj_new() $
    , imageLabels : obj_new() $
    , backgroundValue : ptr_new() $
  }
end

;+
; :Hidden:
;-
pro test_hubIOImgSampleSet

  filenameFeatures = hub_getTestImage('Hymap_Berlin-A_Image')
  filenameLabels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')

  sampleSet = hubIOImgSampleSet(filenameFeatures, filenameLabels)

  tic
  indices = sampleSet.getIndices()
  labels = sampleSet.getLabels(indices)
  features = sampleSet.getFeatures(indices)
  toc
  help, features, labels

;  tic
;  sample = sampleSet.getSample()
;  toc
;  help, sample.features, sample.labels

end