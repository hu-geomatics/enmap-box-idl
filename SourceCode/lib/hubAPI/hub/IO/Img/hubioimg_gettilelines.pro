;+
; :Description:
;    This function returns the number of tile lines that fits best to the 
;    maximum amount of memory to be used for calculations.
;    This value is defined in the hubAPI settings as 'tileSizeMB'.
;
; :Params:
;    samples: in, required, type=integer
;    bands: in, required, type=integer
;    datatype: in, required, type=data type
;
; :Keywords:
;    image: in, optional, type=hubIOImage()
;      Use this to read the values of `samples`, `bands` and `datatype` directly from an image. 
;
;-
function hubIOImg_getTileLines, samples, bands, datatype, image=image
  
  maxMB = double((hub_getSettings())['tileSizeMB'])    
  if maxMB eq 0 then maxMB = 100
  
  
  if isa(image) then begin
    _samples  = image.getMeta('samples')
    _bands    = image.getMeta('bands')
    _dataType = image.getMeta('data type')
  endif else begin
    _samples  = samples
    _bands    = bands
    _dataType = dataType
  endelse
  
  lineMB = double(_samples) * _bands * hubHelper.getDataTypeInfo(_datatype,/TypeSizes) / 2.^20
  numberOfTileLines = long64(maxMB / lineMB) > 1ll
  return, numberOfTileLines
end

pro test_hubIOImg_getTileLines

  image =   hubIOImgInputImage(hub_getTestImage('Hymap_Berlin-A_Image'))
  print, hubIOImg_getTileLines(image=image)
end