;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Copies meta infos from one image to another.
;
; :Keywords:
;    Open : in, optional, type=boolean
;      Set this keyword to open the image after setting the new metadata. 
;    
; :Params:
;    sourceFilename : in, required, type=string
;      Use this argument to specify the source image filename.
;      
;    targetFilename : in, required, type=string
;      Use this argument to specify the target image filename.
;      
;    metaNames : in, required, type=string[]
;      Use this argument to specify the meta information to be copied.
;-
pro hubIOImg_copyMeta, sourceFilename, targetFilename, metaNames, Open=open, $
  CopyFileType=copyFileType, CopyLabelInformation=copyLabelInformation, CopySpatialInformation=copySpatialInformation, CopySpectralInformation=copySpectralInformation
  MetaTags=MetaTags

  sourceHeader = (hubIOImgInputImage(sourceFilename)).getHeader()
  targetHeader = (hubIOImgInputImage(targetFilename)).getHeader()
  targetHeader.copyMeta, sourceHeader, metaNames, CopyFileType=copyFileType, CopyLabelInformation=copyLabelInformation, CopySpatialInformation=copySpatialInformation, CopySpectralInformation=copySpectralInformation
  
  if isa(metaTags) then begin
    foreach metaTag, metaTags do begin
      targetHeader.setMeta, metaTag, sourceHeader.getMeta(metaTag)
    endforeach
  endif
  
  targetHeader.writeFile, targetHeader.getMeta('filename header')
  
  if keyword_set(open) then begin
    hubProEnvHelper.openImage, targetFilename
  endif
end