;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

;+
; :Description:
;    Returns the sample set for specified positions.
;
; :Params:
;    filenameFeatures : in, required, type=string
;      Use this argument to specify the feature image filename.
;      
;    filenameLabels : in, required, type=string
;      Use this argument to specify the label image filename.
;
;-
function hubIOImg_readSample, filenameFeatures, filenameLabels,$
  OutNumberOfFeatures=numberOfFeatures, OutNumberOfSamples=numberOfSamples
  sampleSet = hubIOImgSampleSet(filenameFeatures, filenameLabels)
  sampleSetData = sampleSet.getSample()
  size = size(sampleSetData.features, /DIMENSIONS)
  numberOfFeatures = size[0]
  numberOfSamples = size[1]
  return, sampleSetData
end