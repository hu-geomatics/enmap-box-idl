;+
; :Description:
;    Is a procedural wrapper for `hubIOImgInputImage::getProfile`.
;
;-
function hubIOImg_readProfiles, filename, pixelPositions, Header=header, SubsetBandPositions=bandPositions
  inputImage = hubIOImgInputImage(filename)
  header = inputImage.getHeader()
  result = inputImage.getProfile(pixelPositions, SubsetBandPositions=bandPositions)
  return, result
end

pro test_hubIOImg_readProfiles
  profiles = hubIOImg_readProfiles(hub_getTestImage('Hymap_Berlin-A_Image'), [13,23,44], SUBSETBANDPOSITIONS=indgen(100))
  plot, profiles[*,0]
  oplot, profiles[*,1]
  oplot, profiles[*,2]
end