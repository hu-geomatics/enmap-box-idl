;+
; This object reads ASD FieldSpec files.
; 
;  
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
; 	
; :Examples:
;   This example shows you how to read and print the spectral values of ASD FieldSpec Files::
;   
;      file1 = 'D:\Sandbox\Messprotokoll\In\gi.000'
;      file2 = 'D:\Sandbox\Messprotokoll\In\gi.005'
;      
;      reader = hubIOASDReader()
;      reader.setFileName, file1
;      plot, reader.getBands(), reader.getSpectrum()
;      reader.setFileName, file2
;      oplot, reader.getBands(), reader.getSpectrum()
;-

;+
; :Description:
;    Constructor.
;
; :Params:
;    fileName: in, optional, type = string
;     File name of an ASD FieldSpec file.
;
;
;-
function hubIOASDReader::init, fileName
  if isa(fileName) then self.setFileName, fileName
  return,1b
end

;+
; :Description:
;    Returns a structure representing the header of an ASD FieldSpec file.
;
;-
function hubIOASDReader::getHeader
  return, *self.header
end
;+
; :Description:
;    Returns the spectral values.
;-
function hubIOASDReader::getSpectrum
  return, *self.spectrum
end
;+
; :Description:
;    Returns the number of bands.
;
;-
function hubIOASDReader::getBands
  return, *self.bands
end

;+
; :Description:
;    Sets the ASD file reader on the ASD FieldSpec File. 
;
; :Params:
;    fileName: in, required, type = string
;     File path of ASD FieldSpec File.
;
;-
pro hubIOASDReader::setFileName, fileName
  finfo = FILE_INFO(filename)
  if ~finfo.exists then message, 'File: ' + string(filename) + ' does not exist'
  if ~finfo.read then message, 'Can not read file: ' + string(filename)
  self.fileName = fileName
  openr, lun, self.fileName, /GET_LUN
    header = self._readHeaderStruct(lun)
    self.header = ptr_new(header)
    self.spectrum =  ptr_new(self._readSpectrum(lun, *self.header))
  free_lun, lun
  bands = findgen(header.channels) * header.wavel_step + header.ch1_wavel
  self.bands = ptr_new(bands)
end

;+
; :hidden:
; :Description:
;    Reads the spectal values of the recent ASD file.
;
; :Params:
;    lun
;    hdrStruct
;
;
;
;-
function hubIOASDReader::_readSpectrum, lun, hdrStruct
  point_lun, lun, 484
  case hdrStruct.data_format of
    0 : values = make_array(hdrStruct.channels, /Float)
    1 : values = make_array(hdrStruct.channels, /Integer)
    2 : values = make_array(hdrStruct.channels, /Double)
    3 : message, 'unknown data type'
  endcase
  readu, lun, values
  return, values
end

;+
; :hidden:
; :Description:
;    Reads the header of the recent ASD file.
;
; :Params:
;    lun
;
;
;
;-
function hubIOASDReader::_readHeaderStruct, lun
  ;INIT VARIABLES TO READ
  point_lun, lun, 0
  hdrInfo = { $
     co      :strjoin(self._readBytes(lun, string(replicate(' ', 3)))) $
   , comments:strjoin(self._readBytes(lun, string(replicate(' ', 157)))) $
   , when: { $
       tm_sec:self._readBytes(lun, 0) $
      ,tm_min:self._readBytes(lun, 0) $
      ,tm_hour:self._readBytes(lun, 0) $
      ,tm_mday:self._readBytes(lun, 0) $
      ,tm_mon:self._readBytes(lun, 0) $
      ,tm_year:self._readBytes(lun, 0) $
      ,tm_wday:self._readBytes(lun, 0) $
      ,tm_yday:self._readBytes(lun, 0) $
      ,tm_isdst:self._readBytes(lun, 0) $
      } $
   , program_version: self._readBytes(lun, 0b) $
   , file_version : self._readBytes(lun, 0b) $
   , itime : self._readBytes(lun, 0b) $
   , dc_corr:self._readBytes(lun, 0b) $
   , dc_time:self._readBytes(lun, 0l) $
   , data_type:self._readBytes(lun, 0b) $
   , ref_time:self._readBytes(lun, 0l) $
   , ch1_wavel:self._readBytes(lun, 0.) $
   , wavel_step:self._readBytes(lun, 0.) $
   , data_format:self._readBytes(lun, 0b) $
   , old_dc_count:self._readBytes(lun, 0b) $
   , old_ref_count:self._readBytes(lun, 0b) $
   , old_sample_count:self._readBytes(lun, 0b) $
   , application:self._readBytes(lun, 0b) $
   , channels:self._readBytes(lun, 0u) $
   ;, app_data:BYTARR(128) $ ;not supported
   , gps_data: { $
          true_heading: self._readBytes(lun, 0d, position=334) $
         ,speed: self._readBytes(lun, 0d) $
         ,latitude: self._readBytes(lun, 0d) $
         ,longitude: self._readBytes(lun, 0d) $
         ,altitude: self._readBytes(lun, 0d) $
         ,flags: self._readBytes(lun, [0b,0b]) $
         ,hardware_mode: self._readBytes(lun, string(' ')) $
         ,timestamp: self._readBytes(lun, 0l) $
         ,flags2:self._readBytes(lun, [0b,0b]) $
         ,satellites:self._readBytes(lun, string(replicate(' ', 5))) $
         ,filler:self._readBytes(lun, string(' ')) $         
      } $
   , it:self._readBytes(lun, ULONG(0), position=390) $
   , fo:self._readBytes(lun, fix(0)) $
   , dcc:self._readBytes(lun, fix(0)) $
   , calibration: self._readBytes(lun, 0u) $
   , instrument_num: self._readBytes(lun, 0u) $
   , ymin:self._readBytes(lun, 0.) $
   , ymax:self._readBytes(lun, 0.) $
   , xmin:self._readBytes(lun, 0.) $
   , xmax:self._readBytes(lun, 0.) $
   , ip_numbits: self._readBytes(lun, 0u) $ 
   , xmode:self._readBytes(lun, 0b) $
   , flags:self._readBytes(lun, BYTARR(4)) $
   , dc_count: self._readBytes(lun, 0u) $
   , ref_count: self._readBytes(lun, 0u) $
   , sample_count: self._readBytes(lun, 0u) $
   , instrument:self._readBytes(lun, 0b) $
   , bulb:self._readBytes(lun, 0ul) $
   , swir1_gain: self._readBytes(lun, 0u) $
   , swir2_gain: self._readBytes(lun, 0u) $
   , swir1_offset: self._readBytes(lun, 0u) $
   , swir2_offset: self._readBytes(lun, 0u) $
   , splice1_wavelength: self._readBytes(lun, 0.) $
   , splice2_wavelength: self._readBytes(lun, 0.) $
   ;, when_in_ms: self._readBytes(lun, bytarr(12)) $
  }
  
  ;spare = self._readBytes(lun, bytarr(20)) ;not supported 
  
  return, hdrInfo
end

;+
; :hidden:
; :Description:
;    Describe the procedure.
;
; :Params:
;    lun
;    type
;
; :Keywords:
;    position
;
;-
function hubIOASDReader::_readBytes, lun, type, position=position
  if isa(position) then point_lun, lun, position
  t2 = type
  readu, lun, t2
  return, t2 
end

;+
; :Hidden:
;-
pro hubIOASDReader__define
  struct = {hubIOASDReader $
    ,inherits IDL_Object $
    ,spectrum:ptr_new() $
    ,bands: ptr_new() $
    ,header: ptr_new() $
    ,filename:'' $
  }
end

;+
; :hidden:
;
;-
pro test_hubIOASDReader
  file1 = 'D:\Sandbox\Messprotokoll\In\gi.000'
  file2 = 'D:\Sandbox\Messprotokoll\In\gi.005'
  
  reader = hubIOASDReader()
  reader.setFileName, file1
  plot, reader.getBands(), reader.getSpectrum()
  reader.setFileName, file2
  oplot, reader.getBands(), reader.getSpectrum()
end