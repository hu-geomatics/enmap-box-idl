;+
; This object provides access on ascii files and allows to read them sequentially.
; 
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
; 	
; :Examples:
;   This shows you how to initialize a `hubIOASCIIInput` object and read all lines of a text file
;   in blocks of 10 lines::
;
;        fileName = enmapBox_getTestFile('ASCII_List')
;        
;        reader = hubIOASCIIInput(fileName)
;        
;        
;        print, 'Total number of lines:', reader.FileLines()
;        reader.initReader, 10, ExcludeEmptyLines=0
;        
;        while ~reader.readingDone() do begin &$
;          print, reader.getData()            &$
;          print, '--------------------'      &$
;        endwhile                             
;        reader.cleanup
;
;   The results should look like::
;         
;        . . . 
;        674105.367000 5298022.492000  3 3.000000  0.05  90.0000
;        672905.367000 5297812.492000  3 9.000000  0.05  42.1269
;        673325.367000 5297332.492000  3 8.034620  0.05  87.0082
;        676865.367000 5297092.492000  3 3.000000  0.05  40.0000
;        
;        
;        
;        --------------------
;        
;   You can exclude the empty lines by `ExcludeEmptyLines=1`.
;  
;-

;+
; :Description:
;    Constructor of `hubIOASCIIInput` object.
;    
;
; :Params:
;    fileName: in, required, type=string
;     The file path of the text file you want to read.
;
;-
function hubIOASCIIInput::init, fileName

  finfo = FILE_INFO(filename)
  if ~finfo.exists then message, 'File: ' + string(filename) + ' does not exist'
  if ~finfo.read then message, 'Can not read file: ' + string(filename)
  
  self.fileName = fileName
  self.LinesTotal = FILE_LINES(fileName)
  self.consistencyRegex = ''
  openr, lun, self.fileName, /GET_LUN
  self.logicalUnitNumber = lun
  self.linesPerStep = 500
  self.recentLine = 1 ;count lines beginning with one
  return,1b
end

;+
; :Description:
;    Returns the absolute number of file lines.
;-
function hubIOASCIIInput::numberOfFileLines
  return, self.linesTotal
end

;+
; :Description:
;    Initializes the reader settings.
;
; :Params:
;    numberOfLines: in, optional, type=int, default=maximum number of lines
;     The maximum number of lines to read within one reading step.
;      
; :Keywords:
;    firstLine: in, optional, type=int, default = 1
;     The number of the first line to read.
;
;    excludeEmptyLines: in, optional, type=boolean
;     Set this to exclude empty lines. An empty line is a line filled with
;     white-space characters only. 
;
;    excludeLineComments: in, optional, type=boolean
;     Set this to exclude commented lines. A commented line is a line that
;     starts with a `#` or `;` character.
;-
pro hubIOASCIIInput::initReader, numberOfLines $
      , firstLine=firstLine $
      , excludeEmptyLines=excludeEmptyLines $
      , excludeLineComments=excludeLineComments $
      , consistencyRegex=consistencyRegex 
      
  if self.readerInitialized then message, 'hubIOASCIIInput.Reader already initialized.'
  
  if isa(consistencyRegex) then begin
      self.consistencyRegex = consistencyRegex
  endif else begin
    self.consistencyRegex = ''
    patterns = []
    if keyword_set(excludeEmptyLines) then patterns = [patterns, '^[ ]*$']
    if keyword_set(excludeLineComments) then patterns = [patterns, '^[ ]*[#;].*$']
    if n_elements(patterns) gt 0 then begin
      self.consistencyRegex = '('+strjoin(patterns, '|')+')'
    endif
  endelse
  
  if ~keyword_set(numberOfLines) then begin
    self.LinesPerStep = self.LinesTotal
  endif else begin
    if numberOfLines le 0 then message, 'numberOfLines must be greater than zero'
    self.LinesPerStep = numberOfLines
  endelse
  
  self._setLine, isa(firstLine) ? firstLine : 1
  self.readerInitialized = 1b  
end

pro hubIOASCIIInput::finishReader
  self.readerInitialized = 0b
end


;+
; :Description:
;    Shifts the file pointer to a certain text line
;
; :Params:
;    line: in, required, type=int 
;     the line you want to jump to. (one based -> first line = number 1)
;-
pro hubIOASCIIInput::_setLine, line
  lineShift = line - self.recentLine 
  if lineShift eq 0 then return
  if lineShift gt 0 then begin
    skip_lun, self.logicalUnitNumber, lineShift, /Lines
  endif else begin
    point_lun, self.logicalUnitNumber, 0
    SKIP_LUN, self.logicalUnitNumber, line-1, /Lines
  endelse
  self.recentLine += lineShift  
end


;+
; :Description:
;    Returns the recent line number the reader is pointing to.
;-
function hubIOASCIIInput::getRecentLine
  return, self.recentLine
end

;+
; :Description:
;    Allows random access on a certain line
;
; :Params:
;    lineStart: in, required, type=int
;     Index of the first line to read from the text file. 
;    linesToRead: in, required, type=int
;     The maximum number of lines to read. You will get less in case EOF is reached.
;
;
;
;-
function hubIOASCIIInput::_getLines, lineStart, linesToRead
  if lineStart lt 0 or linesToRead le 0 then message,'lineStart and linesToRead must be positive (long) integer values'
  if lineStart ge self.linesTotal then return, !NULL
  
  ;save initial state
  thisLine = self.recentLine
  
  ;set pointer on first line to read
  self._setLine, lineStart
  
  ;read the lines
  nLines = (self.linesTotal - lineStart) < linesToRead
  lines = strarr(nLines) 
  readf, self.logicalUnitNumber, lines
  self.recentLine += nLines
  ;restore initial state
  self._setLine, thisLine
  return, lines
end


;+
; :Description:
;    This function reads the text file and returns the lines in a text array. 
;    
;    Use `initReader` to limit the maximum number of lines returned in one reading step
;    and to specify which pattern the returned lines must match to (e.g. do not read empty lines). 
;    
;    The following example shows how to read 10 lines per iteration step::
;       
;       reader = hubIOASCIIInput('your textfile')
;       reader.init, 10
;       while ~reader.readingDone() do begin
;         print, reader.getData()
;       endwhile
;
;-
function hubIOASCIIInput::getData, linesToRead
  if ~self.readerInitialized then self.initReader
  if self.readingDone() then message, 'reached end-of-file. finish reader and re-initialize it to restart reading'
  if isa(linesToRead) then begin
    nLines = linesToRead < (self.LinesTotal - self.recentLine + 1)
  endif else begin
    nLines = self.linesPerStep < (self.LinesTotal - self.recentLine + 1)
  endelse
  nNewLines = nLines 
  lines = []
  while nNewLines gt 0 do begin
    ;1. get new lines
    newLines = self.readf(nNewLines)
    
    ;2. if required, perform a consistency check for each line
    if self.consistencyRegex ne '' then begin
      iConsistent = where(stregex(newLines, self.consistencyRegex, /BOOLEAN) ne 1 $
                         , /NULL, nComplement=nNotConsistent)
      if isa(iConsistent) then lines = [lines, newLines[iConsistent]]
      nNewLines = nNotConsistent < (self.LinesTotal - self.recentLine + 1)
    endif else begin
      lines = [lines, newLines]
      nNewLines = 0
    endelse
    
  endwhile
    
  return, isa(lines) ? transpose(lines) : !NULL
end


;+
; :Description:
;    Returns the number of text lines. 
;    Use parameter `pattern` to specify a regular expression for counting 
;    specific lines mathing this pattern only.
;    
; :Params:
;    pattern: in, optional, type=regular expression
;      Use this argument to specify a regular expression which has to match for each
;      line that is to be counted.  
;
;-
function hubIOASCIIInput::FileLines, pattern
  lineCount = !NULL
  if ~isa(pattern) then begin
    lineCount = self.LinesTotal
  endif else begin 
    recentLine = self.recentLine
    
    lineCount = 0l
    self._setLine, 0
    while ~self.readingDone() do begin
      linesToRead = self.linesPerStep < (self.LinesTotal - self.recentLine)
      lineCount +=  long(total(stregex(self.readf(linesToRead), pattern, /Boolean)))
    endwhile
    self._setLine, recentLine
  endelse
  return, lineCount
end

;+
; :Description:
;    Wrapper of IDLs internal readf function.
;    Reads a specific number of lines, returns it as string array and
;    increments the number of lines to the objects counter variable
;    `self.recentLine`.
;    
;
; :Params:
;    numberOfLines: in, required, type=int
;     The number of lines to read.
;     Will fail in case it is larger than the number of lines remaining until EOF is reached.
;
;
;-
function hubIOASCIIInput::readf, numberOfLines
  lines = strarr(numberOfLines)
  readf, self.logicalUnitNumber, lines
  self.recentLine += numberOfLines
  return, lines
end

;+
; :Description:
;    Is true in case all lines were read and the file pointer points on EOF. 
;-
function hubIOASCIIInput::readingDone
  return, EOF(self.logicalUnitNumber)
end



;+
; :Description:
;    
;-
pro hubIOASCIIInput::cleanup
  self.finishReader
  free_lun, self.logicalUnitNumber
end

;+
; :Hidden:
;-
pro hubIOASCIIInput__define
  struct = {hubIOASCIIInput $
    ,inherits IDL_Object $
    ,fileName:'' $
    ,consistencyRegex:'' $
    ,linesTotal : 0l $
    ,linesPerStep : -1l $
    ,recentLine:0l $
    ,logicalUnitNumber : 0l $
    ,readerInitialized : 0b $
  }
end

;+
; :Hidden:
;-
pro test_hubIOASCIIInput
fileName = enmapBox_getTestFile('ASCII_List')

reader = hubIOASCIIInput(fileName)


print, 'Total number of lines:', reader.FileLines()
reader.initReader, 10, ExcludeEmptyLines=0

while ~reader.readingDone() do begin
  print, reader.getData()
  print, '--------------------'
endwhile
reader.cleanup
end