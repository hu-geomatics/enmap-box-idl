;+
; :hidden:
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

;+
; :Description:
;    This function parses the beginning of an ENVI ASCII Output styled 
;    ASCII file for relevant ENVI header information.
;    
;
; :Params:
;    filename: in, required, type=string
;     The file path of the ascii file to read.
;     
; :Keywords:
;    lines: in, optional, type=integer
;     Set this to limit the number of text line to read.
;
;-
function hubIOASCIIHeader_parseENVIASCIIHeader, filename, lines=lines, tolerant=tolerant
  tags = Hash()
  if ~(hubIOHelper()).fileReadable(filename) then begin
    message,'Can not open file: '+string(filename)
  endif
  ascii = (hubIOASCIIHelper()).readFile(filename, Lines=lines)
  

  foreach line, ascii do begin
    if stregex(line, 'file dimensions:', /BOOLEAN, /FOLD_CASE) then begin
      samples = stregex(line, '[0-9]+ samples', /EXTRACT, /FOLD_CASE)
      lines   = stregex(line, '[0-9]+ lines', /EXTRACT, /FOLD_CASE)
      bands   = stregex(line, '[0-9]+ bands', /EXTRACT, /FOLD_CASE)
      
      samples = stregex(samples, '^[0-9]+', /EXTRACT)
      lines   = stregex(lines, '^[0-9]+', /EXTRACT)
      bands   = stregex(bands, '^[0-9]+', /EXTRACT)
      if strlen(samples) gt 0 then tags['samples'] = samples
      if strlen(lines) gt 0 then tags['lines'] = lines
      if strlen(bands) gt 0 then tags['bands'] = bands
      
      ;that's all. Stop parsing and create meta objects
      break
    endif
  endforeach
  
  
  
  metas = list()
  foreach metaName, tags.keys() do begin
    metas.add, hubIOImgMeta_createFromValue(metaName, tags[metaName])
  endforeach

  return,  metas
  

end


;+
; :hidden:
;-
pro test_hubIOASCIIheader_parseENVIASCIIHeader
  filename = hub_getTestImage('Hymap_Berlin-A_Image')+'.hdr'
  filename = 'D:\Sandbox\ASCII_Data\fromarcgis.txt'
  metas = hubIOASCIIheader_parseENVIASCIIHeader(filename)
  print ,'+++++++++','PRINT: meta values:'
  foreach meta, metas do begin
    print, meta
  endforeach 

end