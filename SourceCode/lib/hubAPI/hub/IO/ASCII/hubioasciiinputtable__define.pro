;+
; This object reads text files containing CSV structured tables.
; 
;  
;  
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
; 	
; :Examples:
;   First we should have a look into the text file we want to read::
;     
;     fileName = enmapBox_getTestFile('ASCII_List')
;     hubHelper.openFile, fileName
;       
;   As you can see, this CSV file is structured by   
;
;    - Line 1 - 6: some descriptions
;    
;    - Line 7: column names, separated by a tabulator character
;    
;    - Line 8 - 127: data lines, separated by a tabulator character
;    
;    - Line 128 - 131: some empty lines
;     
;   Since we are interested in the data lines, we need to start reading in line 8::
;   
;      reader = hubIOASCIIInputTable(fileName)
;      reader.initReader, FirstDataLine=8, delimiter=string(9b)
;      data = reader.getData()
;      reader.cleanup
;
;   Note that the empty lines (128-131) are not part of the CSV table::
;    
;       IDL> help, data
;       DATA            STRING    = Array[6, 120]
;       IDL> print, data[*,-1]
;       676865.367000 5297092.492000 3 3.000000 0.05 40.0000
;   
;-

;+
; :Description:
;    Constructor to create a `hubIOASCIIInputTable` object. 
;    
;    
;    
;   
;   
; :Params:
;    fileName: in, required, type=filepath
;     Path of CSV-table file.
; 
;-
function hubIOASCIIInputTable::init, fileName
      
  !NULL = self->hubIOASCIIInput::Init(fileName)
  
  self.headerComments = List()
  self.headerData = Hash()
  
  self._setDelimiter
  
  self.numberOfColumns = -1 ;default behaviour: return as many colums as possible single string
  self._nColumns = -1 ;internal variable to safe the max. number of columns to read
  self.firstDataLine = 1l
  return,1b
end


;+
; :Description:
;    Returns the (non-zero based) line number where the reader starts to read data values.
;    This value is set using the `initReader` routine.
;-
function hubIOASCIIInputTable::getFirstDataLine
  return, self.firstDataLine
end

;+
; :Hidden:
; :private:
; 
;-
pro hubIOASCIIInputTable::_setDelimiter, delimiter=delimiter, isRegex=isRegex
  if isa(delimiter) then begin 
    self.delimiter = delimiter
    self.delimiterIsRegex = keyword_set(isRegex)
  endif else begin
    self.delimiter = '[ ]+'
    self.delimiterIsRegex = 1b
  endelse  
end


;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    numberOfLines: in, required, type=int
;     The maximum number of text lines to read in one step.
;
; :Keywords:
;    firstDataLine: in, optional, type=int, default=1
;     The first line in the text file containing data values.
;     
;    delimiter: in, optional, type=string, default='[ ]+'
;     Use a single character or regular expression to describe how the columns are separated
;     within each text line.
;     
;     The default setting is `delimiter = '[ ]+'` and `/isRegex` which basically means
;     that each white space within a line is interpreted as separating value. 
;     For example the string `'abc 99.99    xyz     -23'` will be splitted into
;     ['abc','99.00','xyz','-23'].
;
;    numberOfColumns: in, optional, type=int
;     The following values are allowed::
;       numberOfColumns = -1 : (default) returns the data with the maximum number of columns that can 
;                              be separated by the delimiter
;       numberOfColumns = 0  : returns text lines without separating them by a delimiter
;       numberOfColumns = n  : returns an array with n columns (and throws an error in case there are less or more colums) 
;     
;    isRegex: in, optional, type=string
;     Use this to tell that the `delimiter` used is a regular expression.
;
;-
pro hubIOASCIIInputTable::initReader, numberOfLines $
      , firstDataLine=firstDataLine $
      , delimiter=delimiter $
      , isRegex = isRegex $
      , numberOfColumns=numberOfColumns 
      
  if isa(numberOfColumns) then begin
    self.numberOfColumns = numberOfColumns
  endif
  
  if isa(delimiter) then begin
    self._setDelimiter, delimiter=delimiter, isRegex = keyword_set(isRegex)
  endif 
   
 
  if isa(firstDataLine) then begin
    self.firstDataLine = firstDataLine
  endif
  self._nColumns = -1
  self->hubIOASCIIInput::initReader, numberOfLines, firstLine=self.firstDataLine, /ExcludeEmptyLines, /EXCLUDELINECOMMENTS
end



;+
; :Description:
;   This functions reads the lines from a text file and returns it as string-array.
;   
;   If the `delimiter` property is used, each line will be splitted and the array has the following
;   dimensions: `array[columns,lines]`.
;   
;   Leading and trailing white-spaces will be removed
;-
function hubIOASCIIInputTable::getData, linesToRead
  ;get blank text lines
  lines = self->hubIOASCIIInput::getData(linesToRead)
  
  if ~isa(lines) then return, !NULL
  
  ;remove leading and trailing white-spaces
  lines = strtrim(lines, 2)
  
  if strlen(self.delimiter) eq 0 then return, lines
  
  ;split lines by delimiter 
  ;-> lines becomes a list of strings
  lines = strsplit(lines, self.delimiter, Regex=self.delimiterIsRegex, /Preserve_null, /extract, Count=count)
  if typename(lines) ne 'LIST' then lines = List(lines)
    

  ;set the maximum number of columns of the returned data array
  ;if there are more than one row/line, use the first one 
  ;in case other lines have more cells, they will be truncated
  ;so always ensure to have a proper first line
  if self._nColumns eq -1 then begin
    case self.numberOfColumns of
       -1 : self._nColumns = n_elements(lines[0])
        0 : self._nColumns = 1
      else: self._nColumns = self.numberOfColumns
    endcase
  endif
  
  ;initialize the final returned string array 
  if self.numberOfColumns eq 0 then begin
    array = strarr(self._nColumns, total(count))
  endif else begin
    array = strarr(self._nColumns, n_elements(lines))
  endelse
  
  ;add each cell/line values to final string array
  foreach line, lines, iRow do begin
      nCells = n_elements(line)
      
      ;check if the line has at least _nColumns
      ;in case of a predefined number of required columns
;      if self.numberOfColumns ne 0 && nCells lt self._nColumns then begin
;          message, string(format='(%"got %i instead of %i column values in/before line %i. Wrong delimiter used?")' $
;                   , nCells, self._nColumns, iRow + self.firstDataLine)
;      endif
      
      ;add cell values to array
      ;add line to array
      if self.numberOfColumns eq 0 then begin
        array[0, offset:offset+nCells-1] = line
        offset += nCells
      endif else begin
        iLast = (nCells < self._nColumns)-1
        if self.numberOfColumns ne -1 && nCells ne self._nColumns then begin
          message, string(format='(%"Got %i cells instead of %i in line %i")' $
                   , ncells, self._nColumns, iRow + self.recentLine - n_elements(lines))
        endif
        
        array[0:iLast, iRow] = line[0:iLast]
      endelse
  endforeach 
    
  array = strtrim(array)
  length = total(strlen(array),1)
  array = array[*, where(length gt 0, /NULL)]
  !NULL = temporary(lines)
  return, array
end



;+
; :Hidden:
;
;
;
;
;
;-
pro hubIOASCIIInputTable__define
struct = {hubIOASCIIInputTable $
    ,inherits hubIOASCIIInput $
    ,headerData:hash() $
    ,headerComments:List() $
    ,ioImgHeader:hubIOImgHeader() $
    ,delimiter:'' $ 
    ,delimiterIsRegex:0b $
    ,firstDataLine:1l $
    ,numberOfColumns:-1 $
    ,_nColumns:-1 $
}
end


;+
; :Hidden:
;-
pro test_hubIOASCIIInputTable
 
    fileName = enmapBox_getTestFile('ASCII_List')
    hubHelper.openFile, fileName
    
    reader = hubIOASCIIInputTable(fileName)
    reader.initReader, FirstDataLine=8, delimiter=string(9b)
    data = reader.getData()
    reader.cleanup
    help, data
    
    reader.cleanup

end