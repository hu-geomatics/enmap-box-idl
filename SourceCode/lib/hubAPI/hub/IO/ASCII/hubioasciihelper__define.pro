;+
; This object helps to read or write ACSII text files.
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the content of an ASCII file (`result = string[]`).
;
; :Params:
;    filename : in, required, type=string
;      Full path to the ascii file.
;
; :Keywords:
;    Lines: in, optional, type=int
;      Last line to read.
;
function hubIOASCIIHelper::readFile, filename, Lines=lines
  compile_opt static, strictarr  
  if ~file_test(filename) then begin
    message, 'Can not find file: ' + string(filename)
  endif
  
  if ~isa(lines) then begin
    lines = file_lines(filename)
  endif
  
  if lines gt 0 then begin 
    ascii = strarr(lines)
    openr, unit, filename, /GET_LUN
    readf, unit, ascii
    free_lun, unit
    return, ascii
  endif else begin
    ascii = ['']
  endelse
end

;+
; :Description:
;    Writes content to an ASCII file.
;
; :Params:
;    filename : in, required, type=string
;      Full path to the ascii file.
;
;    ascii : in, required, type=string[]
;      Array with text.
;
; :Keywords:
;    append: in, optional, type=boolean
;     Set this keyword to append the ascii data to an probably existing file.
;
;-
pro hubIOASCIIHelper::writeFile, filename, ascii, append=append
  compile_opt static, strictarr
  if ~file_test(file_dirname(filename)) then begin
    file_mkdir, file_dirname(filename)
  endif
  
  if isa(ascii) then begin
    width = max(strlen(ascii))
  endif
  
  openw, unit, filename, /GET_LUN, WIDTH=width, APPEND=append

  if isa(ascii) then begin
    printf, unit, transpose(ascii[*])
  endif
  
  flush, unit
  free_lun, unit

end

;+
; :Description:
;    Reads a text file and prints it to the IDL console.
;
; :Params:
;    filename: in, required, type = string
;     Absolute file path of the text file that is to print. 
;
;
;
;-
pro hubIOASCIIHelper::printFile, filename
  compile_opt static, strictarr
  ascii = self.readFile(filename)
  print, transpose(ascii[*])

end

;+
; :hidden:
;-
pro hubIOASCIIHelper__define
  struct = {hubIOASCIIHelper $
    ,inherits IDL_Object $
  }
end