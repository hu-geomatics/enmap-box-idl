;+
; :hidden:
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    This routine parses the beginning of an ArcGIS exported 
;    ASCII file conaining image grid data. It returns a list with hubIOImkgMeta_... objects.
;
; :Params:
;    filename: in, required, type=string
;     File path of ascii file that is to read.
;
; :Keywords:
;    lines: in, optional, type=integer
;     Set this to limit the number of lines that are read for parsing.
;
;-
function hubIOASCIIHeader_parseArcGISHeader, filename, lines=lines, Tolerant=Tolerant
  tags = Hash()
  if ~(hubIOHelper()).fileReadable(filename) then begin
    message,'Can not open file: '+string(filename)
  endif
  ascii = (hubIOASCIIHelper()).readFile(filename, Lines=lines)
  
  regIsMetaValue = '^[A-Za-z_]+[ ]+[A-Za-z0-9+-\,\.]+'
  regIsEmptyLine = '^[ \t]*$'
  regIsDataLine  = '^[ \t]*[0-9]+'
  foreach line, ascii do begin
    ;line is empty? go to next line
    if stregex(line, regIsEmptyLine, /BOOLEAN) then begin
      continue
    endif
    
    ;line starts with a word? try to parse tag name and tag value
    if stregex(line, regIsMetaValue, /BOOLEAN) then begin
      tag = stregex(line, regIsMetaValue, /Extract)
      splitted = strsplit(tag, '[ \t]+', /Regex, /EXTRACT)
      splitted = strlowcase(splitted)
      ;replace comma with dot
      splitted[1] = strjoin(strsplit(splitted[1], ',', /Extract),'.')
      case splitted[0] of
        'ncols'        : tags['samples']   = fix(splitted[1])
        'nrows'        : tags['lines']     = fix(splitted[1])
        'xllcorner'    : tags['xllcorner'] = double(splitted[1])
        'yllcorner'    : tags['yllcorner'] = double(splitted[1])
        'cellsize'     : tags['cellsize']  = double(splitted[1])
        'nodata_value' : tags['data ignore value'] = double(splitted[1])
        else: if ~keyword_set(tolerant) then message, 'Unknown header value: '+tag
      endcase
      continue ;go to next line
    endif 
    ;nothing found? 
    ;must be the data part, so stop parsing
    break 
  endforeach
  
  
  
  metas = list()
  if tags.hubIsa(['samples','lines','xllcorner','yllcorner','cellsize'], /EVALUATEAND) then begin
    meta = hubIOImgMeta_mapInfo()
    mapInfo = meta.createEmptyMapInfoStruct()
    mapInfo.pixelX = 1
    mapInfo.pixelY = tags['lines']
    mapInfo.sizeX = tags['cellsize']
    mapInfo.sizeY = tags['cellsize']
    mapInfo.easting = tags['xllcorner']
    mapInfo.northing = tags['yllcorner']
    meta.setValue, mapInfo
    metas.add, meta
  endif
  tags.hubRemove, ['yllcorner','xllcorner','cellsize']
  foreach metaName, tags.keys() do begin
    metas.add, hubIOImgMeta_createFromValue(metaName, tags[metaName])
  endforeach

  return,  metas
  

end


;+
; :hidden:
;-
pro test_hubIOASCIIHeader_parseArcGISHeader

  filename = hub_getTestImage('Hymap_Berlin-A_Image')+'.hdr'
  filename = 'D:\Sandbox\ASCII_Data\fromarcgis.txt'
  metas = hubIOASCIIHeader_parseArcGISHeader(filename)
  print ,'+++++++++','PRINT: meta values:'
  foreach meta, metas do begin
    print, meta
  endforeach 

end