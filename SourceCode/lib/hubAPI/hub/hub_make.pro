;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro hub_make, Distribution=distribution

  ; copy files
  codeDir = filepath('hubAPI', ROOT_DIR=enmapBox_getDirname(/EnMAPProject, /SourceCode), SUBDIRECTORY='lib')
  targetDir = filepath('hubAPI', ROOT_DIR=enmapBox_getDirname(/EnMAPProject), SUBDIRECTORY='lib')
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir
  if ~enmapboxmake.copyOnly() then begin
    
    ; create SAVE
    SAVEFile = filepath('hubAPI.sav', ROOT_DIR=targetDir)
    hubDev_compileDirectory, codeDir, SAVEFile, nolog=enmapboxmake.nolog()
  
    ; create documentation
    if ~enmapboxmake.noIDLDoc() then begin
      docDir = filepath('', ROOT_DIR=targetDir, SUBDIR=['help','idldoc'])
      title='hubAPI Documentation'
      hub_idlDoc, codeDir, docDir, title, NOSHOW=enmapboxmake.noShow()
    endif
    
     ; create distributen
    
    if isa(distribution) then begin
    
      if ~isa(distribution, 'string') then begin
        title = 'Select a directory for writing the distribution.'
        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
        if directory eq '' then return
      endif else begin
        directory = distribution
      endelse

      destination = filepath(file_basename(targetDir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(targetDir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
      file_copy, targetDir, destination, /OVERWRITE, /RECURSIVE
      destination = filepath(file_basename(targetDir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(targetDir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
      file_copy, targetDir, destination, /OVERWRITE, /RECURSIVE
      
    endif
  endif
end