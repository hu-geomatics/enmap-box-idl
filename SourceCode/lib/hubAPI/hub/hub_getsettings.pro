;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query hubAPI setting. The settings file is located under:
;    `.\enmapProject\lib\hubAPI\resource\settings.txt`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, hub_getSettings()
;      
;-
function hub_getSettings
  return, hubSettings_getSettings()
end

;+
; :Hidden:
;-
pro test_hub_getSettings

  print, hub_getSettings()

end