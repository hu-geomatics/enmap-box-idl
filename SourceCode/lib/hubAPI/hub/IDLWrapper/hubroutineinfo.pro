;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Wrapper for IDL `routine_info` function used with the `/PARAMETERS` keyword. 
;    Additionally, this wrapper adds the `MISSING `and the `SYSTEM `tags to the result structure.
;    MISSING is 1 if the routine can not be compiled and 0 otherwise.
;    SYSTEM is 1 if the routine is a system routine.
;    Note that both, missing and system routines, do not support the `routine_info` result structure.
;
; :Params:
;    routine: in, required, type=routine name
;      Use this keyword to specify the routine name.
;
; :Keywords:
;    Functions=functions
;      Set this keyword to look for a function (locking for a procedure is the default). 
;-
function hubRoutineInfo, routine, Functions=functions

  systemRoutines = routine_info(/SYSTEM, FUNCTIONS=functions)
  if isa(where(/NULL, strcmp(systemRoutines, routine, /FOLD_CASE))) then begin
    system = {system : 1b}
  endif else begin
    system = {system : 0b}
  endelse

  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    return, create_struct({missing:~system.system}, system)
  endif
  resolve_routine, routine, /NO_RECOMPILE, IS_FUNCTION=functions
  catch, /CANCEL

  result = routine_info(routine, _EXTRA=_extra, /PARAMETERS, FUNCTIONS=functions)
  result = create_struct(result, {missing:0b}, system)
  return, result
end