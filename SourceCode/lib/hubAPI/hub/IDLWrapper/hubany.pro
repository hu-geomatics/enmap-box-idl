function hubAny, condition, indices=indices
  compile_opt strictarr
  indices = where(condition, count)
  return, count gt 0L
end

pro test_hubAny

  a = [1,2,3,4,5]
  if hubany(a ge 3, indices=i) then begin
    print, 'True'
    print, a[i]
  endif
end