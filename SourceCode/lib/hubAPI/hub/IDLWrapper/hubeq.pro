;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Wrapper for IDL equality operator `EQ`.
;
; :Keywords:
;    Scalar=scalar
;      Set this keyword to return a scalar, even if the arguments are arrays. 
;      For `hubEQ([1,2,3], [1,2,5])` the function returns 0 instead of [1,1,0].   
;-
function hubEQ, arg1, arg2, Scalar=scalar

  result = arg1 eq arg2
  if keyword_set(scalar) then begin
    result = product(result, /PRESERVE_TYPE) eq 1b
  endif
  return, result
end