;+
; Use this batch to force IDL to check if a callback (widget events,  occurs.   
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;
;-

  !null = widget_event(/NOWAIT)