;+
; :Description:
;    Returns true in case one of up to 10 keywords is set. 
;
; :Params:
;    key1: in, optional, type='Hash'
;     
;    key2: in, optional, type='Hash'
;    key3: in, optional, type='Hash'
;    key4: in, optional, type='Hash'
;    key5: in, optional, type='Hash'
;    key6: in, optional, type='Hash'
;    key7: in, optional, type='Hash'
;    key8: in, optional, type='Hash'
;    key9: in, optional, type='Hash'
;    key10: in, optional, type='Hash'
;
;  :Returns:
;     `false` in case no keyword is set.
;     
;     `false` in case more than one keyword is set.
;     
;     `true` in case only one of the specified keywords was set.
;
;-
function hub_isExclusiveKeyword, key1, key2, key3, key4, key5, key6, key7, key8, key9, key10

  isExclusive = 1 eq total([keyword_set(key1), keyword_set(key2), keyword_set(key3), keyword_set(key4), keyword_set(key5), keyword_set(key6), keyword_set(key7), keyword_set(key8), keyword_set(key9), keyword_set(key10)])
  return, isExclusive

end

