pro hub_saveAppStates_clearHash, hash
  ; eliminate variables that can not be stored in SAVE files
  foreach value, hash, key do begin
    if isa(value, 'IDLJAVAOBJECT') then hash[key] = !null
    if isa(value, 'HASH') then hub_saveAppStates_clearHash, value
  endforeach
end

pro hub_saveAppStates, filename
  appStates = hub_getAppState(/All)
  hub_saveAppStates_clearHash, appStates
  save, appStates, FILENAME=filename
end