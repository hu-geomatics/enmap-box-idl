;+
; :Description:
;    Starts hubAPI developer documentation.
;-
pro hubHelp
  filename = filepath('index.html', ROOT_DIR=hub_getDirname(), SUBDIRECTORY=['help','idldoc'])
  hubHelper.openFile, filename, /HTML
end