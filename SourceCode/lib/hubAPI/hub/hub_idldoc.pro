;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Wrapper for `IDLdoc`, provides some predefined settings, is used as a short cut.
;
; :Params:
;    root:
;      See `IDLdoc`.
;      
;    output: 
;      See `IDLdoc`.
;       
;    title:
;      See `IDLdoc`.
;
; :Keywords:
;    OVERVIEW: in, optional, type=filename
;      See `IDLdoc`. This wrapper looks for a default overview file under root/_IDLDOC/overview.
;    
;    FOOTER: in, optional, type=filename
;      See `IDLdoc`. This wrapper looks for the default EnMAP-Box footer.
;      
;    SUBTITLE: in, optional, type=string
;      See IDLdoc`.
;      
;    NoShow: in, optional, type=boolean
;      Set this keyword to NOT open the documentation inside the web browser after creation.
;
;-
pro hub_idlDoc, root, output, title, OVERVIEW=overview, FOOTER=footer, NoShow=noShow, subtitle=subtitle
  
  ;todo: use global flag instead keyword
  if ~isa(noShow) then noShow = 1b
  
  
  if ~isa(root) then message, 'Missing argument: root.'
  if ~isa(output) then message, 'Missing argument: output.'
  if ~isa(title) then message, 'Missing argument: title.'

  if ~isa(overview) then begin
    defaultOverview = filepath('overview', ROOT_DIR=root, SUBDIR='_IDLDOC')
    if file_test(defaultOverview, /REGULAR) then begin
      overview=defaultOverview
    endif
  endif
  
  if ~isa(footer) then begin
    defaultFooter=filepath('IDLdocFooter', ROOT=enmapBox_getDirname(/EnmapProject,/SourceCode))
    if file_test(defaultFooter, /REGULAR) then begin
      footer=defaultFooter
    endif
  endif

  file_delete, output, /Recursive, /Allow_nonexistent

  idldoc, ROOT=root $
        , OUTPUT=output $
        , OVERVIEW=overview $
        , TITLE=title $
        , SUBTITLE=subtitle $
        , FOOTER=footer $
        , NoSource=0b $
        , FORMAT_STYLE='rst' $
        , /COLOR_OUTPUTLOG $
        , /QUIET $
        , /USER

  if ~keyword_set(noShow) then begin
    hubHelper.openFile, /HTML, filepath('index.html', ROOT_DIR=output)
  endif

end