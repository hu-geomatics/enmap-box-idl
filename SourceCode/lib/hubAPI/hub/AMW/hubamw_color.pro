;+
; :Description:
;    This widget allows the selection of an RBG color.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;    .. image:: _IDLDOC/color1.gif
;    .. image:: _IDLDOC/color2.gif
;        
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash key returns the RGB color triple.
;
; :Keywords:
;    Title: in, optional, type = string
;      Use this keyword to specify the title of the parameter-widget. 
;      The title is placed prior ti the rectancle showing the selected color.
;      
;    SuffixTitle: in, optional, type = string
;      Use this keyword to specify the title  of the parameter-widget.
;      The title is placed behind the rectancle showing the selected color.
;       
;    Value : in, optional, type = byte[3]
;      Use this keyword to specify a default RGB color.
;      
;    Editable : in, optional, type=boolean, default=1
;      Use this keyword to allow or forbid color selection.
;      
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;       
;    XSize : in, optional, type=int
;       Use this keyword to specify the widget width in pixels (only the colored rectangle).     
;
;    YSize : in, optional, type=int
;       Use this keyword to specify the widget hight in pixels (only the colored rectangle).     
;      
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create an color selection widget::
;    
;      hubAMW_program 
;      hubAMW_color, 'color', Title='Select a Color', Value=[255, 0, 0]
;      result = hubAMW_manage()
;      print, result
;       
;   .. image:: _IDLDOC/color1.gif
;
;   Click on the colored rectangle and select a color from the newly created dialog:
;
;   .. image:: _IDLDOC/color2.gif
;
;-
pro hubAMW_color $
  ,name $
  ,Title=title $ 
  ,SuffixTitle=suffixTitle $   
  ,Value=value $ 
  , Editable=editable $
  ,Tooltip=tooltip $
  ,XSize=xSize $
  ,YSize=ySize $  
  ,Optional=optional $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWColor($
     name $
    ,Title=title $ 
    ,SuffixTitle=suffixTitle $   
    ,Value=value $
    , Editable=editable $
    ,Tooltip=tooltip $
    ,XSize=xSize $
    ,YSize=ySize $ 
    ,Optional=optional $
    ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end