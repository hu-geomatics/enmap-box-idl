;+
; :Description:
;    This procedure is used to create an auto-managed widget program window for inserting auto-managed widgets. 
;    The window is a modal top level base with Accept and Cancel buttons.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
; :Params:
;    groupLeader : in, optional, type=long
;      Use this argument to specify the widget ID of an existing widget that serves as group leader for the newly-created auto-managed widget program. 
;      For details search inside the IDL Help for `WIDGET_BASE` procedure and `GROUP_LEADER` keyword. 
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify a title for the auto-managed widgets program window.
;      
; :Examples:
;    Here is a simple hello world example::
;    
;      hubAMW_program, Title='Hello World GUI'
;      hubAMW_parameter, 'text', Title='Enter text ', Value='hello world'
;      result = hubAMW_manage()
;      
; .. image:: _IDLDOC/program1.gif
;-
pro hubAMW_program $
  ,groupLeader $
  ,Title=title $
  ,HideButtons=hideButtons $
  ,NoBlock=noBlock
  
  if ~isa(groupLeader) then begin
    if ~keyword_set(noBlock) then begin
      print,'% No group leader defined via the groupLeader argument. A temporary top level base is created which serves as group leader.'
      groupLeader_ = widget_base()
      _temporaryGroupLeader=1b
    endif
  endif else begin
    groupLeader_ = groupLeader
  endelse
  
  hubAMWProgram = hubAMWProgram(Title=title, groupLeader_, _TemporaryGroupLeader=_temporaryGroupLeader, HideButtons=hideButtons, NoBlock=noBlock)
  hubAMW_setProgram, hubAMWProgram

end

pro test_hubAMW_program

  hubAMW_program, HideButtons=1
  hubAMW_label, replicate('abcdefghijklmnopqrstuvwxyz',10)
  result = hubAMW_manage()

end

pro test_hubAMW_program2
  ; no blocking widget
  hubAMW_program, /NoBlock 
  hubAMW_label, 'this is a not blocking amw programm'
  hubAMW_parameter, 'p', /STRING

  !null = hubAMW_manage()
end
