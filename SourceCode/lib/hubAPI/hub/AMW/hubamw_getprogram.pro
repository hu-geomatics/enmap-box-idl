;+
; :Hidden:
;-

function hubAMW_getProgram

  on_error,2

  common hubAMW_programs,hubAMWPrograms

  if n_elements(hubAMWPrograms) eq 0 then begin
    message,'No active auto-managed widget program.'
  endif

  hubAMWProgram = hubAMWPrograms[-1]

  return,hubAMWProgram

end