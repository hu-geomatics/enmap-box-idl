;+
; :Hidden:
;-

function hubAMWSlider::init $
  ,name $
  ,Title=title $ 
  ,Unit=unit $ 
  ,Value=value $ 
  ,Minimum=minimum $
  ,Maximum=maximum $
  ,Steps=steps $
  ,Integer=integer $
  ,Float=float $ 
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced

  conflictingKeywords = total([keyword_set(integer), keyword_set(float)]) ne 1
  if conflictingKeywords then begin
    message,'Set one of the keywords: Integer, Float.'
  endif
  
  !NULL=self->hubAMWObject::init(name, Optional=optional, Advanced=advanced)

  self.setKeywordDefault, 'size', [200,100]
  self.setKeywordDefault, 'title', ''
  self.setKeywordDefault, 'unit', ''
  self.setKeywordDefault, 'value', '' 
  self.setKeywordDefault, 'integer', 0
  self.setKeywordDefault, 'float', 0
  return,1b
end

pro hubAMWSlider::setValue, value
  value >= self.getKeyword('minimum')
  value <= self.getKeyword('maximum')
  value = double(value)
  range = double(self.getKeyword('maximum')-self.getKeyword('minimum'))
  stepSize = range/(self.getKeyword('steps')-1)
  step = (value-self.getKeyword('minimum'))/stepSize
  if self.getKeyword('integer') then value = long64(value)
  if self.getKeyword('float') then value = double(value)
  value = ' '+strtrim(value, 2)+self.getKeyword('unit')
  widget_control, self.getWidgetID('slider'), SET_VALUE=step
  help,value
  widget_control, self.getWidgetID('text'), SET_VALUE=value
end

function hubAMWSlider::getValue $
  ,Message=message

  on_error,2
    
  widget_control,self.getWidgetID('text'),GET_VALUE=valueRaw
  
  value = strtrim(valueRaw,2)

  if value eq '' then begin
    message = 'empty parameter.'
    return,!null   
  endif

  on_ioerror,errorLabel
  catch, errorStatus
  if (errorStatus ne 0) then begin
    errorLabel:
    catch, /CANCEL
    if self.getKeyword('integer') then begin
      message = 'unable to convert string to integer number'
    endif
    if self.getKeyword('float') then begin
      message = 'unable to convert string to floating-point number'
    endif
    return,!null
  endif

  if self.getKeyword('integer') then value = long64(valueRaw)
  if self.getKeyword('float') then value = double(valueRaw)
  catch, /CANCEL
  
  if self.getKeyword('integer') then begin
    if long64(valueRaw) ne double(valueRaw) then begin
      message = 'not an integer number'
      return,!null 
    endif
  endif
  
  value = value[0] ; make a scalar from 1-elemental array
  
  return,value

end

pro hubAMWSlider::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base,/ROW,XPAD=0,YPAD=0)

  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')

  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif

  self.setWidgetID,'slider',widget_slider(base, UVALUE=self, /DRAG, SCR_XSIZE=(self.getKeyword('size'))[0] $
    , MINIMUM=0, MAXIMUM=self.getKeyword('steps')-1, /SUPPRESS_VALUE)
  self.setWidgetID,'text',widget_label(base,VALUE=' ',XSIZE=(self.getKeyword('size'))[1])

  self.setValue, self.getKeyword('value')

end

pro hubAMWSlider::handleEvent $
  ,event
  
  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return

  if isa(event, 'WIDGET_SLIDER') then begin
    step = event.value
    range = double(self.getKeyword('maximum')-self.getKeyword('minimum'))
    stepSize = range/(self.getKeyword('steps')-1)
    value = self.getKeyword('minimum') + step * stepSize
    self.setValue, value
  endif
end

pro hubAMWSlider__define

  define = {hubAMWSlider $
    ,inherits hubAMWObject $
  }
  
end