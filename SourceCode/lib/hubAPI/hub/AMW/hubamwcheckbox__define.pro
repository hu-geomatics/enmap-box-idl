;+
; :Hidden:
;-

function hubAMWCheckbox::init $
  ,name $
  ,Title=title $
  ,Value=value $
  ,Advanced=advanced

  checklistValue = keyword_set(value) ? 0 : !null
  checklistList = isa(title) ? title : '*checkbox title*'
  !NULL=self->hubAMWChecklist::init(name, $
    Value=checklistValue, List=checklistList, Title='', /AllowEmptySelection, /MultipleSelection, Advanced=advanced)

  return,1b
end

function hubAMWCheckbox::getValue

  valueCheckList = self->hubAMWChecklist::getValue()
  value = isa(valueCheckList)
  return, value

end

pro hubAMWCheckbox__define

  define = {hubAMWCheckbox $
    ,inherits hubAMWChecklist $
  }
  
end