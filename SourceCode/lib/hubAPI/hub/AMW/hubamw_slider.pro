;+
; :Description:
;    This widget allows the selection of integer or floating-point parameters via a slider.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;    .. image:: _IDLDOC/slider1.gif
;    
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash key returns the slider value.
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify the title of the widget. 
;      
;    Unit : in, optional, type=string
;      Use this keyword to specify an unit title, e.g. '%', 'Seconds', 'm^2'
;       
;    Value : in, optional, type=number
;      Use this keyword to specify a default value.
;     
;    Minimum : in, required, type=number
;      Use this keyword to specify the slider minimum value.
;           
;    Maximum : in, required, type=number
;      Use this keyword to specify the slider maximum value. 
;
;    Steps : in, required, type=number
;      Use this keyword to specify the number of steps. 
;
;    Integer : in, optional, type=boolean
;       Set this keyword to select an integer value.   
;       
;    Float : in, optional, type=boolean
;       Set this keyword to select a floating-point value. 
;       
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;
;    Size : in, optional, type=int[2]
;       Use this keyword to specify the widget width in pixels. The first element specifies the slider width
;       and the second the width of the label that show the current value.      
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create a slider widget for selecting a percentage between 0 and 100%::
;    
;      hubAMW_program 
;      hubAMW_program 
;      hubAMW_slider, 'slider',  Title='Select a percentage', Minimum=0, Maximum=100, Steps=101, /Integer, Value=75, Unit='%'
;      result = hubAMW_manage()
;      print, result
;      
; .. image:: _IDLDOC/slider1.gif
; 
;-
pro hubAMW_slider $
  ,name $
  ,Title=title $ 
  ,Unit=unit $ 
  ,Value=value $ 
  ,Minimum=minimum $
  ,Maximum=maximum $
  ,Steps=steps $
  ,Integer=integer $
  ,Float=float $ 
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWSlider($
   name $
  ,Title=title $ 
  ,Unit=unit $ 
  ,Value=value $ 
  ,Minimum=minimum $
  ,Maximum=maximum $
  ,Steps=steps $
  ,Integer=integer $
  ,Float=float $ 
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end

;+
; :Hidden:
;-
pro test_hubAMW_slider
    
  hubAMW_program 
  hubAMW_slider, 'slider1',  Minimum=0, Maximum=100, Steps=101, /Integer, Value=75, Unit='%', Title='Integer Slider'
  hubAMW_slider, 'slider2',  Minimum=0, Maximum=100, Steps=101, /Float, Value=75, Unit='%', Title='Float Slider'
  result = hubAMW_manage()
  print, result
       
;  hubAMW_program 
;  hubAMW_slider, 'sliderFloat', Minimum=-1., Maximum=1., STEPS=101, /Float, VALUE=0.
;  result = hubAMW_manage()

end 