;+
; :Hidden:
;-

function hubAMWList::init $
  ,name $
  ,Title=title $
  ,Value=value $ ; initially selected list index
  ,List=list $ ; names of the list elements
  ,AllowEmptySelection=allowEmptySelection $
  ,MultipleSelection=multipleSelection $
  ,Extract=extract $
  ,ShowButtonBar=showButtonBar $
  ,Tooltip=tooltip $
  ,XSize=xsize $
  ,YSize=ysize $
  ,Optional=optional $
  ,Advanced=advanced
  

  !NULL=self->hubAMWObject::init(name,Optional=optional,Advanced=advanced)

  if ~isa(list) then begin
    message,'Missing keyword: List.'
  endif

  self.setKeywordDefault,'allowEmptySelection', 0
  self.setKeywordDefault,'multipleSelection', 0
  self.setKeywordDefault,'showButtonBar', 1
  self.setKeywordDefault,'xsize', 100
  self.setKeywordDefault,'ysize', 50
  
  self.setInformation,'numberOfListElements',n_elements(list)

  return,1b
end

function hubAMWList::getValue

  on_error,2
  
  value = widget_info(self.getWidgetID('list'),/LIST_SELECT)

  if value[0] eq -1 then return,!null ; return !null if selection is empty
  if ~self.getKeyword('multipleSelection') then begin
    value = value[0] ; return a scaler if a single element is selected
  endif
  
  if isa(self.getKeyword('extract')) then begin
    if isa(self.getKeyword('extract'), /SCALAR) then begin
      value = (self.getKeyword('list'))[value]
    endif
    if isa(self.getKeyword('extract'), /ARRAY) then begin
      value = (self.getKeyword('extract'))[value]
    endif
  endif
  
  return,value

end

pro hubAMWList::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase
  
  base = widget_base(self.base,/COLUMN,XPAD=0,YPAD=0)
  if isa(self.getKeyword('title')) then begin
    labelID = widget_label(base,VALUE=self.getKeyword('title'),/ALIGN_LEFT   $
      , UVALUE=self, /TRACKING_EVENTS)
    (self.tooltips)[labelID] = self.getKeyword('tooltip')
  endif

  self.setWidgetID, /Optional,'list',widget_list(base,VALUE=self.getKeyword('list') $
    ,MULTIPLE=self.getKeyword('multipleSelection') $ 
    ,SCR_XSIZE=self.getKeyword('xsize'),SCR_YSIZE=self.getKeyword('ysize') $
    ,UVALUE=self)
  widget_control,self.getWidgetID('list'),SET_LIST_SELECT=self.getKeyword('value'),SET_LIST_TOP=0
    
  if self.getKeyword('multipleSelection') and self.getKeyword('showButtonBar') then begin
    buttonBase = widget_base(self.base,/Row)
    self.setWidgetID, /Optional,'buttonSelectAll',widget_button(buttonBase,Value='All',UVALUE=self)
    self.setWidgetID, /Optional,'buttonSelectNone',widget_button(buttonBase,Value='None',UVALUE=self)
    self.setWidgetID, /Optional,'buttonEnterSelection',widget_button(buttonBase,Value='Enter',UVALUE=self)
    
  endif

end

function hubAMWList::checkConsistenty $
  ,Message=message
  
  
  value = self.getValue()
  if ~self.getKeyword('allowEmptySelection') then begin
    message = 'no item selected'
    if ~isa(value) then return,0b
  endif
      
  message = [] 
  return,1b
  
end

pro hubAMWList::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return
  
  if event.id eq self.getWidgetID('list') then return

  if event.id eq self.getWidgetID('buttonSelectAll') then begin
    widget_control,self.getWidgetID('list'),SET_LIST_SELECT=indgen(self.getInformation('numberOfListElements'))
  endif
  
  if event.id eq self.getWidgetID('buttonSelectNone') then begin
    widget_control,self.getWidgetID('list'),SET_LIST_SELECT=-1
  endif
  
  if event.id eq self.getWidgetID('buttonEnterSelection') then begin
    value = widget_info(self.getWidgetID('list'),/LIST_SELECT)
    currentSelection = value[0] eq -1 ? !null : strjoin(strcompress(/REMOVE_ALL, value+1), ' ')
    hubAMW_program, Title='Enter List Selection'
    hubAMW_parameter, 'selection', Title='Selection', /String, Value=currentSelection, Tooltip='Enter selection, e.g. 1 3 5 to select item 1, 3 and 5.', Size=50
    result = hubAMW_manage()
    if result['accept'] then begin
      selection = fix(strsplit(/EXTRACT, result['selection']))-1
      widget_control, self.getWidgetID('list'), SET_LIST_SELECT=selection
    endif
  endif
  

end

pro hubAMWList__define

  define = {hubAMWList $
    ,inherits hubAMWObject $
  }
  
end
