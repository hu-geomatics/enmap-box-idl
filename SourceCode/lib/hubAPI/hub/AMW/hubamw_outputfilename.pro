;+
; :Description:
;    This widget allows the selection of an output filename.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;     
;    .. image:: _IDLDOC/outputFilename1.gif
;     
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash value is given by the selected filename, or, in case of an unchecked optional 
;      filename, by !NULL.
;
; :Keywords:
;    Title : in, optional, type=string, default='Result File'
;      Use this keyword to specify the widget title.
;      
;    Value : in, optional, type=string
;      Use this keyword to specify a default filename.
;      Specifying only the file basename (e.g. result.txt),
;      to prepand the users temp-directory
;      (e.g. C:\Users\USERNAME\AppData\Local\Temp\result.txt)     
;       
;    Extension : in, optional, type=string
;      Use this keyword to specify a default file extension, e.g. 'txt', 'bmp'.
;      
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;
;    Size : in, optional, type=int, default=500
;      Use this keyword to specify the widget width in pixel.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create an output filename::
;    
;      hubAMW_program 
;      hubAMW_outputFilename, 'outputFilename'
;      result = hubAMW_manage()
;      print, result
;
;    .. image:: _IDLDOC/outputFilename1.gif
;
;    Create an optional output filename, default file basename should be 'result.txt'. 
;    Note that the default directory is the users temp-directory::
;      
;      hubAMW_program 
;      hubAMW_outputFilename, 'outputFilename', Value='result.txt', Extension='txt', Optional=2
;      result = hubAMW_manage()
;      print, result
;        
;    .. image:: _IDLDOC/outputFilename2.gif
;            
;-
pro hubAMW_outputFilename $
  , name $
  , Title=title $
  , Value=value $
  , Extension=extension $
  , AllowEmptyFilename=allowEmptyFilename $
  , Tooltip=tooltip $
  , Size=size $
  , TSize=tSize $
  , Optional=optional $
  , Advanced=advanced

  on_error,2
  
  hubAMWObject = hubAMWOutputFilename( $
    name $
    ,Title=title $
    ,Value=value $
    ,Extension=extension $
    ,AllowEmptyFilename=allowEmptyFilename $
    ,Tooltip=tooltip $  
    ,Size=size $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end