;+
; :Hidden:
;-

pro hubAMWProgram::handleEvent $
  ,event

  if isa(event,'WIDGET_BUTTON') then begin
    case event.id of
      (self.buttonIDs)['accept'] : begin
        self._handleEventAccept,event
        return
      end
      (self.buttonIDs)['cancel'] : begin
        self._handleEventCancel,event
        return
      end
      (self.buttonIDs)['advanced'] : begin
        self._handleEventAdvanced,event
        return
      end
      (self.buttonIDs)['help'] : begin
        self._handleEventHelp,event
        return
      end
      
      else : ;
    endcase
  endif

  self._handleEventExclusiveSubFrameSelection,event

end

function hubAMWProgram::_getEmptyResultHash

  resultHash = orderedHash()
  resultHash['accept'] = 0
  return, resultHash

end

function hubAMWProgram::_getInternalConsistencyMessages, ResultHashKeys=resultHashKeys

 ; check internal widget consistency

  messages = list()
  foreach hubAMWObject,self.hubAMWObjects,i do begin
    
    ; only check sensitive widgets
    
    if ~hubAMWObject.getSensitivity() then continue
    if ~hubAMWObject.getOptionalSelection() then continue

    ; if ResultHashKeys keyword is given, check only those hashKeys
    if isa(resultHashKeys) then begin
      insideList = isa(where(/NULL, resultHashKeys eq hubAMWObject.getName()))
      if ~insideList then continue
    endif
    
    isConsistent = hubAMWObject.checkConsistenty(Message=message)
    if ~isConsistent then begin
      message += (hubAMWObject.getKeyword('title') eq '' || ~isa(hubAMWObject.getKeyword('title'))) ? '' : ' ('+strtrim(hubAMWObject.getKeyword('title'),2)+')'
      messages.add,message
    endif
  endforeach
  
  messages = messages.toArray()
  return, messages

end

function hubAMWProgram::_getUserConsistencyMessages, consistencyCheckFunction, userInformation

  resultHash = self._getResultHash()
  if (consistencyCheckFunction ne '') and isa(consistencyCheckFunction) then begin
    if isa(userInformation) then begin
      isConsistent = call_function(consistencyCheckFunction, resultHash, Message=message, UserInformation=userInformation) 
    endif else begin
      isConsistent = call_function(consistencyCheckFunction, resultHash, Message=message)
    endelse
  endif else begin
    isConsistent = 1b
  endelse
  
  messages = isConsistent ? [] : [message]
  return, messages

end

pro hubAMWProgram::_showErrorMessage, messages
  ok = dialog_message(['Requested parameters are incomplete:','',messages], TITLE='Consistency Check', /CENTER, /ERROR)
end

function hubAMWProgram::_getResultHash, Accept=accept

  resultHash = self._getEmptyResultHash()

  foreach hubAMWObject,self.hubAMWObjects do begin
    if ~hubAMWObject.getOptionalSelection() then continue
    if ~hubAMWObject.getSensitivity() then continue
                     
    hubAMWObjectName = hubAMWObject.getName()
    if hubAMWObjectName eq '' then continue
    resultHash[hubAMWObjectName] = hubAMWObject.getValue()
  endforeach
  
  ; add exclusive subframe indexes
  
  if n_elements(self.exclusiveSubFrames) ne 0 then begin
    exclusiveSubFrameNames = list()
    exclusiveSubFrameButtonSelected = list()
    foreach frame,self.exclusiveSubFrames do begin
      exclusiveSubFrameNames.add,frame.name
      exclusiveSubFrameButtonSelected.add,widget_info(/BUTTON_SET,frame.button)
    endforeach
    
    a = exclusiveSubFrameNames.toArray()
    exclusiveGroupNames = list(a[uniq(a,sort(a))],/EXTRACT)
    
    foreach exclusiveGroupName,exclusiveGroupNames do begin
      index = where(/NULL,exclusiveSubFrameNames.toArray() eq exclusiveGroupName)
      flags = exclusiveSubFrameButtonSelected[[index]]
      if typename(flags) eq 'LIST' then flags = flags.toArray() 
      selection = where(/NULL,flags)
  
      if ~isa(selection) then begin
        self._showErrorMessage, ['Select one of the exclusive subframes.']  
        return, self._getEmptyResultHash()
      endif
      
      resultHash[exclusiveGroupName] = selection[0]
      
    endforeach
  endif

  resultHash['accept'] = keyword_set(accept)
  return, resultHash

end

pro hubAMWProgram::_handleEventAccept $
  ,event, ResultHash=resultHash

  @huberrorcatch

  ; internal consistency check
  messagesInternal = self._getInternalConsistencyMessages()
  if isa(messagesInternal) then begin
    self._showErrorMessage, messagesInternal
    return
  endif

  ; user consistency check
  messagesUser = self._getUserConsistencyMessages(self.consistencyCheckFunction, self.userInformation)
  if isa(messagesUser) then begin
    self._showErrorMessage, messagesUser
    return
  endif

  ; get result hash and save it

  self.resultHash = self._getResultHash(/Accept)
  
  ; destroy widget program
  
  widget_control,/DESTROY,self.tlb
    
end

pro hubAMWProgram::_handleEventCancel $
  ,event
  
  self.resultHash = self._getEmptyResultHash()
  widget_control,/DESTROY,self.tlb

end

pro hubAMWProgram::_handleEventAdvanced $
  ,event

  widget_control, self.tlb, UPDATE=0
  self._showAdvancedFrames
  self._showAdvancedWidgets
  widget_control,(self.buttonIDs)['advanced'],/DESTROY
  self._scaleScrollingBase
  (hubGUIHelper()).centerTLB, self.tlb
  widget_control, self.tlb, UPDATE=1
    
end

pro hubAMWProgram::_handleEventHelp $
  ,event
  
  hubHelper.openFile, self.helpFile

end
   
pro hubAMWProgram::_handleEventExclusiveSubFrameSelection $
  ,event
  
  ; find selected exclusiveSubFrames
  
  foreach frame,self.exclusiveSubFrames do begin
    if frame.button ne event.id then continue
    selectedFrame =  frame
  endforeach
  if ~isa(selectedFrame) then return
  
  ; set sensitivity

  foreach frame,self.exclusiveSubFrames do begin
    if frame.name ne selectedFrame.name then continue
    widget_control,frame.button,SET_BUTTON=(frame.button eq selectedFrame.button),/CLEAR_EVENTS
    widget_control,frame.base,SENSITIVE=(frame.button eq selectedFrame.button),/CLEAR_EVENTS
  endforeach

end

pro hubAMWProgram::_createTopLevelBase

  self.tlb = widget_base(TITLE=self.title, /COLUMN, MODAL=~self.noBlock, GROUP_LEADER=*self.groupLeader,XPAD=1,YPAD=1,SPACE=1,UVALUE=self)    
 
  self.baseForScrolling = widget_base(self.tlb,COLUMN=0,XPAD=0,YPAD=0,SPACE=0,/SCROLL)
;  self.baseForScrolling = widget_base(self.tlb,COLUMN=0,XPAD=0,YPAD=0,SPACE=0)
  
  self.baseForContent = widget_base(self.baseForScrolling,/COLUMN,XPAD=0,YPAD=0,SPACE=1)
  
  dummy = widget_base(self.tlb,/ROW,XPAD=0,YPAD=0,SPACE=1) ; only for correct appearance of the frame around the buttons
  self.baseForButtons = widget_base(dummy,/ROW,XPAD=0,YPAD=0,SPACE=0,/Frame)
  self.buttonIDs = hash()
  (self.buttonIDs)['accept'] = widget_button(self.baseForButtons,VALUE='Accept',UVALUE=self)
  (self.buttonIDs)['cancel'] = widget_button(self.baseForButtons,VALUE='Cancel',UVALUE=self)
  (self.buttonIDs)['advanced'] = 0
  (self.buttonIDs)['help'] = 0
  self.baseForUserButtons = widget_base(dummy,/ROW,XPAD=0,YPAD=0,SPACE=0,/Frame,MAP=0)
  
  if self.hideButtons then begin
    widget_control, self.baseForButtons, MAP=0, SCR_YSIZE=1
  endif

end

pro hubAMWProgram::insertFrame $
  ,Title=title $
  ,Advanced=advanced $
  ,XSize=xSize, YSize=ySize $
  ,_NoPad=_noPad

  on_error,2

  scroll = isa(xSize) or isa(ySize)

  frameBase = widget_base(self.baseForContent,/COLUMN,XPAD=0,YPAD=0,SPACE=1)
  if !VERSION.OS_FAMILY eq 'unix' then begin
    dummy = widget_base(frameBase, YSIZE=1)  
  endif
  if isa(title) then !NULL = widget_label(frameBase,VALUE=title,/ALIGN_LEFT)
  contentBase = widget_base(frameBase,/COLUMN,FRAME=(keyword_set(_noPad) ? 0 : 1),XPAD=0,YPAD=(keyword_set(_noPad) ? 0 : 1),SPACE=1 $
    ,SCR_XSIZE=xSize, SCR_YSIZE=ySize, SCROLL=scroll)
  (self.contentBases).add,contentBase
  self.insertSubFrame,/Column,_NoPad=_noPad
  if keyword_set(advanced) then begin
    (self.advancedFrames).add,frameBase
    self._addAdvancedButton
  endif
end

pro hubAMWProgram::_addAdvancedButton
  ; insert "advanced" button when the first advanced frame is inserted
  if (self.buttonIDs)['advanced'] eq 0 then begin 
    (self.buttonIDs)['advanced'] = widget_button(self.baseForButtons,VALUE='Advanced',UVALUE=self)
  endif
  
end

pro hubAMWProgram::insertTabFrame $
  ,Title=title $
  ,Advanced=advanced $
  ,XSize=xSize, YSize=ySize

  on_error,2

  self.insertFrame, Title=title, Advanced=advanced, XSize=xSize, YSize=ySize,/_NoPad
  self.currentTabWidget = widget_tab(self.currentBaseForWidgets)

end

pro hubAMWProgram::insertTabPage $
  ,Title=title

  on_error,2

  tabPage = widget_base(self.currentTabWidget, Title=title, /Column,XPAD=0,YPAD=0)
  self.contentBases.add, tabPage
  self.insertSubFrame,/Column
    
end

pro hubAMWProgram::insertSubFrame $
  ,Title=title $
  ,Column=column $
  ,Row=row $
  ,ExclusiveGroupName=exclusiveGroupName $
  ,SetButton=setButton $
  ,TargetBase=targetBase $
  ,_NoPad=_noPad
  
  on_error,2
  
  noContentBaseAvailable = n_elements(self.contentBases) eq 0
  if noContentBaseAvailable then begin
    self.insertFrame
    ;message,'Before you can insert subframes you need to insert a frame.' 
  endif
  
  singleKeywordSet = keyword_set(column) xor keyword_set(row)
  if ~singleKeywordSet then begin
    message,'Set either the COLUMN or the ROW keyword.'
  endif
  
  if isa(exclusiveGroupName) then begin
    if (typename(exclusiveGroupName) ne 'STRING') or ~isa(exclusiveGroupName,/Scalar) then begin
      message,'Keyword exclusiveGroupName must be a scalar string.'
    endif
  endif 
  
  if ~isa(targetBase) then targetBase = (self.contentBases)[-1]
  mainBase = widget_base(targetBase,/COLUMN,XPAD=(keyword_set(_noPad) ? 0 : 1),YPAD=0,SPACE=0)
;  titleBase = widget_base(mainBase,/ROW,XPAD=0,YPAD=0,SPACE=1)

  if isa(exclusiveGroupName) then begin
    buttonBase = widget_base(mainBase,/ROW,XPAD=0,YPAD=0,SPACE=0,/EXCLUSIVE)
    buttonText = isa(title) ? title : ''
    exclusiveButton = widget_button(buttonBase,VALUE=buttonText,UVALUE=self,/NO_RELEASE)
    widget_control,exclusiveButton,SET_BUTTON=keyword_set(setButton)
    tmpBase = widget_base(mainBase,/ROW,XPAD=0,YPAD=0,SPACE=0)
    tmpSubbase = widget_base(tmpBase,/ROW, XSIZE=20)
    mainBase = widget_base(tmpBase,/COLUMN,XPAD=0,YPAD=0,SPACE=0)
  endif else begin
    if isa(title) then !NULL = widget_label(mainBase,VALUE=title,/ALIGN_LEFT)
  endelse

  baseForWidgets = widget_base(mainBase,COLUMN=keyword_set(column),ROW=keyword_set(row),XPAD=0,YPAD=0,SPACE=1)
  if isa(exclusiveGroupName) then begin
    widget_control,baseForWidgets,SENSITIVE=keyword_set(setButton)
    self._addExclusiveSubFrame,exclusiveGroupName,exclusiveButton,baseForWidgets
  endif
  self.currentBaseForWidgets =  baseForWidgets
  
end

pro hubAMWProgram::_addExclusiveSubFrame $
  ,exclusiveGroupName $
  ,exclusiveButton $
  ,baseForWidgets
  
  self.exclusiveSubFrames.add,{name:exclusiveGroupName,base:baseForWidgets,button:exclusiveButton}
    
end

pro hubAMWProgram::insertWidget $
  ,hubAMWObject

  if ~isa(hubAMWObject,'hubAMWObject') then begin
    message,'Argument must be an auto-managed widget object.'
  endif
  
  if self.currentBaseForWidgets eq 0 then begin
    self.insertFrame
  endif
  
  hubAMWObject.setAMWProgram, self  
  hubAMWObject.defineWidgetHierarchy, self.currentBaseForWidgets
  hubAMWObject.setSensitivity
  (self.hubAMWObjects).add,hubAMWObject
  
  

end

pro hubAMWProgram::_scaleContentBases

  geo1 = widget_info(/Geometry,self.baseForContent)
  geo2 = widget_info(/Geometry,self.baseForButtons)
  scr_xsize = max([geo1.scr_xsize,geo2.scr_xsize])
  
  foreach contentBase,self.contentBases do begin
    isTabPage = widget_info(widget_info(contentBase, /PARENT),/TYPE) eq 10
    widget_control,contentBase,SCR_XSIZE=scr_xsize-(isTabPage?6:0)
  endforeach
  
end

pro hubAMWProgram::_scaleScrollingBase

  contentGeometry = widget_info(/GEOMETRY, self.baseForContent)
  contentSize = [contentGeometry.xsize, contentGeometry.ysize]
  maxSize = get_screen_size()*[0.8,0.8]
  scrollSize = contentSize < maxSize
  ; workaround for a bug that places useless scroll bars into the widget program
  if !VERSION.OS_FAMILY eq 'unix' then begin
    scrollSize += 4
  endif
  widget_control, self.baseForScrolling, SCR_XSIZE=scrollSize[0], SCR_YSIZE=scrollSize[1]
  
end

function hubAMWProgram::manage $
  , Structure=structure $
  , Dictionary=dictionary $
  , Flat=flat $
  , ConsistencyCheckFunction=consistencyCheckFunction $
  , UserInformation=userInformation $
  , HelpFile=helpFile $
  , ButtonNameAccept=buttonNameAccept $
  , ButtonNameCancel=buttonNameCancel $
  , ButtonNameAdvanced=buttonNameAdvanced

  on_error,2

  if isa(consistencyCheckFunction) then begin
    self.consistencyCheckFunction = consistencyCheckFunction
  endif 
  
  if isa(userInformation) then begin
    self.userInformation = userInformation
  endif
  
  if isa(helpFile) then begin
    self.helpFile = helpFile
    (self.buttonIDs)['help'] = widget_button(self.baseForButtons, VALUE='?', UVALUE=self)
  endif
  
  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch,/CANCEL
    help,/Last_message
    message,'An error occured inside the auto-managed widget program.'
  endif
  
  self._scaleContentBases
  self._hideAdvancedFrames
  self._hideAdvancedWidgets

  if isa(buttonNameAccept) then begin
    widget_control, (self.buttonIDs)['accept'], SET_VALUE=buttonNameAccept, DESTROY=(buttonNameAccept eq '')
  endif
  if isa(buttonNameCancel) then begin
    widget_control, (self.buttonIDs)['cancel'], SET_VALUE=buttonNameCancel, DESTROY=(buttonNameCancel eq '')
  endif
  if isa(buttonNameAdvanced) && widget_info((self.buttonIDs)['advanced'],/VALID_ID) then begin
    widget_control, (self.buttonIDs)['advanced'], SET_VALUE=buttonNameAdvanced
  endif
  if (~widget_info(/VALID_ID, (self.buttonIDs)['accept']) and $
     ~widget_info(/VALID_ID, (self.buttonIDs)['cancel']) and $
     ~widget_info(/VALID_ID, (self.buttonIDs)['advanced'])) then begin
    widget_control, self.baseForButtons, /DESTROY
  endif

  self._scaleScrollingBase
  (hubGUIHelper()).centerTLB, self.tlb
  widget_control,self.tlb,/REALIZE
  self._drawInitialDrawWidgets
  self._pressInitialButtonWidgets
  xmanager,self.name,self.tlb,EVENT_HANDLER='hubObjCallback_event',/NO_BLOCK
  
  ; destroy temporary group leader
  
  if self.temporaryGroupLeader then begin
    widget_control, *self.groupLeader, /DESTROY
  endif

  result = self.resultHash + orderedHash()
  
  if keyword_set(flat) then begin
    foreach value, result, key do begin
      if isa(value, 'hash') then begin
        foreach subvalue, value, subkey do begin
          result[key+'_'+subkey] = subvalue
        endforeach
        result.remove, key
      endif
    endforeach 
  endif
    
  if keyword_set(structure) then begin
    result = self._getResultStructure(result)
  endif

  if keyword_set(dictionary) then begin
    result = self._getResultDictionary(result)
  endif
  return, result

end

function hubAMWProgram::_getResultStructure, resultHash

  hashValues = resultHash.values()
  hashKeys = (resultHash.keys()).toArray()
  sortedIndex = reverse(sort(hashKeys))
  hashKeys = hashKeys[sortedIndex]
  hashValues = hashValues[sortedIndex]
  resultStructure = create_struct('accept',resultHash['accept'])    
  foreach hashKey,hashKeys,i do begin
    if hashKey eq 'accept' then continue 
    resultStructure = create_struct(hashKey,hashValues[i],resultStructure)
  endforeach
  return,resultStructure

end

function hubAMWProgram::_getResultDictionary, resultHash
  resultDictionary = dictionary()+resultHash
  return, resultDictionary
end

pro hubAMWProgram::_showAdvancedFrames
  foreach base,self.advancedFrames do begin
 
    ; restore frame size
    widget_control,base,GET_UVALUE=scr_ysize
    
    ; show frame
    widget_control,base,SCR_YSIZE=scr_ysize
  endforeach
  (hubGUIHelper()).centerTLB,self.tlb
end

pro hubAMWProgram::_hideAdvancedFrames
  foreach base,self.advancedFrames do begin

    ; save frame size
    geometry = widget_info(base,/GEOMETRY)
    widget_control,base,SET_UVALUE=geometry.scr_ysize
    
    ; hide frame
    widget_control,base,SCR_YSIZE=1
  endforeach

end

pro hubAMWProgram::_showAdvancedWidgets

  foreach widget, self.hubAMWObjects do begin
    widget._showAdvancedWidget
  endforeach

end

pro hubAMWProgram::_hideAdvancedWidgets

  foreach widget, self.hubAMWObjects do begin
    if widget._isAdvanced() then begin
      widget._hideAdvancedWidget
      self._addAdvancedButton
    endif
  endforeach

end

pro hubAMWProgram::_pressInitialButtonWidgets

  event = {id:-999}
  foreach widget, self.hubAMWObjects do begin
    if isa(widget, 'hubAMWButton') then begin
      if keyword_set(widget.getKeyword('press')) then begin
        widget.handleEvent, event
      endif
    endif
  endforeach

end

pro hubAMWProgram::_drawInitialDrawWidgets

  foreach widget, self.hubAMWObjects do begin
    if isa(widget, 'hubAMWDraw') then begin
      widget._drawInitialImage
    endif
    if isa(widget, 'hubAMWColor') then begin
      widget.draw
    endif
    
  endforeach

end

function hubAMWProgram::getTopLevelBase
  
  return, self.tlb
  
end

function hubAMWProgram::getCurrentBase
  return, self.currentBaseForWidgets
end

pro hubAMWProgram::setCurrentBase, base
  self.currentBaseForWidgets = base
end

function hubAMWProgram::getUserButtonBase
  return, self.baseForUserButtons
end

function hubAMWProgram::init $
  , groupLeader $
  , Title=title $
  , HideButtons=hideButtons $
  , NoBlock=noBlock $
  , _TemporaryGroupLeader=_temporaryGroupLeader

;  xmanager,CATCH=0
  (hubGUIHelper()).setFixedWidgetFont
  if isa(title) then begin
    self.title = title    
  endif else begin
    self.title = 'auto-managed widget program'
  endelse
  self.hideButtons = keyword_set(hideButtons)
  self.name = obj_class(self)
  self.noBlock = keyword_set(noBlock)
  self.groupLeader = ptr_new(groupLeader)  
  self._createTopLevelBase
  self.contentBases = list()
  self.exclusiveSubFrames = list()
  self.advancedFrames = list()
  self.hubAMWObjects = list()
  self.temporaryGroupLeader = keyword_set(_temporaryGroupLeader)
  self.resultHash = self._getEmptyResultHash()
 
  return, 1B
end

pro hubAMWProgram__define

  define = {hubAMWProgram $
    , inherits IDL_Object $
    , groupLeader : ptr_new() $
    , tlb : 0L $
    , baseForScrolling : 0L $
    , baseForContent : 0L $
    , baseForButtons : 0L $
    , baseForUserButtons : 0L $
    , hideButtons : 0b $
    , currentBaseForWidgets : 0L $
    , currentTabWidget : 0L $
    , contentBases : list() $
    , exclusiveSubFrames : list() $
    , advancedFrames : list() $
    , buttonIDs : obj_new() $
    , hubAMWObjects : list() $
    , title : '' $
    , name : '' $
    , consistencyCheckFunction : '' $
    , userInformation:obj_new() $
    , helpFile : '' $
    , resultHash : obj_new() $
    , temporaryGroupLeader : 0b $
    , noBlock : 0b $
  }

end

pro test_hubAMWProgram

  groupLeader = widget_base()

  hubAMWProgram = hubAMWProgram(Title='Scale Data',groupLeader, /HideButtons)
  hubAMWProgram.insertFrame,Title='Input Data Range',ADVANCED=1
  
  hubAMWProgram.insertSubFrame,Title='by Data Value',/Row,ExclusiveGroupName='inputDataRange'
  hubAMWProgram.insertWidget,hubAMWParameter('min',Size=3,Title='Min = ',Value='0',/FLOAT)
  hubAMWProgram.insertWidget,hubAMWParameter('max',Size=3,Title='Max = ',Value='100',/FLOAT)
  hubAMWProgram.insertWidget,hubAMWCheckbox('getStatisticsFromAdditionalImage',Title='from additional Image',Value=0)
  
  hubAMWProgram.insertSubFrame,Title='by Image Statistics',/Row,ExclusiveGroupName='inputDataRange',/SetButton
  hubAMWProgram.insertWidget,hubAMWParameter('min',Size=3,Title='Min = ',Value=2,Unit='%',/FLOAT)
  hubAMWProgram.insertWidget,hubAMWParameter('max',Size=3,Title='Max = ',Value=98,Unit='%',/FLOAT)
  hubAMWProgram.insertWidget,hubAMWCheckbox('getStatisticsFromAdditionalImage',Title='from additional Image',Value=0)
  

  hubAMWProgram.insertFrame,Title='Output'
  hubAMWProgram.insertWidget,hubAMWOutputFilename('fileName',Title='Output File',Value='t:\image_scaled',Optional=0,Size=300)
  result = hubAMWProgram.manage(/STRUCTURE)
  print
  help,result
  widget_control,groupLeader,/DESTROY

end

