;+
; :Hidden:
;-

function hubAMWInputFilename::init $
  ,name $
  ,Title=title $
  ,Value=value $
  ,Extension=extension $
  ,AllowEmptyFilename=allowEmptyFilename $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  !NULL=self->hubAMWObject::init(name, Optional=optional,Advanced=advanced)

  self.setKeywordDefault, 'title', 'Input File'
  self.setKeywordDefault, 'value', ' '
  self.setKeywordDefault, 'size', 500
  
  self.hubAMWInputFilename::saveInformation
  
  return,1b
end

function hubAMWInputFilename::getFileList
  extension = self.getKeyword('extension')
  if isa(extension) then begin
    type = '*.'+extension
    fileList = hubProEnvHelper.getFilenames(Type=type)
  endif else begin
    filelist = []
  endelse
  return, fileList
end

pro hubAMWInputFilename::saveInformation

  ; get filelist from ENVI/EnMAP session
  
  fileList = self.getFileList()

  ; filter filelist for extension 

;  fileList = self.filterFileList(fileList)

  ; insert default filename

  fileList = [self.getKeyword('value'),fileList]

  ; save complete filepath and extract filebasenames for visualization inside combobox
  self.setInformation,'fileList',fileList
  if isa(fileList) then begin
    fileListBasename = file_basename(fileList)
  endif

  ; create combobox text
  
  self.setInformation,'textOpenFile','<open file>'
  self.setInformation,'textCombobox',[fileListBasename,self.getInformation('textOpenFile')]

end

function hubAMWInputFilename::filterFileList $
  ,fileList     

  if isa(self.getKeyword('extension')) and isa(fileList) then begin
  
    fileListFiltered = list()
    removeSuffix = '.'+self.getKeyword('extension')
    foreach file,fileList,i do begin
      hasExtention = file_basename(file,removeSuffix,/FOLD_CASE) ne file_basename(file,/FOLD_CASE)
      if hasExtention then fileListFiltered.add,file
    endforeach
    
    fileListFiltered = fileListFiltered.toArray()

  endif else begin
  
    fileListFiltered = fileList
  
  endelse
  
  return, fileListFiltered

end

function hubAMWInputFilename::getValue

  ; get combobox text

  text = widget_info(self.getWidgetID('combobox'),/COMBOBOX_GETTEXT)

  ; find combobox index
  
  index = (where(/NULL,self.getInformation('textCombobox') eq text))[0]
  
  ; get fileName
  
  value = (self.getInformation('fileList'))[index]
  value = strtrim(value,2)
  
  return,value

end

pro hubAMWInputFilename::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base, /ROW, XPAD=0, YPAD=0)
  
  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT   $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')

  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif

  self.setWidgetID, /Optional, 'combobox',widget_combobox(base $
    , VALUE=self.getInformation('textCombobox') $
    , SCR_XSIZE=self.getKeyword('size') $
    , UVALUE=self)
  widget_control, self.getWidgetID('combobox'), SET_COMBOBOX_SELECT=0
  
end

pro hubAMWInputFilename::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return

  case event.str of
    self->getInformation('textOpenFile') : self.handleEventOpenFile,event
    else : ;
  endcase

end

 ; filename = dialog_pickfile(DEFAULT_EXTENSION='svc',FILTER='*.svc',/READ,/MUST_EXIST)
     
pro hubAMWInputFilename::handleEventOpenFile $
  ,event
  
  ; get filename
  
  widget_control,event.top,/CLEAR_EVENTS
  if isa(self.getKeyword('extension')) then begin
    filter = self.getKeyword('extension') eq '' ? '*.*' : '*.'+self.getKeyword('extension')
  endif
  
  filename = dialog_pickfile($
     DEFAULT_EXTENSION=self.getKeyword('extension') $
    ,DIALOG_PARENT=event.top $
    ,FILTER=filter $
    ,/READ,/MUST_EXIST $
    ,TITLE=!null)
  widget_control,event.top,/CLEAR_EVENTS
  
  dialogCanceled = filename eq ''
  
  if dialogCanceled then begin

    ; set combobox to the default filename
  
    widget_control,self.getWidgetID('combobox'),SET_COMBOBOX_SELECT=0,/CLEAR_EVENTS
  
  endif else begin
  
    ; open image files
  
    if isa(self,'hubAMWInputSampleSet') then begin
      catch, errorStatus
       if (errorStatus ne 0) then begin
        catch, /CANCEL
        widget_control,self.getWidgetID('combobox'),SET_COMBOBOX_SELECT=0,/CLEAR_EVENTS
        help, /LAST_MESSAGE, OUTPUT=output
        !null = dialog_message(/Error,['Can not open file: '+filename, output[0]])
        return
      endif
      hubProEnvHelper.openImage, filename
      (self->getInformation('hubIOImgInputImages'))[filename] = hubIOImgInputImage(filename)
      widget_control, event.top, /SHOW
    endif

    ; add new file basename to combobox
    
    fileBasename = file_basename(filename)
    self.setInformation,'textCombobox',[(self.getInformation('textCombobox'))[0],fileBasename,(self.getInformation('textCombobox'))[1:*]]
    widget_control,self.getWidgetID('combobox'),SET_VALUE=self.getInformation('textCombobox'),/CLEAR_EVENTS
    widget_control,self.getWidgetID('combobox'),SET_COMBOBOX_SELECT=1,/CLEAR_EVENTS

    ; add new file

    oldFileList = self.getInformation('fileList')
    newFileList = [oldFileList[0],filename]
    if n_elements(oldFileList) ge 2 then begin
      newFileList = [newFileList,oldFileList[1:*]]
    endif
    self.setInformation,'fileList',newFileList

    ; set to path as current working directory
    
    cd, file_dirname(filename) 
    
  endelse

end

function hubAMWInputFilename::checkConsistenty $
  ,Message=message
  
  value = self.getValue()

  ; check if filename is empty
  if value eq '' and ~keyword_set(self.getKeyword('allowEmptyFilename')) then begin
    message = 'filename is empty'
    return,0b
  endif

  ; check if file exists
  if ~ (hubIOHelper()).fileReadable(value) and ~keyword_set(self.getKeyword('allowEmptyFilename')) then begin
    message = 'file does not exist'
    return,0b
  endif
  
  message = [] 
  return,1b
  
end

pro hubAMWInputFilename__define

  define = {hubAMWInputFilename $
    , inherits hubAMWObject $
  }
  
end
