;+
; :Description:
;    This widget allows the selection of string, integer or floating-point parameters.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;    .. image:: _IDLDOC/parameter1.gif
;    
;    .. image:: _IDLDOC/parameter2.gif
;    
;    .. image:: _IDLDOC/parameter3.gif
;    
;    
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash key returns the parameter value.
;
; :Keywords:
;    Title : in, optional, type = string, default='*set title*'
;      Use this keyword to specify the title of the parameter-widget. 
;      
;    Unit : in, optional, type = string
;      Use this keyword to specify an unit title, e.g. '%', 'Seconds', 'm^2'
;       
;    Value : in, optional, type = {string | number }
;      Use this keyword to specify a default value.
;      
;    DefaultResult : in, optional, type = {string | number }
;      Use this keyword to specify a value that is returned when the parameter field is not filled.
;       
;    IsGE : in, optional, type = number
;       Use this keyword to specify an inclusive lower boundary
;       (selection must be greater than or equal to the boundary).
;       
;    IsGT : in, optional, type = number
;       Use this keyword to specify an exclusive lower boundary
;       (selection must be greater than the boundary).
;       
;    IsLE : in, optional, type = number
;       Use this keyword to specify an inclusive upper boundary
;       (selection must be lower than or equal to the boundary).
;             
;    IsLT : in, optional, type = number
;       Use this keyword to specify an exclusive upper boundary
;       (selection must be lower than the boundary).
;       
;    String : in, optional, type=boolean
;       Set this keyword to select a string value.  
;       
;    Integer : in, optional, type=boolean
;       Set this keyword to select an integer value.   
;       
;    Float : in, optional, type=boolean
;       Set this keyword to select a floating-point value. 
;       
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;
;    Size : in, optional, type=int
;       Use this keyword to specify the widget width in characters.     
;       
;    ScrSize : in, optional, type=int
;       Use this keyword to specify the widget width in pixel.
;
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create an parameter widget for selecting a percentage between 0 and 100%::
;    
;      hubAMW_program 
;      hubAMW_parameter, 'parameterValue', /Integer, Title='Percentage', IsGE=0, IsLE=100, Unit='%', Size=3
;      result = hubAMW_manage()
;      print, result
;       
; .. image:: _IDLDOC/parameter4.gif
; 
;-
pro hubAMW_parameter $
  ,name $
  ,Title=title $ 
  ,Unit=unit $ 
  ,Value=value $
  ,DefaultResult=defaultResult $
  ,IsGE=isGE $ 
  ,IsGT=isGT $ 
  ,IsLE=isLE $ 
  ,IsLT=isLT $
  ,String=string $
  ,Integer=integer $
  ,Float=float $ 
  ,AllowEmptyValue=allowEmptyValue $
  ,Tooltip=tooltip $
  ,Size=size $
  ,ScrSize=scrSize $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWParameter($
     name $
    ,Title=title $ 
    ,Unit=unit $ 
    ,Value=value $ 
    ,DefaultResult=defaultResult $
    ,IsGE=isGE $ 
    ,IsGT=isGT $
    ,IsLE=isLE $
    ,IsLT=isLT $
    ,String=string $ 
    ,Integer=integer $
    ,Float=float $
    ,AllowEmptyValue=allowEmptyValue $
    ,Tooltip=tooltip $
    ,Size=size $
    ,ScrSize=scrSize $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end
