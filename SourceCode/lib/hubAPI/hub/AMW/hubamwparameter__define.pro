;+
; :Hidden:
;-

function hubAMWParameter::init $
  ,name $
  ,Title=title $
  ,Unit=unit $
  ,Value=value $ 
  ,DefaultResult=defaultResult $
  ,IsGE=isGE $ 
  ,IsGT=isGT $ 
  ,IsLE=isLE $ 
  ,IsLT=isLT $ 
  ,String=string $
  ,Integer=integer $
  ,Float=float $ 
  ,AllowEmptyValue=allowEmptyValue $
  ,Tooltip=tooltip $
  ,Size=size $
  ,ScrSize=scrSize $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced

  conflictingKeywords = total([keyword_set(string), keyword_set(integer), keyword_set(float)]) ne 1
  if conflictingKeywords then begin
    message,'Set one of the keywords: String, Integer, Float.'
  endif
  
  !NULL=self->hubAMWObject::init(name, Optional=optional, Advanced=advanced)

  self.setKeywordDefault,'size',15
  if keyword_set(string)  then self.setKeywordDefault, 'title', 'Enter a String'
  if keyword_set(integer) then self.setKeywordDefault, 'title', 'Enter an Integer Number'
  if keyword_set(float)   then self.setKeywordDefault, 'title', 'Enter a Floating-Point Number'

  self.setKeywordDefault,'unit',''
  self.setKeywordDefault,'value',''
  self.setKeywordDefault,'integer', 0
  self.setKeywordDefault,'float', 0
  self.setKeywordDefault,'string', 0
  self.setKeywordDefault,'allowEmptyValue', 0b

  return,1b
end

function hubAMWParameter::getValue $
  ,Message=message

  on_error,2
    
  widget_control,self.getWidgetID('text'),GET_VALUE=valueRaw
  
  value = strtrim(valueRaw,2)

  if value eq '' then begin
    if isa(self.getKeyword('defaultResult')) then begin
      value = self.getKeyword('defaultResult')
      return, value
    endif else begin
      message = 'empty parameter.'
      return,!null
    endelse   
  endif

  on_ioerror,errorLabel
  catch, errorStatus
  if (errorStatus ne 0) then begin
    errorLabel:
    catch, /CANCEL
    if self.getKeyword('integer') then begin
      message = 'unable to convert string to integer number'
    endif
    if self.getKeyword('float') then begin
      message = 'unable to convert string to floating-point number'
    endif
    
    return,!null
  endif

  if self.getKeyword('integer') then value = long64(valueRaw)
  if self.getKeyword('float') then value = double(valueRaw)
  catch, /CANCEL
  
  if self.getKeyword('integer') then begin
    if long64(valueRaw) ne double(valueRaw) then begin
      message = 'not an integer number'
      return,!null 
    endif
  endif
  
  value = value[0] ; make a scalar from 1-elemental array
  
  return,value

end

pro hubAMWParameter::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base,/ROW,XPAD=0,YPAD=0)

  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')
    
  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif

  self.setWidgetID, /Optional,'text',widget_text(base,VALUE=strtrim(self.getKeyword('value'),2),XSIZE=self.getKeyword('size'),SCR_XSIZE=self.getKeyword('scrSize'),/EDITABLE,/NO_NEWLINE)

  if self.getKeyword('unit') ne '' then begin
    !NULL = widget_label(base,VALUE=self.getKeyword('unit'))
  endif

end

function hubAMWParameter::checkConsistenty $
  ,Message=message
  
  value = self.getValue(Message=message)

  if ~isa(value) then begin
    if keyword_set(self.getKeyword('allowEmptyValue')) then begin
      return, 1b
    endif else begin
      return,0b
    endelse
  endif

  ; check relational consistency

  relationalOperator = 'isge'
  if isa(self.getKeyword(relationalOperator)) then begin
    message = 'value must be greater than or equal to '+strcompress(/REMOVE_ALL,self.getKeyword(relationalOperator))
    if ~(value ge self.getKeyword(relationalOperator)) then return,0b
  endif
  
  relationalOperator = 'isgt'
  if isa(self.getKeyword(relationalOperator)) then begin
    message = 'value must be greater than '+strcompress(/REMOVE_ALL,self.getKeyword(relationalOperator))
    if ~(value gt self.getKeyword(relationalOperator)) then return,0b
  endif
  
    relationalOperator = 'isle'
  if isa(self.getKeyword(relationalOperator)) then begin
    message = 'value must be lower than or equal to '+strcompress(/REMOVE_ALL,self.getKeyword(relationalOperator))
    if ~(value le self.getKeyword(relationalOperator)) then return,0b
  endif
  
    relationalOperator = 'islt'
  if isa(self.getKeyword(relationalOperator)) then begin
    message = 'value must be lower than '+strcompress(/REMOVE_ALL,self.getKeyword(relationalOperator))
    if ~(value lt self.getKeyword(relationalOperator)) then return,0b
  endif
      
  message = [] 
  return,1b
  
end

pro hubAMWParameter__define

  define = {hubAMWParameter $
    ,inherits hubAMWObject $
  }
  
end