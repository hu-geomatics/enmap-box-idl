;+
; :Description:
;    This widget allows the selection of a single list item.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;    .. image:: _IDLDOC/combobox1.gif
;    
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash value is given by the index of the selected item.
;      
; :Keywords:
;    Title : in, optional, type=string, default= 'Select Item'
;      Use this keyword to specify the widget title.
;     
;    List : in, required, type=string[]
;      Use this keyword to specify an array of list items.
;    
;    Value : in, optional, type=int, default=0 
;      Use this keyword to define the default list selection. 
;      
;    Extract : in, optional, type={boolean | string[]}, default=0
;      Set this keyword to return the selected item name instead of the index.
;      Alternatively, use this keyword to specify an array of item names, 
;      which will be returned.
;      
;    Size : in, optional, type=int
;      Use this keyword to specify the widget width in pixels.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create a combobox::
;       
;      hubAMW_program, Title='auto-managed widget program'  
;      hubAMW_combobox, 'combobox_value', List=['item A', 'item B', 'item C']
;      result = hubAMW_manage()
;      print, result 
;  
;    .. image:: _IDLDOC/combobox1.gif
;
;-
pro hubAMW_combobox $
  ,name $
  ,Title=title $
  ,List=list $
  ,Value=value $
  ,Extract=extract $  
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWCombobox( $
     name $
    ,Title=title $
    ,List=list $
    ,Value=value $
    ,Extract=extract $
    ,Tooltip=tooltip $
    ,Size=size $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end
