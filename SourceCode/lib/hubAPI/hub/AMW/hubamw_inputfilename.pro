;+
; :Description:
;    This widget allows the selection of an existing input filename.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
;    .. image:: _IDLDOC/inputFilename1.gif
;    
; :Params:
;    name : in, required, type=string
;     Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;     By default, the hash value is given by the selected filename.
;     If the SelectBand or SelectPixel keyword is used,
;     the hash value is given by a hash variable containing the information::
;      
;        hash key         | description               | type
;        -----------------|---------------------------|---------
;        filename         | label image filename      | string
;        band             | selected band,            | integer (optional)
;                         | use /SelectBand keyword   | 
;        pixel            | selected pixel position   | integer[2] (optional)
;                         | use /SelectPixel keyword  |   
;     
; :Keywords:
;    Title : in, optional, type=string, default='Input File'
;     Use this keyword to specify the widget title.
;          
;    Value : in, optional, type=string
;     Use this keyword to specify a default filename. 
;
;    Extension : in, optional, type=string, default='*'
;     Use this keyword to specify the file types to be displayed in the file selection dialog, e.g. 'txt', 'bmp'.
;     
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;     
;    Size : in, optional, type=int, default=500
;     Use this keyword to specify the widget width in pixel.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
;
; :Examples:
;     Select an input filename::
;     
;       hubAMW_program, Title='auto-managed widget program'  
;       hubAMW_inputFilename, 'inputFilename_value'
;       result = hubAMW_manage()
;
;     .. image:: _IDLDOC/inputFilename1.gif
;
;-
pro hubAMW_inputFilename $
  ,name $
  ,Title=title $
  ,Value=value $
  ,Extension=extension $
  ,AllowEmptyFilename=allowEmptyFilename $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  on_error,2

  hubAMWObject = hubAMWInputFilename( $
     name $
    ,Title=title $
    ,Value=value $
    ,Extension=extension $
    ,AllowEmptyFilename=allowEmptyFilename $
    ,Tooltip=tooltip $
    ,Size=size $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced)

  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject

end