.. title:: Renaming and Deleting Buttons

Sometimes it might be suitable to rename or delete some of the widget program buttons.
Therefor use the `hubAMW_manage` in conjunction with the` ButtonNameAccept`,
` ButtonNameCancel` or` ButtonNameAdvanced` keywords.

Here is an example showing how to rename the buttons::
 
  hubAMW_program
  hubAMW_label, ['Rename Buttons','  Accept   => Yes','  Cancel   => No']
  hubAMW_label, '  Advanced => More', /Advanced
  result = hubAMW_manage(ButtonNameAccept='Yes', ButtonNameCancel='No', ButtonNameAdvanced='More')

.. image:: _IDLDOC/renameButtons1.gif 
.. image:: _IDLDOC/renameButtons2.gif

For deleting specific buttons, simply set their names to the empty string ''::
  hubAMW_program
  hubAMW_label, 'All buttons are deleted.'
  result = hubAMW_manage(ButtonNameAccept='', ButtonNameCancel='')

.. image:: _IDLDOC/renameButtons3.gif
