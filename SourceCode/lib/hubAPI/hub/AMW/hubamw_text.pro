;+
; :Description:
; 
;    This widget adds a text box. 
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
;    .. image:: _IDLDOC/text1.gif
;
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash value is given by the text box text in form of a string array.
;
; :Keywords:
;    Title : in, optional, type=string, default='Enter Text'
;      Use this keyword to specify the widget title.
;
;    Value : in, optional, type=string[]
;      Use this keyword to specify the default text.
;
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;
;    XSize : in, optional, type=int, default=300
;      Use this keyword to specify the widget width in pixels.
;
;    YSize : in, optional, type=int, default=150
;      Use this keyword to specify the widget height in pixels.
;      
;    Editable : in, optional, type=boolean, default=1
;      Set this keyword the make the text editable. 
;    
;    Wrap : in, optional, type=boolean, default=0
;      Set this keyword to indicate that the text widget should automatically break lines 
;      at the right edge of the text box.
;      
;    Scroll : in, optional, type=boolean, default=0
;      Set this keyword to insert scroll bars.
;
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create a text box:
;
;      hubAMW_program
;      hubAMW_text, 'text', Value=['This is a line of text...', '...and this a another line of text.']
;      result = hubAMW_manage()
;      print, result
;
;    .. image:: _IDLDOC/text1.gif
;-
pro hubAMW_text $
  ,name $
  ,Title=title $
  ,Value=value $ 
  ,Tooltip=tooltip $
  ,XSize=xsize $
  ,YSize=ysize $
  ,Editable=editable $
  ,Wrap=wrap $
  ,Scroll=scroll $
  ,Optional=optional $
  ,Advanced=advanced $
  ,HubAMWObject=hubAMWObject
  
  on_error,2
  
  hubAMWObject = hubAMWText( $
    name $
    ,Title=title $
    ,Value=value $
    ,Tooltip=tooltip $
    ,XSize=xsize $
    ,YSize=ysize $
    ,Editable=editable $
    ,Wrap=wrap $
    ,Scroll=scroll $
    ,Optional=optional $
    ,Advanced=advanced)
    
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end

;+
; :Hidden:
;-
pro test_hubAMW_text

  
  hubAMW_program
  hubAMW_text, 'text', Value=['This is a line of text...', '...and this a another line of text.'],/ADVANCED,/OPTIONAL
  result = hubAMW_manage()
  print, result

end