;+
; :Description:
;    This procedure is used to insert a tab frame into an auto-managed widget program. 
;    After inserting a tab frame, you can insert tab papes (see `hubAMW_tabPage`) into it. 
;    For details on how to build auto-managed widget programs also see `auto-managed-widget-program.idldoc`.
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify a title for the tab frame.
;      
;    XSize : in, optional, type=int, default=100
;      Use this keyword to specify the tab frame width in pixels.
;
;    YSize : in, optional, type=int, default=50
;      Use this keyword to specify the tab frame height in pixels.
;      
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the tab frame (and all widgets belonging to it) by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
;-
pro hubAMW_tabFrame $
  ,Title=title $
  ,Advanced=advanced $
  ,XSize=xSize, YSize=ySize
  
  on_error,2
    
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertTabFrame, Title=title, Advanced=advanced, XSize=xSize, YSize=ySize
    
end

pro test_hubAMW_tabFrame

  hubAMW_program
  hubAMW_tabFrame, Title='My TabFrame',/ADVANCED
  hubAMW_tabPage, Title='Page 1'
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'p1a', Value='1', /Integer, TITLE='xag art rtrea='
  hubAMW_parameter, 'p1b', Value='2', /Integer, TITLE='x='
  hubAMW_tabPage, Title='Page 2'
  hubAMW_parameter, 'p2a', Value='3', /Integer, TITLE='x='
  hubAMW_parameter, 'p2b', Value='4', /Integer, TITLE='x='
  hubAMW_frame, Title='Frame 3'
  hubAMW_parameter, 'p3a', Value='5', /Integer, TITLE='x='
  hubAMW_parameter, 'p4b', Value='6', /Integer, TITLE='x='
  
  result = hubAMW_manage()
  print,result

end