;+
; :Description:
;    This widget allows the selection of an output directory.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
;    .. image:: _IDLDOC/outputDirectoryName1.gif
;    
; :Params:
;    name : in, required, type=string
;     Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;     The hash value is given by the selected directory name.
;     
; :Keywords:
;    Title : in, optional, type=string, default='Output Directory'
;     Use this keyword to specify the widget title.
;          
;    Value : in, optional, type=string
;     Use this keyword to specify a default directory name.
;      
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;     
;    Size : in, optional, type=int
;     Use this keyword to specify the widget width in characters.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;     Select an output directory::
;     
;       hubAMW_program
;       hubAMW_outputDirectoryName, 'outputDirectoryName_value'
;       result = hubAMW_manage()
;       print, result
;
;     .. image:: _IDLDOC/outputDirectoryName1.gif
;
;-
pro hubAMW_outputDirectoryName $
  ,name $
  ,Title=title $
  ,Value=value $
  ,DefaultResult=defaultResult $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
 ; on_error,2

  hubAMWObject = hubAMWOutputDirectoryName( $
     name $
    ,Title=title $
    ,Value=value $
    ,DefaultResult=defaultResult $
    ,Tooltip=tooltip $
    ,Size=size $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced)

  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end
