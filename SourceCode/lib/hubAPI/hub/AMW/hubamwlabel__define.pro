;+
; :Hidden:
;-

function hubAMWLabel::init $
  ,value $ ; array of strings
  ,Advanced=advanced
  
  !NULL=self->hubAMWObject::init('', Advanced=advanced)

  self.setKeywordDefault,'value',''

  return,1b
end

pro hubAMWLabel::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  foreach line,self.getKeyword('value') do begin
    !NULL = widget_label(self.base,VALUE=line,/ALIGN_LEFT)
  endforeach
end

pro hubAMWLabel__define

  define = {hubAMWLabel $
    ,inherits hubAMWObject $
  }
  
end
