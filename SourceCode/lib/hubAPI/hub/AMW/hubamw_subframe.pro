;+
; :Description:
;    This procedure is used to insert a subframe into an auto-managed widget program. After inserting a subframe, you can insert widgets into it.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
; :Params:
;    exclusiveGroupName : in, optional, type=string
;      Use this argument to assign the subframe to an exclusive subframe group. The name of the exclusive subframe group is given by this argument and will be part of the result hash returned by `hubAMW_manage` function. 
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify a title for the subframe.
;      
;    Column : in, optional, type=boolean
;      Set this keyword to arrange widgets in a column based layout. You have to set either the `Column` or the `Row` keyword.
;    
;    Row : in, optional, type=boolean
;      Set this keyword to arrange widgets in a row based layout. You have to set either the `Column` or the `Row` keyword.
;    
;    SetButton : in, optional, type=boolean
;      Set this keyword only in conjunction with the `exclusiveGroupName` argument to initially select the subframe.
;    
;    TargetBase: in, optional, type=base identifier, hidden
;
;-
pro hubAMW_subframe $
  ,exclusiveGroupName $
  ,Title=title $
  ,Column=column $
  ,Row=row $
  ,SetButton=setButton $
  ,TargetBase=targetBase

  on_error,2
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertSubFrame $
    ,Title=title $
    ,Column=column $
    ,Row=row $
    ,ExclusiveGroupName=exclusiveGroupName $
    ,SetButton=setButton $
    ,TargetBase=targetBase

end