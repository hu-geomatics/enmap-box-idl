;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This widget adds a drawing area. Use the `Image` keyword to draw an initial image or
;    use an `hubAMW_button` widget to allow to refresh the drawing area in dependence to the current
;    user selection.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;   .. image:: _IDLDOC/draw1.gif
;   .. image:: _IDLDOC/draw2.gif
;
; :Params:
;    name: in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash 
;      (see `hubAMW_manage` for details).
;      The hash value is given by the draw widget window id. 
;      This window id can be used in combination with `hubAMW_button` widgets to refresh the
;      drawing area. Note that after the amw program is closed, this window id is not valid anymore.
;     
; :Keywords:
;    Image: in, optional, type={byte[][] | byte[][][3]}
;      Use this keyword to display a greyscale or RGB image (interleave bsq).
;      
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the drawing area.
;      
;    XSize: in, optional, type=int
;      Use this keyword to specify the widget width in pixels.
;
;    YSize: in, optional, type=int
;      Use this keyword to specify the widget height in pixels.
;    
;    Advanced: in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;    
;    IDLObjectGraphics: in, optional, type=boolean, default=false
;     Set this to use IDL Object graphics instead of Direct Graphics.
;     
; :Examples:
; 
;    Create a drawing area, that shows a random RGB image::
;      
;      randomImage = bytscl(randomu(seed, 100, 100, 3))
;      
;      hubAMW_program
;      hubAMW_draw, 'draw', Image=randomImage
;      result = hubAMW_manage()
;      print, result
;
;  .. image:: _IDLDOC/draw1.gif
;  
;    Second example: Create a drawing area, that can be refreshed in dependence to the user input
;    via a button. First compile the buttons event handler that refreshes the drawing area. 
;    This event handler uses the user input given by the parameter widget "exponent" (Exponent a)
;    to plot the functional dependency between x and x^a in the range 0 <= x <= 99:: 
;    
;      pro buttonEventHandler, resultHash
;        
;        ; set the draw widget window id
;         
;        windowID = resultHash['draw'] 
;        wset, windowID
;        
;        ; plot the functional dependency
;        
;        a = resultHash['exponent']
;        plot, findgen(100)^a, XTITLE='x', YTITLE='f(x)', TITLE='f(x) = x^'+strtrim(string(a, Format='(%"%10.2f")'), 2)
;        
;      end
;
;    Now, run the amw program::
;
;      hubAMW_program
;      hubAMW_draw, 'draw', XSize=300, YSize=300
;      hubAMW_parameter, 'exponent', Title='Exponent a =', Value = 2, /Float
;      hubAMW_label, ['Change the parameter and refresh the plot','by clicking on the Refresh button.']
;      hubAMW_button, Title='Refresh', EventHandler='buttonEventHandler', /Bar, /Press
;      result = hubAMW_manage()
;      print, result
;      
;  .. image:: _IDLDOC/draw2.gif
;-
pro hubAMW_draw $
  ,name $
  ,Image=image $
  ,Tooltip=tooltip $
  ,XSize=xSize $
  ,YSize=ySize $  
  ,Advanced=advanced $
  ,IDLObjectGraphics=IDLObjectGraphics
   
  on_error,2
  
  hubAMWObject = hubAMWDraw($
     name $
    ,Image=image $
    ,Tooltip=tooltip $
    ,XSize=xSize $
    ,YSize=ySize $
    ,Advanced=advanced $
    ,IDLObjectGraphics=IDLObjectGraphics $
    )
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end

;+
; :Hidden:
;-
pro test_hubAMW_draw
    
  hubAMW_program
  hubAMW_draw, 'drawValue'
  result = hubAMW_manage()
  print,result

end 