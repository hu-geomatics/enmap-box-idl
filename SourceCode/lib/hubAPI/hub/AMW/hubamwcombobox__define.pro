;+
; :Hidden:
;-

function hubAMWCombobox::init $
  ,name $
  ,Title=title $
  ,List=list $
  ,Value=value $
  ,Extract=extract $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  if ~isa(list) then begin
    message,'Missing keyword: List.'
  endif 
  
  !NULL=self->hubAMWObject::init(name,Optional=optional,Advanced=advanced)

  self.setKeywordDefault,'title','Select Item'
  self.setKeywordDefault,'value',0

  return,1b
end

function hubAMWCombobox::getValue

  on_error,2
  
  comboboxText = widget_info(self.getWidgetID('combobox'),/COMBOBOX_GETTEXT)
  comboboxIndex = where(comboboxText eq self.getKeyword('list'))
  value = comboboxIndex[0]
  
  if isa(self.getKeyword('extract')) then begin
    
    if isa(self.getKeyword('extract'), /SCALAR) then begin
      value = (self.getKeyword('list'))[value]
    endif

    if isa(self.getKeyword('extract'), /ARRAY) then begin
      value = (self.getKeyword('extract'))[value]
    endif
    
  endif

  return,value

end

pro hubAMWCombobox::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base,/ROW,XPAD=0,YPAD=0)

  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT   $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')

  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif

  self.setWidgetID, /Optional, 'combobox',widget_combobox(base,VALUE=self.getKeyword('list'),SCR_XSIZE=self.getKeyword('size'),UVALUE=self)
  widget_control,self.getWidgetID('combobox'),SET_COMBOBOX_SELECT=self.getKeyword('value')
  
end

pro hubAMWCombobox__define

  define = {hubAMWCombobox $
    ,inherits hubAMWObject $
  }
  
end
