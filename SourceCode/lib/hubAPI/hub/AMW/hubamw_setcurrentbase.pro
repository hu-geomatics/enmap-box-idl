;+
; :Private:
; 
; :Description:
;    Use this procedure to set the current base for inserting auto-managed widgets. 
;    Use it in conjunction with the `hubAMW_getCurrentBase` function to create AMW programs
;    with arbitrary widget hierarchies.
;
; :Params:
;    base : in, required, type=widget base
;
;-
pro hubAMW_setCurrentBase, base
    
  on_error,2
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.setCurrentBase, base
  
end