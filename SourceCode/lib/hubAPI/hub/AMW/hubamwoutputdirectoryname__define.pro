;+
; :Hidden:
;-

function hubAMWOutputDirectoryName::init $
  ,name $
  ,Title=title $
  ,Value=value $
  ,DefaultResult=defaultResult $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  !NULL=self->hubAMWObject::init(name, Optional=optional, Advanced=advanced)

  self.setKeywordDefault, 'title', 'Output Directory'
  self.setKeywordDefault, 'value', ''
  self.setKeywordDefault, 'size', 100
 
  return,1b
end

function hubAMWOutputDirectoryName::getValue

  ; get combobox text

  widget_control,self.getWidgetID('text'), GET_VALUE=value

  ; get fileName
  
  value = strtrim(value[0],2)
  
  if value eq '' then begin
    if isa(self.getKeyword('defaultResult')) then begin
      value = self.getKeyword('defaultResult')
      return, value
    endif else begin
      message = 'empty directory name.'
      return,!null
    endelse
  endif
  
  return,value
end

pro hubAMWOutputDirectoryName::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base, /ROW, XPAD=0, YPAD=0)
  
  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT   $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')
    
  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif
  
  textDefaultFile = self.getKeyword('value')+' '
  textTemporaryFile = hub_getUserTemp(file_basename(self.getKeyword('value')))
  if file_dirname(textDefaultFile) eq '.' then begin
    textDefaultFile = textTemporaryFile
  endif
  
  self.setWidgetID, /Optional,'text',widget_text(base,VALUE=textDefaultFile, XSIZE=self.getKeyword('size'),/NO_NEWLINE,/EDITABLE)
  self.setWidgetID, /Optional,'button',widget_button(base,VALUE='...', UVALUE=self)

end

pro hubAMWOutputDirectoryName::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return

  ; get directory name
  
  filename = dialog_pickfile($
     DIALOG_PARENT=event.top $
    ,/Directory $
    ,/WRITE)
  
  if filename ne '' then begin
    widget_control,self.getWidgetID('text'), SET_VALUE=filename
    ; set to path as current working directory
    cd, file_dirname(filename)

  endif
   
end

function hubAMWOutputDirectoryName::checkConsistenty $
  ,Message=message
  
  value = self.getValue()

  ; check if directory name is empty
  
  if value eq '' then begin
    message = 'directory name is empty'
    return,0b
  endif

  ; check if directory already exists

  if (hubIOHelper()).fileReadable(value) then begin
    dialogMessage = [ $
       'Directory: '+value $
      ,'' $
      ,'This directory already exists.' $
      ,'Overwrite this directory?' $
    ]
    ok = dialog_message(dialogMessage,/CENTER,DIALOG_PARENT=self.base,/QUESTION,TITLE='Output Directory')
    if ok eq 'No' then begin
      message = 'directory already exists'
      return,0b
    endif
  endif

  message = [] 
  return,1b
  
end

pro hubAMWOutputDirectoryName__define

  define = {hubAMWOutputDirectoryName $
    , inherits hubAMWObject $
  }
  
end