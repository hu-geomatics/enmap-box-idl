;+
; :Hidden:
;-

function hubAMWInputImageFilename::init $
  , name $
  , Title=title $
  , Value=value $
  , BandValue=bandValue $
  , PixelValue=pixelValue $
  , SpectralSubsetValue=spectralSubsetValue $   
  , SelectBand=selectBand $
  , SelectPixel=selectPixel $
  , SelectSpectralSubset=selectSpectralSubset $
  , AllowEmptyImage=allowEmptyImage $
  , AllowEmptyBand=allowEmptyBand $
  , AllowEmptyPixel=allowEmptyPixel $
  , Tooltip=tooltip $
  , Size=size $
  , TSize=tSize $
  , Optional=optional $
  ,Advanced=advanced

  !null = self->hubAMWInputSampleSet::init(name, Title=title, Value=value, Size=size, TSize=tSize, /FeatureImageOnly,Optional=optional,Advanced=advanced,Tooltip=tooltip $
    , BandValue=bandValue $
    , PixelValue=pixelValue $
    , SpectralSubsetValue=spectralSubsetValue $    
    , SelectBand=selectBand $
    , SelectPixel=selectPixel $
    , SelectSpectralSubset=selectSpectralSubset $
    , AllowEmptyImage=allowEmptyImage $
    , AllowEmptyBand=allowEmptyBand $
    , AllowEmptyPixel=allowEmptyPixel)

  return,1b
end

function hubAMWInputImageFilename::getValue

  valueInputSampleSet = self->hubAMWInputSampleSet::getValue()
  
  value = hash()
  value['filename'] = valueInputSampleSet['featureFilename']
  if self.getKeyword('selectBand') then begin
    value['band'] = valueInputSampleSet['band']
  endif
  if self.getKeyword('selectPixel') then begin
    value['pixel'] = valueInputSampleSet['pixel']
  endif
  if self.getKeyword('selectSpectralSubset') then begin 
    value['spectralSubset'] = valueInputSampleSet['spectralSubset']  
  endif
  
  if n_elements(value) eq 1 then begin
    value = value['filename'] 
  endif
  return, value

end

function hubAMWInputImageFilename::checkConsistenty $
  ,Message=message
  
  hubHelper = hubHelper()
  value = self.getValue()
  filename = isa(value, 'hash') ? value['filename'] : value
 
  ; check if image filename is empty
  
  if filename eq '' and ~keyword_set(self.getKeyword('allowEmptyImage')) then begin
    message = 'image filename is empty'
    return, 0b
  endif

  ; check if image file exists
  
  if ~ (hubIOHelper()).fileReadable(filename)  and ~keyword_set(self.getKeyword('allowEmptyImage')) then begin
    message = 'image file does not exist'
    return, 0b
  endif
  
  message = []
  return, 1b

end

pro hubAMWInputImageFilename__define

  define = {hubAMWInputImageFilename $
    ,inherits hubAMWInputSampleSet $
  }
  
end