;+
; :Hidden:
;-

function hubAMWChecklist::init $
  ,name $
  ,Title=title $
  ,Value=value $ ; initially selected list index
  ,List=list $ ; names of the list elements
  ,AllowEmptySelection=allowEmptySelection $
  ,MultipleSelection=multipleSelection $
  ,ShowButtonBar=showButtonBar $
  ,Extract=extract $
  ,InsensitiveList=insensitiveList $
  ,Flag=flag $
  ,Column=column $
  ,XSize=xsize $
  ,YSize=ysize $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced $
  ,Tooltip=tooltip

  if ~isa(list) then begin
    message,'Missing keyword: List.'
  endif 

  !NULL=self->hubAMWObject::init(name  ,Optional=optional,Advanced=advanced)

  self.setKeywordDefault,'title','Select Item'
  self.setKeywordDefault,'allowEmptySelection',0
  self.setKeywordDefault,'multipleSelection',0
  self.setKeywordDefault,'showButtonBar',0
  self.setKeywordDefault,'column',0
  self.setKeywordDefault,'flag',0
  self.setKeywordDefault,'insensitiveList', []
  self.setInformation,'numberOfListElements',n_elements(list)
  
  return,1b
end

function hubAMWChecklist::getValue

  value = list()
  foreach button, self.getInformation('checkListButtonIDs'),i do begin
    if widget_info(button,/BUTTON_SET) then value.add,i
  endforeach
  value = value.toArray()
  
  if isa(self.getKeyword('extract')) then begin
    if isa(self.getKeyword('extract'), /SCALAR) then begin
      value = (self.getKeyword('list'))[value]
    endif
    if isa(self.getKeyword('extract'), /ARRAY) then begin
      value = (self.getKeyword('extract'))[value]
    endif
  endif

  if self.getKeyword('flag') then begin
    flags = bytarr(self.getInformation('numberOfListElements'))
    flags[value] = 1
    value = flags
  endif


  return, value
end

pro hubAMWChecklist::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

;  column = keyword_set(self.getKeyword('column'))
;  row = ~column

  column = self.getKeyword('column')
  row = ~keyword_set(column)

  base = widget_base(self.base,COLUMN=column,ROW=row,XPAD=0,YPAD=0)

  if self.getKeyword('title') ne '' then begin
    titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
    labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT $
      , UVALUE=self, /TRACKING_EVENTS)
    (self.tooltips)[labelID] = self.getKeyword('tooltip')
  endif

  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif
  
  valueFlag = replicate(0b, self.getInformation('numberOfListElements'))
  valueFlag[self->getKeyword('value')] = 1b
  sensitiveFlag = replicate(1b, self.getInformation('numberOfListElements'))
  sensitiveFlag[self->getKeyword('insensitiveList')] = 0b
  
  
  buttonBase = widget_base(base,COLUMN=column,ROW=row,XPAD=0,YPAD=0 $
    ,EXCLUSIVE=~self.getKeyword('multipleSelection'),NONEXCLUSIVE=self.getKeyword('multipleSelection') $
    ,SCROLL=keyword_set(self.getKeyword('xsize')) or keyword_set(self.getKeyword('ysize')) $
    ,SCR_XSIZE=self.getKeyword('xsize'),SCR_YSIZE=self.getKeyword('ysize'))
  self.optionalWidgetIDs.add, buttonBase
  self.setInformation,'checkListButtonIDs',list()
  foreach name,self.getKeyword('list'),i do begin
    button = widget_button(buttonBase,VALUE=name)
    widget_control,button,SET_BUTTON=valueFlag[i], SENSITIVE=sensitiveFlag[i], /CLEAR_EVENTS
    (self->getInformation('checkListButtonIDs')).add,button
  endforeach

  if self.getKeyword('multipleSelection') and self.getKeyword('showButtonBar') then begin
    buttonBase = widget_base(self.base,/ROW,XPAD=0,YPAD=0)
    self.setWidgetID, /Optional, 'buttonSelectAll',widget_button(buttonBase,Value='Select All',UVALUE=self)
    self.setWidgetID, /Optional, 'buttonSelectNone',widget_button(buttonBase,Value='Select None',UVALUE=self)
  endif

end

function hubAMWChecklist::checkConsistenty $
  ,Message=message

  value = self.getValue()
  if ~self.getKeyword('allowEmptySelection') then begin
    message = 'no item selected'
    if n_elements(value) eq 0 then return,0b
  endif

  message = [] 
  return,1b
  
end

pro hubAMWChecklist::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return

  if event.id eq self.getWidgetID('buttonSelectAll') then begin
    foreach button,self.getInformation('checkListButtonIDs') do begin
      widget_control,button,SET_BUTTON=1,/CLEAR_EVENTS
    endforeach
  endif
  
  if event.id eq self.getWidgetID('buttonSelectNone') then begin
    foreach button,self.getInformation('checkListButtonIDs') do begin
      widget_control,button,SET_BUTTON=0,/CLEAR_EVENTS
    endforeach
  endif
end

pro hubAMWChecklist__define

  define = {hubAMWChecklist $
    ,inherits hubAMWObject $
  }
  
end

pro test_hubAMWChecklist

  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch,/CANCEL
    help,/Last_message
    return
  endif
  
  groupLeader = widget_base()
  hubAMWProgram = hubAMWProgram(Title='Auto-Managed Widget Program',groupLeader)
  hubAMWProgram.insertFrame,Title='Input'
  hubAMWProgram.insertWidget,hubAMWChecklist('checkList' $
    ,Title='Select items:' $
    ,Value=2 $
    ,List='list item'+strcompress(indgen(50)+1) $ 
    ,AllowEmptySelection=0 $
    ,MultipleSelection=1 $
    ,Column=1 $
    ,XSize=300 $
    ,YSize=300 $
    ,ShowButtonBar=0 $
    )
  
  result = hubAMWProgram.manage()
  widget_control,groupLeader,/DESTROY
  if ~result['accept'] then return

  print,result
  


end
