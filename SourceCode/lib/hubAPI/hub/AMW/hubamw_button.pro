;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This widget adds a button.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;   .. image:: _IDLDOC/button1.gif
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the widget title.
;      
;    Bar: in, optional, type=boolean
;      Set this keyword to place the button next to the Accept and Cancel buttons of the amw program.
;    
;    ConsistencyCheckFunction: in, optional, type=string
;      A string containing the name of a user-defined consistency check function to be called prior to the event handler routine. 
;      A Consistency check function has one required input argument called resultHash and one required output keyword called Message.
;      Optionally, the function can have an additional keyword called UserInformation (see `UserInformation`) to provide some extra information.
;      The return value is boolean (0b or 1b). 
; 
;      For details on how to define a consistency check function see `check-consistency.idldoc`.
;
;    EventHandler: in, required, type=string
;      Use this keyword to specify the buttons event handler procedure.
;      This procedure has one required input argument called resultHash.
;      Via the resultHash argument, you can access the current user selection of the amw program.
;      The result hash is limited to the subhash that is specified via the `ResultHashKeys` keyword.
;
;    Press: in, optional, type=boolean
;      Set this keyword to execute the event handler at start up.
;      
;    ResultHashKeys: in, optional, type=string[]
;      Use this keyword to specify an array with hash keys, that should be passed to the event handler.  
;    
;    UserInformation: in, optional, type=Hash
;      Hash to provide further information to the consistency check function that are not part of the regular result hash.
;
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;      
;    Advanced: in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
; 
;    Create a button that says Hello World::
;      First compile the buttons event handler::
;
;        pro eventHandler, resultHash
;          ok = dialog_message('Hello World', /INFORMATION)
;        end
;
;      Now, run the amw program::
;
;        hubAMW_program 
;        hubAMW_button, Title='Button', EventHandler='eventHandler'
;        result = hubAMW_manage()
;        
;    .. image:: _IDLDOC/button1.gif
;  
;    Press the button:
;  
;    .. image:: _IDLDOC/button2.gif
;-
pro hubAMW_button $
  ,Title=title $ 
  ,Bar=bar $
  ,ConsistencyCheckFunction=consistencyCheckFunction $
  ,ResultHashKeys=resultHashKeys $
  ,UserInformation=userInformation $
  ,Tooltip=tooltip $
  ,EventHandler=eventHandler $
  ,Press=press $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWButton($
     Title=title $
    ,Bar=bar $  
    ,ConsistencyCheckFunction=consistencyCheckFunction $
    ,ResultHashKeys=resultHashKeys $    
    ,UserInformation=userInformation $   
    ,Tooltip=tooltip $ 
    ,EventHandler=eventHandler $
    ,Press=press $
    ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end

;+
; :Hidden:
;-
pro test_hubAMW_button

  hubAMW_program
  hubAMW_list, 'list1', Title='My List 1', List=['1','2','3']
  hubAMW_list, 'list2', Title='My List 2', List=['a','b','c']
  hubAMW_list, 'list3', Title='My List 3', List=['-','*','#']
  
  hubAMW_button, Title='Button', EventHandler='test_hubAMW_button_eventHandler', UserInformation='userInfo' $
    , ConsistencyCheckFunction='test_hubAMW_button_consistencyCheckFunction' $
    , ResultHashKeys=[];'list1']
  result = hubAMW_manage()
  print,result

end

;+
; :Hidden:
;-
pro test_hubAMW_button_eventHandler, resultHash, UserInformation=userInformation

  print, '+++ This is the event handler procedure +++
  print
  print, 'User Information is available'
  print, userInformation
  print
  print, 'Result Hash is available'
  print, resultHash
  print
  
  ok = dialog_message('Hello World', /INFORMATION)

end

;+
; :Hidden:
;-
function test_hubAMW_button_consistencyCheckFunction, resultHash, Message=message, UserInformation=userInformation

  ; this consistency check function does nothing usefull,
  ; but it can use user information
  ; ...

  print, '+++ This is the consistency check function +++
  print
  print, 'User Information is available'
  print, userInformation
  print
  print, 'Result Hash is available'
  print, resultHash
  print
  message = 'Error Message'
   
  return, 1b
end