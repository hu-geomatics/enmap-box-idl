;+
; :Description:
;    This widget is a wrapper for the IDL `dialog_pickfile` function. 
;    All `_EXTRA` keywords are directly passed to `dialog_pickfile`.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
;    .. image:: _IDLDOC/pickfile1.gif
;    
; :Params:
;    name : in, required, type=string
;     Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;     The hash value is given by the `dialog_pickfile` result value.
;     
; :Keywords:
;    Title : in, optional, type=string, default='File'
;     Use this keyword to specify the widget title.
;          
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;     
;    Size : in, optional, type=int, default=500
;     Use this keyword to specify the widget width in pixel.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
;    _EXTRA : 
;      All extra keywords are passed to the `dialog_pickfile` function.
;
; :Examples:
;    Select multiple files from a folder. 
;    Note that the MULTIPLE_FILES and MUST_EXIST keywords are _EXTRA keywords that 
;    are passed to the internally re-used `dialog_pickfile` function::
;     
;      hubAMW_program
;      hubAMW_pickfile, 'pickfile', /MULTIPLE_FILES, /MUST_EXIST
;      result = hubAMW_manage()
;      print, result
;
;    .. image:: _IDLDOC/pickfile1.gif
;
;-
pro hubAMW_pickfile $
  ,name $
  ,Title=title $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced $
  ,_EXTRA=_extra
  
  on_error,2

  hubAMWObject = hubAMWPickfile( $
     name $
    ,Title=title $
    ,Tooltip=tooltip $
    ,Size=size $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced $
    ,_EXTRA=_extra)

  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject

end

;+
; :Hidden:
;-
pro test_hubAMW_pickfile

  hubAMW_program
  hubAMW_pickfile, 'pickfile', TOOLTIP='Tooltip', File='',/MULTIPLE_FILES, /MUST_EXIST
  result = hubAMW_manage()
  print, result

end