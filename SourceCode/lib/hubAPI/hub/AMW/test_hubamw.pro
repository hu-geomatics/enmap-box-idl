;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Hidden:
;-

pro test_hubAMW

  @huberrorcatch

  hubAMW_program

  list = 'item'+strcompress(indgen(5)+1)
  listLongnames = 'Longname '+list
  optional=0
  advanced=0
  tSize = 150
  tooltip = ['You can use tool tips to explain the interface.', $
             'You can use tool tips to explain the interface.']
  case 3 of
    -1 : begin
;      hubAMW_inputSampleSet, 'inputSampleSet', Optional=optional, Tooltip=tooltip, TSIZE=tSize $
;        , Value= hub_getTestImage('Hymap_Berlin-B_Image'), /REGRESSION $
;        , /SelectSpectralSubset, SpectralSubsetValue=[2,4,6]
      hubAMW_inputImageFilename, 'inputImageFilename', Optional=optional, Tooltip=tooltip, TSIZE=tSize $
        , Value= hub_getTestImage('Hymap_Berlin-B_Image') $
        , /SelectSpectralSubset, SpectralSubsetValue=[2,4,6] $
        , /SELECTBAND
        
    end
    0 : begin
      widgetName = 'checkbox widget (hubAMW_checkbox)'
      hubAMW_checkbox, 'checkbox_value'
    end
    1 : begin
      widgetName = 'checklist widget (hubAMW_checklist)'
      hubAMW_checklist, 'checklist_value1', Title='abc', List=list, Value=1, Tooltip=tooltip,OPTIONAL=optional
    end
    2 : begin
      widgetName = 'combobox widget (hubAMW_combobox)'
      hubAMW_combobox, 'combobox_value1', List=list, Optional=optional, Tooltip=tooltip
    end
    3 : begin
      widgetName = 'input filename widget (hubAMW_inputFilename)'
      hubAMW_inputFilename, 'inputFilename_value',Value=!NULL, Extension='svc', Optional=optional, Tooltip=tooltip
    end
    4 : begin
      widgetName = 'input image filename widget (hubAMW_inputImageFilename)'
      hubAMW_inputImageFilename, 'inputImageFilename', Optional=optional, Tooltip=tooltip $
        , Value          = hub_getTestImage('Hymap_Berlin-B_Image') $
        , /SelectBand, /SelectPixel $
        , /AllowEmptyBand, /AllowEmptyPixel $
        , BandValue=23, PixelValue=[13,15]
    end
    5 : begin
      widgetName = 'label widget (hubAMW_label)'
      hubAMW_label, 'This is a label.'
    end
    6 : begin
      widgetName = 'list widget (hubAMW_list)'
      hubAMW_list, 'list_value', List=list, Optional=optional, Tooltip=tooltip,/MULTIPLESELECTION,/SHOWBUTTONBAR
    end
    7 : begin
      widgetName = 'output filename widget (hubAMW_outputFilename)'
      hubAMW_outputFilename, 'outputFilename_value', Value='test', Optional=optional, EXTENSION='html', Tooltip=tooltip
    end
    8 : begin
      widgetName = 'parameter widget (hubAMW_parameter)'
      hubAMW_parameter,'parameter_value', Optional=optional, /INTEGER ,Value=!null,Unit='%', Tooltip=tooltip
    end
    9 : begin
      widgetName = 'input sample set widget (hubAMW_inputSampleSet)'
      hubAMW_inputSampleSet, 'inputSampleSet', Optional=optional, /Regression $
        , Title          = 'Spectral Image  ' $
        , ReferenceTitle = 'Regression Image' $
     ;   , Value          = hub_getTestImage('Hymap_Berlin-B_Image') $
        , ReferenceValue = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample') $
        , /SelectBand, /SelectPixel $
        , /AllowEmptyBand, /AllowEmptyPixel; $
;        , BandValue=23, PixelValue=[13,15]
    end
    10 : begin
      widgetName = 'input directory widget (hubAMW_inputDirectory)'
      hubAMW_inputDirectoryName, 'inputDirectory_value', Value='t:\testDir2', Optional=optional, Tooltip=tooltip 
    end
    11 : begin
      widgetName = 'output directory widget (hubAMW_outputDirectory)'
      hubAMW_outputDirectoryName, 'outputDirectory_value', Value='t:\testDir2', Optional=optional, Tooltip=tooltip
    end
    12 : begin
      widgetName = 'drawing area widget (hubAMW_draw)'
      hubAMW_draw, 'draw', Image=bytscl(randomu(seed,100,100)), XSIZE=300, YSIZE=250 $
          , Tooltip=tooltip, IDLObjectGraphics=1
    end
    13 : begin
      widgetName = 'button widget (hubAMW_button)'
      hubAMW_button, Tooltip=tooltip
    end
    14 : begin
      widgetName = 'color selection widget (hubAMW_color)'
      hubAMW_color, 'color', Tooltip=tooltip, OPTIONAL=optional
    end
    15 : begin
      widgetName = 'text widget (hubAMW_text)'
      hubAMW_text, 'text', Tooltip=tooltip,OPTIONAL=optional
    end
    
    else:;
    
  endcase

;  if isa (widgetName) then hubAMW_label,'*'+widgetName+'*'

  result = hubAMW_manage(/Flat)
  print,result
  ;if result['accept'] then print,result['inputImageFilename']

end