;+
; :Private:
; :Description:
;    Use this function to get the current base for inserting auto-managed widgets. 
;    Use it in conjunction with the `hubAMW_setCurrentBase` procedure to create AMW programs
;    with arbitrary widget hierarchies.
;
; :Examples:
;    Building an AMW program with a hierarchy that is to complex for simply using hubAMW_frame and hubAMW_subframe::
;      
;      hubAMW_program
;      hubAMW_frame, TITLE='Frame'
;    
;      ; create widget base hierarchy
;      
;      currentBase = hubAMW_getCurrentBase()
;      baseLevel1 = widget_base(currentBase, /ROW)
;      baseLevel11 = widget_base(baseLevel1, /COLUMN, XPAD=0, YPAD=0)
;      baseLevel12 = widget_base(baseLevel1, /COLUMN, XPAD=0, YPAD=0)
;      baseLevel121 = widget_base(baseLevel12, /COLUMN, XPAD=0, YPAD=0)
;      baseLevel122 = widget_base(baseLevel12, /COLUMN, XPAD=0, YPAD=0)
;    
;      ; insert AMW widgets
;    
;      hubAMW_setCurrentBase, baseLevel11
;      hubAMW_label, 'level 1.1'
;      hubAMW_setCurrentBase, baseLevel121
;      hubAMW_label, 'level 1.2.1'
;      hubAMW_setCurrentBase, baseLevel122
;      hubAMW_label, 'level 1.2.2'
;      result = hubAMW_manage()
;-
function hubAMW_getCurrentBase
    
  on_error,2
  
  hubAMWProgram = hubAMW_getProgram()
  return, hubAMWProgram.getCurrentBase()
  
end