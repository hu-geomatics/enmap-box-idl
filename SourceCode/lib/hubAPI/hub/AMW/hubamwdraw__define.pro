;+
; :Hidden:
;-

function hubAMWDraw::init $
  ,name $
  ,Image=image $ 
  ,Tooltip=tooltip $
  ,XSize=xSize $
  ,YSize=ySize $
  ,Advanced=advanced $
  ,IDLObjectGraphics=IDLObjectGraphics 
  
  
  !NULL=self->hubAMWObject::init(name, Advanced=advanced)
  
  self.useObjectGraphics = keyword_set(IDLObjectGraphics) 
  return,1b
end

function hubAMWDraw::getValue

  widget_control, self.getWidgetID('draw'), GET_VALUE=windowID
  return, windowID
end

pro hubAMWDraw::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase
  
  if self.useObjectGraphics then begin
    drawID = widget_window(self.base $
      , XSIZE=self.getKeyword('xSize') $
      , YSIZE=self.getKeyword('ySize') $
       , UVALUE=self, /TRACKING_EVENTS)
      
  endif else begin
    drawID = widget_draw(self.base $
      , SCR_XSIZE=self.getKeyword('xSize') $
      , SCR_YSIZE=self.getKeyword('ySize')   $
      , UVALUE=self, /TRACKING_EVENTS)  
  endelse
  
  (self.tooltips)[drawID] = self.getKeyword('tooltip')
  self.setWidgetID, 'draw', drawID
end

pro hubAMWDraw::_drawInitialImage

  if isa(self.getKeyword('image')) then begin
    img = self.getKeyword('image')
    w = self.getValue()
    if self.useObjectGraphics then begin
      
      !NULL = image(img, CURRENT=w)
    endif else begin
      wset, w
      true = size(self.getKeyword('image'), /N_DIMENSIONS) eq 3 ? 3 : !null
      tv, img, TRUE=true
    endelse
    
  endif 

end


pro hubAMWDraw__define

  define = {hubAMWDraw $
    ,inherits hubAMWObject $ 
    ,useObjectGraphics:0b $
  }
  
end