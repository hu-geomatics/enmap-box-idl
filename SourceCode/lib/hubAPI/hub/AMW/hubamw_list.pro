;+
; :Description:
; 
;    This widget allows the selection of a single or multiple list items. 
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
;    .. image:: _IDLDOC/list1.gif
;
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash value is given by the selected item index, 
;      by an array of selected item indices (if MultipleSelection=1),
;      or, in case of an empty selection, by !NULL.
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify the widget title.
;
;    Value : in, optional, type={int | int[]}
;      Use this keyword to specify the default list selection index.
;      In conjunction with the MultipleSelection keyword, 
;      use it to specify an array of default list selection indices.
;
;    List : in, required, type=string[]
;      Use this keyword to specify an array of list items.
;    
;    AllowEmptySelection : in, optional, type=boolean, default=0
;      Set this keyword to enable empty list selections. 
;
;    MultipleSelection : in, optional, type=boolean, default=0
;      Set this keyword to enable the selection of multiple items.
;      
;    Extract : in, optional, type={boolean | string[]}, default=0
;      Set this keyword to return the selected item name instead of the index.
;      Alternatively, use this keyword to specify an array of item names, 
;      which will be returned.
;      
;    ShowButtonBar : in, optional, type=boolean, default=1
;      Set this keyword to insert "Select All", "Select None" and "Enter Selection" buttons to the widget.
;      Use it in conjunction with the MultipleSelection keyword.
;      
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;      
;    XSize : in, optional, type=int, default=100
;      Use this keyword to specify the widget width in pixels.
;
;    YSize : in, optional, type=int, default=50
;      Use this keyword to specify the widget height in pixels.
;
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create an exclusive list with three items, select the second item by default::
;
;      hubAMW_program
;      hubAMW_list, 'listValue', List=['item A', 'item B', 'item C'], Value=1
;      result = hubAMW_manage()
;      print, result
;
;    .. image:: _IDLDOC/list1.gif
;    
;    Create an non-exclusive checklist with three items, select the first and the third items by default:
;
;      hubAMW_program
;      hubAMW_list, 'listValue', List=['item A', 'item B', 'item C'], Value=[0, 2], /MultipleSelection
;      result = hubAMW_manage()
;      print, result
;  
;    .. image:: _IDLDOC/list2.gif  
;    
;-
pro hubAMW_list $
  ,name $
  ,Title=title $
  ,Value=value $ 
  ,List=list $
  ,AllowEmptySelection=allowEmptySelection $
  ,MultipleSelection=multipleSelection $
  ,Extract=extract $
  ,ShowButtonBar=showButtonBar $
  ,Tooltip=tooltip $
  ,XSize=xsize $
  ,YSize=ysize $
  ,Optional=optional $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWList( $
    name $
    ,Title=title $
    ,Value=value $
    ,List=list $
    ,AllowEmptySelection=allowEmptySelection $
    ,MultipleSelection=multipleSelection $
    ,Extract=extract $
    ,ShowButtonBar=showButtonBar $
    ,Tooltip=tooltip $
    ,XSize=xsize $
    ,YSize=ysize $
    ,Optional=optional $
    ,Advanced=advanced)
    
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end

