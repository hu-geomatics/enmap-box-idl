;+
; :Description:
;    This widget adds a  single or multiple text lines.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
; .. image:: _IDLDOC/label1.gif
;   
; :Params:
;    value: in, required, type=string[]
;      Use this argument to specify the text to be displayed.
;
; :Keywords:
; 
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Add some text lines::
;   
;      hubAMW_program 
;      hubAMW_label, ['This is a line of text.', 'This is another line of text.']
;      result = hubAMW_manage()
;      
;    .. image:: _IDLDOC/label1.gif
;
;-
pro hubAMW_label $
  ,value $
  ,Advanced=advanced
    
  on_error,2
  
  hubAMWObject = hubAMWLabel(value, Advanced=advanced)
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end