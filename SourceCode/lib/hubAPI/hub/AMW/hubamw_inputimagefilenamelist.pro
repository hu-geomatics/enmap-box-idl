;+
; :Description:
;    This widget allows the selection of a list of input image filenames.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash value is given by the selected image filename.
;
; :Keywords:
;    Title : in, optional, type=string, default='Input Image'
;      Use this keyword to specify the title of this widget.
;      
;    Value : in, optional, type=string
;      Use this keyword to specify a default image filename.
;
;    SelectBand : in, optional, type=boolean, default=0
;      Set this keyword to additionally select a specific band.
;      
;    SelectPixel : in, optional, type=boolean, default=0
;      Set this keyword to additionally select a specific pixel position.
;
;    BandValue : in, optional, type=int
;      Use this keyword to specify a default band.
;    
;    PixelValue : in, optional, type=int
;      Use this keyword to specify a default pixel position.
;    
;    AllowEmptyBand : in, optional, type=boolean, default=0
;      Set this keyword to allow an empty band selection.
;    
;    AllowEmptyPixel : in, optional, type=boolean, default=0
;      Set this keyword to allow an empty pixel location.
;    
;    Tooltip : in, optional, type=string[]
;      Use this keyword to specify a tooltip text. 
;      The tooltip is displayed when the mouse enters the widget title label.
;
;    Size : in, optional, type=int, default=500
;      Use this keyword to specify the widget width in pixel.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
;
; :Examples:
;    Create an input image widget:: 
;    
;      hubAMW_program, Title='auto-managed widget program'  
;      hubAMW_inputImageFilename, 'imageFilename'
;      result = hubAMW_manage()
;      print, result
;      
;    .. image:: _IDLDOC/inputImageFilename1.gif
;-
pro hubAMW_inputImageFilenameList $
  , name $
  , Title=title $
  , Value=value $
  , BandValue=bandValue $
  , PixelValue=pixelValue $
  , SpectralSubsetValue=spectralSubsetValue $
  , SelectBand=selectBand $
  , SelectPixel=selectPixel $
  , SelectSpectralSubset=selectSpectralSubset $
  , AllowEmptyImage=allowEmptyImage $
  , AllowEmptyBand=allowEmptyBand $
  , AllowEmptyPixel=allowEmptyPixel $
  , Tooltip=tooltip $
  , Size=size $
  , TSize=tSize $
  , Optional=optional $
  , Advanced=advanced
  
  hubAMWObject = hubAMWInputImageFilenameList( $
      name $
    , Title=title $
    , Value=value $
    , BandValue=bandValue $
    , PixelValue=pixelValue $
    , SpectralSubsetValue=spectralSubsetValue $    
    , SelectBand=selectBand $
    , SelectPixel=selectPixel $
    , SelectSpectralSubset=selectSpectralSubset $
    , AllowEmptyImage=allowEmptyImage $
    , AllowEmptyBand=allowEmptyBand $
    , AllowEmptyPixel=allowEmptyPixel $
    , Tooltip=tooltip $
    , Size=size $
    , TSize=tSize $
    , Optional=optional $
    , Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end

pro test_hubAMW_inputImageFilenameList
  hubAMW_program
  hubAMW_inputImageFilenameList, 'filenames'
  result = hubAMW_manage()
  print, result
end