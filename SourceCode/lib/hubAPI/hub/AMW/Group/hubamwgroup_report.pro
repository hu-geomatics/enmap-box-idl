;+
; :Description:
;    This widget group adds a some widgets for selecting some details for HTML reports. 
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;   .. image:: _IDLDOC/report.gif
;   
; :Params:
;    name: in, required, type=string
;      Hash key prefix for the different widget group widgets::
;      
;        name+'Show' for the checkbox widget "Show Report"
;        name+'Plots' for the checkbox widget "Add plots to Report"
;        name+'Filename' for output filename widget "Save Report"
;
; :Keywords:
;    ShowReportStyle: in, optional, type={0|1|2}, default=1
;      Use this keyword to specify the "Show Report" checkbox appearance::
;      
;        0 - hide the checkbox
;        1 - show the checkbox and check it (this is the default)
;        2 - show the checkbox and uncheck it
;      
;    AddPlotsStyle: in, optional, type={0|1|2}, default=0
;      Use this keyword to specify the "Add Plots to Report" checkbox appearance::
;      
;        0 - hide the checkbox (this is the default)
;        1 - show the checkbox and check it 
;        2 - show the checkbox and uncheck it
;        
;    SaveFileStyle: in, optional, type={0|1|2}, default=2
;      Use this keyword to specify the "Save Report" checkbox appearance::
;      
;        0 - hide the output filename widget
;        1 - show the output filename widget and check it 
;        2 - show the output filename widget and uncheck it (this is the default)
;        
;    SaveFileSize: in, optional, type=int
;      Use this keyword to specify the output filename widget width in pixels.
;
;    SaveFileValue: in, optional, type=filepath
;      Use this keyword to specify the report filename.
;
; :Examples:
;    Create a report widget group::
;      
;      hubAMW_program
;      hubAMWGroup_report, 'report'
;      result = hubAMW_manage()
;      print, result
;      
;  .. image:: _IDLDOC/report.gif
;  
;-
pro hubAMWGroup_report, name $
  , ShowReportStyle=showReportStyle $
  , AddPlotsStyle=addPlotsStyle $
  , SaveFileStyle=saveFileStyle $
  , SaveFileSize = saveFileSize $
  , SaveFileTSize = saveFileTSize $
  , SaveFileValue = saveFileValue

  hubAMW_subframe, /Row
  
  if ~isa(showReportStyle) then begin
    value = 1
  endif else begin
    case showReportStyle of
      0 :  hideShowReport = 1b
      1 :  value = 1
      2 :  value = 0
      else : ;do nothing
    endcase
  endelse
  if ~keyword_set(hideShowReport) then begin
    hubAMW_checkbox, name+'Show', Title=' Show Report', Value=value
  endif

  if ~isa(addPlotsStyle) then begin
    hideAddPlots = 1b
  endif else begin
    case addPlotsStyle of
      0 :  hideAddPlots = 1b
      1 :  value = 1
      2 :  value = 0
      else : ;do nothing
    endcase
  endelse
  if ~keyword_set(hideAddPlots) then begin
    hubAMW_checkbox, name+'Plots', Title='Add Plots to Report', Value=value
  endif

  hubAMW_subframe, /Column

  if ~isa(saveFileStyle) then begin
    optional = 2
  endif else begin
    case saveFileStyle of
      0 :  hideSaveFile = 2
      1 :  optional = 1
      2 :  optional = 2
      else : ;do nothing
    endcase
  endelse
  if ~keyword_set(hideSaveFile) then begin
    hubAMW_outputFilename, name+'Filename', Title='Save Report', Optional=optional, Extension='html', SIZE=saveFileSize, TSIZE=saveFileTSize, Value=saveFileValue
  endif

end

;+
; :Hidden:
;-
pro test_hubAMWGroup_report

  hubAMW_program
  hubAMWGroup_report, 'report'
  result = hubAMW_manage()
  print,result

end