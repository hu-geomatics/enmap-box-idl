;+
; :Description:
;    This function is used to start an auto-managed widget program that was created 
;    with the `hubAMW_program` procedure, it shows it on the screen, monitors for events, 
;    and returns the user input as a hash variable.
;    For details on how to build auto-managed widget programs also see `auto-managed-widget-program.idldoc`.
;
; :Returns:
;    In the case that the user clicked on the Accept button, a hash variable (result hash) is returned, that stores the user input. 
;    Each widget that was inserted into the auto-managed widget program, specifies a hash key/value pair stored in the result hash. The hash key is specified by the widgets name argument, the hash value is selected by the user.  
;    
;    You should always check if the user accepted (result['accept'] is equal to 1) or canceled (result['accept'] is equal to 0) the widget program. 
;
; :Keywords:
; 
;    ConsistencyCheckFunction : in, optional, type=string
;      A string containing the name of a user-defined consistency check function to be called by the auto-managed widget program. 
;      A Consistency check function has one required input argument called resultHash and one required output keyword called Message.
;      Optionally, the function can have an additional keyword called UserInformation (see `UserInformation`) to provide some extra information.
;      The return value is boolean (0b or 1b). 
; 
;      For details on how to define a consistency check function see `check-consistency.idldoc`.
;
;    Structure : in, optional, type=boolean, default=0
;      Set this keyword to return a structure variable instead of a hash variable. 
;      Note that returning the result as a structure has the same disadvantaged that are mentioned for the `hash::ToStruct` methode. 
;      IDL Help says: "any special characters in the string [the hash key] will be converted to underscores to produce a valid IDL tag name. If a value is undefined (!NULL) it will also be skipped."
;
;    Dictionary : in, optional, type=boolean, default=0
;      Set this keyword to return a dictionary variable instead of a hash variable.
;      Note that returning the result as a structure has the same disadvantaged that are mentioned for the `hash::ToStruct` methode.
;      IDL Help says: "any special characters in the string [the hash key] will be converted to underscores to produce a valid IDL tag name. If a value is undefined (!NULL) it will also be skipped."
;
;    Flat : in, optional, type=boolean, default=0
;      Set this keyword to return a "flat" result hash that contains no other hashes. 
;      For each hash contained in the result hash, all of it's key-value pairs are added directly to the result hash.
;      Example:
;      
;        
;
;    UserInformation : in, optional, type=Hash
;      Hash to provide further information to the consistency check function that are not part of the regular result hash.
;      
;    ButtonNameAccept : in, optional, type=string
;      Use this keyword to specify the name of the Accept button.
;      Specify the empty string '' to skip the button.
;       
;    ButtonNameCancel : in, optional, type=string
;      Use this keyword to specify the name of the Cancel button.
;      Specify the empty string '' to skip the button.
;      
;    ButtonNameAdvanced : in, optional, type=string
;      Use this keyword to specify the name of the Advanced button.
;
;-
function hubAMW_manage $
  , ConsistencyCheckFunction=consistencyCheckFunction $
  , Structure=structure $
  , Dictionary=dictionary $
  , Flat=flat $
  , UserInformation=userInformation $
  , HelpFile=helpFile $
  , ButtonNameAccept=buttonNameAccept $
  , ButtonNameCancel=buttonNameCancel $
  , ButtonNameAdvanced=buttonNameAdvanced

  hubAMWProgram = hubAMW_getProgram()
  result = hubAMWProgram.manage(UserInformation=userInformation, Structure=structure, Dictionary=dictionary, Flat=flat, ConsistencyCheckFunction=consistencyCheckFunction, HelpFile=helpFile $
    , ButtonNameAccept=buttonNameAccept, ButtonNameCancel=buttonNameCancel, ButtonNameAdvanced=buttonNameAdvanced)
  hubAMW_deleteProgram
  return,result
end
