;+
; :Hidden:
;-

function hubAMWText::init $
  ,name $
  ,Title=title $
  ,Value=value $
  ,Tooltip=tooltip $
  ,XSize=xsize $
  ,YSize=ysize $
  ,Editable=editable $
  ,Wrap=wrap $
  ,Scroll=scroll $
  ,Optional=optional $
  ,Advanced=advanced
  

  !NULL=self->hubAMWObject::init(name,Optional=optional,Advanced=advanced)

  self.setKeywordDefault,'xsize', 300
  self.setKeywordDefault,'ysize', 150
  self.setKeywordDefault,'editable', 1b
  self.setKeywordDefault,'wrap', 0b
  self.setKeywordDefault,'scroll', 0b
  
  return,1b
end

function hubAMWText::getValue
  widget_control, self.getWidgetID('text'), GET_VALUE=value
  return,value
end

pro hubAMWText::setValue, value
  widget_control, self.getWidgetID('text'), SET_VALUE=value
end

pro hubAMWText::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase
  
  base = widget_base(self.base,/COLUMN,XPAD=0,YPAD=0)
  if isa(self.getKeyword('title')) then begin
    labelID = widget_label(base,VALUE=self.getKeyword('title'),/ALIGN_LEFT $
      , UVALUE=self, /TRACKING_EVENTS)
    (self.tooltips)[labelID] = self.getKeyword('tooltip')
  endif

  self.setWidgetID, /Optional, 'text',widget_text(base,VALUE=self.getKeyword('value') $
    ,SCR_XSIZE=self.getKeyword('xsize'),SCR_YSIZE=self.getKeyword('ysize'), YSIZE=2  $
    ,EDITABLE=self.getKeyword('editable'), WRAP=self.getKeyword('wrap'), SCROLL=self.getKeyword('scroll'))

end

pro hubAMWText__define

  define = {hubAMWText $
    ,inherits hubAMWObject $
  }
  
end
