;+
; :Hidden:
;-

function hubAMWInputSampleSet::init $
  , name $
  , Title=title $
  , Value=value $
  , ReferenceValue=referenceValue $
  , Classification=classification $
  , Regression=regression $
  , Density=density $
  , Masking=masking $
  , FeatureImageOnly=featureImageOnly $
  , FeatureIsClassification=featureIsClassification $
  , ReferenceTitle=referenceTitle $
  , ReferenceOptional=referenceOptional $
  , SelectBand=selectBand $
  , SelectPixel=selectPixel $
  , SelectSpectralSubset=selectSpectralSubset $
  , AllowEmptyImage=allowEmptyImage $
  , AllowEmptyBand=allowEmptyBand $
  , AllowEmptyPixel=allowEmptyPixel $
  , BandValue=bandValue $
  , PixelValue=pixelValue $
  , SpectralSubsetValue=spectralSubsetValue $    
  , Size=size $
  , TSize=tSize $
  , Optional=optional $
  , Advanced=advanced $
  , Tooltip=tooltip $
  , ReferenceTooltip=referenceTooltip

  extension = !null
  !NULL = self->hubAMWInputFilename::init(name, Title=title, Extension=extension, Size=size, Optional=optional,Advanced=advanced)
  !NULL = self->hubAMWObject::init(name, Optional=optional,Advanced=advanced)

  self.setKeywordDefaults
  
  if total([self.getKeyword('masking'), self.getKeyword('density'), self.getKeyword('classification'), self.getKeyword('regression'), self.getKeyword('featureImageOnly')]) ne 1 then begin
    message,'Conflicting keywords: Classification, Regression, Density, Masking.'
  endif
  
  self.saveInformation
  return,1b
end

pro hubAMWInputSampleSet::setKeywordDefaults

  self.setKeywordDefault,'title','Input Image'
  self.setKeywordDefault,'value',' '
  self.setKeywordDefault,'size', 500
  
  self.setKeywordDefault,'classification', 0
  self.setKeywordDefault,'regression', 0
  self.setKeywordDefault,'density', 0
  self.setKeywordDefault,'masking', 0
  self.setKeywordDefault,'featureImageOnly', 0
  self.setKeywordDefault,'referenceOptional', 0
  self.setKeywordDefault,'selectBand', 0
  self.setKeywordDefault,'selectPixel', 0
  self.setKeywordDefault,'selectSpectralSubset', 0
  self.setKeywordDefault,'allowEmptyImage', 0
  self.setKeywordDefault,'allowEmptyBand', 0
  self.setKeywordDefault,'allowEmptyPixel', 0
  self.setKeywordDefault,'bandValue', !null
  self.setKeywordDefault,'pixelValue', !null
  self.setKeywordDefault,'spectralSubsetValue', !null
  
  if self.getKeyword('masking') then begin
    referenceTitle = 'Mask'
  endif else begin
    referenceTitle = 'Reference Areas'
  endelse
  
  self.setKeywordDefault,'referenceTitle',referenceTitle

end

function hubAMWInputSampleSet::getFileList

  fileList = hubProEnvHelper.getFileNames(/Image,/SpectralLibrary)
  return, fileList

end

pro hubAMWInputSampleSet::saveInformation

  self->hubAMWInputFilename::saveInformation

  if self.getKeyword('masking') then textNoReference = '<no mask selected>'
  if self.getKeyword('classification') then textNoReference = '<no classification reference selected>'
  if self.getKeyword('regression') then textNoReference = '<no regression reference selected>'
  if self.getKeyword('density') then textNoReference = '<no density reference selected>'
  self.setInformation,'textNoReference',textNoReference
  self.setInformation,'queryReference', ~self.getKeyword('featureImageOnly')

  ; create hubIOImgInputImage objects for meta information access
  
  fileList = self.getInformation('fileList')
  
  self.setInformation, 'hubIOImgInputImages', hash()

  foreach filename, fileList, i do begin
    if strtrim(fileList[i], 2) ne '' then begin

      ; fix a bug that occurs when a default filename in a dialog doesn't exist anymore 
      Catch, errorStatus
      if (errorStatus ne 0) then begin
        Catch, /CANCEL
        fileList[i] = ''
        continue
      endif

      hubIOImgInputImage = hubIOImgInputImage(fileList[i])
      Catch, /CANCEL
      (self->getInformation('hubIOImgInputImages'))[fileList[i]] = hubIOImgInputImage
    endif
  endforeach

;-----
;  fileListBasename = file_basename(self.filterReferenceFileList())
;  self.setInformation,'textCombobox',[fileListBasename,self.getInformation('textOpenFile')]
  self.setInformation,'textComboboxReference',textNoReference


end

;  featureFilelist = self.filterFeatureFilelist()
pro hubAMWInputSampleSet::defineWidgetHierarchy $
  , parentBase

  self->hubAMWObject::defineWidgetHierarchy, parentBase

  base = widget_base(self.base, /COLUMN, XPAD=0, YPAD=0)

  ; image combobox

  subbase = widget_base(base, /ROW, XPAD=0, YPAD=0, /BASE_ALIGN_CENTER)
  baseResizeable = widget_base(subbase, /ROW, XPAD=0, YPAD=0) & resizeableList = list(baseResizeable)

  titleBase = widget_base(baseResizeable, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')
    
    
  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif
  
  self.setWidgetID, /Optional, 'combobox', widget_combobox(subbase $
    , VALUE=self.getInformation('textCombobox') $
    , SCR_XSIZE=self.getKeyword('size'), UVALUE=self)
  
  if self.getKeyword('selectBand') then begin 
    self.setWidgetID, /Optional, 'band', widget_button(subbase, VALUE=' ', UVALUE=self, SCR_XSIZE=150)
  endif
  if self.getKeyword('selectPixel') then begin 
    self.setWidgetID, /Optional, 'pixel', widget_button(subbase, VALUE=' ', UVALUE=self, SCR_XSIZE=150)
  endif
  if self.getKeyword('selectSpectralSubset') then begin 
    self.setWidgetID, /Optional, 'spectralSubset', widget_button(subbase, VALUE=' ', UVALUE=self, SCR_XSIZE=150)
  endif
     
  ; reference combobox

  if self.getInformation('queryReference') then begin
  
    subbase = widget_base(base, /ROW, XPAD=0, YPAD=0, /BASE_ALIGN_CENTER)
    baseResizeable = widget_base(subbase, /ROW, XPAD=0, YPAD=0) & resizeableList.add, baseResizeable
    labelID = widget_label(baseResizeable, VALUE=(self.getKeyword('referenceTitle'))[0] $
      , UVALUE=self, /TRACKING_EVENTS)

    (self.tooltips)[labelID] = self.getKeyword('referenceTooltip')

    self.setWidgetID, /Optional, 'comboboxReference', widget_combobox(subbase $
      ,SCR_XSIZE=self.getKeyword('size'), UVALUE=self)
    
  endif else begin
    self.setWidgetID, 'comboboxReference', -1
    self.setWidgetID, 'textReference', -1
  endelse

  ; match size of label bases
  
  max_scr_xsize = 0
  foreach base, resizeableList do max_scr_xsize >= (widget_info(/GEOMETRY, base)).scr_xsize
  foreach base, resizeableList do widget_control, base, SCR_XSIZE=max_scr_xsize

  ; select first image in list
  
  self.setCombobox,0
  if isa(self.getKeyword('referenceValue')) then begin
    self.setComboboxReference,0
  endif
  
  ; select band
  if self.getKeyword('selectBand') then begin
    self.setBand, self.getKeyword('bandValue')
  endif

  ; select pixel
  if self.getKeyword('selectPixel') then begin
    self.setPixel, self.getKeyword('pixelValue')
  endif
  
  ; select spectral subset
  if self.getKeyword('selectSpectralSubset') then begin
    self.setSpectralSubset, self.getKeyword('spectralSubsetValue')
  endif
  
  self.setSubsettingButtonSensitivity

end

pro hubAMWInputSampleSet::setCombobox $
  , index
  
  ; select image file 
  
  widget_control, self.getWidgetID('combobox'), SET_COMBOBOX_SELECT=index, /CLEAR_EVENTS
  
  self.refreshReference
  
end

pro hubAMWInputSampleSet::setComboboxReference $
  , index
  
  ; select reference image file 
  
  widget_control, self.getWidgetID('comboboxReference'), SET_COMBOBOX_SELECT=index, /CLEAR_EVENTS
  
end

pro hubAMWInputSampleSet::setBand $
  , position
  
  if self.getKeyword('selectBand') then begin
    value = isa(position) ? 'Band '+strtrim(position+1, 2) : 'Select Band'  
    widget_control, self.getWidgetID('band'), SET_VALUE=value
    self.setInformation, 'selectedBand', position
  endif

end

pro hubAMWInputSampleSet::setPixel $
  , position
  
  if self.getKeyword('selectPixel') then begin
    value = isa(position) ? 'Pixel ('+strtrim(position[0]+1, 2)+','+strtrim(position[1]+1, 2)+')' : 'Select Pixel'  
    widget_control, self.getWidgetID('pixel'), SET_VALUE=value
    self.setInformation, 'selectedPixel', position
  endif
 
end

pro hubAMWInputSampleSet::setSpectralSubset $
  , positions

  if self.getKeyword('selectSpectralSubset') then begin  
    value = isa(positions) ? strtrim(n_elements(positions), 2)+' Bands Selected' : 'Select Bands'  
    widget_control, self.getWidgetID('spectralSubset'), SET_VALUE=value
    self.setInformation, 'selectedSpectralSubset', positions
  endif
  
end

function hubAMWInputSampleSet::getBand
  return, self.getInformation('selectedBand')  
end

function hubAMWInputSampleSet::getPixel
  return, self.getInformation('selectedPixel')  
end

function hubAMWInputSampleSet::getSpectralSubset
  return, self.getInformation('selectedSpectralSubset')  
end

pro hubAMWInputSampleSet::setSubsettingButtonSensitivity
  
  sensitive = self.hubAMWInputFilename::getValue() ne ''
  if isa(self.getWidgetID('pixel')) then begin
    widget_control, self.getWidgetID('pixel'), SENSITIVE=sensitive
  endif
  if isa(self.getWidgetID('band')) then begin
    widget_control, self.getWidgetID('band'), SENSITIVE=sensitive
  endif
  if isa(self.getWidgetID('spectralSubset')) then begin
    widget_control, self.getWidgetID('spectralSubset'), SENSITIVE=sensitive
  endif
end

pro hubAMWInputSampleSet::refreshReference

  if self.getInformation('queryReference') then begin 
  
    ; filter reference files

    referenceFileList = self.filterReferenceFileList()
    
    if isa(self.getKeyword('referenceValue')) then begin
      referenceFileList = [self.getKeyword('referenceValue'), referenceFileList]
    endif  
    
    ; get file basenames for visualization

    if isa(referenceFileList) then begin
      referenceFileListBasename = file_basename(referenceFileList)
    endif else begin
      referenceFileListBasename = !null
    endelse

    ; save reference filelist
    
    self.setInformation,'fileListReference',referenceFileList
    
    ; set reference combobox list

    self.setInformation,'textComboboxReference',[referenceFileListBasename,self.getInformation('textNoReference')]
    widget_control, self.getWidgetID('comboboxReference'), SET_VALUE=self.getInformation('textComboboxReference'), /CLEAR_EVENTS

    ; select <no ref selected> element or reference default

    if isa(self.getKeyword('referenceValue')) then begin
      index = 0
    endif else begin
      index = n_elements(referenceFileList)
    endelse  
    widget_control, self.getWidgetID('comboboxReference'), SET_COMBOBOX_SELECT=index, /CLEAR_EVENTS

  endif

end

function hubAMWInputSampleSet::filterReferenceFileList

  fileList = self.getInformation('fileList')
  referenceFileList = []

  featureFilename = self.hubAMWInputFilename::getValue()

  if featureFilename ne '' then begin

    addToListIndex = list()

    featureImage = (self->getInformation('hubIOImgInputImages'))[featureFilename]

    foreach labelFilename, fileList, i do begin

; IDL bug under Linux, must replace "labelFilename" with "fileList[i]"
      labelFilename = fileList[i]
      
      if strtrim(labelFilename,2) eq '' then begin
       
        addToList = 0b

      endif else begin 

        labelImage = (self->getInformation('hubIOImgInputImages'))[labelFilename]    

        addToList = 1b
        
        if self.getKeyword('classification') then begin
          addToList and= labelImage.isClassification()
        endif
        if self.getKeyword('regression') then begin
          addToList and= labelImage.isRegression()
        endif
        if self.getKeyword('density') then begin
          addToList and= labelImage.isMask()
        endif
        if self.getKeyword('masking') then begin
          addToList and= labelImage.isMask()
        endif
        
      endelse
      
      if addToList then begin
        addToListIndex.add,i
      endif
      
    endforeach

    ; add files to list

    referenceFileList = fileList[addToListIndex.ToArray()]
    
  endif
    
  return, referenceFileList

end

function hubAMWInputSampleSet::filterFileList

  fileList = self.getInformation('fileList')
  featureFileList = []

  addToListIndex = list()
  foreach filename, fileList, i do begin
  
    if strcompress(/Remove_all, filename) eq '' then continue
    help,filename

    featureImage = (self->getInformation('hubIOImgInputImages'))[filename]    
    addToList = 1b
    if self.getKeyword('featureIsClassification') then begin    
      addToList and= featureImage.isClassification()
    endif
    if addToList then begin
      addToListIndex.add,i
    endif
      
  endforeach

  ; add files to list

  featureFileList = fileList[addToListIndex.ToArray()]
    
  return, featureFileList

end

pro hubAMWInputSampleSet::handleEvent $
  , event

;  self.hubAMWInputFilename::handleEvent, event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return
  
  if event.id eq self.getWidgetID('combobox') then begin
  
    case event.str of
      
      self->getInformation('textOpenFile') : begin
        self.handleEventOpenFile,event
      end
      
      else : begin
        self.setCombobox,event.index
      end
  
    endcase
    
    self.setBand, !null
    self.setPixel, !null
    self.setSpectralSubset, !null
    self.setSubsettingButtonSensitivity
    
  endif
  
  if event.id eq self.getWidgetID('comboboxReference') then begin
    self.setComboboxReference,event.index
  endif

  if event.id eq self.getWidgetID('band') then begin
    imageFilename = self.hubAMWInputFilename::getValue()
    if imageFilename eq '' then return
    inputImage = hubIOImgInputImage(imageFilename)
    bandNames = inputImage.getMeta('band names')
    if isa(inputImage.getMeta('wavelength')) then begin
      bandNames += ' ('+strtrim(inputImage.getMeta('wavelength'), 2)+')'
    endif

    hubAMW_program, TITLE='Select Band', self.base
    hubAMW_list, 'selection', List=bandNames, XSIZE=500, YSIZE=500, Value=self.getBand()
    result = hubAMW_manage(/Structure)
    if result.accept then begin
      self.setBand, result.selection
    endif else begin
      self.setBand, !null
    endelse
  endif

  if event.id eq self.getWidgetID('pixel') then begin
    imageFilename = self.hubAMWInputFilename::getValue()
    if imageFilename eq '' then return
    inputImage = hubIOImgInputImage(imageFilename)
    hubAMW_program, TITLE='Select Pixel', self.base
;    hubAMW_subframe, /ROW
    hubAMW_parameter, 'sample', Size=5, Title='Sample', /Integer, IsGE=1, IsLE=inputImage.getMeta('samples'), Value=isa(self.getPixel()) ? (self.getPixel())[0]+1 : !null, UNIT='/'+strtrim(inputImage.getMeta('samples'),2)
    hubAMW_parameter, 'line',   Size=5, Title='Line  ',   /Integer, IsGE=1, IsLE=inputImage.getMeta('lines'),   Value=isa(self.getPixel()) ? (self.getPixel())[1]+1 : !null, UNIT='/'+strtrim(inputImage.getMeta('lines'),2)
    result = hubAMW_manage(/Structure)
    if result.accept then begin
      result.sample--
      result.line--
      self.setPixel, [result.sample, result.line]
    endif else begin
      self.setPixel, !null
    endelse
  endif
  
  if event.id eq self.getWidgetID('spectralSubset') then begin
    imageFilename = self.hubAMWInputFilename::getValue()
    if imageFilename eq '' then return
    inputImage = hubIOImgInputImage(imageFilename)
    bandNames = inputImage.getMeta('band names')
    if isa(inputImage.getMeta('wavelength')) then begin
      bandNames += ' ('+strtrim(inputImage.getMeta('wavelength'), 2)+')'
    endif

    hubAMW_program, TITLE='Select Spectral Subset', self.base
    hubAMW_list, 'selection', List=bandNames, /MultipleSelection, /ShowButtonBar, XSIZE=500, YSIZE=500, Value=self.getSpectralSubset()
    result = hubAMW_manage(/Structure)
    if result.accept then begin
      self.setSpectralSubset, result.selection
    endif else begin
      self.setSpectralSubset, !null
    endelse
  endif

end

function hubAMWInputSampleSet::getValue

  ; get image filename

  imageFilename = self.hubAMWInputFilename::getValue()

  ; get reference filename

  if self.getInformation('queryReference') then begin

    ; get reference image filebasename

    referenceText = widget_info(self.getWidgetID('comboboxReference'), /COMBOBOX_GETTEXT)

    ; get reference image filename

    if referenceText eq self.getInformation('textNoReference') then begin

;      referenceImageFilename = ''
      referenceImageFilename = !null

    endif else begin

      ; find reference combobox index

      index = (where(/NULL,self.getInformation('textComboboxReference') eq referenceText))[0]
      
      ; get reference fileName
      
      referenceImageFilename = (self.getInformation('fileListReference'))[index]
  
    endelse
      
  endif
  
  if self.getKeyword('selectBand') then begin 
    band = self.getBand()    
  endif
  
  if self.getKeyword('selectPixel') then begin 
    pixel = self.getPixel()    
  endif
    
  if self.getKeyword('selectSpectralSubset') then begin 
    spectralSubset = self.getSpectralSubset()    
  endif

  value = hash()
  value['featureFilename'] = imageFilename
  value['labelFilename'] = referenceImageFilename
  if self.getKeyword('selectBand') then begin 
    value['band'] = band   
  endif
  if self.getKeyword('selectPixel') then begin 
    value['pixel'] = pixel   
  endif
  if self.getKeyword('selectSpectralSubset') then begin 
    value['spectralSubset'] = spectralSubset   
  endif
  
  return,value

end
 
function hubAMWInputSampleSet::checkConsistenty $
  ,Message=message
  value = self.getValue()

  ; check if image filename is empty

  if value['featureFilename'] eq '' then begin
    message = 'image filename is empty'
    return,0b
  endif

  ; check if image file exists
  
  if ~ (hubIOHelper()).fileReadable(value['featureFilename']) then begin
    message = 'image file does not exist'
    return,0b
  endif
  
  if self.getInformation('queryReference') then begin
  
    ; check if reference image filename is empty
    
    referenceSelected = isa(value['labelFilename'])
    if ~referenceSelected && ~self.getkeyword('referenceOptional') then begin
      message = 'reference image filename is empty'
      return,0b
    endif

    ; check if reference image file exists
    
    if ~ (hubIOHelper()).fileReadable(value['labelFilename']) && ~self.getkeyword('referenceOptional') then begin
      message = 'reference image file does not exist'
      return,0b
    endif

    ; check if image and reference image match spatially
    if referenceSelected then begin    
      spatialSize = (hubIOImgInputImage(value['featureFilename'])).getSpatialSize()
      correctSpatialSize = (hubIOImgInputImage(value['labelFilename'])).isCorrectSpatialSize(spatialSize)
      if ~correctSpatialSize then begin
        message = 'image and reference image spatial size do not match'
        return,0b
      endif
    endif

    if self.getKeyword('selectBand') && ~isa(value['band']) && ~self.getKeyword('allowEmptyBand') then begin 
      message = 'no band selected'
      return,0b
    endif
    
    if self.getKeyword('selectPixel') && ~isa(value['pixel']) && ~self.getKeyword('allowEmptyPixel') then begin 
      message = 'no pixel selected'
      return,0b
    endif
  
  endif
  
  message = [] 
  return,1b
  
end

pro hubAMWInputSampleSet__define

  define = {hubAMWInputSampleSet $
    ,inherits hubAMWInputFilename $
  }
  
end