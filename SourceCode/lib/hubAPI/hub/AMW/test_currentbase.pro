pro test_currentBase

  hubAMW_program
  hubAMW_frame, TITLE='Frame'

  ; create widget base hierarchy
  
  currentBase = hubAMW_getCurrentBase()
  baseLevel1 = widget_base(currentBase, /ROW)
  baseLevel11 = widget_base(baseLevel1, /COLUMN, XPAD=0, YPAD=0)
  baseLevel12 = widget_base(baseLevel1, /COLUMN, XPAD=0, YPAD=0)
  baseLevel121 = widget_base(baseLevel12, /COLUMN, XPAD=0, YPAD=0)
  baseLevel122 = widget_base(baseLevel12, /COLUMN, XPAD=0, YPAD=0)

  ; insert AMW widgets

  hubAMW_setCurrentBase, baseLevel11
  hubAMW_label, 'level 1.1'
  hubAMW_setCurrentBase, baseLevel121
  hubAMW_label, 'level 1.2.1'
  hubAMW_setCurrentBase, baseLevel122
  hubAMW_label, 'level 1.2.2'
  result = hubAMW_manage()

end