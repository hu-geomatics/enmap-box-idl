;+
; :Description:
;    This widget adds a checkbox.
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;
;   .. image:: _IDLDOC/checkbox1.gif
;   
; :Params:
;    name: in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash 
;      (see `hubAMW_manage` for details).
;      The hash value is 0 for an unchecked and 1 for a checked checkbox.
;      
; :Keywords:
;    Title: in, optional, type=string, default='*checkbox title*'
;      Use this keyword to specify the widget title.
;     
;    Value: in, optional, type={0 | 1}, default=0
;      Use this keyword to specify the widget default value.
;      Set Value=1 for a checked and Value=0 for an unchecked checkbox.
; 
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create a checkbox, that is checked by default::
;      
;      hubAMW_program
;      hubAMW_checkbox, 'checkbox_value', Value=1
;      result = hubAMW_manage()
;      print, result
;      
;  .. image:: _IDLDOC/checkbox1.gif
;  
;-
pro hubAMW_checkbox $
  ,name $
  ,Title=title $
  ,Value=value $
  ,Advanced=advanced
  
  on_error,2
  
  hubAMWObject = hubAMWCheckbox( $
   name $
  ,Title=title $
  ,Value=value $
  ,Advanced=advanced)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end