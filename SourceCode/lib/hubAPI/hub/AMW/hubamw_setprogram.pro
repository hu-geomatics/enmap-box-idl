;+
; :Hidden:
;-

pro hubAMW_setProgram $
  ,hubAMWProgram

  common hubAMW_programs,hubAMWPrograms
  if ~isa(hubAMWPrograms) then begin
    hubAMWPrograms = list()
  endif
  
  hubAMWPrograms.add,hubAMWProgram

end