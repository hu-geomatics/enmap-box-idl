;+
; :Description:
;    This procedure is used to insert a new tab page into a tab frame. 
;    After inserting a tab page, you can insert subframes (see `hubAMW_subframe`) and widgets into it. 
;    Tab frames have a column based layout, use subframes to lay out widgets in a row.
;    For details on how to build auto-managed widget programs also see `auto-managed-widget-program.idldoc`.
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify a title for the tab frame.
;
;-
pro hubAMW_tabPage $
  ,Title=title
    
  on_error,2
    
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertTabPage, Title=title
    
end