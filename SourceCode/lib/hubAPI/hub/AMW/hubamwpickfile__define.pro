;+
; :Hidden:
;-

function hubAMWPickfile::init $
  ,name $
  ,Title=title $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced $
  ,_EXTRA=_extra
  
  !NULL=self->hubAMWObject::init(name, Optional=optional,Advanced=advanced)

  self.setKeywordDefault, 'title', 'Input File'
  self.setKeywordDefault, 'size', 500
  self.pickfileResult = ptr_new(!null)
  
  return,1b
end

function hubAMWPickfile::getValue

  value = *(self.pickfileResult)
  return,value

end

pro hubAMWPickfile::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base, /ROW, XPAD=0, YPAD=0)
  
  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT   $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')
    
  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif
  
  self.setWidgetID,'text',widget_text(base,VALUE='',SCR_XSIZE=self.getKeyword('size'),/NO_NEWLINE)
  self.setWidgetID,'button',widget_button(base,VALUE='...', UVALUE=self)
  
end

pro hubAMWPickfile::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return
  
  ; get directory name
  
  filename = dialog_pickfile(_EXTRA=self.getKeyword('_extra'))
  
  if filename[0] ne '' then begin
    self.pickfileResult = ptr_new(filename)
    filenameString = strjoin(file_basename(filename), PATH_SEP(/SEARCH_PATH)+' ')
    widget_control,self.getWidgetID('text'), SET_VALUE=filenameString
    cd, file_dirname(filename[0]) 
  endif 
 
end

function hubAMWPickfile::checkConsistenty $
  ,Message=message
  
  value = self.getValue()

  ; check if directory name is empty
  
  if ~isa(value) then begin
    message = 'empty selection'
    return,0b
  endif
  
  message = [] 
  return,1b
  
end

pro hubAMWPickfile__define

  define = {hubAMWPickfile $
    ,inherits hubAMWObject $
    ,pickfileResult : ptr_new() $
  }
  
end