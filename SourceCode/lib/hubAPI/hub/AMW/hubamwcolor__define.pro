;+
; :Hidden:
;-
function hubAMWColor::init $
  ,name $
  ,Title=title $ 
  ,SuffixTitle=suffixTitle $   
  ,Value=value $ 
  ,Editable=editable $
  ,Tooltip=tooltip $  
  ,XSize=xSize $
  ,YSize=ySize $  
  ,Optional=optional $
  ,Advanced=advanced

  !NULL=self->hubAMWObject::init(name, Optional=optional, Advanced=advanced)

  self.setKeywordDefault,'xSize', 15
  self.setKeywordDefault,'ySize', 15
  self.setKeywordDefault,'editable', 1b
  
  if ~isa(suffixTitle) then begin
    self.setKeywordDefault,'title', 'Select Color'
  endif
    
  self.colorSelected = isa(value)
  if self.colorSelected then begin
    self.color = value
  endif

  return,1b
end

function hubAMWColor::getValue

  if self.colorSelected then begin
    widget_control,self.getWidgetID('draw'), GET_VALUE=windowID
    wset, windowID
    value = reform(tvrd(0,0,1,1, TRUE=3))
  endif else begin
    value = !null
  endelse
  
  return,value

end

pro hubAMWColor::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase
  base = widget_base(self.base,/ROW,XPAD=0,YPAD=0)

  if isa(self.getKeyword('title')) then begin
    labelID = widget_label(base, VALUE=self.getKeyword('title')+' ',/ALIGN_CENTER $
      , UVALUE=self, /TRACKING_EVENTS)
    (self.tooltips)[labelID] = self.getKeyword('tooltip')
  endif

  self.setWidgetID, /Optional, 'draw', widget_draw(base, SCR_XSIZE=self.getKeyword('xSize'),SCR_YSIZE=self.getKeyword('ySize') $
    , /BUTTON_EVENTS, UVALUE=self, RETAIN=2, SENSITIVE=self.getKeyword('editable'))

  if isa(self.getKeyword('suffixTitle')) then begin
    labelID = widget_label(base, VALUE=' '+self.getKeyword('suffixTitle'),/ALIGN_CENTER $
      , UVALUE=self, /TRACKING_EVENTS)
    (self.tooltips)[labelID] = self.getKeyword('tooltip')
  endif

end

function hubAMWColor::checkConsistenty $
  ,Message=message
  
  if ~self.colorSelected then begin
    message = 'no color selected'
    return, 0b
  endif
  return,1b
  
end

pro hubAMWColor::handleEvent, event
  
  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return
  
  case event.id of 
    self.getWidgetID('draw', /NoNull) : begin
      if event.press eq 1 then begin
        self.handleEvent_draw, event
      endif
      
    end

    self.getWidgetID('rgbSlider', /NoNull) : begin
      self.handleEvent_rgb, event
    end
     
    self.getWidgetID('accept', /NoNull) : begin
      self.handleEvent_accept, event
    end
      
    self.getWidgetID('cancel', /NoNull) : begin
      self.handleEvent_cancel, event
    end
      
    else : begin
      if widget_info(event.id, /UNAME) eq 'colorButton' then begin
        color = (self.colorButtons)[event.id]
        widget_control, self.getWidgetID('rgbSlider'), SET_VALUE=color
        self.color = color
      endif
    endelse
      
  endcase
  self.draw

end

pro hubAMWColor::handleEvent_draw, event

  (self.restore) = hash()
  (self.restore)['colorSelected'] = self.colorSelected
  (self.restore)['color'] = self.color
  self.setWidgetID, 'rgbTlb', widget_base(TITLE='Color Selection',/Modal, Group_leader=self.base, /COLUMN)
  dummy = widget_base(self.getWidgetID('rgbTlb'),/COLUMN,/FRAME)
  self.setWidgetID, 'rgbSlider', cw_rgbslider(dummy, VALUE=self.color, GRAPHICS_LEVEL=2, LENGTH=350, UVALUE=self)
  !null = widget_label(dummy, Value='Predefined Colors', /ALIGN_LEFT)
  colorsBase = widget_base(dummy,Column=10, XPAD=0,YPAD=0,SPACE=1)
 
  dummy = widget_base(self.getWidgetID('rgbTlb'),/ROW,XPAD=0,YPAD=0,SPACE=1)
  buttonBase = widget_base(dummy,/ROW,XPAD=0,YPAD=0,SPACE=0)
  self.setWidgetID, 'accept', widget_button(buttonBase,VALUE='Accept',UVALUE=self)
  self.setWidgetID, 'cancel', widget_button(buttonBase,VALUE='Cancel',UVALUE=self)
  
  ; predefined envi colors
  allColors = [ $
       0,   0,   0,$
     255, 255, 255,$
     255,   0,   0,$
       0, 255,   0,$
       0,   0, 255,$
     255, 255,   0,$
       0, 255, 255,$
     255,   0, 255,$
     176,  48,  96,$
      46, 139,  87,$
     160,  32, 240,$
     255, 127,  80,$
     127, 255, 212,$
     218, 112, 214,$
     160,  82,  45,$
     127, 255,   0,$
     216, 191, 216,$
     238,   0,   0,$
     205,   0,   0,$
     139,   0,   0,$
       0, 238,   0,$
       0, 205,   0,$
       0, 139,   0,$
       0,   0, 238,$
       0,   0, 205,$
       0,   0, 139,$
     238, 238,   0,$
     205, 205,   0,$
     139, 139,   0,$
       0, 238, 238,$
       0, 205, 205,$
       0, 139, 139,$
     238,   0, 238,$
     205,   0, 205,$
     139,   0, 139,$
     238,  48, 167,$
     205,  41, 144,$
     139,  28,  98,$
     145,  44, 238,$
     125,  38, 205,$
      85,  26, 139,$
     255, 165,   0,$
     238, 154,   0,$
     205, 133,   0,$
     139,  90,   0,$
     238, 121,  66,$
     205, 104,  57,$
     139,  71,  38,$
     238, 210, 238,$
     205, 181, 205]

  !null = reform(allColors,3,n_elements(allColors)/3,/OVERWRITE)
  
  self.colorButtons = hash()
  for i=0,n_elements(allColors[0,*])-1 do begin
    color = allColors[*,i]
    image = byte(rebin(reform(color,1,1,3),20,20,3))
    buttonID = widget_button(colorsBase, Value=image, /BITMAP, FLAT=0, FRAME=0, UVALUE=self, UNAME='colorButton', Mask=0)
    (self.colorButtons)[buttonID] = color
  endfor
  
  widget_control, self.getWidgetID('rgbTlb'), /REALIZE
  xmanager, 'hubAMWColor', self.getWidgetID('rgbTlb'), /NO_BLOCK, EVENT_HANDLER='hubObjCallback_event'

end

pro hubAMWColor::handleEvent_rgb, event

  self.colorSelected = 1b
  self.color = [event.r,event.g,event.b]

end

pro hubAMWColor::handleEvent_accept, event
  self.colorSelected = 1b
  widget_control, self.getWidgetID('rgbTlb'), /DESTROY
end

pro hubAMWColor::handleEvent_cancel, event
  self.colorSelected = (self.restore)['colorSelected']
  self.color = (self.restore)['color']
  widget_control, self.getWidgetID('rgbTlb'), /DESTROY
end

pro hubAMWColor::draw

  widget_control,self.getWidgetID('draw'), GET_VALUE=windowID
  wset, windowID
  
  if ~self.colorSelected then begin
    cgPlot,[0,1],POSITION=[0,0,1,1],xstyle=4+8,ystyle=4+8,COLOR='red',BACKGROUND='white',THICK=1
    cgPlot,[1,0],COLOR='red',BACKGROUND='white',THICK=1,/OVERPLOT
  endif else begin
    tv, rebin(reform(self.color,1,1,3),self.getKeyword('xSize'),self.getKeyword('ySize'),3), TRUE=3
  endelse

end

pro hubAMWColor__define

  define = {hubAMWColor $
    ,inherits hubAMWObject $
    ,colorSelected:0b $
    ,color:[0b,0b,0b] $
    ,restore:hash() $
    ,colorButtons:hash() $
  }
  
end

;+
; :Hidden:
;-
pro test_hubAMW_color
  @huberrorcatch
  hubAMW_program 
  hubAMW_color, 'color', Title='Select color', /OPTIONAL ;, XSize=20,YSize=20;, VALUE=[255,0,255]
  result = hubAMW_manage()
  print,result

end  