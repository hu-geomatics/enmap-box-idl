;+
; :Hidden:
;-

function hubAMWOutputFilename::init $
  ,name $
  ,Title=title $
  ,Value=value $
  ,Extension=extension $
  ,AllowEmptyFilename=allowEmptyFilename $
  ,Tooltip=tooltip $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional  $
  ,Advanced=advanced
  
  !NULL=self->hubAMWObject::init(name, Optional=optional,Advanced=advanced)

  self.setKeywordDefault,'title', 'Result File'
  self.setKeywordDefault,'value', ''
  self.setKeywordDefault,'size', 500
  self.setKeywordDefault,'allowEmptyFilename', 0b
  
  
  self.saveInformation
  
  return,1b
end

pro hubAMWOutputFilename::saveInformation

  textDefaultFile = self.getKeyword('value')+' '
  textTemporaryFile = hub_getUserTemp(file_basename(self.getKeyword('value'))) 
  if (textDefaultFile ne ' ') and (file_dirname(textDefaultFile) eq '.') then begin
    textDefaultFile = textTemporaryFile
  endif 
  self.setInformation,'textChooseFile', '<choose file>'
  self.setInformation,'textCombobox', [textDefaultFile,textTemporaryFile, self.getInformation('textChooseFile')]

end

function hubAMWOutputFilename::getValue $
  , NotAddExtension=notAddExtension

  if ~widget_info(/SENSITIVE,self.getWidgetID('combobox')) then return, !null
  value = widget_info(self.getWidgetID('combobox'), /COMBOBOX_GETTEXT)
  value = strtrim(value, 2)
  
  if isa(self.getKeyword('extension')) $
     and ~keyword_set(notAddExtension) $
     and (value ne '') then begin
    extension = '.'+self.getKeyword('extension')
    value = filepath(file_basename(value, extension)+extension,ROOT_DIR=file_dirname(value))
  endif
  
  return,value

end

pro hubAMWOutputFilename::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base,/ROW,XPAD=0,YPAD=0)
  
  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')
    
  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif
  
  self.setWidgetID, /Optional,'combobox',widget_combobox(base $
    ,VALUE=self.getInformation('textCombobox') $
    ,SCR_XSIZE=self.getKeyword('size'),/EDITABLE,UVALUE=self)
  self.setInformation,'currentValue',self.getValue(/NotAddExtension)

end

pro hubAMWOutputFilename::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return

  case event.str of
    self->getInformation('textChooseFile') : self.handleEventChooseFile,event
    else : return
  endcase

end

pro hubAMWOutputFilename::handleEventChooseFile $
  ,event

  ; select an output filename
  
  if isa(self.getKeyword('extension')) then begin
    filter = '*.'+self.getKeyword('extension')
  endif
  
  dp_file = isa(self.getInformation('currentValue')) ? file_basename(self.getInformation('currentValue')) : !null
  ;dp_path = isa(self.getInformation('currentValue')) ? file_dirname(self.getInformation('currentValue')) : !null

  widget_control,event.top,/CLEAR_EVENTS
  fileName = dialog_pickfile($
     DEFAULT_EXTENSION=self.getKeyword('extension') $
    ,DIALOG_PARENT=event.top $
    ,FILE=dp_file $
    ,PATH=dp_path $
    ,FILTER=filter $
    ,/WRITE)
  widget_control,event.top,/CLEAR_EVENTS

  ; check if the file select dialog was canceled 

  dialogCanceled = fileName eq ''

  if dialogCanceled then begin
  
    ; restore the last combobox selection
    
    combobox_select = (where(self.getInformation('textCombobox') eq self.getInformation('currentValue')))[0] > 0
    widget_control,self.getWidgetID('combobox'),SET_COMBOBOX_SELECT=combobox_select,/CLEAR_EVENTS

  endif else begin

    ; add selected file to combobox list

    self.setInformation,'textCombobox',[(self.getInformation('textCombobox'))[0],fileName,(self.getInformation('textCombobox'))[1:*]]
    widget_control,self.getWidgetID('combobox'),SET_VALUE=self.getInformation('textCombobox'),/CLEAR_EVENTS
    widget_control,self.getWidgetID('combobox'),SET_COMBOBOX_SELECT=1,/CLEAR_EVENTS
    self.setInformation,'currentValue',self.getValue(/NotAddExtension)
    cd, file_dirname(fileName)
  endelse
  
end

function hubAMWOutputFilename::checkConsistenty $
  ,Message=message

  value = self.getValue()

  ; check if filename is empty
  
  if value eq '' then begin
    if self.getKeyword('allowEmptyFilename') then begin
      message = []
      return,1b
    endif else begin
      message = 'filename is empty'
      return,0b
    endelse
  endif

  ; check if is full path

  if ~(expand_path(value) eq value) then begin
    message = 'filename must include a fully qualified path'
    return,0b
  endif

  ; check if file already exists
  
  if (hubIOHelper()).fileReadable(value) then begin
    dialogMessage = [ $
       'File: '+value $
      ,'' $
      ,'This file already exists.' $
      ,'Overwrite this file?' $
    ]
    ok = dialog_message(dialogMessage,/CENTER,DIALOG_PARENT=self.base,/QUESTION,TITLE='Output Filename')
    if ok eq 'No' then begin
      message = 'file already exists'
      return,0b
    endif
  endif

  message = [] 
  return,1b
  
end

pro hubAMWOutputFilename__define

  define = {hubAMWOutputFilename $
    ,inherits hubAMWObject $
  }
  
end