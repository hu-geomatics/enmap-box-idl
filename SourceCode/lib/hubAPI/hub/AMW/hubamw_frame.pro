;+
; :Description:
;    This procedure is used to insert a frame into an auto-managed widget program. 
;    After inserting a frame, you can insert subframes (see `hubAMW_subframe`) and widgets into it. 
;    Frames always have a column based layout, use subframes to lay out widgets in a row.
;    For details on how to build auto-managed widget programs also see `auto-managed-widget-program.idldoc`.
;
; :Keywords:
;    Title : in, optional, type=string
;      Use this keyword to specify a title for the frame.
;      
;    XSize : in, optional, type=int, default=100
;      Use this keyword to specify the frame width in pixels.
;
;    YSize : in, optional, type=int, default=50
;      Use this keyword to specify the frame height in pixels.
;      
;    Advanced : in, optional, type=boolean
;      Set this keyword to hide this frame (and all widgets belonging to it) by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
;-
pro hubAMW_frame $
  ,Title=title $
  ,Advanced=advanced $
  ,XSize=xSize, YSize=ySize
  
  on_error,2
    
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertFrame, Title=title, Advanced=advanced, XSize=xSize, YSize=ySize
    
end

pro test_hubAMW_frame

  hubAMW_program
  hubAMW_frame, Title='My Frame', XSize=500, YSize=300
  hubAMW_list, 'list', LIST=sindgen(100), XSIZE=300,YSIZE=1000
  hubAMW_checklist, 'list', LIST=sindgen(100)
  hubAMW_frame, Title='My Frame', XSize=100, YSize=500
  hubAMW_checklist, 'list2', LIST=sindgen(100),xsize=200,ysize=50
  
  result = hubAMW_manage()

end