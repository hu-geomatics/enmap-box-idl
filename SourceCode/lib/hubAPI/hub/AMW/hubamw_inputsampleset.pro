;+
; :Description:
;
;    This widget allows the selection of sample set for classification, regression and density estimation, 
;    or an image mask. In any case, the user has to select an image, representing the features, and
;    an associated label image, representing the labels. Note that the list of 
;    label images is filtered in accordance to the spatial size of the feature image.
;     
;    Sample set for classification:
;    
;    .. image:: _IDLDOC/inputSampleSet1.gif
;    
;    Sample set for regression:
;    
;    .. image:: _IDLDOC/inputSampleSet2.gif
;    
;    Sample set for density estimation:
;    
;    .. image:: _IDLDOC/inputSampleSet3.gif
;
;    Image masking:
;
;    .. image:: _IDLDOC/inputSampleSet4.gif
;    
;    For details on auto-managed widget programs see `auto-managed-widget-program.idldoc`.
;    
; :Params:
;    name : in, required, type=string
;      Use this argument to specify a hash key in the auto-managed widget program result hash (see `hubAMW_manage` for details).
;      The hash value is given by a hash variable containing the information::
;      
;        hash key         | description               | type
;        -----------------|---------------------------|---------
;        featureFilename  | feature image filename    | string
;        labelFilename    | label image filename      | string
;        band             | selected band,            | integer (optional)
;                         | use /SelectBand keyword   | 
;        pixel            | selected pixel position   | integer[2] (optional)
;                         | use /SelectPixel keyword  |     
;
; :Keywords:
;    Title : in, optional, type=string, default='Input Image'
;      Use this keyword to specify the feature image title.
;   
;    Value : in, optional, type=string
;      Use this keyword to specify the default image filename.
;     
;    Classification: in, optional, type=boolean, default=0
;      Set this keyword to select a classification sample set.
;      
;    Regression: in, optional, type=boolean, default=0
;      Set this keyword to select a regression sample set.
;
;    Density: in, optional, type=boolean, default=0
;      Set this keyword to select a density estimation sample set.
;     
;    Masking : in, optional, type=boolean, default=0
;      Set this keyword to select an image with an associated mask.
;
;    ReferenceOptional : in, optional, type=boolean, default=0
;      Set this keyword to make the selection of the label image optional. 
;      If no label image is selected, the labelFilename hash value is set to !null.  
;
;    ReferenceTitle : in, optional, type = string
;      Use this keyword to specify the label image title.
;      
;    SelectBand : in, optional, type=boolean, default=0
;      Set this keyword to additionally select a specific band.
;      
;    SelectPixel : in, optional, type=boolean, default=0
;      Set this keyword to additionally select a specific pixel position.
;      
;    SelectSpectralSubset : in, optional, type=boolean, default=0
;      Set this keyword to additionally select a spectral subset.
;
;    BandValue : in, optional, type=int
;      Use this keyword to specify a default band.
;    
;    PixelValue : in, optional, type=int
;      Use this keyword to specify a default pixel position.
;    
;    SpectralSubsetValue : in, optional, type=int[]
;      Use this keyword to specify a default spectral subset.
;    
;    AllowEmptyBand : in, optional, type=boolean, default=0
;      Set this keyword to allow an empty band selection.
;    
;    AllowEmptyPixel : in, optional, type=boolean, default=0
;      Set this keyword to allow an empty pixel location.
;    
;    Size : in, optional, type=int, default=500
;      Use this keyword to specify the widget width in pixel.
;      
;    TSize : in, optional, type=int
;      Use this keyword to specify the widget title width in pixels.
; 
;    Optional : in, optional, type=int {0 | 1 | 2}, default=0
;      Use this keyword to add a checkbox that marks the widget as optional:
;         
;      0 - no checkbox
;          
;      1 - checkbox is checked by default
;         
;      2 - checkbox is unchecked by default
;
;    Advanced : in, optional, type=boolean 
;      Set this keyword to hide the widget by default. 
;      This will insert an Advanced button to the auto-managed widget program window, 
;      next to the Accept and Cancel buttons.
;
; :Examples:
;    Create a widget for selecting regression training data::
;
;      hubAMW_program
;      hubAMW_frame, Title='Training Data for Supervised Regression'
;      featureFilename = hub_getTestImage('Hymap_Berlin-B_Image')
;      labelFilename =   hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth')
;      hubAMW_inputSampleSet, 'regressionSample', /Regression, Value=featureFilename, ReferenceValue=labelFilename
;      result = hubAMW_manage()
;
;    .. image:: _IDLDOC/inputSampleSet2.gif
;  
;-
pro hubAMW_inputSampleSet $
  ,name $
  ,Title=title $
  ,Value=value $
  ,ReferenceValue=referenceValue $
  ,Classification=classification $
  ,Regression=regression $
  ,Density=density $
  ,Masking=masking $
  ,ReferenceTitle=referenceTitle $
  ,ReferenceOptional=referenceOptional $
  ,SelectBand=selectBand $
  ,SelectPixel=selectPixel $
 , SelectSpectralSubset=selectSpectralSubset $
  ,BandValue=bandValue $
  ,PixelValue=pixelValue $
 , SpectralSubsetValue=spectralSubsetValue $
  ,AllowEmptyBand=allowEmptyBand $
  ,AllowEmptyPixel=allowEmptyPixel $
  ,Size=size $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced $
  ,Tooltip=tooltip $
  ,ReferenceTooltip=ReferenceTooltip
  
  hubAMWObject = hubAMWInputSampleSet( $
     name $
    ,Title=title $
    ,Value=value $
    ,ReferenceValue=referenceValue $
    ,BandValue=bandValue $
    ,PixelValue=pixelValue $
    ,SpectralSubsetValue=spectralSubsetValue $
    ,Classification=classification $
    ,Regression=regression $
    ,Density=density $
    ,Masking=masking $
    ,ReferenceTitle=referenceTitle $
    ,ReferenceOptional=referenceOptional $
    ,SelectBand=selectBand $
    ,SelectPixel=selectPixel $
    ,SelectSpectralSubset=selectSpectralSubset $
    ,AllowEmptyBand=allowEmptyBand $
    ,AllowEmptyPixel=allowEmptyPixel $    
    ,Size=size $
    ,TSize=tSize $
    ,Optional=optional $
    ,Advanced=advanced $
    ,Tooltip=tooltip $
    ,ReferenceTooltip=ReferenceTooltip)
  
  hubAMWProgram = hubAMW_getProgram()
  hubAMWProgram.insertWidget,hubAMWObject
  
end
