;+
; :Hidden:
;-

function hubAMWButton::init $
  ,Title=title $
  ,Bar=bar $  
  ,ConsistencyCheckFunction=consistencyCheckFunction $
  ,ResultHashKeys=resultHashKeys $    
  ,UserInformation=userInformation $    
  ,Tooltip=tooltip $
  ,EventHandler=eventHandler $
  ,Press=press $
  ,Advanced=advanced

  !NULL=self->hubAMWObject::init('', Advanced=advanced)
  self.setKeywordDefault,'resultHashKeys', ''

  return,1b
end

function hubAMWButton::getConsistencyCheckFunction
  return, self.getKeyword('consistencyCheckFunction')
end

function hubAMWButton::getResultHashKeys
  return, self.getKeyword('resultHashKeys')
end

function hubAMWButton::getValue
  return, !null
end

pro hubAMWButton::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase
  if keyword_set(self.getKeyword('bar')) then begin
    base = self.amwProgram.getUserButtonBase()
    widget_control, base, MAP=1
  endif else begin
    base = self.base
  endelse
  
  self.setWidgetID, 'button', widget_button(base, VALUE=self.getKeyword('title'), UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[self->getWidgetID('button')] = self.getKeyword('tooltip')
end

pro hubAMWButton::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return

  ; check amw program consistency

  messagesInternal = self.amwProgram._getInternalConsistencyMessages(ResultHashKeys=self.getKeyword('resultHashKeys'))
  messagesUser = self.amwProgram._getUserConsistencyMessages(self.getKeyword('consistencyCheckFunction'), self.getKeyword('userInformation'))
  messages = [messagesInternal, messagesUser]

  if isa(messages) then begin
    self.amwProgram._showErrorMessage, messages
    return
  endif

  ; get result hash

  if (self.getKeyword('resultHashKeys'))[0] ne '' then begin
  
    resultHash = self.amwProgram._getResultHash(/Accept)
    
    ; create subhash over keys provided by the user via resultHashKeys keyword
    
    resultHash = resultHash.hubGetSubhash(self.getKeyword('resultHashKeys'))

  endif else begin
  
    resultHash = hash()
  
  endelse
  
  ; call event handler

  if isa(self.getKeyword('eventHandler')) then begin
    split = strsplit(self.getKeyword('eventHandler'), '::', /EXTRACT)
    isClassMethod = n_elements(split) eq 2
    if isa(self.getKeyword('userInformation')) then begin
      
      if isClassMethod then begin
        call_method, split[1], obj_new(split[0]), resultHash, UserInformation=self.getKeyword('userInformation')        
      endif else begin
        call_procedure, split[0], resultHash, UserInformation=self.getKeyword('userInformation')
      endelse
    
    endif else begin

      if isClassMethod then begin
        call_method, split[1], obj_new(split[0]), resultHash
      endif else begin
        call_procedure, split[0], resultHash
      endelse
    
    endelse
  endif
  
end

pro hubAMWButton__define

  define = {hubAMWButton $
    ,inherits hubAMWObject $
  }
  
end