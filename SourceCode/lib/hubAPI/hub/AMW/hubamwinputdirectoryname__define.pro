;+
; :Hidden:
;-

function hubAMWInputDirectoryName::init $
  ,name $
  ,Title=title $
  ,Value=value $
  ,DefaultResult=defaultResult $
  ,Editable=editable $
  ,AllowNonExistent=allowNonExistent $
  ,Tooltip=tooltip $
  ,Size=size  $
  ,TSize=tSize $
  ,Optional=optional $
  ,Advanced=advanced
  
  !NULL=self->hubAMWObject::init(name, Optional=optional,Advanced=advanced)

  self.setKeywordDefault, 'title', 'Input Directory'
  self.setKeywordDefault, 'value', ' '
  self.setKeywordDefault, 'size', 500
 
  return,1b
end

function hubAMWInputDirectoryName::getValue

  ; get combobox text

  widget_control,self.getWidgetID('text'), GET_VALUE=value

  ; get fileName
  
  value = strtrim(value,2)
  
  if value eq '' then begin
    if isa(self.getKeyword('defaultResult')) then begin
      value = self.getKeyword('defaultResult')
      return, value
    endif else begin
      message = 'empty directory name.'
      return,!null
    endelse
  endif
  
  return,value

end

pro hubAMWInputDirectoryName::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase

  base = widget_base(self.base, /ROW, XPAD=0, YPAD=0)
  
  titleBase = widget_base(base, ROW=row, XPAD=0, YPAD=0, SPACE=0,/ALIGN_CENTER)
  labelID = widget_label(titleBase,VALUE=self.getKeyword('title')+' ',/ALIGN_LEFT   $
    , UVALUE=self, /TRACKING_EVENTS)
  (self.tooltips)[labelID] = self.getKeyword('tooltip')
    
  if isa(self.getKeyword('tSize')) then begin
    widget_control, titleBase, SCR_XSIZE=self.getKeyword('tSize')-(self.optional ne 0)*(20+2)
  endif
  
  self.setWidgetID, /Optional, 'text',widget_text(base,VALUE=strtrim(self.getKeyword('value'),2),SCR_XSIZE=self.getKeyword('size'),/NO_NEWLINE, EDITABLE=self.getKeyword('editable'))
  self.setWidgetID, /Optional, 'button',widget_button(base,VALUE='...', UVALUE=self)

end

pro hubAMWInputDirectoryName::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return
  
  ; get directory name
  
  filename = dialog_pickfile($
     DIALOG_PARENT=event.top $
    ,PATH=self.getKeyword('value') $
    ,/Directory $
    ,/READ,/MUST_EXIST)
  
  if filename ne '' then begin
    widget_control,self.getWidgetID('text'), SET_VALUE=filename
  endif

  ; set to path as current working directory
    
  cd, file_dirname(filename) 
   
end

function hubAMWInputDirectoryName::checkConsistenty $
  ,Message=message
  
  value = self.getValue()

  ; check if directory name is empty
  
  if value eq '' && ~isa(self.getKeyword('defaultResult')) then begin
    message = 'directory name is empty'
    return,0b
  endif

  ; check if directory exists
  if ~(hubIOHelper()).fileReadable(value) && ~isa(self.getKeyword('allowNonExistent')) then begin
    message = 'directory does not exist'
    return,0b
  endif
  
  message = [] 
  return,1b
  
end

pro hubAMWInputDirectoryName__define

  define = {hubAMWInputDirectoryName $
    , inherits hubAMWObject $
  }
  
end