;+
; :Hidden:
;-

function hubAMWTreeList::init $
  ,name $
  ,Title=title $
  ,Value=value $ ; initially selected list index
  ,List=list $ ; names of the list elements
  ,AllowEmptySelection=allowEmptySelection $
  ,MultipleSelection=multipleSelection $
  ,Extract=extract $
  ,ShowFilter=showFilter $
  ,Tooltip=tooltip $
  ,XSize=xsize $
  ,YSize=ysize $
  ,Optional=optional $
  ,Advanced=advanced

  !NULL=self->hubAMWObject::init(name,Optional=optional,Advanced=advanced)

  if ~isa(list) then begin
    message,'Missing keyword: List.'
  endif

  self.setKeywordDefault,'allowEmptySelection', 0
  self.setKeywordDefault,'multipleSelection', 0
  self.setKeywordDefault,'showFilter', 1
  self.setKeywordDefault,'xsize', 100
  self.setKeywordDefault,'ysize', 50
  
  self.setInformation, 'folderArray', list[0:-2,*]
  self.setInformation, 'leafArray', reform(list[-1,*])
  
  self.setInformation,'numberOfListElements',n_elements(list[0,*])

  return,1b
end

function hubAMWTreeList::getValue

  on_error,2
  
  treeSelection = widget_info(self.getWidgetID('list'), /TREE_SELECT)
  
  if treeSelection[0] eq -1 then return, !null ; return !null if selection is empty

  value = list()
  foreach id, treeSelection, i do begin
    widget_control, id, GET_UVALUE=uvalue
    if isa(uvalue) then begin
      value.add, uvalue
    endif
  endforeach
  value = value.toArray()

  if ~self.getKeyword('multipleSelection') then begin
    value = value[0] ; return a scaler if a single element is selected
  endif
  
  if isa(self.getKeyword('extract')) then begin
    if isa(self.getKeyword('extract'), /SCALAR) then begin
      value = (self.getInformation('leafArray'))[value]
    endif
    if isa(self.getKeyword('extract'), /ARRAY) then begin
      value = (self.getKeyword('extract'))[value]
    endif
  endif
  
  return,value

end

pro hubAMWTreeList::defineWidgetHierarchy $
  ,parentBase

  self->hubAMWObject::defineWidgetHierarchy,parentBase
  
  base = widget_base(self.base,/COLUMN,XPAD=0,YPAD=0)
  self.setWidgetID, 'base', base
  
  if isa(self.getKeyword('title')) then begin
    labelID = widget_label(base,VALUE=self.getKeyword('title'),/ALIGN_LEFT   $
      , UVALUE=self, /TRACKING_EVENTS)
    (self.tooltips)[labelID] = self.getKeyword('tooltip')
  endif

  if isa(self.getKeyword('showFilter')) then begin
    subbase = widget_base(base, /ROW)
    !null = widget_label(subbase, VALUE='Filter')
    filter = widget_text(subbase, SCR_XSIZE=self.getKeyword('xsize'), UVALUE=self, /EDITABLE)
    self.setWidgetID, /Optional, 'filter', filter
  endif

  self.filterTreeList
end

pro hubAMWTreeList::filterTreeList, matches
  base = self.getWidgetID('base')
  widget_control, UPDATE=0, base
  if isa(self.getWidgetID('list')) then begin
    widget_control, /DESTROY, self.getWidgetID('list')
  endif

  root = widget_tree(base, /FOLDER, MULTIPLE=self.getKeyword('multipleSelection') $
    , SCR_XSIZE=self.getKeyword('xsize'),SCR_YSIZE=self.getKeyword('ysize') $
    , UVALUE=self, /EXPANDED)
  self.setWidgetID, /Optional,'list', root
  
  nodeHashes = objarr(10)
  leafList = list()
  
  for i=0,9 do begin
    nodeHashes[i] = hash()
  endfor

  for iItem=0,self.getInformation('numberOfListElements')-1 do begin
    if isa(matches) && ~matches[iItem] then continue 
    folderValues = (self.getInformation('folderArray'))[*,iItem]
    folderValues = folderValues[where(folderValues ne '')]
    foreach folderValue, folderValues, iFolder do begin
      nodeHash = nodeHashes[iFolder]
      if iFolder eq 0 then begin
        parent = root
      endif else begin
        parent = (nodeHashes[iFolder-1])[folderValues[iFolder-1]]
      endelse
      if nodeHash.hasKey(folderValue) then begin
        node = nodeHash[folderValue]
      endif else begin
        node = widget_tree(parent, Value=folderValue, /FOLDER, /EXPANDED)
        nodeHash[folderValue] = node
      endelse
    endforeach
    leafList.add, widget_tree(node, Value=(self.getInformation('leafArray'))[iItem], UVALUE=iItem)
  endfor
  self.setInformation, 'leafList', leafList
  widget_control, UPDATE=1, base
end

function hubAMWTreeList::checkConsistenty $
  ,Message=message
  
  value = self.getValue()
  if ~self.getKeyword('allowEmptySelection') then begin
    message = 'no item selected'
    if ~isa(value) then return,0b
  endif
      
  message = [] 
  return,1b
  
end

pro hubAMWTreeList::handleEvent $
  ,event

  self->hubAMWObject::handleEvent, event, EventHandled=eventHandled
  if eventHandled then return
  
  case event.id of 
    self.getWidgetID('list') : ; do nothing
    self.getWidgetID('filter') : begin
      widget_control, self.getWidgetID('filter'), GET_VALUE=filterString
      if filterString eq '' then begin
        matches = !null
      endif else begin
        matches = stregex(self.getInformation('leafArray'), filterString, /BOOLEAN)
      endelse
      self.filterTreeList, matches
    end
  endcase
end

pro hubAMWTreeList__define

  define = {hubAMWTreeList $
    ,inherits hubAMWObject $
  }
  
end
