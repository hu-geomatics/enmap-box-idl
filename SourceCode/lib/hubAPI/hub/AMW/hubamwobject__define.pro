;+
; :Hidden:
;-

function hubAMWObject::init $
  , name $
  , Optional=optional $
  , Advanced=advanced

  if (typename(name) ne 'STRING') or ~isa(name,/Scalar) then begin
    message,'Argument NAME must be a scalar string.' 
  endif

  self.name = name
  self.optional = isa(optional) ? optional : 0
  self.optionalWidgetIDs = list()
  self.advanced = keyword_set(advanced)
  self.keywords = hash('title','')
  self.keywordDefaults = hash()
  self.widgetIDs = hash()
  self.information = hash()
  self.tooltips = hash()
  self.saveKeywords
  
  return,0b ; This is a base class to inherit from to create an auto-managed widget object.
 
end

pro hubAMWObject::defineWidgetHierarchy $
  ,parentBase

  ; insert checkbox for turning widget on/off, if optional keyword was set
  
  if self.optional ne 0 then begin
    
    parentBase_ = widget_base(parentBase, /ROW, XPAD=0, YPAD=0)
    if !VERSION.OS_FAMILY eq 'unix' then begin
      scr_xsize = 15
    endif else begin
      scr_xsize = 20
    endelse
    checkboxBase = widget_base(parentBase_, /ROW, XPAD=0, YPAD=0, /NONEXCLUSIVE, SCR_XSIZE=scr_xsize)
    self.optionalID = widget_button(checkboxBase, VALUE=' ', UVALUE=self, SCR_XSIZE=scr_xsize)

    sensitive = self.optional eq 1
    widget_control, self.optionalID, SET_BUTTON=sensitive
    
  endif else begin

    parentBase_ = parentBase
  
  endelse

  self.base = widget_base(parentBase_ $
    , PRO_SET_VALUE='autoManagedWidget_set_value' $
    , FUNC_GET_VALUE='autoManagedWidget_get_value' $
    , /COLUMN, XPAD=0, YPAD=0)


end

pro hubAMWObject::saveKeywords

  keywordNames = strlowcase(scope_varname(LEVEL=-2))
  if keywordNames[0] eq '' then return 
  foreach keywordName,keywordNames do begin
    if keywordName eq 'self' then continue
    (self.keywords)[keywordName] = scope_varfetch(keywordName,LEVEL=-2)
  endforeach

end

pro hubAMWObject::setKeywordDefault $
  ,hashKey $
  ,hashValueDefault
  
  if ~self.keywords.hasKey(strlowcase(hashKey)) then begin
    message,'Keyword is not allowed: '+strupcase(hashKey)
  endif  
  
  (self.keywordDefaults)[strlowcase(hashKey)] = hashValueDefault

end

function hubAMWObject::getKeyword $
  ,hashKey

  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch,/CANCEL
    help,/Last_message
    message,'An error occured inside hubAMWObject::getKeyword.'
  endif

  keywordValue = (self.keywords)[strlowcase(hashKey)]
  if ~isa(keywordValue) and self.keywordDefaults.hasKey(strlowcase(hashKey)) then begin
    keywordValue = (self.keywordDefaults)[strlowcase(hashKey)]
  endif
  return,keywordValue
  
end

pro hubAMWObject::setInformation $
  ,hashKey $
  ,hashValue

  (self.information)[strlowcase(hashKey)] = hashValue
  
end

function hubAMWObject::getInformation $
  ,hashKey
Catch, errorStatus

; Error handler
if (errorStatus ne 0) then begin
  Catch, /CANCEL
; TODO: Write error handler
  help,/LAST_MESSAGE
  stop
endif

  hashValue = (self.information)[strlowcase(hashKey)]
  return,hashValue
  
end

pro hubAMWObject::setWidgetID $
  ,widgetName $
  ,widgetID $
  ,Optional=optional
  
  (self.widgetIDs)[widgetName] = widgetID
  if keyword_set(optional) then begin
    self.optionalWidgetIDs.add, widgetID
  endif

end

function hubAMWObject::getWidgetID $
  ,widgetName, NoNull=noNull
  
  if (self.widgetIDs).hasKey(widgetName) then begin
    widgetID = (self.widgetIDs)[widgetName]
  endif else begin
    if keyword_set(noNull) then begin
      widgetID = -1
    endif else begin
      widgetID = !null
    endelse
  endelse
  
  return, widgetID
end

function hubAMWObject::getName

  return,self.name

end

function hubAMWObject::checkConsistenty $
  ,Message=message
  
  message = []
  return,1b

end

function hubAMWObject::getSensitivity

  result = widget_info(/SENSITIVE, self.base)
  return, result
  
end

function hubAMWObject::getOptionalSelection

  if self.optional eq 0 then begin
    result = 1b
  endif else begin
    result = widget_info(self.optionalID, /BUTTON_SET)
  endelse
  return, result
  
end

pro hubAMWObject::setSensitivity

  if widget_info(/VALID_ID, self.optionalID) then begin 
    sensitive = widget_info(/BUTTON_SET, self.optionalID)
    foreach id, self.optionalWidgetIDs do begin
      widget_control, id, SENSITIVE=sensitive
    endforeach
  endif
  
end

function hubAMWObject::getValue

  on_error,2

  return,!NULL

end

pro hubAMWObject::handleEvent $
  , event $
  , EventHandled=eventHandled

  eventHandled = 0b

  ; turn widget sensitivity on/off
  
  if event.id eq self.optionalID then begin
    self.setSensitivity
    eventHandled = 1b
  endif

  ; show tooltip

  if isa(event, 'WIDGET_TRACKING') then begin
    if isa((self.tooltips)[event.id]) then begin
    
      if event.enter eq 1 then begin
        self.tlbTooltip = widget_base(GROUP_LEADER=self.base, TLB_FRAME_ATTR=1+2+4+8+16)
        
        dummyBase = widget_base(self.tlbTooltip, /FRAME, /COLUMN) 
        foreach line, (self.tooltips)[event.id] do begin
          !null = widget_label(dummyBase, VALUE=line)
        endforeach
       
        ; set position
        
        geoAMWProgram = widget_info((self.amwProgram).getTopLevelBase(), /GEOMETRY)
        geoWidget = widget_info(self.tlbTooltip, /GEOMETRY)
        yxOffset =   [geoAMWProgram.xoffset, geoAMWProgram.yoffset] $
                   - [0                    , geoWidget.scr_ysize]
                   
        if !version.OS_FAMILY eq 'unix' then begin
          yxOffset -= [0, 55]
        endif 
        
        
        widget_control, self.tlbTooltip, XOFFSET=yxOffset[0], YOFFSET=yxOffset[1]
        widget_control, self.tlbTooltip, /REALIZE
  
      endif else begin
        if widget_info(self.tlbTooltip, /VALID_ID) then begin
          widget_control, self.tlbTooltip, /DESTROY
        endif
      endelse
      
    endif
    eventHandled = 1b
  endif

end

pro hubAMWObject::_showAdvancedWidget

  if self.advanced then begin
  
    if self.optional ne 0 then begin
      base = widget_info(/PARENT, self.base)
    endif else begin
      base = self.base
    endelse
    
  ; restore base size
  widget_control,base,GET_UVALUE=scr_size
  widget_control,base, SCR_XSIZE=scr_size[0], SCR_YSIZE=scr_size[1]
  widget_control,base, Map=1

  endif
    
end

pro hubAMWObject::_hideAdvancedWidget

  if self.advanced then begin
  
    if self.optional ne 0 then begin
      base = widget_info(/PARENT, self.base)
    endif else begin
      base = self.base
    endelse
  
    ; save base size
    geometry = widget_info(base,/GEOMETRY)
    scr_size = [geometry.scr_xsize, geometry.scr_ysize]
    widget_control,base,SET_UVALUE=scr_size
  
    ; hide base
    widget_control,base,SCR_XSIZE=1,SCR_YSIZE=1
    widget_control,base,MAP=0

  endif
    
end

function hubAMWObject::_isAdvanced

  return, self.advanced

end

pro hubAMWObject::setAmwProgram, amwProgram

  self.amwProgram = amwProgram

end

pro hubAMWObject__define

  define = {hubAMWObject $
    ,inherits IDL_Object $
    ,base : 0L $
    ,name : '' $
    ,optional : 0 $
    ,advanced : 0b $
    ,optionalID : 0l $            ; widget ID for checkbox (turn sensitivity on/off)
    ,optionalWidgetIDs : list() $ ; list of widget IDs for that sensitivity can be turned on/off by the checkbox
    ,keywords : hash() $          ; hash for contructor keywords
    ,keywordDefaults : hash() $   ; hash for default values of undefined contructor keywords
    ,widgetIDs : hash() $         ; hash for widget IDs
    ,information : hash() $       ; hash for user data
    ,amwProgram : obj_new() $
    ,tlbTooltip : 0l $
    ,tooltips : hash() $
  }
  
end
