pro hubJavaBridge_info
  oBridgeSession = obj_new("IDLJavaObject$IDLJAVABRIDGESESSION")
  oVersion = oBridgeSession->getVersionObject()
  ok = dialog_message(/INFORMATION, [$
    'Java version: '+ oVersion->getJavaVersion() $
    ,'Bridge version: '+ oVersion->getBridgeVersion() $
    ,'Build date: '+ oVersion->getBuildDate() $
    ])
end
