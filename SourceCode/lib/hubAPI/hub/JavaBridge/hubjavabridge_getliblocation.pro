function hubJavaBridge_getLibLocation

  os = hubDev_getOSName()

;---------------------------------------------------------------------------------------
;get location of JVM shared library that is installed along with IDL (IDL 7.1 or higher)

  ;--------------------------------------
  ;IDL Help: on Windows systems, directory contains the JVM.DLL library file

  ;WIN32
  ;  IDL711WIN32_SETUP.EXE downloaded from www.ittvis.com
  ;  libjvm.so found in two locations:
  ;    !IDL\idlde\jre\bin\client
  ;    !IDL\idlde\jre\bin\server (SELECTED) (OK - test ARabe)
;strcmp(os,'
  if strcmp(os,'win32')     then libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['bin','bin.x86','jre','bin','server'])

  ;WIN64
  ;  IDL711WIN64_SETUP.EXE downloaded from www.ittvis.com
  ;  libjvm.so found in three locations:
  ;    !IDL\idlde\jre\bin\client
  ;    !IDL\idlde\jre\bin\server    
  ;    !IDL\idlde\bin.x86_64\jre\bin\server (SELECTED) (OK - test ARabe)
  if strcmp(os,'win64')     then libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['bin','bin.x86_64','jre','bin','server'])
 
  ;--------------------------------------
  ;IDL Help: on Solaris and Linux systems, directory contains the LIBJVM.SO shared library file
  
  ;LIN32
  ;  IDL711LINUX.X86.TAR.GZ downloaded from www.ittvis.com
  ;  libjvm.so found in two locations:
  ;    !DIR/idlde/bin.linux.x86/jre/lib/i386/client/libjvm.so
  ;    !DIR/idlde/bin.linux.x86/jre/lib/i386/server/libjvm.so                                    
  if strcmp(os,'lin32')     then libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['idlde','bin.linux.x86','jre','lib','i386','server'])
  
  ;LIN64
  ;  IDL711LINUX.X86.TAR.GZ downloaded from www.ittvis.com
  ;  libjvm.so found in one location:
  ;  !IDL/idlde/bin.linux.x86_64/jre/lib/amd64/server/libjvm.so (??? - test ToDo)
  if strcmp(os,'lin64')     then libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['idlde','bin.linux.x86_64','jre','lib','amd64','server'])

  ;SUN32 and SUN64
  ;  IDL711SOLARIS2.SPARC.TAR.GZ downloaded from www.ittvis.com
  ;  libjvm.so found in four locations:
  ;    !IDL\idlde\bin.solaris2.sparc\jre\lib\sparc
  ;    !IDL\idlde\bin.solaris2.sparc\jre\lib\sparc\client
  ;    !IDL\idlde\bin.solaris2.sparc\jre\lib\sparc\server   (SELECTED) (??? - test ToDo) 
  ;    !IDL\idlde\bin.solaris2.sparc\jre\lib\sparc9\server
  if strcmp(os,'sun32')     then stop;libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['idlde','bin.solaris2.sparc','jre','lib','sparc','server'])
  if strcmp(os,'sun64')     then stop;libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['idlde','bin.solaris2.sparc','jre','lib','sparc','server'])
  
  ;SUNX86_64
  ;  IDL711SOLARIS2.X86_64.TAR.GZ downloaded from www.ittvis.com
  ;  libjvm.so found in four locations:
  ;    !IDL\idlde\bin.solaris2.x86_64\jre\lib\i386
  ;    !IDL\idlde\bin.solaris2.x86_64\jre\lib\amd64\server
  ;    !IDL\idlde\bin.solaris2.x86_64\jre\lib\i386\client
  ;    !IDL\idlde\bin.solaris2.x86_64\jre\lib\i386\server   (SELECTED) (??? - test ToDo)
  if strcmp(os,'sunx86_64') then stop;libLocation = filepath('',ROOT_DIR=!DIR,SUBDIRECTORY=['idlde','bin.solaris2.x86_64','jre','lib','i386','server'])

  ;--------------------------------------
  ;IDL Help: on Macintosh platforms, IDL uses the JVM provided by the operating system, and ignores the value of IDLJAVAB_LIB_LOCATION
  if strcmp(os,'macint32')  then return,'';libLocation = ''
  if strcmp(os,'macint64')  then return,'';libLocation = ''
  if strcmp(os,'macppc32')  then message,os+' not testet';libLocation = ''

  return,libLocation
end