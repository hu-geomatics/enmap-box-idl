pro hubJavaBridge_init, jarFiles, path, ShowInfo=showInfo

  if keyword_set(hub_getAppState('hubAPI', 'javaBridgeInitialized')) then begin
    return
  endif

  ; Java CLASSPATH setting

  jvmJarFiles = []
;  jvmJarFiles = [file_search(filepath('',ROOT_DIR=enmapBox_getDirname(/EnmapProject)),'*.jar')]
  jvmJarFiles = [jvmJarFiles, jarFiles]
  jvmJarFiles = [jvmJarFiles, filepath('jbexamples.jar',ROOT_DIR=!DIR,SUBDIRECTORY=['resource','bridges','import','java'])]

  jvmJarFilesSetting = 'JVM Classpath = $CLASSPATH'
  for i=0,n_elements(jvmJarFiles)-1 do begin
    jvmJarFilesSetting += path_sep(/Search_Path) + jvmJarFiles[i]
  endfor

  ; Java virtual machine locations

  jvmLocation = hubJavaBridge_getLibLocation()
  if !VERSION.OS_FAMILY eq 'Windows' then begin
    jvmLocationSetting = 'JVM LibLocation = '+jvmLocation
  endif else begin
    jvmLocationSetting = 'setenv IDLJAVAB_LIB_LOCATION '+jvmLocation
  endelse

  ; IDL Java bridge configuration file
  
  if !VERSION.OS_FAMILY eq 'Windows' then begin
    idlJavaBridgeConfigFile = filepath('idljavabrc', /TMP)
  endif else begin
    idlJavaBridgeConfigFile = filepath('.idljavabrc', /TMP)
  endelse

  javaBridgeConfigContent = [jvmLocationSetting,jvmJarFilesSetting]
  (hubIOASCIIHelper()).writeFile, idlJavaBridgeConfigFile, javaBridgeConfigContent
  setenv,'IDLJAVAB_CONFIG='+idlJavaBridgeConfigFile

  ; init bridge
  progressBar = hubProgressBar(Title='Initialize IDL Java Bridge')
  obj_destroy, progressBar

  hub_setAppState, 'hubAPI', 'javaBridgeInitialized', 1b
end
