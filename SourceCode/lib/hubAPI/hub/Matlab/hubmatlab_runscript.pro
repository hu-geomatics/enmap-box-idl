;+
; :Description:
;    Use this function to run a R script. Returns a result hash specified in R.
;
;    For usage details also see
;    `Running R Scripts and Code <./_IDLDOC/rscripts>`
;
; :Params:
;    scriptFilename: in, required, type=string
;      Path to R script.
;
;    parameters: in, optional, type=hash
;      Specify a hash of input parameters that is imported into R.
;      Note that data is passed via ASCII file I/O, which can be a bottle kneck.
;
;    spawnResult: out, optional, type=string[]
;      Standard output produced by R.
;
;    spawnError: out, optional, type=string[]
;      Error output produced by R.
;
; :Keywords:
;    GroupLeader: in, optional, type=widgetID
;      Group leader for progress bar.
;
;    Title: in, optional, type=widgetID, default='R Script Output'
;      Report title.
;
;    NoReport: in, optional, type=boolean, default=0b
;      Set to suppress the HTML report that is showing the `spawnResult`  and `spawnError` information.
;
; :Author: Andreas Rabe
;-
function hubMatlab_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=noReport

  ; get Rscript executable
  case !VERSION.OS_FAMILY of
    'Windows' :   begin
;      pathMatlab = (hub_getSettings()).hubGetValue('dir_matlab')
;      if ~isa(pathMatlab) || ~file_test(pathMatlab) then begin
;        ok = dialog_message(['Path to Matlab installation is not set correctly.',$
;          'Use "dir_matlab = <my Matlab install path>" to define it.'])
;        hubHelper.openFile, filepath('settings.txt', ROOT_DIR=hub_getDirname(), SUBDIRECTORY='resource')
        ;        spawnResult = ' '
        ;        spawnError = 'Path to R installation is not set correctly.'
;        message, 'huberror-ignore', /NONAME
     ; endif
    end
    'unix' : begin
      spawn, 'PATH=$PATH:/usr/bin; which matlab', spawnResult
      if spawnResult[0] eq '' then begin
        ok = dialog_message("The program 'matlab' is currently not installed.")
        message, 'huberror-ignore', /NONAME
      endif
    end
  endcase

  ; write inputs
  inputFilename = filepath(/TMP, 'hubMatlab_input.txt')
  outputFilename = filepath(/TMP, 'hubMatlab_output.txt')
  hubExternal_writeJSONFile, parameters, inputFilename, outputFilename
 
  ; run R script
  case !VERSION.OS_FAMILY of
    'Windows' : begin
      cmd = 'matlab -automation -wait -r ' + '"' + file_basename(scriptFilename,'.m') +' '+ inputFilename+'"'
      cd, file_dirname(scriptFilename), CURRENT=currentDirectory
    end
    'unix' : begin
      stop
      ;cmd = 'PATH=$PATH:/usr/bin; unset LD_LIBRARY_PATH; Rscript '+scriptFilename +' '+ inputFilename
      ;cd, CURRENT=currentDirectory
    end
  endcase
  progressBar = hubProgressBar(Cancel=0, GroupLeader=groupLeader, Info='Running Matlab Script: '+file_basename(scriptFilename), Title=title)
  spawn, cmd, spawnResult, spawnError
  obj_destroy, progressBar
  cd, currentDirectory

  ; show spawn output in HTML report
  if ~keyword_set(noReport) then begin
    hubMatlab_showErrorReport, spawnResult, spawnError, Title=title
  endif

  ; read outputs
  result = hubExternal_readJSONFile(outputFilename)

  ; cleanup
;  file_delete, inputFilename, outputFilename, /ALLOW_NONEXISTENT, /NOEXPAND_PATH, /QUIET

  return, result
end

;+
; :Description:
;    Behaves like `hubMatlab_runScript` function, simply the result is skipped.
;
; :Author: Andreas Rabe
;-
pro hubMatlab_runScript, scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title
  !null = hubMatlab_runScript(scriptFilename, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title)
end

;+
; :hidden:
;-
pro test_hubMatlab_runScript
  scriptFilename = filepath('script.r', ROOT_DIR=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY=['TEMPLATE_R','_lib'])
  parameters = hash('inputText', 'Hello World from EnMAP-Box :-).')
  scriptResult = hubMatlab_runScript(scriptFilename, parameters, NoReport=0)
  print, scriptResult['outputText']
end

pro test_hubMatlab_runScript2
  scriptFilename = 'D:\Users\geo_mahe\Documents\MATLAB\plotit.m'
  parameters = hash('x',findgen(100),'y',findgen(100)^2, 'title','Title', 'xlab','xtitle','ylab','ytitle','plotFilename',filepath('myPlot.pdf',/TMP))
  scriptResult = hubMatlab_runScript(scriptFilename, parameters, NoReport=0)

;  hubHelper.openFile, filepath('myPlot.pdf',/TMP)
;  print, scriptResult['outputText']
end

