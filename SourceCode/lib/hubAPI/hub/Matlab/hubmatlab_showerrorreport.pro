pro hubMatlab_showErrorReport, spawnResult, spawnError, Title=title
  if ~isa(title) then begin
    title = 'R'
  endif
  hubExternal_showErrorReport, spawnResult, spawnError, Title=title
end
