function hubColor_getENVIColors, indices, Names=names

  allNames = [ $
    'Black', 'Red', 'Green', 'Blue', 'Yellow', 'Cyan', 'Magenta',$
    'Maroon', 'Sea Green', 'Purple', 'Coral', 'Aquamarine', 'Orchid', 'Sienna', 'Chartreuse',$
    'Thistle', 'Red1', 'Red2', 'Red3', 'Green1', 'Green2', 'Green3', 'Blue1',$
    'Blue2', 'Blue3', 'Yellow1', 'Yellow2', 'Yellow3', 'Cyan1', 'Cyan2', 'Cyan3',$
    'Magenta1', 'Magenta2', 'Magenta3', 'Maroon1', 'Maroon2', 'Maroon3', 'Purple1', 'Purple2',$
    'Purple3', 'Orange1', 'Orange2', 'Orange3', 'Orange4', 'Sienna1', 'Sienna2', 'Sienna3',$
    'Thistle1', 'Thistle2', 'White']
  
  numberOfColors = n_elements(allNames)
  
  allColor = [ $
    0,   0,   0,$
    255,   0,   0,$
    0, 255,   0,$
    0,   0, 255,$
    255, 255,   0,$
    0, 255, 255,$
    255,   0, 255,$
    176,  48,  96,$
    46, 139,  87,$
    160,  32, 240,$
    255, 127,  80,$
    127, 255, 212,$
    218, 112, 214,$
    160,  82,  45,$
    127, 255,   0,$
    216, 191, 216,$
    238,   0,   0,$
    205,   0,   0,$
    139,   0,   0,$
    0, 238,   0,$
    0, 205,   0,$
    0, 139,   0,$
    0,   0, 238,$
    0,   0, 205,$
    0,   0, 139,$
    238, 238,   0,$
    205, 205,   0,$
    139, 139,   0,$
    0, 238, 238,$
    0, 205, 205,$
    0, 139, 139,$
    238,   0, 238,$
    205,   0, 205,$
    139,   0, 139,$
    238,  48, 167,$
    205,  41, 144,$
    139,  28,  98,$
    145,  44, 238,$
    125,  38, 205,$
    85,  26, 139,$
    255, 165,   0,$
    238, 154,   0,$
    205, 133,   0,$
    139,  90,   0,$
    238, 121,  66,$
    205, 104,  57,$
    139,  71,  38,$
    238, 210, 238,$
    205, 181, 205,$
    255, 255, 255]
  allColors = reform(/OVERWRITE, allColor, 3, numberOfColors)
  indicesMod = indices mod numberOfColors
  colors = allColors[*,indicesMod]
  names = allNames[indicesMod]
  return, colors
end