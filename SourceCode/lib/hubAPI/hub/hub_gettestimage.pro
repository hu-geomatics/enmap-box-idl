;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns a specific or all hubAPI test image filenames.
;    
; :Params:
;    basename: in, optional, type=file basename
;      Use this argument to specify the file basename that should be returned. If omitted, all test image filenames are returned.
;    
; :Examples:
;    Print all hubAPI test image filenames::
;      print, hub_getTestImage()
;      
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Classification-Estimation
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Classification-GroundTruth
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Classification-Training-Sample
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Classification-Validation-Sample
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Image
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Mask
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Image
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Mask
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Regression-Estimation
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Regression-GroundTruth
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Regression-Training-Sample
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Regression-Validation-Sample
;    
;    Print a specific filename::
;      print, hub_getTestImage('Hymap_Berlin-A_Image')
;      
;    IDL prints something like::
;      D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Image
;    
;    Try to get a filename that is not part of the test image set::
;      print, hub_getTestImage('MyImage')
;      
;    IDL trows an error::
;      % HUB_GETTESTIMAGE: File is not part of hubAPI test image set: D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\MyImage
;-
function hub_getTestImage, basename
  dirname = filepath('', ROOT=hub_getDirname(), SUBDIR=['resource','testData','image'])
  return, hub_getDirectoryImageFilename(dirname, basename)
end

;+
; :Hidden:
;-
pro test_hub_getTestImage
  print, hub_getTestImage()
  print, hub_getTestImage('Hymap_Berlin-A_Image')
end
