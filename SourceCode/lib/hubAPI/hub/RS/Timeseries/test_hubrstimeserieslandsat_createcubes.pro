pro test_hubRSTimeseriesLandsat_createCubes

  compile_opt idl2

  timeseriesFolder = 'G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014'
  timeseries = hubRSTimeseriesLandsat(timeseriesFolder);, hubtime(2013), hubtime(2014))

  subsetReferenceFilename = 'G:\Rohdaten\Brazil\Raster\Maps\Study_area_brasilia\PNB_IBGE_mosaic_23N.bsq'
  subsetReferenceFilename = 'G:\temp\temp_Marcel\Time_Series\rbf_Timesat\TCG\TCG_2009_2011_rbf.bsq'
  
  subset = (hubIOImgInputImage(subsetReferenceFilename)).getMapCoordinates(/BoundingBox)
;  subset = subset[[0,1,0,1]]+[0,0,30,-30]*500 ; small subset for testing
  ul = subset[0:1]+[100,-675]*30
  lr = ul+[1000,-400]*30
  subset = [ul,lr]
;  subset = subset[[0,1,0,1]]+[300,-475, 300+1000,-475-400]*30  ; 1000x400 Subset mit Marcel ausgesucht


  root = 'D:\work\AR\'
  for i=0,5 do begin
    expression = 'toa['+strtrim(i,2)+']'
    cube = timeseries.createCube(expression, Subset=subset, TargetSpacing=8, OutputTargetTime=targetTime, OutputMapInfo=mapInfo)
    hubIOImg_writeImage, cube, root+'ts_band'+strtrim(i+1,2)
  endfor
  
  expression = 'fmask'
  cube = timeseries.createCube(expression, !null, 255, Subset=subset, TargetSpacing=8, OutputTargetTime=targetTime, OutputMapInfo=mapInfo)
  hubIOImg_writeImage, cube, root+'ts_mask'

  targetTime.getProperty, SYEAR=syear, SMONTH=smonth, SDAY=sday, SDOY=sdoy, SDYEAR=sdyear, Julian=julian
  text = syear+' '+sdoy+' '+smonth+' '+sday+' '+strtrim(julian,2)+' '+sdyear ; year doy month day julian dyear
  hubIOASCIIHelper.writeFile, root+'meta.txt', text
  
  stop  
cube2=cube
  ;cube2 = cube[500:999,500:999,1:1]  
  window,0,xsize=n_elements(cube2[*,0,0]), ysize=n_elements(cube2[0,*,0])
  for i=0,n_elements(cube2[0,0,*])-1 do begin &$
    tvscl, cube2[*,*,i] >200 <3000, /Order &$
    stop &$
  endfor
  stop
end