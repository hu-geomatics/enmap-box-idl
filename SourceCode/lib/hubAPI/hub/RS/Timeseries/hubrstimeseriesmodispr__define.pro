function hubRSTimeseriesModisPR::init, scenes;, startTime, endTime
  if isa(scenes, 'string') then begin
    scenesMainFolder = filepath('COMPOSITE', ROOT_DIR=scenes)
    sceneFilenames = file_search(filepath('*DOY.dat', ROOT_DIR=scenesMainFolder))
;print, 'DEBUG hubRSTimeseriesModisPR::init'
;sceneFilenames = sceneFilenames[0:10]
    scenes_ = hubRSSceneModisPR(sceneFilenames);, startTime, endTime)  
  endif else begin
    scenes_ = scenes
  endelse
  !null = self.hubRSTimeseries::init(scenes_)
  return, 1b
end

function hubRSTimeseriesModisPR::getCloudFractions, Plot=plot
  
  compile_opt idl2
    
  histos = hash([-1,0,1,2,3])
  foreach key,histos.keys() do histos[key] = list()
  
  filenames = self.scenes.path.mask
  foreach filename,filenames, file do begin
    print, strtrim(file+1,2)+'/'+strtrim(n_elements(filenames),2)
    band = hubIOImg_readBand(filename,0)
    bandHisto = 1.*histogram(band,MIN=-1,MAX=3,BINSIZE=1, LOCATIONS=locations)/n_elements(band)
    foreach histo,histos,key do histo.add, bandHisto[where(locations eq key)]
  endforeach
  foreach key,histos.keys() do histos[key] = (histos[key]).toArray()
  
  if keyword_set(plot) then begin
    p = plot(self.scenes.time.dyear, histos[0])
    p = plot(/OVERPLOT, self.scenes.time.dyear, histos[1], COLOR='red')
    p = plot(/OVERPLOT, self.scenes.time.dyear, histos[2]+histos[3], COLOR='blue')
  endif
  return, histos
end

pro hubRSTimeseriesModisPR__define
  struct = {hubRSTimeseriesModisPR, inherits hubRSTimeseries}
end

;pro test_hubRSTimeseriesModisPR
;
;  ; init timeseries
;  timeseriesFolder = 'Y:\DAR_Data\01_Processing\Processed_Data\MODIS\MODIS13Q1\h26v06'
;  timeseries = hubRSTimeseriesModisPR(timeseriesFolder)
;  
;  ul = [8895604.1573329996, 3335851.5589999999]
;  pixelSize = [231.65636, 231.65636]
;  subset=[ul,ul+[+1,-1]*pixelSize*[4800,4800]]
;  timeseries.openInENVI, 'ndvi*(mask eq 0 or mask eq 1)', Subset=subset
;
;
;end
