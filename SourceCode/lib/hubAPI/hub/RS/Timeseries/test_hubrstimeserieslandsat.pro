pro test_hubRSTimeseriesLandsat

  compile_opt idl2

  timeseriesFolder = 'G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014'
  timeseries = hubRSTimeseriesLandsat(timeseriesFolder);, hubtime(2009), hubtime(2010))


  subsetReferenceFilename = 'G:\temp\temp_Marcel\Time_Series\SBSR_classification\Maps\PNB_IBGE_REAE_23N_byte.bsq'
  ;subsetReferenceFilename = 'G:\Rohdaten\Brazil\Raster\Maps\Study_area_brasilia\PNB_IBGE_mosaic_23N.bsq'
  subset = (hubIOImgInputImage(subsetReferenceFilename)).getMapCoordinates(/BoundingBox)
;  subset = subset[[0,1,0,1]]+[0,0,30,-30]*500 ; small subset for testing

  expressions = dictionary() 
  ;expressions.ndvi = 'float(toa[3] - toa[2]) / float(toa[3] + toa[2])'                     ; (NIR-RED)/(NIR+RED)
  ;expressions.evi  = ' 2.5*(toa[3] - toa[2]) / (10000. + toa[3] + 6.*toa[2] - 7.5*toa[0])' ; 2.5* ((NIR - RED) / (ScaleFactor + NIR + 6*RED - 7.5*BLUE))
  ;expressions.nbr  = 'float(toa[3] - toa[5]) / float(toa[3] + toa[5])'                     ; (NIR-SWIR2)/(NIR+SWIR2)
  expressions.tcg  = '-0.3344*toa[0]-0.3544*toa[1]-0.4556*toa[2]+0.6966*toa[3]-0.0242*toa[4]-0.2630*toa[5]'
  ;changed TCG coefficient to positive in Band 3 adjusted by Marcel 03.11.2014
  expressions.tcw  = '+0.2626*toa[0]+0.2141*toa[1]+0.0926*toa[2]+0.0656*toa[3]-0.7629*toa[4]-0.5388*toa[5]'
  expressions.tcb  = '+0.3561*toa[0]+0.3972*toa[1]+0.3904*toa[2]+0.6966*toa[3]+0.2286*toa[4]+0.1596*toa[5]'

  maskExpression = '(fmask eq 0) and (toa[3] ne 0) and (toa[2] ne 0)' ; clear land only
  spikeDeviation = 1. ; measured in units of global standard deviations
  targetSpacing  = 8
  
  foreach vi, expressions.keys() do begin
          ; adjusted by Marcel 03.11.2014 
          ;vi = 'TCG'
   expression = expressions[vi]
   
    filename       = 'G:\temp\temp_marcel\time_series\rbfFits\2000-2014\'+vi+'\'+vi
    timeseries.rbfFit, expression, maskExpression, spikeDeviation, targetSpacing, filename, Subset=subset, $
;      ZPlotRange=zPlotRange, DefaultStretch=defaultStretch,$
      /WriteTS, /WriteGap, /WriteDensity
  endforeach
  
  
end