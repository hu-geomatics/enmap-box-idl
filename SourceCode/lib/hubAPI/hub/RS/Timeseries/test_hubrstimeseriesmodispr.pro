pro test_hubRSTimeseriesModisPR

  compile_opt idl2

  timeseriesFolder = 'Y:\DAR_Data\01_Processing\Processed_Data\MODIS\MODIS13Q1\h26v06'
  timeseries = hubRSTimeseriesModisPR(timeseriesFolder);, hubtime(2001), hubtime(2001100))

;  subset = [0.5,0.5,0.6,0.6]
  expression     = 'ndvi'
  maskExpression = 'mask eq 0 or mask eq 1'
  
  maskValue      = 0 ;!VALUES.F_NAN
  filename       = 'Y:\DAR_Data\01_Processing\Processed_Data\MODIS\MODIS13Q1\tempTimeseries'
  zPlotRange     = [0,10000]
  defaultStretch = [0,10000]

  timeseries.openInENVI, expression, maskExpression, maskValue, filename, /ApplyMaskWithWhere, Subset=subset,$
    DefaultStretch=defaultStretch, ZPlotRange=zPlotRange,$
    /RGBSelf, NoOverwrite=0b

end