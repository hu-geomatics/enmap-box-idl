function hubRSTimeseriesSVR::train, decimalYear, y, c, gArray, loss, stop, libsvmObj
  compile_opt idl2, static

  svrs = list()
  foreach g,gArray do begin
    svrs.add, imageSVM_svrFit(transpose(decimalYear), y, c, g, loss, stop, libsvmObj)
  endforeach
  return, svrs
end

function hubRSTimeseriesSVR::apply, svrs, decimalYear
  compile_opt idl2, static
  
  decimalYear_ = hub_fix2d(transpose([decimalYear]))
  
  predictions = list(!null)
  predictionMean = fltarr(n_elements(decimalYear))
  foreach svr, svrs do begin
    prediction = (svr.apply(decimalYear_)).labels
    predictions.add, prediction
    predictionMean += prediction
  endforeach
  predictions[0] = predictionMean/svrs.count()
  return, predictions
end

pro hubRSTimeseriesSVR__define
  struct = {hubRSTimeseriesSVR, inherits IDL_Object}
end