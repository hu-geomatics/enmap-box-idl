function hubRSTimeseries_svrFit_slice, slice, maskValue, dyear, g, loss, stop, libsvmObj
  compile_opt idl2

  progressBar = hubProgressBar(Title='SVR Fit')
  sliceSize = size(/DIMENSIONS, slice)
  svrFit_slice = fltarr(sliceSize, /NOZERO)
  for x=0,sliceSize[0]-1 do begin
    progressBar.setProgress, float(x+1)/sliceSize[0]
    print, x
    values = slice[x,0,*]
    ; extract valid data
    valid = where(/NULL, values ne maskValue)
    dyearValid = transpose(dyear[valid])
    valuesValid = values[valid]
    c = 5.*mean(abs(valuesValid-mean(valuesValid)))
    svr = imageSVM_svrFit(dyearValid, valuesValid, c, g, loss, stop, libsvmObj)
    print, x, 'finished'
    estimate = (svr.apply(dyear)).labels
    svrFit_slice[x,0,*] = estimate
  endfor
  obj_destroy, progressBar
  return, svrFit_slice
end