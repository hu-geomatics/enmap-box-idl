function hubRSTimeseriesLandsat, directory, startTime, endTime
  sensors = ['LT4', 'LT5', 'LE7', 'LC8']
  sceneFolders = file_search(filepath(sensors+'*', ROOT_DIR=directory), /TEST_DIRECTORY)
  scenes = hubRSSceneLandsat(sceneFolders, startTime, endTime)
  return, hubRSTimeseries(scenes)
end



pro test_hubRSTimeseriesLandsat_453

  compile_opt idl2

  ; init timeseries
  timeseriesFolder = 'G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014'
  timeseries = hubRSTimeseriesLandsat(timeseriesFolder, hubTime(2001080), hubTime(2001082))
  
  print, timeseries.scenes

  subsetFraction     = 0.01
  subset             = [0,0,1,1]*subsetFraction+0.5-subsetFraction/2.
  defaultStretch     = envi_default_stretch_create(/LINEAR, VAL1=200, VAL2=5000)
  defaultZRange      = [0.001,10000]

  ; with mask applied
;  for i=0,5 do begin
;    expression         = 'toa['+strtrim(i,2)+']'
;    maskExpression     = 'fmask le 1'
;    outname = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+strtrim(i+1,2)
;    cube = timeseries.createCube(expression, maskExpression, SUBSET=subset, DefaultStretch=defaultStretch, DefaultZRange=defaultZRange)
;    envi_write_envi_file, cube, OUT_NAME=outname, R_FID=fidTimeseries, DESCRIP=expression, BNAMES=timeseries.scenes.name, INTERLEAVE=0, /NO_COPY, WL=timeseries.scenes.time.dyear, DEF_STRETCH=defaultStretch, ZRANGE=defaultZRange
;  endfor

  ; without mask applied
  for i=0,5 do begin
    expression         = 'toa['+strtrim(i,2)+']'
;    maskExpression     = 'fmask le 1'
    outname = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+strtrim(i+1,2)+'_noMask'
    cube = timeseries.createCube(expression, maskExpression, SUBSET=subset, DefaultStretch=defaultStretch, DefaultZRange=defaultZRange)
    envi_write_envi_file, cube, OUT_NAME=outname, R_FID=fidTimeseries, DESCRIP=expression, BNAMES=timeseries.scenes.name, INTERLEAVE=0, /NO_COPY, WL=timeseries.scenes.time.dyear, DEF_STRETCH=defaultStretch, ZRANGE=defaultZRange
  endfor

end
