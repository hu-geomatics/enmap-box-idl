function hubRSTimeseries::init, scenes
  self.scenes = scenes
  return, 1b
end

pro hubRSTimeseries::getProperty, scenes=scenes
  if arg_present(scenes) then scenes = self.scenes
end

function hubRSTimeseries::subset, first, last
  scenes = self.scenes.subset(first, last)
  timeseries = hubRSTimeseries(scenes)
  return, timeseries
end

;function hubRSTimeseries::getFilenames, product, Missing=missing
;
;  filenamesHash = orderedHash(self.time.ydoy, replicate(isa(missing)?missing:'', self.time.count))
;  
;  ; fill valid observations
;  productFilenames = (self.scenes.path)[product]
;  foreach ydoy,self.scenes.time.ydoy,i do begin
;    if ~filenamesHash.hasKey(ydoy) then continue
;    filenamesHash[ydoy] = productFilenames[i]
;  endforeach
;  filenames = (filenamesHash.values()).toArray()
;  return, filenames
;  
;end

;function hubRSTimeseries::getMissing
;
;  flag = orderedHash(self.time.ydoy, 0b)
;  foreach ydoy,self.scenes.time.ydoy,i do begin
;    if ~flag.hasKey(ydoy) then continue
;    flag[ydoy] = 1b
;  endforeach
;  invalid = (flag.values()).where(0b)
;  time = hubTimeUTC((self.time.stamp)[invalid])
;  return, time
;end

function hubRSTimeseries::createCube_parseExpressionProduct, expression, product
  compile_opt static
  found = 0b
  length = strlen(product)
  bandIndex = list()
  while 1b do begin
    ; find product name; return, if not found
    offset = strpos(expression, product)           ; "product" start index
    if offset eq -1 then break
    found = 1b
    ; find band index inside brackets [..]
    if strmid(expression, offset+length, 1) eq '[' then begin
      offsetClosing = strpos(expression, ']', offset+strlen(product))
      bandIndex.add, fix(strmid(expression, offset+length+1, offsetClosing-offset-1))
    endif else begin
      bandIndex.add, -1
    endelse
    ; delete located incidence
    expression = strmid(expression, 0, offset)+strmid(expression, offset+length)
  endwhile
  return, bandIndex
end

function hubRSTimeseries::createCube_parseExpression, expression_
  allProducts = self.scenes.path.keys()
  allProducts = allProducts[sort(strlen(allProducts))] ; parse longest names first to avoid confusion with names that are a real substrings of another name
  variables = hash()
  expression = isa(expression_) ? expression_ : ''
  foreach product, allProducts do begin
    variables[product] = self.createCube_parseExpressionProduct(expression, product)
  endforeach
  return, variables
end

function hubRSTimeseries::createCube_parseVariables, expression, maskExpression
  variables1 = self.createCube_parseExpression(expression)
  variables2 = self.createCube_parseExpression(maskExpression)
  variables = dictionary()
  foreach  product, self.scenes.path.keys() do begin
    bandIndices = (variables1[product]+variables2[product])
    if ~bandIndices.isEmpty() then begin
      bandIndices = bandIndices.toArray()
      bandIndices = bandIndices[uniq(bandIndices, sort(bandIndices))]
      variables[product] = bandIndices
    endif
  endforeach
  return, variables
end

function hubRSTimeseries::createCube, expression, maskExpression, maskValue, Subset=subset, ApplyMaskWithWhere=applyMaskWithWhere, _EXTRA=_extra, $
  TargetSpacing=targetSpacing, OutputTargetTime=targetTime, OutputMapInfo=mapInfo

  missingValue = isa(maskValue) ? maskValue : 0

  ; check _extra variables
  if isa(_extra) then begin
    productNames = self.scenes.path.keys()
    foreach value, _extra+hash(), _extraName do begin
      if isa(where(/NULL,strcmp(productNames, _extraName, /FOLD_CASE))) then begin
        message, 'Conficting _EXTRA keyword, change variable name: '+_extraName 
      endif
    endforeach
  endif
  
  ; get cube extent
  if isa(subset, 'string') then begin
    subsetImage = hubIOImgInputImage(subset)
    extent = subsetImage.getMapCoordinates(/BoundingBox)
  endif else begin
    normalized = isa(subset) ? (min(subset) ge 0) and (max(subset) le 1) : 0 
    extent = self.scenes.getExtent(subset, Normalized=normalized)
  endelse
  
  print, 'todo map info'
;  if arg_present(mapInfo) then begin
;    mapInfo = subsetImage.getMeta('map info')
;    mapInfo.pixelX = 0.00000000
;    mapInfo.pixelY = 0.00000000
;    mapInfo.easting = extent[0]
;    mapInfo.northing = extent[1]
;  endif
   
  
  ; parse expression for used products
  variables = self.createCube_parseVariables(expression, maskExpression)

  ; create cube
  
  observedTime = self.scenes.time
  if isa(targetSpacing) then begin
    targetJulian = [floor((observedTime.julian)[0])+0.5:ceil((observedTime.julian)[-1]):targetSpacing]
    targetTime = hubTimeJulian(targetJulian)
  endif else begin
    targetTime = observedTime
  endelse
  observedIndices = targetTime.locate(observedTime, Distance=distance)  
  ntimes = targetTime.count
  nfiles = observedTime.count

  for i=0,nfiles-1 do begin
    hubProgress, 'Files loaded:',i+1,nfiles
    
    ; enter variables as hashes with integer keys
    foreach bandIndices, variables, product do begin
      !null = execute(product+' = list(LENGTH=max(bandIndices)+1)')
      foreach bandIndex,bandIndices do begin
        if bandIndex ge 0 then begin
          band = hubIOImg_readBand(((self.scenes.path)[product])[i], bandIndex, SubsetRectangle=extent)
          !null = execute(product+'[bandIndex] = temporary(band)')
        endif else begin
          band = hubIOImg_readBand(((self.scenes.path)[product])[i], 0, SubsetRectangle=extent)
          !null = execute(product+' = temporary(band)')
        endelse
      endforeach
    endforeach

    ; enter _extra variables
    if isa(_extra) then begin
      foreach value, _extra+dictionary(), name do begin
        !null = execute(name+' = value')
      endforeach
    endif

    ; execute expression
    hubMath_mathErrorsOff, mathErrorState
    success = execute('band = '+expression)
    if ~success then message, 'Can not evaluate expression.'

    ; apply mask
    if isa(maskExpression) then begin
      ; execute mask expression
      success = execute('maskBand = '+maskExpression)
      if ~success then message, 'Can not evaluate mask expression.'
    
      if keyword_set(applyMaskWithWhere) then begin
        band[where(/NULL, ~maskBand)] = missingValue
      endif else begin
        band *= maskBand
        if isa(maskValue) then band += (~maskBand)*missingValue
      endelse
    endif

    hubMath_mathErrorsOn, mathErrorState
    
    ; init cube after evaluating the first band
    if i eq 0 then begin
      cube = make_array([size(/DIMENSIONS, band),ntimes], TYPE=size(/TYPE, band), VALUE=missingValue)
    endif

    cube[0,0,observedIndices[i]] = band
  endfor

  return, cube
 
end

pro hubRSTimeseries::openInENVI, expression, maskExpression, maskValue, filename, ApplyMaskWithWhere=applyMaskWithWhere, Subset=subset, _EXTRA=_extra,$
  RGBSelf=rgbSelf, RGBExpressions=rgbExpressions, RGBMaskExpressions=rgbMaskExpressions, RGBMaskValues=rgbMaskValues,$
  DefaultStretch=defaultStretch, RGBDefaultStretch=rgbDefaultStretch,$
  ZPlotRange=zPlotRange, NoOverwrite=noOverwrite
    
  compile_opt idl2
  
  startTime = hubTime()
  stackTimeseries  = filename
  stackChips       = filename+'_chips'+['R','G','B']

  ; create timestacks
  if ~keyword_set(noOverwrite) then begin
    cube = self.createCube(expression, maskExpression, maskValue, Subset=subset, _EXTRA=_extra)
    outputImage = hubIOImgOutputImage(stackTimeseries)
    outputImage.setMeta, 'description', expression
    outputImage.setMeta, 'band names', self.scenes.name
    outputImage.setMeta, 'wavelength', self.scenes.time.dyear
    outputImage.setMeta, 'z plot range',  ZPlotRange
    outputImage.setMeta, 'default stretch', strjoin(defaultStretch, ' ')+' linear'
    outputImage.writeImage, cube

    
    ; create rgb stack
    if ~keyword_set(rgbSelf) then begin
      for channel=0,2 do begin
        cube = self.createCube(rgbExpressions[channel], Subset=subset, _EXTRA=_extra,$
                                      isa(rgbMaskExpressions) ? rgbMaskExpressions[channel] : !null,$
                                      isa(rgbMaskValues)?       rgbMaskValues[channel]      : !null)

        outputImage = hubIOImgOutputImage(stackChips[channel])
        outputImage.setMeta, 'description', expression
        outputImage.setMeta, 'band names', self.scenes.name
        outputImage.setMeta, 'wavelength', self.scenes.time.dyear
        outputImage.setMeta, 'z plot range',  ZPlotRange
        outputImage.setMeta, 'default stretch', strjoin(rgbDefaultStretch, ' ')+' linear'
        outputImage.writeImage, cube
      endfor
    endif
    print, 'Timestack and ChipStacks created in '+ startTime.elapsed(/Report)
  endif
  
  ; open files in envi
  envi
  envi_open_file, stackTimeseries, R_FID=fidTimeseries
  fidRGB = lonarr(3)  
  if keyword_set(rgbSelf) then begin
    fidRGB[*] = fidTimeseries
    rgbDefaultStretch = defaultStretch
  endif else begin
    foreach stackChip,stackChips,i do begin
      envi_open_file, stackChip, R_FID=ifidRGB
      fidRGB[i] = ifidRGB
    endforeach
  endelse
  
  ; start viewer
  viewer = hubProEnvEnviTimeseriesViewer(fidTimeseries, fidRGB,$
    Expression=expression,         MaskExpression=maskExpression,         MaskValue=maskValue,$
    RGBExpressions=rgbExpressions, RGBMaskExpressions=rgbMaskExpressions, RGBMaskValues=rgbMaskValues,$
    DefaultStretch=defaultStretch, RGBDefaultStretch=rgbDefaultStretch,$
    DefaultZRange=defaultZRange)
end

function hubRSTimeseries::rbfFit_fitCube, yCube, spikeDeviation, startTime,$
  OutputDensityCube=densityCubeTotal, OutputOutliers=outliers

  compile_opt idl2, static
  
  if ~isa(startTime) then startTime = hubTime()
  
  cubeSize = size(/DIMENSIONS, yCube)
  
;  ; fit data using CONVOL with smoother RBF Kernel
;  rbf = gaussian_function(5, /NORMALIZE) ; rbf kernel for +/- 3xSigma
;  rbf = reform(/OVERWRITE, rbf, 1, 1, n_elements(rbf))
;  fitCube = convol(yCube, rbf, /CENTER, /EDGE_TRUNCATE, $
;    INVALID=missingValue,$  ; exclude missing values
;    /NAN,$                  ; exclude NaN/Inf missing values
;    MISSING=missingValue,$  ; fill data caps inside the result with NaN
;    /NORMALIZE)             ; normalize filter weights to handle invalid/NaN/Inf cases correctly
;  print, 'RBF Fit Sigma=5 for Outlier-Detection '+ startTime.elapsed(/Report)
;
;;print, densityCube
;  ; eliminate outliers
;  residualMax = 0.15*1000
;  residualCube = abs(fitCube-yCube)
;  yCube[where(/NULL, residualCube gt residualMax)] = missingValue
;  print, 'Outlier eliminated '+ startTime.elapsed(/Report)

  ; outlier detection using deviation from median
  
;  ; - calculate standard deviations
  yBandStddev = stddev(yCube, DIMENSION=3, /NAN) ; standard deviation for each pixel
  yCubeStddev = rebin(/SAMPLE, yBandStddev, cubeSize) ; expand to cube size
;  
;  ; - calculate moving-window median 
;  yCubeExtended = make_array(cubeSize+[0,0,12], VALUE=!VALUES.F_NAN) ; need to extent 6 bands at the beginning and 6 bands at the end for the median filter
;  yCubeExtended[0,0,6] = yCube
;  yVector = reform(/OVERWRITE, temporary(yCubeExtended), cubeSize[0]*cubeSize[1]*(cubeSize[2]+12))  ; 1d vector
;  yVectorMedian = median(yVector,13) ; filter 1d vector (NaN is treated as missing data)
;  yCubeMedianExtended = temporary(yVectorMedian)
;  yCubeMedianExtended = reform(/OVERWRITE, yCubeMedianExtended, cubeSize+[0,0,12])
;  yCubeMedian = (temporary(yCubeMedianExtended))[*,*,6:6+cubeSize[2]-1]
;  
  
  rbf = gaussian_function(5, /NORMALIZE)
  rbf = reform(/OVERWRITE, rbf, 1, 1, n_elements(rbf))
  yCubeRBF = convol(yCube, rbf, /CENTER, /EDGE_TRUNCATE, /NAN, MISSING=0, /NORMALIZE)
  
  ; - exclude outliers
  yCubeResiduals = abs(temporary(yCubeRBF) - yCube)
  yCubeResidualsInStdev = temporary(yCubeResiduals) / yCubeStddev
  outliers = where(/NULL, yCubeResidualsInStdev ge spikeDeviation)
  yCube[outliers] = !VALUES.F_NAN
  
  print, 'Outlier eliminated '+ startTime.elapsed(/Report)
  
  ; fit data with different kernel sizes -> weighted ensemble
;  rbfSigmas = [3];,5,7];,11,13];,50];,cubeSize[2]/6-1]
  rbfSigmas = [3,5];,5,7];,11,13];,50];,cubeSize[2]/6-1]

  presenceCube = fix(finite(yCube))

  fitCube = fltarr(cubeSize)
  densityCubeTotal = fltarr(cubeSize)
  foreach rbfSigma,rbfSigmas,i do begin

    rbf = gaussian_function(rbfSigma, /NORMALIZE)
    rbf = reform(/OVERWRITE, rbf, 1, 1, n_elements(rbf))

    ; estimate density
    avg = replicate(1, 1, 1, n_elements(rbf))
    idensityCube = convol(presenceCube, avg, /CENTER, /EDGE_TRUNCATE)
  ;  idensityCube *= (n_elements(rbfSigmas)-i)^2
    print, 'Density Estimated with Sigma: '+strtrim(rbfSigma,2)+', ', startTime.elapsed(/Report)

    ; fit data
    ifitCube = convol(yCube, rbf, /CENTER, /EDGE_TRUNCATE, INVALID=missingValue, /NAN, MISSING=0, /NORMALIZE)
    print, 'RBF Fitted with Sigma: '+strtrim(rbfSigma,2)+', ', startTime.elapsed(/Report)
    
    ; add weighted ifit to fit 
    ifitCube *= idensityCube
    fitCube += ifitCube
    densityCubeTotal += idensityCube
  endforeach
  fitCube /= densityCubeTotal
  print, 'RBF Ensemble fit done '+startTime.elapsed(/Report)
  
  return, fitCube
end

pro hubRSTimeseries::rbfFit, expression, maskExpression, spikeDeviation, targetSpacing, filename, Subset=subset, $
  ZPlotRange=zPlotRange, DefaultStretch=defaultStretch,$
  WriteTS=writeTS, WriteGap=writeGap, WriteDensity=writeDensity

  compile_opt idl2

  filenameTS  = filename+'_ts'
  filenameFit = filename+'_rbfFit'
  filenameGap = filename+'_gap'
  filenameDensity = filename+'_density'

  startTime = hubTime()

  debug = 0b
  if debug then begin
    print, 'DEBUG: open yCube from File'
    yCube = hubIOImg_readImage(filenameTS,Header=header)
    targetTime = hubTime(header.getMeta('wavelength'))
    mapInfo    = header.getMeta('map info')
  endif else begin
  
    ; read data always as float and set maskValues always to NaN
    yCube = self.createCube('float('+expression+')', maskExpression, !VALUES.F_NAN,$
      Subset=subset, /ApplyMaskWithWhere, TargetSpacing=targetSpacing, OutputTargetTime=targetTime, OutputMapInfo=mapInfo)
    print, 'Timestack created '+ startTime.elapsed(/Report)
  
  endelse

  ; calculate default strech from data
  !null = moment(yCube, MAXMOMENT=2, MEAN=m, SDEV=s, /NAN)
  if ~isa(defaultStretch) then begin
    defaultStretch = m+1*[-s,+s]
  endif
  if ~isa(zPlotRange) then begin
    ZPlotRange    = m+1*[-s,+s]
  endif

  if keyword_set(writeTS) then begin
    outputImage = hubIOImgOutputImage(filenameTS)
    outputImage.setMeta, 'description', 'Original Timeseries'
    outputImage.setMeta, 'map info', mapInfo
    outputImage.setMeta, 'band names', targetTime.stamp
    outputImage.setMeta, 'wavelength', targetTime.dyear
    outputImage.setMeta, 'z plot range',  ZPlotRange
    outputImage.setMeta, 'default stretch', strjoin(defaultStretch, ' ')+' linear'
    outputImage.writeImage, fix(yCube)
    print, 'immer als INT!!!'
    print, 'Timestack saved '+ startTime.elapsed(/Report)
    print, '  '+filenameTS
  endif

  ; fit RBF-CONVOL
  fitCube = self.rbfFit_fitCube(yCube, spikeDeviation, startTime, OutputDensityCube=densityCubeTotal)
  
  ; save fitted image
  outputImage = hubIOImgOutputImage(filenameFit)
  outputImage.setMeta, 'description', 'SVR Fit'
  outputImage.setMeta, 'map info', mapInfo
  outputImage.setMeta, 'band names', targetTime.stamp
  outputImage.setMeta, 'wavelength', targetTime.dyear
  outputImage.setMeta, 'z plot range',  ZPlotRange
  outputImage.setMeta, 'default stretch', strjoin(defaultStretch, ' ')+' linear'
  outputImage.writeImage, fix(fitCube)
  print, 'RBF-CONVOL fit saved '+ startTime.elapsed(/Report)

  ; save density image
  if keyword_set(writeDensity) then begin
    outputImage = hubIOImgOutputImage(filenameDensity)
    outputImage.setMeta, 'description', 'Observation Density'
   outputImage.setMeta, 'map info', mapInfo
    outputImage.setMeta, 'band names', targetTime.stamp
    outputImage.setMeta, 'wavelength', targetTime.dyear
    outputImage.setMeta, 'default stretch', '0 '+strtrim(max(densityCubeTotal),2)+' linear'
    outputImage.writeImage, densityCubeTotal
    print, 'Density image saved '+ startTime.elapsed(/Report)
  endif

  ; save gap image
  if keyword_set(writeGap) then begin
    outputImage = hubIOImgOutputImage(filenameGap)
    outputImage.setMeta, 'description', 'Gab image'
    outputImage.setMeta, 'map info', mapInfo
    outputImage.setMeta, 'default stretch', '0 1 linear'
    outputImage.writeImage,  product(finite(fitCube),3)
    print, 'Gap image saved '+ startTime.elapsed(/Report)
  endif


envi
envi_open_file, filenameTS,      R_FID=fid1 
envi_open_file, filenameFit,     R_FID=fid2
envi_open_file, filenameGap,     R_FID=fid3
envi_open_file, filenameDensity, R_FID=fid4

envi_display_bands, fid1, 0, /NEW
envi_display_bands, fid2, 0, /NEW
envi_display_bands, fid3, 0, /NEW
envi_display_bands, fid4, 0, /NEW




end

function hubRSTimeseries::_overloadPrint
  return, self.scenes._overloadPrint()
end

pro hubRSTimeseries__define
  struct = {hubRSTimeseries, inherits IDL_Object,$
    scenes : obj_new()}
end

