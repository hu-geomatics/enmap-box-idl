pro test_hubRSTimeseriesLandsat2

  compile_opt idl2

  ; init timeseries
  timeseriesFolder = 'G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014'
  timeseries = hubRSTimeseriesLandsat(timeseriesFolder);, hubtime(2001), hubtime(2002))

  subset = [168479.882, -1724149.650, 199568.485, -1777000.276] ; brasilia bigSubset
    
    npx = 100
    subset = [[168479.882, -1724149.650], [168479.882, -1724149.650]+[+30,-30]*npx] ; brasilia bigSubset first npx*npx pixel    


;  subset = 'G:\Rohdaten\Brazil\Raster\Maps\Study_area_brasilia\PNB_IBGE_mosaic.bsq'
  


  expression         = 'toa[3]'
  maskExpression     = 'fmask le 1' ; clear land  and water
  defaultStretch     = [200, 2500]
  maxResidual        = 200 ; 1% reflectance
  timeseries.rbfFit, expression, maskExpression, maxResidual, SUBSET=subset, $
    DefaultStretch=defaultStretch,$
    OutputFilename='G:\temp\temp_ar\testTOA\tsTOA4',$
    TargetSpacing=8

end