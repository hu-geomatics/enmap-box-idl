function hubRSTimeseriesHyperion, directory, startTime, endTime, Skip=skip
  sensors = ['EO']
  sceneFolders = file_search(filepath(sensors+'*1T', ROOT_DIR=directory), /TEST_DIRECTORY)
  scenes = hubRSSceneHyperion(sceneFolders, startTime, endTime)
  ; exclude scenes with missing *_L1T.dat file
  scenes = hubRSSceneHyperion(sceneFolders[where(/NULL, file_test(scenes.path.data))])
  return, hubRSTimeseries(scenes)
end

pro test_hubRSTimeseriesHyperion
  
  compile_opt idl2
  
  ; init timeseries
  timeseriesFolder = 'G:\Rohdaten\Brazil\Raster\Hyperion\REAE'
  skip = ['EO1H2210712006241110P0_1T']
  timeseries = hubRSTimeseriesHyperion(timeseriesFolder, Skip=skip);, hubTime(2000),hubTime(2006240))

  envi
  subsetFraction = 0.3
  subset = [0,0,1,1]*subsetFraction+0.5-subsetFraction/2.
  expression         = 'float(data[71]-data[30])/float(data[71]+data[30])' ; ndviIndices=(71,30)
 ; maskExpression     = 'mask'
  rgbExpressions     = ['data[71]','data[30]','data[19]']       ; cirIndices=(71,29,19) trueIndices=(28,19,10)
 ; rgbMaskExpressions = ['mask','mask','mask']
  defaultStretch     = envi_default_stretch_create(/LINEAR, VAL1=-1,  VAL2=1)
  rgbDefaultStretch  = envi_default_stretch_create(/LINEAR, VAL1=200, VAL2=5000)
  defaultZRange      = [0.001,1000]
  timeseries.openInENVI, expression, maskExpression, SUBSET=subset,$
    RGBExpressions=rgbExpressions, RGBMaskExpressions=rgbMaskExpressions, RGBMaskValues=rgbMaskValues,$
    DefaultStretch=defaultStretch, RGBDefaultStretch=rgbDefaultStretch, DefaultZRange=defaultZRange

end

;where = 32 seconds, 19 seconds
; hyperion EO1     H         221  071  2003152  110 K X_1T
;          satName hyperion  path row  yearDoy      P L product
; 
; G:\Rohdaten\Brazil\Raster\Hyperion\REAE\EO1H2210712003152110KX_1T