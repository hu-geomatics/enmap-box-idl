pro test_hubRSTimeseriesLandsat_viewer

  compile_opt idl2

  timeseriesFolder = 'G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014'
  timeseries = hubRSTimeseriesLandsat(timeseriesFolder);, hubtime(2009), hubtime(2010))

  subsetReferenceFilename = 'G:\Rohdaten\Brazil\Raster\Maps\Study_area_brasilia\PNB_IBGE_mosaic_23N.bsq'
  subset = (hubIOImgInputImage(subsetReferenceFilename)).getMapCoordinates(/BoundingBox)
  subset = subset[[0,1,0,1]]+[0,0,30,-30]*500 ; small subset for testing

  ndvi = 'float(toa[3] - toa[2]) / float(toa[3] + toa[2])'                     ; (NIR-RED)/(NIR+RED)
  mask = '(fmask eq 0)' ; clear land only
  
  filename = 'D:\work\AR\ndvi'
  timeseries.openInENVI, $
    'float(toa[3] - toa[2]) / float(toa[3] + toa[2])', $     ; (NIR-RED)/(NIR+RED)
    '(fmask eq 0)', $                                        ; clear land only
    0, $
    'D:\work\AR\ndvi', $
    Subset=subset, $
    RGBExpressions=['toa[3]','toa[4]','toa[2]'], $
    DefaultStretch=[0,1],$
    RGBDefaultStretch=[0,5000], $
    ZPlotRange=[0,1], $
    NoOverwrite=1
  
  
end