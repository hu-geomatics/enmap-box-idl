function hubRSSceneHyperion::init, folder, startTime, endTime
  self.products = dictionary(['data',       'mtl',        'mask'        ],$
                             ['L1T.dat'  , 'MTL_L1T.TXT','L1T_mask.dat'])
  
  ; temporal subset
  time = hubTime(strmid(file_basename(folder),10,7))
  folderSubset = folder[time.subset(/Where, startTime, endTime)]
  
  ; parse information
  name = strmid(file_basename(folderSubset),0,22)
  time = hubTime(strmid(name,10,7))
  !null = self.hubRSScene::init(folderSubset, time, name)
  return, 1b
end

function hubRSSceneHyperion::getBoundingBox
  return, self.hubRSScene::getBoundingBox('data')
end

function hubRSSceneHyperion::getPath
  path = self.hubRSScene::getPath()
  pathPrefix =  file_expand_path(self.info.folder+path_sep()+self.info.name)+'_'
  foreach product, self.products, key do begin
    path[key] = pathPrefix+product
  endforeach
  return, path
end

pro hubRSSceneHyperion__define
  struct = {hubRSSceneHyperion, inherits hubRSScene}
end

pro test_hubRSSceneHyperion
  sceneFolders = file_search('G:\Rohdaten\Brazil\Raster\Hyperion\REAE\EO1*_1T', /TEST_DIRECTORY)
  scenes = hubRSSceneHyperion(sceneFolders);, hubTime(2001), hubTime(2005))
  print, scenes.mtl.keys()
end