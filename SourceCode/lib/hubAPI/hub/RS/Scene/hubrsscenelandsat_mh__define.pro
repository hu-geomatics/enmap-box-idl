function hubRSSceneLandsat_mh::init, toaFilenames, startTime, endTime

  ; temporal subset

  year = strmid(file_basename(toaFilenames),10,4)
  doy = strmid(file_basename(toaFilenames),15,3)
  time = hubTime(year+doy)
  
  toaFilenamesSubset = toaFilenames[time.subset(/Where, startTime, endTime)]
  
  ; parse information
  name = strmid(file_basename(toaFilenames),0,18)
  year = strmid(file_basename(toaFilenamesSubset),10,4)
  doy = strmid(file_basename(toaFilenamesSubset),15,3)
  time = hubTime(year+doy)

;  self.products = dictionary(['mtl',          'fmask',   'toa'    ],$
;                             [name+'_MTL.txt','fmask.'+name+'.c3s3p10.bsq','TOA.bsq')

  folderSubset = file_dirname(toaFilenamesSubset)
  !null = self.hubRSScene::init(folderSubset, time, name)
  self.info.filenamePrefix = folderSubset+PATH_SEP()+name
  self.parseMTL
  return, 1b
end

function hubRSSceneLandsat_mh::getBoundingBox
  return, self.hubRSScene::getBoundingBox('toa')
end

function hubRSSceneLandsat_mh::getPath
  path = self.hubRSScene::getPath()
  path.toa = self.info.filenamePrefix+'_archv.bsq'
  path.mtl = self.info.filenamePrefix+'_archv_MTL.txt'
  return, path
end

pro hubRSSceneLandsat_mh__define
  struct = {hubRSSceneLandsat_mh, inherits hubRSScene}
end

pro test_hubRSSceneLandsat_mh
  mainFolder = 'I:\iREDD\data\landsat\utm\2130045' 
  toaFilenames = file_search(mainFolder,'L*archv.bsq')
  scenes = hubRSSceneLandsat_mh(toaFilenames, hubTime(2001), hubTime(2005))
  print, scenes
stop

  ;  print, fix((landsatScene.time.julian)[1:*]-(landsatScene.time.julian)[0:-2])
;  landsatScene.calculateFmask,3,3,0,10, DataType=2
 ; landsatScene.calculate, 'NDVI', Scale=1000, DataType=2
end