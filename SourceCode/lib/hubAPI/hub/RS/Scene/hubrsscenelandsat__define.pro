function hubRSSceneLandsat::init, folder, startTime, endTime
  self.products = dictionary(['mtl',    'fmask',   'toa',    'ndvi',    'nbr',    'tcg',    'tcw',    'tcb'],$
                             ['MTL.txt','MTLFmask','TOA.bsq','NDVI.bsq','NBR.bsq','TCG.bsq','TCW.bsq','TCB.bsq'])
  
  ; temporal subset
  time = hubTime(strmid(file_basename(folder),9,7))
  folderSubset = folder[time.subset(/Where, startTime, endTime)]
  
  ; parse information
  name = file_basename(folderSubset)
  time = hubTime(strmid(name,9,7))
  !null = self.hubRSScene::init(folderSubset, time, name)
;  self.parseMTL
  return, 1b
end

function hubRSSceneLandsat::getBoundingBox
  return, self.hubRSScene::getBoundingBox('toa')
end

function hubRSSceneLandsat::getPath
  path = self.hubRSScene::getPath()
  pathPrefix =  file_expand_path(self.info.folder+path_sep()+self.info.name)+'_'
  foreach product, self.products, key do begin
    path[key] = pathPrefix+product
  endforeach
  return, path
end

;pro hubRSSceneLandsat::calculateFmask, cldpix, sdpix, snpix, cldprob, DataType=dataType
;  ; "cldpix" is dilated number of pixels for cloud with default values of 3.
;  ; "sdpix" is dilated number of pixels for cloud shadow with default values of 3.
;  ; "snpix" is dilated number of pixels for snow with default values of 0.
;  ; "cldprob" is the cloud probability threshold with default values of 22.5 (range from 0~100). 
;  ; also see https://code.google.com/p/fmask/
;
;  print, 'Calculate Fmask...'
;  timeStart = hubTime()
;  
;  ; calculate Fmask
;  case n_params() of
;    0 : arguments = '3 3 0 10'
;    4 : arguments = strjoin(strtrim([cldpix, sdpix, snpix, cldprob],2), ' ')
;    else : message, 'wrong usage'
;  endcase 
;
;  fmaskExe = filepath('Fmask.exe', ROOT_DIR=fmask_getDirname(/SourceCode), SUBDIRECTORY='_resource')
;  cd, CURRENT=oldWorkDir
;  foreach folder, self.info.folder, i do begin
;    timeI = hubTime()
;    cd, folder
;;    spawn, fmaskExe+' '+arguments
;    print, '  '+ file_basename(folder)+' ('+strtrim(i+1,2)+'/'+strtrim(self.info.count,2)+') ('+timeI.elapsed(/Report)+')'
;  endforeach
;  cd, oldWorkDir
;  
;  print, '...done ('+timeStart.elapsed(/Report)+')'
;
;  ; convert to output data type
;  if isa(dataType) then begin
;    print, 'Convert Fmask data type...'
;    timeStart = hubTime()
;    path = self.getPath()
;    foreach fmaskFilename, path.fmask, i do begin
;      timeI = hubTime()
;;      inputImage = hubIOImgInputImage(fmaskFilename)
;;      outputImage = hubIOImgOutputImage(fmaskFilename)
;;      outputImage.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation, /CopySpectralInformation
;;      outputImage.writeImage, inputImage.getCube(DataType=dataType), /NoOpen
;      print, '  '+file_basename(fmaskFilename)+' ('+strtrim(i+1,2)+'/'+strtrim(self.info.count,2)+') ('+timeI.elapsed(/Report)+')'
;    endforeach
;    print, '...done ('+timeStart.elapsed(/Report)+')'
;  
;  endif
;end
;
;pro hubRSSceneLandsat::calculateNDVI, Scale=scale, DataType=dataType
;  hubMath_mathErrorsOff, oldState
;  print, 'Calculate NDVI...'
;  timeStart = hubTime()
;  path=self.getPath()
;  ndviFilenames = path.ndvi
;  toaFilenames = path.toa
;  for i=0,self.info.count-1 do begin
;    timeI = hubTime()
;    inputImage = hubIOImgInputImage(toaFilenames[i])
;    outputImage = hubIOImgOutputImage(ndviFilenames[i])
;    outputImage.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation
;    outputImage.setMeta, 'band names', 'NDVI'
;    nir = inputImage.getBand(3, DataType=4)
;    red = inputImage.getBand(2, DataType=4)
;    ndvi = (nir-red)/(nir+red)
;    hubRSSceneLandsat._format, ndvi, Scale=scale, DataType=dataType
;    outputImage.writeImage, ndvi, /NoOpen
;    print, '  '+file_basename(ndviFilenames[i])+' ('+strtrim(i+1,2)+'/'+strtrim(self.info.count,2)+') ('+timeI.elapsed(/Report)+')'
;  endfor
;  print, '...done ('+timeStart.elapsed(/Report)+')'
;  hubMath_mathErrorsOn, oldState
;end
;
;pro hubRSSceneLandsat::calculateNBR, Scale=scale, DataType=dataType
;  hubMath_mathErrorsOff, oldState
;  print, 'Calculate NBR...'
;  path=self.getPath()
;  nbrFilenames = path.nbr
;  toaFilenames = path.toa
;  timeStart = hubTime()
;  for i=0,self.info.count-1 do begin
;    timeI = hubTime()
;    inputImage = hubIOImgInputImage(toaFilenames[i])
;    outputImage = hubIOImgOutputImage(nbrFilenames[i])
;    outputImage.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation
;    outputImage.setMeta, 'band names', 'NBR'
;    b4 = inputImage.getBand(3, DataType=4)
;    b6 = inputImage.getBand(5, DataType=4)
;    nbr = (b4-b6)/(b4+b6)
;    hubRSSceneLandsat._format, nbr, Scale=scale, DataType=dataType
;    outputImage.writeImage, nbr, /NoOpen
;    print, '  '+file_basename(nbrFilenames[i])+' ('+strtrim(i+1,2)+'/'+strtrim(self.info.count,2)+') ('+timeI.elapsed(/Report)+')'
;  endfor
;  print, '...done ('+timeStart.elapsed(/Report)+')'
;  hubMath_mathErrorsOn, oldState
;end
;
;pro hubRSSceneLandsat::calculateTCG, component, Scale=scale, DataType=dataType
;  self.calculateTC, 'tcg', Scale=scale, DataType=dataType
;end
;
;pro hubRSSceneLandsat::calculateTCB, component, Scale=scale, DataType=dataType
;  self.calculateTC, 'tcb', Scale=scale, DataType=dataType
;end
;
;pro hubRSSceneLandsat::calculateTCW, component, Scale=scale, DataType=dataType
;  self.calculateTC, 'tcw', Scale=scale, DataType=dataType
;end
;
;pro hubRSSceneLandsat::calculateTC, component, Scale=scale, DataType=dataType
;  hubMath_mathErrorsOn, oldState
;  
;  name = dictionary(['tcg','tcw','tcb'], 'tasseled cap '+['greeness','wetness','brightness'])
;  coeffs = dictionary()
;  coeffs.tcg = [-0.3344, -0.3544, -0.4556, 0.6966, -0.0242, -0.2630]
;  coeffs.tcw = [ 0.2626,  0.2141,  0.0926, 0.0656, -0.7629, -0.5388]
;  coeffs.tcb = [ 0.3561,  0.3972,  0.3904, 0.6966,  0.2286,  0.1596]
;  
;  print, 'Calculate '+name[component]+'...'
;  path=self.getPath()
;  filenames = path[component]
;  toaFilenames = path.toa
;  timeStart = hubTime()
;  for i=0,self.info.count-1 do begin
;    timeI = hubTime()
;    inputImage = hubIOImgInputImage(toaFilenames[i])
;    outputImage = hubIOImgOutputImage(filenames[i])
;    outputImage.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation
;    outputImage.setMeta, 'band names', name[component]
;
;    cube = inputImage.getCube(DataType=4)
;    foreach coeff,coeffs[component],band do begin
;      cube[*,*,band] *= coeff
;    endforeach
;    tc = total(cube, 3)
;    hubRSSceneLandsat._format, tc, Scale=scale, DataType=dataType
;    outputImage.writeImage, tc, /NoOpen
;    print, '  '+file_basename(filenames[i])+' ('+strtrim(i+1,2)+'/'+strtrim(self.info.count,2)+') ('+timeI.elapsed(/Report)+')'
;  endfor
;  print, '...done ('+timeStart.elapsed(/Report)+')'
;  hubMath_mathErrorsOff, oldState
;end
;
;pro hubRSSceneLandsat::_format, data, Scale=scale, DataType=dataType
;  compile_opt static
;  data[where(~finite(data))] = 0
;  if isa(scale) then data *= scale
;  if isa(dataType) then data = fix(data, TYPE=dataType)
;end

pro hubRSSceneLandsat__define
  struct = {hubRSSceneLandsat, inherits hubRSScene}
end

pro test_hubRSSceneLandsat
  sceneFolders = file_search('G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014\L*', /TEST_DIRECTORY)
  scenes = hubRSSceneLandsat(sceneFolders, hubTime(2001), hubTime(2005))
  print, scenes


  ;  print, fix((landsatScene.time.julian)[1:*]-(landsatScene.time.julian)[0:-2])
;  landsatScene.calculateFmask,3,3,0,10, DataType=2
 ; landsatScene.calculate, 'NDVI', Scale=1000, DataType=2
end