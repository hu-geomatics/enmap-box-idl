function hubRSScene::init, folder, time, name
  self.info = dictionary()
  self.info.count = n_elements(folder)
  order = sort(time.julian)
  self.info.folder = folder[order]
  self.info.time = hubTime((time.stamp)[order], /ISO)
  self.info.name = (name)[order]
  self.info.meta = dictionary()
  return, 1b
end

function hubRSScene::getPath
  path = dictionary()
  path.root = self.info.folder
  return, path
end

function hubRSScene::getBoundingBox, product
  self.parseMeta, product
  meta = (self.info.meta)[product]
  ul =        [transpose((meta.map_info_flat.EASTING).toArray()), transpose((meta.map_info_flat.NORTHING).toArray())]
  sceneSize = [transpose((meta.samples).toArray()),               transpose((meta.lines).toArray())]
  pixelSize = [transpose((meta.map_info_flat.sizeX).toArray()),   transpose(-(meta.map_info_flat.sizeY).toArray())]
  boundingBox = hub_fix2d([ul,ul+pixelSize*sceneSize])
  return, boundingBox
end

pro hubRSScene::getProperty, info=info, forlder=folder, time=time, name=name, $
  path=path, mtl=mtl, meta=meta, boundingBox=boundingBox, count=count
  
  if arg_present(info) then info = self.info
  if arg_present(folder) then folder = self.info.folder
  if arg_present(time) then time = self.info.time
  if arg_present(name) then name = self.info.name

  if arg_present(path) then path = self.getPath()
  if arg_present(mtl) then begin
    self.parseMTL
    mtl = self.info.mtl
  endif
  if arg_present(meta) then meta = self.info.meta
  if arg_present(boundingBox) then boundingBox = self.getBoundingBox()
  if arg_present(count) then count = self.info.count
end

pro hubRSScene::parseMTL

  if self.info.hasKey('mtl') then return
  
  startTime = hubTime()
  
  mtlFilenames = (self.getPath()).mtl
  mtl = dictionary()
  foreach mtlFilename,mtlFilenames,iFile do begin
    content = hubIOASCIIHelper.readFile(mtlFilename)
    foreach line, content do begin
      split = strtrim(strsplit(line, '=', /EXTRACT),2)
      if isa(where(/NULL, split[0] eq ['END','','GROUP','END_GROUP'])) then continue
      if ~mtl.hasKey(split[0]) then begin
        mtl[split[0]] = list('', Length=n_elements(mtlFilenames))
      endif
      (mtl[split[0]])[iFile] = split[1]
    endforeach
  endforeach
  foreach value, mtl, key do begin
    mtl[key] = value.toArray()
  endforeach
  self.info.mtl = mtl
  
  print, 'MTL files parsed '+ startTime.elapsed(/Report)
end

pro hubRSScene::parseMeta, products
  foreach product,products do begin
    startTime = hubTime()
    if self.info.meta.hasKey(product) then continue ; parse only once!
    
    filenames = (self.getPath())[product]
    meta = dictionary()
    foreach filename,filenames,iFile do begin
      header = hubIOImg_getMeta(filename)
      foreach value, header.getMeta(), key do begin
        validKey = idl_validname(key, /CONVERT_ALL, /CONVERT_SPACES)
        if ~meta.hasKey(validKey) then meta[validKey] = list(Length=n_elements(filenames))
        (meta[validKey])[iFile] = value
      endforeach
    endforeach
    
    ; unfold map info dictionary
    map_info_flat = dictionary()
    foreach mapInfo, meta.map_info, iFile do begin
      foreach value, mapInfo, key do begin
        validKey = idl_validname(key, /CONVERT_ALL, /CONVERT_SPACES)
        if ~map_info_flat.hasKey(validKey) then map_info_flat[validKey] = list(Length=n_elements(filenames))
        (map_info_flat[validKey])[iFile] = value
      endforeach
    endforeach
    meta.map_info_flat = map_info_flat
    
    (self.info.meta)[product] = meta

    print, product+' ENVI header parsed '+ startTime.elapsed(/Report)
  endforeach
end

function hubRSScene::getExtent, subset, Normalized=normalized, Plot=plot

  if ~isa(subset) or keyword_set(normalized) or keyword_set(plot) then begin
    boundingBox = self.getBoundingBox()
    minValues = min(boundingBox,DIMENSION=2, MAX=maxValues)
    fullExtent = [minValues[0],maxValues[1], maxValues[2], minValues[3]]
  endif

  if isa(subset) then begin
    if keyword_set(normalized) then begin
      sizeFull = [fullExtent[2]-fullExtent[0],fullExtent[3]-fullExtent[1]]
      extent   = [fullExtent[0:1],fullExtent[0:1]] + subset*[sizeFull,sizeFull]
    endif else begin
      extent = subset
    endelse
  endif else begin
    extent = fullExtent
  endelse
  
  if keyword_set(plot) then begin
    window, 0
    color = '000000'x
    background = 'FFFFFF'x
    plot, fullExtent[[0,2,2,0,0]],fullExtent[[1,1,3,3,1]], XSTYLE=4, YSTYLE=4, THICK=2, Color=color, BACKGROUND=background
    xyouts, 0.5, 0.9, /Normal, 'UL=('+strjoin(strtrim(extent[0:1],2),',')+')   LR=('+strjoin(strtrim(extent[2:3],2),',')+')', Color=color, ALIGNMENT=0.5
    for i=0,self.info.count-1 do begin 
      iboundingBox = boundingBox[*,i] 
      oplot, iboundingBox[[0,2,2,0,0]],iboundingBox[[1,1,3,3,1]], THICK=1 , Color='000000'x
    endfor
  endif

  return, extent
end

function hubRSScene::subset, first, last
  folderSubset = (self.info.folder)[time.subset(/Where, startTime, endTime)]
  scene = obj_new(typename(self), folderSubset)
  return, scene
end

function hubRSScene::_overloadPrint
  return, transpose(self.info.time.stamp+' '+self.info.name)
end

pro hubRSScene__define
  struct = {hubRSScene, inherits IDL_Object, info:obj_new(), products:obj_new()}
end