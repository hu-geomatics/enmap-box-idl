function hubRSSceneModisPR::init, sceneFilenames

  self.products = dictionary(['doy',              'ndvi',    'mask'],$
                             ['composite_DOY.dat','NDVI.dat','pixel_reliability_16bit.dat'])
  
  folder = file_dirname(sceneFilenames)
  basename = file_basename(sceneFilenames)
  name    = [strmid(basename, 10,19)]
  time    = hubTime(strmid(name,1,7))
  !null = self.hubRSScene::init(folder, time, name)
  return, 1b
end

function hubRSSceneModisPR::getBoundingBox
  return, self.hubRSScene::getBoundingBox('doy')
end

pro hubRSSceneModisPR::getProperty, _REF_EXTRA=_extra, mtl=mtl
  self.hubRSScene::getProperty, _EXTRA=_extra
  if arg_present(mtl) then mtl = self.info.mtl
end

function hubRSSceneModisPR::getPath
  path = self.hubRSScene::getPath()
  pathPrefix =  file_expand_path(self.info.folder+path_sep()+'COMPOSITE.'+self.info.name+'_250m_08_days_')
  foreach product, self.products, key do begin
    path[key] = pathPrefix+product
  endforeach
  return, path
end

function hubRSSceneModisPR::subset, first, last
  valid = where(/NULL, self.info.time.julian ge first.julian and self.info.time.julian le last.julian, count)
  if count eq 0 then return, !null
  sceneFilenames = ((self.getPath()).doy)[valid]
  scene = hubRSSceneModisPR(sceneFilenames)
  return, scene
end

pro hubRSSceneModisPR::_format, data, Scale=scale, DataType=dataType
  compile_opt static
  data[where(~finite(data))] = 0
  if isa(scale) then data *= scale
  if isa(dataType) then data = fix(data, TYPE=dataType)
end

pro hubRSSceneModisPR__define
  struct = {hubRSSceneModisPR, inherits hubRSScene}
end
;
;pro test_hubRSSceneModisPR
;  sceneFilenames = file_search('Y:\DAR_Data\01_Processing\Processed_Data\MODIS\MODIS13Q1\h26v06\COMPOSITE\*DOY.dat')
;  scenes = hubRSSceneModisPR(sceneFilenames)
;  scenes = scenes.subset(hubTime(2001),hubTime(2001100))
;  print, scenes.time
;
;end