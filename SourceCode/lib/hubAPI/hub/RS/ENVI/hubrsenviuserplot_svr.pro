pro hubRSENVIUserPlotTimerCallback_svr, id, callbackInfo
  wset, callbackInfo.window
  
  binsize = 3./12.
  histo = histogram(callbackInfo.decimalYearValid, MIN=(callbackInfo.decimalYearGrid)[0], MAX=(callbackInfo.decimalYearGrid)[-1],BINSIZE=binsize, LOCATIONS=locations)
  oplot, locations+binsize/2, 1.*histo/10., PSYM=10, COLOR='202020'x

  if callbackInfo.plotKernel then begin
    foreach svr,callbackInfo.svrs do begin
      svScaled = (svr.libSVMOutput)['sv']
      ay =       (svr.libSVMOutput)['ay']
      b =       -((svr.libSVMOutput)['rho'])[0]
      g =        svr.imageSVMTrainingParameter.getG()
      c =        svr.imageSVMTrainingParameter.getC()
      sv =       svr.imageSVMSampleSet.scaleFeaturesBackward(svScaled)
      xGrid = callbackInfo.decimalYearGrid
      oplot, [xGrid[0],xGrid[-1]], [b,b]
      for i=0, n_elements(sv)-1 do oplot, xGrid, b+(ay[i]*exp(-g*(xGrid-sv[i])^2))/c/2
    endforeach
  endif
  if callbackInfo.plotEnsemble then begin
    foreach yGrid, callbackInfo.yGrids do begin
      oplot, callbackInfo.decimalYearGrid, yGrid, COLOR=255, THICK=2 ;, LINESTYLE=1
    endforeach
  endif
  if callbackInfo.plotSVR then begin
    oplot, callbackInfo.decimalYearGrid, callbackInfo.yGrid, COLOR=255, THICK=2
  endif




 ; oplot, callbackInfo.decimalYearValid, callbackInfo.yValid, THICK=2, LINESTYLE=1



end

pro hubRSENVIUserPlot_svr_event, event
stop
end

function hubRSENVIUserPlot_svr, decimalYearAll, yAll, bbl, bbl_list, _EXTRA=_extra
  compile_opt idl2

  ; get ids
  p_data = scope_varfetch('p_data', LEVEL=-1)
  drawID = (*p_data).win
  widget_control, (*p_data).win, GET_VALUE=window
  base = widget_info(/PARENT, drawID)
  drawSize = widget_info(/GEOMETRY, drawID) 
  widget_control, drawID, XOFFSET=100

  ; setup envi display settings
;  pInfo = self.info.pInfo.profile
;  (*pInfo).PTITLE = 'Temporal Plot'
;  (*pInfo).axis.title = ['Decimal Year','Value']
  (*p_data).MIN_VAL = 0.0001
  pPlotParams = (*p_data).p_params

  (*pPlotParams).COLOR = 1
  (*pPlotParams).LSTYLE = 0         ; 0=sold, 1=dotted, 2=dashed 3=dash dot, 4=dash dot dot, 5=long dashes, 6=no line
  (*pPlotParams).THICK = 1
  (*pPlotParams).PSYM = 4           ; 1=Plus sign (+), 2=Asterisk (*), 3=Period (.), 4=Diamond, 5=Triangle, 6=Square, 7=X
  (*pPlotParams).PSYMSIZE = 8
  (*pPlotParams).LFAC = 1           ; -1=show line, 1=hide line

  ; get GUI settings
  if widget_info(/N_CHILDREN, base) eq 2 then begin
    ; init SVR settings GUI
    subbase = widget_base(base, /COLUMN, /ALIGN_LEFT, /BASE_ALIGN_LEFT);, YOFFSET=drawSize.scr_ysize)
    !null = widget_label(subbase, VALUE='C = SD(y) x')
    !null = cw_field(subbase, /ROW, /STRING, UNAME='cFactor', VALUE='3', XSIZE=3,TITLE=' ' )
    !null = widget_label(subbase, VALUE='RBF in Month')
    !null = cw_field(subbase, /ROW, /STRING, UNAME='rbf', VALUE='3', XSIZE=10,TITLE=' ')
    !null = widget_label(subbase, VALUE='Epsilon-Loss')
    !null = cw_field(subbase, /ROW, /STRING, UNAME='loss', VALUE='0', XSIZE=5,TITLE=' ')
    !null = widget_label(subbase, VALUE='Y Range')
    !null = cw_field(subbase, /ROW, /STRING, UNAME='yrange', VALUE='0 1', XSIZE=10,TITLE=' ')
    !null = cw_bgroup(subbase, ['SVR','SVR with Kernels','Ensemble'], /COLUMN, /EXCLUSIVE, SET_VALUE=0, UNAME='plot') 
  endif
  widget_control, widget_info(base, FIND_BY_UNAME='cFactor'), GET_VALUE=cFactor
  widget_control, widget_info(base, FIND_BY_UNAME='rbf'), GET_VALUE=rbf
  widget_control, widget_info(base, FIND_BY_UNAME='loss'), GET_VALUE=loss
  widget_control, widget_info(base, FIND_BY_UNAME='yrange'), GET_VALUE=yrange
  widget_control, widget_info(base, FIND_BY_UNAME='plot'), GET_VALUE=plotSelection
  yrange = float(strsplit(/EXTRACT, yrange, ' '))
  yaxes = (((*p_data).axis)[1])
  yaxes.def_range = yrange
  yaxes.range = yrange

  (*p_data).axis[1] = yaxes
  sigmaInMonth = float(strsplit(rbf, ' ', /EXTRACT))
  gArray = 1./(2.*(sigmaInMonth/12.)^2)
  cFactor = float(cFactor[0])
  loss = float(loss[0])

  ; extract valid data
  valid = where(/NULL, yAll gt 0, nvalid)
  decimalYearValid = decimalYearAll[valid]
  yValid = yAll[valid]
  
  ; c is cFactor times stdDev of yValid
;  c = stddev(yValid)*cFactor
  ; c is cFactor times meanResiduals of yValid
  c = mean(abs(yValid-mean(yValid)))*cFactor

  


  ; train svr ensemble (multiple g)
  svrs = hubRSTimeseriesSVR.train(decimalYearValid, yValid, c, gArray, loss, 0.001)

  ; sample grid for plotting
  step = 8 ; sampling intervall in days 
  decimalYearGrid = [(decimalYearAll[0]):(decimalYearAll[-1]):1./12./30.*step] 
  yGrids = hubRSTimeseriesSVR.apply(svrs, decimalYearGrid)

  callbackInfo = dictionary()
  callbackInfo.window = window
  callbackInfo.decimalYearValid = decimalYearValid
  callbackInfo.yValid = yValid
  callbackInfo.decimalYearGrid = decimalYearGrid
  callbackInfo.yGrids = yGrids[1:*]
  callbackInfo.yGrid = yGrids[0]
  callbackInfo.plotSVR = plotSelection eq 0 or plotSelection eq 1
  callbackInfo.plotKernel = plotSelection eq 1
  callbackInfo.plotEnsemble = plotSelection eq 2
  callbackInfo.svrs = svrs

  ; can not plot the original here, because ENVI will overplot it
  ; use a timer to overplot later, after ENVI plots the original data
  id = timer.set(0.1, 'hubRSENVIUserPlotTimerCallback_svr', callbackInfo )
  return, yAll

end

pro test_hubRSENVIUserPlot_svr

  ; read data cube and meta data
  filename = 'G:\temp\temp_ar\4dirk\timestack'
  image = hubIOImgInputImage(filename)
  cube = image.getCube()
  decimalYears = image.getMeta('wavelength')

  yAll = cube[84,55,*]
 ; yAll = cube[randomu(seed, 1, /LONG) mod image.getMeta('samples'),randomu(seed, 1, /LONG) mod image.getMeta('lines'),*]

  !null = hubRSENVIUserPlot_svr(decimalYears, yAll, /StandAlone)
end

pro test_hubRSENVIUserPlot_svr2, i
  compile_opt idl2

  ; read data cube and meta data
  filename = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+strtrim(i,2)
  outFilename = filename+'Fitted'
  image = hubIOImgInputImage(filename)
  cube = image.getCube()
  decimalYears = image.getMeta('wavelength')

  !null = hubRSENVIUserPlot_svr(decimalYears, cube[0,0,*], OutDyear=xGrid, OutFit=yFitGrid)
  ntime = n_elements(xGrid)
  cubeSize = [(size(/DIMENSIONS, cube))[0:1], ntime]
  cubeFitted = make_array(cubeSize, /NOZERO)
  time = hubTime()
  for line=0,cubeSize[1]-1 do begin
    print, hubProgress('Progress:', line+1,cubeSize[1])+' '+time.elapsed(/Report)
    for sample=0,cubeSize[0]-1 do begin
      !null = hubRSENVIUserPlot_svr(decimalYears, cube[sample,line,*], OutDyear=xGrid, OutFit=yFitGrid)
      cubeFitted[sample,line,*] = yFitGrid
    endfor
    
  endfor
  envi_write_envi_file, cubeFitted, R_FID=fid, OUT_NAME=outFilename, INTERLEAVE=0, WL=xGrid, DEF_STRETCH=envi_default_stretch_create(/LINEAR, VAL1=0,  VAL2=700), ZRANGE=[0.001,1000]

end

pro test_hubRSENVIUserPlot_svr_createVideo

  compile_opt idl2

  origFilename = 'G:\temp\temp_ar\4dirk\timestack'
  fitFilename = 'G:\temp\temp_ar\4dirk\timestackFitted'

  inFilename   = 'G:\temp\temp_ar\4dirk\mosaick'
  outFilename  = 'G:\temp\temp_ar\4dirk\video2.avi'
  
  image = hubIOImgInputImage(inFilename)
  time = hubTime(image.getMeta('wavelength'))
  timestamps = strmid(time.stamp,0,10)+' / '+time.sdoy
  ntimes = image.getSpectralSize()
  size = image.getSpatialSize()
  msize = [0,60]
  isize = size*4
  psize = [size[0]*4,300]
  vsize = [size[0]*4,msize[1]+isize[1]+psize[1]]
  
  cube = image.getCube()              ;    [ns, nl, ntimes]
  video = bytarr([3,vsize,ntimes], /NOZERO)  ; [3, ns, nl, ntimes]
  
  coord = [[76,58]]
  yrange = [0,1000]
  zprofile = hubIOImg_readProfiles(origFilename, coord)
  zprofileFit = hubIOImg_readProfiles(fitFilename, coord)
  !null = window(DIMENSIONS=psize, LOCATION=[0,500], BUFFER=1)
  !null = plot(time.dyear, zprofile, SYMBOL=4, LINESTYLE=6, YRANGE=yrange, MIN_VALUE=0.0001, /CURRENT, Title='Support Vector Regression Fit',YTITLE='NDVI', SYM_THICK=1)
  !null = plot(time.dyear, zprofileFit, YRANGE=yrange, /OVERPLOT, COLOR='red', THICK=2)
  w = getwindows(/CURRENT)
  window, 0, XSIZE=vsize[0], YSIZE=vsize[1], PIXMAP=1
  for t=0,ntimes-1 do begin
    hubProgress, 'Frame',t,ntimes
    erase
    ; insert image
    TVLCT, intarr(256), indgen(256), intarr(256)
    timeband = rebin(hub_fix3d(bytscl(cube[*,*,t], 0, MIN=0, MAX=700)), [isize,3], /SAMPLE)
    timeband[*,*,[[0,2]]] *= 0 
    tv, timeband, 0, psize[1], /ORDER, TRUE=3
    ; insert zprofile
    bar = plot((time.dyear)[[t,t]], yrange, YRANGE=yrange, /OVERPLOT, COLOR='blue', THICK=2)
    plotImage = w.CopyWindow(Width=psize[0], Hight=psize[1])
    bar.Delete
    tv, plotImage, TRUE=1
    ; insert title
    xyouts, /NORMAL, 0.5, 0.94, timestamps[t], CHARSIZE=2, CHARTHICK=2, ALIGNMENT=0.5
    xyouts, /NORMAL, 0.05, 0.94, 'original NDVI', CHARSIZE=1, CHARTHICK=1, ALIGNMENT=0.0
    xyouts, /NORMAL, 0.95, 0.94, 'reconstructed NDVI', CHARSIZE=1, CHARTHICK=1, ALIGNMENT=1

    ;write videoFrame
    video[0,0,0,t] = tvrd(TRUE=1)
  endfor
  write_video, outFilename, video, VIDEO_FPS=20
  
  hubHelper.openFile, outFilename


end

pro test_hubRSENVIUserPlot_SVR3
  tmOrig = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+['1','2','3','4','5','6']
  tmFit = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+['1','2','3','4','5','6']+'Fitted'
  fidOrig = list(!null)
  fidFit = list(!null)
;  foreach file, tmOrig do begin
;    envi_open_file, file, R_FID=fid
;    fidOrig.add, fid
;  endforeach
  foreach file, tmFit do begin
    envi_open_file, file, R_FID=fid
    fidFit.add, fid
  endforeach
;  rgb = [4,5,3]
;  for i=1,6 do begin
;    envi_display_bands, (fidOrig[rgb]).toArray(), [0,0,0], NEW=1, WINDOW_STYLE=2
;  endfor
;  for i=1,6 do begin
;    envi_display_bands, fidFit[i], 0, NEW=1, WINDOW_STYLE=2
;  endfor
  
  ; get stretching stats
  
end