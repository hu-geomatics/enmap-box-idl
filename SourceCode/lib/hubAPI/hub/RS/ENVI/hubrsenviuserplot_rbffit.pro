pro hubRSENVIUserPlotTimerCallback_rbffit, id, callbackInfo
  wset, callbackInfo.window
;  oplot, callbackInfo.xGrid, callbackInfo.fitGrid, COLOR=255, THICK=2

  colors = ['FF0000'x,'00FF00'x,'0000FF'x,'FF00FF'x,'FFFF00'x,'00FFFF'x]
  oplot, callbackInfo.xGrid, callbackInfo.densityGrid/max(callbackInfo.densityGrid), PSYM=10, COLOR='505050'x
  oplot, callbackInfo.xGrid, callbackInfo.fitGrid, COLOR=colors[1], THICK=1, LINESTYLE=0
  print, 'noutliers:',n_elements(callbackInfo.outliers)
  if isa(callbackInfo.outliers) then begin
    print, 'outliers x:', (callbackInfo.xGrid)[callbackInfo.outliers]
    oplot, (callbackInfo.xGrid)[callbackInfo.outliers], (callbackInfo.ygrid)[callbackInfo.outliers], PSYM=1, COLOR='00FF00'x
  endif

end

function hubRSENVIUserPlot_rbffit, decimalYearAll, yAll, bbl, bbl_list, _extra=_extra

  compile_opt idl2

  ; set nicer plot settings
  p_data = scope_varfetch('p_data', LEVEL=-1)
  pPlotParams = (*p_data).p_params
  (*pPlotParams).COLOR = 1
  (*pPlotParams).LSTYLE = 0         ; 0=sold, 1=dotted, 2=dashed 3=dash dot, 4=dash dot dot, 5=long dashes, 6=no line
  (*pPlotParams).THICK = 1
  (*pPlotParams).PSYM = 4           ; 1=Plus sign (+), 2=Asterisk (*), 3=Period (.), 4=Diamond, 5=Triangle, 6=Square, 7=X
  (*pPlotParams).PSYMSIZE = 8
  (*pPlotParams).LFAC = 1           ; -1=show line, 1=hide line

  ; extract valid observed data
  valid = where(/NULL, finite(yAll))
  decimalYearValid = decimalYearAll[valid]
  yValid = float(yAll[valid])

  ; create pseudo-dense timeseries, data gaps are filled with NaN
  timeValid = hubTime(decimalYearValid)
  julian = round(timeValid.julian)
  xOffsetObserved = julian-julian[0]
  histo = histogram(xOffsetObserved, MIN=0, BINSIZE=8, LOCATIONS=xGridOffset)  ; binSize of 8 makes only sense for LANDSAT!!!!
  timeFirst = hubTime((timeValid.ydoy)[0])
  timeGrid = timeFirst.plus(xGridOffset, 'days')
  xGrid = timeGrid.dyear
  nsample = n_elements(histo)
  indicesObserved = where(histo ne 0) 
  yGrid = make_array(nsample, VALUE=!VALUES.F_NAN)
  yGrid[indicesObserved] = yValid
  ;print, tring(transpose(xgrid))+string(transpose(ygrid))
  
  ; fit data
  yCube = reform(yGrid, 1, 1, n_elements(yGrid))
  spikeDeviation = 1.
  startTime = hubTime()
  fitCube = hubRSTimeseries.rbfFit_fitCube(yCube, spikeDeviation, startTime, OutputDensityCube=densityCube, OutputOutliers=outliers)
  fitGrid = fitCube[*]
  densityGrid = densityCube[*]

  ; setup callback information
  widget_control, (*p_data).win, GET_VALUE=window
  callbackInfo = dictionary()
  callbackInfo.window = window
  callbackInfo.xGrid = xGrid
  callbackInfo.yGrid = yGrid
  callbackInfo.fitGrid = fitGrid
  callbackInfo.densityGrid = densityGrid
  callbackInfo.outliers = outliers
  ; can not plot the original here, because ENVI will overplot it
  ; use a timer to overplot later, after ENVI plots the original data
  id = timer.set(0.1, 'hubRSENVIUserPlotTimerCallback_rbffit', callbackInfo )
  return, yAll

end
