function test_createVideo_getImageNormalizedDifferenceIndex, imageA, imageB, pos
  compile_opt idl2
  ;  stretch = (strsplit(image.getmeta('default stretch'), ' ', /EXTRACT))[0:1]*1.
  cubeA = imageA.getCube(SubsetBandPositions=[pos,pos,pos], DataType='float')
  cubeB = imageB.getCube(SubsetBandPositions=[pos,pos,pos], DataType='float')
  cube  = (cubeA-cubeB)/(cubeA+cubeB)
  cube = bytscl(cube, MIN=0.1, MAX=0.9)
  cube = rebin(/SAMPLE, cube, size(/DIMENSIONS, cube)*[6,6,1])
  cube[*,0,*] = 0 & cube[*,-1,*] = 0 & cube[0,*,*] = 0 & cube[-1,*,*] = 0
  cube[*,*,0] = 0
  cube[*,*,2] = 0

  return, cube
end

function test_createVideo_getImageGrey, image, pos
  compile_opt idl2
  stretch = (strsplit(image.getmeta('default stretch'), ' ', /EXTRACT))[0:1]*1.
  cube = image.getCube(SubsetBandPositions=[pos,pos,pos])
  cube = bytscl(cube, MIN=stretch[0], MAX=stretch[1])
  cube[*,0,*] = 0 & cube[*,-1,*] = 0 & cube[0,*,*] = 0 & cube[-1,*,*] = 0   
  return, cube
end

function test_createVideo_getImageRGB, images, pos
  compile_opt idl2
  stretchR = (strsplit((images[0]).getmeta('default stretch'), ' ', /EXTRACT))[0:1]*1.
  stretchG = (strsplit((images[1]).getmeta('default stretch'), ' ', /EXTRACT))[0:1]*1.
  stretchB = (strsplit((images[2]).getmeta('default stretch'), ' ', /EXTRACT))[0:1]*1.
  stretchMean = (stretchR+stretchG+stretchB)/3
  stretchMinMax = [min([stretchR,stretchG,stretchB]),max([stretchR,stretchG,stretchB])]
  stretchR = stretchMinMax
  stretchG = stretchMinMax
  stretchB = stretchMinMax

  
  r = (images[0]).getCube(SubsetBandPositions=pos)
  b = (images[1]).getCube(SubsetBandPositions=pos)
  g = (images[2]).getCube(SubsetBandPositions=pos)
  cube = [[[bytscl(r, MIN=stretchR[0], MAX=stretchR[1])]],$
          [[bytscl(g, MIN=stretchG[0], MAX=stretchG[1])]],$
          [[bytscl(b, MIN=stretchB[0], MAX=stretchB[1])]]]
  cube = rebin(/SAMPLE, cube, size(/DIMENSIONS, cube)*[2,2,1])
  cube[*,0,*] = 0 & cube[*,-1,*] = 0 & cube[0,*,*] = 0 & cube[-1,*,*] = 0
  return, cube
end

pro test_createVideo

  compile_opt idl2

  withMask = 0b

  outFilename       = 'G:\temp\temp_ar\4dirk\tmbands\video'
  tmOrigWithMask    = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+['1','2','3','4','5','6']
  tmOrigWithoutMask = tmOrigWithMask+'_noMask'

  if withMask then begin
    outFilename += '.avi'
    tmOrig       = tmOrigWithMask
  endif else begin
    outFilename += '_noMask.avi'
    tmOrig       = tmOrigWithoutMask
  endelse
  tmFit        = 'G:\temp\temp_ar\4dirk\tmbands\timestack'+['1','2','3','4','5','6']+'Fitted'
  tmOrigImages = list()
  tmFitImages  = list()
  tmBandNames  = ['Blue', 'Green', 'Red', 'NIR', 'SWIR1', 'SWIR2']

  for i=0,5 do begin
    tmOrigImages.add, hubIOImgInputImage(tmOrig[i])
    tmFitImages.add, hubIOImgInputImage(tmFit[i])
  endfor

  image0 = tmOrigImages[0] ; use this image for querying meta infoemation
  time = hubTime(image0.getMeta('wavelength'))
  timestamps = strmid(time.stamp,0,10)+' / '+time.sdoy
  ntimes = image0.getSpectralSize()
  imageSize = image0.getSpatialSize()
  zplotYSize = 300
  titleYSize = 60
  videoSize = imageSize*[6+6,6] + [0,titleYSize+zplotYSize]
  zplotYSize = [videoSize[0], zplotYSize]
  
  tmStretches = list()
  foreach image,tmOrigImages do begin
    tmStretches.add, (strsplit(image.getmeta('default stretch'), ' ', /EXTRACT))[0:1]*1.
  endforeach

  video = bytarr([3,videoSize,ntimes], /NOZERO)  ; [3, ns, nl, ntimes]

  ; init window for video frame 
  window, 0, XSIZE=videoSize[0], YSIZE=videoSize[1], PIXMAP=1

  ; init window for ndvi zplot (ndvi = (nir-red)/(nir+red))
  coord = [[76,58]]
  yrange = [0,1]
  margin = [0.05,0.1,0.,0.]  ; [left, bottom, right, top] 
  
  zprofileNir    = hubIOImg_readProfiles(tmOrigWithMask[3], coord)
  zprofileRed    = hubIOImg_readProfiles(tmOrigWithMask[2], coord)
  zprofileFitNir = hubIOImg_readProfiles(tmFit[3], coord)
  zprofileFitRed = hubIOImg_readProfiles(tmFit[2], coord)
  
  zprofileNDVI    = float(zprofileNir-zprofileRed)/float(zprofileNir+zprofileRed)
  zprofileNDVIFit = float(zprofileFitNir-zprofileFitRed)/float(zprofileFitNir+zprofileFitRed)
  zprofileNDVI[where(/NULL, ~finite(zprofileNDVI))] = 0
  !null = window(DIMENSIONS=zplotYSize, LOCATION=[0,500], BUFFER=1)
  !null = plot(time.dyear, zprofileNDVI, MARGIN=margin,SYMBOL=4, LINESTYLE=6, YRANGE=yrange, MIN_VALUE=0.0001, /CURRENT, Title='Support Vector Regression Fit',YTITLE='NDVI', SYM_THICK=1)
  !null = plot(time.dyear, zprofileNDVIFit, YRANGE=yrange, /OVERPLOT, COLOR='red', THICK=2)
  w = getwindows(/CURRENT)


  for t=0,ntimes-1 do begin
    
;for t=10,ntimes-1 do begin
    
    
   hubProgress, 'Frame',t+1,ntimes
   
    erase
    
    ; print title
    xyouts, /NORMAL, 0.5, 0.95, timestamps[t], CHARSIZE=2, CHARTHICK=6, ALIGNMENT=0.5, COLOR=255
    xyouts, /NORMAL, 0.5, 0.95, timestamps[t], CHARSIZE=2, CHARTHICK=2, ALIGNMENT=0.5

    
    ; plot origigal TM bands 
    for i=0,5 do begin
      chip = test_createVideo_getImageGrey(tmOrigImages[i], t)
      tvscl, chip, i*imageSize[0], videoSize[1]-imageSize[1]-titleYSize, /ORDER, TRUE=3
    endfor

    ; plot reconstructed TM bands
    for i=0,5 do begin
      chip = test_createVideo_getImageGrey(tmFitImages[i], t)
      tvscl, chip, i*imageSize[0], videoSize[1]-2*imageSize[1]-titleYSize, /ORDER, TRUE=3
    endfor
    
    ; print band names
    for i=0,5 do begin
      xyouts, /DEVICE, i*imageSize[0]+imageSize[0]/2, videoSize[1]-imageSize[1]-titleYSize-8, tmBandNames[i], CHARSIZE=2, CHARTHICK=6, ALIGNMENT=0.5, COLOR=255
      xyouts, /DEVICE, i*imageSize[0]+imageSize[0]/2, videoSize[1]-imageSize[1]-titleYSize-8, tmBandNames[i], CHARSIZE=2, CHARTHICK=2, ALIGNMENT=0.5
    endfor
    
    ; plot original TM composites
    poss = list([3,2,1],[2,1,0],[3,4,2])
    foreach pos,poss,i do begin
      chip = test_createVideo_getImageRGB(tmOrigImages[pos], t)
      tvscl, chip, i*2*imageSize[0], videoSize[1]-4*imageSize[1]-titleYSize, /ORDER, TRUE=3
    endforeach

    ; plot reconstructed TM composites
    poss = list([3,2,1],[2,1,0],[3,4,2])
    foreach pos,poss,i do begin
      chip = test_createVideo_getImageRGB(tmFitImages[pos], t)
      tvscl, chip, i*2*imageSize[0], videoSize[1]-6*imageSize[1]-titleYSize, /ORDER, TRUE=3
    endforeach

    ; print composites band names
    labels = ['CIR','True','453']
    for i=0,2 do begin
      xyouts, /DEVICE, (i+0.5)*2*imageSize[0], videoSize[1]-4*imageSize[1]-titleYSize-8, labels[i], CHARSIZE=2, CHARTHICK=6, ALIGNMENT=0.5, COLOR=255
      xyouts, /DEVICE, (i+0.5)*2*imageSize[0], videoSize[1]-4*imageSize[1]-titleYSize-8, labels[i], CHARSIZE=2, CHARTHICK=2, ALIGNMENT=0.5
    endfor

    ; plot NDVI
    chip = test_createVideo_getImageNormalizedDifferenceIndex(tmFitImages[3], tmFitImages[2], t)
    tvscl, chip, 6*imageSize[0], videoSize[1]-6*imageSize[1]-titleYSize, /ORDER, TRUE=3

    ; print NDVI label
    for i=0,2 do begin
      xyouts, /DEVICE, 9*imageSize[0], videoSize[1]-3*imageSize[1]-titleYSize-8, 'NDVI', CHARSIZE=2, CHARTHICK=6, ALIGNMENT=0.5, COLOR=255
      xyouts, /DEVICE, 9*imageSize[0], videoSize[1]-3*imageSize[1]-titleYSize-8, 'NDVI', CHARSIZE=2, CHARTHICK=2, ALIGNMENT=0.5
    endfor

    ; plot NDVI zprofile
    bar = plot((time.dyear)[[t,t]], yrange, YRANGE=yrange, /OVERPLOT, COLOR='blue', THICK=2)
    plotImage = w.CopyWindow(Width=zplotYSize[0], Hight=zplotYSize[1])
    bar.Delete
    tv, plotImage, TRUE=1


    ;pro hubRSSceneLandsat::calculateTC, component, Scale=scale, DataType=dataType
    ;  hubMath_mathErrorsOn, oldState
    ;
    ;  name = dictionary(['tcg','tcw','tcb'], 'tasseled cap '+['greeness','wetness','brightness'])
    ;  coeffs = dictionary()
    ;  coeffs.tcg = [-0.3344, -0.3544, -0.4556, 0.6966, -0.0242, -0.2630]
    ;  coeffs.tcw = [ 0.2626,  0.2141,  0.0926, 0.0656, -0.7629, -0.5388]
    ;  coeffs.tcb = [ 0.3561,  0.3972,  0.3904, 0.6966,  0.2286,  0.1596]
  

    ;write videoFrame
    video[0,0,0,t] = tvrd(TRUE=1)
;wset
;return

  endfor

stop
br = 2000
write_video, 'G:\temp\temp_ar\4dirk\tmbands\video_fps10.mp4', video, VIDEO_FPS=10, BIT_RATE=1080


  write_video, outFilename, video, VIDEO_FPS=20, BIT_RATE=1080
  hubHelper.openFile, outFilename
  
  
  
  return

  cube = image.getCube()              ;    [ns, nl, ntimes]
  video = bytarr([3,vsize,ntimes], /NOZERO)  ; [3, ns, nl, ntimes]

  coord = [[76,58]]
  yrange = [0,1000]
  zprofile = hubIOImg_readProfiles(origFilename, coord)
  zprofileFit = hubIOImg_readProfiles(fitFilename, coord)
  !null = window(DIMENSIONS=psize, LOCATION=[0,500], BUFFER=1)
  !null = plot(time.dyear, zprofile, SYMBOL=4, LINESTYLE=6, YRANGE=yrange, MIN_VALUE=0.0001, /CURRENT, Title='Support Vector Regression Fit',YTITLE='NDVI', SYM_THICK=1)
  !null = plot(time.dyear, zprofileFit, YRANGE=yrange, /OVERPLOT, COLOR='red', THICK=2)
  w = getwindows(/CURRENT)
  window, 0, XSIZE=vsize[0], YSIZE=vsize[1], PIXMAP=1
  for t=0,ntimes-1 do begin
    hubProgress, 'Frame',t,ntimes
    erase
    ; insert image
    TVLCT, intarr(256), indgen(256), intarr(256)
    timeband = rebin(hub_fix3d(bytscl(cube[*,*,t], 0, MIN=0, MAX=700)), [isize,3], /SAMPLE)
    timeband[*,*,[[0,2]]] *= 0
    tv, timeband, 0, psize[1], /ORDER, TRUE=3
    ; insert zprofile
    bar = plot((time.dyear)[[t,t]], yrange, YRANGE=yrange, /OVERPLOT, COLOR='blue', THICK=2)
    plotImage = w.CopyWindow(Width=psize[0], Hight=psize[1])
    bar.Delete
    tv, plotImage, TRUE=1
    ; insert title
    xyouts, /NORMAL, 0.5, 0.94, timestamps[t], CHARSIZE=2, CHARTHICK=2, ALIGNMENT=0.5
    xyouts, /NORMAL, 0.05, 0.94, 'original NDVI', CHARSIZE=1, CHARTHICK=1, ALIGNMENT=0.0
    xyouts, /NORMAL, 0.95, 0.94, 'reconstructed NDVI', CHARSIZE=1, CHARTHICK=1, ALIGNMENT=1

    ;write videoFrame
    video[0,0,0,t] = tvrd(TRUE=1)
  endfor
  write_video, outFilename, video, VIDEO_FPS=20

  hubHelper.openFile, outFilename


end