pro hubRSENVIUserPlotTimerCallback_polyfit, id, callbackInfo
  time = hubTime(callbackInfo.startOfSeasonDOY)
  wset, callbackInfo.window
  oplot, callbackInfo.decimalYearGrid, callbackInfo.yGrid
end

function hubRSENVIUserPlot_polyfit, decimalYears, yAll, bbl, bbl_list, _extra=_extra, StandAlone=standAlone

  showPlot = 1b

  ; extract valid data and sort according to DOY
  time = hubTime(decimalYears)
  doyAll = time.doy
  valid = where(/NULL, yAll gt 0, nvalid)
  doyValid = doyAll[valid]
  yValid = yAll[valid]
  sortedDOY = sort(doyValid)
  doyValidSorted = doyValid[sortedDOY]
  yValidSorted = yValid[sortedDOY]

  ; find start of season (sos) DOY
  degree = 3
  coeffs = poly_fit(doyValidSorted, yValidSorted, degree, /DOUBLE)
  doyGrid = [1.:365.]
  yGrid = doyGrid*0. & foreach coeff,coeffs,i do yGrid += coeff*doyGrid^i
  sosIndex = (where(yGrid eq min(yGrid)))[0]
  startOfSeasonDOY = doyGrid[sosIndex]
  startOfSeasonY = yGrid[sosIndex]
  ; - plot
  yRange = hubMathSpan(yValid)
  if keyword_set(standAlone) then begin
    !null = window(DIMENSIONS=[550,300], LOCATION=[0,100])
    !null = plot(doyValidSorted, yValidSorted, SYMBOL=4, LINESTYLE=6, XRANGE=[1,365], XSTYLE=1, YRANGE=yrange, YSTYLE=1, /CURRENT, Title='find start of season',XTitle='day of year', YTITLE='NDVI')
    !null = plot(doyGrid, yGrid, /OVERPLOT)
    arrowX = [doyGrid[sosIndex],doyGrid[sosIndex]]
    arrowY = [yGrid[sosIndex]+(yRange-yGrid[sosIndex])/2,yGrid[sosIndex]]
    !null = arrow(arrowX, arrowY, COLOR='blue', /DATA, /CURRENT)
    !null = text(arrowX[0],arrowY[0], 'start of season', COLOR='blue', /DATA, /CURRENT, ALIGNMENT=0.5)
  endif
  
  ; convert/shift DOY to DOS (day of season) and fit season
  dos = doyValidSorted-round(startOfSeasonDOY)+1
  tail = where(/NULL, dos le 0, COMPLEMENT=head)
  dos = [dos[head], dos[tail]+365]
  y = yValidSorted[[head,tail]]
  degree = 2
  coeffs = poly_fit(dos, y, degree, /DOUBLE)
  dosGrid = [1.:365.]
  yGrid = dosGrid*0. & foreach coeff,coeffs,i do yGrid += coeff*dosGrid^i
  ; - plot
  if keyword_set(standAlone) then begin
    !null = window(DIMENSIONS=[550,300], LOCATION=[600,100])
    !null = plot(/CURRENT, dos, y, SYMBOL=4, LINESTYLE=6, XRANGE=[1,365], XSTYLE=1, YRANGE=yrange, YSTYLE=1, Title='shifted day of years fit',XTitle='day of season', YTITLE='NDVI')
    !null = plot(/OVERPLOT, dosGrid, yGrid)
  endif
  
  ; fit on DecimalYears
  decimalYearAll = time.dyear
  decimalYearValid = decimalYearAll[valid]
  decimalYearGrid = [floor(decimalYearAll[0]):ceil(decimalYearAll[-1]):1./24]
  doyGrid = double((hubTime(decimalYearGrid)).doy)
  dosGrid = doyGrid-round(startOfSeasonDOY)+1
  dosGrid += 365*(dosGrid le 0)
  yGrid = dosGrid*0. & foreach coeff,coeffs,i do yGrid += coeff*dosGrid^i
  if keyword_set(standAlone) then begin
    !null = window(DIMENSIONS=[1150,300], LOCATION=[0,500])
    !null = plot(decimalYearValid, yValid, SYMBOL=4, LINESTYLE=6,/CURRENT, Title='complete timeseries fit',XTitle='decimal year', YTITLE='NDVI')
    !null = plot(decimalYearGrid, yGrid, /OVERPLOT)
    return, !null
  endif

  if ~keyword_set(standAlone) then begin
    p_data = scope_varfetch('p_data', LEVEL=-1)
    widget_control, (*p_data).win, GET_VALUE=window
    callbackInfo = dictionary()
    callbackInfo.window = window
    callbackInfo.decimalYearGrid = decimalYearGrid
    callbackInfo.yGrid = yGrid
    callbackInfo.startOfSeasonDOY = startOfSeasonDOY
    
    ; can not plot the original here, because ENVI will overplot it
    ; use a timer to overplot later, after ENVI plots the original data
    id = timer.set(0.1, 'hubRSENVIUserPlotTimerCallback_polyfit', callbackInfo )
    return, yAll
  endif
end

pro test_hubRSENVIUserPlot_polyfit

  ; read data cube and meta data
  filename = 'G:\temp\temp_ar\4dirk\timestack'
  image = hubIOImgInputImage(filename)
  cube = image.getCube()
  decimalYears = image.getMeta('wavelength')

  yAll = cube[84,55,*]
  yAll = cube[randomu(seed, 1, /LONG) mod image.getMeta('samples'),randomu(seed, 1, /LONG) mod image.getMeta('lines'),*]

  yFit = hubRSENVIUserPlot_polyfit(decimalYears, yAll, /StandAlone)
end

pro test_hubRSENVIUserPlot_polyfit2
  envi
  envi_open_file, 'G:\temp\temp_ar\4dirk\timestack'
  ; open z profile and change to user-def plot function
end