function hubTBContentFilelist::init, toolbox
  !null = self.hubTBContentMVC::init(hubTBContentFilelistController(), hubTBContentFilelistModel(), toolbox)
  return, 1b
end

pro hubTBContentFilelist::openFile, filename, type, Extension=extension
  self.controller.openFile, filename, type, Extension=extension
end

pro hubTBContentFilelist::closeFile, filename
  self.controller.closeFile, filename
end

function hubTBContentFilelist::getFiles, type
  return, self.controller.getFiles(type)
end

pro hubTBContentFilelist__define

  define = {hubTBContentFilelist, inherits hubTBContentMVC}
  
end