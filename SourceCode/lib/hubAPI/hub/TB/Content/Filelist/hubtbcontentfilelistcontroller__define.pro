function hubTBContentFilelistController::newView
  return, hubTBContentFilelistView()
end

pro hubTBContentFilelistController::openFile, filename, type, Extension=extension
  if ~file_test(filename) then begin
    message, 'Can not open file: '+filename
  endif
  
  if keyword_set(extension) then begin
    fileExtension = strlowcase(hubHelper.getFileExtension(filename))
    type = fileExtension eq '' ? '*' : '*.'+fileExtension
  endif
  
  if ~isa(type, 'string') then begin
    message, 'Missing argument.'
  endif

  foreach view, self.views do begin
    view.addFile, filename, type
  endforeach
  self.model.addFile, filename, type
  
end

pro hubTBContentFilelistController::closeFile, filename
  foreach view, self.views do begin
    view.removeFile, filename
  endforeach
  self.model.removeFile, filename
end

function hubTBContentFilelistController::getFiles, type
  return, self.model.getFiles(type)
end

pro hubTBContentFilelistController__define
  define = {hubTBContentFilelistController, inherits hubTBContentMVCController}
end
