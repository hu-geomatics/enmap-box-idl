function hubTBContentFilelistModel::init
  self.title = 'Filelist'
  self.files = hash()
  return, 1b
end

pro hubTBContentFilelistModel::addFile, filename, type
  type_ = strlowcase(type)
  if ~self.files.hasKey(type_) then begin
    self.files[type] = list()
  endif
  
  if type_ eq 'image' or type_ eq 'speclib' then begin 
    inputImage = hubIOImgInputImage(filename)
    header = inputImage.getHeader()
  endif else begin
    header = hubIOImgHeader()
    header.setMeta, 'filename data', filename
  endelse
  ; add fileheader
  self.files[type].add, header
end

pro hubTBContentFilelistModel::removeFile, filename
  foreach type, self.files.keys() do begin
    indices = list()
    foreach header, self.files[type], index do begin
      if header.getMeta('filename data') eq filename then begin
        indices.add, index
      endif
    endforeach
    indices = indices.toArray()
    if isa(indices) then begin
      (self.files[type]).remove, indices
    endif
  endforeach
end

function hubTBContentFilelistModel::getFiles, type
  type_ = strlowcase(type)
  if self.files.hasKey(type_) then begin
    files = self.files[type_]  
  endif else begin
    files = list()
  endelse
  return, files
end

pro hubTBContentFilelistModel__define

  define = {hubTBContentFilelistModel, inherits hubTBContentMVCModel, $
    files : hash()}
end