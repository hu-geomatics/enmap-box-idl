pro hubTBContentFilelistView::create
  self.treeRoot = widget_tree(self.base, /CONTEXT_EVENTS, UVALUE=hubObjCallbackEvent(self, 'filelist'))
  self.treeFileTypes = list()
end


pro hubTBContentFilelistView::createContext, treeSelection

  if widget_info(/VALID_ID, self.contextBase) then begin
    widget_control, /DESTROY, self.contextBase
  endif
  self.contextBase = widget_base(self.base, /CONTEXT_MENU)
  treeInfo = self.getTreeInfo(treeSelection)
  
  case treeInfo.name of
    'file type folder' : ;
    'file folder' : begin
      case treeInfo.filetype of
        'images' : self.createContextImages
        'speclibs' : self.createContextImages
        else : self.createContextOthers
      endcase
    end
    'band leaf' :self.createContextImageBand
    else : ;do nothing
  endcase
end

pro hubTBContentFilelistView::createContextImages
  !null = widget_button(self.contextBase, VALUE='Close File', UVALUE=hubObjCallbackEvent(self, 'closeFile'))
  !null = widget_button(self.contextBase, VALUE='Quick Statistics', UVALUE=hubObjCallbackEvent(self, 'quickStatistics'))
  !null = widget_button(self.contextBase, VALUE='Edit Header File', UVALUE=hubObjCallbackEvent(self, 'editHeaderFile'))
  !null = widget_button(self.contextBase, VALUE='Refresh Header File', UVALUE=hubObjCallbackEvent(self, 'refreshHeaderFile'))
  !null = widget_button(self.contextBase, VALUE='Copy Meta Information', UVALUE=hubObjCallbackEvent(self, 'copyMetaInfos'))
  !null = widget_button(self.contextBase, VALUE='Show in Folder', UVALUE=hubObjCallbackEvent(self, 'showInFolder'))
  !null = widget_button(self.contextBase, VALUE='Copy File', UVALUE=hubObjCallbackEvent(self, 'copyFile'))
  !null = widget_button(self.contextBase, VALUE='Move File', UVALUE=hubObjCallbackEvent(self, 'moveFile'))
;  !null = widget_button(self.contextBase, VALUE='Delete File', UVALUE=hubObjCallbackEvent(self, 'deleteFile'))
  
  ; options belonging to all files
  !null = widget_button(self.contextBase, VALUE='Show File Sources', UVALUE=hubObjCallbackEvent(self, 'showFileSources'),/SEPARATOR)
  !null = widget_button(self.contextBase, VALUE='Close All Files', UVALUE=hubObjCallbackEvent(self, 'closeAllFiles'))
end

pro hubTBContentFilelistView::createContextImageBand
  !null = widget_button(self.contextBase, VALUE='Quick Statistics', UVALUE=hubObjCallbackEvent(self, 'quickStatistics'))
end

pro hubTBContentFilelistView::createContextOthers
  !null = widget_button(self.contextBase, VALUE='Show in Folder', UVALUE=hubObjCallbackEvent(self, 'showInFolder'))
  !null = widget_button(self.contextBase, VALUE='Show File Sources', UVALUE=hubObjCallbackEvent(self, 'showFileSources'))
  !null = widget_button(self.contextBase, VALUE='Close File', UVALUE=hubObjCallbackEvent(self, 'closeFile'))
  !null = widget_button(self.contextBase, VALUE='Close All Files', UVALUE=hubObjCallbackEvent(self, 'closeAllFiles'))
end

pro hubTBContentFilelistView::delete
  widget_control, /DESTROY, self.treeRoot
end

pro hubTBContentFilelistView::updateSize
  size = self.getSize()
  widget_control, self.treeRoot, SCR_XSIZE=size[0]>1, SCR_YSIZE=size[1]>1
end

pro hubTBContentFilelistView::updateView
end

function hubTBContentFilelistView::getTreeInfo, tree
  widget_control, tree, GET_UVALUE=objEvent
  result = hash()
  result['name'] = objEvent.name
  case objEvent.name of
    'file type folder' : begin
      result['fileType'] = objEvent.parameters.type
    end
    'file folder' : begin
      result['header'] = objEvent.parameters.header
      result['fileType'] = objEvent.parameters.type
    end
    'band leaf' : begin
      result['header'] = objEvent.parameters.header
      result['band'] = objEvent.parameters.band
    end
    'profile leaf' : begin
      result['header'] = objEvent.parameters.header
      result['profile'] = objEvent.parameters.profile
    end
    
    'non interactive' : ; do nothing
    else : print, objEvent.name
  endcase
  return, result.toStruct()
end

function hubTBContentFilelistView::getTreeSelection
  treeSelection = widget_info(/TREE_SELECT, self.treeRoot)
  if treeSelection eq -1 then begin
    result = !null
  endif else begin
    result = treeSelection
  endelse
  return, result
end

pro hubTBContentFilelistView::addFile, filename, type

  ; find file type tree
  foreach treeFileType, self.treeFileTypes do begin
    treeInfo = self.getTreeInfo(treeFileType)
    if treeInfo.fileType eq type then begin
      targetTreeFileType = treeFileType
      break
    endif
  endforeach
  
  ; create file type tree if not existing
  if ~isa(targetTreeFileType) then begin
    targetTreeFileType = self.addFileType(type)
    self.treeFileTypes.add, targetTreeFileType
  endif
  
  ;turn widget update off
  widget_control, self.treeRoot, UPDATE=0

  ; insert file
  
  ; - get file specific infos 
  case type of
    'images' : begin
      draggable = 1b
      inputImage = hubIOImgInputImage(filename)
      header = inputImage.getHeader()
      if inputImage.getMeta('bands') gt 1 then begin
        bitmap = hubTBAuxiliaryGetIconBitmaps(/Multi)
      endif else begin
        if inputImage.getMeta('data type') eq 1 then begin
          bitmap = hubTBAuxiliaryGetIconBitmaps(/Mask)
        endif else begin
          bitmap = hubTBAuxiliaryGetIconBitmaps(/Single)
        endelse
        if inputImage.isClassification() then begin
          bitmap = hubTBAuxiliaryGetIconBitmaps(/Classification)
        endif
      endelse
    end
    'speclibs' : begin
      draggable = 1b
      inputImage = hubIOImgInputImage(filename)
      header = inputImage.getHeader()
      bitmap = hubTBAuxiliaryGetIconBitmaps(/Speclib)
    end
    else : begin
      bitmap = hubTBAuxiliaryGetIconBitmaps(/File)
      header = hubIOImgHeader()
      header.setMeta, 'filename data', filename
    end
  endcase
  treeFile = widget_tree(targetTreeFileType, UVALUE=hubObjCallbackEvent(self, 'file folder', {header:header, type:type}), VALUE=file_basename(filename), /FOLDER, MASK=1, BITMAP=bitmap, DRAGGABLE=draggable, /CONTEXT_EVENTS)
  
  ; insert file information
  bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 6, [255,255,255])
  treeInfo = widget_tree(treeFile, VALUE='File Information', UVALUE=hubObjCallbackEvent(self, 'non interactive'), /FOLDER, DRAGGABLE=0, MASK=1, BITMAP=hubTBAuxiliaryGetIconBitmaps(/Info), CONTEXT_EVENTS=0)
  case type of
    'images' : begin
      self.addFile_imageInfoFolder, treeInfo, inputImage
    
      ;create filetype specific information folder
      if strcmp(inputImage.getMeta('file type'), 'envi classification', /FOLD_CASE) then begin
        bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 4, [255,255,255])
        treeInfo = widget_tree(treeFile, UVALUE=hubObjCallbackEvent(self, 'non interactive'), VALUE='Class Information', /FOLDER, /EXPANDED, DRAGGABLE=0, /MASK, BITMAP=bitmap, CONTEXT_EVENTS=0)
        classColors = strcompress(fix(inputImage.getMeta('class lookup')), /REMOVE_ALL)
        for i=0,inputImage.getMeta('classes')-1 do begin
          bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 1, classColors[*,i])
          !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'), VALUE=strcompress(i, /REMOVE_ALL)+' "'+(inputImage.getMeta('class names'))[i]+'"', /MASK, BITMAP=bitmap)
        endfor
      endif
    
      ;create band folder
      value = 'Bands'
      if isa(inputImage.getMeta('wavelength units')) then begin
        value += ' ('+inputImage.getMeta('wavelength units')+')'
      endif
      bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 4, [255,255,255])
      treeBands = widget_tree(treeFile, UVALUE=hubObjCallbackEvent(self, 'non interactive'), VALUE='Bands', /FOLDER, /EXPANDED, DRAGGABLE=0, /MASK, BITMAP=bitmap, CONTEXT_EVENTS=0)
      bandLongNames = strcompress(ulindgen(inputImage.getMeta('bands'))+1,/REMOVE_ALL)+': '+inputImage.getMeta('band names')
      if isa(inputImage.getMeta('wavelength')) then begin
        bandLongNames += ' ('+strcompress(/REMOVE_ALL, inputImage.getMeta('wavelength'))+')'
      endif
      bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 6, [255,255,255])
      for i=0L,n_elements(bandLongNames)-1 do begin
        !null = widget_tree(treeBands, UVALUE=hubObjCallbackEvent(self, 'band leaf', {header:header, type:type, band:i}), VALUE=bandLongNames[i], DRAGGABLE=1, /MASK, BITMAP=bitmap, CONTEXT_EVENTS=1)
      endfor
    end
    'speclibs' : begin
      self.addFile_imageInfoFolder, treeInfo, inputImage
      
      ;create profile folder
;      profileNames = inputImage.getMeta('spectra names')
;      bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 4, [255,255,255])
;      treeProfiles = widget_tree(treeFile, UVALUE=hubObjCallbackEvent(self, 'non interactive'), VALUE='Profiles', /FOLDER, /EXPANDED, DRAGGABLE=0, /MASK, BITMAP=bitmap, CONTEXT_EVENTS=0)
;      bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 6, [255,255,255])
;      for i=0L,n_elements(profileNames)-1 do begin
;        !null = widget_tree(treeProfiles, UVALUE=hubObjCallbackEvent(self, 'profile leaf', {header:header, type:type, profile:i}), VALUE=profileNames[i], DRAGGABLE=0, /MASK, BITMAP=bitmap, CONTEXT_EVENTS=0)
;      endfor
    end
    else : begin
      infos = ['Filename: '+filename,$
               'Size: '+ strtrim((file_info(filename)).size,2)+' byte']
      foreach info, infos do begin
        !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'), VALUE=info, /MASK, BITMAP=bitmap, CONTEXT_EVENTS=0)
      endforeach
    end
  endcase
  
  ;turn widget update on
  widget_control, self.base, UPDATE=1
    
  ;set select file FOLDER
  widget_control, treeFile, /SET_TREE_SELECT
end

pro hubTBContentFilelistView::addFile_imageInfoFolder, treeInfo, inputImage
  bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 6, [255,255,255])
  !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'),VALUE='Filename: '+inputImage.getMeta('filename data'), /MASK, BITMAP=bitmap)
  !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'),VALUE='File Type: '+inputImage.getMeta('file type'), /MASK, BITMAP=bitmap)
  !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'),VALUE='DIMS: '+strcompress(/REMOVE_ALL, inputImage.getMeta('samples'))+'x'+strcompress(/REMOVE_ALL, inputImage.getMeta('lines'))+'x'+strcompress(/REMOVE_ALL, inputImage.getMeta('bands'))+' ['+inputImage.getMeta('interleave')+']', /MASK, BITMAP=bitmap)
  !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'),VALUE='Size: ['+hubHelper.getDataTypeInfo(inputImage.getMeta('data type'), /TypeNames)+'] '+strcompress(/REMOVE_ALL, inputImage.getFileSize())+' bytes', /MASK, BITMAP=bitmap)
  !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'),VALUE='Description: '+inputImage.getMeta('description', Default=''), /MASK, BITMAP=bitmap)
  if isa(inputImage.getMeta('data ignore value')) then begin
    !null = widget_tree(treeInfo, UVALUE=hubObjCallbackEvent(self, 'non interactive'), VALUE='Data Ignore Value: '+strcompress(/REMOVE_ALL, inputImage.getMeta('data ignore value')+0), MASK=1, BITMAP=bitmap)
  endif
end

function hubTBContentFilelistView::addFileType, type
  parameters = {type:type}
  uvalue = hubObjCallbackEvent(self, 'file type folder', parameters)
  tree = widget_tree(self.treeRoot, UVALUE=uvalue, VALUE=type, /FOLDER, /EXPANDED, CONTEXT_EVENTS=0)
  return, tree
end

pro hubTBContentFilelistView::removeFile, filename
  foreach treeFileType, self.treeFileTypes, i do begin
    treeFiles = widget_info(/ALL_CHILDREN, treeFileType)
    foreach treeFile, treeFiles do begin 
      treeInfo = self.getTreeInfo(treeFile)
      if strcmp(filename, treeInfo.header.getMeta('filename data'), /FOLD_CASE) then begin
        widget_control, treeFile, /DESTROY
        removed = 1b
      endif
    endforeach
    
    ; if all files are removed, then remove file type folder
    if keyword_set(removed) and ((widget_info(/ALL_CHILDREN, treeFileType))[0] eq 0) then begin
      widget_control, treeFileType, /DESTROY
      self.treeFileTypes.remove, i
      return
    endif
  endforeach
end

pro hubTBContentFilelistView::handleEvent, objEvent
  case objEvent.category of
    'tree event' : self.handleTreeEvent, objEvent
    'button event' : self.handleButtonEvent, objEvent
    else : print, objEvent.category
  endcase
end

pro hubTBContentFilelistView::handleContextEvent, objEvent
  treeSelection = self.getTreeSelection()
  if isa(treeSelection) then begin
    self.createContext, treeSelection
    widget_displaycontextmenu, self.base, objEvent.event.x, objEvent.event.y, self.contextBase
  endif
end

pro hubTBContentFilelistView::handleTreeEvent, objEvent
  case objEvent.subcategory of
    'context event' : self.handleContextEvent, objEvent
    else : ; print, objEvent.subcategory
  endcase
end

pro hubTBContentFilelistView::handleButtonEvent, objEvent

  treeSelection = self.getTreeSelection()
  treeInfo = self.getTreeInfo(treeSelection)
  case objEvent.name of
    'closeFile' : begin
      filename = treeInfo.header.getMeta('filename data')
      self.controller.closeFile, filename
    end
    'quickStatistics' : begin
      filename = treeInfo.header.getMeta('filename data')
      parameters = hash()
      parameters['inputImage'] = filename
      if treeInfo.name eq 'band leaf' then begin
        parameters['targetBands'] = treeInfo.band
      endif
      result = hubApp_imageStatistics_processing(parameters)
      report = hubApp_imageStatistics_getReport(result, Settings=hash('createPlots',1b))
      report.saveHTML, /SHOW
    end
    'editHeaderFile' : begin
      filename = treeInfo.header.getMeta('filename header')
      hubHelper.openFile, filename, /ASCII
    end
    'refreshHeaderFile' : begin
      filename = treeInfo.header.getMeta('filename data')
      hubProEnvHelper.openImage, filename
    end
    'copyMetaInfos' : begin
      imageHeader = treeInfo.header
      filename = imageHeader.getMeta('filename data')
      widget_control, objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, TITLE=title
      hubAMW_label, [$
        'Replacing meta information of the following file:',$
        filename]
      hubAMW_inputImageFilename, 'sourceFilename', TITLE='Image with new meta information'
      result = hubAMW_manage(/STRUCTURE)
      if result.accept then begin
        sourceImage = hubIOImgInputImage(result.sourceFilename)
        sourceImageHeader = sourceImage.getHeader()
        sourceMetaNames = sourceImageHeader.getMetaNames()
        hubAMW_program, objEvent.event.top, TITLE=title
        hubAMW_label, [$
          'Replacing meta information of the following file:',$
          filename,$
          'Select meta information to be replaced from the following file:',$
          result.sourceFilename]
        hubAMW_checklist, 'metaNames', TITLE='Available meta information', List=sourceMetaNames, /ALLOWEMPTYSELECTION, /MULTIPLESELECTION, /COLUMN, /EXTRACT
        result2 = hubAMW_manage()
        if result2['accept'] then begin
          backupImage = hubIOImgInputImage(filename)
          backupImageHeader = backupImage.getHeader()
          imageHeader.copyMeta, sourceImageHeader, result2['metaNames']
          ; try to open image with new header
          catch, errorStatus
          if (errorStatus ne 0) then begin
            catch, /CANCEL
            ok = dialog_message(/ERROR, DIALOG_PARENT=objEvent.event.top, 'Created image header is not consistent and the old header will be restored.')
            backupImageHeader.writeFile, backupImageHeader.getMeta('filename header')
            hubProEnvHelper.openImage, filename
            return    
          endif
          imageHeader.writeFile, imageHeader.getMeta('filename header')
          hubProEnvHelper.openImage, filename
        endif
      endif
    end
    'showInFolder': begin
      filename = treeInfo.header.getMeta('filename data')
      hubHelper.openFileLocation, filename
    end
    'copyFile' : begin
      filenameData = treeInfo.header.getMeta('filename data')
      newFilenameData = dialog_pickfile(DIALOG_PARENT=objEvent.event.top, FILE=filenameData, /WRITE)
      if newFilenameData ne '' then begin
        hubProEnvHelper.copyImage, filenameData, newFilenameData
        hubProEnvHelper.openImage, newFilenameData
      endif
    end
    'moveFile' : begin
      filenameData = treeInfo.header.getMeta('filename data')
      newFilenameData = dialog_pickfile(DIALOG_PARENT=objEvent.event.top, FILE=filenameData, /WRITE)
      if newFilenameData ne '' then begin
        hubProEnvHelper.moveImage, filenameData, newFilenameData
        hubProEnvHelper.openImage, newFilenameData
      endif
    end
;    'deleteFile' : begin
;      filenameData = treeInfo.header.getMeta('filename data')
;      answer = dialog_message(/QUESTION, DIALOG_PARENT=objEvent.event.top, 'Are you sure you want to delete this file permanently?')
;      if answer eq 'Yes' then begin
;        hubProEnvHelper.deleteImage, filenameData
;      endif
;    end
    'showFileSources' : begin
      files = self.controller.getFiles(treeInfo.fileType)+list()
      directories = hash()
      foreach header, files do begin
        filenameData = header.getMeta('filename data')
        basename = file_basename(filenameData)
        dirname = file_dirname(filenameData)
        if ~directories.hasKey(dirname) then begin
          directories[dirname] = list()
        endif
        (directories[dirname]).add, basename
      endforeach
      result = list()
      foreach basenames, directories, dirname do begin
        result.add, dirname
        foreach basename, basenames do begin
          result.add, '  '+basename
        endforeach
      endforeach
      result = result.toArray()
      widget_control, objEvent.event.id, GET_VALUE=title
      xdisplayfile, Text=result, TITLE=title, GROUP=objEvent.event.top
      print, transpose(result)
    end
    'closeAllFiles' : begin
      files = self.controller.getFiles(treeInfo.fileType)+list()
      foreach header, files do begin
        filenameData = header.getMeta('filename data')
        self.controller.closeFile, filenameData
      endforeach
    end
    else : print, objEvent.name
  endcase
end
  
;  ;tree context menu buttons
;  
;  
;  
;  if strcmp(uname,'close_all',9) then begin
;    if strcmp(uname,'close_all_image') then close_all_image=1
;    if strcmp(uname,'close_all_spectral') then close_all_spectral=1
;    self->close_file, CLOSE_ALL_IMAGE=close_all_image,CLOSE_ALL_SPECTRAL=close_all_spectral
;    return
;  endif
;  
;endif
;
;;--------------
;;context events
;if event_name eq 'WIDGET_CONTEXT' then begin
;  tree_selection = self->get_tree_selection(EVENT_ID=event.id)
;  if tree_selection.empty then return
;  if tree_selection.file then begin
;    if tree_selection.image then context_base = widget_info(event.top,FIND_BY_UNAME='context_menu_image')
;    if tree_selection.spectral then context_base = widget_info(event.top,FIND_BY_UNAME='context_menu_spectral')
;  endif
;  
;  if n_elements(context_base) eq 0 then return
;  widget_displaycontextmenu,event.id,event.x,event.y,context_base
;  return
;endif


pro hubTBContentFilelistView__define
  define = {hubTBContentFilelistView, inherits hubTBContentMVCView,$
    treeRoot:0l, treeFileTypes:list(), contextBase:0l}
end