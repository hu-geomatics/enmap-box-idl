function hubTBContentConsoleModel::init
  self.title = 'Console'
  self.text = ptr_new('')
  self.bufferSize = 300
  return, 1b
end

pro hubTBContentConsoleModel::addText, text
  bufferText = [*self.text, string(text[*])]
  if n_elements(bufferText) gt self.bufferSize then begin
    bufferText = bufferText[-self.bufferSize:-1]
  endif
  self.text = ptr_new(bufferText[*])
end

pro hubTBContentConsoleModel::setProperty, _EXTRA=_extra, text=text
  self.hubTBContentMVCModel::setProperty, _EXTRA=_extra
  if isa(text) then self.text = ptr_new(text[*])
end

pro hubTBContentConsoleModel::getProperty, text=text
  if arg_present(text) then text = *self.text
end

pro hubTBContentConsoleModel__define

  define = {hubTBContentConsoleModel, inherits hubTBContentMVCModel, $
    text : ptr_new(),$
    bufferSize : 0l}
end