function hubTBContentConsole::init, toolbox
  !null = self.hubTBContentMVC::init(hubTBContentConsoleController(), hubTBContentConsoleModel(), toolbox)
  return, 1b
end

pro hubTBContentConsole::print, text
  self.controller.print, text
end

pro hubTBContentConsole__define

  define = {hubTBContentConsole, inherits hubTBContentMVC}
  
end