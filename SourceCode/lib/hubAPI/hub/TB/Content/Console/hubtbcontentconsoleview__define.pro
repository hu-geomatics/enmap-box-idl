pro hubTBContentConsoleView::create
  self.textID = widget_text(self.base, /EDITABLE, YSIZE=2, /SCROLL)
end

pro hubTBContentConsoleView::delete
  widget_control, /DESTROY, self.textID
end

pro hubTBContentConsoleView::updateSize
  size = self.getSize() 
  widget_control, self.textID, SCR_XSIZE=size[0]>1, SCR_YSIZE=size[1]>1
end

pro hubTBContentConsoleView::updateView
  text = self.controller.getText()
  widget_control, self.textID, UPDATE=0
  widget_control, self.textID, SET_VALUE=reform(text, 1, n_elements(text))
  case !VERSION.OS_FAMILY of
    'Windows' :   widget_control, self.textID, SET_TEXT_TOP_LINE=n_elements(text)
    'unix' :   widget_control, self.textID, SET_TEXT_TOP_LINE=(n_elements(text)-5)>0
  endcase
  widget_control, self.textID, UPDATE=1
end

pro hubTBContentConsoleView__define
  define = {hubTBContentConsoleView, inherits hubTBContentMVCView,$
    textID : 0l}
  
end