function hubTBContentConsoleController::newView
  return, hubTBContentConsoleView()
end

pro hubTBContentConsoleController::print, text
  self.model.addText, text
  foreach view, self.views do begin  
    view.updateView
  endforeach
end

function hubTBContentConsoleController::getText
  return, self.model.text
end

pro hubTBContentConsoleController::handleViewMessage, message
  case message.name of
    'updateContent' : self.view.updateText, self.model.text
    else : stop
  endcase
end

pro hubTBContentConsoleController__define
  define = {hubTBContentConsoleController, inherits hubTBContentMVCController}
end
