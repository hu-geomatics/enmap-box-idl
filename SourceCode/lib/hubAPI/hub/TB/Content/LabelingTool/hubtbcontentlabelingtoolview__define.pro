pro hubTBContentLabelingToolView::create
  
  case self.getType() of
    'image' : nameRegion = 'ROI'
    'speclib' : nameRegion = 'SOI'
  endcase
  case self.getType() of
    'image' : nameElement = 'Pixel'
    'speclib' : nameElement = 'Profile'
  endcase
  case self.getType() of
    'image' : namePseudo = ''
    'speclib' : namePseudo = ' (pseudo image)'
  endcase
  
  self.mainBase = widget_base(self.base, /COLUMN, XPAD=0, YPAD=0, SPACE=0)
  self.menuBase = widget_base(self.mainBase, /ROW, XPAD=0, YPAD=0, SPACE=0,/BASE_ALIGN_CENTER )
  menu = widget_button(self.menuBase, VALUE='File', /MENU)
  !null = widget_button(menu, VALUE='Open '+nameRegion+'s', UVALUE=hubObjCallbackEvent(self, 'openROIs'))
  !null = widget_button(menu, VALUE='Save '+nameRegion+'s', UVALUE=hubObjCallbackEvent(self, 'saveROIs'))

  !null = widget_button(menu, VALUE='Import '+nameRegion+'s from Label Image'+namePseudo, UVALUE=hubObjCallbackEvent(self, 'importROIsFromRaster'), /SEPARATOR)

  if self.getType() eq 'image' then begin 
    !null = widget_button(menu, VALUE='Import '+nameRegion+'s from Shapefile', UVALUE=hubObjCallbackEvent(self, 'importROIsFromShapefile'))
  endif
  !null = widget_button(menu, VALUE='Export '+nameRegion+'s to Label Image'+namePseudo, UVALUE=hubObjCallbackEvent(self, 'exportROIsToRaster'))
  !null = widget_button(menu, VALUE='Export '+nameRegion+'s to Classification Image'+namePseudo, UVALUE=hubObjCallbackEvent(self, 'exportROIsToClassification'))
  
  if self.getType() eq 'image' then begin
    !null = widget_button(menu, VALUE='Export '+nameRegion+'s to Shapefile', UVALUE=hubObjCallbackEvent(self, 'exportROIsToShapefile'))
  endif
  !null = widget_button(menu, VALUE='Close', UVALUE=hubObjCallbackEvent(self, 'close'), /SEPARATOR)

  menu = widget_button(self.menuBase, VALUE='Edit', /MENU)
;  !null = widget_button(menu, VALUE='Add Attribute', UVALUE=hubObjCallbackEvent(self, 'addAttribute'))
;  !null = widget_button(menu, VALUE='Delete Attribute', UVALUE=hubObjCallbackEvent(self, 'deleteAttribute'))
;  !null = widget_button(menu, VALUE='Edit Attribute', UVALUE=hubObjCallbackEvent(self, 'editAttribute'))
  !null = widget_button(menu, VALUE='Calculate Attribute', UVALUE=hubObjCallbackEvent(self, 'calculateAttribute'), /SEPARATOR)
  !null = widget_button(menu, VALUE='Dissolve Attribute', UVALUE=hubObjCallbackEvent(self, 'dissolveAttribute'))
  !null = widget_button(menu, VALUE='Check for '+nameRegion+' Overlap', UVALUE=hubObjCallbackEvent(self, 'checkROIOverlap'), /SEPARATOR)

  if self.getType() eq 'image' then begin 
    !null = widget_label(self.menuBase, Value='  Mode ')
    checkBase = widget_base(/EXCLUSIVE, self.menuBase, /ROW, XPAD=0, YPAD=0, SPACE=0)
    self.modeAdd = widget_button(checkBase, VALUE='Add Pixel')
    self.modeRemove = widget_button(checkBase, VALUE='Remove Pixel')
    self.modeOff = widget_button(checkBase, VALUE='Off')
    widget_control, self.modeAdd, SET_BUTTON=1
  endif

  self.table = widget_table(self.mainBase,$
    COLUMN_LABELS='Attribure '+strtrim(indgen(10)+1,2), ROW_LABELS=+strtrim(indgen(20)+1,2),$
    COLUMN_WIDTHS=150,$
    XSIZE=10, YSIZE=20,$
    /ALL_EVENTS, /CONTEXT_EVENTS, /EDITABLE, KBRD_FOCUS_EVENTS=0, TRACKING_EVENTS=0,$
    /RESIZEABLE_COLUMNS, /SCROLL, $
    UVALUE=hubObjCallbackEvent(self, 'table'))
  self.buttonBase = widget_base(self.mainBase, /COLUMN, XPAD=0, YPAD=0, SPACE=0)
  subButtonBase = widget_base(self.buttonBase, /ROW, XPAD=0, YPAD=0, SPACE=0)
  !null = widget_button(subButtonBase, VALUE='New '+nameRegion,    UVALUE=hubObjCallbackEvent(self, 'newRegion'))
  !null = widget_button(subButtonBase, VALUE='Delete '+nameRegion, UVALUE=hubObjCallbackEvent(self, 'deleteRegion'))
  !null = widget_button(subButtonBase, VALUE='New Attribute', UVALUE=hubObjCallbackEvent(self, 'addAttribute'))
  !null = widget_button(subButtonBase, VALUE='Delete Attribute', UVALUE=hubObjCallbackEvent(self, 'deleteAttribute'))
  !null = widget_button(subButtonBase, VALUE='Edit Attribute', UVALUE=hubObjCallbackEvent(self, 'editAttribute'))
;  !null = widget_button(subButtonBase, VALUE='+++ Udate Table +++', UVALUE=hubObjCallbackEvent(self, '+++updateTable+++'))
  
  subButtonBase = widget_base(self.buttonBase, /ROW, XPAD=0, YPAD=0, SPACE=0)
  !null = widget_button(subButtonBase, VALUE='Goto Next '+nameElement,     UVALUE=hubObjCallbackEvent(self, 'gotoNextPixel'))
  !null = widget_button(subButtonBase, VALUE='Goto Previous '+nameElement, UVALUE=hubObjCallbackEvent(self, 'gotoPreviousPixel'))
  if self.getType() eq 'image' then begin 
    !null = widget_button(subButtonBase, VALUE=nameRegion+' Statistic',    UVALUE=hubObjCallbackEvent(self, 'regionStatistics'))
  endif
  if self.getType() eq 'speclib' then begin
    !null = widget_button(subButtonBase, VALUE='Select '+nameRegion+' Profiles',    UVALUE=hubObjCallbackEvent(self, 'regionSelection'))
  endif
  !null = widget_button(subButtonBase, VALUE='Copy '+nameRegion+' to Speclib',    UVALUE=hubObjCallbackEvent(self, 'copyToSpeclib'))

  self.initTableContent
end

pro hubTBContentLabelingToolView::delete
  widget_control, /DESTROY, self.mainBase
end

pro hubTBContentLabelingToolView::updateSize
  size = self.getSize()
  geometry = widget_info(/GEOMETRY, self.menuBase)
  menuSize = [geometry.scr_xsize, geometry.scr_ysize]
  geometry = widget_info(/GEOMETRY, self.buttonBase)
  buttonSize = [geometry.scr_xsize, geometry.scr_ysize]
  widget_control, self.table, SCR_XSIZE=size[0]>1, SCR_YSIZE=size[1]-menuSize[1]-buttonSize[1]>1
end

pro hubTBContentLabelingToolView::updateView
end

;
;pro hubTBContentLabelingToolView::updateTableContent, tableContent, attributeNames
;  size = size(/DIMENSIONS, hub_fix2d(tableContent))
;  rowNames = strtrim(lindgen(size[1])+1,2)
;  widget_control, self.table, SET_VALUE=tableContent, ROW_LABELS=rowNames, COLUMN_LABELS=attributeNames, TABLE_XSIZE=size[0]+1, TABLE_YSIZE=size[1]+1;, XSIZE=size[0]+1, YSIZE=size[1]+1
;end

pro hubTBContentLabelingToolView::updateTableRegionSelection, regionSelection, Last=last, Scroll=scroll
Catch, errorStatus

  ; Error handler
  if (errorStatus ne 0) then begin
    Catch, /CANCEL
    ; TODO: Write error handler
help,/LAST_MESSAGE
stop
  endif

  numberOfRegions = (widget_info(/GEOMETRY, self.table)).ysize
  if keyword_set(last) then begin
    regionSelection = numberOfRegions-1
  endif
  regionSelection <= numberOfRegions-1
  if regionSelection ge 0 then begin
    row_labels = strarr(numberOfRegions)
    row_labels = hub_fix1d(string(Format='(a9)', strtrim(sindgen(numberOfRegions, START=1),2)))
    selectedRow = row_labels[regionSelection]
    strput, selectedRow, '*', 0
    row_labels[regionSelection] = selectedRow
    widget_control, self.table, ROW_LABELS=row_labels
    self.regionSelection = regionSelection
    if keyword_set(scroll) then begin
      widget_control, self.table, SET_TABLE_VIEW=[0,regionSelection-5]
    endif
  endif
end

pro hubTBContentLabelingToolView::initTableContent, ROIIndex=roiIndex
  roiLayer = self.controller.getROILayer()
  tableContent = roiLayer.getAttributeTable(/Text)
  numberOfAttributes = roiLayer.getNumberOfAttributes()
  numberOfGeometries = roiLayer.getNumberOfGeometries()
 ; roiSizes = (roiLayer.getROISizes()).toArray()

  attributeNames = (roiLayer.getAttributeNames()).toArray(Type='string')
  attributeFormats = roiLayer.getAttributeFormats()
  attributeFormatStrings = roiLayer.getAttributeFormats(/IDLFormatString)
  roiColors = (roiLayer.getColors()).toArray(/TRANSPOSE)

  numberOfTableColumns = numberOfAttributes+1
  tableAlignment = intarr(numberOfTableColumns, numberOfGeometries)
  foreach attributeFormat,attributeFormats, i do begin
    if attributeFormat[0] ne 'string' then begin
      tableAlignment[i,*] = 2
    endif
  endforeach
  
    
  ; set table size and labels
  widget_control, self.table, UPDATE=1
  widget_control, self.table, COLUMN_LABELS=[attributeNames, 'Color'], TABLE_XSIZE=numberOfTableColumns, TABLE_YSIZE=numberOfGeometries, ALIGNMENT=tableAlignment, BACKGROUND_COLOR=[255,255,255]
  
  tableContent = [hub_fix2d(tableContent),hub_fix2d(strarr(1,numberOfGeometries))]
  
  tableColors = replicate(255b, [3,size(/DIMENSIONS, tableContent)])
  for i=0,2 do tableColors[*,numberOfTableColumns-1,*] = roiColors
  
  tableColors = reform(tableColors, 3, product(size(/DIMENSIONS, tableContent)))
  widget_control, self.table, SET_VALUE=tableContent, BACKGROUND_COLOR=tableColors

  widget_control, self.table, SET_TABLE_SELECT=[-1,-1,-1,-1]
  self.initTableContentSizes
  widget_control, self.table, UPDATE=1

  self.updateTableRegionSelection, isa(roiIndex) ? roiIndex : 0
  
end

pro hubTBContentLabelingToolView::initTableContentSizes
  roiLayer = self.controller.getROILayer()
  roiSizes = (roiLayer.getROISizes()).toArray()
  widget_control, self.table, GET_VALUE=tableContent
  tableContent[-1,*] = strtrim(roiSizes, 2)
  widget_control, self.table, SET_VALUE=tableContent
end

pro hubTBContentLabelingToolView::handleEvent, objEvent
  case objEvent.category of
    'button event' : self.handleButtonEvent, objEvent
    'table event' : self.handleTableEvent, objEvent
    else : begin
;      help, objEvent.event
;      print, objEvent.category,', ',objEvent.subcategory
    end
  endcase
;
;  if self.doUpdateView then begin
;    self.view.updateView
;    self.doUpdateView = 0b
;  endif
end

pro hubTBContentLabelingToolView::handleButtonEvent, objEvent

  case objEvent.name of
    'gotoNextPixel' : begin
      self.gotoPosition++
      self.controller.gotoROIPixel, self.regionSelection, self.gotoPosition
    end 
    'gotoPreviousPixel' : begin
      self.gotoPosition--
      self.controller.gotoROIPixel, self.regionSelection, self.gotoPosition
    end
    'newRegion' : begin
      self.controller.newROI
    end
    'deleteRegion' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_checklist, 'index', Title='', List=['delete RIO','empty ROI'], Value=0
      result = hubAMW_manage(/STRUCTURE)
;      case self.getType() of
;        'image'   :       answer = dialog_message(/QUESTION, 'Delete ROI?'
;        'speclib' : hubAMW_inputFilename, 'filename', Title='enmapSOI File', Extension='enmapSOI'
;      endcase
      if result.accept then begin
        roiIndex = self.regionSelection
        if result.index eq 0 then begin
          self.controller.deleteROI, roiIndex
        endif else begin
          self.controller.emptyROI, roiIndex
        endelse
        self.updateTableRegionSelection, roiIndex
      endif
    end
    'regionStatistics' : begin
      roiLayer = self.controller.getROILayer()
      sourceContent = self.controller.getSourceContent()
      imageFilename = (sourceContent.getImage()).getMeta('filename data')
      roiLayer.showROIImageStatisticsReport, self.regionSelection, imageFilename
    end
    'regionSelection' : begin
      roiLayer = self.controller.getROILayer()
      indices = roiLayer.getGeometry(self.regionSelection)
      sourceContent = self.controller.getSourceContent()
      sourceContent.controller.setPlotSelectionByIndices, indices
      sourceContent.updateView
    end
    'copyToSpeclib' : begin
      roiLayer = self.controller.getROILayer()
      indices = roiLayer.getGeometry(self.regionSelection)
      if ~isa(indices) then return

      sourceContent = self.controller.getSourceContent()
      gridManager = sourceContent.controller.getGridFrameManager()
      views = gridManager.getContents(/Speclibs)
      selfIndex = views.where(sourceContent)
      if isa(selfIndex) then begin
        views.remove, selfIndex
      endif
      if views.isEmpty() then begin
        ok = dialog_message(/Error, 'No spectral view available.')
        return
      endif
      names = list()
      foreach view, views do begin
        managedFrame = (view.controller.getFrames())[0]
        names.add, managedFrame.getTitle()
      endforeach
      names = names.toArray()
      
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_checklist, 'index', Title='', List=names, Value=0
      result = hubAMW_manage(/STRUCTURE)
      if ~result.accept then return

      view = views[result.index[0]]
      case self.getType() of
        'image' : begin
          image = sourceContent.getImage()
          imageFilename = image.getMeta('filename data')
          wavelength = image.getMeta('wavelength')
          units = image.getMeta('wavelength units')
          dataIgnoreValue = image.getMeta('data ignore value')
          data = hubIOImg_readProfiles(imageFilename, indices)
          indices2d = strtrim(array_indices(image.getSpatialSize(), indices, /DIMENSIONS), 2)
          names = '['+indices2d[0,*]+','+indices2d[1,*]+'] '+file_basename(imageFilename)
          for i=0,n_elements(data[0,*])-1 do begin
            profile = hubTBManagedContentSpeclibProfile(data[*,i], wavelength, units, Name=names[i], DataIgnoreValue=dataIgnoreValue)
            view.addProfile, profile
          endfor
        end
        'speclib' : begin
          profiles = (sourceContent.controller.getLibrary()).getProfiles()
          foreach index,indices do begin
            newProfile = (profiles[index]).copy()
            newProfile.setColor, [0,0,0]
            view.addProfile, newProfile
          endforeach
        end      
      endcase
      view.controller.resetPlot
      view.updateView
    end
    'openROIs' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      case self.getType() of
        'image' : hubAMW_inputFilename, 'filename', Title='enmapROI File', Extension='enmapROI'
        'speclib' : hubAMW_inputFilename, 'filename', Title='enmapSOI File', Extension='enmapSOI'
      endcase
      result = hubAMW_manage(/STRUCTURE)
      if result.accept then begin
        roiLayer = hubROILayerReader.readROIFile(result.filename)
        self.controller.appendROILayer, roiLayer
      endif
    end
    'saveROIs' : begin
        widget_control,objEvent.event.id, GET_VALUE=title
        hubAMW_program, objEvent.event.top, Title=title
        case self.getType() of
          'image' : hubAMW_outputFilename, 'filename', Title='enmapROI File', VALUE='regionOfInterest.enmapROI', Extension='enmapROI'  
          'speclib' : hubAMW_outputFilename, 'filename', Title='enmapSOI File', VALUE='regionOfInterest.enmapSOI', Extension='enmapSOI'
        endcase
        result = hubAMW_manage(/STRUCTURE)
        if result.accept then begin
          roiLayer = self.controller.getROILayer()
          writer = hubROILayerWriter()
          writer.writeROIFile, result.filename, roiLayer
        endif
      end
    'importROIsFromShapefile' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_inputFilename, 'shapefileFilename', Title='Shapefile',EXTENSION='shp' 
      result = hubAMW_manage(/STRUCTURE)
      if result.accept then begin
        imageFilename = ((self.controller.getsourceContent()).getImage()).getMeta('filename data')
        roiLayer = hubROILayerReader.importShapefile(result.shapefileFilename, imageFilename)
        self.controller.appendROILayer, roiLayer
      endif
    end
    'importROIsFromRaster' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_inputImageFilename, 'filename', Title='Label Image'
      roiLayer = self.controller.getROILayer()
      userInformation = hash('layerSize', roiLayer.getLayerSize(), 'layerType', self.getType())
      result = hubAMW_manage(/STRUCTURE, ConsistencyCheckFunction='hubTBContentLabelingToolView_consistencyCheck_importROIsFromRasterBand', UserInformation=userInformation)
      if result.accept then begin
        image = hubIOImgInputImage(result.filename)
        if image.isClassification() then begin
          answer = dialog_message('Prepare class scheme for re-labeling?', /QUESTION, DIALOG_PARENT=objEvent.event.top, TITLE=title)
          prepareRelabeling = answer eq 'Yes' 
        endif
        roiLayer = hubROILayerReader.importRaster(result.filename, PrepareRelabeling=prepareRelabeling)
        self.controller.appendROILayer, roiLayer
      endif
    end
    'exportROIsToShapefile' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_outputFilename, 'shapefileFilename', Title='Shapefile', Value='shapefile.shp', EXTENSION='shp'
      result = hubAMW_manage(/STRUCTURE)
      if result.accept then begin
        roiLayer = self.controller.getROILayer()
        writer = hubROILayerWriter()
        sourceContent = self.controller.getsourceContent()
        imageFilename = (sourceContent.getImage()).getMeta('filename data')
        writer.writeShapeFile, result.shapefileFilename, roiLayer, referenceFile=imageFilename
      endif
    end
    'exportROIsToClassification' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      tsize = 200
      hubAMW_program, objEvent.event.top, Title=title
      roiLayer = self.controller.getROILayer()
      attributeNames = (roiLayer.getAttributeNames()).toArray()
      hubAMW_combobox, 'classID',TITLE='Class ID Attribute', LIST=attributeNames, TSIZE=tsize
      hubAMW_combobox, 'classNames',TITLE='Class Names Attribute', LIST=attributeNames, /OPTIONAL, TSIZE=tsize
      hubAMW_outputFilename, 'filename', Title='Classification Image', Value='classification', TSIZE=tsize
      result = hubAMW_manage(/Dictionary)
      if result.accept then begin
        roiLayer = self.controller.getROILayer()
        sourceContent = self.controller.getsourceContent()
        case self.getType() of
          'image' : begin
            mapInfo = (sourceContent.getImage()).getMeta('map info')
            coordinateSystemString = (sourceContent.getImage()).getMeta('coordinate system string')
          end
          'speclib' : ;
        endcase
        writer = hubROILayerWriter()
        writer.writeClassification, result.filename, roiLayer, result.classID, result.hubGetValue('classNames'), MapInfo=mapInfo, CoordinateSystemString=coordinateSystemString
      endif
      
    end
    'exportROIsToRaster' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      roiLayer = self.controller.getROILayer()
      attributeNames = (roiLayer.getAttributeNames()).toArray()
      helper = hubHelper()
      dataTypeNames  = helper.getDataTypeInfo(/TypeDescriptions)
      dataTypeValues = helper.getDataTypeInfo(/TypeCodes)
      hubAMW_combobox,       'dataType',        Title='Data Type        ', Value=3, List=dataTypeNames, Extract=dataTypeValues
      hubAMW_parameter,      'dataIgnoreValue', Title='Data Ignore Value', /Float
      hubAMW_checklist,      'attributeIndices',TITLE='Select Attributes', LIST=attributeNames, /MultipleSelection, /COLUMN, /SHOWBUTTONBAR
      hubAMW_outputFilename, 'filename',        Title='Label Image', Value='raster'
      result = hubAMW_manage(/STRUCTURE)
      if result.accept then begin
        roiLayer = self.controller.getROILayer()
        sourceContent = self.controller.getSourceContent()
        case self.getType() of
          'image' : begin
            mapInfo = (sourceContent.getImage()).getMeta('map info')
            coordinateSystemString = (sourceContent.getImage()).getMeta('coordinate system string')
          end
          'speclib' : ;
        endcase
        writer = hubROILayerWriter()
        writer.writeRaster, result.filename, roiLayer, result.attributeIndices, result.dataType, result.dataIgnoreValue, MapInfo=mapInfo, CoordinateSystemString=coordinateSystemString
      endif
    end
    'close' : begin
      sourceContent = self.controller.getSourceContent()
      sourceContent.hideLabelingTool
    end
    'addAttribute' : begin
      widget_control,objEvent.event.id, GET_VALUE=title
      result = self.dialogNewAttribute(title, objEvent.event.top)
      if isa(result) then begin
        self.controller.addROILayerAttribute, result.name, result.format
      endif
    end
    'editAttribute' : begin
       widget_control,objEvent.event.id, GET_VALUE=title
       result = self.dialogSelectAttribute(title, objEvent.event.top, Index=self.getAttributeSelection())
       if isa(result) then begin
         roiLayer = self.controller.getROILayer()
         formats = roiLayer.getAttributeFormats()
         format = formats[result.index]
         names = roiLayer.getAttributeNames()
         name = names[result.index]
         result2 = self.dialogNewAttribute(title, objEvent.event.top, /EditAttribute, Name=name, Format=format)
         if isa(result2) then begin
           newFormat = result2.format
           newName = result2.name
           isLossyCast = (format[0] eq 'string' and newFormat[0] ne 'string') or (format[0] eq 'decimal' and newFormat[0] eq 'integer')
           if isLossyCast then begin
             ok = dialog_message(/QUESTION, ['Type cast from '+format[0]+' to '+newFormat[0]+' may be lossy.','Change attribute type?'])
             if ok eq 'No' then newFormat = format
           endif
           self.controller.editROILayerAttribute, newName, newFormat, result.index
         endif
       endif
      end
    'deleteAttribute' : begin
      widget_control, objEvent.event.id, GET_VALUE=title
      result = self.dialogSelectAttribute(title, objEvent.event.top, Index=self.getAttributeSelection())
      if isa(result) then begin
        self.controller.deleteROILayerAttribute, result.index
      endif
    end
    'calculateAttribute' : begin
      widget_control, objEvent.event.id, GET_VALUE=title
      result = self.dialogCalculateAttribute(title, objEvent.event.top, Index=self.getAttributeSelection())
      if isa(result) then begin
        self.controller.calculateROILayerAttribute, result.targetIndex, result.inputIndices, result.operator
      endif
    end
    'dissolveAttribute' : begin
      widget_control, objEvent.event.id, GET_VALUE=title
      result = self.dialogDissolveAttribute(title, objEvent.event.top, Index=self.getAttributeSelection())
      if isa(result) then begin
        self.controller.dissolveROILayerAttribute, result.targetAttribute, result.operators
      endif
    end
    'checkROIOverlap' : begin
      roiLayer = self.controller.getROILayer()
      geometryDuplicates = roiLayer.getGeometryDuplicates()
      if isa(geometryDuplicates) then begin
        roiLayer.addROI, geometryDuplicates, !null, [255,0,255]
        self.controller.initTableContent
      endif
      widget_control, objEvent.event.id, GET_VALUE=title
      case self.getType() of
        'image' :   ok = dialog_message('Number of overlapping pixels: '+strtrim(n_elements(geometryDuplicates),2)+'.', /INFORMATION, DIALOG_PARENT=objEvent.event.top, TITLE=title)
        'speclib' : ok = dialog_message('Number of overlapping profiles: '+strtrim(n_elements(geometryDuplicates),2)+'.', /INFORMATION, DIALOG_PARENT=objEvent.event.top, TITLE=title)
      endcase
    end
     '+++updateTable+++': begin
      self.initTableContent
    end
    else : begin
;      help, objEvent.event
;      print, objEvent.name
    end
  endcase
end


pro hubTBContentLabelingToolView::handleTableEvent, objEvent
  event = objEvent.event
  case objEvent.subcategory of
    'cell selection event' : begin
      eventSelection = [event.sel_left, event.sel_top, event.sel_right, event.sel_bottom]
      if product(-1 eq eventSelection) eq 1 then return ; ignore the second event were all cells are deselected; no idea why this event is generated
      self.handleTableCellEditEvent
      tableSelection = widget_info(self.table, /TABLE_SELECT)
      self.updateTableRegionSelection, tableSelection[1]
      if self.isRowSelected() then begin
        widget_control, self.table, SET_TABLE_SELECT=[-1,-1,-1,-1]
      endif
      if self.isColumnSelected() then begin
        widget_control, self.table, SET_TABLE_SELECT=[-1,-1,-1,-1]
      endif
      if self.isColorCellSelected() then begin
         widget_control, self.table, SET_TABLE_SELECT=[-1,-1,-1,-1]
         roiLayer = self.controller.getROILayer()
         colors = roiLayer.getColors()
         roiIndex = self.getRegionSelection()
         color = colors[roiIndex]
         hubAMW_program, objEvent.event.top, Title='Select ROI Color'
         hubAMW_color, 'color', Title='Select a Color', Value=color
         result = hubAMW_manage(/STRUCTURE)
         if result.accept then begin
           self.controller.setROIColor, roiIndex, result.color
         endif
      endif
    end
    'insert character event' : begin
      self.isLastEdittedCellUnformatted = 1b
      self.lastEdittedCell = [event.x, event.y]
      if event.ch eq 13 then begin
        self.handleTableCellEditEvent
      endif
    end
    'text selection event' : begin
      if self.isColorCellSelected() then begin
        widget_control, self.table, SET_TABLE_SELECT=[-1,-1,-1,-1]
      endif
    end
    else : begin
;      help, objEvent.event
;      print, '***NOT HANDLE***:', objEvent.subcategory
    end
  endcase
  self.updateView
end

pro hubTBContentLabelingToolView::handleTableCellEditEvent
  if self.isLastEdittedCellUnformatted then begin
    tableView = widget_info(self.table, /TABLE_VIEW)
    tableSelection = widget_info(self.table, /TABLE_SELECT)
    attributeIndex = tableSelection[0]
    geometryIndex = tableSelection[1]
    roiLayer = self.controller.getROILayer()
    value = self.getCellValue(self.lastEdittedCell)
    formattedValue = roiLayer.getFormattedAttributeValue(value, attributeIndex)
    self.setCellValue, self.lastEdittedCell, formattedValue
;    print, attributeIndex, geometryIndex, ' ', formattedValue
    roiLayer.setAttributeValue, self.lastEdittedCell[0], self.lastEdittedCell[1], formattedValue
;    print, roiLayer.getAttributeValue(self.lastEdittedCell[0], self.lastEdittedCell[1])
    self.isLastEdittedCellUnformatted = 0b
  endif
end

function hubTBContentLabelingToolView::dialogSelectAttribute, title, groupLeader, Index=index
  roiLayer = self.controller.getROILayer()
  attributeNames = roiLayer.getAttributeNames()
  hubAMW_program, groupLeader, Title=title
  hubAMW_list, 'index', LIST=attributeNames.toArray(), Title='Select Attribute', Value=index, XSIZE=300, YSIZE=300
  result = hubAMW_manage(/STRUCTURE)
  if result.accept then begin
    parameters = {index:result.index}
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

function hubTBContentLabelingToolView::dialogCalculateAttribute, title, groupLeader, Index=index
  roiLayer = self.controller.getROILayer()
  attributeNames = roiLayer.getAttributeNames()
  hubAMW_program, groupLeader, Title=title
  hubAMW_combobox, 'targetIndex', Title='Target Attribute', LIST=attributeNames.toArray(), Value=index
  hubAMW_checklist, 'inputIndices', LIST=attributeNames.toArray(), Title='Input Attributes', /MultipleSelection, /Column
  hubAMW_combobox, 'operator', Title='Operator',$ 
    List   =['First','Last','Concatinated String','Sum','Mean','Min','Max','Range','StdDev'],$
    Extract=['first','last','concat',             'sum','mean','min','max','range','stdDev']
  result = hubAMW_manage(/STRUCTURE)
  if ~result.accept then begin
    result = !null
  end
  return, result
end

function hubTBContentLabelingToolView::dialogDissolveAttribute, title, groupLeader, Index=index
  result1 = self.dialogSelectAttribute(title, groupLeader, Index=index)
  if isa(result1) then begin 
    roiLayer = self.controller.getROILayer()
    attributeNames = roiLayer.getAttributeNames()
    numberOfAttributes = roiLayer.getNumberOfAttributes()
    hubAMW_program, groupLeader, Title=title
    hubAMW_label, 'Select Features and choose Operator.'
    for i=0,numberOfAttributes-1 do begin
      if i eq result1.index then continue
      hubAMW_combobox, string(i), Title=attributeNames[i], Optional=1, TSize=150, $
        List   =['First','Last','Concatinated String','Sum','Mean','Min','Max','Range','StdDev'],$
        Extract=['first','last','concat',             'sum','mean','min','max','range','stdDev']
    endfor
    hubAMW_checkbox, 'deleteROIs', Title='Delete current ROIs?', Value=0
    result2 = hubAMW_manage()
    if ~result2['accept'] then begin
      result = !null
    endif else begin
      result2.remove, 'accept'
      result = hash()
      result['deleteROIs'] = result2.remove('deleteROIs')
      result['targetAttribute'] = result1.index
      operators = list(LENGTH=numberOfAttributes)
      foreach operator, result2, key do begin
        operators[key] = operator
      endforeach
      operators[result1.index] = 'first' 
      result['operators'] = operators.toArray()
      result = result.toStruct()
    endelse
  endif else begin
    result = !null
  endelse
  return, result
end

function hubTBContentLabelingToolView::dialogNewAttribute, title, groupLeader, EditAttribute=editAttribute, Format=format, Name=name
  hubAMW_program, groupLeader, Title=title
  hubAMW_parameter, 'name', Title='Name', Value=keyword_set(editAttribute)?name:'AttributeName', /STRING
  hubAMW_subframe, 'selection', Title='String', /ROW, SetButton=keyword_set(editAttribute) && format[0] eq 'string'
  hubAMW_subframe, 'selection', Title='Integer Number', /ROW, SetButton=(keyword_set(editAttribute) && format[0] eq 'integer') || ~keyword_set(editAttribute)
  hubAMW_subframe, 'selection', Title='Decimal Number', /ROW, SetButton=keyword_set(editAttribute) && format[0] eq 'decimal'
  hubAMW_parameter, 'decimal', Title='Decimal Precision', Value=(keyword_set(editAttribute) && (format[0] eq 'decimal') ? format[1] : 2), IsGE=0, /INTEGER
  result = hubAMW_manage(/STRUCTURE)
  if result.accept then begin
    case result.selection of
      0 : attributeFormat = 'string'
      1 : attributeFormat = 'integer'
      2 : attributeFormat = ['decimal',strtrim(result.decimal,2)]
    endcase
    attributeName = result.name
    parameters = {format:attributeFormat, name:attributeName}
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

pro hubTBContentLabelingToolView::setMode, Add=add, Remove=remove, Off=off
  case 1b of
    keyword_set(add) :    widget_control, /SET_BUTTON, self.modeAdd
    keyword_set(remove) : widget_control, /SET_BUTTON, self.modeRemove
    keyword_set(off) :    widget_control, /SET_BUTTON, self.modeOff
   endcase
end

function hubTBContentLabelingToolView::getMode, Add=add, Remove=remove, Off=off
  case 1b of
    keyword_set(add) :    return, widget_info(/BUTTON_SET, self.modeAdd)
    keyword_set(remove) : return, widget_info(/BUTTON_SET, self.modeRemove)
    keyword_set(off) :    return, widget_info(/BUTTON_SET, self.modeOff)
  endcase
end

function hubTBContentLabelingToolView::getRegionSelection
  return, self.regionSelection
end

pro hubTBContentLabelingToolView::setRegionSelection, roiIndex
  self.regionSelection = roiIndex
end

function hubTBContentLabelingToolView::getAttributeSelection
  tableSelection = widget_info(self.table, /TABLE_SELECT)
  index = tableSelection[0]
  if index ge 0 and ~self.isColorCellSelected() then begin
    result = index
  endif else begin
    result = !null
  endelse
  return, result
end

function hubTBContentLabelingToolView::getCellValue, cellPosition
  tableView = widget_info(self.table, /TABLE_VIEW)
  tableSelection = widget_info(self.table, /TABLE_SELECT)
  attributeIndex = tableSelection[0]
  widget_control, self.table, UPDATE=0
  widget_control, self.table, SET_TABLE_SELECT=[cellPosition,cellPosition]
  widget_control, self.table, GET_VALUE=value, /USE_TABLE_SELECT
  widget_control, self.table, SET_TABLE_SELECT=tableSelection
  widget_control, self.table, SET_TABLE_VIEW=tableView
  widget_control, self.table, UPDATE=1
  value = hub_fix0d(value)
  return, value
end

pro hubTBContentLabelingToolView::setCellValue, cellPosition, value
  tableView = widget_info(self.table, /TABLE_VIEW)
  tableSelection = widget_info(self.table, /TABLE_SELECT)
  widget_control, self.table, UPDATE=0
  widget_control, self.table, SET_TABLE_SELECT=[cellPosition,cellPosition]
  widget_control, self.table, SET_VALUE=[value], /USE_TABLE_SELECT
  widget_control, self.table, SET_TABLE_SELECT=tableSelection
  widget_control, self.table, SET_TABLE_VIEW=tableView
  widget_control, self.table, UPDATE=1
end

function hubTBContentLabelingToolView::getType
  return, self.controller.getType()
end

function hubTBContentLabelingToolView::isColorCellSelected
  roiLayer = self.controller.getROILayer()
  numberOfAttributes = roiLayer.getNumberOfAttributes()
  tableSelection = widget_info(self.table, /TABLE_SELECT)
  result = tableSelection[0] eq numberOfAttributes and tableSelection[2] eq numberOfAttributes 
  return, result
end

function hubTBContentLabelingToolView::isRowSelected
  roiLayer = self.controller.getROILayer()
  numberOfAttributes = roiLayer.getNumberOfAttributes()
  tableSelection = widget_info(self.table, /TABLE_SELECT)
  result = tableSelection[0] ne tableSelection[2] and tableSelection[1] eq tableSelection[3]
  return, result
end

function hubTBContentLabelingToolView::isColumnSelected
  roiLayer = self.controller.getROILayer()
  numberOfGeometries = roiLayer.getNumberOfGeometries()
  tableSelection = widget_info(self.table, /TABLE_SELECT)
  result = tableSelection[0] eq tableSelection[2] and tableSelection[1] ne tableSelection[3]
  return, result
end

pro hubTBContentLabelingToolView__define
  define = {hubTBContentLabelingToolView, inherits hubTBContentMVCView,$
    mainBase : 0l,$
    menuBase : 0l,$
    buttonBase : 0l,$
    table : 0l,$
    modeAdd : 0l,$
    modeRemove : 0l,$
    modeOff : 0l,$
    regionSelection:0l,$
    gotoPosition:0ull,$
    isLastEdittedCellUnformatted:0b,$
    lastEdittedCell:[0l,0l]}    
end