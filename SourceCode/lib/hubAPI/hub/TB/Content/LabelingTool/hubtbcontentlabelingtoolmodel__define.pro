function hubTBContentLabelingToolModel::getType
  return, self.controller.getType()
end

pro hubTBContentLabelingToolModel::setSourceContent, sourceContent
  case self.getType() of
    'image' : correctType = isa(sourceContent, 'hubTBManagedContentImage')
    'speclib' : correctType = isa(sourceContent, 'hubTBManagedContentSpeclib')    
  endcase
  if ~correctType then begin
    message, 'Wrong argument.'
  endif
  self.sourceContent = sourceContent
  self.title = 'Labeling Tool: '+sourceContent.getTitle()
  case self.getType() of
    'image' : layerSize = sourceContent.getImageSize()
    'speclib' : layerSize = [1,sourceContent.getSpeclibSize()]
  endcase
  self.roiLayer = hubROILayer(layerSize)
  self.ensureROI
end

function hubTBContentLabelingToolModel::getSourceContent
  return, self.sourceContent
end

function hubTBContentLabelingToolModel::getROILayer
  return, self.roiLayer
end

pro hubTBContentLabelingToolModel::setNofifyLabeling, boolean
  self.notifyLabeling = boolean
end

function hubTBContentLabelingToolModel::getNofifyLabeling
  return, self.notifyLabeling
end

pro hubTBContentLabelingToolModel::appendROILayer, roiLayer
  self.finishGraphic
  newLayer = hubROILayerProcessor.join(self.roiLayer, roiLayer)
  self.roiLayer = newLayer
  self.initGraphic
  self.controller.initTableContent 
  self.setNofifyLabeling, 1b
end

pro hubTBContentLabelingToolModel::addROILayerAttribute, attributeName, attributeFormat
  ; efficient implementation needed?
  self.roiLayer.addAttribute, attributeName, attributeFormat
  self.controller.initTableContent
end

pro hubTBContentLabelingToolModel::editROILayerAttribute, attributeName, attributeFormat, attributeIndex
  ; efficient implementation needed?
  self.roiLayer.editAttribute, attributeName, attributeFormat, attributeIndex
  self.controller.initTableContent
end

pro hubTBContentLabelingToolModel::deleteROILayerAttribute, attributeIndex
  ; efficient implementation needed?
  self.roiLayer.deleteAttribute, attributeIndex
  self.controller.initTableContent
end

pro hubTBContentLabelingToolModel::calculateROILayerAttribute, targetAttributeIndex, inputAttributeIndices, operator
  self.roiLayer.calculateAttribute, targetAttributeIndex, inputAttributeIndices, operator
  self.controller.initTableContent
end

pro hubTBContentLabelingToolModel::dissolveROILayerAttribute, attributeIndex, operators
  self.finishGraphic
  processor = hubROILayerProcessor()
  newLayer = processor.dissolve(self.roiLayer, attributeIndex, operators)
  self.roiLayer = newLayer
  self.initGraphic
  self.controller.initTableContent
end

pro hubTBContentLabelingToolModel::deleteROI, roiIndex
  ; efficient implementation needed?
  self.roiLayer.deleteROI, roiIndex
  self.ensureROI
  self.controller.initTableContent
  self.setNofifyLabeling, 1b
  self.sourceContent.updateView
end

pro hubTBContentLabelingToolModel::emptyROI, roiIndex
  self.roiLayer.emptyROI, roiIndex
  self.controller.initTableContent
  self.setNofifyLabeling, 1b
  self.sourceContent.updateView
end

pro hubTBContentLabelingToolModel::ensureROI
  if self.roiLayer.getNumberOfGeometries() eq 0 then begin
    self.newROI
  endif
end

pro hubTBContentLabelingToolModel::newROI
  self.roiLayer.addROI, [], []
  self.controller.initTableContent
  self.sourceContent.updateView
  self.controller.setRegionSelection, /Last
end

pro hubTBContentLabelingToolModel::setROIColor, roiIndex, color
  self.roiLayer.setROIColor, roiIndex, color
  self.controller.initTableContent, ROIIndex=roiIndex
  self.setNofifyLabeling, 1b
  self.sourceContent.updateView
end

pro hubTBContentLabelingToolModel::initGraphic
  oModel = self.sourceContent.controller.model.getModel()
  self.roiLayer.initGraphic, oModel
  if self.getType() eq 'speclib' then begin
    (self.roiLayer.getModel()).setProperty, hide=1b
  endif
;  self.sourceContent.updateSize
;  self.sourceContent.updateView
end

pro hubTBContentLabelingToolModel::finishGraphic
  self.roiLayer.finishGraphic
  self.sourceContent.updateView
end

pro hubTBContentLabelingToolModel::show
  self.initGraphic
end

pro hubTBContentLabelingToolModel::hide
  self.finishGraphic
end

pro hubTBContentLabelingToolModel__define
  define = {hubTBContentLabelingToolModel, inherits hubTBContentMVCModel,$
    sourceContent:obj_new(),$
    roiLayer:obj_new(),$
    notifyLabeling:0b $
  } 
    
end