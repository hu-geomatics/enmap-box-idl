function hubTBContentLabelingTool::init, toolbox, type
  case type of
    'image' :   ; ok
    'speclib' : ; ok
  endcase
  self.type = type
  !null = self.hubTBContentMVC::init(hubTBContentLabelingToolController(), hubTBContentLabelingToolModel(), toolbox)
  return, 1b
end

function hubTBContentLabelingTool::getType
  return, self.type
end

pro hubTBContentLabelingTool::setSourceContent, sourceContent
  self.controller.setSourceContent, sourceContent
end

pro hubTBContentLabelingTool::show
  self.controller.show
end

pro hubTBContentLabelingTool::hide
  self.controller.hide
end

function hubTBContentLabelingTool::isActive
  return, self.controller.isActive()
end

pro hubTBContentLabelingTool::setMode, _EXTRA=_extra
  self.controller.setMode, _EXTRA=_extra
end

function hubTBContentLabelingTool::getMode, _EXTRA=_extra
  return, self.controller.getMode(_EXTRA=_extra)
end

function hubTBContentLabelingTool::getROILayer
  return, self.controller.getROILayer()
end

function hubTBContentLabelingTool::getRegionSelection
  return, self.controller.getRegionSelection()
end

pro hubTBContentLabelingTool::addPixel, roiIndex, pixelIndex
  self.controller.addPixel, roiIndex, pixelIndex
end

pro hubTBContentLabelingTool::removePixel, roiIndex, pixelIndex
  self.controller.removePixel, roiIndex, pixelIndex
end

pro hubTBContentLabelingTool__define

  define = {hubTBContentLabelingTool, inherits hubTBContentMVC, type:''}
  
end