function hubTBContentLabelingToolView_consistencyCheck_importROIsFromRasterBand, resultHash, Message=message, UserInformation=userInformation
  
  image = hubIOImgInputImage(resultHash['filename'])
  imageSize = image.getSpatialSize()
  layerSize = userInformation['layerSize']
  layerType = userInformation['layerType']
  case layerType of
    'image' : layerName = 'ROI'
    'speclib' : layerName = 'SOI'
  endcase
  message = layerType+' size ['+strjoin(strtrim(imageSize,2), ',')+'] is not matching '+layerName+' layer size ['+strjoin(strtrim(layerSize,2), ',')+'].'
  isConsistent = image.isCorrectSpatialSize(layerSize)
  return, isConsistent
end