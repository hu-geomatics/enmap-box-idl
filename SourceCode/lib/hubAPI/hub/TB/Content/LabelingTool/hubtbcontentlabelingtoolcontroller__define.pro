function hubTBContentLabelingToolController::newView
  return, hubTBContentLabelingToolView()
end

function hubTBContentLabelingToolController::getType
  return, self.mvc.getType()
end

pro hubTBContentLabelingToolController::setSourceContent, sourceContent
  self.model.setSourceContent, sourceContent
end

function hubTBContentLabelingToolController::getSourceContent
  return, self.model.getSourceContent()
end

function hubTBContentLabelingToolController::getROILayer
  return, self.model.getROILayer()
end

pro hubTBContentLabelingToolController::appendROILayer, roiLayer
  self.model.appendROILayer, roiLayer
end

pro hubTBContentLabelingToolController::addROILayerAttribute, attributeName, attributeFormat
  self.model.addROILayerAttribute, attributeName, attributeFormat
end

pro hubTBContentLabelingToolController::editROILayerAttribute, attributeName, attributeFormat, attributeIndex
  self.model.editROILayerAttribute, attributeName, attributeFormat, attributeIndex
end

pro hubTBContentLabelingToolController::deleteROILayerAttribute, attributeIndex
  self.model.deleteROILayerAttribute, attributeIndex
end

pro hubTBContentLabelingToolController::calculateROILayerAttribute, targetAttributeIndex, inputAttributeIndices, operator
  self.model.calculateROILayerAttribute, targetAttributeIndex, inputAttributeIndices, operator
end

pro hubTBContentLabelingToolController::dissolveROILayerAttribute, targetAttributeIndex, operators
  self.model.dissolveROILayerAttribute, targetAttributeIndex, operators
end

pro hubTBContentLabelingToolController::gotoROIPixel, roiIndex, pixelIndex
  roiLayer = self.getROILayer()
  position = roiLayer.getPixel(roiIndex, pixelIndex)
  if isa(position) then begin
    sourceContent = self.model.getSourceContent()
    case self.getType() of
      'image' : begin
        sourceContent.setImageSelection, position, /PixelCenter
        sourceContent.setImageCenter, /ImageSelection
        sourceContent.controller.synchronizeLinkedImages
        sourceContent.controller.synchronizeLinkedSpeclibs
       end
       'speclib' : begin
         sourceContent.controller.setPlotSelectionByIndices, position
      end
     endcase
    sourceContent.updateView
  endif else begin
    case self.getType() of
      'image' : ok = dialog_message(/INFORMATION, 'ROI is empty.')
      'speclib' : ok = dialog_message(/INFORMATION, 'SOI is empty.')
    endcase
  endelse
end

pro hubTBContentLabelingToolController::deleteROI, roiIndex
  self.model.deleteROI, roiIndex
end

pro hubTBContentLabelingToolController::emptyROI, roiIndex
  self.model.emptyROI, roiIndex
end

pro hubTBContentLabelingToolController::newROI
  self.model.newROI
end

pro hubTBContentLabelingToolController::setROIColor, roiIndex, color
  self.model.setROIColor, roiIndex, color
end

pro hubTBContentLabelingToolController::show
  self.model.show
end

pro hubTBContentLabelingToolController::hide
  self.model.hide
end

pro hubTBContentLabelingToolController::initTableContent, ROIIndex=roiIndex
  if obj_valid(self.views) then (self.views)[0].initTableContent, ROIIndex=roiIndex
end

function hubTBContentLabelingToolController::isActive
  frames = self.getFrames()
  isActive = frames.count() gt 0
  return, isActive
end

pro hubTBContentLabelingToolController::setMode, _EXTRA=_extra
  (self.views)[0].setMode, _EXTRA=_extra
end

function hubTBContentLabelingToolController::getMode, _EXTRA=_extra
  return, (self.views)[0].getMode(_EXTRA=_extra)
end

function hubTBContentLabelingToolController::getRegionSelection
  return, (self.views)[0].getRegionSelection()
end

pro hubTBContentLabelingToolController::setRegionSelection, roiIndex, Last=last
  if obj_valid(self.views) then (self.views)[0].updateTableRegionSelection, roiIndex, Last=last, /Scroll
end

pro hubTBContentLabelingToolController::addPixel, roiIndex, pixelIndex
  roiLayer = self.getROILayer()
  roiLayer.addPixel, roiIndex, pixelIndex
  (self.views)[0].initTableContentSizes
end

pro hubTBContentLabelingToolController::removePixel, roiIndex, pixelIndex
  roiLayer = self.getROILayer()
  roiLayer.removePixel, roiIndex, pixelIndex 
  (self.views)[0].initTableContentSizes
end

pro hubTBContentLabelingToolController::setNofifyLabeling, boolean
  self.model.setNofifyLabeling, boolean
end

function hubTBContentLabelingToolController::getNofifyLabeling
  return, self.model.getNofifyLabeling()
end

pro hubTBContentLabelingToolController__define
  define = {hubTBContentLabelingToolController, inherits hubTBContentMVCController}
end
