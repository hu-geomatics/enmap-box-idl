;pro hubTBContentMVCController::create
;  stop ; not needer or?
;  foreach view, self.views do begin
;    self.createView, view
;  endforeach
;end

pro hubTBContentMVCController::createView, view
  message, 'abstract methode'
end

pro hubTBContentMVCController::updateSize
  foreach view, self.views do begin
    if ~view.isCreated() then message, 'View not created.'
    frame = self.findFrameByView(view)
    if ~frame.isDetached then begin
      view.updateSize  
    endif
  endforeach
end

pro hubTBContentMVCController::updateView
  foreach view, self.views do begin
    if ~view.isCreated() then message, 'View not created.'
;    frame = self.findFrameByView(view)
    view.updateView
  endforeach
end

pro hubTBContentMVCController::subscribeFrame, frame
  ; init list if not already done
  if ~isa(self.subscribedFrames, 'list') then self.subscribedFrames = list()
  
  ; check if frame is already subscriped
  if self.subscribedFrames.hubIsElement(frame) then begin
    message, 'Frame already subscribed.'  
  endif
  
  ; add frame to subcribed frames list 
  self.subscribedFrames.add, frame

end

pro hubTBContentMVCController::unsubscribeFrame, frame
  
  ; check if frame is already subscriped
  if ~self.subscribedFrames.hubIsElement(frame, Index=index) then begin
    message, 'Frame not subscribed.'
  endif

  ; delete frame from subcribed frames list
  self.subscribedFrames.remove, index
end

function hubTBContentMVCController::getFrames
  return, self.subscribedFrames
end

function hubTBContentMVCController::findFrameByView, view
  foreach frame, self.subscribedFrames do begin
    if view eq frame.subscribedView then begin
      return, frame
    endif
  endforeach
  message, 'unexpected error'
end  

function hubTBContentMVCController::getViews
  views = list()
  foreach frame, self.subscribedFrames do begin
    views.add, frame.subscribedView
  endforeach
  return, views
end

function hubTBContentMVCController::getViewSize, view
  frame = self.findFrameByView(view)
  viewSize = frame.getContentSize()
  return, viewSize
end

function hubTBContentMVCController::getTopLevelBase
  return, self.mvc.toolbox.getTopLevelBase()
end

function hubTBContentMVCController::getTitle
  return, self.model.getTitle()
end

function hubTBContentMVCController::getToolbox
  return, self.mvc.toolbox
end

pro hubTBContentMVCController__define
  define = {hubTBContentMVCController, inherits hubMVCController,$
    subscribedFrames:list()}
end