pro hubTBContentMVCView::create
  message, 'abstract methode'
end

pro hubTBContentMVCView::delete
  message, 'abstract methode'
end

pro hubTBContentMVCView::updateSize
  message, 'abstract methode'
end

pro hubTBContentMVCView::updateView
  message, 'abstract methode'
end

;pro hubTBContentMVCView::sendMessageUpdate
;  self.controller.updateView
;end

pro hubTBContentMVCView::setBase, base
  if ~widget_info(/VALID_ID, base) then message, 'Not a valid widget id.'
  self.base = base
end

function hubTBContentMVCView::isCreated
  return, widget_info(/VALID_ID, self.base)
end

function hubTBContentMVCView::isDetached
  return, self.controller.isDetached(self)
end

function hubTBContentMVCView::getSize
  return, self.controller.getViewSize(self)
end

pro hubTBContentMVCView::setProperty, _EXTRA=_extra, base=base
  self.hubMVCView::setProperty, _EXTRA=_extra
  if isa(base) then self.base = base
end

function hubTBContentMVCView::getToolbox
  return, self.controller.getToolbox()
end

pro hubTBContentMVCView::getProperty, _REF_EXTRA=_extra, base=base
  self.hubMVCView::getProperty, _EXTRA=_extra
  if arg_present(base) then base = self.base
end

pro hubTBContentMVCView__define
  define = {hubTBContentMVCView, inherits hubMVCView,$
    base : 0l}
end