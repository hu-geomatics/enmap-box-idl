function hubTBContentMVC::init, controller, model, toolbox
  if ~isa(toolbox, 'hubTBToolbox') then message, 'Wrong argument.'
  self.toolbox = toolbox
  !null = self.hubMVC::init(controller, model)
  return, 1b
end

pro hubTBContentMVC::create
  self.controller.create
end

pro hubTBContentMVC::updateSize
  self.controller.updateSize
end

pro hubTBContentMVC::updateView
  self.controller.updateView
end

pro hubTBContentMVC::subscribeFrame, frame
  self.controller.subscribeFrame, frame
end

pro hubTBContentMVC::unsubscribeFrame, frame
  self.controller.unsubscribeFrame, frame
end

function hubTBContentMVC::getTitle
  return, self.controller.getTitle()
end

pro hubTBContentMVC::getProperty, _REF_EXTRA=_extra, toolbox=toolbox
  self.hubMVC::getProperty, _EXTRA=_extra
  if arg_present(toolbox) then toolbox = self.toolbox
end

pro hubTBContentMVC__define

  define = {hubTBContentMVC, inherits hubMVC,$
    toolbox:obj_new()}
  
end