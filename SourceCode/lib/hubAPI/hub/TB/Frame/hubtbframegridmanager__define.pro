function hubTBFrameGridManager::init, parentFrame, tabFrameManager, _EXTRA=_extra
  !null = self.hubTBFrame::init(parentFrame, _EXTRA=_extra, /Frame, /ShowTitleBar)
  self.setTabFrameManager, tabFrameManager
  return, 1b
end

pro hubTBFrameGridManager::createChildFrames
end

pro hubTBFrameGridManager::createTitleBar, base
  self.hubTBFrame::createTitleBar, base
  self.baseButton = widget_base(base, /ROW, XPAD=0, YPAD=0, SPACE=0)
  
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/AddLeft), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'insertLeft'), TOOLTIP='Add new view to the left.')
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/AddRight), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'insertRight'), TOOLTIP='Add new view to the right.')
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/AddTop), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'insertAbove'), TOOLTIP='Add new view above.')
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/AddDown), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'insertBelow'), TOOLTIP='Add new view below.')
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/Detach), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'detachView'), TOOLTIP='Detach view.')
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/Close), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'closeView'), TOOLTIP='Remove view.')
  !null = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/LinkImages), /BITMAP, /MASK, UVALUE=hubObjCallbackEvent(self, 'linkImages'), TOOLTIP='Link image views.')
  buttonAlignment = widget_button(self.baseButton, Value=hubTBAuxiliaryGetIconBitmaps(/Align), /BITMAP, /MASK, /Menu, TOOLTIP='Align views.')
  !null = widget_button(buttonAlignment, Value='Single View', UVALUE=hubObjCallbackEvent(self, 'alignment', [1]))
  for i=1,4 do for j=1,4 do begin
    if i eq 1 and j eq 1 then continue
    strI = strtrim(i,2)
    strJ = strtrim(j,2)
    !null = widget_button(buttonAlignment, Value=strI+'x'+strJ+' Views', UVALUE=hubObjCallbackEvent(self, 'alignment', replicate(i,j)))    
  endfor
  
  tmpBase = widget_base(self.baseButton, XPAD=0, YPAD=0, /NONEXCLUSIVE, MAP=0)
  self.buttonShowMenuBars = widget_button(tmpBase, Value='Show Menus', UVALUE=hubObjCallbackEvent(self, 'showViewBars'))

end

pro hubTBFrameGridManager::updateTitleBar
  sizeContent = self.getContentSize()
  labelWidth = (widget_info(/GEOMETRY, self.labelTitle)).scr_xsize  
  buttonsOnTheLeftWidth = (widget_info(/GEOMETRY, self.baseButton)).scr_xsize
  buttonsOnTheRightWidth = 0
  if self.showClose then buttonsOnTheRightWidth += (widget_info(/GEOMETRY, self.buttonClose)).scr_xsize
  if self.showDetach then buttonsOnTheRightWidth += (widget_info(/GEOMETRY, self.buttonDetach)).scr_xsize
  fillerSize = sizeContent[0]-buttonsOnTheLeftWidth-buttonsOnTheRightWidth-labelWidth
  widget_control, self.baseTitleBarFiller, SCR_XSIZE=fillerSize>1
end

pro hubTBFrameGridManager::update, _EXTRA=_extra
  self.hubTBFrame::update, _EXTRA=_extra
  self.setTitles
  if ~isa(self.getSelectedFrame()) and ~self.childFrames.isEmpty()  then begin
    self.selectFrame, self.childFrames[0]
  endif
end

pro hubTBFrameGridManager::setTabFrameManager, tabFrameManager
  if ~isa(tabFrameManager, 'hubTBFrameTabManager') then begin
    message, 'Wrong argument.'
  endif
  self.tabFrameManager = tabFrameManager
end

function hubTBFrameGridManager::getTabFrameManager
  return, self.tabFrameManager
end

pro hubTBFrameGridManager::addFrame, frame, index, UserAligned=userAligned
  if ~isa(frame, 'hubTBManagedFrame') then message, 'Wrong argument.'
  self.hubTBFrame::addFrame, frame, index, UserAligned=userAligned
end

function hubTBFrameGridManager::addFrameWithContent, Content=content, _EXTRA=_extra
  if ~isa(content) then begin
    content = hubTBManagedContentMVC(self.toolbox, self)
  endif
  managedFrame = hubTBManagedFrame(self, content, _EXTRA=_extra)
  return, managedFrame
end

function hubTBFrameGridManager::addFrameWithContentRelativeToTargetFrame, targetFrame,  _EXTRA=_extra, $
  Left=left, Right=right, Above=above, Below=below
  
  indexTargetFrame = self.findFrame(targetFrame)
  targetFrameSegmentIndex = self.layoutManager.findFrameSegment(targetFrame)
  alignment = self.layoutManager.alignment
  if ~self.layoutManager.column then begin
    if keyword_set(left) then begin
      alignment[targetFrameSegmentIndex] += 1
      index = indexTargetFrame
    endif
    if keyword_set(right) then begin
      alignment[targetFrameSegmentIndex] += 1
      index = indexTargetFrame+1
    endif
    if keyword_set(above) then begin
      firstFrameInTargetFrameSegmentIndex = 0
      for i=0,targetFrameSegmentIndex-1 do firstFrameInTargetFrameSegmentIndex += alignment[i]
      index = firstFrameInTargetFrameSegmentIndex
      alignment.add, 1, targetFrameSegmentIndex
    endif
    if keyword_set(below) then begin
      firstFrameInTargetFrameSegmentIndex = 0
      for i=0,targetFrameSegmentIndex do firstFrameInTargetFrameSegmentIndex += alignment[i]
      index = firstFrameInTargetFrameSegmentIndex
      alignment.add, 1, targetFrameSegmentIndex+1
    endif

  endif else begin
    message, 'not yet implemented.'
  endelse
  managedFrame = self.addFrameWithContent(Index=index, /UserAligned, _EXTRA=_extra)
  return, managedFrame
end

pro hubTBFrameGridManager::removeManagedFrame, frame, NoUpdate=noUpdate, RemoveDependencies=removeDependencies
  if keyword_set(removeDependencies) then begin
    content = frame.getContent()
    content.controller.removeDependencies
  endif
  self.hubTBFrame::removeFrame, frame
  self.ensureDefaultFrame
  if ~keyword_set(noUpdate) then begin
    self.updateView
  endif
end

pro hubTBFrameGridManager::detachManagedFrame, frame
  self.toolbox.detachFrame, frame
end

pro hubTBFrameGridManager::swapFrames, dragFrame, dropFrame

  if isa(dropFrame, 'hubTBManagedFrame') then begin
    dragIndex = self.findFrame(dragFrame)
    dropIndex = self.findFrame(dropFrame)
    (self.childFrames)[dragIndex] = dropFrame
    (self.childFrames)[dropIndex] = dragFrame
    self.updateView
    self.updateContent
  endif else begin
    dragContent = dragFrame.getContent()
    dropContent = dropFrame.getContent()
    self.toolbox.deleteContentInFrame, dragFrame
    self.toolbox.deleteContentInFrame, dropFrame
    self.toolbox.showContentInFrame, dragContent, dropFrame
    self.toolbox.showContentInFrame, dropContent, dragFrame
    dragFrame.updateView, /SizeOnly, /OffsetOnly
    dropFrame.updateView, /SizeOnly, /OffsetOnly
  endelse
 
end

pro hubTBFrameGridManager::ensureDefaultFrame
  numberOfAttachedFrames = 0
  foreach frame,self.childFrames do begin
    numberOfAttachedFrames += (frame.isDetached eq 0)
  endforeach
  if numberOfAttachedFrames eq 0 then begin
    newFrame = self.addFrameWithContent(/ShowTitleBar, /ShowDetach, /ShowClose)
  endif
  self.ensureDefaultSelection
end

pro hubTBFrameGridManager::ensureDefaultSelection
  if ~isa(self.getSelectedFrame()) then begin
    self.selectFrame, (self.childFrames)[0]
  endif
end

pro hubTBFrameGridManager::selectContent, selectedContent
  foreach content, self.getContents() do begin
    isSelected = (content eq selectedContent) 
    content.setIsSelected, isSelected
  endforeach
  self.updateView
  self.updateContentView
end

pro hubTBFrameGridManager::selectFrame, selectedFrame
  self.selectContent, selectedFrame.getContent()
end

function hubTBFrameGridManager::getSelectedFrame
  result = !null
  foreach frame, self.childFrames do begin
    content = frame.getContent()
    if content.getIsSelected() then begin
      result = frame
      break
    endif  
  endforeach
  return, result
end

function hubTBFrameGridManager::getContents, Images=images, Speclibs=speclibs
  contents = list()
  foreach frame, self.childFrames do begin
    content = frame.getContent()
    if keyword_set(images) && ~isa(content, 'hubTBManagedContentImage') then continue ;only images
    if keyword_set(speclibs) && ~isa(content, 'hubTBManagedContentSpeclib') then continue ;only speclibs
    contents.add, content
  endforeach
  return, contents
end

pro hubTBFrameGridManager::swapShowMenuBars
  widget_control, self.buttonShowMenuBars, SET_BUTTON=~widget_info(/BUTTON_SET, self.buttonShowMenuBars)
end

function hubTBFrameGridManager::getShowMenuBars
  return, widget_info(/BUTTON_SET, self.buttonShowMenuBars)
end

pro hubTBFrameGridManager::setAlignment, alignmentCounts, FillMissing=fillMissing
  
  totalAlignmentCounts = fix(total(alignmentCounts))
  if keyword_set(fillMissing) then begin
    numberOfMissingFrames = totalAlignmentCounts-self.childFrames.count()
    ; add empty frames if needed
    for i=1, numberOfMissingFrames do begin
      newFrame = self.addFrameWithContent(/ShowTitleBar, /ShowDetach, /ShowClose)
    endfor
    ;check if non empty frames have to be removed
    nonEmptyFramesPresent = 0b
    for i=totalAlignmentCounts,self.childFrames.count()-1 do begin
      frame = (self.childFrames)[i]
      content = frame.getContent()
      isEmpty = strcmp(typename(content), 'HubTBManagedContentMVC', /FOLD_CASE)
      nonEmptyFramesPresent or= ~isEmpty
    endfor

    if nonEmptyFramesPresent then begin
      ok = dialog_message(/QUESTION, 'Non-empty views are outside the alignment. Should they be removed?')
      if ok eq 'No' then return
    endif
    ; remove empty frames if needed
    for i=totalAlignmentCounts,self.childFrames.count()-1 do begin
      self.removeFrame, (self.childFrames)[-1]
    endfor
  endif

  if totalAlignmentCounts ne self.childFrames.count() then begin
    message, 'Inconsistent alignment.'
  endif
  alignment = self.layoutManager.alignment
  alignment.remove, /All
  alignment.add, alignmentCounts, /EXTRACT
end

pro hubTBFrameGridManager::setTitles
  foreach frame, self.childFrames do begin
    frame.setTitle
  endforeach
end

pro hubTBFrameGridManager::setImageLinkType, type
  types = list('no link','pixel link','geo link')
  if ~types.hubIsElement(type) then begin
    message, 'Wrong type.'
  endif
  self.imageLinkType = type
end

pro hubTBFrameGridManager::synchronizeLinkedImages, masterSourceContent
  
  if self.imageLinkType eq 'no link' then return
  if ~masterSourceContent.getImagePositionLinked() then return
  
  imageCenterMaster = masterSourceContent.getImageCenter()
  imageSelectionMaster = masterSourceContent.getImageSelection()

  imageMaster = masterSourceContent.getImage()
  xyOffsetMaster = [imageMaster.getMeta('x start', Default=0), imageMaster.getMeta('y start', Default=0)]
  zoomMaster = masterSourceContent.getImageZoom()
  case self.imageLinkType of
    'pixel link' : begin
      imageCenterMaster += xyOffsetMaster
      imageSelectionMaster += xyOffsetMaster
    end
    'geo link' : begin
      mapInfoMaster = imageMaster.getMeta('map info')
      if ~isa(mapInfoMaster) then return
      pixelSizeMaster = mean([mapInfoMaster.sizeX, mapInfoMaster.sizeY])
      imageCenterMaster = masterSourceContent.convertPixelToGeo(imageCenterMaster)
      imageSelectionMaster = masterSourceContent.convertPixelToGeo(imageSelectionMaster)
    end
  endcase

  foreach slaveSourceContent, self.getContents() do begin
    if ~isa(slaveSourceContent, 'hubTBManagedContentImage') then continue
    if slaveSourceContent eq masterSourceContent then continue
    if ~slaveSourceContent.getImagePositionLinked() then continue
    
    imageSlave = slaveSourceContent.getImage()
    xyOffsetSlave = [imageSlave.getMeta('x start', Default=0), imageSlave.getMeta('y start', Default=0)]
    case self.imageLinkType of
      'pixel link' : begin
        imageCenterSlave = imageCenterMaster-xyOffsetSlave
        imageSelectionSlave = imageSelectionMaster-xyOffsetSlave
      end
      'geo link' : begin
        if ~imageSlave.hasMeta('map info') then return
        imageCenterSlave = slaveSourceContent.convertGeoToPixel(imageCenterMaster)
        imageSelectionSlave = slaveSourceContent.convertGeoToPixel(imageSelectionMaster)
      end
    endcase
    
    slaveSourceContent.setImageSelection, imageSelectionSlave, /SynchronizeLinkedSpeclibs
    
    if masterSourceContent.getImageZoomLinked() then begin
      if slaveSourceContent.getImageZoomLinked() then begin
        slaveSourceContent.setImageCenter, imageCenterSlave
        if self.imageLinkType eq 'geo link' then begin
          mapInfoSlave = imageSlave.getMeta('map info')
          pixelSizeSlave = mean([mapInfoSlave.sizeX, mapInfoSlave.sizeY])
          zoomSlave = zoomMaster*pixelSizeSlave/pixelSizeMaster
          slaveSourceContent.setImageZoom, zoomSlave
        endif
        if self.imageLinkType eq 'pixel link' then begin
          zoomSlave = zoomMaster
          slaveSourceContent.setImageZoom, zoomSlave
        endif
      endif
    endif else begin
      slaveSourceContent.setImageCenter, imageSelectionSlave
    endelse
    slaveSourceContent.controller.showPermanentMessage, slaveSourceContent.controller.getFormattedImageCursorValue()
    slaveSourceContent.updateView
  endforeach
  
end

pro hubTBFrameGridManager::handleEvent, objEvent

  self.hubTBFrame::handleEvent, objEvent
  selectedFrame = self.getSelectedFrame()
  event = objEvent.event
  
  if isa(selectedFrame) then begin 
    case objEvent.name of
      'insertLeft' : newFrame = self.addFrameWithContentRelativeToTargetFrame(/Left, selectedFrame, /ShowTitleBar, /ShowDetach, /ShowClose)
      'insertRight' : newFrame = self.addFrameWithContentRelativeToTargetFrame(/Right, selectedFrame, /ShowTitleBar, /ShowDetach, /ShowClose)
      'insertAbove' : newFrame = self.addFrameWithContentRelativeToTargetFrame(/Above, selectedFrame, /ShowTitleBar, /ShowDetach, /ShowClose)
      'insertBelow' : newFrame = self.addFrameWithContentRelativeToTargetFrame(/Below, selectedFrame, /ShowTitleBar, /ShowDetach, /ShowClose)
      'closeView' : self.removeManagedFrame, selectedFrame, /RemoveDependencies
      'detachView' : self.detachManagedFrame, selectedFrame
      'alignment' : self.setAlignment, objEvent.parameter, /FillMissing
      'showViewBars' : self.updateView
      'linkImages' : begin
        result = self.dialogLinkImages(event.top)
        if isa(result) then begin
          self.setImageLinkType, result.type
          views = result.views
          for i=0,views.count()-1 do begin
            views[i].setImageLink, result.positions[i], result.zooms[i]
          endfor
        endif
      end
      else : 
    endcase
  endif
  self.updateView
  self.updateContent
end

function hubTBFrameGridManager::dialogLinkImages, groupLeader
  views = self.getContents(/Images)
  if views.isEmpty() then begin
    return, !null
  endif
  currentPositions = bytarr(views.count())
  currentZooms = bytarr(views.count())
  names =  strarr(views.count())
  foreach view, views, i do begin
    currentPositions[i] = view.getImagePositionLinked()
    currentZooms[i] = view.getImageZoomLinked()
    names[i] = view.getTitle()
  endforeach
  currentImageLinkType = self.imageLinkType

  ; get user selection
  hubAMW_program, groupLeader, Title='Link Images'
  imageLinkTypes = ['no link','pixel link','geo link']
  hubAMW_checklist, 'type', Title='', List=['No Link','Pixel Link', 'Geo Link'], Value=where(currentImageLinkType eq imageLinkTypes), Extract=imageLinkTypes
  hubAMW_subframe, /ROW
  hubAMW_checklist, 'positions', Title='Link Position', List=names, Value=where(/NULL, currentPositions), /MultipleSelection, /ALLOWEMPTYSELECTION, /Flag, /Column, /SHOWBUTTONBAR
  hubAMW_checklist, 'zooms', Title='Link Zoom', List=names, Value=where(/NULL, currentZooms), /MultipleSelection, /ALLOWEMPTYSELECTION, /Flag, /Column, /SHOWBUTTONBAR
  result = hubAMW_manage()
  
  if result['accept'] then begin
    result['views'] = views
    result = result.toStruct()
  endif else begin
    result = !null
  endelse
  return, result
end

pro hubTBFrameGridManager::setProperty, _EXTRA=_extra, frameArrangement=frameArrangement
  self.hubTBFrame::setProperty, _EXTRA=_extra
  if isa(frameArrangement) then self.frameArrangement = frameArrangement
end

pro hubTBFrameGridManager::getProperty, _REF_EXTRA=_extra, frameArrangement=frameArrangement, buttonShowMenuBars=buttonShowMenuBars
  self.hubTBFrame::getProperty, _EXTRA=_extra
  if arg_present(frameArrangement) then frameArrangement = self.frameArrangement
  if arg_present(buttonShowMenuBars) then buttonShowMenuBars = self.buttonShowMenuBars
end

pro hubTBFrameGridManager__define

  define = {hubTBFrameGridManager, inherits hubTBFrame,$
    tabFrameManager:obj_new(),$
    baseButton:0l,$
;    buttonInsertLeft:0l,$
;    buttonInsertRight:0l,$
;    buttonInsertAbove:0l,$
;    buttonInsertBelow:0l,$
;    buttonRemove:0l,$
;    buttonAlignment:0l,$
    buttonShowMenuBars:0l,$
    imageLinkType:''}
end