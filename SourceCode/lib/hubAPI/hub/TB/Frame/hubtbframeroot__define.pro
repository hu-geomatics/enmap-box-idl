function hubTBFrameRoot::init, toolbox, Title=title, Column=column, Size=size, Offset=offset, XRegisterName=XRegisterName, Icon=icon, GroupLeader=groupLeader, NoMenu=noMenu
  !null = self.hubTBFrame::init(Title=title, Column=column)
  self.toolbox = toolbox
  self.icon = ptr_new(icon)
  self.setProperty, Size=size, Offset=offset
  self.XRegisterName = XRegisterName
  self.groupLeader = ptr_new(groupLeader)
  self.noMenu = keyword_set(noMenu)
  return, 1b
end

pro hubTBFrameRoot::createFrameBase
  if keyword_set(self.noMenu) then begin
    self.baseFrame = widget_base(FRAME=self.frame, TITLE='', /ROW, XPAD=0, YPAD=0, SPACE=0, GROUP_LEADER=*self.groupLeader, $
      UVALUE=hubObjCallbackEvent(self, 'topLevelBase'), $
      /TLB_KILL_REQUEST_EVENTS, TLB_SIZE_EVENTS=1, TLB_MOVE_EVENTS=0, BITMAP=*self.icon, MASK=0)
  endif else begin
    self.baseFrame = widget_base(FRAME=self.frame, TITLE='', /ROW, XPAD=0, YPAD=0, SPACE=0, GROUP_LEADER=*self.groupLeader, $
      UVALUE=hubObjCallbackEvent(self, 'topLevelBase'), $
      /TLB_KILL_REQUEST_EVENTS, TLB_SIZE_EVENTS=1, TLB_MOVE_EVENTS=0, BITMAP=*self.icon, MASK=0, MBAR=baseMenu)
    self.baseMenu = baseMenu
  endelse
  self.setTitle
  widget_control, self.baseFrame, /REALIZE
  self.setMarginSize
  xmanager, self.XRegisterName, self.baseFrame, EVENT_HANDLER='hubObjCallback_event', /NO_BLOCK
end

function hubTBFrameRoot::getTopLevelBase
  return, self.baseFrame
end

function hubTBFrameRoot::getMenuBase
  return, self.baseMenu
end

pro hubTBFrameRoot::updateView, _EXTRA=_extra
  widget_control, self.baseFrame, UPDATE=0
  self.hubTBFrame::updateView, _EXTRA=_extra
  widget_control, self.baseFrame, UPDATE=1
end

pro hubTBFrameRoot::updateSize
  widget_control, self.baseFrame, XSIZE=self.size[0], YSIZE=self.size[1], /CLEAR_EVENTS ; /CLEAR_EVENTS needed under MacOS
end

pro hubTBFrameRoot::updateOffset
  widget_control, self.baseFrame, TLB_SET_XOFFSET=self.offset[0], TLB_SET_YOFFSET=self.offset[1]
end

pro hubTBFrameRoot::setTitle, title
  if ~isa(title) then begin
    title = self.getTitle()
  endif
  widget_control, self.getTopLevelBase(), TLB_SET_TITLE=title
end

function hubTBFrameRoot::getTitle
  return, self.title
end

function hubTBFrameRoot::getIcon
  return, *self.icon
end

pro hubTBFrameRoot::setMarginSize
  case !VERSION.OS_FAMILY of
    'Windows' : marginSize = [0,0]
    'unix' : begin
      size = [100,100]
      if self.noMenu then begin
        tlb = widget_base(SCR_XSIZE=size[0], SCR_YSIZE=size[1])
      endif else begin
        tlb = widget_base(SCR_XSIZE=size[0], SCR_YSIZE=size[1], MBAR=mbar)
        !null = widget_button(mbar, VALUE='dummy')
      endelse
      widget_control, /REALIZE, tlb, MAP=0
      geo = widget_info(/GEOMETRY, tlb)
      widgetInfoSize = [geo.scr_xsize, geo.scr_ysize]
      marginSize = widgetInfoSize-size
      widget_control, /DESTROY, tlb
    end      
  endcase
  self.marginSize = marginSize
end

pro hubTBFrameRoot::handleEvent, objEvent
  event = objEvent.event
  case typename(event) of
    'WIDGET_BASE' : begin
      self.size = [event.x, event.y]
      self.size[1] -= self.marginSize[1]
      self.toolbox.updateView, /SizeOnly
      self.toolbox.updateContent
      self.toolbox.updateContent ; second update needer for detached empty frames, don't no why it is needed, but now it works 
    end
;    'WIDGET_TLB_MOVE' : begin
;      self.toolbox.updateView, /OffsetOnly
;    end
    'WIDGET_KILL_REQUEST' : begin
      self.toolbox.finish
    end
    'WIDGET_BUTTON' : begin
      @huberrorcatch
      eventHandler = widget_info(event.id, /UNAME)
      call_procedure, eventHandler, event
    end
  endcase
end

pro hubTBFrameRoot::finish
  self.hubTBFrame::finish
end

pro hubTBFrameRoot::cleanup
  widget_control, /DESTROY, self.baseFrame
end

pro hubTBFrameRoot__define

  define = {hubTBFrameRoot, inherits hubTBFrame, $
    XRegisterName : '',$
    icon : ptr_new(),$
    baseMenu : 0l,$
    groupLeader : ptr_new(),$
    noMenu:0b,$
    marginSize:[0,0]}
  
end