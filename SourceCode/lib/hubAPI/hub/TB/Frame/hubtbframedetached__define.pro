function hubTBFrameDetached::init, frame, GroupLeader=groupLeader
  if ~isa(frame, 'hubTBFrame') then begin
    message, 'Wrong argument type.' 
  endif
  self.attachedFrame = frame
  frame.detachedFrame = self
  self.toolbox = frame.toolbox

  ; a detached frame is a pseudo-toolbox of its own, consisting of a single frame, showing the content oft the original frame
  !null = self.hubTBToolbox::init(Title=frame.getTitle(), XRegisterName='hubTBFrameDetached', Icon=frame.getIcon(), Size=frame.size, GroupLeader=groupLeader, /NoMenu)
  
  self.start
  self.setTitle
  if frame.hasContent() then begin
    self.showContentInFrame, frame.getContent(), self.getFrameRoot()
  endif
  self.updateContent
  self.show
  
  return, 1b
end

pro hubTBFrameDetached::finish
  ; if a detached frame is closed, it is attached back to its original frame
  self.toolbox.attachFrame, self
  self.controller.finish
end

pro hubTBFrameDetached::setTitle
  self.controller.setTitle, self.attachedFrame.getTitle()
end

function hubTBFrameDetached::getContent
  return, (self.getFrameRoot()).getContent()
end

function hubTBFrameDetached::getFrameManager
  return, self.frame.parent
end

pro hubTBFrameDetached::getProperty, _REF_EXTRA=_extra, attachedFrame=attachedFrame, frame=frame
  self.hubTBToolbox::getProperty, _EXTRA=_extra
  if arg_present(attachedFrame) then attachedFrame = self.attachedFrame
  if arg_present(frame) then stop
end

pro hubTBFrameDetached__define

  define = {hubTBFrameDetached, inherits hubTBToolbox,$
    attachedFrame : obj_new(), $  ; hubTBFrame
    toolbox : obj_new()}
  
end