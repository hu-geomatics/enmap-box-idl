function hubTBFrameLayout::init, frame, childFrames, Column=column
  self.frame = frame
  self.childFrames = childFrames
  self.column = keyword_set(column)
  self.alignment = list(0)
  return, 1b
end

function hubTBFrameLayout::getUnixFixMargin
  return, 2
end

pro hubTBFrameLayout::layout
  index = ~self.column
  if self.childFrames.count() eq 0 then return
  
  ; calculate number of segments (segments having all frames detached, do not count)
  numberOfEmptySegments = 0
  firstFrameInSegment = 0
  allSegmentFramesDetached = bytarr(self.childFrames.count())
  foreach numberOfFramesInSegment, self.alignment, i do begin
    frames = (self.childFrames)[firstFrameInSegment:firstFrameInSegment+numberOfFramesInSegment-1]
    allDetached = 1b
    foreach frame, frames do begin
      allDetached and= frame.isDetached
    endforeach
    allSegmentFramesDetached[i] = allDetached
    firstFrameInSegment += numberOfFramesInSegment
  endforeach
  numberOfEmptySegments = total(allSegmentFramesDetached)
  
  numberOfSegments = self.alignment.count() - numberOfEmptySegments
  segmentSize = self.frame.getContentSize()
  if numberOfSegments ne 0 then begin
    segmentSize[index] = floor(segmentSize[index]/numberOfSegments)
    ; todo calculate size for last segment (in analogy to the frames inside ::layoutSegment)
  endif
  segmentOffset = [0,0] 
  firstFrameInSegment = 0

  foreach numberOfFramesInSegment, self.alignment, i do begin
    frames = (self.childFrames)[firstFrameInSegment:firstFrameInSegment+numberOfFramesInSegment-1]

    case !VERSION.OS_FAMILY of
      'unix' : begin 
        if i eq numberOfSegments-1 then begin
          segmentSize[index] -= self.getUnixFixMargin()
   ;       print,self.frame.getTitle()
        endif
      end
      else : ;
    endcase
    
    self.layoutSegment, frames, segmentOffset, segmentSize
    firstFrameInSegment += numberOfFramesInSegment
    if ~allSegmentFramesDetached[i] then begin
      segmentOffset[index] += segmentSize[index]
    endif
  endforeach
end

pro hubTBFrameLayout::layoutSegment, frames, segmentOffset, segmentSize

  index = self.column
  
  ; calculate and set size of frames and update
  numberOfFrames = frames.count()
  if numberOfFrames eq 0 then return
  overallSize = segmentSize
  
  ; calculate size for relativ scaling (nessary for all frames that do not have a fixed height or width)
  unfixedSize = overallSize
  numberOfUnfixedFrames = 0
  foreach frame, frames do begin
  
    ; for fixed frames take their fixed size
    if (frame.isFixedSize)[index] then begin
      fixedSize = frame.getFixedFrameSize(/Border)
    endif else begin
      fixedSize = [1,1]
    endelse
    
    ; minimized frames overwrites the fixed size to 1 (for the content) plus the size of the buttons and the border
    if (frame.isMinimized)[index] then begin
      fixedSize[index] = (frame.getMinimizedFrameSize(/Border))[index]
    endif
    
    ; detached frames overwrites the fixed size to 1 (for the content) plus the border
    if frame.isDetached then begin
      fixedSize[index] = (frame.getDetachedFrameSize(/Border))[index]
    endif
    
    ; if frame is fixed or minimized update the overall unfixed size
    if (frame.isFixedSize)[index] || (frame.isMinimized)[index] || frame.isDetached  then begin
      unfixedSize[index] -= fixedSize[index]
    endif else begin
      numberOfUnfixedFrames++
      lastUnfixedFrame = frame
    endelse

    if ~frame.isDetached  then begin
      lastFrame = frame
    endif
    
  endforeach
 
  ; see if all frames are detached
  allFramesDetached = 1b
  foreach frame, frames do begin
    allFramesDetached and= frame.isDetached
  endforeach
  
  if ~isa(lastUnfixedFrame) && ~allFramesDetached then begin
    message, 'at least one frame must be unfixed.'
  endif

  unfixedFrameSize = overallSize
;  if numberOfUnfixedFrames ne 0 then unfixedFrameSize[index] = unfixedSize[index]/numberOfUnfixedFrames
  if numberOfUnfixedFrames ne 0 then unfixedFrameSize[index] = round(unfixedSize[index]/numberOfUnfixedFrames)
  
  ; set size and position of frames
  frameOffset = segmentOffset
  for i=0,numberOfFrames-1 do begin
    frame = frames[i]
    
    ; set unfixed frame size as default
    frameSize = unfixedFrameSize
    
    ; fixed frames overwrites their frame size by its fixed size
    if (frame.isFixedSize)[index] then begin
      frameSize[index] = (frame.getFixedFrameSize(/Border))[index]
    endif
    
    ; minimized frames overwrites their frame size by its minimized size
    if (frame.isMinimized)[index] then begin
      frameSize[index] = (frame.getMinimizedFrameSize(/Border))[index]
    endif
    
    ; detached frames overwrites their frame size by its detached size
    if frame.isDetached then begin
      frameSize[index] = (frame.getDetachedFrameSize(/Border))[index]
    endif
    
    ; special handling for last nonfixed frame (correct rounding effect)
    if ~allFramesDetached && (frame eq lastUnfixedFrame) then begin
      roundingError = unfixedSize[index] - unfixedFrameSize[index]*numberOfUnfixedFrames
      frameSize[index] += roundingError
    endif
    
    case !VERSION.OS_FAMILY of
      'unix' : begin
        ; special handling for last frame
        if ~allFramesDetached && (frame eq lastFrame) then begin
          frameSize[index] -= self.getUnixFixMargin()
;          print, lastFrame.getTitle()
        endif
      end
      else : ;
    endcase
    
    ; special handling if all frames are detached
    if allFramesDetached then begin
      frameSize = [1,1]
    endif
;print,'set child size: ', frame.getTitle(), frameSize   
    frame.size = frameSize
    frame.offset = frameOffset
    frameOffset[index] += frameSize[index]
  endfor
  
end

function hubTBFrameLayout::findFrameSegment, frame
  frameIndex = self.frame.findFrame(frame)
  totalNumberOfFrames = 0
  foreach numberOfFrames, self.alignment, index do begin
    totalNumberOfFrames += numberOfFrames
    if totalNumberOfFrames ge frameIndex+1 then begin
      return, index
    endif
  endforeach
  message, 'Frame does not exist.'
end

pro hubTBFrameLayout::getProperty, alignment=alignment, column=column
  if arg_present(alignment) then alignment = self.alignment
  if arg_present(column) then column = self.column
end

pro hubTBFrameLayout__define

  define = {hubTBFrameLayout, inherits IDL_Object, $ 
    frame : obj_new(),$
    childFrames : list(),$
    column : 0b, $
    alignment : list() $
  }
  
end