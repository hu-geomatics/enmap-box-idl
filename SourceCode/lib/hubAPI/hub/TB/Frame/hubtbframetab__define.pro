function hubTBFrameTab::init, parentFrame, Title=title, ShowTitleBar=showTitleBar, ShowDetach=showDetach, ShowClose=showClose
  !null = self.hubTBFrame::init(parentFrame, Title=title, ShowTitleBar=showTitleBar, ShowDetach=showDetach, ShowClose=showClose, /UserAligned)
  return, 1b
end

pro hubTBFrameTab::finish
  tabBase = widget_info(/PARENT, self.baseFrame)
  self.hubTBFrame::finish
  widget_control, tabBase, /DESTROY
end

pro hubTBFrameTab__define
  define = {hubTBFrameTab, inherits hubTBFrame}
end