function hubTBFrameTabManager::init, parentFrame, _EXTRA=_extra
  !null = self.hubTBFrame::init(parentFrame, _EXTRA=_extra, /Frame)
  self.layoutManager = hubTBFrameTabLayout(self, self.childFrames)
  self.toolbox.setTabFrameManager, self
  return, 1b
end

pro hubTBFrameTabManager::create
  self.hubTBFrame::create
  self.tab = widget_tab(self.baseContent, LOCATION=1)
end

pro hubTBFrameTabManager::createChildFrames
end

pro hubTBFrameTabManager::updateSize
  widget_control, self.tab, SCR_XSIZE=self.size[0]>1, SCR_YSIZE=self.size[1]>1
end

function hubTBFrameTabManager::addContent, content, ShowTitleBar=showTitleBar, ShowDetach=showDetach, ShowClose=showClose
  self.baseContent = widget_base(self.tab, Title=content.getTitle())
  widget_control, self.tab, SET_TAB_CURRENT=n_elements(widget_info(/ALL_CHILDREN, self.tab))-1
  frame = hubTBFrameTab(self, Title=content.getTitle(), ShowTitleBar=showTitleBar, ShowDetach=showDetach, ShowClose=showClose)
  frame.create
  self.toolbox.showContentInFrame, content, frame
;  self.toolbox.updateView
  self.updateView
  return, frame
end

pro hubTBFrameTabManager::removeContent, content
  foreach frame,content.controller.getFrames() do begin
   ; self.removeFrame, frame
     self.toolbox.removeFrame, frame
  endforeach
  widget_control, self.tab, SET_TAB_CURRENT=0
end

pro hubTBFrameTabManager::getProperty, _REF_EXTRA=_extra, tab=tab
  self.hubTBFrame::getProperty, _EXTRA=_extra
  if arg_present(tab) then tab = self.tab

end

pro hubTBFrameTabManager__define

  define = {hubTBFrameTabManager, inherits hubTBFrame,$
    tab:0l}
  
end