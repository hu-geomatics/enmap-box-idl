function hubTBFrameTabLayout::init, frame, childFrames
  self.frame = frame
  self.childFrames = childFrames
  return, 1b
end

pro hubTBFrameTabLayout::layout
  geometry = widget_info(/GEOMETRY, self.frame.tab)
  size = [geometry.xsize, geometry.ysize]
 
  foreach frame, self.childFrames do begin
    if frame.isDetached then begin
      frame.size = [1,1]
    endif else begin
      frame.size = size
    endelse
    frame.offset = [0,0]
  endforeach
end

pro hubTBFrameTabLayout__define
  define = {hubTBFrameTabLayout, inherits IDL_Object, $ 
    frame : obj_new(),$
    childFrames : list() $
  }
  
end