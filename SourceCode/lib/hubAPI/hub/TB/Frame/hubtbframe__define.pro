function hubTBFrame::init, parentFrame, Index=index, Title=title, Column=column, FRAME=frame,$
  ShowButtonLeft=showButtonLeft, ShowButtonRight=showButtonRight, ShowButtonUp=showButtonUp, ShowButtonDown=showButtonDown, ShowTitleBar=showTitleBar, $
  ShowDetach=showDetach, ShowClose=showClose,$
  IsFixedSize=isFixedSize, FixedSize=fixedSize,$
  UserAligned=userAligned

  self.childFrames = list()
  self.layoutManager = hubTBFrameLayout(self, self.childFrames, Column=column)
  if isa(parentFrame, 'hubTBFrame') then begin
    self.setParentFrame, parentFrame, index, UserAligned=userAligned
  endif
  if isa(parentFrame, 'hubTBToolbox') then begin
    toolbox = parentFrame
    self.setParentFrame, toolbox.getFrameRoot(), index, UserAligned=userAligned
  endif
  if total(list(showButtonLeft, showButtonRight, showButtonUp, showButtonDown) eq 1) gt 1 then begin
    message, 'Set at most one showButtonXYZ keyword.' 
  endif

  self.frame = keyword_set(frame)
  self.title = isa(title) ? title : 'no title'
  self.showButtonLeft = keyword_set(showButtonLeft)
  self.showButtonRight = keyword_set(showButtonRight)
  self.showButtonUp = keyword_set(showButtonUp)
  self.showButtonDown = keyword_set(showButtonDown)
  self.showTitleBar = keyword_set(showTitleBar)
  self.showDetach = keyword_set(showDetach)
  self.showClose = keyword_set(showClose)
  self.setProperty, IsFixedSize=isFixedSize, FixedSize=fixedSize
  return, 1b
end

pro hubTBFrame::create
  
  ; create Min/Max buttons and base for content
  self.createFrameBase
  buttonSize = 15
  if self.showButtonLeft then self.buttonLeft = widget_button(self.baseFrame, VALUE=' ', UVALUE=hubObjCallbackEvent(self, 'minMaxWidth'), SCR_XSIZE=buttonSize, /DYNAMIC_RESIZE)
  self.baseMiddle = widget_base(self.baseFrame, /COLUMN, XPAD=0, YPAD=0, SPACE=0)
  if self.showButtonUp then self.buttonUp = widget_button(self.baseMiddle, VALUE=' ', UVALUE=hubObjCallbackEvent(self, 'minMaxHeight'), SCR_YSIZE=buttonSize, /DYNAMIC_RESIZE)
  if self.showTitleBar then begin
    self.baseTitleBar = widget_base(self.baseMiddle, /COLUMN, XPAD=0, YPAD=0, SPACE=0)
    subbase = widget_base(self.baseTitleBar, /ROW, XPAD=0, YPAD=0, SPACE=0)
    self.createTitleBar, subbase
    self.baseTitleBarFiller = widget_base(subbase)
    if self.showDetach then begin
      bitmap = hubTBAuxiliaryGetIconBitmaps(/Detach)
      self.buttonDetach = widget_button(subbase, VALUE=bitmap, UVALUE=hubObjCallbackEvent(self, 'detach'), /BITMAP, /MASK)
    endif
    if self.showClose then begin
      bitmap = hubTBAuxiliaryGetIconBitmaps(/Close)
      self.buttonClose = widget_button(subbase, VALUE=bitmap, UVALUE=hubObjCallbackEvent(self, 'close'), /BITMAP, /MASK)
    endif
  endif
  self.baseContent = widget_base(self.baseMiddle)
  if self.showButtonDown then self.buttonDown = widget_button(self.baseMiddle, VALUE=' ', UVALUE=hubObjCallbackEvent(self, 'minMaxHeight'), SCR_YSIZE=buttonSize, /DYNAMIC_RESIZE)
  if self.showButtonRight then self.buttonRight = widget_button(self.baseFrame, VALUE=' ', UVALUE=hubObjCallbackEvent(self, 'minMaxWidth'), SCR_XSIZE=buttonSize, /DYNAMIC_RESIZE)
  
  self.createChildFrames
end

pro hubTBFrame::createFrameBase
  self.baseFrame = widget_base(self.parent.baseContent, FRAME=self.frame, /ROW, XPAD=0, YPAD=0, SPACE=0)
end

pro hubTBFrame::createChildFrames
  foreach childFrame, self.childFrames do begin
    childFrame.create
  endforeach
end

pro hubTBFrame::createTitleBar, base
  self.labelTitle = widget_label(base, VALUE=self.title)
end

pro hubTBFrame::finish
  foreach frame, self.childFrames do begin
    frame.finish
  endforeach
  widget_control, self.baseFrame, /DESTROY
end

pro hubTBFrame::updateView, SizeOnly=sizeOnly, OffsetOnly=offsetOnly

  ; update frame size and position
  if ~keyword_set(sizeOnly) then self.updateOffset
  if ~keyword_set(offsetOnly) then self.updateSize

  if self.isDetached then return 

  ; resize frame buttons
  sizeFrame = self.getFrameSize(Border=0)
  if self.showButtonLeft then widget_control, self.buttonLeft, SCR_YSIZE=sizeFrame[1]>1
  if self.showButtonRight then widget_control, self.buttonRight, SCR_YSIZE=sizeFrame[1]>1
  if self.showButtonUp then widget_control, self.buttonUp, SCR_XSIZE=sizeFrame[0]>1
  if self.showButtonDown then widget_control, self.buttonDown, SCR_XSIZE=sizeFrame[0]>1

  ; resize frame title bar
  sizeContent = self.getContentSize()
  widget_control, self.baseMiddle, SCR_XSIZE=sizeContent[0]>1
  if self.showTitleBar then begin
    widget_control, self.baseTitleBar, SCR_XSIZE=sizeContent[0]>1
    self.updateTitleBar
  endif
  if ~( (self.isMinimized)[0] || (self.isMinimized)[1] || self.isDetached) then begin 
    if widget_info(/VALID_ID, self.baseContent) then begin
      widget_control, self.baseContent, SCR_XSIZE=sizeContent[0]>1, SCR_YSIZE=sizeContent[1]>1
    endif
  endif

;print, self.getTitle()
;print, 'size       ',self.size
;help, widget_info(/GEOMETRY, self.baseFrame)
;help, widget_info(/GEOMETRY, self.baseMiddle)
;help, widget_info(/GEOMETRY, self.baseContent)

  ; update content
;  if obj_valid(self.subscribedView) then begin
;    self.subscribedView.sendMessageUpdate
;  endif

  ; layout child frames (calculate size and position for row or column layout)
  self.layoutManager.layout
  
  ; update all child frames
  foreach childFrame, self.childFrames do begin
    childFrame.updateView
  endforeach
end

pro hubTBFrame::updateSize
; linux debug
;widget_control, self.baseFrame, UPDATE=0
;widget_control, self.baseFrame, XOFFSET=0, YOFFSET=0

  widget_control, self.baseFrame, SCR_XSIZE=self.size[0]>1, SCR_YSIZE=self.size[1]>1

; linux debug
;widget_control, self.baseFrame, XOFFSET=self.offset[0], YOFFSET=self.offset[1]
;widget_control, self.baseFrame, UPDATE=1

end

pro hubTBFrame::updateOffset
  widget_control, self.baseFrame, XOFFSET=self.offset[0], YOFFSET=self.offset[1]
end

pro hubTBFrame::updateTitleBar

  sizeContent = self.getContentSize()
  labelWidth = (widget_info(/GEOMETRY, self.labelTitle)).scr_xsize
  buttonWidth = 0
  if self.showClose then buttonWidth += (widget_info(/GEOMETRY, self.buttonClose)).scr_xsize
  if self.showDetach then buttonWidth += (widget_info(/GEOMETRY, self.buttonDetach)).scr_xsize
  fillerSize = sizeContent[0]-buttonWidth-labelWidth
  widget_control, self.baseTitleBarFiller, SCR_XSIZE=fillerSize>1
end

pro hubTBFrame::updateContent
  self.updateContentSize
  self.updateContentView
end

pro hubTBFrame::updateContentSize

  if self.hasContent() then begin
    content = self.getContent()
    content.updateSize
  endif

  foreach childFrame, self.childFrames do begin
    childFrame.updateContentSize
  endforeach
end

pro hubTBFrame::updateContentView

  if self.hasContent() then begin
    content = self.getContent()
    content.updateView
  endif

  foreach childFrame, self.childFrames do begin
    childFrame.updateContentView
  endforeach
end

pro hubTBFrame::addFrame, frame, index, UserAligned=userAligned
  if ~isa(frame, 'hubTBFrame') then message, 'Wrong argument.'
  self.childFrames.add, frame, index
  if ~keyword_set(userAligned) then begin
    alignment = (self.layoutManager.alignment)
    alignment[-1] += 1
  endif
end

pro hubTBFrame::removeFrame, frame

  if ~isa(frame, 'hubTBFrame') then message, 'Wrong argument.'

  ; update alignment
  if isa(self.layoutManager, 'hubTBFrameLayout') then begin
    segmentIndex = self.layoutManager.findFrameSegment(frame)
    alignment = self.layoutManager.alignment
    if alignment[segmentIndex] gt 1 then begin
      alignment[segmentIndex] = alignment[segmentIndex]-1 ; decrease segment size by one
    endif else begin
      alignment.remove, segmentIndex ; remove segment if it is empty
      if alignment.isEmpty() then begin
        alignment.add, 0
      endif
    endelse
  end

  ; remove frame
  frameIndex = self.findFrame(frame)
  self.childFrames.remove, frameIndex
  frame.finish
end

pro hubTBFrame::remove
  self.parent.removeFrame, self  
end

function hubTBFrame::findFrame, frame
  if ~self.childFrames.hubIsElement(frame, Index=index) then begin
    message, 'Frame does not exist.'
  endif
  return, index
end

pro hubTBFrame::setParentFrame, parentFrame, index, UserAligned=userAligned
  self.parent = parentFrame
  self.toolbox = parentFrame.toolbox
  parentFrame.addFrame, self, index, UserAligned=userAligned
end

pro hubTBFrame::subscribeView, view
  if ~isa(view, 'hubTBContentMVCView') then message, 'Wrong argument.'
  if obj_valid(self.subscribedView) then message, 'Another view is already subscribed.'
  self.subscribedView = view
end

pro hubTBFrame::unsubscribeView
  self.subscribedView = obj_new()
end

function hubTBFrame::getContentSize
;  return, self.getFrameSize(/Border)-self.getButtonSize()-[0,1]*self.getTitleSize()
  return, self.getFrameSize()-self.getButtonSize()-[0,1]*self.getTitleSize()
end

function hubTBFrame::getFrameSize, Border=border
  return, self.size-(keyword_set(border) ? 0 : self.getBorderSize())
end

function hubTBFrame::getBorderSize
  return, 2*self.frame
end

function hubTBFrame::getFixedFrameSize, Border=border
  return, self.fixedSize-(keyword_set(border) ? 0 : self.getBorderSize())
end

function hubTBFrame::getMinimizedFrameSize, Border=border
  return, 1+self.getButtonSize()+(keyword_set(border) ? 0 : self.getBorderSize())
end

function hubTBFrame::getDetachedFrameSize, Border=border
  return, [1,1]+(keyword_set(border) ? 0 : self.getBorderSize())
end

function hubTBFrame::getButtonSize
  xsize = 0
  if self.showButtonLeft then xsize += (widget_info(/GEOMETRY, self.buttonLeft)).scr_xsize
  if self.showButtonRight then xsize += (widget_info(/GEOMETRY, self.buttonRight)).scr_xsize
  ysize = 0
  if self.showButtonUp then ysize += (widget_info(/GEOMETRY, self.buttonUp)).scr_ysize
  if self.showButtonDown then ysize += (widget_info(/GEOMETRY, self.buttonDown)).scr_ysize
  return, [xsize, ysize]
end

function hubTBFrame::getTitleSize
  if self.showTitleBar then begin
    geometry = widget_info(/GEOMETRY, self.baseTitleBar)
    size = [geometry.scr_xsize, geometry.scr_ysize]
  endif else begin
    size = [0,0]
  endelse
  return, size
end

function hubTBFrame::getTitle
  return, self.title
end

function hubTBFrame::getIcon
  return, self.parent.getIcon()
end

function hubTBFrame::hasContent
  return, obj_valid(self.subscribedView)
end

function hubTBFrame::getContent
  return, self.subscribedView.controller.mvc
end

function hubTBFrame::getFrameManager
  if self.isDetached then begin
    managedFrameManager = self.frame.getFrameManager()
  endif else begin
    if isa(self, 'HubTBFrameRoot') then begin
      detachedFrame = self.toolbox
      attachedFrame = detachedFrame.attachedFrame
      managedFrameManager = attachedFrame.parent
    endif else begin
      managedFrameManager = self.parent
    endelse
  endelse
  return, managedFrameManager
end

pro hubTBFrame::handleEvent, objEvent
  
  event = objEvent.event
  case objEvent.name of
    'minMaxWidth' : if self.isMinimized[0] then self.maximizeWidth else self.minimizeWidth
    'minMaxHeight' : if self.isMinimized[1] then self.maximizeHeight else self.minimizeHeight
    'detach' : begin
      if self.hasContent() then begin
        self.toolbox.detachFrame, self
      endif else begin
        return
        message, 'Can not detach frame with no content.'
      endelse
    end
    'close' : begin
      self.toolbox.removeFrame, self
      self.toolbox.updateView, /SizeOnly
      self.toolbox.updateContent
    end
    else : ; do nothing
  endcase
end

pro hubTBFrame::minimizeWidth
  self.isMinimized[0] = 1
  self.toolbox.updateView, /SizeOnly
  self.toolbox.updateContent
end

pro hubTBFrame::maximizeWidth
  self.isMinimized[0] = 0
  self.toolbox.updateView, /SizeOnly
  self.toolbox.updateContent 
end
                
pro hubTBFrame::minimizeHeight
  self.isMinimized[1] = 1
  self.toolbox.updateView, /SizeOnly
  self.toolbox.updateContent
end

pro hubTBFrame::maximizeHeight
  self.isMinimized[1] = 0
  self.toolbox.updateView, /SizeOnly
  self.toolbox.updateContent
end

pro hubTBFrame::setProperty, Size=size, Offset=offset, fixedSize=fixedSize, isFixedSize=isFixedSize, isDetached=isDetached, detachedFrame=detachedFrame
  if isa(size) then self.size = size
  if isa(offset) then self.offset = offset
  if isa(fixedSize) then self.fixedSize = fixedSize
  if isa(isFixedSize) then self.isFixedSize = isFixedSize
  if isa(isDetached) then self.isDetached = isDetached
  if isa(detachedFrame) then self.detachedFrame = detachedFrame
end

pro hubTBFrame::getProperty, BaseContent=baseContent, BaseFrame=baseFrame, Size=size, Offset=offset, fixedSize=fixedSize, isFixedSize=isFixedSize, isMinimized=isMinimized,$
  title=title, subscribedView=subscribedView, isDetached=isDetached, detachedFrame=detachedFrame, parent=parent, toolbox=toolbox
  
  if arg_present(baseContent) then baseContent = self.baseContent
  if arg_present(baseFrame) then baseFrame = self.baseFrame
  if arg_present(size) then size = self.size
  if arg_present(offset) then offset = self.offset
  if arg_present(fixedSize) then fixedSize = self.fixedSize
  if arg_present(isFixedSize) then isFixedSize = self.isFixedSize
  if arg_present(isMinimized) then isMinimized = self.isMinimized
  if arg_present(title) then title = self.title
  if arg_present(subscribedView) then subscribedView = self.subscribedView
  if arg_present(isDetached) then isDetached = self.isDetached
  if arg_present(detachedFrame) then detachedFrame = self.detachedFrame
  if arg_present(parent) then parent = self.parent
  if arg_present(toolbox) then toolbox = self.toolbox
  
end

pro hubTBFrame__define

  define = {hubTBFrame, inherits IDL_Object, $
    toolbox : obj_new(), $ ; hubTBToolbox
    subscribedView : obj_new(), $ ; hubTBContentMVCView
    parent : obj_new(), $ ; hubTBFrame
    childFrames : list(), $ ; list of hubTBFrame
    layoutManager : obj_new(), $ ; hubTBFrameLayout
    size : [0,0], $
    offset : [0,0], $
    frame : 0b, $
    title : '', $
    fixedSize : [0,0], $
    isFixedSize : [0b,0b], $
    isMinimized : [0b,0b], $
    baseFrame : 0l,$
    baseMiddle : 0l,$
    baseTitleBar : 0l,$
    baseContent : 0l,$
    baseTitleBarFiller : 0l,$
    buttonLeft : 0l,$
    buttonUp : 0l,$
    buttonDown : 0l,$
    buttonRight : 0l,$
    showButtonLeft : 0b,$
    showButtonRight : 0b,$
    showButtonUp : 0b,$
    showButtonDown : 0b,$
    showTitleBar : 0b, $
    showDetach : 0b, $
    showClose : 0b, $
    draggable : 0b, $
    labelTitle : 0l, $
    buttonDetach : 0l, $
    buttonClose : 0l, $
    isDetached : 0b, $
    detachedFrame : obj_new() $
  }
  
end