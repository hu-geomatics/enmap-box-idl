function hubTBAuxiliaryGetIconBitmaps, File=file, Multi=multi, Single=single, Classification=classification, Mask=mask, Info=info, Speclib=speclib, Hand=hand, Close=close, Detach=detach, $
  AddLeft=addLeft, AddRight=addRight, AddTop=addTop, AddDown=addDown, Align=align, LinkImages=linkImages, $
  Rectangle=rectangle, size, color
  
  if keyword_set(rectangle) then begin
    bitmap = make_array(/Byte,Value=255B,16,16,3)
    bitmap[0+size:15-size,0+size:15-size,*] = 0B
    size += 1
    for i=0,2 do bitmap[0+size:15-size,0+size:15-size,i] = color[i]
  endif else begin
    fileDirectory = filepath('', ROOT_DIR=hub_getDirname(),Subdirectory=['resource','TBBitmaps'])
    if keyword_set(file) then fileBasename = 'file.bmp'
    if keyword_set(multi) then fileBasename = 'multiband.bmp'
    if keyword_set(single) then fileBasename = 'singleband.bmp'
    if keyword_set(classification) then fileBasename = 'classification.bmp'
    if keyword_set(mask) then fileBasename = 'mask.bmp'
    if keyword_set(info) then fileBasename = 'information.bmp'
    if keyword_set(speclib) then fileBasename = 'library.bmp'
    if keyword_set(hand) then fileBasename = 'hand.bmp'
    if keyword_set(close) then fileBasename = 'close.bmp'
    if keyword_set(detach) then fileBasename = 'detach.bmp'
    if keyword_set(addLeft) then fileBasename = 'addleft.bmp'
    if keyword_set(addRight) then fileBasename = 'addright.bmp'
    if keyword_set(addTop) then fileBasename = 'addtop.bmp'
    if keyword_set(addDown) then fileBasename = 'adddown.bmp'
    if keyword_set(align) then fileBasename = 'align.bmp'
    if keyword_set(linkImages) then fileBasename = 'linkimages.bmp'

    filename = filepath(fileBasename, ROOT_DIR=fileDirectory)
    bitmap = read_bmp(filename)
    if size(bitmap,/N_DIMENSIONS) eq 3 then begin
      bitmap = transpose(bitmap,[1,2,0])
      bitmap = bitmap[*,*,[2,1,0]]
    endif
  endelse
  return, bitmap
  
end