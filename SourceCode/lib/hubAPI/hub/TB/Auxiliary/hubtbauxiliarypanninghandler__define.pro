function hubTBAuxiliaryPanningHandler::init
  self.finish
  return, 1b
end

pro hubTBAuxiliaryPanningHandler::start, currentPosition, Info=info
  self.active = 1b
  self.currentPosition = ptr_new(currentPosition)
  self.info = ptr_new(info)
end

function hubTBAuxiliaryPanningHandler::isActive
  return, self.active
end

function hubTBAuxiliaryPanningHandler::getChange, newPosition, InvertX=invertX, InvertY=invertY
  if ~self.active then message, 'Panning is not initialized.'
  positionChange = *self.currentPosition-newPosition
  if keyword_set(invertX) then begin
    positionChange[0] *= -1    
  endif
  if keyword_set(invertY) then begin
    positionChange[1] *= -1
  endif
  *self.currentPosition = newPosition
  return, positionChange
end

function hubTBAuxiliaryPanningHandler::getInfo
  return, *self.info
end

pro hubTBAuxiliaryPanningHandler::finish
  self.active = 0b
  self.currentPosition = ptr_new(!null)
  self.info = ptr_new(!null)
end

pro hubTBAuxiliaryPanningHandler__define
  define = {hubTBAuxiliaryPanningHandler, inherits IDL_Object,$
    active:0b, $
    currentPosition:ptr_new(),$
    info:ptr_new()}
end