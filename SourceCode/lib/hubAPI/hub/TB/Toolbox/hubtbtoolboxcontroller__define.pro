pro hubTBToolboxController::start
  self.create
  self.updateSize
  self.updateView
end

pro hubTBToolboxController::create
  (self.views[0]).create
end

pro hubTBToolboxController::createMenu, menuFiles, Log=log
  hubGUIMenu = hubGUIMenu()
  log = list()
  foreach menuFile, menuFiles do begin
    log.add, menuFile
    hubGUIMenu.extendMenu, menuFile
  endforeach
  log = log.toArray()
  menuBase = (self.getFrameRoot()).getMenuBase()
  
  uvalue = hubObjCallbackEvent(self.getFrameRoot(), 'menuButton', parameters)
  hubGUIMenu.createMenuBar, menuBase, UVALUE=uvalue
end

pro hubTBToolboxController::updateSize
  (self.views[0]).updateSize
end

pro hubTBToolboxController::updateOffset
  (self.views[0]).updateOffset
end

pro hubTBToolboxController::updateView, _EXTRA=_extra
  (self.views[0]).updateView, _EXTRA=_extra
end

pro hubTBToolboxController::updateContentView
  (self.views[0]).updateContentView
end

pro hubTBToolboxController::updateContentSize
  (self.views[0]).updateContentSize
end

pro hubTBToolboxController::fixDisplay
  case !VERSION.OS_FAMILY of
    'unix' : begin
      tlb = self.getTopLevelBase()
      widget_control, tlb, Map=0
      widget_control, tlb, Map=1
    end
    else : ; 
  endcase
end

pro hubTBToolboxController::setSize, size
  (self.views[0]).setSize, size
end

function hubTBToolboxController::getSize
  return, (self.views[0]).getSize()
end

pro hubTBToolboxController::setOffset, offset
  (self.views[0]).setOffset, offset
end

function hubTBToolboxController::getOffset
  return, (self.views[0]).getOffset()
end

pro hubTBToolboxController::setTitle, title
  frameRoot = self.getFrameRoot()
  frameRoot.setTitle, title
end

function hubTBToolboxController::getFrameRoot
  return, (self.views)[0].frameRoot
end

function hubTBToolboxController::getTopLevelBase
  return, (self.getFrameRoot()).getTopLevelBase()
end

pro hubTBToolboxController::showContentInFrame, content, frame, NoUpdate=noUpdate
  if ~isa(frame, 'hubTBFrame') then begin
    message, 'Wrong argument type.'
  endif
  if ~isa(content, 'hubTBContentMVC') then begin
    message, 'Wrong argument type.'
  endif
  
  ; create new view of the specific type of content
  newView = content.newView()
  
  ; subscribe the view to the content
  content.subscribeView, newView
  
  ; subscribe the view to the frame  
  frame.subscribeView, newView

  ; subscribe the frame to the content
  content.subscribeFrame, frame

  ; set the view base to the base of the frame and create the view widgets inside the frame
  newView.setBase, frame.baseContent
  newView.create
  
  ; update content
  if ~keyword_set(noUpdate) then begin
    frame.updateView, /SizeOnly, /OffsetOnly
    frame.updateContent
  endif

end

pro hubTBToolboxController::show
  widget_control, /SHOW, self.getTopLevelBase()
end

pro hubTBToolboxController::deleteContentInFrame, frame, RemoveDependencies=removeDependencies

  view = frame.subscribedView
  if ~obj_valid(view) then return

  content = view.controller.mvc
  if keyword_set(removeDependencies) then begin
    content.controller.removeDependencies
  endif
  
  ; unsubscribe the view from the content
  content.unsubscribeView, view
  
  ; unsubscribe the view to the frame  
  frame.unsubscribeView

  ; unsubscribe the frame to the content
  content.unsubscribeFrame, frame

  ; delete the view widgets inside the frame
  view.delete  
end

pro hubTBToolboxController::removeFrame, frame, RemoveDependencies=removeDependencies
  self.deleteContentInFrame, frame, RemoveDependencies=removeDependencies
  frame.parent.removeFrame, frame
  if isa(frame, 'HubTBManagedFrame') then begin
    (frame.getGridFrameManager()).ensureDefaultFrame
  endif
end

pro hubTBToolboxController::detachFrame, frame
  if ~isa(frame, 'hubTBFrame') then begin
    message, 'Wrong argument type.'
  endif
  
  ; create detachedFrame (implicitely copies the content)
  detachedFrame = hubTBFrameDetached(frame, GroupLeader=frame.baseContent)
  
  ; delete content from original frame
  self.deleteContentInFrame, frame
    
  ; set detached flag to the original frame
  frame.isDetached = 1b
  frame.parent.updateView
  frame.parent.updateContent
  
  
  ; mac workaround
  if !VERSION.OS_NAME eq 'Mac OS X' then begin
    detachedFrame.setSize, detachedFrame.getSize()+1
    detachedFrame.updateContent
    detachedFrame.updateContent
  endif
end

pro hubTBToolboxController::attachFrame, detachedFrame
  if ~isa(detachedFrame, 'hubTBFrameDetached') then begin
    message, 'Wrong argument type.'
  endif

;  ; destroy detachedFrame
;  detachedFrame.finish

  ; show content in original frame
  self.showContentInFrame, detachedFrame.getContent(), detachedFrame.attachedFrame

  ; delete content from detached frame
  self.deleteContentInFrame, detachedFrame.getFrameRoot()
  
  ; unset detached flag to the original frame
  detachedFrame.attachedFrame.isDetached = 0b
  detachedFrame.attachedFrame.parent.updateView
  detachedFrame.attachedFrame.parent.updateContent
end

pro hubTBToolboxController__define
  define = {hubTBToolboxController, inherits hubMVCController}
end

