function hubTBToolbox::init, Title=title, Column=column, XRegisterName=XRegisterName, Icon=icon, Size=size, GroupLeader=groupLeader, NoMenu=noMenu
  !null = self.hubMVC::init(hubTBToolboxController(), hubTBToolboxModel())
  self.subscribeView, hubTBToolboxView()
  (self.controller.views)[0].frameRoot = hubTBFrameRoot(self, /Column, Title=title, XRegisterName=XRegisterName, Icon=icon, GroupLeader=groupLeader, NoMenu=noMenu)
  if isa(size) then begin
    _size = size
  endif else begin 
    _size = get_screen_size()*0.75
  endelse
  _offset = get_screen_size()/2. - _size/2
  self.setSize, _size
  self.setOffset, _offset
  return, 1b
end

pro hubTBToolbox::createMenu, menuFiles, Log=log
  self.controller.createMenu, menuFiles, Log=log
end

pro hubTBToolbox::setSize, size, Update=update
  self.controller.setSize, size
  if keyword_set(update) then begin
    self.controller.updateView
  endif
end

function hubTBToolbox::getSize
  return, self.controller.getSize()
end

pro hubTBToolbox::setOffset, offset, Update=update
  self.controller.setOffset, offset
  if keyword_set(update) then begin
    self.controller.updateView, /OffsetOnly
  endif
end

function hubTBToolbox::getOffset
  return, self.controller.getOffset()
end

pro hubTBToolbox::updateView, _EXTRA=_extra
  self.controller.updateView, _EXTRA=_extra
end

pro hubTBToolbox::updateContentSize
  self.controller.updateContentSize
end

pro hubTBToolbox::updateContentView
  self.controller.updateContentView
end

pro hubTBToolbox::updateContent
  self.updateContentSize
  self.updateContentView
end

pro hubTBToolbox::fixDisplay
  self.controller.fixDisplay
end

function hubTBToolbox::getFrameRoot
  return, self.controller.getFrameRoot()
end

function hubTBToolbox::getTopLevelBase
  return, self.controller.getTopLevelBase()
end

function hubTBToolbox::getTabFrameManager
  return, self.tabFrameManager
end

pro hubTBToolbox::setTabFrameManager, tabFrameManager
  self.tabFrameManager = tabFrameManager
end

pro hubTBToolbox::showContentInFrame, content, frame, NoUpdate=noUpdate
  self.controller.showContentInFrame, content, frame, NoUpdate=noUpdate
end

pro hubTBToolbox::show
  self.controller.show
end

pro hubTBToolbox::deleteContentInFrame, frame, RemoveDependencies=removeDependencies
  self.controller.deleteContentInFrame, frame, RemoveDependencies=removeDependencies
end

pro hubTBToolbox::removeFrame, frame, RemoveDependencies=removeDependencies
  self.controller.removeFrame, frame, RemoveDependencies=removeDependencies
end

pro hubTBToolbox::detachFrame, frame
  self.controller.detachFrame, frame
end

pro hubTBToolbox::attachFrame, frame
  self.controller.attachFrame, frame
end

pro hubTBToolbox::start
  self.controller.start
end

pro hubTBToolbox::finish
  ok = dialog_message(/QUESTION, 'Exit Application?', DIALOG_PARENT=self.getTopLevelBase())
  if ok eq 'Yes' then begin
    self.finishProcedure
    self.controller.finish
  endif
end

pro hubTBToolbox::finishProcedure
  if self.finishProcedure ne '' then begin
    call_procedure, self.finishProcedure, self
  endif
end

pro hubTBToolbox::setFinishProcedure, procedureName
  self.finishProcedure = procedureName
end
      
pro hubTBToolbox__define

  define = {hubTBToolbox, inherits hubMVC,$
    tabFrameManager:obj_new(),$
    finishProcedure:'' $
    }
  
end