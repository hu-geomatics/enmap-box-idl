pro hubTBToolboxView::create
  self.frameRoot.create
end

pro hubTBToolboxView::finish
  self.frameRoot.finish
end

pro hubTBToolboxView::updateView, _EXTRA=_extra
  self.frameRoot.updateView, _EXTRA=_extra
end

pro hubTBToolboxView::updateSize
  self.frameRoot.updateSize
end

pro hubTBToolboxView::updateOffset
  self.frameRoot.updateOffset
end

pro hubTBToolboxView::updateContentView
  self.frameRoot.updateContentView
end

pro hubTBToolboxView::updateContentSize
  self.frameRoot.updateContentSize
end

pro hubTBToolboxView::setSize, size
  self.frameRoot.size = size
end

function hubTBToolboxView::getSize
  return, self.frameRoot.size
end

pro hubTBToolboxView::setOffset, offset
  self.frameRoot.offset = offset
end

function hubTBToolboxView::getOffset
  return, self.frameRoot.offset
end

pro hubTBToolboxView::setProperty, _EXTRA=_extra, frameRoot=frameRoot
  self.hubMVCView::setProperty, _EXTRA=_extra
  if isa(frameRoot) then self.frameRoot = frameRoot
end

pro hubTBToolboxView::getProperty, frameRoot=frameRoot
  if arg_present(frameRoot) then frameRoot = self.frameRoot
end

pro hubTBToolboxView::handleEvent, objEvent
  self.frameRoot.handleEvent, objEvent
end

pro hubTBToolboxView::cleanup
  obj_destroy, self.frameRoot
end

pro hubTBToolboxView__define
  define = {hubTBToolboxView, inherits hubMVCView,$
    frameRoot : obj_new()}
  
end