function hubTBManagedFrame::init, gridFrameManager, content, _EXTRA=_extra
  if ~isa(gridFrameManager, 'hubTBFrameGridManager') then message, 'Wrong argument.'
  if ~isa(content, 'hubTBManagedContentMVC') then message, 'Wrong argument.'
  
  !null = self.hubTBFrame::init(gridFrameManager, _EXTRA=_extra, /FRAME)
  self.gridFrameManager = gridFrameManager
  self.create
  self.toolbox.showContentInFrame, content, self, /NoUpdate
  self.setTitle
  return, 1b
end

pro hubTBManagedFrame::createChildFrames
end

pro hubTBManagedFrame::createTitleBar, base
  self.treeTitleBase = widget_base(base)
  objEvent = hubObjCallbackEvent(self, 'managedFrame', {draginfo:'Info Text...'})
  self.treeTitleRoot = widget_tree(self.treeTitleBase, UVALUE=objEvent, YSIZE=25, /DRAGGABLE, /TRACKING_EVENTS, NO_BITMAPS=0)
  bitmap = hubTBAuxiliaryGetIconBitmaps(/Hand)
  self.treeTitle = widget_tree(self.treeTitleRoot, UVALUE=objEvent, VALUE=' ', /DRAGGABLE, /TRACKING_EVENTS, BITMAP=bitmap, /MASK)
end

pro hubTBManagedFrame::updateView, _EXTRA=_extra
  self.hubTBFrame::updateView, _EXTRA=_extra
  if self.isDetached then begin
    self.detachedFrame.updateView, /SizeOnly, /OffsetOnly ; don't update size and position
  endif
end

pro hubTBManagedFrame::updateTitleBar
  sizeContent = self.getContentSize()
  buttonsWidth = 0
  if self.showClose then buttonsWidth += (widget_info(/GEOMETRY, self.buttonClose)).scr_xsize
  if self.showDetach then buttonsWidth += (widget_info(/GEOMETRY, self.buttonDetach)).scr_xsize
  baseWidth =sizeContent[0]-buttonsWidth>1
  if self.parent.getShowMenuBars() then begin
    baseHight = (widget_info(/GEOMETRY, self.treeTitleRoot)).scr_ysize
    map = 1
  endif else begin
    case !VERSION.OS_FAMILY of
      'Windows' : baseHight = 0  
      'unix' : baseHight = 1
    endcase
    map = 0
  endelse
  widget_control, self.treeTitleBase, XSIZE=baseWidth
  widget_control, self.treeTitleRoot, XSIZE=5000
  widget_control, self.treeTitle, SET_TREE_SELECT=0, SET_VALUE=self.getTitle()
  baseMenuBar = widget_info(/PARENT, self.treeTitleBase)
  widget_control, baseMenuBar, YSIZE=baseHight, MAP=map
end

pro hubTBManagedFrame::addFrame, frame
  message, 'Managed frame can not have child frames.'
end

pro hubTBManagedFrame::subscribeView, view
  if ~isa(view, 'hubTBManagedContentMVCView') then message, 'Wrong argument.'
  self.hubTBFrame::subscribeView, view
end

pro hubTBManagedFrame::unsubscribeView
  self.subscribedView = obj_new()
end

function hubTBManagedFrame::getContent
  if self.isDetached then begin
    content = self.detachedFrame.getContent()
  endif else begin
    content = self.subscribedView.controller.mvc
  endelse
  return, content
end

pro hubTBManagedFrame::setTitle

  if self.isDetached then begin
    self.detachedFrame.setTitle
  endif else begin
    widget_control, self.treeTitle, SET_VALUE=self.getTitle()
  endelse
  
end

pro hubTBManagedFrame::newFrameRelativeTo, _EXTRA=_extra
  managedFrameManager = self.parent
  newFrame = managedFrameManager.addFrameWithContentRelativeToTargetFrame(_EXTRA=_extra, self, /ShowTitleBar)
  managedFrameManager.updateView
end

function hubTBManagedFrame::getTitle
  index = self.parent.findFrame(self)
  if self.isDetached then begin
    content = self.detachedFrame.getContent()
  endif else begin
    content = self.getContent()
  endelse
  contentTitle = content.getTitle()
  title = 'View '+strcompress(/REMOVE_ALL, index+1)+':'+contentTitle
  return, title
end

pro hubTBManagedFrame::select
  self.gridFrameManager.selectFrame, self
end

function hubTBManagedFrame::getGridFrameManager
  return, self.gridFrameManager
end

pro hubTBManagedFrame::handleEvent, objEvent
  self.hubTBFrame::handleEvent, objEvent
  event = objEvent.event
  case objEvent.name of
    'managedFrame' : begin
      case !VERSION.OS_FAMILY of
        'Windows' : widget_control, self.treeTitle, SET_TREE_SELECT=0
        'unix' : if objEvent.subcategory eq 'tracking event' then widget_control, self.treeTitle, SET_TREE_SELECT=0
        else : ;
      endcase
    end
    'close' : begin
      gridFrameManager = self.getGridFrameManager()
      gridFrameManager.ensureDefaultSelection
      self.updateContentView
    end
    else : ; do nothing
  endcase
end

pro hubTBManagedFrame__define
  define = {hubTBManagedFrame, inherits hubTBFrame, $
    gridFrameManager : obj_new(),$
    treeTitleRoot : 0l, $
    treeTitle : 0l, $
    treeTitleBase : 0l}
end
