function hubTBManagedContentMVCModel::init
  case !VERSION.OS_FAMILY of
    'unix' : self.fillColor = replicate(192b, 3)
    'Windows' : self.fillColor = replicate(240b, 3)
  endcase
  self.oScene = IDLgrScene()
  self.oView = IDLgrView(Color=self.fillColor)
  self.oModel = IDLgrModel()
  self.oFrame = IDLgrPlot(COLOR=[0,0,0],/USE_ZVALUE,ZVALUE=1,THICK=4.0)
  font = IDLgrFont('Helvetica', SIZE=8, THICK=1.0)
  self.oTextTemporaryMessage = IDLgrText('', /ENABLE_FORMATTING, /ONGLASS, FILL_COLOR=self.fillColor, /FILL_BACKGROUND, VERTICAL_ALIGNMENT=1, HIDE=1, Font=font)
  self.oTextPermanentMessage = IDLgrText('', /ENABLE_FORMATTING, /ONGLASS, FILL_COLOR=self.fillColor, /FILL_BACKGROUND, VERTICAL_ALIGNMENT=1, HIDE=1, Font=font)

  self.textMessageTime = 1.
  self.oModel.add, self.oFrame
  (self.getTextMessageModel()).add, self.oTextTemporaryMessage
  (self.getTextMessageModel()).add, self.oTextPermanentMessage
  
  self.oView.add, self.oModel
  self.oScene.add, self.oView
  self.title = 'empty'
  return, 1b
end

function hubTBManagedContentMVCModel::getTextMessageModel
  return, self.oModel
end

function hubTBManagedContentMVCModel::getTextMessageView
  return, self.oView
end

pro hubTBManagedContentMVCModel::adjustForViewSpecifica, view
  ; adjust model border
  view.oWindow.getProperty, DIMENSION=windowSize
  self.oView.getProperty, viewplane_rect=viewplaneRectangle
  viewplaneOffset = viewplaneRectangle[0:1]
  viewplaneSize = viewplaneRectangle[2:3]
  framePoints = [[viewplaneOffset], [viewplaneOffset+viewplaneSize*[0,1]], [viewplaneOffset+viewplaneSize*[1,1]], [viewplaneOffset+viewplaneSize*[1,0]], [viewplaneOffset]]
  self.oFrame.setProperty, DATAX=framePoints[0,*], DATAY=framePoints[1,*], HIDE=~self.isSelected

  ; adjust text position
  oView = self.getTextMessageView()
  oView.getProperty, viewplane_rect=viewplaneRectangle
  viewplaneOffset = viewplaneRectangle[0:1]
  viewplaneSize = viewplaneRectangle[2:3]
  locations = [viewplaneOffset+viewplaneSize*[0.99,0.99], 1]
  self.oTextTemporaryMessage.setProperty, LOCATIONS=locations, ALIGNMENT=1
  locations = [viewplaneOffset+viewplaneSize*[0.01,0.99], 1]
  self.oTextPermanentMessage.setProperty, LOCATIONS=locations, ALIGNMENT=0
  
end

function hubTBManagedContentMVCModel::getFormattedMessage, text
  if isa(text) then begin
    formattedText = ' '+text[0]+' '
    for i=1,n_elements(text)-1 do formattedText += '!C '+text[i]+' '
  endif else begin
    formattedText = !null 
  endelse
  return, formattedText
end

pro hubTBManagedContentMVCModel::showTemporaryMessage, text
  formattedText = self.getFormattedMessage(text)
  self.oTextTemporaryMessage.setProperty, STRINGS=formattedText, HIDE=0  
end

pro hubTBManagedContentMVCModel::hideTemporaryMessage
  self.oTextTemporaryMessage.setProperty, HIDE=1
end

pro hubTBManagedContentMVCModel::showPermanentMessage, text, TurnOn=turnOn
  if isa(text) then begin 
    formattedText = self.getFormattedMessage(text)
  endif else begin
    formattedText = ''
  endelse
  self.oTextPermanentMessage.setProperty, STRINGS=formattedText  
  if keyword_set(turnOn) then begin
    self.oTextPermanentMessage.setProperty, Hide=0  
  endif
end

pro hubTBManagedContentMVCModel::hidePermanentMessage
  self.oTextPermanentMessage.setProperty, HIDE=1
end

function hubTBManagedContentMVCModel::permanentMessageVisible
  self.oTextPermanentMessage.getProperty, HIDE=hide
  return, ~hide
end

pro hubTBManagedContentMVCModel::setProperty, _EXTRA=_extra, isSelected=isSelected
  self.hubTBContentMVCModel::setProperty, _EXTRA=_extra
  if isa(isSelected) then self.isSelected = isSelected
end

function hubTBManagedContentMVCModel::getModel
  return, self.oModel
end

pro hubTBManagedContentMVCModel::getProperty, _REF_EXTRA=_extra, oScene=oScene, oView=oView, oFrame=oFrame, isSelected=isSelected
  self.hubTBContentMVCModel::getProperty, _EXTRA=_extra
  if arg_present(oScene) then oScene = self.oScene
  if arg_present(oView) then oView = self.oView
  if arg_present(oFrame) then oFrame = self.oFrame
  if arg_present(isSelected) then isSelected = self.isSelected
end

pro hubTBManagedContentMVCModel__define
  define = {hubTBManagedContentMVCModel, inherits hubTBContentMVCModel, $
    oScene:obj_new(), $
    oView:obj_new(), $
    oModel:obj_new(), $
    oFrame:obj_new(), $
    oTextTemporaryMessage:obj_new(), $
    oTextPermanentMessage:obj_new(), $
    fillColor:[0b,0b,0b], $
    textMessageTime:0., $
    isSelected:0b}
end