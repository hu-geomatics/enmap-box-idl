function hubTBManagedContentMVCView::init
  self.eventHandler = hubTBManagedContentMVCViewEventHandler(self)
  return, 1b
end

pro hubTBManagedContentMVCView::create
  uvalue = hubObjCallbackEvent(self.eventHandler, 'window')
  self.windowID = widget_draw(self.base, GRAPHICS_LEVEL=2, RETAIN=2, UVALUE=uvalue,$
    /WHEEL_EVENTS, /BUTTON_EVENTS, /KEYBOARD_EVENTS, /DROP_EVENTS, /MOTION_EVENTS, /TRACKING_EVENTS)
  widget_control, self.windowID, GET_VALUE=oWindow
  self.oWindow = oWindow
end

pro hubTBManagedContentMVCView::createContext

  if widget_info(/VALID_ID, self.contextBase) then begin
    widget_control, /DESTROY, self.contextBase
  endif

  self.contextBase = widget_base(self.base, /CONTEXT_MENU)
  
  if ~strcmp(typename(self), 'hubTBManagedContentMVCView', /FOLD_CASE) then begin
    !null = widget_button(self.contextBase, VALUE='Clear View', UVALUE=hubObjCallbackEvent(self.eventHandler, 'clearView'))
  endif
  !null = widget_button(self.contextBase, VALUE='Remove View', UVALUE=hubObjCallbackEvent(self.eventHandler, 'removeView'))
  if ~self.isDetached() then begin
    menu = widget_button(/MENU, self.contextBase, VALUE='Insert New View')
    !null = widget_button(menu, VALUE='to the Left', UVALUE=hubObjCallbackEvent(self.eventHandler, 'newViewLeft'))
    !null = widget_button(menu, VALUE='to the Right', UVALUE=hubObjCallbackEvent(self.eventHandler, 'newViewRight'))
    !null = widget_button(menu, VALUE='above', UVALUE=hubObjCallbackEvent(self.eventHandler, 'newViewAbove'))
    !null = widget_button(menu, VALUE='below', UVALUE=hubObjCallbackEvent(self.eventHandler, 'newViewBelow'))
  end
  !null = widget_button(self.contextBase, VALUE='New Spectral Library', UVALUE=hubObjCallbackEvent(self.eventHandler, 'newSpeclib'))
  !null = widget_button(self.contextBase, VALUE='Show/Hide View Menus [m]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'showViewMenus'), /SEPARATOR)
end

pro hubTBManagedContentMVCView::delete
  widget_control, /DESTROY, self.windowID
end

pro hubTBManagedContentMVCView::updateSize
  size = self.getSize()
  widget_control, self.windowID, SCR_XSIZE=size[0]>1, SCR_YSIZE=size[1]>1
end

pro hubTBManagedContentMVCView::updateView
  self.controller.adjustModelForViewSpecifica, self
  self.draw
end

pro hubTBManagedContentMVCView::draw
  hubMath_mathErrorsOff, state
  self.oWindow.draw, self.controller.getGraphicScene()
  hubMath_mathErrorsOn, state
end

pro hubTBManagedContentMVCView::setInputFocus
  if self.controller.getIsSelected() then begin
    widget_control, self.windowID, /INPUT_FOCUS
  endif
end

pro hubTBManagedContentMVCView::displayWindowContextMenu, x, y, base
  self.createContext
  if ~isa(base) then begin
    base = self.base
  endif
  widget_displaycontextmenu, base, x, y, self.contextBase
end

;function hubTBManagedContentMVCView::isWindowContextMenuDisplayed
;  return, 0
;end

pro hubTBManagedContentMVCView::showTemporaryMessage, text
  self.controller.showTemporaryMessage, text
end

pro hubTBManagedContentMVCView::showPermanentMessage, text, TurnOn=turnOn
  self.controller.showPermanentMessage, text, TurnOn=turnOn
end

function hubTBManagedContentMVCView::getSelectedView, windowSelection
  selection = (self.oWindow.select(self.controller.getGraphicScene(), windowSelection))[0]
  oView = isa(selection, 'IDLgrView') ? selection : obj_new()
  return, oView
end

function hubTBManagedContentMVCView::getGroupLeader
  stop;return, oView
end

pro hubTBManagedContentMVCView::getProperty, _REF_EXTRA=_extra, oWindow=oWindow, windowID=windowID
  self.hubTBContentMVCView::getProperty, _EXTRA=_extra
  if arg_present(oWindow) then oWindow = self.oWindow
  if arg_present(windowID) then windowID = self.windowID  
end

pro hubTBManagedContentMVCView__define
  define = {hubTBManagedContentMVCView, inherits hubTBContentMVCView,$
    windowID:0l, $
    oWindow:obj_new(), $
    eventHandler:obj_new(), $
    contextBase:0l $
    }
end