function hubTBManagedContentMVCViewEventHandler::init, view
  self.view = view
  return, 1b
end

pro hubTBManagedContentMVCViewEventHandler::handleEvent, objEvent

  case objEvent.category of
    'drop event' : self.handleDropEvent, objEvent
    'timer event'  : self.handleTimerEvent, objEvent
    'window event' : self.handleWindowEvent, objEvent
    'button event' : self.handleButtonEvent, objEvent
    'tree event' : self.handleTreeEvent, objEvent
    else : begin
      help, objEvent.event
      print, objEvent.category
    end
  endcase

  if (objEvent.name ne 'clearView') and (objEvent.name ne 'removeView') and (objEvent.name ne 'newSpeclib') then begin
    if self.doUpdateView then begin
      self.view.updateView
    endif
    self.doUpdateView = 0b
  endif
end

pro hubTBManagedContentMVCViewEventHandler::handleWindowEvent, objEvent

;  case !VERSION.OS_FAMILY of 
;    'unix' : begin
;      if self.view.isWindowContextMenuDisplayed() then begin
;        return
;      endif
;    end
;    'Windows' : ;
;  endcase


  case objEvent.subcategory of
    'button pressed event' : self.view.controller.selectView, self.view, /ForceReselection
    'button released event' : begin
      case objEvent.subsubcategory of
        'right mouse button released event' : self.handleWindowContextEvent, [objEvent.event.x,objEvent.event.y]
        else : ;
      endcase
    end
    'ascii key pressed event' : begin
      if objEvent.event.ch eq 109 then self.view.controller.swapMenu
    end
;    'tracking event' : begin
;      ; macDebug
;      self.view.controller.trackingEvent, self.view
;    end 
    else : begin
     ; print, objEvent.subcategory
    end
  endcase
end

pro hubTBManagedContentMVCViewEventHandler::handleButtonEvent, objEvent
  case objEvent.name of
    'newSpeclib' : begin
      self.view.controller.newSpeclibView, self.view
    end
    'clearView' : begin
      self.view.controller.clearView, self.view, /RemoveDependencies
    end
    'removeView' : begin
      self.view.controller.removeView, self.view, /RemoveDependencies
      updateGridManager = 1b
    end
    'newViewLeft' : begin
      self.view.controller.newFrameRelativeToView, self.view, /Left, /ShowTitleBar, /ShowDetach, /ShowClose
      updateGridManager = 1b
    end
    'newViewRight' : begin
      self.view.controller.newFrameRelativeToView, self.view, /Right, /ShowTitleBar, /ShowDetach, /ShowClose
      updateGridManager = 1b    
    end
    'newViewAbove' : begin
      self.view.controller.newFrameRelativeToView, self.view, /Above, /ShowTitleBar, /ShowDetach, /ShowClose
      updateGridManager = 1b      
    end
    'newViewBelow' : begin
      self.view.controller.newFrameRelativeToView, self.view, /Below, /ShowTitleBar, /ShowDetach, /ShowClose
      updateGridManager = 1b      
    end
    'showViewMenus' : begin
      gridFrameManager = self.view.controller.getGridFrameManager() 
      gridFrameManager.swapShowMenuBars
      updateGridManager = 1b
    end
    else : ; do nothing
  endcase
  
  if keyword_set(updateGridManager) then begin
    gridFrameManager = self.view.controller.getGridFrameManager()
    gridFrameManager.updateView
    gridFrameManager.updateContent
  endif
  
end

pro hubTBManagedContentMVCViewEventHandler::handleWindowContextEvent, xy, base, NoFlip=noFlip
  if ~keyword_set(noFlip) then begin
    self.view.oWindow.getProperty, DIMENSIONS=windowSize
    xy[1] = windowSize[1]-xy[1]
  endif
  self.view.displayWindowContextMenu, xy[0], xy[1], base
end

pro hubTBManagedContentMVCViewEventHandler::handleTimerEvent, objEvent
  self.view.controller.hideTemporaryMessage
  self.doUpdateView = 1b
end

pro hubTBManagedContentMVCViewEventHandler::handleDropEvent, objEvent

  dragAndDropInformation = objEvent.information
  dragObjEvent = dragAndDropInformation.dragObjEvent
  dragParameters = (dragObjEvent.parameters)
  case dragObjEvent.name of
    'managedFrame' : begin
      dragFrame = dragObjEvent.object
      dragView = dragFrame.subscribedView
      dropView = self.view
      self.view.controller.swapViews, dragView, dropView
    end
    'filelist' : begin
      filelistView = dragObjEvent.object
;      filelist = filelistView.view.controller.mvc
      draggedTree = filelistView.getTreeSelection() 
      draggedTreeInfo = filelistView.getTreeInfo(draggedTree)
      case draggedTreeInfo.name of
        'file folder' : begin
          case draggedTreeInfo.fileType of
            'images' : self.handleDropImageEvent, draggedTreeInfo.header, objEvent.event.top
            'speclibs' : self.handleDropSpeclibEvent, draggedTreeInfo.header, objEvent.event.top
          endcase
        end
        'band leaf' : self.handleDropImageBandEvent, draggedTreeInfo.header, draggedTreeInfo.band, objEvent.event.top
        else : stop
      endcase
    end

    else : message, 'Wrong drag source.'
  endcase
end

pro hubTBManagedContentMVCViewEventHandler::handleDropImageEvent, header, guiGroupLeader
  toolbox = self.view.getToolbox()
  frame = self.view.controller.findFrameByView(self.view)
  gridFrameManager = self.view.controller.getGridFrameManager()
  filename = header.getMeta('filename data')
  if header.getMeta('bands') eq 1 then begin
    bands = 0
  endif
  if header.hasMeta('default bands') then begin
    bands = header.getMeta('default bands')
  endif
  if ~isa(bands) then begin
    dialog = hubTBManagedContentImageDialogBandSelection(header, [0,0,0], guiGroupLeader)
    bands = dialog.getResult()
    if ~isa(bands) then return
    if min(bands) eq max(bands) then begin
      bands = bands[0]
    endif
  endif

  classification = strcmp(header.getMeta('file type'), 'envi classification', /FOLD_CASE)
  sourceContent = hubTBManagedContentImage(filename, bands, toolbox, gridFrameManager, Classification=classification)
  imageSize = [header.getMeta('samples'),header.getMeta('lines')]
  sourceContent.setImageCenter, imageSize/2
  sourceContent.setImageSelection, imageSize/2
  sourceContent.setImageZoom, /SceneFull, ImageView=self.view
  toolbox.deleteContentInFrame, frame, /RemoveDependencies
  toolbox.showContentInFrame, sourceContent, frame
  frame.select
end

pro hubTBManagedContentMVCViewEventHandler::handleDropImageBandEvent, header, band, guiGroupLeader
  toolbox = self.view.getToolbox()
  frame = self.view.controller.findFrameByView(self.view)
  gridFrameManager = self.view.controller.getGridFrameManager()
  filename = header.getMeta('filename data')
  classification = strcmp(header.getMeta('file type'), 'envi classification', /FOLD_CASE)
  sourceContent = hubTBManagedContentImage(filename, band, toolbox, gridFrameManager, Classification=classification)
  imageSize = [header.getMeta('samples'),header.getMeta('lines')]
  sourceContent.setImageCenter, imageSize/2
  sourceContent.setImageSelection, imageSize/2
  sourceContent.setImageZoom, /SceneFull, ImageView=self.view
  toolbox.deleteContentInFrame, frame, /RemoveDependencies
  toolbox.showContentInFrame, sourceContent, frame
  frame.select
end

pro hubTBManagedContentMVCViewEventHandler::handleDropSpeclibEvent, header, guiGroupLeader

  if isa(self.view, 'hubTBManagedContentSpeclibView') then begin
    speclibContent = self.view
    
  endif else begin 
    hubAMW_program, guiGroupLeader, Title='Visualize Speclib'
    hubAMW_checklist, 'index', LIST=['Spectral View or','Image View'], Title='Visualize Speclib inside', Value=0
    result = hubAMW_manage(/STRUCTURE)
    if ~result.accept then return
    
    if result.index eq 1 then begin
      self.handleDropImageEvent, header, guiGroupLeader
      return
    endif
    self.view.controller.hubTBManagedContentMVCController::newSpeclibView, self.view, SpeclibContent=speclibContent, SpeclibFrame=speclibFrame
    speclibFrame.select
  endelse  

  filename = header.getMeta('filename data')
  speclibContent.controller.importENVISpeclib, filename
end

pro hubTBManagedContentMVCViewEventHandler__define
  define = {hubTBManagedContentMVCViewEventHandler, inherits IDL_Object,$
    view:obj_new(),$
    doUpdateView:0b}
end