function hubTBManagedContentMVCController::newView
  return, hubTBManagedContentMVCView()
end

pro hubTBManagedContentMVCController::adjustModelForViewSpecifica, view
  self.model.adjustForViewSpecifica, view
end

function hubTBManagedContentMVCController::getGraphicScene
  return, self.model.oScene
end

function hubTBManagedContentMVCController::getGraphicView, _EXTRA=_extra
  return, self.model.getGraphicView(_EXTRA=_extra)
end

pro hubTBManagedContentMVCController::clearView, view, RemoveDependencies=removeDependencies
  frame = self.findFrameByView(view)
  toolbox = self.mvc.toolbox
  toolbox.deleteContentInFrame, frame, RemoveDependencies=removeDependencies
  emptyContent = hubTBManagedContentMVC(toolbox, self.gridFrameManager)
  toolbox.showContentInFrame, emptyContent, frame
  frame.updateView
end

;pro hubTBManagedContentMVCController::trackingEvent, view
;  frame = self.findFrameByView(view)
;  frame.setSize
;  frame.updateContent
;  
;end


pro hubTBManagedContentMVCController::removeView, view, RemoveDependencies=removeDependencies
  frame = self.findFrameByView(view)
  self.mvc.toolbox.removeFrame, frame, RemoveDependencies=removeDependencies
  self.gridFrameManager.ensureDefaultFrame
  self.gridFrameManager.updateView
  self.gridFrameManager.updateContent
end

pro hubTBManagedContentMVCController::newSpeclibView, view, SpeclibContent=speclibContent, SpeclibFrame=speclibFrame
  speclibFrame = self.findFrameByView(view)
  toolbox = self.mvc.toolbox
  toolbox.deleteContentInFrame, speclibFrame, /RemoveDependencies
  speclibContent = hubTBManagedContentSpeclib(toolbox, self.gridFrameManager)
  toolbox.showContentInFrame, speclibContent, speclibFrame
  speclibFrame.updateView
  speclibFrame.select
end

pro hubTBManagedContentMVCController::removeDependencies
  ; do nothing
end

pro hubTBManagedContentMVCController::selectView, view, ForceReselection=forceReselection
  ; do nothing if view is already selected, this prevents unneccessary redrawing and avoids flickering effect
  ; under linux it is necessary because of a bug with context menus
  case !VERSION.OS_FAMILY of 
    'Windows' : if self.model.isSelected then return
    'unix' : if ~keyword_set(forceReselection) && self.model.isSelected then return
  endcase
  
  frame = self.findFrameByView(view)
  managedFrameManager = frame.getFrameManager()
  managedFrameManager.selectContent, self.mvc
end

pro hubTBManagedContentMVCController::swapViews, dragView, dropView
  dragFrame = dragView.controller.findFrameByView(dragView)
  dropFrame = dropView.controller.findFrameByView(dropView)
  managedFrameManager = dragFrame.getFrameManager()
  managedFrameManager.swapFrames, dragFrame, dropFrame
;  managedFrameManager.update
end

pro hubTBManagedContentMVCController::setIsSelected, isSelected
  self.model.isSelected = isSelected
end

function hubTBManagedContentMVCController::getIsSelected
  return, self.model.isSelected
end

pro hubTBManagedContentMVCController::newFrameRelativeToView, view, _EXTRA=_extra
  frame = self.findFrameByView(view)
  frame.newFrameRelativeTo, _EXTRA=_extra
end

function hubTBManagedContentMVCController::isDetached, view
  frame = self.findFrameByView(view)
  return, isa(frame, 'HubTBManagedFrame') eq 0 
end

pro hubTBManagedContentMVCController::showTemporaryMessage, text
  if isa(text) then begin
    self.model.showTemporaryMessage, text
    if isa(((self.views)[0])) then begin
      widget_control, ((self.views)[0]).windowID, TIMER=2.
    endif
  endif else begin
    self.hideTemporaryMessage
  endelse
  self.updateView
end

pro hubTBManagedContentMVCController::hideTemporaryMessage
  self.model.hideTemporaryMessage
  self.updateView
end

pro hubTBManagedContentMVCController::showPermanentMessage, text, TurnOn=turnOn
  self.model.showPermanentMessage, text, TurnOn=turnOn
  self.updateView
end

pro hubTBManagedContentMVCController::hidePermanentMessage
  self.model.hidePermanentMessage
  self.updateView
end

pro hubTBManagedContentMVCController::swapMenu
  setButton = widget_info(/BUTTON_SET, self.gridFrameManager.buttonShowMenuBars)
  widget_control, self.gridFrameManager.buttonShowMenuBars, SET_BUTTON=~setButton
  self.gridFrameManager.toolbox.updateView, /SizeOnly
  self.gridFrameManager.toolbox.updateContent
end

function hubTBManagedContentMVCController::permanentMessageVisible
  return, self.model.permanentMessageVisible()
end

pro hubTBManagedContentMVCController::setGridFrameManager, gridFrameManager
  if ~isa(gridFrameManager, 'hubTBFrameGridManager') then begin
    message, 'Wrong argument.'
  endif
  self.gridFrameManager = gridFrameManager
end

function hubTBManagedContentMVCController::getGridFrameManager
  return, self.gridFrameManager
end

function hubTBManagedContentMVCController::getTabFrameManager
  return, self.gridFrameManager.getTabFrameManager()
end

pro hubTBManagedContentMVCController__define
  define = {hubTBManagedContentMVCController, inherits hubTBContentMVCController,$
    gridFrameManager:obj_new()}
end
