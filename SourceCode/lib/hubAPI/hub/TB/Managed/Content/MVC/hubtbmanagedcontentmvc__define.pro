function hubTBManagedContentMVC::init, toolbox, gridFrameManager
  !null = self.hubTBContentMVC::init(self.createController(), self.createModel(), toolbox)
  self.controller.setGridFrameManager, gridFrameManager
  self.controller.showPermanentMessage, /TurnOn
  return, 1b
end

function hubTBManagedContentMVC::createController
  return, hubTBManagedContentMVCController()
end

function hubTBManagedContentMVC::createModel
  return, hubTBManagedContentMVCModel()
end

pro hubTBManagedContentMVC::setIsSelected, isSelected
  self.controller.setIsSelected, isSelected
end

function hubTBManagedContentMVC::getIsSelected
  return, self.controller.getIsSelected()
end

pro hubTBManagedContentMVC__define
  define = {hubTBManagedContentMVC, inherits hubTBContentMVC}
end