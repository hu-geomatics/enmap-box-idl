function hubTBManagedContentSpeclibModel::init

  ; profiles
  self.oViewProfiles = IDLgrView()
  self.oModelProfiles = IDLgrModel()
  font = IDLgrFont('Helvetica', SIZE=8, THICK=1.)
  self.oAxisX = IDLgrAxis(0, TITLE=IDLgrText(FONT=font, /ENABLE_FORMATTING, /ONGLASS))
  self.oAxisY = IDLgrAxis(1, TITLE=IDLgrText(FONT=font, /ENABLE_FORMATTING, /ONGLASS))
  self.oAxisX.getProperty, TICKTEXT=oTextTicksX
  self.oAxisY.getProperty, TICKTEXT=oTextTicksY
  oTextTicksX.setProperty, FONT=font, /ENABLE_FORMATTING, /ONGLASS
  oTextTicksY.setProperty, FONT=font, /ENABLE_FORMATTING, /ONGLASS

  self.oPlotProfilesCrosshair[0] = IDLgrPlot(COLOR=[255,0,0], LINESTYLE=2, /USE_ZVALUE, ZVALUE=0.9, THICK=1.0)
  self.oPlotProfilesCrosshair[1] = IDLgrPlot(COLOR=[255,0,0], LINESTYLE=2, /USE_ZVALUE, ZVALUE=0.9, THICK=1.0)
  self.oModelProfiles.add, self.oPlotProfilesCrosshair[0]
  self.oModelProfiles.add, self.oPlotProfilesCrosshair[1]

  ; window
  !null = self.hubTBManagedContentMVCModel::init()


  self.oScene.add, self.oViewProfiles
  self.oViewProfiles.add, self.oModelProfiles
  self.oModelProfiles.add, self.oAxisX
  self.oModelProfiles.add, self.oAxisY

  self.library = hubTBManagedContentSpeclibProfileLibrary()
  self.libraryLinkedImageProfiles = hubTBManagedContentSpeclibProfileLibrary()
  
  self.setPlotXUnits, (['nanometers','indices'])[1]
  self.setPlotYUnits, (['continuum-removed','normal'])[1]
  
 ; self.setPlotZoom, [0,0], 1.

  self.title = 'Spectral Library'
  self.linkedImages = list()
  self.setFixedXRange, [-1,+1]*!VALUES.F_INFINITY
  self.setFixedYRange, [-1,+1]*!VALUES.F_INFINITY
  
  
  return, 1b
end

function hubTBManagedContentSpeclibModel::getSpeclibSize
  return, self.library.getNumberOfProfiles()
end

function hubTBManagedContentSpeclibModel::getTextMessageModel
  return, self.oModelProfiles
end

function hubTBManagedContentSpeclibModel::getTextMessageView
  return, self.oViewProfiles
end

pro hubTBManagedContentSpeclibModel::setPlotXUnits, units, Refresh=refresh
  case units of
    'nanometers' : ; ok
    'micrometers' : ; ok
    'values' : ; ok'
    'indices' : ; ok'
  endcase
  self.plotXUnits = units
  if keyword_set(refresh) then begin
    self.library.setPlotDataXByUnits, units
    self.libraryLinkedImageProfiles.setPlotDataXByUnits, units
    self.resetPlot
  endif
end

pro hubTBManagedContentSpeclibModel::setPlotYUnits, units, Refresh=refresh
  case units of
    'normal' : ; ok
    'continuum-removed' : ; ok
  endcase
  self.plotYUnits = units
  if keyword_set(refresh) then begin
    self.library.setPlotDataYByUnits, units
    self.libraryLinkedImageProfiles.setPlotDataYByUnits, units
    self.resetPlot
  endif
end

function hubTBManagedContentSpeclibModel::getPlotXUnits
  return, self.plotXUnits
end

function hubTBManagedContentSpeclibModel::getPlotYUnits
  return, self.plotYUnits
end

pro hubTBManagedContentSpeclibModel::resetPlot
  infRange = [+1,-1]*!VALUES.F_INFINITY
  if ~self.library.isEmpty() then begin 
    xrangeLibrary = self.library.getXRange()
    yrangeLibrary = self.library.getYRange()
  endif else begin
    xrangeLibrary = infRange
    yrangeLibrary = infRange
  endelse

  if ~self.libraryLinkedImageProfiles.isEmpty() then begin
    xrangeLinkedImages = self.libraryLinkedImageProfiles.getXRange()
    yrangeLinkedImages = self.libraryLinkedImageProfiles.getYRange()
  endif else begin
    xrangeLinkedImages = infRange
    yrangeLinkedImages = infRange
  endelse

  if self.library.isEmpty() and self.libraryLinkedImageProfiles.isEmpty() then begin
    xrange = [-1,+1]
    yrange = [-1,+1]
  endif else begin
    xrange = [min([xrangeLibrary[0],xrangeLinkedImages[0]]) , max([xrangeLibrary[1],xrangeLinkedImages[1]])]
    yrange = [min([yrangeLibrary[0],yrangeLinkedImages[0]]) , max([yrangeLibrary[1],yrangeLinkedImages[1]])]
  endelse
    
  xspan = xrange[1] - xrange[0]
  yspan = yrange[1] - yrange[0]
  
  self.setDataXRange, xrange
  self.setDataYRange, yrange
  self.setPlotCenter, [xrange[0]+xspan/2, yrange[0]+yspan/2]
  self.setPlotZoom, plotCenter, 1.
end

pro hubTBManagedContentSpeclibModel::setDataXRange, dataXRange
  self.dataXRange = dataXRange
end

function hubTBManagedContentSpeclibModel::getDataXRange
  return, self.dataXRange
end

pro hubTBManagedContentSpeclibModel::setDataYRange, dataYRange
  self.dataYRange = dataYRange
end

function hubTBManagedContentSpeclibModel::getDataYRange
  return, self.dataYRange
end

pro hubTBManagedContentSpeclibModel::setFixedXRange, range
  self.fixedXRange = range
end

pro hubTBManagedContentSpeclibModel::setFixedYRange, range
  self.fixedYRange = range
end

function hubTBManagedContentSpeclibModel::getFixedXRange
  return, self.fixedXRange
end

function hubTBManagedContentSpeclibModel::getFixedYRange
  return, self.fixedYRange
end

pro hubTBManagedContentSpeclibModel::setPlotCenter, plotCenter, Move=move
  if isa(move) then begin
    self.oViewProfiles.getProperty, VIEWPLANE_RECT=viewplaneRectangle ;[x, y, width, height]
    plotOffset = viewplaneRectangle[0:1]
    plotSize = viewplaneRectangle[2:3]
    plotCenter = plotSize/10.*move + self.plotCenter
    
  endif
  self.plotCenter = plotCenter
  self.adjustPlotCenter
end

pro hubTBManagedContentSpeclibModel::adjustPlotCenter
  hubMath_mathErrorsOff, state
  fixedXRange = self.fixedXRange
  fixedYRange = self.fixedYRange
  dataXRange = self.dataXRange
  dataYRange = self.dataYRange
  dataXRange = [max([dataXRange[0],fixedXRange[0]]), min([dataXRange[1],fixedXRange[1]])]
  dataYRange = [max([dataYRange[0],fixedYRange[0]]), min([dataYRange[1],fixedYRange[1]])]

  dataSize = [dataXRange[1]-dataXRange[0], dataYRange[1]-dataYRange[0]]
  dataOffset = [dataXRange[0],dataYRange[0]]
  plotSize = dataSize/self.plotZoom
  plotCenter = self.plotCenter
  plotOffset = plotCenter-plotSize/2.
  margin = plotSize*0.05

  ; assure that plot stays inside data boundaries
  plotOffset >= dataOffset-margin
  plotOffset <= dataOffset+dataSize-plotSize+margin

  ; save adjusted center
  plotCenter = plotOffset+plotSize/2.
  self.plotCenter = plotCenter
  hubMath_mathErrorsOn, state
end

function hubTBManagedContentSpeclibModel::getPlotCenter
  return, self.plotCenter
end

pro hubTBManagedContentSpeclibModel::setPlotZoom, plotCenter, plotZoom, Increase=increase, Decrease=decrease
  if keyword_set(increase) then begin
    plotZoom = self.plotZoom*1.1
  endif
  if keyword_set(decrease) then begin
    plotZoom = self.plotZoom/1.1
  endif
  self.plotZoom = plotZoom > 0.9
  if isa(plotCenter) then begin
    self.plotCenter = plotCenter
  endif
  self.adjustPlotCenter
end

function hubTBManagedContentSpeclibModel::getPlotZoom
  return, self.plotZoom
end

pro hubTBManagedContentSpeclibModel::setPlotObservation, profile, wavelengthIndex
  ; reset old
;  if obj_valid(self.plotObservationProfile) then begin
;    self.plotObservationProfile.setSelected, 0
;  endif

  ; set new
  if isa(profile) then begin
    self.plotObservationProfile = profile
;    self.plotObservationProfile.setSelected, 1
    self.setPlotObservationPoint, wavelengthIndex
    self.plotObservationProfileSelected = 1b
  endif else begin
    self.plotObservationProfileSelected = 0b
  endelse
end

function hubTBManagedContentSpeclibModel::getPlotObservation
  return, {point:self.plotObservationPoint, profile: self.plotObservationProfile, wavelengthIndex:self.plotObservationWavelengthIndex,$
    profileSelected:self.plotObservationProfileSelected, wavelengthSelected:self.plotObservationWavelengthIndexSelected}
end

pro hubTBManagedContentSpeclibModel::setPlotObservationByWindowPosition, windowSelection, view
  profile = self.getProfileByWindowPosition(windowSelection, view, WavelengthIndex=wavelengthIndex)
  self.setPlotObservation, profile, wavelengthIndex
end

pro hubTBManagedContentSpeclibModel::setPlotObservationPoint, wavelengthIndex, Left=left, Right=right
  if keyword_set(left) then begin
    wavelengthIndex = self.plotObservationWavelengthIndex-1 > 0
  endif
  if keyword_set(right) then begin
    wavelengthIndex = self.plotObservationWavelengthIndex+1
    wavelengthIndex <= self.plotObservationProfile.getNumberOfWavelengths()-1
  endif
  self.plotObservationWavelengthIndexSelected = isa(wavelengthIndex)
  if self.plotObservationWavelengthIndexSelected then begin
    self.plotObservationWavelengthIndex = wavelengthIndex
    self.plotObservationPoint = [(self.plotObservationProfile.getPlotDataX())[wavelengthIndex],( self.plotObservationProfile.getPlotDataY())[wavelengthIndex]]
  endif
end

function hubTBManagedContentSpeclibModel::getPlotSelection, Indices=indices
  return, self.library.getSelection(Indices=indices)
end

pro hubTBManagedContentSpeclibModel::setPlotSelectionByIndices, indices, All=all, Invert=invert

  numberOfProfiles = self.library.getNumberOfProfiles()
  if numberOfProfiles eq 0 then return

  if keyword_set(all) then begin
    indices = lindgen(numberOfProfiles)
  endif
  
  if keyword_set(invert) then begin
    flags = replicate(1b, numberOfProfiles)
    !null = self.getPlotSelection(Indices=selectedIndices)
    flags[selectedIndices] = 0b
    indices = where(/NULL, flags)
  endif
  
  ; empty old selection
  foreach profile, self.getPlotSelection() do begin
    self.setPlotSelectionByProfile, 0b, profile
  endforeach

  ; set new selection
  profiles = self.library.getProfiles()
  foreach index, indices do begin
    self.setPlotSelectionByProfile, 1b, profiles[index]
  endforeach
end

pro hubTBManagedContentSpeclibModel::setPlotSelectionByProfile, isSelected, profile
  profile.setSelected, isSelected
end

pro hubTBManagedContentSpeclibModel::setPlotSelectionByWindowPosition, isSelected, windowSelection, view
  profile = self.getProfileByWindowPosition(windowSelection, view, IsLinkedImageProfile=isLinkedImageProfile)
  if isa(profile) and ~isLinkedImageProfile then begin
    self.setPlotSelectionByProfile, isSelected, profile
  endif
end

pro hubTBManagedContentSpeclibModel::setPlotVisibilityByProfile, isVisible, profile
  profile.setVisible, isVisible
end

function hubTBManagedContentSpeclibModel::getPlotMovementByWindowMovement, windowMovement, view
  dataSize = [self.dataXRange[1]-self.dataXRange[0], self.dataYRange[1]-self.dataYRange[0]]
  plotSize = dataSize/self.plotZoom
  windowSize = view.getSize(/Window)
  plotMovement = windowMovement*plotSize/windowSize
  return, plotMovement
end

function hubTBManagedContentSpeclibModel::getPlotPositionByWindowPosition, windowPosition, view
  status = view.oWindow.PickData(self.oViewProfiles, self.oModel, windowPosition, XYZLocation)
  plotPosition = XYZLocation[0:1]
  return, plotPosition
end

function hubTBManagedContentSpeclibModel::getProfileByWindowPosition, windowSelection, view, WavelengthIndex=wavelengthIndex, IsLinkedImageProfile=isLinkedImageProfile

 ; find nearest profile
  view.oWindow.getProperty, DIMENSION=windowSize
  bufferSize = [1,ceil(windowSize[1]*0.05)]
  selections = view.oWindow.Select(self.oViewProfiles, windowSelection, DIMENSIONS=bufferSize);[11,11])
  profiles = list()
  isLinkedImageProfiles = list() 
  foreach selection, selections do begin
    if ~isa(selection, 'IDLgrPlot') then continue
    if selection eq self.oPlotProfilesCrosshair[0] then continue
    if selection eq self.oPlotProfilesCrosshair[1] then continue
    
    linkedImageProfile = self.libraryLinkedImageProfiles.findProfileByPlot(selection)
    isLinkedImageProfiles.add, isa(linkedImageProfile)
    if isLinkedImageProfiles[-1] then begin
      profiles.add, linkedImageProfile
    endif else begin
      profiles.add, self.library.findProfileByPlot(selection)
    endelse
  endforeach

  if ~profiles.isEmpty() then begin
    plotPosition = self.getPlotPositionByWindowPosition(windowSelection, view)
    distances = list()
    xindices = list()
    foreach profile, profiles do begin
      distances.add, profile.getDistanceToPoint(plotPosition, XIndex=xindex)
      xindices.add, xindex
    endforeach
    distances = distances.toArray()
    xindices = xindices.toArray()
    nearestIndex = (where(distances eq min(distances)))[0]
    nearestProfile = profiles[nearestIndex]
    wavelengthIndex = xindices[nearestIndex]
    isLinkedImageProfile = isLinkedImageProfiles[nearestIndex]
  endif else begin
    isLinkedImageProfile = 0b
    nearestProfile = !null
  endelse
  return, nearestProfile
end

pro hubTBManagedContentSpeclibModel::addProfile, profile
  self.library.addProfile, profile
  profile.setModel, self.oModelProfiles
  profile.setPlotDataXByUnits, self.getPlotXUnits()
  profile.setPlotDataYByUnits, self.getPlotYUnits()
end

pro hubTBManagedContentSpeclibModel::removeProfile, profile, Index=index
  self.library.removeProfile, profile, Index=index
  profile.setModel, /Remove
end

function hubTBManagedContentSpeclibModel::getGraphicView, profiles=profiles
  result = self.oView
  if keyword_Set(profiles) then begin
    result = self.oViewProfiles
  endif
  return, result
end

function hubTBManagedContentSpeclibModel::getLibrary
  return, self.library
end

pro hubTBManagedContentSpeclibModel::setImageLinked, sourceContent, linked
  if ~isa(sourceContent, 'hubTBManagedContentImage') then begin
    message, 'Wrong argument.'
  endif
  
  index = self.linkedImages.where(sourceContent)
  if linked then begin
    if ~isa(index) then begin
      self.linkedImages.add, sourceContent
      imageProfile = self.getLinkedImageProfile(sourceContent)
      profile = imageProfile.copy()
      profile.setColor, [255,0,0]
      profile.setModel, self.oModelProfiles
      profile.setPlotDataXByUnits, self.getPlotXUnits()
      profile.setPlotDataYByUnits, self.getPlotYUnits()
      self.libraryLinkedImageProfiles.addProfile, profile
    endif
  endif else begin
    if isa(index) then begin
      self.linkedImages.remove, index
      self.libraryLinkedImageProfiles.removeProfileByIndex, index
    endif
  endelse
end

function hubTBManagedContentSpeclibModel::getLinkedImageProfile, sourceContent
  profile = sourceContent.controller.getProfile()
  return, profile
end

function hubTBManagedContentSpeclibModel::getLinkedImageIndex, sourceContent
  index = self.linkedImages.where(sourceContent)
  return, index
end

pro hubTBManagedContentSpeclibModel::synchronizeLinkedImageProfile, sourceContent, data, name
  index = self.getLinkedImageIndex(sourceContent)
  profile = self.libraryLinkedImageProfiles.getProfile(index)
  profile.setData, data
  profile.setPlotDataYByUnits, self.getPlotYUnits()
  profile.setName, name
  profile.setColor, [255,0,0]
  self.resetPlot
end

pro hubTBManagedContentSpeclibModel::initLabelingTool
  self.labelingTool = hubTBContentLabelingTool(self.controller.mvc.toolbox, 'speclib')
  self.labelingTool.setSourceContent, self.controller.mvc
end

function hubTBManagedContentSpeclibModel::getLabelingTool
  return, self.labelingTool
end

pro hubTBManagedContentSpeclibModel::showLabelingTool
  self.labelingTool.show
end

pro hubTBManagedContentSpeclibModel::hideLabelingTool
  self.labelingTool.hide
  foreach profile,self.library.getProfiles() do begin
    profile.setColor, [0,0,0]
  endforeach
end

pro hubTBManagedContentSpeclibModel::drawViewToClipboard, view
  plotSize = view.getSize(/Plot)
  oClipboard = IDLgrClipboard(DIMENSIONS=plotSize)
  oClipboard.draw, self.oViewProfiles
  obj_destroy, oClipboard
end

pro hubTBManagedContentSpeclibModel::adjustForViewSpecifica, view
  self.adjustForViewSpecificaOfTheProfiles, view
  self.hubTBManagedContentMVCModel::adjustForViewSpecifica, view
end

pro hubTBManagedContentSpeclibModel::adjustForViewSpecificaOfTheProfiles, view

  hubMath_mathErrorsOff, state
  fixedXRange = self.fixedXRange
  fixedYRange = self.fixedYRange
  dataXRange = self.dataXRange
  dataYRange = self.dataYRange
  dataXRange = [max([dataXRange[0],fixedXRange[0]]), min([dataXRange[1],fixedXRange[1]])]
  dataYRange = [max([dataYRange[0],fixedYRange[0]]), min([dataYRange[1],fixedYRange[1]])] 

  dataSize = [dataXRange[1]-dataXRange[0], dataYRange[1]-dataYRange[0]]
  dataOffset = [dataXRange[0],dataYRange[0]]
  
  plotSize = dataSize/self.plotZoom
  plotOffset = self.plotCenter-plotSize/2.
  plotXRange = plotOffset[0] + [0,plotSize[0]]
  plotYRange = plotOffset[1] + [0,plotSize[1]]

  ; set axis text
  self.oAxisX.getProperty, TITLE=oTextTitleX
  self.oAxisY.getProperty, TITLE=oTextTitleY
  oTextTitleX.setProperty, STRINGS=self.getPlotXUnits()
  oTextTitleY.setProperty, STRINGS=self.getPlotYUnits()+' data'
  
  axisXSize = view.oWindow.GetTextDimensions(self.oAxisX)
  axisYSize = view.oWindow.GetTextDimensions(self.oAxisY)

  plotXMargin = [axisYSize[0],0] + plotSize[0]*0.05
  plotYMargin = [axisXSize[1],0] + plotSize[1]*0.05
 
  viewplaneOffset = plotOffset-[plotXMargin[0], plotYMargin[0]]
  viewplaneSize = plotSize+[total(plotXMargin),total(plotYMargin)]
  viewplaneRectangle = [viewplaneOffset, viewplaneSize]
  frameThick = view.getSize(/FrameThick)
  viewSize = view.getSize(/Plot)
  viewOffset = [frameThick, frameThick]
  self.oViewProfiles.setproperty, VIEWPLANE_RECT=viewplaneRectangle, DIMENSIONS=viewSize, LOCATION=viewOffset

  axisOrigion = plotOffset ;[plotXRange[0], plotYRange[0], 0]
  self.oAxisX.setProperty, RANGE=plotXRange, LOCATION=axisOrigion, TICKLEN=0., /EXACT
  self.oAxisY.setProperty, RANGE=plotYRange, LOCATION=axisOrigion, TICKLEN=0., /EXACT
  
  ; set profile plot ranges
  self.library.setPlotRanges, plotXRange, plotYRange
  self.libraryLinkedImageProfiles.setPlotRanges, plotXRange, plotYRange
  self.libraryLinkedImageProfiles.setPlotZ, 1.
  
  ; set plot colors if labeled
  if obj_valid(self.labelingTool) then begin
    labelingToolModel = self.labelingTool.controller.model
    if labelingToolModel.getNofifyLabeling() then begin
      profiles = self.library.getProfiles()
      roiLayer = labelingToolModel.getROILayer()
      geometries = roiLayer.getGeometries()
      colors = roiLayer.getColors()
      ; set all to default (=black)
      foreach profile,profiles do begin
        profile.setColor, [0,0,0]
      endforeach
      foreach geometry, geometries, i do begin
        foreach index, geometry do begin
          (profiles[index]).setColor, colors[i]
        endforeach
      endforeach
      labelingToolModel.setNofifyLabeling, 0b
    endif
  endif
  
  ; set crosshair
  hideCrosshair = ~(self.plotObservationProfileSelected and self.plotObservationWavelengthIndexSelected)
  self.oPlotProfilesCrosshair[0].setProperty, DATAX=plotXRange, XRANGE=plotXRange, DATAY=replicate(self.plotObservationPoint[1], 2), YRANGE=replicate(self.plotObservationPoint[1], 2), HIDE=hideCrosshair
  self.oPlotProfilesCrosshair[1].setProperty, DATAY=plotYRange, YRANGE=plotYRange, DATAX=replicate(self.plotObservationPoint[0], 2), XRANGE=replicate(self.plotObservationPoint[0], 2), HIDE=hideCrosshair
  hubMath_mathErrorsOn, state
end

pro hubTBManagedContentSpeclibModel::getProperty, _REF_EXTRA=_extra
  self.hubTBManagedContentMVCModel::getProperty, _EXTRA=_extra
 ; if arg_present(image) then image = self.image
end

pro hubTBManagedContentSpeclibModel::removeDependencies
  sourceContents = self.linkedImages + list() ; need to make a copy of the list, because ::setSpeclibLinked will change the list
  foreach sourceContent, sourceContents do begin
    sourceContent.setSpeclibLinked, self.controller.mvc, 0b
  endforeach
end

pro hubTBManagedContentSpeclibModel__define
  define = {hubTBManagedContentSpeclibModel, inherits hubTBManagedContentMVCModel, $
    
    oViewProfiles:obj_new(), $
    oModelProfiles:obj_new(), $
    oPlotProfilesCrosshair:objarr(2), $
    oAxisX:obj_new(), $
    oAxisY:obj_new(), $
    
    library:obj_new(), $
    labelingTool:obj_new(), $
    
    dataXRange:[0.,0.], $
    dataYRange:[0.,0.], $
    fixedXRange:[0.,0.], $
    fixedYRange:[0.,0.], $
    
;    plotXRange:[0.,0.], $
;    plotYRange:[0.,0.], $
    plotCenter:[0.,0.], $
    plotZoom:0., $
    plotObservationProfile:obj_new(), $
    plotObservationProfileSelected:0b, $
    plotObservationPoint:[0.,0.], $
    plotObservationWavelengthIndex:0l, $
    plotObservationWavelengthIndexSelected:0b, $
    plotXUnits:'',$
    plotYUnits:'',$
        
    linkedImages:list(),$
    libraryLinkedImageProfiles:obj_new(), $
    
    contextBaseProfile:0l $      
  }
end