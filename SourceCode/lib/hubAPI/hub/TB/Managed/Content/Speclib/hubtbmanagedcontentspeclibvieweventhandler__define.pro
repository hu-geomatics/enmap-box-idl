function hubTBManagedContentSpeclibViewEventHandler::init, view
  !null = self.hubTBManagedContentMVCViewEventHandler::init(view)
  self.profilesViewLeftMousePanningHandler = hubTBAuxiliaryPanningHandler()
  self.profilesViewMiddleMousePanningHandler = hubTBAuxiliaryPanningHandler()
  return, 1b
end

pro hubTBManagedContentSpeclibViewEventHandler::handleWindowEvent, objEvent
  self.hubTBManagedContentMVCViewEventHandler::handleWindowEvent, objEvent
  
  if objEvent.subcategory eq 'tracking event' then begin
    self.profilesViewLeftMousePanningHandler.finish
    self.profilesViewMiddleMousePanningHandler.finish
    return
  endif
  
  oView = self.view.getSelectedView([objEvent.event.x, objEvent.event.y])
  case oView of
    self.view.controller.getGraphicView(/Profile) : begin
      self.handleWindowProfilesViewEvent, objEvent
    end
    else : ;
  endcase
end

pro hubTBManagedContentSpeclibViewEventHandler::handleWindowProfilesViewEvent, objEvent
  event = objEvent.event
  case objEvent.subsubcategory of
    'middle mouse button pressed event' : self.profilesViewMiddleMousePanningHandler.start, [event.x, event.y]
    'middle mouse button released event' : self.profilesViewMiddleMousePanningHandler.finish
    'motion event' : begin
      self.view.controller.setPlotObservationByWindowPosition, [event.x, event.y], self.view
      self.view.showPermanentMessage, self.view.controller.getFormattedPlotCursorValue()
      if self.profilesViewMiddleMousePanningHandler.isActive() then begin
        mouseMovement = self.profilesViewMiddleMousePanningHandler.getChange([event.x, event.y], InvertY=0)
        plotMovement = self.view.controller.getPlotMovementByWindowMovement(mouseMovement, self.view)
        plotCenter = self.view.controller.getPlotCenter()+plotMovement
        self.view.controller.setPlotCenter, plotCenter
        self.doUpdateView = 1b
      endif
      if self.profilesViewLeftMousePanningHandler.isActive() then begin
        if event.modifiers eq 1 then begin ; [Shift] key
          self.view.controller.setPlotSelectionByWindowPosition, 1b, [event.x, event.y], self.view
        endif
        self.doUpdateView = 1b
        doReturn = 0b
      endif
      self.doUpdateView = 1b
    end
    'left mouse button pressed event' : begin
      if event.modifiers eq 0 then begin ; no modifiers
        self.view.controller.setPlotSelectionByIndices, [] 
        self.view.controller.setPlotSelectionByWindowPosition, 1b, [event.x, event.y], self.view
        if event.clicks eq 2 then begin
          plotSelection = self.view.controller.getPlotPositionByWindowPosition([event.x, event.y], self.view)
          self.view.controller.setPlotCenter, plotSelection
        endif
      endif
      if event.modifiers eq 1 then begin ; [Shift] key
        self.view.controller.setPlotSelectionByWindowPosition, 1b, [event.x, event.y], self.view
      endif
      if event.modifiers eq 2 then begin ; [Strg] key
        self.view.controller.setPlotSelectionByWindowPosition, !null, [event.x, event.y], self.view
      endif
      self.profilesViewLeftMousePanningHandler.start, [0,0]
    end
    'left mouse button released event' : self.profilesViewLeftMousePanningHandler.finish
    'wheel scrolled event' : begin
      ; zoom in/out
      plotCenter = self.view.controller.getPlotPositionByWindowPosition([event.x, event.y], self.view)
      if event.clicks ge 1 then self.view.controller.setPlotZoom, plotCenter, /Increase
      if event.clicks le -1 then self.view.controller.setPlotZoom, plotCenter, /Decrease
    end
    'non-ascii key pressed event' : begin
      if event.modifiers eq 0 then begin ; no modifiers
        case event.key of
          5 : self.view.controller.setPlotCenter, Move=[-1, 0]
          6 : self.view.controller.setPlotCenter, Move=[+1, 0]
          7 : self.view.controller.setPlotCenter, Move=[ 0,+1]
          8 : self.view.controller.setPlotCenter, Move=[ 0,-1]
          else : return
        endcase
      endif      
      if event.modifiers eq 2 then begin ; no modifiers
        widget_control, /HOURGLASS
        case event.key of
          5 : self.view.controller.setPlotObservationPoint, /Left
          6 : self.view.controller.setPlotObservationPoint, /Right
          else : return
        endcase
        self.view.showPermanentMessage, self.view.controller.getFormattedPlotCursorValue()
      endif
    end
    'ascii key released event' : begin
      case event.ch of
        1   : if event.modifiers eq 2 then self.handleSelectAll
        127 : self.handleDeleteProfiles, objEvent
        else : return
      endcase
    end
    'ascii key pressed event' : begin
      case event.ch of
        3 : begin ; [c]: copy to clipboard
          if event.modifiers eq 2 then begin ;[Strg]
            self.view.controller.drawViewToClipboard, self.view
            self.view.showTemporaryMessage, 'Spectral View copied to Clipboard'
          endif
        end
        43 : self.view.controller.setPlotZoom, /Increase
        45 : self.view.controller.setPlotZoom, /Decrease
        else : ;
      endcase
    end
    else : ; print, objEvent.subsubcategory
  endcase
  self.doUpdateView = 1b
end

pro hubTBManagedContentSpeclibViewEventHandler::handleTreeEvent, objEvent
  event = objEvent.event
  case objEvent.subcategory of
    'selection event' : begin
      self.view.controller.setPlotSelectionByTreeSelection, self.view
      self.view.controller.selectView, self.view
;      self.view.showPermanentMessage, self.view.controller.getFormattedPlotCursorValue()
    end
    'checked event' : begin
      widget_control, event.id, GET_UVALUE=objEvent
      profile = objEvent.parameters.profile
      isVisible = objEvent.event.state
      self.view.controller.setPlotVisibilityByProfile, isVisible, profile
      self.view.controller.selectView, self.view
    end
    'context event' : begin
      self.handleWindowContextEvent, [event.x,event.y], event.id, /NoFlip
    end
    'tracking event' : ;    
    else : ; print, objEvent.subcategory
  endcase
  
  ; give keyboard focus to draw widget
  self.view.resetInputFokus
  self.doUpdateView = 1b
end

pro hubTBManagedContentSpeclibViewEventHandler::handleButtonEvent, objEvent
  self.hubTBManagedContentMVCViewEventHandler::handleButtonEvent, objEvent
  case objEvent.name of
    'deleteSelection' : self.handleDeleteProfiles, objEvent
    'invertSelection' : self.handleInvertSelection
    'selectAll' : self.handleSelectAll
    'showSelection' : self.handleShowSelection, 1b
    'hideSelection' : self.handleShowSelection, 0b
    'copySelection' : self.handleCopySelection, objEvent
    'addSelectionToSOI' : self.handleAddSelectionToSOI
    'removeSelectionFromSOI' : self.handleRemoveSelectionFromSOI
    'createSOIforSelection' : self.handleCreateSOIforSelection
    'editPlotSettings' : self.handleEditPlotSettings, objEvent
    'hideLabelingTool': self.view.controller.hideLabelingTool
    'showLabelingTool': self.view.controller.showLabelingTool
    'openEnmapSpeclib' : self.handleOpenEnmapSpeclib, objEvent
    'saveEnmapSpeclib' : self.handleSaveEnmapSpeclib, objEvent
    'importEnviSpeclib' : self.handleImportEnviSpeclib, objEvent
    'exportEnviSpeclib' : self.handleExportEnviSpeclib, objEvent
    else : ;print, objEvent.name
  endcase
  self.doUpdateView = 1b
end

pro hubTBManagedContentSpeclibViewEventHandler::handleDeleteProfiles, objEvent
  profiles = self.view.controller.getPlotSelection()
  answer = dialog_message(/QUESTION, 'Delete profiles?')
  self.view.resetInputFokus
  if answer eq 'No' then return
  widget_control, objEvent.event.top, UPDATE=0 
  foreach profile, profiles do begin
    self.view.controller.removeProfile, profile
  endforeach
  widget_control, objEvent.event.top, UPDATE=1
  self.view.controller.resetPlot
  labelingTool = self.view.controller.getLabelingTool()
  if obj_valid(labelingTool) then begin
    labelingTool.controller.initTableContent, ROIIndex=labelingTool.controller.getRegionSelection()
  endif
end

pro hubTBManagedContentSpeclibViewEventHandler::handleInvertSelection
  self.view.controller.setPlotSelectionByIndices, /Invert
end

pro hubTBManagedContentSpeclibViewEventHandler::handleSelectAll
  self.view.controller.setPlotSelectionByIndices, /All
end

pro hubTBManagedContentSpeclibViewEventHandler::handleShowSelection, show
  foreach profile, self.view.controller.getPlotSelection() do begin
    self.view.controller.setPlotVisibilityByProfile, show, profile
  endforeach
end

pro hubTBManagedContentSpeclibViewEventHandler::handleAddSelectionToSOI
  !null = self.view.controller.getPlotSelection(Indices=indices)
  labelingTool = self.view.controller.getLabelingTool()
  roiIndex = labelingTool.controller.getRegionSelection()
  foreach index,indices do begin
    labelingTool.controller.addPixel, roiIndex, index
  endforeach
  labelingTool.controller.setNofifyLabeling, 1b
end

pro hubTBManagedContentSpeclibViewEventHandler::handleRemoveSelectionFromSOI
  !null = self.view.controller.getPlotSelection(Indices=indices)
  labelingTool = self.view.controller.getLabelingTool()
  roiIndex = labelingTool.controller.getRegionSelection()
  foreach index,indices do begin
    labelingTool.controller.removePixel, roiIndex, index
  endforeach
  labelingTool.controller.setNofifyLabeling, 1b
end

pro hubTBManagedContentSpeclibViewEventHandler::handleCopySelection, objEvent
  profiles = self.view.controller.getPlotSelection()
  if n_elements(profiles) eq 0 then return
  view = self.dialogSelectTargetSpeclibView(objEvent)
  foreach profile,profiles do begin
    newProfile = profile.copy()
    newProfile.setColor, [0,0,0]
    view.addProfile, newProfile
  endforeach
  view.controller.resetPlot
  view.updateView
end

pro hubTBManagedContentSpeclibViewEventHandler::handleEditPlotSettings, objEvent
  
  xrange = self.view.controller.model.getFixedXRange()
  yrange = self.view.controller.model.getFixedYRange()
  xUnits = ['Indices','Values','Nanometers','Micrometers']
  yUnits = ['Normal','Continuum-Removed']
  currentXUnit = self.view.controller.model.getPlotXUnits()
  currentYUnit = self.view.controller.model.getPlotYUnits()
  currentXUnitIndex = where(/NULL, strcmp(/FOLD_CASE, xUnits, currentXUnit))
  currentYUnitIndex = where(/NULL, strcmp(/FOLD_CASE, yUnits, currentYUnit))

  widget_control, objEvent.event.id, GET_VALUE=title
  hubAMW_program, objEvent.event.top, Title=title
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'minX', Title='X Range from', VALUE=xrange[0], /FLOAT, DefaultResult=-!VALUES.F_INFINITY
  hubAMW_parameter, 'maxX', Title='to', VALUE=xrange[1], /FLOAT, DefaultResult=+!VALUES.F_INFINITY
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'minY', Title='Y Range from', VALUE=yrange[0], /FLOAT, DefaultResult=-!VALUES.F_INFINITY
  hubAMW_parameter, 'maxY', Title='to', /FLOAT, VALUE=yrange[1], DefaultResult=+!VALUES.F_INFINITY
  hubAMW_subframe, /COLUMN
  hubAMW_checklist, 'xUnit', TITLE='X Axis', LIST=xUnits, Value=currentXUnitIndex, EXTRACT=strlowcase(xUnits) 
  hubAMW_checklist, 'yUnit', TITLE='Y Axis', LIST=yUnits, Value=currentYUnitIndex, EXTRACT=strlowcase(yUnits)
  
  result = hubAMW_manage(/STRUCTURE)
  if ~result.accept then return
  self.view.controller.model.setFixedXRange, [result.minX, result.maxX]
  self.view.controller.model.setFixedYRange, [result.minY, result.maxY]
  if currentXUnit ne result.xUnit then self.view.controller.model.setPlotXUnits, result.xUnit, /Refresh
  if currentYUnit ne result.yUnit then self.view.controller.model.setPlotYUnits, result.yUnit, /Refresh
  self.view.controller.updateView
end

pro hubTBManagedContentSpeclibViewEventHandler::handleOpenEnmapSpeclib, objEvent
  labelingTool = self.view.controller.getLabelingTool()
  widget_control, objEvent.event.id, GET_VALUE=title
  tsize = 200
  hubAMW_program, objEvent.event.top, Title=title
  hubAMW_inputFilename, 'filenameSpeclib', Title='enmapSLI File', Extension='enmapSLI', TSIZE=tsize
  if obj_valid(labelingTool) then begin
    hubAMW_inputFilename, 'filenameSOI', Title='enmapSOI File', Extension='enmapSOI', OPTIONAL=2, TSIZE=tsize
  endif
  result = hubAMW_manage(/DICTIONARY)
  if result.accept then begin
    self.view.controller.loadEnmapSpeclib, result.filenameSpeclib, result.hubGetValue('filenameSOI')
  endif
end

pro hubTBManagedContentSpeclibViewEventHandler::handleSaveEnmapSpeclib, objEvent
  widget_control, objEvent.event.id, GET_VALUE=title
  hubAMW_program, objEvent.event.top, Title=title
  hubAMW_outputFilename, 'filename', Title='enmapSLI File', VALUE='speclib.enmapSLI', Extension='enmapSLI'
  result = hubAMW_manage(/STRUCTURE)
  if result.accept then begin
    self.view.controller.saveEnmapSpeclib, result.filename
  endif
end

pro hubTBManagedContentSpeclibViewEventHandler::handleImportEnviSpeclib, objEvent
  labelingTool = self.view.controller.getLabelingTool()
  widget_control, objEvent.event.id, GET_VALUE=title
  tsize = 250
  hubAMW_program, objEvent.event.top, Title=title
  hubAMW_inputImageFilename, 'filenameSpeclib', Title='ENVI Speclib', TSIZE=tsize
  if obj_valid(labelingTool) then begin
    hubAMW_inputImageFilename, 'filenameLabel', Title='Label File (pseudo image)', OPTIONAL=2, TSIZE=tsize
  endif

  result = hubAMW_manage(/DICTIONARY)
  if result.accept then begin
    if ~(hubIOImgInputImage(result.filenameSpeclib)).isSpectralLibrary() then begin
      ok = dialog_message(/Error, 'File must by a ENVI Spectral Library File.')
      return
    endif
    self.view.controller.importEnviSpeclib, result.filenameSpeclib, result.hubGetValue('filenameLabel')
  endif
end

pro hubTBManagedContentSpeclibViewEventHandler::handleExportEnviSpeclib, objEvent
  widget_control, objEvent.event.id, GET_VALUE=title
  hubAMW_program, objEvent.event.top, Title=title
  hubAMW_outputFilename, 'filename', Title='ENVI Speclib', VALUE='speclib'
  result = hubAMW_manage(/STRUCTURE)
  if result.accept then begin
    self.view.controller.exportEnviSpeclib, result.filename, GUITitle=title, GuiGroupLeader=objEvent.event.top
  endif
end

pro hubTBManagedContentSpeclibViewEventHandler::handleCreateSOIforSelection
  !null = self.view.controller.getPlotSelection(Indices=indices)
  if ~isa(indices) then return
  labelingTool = self.view.controller.getLabelingTool()
  layerSize = (labelingTool.getROILayer()).getLayerSize()
  geometries = list(indices, /EXTRACT)
  attributeNames = list('name')
  attributeFormats = list('string')
  profileNames = ((self.view.controller.getLibrary()).getProfileNames())[indices]
  attributeTable = list(list(profileNames, /EXTRACT))
  colors = [0,0,0]
  roiLayer = hubROILayer(layerSize, geometries, attributeNames, attributeFormats, attributeTable, Colors=colors) 
  labelingTool.controller.appendROILayer, roiLayer
end

function hubTBManagedContentSpeclibViewEventHandler::dialogSelectTargetSpeclibView, objEvent
  speclibContent = self.view.controller.mvc
  gridManager = speclibContent.controller.getGridFrameManager()
  views = gridManager.getContents(/Speclibs)
  selfIndex = views.where(speclibContent)
  if isa(selfIndex) then begin
    views.remove, selfIndex
  endif
  if views.isEmpty() then begin
    ok = dialog_message(/Error, 'No spectral view available.')
    return, !null
  endif
  names = list()
  foreach view, views do begin
    managedFrame = (view.controller.getFrames())[0]
    names.add, managedFrame.getTitle()
  endforeach
  names = names.toArray()
  
  widget_control,objEvent.event.id, GET_VALUE=title
  hubAMW_program, objEvent.event.top, Title=title
  hubAMW_checklist, 'index', Title='', List=names, Value=0
  result = hubAMW_manage(/STRUCTURE)
  if result.accept then begin
    view = views[result.index[0]]
  endif else begin
    view = !null
  endelse
  return, view
end

pro hubTBManagedContentSpeclibViewEventHandler__define
  define = {hubTBManagedContentSpeclibViewEventHandler, inherits hubTBManagedContentMVCViewEventHandler,$
    profilesViewLeftMousePanningHandler:obj_new(), $
    profilesViewMiddleMousePanningHandler:obj_new() $
    }
end