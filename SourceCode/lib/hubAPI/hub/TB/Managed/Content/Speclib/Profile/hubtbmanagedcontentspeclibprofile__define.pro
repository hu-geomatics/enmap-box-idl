function hubTBManagedContentSpeclibProfile::init, data, wavelength, units, Name=name, DataIgnoreValue=dataIgnoreValue, Color=color
  self.oPlot = IDLgrPlot(/USE_ZVALUE)
  self.setDataIgnoreValue, dataIgnoreValue
  self.setData, data
  self.setWavelength, wavelength
  self.setUnits, units
  self.setName, name
  self.setColor, color
  self.isVisible = 1b
  return, 1b
end

pro hubTBManagedContentSpeclibProfile::setData, data
  if isa(data) then begin
    self.data = ptr_new(data[*])
  endif else begin
    self.data = ptr_new(!null)
  endelse
  self.setPlotDataY, *self.data 
end

function hubTBManagedContentSpeclibProfile::getData
  return, *self.data
end

pro hubTBManagedContentSpeclibProfile::setWavelength, wavelength
  if isa(wavelength) then begin
    wavelength_ = wavelength[*]
  endif else begin
    wavelength_ = lindgen(n_elements(self.getData()))
  endelse
  self.wavelength = ptr_new(wavelength_)
end

function hubTBManagedContentSpeclibProfile::getWavelength
  return, *self.wavelength
end

pro hubTBManagedContentSpeclibProfile::setUnits, units
  if isa(units) then begin
    units_ = strlowcase(units)
    case units_ of
      'nanometers' : ; ok
      'micrometers' : ; ok
      else : units_ = 'values' ; everything else
    endcase
    self.units = units_
  endif else begin
    self.units = 'values'
  endelse
end

function hubTBManagedContentSpeclibProfile::getUnits
  return, self.units
end

pro hubTBManagedContentSpeclibProfile::setDataIgnoreValue, dataIgnoreValue
  self.dataIgnoreValue = ptr_new(dataIgnoreValue)
end

function hubTBManagedContentSpeclibProfile::getDataIgnoreValue
  return, *self.dataIgnoreValue
end

pro hubTBManagedContentSpeclibProfile::setPlotDataX, datax
  if isa(datax) then begin
    ; check if datay is already defined
    self.oPlot.getProperty, DATA=data
    if isa(data) then begin
      self.oPlot.setProperty, DATAX=datax
    endif else begin
      self.oPlot.setProperty, DATAX=datax, DATAY=replicate(0., n_elements(datax)) 
    endelse
  endif
end

pro hubTBManagedContentSpeclibProfile::setPlotDataXByUnits, units
  nativeDataX = self.getWavelength()
  case strlowcase(units) of
    'nanometers' : case self.getUnits() of
                     'nanometers'  : datax = nativeDataX
                     'micrometers' : datax = nativeDataX*1000.
                     'values'      : datax = nativeDataX*!VALUES.F_NAN
                   endcase
    'micrometers' : case self.getUnits() of
                      'nanometers'  : datax = nativeDataX/1000.
                      'micrometers' : datax = nativeDataX
                      'values'      : datax = nativeDataX*!VALUES.F_NAN
                    endcase
    'values': datax = nativeDataX
    'indices': datax = findgen(n_elements(nativeDataX))+1
  endcase
  self.setPlotDataX, datax
end

pro hubTBManagedContentSpeclibProfile::setPlotDataYByUnits, units
  nativeDataY = self.getData()
  case units of
    'normal' : datay = nativeDataY
    'continuum-removed' : datay = hubContinuumRemoval(nativeDataY, self.getWavelength())
  endcase
  self.setPlotDataY, datay
end

function hubTBManagedContentSpeclibProfile::getPlotDataX
  self.oPlot.getProperty, DATA=data
  datax = reform(data[0,*])
  return, datax
end

pro hubTBManagedContentSpeclibProfile::setPlotDataY, datay
  if ~isa(datay) then return
  dataIgnoreValue = self.getDataIgnoreValue()
  plotDatay = double(datay)
  if isa(dataIgnoreValue) then begin
    origDatay = self.getData()
    ignoredIndices = where(/NULL, origDatay eq dataIgnoreValue)
    plotDatay[ignoredIndices] = !VALUES.D_NAN
  endif
  self.oPlot.setProperty, DATAY=plotDatay
end

function hubTBManagedContentSpeclibProfile::getPlotDataY
  self.oPlot.getProperty, DATA=data
  datay = reform(data[1,*])
  return, datay
end

pro hubTBManagedContentSpeclibProfile::setPlotZ, zvalue
  self.oPlot.setProperty, ZVALUE=zvalue
end

pro hubTBManagedContentSpeclibProfile::setName, name
  if isa(name) then self.name = name
end

function hubTBManagedContentSpeclibProfile::getName
  return, self.name
end

pro hubTBManagedContentSpeclibProfile::setColor, color
  if isa(color) then self.color = color
  self.updateColor
end

function hubTBManagedContentSpeclibProfile::getColor
  return, self.color
end

pro hubTBManagedContentSpeclibProfile::setModel, oModel, Remove=remove
  self.oPlot.getProperty, Parent=currentModel
  if obj_valid(currentModel) then begin
    currentModel.remove, self.oPlot
  endif
  if keyword_set(remove) then return
  oModel.add, self.oPlot
end

pro hubTBManagedContentSpeclibProfile::setPlotRanges, xrange, yrange
  self.oPlot.setProperty, XRANGE=xrange, YRANGE=yrange
end

function hubTBManagedContentSpeclibProfile::getPlot
  return, self.oPlot
end

function hubTBManagedContentSpeclibProfile::getDistanceToPoint, point, XIndex=xindex, YIndex=yindex
  x = self.getPlotDataX()
  y = self.getPlotDataY()
  distanceX = abs(x-point[0])
  distanceY = abs(y-point[1])
  xindex = (where(distanceX eq min(distanceX)))[0]
  yindex = (where(distanceY eq min(distanceY)))[0]
  nearestX = x[xindex]
  nearestY = y[yindex]
  nearestProfilePoint = [nearestX, nearestY]
  distance = sqrt(total((nearestProfilePoint-point)^2.))
  return, distance
end

function hubTBManagedContentSpeclibProfile::getNumberOfWavelengths
  return, n_elements(*self.wavelength)
end

pro  hubTBManagedContentSpeclibProfile::setSelected, isSelected
  if ~isa(isSelected) then begin ; swap if undefined
    isSelected = ~self.isSelected
  endif
  self.isSelected = isSelected
  self.updateColor
  self.updateZValue  
end

;pro  hubTBManagedContentSpeclibProfile::setObserved, isObserved
;  self.isObserved = isObserved
;  self.updateColor
;end

pro hubTBManagedContentSpeclibProfile::setVisible, isVisible
  self.isVisible = isVisible
  self.oPlot.setProperty, HIDE=~self.isVisible
end

function hubTBManagedContentSpeclibProfile::isSelected
  return, self.isSelected
end

;function hubTBManagedContentSpeclibProfile::isObserved
;  return, self.isObserved
;end

function hubTBManagedContentSpeclibProfile::isVisible
  return, self.isVisible
end

pro  hubTBManagedContentSpeclibProfile::updateColor
  case 1b of
    self.isSelected : color = [0,0,255]
    else : color = self.color
  endcase
  self.oPlot.setProperty, COLOR=color
end

pro  hubTBManagedContentSpeclibProfile::updateZValue
  case 1b of
    self.isSelected : zvalue = 0.9
    else : zvalue = self.zvalue
  endcase
  self.oPlot.setProperty, ZVALUE=zvalue
end

function hubTBManagedContentSpeclibProfile::copy
  profile = hubTBManagedContentSpeclibProfile(self.getData(), self.getWavelength(), self.getUnits(), Name=self.getName(), DataIgnoreValue=self.getDataIgnoreValue(), Color=self.getColor())
  return, profile
end

function hubTBManagedContentSpeclibProfile::_overloadPrint
  result = self.getName()+' ('+strcompress(/REMOVE_ALL, strjoin(hubRange(self.getWavelength()), '-'))+')'
  return, result
end

pro hubTBManagedContentSpeclibProfile__define
  struct = {hubTBManagedContentSpeclibProfile, inherits IDL_Object,$
    oPlot:obj_new(),$
    wavelength:ptr_new(),$
    data:ptr_new(),$
    units:'', $
    dataIgnoreValue:ptr_new(),$
    name:'',$
    color:[0b,0b,0b],$
    zvalue:0.,$

;    info:hash(), $
    isSelected:0b,$
;    isObserved:0b,$
    isVisible:0b}
end

pro test_hubTBManagedContentSpeclibProfile

  profile = hubTBManagedContentSpeclibProfile(findgen(100)^0.5, findgen(100), 'unknown')
  plot, profile.getWavelength(), profile.getData()
  
end
