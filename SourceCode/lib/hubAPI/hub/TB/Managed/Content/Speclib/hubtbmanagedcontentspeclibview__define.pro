function hubTBManagedContentSpeclibView::init
  self.eventHandler = hubTBManagedContentSpeclibViewEventHandler(self)
  return, 1b
end

pro hubTBManagedContentSpeclibView::create
  case !VERSION.OS_FAMILY of
    'unix' : self.hubTBManagedContentMVCView::create
    else : ;
  endcase
  self.baseTree = widget_base(self.base)
  self.tree = widget_tree(self.baseTree, UVALUE=hubObjCallbackEvent(self.eventHandler, 'tree'), /NO_BITMAPS, /MULTIPLE, /CONTEXT_EVENTS)
  library = self.controller.getLibrary()
  foreach profile, library.getProfiles() do begin
    self.addProfile, profile
  endforeach
  case !VERSION.OS_FAMILY of
    'Windows' : self.hubTBManagedContentMVCView::create
    else : ;
  endcase
end

;pro hubTBManagedContentSpeclibView::createTree
;  self.tree = widget_tree(self.baseTree, UVALUE=hubObjCallbackEvent(self.eventHandler, 'tree'), /NO_BITMAPS, /MULTIPLE, /CONTEXT_EVENTS)
;  library = self.controller.getLibrary()
;  foreach profile, library.getProfiles() do begin
;    self.addProfile, profile
;  endforeach
;end

; needed for an IDL bug workaround
;pro hubTBManagedContentSpeclibView::recreateTree
;  stop
;  widget_control, self.tree, /DESTROY 
;  self.createTree
;end

pro hubTBManagedContentSpeclibView::delete
  self.hubTBManagedContentMVCView::delete
  widget_control, /DESTROY, self.tree
end

pro hubTBManagedContentSpeclibView::createContext
  labelingTool = self.controller.getLabelingTool()
  if obj_valid(labelingTool) then begin
    labelingToolActive = labelingTool.isActive()
  endif else begin
    labelingToolActive = 0b
  endelse


  self.hubTBManagedContentMVCView::createContext
  !null = widget_button(self.contextBase, VALUE='Select all [Ctrl+a]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'selectAll'), /SEPARATOR)
  !null = widget_button(self.contextBase, VALUE='Invert Selection', UVALUE=hubObjCallbackEvent(self.eventHandler, 'invertSelection'))
  submenu = widget_button(self.contextBase, VALUE='Manage Selection', /MENU)
  !null = widget_button(submenu, VALUE='Delete [Delete]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'deleteSelection'))
  !null = widget_button(submenu, VALUE='Copy to Spectral View', UVALUE=hubObjCallbackEvent(self.eventHandler, 'copySelection'))
  !null = widget_button(submenu, VALUE='Show', UVALUE=hubObjCallbackEvent(self.eventHandler, 'showSelection'))
  !null = widget_button(submenu, VALUE='Hide', UVALUE=hubObjCallbackEvent(self.eventHandler, 'hideSelection'))
  if obj_valid(labelingTool) && labelingTool.isActive() then begin
    !null = widget_button(submenu, VALUE='Add to current SOI', UVALUE=hubObjCallbackEvent(self.eventHandler, 'addSelectionToSOI'), /SEPARATOR)
    !null = widget_button(submenu, VALUE='Remove from current SOI', UVALUE=hubObjCallbackEvent(self.eventHandler, 'removeSelectionFromSOI'))
    !null = widget_button(submenu, VALUE='Create one SOI for each', UVALUE=hubObjCallbackEvent(self.eventHandler, 'createSOIforSelection'))
  endif

  !null = widget_button(self.contextBase, VALUE='Edit Plot Settings', UVALUE=hubObjCallbackEvent(self.eventHandler, 'editPlotSettings'), /SEPARATOR)
  if labelingToolActive then begin
    hide = ~labelingTool.isActive()
  endif else begin
    hide = 1
  endelse
  value = (['Show','Hide'])[labelingToolActive]+' Labeling Tool'
  name = (['show','hide'])[labelingToolActive]+'LabelingTool'
  uvalue = hubObjCallbackEvent(self.eventHandler, name, ~labelingToolActive)
  !null = widget_button(self.contextBase, VALUE=value, UVALUE=uvalue)

  !null = widget_button(self.contextBase, VALUE='Open EnMAP-Box Speclib', UVALUE=hubObjCallbackEvent(self.eventHandler, 'openEnmapSpeclib'), /SEPARATOR)
  !null = widget_button(self.contextBase, VALUE='Save EnMAP-Box Speclib', UVALUE=hubObjCallbackEvent(self.eventHandler, 'saveEnmapSpeclib'))

  !null = widget_button(self.contextBase, VALUE='Import Profiles from ENVI Speclib', UVALUE=hubObjCallbackEvent(self.eventHandler, 'importEnviSpeclib'), /SEPARATOR)
  !null = widget_button(self.contextBase, VALUE='Export Profiles to ENVI Speclib', UVALUE=hubObjCallbackEvent(self.eventHandler, 'exportEnviSpeclib'))
 
end

pro hubTBManagedContentSpeclibView::updateSize
  self.hubTBManagedContentMVCView::updateSize
  size = self.getSize()
  frameThick = self.getSize(/FrameThick)
  
  treeSize = [(size[0]/3) < 300  , size[1]-2*frameThick]
  libraryIsEmpty = (self.controller.getLibrary()).isEmpty()
  if libraryIsEmpty then begin
    treeSize[0] = 1
    treeOffset = size+1
  endif else begin
    treeOffset = [size[0]-treeSize[0]-frameThick, frameThick]    
  endelse
  widget_control, self.tree, SCR_XSIZE=treeSize[0]>1, SCR_YSIZE=treeSize[1]>1 ;, XOFFSET=treeOffset[0], YOFFSET=treeOffset[1]
  widget_control, self.baseTree, XOFFSET=treeOffset[0], YOFFSET=treeOffset[1]
end

pro hubTBManagedContentSpeclibView::updateView
  ; check if more tree is visible
  treeXSize = (widget_info(/GEOMETRY, self.tree)).scr_xsize
  treeIsMinimized = treeXSize le 1
  speclibIsEmpty = (self.controller.getLibrary()).isEmpty()
  if (speclibIsEmpty && ~treeIsMinimized) || (~speclibIsEmpty && treeIsMinimized) then begin
    self.updateSize
  endif
  self.hubTBManagedContentMVCView::updateView
end

pro hubTBManagedContentSpeclibView::resetInputFokus
  widget_control, self.windowID, /INPUT_FOCUS
end

function hubTBManagedContentSpeclibView::getSize, Window=window, Tree=tree, Plot=plot, FrameThick=frameThick
  case 1b of
    keyword_set(frameThick) : result = 2
    keyword_set(window) : self.oWindow.getProperty, DIMENSION=result
    keyword_set(tree) : begin
      geometry = widget_info(/GEOMETRY, self.tree)
      result = [geometry.scr_xsize, geometry.scr_ysize]
    end
    keyword_set(plot) : begin
      frameThick = self.getSize(/FrameThick)
      treeSize = self.getSize(/Tree)
      windowSize = self.getSize(/Window)
      result = windowSize-2*frameThick
      result[0] -= treeSize[0]
    end
    else: result = self.hubTBContentMVCView::getSize()
  endcase
  return, result
end

pro hubTBManagedContentSpeclibView::addProfile, profile
  name = profile.getName()
  !null = widget_tree(self.tree, VALUE=name, UVALUE=hubObjCallbackEvent(self.eventHandler, 'tree', {profile:profile}), /CHECKBOX, CHECKED=profile.isVisible())
end

function hubTBManagedContentSpeclibView::findNode, profile
  foreach child, widget_info(/ALL_CHILDREN, self.tree) do begin
    widget_control, child, GET_UVALUE=objEvent
    if objEvent.parameters.profile eq profile then break
  endforeach
  return, child
end

pro hubTBManagedContentSpeclibView::removeProfile, profile
  child = self.findNode(profile)
  widget_control, child, /DESTROY
end

pro hubTBManagedContentSpeclibView::setTreeSelection, indices
;  widget_control, self.tree, UPDATE=0
  widget_control, self.tree, SET_TREE_SELECT=0  
  if isa(indices) then begin
    nodes = widget_info(/ALL_CHILDREN, self.tree)
    selectedNodes = nodes[indices]
    widget_control, self.tree, SET_TREE_SELECT=selectedNodes
    widget_control, selectedNodes[0], /SET_TREE_VISIBLE
  endif
;  widget_control, self.tree, UPDATE=1
end

function hubTBManagedContentSpeclibView::getTreeSelection
  allNodes = widget_info(/ALL_CHILDREN, self.tree)
  allNodesHash = hash(allNodes, indgen(n_elements(allNodes)))
  selectedNodes = widget_info(/TREE_SELECT, self.tree)
  if selectedNodes[0] eq -1 then begin
    return, !null
  endif
  selectedIndices = allNodesHash.hubGetValues(selectedNodes)
  selectedIndices = selectedIndices.toArray()
  return, selectedIndices
end

function hubTBManagedContentSpeclibView::getTreeSelectionProfile
  selection = self.getTreeSelection()
  if isa(selection) then begin
    widget_control, selection, GET_UVALUE=objEvent
    profile = objEvent.parameters.profile
  endif else begin
    profile = !null
  endelse
  return, profile
end

pro hubTBManagedContentSpeclibView::setTreeCheckedByProfile, profile
  child = self.findNode(profile)
  widget_control, child, SET_TREE_CHECKED=profile.isVisible()
end

pro hubTBManagedContentSpeclibView__define
  define = {hubTBManagedContentSpeclibView, inherits hubTBManagedContentMVCView,$
    baseTree:0l,$ ; dummy base for workaround
    tree:0l}
end