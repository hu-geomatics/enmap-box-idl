function test_hubTBManagedContentSpeclibProfileLibrary_getTestLibrary
  inputSpeclib = hubIOImgInputImage(hub_getTestSpeclib('ClassificationSpeclib'))
  dataCube = inputSpeclib.getCube()
  wavelength = inputSpeclib.getMeta('wavelength')
  units = inputSpeclib.getMeta('wavelength units')
  names = inputSpeclib.getMeta('spectra names')
  profileLibrary = hubTBManagedContentSpeclibProfileLibrary()
  for i=0,inputSpeclib.getMeta('lines')-1 do begin
    profile = hubTBManagedContentSpeclibProfile(dataCube[0,i,*], wavelength, units, Name=names[i])
    profile.setPlotDataXByUnits, units
    profileLibrary.addProfile, profile
  endfor
  return, profileLibrary
end