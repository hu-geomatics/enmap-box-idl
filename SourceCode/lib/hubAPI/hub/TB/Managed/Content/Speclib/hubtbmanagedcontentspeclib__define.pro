function hubTBManagedContentSpeclib::init, toolbox, gridFrameManager
  !null = self.hubTBManagedContentMVC::init(toolbox, gridFrameManager)
  self.controller.resetPlot
;  self.controller.showLabelingTool
  return, 1b
end

function hubTBManagedContentSpeclib::createController
  return, hubTBManagedContentSpeclibController()
end

function hubTBManagedContentSpeclib::createModel
  return, hubTBManagedContentSpeclibModel()
end

pro hubTBManagedContentSpeclib::addProfile, profile
  self.controller.addProfile, profile
end

function hubTBManagedContentSpeclib::getSpeclibSize
  return, self.controller.getSpeclibSize()
end

pro hubTBManagedContentSpeclib::showLabelingTool
  self.controller.showLabelingTool
end

pro hubTBManagedContentSpeclib::hideLabelingTool
  self.controller.hideLabelingTool
end

function hubTBManagedContentSpeclib::getLabelingTool
  return, self.controller.getLabelingTool()
end

pro hubTBManagedContentSpeclib__define
  define = {hubTBManagedContentSpeclib, inherits hubTBManagedContentMVC}
end