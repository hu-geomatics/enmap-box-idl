function hubTBManagedContentSpeclibProfileLibrary::init 
  self.profiles = list()
  self.dataRangeOutdated = 1b
  return, 1b
end

pro hubTBManagedContentSpeclibProfileLibrary::addProfile, profile
  if ~isa(profile, 'hubTBManagedContentSpeclibProfile') then begin
    message, 'Wrong argument.''
  endif
  self.profiles.add, profile
  self.dataRangeOutdated = 1b
end

pro hubTBManagedContentSpeclibProfileLibrary::removeProfile, profile, Index=index
  index = self.profiles.where(profile)
  self.removeProfileByIndex, index
end

pro hubTBManagedContentSpeclibProfileLibrary::removeProfileByIndex, index
  if isa(index) then begin
    profile = self.profiles.remove(index)
    profile.setModel, /Remove
    self.dataRangeOutdated = 1b
  endif
end

function hubTBManagedContentSpeclibProfileLibrary::getProfile, index
  return, (self.profiles)[index[0]]
end

function hubTBManagedContentSpeclibProfileLibrary::getProfiles
  return, self.profiles
end

function hubTBManagedContentSpeclibProfileLibrary::getProfileNames
  names = list()
  foreach profile,self.profiles do begin
    names.add, profile.getName()
  endforeach
  return, names.toArray()
end

function hubTBManagedContentSpeclibProfileLibrary::getRange, XRange=xrange, YRange=yrange
  if ~self.dataRangeOutdated then begin
    range = self.dataRange
  endif else begin 
    defaultRange = [-1,+1]
    if self.isEmpty() then begin
      range = defaultRange
    endif else begin
      range = [+1,-1]*!VALUES.F_INFINITY
      foreach profile, self.profiles do begin
        case 1b of
          keyword_set(xrange) : profileRange = hubRange(profile.getPlotDataX())
          keyword_set(yrange) : profileRange = hubRange(profile.getPlotDataY())
        endcase
        if finite(product(profileRange), /NAN) then continue
        range[0] <= profileRange[0]
        range[1] >= profileRange[1]
      endforeach
    endelse
    if range[0] eq range[1] then range += defaultRange
    if ~finite(product(range)) then range = defaultRange
  endelse
  return, range
end

function hubTBManagedContentSpeclibProfileLibrary::getXRange
  return, self.getRange(/XRange)
end

function hubTBManagedContentSpeclibProfileLibrary::getYRange
  return, self.getRange(/YRange)
end

pro hubTBManagedContentSpeclibProfileLibrary::setRangeOutdated
  self.dataRangeOutdated = 1b
end

pro hubTBManagedContentSpeclibProfileLibrary::setPlotRanges, xrange, yrange
  foreach profile, self.profiles do begin
    profile.setPlotRanges, xrange, yrange
  endforeach
end

pro hubTBManagedContentSpeclibProfileLibrary::setPlotZ, zvalue
  foreach profile, self.profiles do begin
    profile.setPlotZ, zvalue
  endforeach
end

pro hubTBManagedContentSpeclibProfileLibrary::setPlotDataXByUnits, units
  foreach profile, self.profiles do begin
    profile.setPlotDataXByUnits, units
  endforeach
end

pro hubTBManagedContentSpeclibProfileLibrary::setPlotDataYByUnits, units
  foreach profile, self.profiles do begin
    profile.setPlotDataYByUnits, units
  endforeach
end

function hubTBManagedContentSpeclibProfileLibrary::whereProfile, profile
  return, self.profiles.where(profile) 
end

function hubTBManagedContentSpeclibProfileLibrary::findProfileByPlot, oPlot
  foreach profile, self.profiles do begin
    if oPlot eq profile.getPlot() then begin
      return, profile
    endif
  endforeach
  return, !null
end

function hubTBManagedContentSpeclibProfileLibrary::getSelection, Indices=indices
  result = list()
  indices = list()
  foreach profile, self.profiles, index do begin
    if profile.isSelected() then begin
      result.add, profile
      indices.add, index
    endif
  endforeach
  result = result.toArray()
  indices = indices.toArray()
  return, result
end

function hubTBManagedContentSpeclibProfileLibrary::getNumberOfProfiles
  return, self.profiles.count()
end

function hubTBManagedContentSpeclibProfileLibrary::isEmpty 
  return, self.profiles.count() eq 0
end



function hubTBManagedContentSpeclibProfileLibrary::_overloadPrint
  result = list()
  foreach profile, self.profiles do begin
    result.add, profile._overloadPrint()
  endforeach
  result = transpose(result.toArray())
  return, result
end

pro hubTBManagedContentSpeclibProfileLibrary__define
  struct = {hubTBManagedContentSpeclibProfileLibrary, inherits IDL_Object,$
    profiles : list(),$
    dataRange : [0d,0d],$
    dataRangeOutdated : 0b}
end

pro test_hubTBManagedContentSpeclibProfileLibrary
  profileLibrary = test_hubTBManagedContentSpeclibProfileLibrary_getTestLibrary()
  print, profileLibrary.getXRange()

end