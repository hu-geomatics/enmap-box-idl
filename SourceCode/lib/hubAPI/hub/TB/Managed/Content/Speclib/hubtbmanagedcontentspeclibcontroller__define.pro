function hubTBManagedContentSpeclibController::newView
  return, hubTBManagedContentSpeclibView()
end

function hubTBManagedContentSpeclibController::getSize, _EXTRA=_extra
  return, self.view.getSize(_EXTRA=_extra)
end

pro hubTBManagedContentSpeclibController::setPlotXUnits, units, Refresh=refresh
  self.model.setPlotXUnits, units, Refresh=refresh
end

pro hubTBManagedContentSpeclibController::setPlotYUnits, units, Refresh=refresh
  self.model.setPlotYUnits, units, Refresh=refresh
end

function hubTBManagedContentSpeclibController::getPlotXUnits
  return, self.model.getPlotXUnits()
end

function hubTBManagedContentSpeclibController::getPlotYUnits
  return, self.model.getPlotYUnits()
end

pro hubTBManagedContentSpeclibController::resetPlot
  self.model.resetPlot
end

pro hubTBManagedContentSpeclibController::setPlotCenter, plotCenter, Move=move
  self.model.setPlotCenter, plotCenter, Move=move
end

function hubTBManagedContentSpeclibController::getPlotCenter
  return, self.model.getPlotCenter()
end

pro hubTBManagedContentSpeclibController::setPlotZoom, plotCenter, plotZoom, Increase=increase, Decrease=decrease
  self.model.setPlotZoom, plotCenter, plotZoom, Increase=increase, Decrease=decrease
end

function hubTBManagedContentSpeclibController::getPlotZoom
  return, self.model.getPlotZoom()
end

function hubTBManagedContentSpeclibController::getPlotObservation
  return, self.model.getPlotObservation()
end

pro hubTBManagedContentSpeclibController::setPlotObservation
  self.model.setPlotObservation
end

pro hubTBManagedContentSpeclibController::setPlotObservationByWindowPosition, windowSelection, view
  self.model.setPlotObservationByWindowPosition, windowSelection, view
end

pro hubTBManagedContentSpeclibController::setPlotObservationPoint, wavelengthIndex, Left=left, Right=right
  self.model.setPlotObservationPoint, wavelengthIndex, Left=left, Right=right
end

function hubTBManagedContentSpeclibController::getPlotSelection, Indices=indices
  return, self.model.getPlotSelection(Indices=indices)
end

pro hubTBManagedContentSpeclibController::setPlotSelectionByIndices, indices, All=all, Invert=invert
  self.model.setPlotSelectionByIndices, indices, All=all, Invert=invert
  self.setTreeSelection
end

pro hubTBManagedContentSpeclibController::setPlotSelectionByTreeSelection, view
  indices = view.getTreeSelection()
  self.model.setPlotSelectionByIndices, indices
;  self.setTreeSelection
end

pro hubTBManagedContentSpeclibController::setPlotSelectionByWindowPosition, isSelected, windowSelection, view
  self.model.setPlotSelectionByWindowPosition, isSelected, windowSelection, view
  self.setTreeSelection
end

pro hubTBManagedContentSpeclibController::setPlotVisibilityByProfile, isVisible, profile 
  self.model.setPlotVisibilityByProfile, isVisible, profile
  foreach view, self.views do begin
    view.setTreeCheckedByProfile, profile
  endforeach
end

function hubTBManagedContentSpeclibController::getPlotMovementByWindowMovement, windowMovement, view
  return, self.model.getPlotMovementByWindowMovement(windowMovement, view)
end

function hubTBManagedContentSpeclibController::getPlotPositionByWindowPosition, windowPosition, view
  return, self.model.getPlotPositionByWindowPosition(windowPosition, view)
end

pro hubTBManagedContentSpeclibController::setTreeSelection
  !null = self.getPlotSelection(Indices=indices)
  foreach view, self.views do begin
    view.setTreeSelection, indices
  endforeach
end

function hubTBManagedContentSpeclibController::getSpeclibSize
  return, self.model.getSpeclibSize()
end

pro hubTBManagedContentSpeclibController::addProfile, profile
  self.model.addProfile, profile
  foreach view, self.views do begin
    view.addProfile, profile
  endforeach
  labelingTool = self.getLabelingTool()
  if obj_valid(labelingTool) then begin
    ; change layer size
    roiLayer = labelingTool.controller.getROILayer()
    roiLayer.setLayerSize, roiLayer.getLayerSize()+[0,1]
  endif
end

pro hubTBManagedContentSpeclibController::removeProfile, profile
  self.model.removeProfile, profile, Index=index
  foreach view, self.views do begin
    view.removeProfile, profile
  endforeach
  labelingTool = self.getLabelingTool()
  if obj_valid(labelingTool) then begin
    roiLayer = labelingTool.controller.getROILayer()
    ; remove profile from SOIs if included
    roiLayer.removePixelFromLayer, index
    ; decrease SOI indices
    roiLayer.decreaseIndices, index
    ; change layer size
    roiLayer.setLayerSize, roiLayer.getLayerSize()+[0,-1]
    labelingTool.controller.setNofifyLabeling, 1b
  endif
end

function hubTBManagedContentSpeclibController::getLibrary
  return, self.model.getLibrary()
end


pro hubTBManagedContentSpeclibController::saveEnmapSpeclib, filename
  profiles = (self.getLibrary()).getProfiles()
  save, FILENAME=filename, profiles
  hubProEnvHelper.openFile, filename
end

pro hubTBManagedContentSpeclibController::loadEnmapSpeclib, filenameSpeclib, filenameSOI
  ; load profiles
  restore, FILENAME=filenameSpeclib
  foreach profile,profiles do begin
    profile.setColor, [0,0,0]
  endforeach

  ; check consistency
  if isa(filenameSOI) then begin
    ; load soiLayer
    soiLayer = hubROILayerReader.readROIFile(filenameSOI)
    ; check speclib and SOILayer size
    if n_elements(profiles) ne (soiLayer.getLayerSize())[1] then begin
      ok = dialog_message(/ERROR, 'Speclib size ['+strtrim(n_elements(profiles),2)+'] is not matching SOI layer size ['+strtrim((soiLayer.getLayerSize())[1],2)+'].')
      return
    endif
    speclibSize = self.getSpeclibSize()
    soiGeometries = soiLayer.getGeometries()
  endif
  
  ; insert profiles
  foreach profile, profiles do begin
    profile.setData, profile.getData()
    self.addProfile, profile
  endforeach

  if isa(filenameSOI) then begin
    ; adjust geometry
    soiLayer.setLayerSize, soiLayer.getLayerSize()+[0,speclibSize]
    foreach geometry,soiGeometries,soiIndex do begin
      if ~isa(geometry) then continue
      soiLayer.setGeometry, soiIndex, geometry+speclibSize
    endforeach
    ; insert SOI layer
    labelingTool = self.getLabelingTool()
    labelingTool.controller.appendROILayer, soiLayer
  endif
  
  
  self.resetPlot
  self.updateView
end

pro hubTBManagedContentSpeclibController::exportENVISpeclib, filename, GuiTitle=guiTitle, GuiGroupLeader=guiGroupLeader
  schemes = list()
  schemeIndices = list()
  profiles = (self.getLibrary()).getProfiles()
  foreach profile, profiles do begin
    currentScheme = dictionary('wavelength', profile.getWavelength(),$
                               'units', profile.getUnits(),$
                               'dataIgnoreValue', profile.getDataIgnoreValue())
    matched = 0b
    foreach scheme, schemes, schemeIndex do begin
      matched = 1b
      matched and= n_elements(scheme.wavelength) eq n_elements(currentScheme.wavelength) 
      matched and= product(scheme.wavelength eq currentScheme.wavelength) eq 1b
      matched and= scheme.units eq currentScheme.units
      matched and= scheme.dataIgnoreValue eq currentScheme.dataIgnoreValue
      if matched then break
    endforeach
    if ~matched then begin
      schemes.add, currentScheme
    endif
    schemeIndices.add, schemeIndex
  endforeach
  
  if schemes.count() ge 2 then begin
    text = list('Multiple definitions found:', '')
    foreach scheme, schemes, i do begin
      text.add, '#'+strtrim(i+1, 2)
      text.add, 'Units: '+scheme.units
      text.add, 'Data Ignore Value: '+scheme.dataIgnoreValue
      text.add, 'Wavelength: '+strjoin(strtrim(scheme.wavelength, 2), ', ')
      text.add, ''
    endforeach
    text = text.toArray()
    hubAMW_program, guiGroupLeader, Title=guiTitle
    size = get_screen_size()
    hubAMW_text, 'text', Value=text, XSize=size[0]*0.5, YSize=size[1]*0.3, /SCROLL
    hubAMW_label, 'Create a separate file for each wavelength definition?'
    result = hubAMW_manage(/STRUCTURE)
    if ~result.accept then return 
  endif

  for i=0, schemes.count()-1 do begin
    scheme = schemes[i]
    indices = schemeIndices.where(i)
    if schemes.count() ge 2 then begin
      outputSpeclib = hubIOSLOutputSpeclib(filename+strtrim(i+1,2))
    endif else begin
      outputSpeclib = hubIOSLOutputSpeclib(filename)
    endelse
    outputSpeclib.setMeta, 'wavelength', scheme.wavelength
    outputSpeclib.setMeta, 'wavelength units', scheme.units
    outputSpeclib.setMeta, 'data ignore value', scheme.dataIgnoreValue
    spectraNames = list()
    foreach profile, profiles[indices] do begin
      spectraNames.add, profile.getName()
      outputSpeclib.writeData, hub_fix2d(profile.getData()) ; array[nb,1]
    endforeach
    outputSpeclib.setMeta, 'spectra names', spectraNames.toArray()
    obj_destroy, outputSpeclib
  endfor
end

pro hubTBManagedContentSpeclibController::importENVISpeclib, filenameSpeclib, filenameLabel
  data = hubIOImg_readImage(filenameSpeclib, /Slice, Header=header)
  wavelength = header.getMeta('wavelength')
  units = header.getMeta('wavelength units')
  names = header.getMeta('spectra names')
  dataIgnoreValue = header.getMeta('data ignore value')
  numberOfProfiles = header.getMeta('lines')

  ; check consistency
  if isa(filenameLabel) then begin
    ; import soiLayer
    soiLayer = hubROILayerReader.importRaster(filenameLabel) 
    ; check speclib and SOILayer size
    if numberOfProfiles ne (soiLayer.getLayerSize())[1] then begin
      ok = dialog_message(/ERROR, 'Speclib size ['+strtrim(numberOfProfiles,2)+'] is not matching label image size ['+strtrim((soiLayer.getLayerSize())[1],2)+'].')
      return
    endif
    speclibSize = self.getSpeclibSize()
    soiGeometries = soiLayer.getGeometries()
  endif

  for i=0,n_elements(data[*,0])-1 do begin
    profile = hubTBManagedContentSpeclibProfile(data[i,*], wavelength, units, Name=names[i], DataIgnoreValue=dataIgnoreValue)
    self.addProfile, profile
  endfor
  
  if isa(filenameLabel) then begin
    ; adjust geometry
    soiLayer.setLayerSize, soiLayer.getLayerSize()+[0,speclibSize]
    foreach geometry,soiGeometries,soiIndex do begin
      if ~isa(geometry) then continue
      soiLayer.setGeometry, soiIndex, geometry+speclibSize
    endforeach
    ; insert SOI layer
    labelingTool = self.getLabelingTool()
    labelingTool.controller.appendROILayer, soiLayer
  endif

  self.resetPlot
  
  self.updateSize
  self.updateView
end

pro hubTBManagedContentSpeclibController::drawViewToClipboard, view
  self.model.drawViewToClipboard, view
end

function hubTBManagedContentSpeclibController::getFormattedPlotCursorValue
  PlotObservation = self.getPlotObservation()
  result = ''
  if plotObservation.profileSelected then begin
    result = 'Name: '+plotObservation.profile.getName()
    if plotObservation.wavelengthSelected then begin
      result = [result,$
      'Wavelength (Index): '+strtrim(plotObservation.point[0],2)+' ('+strtrim(plotObservation.wavelengthIndex+1,2)+')',$
      'Value: '+strtrim(plotObservation.point[1],2)]
    endif
  endif
  return, result
end

pro hubTBManagedContentSpeclibController::showLabelingTool
  self.model.initLabelingTool
  labelingTool = self.model.getLabelingTool()
  tabFrameManager = self.getTabFrameManager()
  labelingToolFrame = tabFrameManager.addContent(labelingTool, /ShowTitleBar, /ShowDetach)
  self.model.showLabelingTool
  labelingToolFrame.updateContent
end

pro hubTBManagedContentSpeclibController::hideLabelingTool
  labelingTool = self.model.getLabelingTool()
  if obj_valid(labelingTool) then begin
    tabFrameManager = self.getTabFrameManager()
    tabFrameManager.removeContent, labelingTool
    self.model.hideLabelingTool
  endif
end

function hubTBManagedContentSpeclibController::getLabelingTool
  return, self.model.getLabelingTool()
end

pro hubTBManagedContentSpeclibController::removeDependencies
  self.hideLabelingTool
  self.model.removeDependencies
end

pro hubTBManagedContentSpeclibController__define
  define = {hubTBManagedContentSpeclibController, inherits hubTBManagedContentMVCController}
end