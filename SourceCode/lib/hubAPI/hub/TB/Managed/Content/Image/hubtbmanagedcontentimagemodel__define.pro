function hubTBManagedContentImageModel::init

  self.tileSize = [256,256]

  ; image
  !null = self.hubTBManagedContentMVCModel::init()
  self.oPlotImageCH[0] = IDLgrPlot(COLOR=[255,0,0],/USE_ZVALUE,ZVALUE=1,THICK=1.0)
  self.oPlotImageCH[1] = IDLgrPlot(COLOR=[255,0,0],/USE_ZVALUE,ZVALUE=1,THICK=1.0)
  self.oPlotImageCH[2] = IDLgrPlot(COLOR=[255,0,0],/USE_ZVALUE,ZVALUE=1,THICK=1.0)
  self.oModel.add, self.oPlotImageCH[0]
  self.oModel.add, self.oPlotImageCH[1]
  self.oModel.add, self.oPlotImageCH[2]
    
  ; overview
  self.oViewOverview = IDLgrView()
  self.oModelOverview = IDLgrModel()
  self.oFrameOverview = IDLgrPlot(COLOR=[0,0,0],/USE_ZVALUE,ZVALUE=1,THICK=4.0)
  self.oModelOverview.add, self.oFrameOverview
;  self.oFrameOverviewVisible = IDLgrPlot(COLOR=[255,255,0],/USE_ZVALUE,ZVALUE=1,THICK=1.0)
  self.oFrameOverviewVisible = IDLgrROI(COLOR=[255,255,0], THICK=1.0)
  self.oModelOverview.add, self.oFrameOverviewVisible
  self.oViewOverview.add, self.oModelOverview
  self.oScene.add, self.oViewOverview
  
  ; spectral
  self.oViewProfile = IDLgrView()
  self.oModelProfile = IDLgrModel()
  self.oFrameProfile = IDLgrPlot(COLOR=[0,0,0],/USE_ZVALUE,ZVALUE=1,THICK=4.0)
  self.oPlotProfile = IDLgrPlot(COLOR=[0,0,0],/USE_ZVALUE,ZVALUE=1,THICK=1.0)
  self.oPlotProfileSelection = IDLgrPlot(COLOR=[0,0,0],/USE_ZVALUE,ZVALUE=1.0, LINESTYLE=2)
  self.oPlotProfileBars[0] = IDLgrPlot(COLOR=[255,0,0],/USE_ZVALUE,ZVALUE=0.9, THICK=1.0)
  self.oPlotProfileBars[1] = IDLgrPlot(COLOR=[0,255,0],/USE_ZVALUE,ZVALUE=0.9, THICK=1.0)
  self.oPlotProfileBars[2] = IDLgrPlot(COLOR=[0,0,255],/USE_ZVALUE,ZVALUE=0.9, THICK=1.0)
  
  self.oModelProfile.add, self.oFrameProfile
  self.oModelProfile.add, self.oPlotProfile
  self.oModelProfile.add, self.oPlotProfileSelection
  self.oModelProfile.add, self.oPlotProfileBars[0]
  self.oModelProfile.add, self.oPlotProfileBars[1]
  self.oModelProfile.add, self.oPlotProfileBars[2]
  self.oViewProfile.add, self.oModelProfile
  self.oScene.add, self.oViewProfile
  
  self.linkedSpeclibs = list()
  return, 1b
end

pro hubTBManagedContentImageModel::initImageGraphics

  ; image
  obj_destroy, self.oImageImage
  if ~isa(self.oImageImagePalette) then begin
    self.oImageImagePalette = IDLgrPalette()
  endif

  imageSize = self.image.getSpatialSize()
  self.oImageImage = IDLgrImage(INTERLEAVE=2, ORDER=1, INTERPOLATE=0, TILING=1, TILE_LEVEL_MODE=1, TILE_DIMENSIONS=self.tileSize, DEPTH_TEST_DISABLE=2, PALETTE=self.oImageImagePalette)
  self.oImageImage.setProperty, TILED_IMAGE_DIMENSIONS=imageSize, LOCATION=[0,0], DIMENSIONS=imageSize
  self.oModel.add, self.oImageImage 
  
;  ; overview
  obj_destroy, self.oImageOverview
  self.oImageOverview = IDLgrImage(INTERLEAVE=2, ORDER=1, INTERPOLATE=0, DEPTH_TEST_DISABLE=2, PALETTE=self.oImageImagePalette)
  self.oModelOverview.add, self.oImageOverview
end

pro hubTBManagedContentImageModel::initLabelingTool
  self.labelingTool = hubTBContentLabelingTool(self.controller.mvc.toolbox, 'image')
  self.labelingTool.setSourceContent, self.controller.mvc
end

function hubTBManagedContentImageModel::getLabelingTool
  return, self.labelingTool
end

pro hubTBManagedContentImageModel::showLabelingTool
  self.labelingTool.show
end

pro hubTBManagedContentImageModel::hideLabelingTool
  self.labelingTool.hide
end

pro hubTBManagedContentImageModel::showProfileSelection
  self.oPlotProfileSelection.setProperty, HIDE=0
end

pro hubTBManagedContentImageModel::hideProfileSelection
  self.oPlotProfileSelection.setProperty, HIDE=1
end

function hubTBManagedContentImageModel::getGraphicView, overview=overview, profile=profile
  result = self.oView
  if keyword_Set(overview) then begin
    result = self.oViewOverview
  endif
  if keyword_Set(profile) then begin
    result = self.oViewProfile
  endif
  return, result
end

pro hubTBManagedContentImageModel::setFilename, filename
  self.setImage, hubIOImgInputImage(filename)
end

pro hubTBManagedContentImageModel::setImage, image
  self.image = image
  filename = self.image.getMeta('filename data')
  self.pyramid = hubIOImgImagePyramid(filename, self.tileSize)
  wavelength = image.getMeta('wavelength', Default=lindgen(image.getMeta('bands')))
  wavelengthUnits = image.getMeta('wavelength units')
  dataIgnoreValue = image.getMeta('data ignore value')
  self.profile = hubTBManagedContentSpeclibProfile(!null, wavelength, wavelengthUnits, DataIgnoreValue=dataIgnoreValue)
  self.profile.setPlotDataXByUnits, self.profile.getUnits()
  self.title = file_basename(filename)
  self.imageSize = image.getSpatialSize()
  self.setImageSelection, [-1,-1]
end

pro hubTBManagedContentImageModel::setBandRGB, bands
  self.bands = ptr_new(bands)
  progressBar = hubProgressBar(Title='Create Image Pyramid', GROUPLEADER=self.controller.getTopLevelBase())
  foreach band, bands, i do begin
    progressBar.setProgress, (i+1.)/n_elements(bands)
    self.pyramid.write, band
;todo self.print, 'pyramid written for band'+strcompress(i)+', '+self.image.getMeta('filename data')
  endforeach
  obj_destroy, progressBar
  self.bandType = 'rgb'
  self.numberOfChannels = 3
  self.initImageGraphics
  self.loadOverviewData
end

pro hubTBManagedContentImageModel::setBandGrey, band
  self.bands = ptr_new(band)
  progressBar = hubProgressBar(Title='Create Image Pyramid', GROUPLEADER=self.controller.getTopLevelBase())
  self.pyramid.write, band
  obj_destroy, progressBar
  self.bandType = 'grey'
  self.numberOfChannels = 1  
  self.oImageImagePalette = IDLgrPalette()
  self.oImageImagePalette.loadct, 0
  self.initImageGraphics
  self.loadOverviewData
end

pro hubTBManagedContentImageModel::setBandClassification
  self.bands = ptr_new(0)
  progressBar = hubProgressBar(Title='Create Image Pyramid', GROUPLEADER=self.controller.getTopLevelBase())
  self.pyramid.write, 0
  obj_destroy, progressBar
  self.bandType = 'classification'
  self.numberOfChannels = 1
  classLookup = self.image.getMeta('class lookup')
  self.oImageImagePalette = IDLgrPalette()
  self.oImageImagePalette.setProperty, RED_VALUES=classLookup[0,*], GREEN_VALUES=classLookup[1,*], BLUE_VALUES=classLookup[2,*]
  self.initImageGraphics
  self.loadOverviewData
end


function hubTBManagedContentImageModel::getBands
  return, (*self.bands)[*]
end

pro hubTBManagedContentImageModel::setImageCenter, imageCenter, ImageSelection=imageSelection

  _imageCenter = isa(imageCenter) ? imageCenter : !null
  if keyword_set(imageSelection) then begin
    _imageCenter = self.imageSelection
  endif
  if ~isa(_imageCenter) then begin
    _imageCenter = self.image.getSpatialSize()/2.
  endif
  if n_elements(_imageCenter) eq 1 then begin
    _imageCenter = array_indices(/DIMENSIONS, self.imageSize, _imageCenter)
  endif
  self.imageCenter = _imageCenter
end

function hubTBManagedContentImageModel::getImageCenter
  return, self.imageCenter
end


pro hubTBManagedContentImageModel::setImageZoom, zoomFactor,$
  Increase=increase, Decrease=decrease, SceneFull=sceneFull, SceneWidth=sceneWidth, SceneHeight=sceneHeight, PixelSize=pixelSize, ImageView=imageView
  
  if keyword_set(increase) then begin
    zoomFactor = self.imageZoom*1.2
  endif
  if keyword_set(decrease) then begin
    zoomFactor = self.imageZoom/1.2
  endif
  if keyword_set(pixelSize) then begin
    zoomFactor = 1.
  endif
  if keyword_set(sceneFull) || keyword_set(sceneWidth) || keyword_set(sceneHeight) then begin
    if ~isa(imageView, 'hubTBManagedContentMVCView') then begin
      message, 'Wrong argument.'
    endif
    imageView.oWindow.getproperty, DIMENSION=windowSize
    self.setImageCenter
    zoomFactors = float(windowSize)/self.imageSize
    case 1 of
      keyword_set(sceneFull)   : zoomFactor = min(zoomFactors) 
      keyword_set(sceneWidth)  : zoomFactor = zoomFactors[0]
      keyword_set(sceneHeight) : zoomFactor = zoomFactors[1]
    endcase
  endif
 
  self.imageZoom = zoomFactor
  
  if ~self.pyramid.buildPyramid and self.imageZoom lt 1.0 then begin
    allPyramidsWritten = 1b
    bands = self.getBands()
    foreach band, bands do begin
      allPyramidsWritten and= self.pyramid.isWritten(band)
    endforeach
    if ~allPyramidsWritten then begin
      ok = dialog_message(/QUESTION, Title='Image Pyramid Calculation',$
        ['The image exceeds the large image threshold of '+strcompress(/REMOVE_ALL, self.pyramid.bigFileThreshold)+' pixels. Due to computation time, image pyramids are not created for large images by default. This enables fast image visualization but constrains zooming levels to zoom factors 1.0 or higher.',$
        '','Do you want to build the image pyramid for the selected bands to enable the full visualization capability?'])
      if ok eq 'Yes' then begin
        progressBar = hubProgressBar(Title='Create Image Pyramid', GROUPLEADER=self.controller.getTopLevelBase())
        self.pyramid.buildPyramid = 1b
        ; build current band selection
        foreach band, bands, i do begin
          progressBar.setProgress, (i+1.)/n_elements(bands)
          self.pyramid.write, band
        endforeach
        obj_destroy, progressBar
      endif else begin
        self.imageZoom = self.imageZoom > 1
      endelse
    endif
  endif
end

function hubTBManagedContentImageModel::getImageZoom
  return, self.imageZoom
end

pro hubTBManagedContentImageModel::setImageStretch, imageStretch, Percentage=percentage, $
  Increase=increase, Decrease=decrease, NoSaveViewExtentPercentage=noSaveViewExtentPercentage
  
  if keyword_set(increase) then begin
    percentageStretch = self.getImageStretchPercentage(/Symmetric)
    percentageStretch[0]++
    percentageStretch[0] <= 49
    percentageStretch[1]--
    percentageStretch[1] >= 51
    self.setImageStretch, percentageStretch, /Percentage
    return
  endif
  
  if keyword_set(decrease) then begin
    percentageStretch = self.getImageStretchPercentage(/Symmetric)
    percentageStretch[0]--
    percentageStretch[0] >= 0
    percentageStretch[1]++
    percentageStretch[1] <= 100
    self.setImageStretch, percentageStretch, /Percentage
    return
  endif
  
  imageStretch = rebin(imageStretch, 2, self.numberOfChannels)
  if keyword_set(percentage) then begin
    for i=0, self.numberOfChannels-1 do begin
      percentiles = self.pyramid.getPercentiles((*self.bands)[i])
      self.imageStretch[*,i] = percentiles[imageStretch[*,i]]
    endfor
  endif else begin
    self.imageStretch = imageStretch
  endelse
  
  self.initImageGraphics
  self.loadOverviewData
  
  ; set current image stretch percentage to view stretch percentage
  if ~keyword_set(noSaveViewExtentPercentage) then begin
    self.setImageStretchPercentageOnViewExtent, self.getImageStretchPercentage()
  endif
  
  ; print to console
;  if self.numberOfChannels eq 1 then begin
;    text =  '['+strjoin(strtrim(imageStretch[*,0], 2)+(keyword_set(percentage)?'%':''), ',')+']'
;  endif else begin
;    text =  'R: ['+strjoin(strtrim(imageStretch[*,0], 2)+(keyword_set(percentage)?'%':''), ',')+']'
;    text += ', G: ['+strjoin(strtrim(imageStretch[*,1], 2)+(keyword_set(percentage)?'%':''), ',')+']'
;    text += 'and B: ['+strjoin(strtrim(imageStretch[*,2], 2)+(keyword_set(percentage)?'%':''), ',')+']'
;  endelse
  ;keyword_set(percentage) ? '%'))
;  self.print, 'image stretch set to '+text, NoLog=noLog
  
  
end

pro hubTBManagedContentImageModel::setImageStretchOnViewExtent, view, percentageStretch, $
  Increase=increase, Decrease=decrease
  
  if keyword_set(increase) then begin
    percentageStretch = self.getImageStretchPercentageOnViewExtent()
    percentageStretch[0]++
    percentageStretch[0] <= 49
    percentageStretch[1]--
    percentageStretch[1] >= 51
    self.setImageStretchOnViewExtent, view, percentageStretch
    return
  endif
  
  if keyword_set(decrease) then begin
    percentageStretch = self.getImageStretchPercentageOnViewExtent()
    percentageStretch[0]--
    percentageStretch[0] >= 0
    percentageStretch[1]++
    percentageStretch[1] <= 100
    self.setImageStretchOnViewExtent, view, percentageStretch 
    return
  endif
  

  ; create new window-buffer
  
  view.oWindow.getProperty, DIMENSIONS=dimensions
  oWindow = IDLgrBuffer(DIMENSIONS=dimensions)
  
  ; hide vector graphics
  graphics = list(self.oFrame, self.oPlotImageCH, /EXTRACT)
  foreach graphic, graphics do graphic.setProperty, Hide=1
  
  ; redraw image with 0-100% stretching
  self.setImageStretch, [0,100], /Percentage, /NoSaveViewExtentPercentage
  imageStretch100Percent = self.getImageStretch()
  self.loadImageTileData, oWindow
  oWindow.draw, self.oView
  
  ; show vector graphics
  foreach graphic, graphics do graphic.setProperty, Hide=0
  
  ; read rgb data
  oImage = oWindow.read()
  oImage.getProperty, Data=rgbData ; [3,samples,lines]

  ; delete buffer
  obj_destroy, oWindow
  
  ; calculate rgb percentiles and recontruct original image data percentiles for the view extent
  rgbPercentiles = list()
  for i=0,2 do begin
    histogram = hubMathHistogram(rgbData[i,*,*], [0,255], 256, InPercentileRanks=percentageStretch, OutPercentiles=percentiles)
    rgbPercentiles.add, percentiles
  endfor
  newImageStretch = dblarr(2,3)
  for i=0,2 do begin
    rangeI = imageStretch100Percent[1,i]-imageStretch100Percent[0,i]
    minI = imageStretch100Percent[0,i]
    newImageStretch[*,i] = rgbPercentiles[i]/255*rangeI+minI
  endfor

  ; set new image stretch and redraw
  self.setImageStretch, newImageStretch, /NoSaveViewExtentPercentage
  
  ; save percentage
  self.setImageStretchPercentageOnViewExtent, percentageStretch
  
end

function hubTBManagedContentImageModel::getImageStretch
  return, self.imageStretch
end

function hubTBManagedContentImageModel::getImageStretchPercentage, Symmetric=symmetric

  percentageStretch = intarr(2, self.numberOfChannels)
  for i=0,self.numberOfChannels-1 do begin
    percentiles = self.pyramid.getPercentiles((*self.bands)[i])
    diffMin = abs(percentiles-self.imageStretch[0,i])
    diffMax = abs(percentiles-self.imageStretch[1,i])
    percentMin = (where(diffMin eq min(diffMin)))[0]
    percentMax = (where(diffMax eq min(diffMax)))[-1]
    percentageStretch[*,i] = [percentMin, percentMax]
  endfor
  percentageStretch =[min(percentageStretch[0,*]), max(percentageStretch[1,*])]
  if keyword_set(symmetric) then begin
    percentageStretch = [percentageStretch[0],100-percentageStretch[0]]
  endif
  return, percentageStretch
end

pro hubTBManagedContentImageModel::setImageStretchPercentageOnViewExtent, percentageStretch
  self.imageStretchPercentOnViewExtent = percentageStretch
end

function hubTBManagedContentImageModel::getImageStretchPercentageOnViewExtent
  return, self.imageStretchPercentOnViewExtent
end

pro hubTBManagedContentImageModel::setImageSelection, imageSelection, Move=move, PixelCenter=pixelCenter, SynchronizeLinkedSpeclibs=synchronizeLinkedSpeclibs

  _imageSelection = isa(imageSelection) ? imageSelection : !null
 
  if isa(move) then begin
    _imageSelection = floor(self.imageSelection)+0.5+move
    _imageSelection >= 0.5
    _imageSelection <= self.imageSize-0.5
  endif
  
  if ~isa(_imageSelection) then begin
    _imageSelection = self.image.getSpatialSize()/2.
  endif
  
  if n_elements(_imageSelection) eq 1 then begin
    _imageSelection = array_indices(/DIMENSIONS, self.imageSize, _imageSelection)
  endif
  
  if keyword_set(pixelCenter) then begin
    _imageSelection = floor(_imageSelection)+0.5
  endif
  
  self.imageSelection = _imageSelection
;  self.profile.setData, self.image.getProfile(floor(self.imageSelection[0]), floor(self.imageSelection[1]))
  self.profile.setData, self.image.getProfile(hub_fix2d(floor(self.imageSelection)))

  name = '['+strjoin(strcompress(/REMOVE_ALL,floor(self.imageSelection)+1),',')+']'+file_basename(self.image.getMeta('filename data'))
  self.profile.setName, name
  
  self.hideProfileSelection
  
  if keyword_Set(synchronizeLinkedSpeclibs) then begin
    self.synchronizeLinkedSpeclibs
  endif
end

function hubTBManagedContentImageModel::getImageSelection, IsInsideImage=isInsideImage
  if arg_present(isInsideImage) then begin
    isInsideImage = product([self.imageSelection ge 0, self.imageSelection lt self.imageSize])
  endif
  return, self.imageSelection
end

pro hubTBManagedContentImageModel::setImageSelectionByWindowSelection, windowSelection
  self.oView.getproperty, VIEWPLANE_RECT=viewplaneRectangle
  imageSelection = viewplaneRectangle[0:1]+windowSelection/self.imageZoom
  imageSelection[1] = (self.image.getSpatialSize())[1]-imageSelection[1]
  self.setImageSelection, imageSelection
end

pro hubTBManagedContentImageModel::setProfileSelection, point, index
  self.profileSelectionPoint = point
  self.profileSelectionIndex = index
end

pro hubTBManagedContentImageModel::setProfileSelectionByWindowSelection, windowSelection, view
  pickStatus = view.oWindow.PickData(self.oViewProfile, self.oPlotProfile, windowSelection, XYZLocation)
;  xs = self.profile.getWavelength()
;  ys = self.profile.getData()
  xs = self.profile.getPlotDataX()
  ys = self.profile.getPlotDataY()

  distXs = abs(xs-XYZLocation[0])
  index = (where(distXs eq min(distXs)))[0] 
  x = xs[index]
  y = ys[index]
  self.setProfileSelection, [x,y], index
  self.showProfileSelection
end

function hubTBManagedContentImageModel::getProfileSelectionPoint, Index=index
  index = self.profileSelectionIndex
  return, self.profileSelectionPoint
end

pro hubTBManagedContentImageModel::setImageLink, imagePositionLinked, imageZoomLinked
  self.imagePositionLinked = imagePositionLinked
  self.imageZoomLinked = imageZoomLinked
end

function hubTBManagedContentImageModel::getImagePositionLinked
  return, self.imagePositionLinked
end

function hubTBManagedContentImageModel::getImageZoomLinked
  return, self.imageZoomLinked
end

pro hubTBManagedContentImageModel::setSpeclibLinked, speclibContent, linked
  if ~isa(speclibContent, 'hubTBManagedContentSpeclib') then begin
    message, 'Wrong argument.'
  endif

  index = self.linkedSpeclibs.where(speclibContent)
  if linked then begin
    if ~isa(index) then self.linkedSpeclibs.add, speclibContent
  endif else begin
    if isa(index) then self.linkedSpeclibs.remove, index
  endelse
  speclibContent.controller.model.setImageLinked, self.controller.mvc, linked
  speclibContent.controller.resetPlot
  speclibContent.controller.updateView
end

function hubTBManagedContentImageModel::getSpeclibLinked, speclibContent
  return, isa(self.linkedSpeclibs.where(speclibContent))
end

pro hubTBManagedContentImageModel::synchronizeLinkedSpeclibs
  if self.linkedSpeclibs.isEmpty() then return
  
  data = self.profile.getData()
  name = self.profile.getName()
  foreach speclibContent, self.linkedSpeclibs do begin
    speclibContent.controller.model.synchronizeLinkedImageProfile, self.controller.mvc, data, name
    speclibContent.controller.updateView
  endforeach
end

pro hubTBManagedContentImageModel::copyProfileToLinkedSpeclibs
  if self.linkedSpeclibs.isEmpty() then return
  profile = self.profile.copy()
  foreach speclibContent, self.linkedSpeclibs do begin
    speclibContent.controller.addProfile, profile
    speclibContent.controller.updateView
  endforeach
end

function hubTBManagedContentImageModel::convertPixelToGeo, pixelPosition
  return, self.image.getMapCoordinates(pixelPosition)
;
;  mapInfo = self.image.getMeta('map info')
;  pixelSize = [mapInfo.sizeX, mapInfo.sizeY]
;  tieMap = [mapInfo.easting, mapInfo.northing]
;  tiePixel = [mapInfo.pixelX, mapInfo.pixelY]
;  geoPosition = tieMap+pixelSize*[+1,-1]*(pixelPosition-tiePixel)
;  return, geoPosition
end

function hubTBManagedContentImageModel::convertGeoToPixel, geoPosition
  return, self.image.getPixelCoordinates(geoPosition)
;
;  mapInfo = self.image.getMeta('map info')
;  pixelSize = [mapInfo.sizeX, mapInfo.sizeY]
;  tieMap = [mapInfo.easting, mapInfo.northing]
;  tiePixel = [mapInfo.pixelX, mapInfo.pixelY]
;  pixelPosition = float((geoPosition-tieMap)/(pixelSize*[+1,-1])+tiePixel)
;  return, pixelPosition
end

pro hubTBManagedContentImageModel::loadImageTileData, oWindow

  requiredTiles = oWindow.queryRequiredTiles(self.oView, self.oImageImage, COUNT=numberOfTiles)
  if numberOfTiles eq 0 then return
  widget_control,/HOURGLASS
  
  ;loading rows of tiles
  cnt=0
  while numberOfTiles gt 0 do begin
    tileInfo = requiredTiles[0]
    level = tileInfo.level
    width = (self.image.getSpatialSize())[0]/(2L^level)
    height = tileInfo.height
    tileInfo.x = 0L
    tileInfo.width = width
    tileData = make_array(TYPE=1, width, height, self.numberOfChannels, /NOZERO)

    foreach band,*self.bands,i do begin
      subsetLines = [tileInfo.y, tileInfo.y+height-1]
      subsetSamples = [0, width-1]
      tileDataBand = self.pyramid.read(band, level, subsetLines, subsetSamples)
      if self.bandType ne 'classification' then begin
        tileDataBand = bytscl(tileDataBand, Min=self.imageStretch[0,i], Max=self.imageStretch[1,i])
      endif
      tileData[0,0,i] = temporary(tileDataBand)
    endforeach
    self.oImageImage.setTileData, tileInfo, tileData
    requiredTiles = oWindow.queryRequiredTiles(self.oView, self.oImageImage, COUNT=numberOfTiles)
  endwhile
  
end

pro hubTBManagedContentImageModel::loadOverviewData
  widget_control,/HOURGLASS

;catch, errorStatus
;  if (errorStatus ne 0) then begin
;    catch, /CANCEL
;    help, /LAST_MESSAGE
;stop
;  endif
  
  ; load overview data
  overviewSize = self.getOverviewDataSize()
  overviewData = hub_fix3d(make_array(TYPE=1, overviewSize[0], overviewSize[1], self.numberOfChannels, /NOZERO))
  foreach band,*self.bands,i do begin
    overviewDataBand = self.pyramid.readOverview(band)
    if self.bandType ne 'classification' then begin
      overviewDataBand = bytscl(overviewDataBand, Min=self.imageStretch[0,i], Max=self.imageStretch[1,i])
    endif
    overviewData[0,0,i] = temporary(overviewDataBand)
  endforeach
  self.oImageOverview.setProperty, Data=overviewData
end

function hubTBManagedContentImageModel::getOverviewDataSize
  return, self.pyramid.getOverviewSize()
end

function hubTBManagedContentImageModel::getImageSize
  return, self.imageSize
end

pro hubTBManagedContentImageModel::adjustForViewSpecifica, view
  hubMath_mathErrorsOff, state
  self.adjustForViewSpecificaOfTheImage, view
  self.adjustForViewSpecificaOfTheOverview, view
  self.adjustForViewSpecificaOfTheProfile, view
  hubMath_mathErrorsOn, state
;  self.lastAdjustedView = view
  self.hubTBManagedContentMVCModel::adjustForViewSpecifica, view
end

pro hubTBManagedContentImageModel::adjustForViewSpecificaOfTheImage, view

  ; set viewplane rectangle
  view.oWindow.getProperty, DIMENSION=windowSize
  imageCenter = self.imageCenter
  imageCenter[1] = self.imageSize[1]-imageCenter[1] ; flip y-axis
  viewplaneOffset = imageCenter-windowSize/self.imageZoom/2
  viewplaneSize = windowSize/self.imageZoom
  viewplaneRectangle = [viewplaneOffset, viewplaneSize]
  self.oView.setproperty, VIEWPLANE_RECT=viewplaneRectangle, LOCATION=[0,0], DIMENSIONS=windowSize
  
  ; set crosshairs
  imageCenter = self.imageCenter
  self.oPlotImageCH[0].setproperty, DATAX=[viewplaneRectangle[0],viewplaneRectangle[0]+viewplaneRectangle[2]],$
    DATAY=self.imageSize[1]-[self.imageSelection[1],self.imageSelection[1]]
  self.oPlotImageCH[1].setproperty, DATAX=[self.imageSelection[0],self.imageSelection[0]],$
    DATAY=[viewplaneRectangle[1],viewplaneRectangle[1]+viewplaneRectangle[3]]
  
  ; set roi symbol size
;  self.labelingTool.oSymbol.setProperty, Size=self.imageZoom
  
  ; load tile data
  self.loadImageTileData, view.oWindow
  
end

pro hubTBManagedContentImageModel::adjustForViewSpecificaOfTheOverview, view

  ; get image view size
  self.oView.getProperty, VIEWPLANE_RECT=viewplaneRectangle
  viewplaneOffset = viewplaneRectangle[0:1]
  viewplaneSize = viewplaneRectangle[2:3]

  ; set overview view size
  view.oWindow.getProperty, DIMENSION=windowSize
  dataSize = self.getOverviewDataSize()
  dataMaxSize = windowSize/2.
  scaleFactor = max(dataSize/dataMaxSize)>1
  viewplaneSizeOverview = dataSize/scaleFactor
  viewplaneRectangleOverview = [[0,0], viewplaneSizeOverview]
  dimensions = viewplaneSizeOverview+[2,0]
  self.oViewOverview.setProperty, VIEWPLANE_RECT=viewplaneRectangleOverview, DIMENSIONS=dimensions

  ; set overview image size 
  self.oImageOverview.setProperty, DIMENSIONS=viewplaneSizeOverview
  
  ; draw visible region frame
  zoomFactor = max(viewplaneSizeOverview/self.imageSize)
  viewplaneOffset *= zoomFactor
  viewplaneSize *= zoomFactor
  framePoints = [[viewplaneOffset], [viewplaneOffset+(viewplaneSize-1)*[0,1]], [viewplaneOffset+(viewplaneSize-1)*[1,1]], [viewplaneOffset+(viewplaneSize-1)*[1,0]], [viewplaneOffset]]
  self.oFrameOverviewVisible.setProperty, DATA=framePoints, THICK=1

  
  ; draw frame
  self.oViewOverview.getProperty, VIEWPLANE_RECT=viewplaneRectangle
  viewplaneOffset = viewplaneRectangle[0:1]
  viewplaneSize = viewplaneRectangle[2:3]
  framePoints = [[viewplaneOffset], [viewplaneOffset+(viewplaneSize)*[0,1]], [viewplaneOffset+(viewplaneSize)*[1,1]], [viewplaneOffset+(viewplaneSize)*[1,0]], [viewplaneOffset]]
  self.oFrameOverview.setProperty, DATAX=framePoints[0,*], DATAY=framePoints[1,*]
  
end

pro hubTBManagedContentImageModel::adjustForViewSpecificaOfTheProfile, view

  ; draw profile
;  datax = self.profile.getWavelength()
;  datay = self.profile.getData()
  datax = self.profile.getPlotDataX()
  datay = self.profile.getPlotDataY()

  self.oPlotProfile.setProperty, DATAX=datax, DATAY=datay
  self.oPlotProfile.getProperty, XRANGE=xrange, YRANGE=yrange
  if xrange[0] eq xrange[1] then begin
    xrange += [-1,+1]
  endif
  if yrange[0] eq yrange[1] then begin
    yrange += [-1,+1]
  endif
  xspan = xrange[1]-xrange[0]
  yspan = yrange[1]-yrange[0]
  xrange += [-1,+1]*xspan*0.05
  yrange += [-1,+1]*yspan*0.05
  viewplaneOffset = [xrange[0], yrange[0]]
  viewplaneSize = [xrange[1]-xrange[0], yrange[1]-yrange[0]]
  viewplaneRectangle = [viewplaneOffset, viewplaneSize]
  view.oWindow.getProperty, DIMENSION=windowSize
  viewSize = windowSize/[2,3]
  viewOffset = [windowSize[0]-viewSize[0], 0]
  self.oViewProfile.setproperty, VIEWPLANE_RECT=viewplaneRectangle, DIMENSIONS=viewSize, LOCATION=viewOffset

  ; set profile bars
  bands = self.getBands()
  case self.bandType of
    'rgb' : begin
      for i=0,2 do begin
        (self.oPlotProfileBars[i]).setProperty, DATAX=replicate(datax[bands[i]], 2), DATAY=yrange, HIDE=0
      endfor
    end
    'grey': begin
      (self.oPlotProfileBars[0]).setProperty, DATAX=replicate(datax[bands[0]], 2), DATAY=yrange
      for i=1,2 do (self.oPlotProfileBars[i]).setProperty, HIDE=1
    end
    'classification' : self.controller.hideProfile
  endcase

  ; selected profile location
  self.oPlotProfileSelection.setProperty, DATAX=replicate(self.profileSelectionPoint[0],2), DATAY=yrange
  
  ; draw frame
  framePoints = [[viewplaneOffset], [viewplaneOffset+(viewplaneSize)*[0,1]], [viewplaneOffset+(viewplaneSize)*[1,1]], [viewplaneOffset+(viewplaneSize)*[1,0]], [viewplaneOffset]]
  self.oFrameProfile.setProperty, DATAX=framePoints[0,*], DATAY=framePoints[1,*]

  ; hide (moving it out of the window) if image selection is out of scene
  !null = self.getImageSelection(IsInsideImage=isInsideImage)
  if ~isInsideImage then begin
    self.oViewProfile.setProperty, LOCATION=windowSize+1 ; need to add 1, must be a idl bug?
  endif
end

pro hubTBManagedContentImageModel::drawViewToClipboard, view
  view.oWindow.getProperty, DIMENSIONS=dimensions
  oClipboard = IDLgrClipboard(DIMENSIONS=dimensions)
  self.loadImageTileData, oClipboard
  oClipboard.draw, self.oScene
  obj_destroy, oClipboard
end

pro hubTBManagedContentImageModel::saveViewToFile, view, filename, format
  oImage = view.oWindow.read()
  oImage.getProperty, Data=rgbData ; [3,samples,lines]
  write_image, filename, format, rgbData
end

pro hubTBManagedContentImageModel::getProperty, _REF_EXTRA=_extra, image=image, profile=profile, bandType=bandType
  self.hubTBManagedContentMVCModel::getProperty, _EXTRA=_extra
  if arg_present(image) then image = self.image
  if arg_present(profile) then profile = self.profile  
  if arg_present(bandType) then bandType = self.bandType
end

pro hubTBManagedContentImageModel::removeDependencies
  speclibContents = self.linkedSpeclibs + list() ; need to make a copy of the list, because ::setSpeclibLinked will change the list
  foreach speclibContent, speclibContents, i do begin
    self.setSpeclibLinked, speclibContent, 0b
  endforeach
end

pro hubTBManagedContentImageModel__define
  define = {hubTBManagedContentImageModel, inherits hubTBManagedContentMVCModel, $
    oImageImage:obj_new(), $
    oImageImagePalette:obj_new(), $
    oPlotImageCH:objarr(3), $

    oViewOverview:obj_new(), $
    oModelOverview:obj_new(), $
    oImageOverview:obj_new(), $
    oFrameOverview:obj_new(), $
    oFrameOverviewVisible:obj_new(), $
    
    oViewProfile:obj_new(), $
    oModelProfile:obj_new(), $
    oFrameProfile:obj_new(), $
    oPlotProfile:obj_new(), $
    oPlotProfileSelection:obj_new(), $
    oPlotProfileBars:objarr(3), $
    
    image:obj_new(), $
    profile:obj_new(), $
    bands:ptr_new(), $
    numberOfChannels:0, $
    bandType:'',$ ; rgb, grey, classification
    pyramid:obj_new(), $
    tileSize:[0l,0l], $
    imageSize:[0l,0l], $
    imageCenter:[0.,0.], $
    imageZoom:0., $
    imageStretch:dindgen(2,3), $
    imageStretchPercentOnViewExtent:dindgen(2), $
    imageSelection:[0.,0.], $
    profileSelectionPoint:[0.,0.], $
    profileSelectionIndex:0l, $
   
    imagePositionLinked:0b,$
    imageZoomLinked:0b,$
    
    linkedSpeclibs:list(),$
    
    labelingTool:obj_new(), $
    contextBaseProfile:0l $      
  }
end