function hubTBManagedContentImageDialogBandSelection::init, imageHeader, oldBands, groupLeader
  self.imageHeader = imageHeader
  self.oldBands = ptr_new(oldBands)
  self.groupLeader = groupLeader
  case !VERSION.OS_FAMILY of
    'Windows' : self.backgroundColor = [255,255,255]
    'unix' : self.backgroundColor = [192,192,192]
  endcase
  self.result = ptr_new(!null)
  return, 1b
end

function hubTBManagedContentImageDialogBandSelection::getResult
  self.create
  return, *self.result
end

pro hubTBManagedContentImageDialogBandSelection::create
  tlb = widget_base(/COLUMN, TITLE='Change Bands', GROUP_LEADER=self.groupLeader, UVALUE=self, /MODAL);, TLB_FRAME_ATTR=2)
  self.label = widget_label(tlb, Value=' ', /DYNAMIC_RESIZE) 
  self.tree = widget_tree(tlb, UVALUE=self, SCR_XSIZE=300, SCR_YSIZE=500)
  bitmap = hubTBAuxiliaryGetIconBitmaps(/Multi)
  treeImage = widget_tree(self.tree, UVALUE=self, VALUE=file_basename(self.imageHeader.getMeta('filename data')), /FOLDER, BITMAP=bitmap, /MASK, /EXPANDED )
 
  bandLongNames = strcompress(ulindgen(self.imageHeader.getMeta('bands'))+1,/REMOVE_ALL)+': '+self.imageHeader.getMeta('band names')
  if isa(self.imageHeader.getMeta('wavelength')) then begin
    bandLongNames += ' ('+strcompress(/REMOVE_ALL, self.imageHeader.getMeta('wavelength'))+')'
  endif

  bitmap = hubTBAuxiliaryGetIconBitmaps(/RECTANGLE, 1, self.backgroundColor)
  self.bandIDs = list()
  for i=0L,n_elements(bandLongNames)-1 do begin
    self.bandIDs.add, widget_tree(treeImage, UVALUE=self, VALUE=bandLongNames[i], /MASK, BITMAP=bitmap)
  endfor

  !null = widget_label(tlb, Value='Predefined RGB Combinations ', /DYNAMIC_RESIZE)
  buttonBase = widget_base(tlb, /ROW, XPAD=0, YPAD=0, SPACE=0)
  self.wlTrue = widget_button(buttonBase, Value='True Color', UVALUE=self, SENSITIVE=isa(self.imageHeader.getMeta('wavelength units')))
  self.wlCIR = widget_button(buttonBase, Value='Colored Infrared', UVALUE=self, SENSITIVE=isa(self.imageHeader.getMeta('wavelength units')))
  self.wlDefault = widget_button(buttonBase, Value='Default Bands', UVALUE=self, SENSITIVE=isa(self.imageHeader.getMeta('default bands')))
  
  
  dummyBase = widget_base(tlb, /ROW, XPAD=0, YPAD=0, SPACE=0)
  buttonBase = widget_base(dummyBase, /ROW, XPAD=0, YPAD=0, SPACE=0, /FRAME)
  self.accept = widget_button(buttonBase, Value='Accept', UVALUE=self)
  self.cancel = widget_button(buttonBase, Value='Cancel', UVALUE=self)
  widget_control, tlb, /REALIZE

  (hubGUIHelper()).centerTLB, tlb
  
  bands = rebin(*self.oldBands, 3)
  self.setSelectedBands, bands
  self.setCurrentChannel, 0
  xmanager, 'hubTBManagedContentImageDialogBandSelection', EVENT_HANDLER='hubObjCallback_event', tlb
 
end
  
pro hubTBManagedContentImageDialogBandSelection::setSelectedBands, selectedBands
  
  self.selectedBands = selectedBands
  foreach bandID, self.bandIDs, i do begin
    bitmap = rebin(/SAMPLE,reform(self.backgroundColor,1,1,3), 16, 16, 3)
    bitmap[2:-1, 1:-2, *] = 0
    bitmap[3:-2, 2:-3, *] = 255
    if i eq selectedBands[0] then bitmap[3:6, 2:-3, [1,2]] = 0
    if i eq selectedBands[1] then bitmap[7:10, 2:-3, [0,2]] = 0
    if i eq selectedBands[2] then bitmap[11:14, 2:-3, [0,1]] = 0
    widget_control, bandID, SET_TREE_BITMAP=bitmap
  endforeach

end

pro hubTBManagedContentImageDialogBandSelection::setCurrentChannel, currentChannel
  self.currentChannel = currentChannel
  widget_control, self.label, SET_VALUE='Select band for '+(['red','green','blue'])[currentChannel]+' channel.' 
end

function hubTBManagedContentImageDialogBandSelection::getPredefinedBands, Default=default, CIR=cir, TRUE=true
  if keyword_set(default) then begin
    result = rebin([self.imageHeader.getMeta('default bands')], 3)
  endif

  if keyword_set(true) then begin
    result = self.imageHeader.locateWavelength(/TrueColor)
  endif
  
  if keyword_set(cir) then begin
    result = self.imageHeader.locateWavelength(/ColoredInfrared)
  endif
  
  return, result
end

pro hubTBManagedContentImageDialogBandSelection::handleEvent, event
  case event.id of
    self.accept : begin
      *self.result = self.selectedBands
      widget_control, event.top, /DESTROY 
    end
    self.cancel : begin
      widget_control, event.top, /DESTROY 
    end
    self.wlDefault : begin
      self.setSelectedBands, self.getPredefinedBands(/Default)
      *self.result = self.selectedBands
      widget_control, event.top, /DESTROY 
    end
    self.wlTrue : begin
      self.setSelectedBands, self.getPredefinedBands(/True)
      *self.result = self.selectedBands
      widget_control, event.top, /DESTROY 
    end
    self.wlCIR : begin
      self.setSelectedBands, self.getPredefinedBands(/CIR) 
      *self.result = self.selectedBands
      widget_control, event.top, /DESTROY 
    end
    else : begin
      band = self.bandIDs.where(event.id)
      if isa(band) then begin
        selectedBands = self.selectedBands
        selectedBands[self.currentChannel] = band
        self.setSelectedBands, selectedBands
        self.setCurrentChannel, (self.currentChannel+1) mod 3
      endif
    end
  endcase
end

pro hubTBManagedContentImageDialogBandSelection__define

  define = {hubTBManagedContentImageDialogBandSelection, inherits IDL_OBJECT, $
    imageHeader:obj_new(), $
    oldBands:ptr_new(),$
    groupLeader:0l,$
    backgroundColor:[0,0,0],$
    bandIDs:list(), $
    label:0l,$
    tree:0l, $
    selectedBands:[0l,0l,0l],$
    currentChannel:0, $
    result:ptr_new(), $
    accept:0l, $
    cancel:0l, $
    wlDefault:0l,$
    wlTrue:0l, $
    wlCIR:0l $
  }
  
end