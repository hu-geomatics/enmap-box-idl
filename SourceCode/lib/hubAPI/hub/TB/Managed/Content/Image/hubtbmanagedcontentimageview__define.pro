function hubTBManagedContentImageView::init
  self.eventHandler = hubTBManagedContentImageViewEventHandler(self)
  return, 1b
end

pro hubTBManagedContentImageView::createContext
  self.hubTBManagedContentMVCView::createContext

  bandType = self.controller.getBandType()
  isClassification = bandType eq 'classification'
  
  if ~isClassification then begin
    !null = widget_button(self.contextBase, VALUE='Change RGB Bands', UVALUE=hubObjCallbackEvent(self.eventHandler, 'rgbBands'), /SEPARATOR)
  endif
  
  submenu = widget_button(/MENU, self.contextBase, VALUE='Zoom to')
  !null = widget_button(submenu, VALUE='Pixel Size [1]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'zoomLevel', 1.0))
  !null = widget_button(submenu, VALUE='Scene Size [2]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'zoomLevelSceneFull'))
  !null = widget_button(submenu, VALUE='Scene Width [3]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'zoomLevelSceneWidth'))
  !null = widget_button(submenu, VALUE='Scene Height [4]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'zoomLevelSceneHeight'))
  subsubmenu = widget_button(/MENU, submenu, VALUE='Level [MouseWheel]')
  zoomLevels = ['800', '400', '200', '100','50', '25', '12.5', '6.3', '3.1', '1.6', '0.8']
  foreach zoomLevel, zoomLevels, i do begin
    !null = widget_button(subsubmenu, VALUE=zoomLevel+'%', UVALUE=hubObjCallbackEvent(self.eventHandler, 'zoomLevel', float(zoomLevel)/100))
  endforeach

  submenu = widget_button(/MENU, self.contextBase, VALUE='Move to')
  !null = widget_button(submenu, VALUE='Scene Center [Shift+Enter]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'moveSceneCenter'))
  !null = widget_button(submenu, VALUE='Scene Left Edge [Shift+Left]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'moveSceneLeftEdge'))
  !null = widget_button(submenu, VALUE='Scene Right Edge [Shift+Right]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'moveSceneRightEdge'))
  !null = widget_button(submenu, VALUE='Scene Upper Edge [Shift+Upper]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'moveSceneUpperEdge'))
  !null = widget_button(submenu, VALUE='Scene Lower Edge [Shift+Lower]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'moveSceneLowerEdge'))

  if ~isClassification then begin
    submenu = widget_button(/MENU, self.contextBase, VALUE='Set Data Stretch on')
   ; !null = widget_button(submenu, VALUE='Custom Stretch' , UVALUE=hubObjCallbackEvent(self.eventHandler, 'imageStretchCustom'), /SEPARATOR)
    stretchLevel = [0,1,2,5,10,20,30,40]
    stretchLevelsMin = strcompress(/REMOVE_ALL, stretchLevel)
    stretchLevelsMax = strcompress(/REMOVE_ALL, 100-stretchLevel)
    subsubmenu = widget_button(/MENU, submenu, VALUE='Full Extent [Ctrl+MouseWheel]')
    for i=0,n_elements(stretchLevel)-1 do begin
      uvalue = hubObjCallbackEvent(self.eventHandler, 'stretchOnFullExtent', fix([stretchLevelsMin[i], stretchLevelsMax[i]]))
      value = 'to '+stretchLevelsMin[i]+'-'+stretchLevelsMax[i]+'%'
      !null = widget_button(subsubmenu, VALUE=value, UVALUE=uvalue)
    endfor
    subsubmenu = widget_button(/MENU, submenu, VALUE='View Extent [Ctrl+Shift+MouseWheel]')
    for i=0,n_elements(stretchLevel)-1 do begin
      uvalue = hubObjCallbackEvent(self.eventHandler, 'stretchOnViewExtent', fix([stretchLevelsMin[i], stretchLevelsMax[i]]))
      value = 'to '+stretchLevelsMin[i]+'-'+stretchLevelsMax[i]+'%'
      !null = widget_button(subsubmenu, VALUE=value, UVALUE=uvalue)
    endfor
  
    !null = widget_button(submenu, VALUE='Custom Stretch [Ctrl+MiddleMouse]', /SEPARATOR, UVALUE=hubObjCallbackEvent(self.eventHandler, 'stretchCustom'))
    !null = widget_button(submenu, VALUE='Match Stretch with Image View', UVALUE=hubObjCallbackEvent(self.eventHandler, 'stretchMatch'))
  end
  
  submenu = widget_button(/MENU, self.contextBase, VALUE='Show/Hide Views')
  
  oViewOverview = (self.controller.getGraphicView(/Overview))
  oViewOverview.getProperty, Hide=hide
  value = (['Hide','Show'])[hide]+' Overview View [o]'
  name = (['hide','show'])[hide]+'Overview'
  uvalue = hubObjCallbackEvent(self.eventHandler, name, ~hide)
  !null = widget_button(submenu, VALUE=value, UVALUE=uvalue)
 
  if ~isClassification then begin
    oViewProfile = (self.controller.getGraphicView(/Profile))
    oViewProfile.getProperty, Hide=hide
    value = (['Hide','Show'])[hide]+' Profile View [p]'
    name = (['hide','show'])[hide]+'Profile'
    uvalue = hubObjCallbackEvent(self.eventHandler, name, ~hide)
    !null = widget_button(submenu, VALUE=value, UVALUE=uvalue)
  endif

  hide = ~self.controller.permanentMessageVisible()
  value = (['Hide','Show'])[hide]+' Cursor Information [i]'
  name = (['hide','show'])[hide]+'CursorValue'
  uvalue = hubObjCallbackEvent(self.eventHandler, name, ~hide)
  !null = widget_button(submenu, VALUE=value, UVALUE=uvalue)

  labelingTool = self.controller.getLabelingTool()
  if obj_valid(labelingTool) then begin
    hide = ~labelingTool.isActive()
  endif else begin
    hide = 1
  endelse
  value = (['Hide','Show'])[hide]+' Labeling Tool'
  name = (['hide','show'])[hide]+'LabelingTool'
  uvalue = hubObjCallbackEvent(self.eventHandler, name, ~hide)
  !null = widget_button(submenu, VALUE=value, UVALUE=uvalue)
  !null = widget_button(self.contextBase, VALUE=value, UVALUE=uvalue) ; also show here

  if ~isClassification then begin
    !null = widget_button(self.contextBase, VALUE='Link Image Profile to Spectral View', UVALUE=hubObjCallbackEvent(self.eventHandler, 'linkProfileToSpeclib'), /SEPARATOR)
    !null = widget_button(self.contextBase, VALUE='Copy Image Profile to linked Spectral View [Shift+LeftMouse]', UVALUE=hubObjCallbackEvent(self.eventHandler, 'copyProfileToSpeclib'))
  endif
  
  !null = widget_button(self.contextBase, VALUE='Copy View to Clipboard [Ctrl+c]' , UVALUE=hubObjCallbackEvent(self.eventHandler, 'copyViewToClipboard'), /SEPARATOR)
  !null = widget_button(self.contextBase, VALUE='Save View to File [Ctrl+s]' , UVALUE=hubObjCallbackEvent(self.eventHandler, 'saveViewToFile'))
  
  if ~isClassification then begin
    !null = widget_button(self.contextBase, VALUE='Create Profile Plot with R', UVALUE=hubObjCallbackEvent(self.eventHandler, 'drawProfileInR'))
  endif
  
;  linked = self.getImageToImageLinkerPosition()
;  value = (['Link','Unlink'])[linked]+' Spatially'
;  uvalue = hash(0, self, 'link', ~linked)
;  !null = widget_button(self.contextBase, VALUE=value, UVALUE=uvalue, UNAME='linkSpatially', /SEPARATOR)
;  
;  zoomLinked = self.getImageToImageLinkerZoom()
;  value = (['Link','Unlink'])[zoomLinked]+' Zoom Level'
;  uvalue = hash(0, self, 'linkZoom', ~zoomLinked)
;  !null = widget_button(self.contextBase, VALUE=value, UVALUE=uvalue, UNAME='linkSpatiallyZoom', SENSITIVE=linked)
  
; todo spectral linking
;  submenu = widget_button(/MENU, self.contextBase, VALUE='Link Spectrally to')
;  foreach view, self.toolbox.viewManager.getViews() do begin
;    if view eq self then continue
;    if isa(view, 'enmapBoxGUIImageView') then continue
;    viewNumber = strcompress(/REMOVE_ALL, view.getViewIndex()+1)
;    value = 'View '+viewNumber
;    if isa(view, 'enmapBoxGUISpectralView') then begin
;      value = 'Spectral '+value
;    endif else begin
;      value += ' (create new Spectral View)'
;    endelse
;    !null = widget_button(submenu, VALUE=value, UVALUE=hash(0,self, 'view',view), UNAME='linkProfileToSpectralView')
;  endforeach

end

;pro hubTBManagedContentImageView::draw
;  self.oWindow.draw, self.controller.getGraphicScene()
;end

;function hubTBManagedContentImageView::getSelectedView, windowSelection
;  selection = (self.oWindow.select(self.controller.getGraphicScene(), windowSelection))[0]
;  oView = isa(selection, 'IDLgrView') ? selection : obj_new()
;  return, oView
;end

pro hubTBManagedContentImageView__define
  define = {hubTBManagedContentImageView, inherits hubTBManagedContentMVCView}
end