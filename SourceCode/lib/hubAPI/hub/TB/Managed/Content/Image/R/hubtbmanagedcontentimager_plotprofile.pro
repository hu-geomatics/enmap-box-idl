pro hubTBManagedContentImageR_plotProfile_event1, result, UserInformation=information
  amws = information.amws
  code = result['code']
  parameters = hash('x', information.x, 'y', information.y, 'plotFilename', result['plotFilename'])
  rResult = hubR_runCode(code, parameters, spawnResult, spawnError, GroupLeader=groupLeader, Title=title, NoReport=1)
  amws.log.setValue, [spawnResult,spawnError]
  if rResult.hubIsa('plotFilename') then begin
    hubHelper.openFile, rResult['plotFilename']
  endif
end

pro hubTBManagedContentImageR_plotProfile_event2, result
  hubHelper.openFile, /HTML, 'https://stat.ethz.ch/R-manual/R-devel/library/graphics/html/plot.html'
end

pro hubTBManagedContentImageR_plotProfile, x, y, title, xtitle, ytitle, GroupLeader=groupLeader

  hubAMW_program, groupLeader, Title='Create Profile Plot with R'
  hubAMW_checklist, 'format',  Title='Select File Format      ', List=['tiff','pdf'], /EXTRACT, VALUE=0
  hubAMW_checklist, 'routine', Title='Select Plotting Function', List=['plot()','ggplot()'], /EXTRACT, VALUE=0
  result = hubAMW_manage(/DICTIONARY)

  if ~result.accept then return

  plotFilename = strjoin(strsplit(filepath(/TMP, 'plot.'+result.format),'\', /EXTRACT), '/')
  code = 'r$plotFilename <- "'+plotFilename+'"'
  case result.format of
    'tiff' : code = [code, 'tiff(r$plotFilename, res=300, height=1500, width=1500)']
    'pdf'  : code = [code, 'pdf(r$plotFilename, paper="a4r", height=7, width=7)']
  endcase

  case result.routine of
    'plot()'   : code = [code, 'plot(x=p$x, y=p$y, type="l", col="blue", main="'+title+'", xlab="'+xtitle+'", ylab="'+ytitle+'")']
    'ggplot()' : code = [code, 'require(ggplot2)', 'ggplot(data=data.frame(wavelength=p$x, reflectance=p$y), aes(x=wavelength, y=reflectance)) + geom_line() + labs(title="Plot")']
  endcase
  code = [code, 'dev.off()']

  size = GET_SCREEN_SIZE()*[0.5,0.2]
  amws = dictionary()
  information = dictionary('x',x, 'y',y, 'amws', amws)
  hubAMW_program, groupLeader, Title='Create Profile Plot with R'
  hubAMW_label, 'Use p$x and p$y for x and y data and p$plotFilename for the plot destination path.'
  hubAMW_text, 'code', TITLE='R Code', EDITABLE=1, VALUE=code, XSIZE=size[0], YSIZE=size[1]
  hubAMW_text, 'log', TITLE='R Standard Output/Error Log', EDITABLE=0, XSIZE=size[0], YSIZE=size[1]*0.5, HubAMWObject=hubAMWObject
  amws['log'] = hubAMWObject
  hubAMW_button, /BAR, RESULTHASHKEYS=['code', 'plotFilename'],TITLE='Run R Script', EVENTHANDLER='hubTBManagedContentImageR_plotProfile_event1', USERINFORMATION=information
  hubAMW_button, /BAR, TITLE='Show Plot Options', EVENTHANDLER='hubTBManagedContentImageR_plotProfile_event2'
  result = hubAMW_manage(/DICTIONARY, ButtonNameAccept='', ButtonNameCancel='Done')
end