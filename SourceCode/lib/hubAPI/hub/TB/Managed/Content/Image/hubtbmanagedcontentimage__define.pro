function hubTBManagedContentImage::init, filename, bands, toolbox, gridFrameManager, Classification=classification
  !null = self.hubTBManagedContentMVC::init(toolbox, gridFrameManager)

  ; set filename and rgb bands
  self.controller.setFilename, filename
  if keyword_Set(classification) then begin
    self.controller.setBandClassification
  endif else begin
    if n_elements(bands) eq 1 then begin
      self.controller.setBandGrey, bands  
    endif else begin
      self.controller.setBandRGB, bands  
    endelse
    inputImage = self.controller.getImage()
    if inputImage.getMeta('data type') eq 1 then begin
      self.controller.setImageStretch, [0,100], /Percentage
    endif else begin
      self.controller.setImageStretch, [2,98], /Percentage
    endelse
  endelse
  self.controller.setImageZoom, 1
  self.controller.setImageCenter
  self.controller.setImageSelection
  self.controller.showOverview
  self.controller.showProfile
  self.controller.hideLabelingTool
  return, 1b
end

function hubTBManagedContentImage::createController
  return, hubTBManagedContentImageController()
end

function hubTBManagedContentImage::createModel
  return, hubTBManagedContentImageModel()
end

pro hubTBManagedContentImage::setImageCenter, imageCenter, _EXTRA=_extra
  self.controller.setImageCenter, imageCenter, _EXTRA=_extra
end

function hubTBManagedContentImage::getImageCenter
  return, self.controller.getImageCenter()
end

pro hubTBManagedContentImage::setImageSelection, imageSelection, _EXTRA=_extra
  self.controller.setImageSelection, imageSelection, _EXTRA=_extra
end

function hubTBManagedContentImage::getImageSelection
  return, self.controller.getImageSelection()
end

pro hubTBManagedContentImage::setImageZoom, imageZoom, _EXTRA=_extra
  self.controller.setImageZoom, imageZoom, _EXTRA=_extra
end

function hubTBManagedContentImage::getImageZoom
  return, self.controller.getImageZoom()
end

pro hubTBManagedContentImage::setImageLink, imagePositionLinked, imageZoomLinked
  self.controller.setImageLink, imagePositionLinked, imageZoomLinked
end

function hubTBManagedContentImage::getImageZoomLinked
  return, self.controller.getImageZoomLinked()
end

function hubTBManagedContentImage::getImagePositionLinked
  return, self.controller.getImagePositionLinked()
end

pro hubTBManagedContentImage::setSpeclibLinked, speclibContent, linked
  self.controller.setSpeclibLinked, speclibContent, linked
end

function hubTBManagedContentImage::getImageSize
  return, self.controller.getImageSize()
end

function hubTBManagedContentImage::getImage
  return, self.controller.getImage()
end

function hubTBManagedContentImage::convertPixelToGeo, pixelPosition
  return, self.controller.convertPixelToGeo(pixelPosition)
end

function hubTBManagedContentImage::convertGeoToPixel, geoPosition
  return, self.controller.convertGeoToPixel(geoPosition)
end

pro hubTBManagedContentImage::showLabelingTool
  self.controller.showLabelingTool
end

pro hubTBManagedContentImage::hideLabelingTool
  self.controller.hideLabelingTool
end


pro hubTBManagedContentImage__define
  define = {hubTBManagedContentImage, inherits hubTBManagedContentMVC}
end