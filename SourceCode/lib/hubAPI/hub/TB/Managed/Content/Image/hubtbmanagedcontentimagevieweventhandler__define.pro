function hubTBManagedContentImageViewEventHandler::init, view
  !null = self.hubTBManagedContentMVCViewEventHandler::init(view)
  self.imageViewMiddleMousePanningHandler = hubTBAuxiliaryPanningHandler()
  self.imageViewLeftMousePanningHandler = hubTBAuxiliaryPanningHandler()
  self.overviewViewLeftMousePanningHandler = hubTBAuxiliaryPanningHandler()
;  self.profileViewLeftMousePanningHandler = hubTBAuxiliaryPanningHandler()
  return, 1b
end

pro hubTBManagedContentImageViewEventHandler::handleWindowEvent, objEvent
  self.hubTBManagedContentMVCViewEventHandler::handleWindowEvent, objEvent
  
  if objEvent.subcategory eq 'tracking event' then begin
    self.imageViewMiddleMousePanningHandler.finish
    self.imageViewLeftMousePanningHandler.finish
    self.overviewViewLeftMousePanningHandler.finish
;    self.profileViewLeftMousePanningHandler.finish
    return
  endif
  
  oView = self.view.getSelectedView([objEvent.event.x, objEvent.event.y])
  case oView of
    self.view.controller.getGraphicView() : begin
      self.handleWindowImageViewEvent, objEvent
    end
    self.view.controller.getGraphicView(/Overview) : begin
      self.handleWindowOverviewViewEvent, objEvent
    end
    self.view.controller.getGraphicView(/Profile) : begin
      self.handleWindowProfileViewEvent, objEvent
    end
    else : ;
  endcase
end

pro hubTBManagedContentImageViewEventHandler::handleWindowImageViewEvent, objEvent
  event = objEvent.event
  
  self.overviewViewLeftMousePanningHandler.finish
;  self.profileViewLeftMousePanningHandler.finish
  
  case objEvent.subsubcategory of
    'middle mouse button pressed event' : begin
      if event.modifiers eq 2 then begin
        self.handleCustomStretch, objEvent
      endif else begin
        self.imageViewMiddleMousePanningHandler.start, [event.x, event.y]
      endelse
    end
    'middle mouse button released event' : self.imageViewMiddleMousePanningHandler.finish
    'motion event' : begin
      if self.imageViewMiddleMousePanningHandler.isActive() then begin
        mouseMovement = self.imageViewMiddleMousePanningHandler.getChange([event.x, event.y], /InvertY)
        imageCenter = self.view.controller.getImageCenter()+mouseMovement/self.view.controller.getImageZoom()
        self.view.controller.setImageCenter, imageCenter
        self.view.controller.synchronizeLinkedImages
        self.view.controller.synchronizeLinkedSpeclibs
        self.doUpdateView = 1b
        doReturn = 0b
      endif
      if self.imageViewLeftMousePanningHandler.isActive() then begin
        self.view.controller.setImageSelectionByWindowSelection, [event.x, event.y]
        self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue()
        self.view.controller.synchronizeLinkedImages
        self.view.controller.synchronizeLinkedSpeclibs
        self.handleEditROI, InvertMode=self.imageViewLeftMousePanningHandler.getInfo()
        self.doUpdateView = 1b
      endif
      if ~self.doUpdateView then return  
    end
    'left mouse button pressed event' : begin
      self.view.controller.setImageSelectionByWindowSelection, [event.x, event.y]
      self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue()
      if event.clicks eq 2 then begin
        self.view.controller.setImageCenter, /ImageSelection
      endif
      if event.modifiers eq 1 then begin ; Shift
        self.handleWindowProfileViewCopyProfileToLinkedSpeclibs
      endif
      self.view.controller.synchronizeLinkedImages
      self.view.controller.synchronizeLinkedSpeclibs
      invertMode = event.modifiers eq 2
      self.handleEditROI, InvertMode=invertMode ; invert mode with Strg
      self.imageViewLeftMousePanningHandler.start, [0,0], Info=invertMode
    end
    'left mouse button released event' : self.imageViewLeftMousePanningHandler.finish
    'left mouse button released event' : self.imageViewLeftMousePanningHandler.finish
    'right mouse button pressed event' : return ; not handled
    'right mouse button released event' : return ; not handled
    'ascii key pressed event' : begin
      case event.ch of
        13 : begin
          if event.modifiers eq 1 then begin ; [Shift] key 
            self.handleWindowImageViewMoveToSceneEgde
          endif
        end
        43 : begin
          self.view.controller.setImageZoom, /Increase
          self.view.controller.synchronizeLinkedImages
          self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()
        end
        45 : begin
          self.view.controller.setImageZoom, /Decrease
          self.view.controller.synchronizeLinkedImages
          self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()
        end
        49 : begin ; [1]: set zoom to 1
          self.view.controller.setImageZoom, 1
          self.view.controller.synchronizeLinkedImages
          self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+'(Pixel Size) '
        end
        50 : begin ; [2]: set zoom to full scene and center image
          self.view.controller.setImageCenter
          self.view.controller.setImageZoom, /SceneFull, ImageView=self.view
          self.view.controller.synchronizeLinkedImages
          self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+'(Scene Size) '
        end
        51 : begin ; [3]: set zoom to scene width and center image
          self.view.controller.setImageCenter
          self.view.controller.setImageZoom, /SceneWidth, ImageView=self.view
          self.view.controller.synchronizeLinkedImages
          self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+'(Scene Width) '
        end
        52 : begin ; [4]: set zoom to scene hight and center image
          self.view.controller.setImageCenter
          self.view.controller.setImageZoom, /SceneHeight, ImageView=self.view
          self.view.controller.synchronizeLinkedImages
          self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+'(Scene Height) '
        end
        111 : begin ; [o]: turn overview on/off
          self.view.controller.swapOverview
        end
        112 : begin ; [p]: turn profile on/off
          self.view.controller.swapProfile
        end
        105 : begin ; [i]: turn cursor information on/off
          if self.view.controller.permanentMessageVisible() then begin
            self.view.controller.hidePermanentMessage
          endif else begin
            self.view.controller.showPermanentMessage, self.view.controller.getFormattedImageCursorValue(), /TurnOn
          endelse
        end
        3 : begin ; [c]: copy to clipboard
          if event.modifiers eq 2 then begin
            self.view.controller.drawViewToClipboard, self.view
            self.view.showTemporaryMessage, ' Image View copied to Clipboard '
          endif          
        end
        19 : begin ; [s]: save to file
          if event.modifiers eq 2 then begin
            result = self.dialogSaveViewToFile(objEvent.event.top)
            if isa(result) then begin
              self.view.controller.saveViewToFile, self.view, result.filename, result.format, Open=result.open
            endif
          endif
        end
        
        else : help,event
      endcase
    end
    'ascii key released event' : begin
      case event.ch of
        9 : begin
          labelingTool = self.view.controller.getLabelingTool()
          if obj_valid(labelingTool) && labelingTool.isActive() then begin
            case 1b of
              labelingTool.getMode(/Add) : begin
                labelingTool.setMode, /Remove
              end
              labelingTool.getMode(/Remove) : begin
                labelingTool.setMode, /Off
              end
              labelingTool.getMode(/Off) : begin
                labelingTool.setMode, /Add
              end
            endcase
            self.view.showTemporaryMessage, self.view.controller.getFormattedLabelingModus()
          endif
        end
        else : return
      endcase
    end
    'non-ascii key released event' : begin
      if event.modifiers eq 0 then begin ; no modifiers
        case event.key of
          5 : self.view.controller.setImageSelection, Move=[-1, 0]
          6 : self.view.controller.setImageSelection, Move=[+1, 0]
          7 : self.view.controller.setImageSelection, Move=[ 0,-1]
          8 : self.view.controller.setImageSelection, Move=[ 0,+1]
          else : return
        endcase
        self.view.controller.synchronizeLinkedImages
        self.view.controller.synchronizeLinkedSpeclibs
        self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue()
      endif
      if event.modifiers eq 1 then begin ; [Shift] key 
        case event.key of
          5 : self.handleWindowImageViewMoveToSceneEgde, [-!VALUES.F_INFINITY, 0]
          6 : self.handleWindowImageViewMoveToSceneEgde, [+!VALUES.F_INFINITY, 0]
          7 : self.handleWindowImageViewMoveToSceneEgde, [ 0,-!VALUES.F_INFINITY]
          8 : self.handleWindowImageViewMoveToSceneEgde, [ 0,+!VALUES.F_INFINITY]
          else : return
        endcase
      endif
      if event.modifiers eq 2 then begin ; [Strg] key
        case event.key of
          7 : self.view.controller.setImageStretch, /Increase
          8 : self.view.controller.setImageStretch, /Decrease
          else : return
        endcase
        self.view.showTemporaryMessage, self.view.controller.getFormattedImageStretch()
      endif
      if event.modifiers eq 1+2 then begin ; [Strg]+[Shift] key
        case event.key of
          7 : self.view.controller.setImageStretchOnViewExtent, self.view, /Increase
          8 : self.view.controller.setImageStretchOnViewExtent, self.view, /Decrease
          else : return
        endcase
        self.view.showTemporaryMessage, self.view.controller.getFormattedImageStretch(/ViewExtent)
      endif
    end
    'non-ascii key pressed event' : ;
    'wheel scrolled event' : begin
      ; zoom in/out
      if event.modifiers eq 0 then begin ; no modifiers
        if event.clicks ge 1 then self.view.controller.setImageZoom, /Increase
        if event.clicks le -1 then self.view.controller.setImageZoom, /Decrease
        self.view.controller.synchronizeLinkedImages
        self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()
      endif
      ; full extent stretch up/down
      if event.modifiers eq 2 then begin ; CONTROL modifier
        if event.clicks ge 1 then self.view.controller.setImageStretch, /Increase
        if event.clicks le -1 then self.view.controller.setImageStretch, /Decrease
        self.view.showTemporaryMessage, self.view.controller.getFormattedImageStretch()
      endif
      ; view extent stretch up/down
      if event.modifiers eq 1+2 then begin ; CONTROL+SHIFT modifier
        if event.clicks ge 1 then self.view.controller.setImageStretchOnViewExtent, self.view, /Increase
        if event.clicks le -1 then self.view.controller.setImageStretchOnViewExtent, self.view, /Decrease
        self.view.showTemporaryMessage, self.view.controller.getFormattedImageStretch(/ViewExtent)
      endif
    end
    else : begin
;      print, 'SubSubCat:',objEvent.subsubcategory
;      stop
    end
  endcase

  self.doUpdateView = 1b
return
end

pro hubTBManagedContentImageViewEventHandler::handleWindowOverviewViewEvent, objEvent
  event = objEvent.event
  
  self.imageViewMiddleMousePanningHandler.finish
  self.imageViewLeftMousePanningHandler.finish
;  self.profileViewLeftMousePanningHandler.finish
  
  case objEvent.subsubcategory of
    'left mouse button pressed event' : begin
      windowPixel = [event.x, event.y]
      oViewOverview = (self.view.controller.getGraphicView(/Overview))
      oViewOverview.getProperty, Location=location, Dimensions=dimensions
      overviewPixel = windowPixel-location
      overviewPixel[1] = dimensions[1]-overviewPixel[1]
      zoomFactor = dimensions/self.view.controller.getImageSize()
      imagePixel = overviewPixel/zoomFactor
      self.view.controller.setImageSelection, imagePixel
      self.view.controller.setImageCenter, imagePixel
      self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue()
      self.view.controller.synchronizeLinkedImages
      self.view.controller.synchronizeLinkedSpeclibs
      self.overviewViewLeftMousePanningHandler.start, [event.x, event.y]
    end
    'left mouse button released event' : self.overviewViewLeftMousePanningHandler.finish
    'motion event' : begin
      if self.overviewViewLeftMousePanningHandler.isActive() then begin
        mouseMovement = self.overviewViewLeftMousePanningHandler.getChange([event.x, event.y], /InvertX)
        oViewOverview = (self.view.controller.getGraphicView(/Overview))
        oViewOverview.getProperty, Dimensions=dimensions
        overviewZoom = dimensions/self.view.controller.getImageSize()
        imageCenter = self.view.controller.getImageCenter()+mouseMovement/overviewZoom
        self.view.controller.setImageSelection, imageCenter
        self.view.controller.setImageCenter, imageCenter
        self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue()
        self.view.controller.synchronizeLinkedImages
        self.view.controller.synchronizeLinkedSpeclibs
        self.doUpdateView = 1b
      endif else begin
        return  
      endelse
    end

    else : self.handleWindowImageViewEvent, objEvent ; redirect image view
  endcase
  self.doUpdateView = 1b
end

pro hubTBManagedContentImageViewEventHandler::handleWindowProfileViewEvent, objEvent

  self.imageViewMiddleMousePanningHandler.finish
  self.imageViewLeftMousePanningHandler.finish
  self.overviewViewLeftMousePanningHandler.finish
  
  event = objEvent.event
  case objEvent.subsubcategory of
    'left mouse button pressed event' : begin
      if event.modifiers eq 1 then begin ; Shift
        self.handleWindowProfileViewCopyProfileToLinkedSpeclibs
      endif
    end
    'left mouse button released event' : ;
    'motion event' : begin
 ;     if self.profileViewLeftMousePanningHandler.isActive() then begin
        self.view.controller.setProfileSelectionByWindowSelection, [event.x, event.y], self.view
        self.view.showPermanentMessage, self.view.controller.getFormattedProfileCursorValue()
;      endif else begin
;        return
;      endelse
    end
    else : self.handleWindowImageViewEvent, objEvent ; redirect to image view
  endcase
  self.doUpdateView = 1b
end

;pro hubTBManagedContentImageViewEventHandler::handleWindowOverviewViewPanningEvent, mousePosition, Init=init, Finish=finish
;
;  if keyword_set(finish) then begin
;    self.imagePanningMode=0
;    return
;  endif
;  
;  if keyword_set(init) then begin
;    self.imagePanningMode=1
;    self.imagePanningStartPosition = mousePosition
;    return
;  endif
;  
;  if self.imagePanningMode then begin
;  
;    mouseMovement = self.imagePanningStartPosition-mousePosition
;    mouseMovement[1] *= -1
;    
;    imageCenter = self.view.controller.getImageCenter()+mouseMovement/self.view.controller.getImageZoom()
;    self.imagePanningStartPosition = mousePosition
;    
;    self.view.controller.setImageCenter, imageCenter
;    self.doUpdateView = 1b
;  endif
;end

pro hubTBManagedContentImageViewEventHandler::handleButtonEvent, objEvent
  self.hubTBManagedContentMVCViewEventHandler::handleButtonEvent, objEvent
  case objEvent.name of
    'zoomLevel': begin
      self.view.controller.setImageZoom, objEvent.parameter
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()
    end
    'zoomLevelPixelSize': begin
      self.view.controller.setImageZoom, /PixelSize
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+' (Pixel Size) '
    end
    'zoomLevelSceneFull': begin
      self.view.controller.setImageZoom, /SceneFull, ImageView=self.view
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+' (Scene Size) '
    end
    'zoomLevelSceneWidth': begin
      self.view.controller.setImageZoom, /SceneWidth, ImageView=self.view
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+' (Scene Width) '
    end
    'zoomLevelSceneHeight': begin
      self.view.controller.setImageZoom, /SceneHeight, ImageView=self.view
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageZoom()+' (Scene Height) '
    end
    'moveSceneCenter' : self.handleWindowImageViewMoveToSceneEgde
    'moveSceneLeftEdge': self.handleWindowImageViewMoveToSceneEgde, [-!VALUES.F_INFINITY, 0]
    'moveSceneRightEdge': self.handleWindowImageViewMoveToSceneEgde, [+!VALUES.F_INFINITY, 0]
    'moveSceneUpperEdge': self.handleWindowImageViewMoveToSceneEgde, [ 0,-!VALUES.F_INFINITY]
    'moveSceneLowerEdge': self.handleWindowImageViewMoveToSceneEgde, [ 0,+!VALUES.F_INFINITY]
    'imageStretchCustom': begin
      hub_todo
    end
    'hideOverview': begin
        self.view.controller.hideOverview
      end
    'showOverview': begin
      self.view.controller.showOverview
    end
    'hideProfile': begin
      self.view.controller.hideProfile
    end
    'showProfile': begin
      self.view.controller.showProfile
    end
    'hideLabelingTool': begin
      self.view.controller.hideLabelingTool
    end
    'showLabelingTool': begin
      self.view.controller.showLabelingTool
    end
    'hideCursorValue': begin
      self.view.controller.hidePermanentMessage
    end
    'showCursorValue': begin
      self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue(), /TurnOn
    end
    'stretchOnViewExtent': begin
      self.view.controller.setImageStretchOnViewExtent, self.view, objEvent.parameter
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageStretch(/ViewExtent)
    end
    'stretchOnFullExtent': begin
      self.view.controller.setImageStretch, objEvent.parameter, /Percentage
      self.view.showTemporaryMessage, self.view.controller.getFormattedImageStretch()
    end
    'stretchCustom': begin
      self.handleCustomStretch, objEvent
    end
    'stretchMatch': begin
      bandType = self.view.controller.getBandType()
      gridManager = self.view.controller.getGridFrameManager()
      imageContents = gridManager.getContents(/Images)
      names = list()
      insensitives = list()
      foreach imageContent, imageContents do begin
        managedFrame = (imageContent.controller.getFrames())[0]
        names.add, managedFrame.getTitle()
        insensitives.add, bandType ne imageContent.controller.getBandType()
        print, bandType, imageContent.controller.getBandType()
      endforeach
      names = names.toArray()
      insensitives = where(/NULL, insensitives.toArray())

      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_checklist, 'index', Title='Select Image View', /COLUMN, List=names, INSENSITIVELIST=insensitives
      result = hubAMW_manage(/STRUCTURE)
      if ~result.accept then return

      targetImageContent = hub_fix0d(imageContents[result.index])
      targetImageStretch = targetImageContent.controller.getImageStretch()
      case bandType of
        'rgb' : newImageStretch = targetImageStretch
        'grey': newImageStretch = targetImageStretch[*,0]
      endcase
      self.view.controller.setImageStretch, newImageStretch
    end
    'rgbBands': begin
      header = (self.view.controller.getImage()).getHeader()
      oldBands = rebin(self.view.controller.getBands(), 3)
      dialog = hubTBManagedContentImageDialogBandSelection(header, oldBands, objEvent.event.top)
      bands = dialog.getResult()
      if isa(bands) then begin
        oldPercentStretch = self.view.controller.getImageStretchPercentage()
        if min(bands) eq max(bands) then begin
          self.view.controller.setBandGrey, bands[0]
        endif else begin
          self.view.controller.setBandRGB, bands
        endelse
        self.view.controller.setImageStretch, oldPercentStretch, /Percentage
      endif
    end
    'linkProfileToSpeclib': begin
      gridManager = self.view.controller.getGridFrameManager()
      speclibContents = gridManager.getContents(/Speclibs)
      if speclibContents.isEmpty() then begin
        ok = dialog_message(/Error, 'No spectral view available.')
        return
      endif
      names = list()
      linked = list()
      foreach speclibContent, speclibContents do begin
        managedFrame = (speclibContent.controller.getFrames())[0]
        names.add, managedFrame.getTitle()
        linked.add, self.view.controller.model.getSpeclibLinked(speclibContent)
      endforeach
      names = names.toArray()
      linked = where(/NULL, linked.toArray())
      
      widget_control,objEvent.event.id, GET_VALUE=title
      hubAMW_program, objEvent.event.top, Title=title
      hubAMW_checklist, 'linked', Title='Select Spectral Views', /COLUMN, List=names, Value=linked, /MULTIPLESELECTION, /FLAG
      result = hubAMW_manage(/STRUCTURE)
      if ~result.accept then return

      foreach speclibContent, speclibContents, i do begin
        self.view.controller.setSpeclibLinked, speclibContent, (result.linked)[i]
      endforeach
    end
    'copyProfileToSpeclib': begin
      self.handleWindowProfileViewCopyProfileToLinkedSpeclibs
    end      
    'copyViewToClipboard' : begin
      self.view.controller.drawViewToClipboard, self.view
    end
    'saveViewToFile' : begin
      result = self.dialogSaveViewToFile(objEvent.event.top)
      if isa(result) then begin
        self.view.controller.saveViewToFile, self.view, result.filename, result.format, Open=result.open
      endif
    end
    'drawProfileInR' : begin
      profile = self.view.controller.getProfile()
      hubTBManagedContentImageR_plotProfile, profile.getPlotDataX(), profile.getPlotDataY(), profile.getName(), profile.getUnits(), 'value', GroupLeader=objEvent.event.top
    end
    else : print, objEvent.name ;stop
  endcase
  self.doUpdateView = 1b
end

pro hubTBManagedContentImageViewEventHandler::handleWindowImageViewMoveToSceneEgde, move
  self.view.controller.setImageSelection, Move=move
  self.view.controller.setImageCenter, /ImageSelection
  self.view.controller.synchronizeLinkedImages
  self.view.controller.synchronizeLinkedSpeclibs
  self.view.showPermanentMessage, self.view.controller.getFormattedImageCursorValue()
  if isa(move) then begin
    self.view.showTemporaryMessage, 'Move to Scene Edge'
  endif else begin
    self.view.showTemporaryMessage, 'Move to Scene Center'
  endelse 
end

pro hubTBManagedContentImageViewEventHandler::handleWindowProfileViewCopyProfileToLinkedSpeclibs
  self.view.controller.copyProfileToLinkedSpeclibs
  self.view.showTemporaryMessage, 'Copy Profile to linked Spectral View'
end

function hubTBManagedContentImageViewEventHandler::dialogSaveViewToFile, groupLeader
  formats = ['BMP','GIF','JPEG','PNG','TIFF']
  hubAMW_program, groupLeader, TITLE='Save View to File'
  hubAMW_subframe, /ROW
  hubAMW_outputFilename, 'filename', TITLE='Filename', VALUE='screenshot'
  hubAMW_combobox, 'format', TITLE='', List=formats, /EXTRACT
  hubAMW_subframe, /Column
  hubAMW_checkbox, 'open', TITLE='Open File', VALUE=1b
  result = hubAMW_manage()
  if result['accept'] then begin
    result['filename'] = result['filename']+'.'+strlowcase(result['format'])
    result = result.toStruct()
  endif else begin
    result = !null
  endelse
  return, result
end

pro hubTBManagedContentImageViewEventHandler::handleEditROI, InvertMode=invertMode
  labelingTool = self.view.controller.getLabelingTool()
  if obj_valid(labelingTool) then begin
    if labelingTool.isActive() && (labelingTool.getMode(/Add) or labelingTool.getMode(/Remove)) then begin
      imageSelection = floor(self.view.controller.getImageSelection(IsInsideImage=isInsideImage))
      if isInsideImage then begin
        layerSize = (labelingTool.getROILayer()).getLayerSize()
        pixelIndex = imageSelection[0]+imageSelection[1]*layerSize[0]
        roiIndex = labelingTool.getRegionSelection()
        if labelingTool.getMode(/Add) then begin
          if keyword_set(invertMode) then begin
            labelingTool.removePixel, roiIndex, pixelIndex
          endif else begin
            labelingTool.addPixel, roiIndex, pixelIndex
          endelse
        endif
        if labelingTool.getMode(/Remove) then begin
          if keyword_set(invertMode) then begin
            labelingTool.addPixel, roiIndex, pixelIndex
          endif else begin
            labelingTool.removePixel, roiIndex, pixelIndex
          endelse
        endif
      endif
    endif
  endif
end

pro hubTBManagedContentImageViewEventHandler::handleCustomStretch, objEvent
  imageStretch = reform(string(self.view.controller.getImageStretch(), Format='(f0.4)'),2,3)
  bandType = self.view.controller.getBandType()
  case bandType of
    'rgb' : ;
    'grey': imageStretch = imageStretch[*,0]
  endcase
  
  title = 'Custom Stretch'
  hubAMW_program, objEvent.event.top, Title=title
  case bandType of
    'rgb' : begin
      hubAMW_subframe, /ROW
      hubAMW_parameter, 'r1', Title='Red:     min', Value=imageStretch[0,0], /FLOAT
      hubAMW_parameter, 'r2', Title='  max', Value=imageStretch[1,0], /FLOAT
      hubAMW_subframe, /ROW
      hubAMW_parameter, 'g1', Title='Green:   min', Value=imageStretch[0,1], /FLOAT
      hubAMW_parameter, 'g2', Title='  max', Value=imageStretch[1,1], /FLOAT
      hubAMW_subframe, /ROW
      hubAMW_parameter, 'b1', Title='Blue:    min', Value=imageStretch[0,2], /FLOAT
      hubAMW_parameter, 'b2', Title='  max', Value=imageStretch[1,2], /FLOAT
    end
    'grey' : begin
      hubAMW_subframe, /ROW
      hubAMW_parameter, 'g1', Title='Grey:    min', Value=imageStretch[0], /FLOAT
      hubAMW_parameter, 'g2', Title='  max', Value=imageStretch[1], /FLOAT
    end
  endcase
  result = hubAMW_manage(/STRUCTURE)
  if ~result.accept then return
  case bandType of
    'rgb'  : newImageStretch = reform([hubRange([result.r1, result.r2]), hubRange([result.g1, result.g2]), hubRange([result.b1, result.b2])], 2,3)
    'grey' : newImageStretch = hubRange([result.g1, result.g2])
  endcase
  self.view.controller.setImageStretch, newImageStretch
end

pro hubTBManagedContentImageViewEventHandler__define
  define = {hubTBManagedContentImageViewEventHandler, inherits hubTBManagedContentMVCViewEventHandler,$
    imageViewMiddleMousePanningHandler:obj_new(),$
    imageViewLeftMousePanningHandler:obj_new(), $
    overviewViewLeftMousePanningHandler:obj_new(),$    
    profileViewLeftMousePanningHandler:obj_new() $
    }
end