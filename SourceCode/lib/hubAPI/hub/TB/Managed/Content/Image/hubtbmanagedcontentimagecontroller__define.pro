function hubTBManagedContentImageController::newView
  return, hubTBManagedContentImageView()
end

pro hubTBManagedContentImageController::setFilename, filename
  self.model.setFilename, filename
end

pro hubTBManagedContentImageController::setBandRGB, bands
  self.model.setBandRGB, bands
end

pro hubTBManagedContentImageController::setBandGrey, band
  self.model.setBandGrey, band
end

pro hubTBManagedContentImageController::setBandClassification
  self.model.setBandClassification
end

function hubTBManagedContentImageController::getBands
  return, self.model.getBands()
end

function hubTBManagedContentImageController::getBandType
  return, self.model.bandType
end

pro hubTBManagedContentImageController::setImageStretch, imageStretch, _EXTRA=_extra
  self.model.setImageStretch, imageStretch, _EXTRA=_extra
end

pro hubTBManagedContentImageController::setImageStretchOnViewExtent, view, percentageStretch, _EXTRA=_extra
  self.model.setImageStretchOnViewExtent, view, percentageStretch, _EXTRA=_extra
end

function hubTBManagedContentImageController::getImageStretch
  return, self.model.getImageStretch()
end

function hubTBManagedContentImageController::getImageStretchPercentage
  return, self.model.getImageStretchPercentage()
end

function hubTBManagedContentImageController::getImageStretchPercentageOnViewExtent
  return, self.model.getImageStretchPercentageOnViewExtent()
end

pro hubTBManagedContentImageController::setImageCenter, imageCenter, _EXTRA=extra
  self.model.setImageCenter, imageCenter, _EXTRA=extra
end

function hubTBManagedContentImageController::getImageCenter
  return, self.model.getImageCenter()
end

pro hubTBManagedContentImageController::setImageZoom, zoomFactor, _EXTRA=_extra
  self.model.setImageZoom, zoomFactor, _EXTRA=_extra
end

function hubTBManagedContentImageController::getImageZoom
  return, self.model.getImageZoom()
end

pro hubTBManagedContentImageController::setImageSelection, imageSelection, _EXTRA=_extra
  self.model.setImageSelection, imageSelection, _EXTRA=_extra
end

function hubTBManagedContentImageController::getImageSelection, IsInsideImage=isInsideImage
  return, self.model.getImageSelection(IsInsideImage=isInsideImage)
end

pro hubTBManagedContentImageController::setImageSelectionByWindowSelection, windowSelection
  self.model.setImageSelectionByWindowSelection, windowSelection
end

function hubTBManagedContentImageController::getImageSize
  return, self.model.getImageSize()
end

pro hubTBManagedContentImageController::setImageLink, imagePositionLinked, imageZoomLinked
  self.model.setImageLink, imagePositionLinked, imageZoomLinked
end

function hubTBManagedContentImageController::getImageZoomLinked
  return, self.model.getImageZoomLinked()
end

function hubTBManagedContentImageController::getImagePositionLinked
  return, self.model.getImagePositionLinked()
end

pro hubTBManagedContentImageController::setSpeclibLinked, speclibContent, linked
  self.model.setSpeclibLinked, speclibContent, linked
end

pro hubTBManagedContentImageController::setProfileSelectionByWindowSelection, windowSelection, view
  self.model.setProfileSelectionByWindowSelection, windowSelection, view
end

function hubTBManagedContentImageController::getProfileSelectionPoint, Index=index
  return, self.model.getProfileSelectionPoint(Index=index)
end

pro hubTBManagedContentImageController::synchronizeLinkedImages
  frameManager = self.getGridFrameManager()
  frameManager.synchronizeLinkedImages, self.mvc
end

pro hubTBManagedContentImageController::synchronizeLinkedSpeclibs
  self.model.synchronizeLinkedSpeclibs
end

pro hubTBManagedContentImageController::copyProfileToLinkedSpeclibs
  self.model.copyProfileToLinkedSpeclibs
end

function hubTBManagedContentImageController::convertPixelToGeo, pixelPosition
  return, self.model.convertPixelToGeo(pixelPosition)
end

function hubTBManagedContentImageController::convertGeoToPixel, geoPosition
  return, self.model.convertGeoToPixel(geoPosition)
end

pro hubTBManagedContentImageController::adjustModelForViewSpecifica, view
  self.model.adjustForViewSpecifica, view
  self.hubTBManagedContentMVCController::adjustModelForViewSpecifica, view
end

function hubTBManagedContentImageController::getView, _EXTRA=_extra
  return, self.model.getGraphicView(_EXTRA=_extra)
end

pro hubTBManagedContentImageController::hideOverview
  oViewOverview = self.getView(/Overview)
  oViewOverview.setProperty, HIDE=1
end

pro hubTBManagedContentImageController::showOverview
  oViewOverview = self.getView(/Overview)
  oViewOverview.setProperty, HIDE=0
end

pro hubTBManagedContentImageController::swapOverview
  oViewOverview = self.getView(/Overview)
  oViewOverview.getProperty, HIDE=hide
  oViewOverview.setProperty, HIDE=~hide
end

pro hubTBManagedContentImageController::hideProfile
  oViewProfile = self.getView(/Profile)
  oViewProfile.setProperty, HIDE=1
end

pro hubTBManagedContentImageController::showProfile
  image = self.getImage()
    oViewProfile = self.getView(/Profile)
  if image.getMeta('bands') gt 1 then begin
    oViewProfile.setProperty, HIDE=0
  endif else begin
    oViewProfile.setProperty, HIDE=1
  endelse
end

pro hubTBManagedContentImageController::swapProfile
  image = self.getImage()
  if image.getMeta('bands') gt 1 then begin 
    oViewProfile = self.getView(/Profile)
    oViewProfile.getProperty, HIDE=hide
    oViewProfile.setProperty, HIDE=~hide
  endif
end

pro hubTBManagedContentImageController::showLabelingTool
  self.model.initLabelingTool
  labelingTool = self.model.getLabelingTool()
  tabFrameManager = self.getTabFrameManager()
  labelingToolFrame = tabFrameManager.addContent(labelingTool, /ShowTitleBar, /ShowDetach)
  self.model.showLabelingTool
  labelingToolFrame.updateContent
end

pro hubTBManagedContentImageController::hideLabelingTool
  labelingTool = self.model.getLabelingTool()
  if obj_valid(labelingTool) then begin
    tabFrameManager = self.getTabFrameManager()
    tabFrameManager.removeContent, labelingTool
    self.model.hideLabelingTool
  endif
end

function hubTBManagedContentImageController::getLabelingTool
  return, self.model.getLabelingTool()
end

function hubTBManagedContentImageController::getImage
  return, self.model.image
end

function hubTBManagedContentImageController::getProfile
  return, self.model.profile
end

function hubTBManagedContentImageController::getFormattedImageZoom
  imageZoom = self.getImageZoom()*100
  
  format = '(f20.4)'
  if imageZoom ge 1 then begin
    format = '(f20.2)'
  endif
  if imageZoom ge 100 then begin
    format = '(i20)'
  endif
  
  result = strcompress(/REMOVE_ALL, string(imageZoom, Format=format))+'% Zoom'
  return, result
end

function hubTBManagedContentImageController::getFormattedImageStretch, ViewExtent=viewExtent
  if keyword_set(viewExtent) then begin
    imageStretch = self.getImageStretchPercentageOnViewExtent()
    result = strcompress(/REMOVE_ALL, string(strjoin(fix(imageStretch),'-')))+'% Stretch on View Extend'
  endif else begin
    imageStretch = self.getImageStretchPercentage()
    result = strcompress(/REMOVE_ALL, string(strjoin(fix(imageStretch),'-')))+'% Stretch'
  endelse
  return, result
end

function hubTBManagedContentImageController::getFormattedImageSelection
  imageSize = self.getImageSize()
  imageSelection = self.getImageSelection(IsInsideImage=isInsideImage)
  if isInsideImage then begin
    result = 'Pixel: '+strjoin(strtrim(floor(imageSelection)+1, 2), ', ')
    image = self.getImage()
    if image.hasMeta('map info') then begin
      geoImageSelection = self.convertPixelToGeo(imageSelection)
      result = [result, 'Map: '+strjoin(strtrim(string(geoImageSelection, FORMAT='(f0.2)'), 2), ', ')]
    endif
  endif else begin
    result = !null
  endelse
  return, result
end

function hubTBManagedContentImageController::getFormattedLabelingModus
  result = 'Labeling Modus: '
  labelingTool = self.getLabelingTool()
  case 1b of
    labelingTool.getMode(/Add) :    (result += 'Add Pixel')
    labelingTool.getMode(/Remove) : (result += 'Remove Pixel')
    labelingTool.getMode(/Off) :    (result += 'Off')
  endcase
  return, result
end

function hubTBManagedContentImageController::getFormattedImageCursorValue
  profile = self.getProfile()
  data = profile.getData()
  if isa(data) then begin
    rgbBands = self.getBands()
    rgbData = data[rgbBands]
    result = [self.getFormattedImageSelection(),$
              'Data: '+strjoin(strtrim(hubString(rgbData),2), ', ')]
    if self.getBandType() eq 'classification' then begin
      image = self.getImage()
      classNames = image.getMeta('class names')
      result[-1] += ' ('+classNames[rgbData]+')'
    endif
  endif else begin
    result = !null
  endelse
  return, result
end

function hubTBManagedContentImageController::getFormattedProfileCursorValue
  profileSelection = self.getProfileSelectionPoint(Index=index)
  bandName = ((self.getImage()).getMeta('band names'))[index]
  result = [self.getFormattedImageCursorValue(),$
            'Wavelength/Value: '+strjoin(strtrim(hubString(profileSelection),2), ', '),$
            'Band Name (Index): '+bandName+' ('+strtrim(index+1,2)+')']
  return, result
end

pro hubTBManagedContentImageController::drawViewToClipboard, view
  self.model.drawViewToClipboard, view
end

pro hubTBManagedContentImageController::saveViewToFile, view, filename, format, Open=open
  self.model.saveViewToFile, view, filename, format
  if keyword_set(open) then begin
    hubHelper.openFile, filename
  endif
end

pro hubTBManagedContentImageController::removeDependencies
  self.hideLabelingTool
  self.model.removeDependencies
end

pro hubTBManagedContentImageController__define
  define = {hubTBManagedContentImageController, inherits hubTBManagedContentMVCController}
end