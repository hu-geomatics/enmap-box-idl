pro debug_hubTB
 
  toolbox = hubTBToolbox(Title='Test Toolbox', /Column, XRegisterName='testTB')
  frame1 =hubTBFrame(toolbox, Title='Frame 1', /SHOWTITLEBAR, /FRAME)
  frame11 =hubTBFrame(frame1, Title='Frame 11', /SHOWTITLEBAR, /FRAME)
  frame12 =hubTBFrame(frame1, Title='Frame 12', /SHOWTITLEBAR, /FRAME)
  for i=1,9 do !null = hubTBFrame(frame12, Title='Frame 12'+strtrim(i,2), /SHOWTITLEBAR, /FRAME)
  frame2 =hubTBFrame(toolbox, Title='Frame 2', /SHOWTITLEBAR, /FRAME)
 
  
  
;  frame11 = hubTBFrame(frame1, Title='Filelist', /FRAME);, /ShowButtonRight);, IsFixedSize=[1,0], FixedSize=[300,0])
;  frame12 = hubTBFrame(frame1, /Column)
;  frame2 = hubTBFrame(toolbox, Title='Frame 2', /FRAME);, /ShowButtonUp);, IsFixedSize=[0,1], FixedSize=[0,300])
;  tabFrameManager = hubTBFrameTabManager(frame2, /FRAME)
;  gridFrameManager = hubTBFrameGridManager(frame12, tabFrameManager, Title='',/FRAME)
  toolbox.start

  ; add menu
  menuFiles = list()
  menuFiles.add, filepath('enmapInit.men', ROOT=enmapBox_getDirname(/ENMAPBOX), SUBDIR='resource')
  toolbox.createMenu, menuFiles


  ss = GET_SCREEN_SIZE()
  
  toolbox.setOffset, [0,0]
;  toolbox.setSize, ss*0.5, /Update ; start
;  toolbox.setSize, ss*0.75, /Update ; ziel
;  toolbox.setSize, ss*0.75+0.1, /Update ; ziel+delte
;  toolbox.setSize, ss*0.75+0.11, /Update ; ziel+delte
;  toolbox.setSize, ss*0.75+0.12, /Update ; ziel+delte
;  toolbox.setSize, ss*0.75+0.13, /Update ; ziel+delte
;  toolbox.setSize, ss*0.75+0.14, /Update ; ziel+delte

  toolbox.setSize, [600,500], /Update ; ziel
;  
;tlb = toolbox.getTopLevelBase()
;print,tlb
;widget_control, tlb, Map=0
;;widget_control, tlb, /CLEAR_EVENTS
;widget_control, tlb, Map=1
;;widget_control, tlb, /CLEAR_EVENTS

return
  ; - filelist
  filelist = hubTBContentFilelist(toolbox)
  toolbox.showContentInFrame, filelist, frame11
  testFile = filepath(/TMP, 'test.txt')
  hubIOASCIIHelper.writeFile, testFile, ['dummy']
  filelist.openFile, testFile, /EXTENSION

  ; - console
;  console = hubTBContentConsole(toolbox)
;  toolbox.showContentInFrame, console, frame11

;  toolbox.setSize, GET_SCREEN_SIZE()*0.9, /Update
;  toolbox.setSize, GET_SCREEN_SIZE()*0.5, /Update
  toolbox.updateView

end

