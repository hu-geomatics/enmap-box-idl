function hub_getDirectoryImageFilenames, dirname, Recursive=recursive

  if keyword_set(recursive) then begin
    result = list(file_search(dirname, '*.hdr'), /EXTRACT)
  endif else begin
    result = list(file_search(dirname+'*.hdr'), /EXTRACT)
  endelse
  foreach filename, result, i do begin
    if strcmp(file_basename(filename), 'pyramid.', 8) then begin
      result[i] = ''
    endif else begin
      result[i] = hubProEnvEnviHelper.findDataFile(filename)
    endelse
  endforeach
  removeIndices = result.where('')
  if isa(removeIndices) then result.remove, removeIndices
  return, result.toArray()
  
end

pro test_hub_getDirectoryImageFilenames
  print, hub_getDirectoryImageFilenames(hub_getDirname(), /RECURSIVE)
end