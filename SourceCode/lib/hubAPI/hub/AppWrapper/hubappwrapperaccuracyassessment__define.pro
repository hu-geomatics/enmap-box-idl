function hubAppWrapperAccuracyAssessment::init, labelType, title, groupLeader
  self.labelType = labelType
  self.pTitle = ptr_new(title)
  self.pGroupLeader = ptr_new(groupLeader)
  self.timestamp = strtrim(long(systime(1)),2)
  return, 1b
end

function hubAppWrapperAccuracyAssessment::getLabelType
  return, self.labelType
end

function hubAppWrapperAccuracyAssessment::getTitle
  return, *self.pTitle
end

function hubAppWrapperAccuracyAssessment::getGroupLeader
  return, *self.pGroupLeader
end

function hubAppWrapperAccuracyAssessment::isClassification
  return, self.labelType eq 'classification'
end

function hubAppWrapperAccuracyAssessment::isRegression
  return, self.labelType eq 'regression'
end

pro hubAppWrapperAccuracyAssessment::checkParameters, parameters
  parameters.hubCheckKeys, ['modelFilename', 'featureFilename', 'labelFilename']
end

function hubAppWrapperAccuracyAssessment::getParameters

  hubAMW_program, self.getGroupLeader(), Title=self.getTitle()
  hubAMW_frame, Title='Input'
  hubAMW_inputFilename, 'modelFilename', TSize=self.getGuiTSize(), $
    Title=self.getGUIModelTitle(),$
    Extension=self.getGuiModelExtension(),$
    Value=self.getGUIModelDefaultFilename()
  hubAMW_inputSampleSet, 'sampleSet', TSize=self.getGuiTSize(), Title='Image', ReferenceTitle='Reference Areas' $
    , classification=self.isClassification(), regression=self.isRegression() 
  parameters = hubAMW_manage(/Flat)

  if parameters['accept'] then begin 
    parameters['featureFilename'] = parameters.remove('sampleSet_featureFilename')
    parameters['labelFilename']  = parameters.remove('sampleSet_labelFilename')
  endif else begin
    parameters = !null
  endelse  
  
  return, parameters
end

function hubAppWrapperAccuracyAssessment::getGuiModelTitle
  return, 'Model'
end

function hubAppWrapperAccuracyAssessment::getGuiModelExtension
  return, !null
end

function hubAppWrapperAccuracyAssessment::getGuiModelDefaultFilename
  return, !null
end

function hubAppWrapperAccuracyAssessment::getGuiTSize
  return, 150
end

pro hubAppWrapperAccuracyAssessment::processing, parameters

  self.checkParameters, parameters
  applyParameters = parameters[*]
  applyParameters['outputLabelFilename'] = filepath(/TMP, 'estimation_AccuracyAssessmentWrapper'+self.timestamp)
  self.apply, applyParameters, Title=self.getTitle(), GroupLeader=self.getGroupLeader()

  accAssParameters = hash()
  accAssParameters['inputReference'] = parameters['labelFilename']
  accAssParameters['inputEstimation'] = applyParameters['outputLabelFilename']

  performance = hubApp_accuracyAssessment_processing(accAssParameters)
  
  report = hubApp_accuracyAssessment_getReport(performance, /CreatePlots)
  report.SaveHTML, /show
  
  file_delete, applyParameters['outputLabelFilename'], applyParameters['outputLabelFilename']+'.hdr'
end

pro hubAppWrapperAccuracyAssessment__define
 struct = {hubAppWrapperAccuracyAssessment $
   , inherits IDL_Object $
   , pGroupLeader : ptr_new() $
   , labelType : '' $
   , pTitle : ptr_new() $
   , timestamp : '' $
   }
end
