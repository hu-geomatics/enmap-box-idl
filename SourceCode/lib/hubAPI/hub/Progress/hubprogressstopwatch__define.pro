;+
; A hubProgressStopWatch can be used to stop the time required to run certain process.
; 
; .. image:: _IDLDOC\hubProgressStopWatch1.png
; 
; :Examples:
;   Here you can see the most simpliest way to use a `hubProgressStopWatch` ::
;   
;     watch = hubProgressStopWatch()
;     wait, 3.24 ;this is supposed to simulate a "time consuming process"
;     watch.showResults, title='My App', description=['All operations done','No errors occured']
;   
;   If you like, you can take some split times::
;   
;     watch = hubProgressStopWatch()
;     wait, 1
;     watch.takeSplitTime
;     wait, 1.5
;     watch.takeSplitTime, 'second split time'
;     wait, 0.5
;     watch.showResults, title='My App', description=['All operations done','No errors occured']
; 
;   It is possible to control the start and stop of the watch explicitely::
;   
;     watch = hubProgressStopWatch()
;     ;do something, e.g. wait 1 second
;     wait, 1
;     watch.start 
;     wait, 3.5 ;do something else
;     watch.stop
;     ;and again do something
;     wait, 2
;      
;     ;print the duration between start and stop
;     timeSpan = watch.getStopTime() - watch.getStartTime()
;     print, watch.getTimeSpanString(timeSpan, SHOWMILLISECONDS=1)
;
;    
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
; 	
;-

;+
; :Description:
;    Constructor that returns a `hubProgressStopWatch` object.
;    The `hubProgressStopWatch::start` routine is called implicitely.
;
;-
function hubProgressStopWatch::init, showSplitTimes=showSplitTimes
  self.showSplitTimes = keyword_set(showSplitTimes)
  self.start
  return, 1b
  
end
;+
; :Description:
;    Starts the stop watch and resets the list of split times.
;-
pro hubProgressStopWatch::start
  self.startTime = systime(/seconds)
  self.splitTimes = list() 
  self.stopTime = -1.
end


;+
; :Description:
;   Returns the last time `hubProgressStopWatch::start` was called.
;   Please note that this is done implicitely when the `hubProgressStopWatch` is created.
;-
function hubProgressStopWatch::getStartTime
  return, self.startTime
end
;+
; :Description:
;   Stops the watch. 
;-
pro hubProgressStopWatch::stop
  self.stopTime = systime(/seconds)
end 

;+
; :Description:
;    Return the last time when `hubProgressStopWatch::stop` was called or
;    `!NULL` if this routine was never called before.
;-
function hubProgressStopWatch::getStopTime   
  return, self.stopTime lt 0 ? !NULL : self.stopTime
end


;+
; :Description:
;    Takes a split time.
;
; :Params:
;    description: in, optional, type = string
;       A description related to this split time.
;
;
;-
pro hubProgressStopWatch::takeSplitTime, description
  if self.stopTime gt 0 then message, 'Watch was already stopped'
 
  (self.splitTimes).add, {time:systime(1) $
                         ,description: isa(description) ? string(description) : '' $
                         }
  if self.showSplitTimes then print, description

end


;+
; :Description:
;    Returns a list of all split times taken after the last call of `hubProgressStopWatch:start`.
;    
;    The list items are structs of form::
;      {time:        <double systime value>
;       description: <string with splittime description>
;      }
;
;
;
;-
function hubProgressStopWatch::getSplitTimes
  return, list(self.splitTimes, /EXTRACT)

end

;+
; :Description:
;    Creates a '000:00:00' styled string out of a time difference given in seconds. 
;    
; :Params:
;    timeSpan: in, required, type = double
;     The time difference in seconds.
;
; :Keywords:
;    showMilliSeconds: in, optional, type = boolean
;     Set this to show the elapsed milliseconds too.
;-
function hubProgressStopWatch::getTimeSpanString, timeSpan, showMilliSeconds=ShowMilliSeconds
  rest = timeSpan
  hours = floor(rest / (60*60))
  rest -= hours * 60 * 60
  minutes = floor(rest / 60)
  rest -= minutes * 60
  
  result = ''
  if keyword_set(showMilliSeconds) then begin
    result = string(format='(%"%3i:%2.2i:%-7.4f")', hours, minutes, rest)
  endif else begin
    result = string(format='(%"%3i:%2.2i:%2.2i")', hours, minutes, rest)
  endelse
  
  return, result
end



;+
; :Description:
;    Opens a information dialog to show the time span until this routine was called.
;
; :Keywords:
;    title: in, optional, type = string
;     Set this to specify the dialog title, e.g. 'My Application'
;     
;    description: in, optional, type = string | string[]
;     Set this to specify some additional text to be shown in the dialog.
;
;    showMilliSeconds: in, optional, type = boolean
;     Set this to show the elapsed milliseconds too.
;-
pro hubProgressStopWatch::showResults, title=title, description=description, showMilliSeconds=ShowMilliSeconds
  if self.stopTime lt 0 then self.stop
  
  info = list()
  if isa(description) then begin
    foreach line, description do info.add, line
    info.add, '' ;empty line for separation
  endif
  info.add, 'Time needed: '+self.getTimeSpanString(self.stopTime - self.startTime, showMilliSeconds=ShowMilliSeconds)
  
  if n_elements(self.splitTimes) gt 0 then begin
    info.add, 'Split Times:'
    foreach s, self.splitTimes, i do begin
      info.add, string(format='(%"  %s %s")' $
                 , self.getTimeSpanString(s.time - self.startTime, showMilliSeconds=ShowMilliSeconds) $
                 , s.description $
                 )
    endforeach
  endif
  
  !NULL = dialog_message(info.toArray(),/INFORMATION,TITLE=title)
end

function hubProgressStopWatch::getResultTable
  stopTime = self.stopTime gt 0 ? self.stopTime : systime(1) 
  sTimes = list()
  sNames = list()
  foreach sTime, self.splittimes do begin
    sTimes.add, stime.time
    sNames.add, stime.description
  endforeach
  sTimes.add, stopTime
  sNames.add, 'Stop'
  
  sTimes = sTimes.toArray()
  sNames = sNames.toArray()
  n = n_elements(sTimes)
  aTimes = sTimes - self.startTime
  dTimes = [aTimes[0],aTimes[1:n-1] -aTimes[0:n-2]   ]
  
  table = hubReportTable()
  table.addColumn,aTimes, name='t'
  table.addColumn,dTimes, name='dt'
  table.addColumn,sNames, name='description',FORMATSTRING='(%"%s")' 
  
  return, table.getFormatedASCII(colSep=' ')
end

;+
; :Description:
;    Describe the procedure.
;
;-
function hubProgressStopWatch::_overloadPrint
  info = list()
  info.add, 'hubProgressStopWatch'
  info.add, self.getResultTable(), /Extract
  return,strjoin(info.toArray(),string([10b,13b]))
end

;+
; :hidden:
;-
pro hubProgressStopWatch__define
  struct = {hubProgressStopWatch $
    , inherits IDL_Object $
    , startTime :0d $
    , stopTime :0d $
    , description: '' $
    , showSplitTimes:0b $
    , splitTimes : obj_new() $
  }
end

;+
; :hidden:
;-
pro test_hubProgressStopWatch
  ;watch = hubProgressStopWatch()
  ;wait, 3.24 ;this is supposed to simulate a "time consuming process"
  ;watch.showResults, title='My App', description=['All operations done','No errors occured']
  
  
  watch = hubProgressStopWatch()
  print, watch.getTimeSpanString(60*60)
  wait, 1
  watch.takeSplitTime
  wait, 1.5
  watch.takeSplitTime, 'second split time'
  wait, 0.5
  print, watch.getResultTable()
  watch.showResults, title='My App', description=['All operations done','No errors occured']

  watch = hubProgressStopWatch()
  ;do something, e.g. wait 1 second
  wait, 1
  watch.start 
  wait, 3.5 ;do something else
  watch.stop
  ;and again do something
  wait, 2
  
  ;print the duration between start and stop
  timeSpan = watch.getStopTime() - watch.getStartTime()
  print, watch.getTimeSpanString(timeSpan, SHOWMILLISECONDS=1)
 
  
  
end