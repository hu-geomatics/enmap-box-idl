function hubProgress, text, i,n
  if isa(n) then begin
    return, text+' '+strtrim(i,2)+'/'+strtrim(n,2)
  endif else begin
    return, text+' '+strtrim(round(i*100),2)+'%'
  endelse
end

pro hubProgress, text, i,n
  print, hubProgress(text, i,n)
end


pro test_hubProgress
  print, hubProgress('done', 1./10)
  hubProgress, 'done', 1./10
end