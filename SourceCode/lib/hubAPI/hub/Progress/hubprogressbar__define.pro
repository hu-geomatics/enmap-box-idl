;+
; Class for creating a graphical progress bar.
;
; .. image:: _IDLDOC/hubProgressBar.gif
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; 
; :Examples:
;    Usage example::
;    
;      ; create progress bar with CANCEL button
;    
;      progressBar = hubProgressBar(Title='MyProgressBar', /Cancel)
;    
;      ; do some initialization stuff
;      
;      progressBar.setInfo, ['init processing', ' - note this', ' - note that']
;      wait, 3
;    
;      ; do some processing stuff
;      
;      progressBar.setInfo, ['do the processing', 'input file: '+filepath('input', /TMP), 'output file: '+filepath('output.log', /TMP)]
;      
;      numberOfThingsDoTo = 123
;      for i=0, numberOfThingsDoTo-1 do begin &$
;        
;        ; show progress
;        
;        progressBar.setProgress, float(i)/numberOfThingsDoTo &$
;        
;        ; do something
;        
;        wait,0.05 &$
;    
;      endfor
;    
;      ; hide the bar and do some cleanup stuff
;      
;      progressBar.hideBar
;      progressBar.setInfo, ['finish processing']  
;      wait,3
;    
;      ; destroy the progress bar
;      
;      progressBar.cleanup
;      
;-

;+
; :Description:
;    Object contructor.
;
; :Keywords:
;    Cancel: in, optional, type=boolean, default=1
;      Adds a cancel button to the progress bar. 
;      If the button is pressed the "huberror-ignore" error is thrown, 
;      which will be ignored silently by `@huberrorcatch`.
;      
;    GroupLeader: in, optional, type=top level base
;      Group leader for the widget.
;      
;    Info: in, optional, type=string[]
;      Info message. see `info`.
;       
;    NoShow: in, optional, type=boolean
;      The whole progress bar is not shown on the screen.
;       
;    Range: in, optional, type=float[2]
;      Set the possible progress range that is used to calculate the percental progress. 
;      Default range is [0,1].
;      
;    Title: in, optional, type=string
;      Window title.
;
;-
function hubProgressBar::init $
  , Cancel=cancel $
  , GroupLeader=groupLeader $
  , Info=info $
  , NoShow=noShow $
  , Range=range $
  , Title=title
  
  if isa(groupLeader) then begin
    widget_control, groupLeader, /CLEAR_EVENTS
  endif
  widget_control, /HOURGLASS
  
  
  self.noShow = keyword_set(noShow)
  self.setRange, isa(range) ? range : [0,1]
  self.initGUI, title, info, groupLeader, (isa(cancel) ? cancel : 1)
  self.setProgress, self.range[0]
  self.hideBar

  return, 1b
  
end

;+
; :Hidden:
;-
pro hubProgressBar::initGUI, title, info, groupLeader, cancel

  if self.noShow then return

  self.tlb = widget_base(MODAL=isa(group_leader), GROUP_LEADER=group_leader, $
      /COLUMN, TITLE='', TLB_FRAME_ATTR=2)
  self.barSize = [300,20]
  baseDummy = widget_base(self.tlb, XPAD=0, YPAD=0, SPACE=0)
  self.barDraw = widget_draw(baseDummy, XSIZE=self.barSize[0], YSIZE=self.barSize[1], graphics_level=2)
  self.barText = widget_text(self.tlb, VALUE='', SCR_XSIZE=self.barSize[0], YSIZE=2)
 
  
  if keyword_set(cancel) then begin
    self.barCancel = widget_button(self.tlb, Value='Cancel', UVALUE=self)
  endif
  self.setTitle, isa(title) ? title : 'Progress Bar'
  self.setInfo, isa(info) ? info : 'please wait'
  (hubGUIHelper()).centerTLB, self.tlb
  widget_control, self.tlb, /REALIZE
  xmanager, 'hubProgressBar', self.tlb, EVENT_HANDLER='hubObjCallback_event', NO_BLOCK=1


  ;init plot view
  widget_control, self.barDraw, GET_VALUE=w
  

  view = IDLgrView(Viewplane_rect=[0,0,1,1], Color=[126,126,126])
  model = IDLgrModel()
  
  self.grView = view
  self.grWindow = w
  
  Case !VERSION.OS_FAMILY of
    'Windows' : font = IDLgrFont('arial*bold')
    else : ;system default
  Endcase

  self.grBar  = IDLgrPolygon([0,80,80,0],[0,0,1,1], COLOR=[0,0,255])
  self.grText = IDLgrText('0 %', color=[255,255,255] $
      ,ALIGNMENT=0.5, VERTICAL_ALIGNMENT=0.5, Locations=[.5,.5] $
      ,RECOMPUTE_DIMENSIONS=0, RENDER_METHOD=0, Font=font)
  
  
  model.add, self.grText
  model.add, self.grBar
  
  view.add, model
  
  w.draw, view

end

;+
; :Description:
;    Set window title.
;	
; :Params:
;    title: in, required, type=string
;      'Window title.
;-
pro hubProgressBar::setTitle, title
  if self.noShow then return
  widget_control, self.tlb, TLB_SET_TITLE=title
  self.checkCancel
end

;+
; :Description:
;    Set info message.
; 
; :Params:
;    info: in, required, type=string
;      'Info message.
;-
pro hubProgressBar::setInfo, info
  self.checkCancel
  if self.noShow then return
  widget_control, self.barText, YSIZE=n_elements(info)
  widget_control, self.barText, SET_VALUE=transpose(info[*])
  self.checkCancel
end

;+
; :Description:
;    Set progress.
; 
; :Params:
;    progress: in, required, type=float
;      'The progress will be converted to percental progress in accordance to the given progress range (see `range`)::
;        
;                        progress - range[0]
;        progress in % = -------------------
;                        range[1] - range[0]
;-
pro hubProgressBar::setProgress, progress, Hide=hide
  if self.noShow then return
  if keyword_set(hide) then begin
    self.hideBar
    return
  endif
  self.progress = progress
  self.showBar
  span = (self.range[1]-self.range[0])
  progressFraction = (progress-self.range[0])
  if span gt 0 then progressFraction /= span
  progressFraction >= 0.
  progressFraction <= 1.
  
  p = progressFraction
  (self.grBar).setProperty, data=transpose([[0, p, p, 0], [0,0,1,1]]) 
  (self.grText).setproperty, STRINGS=strtrim(floor(progressFraction*100),2)+'%'
  (self.grWindow).draw, self.grView
  self.checkCancel
  
end

function hubProgressBar::getProgress
  return, self.progress
end

pro hubProgressBar::checkCancel
  hubCallBackCheckID = self.barCancel
  @hubcallbackcheck
end

;+
; :Description:
;    Set progress range.
; 
; :Params:
;    range: in, required, type=float[2]
;      Set the possible progress range that is used to calculate the percental progress. 
;      Default range is [0,1].
;-
pro hubProgressBar::setRange, range
  self.range = float(range)
end

;+
; :Description:
;    Hides the blue bar that shows the percental progress.
;-
pro hubProgressBar::hideBar
  if self.noShow then return
  widget_control, self.barDraw, MAP=0, SCR_YSIZE=1
end

;+
; :Description:
;    Shows the blue bar that shows the percental progress.
;-
pro hubProgressBar::showBar
  if self.noShow then return
  widget_control, self.barDraw, MAP=1, SCR_YSIZE=self.barSize[1]
end

;+
; :Hidden:
;-
pro hubProgressBar::handleEvent, event

  case event.id of
    self.barCancel : begin
      obj_destroy, self
      if (hubDev_isRTSession() and n_elements(scope_traceback()) gt 4) $
      or (~hubDev_isRTSession() and n_elements(scope_traceback()) gt 3) then begin
        message, 'huberror-ignore', /NONAME
      endif else begin
        return
      endelse
    end
  endcase
  
end

;+
; :Description:
;    Destroys the widget window.
;-
pro hubProgressBar::cleanup
  if self.noShow then return
  widget_control, self.tlb, /DESTROY

end

;+
; :Hidden:
;-
pro hubProgressBar__define
  struct = {hubProgressBar $
    , inherits IDL_Object $
    , tlb : 0L $
    , noShow : 0b $
    , barText : 0L $
    , barDraw : 0L $
    , barCancel : 0L $
    , barSize : [0,0] $
    , range : [0.,0.] $
    , progress:0d $
    , grBar : obj_new() $
    , grText : obj_new() $
    , grWindow : obj_new() $
    , grView : obj_new() $
  }
end

;+
; :Hidden:
;-
pro test_hubProgressBar 

  progressBar = hubProgressBar(Title='MyProgressBar');, /Cancel)
  progressBar.setRange, [0,10]
  for i=1,10 do begin
    wait, 0.5
    progressBar.setProgress, i
  endfor
  
  stop
  @huberrorcatch
 
  progressBar = hubProgressBar(Title='MyProgressBar');, /Cancel)

  ; do some initialization stuff
  
  progressBar.setInfo, ['init processing', ' - note this', ' - note that']
;  wait, 3

  ; do some processing stuff
  
  progressBar.setInfo, ['do the processing', 'input file: '+filepath('input', /TMP), 'output file: '+filepath('output.log', /TMP)]
  
  numberOfThingsDoTo = 2
  for i=0, numberOfThingsDoTo-1 do begin &$
    
    ; show progress
    
    progressBar.setProgress, float(i)/numberOfThingsDoTo &$
    
    ; do something
    
    wait,0.05 &$
    print,i
     progressBar2 = hubProgressBar(Title='MyProgressBar');, /Cancel)
      for k=0, numberOfThingsDoTo-1 do begin &$
        
        ; show progress
        
        progressBar2.setProgress, float(k)/numberOfThingsDoTo &$
        
        ; do something
        
        wait,0.1 &$
        
         
      endfor
     progressBar2.cleanup
  endfor

  ; hide the bar and do some cleanup stuff
  
  progressBar.hideBar
  progressBar.setInfo, ['finish processing']  
  wait,3

  ; destroy the progress bar
  
  progressBar.cleanup
 
end