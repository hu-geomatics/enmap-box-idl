;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the OS shortname of your system::
;    
;      shortname | system
;      ----------|----------------------------------------------------
;      lin32     | Linux machines running IDL in 32-bit mode.
;      lin64     | Linux machines running IDL in 64-bit mode.
;      macint32  | Intel-based Macintoshes running IDL in 32-bit mode.
;      macint64  | Intel-based Macintoshes running IDL in 64-bit mode.
;      sun32     | Solaris Sparc machines running IDL in 32-bit mode.
;      sun64     | Solaris Sparc machines running IDL in 64-bit mode.
;      sunx86_64 | Solaris X86 machines running IDL in 64-bit mode.
;      win32     | Windows machines running IDL in 32-bit mode.
;      win64     | Windows machines running IDL in 64-bit mode.
;
;-
function hubDev_getOSName

  case !version.os of
    'Win32': $
      begin
        if !version.memory_bits eq 32 then result = 'win32'
        if !version.memory_bits eq 64 then result = 'win64'
      end
    'darwin': $
      begin
        if !version.arch eq 'ppc' then begin
          if !version.memory_bits eq 32 then result = 'macppc32'
          if !version.memory_bits eq 64 then result = 'macppc64'
        endif
        if !version.arch eq 'i386' then begin
          if !version.memory_bits eq 32 then result = 'macint32'
          if !version.memory_bits eq 64 then result = 'macint64'
        endif
        if !version.arch eq 'x86_64' then begin
          if !version.memory_bits eq 32 then result = 'macint32'
          if !version.memory_bits eq 64 then result = 'macint64'
        endif
      end
    'linux': $
      begin
        if !version.memory_bits eq 32 then result = 'lin32'
        if !version.memory_bits eq 64 then result = 'lin64'
      end
    'sunos': $
      begin
        if !version.arch eq 'x86_64' then result = 'sunx86_64'
        if !version.arch eq 'sparc' && !version.memory_bits eq 32 then result = 'sun32'
        if !version.arch eq 'sparc' && !version.memory_bits eq 64 then result = 'sun64'
      end
  endcase
  
  return, result
end