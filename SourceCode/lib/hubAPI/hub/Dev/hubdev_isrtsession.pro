;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns 1 if IDL runs in Runtime or Virtual Machine Mode, and 0 otherwise.
;-
function hubDev_isRTSession
  traceback = scope_traceback()
  result = traceback[1] eq 'IDLRTMAIN'
  return, result
end