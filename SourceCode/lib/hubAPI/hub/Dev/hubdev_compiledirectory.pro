;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this procedure to compile and store IDL routines of a specific source code directory
;    to an IDL SAVE files. 
;
; :Params:
;    directories: in, required, type=array of directories
;      Use this keyword to specify a directory or an array of directories holding *.pro files
;      that you want to compile and store to the SAVE file.
;      
;    savFilename: in, required, type=filename
;      Filename of the SAVE file.
;
; :Keywords:
;    Resolve: in, optional, type=boolean
;      Set this keyword to call the IDL function `resolve_all` after compiling all *.pro files and
;      before creating the SAVE file. 
;      
;    NoLog: in, optional, type=boolean
;      Set this keyword, if you do not want to see the logfile.
;
; :Examples:
;    Save all hubAPI routines to a SAVE file::
;      codeDirectory = hub_getDirname(/SourceCode)
;      savFilename = filepath('allHubAPIRoutines', /TMP)
;      hubDev_compileDirectory, codeDirectory, savFilename
;
;-
pro hubDev_compileDirectory, directories, savFilename, Resolve=resolve, NoLog=noLog, CompileFirst=compileFirst, CompileNot=compileNot
  
  
  if ~isa(NoLog) then noLog = 0b 
  
  compile_opt strictarr

  files = list()
  for i=0,n_elements(directories)-1 do begin
    directory = directories[i]
    files.add, file_search(directory, '*.pro'), /EXTRACT
  endfor
  
  if isa(compileFirst) then begin
    files.add, compileFirst, 0, /EXTRACT
  endif
  
  if isa(compileNot) then begin
    foreach unwantedFile,compileNot do begin
      index = files.where(compileNot)
      if ~isa(index) then message, 'File mot found.'
      files.remove, index
    endforeach
  endif
 

  file_mkdir, file_dirname(savFilename)
  logFilename = savFilename+'.log'
  file_delete, logFilename, /ALLOW_NONEXISTENT

  ; compile all routines in a fresh idl session
  
  oBridge = obj_new('IDL_IDLBridge',OUTPUT=logFilename)
  oBridge->SetVar, 'pathArray', strsplit(!path, path_sep(/Search_path), /Extract)
  oBridge->execute, "!path = strjoin(pathArray, path_sep(/Search_path))"
  for i=0,n_elements(files)-1 do begin
    file = files[i]
    catch, errorStatus
    if (errorStatus ne 0) then begin
      catch, /CANCEL
      continue
    endif
    oBridge->execute,  ".compile '"+file+"'"
    catch, /CANCEL
  endfor
  
  oBridge->execute, keyword_set(resolve) ? "resolve_all" : ""
  oBridge->execute, "save, /ROUTINES, FILENAME='"+savFilename+"'"
  oBridge->execute, "print, 'SAVE file created: '+'" +savFilename
  oBridge->cleanup

  ; show log output
  
  if ~keyword_set(noLog) then begin
    hubHelper.openFile, /ASCII, logFilename
  endif
end