;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Calls the object widget realizeNotify method.
;
; :Params:
;    widgetID: in, required, type=widget ID
;      Widget ID.
;-
pro hubObjCallback_realizeNotify, widgetID

  object = hubObjCallback_getObjectReference(widgetID)
  if isa(object) then object.realizeNotify, widgetID
  
end