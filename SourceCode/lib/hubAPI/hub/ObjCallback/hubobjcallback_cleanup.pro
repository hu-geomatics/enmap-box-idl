;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Calls the object widget cleanup method.
;
; :Params:
;    widgetID: in, required, type=widget ID
;      Widget ID.
;-
pro hubObjCallback_cleanup, widgetID

  object = hubObjCallback_getObjectReference(widgetID)
  if isa(object) then object.cleanup
  
end