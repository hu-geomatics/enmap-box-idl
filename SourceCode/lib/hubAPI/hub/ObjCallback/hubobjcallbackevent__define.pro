function hubObjCallbackEvent::init, object, name, parameters
  self.object = object
  self.name = name
  self.parameters = ptr_new(parameters)
  return, 1b
end

pro hubObjCallbackEvent::setEvent, event
  self.event = ptr_new(event)
  case strlowcase(typename(event)) of
    'widget_base' : begin
      category = 'base event'
    end
    'widget_button' : begin
      category = 'button event'
      case event.select of
        0 : subcategory = 'released event'
        1 : subcategory = 'pressed event'
      endcase
    end
    'widget_context' : begin
      case strlowcase(widget_info(/NAME, event.id)) of
        'tree' : category = 'tree event'
        'table' : category = 'table event'
        else : stop
      endcase
      subcategory = 'context event'
    end
    'widget_draw' : begin
      category = 'window event'
      case event.type of
        0 : begin
          subcategory = 'button pressed event'
          case event.press of
            0 : subsubcategory = 'unknown '+subcategory
            1 : subsubcategory = 'left mouse '+subcategory
            2 : subsubcategory = 'middle mouse '+subcategory
            4 : subsubcategory = 'right mouse '+subcategory
          endcase
        end
        1 : begin
          subcategory = 'button released event'
          case event.release of
            0 : subsubcategory = 'unknown '+subcategory
            1 : subsubcategory = 'left mouse '+subcategory
            2 : subsubcategory = 'middle mouse '+subcategory
            4 : subsubcategory = 'right mouse '+subcategory
          endcase
        end
        2 : subcategory = 'motion event'
        3 : subcategory = 'viewport moved event'
        4 : subcategory = 'visibility changed event'
        5 : begin
          if event.press then subcategory = 'ascii key pressed event'
          if event.release then subcategory = 'ascii key released event'
        end
        6 : begin
          if event.press then subcategory = 'non-ascii key pressed event'
          if event.release then subcategory = 'non-ascii key released event'
        end
        7 : subcategory = 'wheel scrolled event'
      endcase
    end
    'widget_drop' : begin
      category = 'drop event'
      self.information = self.getDragAndDropInformation()
    end
    'widget_kill_request' : begin
      category = 'kill request event'
    end
    'widget_table_cell_sel' : begin
      category = 'table event'
      subcategory = 'cell selection event'
    end
    'widget_table_ch' : begin
      category = 'table event'
      subcategory = 'insert character event'
    end
    'widget_table_text_sel' : begin
      category = 'table event'
      subcategory = 'text selection event'
    end
    'widget_table_del' : begin
      category = 'table event'
      subcategory = 'delete event'
    end
    'widget_table_col_width' : begin
      category = 'table event'
      subcategory = 'column width changed event'
    end
    'widget_table_str' : begin
      category = 'table event'
      subcategory = 'insert string event'
    end
    'widget_timer' : begin
      category = 'timer event'
    end
    'widget_tracking' : begin
      case strlowcase(widget_info(/NAME,event.id)) of
        'draw' : category = 'window event'
        'tree' : category = 'tree event'
        else : stop
      endcase
      subcategory = 'tracking event'
    end
    'widget_tree_sel' : begin
      category = 'tree event'
      subcategory = 'selection event'
    end
    'widget_tree_expand' : begin
      category = 'tree event'
      subcategory = 'expand event'
    end
    'widget_tree_checked' : begin
      category = 'tree event'
      subcategory = 'checked event'
    end
    
        
    else : begin
      print, typename(event)
      stop
    end
  endcase
  
  if ~isa(subcategory) then subcategory = category
  if ~isa(subsubcategory) then subsubcategory = subcategory
  self.category = category
  self.subcategory = subcategory
  self.subsubcategory = subsubcategory
end

pro hubObjCallbackEvent::handleEvent, event
  self.setEvent, event
  self.object.handleEvent, self
end

function hubObjCallbackEvent::isTimerEvent
  return, isa(*self.event, 'WIDGET_TIMER')
end

function hubObjCallbackEvent::getDragAndDropInformation
  result = hash()
  result['dragID'] = (*self.event).drag_id
  result['dropID'] = (*self.event).id
  widget_control, result['dragID'], GET_UVALUE=dragObjEvent
  result['dragObjEvent'] = dragObjEvent
  return, result
end

pro hubObjCallbackEvent::getProperty, object=object, parameters=parameters, event=event, name=name, typename=typename,$
  category=category, subcategory=subcategory, subsubcategory=subsubcategory, information=information
  if arg_present(object) then object = self.object
  if arg_present(parameters) then parameters = *self.parameters
  if arg_present(event) then event = *self.event
  if arg_present(name) then name = self.name
  if arg_present(typename) then typename = strlowcase(typename(*self.event))
  if arg_present(category) then category = self.category
  if arg_present(subcategory) then subcategory = self.subcategory
  if arg_present(subsubcategory) then subsubcategory = self.subsubcategory
  if arg_present(information) then information = self.information.toStruct()

end

function hubObjCallbackEvent::_overwritePrint
  help, event, OUTPUT=eventText 
  return, eventText
end

pro hubObjCallbackEvent__define

  define = {hubObjCallbackEvent, inherits IDL_Object, $
    object : obj_new(), $
    name : '',$
    parameters : ptr_new(), $
    event : ptr_new(),$
    category : '',$
    subcategory : '',$
    subsubcategory : '',$
    information:hash()}
  
end

