;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Calls the object widget handleEvent method.
;
; :Params:
;    event: in, required, type=event structure
;-
pro hubObjCallback_event, event
  @huberrorcatch
  object = hubObjCallback_getObjectReference(event.id)
  if isa(object) then begin
    object.handleEvent, event
  endif
end
