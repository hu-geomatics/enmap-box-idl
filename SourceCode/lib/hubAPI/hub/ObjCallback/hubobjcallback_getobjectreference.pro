;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Returns the object reference of an object widget that is stored in it's UVALUE.
;
; :Params:
;    widgetID: in, required, type=widget ID
;      Widget ID.
;-
function hubObjCallback_getObjectReference, widgetID

  if widget_info(/VALID_ID, widgetID) then begin
    widget_control, widgetID, GET_UVALUE=object
  endif else begin
    object = !null
  endelse
  


  if isa(object, 'hash') then begin
    object = object[0]
  endif

  if ~isa(object, 'OBJREF') then begin
    object = !null
  endif
  return, object    
  
end
