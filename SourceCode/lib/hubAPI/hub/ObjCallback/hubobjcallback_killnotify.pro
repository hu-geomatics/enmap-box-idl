;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Calls the object widget killNotify method.
;
; :Params:
;    widgetID: in, required, type=widget ID
;      Widget ID.
;-
pro hubObjCallback_killNotify, widgetID

  object = hubObjCallback_getObjectReference(widgetID)
  if isa(object) then object.killNotify
  
end