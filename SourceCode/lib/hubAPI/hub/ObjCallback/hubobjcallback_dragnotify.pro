;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Calls the object widget dragNotify method.
;
;-
function hubObjCallback_dragNotify, widgetID, sourceID, $ 
   X, Y, Modifiers, Default

  object = hubObjCallback_getObjectReference(widgetID)
  if isa(object) then return, object.dragNotify(widgetID, sourceID, X, Y, modifiers, default)
  
end