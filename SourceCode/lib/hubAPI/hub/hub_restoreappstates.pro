pro hub_restoreAppStates, filename
  if file_test(filename) then begin
    restore, FILENAME=filename
    defsysv, '!hubAppStates', {appStates:appStates}
  endif
end

pro test_appStates
  filename = filepath(/TMP, 'enmapBoxAppStates.sav')
  hub_saveAppStates, filename
  hub_restoreAppStates, filename
  print, hub_getAppState(/All)

end