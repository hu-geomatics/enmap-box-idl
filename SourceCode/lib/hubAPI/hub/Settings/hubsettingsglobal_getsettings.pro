function hubSettingsGlobal_getSettings
  filename = hubSettingsGlobal_getFilename()
  settingsManager = hubSettingsManager()
  settingsManager.readSettingsFile, filename
  settings = settingsManager.getValue()
  return, settings
end