pro hubSettings_editor, Title=title, GroupLeader=groupLeader
  localSettingsFilename = hubSettingsLocal_getFilename()
  globalSettingsFilename = hubSettingsGlobal_getFilename()
  hubSettingsManager.editSettingFiles, localSettingsFilename, globalSettingsFilename, Title=title, GroupLeader=groupLeader
end