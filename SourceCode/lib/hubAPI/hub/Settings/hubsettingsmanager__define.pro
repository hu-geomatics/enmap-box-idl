;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

function hubSettingsManager::init, filename
  self.settings = orderedHash()
  if isa(filename) then begin
    self.readSettingsFile, filename
  endif
  return, 1b
end

function hubSettingsManager::getValueNames
  return, (self.settings).keys()
end

function hubSettingsManager::getValue, name
  if isa(name) then begin
    value = (self.settings)[name]
  endif else begin
    value = (self.settings)[*]
  endelse
  return, value
end

function hubSettingsManager::hasValue, name
  return, total((self.settings).keys() eq name) gt 0
end

pro hubSettingsManager::setValue, name, value, Append=Append
  (self.settings)[name] = value 
end

pro hubSettingsManager::setValues, valueHash
  foreach key, valueHash.Keys() do begin
    self.setValue, key, valueHash[key]
  endforeach
end

pro hubSettingsManager::readSettingsFile, filename
  if ~file_test(filename) then begin
    print, 'Settings file does not exist:'
    print, filename
    return  
  endif
  
  ascii = (hubIOASCIIHelper()).readFile(filename)
  
  foreach line, ascii do begin
    ;skip comment lines 
    if STREGEX(line, '^[ ]*(#;)', /BOOLEAN) then continue
    splittedLine = strsplit(line, '=', /EXTRACT)
    if n_elements(splittedLine) ne 2 then continue
    name = strtrim(splittedLine[0],2)
    value = splittedLine[1]
    meta = hubIOImgMeta_createFromENVIString(name, value)
    (self.settings)[name] = meta.getValue()
  endforeach
  
end

pro hubSettingsManager::writeSettingsFile, filename
  catch, error

  if error ne 0 then begin
    print, 'Unable to write settings file to '+filename
    return
  endif


  lines = list()
  foreach key, self.settings.keys(), i do begin
    value = (self.settings)[key]
    value = isa(value) ? STRTRIM(value) : '' ;handle null / empty values
    lines.add, string(format='(%"%s = %s")', key, value)
  endforeach
  hubIOASCIIHelper.writeFile, filename, lines.toArray()
end



;+
; :Description:
;    Create a default settings file. basically to give an idea how it can be used.
;
; :Params:
;    filename
;
;
;
; :Author: geo_beja
;-

pro hubSettingsManager::createDefaultSettingsFile, filename
  compile_opt static
  catch, error
  
  if error ne 0 then begin
    print, 'Unable to write default settings file to '+filename
    return
  endif
  
  FILE_MKDIR, FILE_DIRNAME(filename)
  
  ;add some empty fields
  m = hubSettingsManager()
  m.setValue, 'tileSizeMB', 100
  m.setValue, 'dir_tmp', FILEPATH('',/TMP)
  m.setValue, 'dir_r', !NULL
  m.setValue, 'dir_python_lib', !NULL
  m.setValue, 'dir_python_exe', !NULL
  m.setValue, 'dir_matlab', !NULL
  m.writeSettingsFile, filename

end

pro hubSettingsManager::editSettingFiles, localSettingsFilename, globalSettingsFilename, Title=title, GroupLeader=groupLeader
  compile_opt idl2, static
  
  ;create empty files if necessary
  if ~FILE_TEST(localSettingsFilename)  then hubSettingsManager.createDefaultSettingsFile, localSettingsFilename
  if ~FILE_TEST(globalSettingsFilename) then hubSettingsManager.createDefaultSettingsFile, globalSettingsFilename
  
  localSettingsManager = hubSettingsManager(localSettingsFilename)
  globalSettingsManager = hubSettingsManager(globalSettingsFilename)
  localSettings = localSettingsManager.getValue()
  globalSettings = globalSettingsManager.getValue()
  
  
  screenYSize = (get_screen_size())[1]
  tSize = 200
  hubAMW_program, groupLeader, Title=title
  case !VERSION.OS_FAMILY of
    'unix': begin
      hubAMW_frame, TITLE='local settings'
    end
    else : begin
      hubAMW_tabFrame
      hubAMW_tabPage, TITLE='local settings'      
    end
  endcase
  hubAMW_label, 'Filename: '+localSettingsFilename
  hubAMW_button, UserInformation=localSettingsFilename, EventHandler='hubSettingsManager::editSettingFiles_event', TITLE='Open Local Settings File'
  foreach key, localSettings.keys() do begin
    keySplit = strsplit(key, '_', /EXTRACT)
    amwKey = 'local_'+key
    defValue = localSettings.hubGetValue(key, Default=!NULL)
    if defValue eq '' then defValue = !NULL
    
    case keySplit[0] of
      'path' : hubAMW_inputDirectoryName, amwKey , Title=key, Value=defValue, TSize=200, DefaultResult=' ', /Editable, /AllowNonExistent
      'dir' :hubAMW_inputDirectoryName, amwKey, Title=key, Value=defValue, TSize=200, DefaultResult=' ', /Editable, /AllowNonExistent
      'file': hubAMW_inputFilename, amwKey, Title=key, Value=defValue, TSize=200, /AllowEmptyFilename
      else: hubAMW_parameter, amwKey, Title=key, Value=defValue, /String, TSize=tSize, Size=70, DefaultResult=' '
    endcase 
  endforeach

  case !VERSION.OS_FAMILY of
    'unix': begin
      hubAMW_frame, TITLE='global settings'
    end
    else : begin
      hubAMW_tabPage,TITLE='global settings'
    end
  endcase
  
  hubAMW_label, 'Filename: '+globalSettingsFilename
  hubAMW_button, UserInformation=globalSettingsFilename, EventHandler='hubSettingsManager::editSettingFiles_event', TITLE='Open Global Settings File'
  foreach key, globalSettings.keys() do begin
    keySplit = strsplit(key, '_', /EXTRACT)
    amwKey = 'global_'+key
    defValue = globalSettings.hubGetValue(key, Default=!NULL)
    if defValue eq '' then defValue = !NULL
    case keySplit[0] of
      'path' : hubAMW_inputDirectoryName, amwKey , Title=key, Value=defValue, TSize=200, DefaultResult=' ', /Editable, /AllowNonExistent 
      'dir'  : hubAMW_inputDirectoryName, amwKey, Title=key, Value=defValue, TSize=200, DefaultResult=' ', /Editable, /AllowNonExistent
      'file' : hubAMW_inputFilename, amwKey, Title=key, Value=defValue, TSize=200, /AllowEmptyFilename
      else: hubAMW_parameter, amwKey, Title=key, Value=defValue, /String, TSize=tSize, Size=70, DefaultResult=' '
    endcase 
  endforeach

  result = hubAMW_manage(ButtonNameAccept='Save Changes')
  if result.remove('accept') then begin
    foreach key, result.keys() do begin
      parts = key.split('_')
      if STREGEX(parts[0], 'local', /BOOLEAN) then begin
        localSettingsManager.setValue, (parts[1:*]).join('_'), result[key] 
      endif
      if STREGEX(parts[0], 'global', /BOOLEAN) then begin
        globalSettingsManager.setValue, (parts[1:*]).join('_'), result[key] 
      endif        
    endforeach
   
    localSettingsManager.writeSettingsFile, localSettingsFilename
    globalSettingsManager.writeSettingsFile, globalSettingsFilename
  endif
end

pro hubSettingsManager::editSettingFiles_event, resultHash, UserInformation=userInformation
  compile_opt idl2, static

  settingsFilename = userInformation
  
  if ~file_test(settingsFilename) then begin
    info = string(format='(%"Settings file %s does not exist. Try to create it.")', settingsFilename)
    print, info
  endif else begin
    hubHelper.openFile, /ASCII, settingsFilename
  endelse
  
end

function hubSettingsManager::_overloadPrint
  info = list('hubSettingsManager')
  foreach key, self.getValueNames() do begin
    info.add, string(format='(%"  %s : %s")', key, STRTRIM((self.getValue(key))))
  endforeach
  return, strjoin(info.toArray(), string(13b))
end

pro hubSettingsManager__define

  struct = {hubSettingsManager $
    , inherits IDL_Object $
    , settings : obj_new() $
    }

end