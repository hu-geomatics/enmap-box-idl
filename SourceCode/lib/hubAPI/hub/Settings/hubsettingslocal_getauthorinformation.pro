function hubSettingsLocal_getAuthorInformation
  result = dictionary()
  result.dirname       = 'hub_geomatics'
  result.description   = 'Geomatics Lab at the Geography Department of the Humboldt-Universitaet zu Berlin'
  result.readmeText    = 'This is the user configuration directory for IDL based products from '+result.description+'.'
  result.readmeVersion = 1
  return, result
end