function hubSettings_getSettings
  settings = hubSettingsGlobal_getSettings()
  settings = settings + hubSettingsLocal_getSettings() ; local settings are overwriting global settings
  return, settings
end