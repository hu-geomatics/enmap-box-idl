function hubSettingsLocal_getDirname, Author=author, App=app
  if ~isa(author) then author = hubSettingsLocal_getAuthorInformation()
  if ~isa(app) then app = hubSettingsLocal_getApplicationInformation()
  
  directory = app_user_dir(author.dirname, author.description,$
    app.dirname, app.description, $
    app.readmeText, app.readmeVersion, $
    AUTHOR_README_TEXT=author.readmeText, $
    AUTHOR_README_VERSION=author.readmeVersion, $
    ;RESTRICT_APPVERSION=app.version, $
    RESTRICT_APPVERSION=app.version + '-'+(get_login_info()).machine_name, $
    RESTRICT_FAMILY=0, $
    RESTRICT_ARCH=0, $
    RESTRICT_OS=0)
  
  ;directory += '-'+(get_login_info()).machine_name 
    
  return, directory
end

pro test_hubSettingsLocal_getDirname
  print, hubSettingsLocal_getDirname()
end

