function hubSettingsGlobal_getDirname
  dirname = filepath('', ROOT_DIR=hub_getDirname(), SUBDIRECTORY='resource')
  return, dirname
end