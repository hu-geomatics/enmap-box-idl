function hubSettingsGlobal_getFilename
  filename = filepath('settings.txt', ROOT_DIR=hubSettingsGlobal_getDirname())
  return, filename
end