function hubSettingsLocal_getApplicationInformation
  compile_opt idl2
  result = dictionary()
  result.dirname       = 'hubAPI'
  result.description   = 'Application Programming Interface'
  result.version       = (enmapBoxSettingsLocal_getApplicationInformation()).version
  result.readmeText    = 'This is the configuration directory for the hubAPI.'
  result.readmeVersion = 1
  return, result
end