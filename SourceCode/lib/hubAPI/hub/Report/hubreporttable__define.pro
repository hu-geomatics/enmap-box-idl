;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Constructor of hubReportTable Object.
;
;-
function hubReportTable::init 

  if ~self.hubTable::init() then return, 0b
  
  self.headerDefinitions = list(list())
  self.formats = list()
  
  self.nulls   = list()
  self.separatorRows = list()
  self.separatorCharacter = list()
  
  self.defaultFormatStrings = Hash()
  self.defaultFormatStrings[1] = '(%"%i")'
  self.defaultFormatStrings[2] = '(%"%i")'
  self.defaultFormatStrings[3] = '(%"%i")'
  self.defaultFormatStrings[4] = '(%"%0.2f")'
  self.defaultFormatStrings[5] = '(%"%0.2f")'
  self.defaultFormatStrings[7] = '(%"%s")'
  self.defaultFormatStrings[12] = '(%"%i")'
  self.defaultFormatStrings[13] = '(%"%i")'
  self.defaultFormatStrings[14] = '(%"%i")'
  self.defaultFormatStrings[15] = '(%"%i")'
  
  return, 1b
  
end

function hubReportTable::getHeader
  result = list()
  foreach l, self.headerDefinitions do begin
    headerRow = list()
    foreach i, l do begin
      headerRow.add, i.name
      headerRow.add, i.span
    endforeach
    result.add, headerRow
  endforeach
  
  return, result

end

pro hubReportTable::setDefaultFormatString, dataType, formatString
  for  i=0, n_elements(dataType)-1 do begin
    self.defaultFormatStrings[datatype[i]] = formatString[i]
  endfor
end

pro hubReportTable::setHeader, rowCollection
  
  ;ensure proper wrapped header definition
  ;list( list(<row1>),...,list(<row n>) )
  ;
  ;possible inputs
  ;1. single value (one columns only) 
  ;2. single array/list for single header row
  ;3. nested header rows: list(array,list(),...)
  
  ;setHeader, 'col1'
  ; setHeader, ['col1','col2']
  ; setHeader, list('col1','col2')
  ; setHeader, list('col1','col2')
  
  if ~isa(rowCollection, /ARRAY) then begin
    ;must be single row definition
    rowCollection = list(list(rowCollection, /EXTRACT))
  endif else begin
    r = list()
    
    isNested = isa(rowCollection[0], /ARRAY)
    if ~isNested then begin
      r.add, rowCollection
    endif else begin
      foreach item, rowCollection do begin
        r.add, list(item,/EXTRACT)
      endforeach
    endelse
    rowCollection = r
  endelse
  
  ;convert to internal representation
  definition = list()
  cnt = self.getColumnCount()
  foreach row, rowCollection, iRow do begin
    
    definition.add, self._extractSingleHeaderRow(row, NUMBEROFCOLUMNS=ncolumns)
    if nColumns ne cnt then begin
      message, 'Number of columns missmatches'
    endif
  endforeach
  
  self.headerDefinitions = definition
  
end

;+
; :Hidden:
; :Description:
;    
;
; :Params:
;    columnDescriptions
;
; :Keywords:
;    numberOfColumns
;-
function hubReportTable::_extractSingleHeaderRow, columnDescriptions, numberOfColumns=numberOfColumns
  tname = typename(rowCollection)
  def = list()
  numberOfColumns = 0

  n = n_elements(columnDescriptions)
  for i = 0, n - 1 do begin
    j = i+1
    value = columnDescriptions[i]
    
    if TYPENAME(value) ne 'STRING' then message, 'Header row not well formated: expected STRING value'
    
    tag = {name:value, span:1}
    
    if j lt n && TYPENAME(columnDescriptions[j]) ne 'STRING' then begin
      tag.span = columnDescriptions[j]
      i++
    endif
    
    def.add, tag
    numberOfColumns += tag.span
  endfor
  
  
  return, def
end



;+
; :Description:
;    Adds a new row to the table.
;
; :Params:
;    rowData: in, required, type=array or list
;     A List or array containing n row values with n = number of columns.
;      
;
;
;-
pro hubReportTable::addRow, rowData
  if self.getColumnCount() eq 0 then message, 'Please specify columns first'
  self.hubTable::addRow, rowData
end

pro hubReportTable::addEmptyColumns, n, names=names, titles=titles, dataTypes=dataTypes, formatStrings=formatStrings
  for i=0, n-1 do begin
    self.addColumn $
      , name=isa(names)?names[i] : !NULL $
      , title=isa(titles) ? titles[i] : !NULL $
      , dataType=isa(dataTypes) ? dataTypes[i] : !NULL $
      , formatString=isa(formatStrings)? formatStrings[i] : !NULL  
  endfor

end

;+
; :Description:
;    Adds a new column to the data table.
;
; :Params:
;    columnData: in, required, type=array or list
;     Adds all values of a new column to the table.
;
; :Keywords:
;    name: in, optional, type=string, default = 'Column <number of column>'
;    title: in, optional, type=string, default = column name
;    dataType: in, optional, type=IDL data type
;     Specifies the data type of all values in this column. If no set, the data type is derived from the first of all values. Please note that
;     all following values will get casted to this `dataType`.  
;     
;    formatString: in, optional, type=string
;     Format code to specify the representation of values in `columnData`.
;     Check `http://www.exelisvis.com/docs/Format_Codes.html` for details.
;     
;    nullString: in, optional, type=string, default=''
;     A string that is used in case of missing cell values.
;     
;-
pro hubReportTable::addColumn, columnData, name=name, title = title, dataType=dataType, formatString=formatString, nullString=nullString
  
  cName = isa(name) ? name : string(format='(%"Column %i")', self.getColumnCount()+1)
  cTitle = isa(title)? title : cName
  
  if self.hasColumn(cName) then message, 'Column "'+cname+'" already exists'
  
  self.formats.add, isa(formatString)? formatString[0] : !NULL
  self.nulls.add, isa(nullString) ? nullString[0] : ''
  self.hubTable::addColumn, cName, dataType, values=columnData
  (self.headerDefinitions[-1]).add, {name:cTitle, span:1}
    
end

;+
; :Description:
;    Adds a row separator 
;
; :Params:
;    separatorCharacter
;
;-
pro hubReportTable::addSeparator $
  , separatorCharacter, rowIndex=rowIndex
  
  if isa(separatorCharacter) then begin
    sepChar = strlen(separatorCharacter) eq 1 ? separatorCharacter : '-'
  endif else begin
    sepChar = '-'
  endelse
  
  
  self.separatorRows.add, isa(rowIndex)? rowIndex[0] : self.getRowCount()
  self.separatorCharacter.add, sepChar

  
end

;+
; :Description:
;    Returns a formatted text representation of the report table.
;
; :Keywords:
;    Trim: in, optional, type=bool
;     Set this to trim all string values.
;
;-
function hubReportTable::getFormatedASCII, Trim=trim, colSep = colSep
  
  if ~isa(colSep) then colSep = '|'
  cntRows = self.getRowcount()
  cntCols = self.getColumnCount()
  cntSep  = n_elements(self.separatorRows)
  
  if cntCols eq 0 then return, !NULL
  
  
  columnWidths = indgen(cntCols)
  columnHdr = []
  foreach def, self.headerDefinitions[-1] do begin
    if def.span eq 1 then begin
      columnHdr = [columnHdr, def.name]
    endif else begin
      for i=1, def.span do begin
        columnHdr = [columnHdr, '']
      endfor
    endelse
  endforeach
  
  if cntRows gt 0 then begin
    data = STRARR(cntCols, cntRows)
    foreach c, self.columns, iC do begin
      format = self.formats[iC]
      if ~isa(format) then format = self.defaultFormatStrings[c.getDataType()]
      values = (c.getValues(/STRING, formatString=format, missing=self.nulls[iC])).toArray()
      w = max(strlen(values)) > strlen(columnHdr[iC])
      case c.getDatatype() of
        7    : values = string(format='(%"%-'+strtrim(w,2)+'s")',values)
        else : values = string(format='(%"%-'+strtrim(w,2)+'s")',values)
      endcase
  
      data[iC,*] = values
      columnWidths[iC] = w
    endforeach
  endif else begin
    columnWidths = strlen(columnHdr)
    
  endelse

  dataLines = list()
  ;insert frist underline
;  sepRow = STRARR(cntcols)
;  for iC = 0, cntCols -1 do begin
;    w = columnWidths[iC]
;    seprow[iC] = strjoin(replicate('=',w),'')
;  endfor
;  dataLines.add, seprow
  
  for iR = 0, cntRows do begin
    iSep = self.separatorRows.where(iR)
    if isa(iSep) then begin
      foreach iS, iSep do begin
        sepRow = STRARR(cntcols)
        sepChar = (self.separatorCharacter)[iS]
        for iC = 0, cntCols -1 do begin
          w = columnWidths[iC]
          seprow[iC] = strjoin(replicate(sepChar,w),'')
        endfor
        dataLines.add, seprow
      endforeach
    endif
    if iR lt cntRows then dataLines.add, data[*,iR]
  endfor
;  
  for i = 0, n_elements(dataLines) - 1 do begin
    dataLines[i] = strjoin(dataLines[i], colSep)
  endfor
  

  totalWidth = total(columnWidths)

  headerLines = list()
  foreach headerrowDefinition, self.headerDefinitions do begin
    headerLineValues = list()
    iColumn0 = 0
    foreach columnDefinition, headerrowDefinition, i do begin
      name = columnDefinition.name
      span = columnDefinition.span
      iColumn1 = iColumn0 + span - 1
      w = total(columnWidths[iColumn0:iColumn1], /INTEGER)+ 1*(span -1)
      headerLineValues.add, string(format='(%"%-'+strtrim(w,2)+'s")', name)
      iColumn0 += span
    endforeach
    headerLines.add, strjoin(headerLineValues.toArray(),colSep)
  endforeach  
  
 
  
  

  headerLines.add, strjoin(replicate('=',totalWidth+cntCols-1))
  dataLines = headerLines + dataLines
  
  return, transpose(dataLines.toArray())
  
end

;+
; :Description:
;    Returns the reportTable as HTML code.
;
; :Keywords:
;    attributesTable: in, optional, type=string
;     Use this keyword to provide additional values to be used inside the `<table ....>` HTML tag.
;-
function hubReportTable::getHTMLTable, attributesTable=attributesTable, caption=caption
  code = List()
  if ~isa(attributesTable) then attributesTable = ''
  code.add, '<table '+attributesTable + '>'
  
  if isa(caption) then begin
    code.add, '<caption>'+string(caption)+'</caption>'
  endif
  
  x = self.separatorRows
  ;add headers
  foreach headerRowDefinition, self.headerDefinitions do begin
    code.add, '<tr>'
    foreach columnDefinition,  headerRowDefinition do begin
      name = columnDefinition.name
      name = hubReportHTMLHelper.maskHTMLCharacters(name)
      span = columnDefinition.span
      code.add, span eq 1 ? string(format='(%"<th>%s</th>")', name) $
        : string(format='(%"<th colspan=%i>%s</th>")',span, name)
    endforeach
    code.add, '</tr>'
    
  endforeach
  
  
  columnNullValues = self.nulls
  columnNullValues = columnNullValues.toArray(missing='', type='STRING')
  columnFormats = self.formats
  
  ;specify column attributes
  cntCols = self.getColumnCount()
  cntRows = self.getRowCount()
  
  cssTDStyles = make_array(cntcols, /STRING, value='')
  foreach c, self.columns, iCol do begin
    dataType = c.getDataType()
    case dataType of
      7     : cssTDStyles[iCol] = '<td>'
      else  : cssTDStyles[iCol] = '<td style="text-align:right">'
    endcase
  endforeach
  
  ;add data rows
  for iRow = 0, cntRows -1 do begin
    if total(cntRows eq iRow) ne 0 then begin
      ;add row separator
      rowdata = string(format='(%"<tr><td colspan=%i></td></tr>")', cntCols)
    endif else begin
      rowData = '<tr>'
      for iCol = 0, cntCols - 1 do begin
      
        ; apply user format
        
        cellValue = self.getValue(iCol, iRow)
        ;if n_elements(cellValue) gt 1 then message, 'invalid number of cell values'
        if (~isa(cellValue)) || (strlen(strjoin(strtrim(cellValue,2),' ')) eq 0)  then begin
          cellValue = columnNullValues[iCol]
        endif else begin
          format = columnFormats[iCol]
          if ~isa(format) then format = self.defaultFormatStrings[(self.columns[iCol]).getDataType()]
          cellValue = string(cellValue, format=format)
        endelse
        
        
        cellData = cssTDStyles[iCol]
        cellData += hubReportHTMLHelper.maskHTMLCharacters(cellValue)
        cellData += '</td>'
        rowData += cellData
      endfor
      rowData += '</tr>'
    endelse
    code.add, rowData
  endfor
  
  code.add, '</table>'
  return, code.toArray()
end


;+
; :Hidden:
;-
pro hubReportTable__define
  struct = {hubReportTable $
    , inherits hubTable $
    , headerDefinitions:list() $
    , formats : list() $
    , defaultFormatStrings : Hash() $
    , nulls : list() $
    , filledColumns:0 $
    , separatorRows : list() $
    , separatorCharacter : list() $
  }
  
end

;+
; :Hidden:
;-
pro test_hubReportTablePrint, table, printHTML
  if printHTML then begin
    r = hubReport()
    r.addHTML, table.getHTMLTable()
    r.saveHTML, /SHOW
  endif else begin
     print, table.getFormatedASCII()
  endelse
end

;+
; :Hidden:
;-
pro test_hubReportTable
  
  printHTML = 1b 
  
  ;an empty table
  table = hubReportTable()
  
  ;add some empty columns
  table.addColumn, name='Rank' 
  table.addColumn, name='Land',FORMATSTRING='(%"%-20s")'
  table.addColumn, name='Result', nullString='*'
  test_hubReportTablePrint, table, printHTML
  stop

  ;add the data row-by-row
  table.addRow, list(1,'JAM langer text',9.63)
  table.addSeparator, '+'
  table.addRow, list(2,'JAM',!NULL)
  table.addRow, list(3,'USA',9.75)
  test_hubReportTablePrint, table, printHTML
  stop
    
  ;change the header as you like to have it. 
  ;a single number specify how many columns will be spanned
  table.setHeader, list(list('100 m SPRINT', 3) $
                       ,list('', 'Results' , 2) $
                       ,list('#','Nation','Time'))
  test_hubReportTablePrint, table, printHTML
  stop
  
  
  return
  
 
  
end