;+
; Class for creating HTML reports. 
; 
; .. image:: _IDLDOC/hubReport.png
; 
; :Examples:
;   Create a HTML report with some headings, some text and an image::
;   
;     report = hubReport(title='My Report')
;    
;     ; add different headings
;    
;     report.addHeading, 'Heading'
;     report.addHeading, 'Subheading', 2
;     report.addHeading, 'Subsubheading', 3
;      
;     ; add a paragraph (floating text)
;      
;     report.addParagraph, strjoin(replicate('my text ', 30))
;     
;     ; add monospaced text (non-floating text)  
;     
;     table = [ $
;       'my accuracy = 90%',$
;       '',$
;       '   class 1 |    class 2 |   class 3 ',$
;       '------------------------------------',$
;       '      100% |       100% |       80% ',$
;       '       70% |        50% |       80% ']
;     report.addMonospace, table
;    
;     ; add an image
;    
;     randomImage = bytscl(randomu(seed, 300, 300, 3))
;     report.addImage, randomImage, 'template image caption'
;      
;     ; save to HTML file and open the report
;      
;     report.saveHTML, /Show
;
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; 
;
;-

;+
; :Description:
;    Object contructor.
;
; :Keywords:
; 
;    Title: in, optional, type=string
;      Report title.
;      
;    ShortTitle : in, optional, type=string
;      Report short title.
;      
;    _REF_EXTRA
;      All extra keywords are passed to the report progress bar contructor (see `hubProgressBar`). 
;-
function hubReport::init $
  , Title=title $
  , ShortTitle=shortTitle $
  , _REF_EXTRA = _extra

  self.title = isa(title) ? title : 'HTML Report'
  self.shortTitle = isa(shortTitle) ? shortTitle : self.title
  self.elements = list()
  self.progressBar = hubProgressBar(Title=self.title, Info='creating HTML report', _EXTRA=_extra) 
  return, 1b
  
end

;+
; :Description:
;    Sets the report title.
;
; :Params:
;    title: in, required, type=string
;      Report title.
;
;-
pro hubReport::setTitle, title
  self.title = title 
end

;+
; :Description:
;    Sets the report short title.
;
; :Params:
;    shortTitle: in, required, type=string
;      Report short title.
;
;-
pro hubReport::setShortTitle, shortTitle
  self.shortTitle = shortTitle 
end

;+
; :Description:
;    Returns report title.
;-
function hubReport::getTitle
  return, self.title 
end

;+
; :Description:
;    Returns report short title.
;-
function hubReport::getShortTitle
  return, self.shortTitle 
end

;+
; :Description:
;    Returns report progress bar (see `hubProgressBar`).
;-
function hubReport::getProgressBar
  return, self.progressBar 
end

;+
; :Description:
;    Add a heading to the report.
;	
; :Params:
;    value: in, required, type=string
;      Heading to be added.
;      
;    level: in, optional, type={1 | 2 | 3}, default=1
;      Heading level can be 1 for H1 heading (this is the default),
;      2 for H2 headings (subheading) and
;      3 for H3 headings (subsubheading).
;-
pro hubReport::addHeading $
  , value $
  , level

  element = {type:'heading', value:string(value), level:1}
  if isa(level) then begin
    element.level >= level
  endif
  self.elements.add, element

end

;+
; :Description:
;    Add a monospaced text block to the report.
;
; :Params:
;    value: in, required, type=string[]
;      Text to be added.
;-
pro hubReport::addMonospace $
  , value
  
  element = {type:'monospace', value:string(value)}
  self.elements.add, element

end

;+
; :Description:
;    Add pure HTML code.
;
; :Params:
;    value: in, required, type = string
;     HTML code to be added.
;-
pro hubReport::addHTML, value 
  element = {type:'html', value:string(value)}
  self.elements.add, element  
end

;+
; :Description:
;    Add a text paragraph to the report.
;
; :Params:
;    value: in, required, type=string[]
;      Text to be added.
;-
pro hubReport::addParagraph $
  , value
  
  element = {type:'paragraph', value:string(value)}
  self.elements.add, element

end

;+
; :Description:
;    Add an image to the report. Image can be: 
;    
;    - RGB image [samples,lines,3] or greyscale [samples,lines]
;    
;    - a graphic window or graphic buffer (GRAPHICSWIN or GRAPHICSBUFFER). In this case the image
;      will be retrieved using the copyWindow() function
;
; :Params:
;    value: in, required, type={byte[][] | byte[][][3]}
;      RGB or Grayscale image data.
;      
;    caption: in, optional, type=string
;      Image caption.
;-
pro hubReport::addImage $
  , value $
  , caption $
  , Flip=flip
  
  if isa(value, 'graphic') then begin
    self.addImage, transpose(value.copyWindow(), [1,2,0]), caption, flip=flip
    return
  endif
  
  element = {type:'image', value:value, caption:'', flip:keyword_set(flip)}
  if isa(caption) then begin
    element.caption = caption
  endif
  self.elements.add, element

end

;+
; :Hidden:
; :Description:
;    Returns the internal element list
;-
function hubReport::_getElements
  return, self.elements
end 

;+
; :Description:
;    Saves the report to a HTML file.
;
; :Params:
;    filename: in, optional, type=filename, default=temporary filename
;      Report filename with .html extension.
;
; :Keywords:
;    Show: in, optional, type=boolean
;      Opens the report in the web browser.
;-
pro hubReport::saveHTML $
  , filename $
  , Show=show $
  , CSSFile=cssFile $
  , NoFooter=noFooter
  
  if ~isa(filename) then begin
    prefix =  '_'+hubHelper.getTimeString()
    title = strjoin(strsplit(self.title, '()', /EXTRACT),'_')
    filename = hubHelper.filepath(/TMP, strcompress(title, /REMOVE_ALL)+prefix+'.html')
  endif 
  
  authorDTG = strupcase(getenv('username'))+'  '+ systime()  
  asciiHelper = hubIOASCIIHelper()
  reportHelper = hubReportHTMLHelper(filename)
 
  
  asciiHelper.writeFile, filename $
    ,['<!DOCTYPE html>' $
    , '<html lang="en">']
    
  asciiHelper.writeFile, filename, /APPEND $
    , '  ' + reportHelper.getHeader(self.title $
                  , metaAuthor=authorDTG $
                  , linkCSS = CSSFile $
                  , metaDescription='hubReport' $
                  )
  asciiHelper.writeFile, filename, /APPEND, '  <body>'
  asciiHelper.writeFile, filename, /APPEND $
          , '  ' + reportHelper.getReportSection(self, 'report0')
  
  ;write footer
  asciiHelper.writeFile, filename, /APPEND $
                , '    ' + reportHelper.getFooter()
  ;finish the document
  asciiHelper.writeFile, filename, /APPEND, $
         ['  </body>' $
         ,'</html>  ' ]         

  
  if keyword_set(show) then begin
    hubHelper.openFile, /HTML, filename
  endif

end


;+
; :Description:
;    Destroys the progress bar.
;-
pro hubReport::cleanup
  
  obj_destroy, self.progressBar

end

;+
; :Hidden:
;-
pro hubReport__define
  struct = {hubReport $
    , inherits IDL_Object $
    , title : '' $
    , shortTitle : '' $
    , elements : list() $
    , progressBar : obj_new() $
  }
end

;+
; :Hidden:
;-
pro test_hubReport
  
;  headings = ['Rank'     , 'Land'   , 'Name', 'Result']
;  formats  = ['(%"%-3i")', '(%"%3s")', '(%"%-20s")', '(%"%5.2f")']
;  formats  = ['(%"%-3i")', '(%"%3s")', '(%"%0s")', '(%"%5.2f")']
;  nulls = list('*','--','***','***')
  table = hubReportTable()
  table.addColumn, name='Rank'
  table.addColumn, name='Land'
  table.addColumn, name='Name'
  table.addColumn, name='Result'
  table.addRow, list(1,'JAM','Usain Bolt'       ,9.63)
  table.addRow, list(2,'JAM','Yohan Blake'      ,9.75)
  table.addRow, list(3,'USA','Justin Gatlin'    ,9.79)
  table.addRow, list(4,'USA','Tyson Gay'        ,9.80)
  table.addRow, list(5,'USA','Ryan Bailey'      ,9.88)
  table.addRow, list(6,'NED','Churandy Martina' ,9.94)
  table.addRow, list(7,'TRI','Richard Thompson' ,9.98)
  table.addRow, list(8,'JAM','Asafa Powell'     ,11.99)
  table.addSeparator
  table.addRow, list(!NULL,!NULL,'<pseudo value>'     ,!NULL)
  
  report = hubReport(Title='My Report', /NoShow)

  ; add different headings

  report.addHeading, 'Heading'
  report.addHeading, 'Subheading', 2
  report.addHeading, 'Subsubheading', 3
  
  ; add a paragraph (floating text)
  
  report.addParagraph, strjoin(replicate('my text ', 30))
 
  ; add monospaced text (non-floating text)  
 
  table2 = [ $
    'my accuracy = 90%',$
    '',$
    '   class 1 |    class 2 |   class 3 ',$
    '------------------------------------',$
    '      100% |       100% |       80% ',$
    '       70% |        50% |       80% ']
  
  
  report.addHTML, table.getHTMLTable()
  report.addMonospace, table2
  ; add an image

  randomImage = bytscl(randomu(seed, 300, 300))
  randomImage = bytscl(indgen(300)##replicate(1,200))
  
  report.addImage, randomImage, 'template image caption', /Flip
  
  ; save to HTML file and open the report
  
  report.saveHTML, /Show;, CSSFile='D:\temp\myOwnStylesheet.css' 
  
end