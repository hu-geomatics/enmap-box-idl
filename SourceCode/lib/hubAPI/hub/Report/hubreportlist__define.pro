;+
; Class for creating HTML report lists.
; 
; .. image:: _IDLDOC/hubReportList.png
; 
; :Examples:
; 
; Create a HTML report list with some reports::
; 
;    reportList = hubReportList()
;    
;    ; creating some reports for the list using hubReportList::newReport
;  
;    for i=1,5 do begin                                                         &$
;      shortTitle = '#'+strtrim(i, 2)                                           &$
;      title = 'My Report '+shortTitle                                          &$
;      report = reportList.newReport(Title=title, ShortTitle=shortTitle)        &$
;      report.addParagraph, 'My Report Content'                                  &$
;    endfor
;  
;    ; add another report by using hubReportList::newReport
;  
;    report = hubReport(Title='Another Report', ShortTitle='another report', /NoShow)
;    reportList.addReport, report
;    
;    ; save report list
;  
;    reportList.saveHTML, /Show
;    reportList.cleanup
;    
;    
; 
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the constructor.
;
; :Keywords:
; 
;    title : in, required, type=string
;      Report title.
;
;    _REF_EXTRA : in, optional
;      Extra keywords are passed to the progress bar constructor (see `hubProgressBar::init`).
;    
;-
function hubReportList::init $
  , Title=title $
  , _REF_EXTRA = _extra

  self.title = isa(title) ? title : 'HTML Report List'
  self.reports = list()
  
  self.progressBar = hubProgressBar(Title=self.title, Info='creating HTML report', _EXTRA=_extra) 

  return, 1b
  
end

;+
; :Description:
;    Creates a new report, adds it to the list, and returnes the report reference.
;
; :Keywords:
;    _REF_EXTRA : in, optional
;      Extra keywords are passed to the report constructor (see `hubReport::init`).
;-
function hubReportList::newReport, htmlReportSectionCSS=htmlReportSectionCSS, _REF_EXTRA=_extra
    
  report = hubReport(/NoShow, _EXTRA=_extra)
  self.addReport, report, htmlReportSectionCSS=htmlReportSectionCSS
  return, report

end

;+
; :Description:
;    Adds a report to the list.
;
; :Params:
;    report: in, required, type=hubReport
;-
pro hubReportList::addReport, report, htmlReportSectionCSS=htmlReportSectionCSS
  self.reports.add, report
  
end

;+
; :Description:
;    Returns the report from a specific `index` position.
;
; :Params:
;    index: in, required, type=int
;      Index of the report to be returned.
;-
function hubReportList::getReport, index
  return, (self.reports)[index]
end


;+
; :Description:
;    Returns the number of reports part of this report list.
;-
function hubReportList::getNumberOfReports
  return, n_elements(self.reports)
end


;+
; :Description:
;    Saves the report list to a HTML file.
;
; :Params:
;    filename: in, optional, type=filename, default=temporary filename
;
; :Keywords:
;    Show: in, optional, type=boolean
;      Opens the report list in the web browser.
;      
;     
;    CSSFile: in, optional, type = file path
;     Use this to specify an other CSS file to be used instead of the standard one.
;     
;    SelectedReports: in, optional, type=int[]
;     Use this to specify reports (by its zero-based indices) from the report list that are to show initially in the final HTML document.
;     
;-
pro hubReportList::saveHTML, filename, Show=show, CSSFile=CSSFile, SelectedReports=selectedReports
  if ~isa(filename) then begin
    prefix =  '_'+hubHelper.getTimeString()
    filename = hubHelper.filepath(/TMP, strcompress(self.title, /REMOVE_ALL)+prefix+'.html')
  endif 
  
  if ~stregex(filename, '\.(xhtml|html?)$', /Fold_case, /Boolean) then begin
    
  endif
  
  authorDTG = strupcase(getenv('username'))+'  '+ systime()
    
  asciiHelper = hubIOASCIIHelper()
  reportHelper = hubReportHTMLHelper(filename)

  
  asciiHelper.writeFile, filename $
    ,['<!DOCTYPE html>' $
    , '<html lang="en">']
    
  asciiHelper.writeFile, filename, /APPEND $
    , '  ' + reportHelper.getHeader(self.title $
                  , metaAuthor=authorDTG $
                  , linkCSS=cssFile $
                  , metaDescription='hubReportList' $
                  )
                  
  initialReportIDs = ''
  if isa(SelectedReports) then begin
    n = n_elements(self.reports)
    
    case SelectedReports of
      '*': selected = indgen(n)
      -1 : selected = [n-1]
     else : begin
            selected = list()
            foreach r, selectedReports do begin
              if r lt n then selected.add, r
            endforeach
            selected = selected.toArray()
           end
    endcase
    if isa(selected) then begin
      initialReportIDs = "['"+strjoin('Report'+strtrim(selected+1,2),',')+"']"
    endif
  
  endif
  
  asciiHelper.writeFile, filename, /APPEND $
   ,'  ' + ['<body onload="initDocument('+initialReportIDs+')">' $
           ,'  <header id="header">' $
           ,'    <h1>'+self.title+'</h1>' $
           ,'  </header>']           
       
  navTitles = list()
  foreach report, self.reports, i do begin
    navTitles.add, report.getShortTitle()
  endforeach
  asciiHelper.writeFile, filename, /APPEND $
          , '    ' + reportHelper.getNavigation(navTitles, reportIDs=reportIDs)
          
  ;write single reports
  asciiHelper.writeFile, filename, /Append $
      , '    <div id="ReportSections">'
  foreach report, self.reports, i do begin
    asciiHelper.writeFile, filename, /APPEND $
          , '      ' + reportHelper.getReportSection(report, reportIDs[i])
  endforeach
  asciiHelper.writeFile, filename, /Append $
      , '    </div>'
  
  ;write footer
  asciiHelper.writeFile, filename, /APPEND $
                , '    ' + reportHelper.getFooter()
  ;finish the document
  asciiHelper.writeFile, filename, /APPEND, $
         ['  </body>' $
         ,'</html>  ' ]
   
   if keyword_set(show) then begin
    hubHelper.openFile, /HTML, filename
  endif

end

;+
; :Description:
;    Returns the progress bar reference (see `hubProgressBar`).
;-
function hubReportList::getProgressBar
  return, self.progressBar
end

;+
; :Description:
;    Destroys the progress bar.
;-
pro hubReportList::cleanup
  obj_destroy, self.progressBar
end

;+
; :Hidden:
;-
pro hubReportList__define
  struct = {hubReportList $
    , inherits IDL_Object $
    , title : '' $
    , reports : list() $
    , progressBar : obj_new() $
  }
end

;+
; :Hidden:
;-
pro test_hubReportList

  reportList = hubReportList()
  
  ; creating some reports for the list using hubReportList::newReport

  for i=1,5 do begin
    wait,0.2
    shortTitle = 'Report '+strtrim(i, 2)
    title = 'My Report '+shortTitle+' (added via hubReportList::newReport)'
    report = reportList.newReport(Title=title, ShortTitle=shortTitle)
    randomImage = bytscl(randomu(seed, 300, 300))
    t = fix(300 / (i+1))* i
    randomImage[t,*] = 0.
    randomImage[*,t] = 0.
    report.addImage, randomImage, 'template image caption'
    
    
  endfor

  ; add another report by using hubReportList::newReport

  report = hubReport(Title='Another Report (added via hubReportList::addReport)', ShortTitle='another report', /NoShow)
  report.addHTML, '<table><tr><td>blub</td></tr></table>
  reportList.addReport, report
  
  ; save report list

  reportList.saveHTML, /Show
                   
  
end