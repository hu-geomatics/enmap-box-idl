;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
; 	
;  :Hidden:
;  :Private:
;-

;+
; :Description:
;    Constructor of an report helper object.
;    This object can be used to generate HTML code fragments to be used by
;    `hubReport` and `hubReportList`. 
;
; :Params:
;    reportFileName: in, required, type=filepath
;     Filepath of report file.
;
;-
function hubReportHTMLHelper::init, reportFileName
  if isa(reportFilename) then begin
    self.rootDir = file_dirname(reportFileName)
    basename = file_basename(reportFileName, '.html')
    ;directories - all relative to rootdir 
    self.fileDir = basename+'_files'
    self.imageDir = filepath('img', root_dir=self.fileDir)
  endif 
  self.imageCounter = 0
  self.HTMLSpecialCharactersLUT = hubReportHTMLHelper._getHTMLSpecialCharacterLUT()
 return, 1b  
end

function hubReportHTMLHelper::_getHTMLSpecialCharacterLUT
  compile_opt static, strictarr 
  LUT = list()
  LUT.add,['&','&amp;']
  LUT.add,['<','&lt;']
  LUT.add,['>','&gt;']
  
  LUT.add,['¢','&cent;']
  LUT.add,['£','&pound;']
  LUT.add,['¥','&yen;']
  LUT.add,['€','&euro;']
  LUT.add,['§','&sect;']
  LUT.add,['©','&copy;']
  LUT.add,['®','&reg;']
  LUT.add,['™','&trade;']
  
  LUT.add,['°','&deg;']
  LUT.add,['±','&plusmn;']
  LUT.add,['²','&sup2;']
  LUT.add,['³','&sup3;']
  LUT.add,['´','&acute;']
  LUT.add,['µ','&micro;']
  LUT.add,['¶','&para;']
  LUT.add,['·','&middot;']
  LUT.add,['¸','&cedil;']
  LUT.add,['¹','&sup1;']
  LUT.add,['º','&ordm;']
  LUT.add,['»','&raquo;']
  LUT.add,['¼','&frac14;']
  LUT.add,['½','&frac12;']
  LUT.add,['¾','&frac34;']
  LUT.add,['¿','&iquest;']


  return, LUT
  
end

function hubReportHTMLHelper::maskHTMLCharacters, unmaskedString
  compile_opt static, strictarr
  
  maskedString = unmaskedString
  
  toReplace = hubReportHTMLHelper._getHTMLSpecialCharacterLUT()
  match = 0b
  for i = 0, n_elements(maskedString) -1 do begin
    textLine = maskedString[i]

    n = strlen(textLine)
    if n eq 0  then continue
    
    newLine = make_array(n, /STRING)
    for j=0, n-1 do newLine[j] = strmid(texTLine, j,1)
    foreach pair, toReplace do begin
      for j=0, n-1 do begin
        c = strmid(textLine,j, 1)
        
        if strcmp(c, pair[0]) or (byte(c))[-1] eq (byte(pair[0]))[-1] then begin
          newLine[j] = pair[1]
        endif
      endfor
    endforeach
    
    newLine = strjoin(newLine)
    maskedString[i] = newline
    ;print, newline
    continue
;    
;    
;    lutMatch  = make_array(n, /INTEGER, value=-1)
;    lutLength = make_array(n, /INTEGER, value=0)
;    
;    foreach pair, toReplace , iPair do begin
;      p0 = 0
;      while 1b do begin
;        p = stregex(strmid(textLine, p0), pair[0], length=l)
;        if p eq -1 then break
;        
;        lutMatch[p0+p] = iPair
;        lutLength[p0+p] = l
;        p0 += p+l
;      endwhile
;      
;    endforeach
;    
;    iPositions = where(lutMatch ne -1, /NULL)
;    if isa(iPositions) then begin
;      newline = ''
;      p0 = 0
;      foreach iPos, iPositions do begin
;        pair = (self.HTMLSpecialCharactersLUT)[lutMatch[iPos]]
;        l  = lutLength[iPos]
;        
;        prefix = strmid(textline, p0, iPos-p0)
;        
;        newline += prefix + pair[1]
;        p0 = iPos + l
; 
;      endforeach
;      newline += strmid(textline, p0)
;      maskedString[i] = newline
;    endif     
  endfor
  return, maskedString
end


;+
; :Description:
;    Returns the directory where all files will be stored to.
;-
function hubReportHTMLHelper::getFileDir
  return, self.fileDir
end


;+
; :Description:
;    Returns the subdirectory where all images will be stored to.
;-
function hubReportHTMLHelper::getImageDir
  return, self.imageDir
end

;+
; :Description:
;    Returns the root directory of the report file.
;-
function hubReportHTMLHelper::getRootDir
  return, self.rootDir
end



;+
; :Description:
;    Returns the headers HTML Code.
;
; :Params:
;    title: in, required, type = string
;     Title of the final HTML document.
;
; :Keywords:
;    metaAuthor: in, optional, type=string, default=user name + systime
;     Name of author to be used in the HTMLs meta tags like::
;     
;       <meta name="author" content="'+metaAuthor+'" />'
;       
;    metaDescription: 
;    linkCSS: in, optional, type=string, type = URI
;     Link to an external CSS file to be used for this report.
;      
;    linkJavaScript: in, optional, type=string, type = URI
;     Link to an external JavaScript file to be used for this report.
;
;-
function hubReportHTMLHelper::getHeader, title $
    , metaAuthor = metaAuthor $
    , metaDescription = metaDescription $
    , linkCSS = linkCSS $
    , linkJavaScript = linkJavaScript
    
    if ~isa(metaAuthor) then metaAuthor = strupcase(getenv('username'))+'  '+ systime()
    if ~isa(metaDescription) then metaDescription = 'hubAPI HTML Report'
    if ~isa(linkCSS) then begin
      linkCSS = filepath('stylesheet.css', root_dir=self.fileDir)
      ;copy css file
      sourceFile = filepath('stylesheet.css', ROOT_DIR=hub_getDirname(), SUBDIRECTORY=['resource','html'])
      targetFile = filepath(linkCSS, root_dir=self.rootDir)
      FILE_MKDIR, file_dirname(targetFile)
      file_copy, sourceFile, targetFile, /Overwrite
    endif
    
    if ~isa(linkJavaScript) then begin
      linkJavaScript = filepath('javascript.js', root_dir=self.fileDir)
      ;copy css file
      sourceFile = filepath('javascript.js', ROOT_DIR=hub_getDirname(), SUBDIRECTORY=['resource','html'])
      targetFile = filepath(linkJavaScript, root_dir=self.rootDir)
      FILE_MKDIR, file_dirname(targetFile)
      file_copy, sourceFile, targetFile, /Overwrite
    endif
    
     
    head = ['<head>' $
           ,'  <meta charset="ASCII" />' $
           ,'  <title>'+title+'</title>' $
           ,'  <meta name="description" content="hubAPI ReportList" />' $
           ,'  <meta name="author" content="'+metaAuthor+'" />' $
           ,'  <link rel="stylesheet" type="text/css" href="'+linkCSS+'" media="all" />' $
           ,'  <script type="text/javascript" src="'+linkJavaScript+'"> </script>' $
           ,'    <noscript> </noscript>' $
           ,'</head>' $
           ]
    return, transpose(head)
end

;+
; :Description:
;    Returns the HTML Code of a report section.
;
; :Params:
;    report: in, required, type=hubReport()
;     The hubReport() that is to present as HTML within this section.
;      
;    reportID: in, required, type=string
;     A valid id string to be used within the final HTML document to identify this report section.  
;     This id will be used similar to `<ID>` in::
;       
;       <section class="report" id="<id>">...</section>
;
;
;-
function hubReportHTMLHelper::getReportSection, report, reportID
  html = list()
  html.add, '<section class="report" id="'+reportID+'" style="display:block">'
  html.add, '  <header>'
  html.add, '   <h1>'+report.getTitle()+'</h1>' 
  html.add, '  </header>'
  
  foreach element, report._getElements(), elementIndex do begin
    case element.type of
    
      'heading' : begin
        level = element.level+1
        html.add, string(format='(%"<h%i>%s</h%i>")', level, self.maskHTMLCharacters(element.value), level)
                  end
      'monospace' : begin
        html.add, '  '+ transpose(['<pre><code>' $
                                   ,self.maskHTMLCharacters((element.value)[*]) $
                                   ,'</code></pre>' $
                                   ]), /Extract
                  end
      'paragraph' : begin
        
        html.add, '  '+ transpose(['<p>' $
                            ,self.maskHTMLCharacters((element.value)[*])+'<br />' $
                            , '</p>' $
                            ]), /Extract
       end
       
      'html'      : begin
          html.add, '  ' + transpose(element.value[*]), /Extract
          end
      'image' : begin
        imageLink = filepath(string(format='(%"img%s_%i")' $
                              , reportID, self.imageCounter) $
                              , root_dir=self.imageDir)
        imageFilename = filepath(imageLink, ROOT_DIR=self.rootDIR)
        if self.imageCounter eq 0 then begin
          file_mkdir, file_dirname(imageFilename)
        endif
        valueBSQ = element.value
        if size(valueBSQ, /N_DIMENSIONS) eq 2 then begin
          valueBSQ = [[[valueBSQ]],[[valueBSQ]],[[valueBSQ]]] 
        endif
        if element.flip then begin
          valueBSQ =  valueBSQ[*,reverse(indgen(n_elements(valueBSQ[0,*,0]))),*]
        endif
        valueBIP = transpose(valueBSQ, [2,0,1])
        write_png, imageFilename, valueBIP
        
        html.add, '  ' + [ $
          '<p>' , $
          '  <img src="'+imageLink+'"', $
          '   alt="Image: '+imageLink+'"/>', $
          '  <br /><i>'+self.maskHTMLCharacters(element.caption)+'</i></p>'], /Extract
        self.imageCounter++
      end
    endcase
  endforeach
  
  html.add, '</section>'
  return, transpose(html.toArray())
end


;+
; :Description:
;    Returns HTML Code to create the reports footer. 
;
; :Keywords:
;    metaAuthor: in, optional, type = string, default=systems login name + systime 
;     The name of the auhor of this report.
;     
;    linkHTML5Logo: in, optional, type = string, default=`http://www.w3.org/html/logo/downloads/HTML5_Logo.svg`
;       Link to the HTML5 Logo SVG file.
;       
;    linkCSSLogo: in, optional, type = string, default=`http://jigsaw.w3.org/css-validator/images/vcss`
;       Link to the CSS Logo Image 
;
;-
function hubReportHTMLHelper::getFooter $
  , metaAuthor=metaAuthor $
  , linkHTML5Logo=linkHTML5Logo $
  , linkCSSLogo=linkCSSLogo
   
   username = (get_login_info()).user_name
   
   metaAuthor = isa(metaAuthor) ? metaAuthor : strupcase(username)+'  '+ systime()
   linkHTML5Logo = isa(linkHTML5Logo) ? linkHTML5Logo : 'http://www.w3.org/html/logo/downloads/HTML5_Logo.svg'
   linkCSSLogo   = isa(linkCSSLogo)   ? linkCSSLogo   : 'http://jigsaw.w3.org/css-validator/images/vcss' 
   
   return, transpose([ $
     '<footer id="footer">' $
    ,'  <p> generated by '+ metaAuthor +'</p>' $
    ,'  <p>' $
    ,'    <a href="http://www.w3.org/html/logo/"> ' $
    ,'      <img style="border:0;width:132px;height:43px" src="'+linkHTML5Logo+'" alt="This report uses HTML 5."  /> ' $
    ,'    </a> ' $
    ,'    <a href="http://jigsaw.w3.org/css-validator/check/referer"> ' $
    ,'      <img style="border:0;width:88px;height:31px" src="'+linkCSSLogo+'" alt="CSS ist valide!"/> ' $ 
    ,'    </a>  ' $
    ,'  </p>' $
    ,'</footer>' $
    ])

end

;+
; 
; :Description:
;    Return the Navigation HTML Code 
;
; :Params:
;    reportNavigationTitles: in, required, type = string[]
;     Array of strings to be used as short titles in the navigation list.
;
; :Keywords:
;    reportIDs: out, optional, type=string[]
;     This keyword provides an ID for each single report. Use the IDs to 
;     create report section HTML code with `hubReportHTMLHelper::getReportSection`.
;
;-
function hubReportHTMLHelper::getNavigation, reportNavigationTitles, reportIDs=reportIDs
  nav = List()
  nav.add, '<nav id="nav">'
  nav.add, '  <h1>Navigation</h1>'      
  nav.add, '  <a id="NavLink_showAll" class="NavLink" href="javascript:showAllReports();" style="font-weight:bold">Show All</a>'
  nav.add, '  <ul>'  
  
  nReports = n_elements(reportNavigationTitles)
  reportIDs = sindgen(nReports)
  for i=0, nReports - 1 do begin
    reportID = string(format='(%"Report%i")', i+1)
    reportIDs[i] = reportID
    reportNavTitle = self.maskHTMLCharacters(reportNavigationTitles[i])
    nav.add, '    <li><a id = "NavLink_'+reportID $
           + '" class="navLink" href="javascript:focusReport('''+reportID+''');">' $
           + reportNavTitle + '</a></li>' 
  endfor
  nav.add, '  </ul>'
  nav.add, '</nav>'
  return, transpose(nav.toArray())
end



;+
; :Hidden:
;-
pro hubReportHTMLHelper__define
  struct = {hubReportHTMLHelper $
    , inherits IDL_Object $
    , rootDir:'' $
    , fileDir:'' $
    , imageDir:'' $
    , imageCounter:0l $
    , HTMLSpecialCharactersLUT:Hash() $
  }
end

pro test_hubreportHTMLHelper
  h = hubReportHTMLHelper()
  print, h.maskHTMLCharacters('m²m³')

end
