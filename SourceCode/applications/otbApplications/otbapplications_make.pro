pro otbApplications_make, Cleanup=cleanup, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, Distribution=distribution
  ; find application directory
  appDir = otbApplications_getDirname()
  if keyword_set(cleanup) then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    

    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ; find source code 
  codeDir =  file_dirname((routine_info('otbApplications_make',/SOURCE)).path)
  
  ; check lower case *.pro filenames (important for auto-compiling under unix systems
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  if ~keyword_set(CopyOnly) then begin
     ; save routines inside SAVE file
     
    SAVFile = filepath('otbApplications.sav', ROOT=appDir, SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, /NoLog
    
    if ~keyword_set(noIDLDoc) then begin
      ; create IDLDOC documentation
      
      helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
      hub_idlDoc, codeDir, helpOutputDir, 'otbApplications Documentation' $
                , Subtitle = 'otbApplications Subtitle' $
                , Overview = filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') 
     
    endif
    
    ; create distributen
    
    if isa(distribution) then begin
    
      if ~isa(distribution, 'string') then begin
        title = 'Select a directory for writing the distribution.'
        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
        if directory eq '' then return
      endif else begin
        directory = distribution
      endelse

      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
      file_copy, appDir, destination, /OVERWRITE, /RECURSIVE
      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
      file_copy, codeDir, destination, /OVERWRITE, /RECURSIVE
      
    endif
    
  endif
end
