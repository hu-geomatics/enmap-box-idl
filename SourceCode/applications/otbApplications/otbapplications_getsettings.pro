;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('otbApplications.conf', ROOT=otbApplications_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, otbApplications_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function otbApplications_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('otbApplications.conf'), ROOT=otbApplications_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_otbApplications_getSettings

  print, otbApplications_getSettings()

end
