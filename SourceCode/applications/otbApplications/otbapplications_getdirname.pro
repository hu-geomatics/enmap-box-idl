;+
; :Author: <author name> (<email>)
;-

function otbApplications_getDirname
  
  result = filepath('otbApplications', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_otbApplications_getDirname

  print, otbApplications_getDirname()
  print, otbApplications_getDirname(/SourceCode)

end
