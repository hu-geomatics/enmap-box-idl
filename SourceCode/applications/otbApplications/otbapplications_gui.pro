pro otbApplications_gui_helpEvent, parameters, UserInformation=userInformation
  hubHelper.openFile, /HTML, userInformation.moduleInfo.DOCUMENTATIONLINK
end
pro otbApplications_gui_applyEvent, parameters, UserInformation=userInformation
  compile_opt idl2
  
  moduleInfo = userInformation.moduleInfo
  outputImageFilter = userInformation.outputImageFilter
  outputVectorFilter = userInformation.outputVectorFilter
  
  
  ; remove emtpy parameters
  emptyValueKeys = ((parameters.keys())[(parameters.values()).where(!null)]).toArray()
  nullValueKeys = ((parameters.keys())[(parameters.values()).where('')]).toArray()
  removeKeys = [emptyValueKeys, nullValueKeys]
  if n_elements(removeKeys) gt 0 then begin
    parameters.remove, removeKeys
  endif
  outputImages = list()
  outputFiles = list()
  foreach info, moduleInfo.parameters do begin
    if parameters.hasKey(info.idlname) then begin

      ; remove if value is equal to default value
      if parameters[info.idlname] eq fix(info.OTBDEFAULT, TYPE=size(/TYPE, parameters[info.idlname])) then begin
        parameters.remove, info.idlname
      endif
    
      ; add image to images list and add GDAL ENVI file suffix
      if isa(outputImageFilter.where(info.OTBNAME)) then begin
        outputImages.add, parameters[info.idlname]
        parameters[info.idlname] = parameters[info.idlname]+'.hdr?&gdal:of:ENVI'
      endif

      ; add file to file list
      if isa(outputVectorFilter.where(info.OTBNAME)) then begin
        outputFiles.add, parameters[info.idlname]
      endif

    endif
  endforeach

  otb = hubOTBWrapper(/SHOWCOMMANDS)
  _extra = parameters.toStruct()
  if ~isa(_extra) then _extra = {empty:'no parameters'}
;  otb.Segmentation,  _EXTRA = _extra, /PassErrors, /SkipCheck, SpawnResult=spawnResult, SpawnError=spawnError, SpawnCmd=spawnCmd
  call_method, moduleInfo.name, otb, _EXTRA = _extra, /PassErrors, /SkipCheck, SpawnResult=spawnResult, SpawnError=spawnError, SpawnCmd=spawnCmd
  hubExternal_showErrorReport, spawnResult, spawnError, spawnCmd, Title='Orfeo Toolbox Application'

  foreach outputImage, outputImages do begin
    if file_test(outputImage) then begin
      enmapBox_openImage, outputImage
    endif
  endforeach

  foreach outputFile, outputFiles do begin
    if file_test(outputFile) then begin
      enmapBox_openFile, outputFile
    endif
  endforeach

end

pro otbApplications_gui, applicationName, Title=title, GroupLeader=groupLeader
  compile_opt idl2
  
  otb = hubOTBWrapper()
  moduleInfo = otb._getModuleInfo(applicationName)

  case applicationName of
    'Segmentation' : begin
      inputImageFilter = list('in','mode.vector.inmask')
      inputFileFilter = list()
      outputImageFilter = list('mode.raster.out')
      outputVectorFilter = list('mode.vector.out')
    end
    else : begin
      inputImageFilter = list()
      inputFileFilter = list()
      outputImageFilter = list()
      outputVectorFilter = list()
    end
  endcase
  
  tsize = 300
  xsize = 300
  resultHashKeys = list()
  userInformation = dictionary()
  userInformation.moduleInfo = moduleInfo
  userInformation.outputImageFilter = outputImageFilter
  userInformation.outputVectorFilter = outputVectorFilter
  hubAMW_program, Title='Orfeo Toolbox Application: '+title, /NoBlock
  foreach info, moduleInfo.parameters do begin
    resultHashKeys.add, info.IDLNAME
    defaultValue = info.OTBDEFAULT eq '' ? !null : info.OTBDEFAULT
    hubAMW_subframe, /ROW
    if info.OTBOPTIONS.isEmpty() then begin
      if (info.OTBTYPE eq 'boolean') then begin
        defaultValue = 0
        hubAMW_checklist, info.IDLNAME, Title=info.OTBNAME, Value=defaultValue , List=['off','on'], XSize=xsize, TSIZE=tsize
      endif else begin
        case 1b of
          isa(inputImageFilter.where(info.OTBNAME)) : hubAMW_inputImageFilename, info.IDLNAME, Title=info.OTBNAME, TSIZE=tsize, SIZE=xsize, /AllowEmptyImage
          isa(inputFileFilter.where(info.OTBNAME))  : hubAMW_inputFilename, info.IDLNAME, Title=info.OTBNAME, TSIZE=tsize, SIZE=xsize, /AllowEmptyFilename
          isa(outputImageFilter.where(info.OTBNAME)) : hubAMW_outputFilename, info.IDLNAME, Title=info.OTBNAME, TSIZE=tsize, SIZE=xsize, /AllowEmptyFilename, Value=info.OTBNAME
          isa(outputVectorFilter.where(info.OTBNAME)) : hubAMW_outputFilename, info.IDLNAME, Title=info.OTBNAME, TSIZE=tsize, SIZE=xsize, /AllowEmptyFilename, Value=info.OTBNAME+'.shp'
          else : begin
            isFloat = (info.OTBTYPE eq 'float') or (info.OTBTYPE eq 'double')
            isInteger = (info.OTBTYPE eq 'int32') or (info.OTBTYPE eq 'int64')
            isString = ~isFloat and ~isInteger
            hubAMW_parameter, info.IDLNAME, Title=info.OTBNAME, Value=defaultValue, String=isString, Integer=isInteger, Float=isFloat, TSIZE=tsize, ScrSIZE=xsize, /AllowEmptyValue
          end
        endcase
      endelse
    endif else begin
      defaultValue = info.OTBOPTIONS.where(defaultValue)
      hubAMW_combobox, info.IDLNAME, Title=info.OTBNAME, Value=defaultValue , List=info.OTBOPTIONS.toArray(Type='string'), /Extract, Size=xsize, TSIZE=tsize
    endelse
    hubAMW_label, info.OTBDESCRIPTION 
  endforeach
  hubAMW_subframe, /COLUMN
  hubAMW_label, ' '
  hubAMW_button, /Bar, Title='Apply', ResultHashKeys=resultHashKeys.toArray(), UserInformation=userInformation, EventHandler='otbApplications_gui_applyEvent'
  hubAMW_button, /Bar, Title='Help', UserInformation=userInformation, EventHandler='otbApplications_gui_helpEvent'
  !null = hubAMW_manage(ButtonNameAccept='', ButtonNameCancel='Quit')
end

pro test_otbApplications_gui
  applicationName = 'Segmentation'
  otbApplications_gui, applicationName, Title=applicationName, GroupLeader=groupLeader
end  
