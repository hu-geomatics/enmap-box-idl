pro otbApplications_createMenu
  compile_opt idl2

  ; get structure from txt file
  appStructureFile = filepath('otb_application_structure.txt', ROOT_DIR=enmapBox_getDirname(/SOURCECODE, /APPLICATIONS), SUBDIRECTORY=['otbApplications','_resource'])
  appStructure = hubIOASCIIHelper.readFile(appStructureFile)
  numberOfLines = n_elements(appStructure)
  
;  otb = hubOTBWrapper()
;  appNames = otb._getModuleNames()
;  appNames = appNames[sort(appNames)]

  text = list()
  text.add, '0 {Applications}
  text.add, '  1 {Orfeo Toolbox}

  for i=0, numberOfLines-1 do begin
    line = strtrim(appStructure[i],2)
    if line eq '' then begin
      ; new submenu in next line
      i++
      text.add, '    2 {'+appStructure[i]+'}'
      continue
    endif else begin
      text.add, '      3 {'+line+'} {'+line+'} {otbApplications_event}
    endelse
  endfor

;  text.add, '    2 {Segmentation} {Segmentation} {otbApplications_event}
;  foreach appName, appNames do begin
;    appInfo = otb._getModuleInfo(appName)
;      text.add, '    2 {'+appInfo.name+'} {'+appInfo.name+'} {otbApplications_event}
;  endforeach
  text.add, ''
  text.add, '0 {Help}
  text.add, '  1 {Orfeo Toolbox}
  text.add, '    2 {Official Website}      {http://www.orfeo-toolbox.org}               {enmapBoxUserApp_showHTML_event}
  text.add, '    2 {Applications Overview} {http://www.orfeo-toolbox.org/Applications}  {enmapBoxUserApp_showHTML_event}

  print,text

end  
