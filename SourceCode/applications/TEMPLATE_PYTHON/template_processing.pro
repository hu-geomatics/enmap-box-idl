;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`template_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;
;-
function template_processing, parameters, Title=title, GroupLeader=groupLeader

  parameters['myArgument'] = 'Hello World from EnMAP-Box :-).'
  
  scriptFilename = filepath('script.py', ROOT_DIR=template_getDirname(), SUBDIRECTORY='lib')
  scriptResult = hubPython_runScript(scriptFilename, parameters, spawnResult, spawnError, Title=title, GroupLeader=groupLeader)
  myResult = scriptResult['myResult']

  ; store results to be reported inside a hash
  result = hash()
  result['description'] = myResult+' This Python-script application does nothing usefull. It is for demonstration only.'+ strjoin(replicate('template text ', 100))
  result['table'] = [ $
    'template accuracy = 90%',$
    '',$
    'template 1 | template 2 | template 3',$
    '------------------------------------',$
    '      100% |       100% |       80% ',$
    '       70% |        50% |       80% ']
  result['image'] = bytscl(randomu(seed, 300, 300, 3))
  return, result
end

;+
; :Hidden:
;-
pro test_template_processing
  ; test your routine
  parameters = hash() ;replace by own hash definition
  result = template_processing(parameters)
  print, result
end  
