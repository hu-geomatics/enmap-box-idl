;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro EnSoMapSuite_make, Cleanup=cleanup, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, Distribution=distribution
  ensomask_make, CLEANUP = cleanup, COPYONLY = copyOnly, NOIDLDOC = noidldoc, DISTRIBUTION = distribution
  ensomap_make, CLEANUP = cleanup, COPYONLY = copyOnly, NOIDLDOC = noidldoc, DISTRIBUTION = distribution
  ensocal_make, CLEANUP = cleanup, COPYONLY = copyOnly, NOIDLDOC = noidldoc, DISTRIBUTION = distribution
  ensoval_make, CLEANUP = cleanup, COPYONLY = copyOnly, NOIDLDOC = noidldoc, DISTRIBUTION = distribution
  ensolib_make, CLEANUP = cleanup, COPYONLY = copyOnly, NOIDLDOC = noidldoc, DISTRIBUTION = distribution
end