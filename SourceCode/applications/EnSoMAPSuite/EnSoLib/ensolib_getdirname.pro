;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

function EnSoLib_getDirname
  
  result = filepath('EnSoLib', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnSoLib_getDirname

  print, EnSoLib_getDirname()
  print, EnSoLib_getDirname(/SourceCode)

end
