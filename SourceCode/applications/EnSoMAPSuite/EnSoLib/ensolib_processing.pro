;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
function EnSoLib_GetPointFromCoordinate, xpos, ypos, map_info, dim, WinSize, VALID = valid, OPTION = option
  if keyword_set(option) then begin
    x = xpos - 1
    y = ypos - 1
  endif else begin
    x0    = map_info['pixelX']   ;float(map_info[1]) - 1
    y0    = map_info['pixelY']   ;float(map_info[2]) - 1
    xref  = map_info['easting']  ;float(map_info[3])
    yref  = map_info['northing'] ;float(map_info[4])
    xstep = map_info['sizeX']    ;float(map_info[5])
    ystep = map_info['sizeY']    ;float(map_info[6])
    x = abs(xpos - xref) / xstep + x0
    y = abs(ypos - yref) / ystep + y0
  endelse

  xmin = long64(x - WinSize/2) > 0
  xmax = long64(x + WinSize/2) < (dim[0]-1)
  ymin = long64(y - WinSize/2) > 0
  ymax = long64(y + WinSize/2) < (dim[1]-1)

  get = [xmin, xmax, ymin, ymax]

  ; test get
  test_get = PRODUCT([get[0] GE 0, get[1] LT dim[0], get[0] LE get[1], $
    get[2] GE 0, get[3] LT dim[1], get[2] LE get[3]])

  if not test_get then begin
    valid = 0
    return, -1
  endif
  valid = 1
  return, get
end

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`EnSoLib_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
;    parameters : in, required, type=hash
;    
;-
function EnSoLib_processing, parameters, settings

  ; Read the csv file
  template = ascii_template(parameters['refField'], CANCEL = cancel)
  if cancel then return, hash()
  if template.fieldCount lt 3 then begin
    txt = ['The csv file is not correct, at least 3 columns']
    tmp = dialog_message(txt, /ERROR, /CENTER)
    return, hash()
  endif
  
  ; check that input data has a map-info
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  if not inputImage.hasMeta('map info') then begin
    tmp = ['MAP INFO is not set in your header!', $
      '', $
      'Check your header file']
    tmp = dialog_message(txt, /ERROR, /CENTER)
  endif
  
  ; get map info
  map_info = inputImage.getMeta('map info')
  
  ; check winsize
  case parameters['avWinSize'] of
    0: WinSize = 1
    1: WinSize = 3
    2: WinSize = 5
    3: WinSize = 7
  endcase
  
  ; read the ascii file
  data = read_ascii(parameters['refField'], TEMPLATE = template)
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  outputSL = obj_new('hubIOSLOutputSpeclib', parameters['outputImage'], $
    numberOfBands = inputImage.getMeta('bands'), $
    numberOfProfile = n_elements(data.field1), $
    dataType = inputImage.getMeta('data type'))
  outputSL.setMeta, 'wavelength', inputImage.getMeta('wavelength')
  outputSL.setMeta, 'wavelength units', inputImage.getMeta('wavelength units') 
  
  spectrum = make_array([inputImage.getMeta('bands'), n_elements(data.field1)], $
    TYPE = inputImage.getMeta('data type'))
  
  
  for k=0l, n_elements(data.field1) - 1 do begin
    box = EnSoLib_GetPointFromCoordinate(float(data.field2[k]), $
      float(data.field3[k]), map_info, $
      [inputImage.getMeta('samples'), inputImage.getMeta('lines')], $
      WinSize, VALID = valid, OPTION = parameters['latlonXY'])
    if valid then begin
      inputImage.initReader, SubsetSampleRange = [box[0], box[1]], $
        SubsetLineRange = [box[2], box[3]], /CUBE
      iData = inputImage.getData()
      inputImage.finishReader
      sz = size(iData, /DIMENSIONS)
      spectrum[*, k] = total(reform(iData, sz[0] * sz[1], sz[2]), 1) / (float(WinSize))^2
    endif
  endfor
  outputSL.writeData, spectrum
  outputSL.finishWriter
  
  result = hash()
  return, result

end

pro test_EnSoLib_processing
  result = EnSoLib_processing(parameters, Title='EnSoLib', GroupLeader=groupLeader)
  print, result
end  
