;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

pro ensolib_showHelp2, in2
  mess = ['E N S O L I B:                                                                  ', $
          '--------------------------------------------------------------------------------', $
          '                                                                                ', $
          'The Field "Reference Point File" corresponds to a, at least, 3 columns text file', $
          'in ASCII format                                                                 ', $
          '                                                                                ', $
          'The system is able to detect the separation symbol but you can preferably use   ', $
          'the symbol: COMMA (,) for separation between each column                        ', $
          '                                                                                ', $
          'The three first columns are:                                                    ', $
          '                                                                                ', $
          ' POINT NAME, X_POS, Y_POS                                                       ', $
          ' "NAME1",    12341, 12341                                                       ', $
          ' "NAME2",    12341, 12341                                                       ', $
          '                                                        ', $
          ' X_POS and Y_POS could be given in:                     ', $
          '    - Data Coordinates (LAT/LON, if georeferencement)   ', $
          '    - Image Coordinates (X/Y, if no georeferencement)   ', $
          '      (image coordinates start at [1,1], like in ENVI   ', $
          ' Do not forget to set the option!                       ', $
          '                                                        ', $
          '                                                                                ', $
          ' ATTENTION, IN THE WIZARD ACII TEMPLATE SELECT SET THE LINE:                    ', $
          '                Data start at line:                                             ', $
          ' TO 2 IF FIRST LINE OF THE FIELD REFERENCE POINT FILE HAS A HEADER              ', $
          '                                                                                ', $
          ' PLEASE USE THE ENGLISH DECIMAL SYMBOL: POINT (.) TO INDICATE DECIMAL VALUES    ']
   ok = dialog_message(mess, /INFORMATION, TITLE = 'EnSoLib - Help', /CENTER)
end
pro ensolib_showHelp3, in2
  return
end

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;-
function EnSoLib_getParameters, settings
  
  ;; 
  ;; D E S I G N  T H E   G U I 
  ;; 
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program, groupLeader, Title = settings['title']
    hubAMW_label, 'EnSoLib: Generate Spectral Library'
  ;-------------------------------------------------------
;  hubAMW_frame
;    hubAMW_subframe, /ROW
;      hubAMW_button, TITLE = ' Help '
;      hubAMW_button, TITLE = ' About '
;      hubAMW_button, TITLE = ' Contact '
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "INPUT"
    hubAMW_subframe, /ROW
      hubAMW_inputImageFilename,  'inputImage',  TITLE = 'Hyperspectral data file   '
    hubAMW_subframe, /ROW
      hubAMW_pickfile,  'refField',    TITLE = 'Reference Point File      '
      hubAMW_checklist, 'latlonXY', VALUE = 0, LIST = ['Lat/Lon', 'X/Y'], TITLE = ''
      hubAMW_button, TITLE = 'i', EVENTHANDLER = 'ensolib_showHelp2'
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "OUTPUT"
    hubAMW_subframe, /ROW
      hubAMW_outputFilename, 'outputImage', TITLE = 'Spectral Library data file'
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "PARAMETERS"
    hubAMW_subframe, /ROW
      hubAMW_checklist, 'avWinSize', VALUE = 1, LIST = ['1x1', '3x3', '5x5', '7x7'], $
        TITLE = 'Average Window Size: '
      ;hubAMW_button, TITLE = 'i', EVENTHANDLER = 'ensolib_showHelp3'
 
  parameters = hubAMW_manage(/Dictionary, BUTTONNAMEACCEPT='Launch')
  
  return, parameters
end

pro test_EnSoLib_getParameters
  settings = EnSoLib_getSettings()
  parameters = EnSoLib_getParameters(settings)
  print, parameters
end  
