;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application event handler.
;    
; :Params:
;    event: in, required, type=button event structure
;-
pro EnSoLib_event, event
  @huberrorcatch
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  EnSoLib_application, applicationInfo
end
