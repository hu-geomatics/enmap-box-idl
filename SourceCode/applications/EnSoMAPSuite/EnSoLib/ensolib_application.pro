;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `EnSoLib_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `EnSoLib_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `EnSoLib_showReport`).
; 
; :Params:
;     applicationInfo: in, optional, type=hash
; 
;-
pro EnSoLib_application, applicationInfo
  settings = EnSoLib_getSettings()
  settings = settings + applicationInfo
  parameters = EnSoLib_getParameters(settings)
  if parameters['accept'] then $
    reportInfo = EnSoLib_processing(parameters, settings)
end

pro test_EnSoLib_application
  applicationInfo = hash()
  EnSoLib_application, applicationInfo 
end  
