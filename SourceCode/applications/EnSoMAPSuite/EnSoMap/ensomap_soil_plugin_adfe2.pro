pro ensomap_soil_plugin_adfe2, iData, oData, iMask, GETINFO = getinfo, $
;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths

  tabMenu            = 'Iron'
  productInformation = 'Iron CRAD: 760 -> 1050'
  fileName           = '_IRON_CRAD_760_1050'
  selectedBands      = [760, 1050]
  mode               = 'crad'
  references = ['Perform HYSOMA continuum-removed absorption depth', $
         'between 760 nm and 1050 nm.']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ; get the mask
  ;if n_params() eq 3 then mask = iMask else mask = fltarr((size(iData, /DIMENSIONS))[1]) * 0.0 + 1.0

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---
  sz = size(iData, /DIM)
  oData = fltarr(sz[1]) + !values.f_nan
  for kkk = 0ll, sz[1] - 1 do begin
    y = reform(iData[*, kkk])

    ; check if there is a mask and if data are available
    if total(y) eq 0 or iMask[kkk] eq 0 then continue

    ; check if there is a spectrum in case of saturated data
    if total(abs(deriv(deriv(y)))) lt 5e-5 then continue

    x = indgen(sz[0])
    triangulate, x, y, tr, hull
    hull = reverse(hull)
    hull = [0, hull]
    indmax = where(hull eq (sz[0] - 1), ccc)
    if ccc eq 0 then continue
    indmax = indmax[0]
    ny = interpol(y[hull[0:indmax]], hull[0: indmax], indgen(sz[0]))
    ;Clark's quota
      iii = where(ny ne 0, cccc)
      quot = ny - ny
      if cccc ne 0 then quot[iii] = y[iii] / ny[iii]
      quotMax = max(1 - quot, pos)
      oData[kkk] = quotMax
  endfor
  ; quit the function
  return
end