;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro ensomap_soil_plugin_nsmi, iData, oData, iMask, GETINFO = getinfo, $
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths


  tabMenu            = 'Soil Moisture'
  productInformation = 'Normalized Soil Moisture Index (NSMI)'
  fileName           = '_MOISTURE_NSMI'
  selectedBands      = [1800, 2119]
  mode               = 'index'
  references = ['From:', $
         'Haubrock, S.-N., Chabrillat, S., Kuhnert, M., Hostert, P. and Kaufmann, H.', $
         'Surface soil moisture quantification and validation based on hyperspectral data and field measurements', $
         'Journal of Applied Remote Sensing, Vol. 2, 2008']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ; get the mask
  ;if n_params() eq 3 then mask = iMask else mask = fltarr((size(iData, /DIMENSIONS))[1]) * 0.0 + 1.0

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---
  num = reform(iData[0, *] - iData[1, *])
  den = reform(iData[0, *] + iData[1, *])

  ; generate the output image
  oData = num - num

  ; localize zero
  ind = where(den ne 0 and imask eq 1, nNonZeros, COMPLEMENT = indc, NCOMPLEMENT = nZeros)
  if nNonZeros ne 0 then oData[ind]  = num[ind] / den[ind]
  if nZeros    ne 0 then oData[indc] = !values.F_NAN

  ; quit the function
  return
end