;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
function EnSoMap_file_suffix, file
  name = file ; temporary variable in order not to modify the input file
  name = strsplit(name, '.', /extract, count = cc)
  if cc lt 1 then return, ''
  return, '.' + name[cc-1]
end

function EnSoMAP_processing, parameters, settings

  ;;;;;;;;;;;;;
  ; Information about directory
  outDir = parameters['outputDirName']
  if not file_test(outDir) then begin
    txt = file_basename(outDir) + 'does not exist! Create it?'
    ans = dialog_message(txt, /QUESTION, /CENTER)
    if strcmp(ans, 'NO', /FOLD_CASE) then return, hash()
    file_mkdir, outDir
  endif

  ;;;;;;;;;;;;;
  ; get information about output filename according input filename
  outFileName = file_basename((parameters['inputImage'])['featureFilename'], EnSoMap_file_suffix((parameters['inputImage'])['featureFilename']))

  ; get information about the location of all compiled routines
  listOfRoutines = routine_info()
  ind = where(strcmp(listOfRoutines, 'ensomap_soil_plugin', 19, /FOLD_CASE) eq 1, nSoilProducts)
  listOfPlugins = temporary(listOfRoutines[ind])
  
  ; initialize the number of calculated soil products
  nCalculatedSoilProducts = 0
  
  ;; Open the image file
  inputImage = hubIOImgInputImage((parameters['inputImage'])['featureFilename'])
  hdrSamples      = inputImage.getMeta('samples')
  hdrLines        = inputImage.getMeta('lines')
  hdrDescription  = file_basename(inputImage.getMeta('filename header')) + ': '
  hdrMapInfo      = inputImage.getMeta('map info')
  
  ;; Check if mask is given
  flagIsMaskGiven = (parameters['inputImage'])['labelFilename'] ne !NULL
  if flagIsMaskGiven then inputMask = hubIOImgInputImage((parameters['inputImage'])['labelFilename'])
  
  ;; Initialize the tile lines
  tileLines = 100
  
  
  ;; perform a loop over all soil products
  for kSoilProduct = 0, nSoilProducts - 1 do begin
    
    ; check if the soil product is asked
    if parameters[listOfPlugins[kSoilProduct]] eq 0 then continue
    
    ; get information about the soil product
    call_procedure, listOfPlugins[kSoilProduct], GETINFO = soilProductInfo
    
    ;-------------------- GET BAND INDEX ----------------------------------------------------------------------------------
  	flagSelectedBand = 1
    nSelectedBands = n_elements(soilProductInfo.selectedBands)
    bandIndex      = lonarr(nSelectedBands)
    for kk = 0, nSelectedBands - 1 do $
      bandIndex[kk] = inputImage.locateWavelength((soilProductInfo.selectedBands)[kk])

    ; --> Index
    if strcmp(soilProductInfo.mode, 'index', /FOLD) then begin
      tmp = uniq(bandIndex)
      if n_elements(tmp) le 1 then flagSelectedBand = 0
    endif
    
    ; --> CRAD
    if strcmp(soilProductInfo.mode, 'crad', /FOLD) then begin
      nBands = bandIndex[-1] - bandIndex[0] + 1
      if nBands lt 3 $
        then flagSelectedBand = 0 $
        else bandIndex = lindgen(nBands) + bandIndex[0]
    endif
    
    ; --> SMGM
    if strcmp(soilProductInfo.mode, 'smgm', /FOLD) then begin
      tmp = uniq(bandIndex)
      if n_elements(tmp) ne 3 $
        then flagSelectedBand = 0 $
        else begin
          nBands = bandIndex[1] - bandIndex[0] + 1
          tmp = lonarr(nBands + 1)
          tmp[0] = bandIndex[2]
          tmp[1:*] = lindgen(nBands) + bandIndex[0]
          bandIndex = temporary(tmp)
      endelse
    endif
    
    if flagSelectedBand eq 0 then continue
    ;----------------------------------------------------------------------------------------------------------------------
    
    ; open output image
    outputImage = hubIOImgOutputImage(outDir + outFileName + soilProductInfo.fileName + '.dat')
    
    ; setup the progress bar
    progressBar = hubProgressBar(TITLE = 'EnSoMAP', GROUPLEADER = settings.hubGetValue('groupLeader'))
    progressBar.setInfo, 'Calculating the ' + soilProductInfo.productInformation
    progressBar.setRange, [0, inputImage.getMeta('lines')]
    progressDone = 0
    
    ; initialize the input image reader according to the soil product
    inputImage.initReader, tileLines, /TILEPROCESSING, /SLICE, SUBSETBANDPOSITIONS = bandIndex

    ; initialize the input mask reader
    if flagIsMaskGiven then inputMask.initReader, tileLines, /TILEPROCESSING, /SLICE
    
    ; initialize the soil product image writer
    outputImage.setMeta, 'samples', hdrSamples
    outputImage.setMeta, 'lines',   hdrLines
    outputImage.setMeta, 'bands',   1
    outputImage.setMeta, 'description', hdrDescription + soilProductInfo.productInformation
    outputImage.setMeta, 'header offset', 0
    outputImage.setMeta, 'file type', 'ENVI Standard'
    outputImage.setMeta, 'data type', 4
    outputImage.setMeta, 'interleave', 'BSQ'
    outputImage.setMeta, 'byte order', 0
    outputImage.setMeta, 'map info', hdrMapInfo
    outputImage.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')
    
    ; loop over the whole image
    while ~inputImage.tileProcessingDone() do begin
      
      ; read the input image
      iData = inputImage.getData()
      case size(iData, /TYPE) of
        2:    iData = float(iData) / 10000.
        12:   iData = float(iData) / 100000.
        else: iData = float(iData)
      endcase
      
      ; read the input mask
      if flagIsMaskGiven then iMask = float(inputMask.getData()) else iMask = reform(iData[0, *]) * 0.0 + 1.0
      
      ; Call the procedure to get the soil product
      call_procedure, listOfPlugins[kSoilProduct], iData, oData, iMask, BANDINDEX = bandIndex, $
        WAVELENGTHS = inputImage.getMeta('wavelength')
      
      ; write output image
      outputImage.writeData, oData
      
      ; update the progress bar
      progressDone += tileLines
      progressBar.setProgress, progressDone

    endwhile
    
    ; Close output image
    outputImage.cleanup
    
    ; Close progress bar
    progressBar.cleanup
    
    ; reset input image
    inputImage.finishReader
    
    ; reset the mask 
    if flagIsMaskGiven then inputMask.finishReader
    
    ; update the number of calculated soil product counter.
    nCalculatedSoilProducts++
    
  endfor
  inputImage.cleanup
  if flagIsMaskGiven then inputMask.cleanup

  ; display that no soil product has been calcualted due to no selection
  if nCalculatedSoilProducts eq 0 then $
    tmp = dialog_message('No soil product has been calculated!', /ERROR, /CENTER, $
      TITLE="EnSoMap: Generate Soil Product Maps")

  result = hash()
  return, result

end


pro test_EnSoMAP_processing
  result = EnSoMAP_processing(parameters, Title='EnSoMAP', GroupLeader=groupLeader)
  print, result
end  
