@ensomap_soil_plugin_adc1
@ensomap_soil_plugin_adclay1
@ensomap_soil_plugin_adfe1
@ensomap_soil_plugin_adfe2
@ensomap_soil_plugin_clay1
@ensomap_soil_plugin_fe1
@ensomap_soil_plugin_nsmi
@ensomap_soil_plugin_smgm
@ensomap_soil_plugin_soc1
@ensomap_soil_plugin_soc2
@ensomap_soil_plugin_soc3

;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;   settings: in, optional, type=hash
;-
function EnSoMAP_getParameters, settings

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; GET SOIL PLUGINS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  ; get information about the location of all compiled routines
  listOfRoutines = routine_info()
  
  ; localize 'ensomap_soil_plugin' in the list
  ind = where(strcmp(listOfRoutines, 'ensomap_soil_plugin', 19, /FOLD_CASE) eq 1, nPlugins)
  
  ; reduce the list of routines to a list of plugins (we are sure there are some plugins)
  listOfPlugins = temporary(listOfRoutines[ind])
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; DESIGN THE INTERFACE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  ; get the group leader
  groupLeader = settings.hubGetValue('groupLeader')
  
  ; get the main base
  hubAMW_program, groupLeader, TITLE = settings['title']
  hubAMW_label, 'EnMAP Soil Mapper (EnSoMAP)'
  
  ; line of help, about, contact
  ;hubAMW_frame
  ;hubAMW_subframe, /ROW
  ;hubAMW_button, TITLE = ' Help '
  ;hubAMW_button, TITLE = ' About '
  ;hubAMW_button, TITLE = ' Contact '

  ; Frame for input file
  hubAMW_frame
    hubAMW_inputSampleSet, 'inputImage', /MASKING, TITLE = 'Input-Image            ', /REFERENCEOPTIONAL
  
  ; Frame for soil products
  hubAMW_frame
  hubAMW_tabFrame 
  menuArray = strarr(nPlugins)
  soilProductInformationArray = strarr(nPlugins)
  for k = 0, nPlugins - 1 do begin
    call_procedure, listOfPlugins[k], GETINFO = pluginInfo
    menuArray[k] = pluginInfo.tabMenu
    soilProductInformationArray[k] = pluginInfo.productInformation
  endfor
  
  indSortMenuArray     = sort(menuArray)
  indUniqSortMenuArray = uniq(menuArray[indSortMenuArray])
  nUniqSortMenuArray   = n_elements(indUniqSortMenuArray)
  flagChangeMenu       = 0
  kOverMenu            = 0
  kOverSubProduct      = 0
  
  ; loop over all soil plugins
  for kPlugin = 0, nPlugins - 1 do begin
    
    ; display new menu
    if kPlugin eq 0 or flagChangeMenu eq 1 then begin
      hubAMW_tabpage, TITLE = menuArray[indSortMenuArray[indUniqSortMenuArray[kOverMenu]]]
      flagChangeMenu = 0
      kOverSubProduct = 0
    endif
    
    
    hubAMW_subframe, /ROW
    hubAMW_button, TITLE='i', EVENTHANDLER = listOfPlugins[indSortMenuArray[kPlugin]]
    hubAMW_checkbox, listOfPlugins[indSortMenuArray[kPlugin]], $
      TITLE = soilProductInformationArray[indSortMenuArray[kPlugin]]
    kOverSubProduct++
    
    ; check if menu has to be change
    if kPlugin eq indUniqSortMenuArray[kOverMenu] then begin
      kOverMenu++
      flagChangeMenu = 1
    endif
  endfor
  
  hubAMW_frame
    hubAMW_outputDirectoryName, 'outputDirName', TITLE = 'Output Directory'
 
  parameters = hubAMW_manage(/Dictionary)
  
  return, parameters
end

pro test_EnSoMAP_getParameters
  settings = EnSoMAP_getSettings()
  parameters = EnSoMAP_getParameters(settings)
  print, parameters
end  
