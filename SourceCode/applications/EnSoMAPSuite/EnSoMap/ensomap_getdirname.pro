;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

function EnSoMAP_getDirname
  
  result = filepath('EnSoMAP', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnSoMAP_getDirname

  print, EnSoMAP_getDirname()
  print, EnSoMAP_getDirname(/SourceCode)

end
