;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('EnSoMAP.conf', ROOT=EnSoMAP_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, EnSoMAP_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function EnSoMAP_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('EnSoMAP.conf', ROOT=EnSoMAP_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_EnSoMAP_getSettings

  print, EnSoMAP_getSettings()

end
