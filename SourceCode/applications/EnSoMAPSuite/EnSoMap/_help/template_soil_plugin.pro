;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           T E M P L A T E   F O R   S O I L   P L U G I N                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;;
;; This file is a template to generate a soil product calculation as soil plugin
;; It contains 2 parts that have to be edited and one part that should not, 
;; otherwise the plugin will not work.
;;
;; Soil product plugin file is intended to simplify the creation of soil product
;; to be inserted inside EnSoMap. As this routine will become a part of EnSoMAP
;; some restrictions have to be defined.
;;
;;  1) The name of the routine should ALWAYS starts with "ensomap_soil_plugin_",
;;     then comes the desired name (which is off importance).
;;     Example: "ensomap_soil_plugin_fe1.pro". One can use any case for each 
;;     letter. This is important because EnSoMAP is looking for compiled routine
;;     names that start with "ensomap_soil_plugin", in order to identify them as
;;     soil plugin.
;;
;;  2) Do not change the name of the variable. As you imagine these variables are
;;     used in the main program to identify the different information of your 
;;     product. Example: tabMenu = 'Iron'. This variable is used to identify the
;;     tab title where the soil product will be displayed. You can choose one of
;;     the already defined, or create an original one. Note that is could be in
;;     confortable if using too many tab name.
;;
;;  3) Do not change the name and the number and the place of each input variable
;;     "iData", "oData", "iMask", and let the keywords as they are 
;;         GETINFO = getInfo, BANDINDEX = bandIndex, WAVELENGTHS = wavelengths
;;     This point is important too. "iData", "oData" and "iMask" are defined in 
;;     EnSoMAP in order to read and save the data.
;;
;;  4) The format of the input data is:
;;      - "iData": 2D data with [# bands, # pixels]. To have access to one specific
;;                 band, use "data = reform(iData[n, *])
;;                 The number of band corresponds to the number of band defined with
;;                 "selectedBand = [XXX, YYY, ...]". Here we have to considere 2 cases
;;                 (a) we need only individual bands, then the bands are stored in the
;;                     same order as given by "selectedBand". Example, if selectedBand
;;                     is defined as follow: "selectedBand = [420, 1250, 453]" then
;;                     "iData" will be defined as 3 x nPixels array, where the first 
;;                     column contains the reflectance at 420 nm (or the closest), 
;;                     the second column will contain the reflectance at 1250, etc.
;;                 (b) we need a continuous spectral between to bands. This you have to
;;                     set the "mode" variable to "crad" for continuum removal absorption
;;                     depth. Then the input data will contain all bands between the 
;;                     given min and max.
;;      - "oData": a 1D data [# pixels]. That is the result of the product calculation.
;;                 Pay attention not to send back a 2D data otherwise the routine will fail.   
