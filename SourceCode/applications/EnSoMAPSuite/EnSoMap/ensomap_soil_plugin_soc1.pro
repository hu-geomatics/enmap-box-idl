;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro ensomap_soil_plugin_soc1, iData, oData, iMask, GETINFO = getinfo, $
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths


  tabMenu            = 'Organic Carbon'
  productInformation = 'SOC [1] 1/Sum 400-700 nm'
  fileName           = '_SOC1_0400_0700'
  selectedBands      = [400, 700]
  mode               = 'crad'
  references = ['From:', $
         'Bartholomeus, H.M., Schaepman, M.E., Kooistra, L., Stevens, A., Hoogmoed, W.B., and Spaargaren, O.S.P.', $
         'Spectral reflectance based indices for soil organic carbon quantification', $
         'Geoderma, Vol. 145, 28-36, 2008']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ; get the mask
  ;if n_params() eq 3 then mask = iMask else mask = fltarr((size(iData, /DIMENSIONS))[1]) * 0.0 + 1.0

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---
  sz = size(iData, /DIM)
  oData = fltarr(sz[1]) + !values.f_nan
  for kkk = 0ll, sz[1] - 1 do begin
    y = reform(iData[*, kkk])

    ; check if there is a mask and if data are available
    if total(y) eq 0 or iMask[kkk] eq 0 then continue

    ; check if there is a spectrum in case of saturated data
    if total(abs(deriv(deriv(y)))) lt 5e-5 then continue

    x = indgen(sz[0])
    triangulate, x, y, tr, hull
    hull = reverse(hull)
    hull = [0, hull]
    indmax = where(hull eq (sz[0] - 1), ccc)
    if ccc eq 0 then continue
    indmax = indmax[0]
    ny = interpol(y[hull[0:indmax]], hull[0: indmax], indgen(sz[0]))
    ;
      iii = where(ny ne 0, cccc)
      quot = ny - ny
      if cccc ne 0 then quot[iii] = ny[iii] - y[iii]
      sum = total(quot)
      if sum ne 0 then oData[kkk] = 1.0 / sum
  endfor


  ; quit the function
  return
end