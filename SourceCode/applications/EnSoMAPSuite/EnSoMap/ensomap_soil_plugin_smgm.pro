;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro smgm_gauss_funct, x, a, f
  compile_opt idl2, hidden
  on_error, 2

  nx = n_elements(x)
  lambda_0 = 2.8
  sigma    = a[1]
  Rd       = a[0]
  if sigma ne 0.0 then begin
    z = (x - lambda_0) / sigma
    ez = exp(-z^2 / 2.)
  endif else begin
    z = replicate(fix(100, type = size(x, /TYPE)), nx)
    ez = z * 0
  endelse
  f = 1 - Rd * ez
end


pro ensomap_soil_plugin_smgm, iData, oData, iMask, GETINFO = getinfo, $
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths


  tabMenu            = 'Soil Moisture'
  productInformation = 'Soil Moisture Gaussian Model (SMGM)'
  fileName           = '_MOISTURE_SMGM'
  selectedBands      = [500, 2410, 2390]
  mode               = 'smgm'
  references = ['From:', $
         'Whiting, M.L., Li, L., and Ustin, S.L.', $
         'Predicting water content using Gaussian model on soil spectra', $
         'Remote Sensing of the Environment, Vol. 89, 535-552, 2004']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ; get the mask
  ;if n_params() eq 3 then mask = iMask else mask = fltarr((size(iData, /DIMENSIONS))[1]) * 0.0 + 1.0

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---
  
  ; get the size of the input image
  sz = SIZE(iData, /DIM) ; get the size of the image
  
  ; create the output image
  oData = FLTARR(sz[1]) + !values.f_nan

  ; set waves
  wave2 = wavelengths[bandIndex[1:*]]
  if mean(wave2) gt 100 then wave2 /= 1000.
  
  for kkk = 0ll, sz[1] - 1 do begin
    
    ; copy wave2 in order to keep an original sources
    wave = wave2
    
    ; reform the input image
    refl = reform(iData[*, kkk])
    
    ; check if there is a mask and if data are available
    if total(refl) eq 0 or iMask[kkk] eq 0 then continue
    
    ; check if there is a spectrum inm case of saturated data
    if total(abs(deriv(deriv(refl[1:*])))) lt 5e-5 then continue
    
    ; get the reflectance at 2.390
    Rd = refl[0]
    refl = refl[1:*]
    
    ; look for 0 insize refl
    indRefl = where(refl ne 0, ccc)
    if ccc ne 0 then begin
      refl = refl[indRefl]
      wave = wave[indRefl]
    endif
    
    ; STEP 1: convert relative reflectance to natural log of reflectance
    ln_refl = alog(refl * 100.)

    ; STEP 3: Determine prominent local maximum reflectance indices
    triangulate, wave, ln_refl, tr, hu
    hu = reverse(shift(hu, -1))
    ind = where(hu eq ((size(ln_refl, /DIM))[0] - 1), ccc)
    if ccc eq 0 then continue
    hu = hu[0:ind]
      
    ; 3.5.4: locate the maximum reflectance to the long wavelength
    maxRefl = max(ln_refl[hu], posMaxRefl)
    nln_refl = ln_refl[hu[posMaxRefl:*]]
    nln_refl /= maxRefl ; normalise
    
    ; Initialise Rd
    Rd = 1.0 - alog(Rd * 100.) / maxRefl
    
    ; Initialise Lambda_0
    l0 = 2.8
    
    ; Initialise Sigma
    Sigma = 0.5
    
    nn_wave = wave[hu[posMaxRefl:*]]
    if n_elements(nln_refl) le 2 then continue
      
    coef = [Rd, Sigma]
    yfit = curvefit(nn_wave, nln_refl, www, coef, function_name = "SMGM_GAUSS_FUNCT", itmax = 20, /NODERIVATIVE, $
      STATUS = st)
    Rd    = coef[0]
    sigma = coef[1]
    li    = wave[hu[posMaxRefl]]
    if st eq 0 then oData[kkk] = Rd * sigma * sqrt(!pi/2.) * erf((l0 - li) / (sqrt(2) * sigma))
    
  endfor

  ; quit the function
  return
end