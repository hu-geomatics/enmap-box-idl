;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `EnSoMAP_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `EnSoMAP_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `EnSoMAP_showReport`).
; 
; :Params:
;   applicationInfo: in, optional, type=hash
;   
;-
pro EnSoMAP_application, applicationInfo 
  
  ;;;;;;;;;; RESTORE SOIL PLUGINS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  ; Localize the directory where the application is
  appEnsomapDir = file_dirname((routine_info('EnSoMAP_application', /SOURCE)).path)
  
  test1 = file_test(appEnsomapDir + path_sep() + '_lib/soil_plugins', /directory)
  test2 = file_test(appEnsomapDir + path_sep() + 'soil_plugins', /DIRECTORY)
  if test1 then plugins_path = appEnsomapDir + path_sep() + '_lib/soil_plugins'
  if test2 then plugins_path = appEnsomapDir + path_sep() + 'soil_plugins'
  
  ; retrieve all .pro and .sav plugins
  plugins_pro = file_search(plugins_path + path_sep() + '*.pro')
  plugins_sav = file_search(plugins_path + path_sep() + '*.sav')
  n_pro = (size(plugins_pro, /DIMENSIONS))[0]
  n_sav = (size(plugins_sav, /DIMENSIONS))[0]
  
  ; compile the .pro files
  if n_pro gt 0 then begin
    cd, CURRENT = tmp
    cd, plugins_path
    for k = 0, n_pro - 1 do begin
      resolve_routine, file_basename(plugins_pro[k], '.pro'), /COMPILE_FULL_FILE, /NO_RECOMPILE, /EITHER
    endfor
    cd, tmp
  endif
  
  ; restore .sav file corresponding to some soil products
  ; The soil product in a sav mode should only be saved using the standard code.
  if n_sav gt 0 then begin
    for k = 0, n_sav - 1 do restore, plugins_sav[k]
  endif

  ; get global settings for this application
  settings = EnSoMAP_getSettings()
  
  parameters = EnSoMAP_getParameters(settings)
  
  if parameters['accept'] then $
    reportInfo = EnSoMAP_processing(parameters, settings)
end

pro test_EnSoMAP_application
  applicationInfo = hash()
  EnSoMAP_application, applicationInfo
end  
