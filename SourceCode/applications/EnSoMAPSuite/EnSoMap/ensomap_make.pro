;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This routine builds the final application. It uses the following steps:
;    
;    1. Create the application directory (`enmapProject/EnSoMAP`) and its subdirectories
;     
;    2. Copy all non *.pro files the application directory, e.g. the enmap.men
;    
;    3. Compile all *.pro files and save its routines to `EnSoMAP/lib/EnSoMAP.sav`
;    
;    4. Create the source code documentation by IDLDoc and write it to `EnSoMAP/help/` 
;     
;
; :Keywords:
;    Cleanup: in, optional, type = boolean
;     Set this to remove this application from its installation directory.
;     
;    CopyOnly: in, optional, type = boolean
;     Set this to create the final folder and file structure only without compiling or running IDLDoc. 
;     
;    NoIDLDoc: in, optional, type = boolean
;     Set this to suppress running IDLDoc. 
;     
;    Distribution: in, optional, type = directory path or boolean
;      Set this to specify an other installation directory. If used as booleand keyword, 
;      a dialog will appear and ask you for the installation directory. 
;        
;
;-
pro EnSoMAP_make, Cleanup=cleanup, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, Distribution=distribution
  ; find application directory
  appDir = EnSoMAP_getDirname()
  if keyword_set(cleanup) then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    

    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ; find source code 
  codeDir =  file_dirname((routine_info('EnSoMAP_make',/SOURCE)).path)
  
  ; check lower case *.pro filenames (important for auto-compiling under unix systems
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  if ~keyword_set(CopyOnly) then begin
     ; save routines inside SAVE file
     
    SAVFile = filepath('EnSoMAP.sav', ROOT=appDir, SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, /NoLog
    
    if ~keyword_set(noIDLDoc) then begin
      ; create IDLDOC documentation
      
      helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
      hub_idlDoc, codeDir, helpOutputDir, 'EnSoMAP Documentation' $
                , Subtitle = 'EnSoMAP Subtitle' $
                , Overview = filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') 
     
    endif
    
    ; create distributen
    
    if isa(distribution) then begin
    
      if ~isa(distribution, 'string') then begin
        title = 'Select a directory for writing the distribution.'
        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
        if directory eq '' then return
      endif else begin
        directory = distribution
      endelse

      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
      file_copy, appDir, destination, /OVERWRITE, /RECURSIVE
      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
      file_copy, codeDir, destination, /OVERWRITE, /RECURSIVE
      
    endif
    
  endif
end
