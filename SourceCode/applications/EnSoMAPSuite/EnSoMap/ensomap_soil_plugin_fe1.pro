;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-



pro ensomap_soil_plugin_fe1, $ ; <-- The soil plugin start always with
  iData, oData, iMask, GETINFO = getinfo, $
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths
  
  

  
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;
  ; Here you can edit the contents but do not change the name of the variables
  ;

  ; Tab menu (Try to choose an existing one)
  tabMenu = 'Iron'

  ; What will be display in front of the checkbox button
  productInformation = 'Redness Index (RI)'

  ; Endin of the filename (without extension, will be add later)
  ; Try to select a comprehensible filename.
  fileName = '_IRON_RI'

  ; Set Bands (in the order that you like it)
  ; For Continuum Removal Absorption Depth, set min and max
  ; Bands should be given in nanometers
  selectedBands = [477, 556, 694]

  ; Set the mode: 'index' for indices, 'CRAD' for continuum removal absorption depth,
  ;               'SMGM' for the SMGM routine
  mode = 'index'

  ; Set some reference: equation of the indices, the literature, ...
  ; This will be displayed when click on 'i' button.
  ; Do not write the text in one line, use array of string (problem displaying on linux)
  references = ['From:', $
    'Madeira, J., Bedidi, A., Cervelle, B., Pouget, M. and Flay, N.', $
    'Visible spectrometric indices of hematite (Hm) and goethite (Gt) content in lateritic soils:',$
    'the application of a Thematic Mapper (TM) image for soil-mapping in Brasilia, Brazil', $
    'Int. J. Remote Sens., vol. 18, no. 13, pp. 2835 - 2852, 1997']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---

  ; get the bands
  R0447 = reform(iData[0, *])
  R0556 = reform(iData[1, *])
  R0693 = reform(temporary(iData[2, *]))

  ; calculate the index
  den = R0447 * (R0556^3)
  num = R0693^2

  ; generate the output image
  oData = num - num

  ; localize zero
  ind = where(den ne 0 and iMask eq 1, nNonZeros, COMPLEMENT = indc, NCOMPLEMENT = nZeros)
  if nNonZeros ne 0 then oData[ind]  = num[ind] / den[ind]
  if nZeros    ne 0 then oData[indc] = !values.F_NAN

  ; return the new array
  return
end