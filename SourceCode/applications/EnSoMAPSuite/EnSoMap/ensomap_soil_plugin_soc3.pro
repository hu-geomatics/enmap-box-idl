;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro ensomap_soil_plugin_soc3, iData, oData, iMask, GETINFO = getinfo, $
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths


  tabMenu            = 'Organic Carbon'
  productInformation = 'SOC [3] 1/Slope 2138-2200 nm'
  fileName           = '_SOC3_2138_2200'
  selectedBands      = [2138, 2200]
  mode               = 'index'
  references = ['From:', $
         'Bartholomeus, H.M., Schaepman, M.E., Kooistra, L., Stevens, A., Hoogmoed, W.B., and Spaargaren, O.S.P.', $
         'Spectral reflectance based indices for soil organic carbon quantification', $
         'Geoderma, Vol. 145, 28-36, 2008']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ; get the mask
  ;if n_params() eq 3 then mask = iMask else mask = fltarr((size(iData, /DIMENSIONS))[1]) * 0.0 + 1.0

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---
  nbands = bandIndex[1] - bandIndex[0] 
  slopes = reform(iData[1, *] - iData[0, *]) / float(nbands)
  
  ; generate out
  oData = (slopes - slopes) * !values.f_nan 
  
  ; locate zero and calculate indice
  ind = where(slopes ne 0 and iMask EQ 1, cc)
  if cc ne 0 then oData[ind] = 1. / slopes[ind]
  ; quit the function
  return
end