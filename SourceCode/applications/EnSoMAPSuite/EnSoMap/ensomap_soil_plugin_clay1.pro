;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro ensomap_soil_plugin_clay1, iData, oData, iMask, GETINFO = getinfo, $
  BANDINDEX = bandIndex, WAVELENGTHS = wavelengths


  tabMenu            = 'Clay'
  productInformation = 'Short-Wave Infrared Fine Particle Index (SWIR FI)'
  fileName           = '_CLAY_SWIRFI'
  selectedBands      = [2133, 2209, 2255]
  mode               = 'index'
  references = ['From:', $
         'Levin, N., Kidron, G.J. and Ben-Dor, E. ', $
         ' Surface properties of stabilizing coastal dunes: combining spectral and field analyses', $
         'Sedimentology, Vol. 54, 771-788, 2007']

  ;;; --- DO NOT MODIFY ---
  if arg_present(getinfo) then begin
    getinfo = create_struct('productInformation', productInformation, $
      'tabMenu',            tabMenu,            $
      'fileName',           fileName,           $
      'selectedBands',      selectedBands,      $
      'mode',               mode,               $
      'references',         references)
    return
  endif

  if n_params() eq 1 then begin
    ok = dialog_message(references, /INFORMATION, /CENTER)
    return
  endif

  ; get the mask
  ;if n_params() eq 3 then mask = iMask else mask = fltarr((size(iData, /DIMENSIONS))[1]) * 0.0 + 1.0

  ;;; --- EDIT ACCORDING TO THE SOIL FUNCTION ---
  R2133 = reform(iData[0, *])
  R2209 = reform(iData[1, *])
  R2225 = reform(temporary(iData[2, *]))

  ; calculate the index
  den = R2225 * (R2209^3)
  num = R2133^2

  ; generate the output image
  oData = num - num

  ; localize zero
  ind = where(den ne 0 and iMask eq 1, nNonZeros, COMPLEMENT = indc, NCOMPLEMENT = nZeros)
  if nNonZeros ne 0 then oData[ind]  = num[ind] / den[ind]
  if nZeros    ne 0 then oData[indc] = !values.F_NAN

  ; quit the function
  return
end