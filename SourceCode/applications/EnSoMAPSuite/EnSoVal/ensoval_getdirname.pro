;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

function EnSoVal_getDirname
  
  result = filepath('EnSoVal', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnSoVal_getDirname

  print, EnSoVal_getDirname()
  print, EnSoVal_getDirname(/SourceCode)

end
