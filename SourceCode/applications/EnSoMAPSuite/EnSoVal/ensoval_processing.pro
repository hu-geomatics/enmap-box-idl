;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
function EnSoVal_GetPointFromCoordinate, xpos, ypos, map_info, dim, WinSize, VALID = valid, OPTION = option
  if keyword_set(option) then begin
    x = xpos - 1
    y = ypos - 1
  endif else begin
    x0    = map_info['pixelX']   ;float(map_info[1]) - 1
    y0    = map_info['pixelY']   ;float(map_info[2]) - 1
    xref  = map_info['easting']  ;float(map_info[3])
    yref  = map_info['northing'] ;float(map_info[4])
    xstep = map_info['sizeX']    ;float(map_info[5])
    ystep = map_info['sizeY']    ;float(map_info[6])
    x = abs(xpos - xref) / xstep + x0
    y = abs(ypos - yref) / ystep + y0
  endelse

  xmin = long64(x - WinSize/2) > 0
  xmax = long64(x + WinSize/2) < (dim[0]-1)
  ymin = long64(y - WinSize/2) > 0
  ymax = long64(y + WinSize/2) < (dim[1]-1)

  get = [xmin, xmax, ymin, ymax]

  ; test get
  test_get = PRODUCT([get[0] GE 0, get[1] LT dim[0], get[0] LE get[1], $
    get[2] GE 0, get[3] LT dim[1], get[2] LE get[3]])

  if not test_get then begin
    valid = 0
    return, -1
  endif
  valid = 1
  return, get
end

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`EnSoVal_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
;    parameters : in, required, type=hash
;    
;-
function EnSoVal_processing, parameters, settings

  ; Read the csv file
  template = ascii_template(parameters['refField'], CANCEL = cancel)
  if cancel then return, hash()
  if template.fieldCount lt 3 then begin
    txt = ['The csv file is not correct, at least 3 columns']
    tmp = dialog_message(txt, /ERROR, /CENTER)
    return, hash()
  endif
  
  ; check if the file has a map-info
  soilProduct = objarr(n_elements(parameters('inputFileName')))
  hasMap      = bytarr(n_elements(parameters('inputFileName')))
  for k=0, n_elements(parameters('inputFileName')) - 1 do begin
    soilProduct[k] = hubIOImgInputImage((parameters['inputFileName'])[k])
    hasMap[k] = (soilProduct[k]).hasMeta('map info')
  endfor
  ind = where(hasMap eq 1, CC)
  if CC eq 0 and parameters['latlonXY'] eq 0 then begin
    tmp = ['MAP INFO is not set in your header!.', $
      '', $
      'Check your header file!']
    tmp = dialog_message(txt, /ERROR, /CENTER)
  endif
  ; get map-info
  map_info = (soilProduct[ind[0]]).getMeta('map info')
  ; check winsize
  case parameters['avWinSize'] of
    0: WinSize = 1
    1: WinSize = 3
    2: WinSize = 5
    3: WinSize = 7
  endcase
  ; read the ascii file
  data = read_ascii(parameters['refField'], TEMPLATE = template)

  openw, fid, parameters['outputImage'], /GET_LUN
  printf, fid, strjoin(['"POINTS"', '"'+file_basename(parameters['inputFileName'], '.dat')+'"'], ', ')
  for k = 0l, n_elements(data.field1) - 1 do begin
    box = EnSoVal_GetPointFromCoordinate(float(data.field2[k]), $
      float(data.field3[k]), map_info, $
      [(soilProduct[0]).getMeta('samples'), (soilProduct[0]).getMeta('lines')], $
      WinSize, VALID = valid, OPTION = parameters['latlonXY'])
      if valid then begin
        line = ['"' + strtrim(data.field1[k],2) + '"']
        for n = 0, n_elements(parameters['inputFileName']) - 1 do begin
          (soilProduct[n]).initReader, SubsetSampleRange = [box[0], box[1]], $
            SubsetLineRange = [box[2], box[3]], /CUBE
          iData =  (soilProduct[n]).getData()
          (soilProduct[n]).finishReader
          iData = mean(iData, /NAN)
          line = [line, strtrim(iData, 2)]
        endfor
        printf, fid, strjoin(line, ', ')
      endif
  endfor
  free_lun, fid
  for k=0, n_elements(parameters('inputFileName')) - 1 do begin
    (soilProduct[k]).cleanup
  endfor

  result = hash()
  return, result

end

pro test_EnSoVal_processing
  result = EnSoVal_processing(parameters, Title='EnSoVal', GroupLeader=groupLeader)
  print, result
end  
