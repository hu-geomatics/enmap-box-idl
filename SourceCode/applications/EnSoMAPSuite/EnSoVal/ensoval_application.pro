;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `EnSoVal_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `EnSoVal_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `EnSoVal_showReport`).
;
; :Keywords:
;    Title : in, optional, type=string
;      Application title.
;      
;    GroupLeader : in, optional, type=widget id
;      Group leader for widget programs.
;
;    Argument : in, optional, type=widget id
;      Argument given button description inside the enmap.men file.
;-
pro EnSoVal_application, applicationInfo
  ; get global settings for this application
  settings = ensoval_getSettings()
  parameters = ensoval_getParameters(settings)
  if parameters['accept'] then $
    reportInfo = ensoval_processing(parameters, settings)
end

pro test_EnSoVal_application
  applicationInfo = hash()
  EnSoVal_application, applicationInfo
end  
