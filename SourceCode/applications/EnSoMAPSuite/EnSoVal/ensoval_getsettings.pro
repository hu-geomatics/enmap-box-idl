;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('EnSoVal.conf', ROOT=EnSoVal_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, EnSoVal_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function EnSoVal_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('EnSoVal.conf', ROOT=EnSoVal_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_EnSoVal_getSettings

  print, EnSoVal_getSettings()

end
