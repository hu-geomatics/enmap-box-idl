;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

pro ensoval_showHelp1, in1
  mess = ['E N S O V A L:                                                                  ', $
    '--------------------------------------------------------------------------------', $
    '                                                                                ', $
    'Select the different soil product files you want to validate them using a       ', $
    'reference point file.', $
    '', $
    'Use CRTL + mouse click in order to select multiple files.', $
    '', $
    '']
  ok = dialog_message(mess, /INFORMATION, TITLE = 'EnSoVal - Help', /CENTER)
    
end

pro ensoval_showHelp2, in2
  mess = ['E N S O V A L:                                                                  ', $
    '--------------------------------------------------------------------------------', $
    '                                                                                ', $
    'The Field "Reference Point File" corresponds to a, at least, 3 columns text file', $
    'in ASCII format                                                                 ', $
    '                                                                                ', $
    'The system is able to detect the separation symbol but you can preferably use   ', $
    'the symbol: COMMA (,) for separation between each column                        ', $
    '                                                                                ', $
    'The three first columns are:                                                    ', $
    '                                                                                ', $
    ' POINT NAME, X_POS, Y_POS                                                       ', $
    ' "NAME1",    12341, 12341                                                       ', $
    ' "NAME2",    12341, 12341                                                       ', $
    '                                                        ', $
    ' X_POS and Y_POS could be given in:                     ', $
    '    - Data Coordinates (LAT/LON, if georeferencement)   ', $
    '    - Image Coordinates (X/Y, if no georeferencement)   ', $
    '      (image coordinates start at [1,1], like in ENVI   ', $
    ' Do not forget to set the option!                       ', $
    '                                                        ', $
    '                                                                                ', $
    ' ATTENTION, IN THE WIZARD ACII TEMPLATE SELECT SET THE LINE:                    ', $
    '                Data start at line:                                             ', $
    ' TO 2 IF FIRST LINE OF THE FIELD REFERENCE POINT FILE HAS A HEADER              ', $
    '                                                                                ', $
    ' PLEASE USE THE ENGLISH DECIMAL SYMBOL: POINT (.) TO INDICATE DECIMAL VALUES    ']
  ok = dialog_message(mess, /INFORMATION, TITLE = 'EnSoVal - Help', /CENTER)
end

pro ensoval_showHelp3, in3
  return
end
;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;-
function EnSoVal_getParameters, settings
  
  ;;
  ;; D E S I G N  T H E   G U I
  ;;
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program, groupLeader, TITLE = settings['title']
    hubAMW_label, 'EnSoVal: Generate Validation File'
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "INPUT"
    hubAMW_subframe, /ROW
      hubAMW_pickfile, 'inputFileName', TITLE = 'Select soil product file(s)', $
        /MULTIPLE_FILES, /MUST_EXIST, FILTER = "*.dat"
      hubAMW_button, TITLE = 'i', EVENTHANDLER = 'ensoval_showHelp1'
    hubAMW_subframe, /ROW
      hubAMW_pickfile,  'refField',     TITLE = 'Reference Point File       ', $
        /MUST_EXIST, FILTER = "*.csv"
      hubAMW_checklist, 'latlonXY', VALUE = 0, LIST = ['Lat/Lon', 'X/Y'], TITLE = ''
      hubAMW_button, TITLE = 'i', EVENTHANDLER = 'ensoval_showHelp2'
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "OUTPUT"
    hubAMW_subframe, /ROW
      hubAMW_outputFilename, 'outputImage', TITLE = 'Validation text file       '
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "PARAMETERS"
    hubAMW_subframe, /ROW
      hubAMW_checklist, 'avWinSize', VALUE = 1, LIST = ['1x1', '3x3', '5x5', '7x7'], $
        TITLE = 'Average Window Size: '
      ;hubAMW_button, TITLE = 'i', EVENTHANDLER = 'ensoval_showHelp3'
  ;-------------------------------------------------------
  parameters = hubAMW_manage(/Dictionary, BUTTONNAMEACCEPT='Launch')
  ;-------------------------------------------------------
  
  return, parameters
end

pro test_EnSoVal_getParameters
  settings = EnSoVal_getSettings()
  parameters = EnSoVal_getParameters(settings)
  print, parameters
end  
