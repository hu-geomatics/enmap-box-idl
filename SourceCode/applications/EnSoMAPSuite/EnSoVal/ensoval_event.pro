;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `EnSoVal_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro EnSoVal_event, event
  
  ; set up a default error handler
  @huberrorcatch
  
  ; get information about the application
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)

  ;call the main procedure application
  EnSoVal_application, applicationInfo 

end
