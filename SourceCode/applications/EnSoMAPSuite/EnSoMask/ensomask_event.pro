;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application event handler.
;     
; :Params:
;    event: in, required, type=button event structure
;-
pro EnSoMask_event, event
  @huberrorcatch
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  EnSoMask_application, applicationInfo
end
