;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `EnSoMask_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `EnSoMask_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `EnSoMask_showReport`).
; 
; :Params:
;     applicationInfo: in, optional, type=hash
; 
;-
pro EnSoMask_application, applicationInfo
  settings = EnSoMask_getSettings()
  settings = settings + applicationInfo
  parameters = EnSoMask_getParameters(settings)
  if parameters['accept'] then $
    reportInfo = EnSoMask_processing(parameters, settings)
end

pro test_EnSoMask_application
  applicationInfo = hash()
  EnSoMask_application, applicationInfo
end  
