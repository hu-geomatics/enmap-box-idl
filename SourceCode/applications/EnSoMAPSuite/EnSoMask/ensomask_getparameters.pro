;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

pro ensomask_about, resultHash
  ok = dialog_message([ $
    'The module EnSoMask is a collection of tools in order to mask everything except pure bare soil', $
    'It is based on the used of 3 indices: ', $
    '   - NDRBI: water index',$
    '   - NDVI:  vegetation index', $
    '   - NCAI:  dry vegetation index'], /INFORMATION, TITLE="EnSoMask - Short Description", /CENTER)

end

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;-
function EnSoMask_getParameters, settings
  
  ;;; DESIGN THE GUI ;;;
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program, groupLeader, TITLE = settings['title']
  hubAMW_label, 'EnSoMask: Generate Soil Dominant Mask'
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;hubAMW_frame
  ;  hubAMW_subframe, /ROW
  ;    hubAMW_button, TITLE = " About ",    EVENTHANDLER = "ensomask_about"
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; Frame for input option and parameters
  hubAMW_frame
    hubAMW_subframe,  /COLUMN
    hubAMW_inputImageFilename, 'inputImage', TITLE = " Hyperspectral Input Data: "
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; Parameters
  hubAMW_frame
    hubAMW_subframe, /COLUMN
      hubAMW_subframe, /ROW
        hubAMW_button, TITLE = "i"
        hubAMW_checkbox, 'checkNDRBI', TITLE = " Mask water areas using NDRBI ", VALUE = 0
      hubAMW_subframe, /ROW
        hubAMW_button, TITLE = "i"
        hubAMW_checkbox, 'checkNDVI', TITLE = " Mask green vegetated areas using NDVI ", VALUE = 0
      hubAMW_subframe, /ROW
        hubAMW_button, TITLE = "i"
        hubAMW_checkbox, 'checkNCAI', TITLE = " Mask dry vegetated areas using NCAI ", VALUE = 0
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; Output Directory
  hubAMW_frame
    ;hubAMW_outputFilename, 'outputImage', TITLE = 'Output-Image root name'
    hubAMW_outputDirectoryName, 'outputDirName', TITLE = 'Output Directory'
      
    
  parameters = hubAMW_manage(/Dictionary)
  return, parameters
end

pro test_EnSoMask_getParameters
  settings = ensomask_getSettings()
  parameters = EnSoMask_getParameters(settings)
  print, parameters
end  
