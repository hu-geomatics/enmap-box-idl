;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

function EnSoMask_file_suffix, file
  name = file ; temporary variable in order not to modify the input file
  name = strsplit(name, '.', /extract, count = cc)
  if cc lt 1 then return, ''
  return, '.' + name[cc-1]
end


function EnSoMask_processing, parameters, settings
  
  settings = ensomask_getsettings()
  
  ;;;;;;;;;;;;;
  ; Transform parameters into structure
  prms = parameters.toStruct()
  
  ;;;;;;;;;;;;;
  ; Information about directory 
  outDir = prms.outputDirName
  if not file_test(outDir) then begin
    txt = file_basename(outDir) + 'does not exist! Create it?'
    ans = dialog_message(txt, /QUESTION, /CENTER)
    if strcmp(ans, 'NO', /FOLD_CASE) then return, hash()
    file_mkdir, outDir
  endif
  
  ;;;;;;;;;;;;;
  ; get information about output filename according input filename
  outFileName = file_basename(prms.inputImage, EnSoMask_file_suffix(prms.inputImage))
  
    
  nmask = 0
  if prms.checkNDRBI eq 1 then nmask++
  if prms.checkNDVI  eq 1 then nmask++
  if prms.checkNCAI  eq 1 then nmask++
  if nmask eq 0 then begin
    tmp = dialog_message('No soil mask given!', /ERROR, /CENTER, TITLE = 'ENSOMASK: Generate Soil Dominant Mask')
    return, hash()
  endif
  
  ;; open the image file
  inputImage = hubIOImgInputImage(prms.inputImage)
  nlines = inputImage.getMeta('lines')
  tileLines = 100
  
  ;; DO NDRBI is needed
  if prms.checkNDRBI eq 1 then begin
    
    hdrSamples      = inputImage.getMeta('samples')
    hdrLines        = inputImage.getMeta('lines')
    hdrDescription  = file_basename(inputImage.getMeta('filename header')) + ': '
    hdrMapInfo      = inputImage.getMeta('map info')
    
    outputMask  = hubIOImgOutputImage(outDir + outFilename + '_water_mask.dat')
    outputImage = hubIOImgOutputImage(outDir + outFilename + '_water.dat') 
    progressBar = hubProgressBar(TITLE = "Generate NDRBI Mask File", GROUPLEADER = settings.hubGetValue('groupLeader'))
    progressBar.setInfo, 'Calculating the NDRBI mask file'
    progressBar.setRange, [0, inputImage.getMeta('lines')]
    progressDone = 0
    
    ; set band index
    bandIndex = [inputImage.locateWavelength(460), inputImage.locateWavelength(660)]
    
    ; initialize the input image reader
    inputImage.initReader, tileLines, /TILEPROCESSING, /SLICE, SUBSETBANDPOSITIONS = bandIndex
    
    ; initialize the soil product image writer
    outputImage.setMeta, 'samples', hdrSamples
    outputImage.setMeta, 'lines',   hdrLines
    outputImage.setMeta, 'bands',   1
    outputImage.setMeta, 'description', hdrDescription + 'Normalized Difference Blue Red Index'
    outputImage.setMeta, 'header offset', 0
    outputImage.setMeta, 'file type', 'ENVI Standard'
    outputImage.setMeta, 'data type', 4
    outputImage.setMeta, 'interleave', 'BSQ'
    outputImage.setMeta, 'byte order', 0
    outputImage.setMeta, 'map info', hdrMapInfo
    outputImage.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')
    print, inputImage.getWriterSettings()

    ; initialize the soil product mask writer
    outputMask.setMeta, 'samples', hdrSamples
    outputMask.setMeta, 'lines',   hdrLines
    outputMask.setMeta, 'bands',   1
    outputMask.setMeta, 'description', hdrDescription + 'Normalized Difference Blue Red Index [MASK]'
    outputMask.setMeta, 'header offset', 0
    outputMask.setMeta, 'file type', 'ENVI Standard'
    outputMask.setMeta, 'data type', 4
    outputMask.setMeta, 'interleave', 'BSQ'
    outputMask.setMeta, 'byte order', 0
    outputMask.setMeta, 'map info', hdrMapInfo
    outputMask.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')
    
    ; calculate the mask
    while ~inputImage.tileProcessingDone() do begin
      idata = inputImage.getData()
      case size(iData, /TYPE) of
        2:    iData = float(iData) / 10000.
        12:   iData = float(iData) / 100000.
        else: iData = float(iData)
      endcase
     	R_r = reform(idata[1, *])
      R_b = reform(idata[0, *])
      sub = R_r - R_b
      sum = R_r + R_b
      out = sub - sub
      ind = where(sum ne 0, cc)
      if cc ne 0 then out[ind] = sub[ind] / sum[ind]
      ; generate mask
      mask = fix(out - out)
      if cc ne 0 then mask[ind] = 1
      lim1 = 0.0
      lim2 = 1.0001
      ind2 = where(mask eq 1 and out gt lim1 and out lt lim2, c2)
      mask *= 0
      if c2 ne 0 then mask[ind2] = 1
      outputImage.writeData, out
      outputMask.writeData, mask
      progressDone += tilelines
      progressBar.setProgress, progressDone
    endwhile
    ; cleanup object
    outputMask.cleanup
    outputImage.cleanup
    progressBar.cleanup
    inputImage.finishReader
  endif
    
  ;; DO NDVI is needed
  if prms.checkNDVI eq 1 then begin
    
    hdrSamples      = inputImage.getMeta('samples')
    hdrLines        = inputImage.getMeta('lines')
    hdrDescription  = file_basename(inputImage.getMeta('filename header')) + ': '
    hdrMapInfo      = inputImage.getMeta('map info')
    
    outputMask  = hubIOImgOutputImage(outDir + outFilename + '_NDVI_mask.dat')
    outputImage = hubIOImgOutputImage(outDir + outFilename + '_NDVI.dat')
    progressBar = hubProgressBar(TITLE = "Generate NDVI Mask File", GROUPLEADER = settings.hubGetValue('groupLeader'))
    progressBar.setInfo, 'Calculating the NDVI mask file'
    progressBar.setRange, [0, inputImage.getMeta('lines')]
    progressDone = 0

    ; set band index
    bandIndex = [inputImage.locateWavelength(660), inputImage.locateWavelength(800)]

    ; initialize the input image reader
    inputImage.initReader, tileLines, /TILEPROCESSING, /SLICE, SUBSETBANDPOSITIONS = bandIndex

    ; initialize the soil product image writer
    ; initialize the soil product image writer
    outputImage.setMeta, 'samples', hdrSamples
    outputImage.setMeta, 'lines',   hdrLines
    outputImage.setMeta, 'bands',   1
    outputImage.setMeta, 'description', hdrDescription + 'Normalized Difference Vegetation Index'
    outputImage.setMeta, 'header offset', 0
    outputImage.setMeta, 'file type', 'ENVI Standard'
    outputImage.setMeta, 'data type', 4
    outputImage.setMeta, 'interleave', 'BSQ'
    outputImage.setMeta, 'byte order', 0
    outputImage.setMeta, 'map info', hdrMapInfo
    outputImage.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')

    ; initialize the soil product mask writer
    outputMask.setMeta, 'samples', hdrSamples
    outputMask.setMeta, 'lines',   hdrLines
    outputMask.setMeta, 'bands',   1
    outputMask.setMeta, 'description', hdrDescription + 'Normalized Difference Vegetation Index [MASK]'
    outputMask.setMeta, 'header offset', 0
    outputMask.setMeta, 'file type', 'ENVI Standard'
    outputMask.setMeta, 'data type', 4
    outputMask.setMeta, 'interleave', 'BSQ'
    outputMask.setMeta, 'byte order', 0
    outputMask.setMeta, 'map info', hdrMapInfo
    outputMask.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')

    ; calculate the mask
    while ~inputImage.tileProcessingDone() do begin
      idata = inputImage.getData()
      case size(iData, /TYPE) of
        2:    iData = float(iData) / 10000.
        12:   iData = float(iData) / 100000.
        else: iData = float(iData)
      endcase
      R_r = reform(idata[1, *])
      R_b = reform(idata[0, *])
      sub = R_r - R_b
      sum = R_r + R_b
      out = sub - sub
      ind = where(sum ne 0, cc)
      if cc ne 0 then out[ind] = sub[ind] / sum[ind]
      ; generate mask
      mask = fix(out - out)
      if cc ne 0 then mask[ind] = 1
      lim1 = -1.0
      lim2 = 0.3
      ind2 = where(mask eq 1 and out gt lim1 and out lt lim2, c2)
      mask *= 0
      if c2 ne 0 then mask[ind2] = 1
      outputImage.writeData, out
      outputMask.writeData, mask
      progressDone += tilelines
      progressBar.setProgress, progressDone
    endwhile
    ; cleanup object
    outputMask.cleanup
    outputImage.cleanup
    progressBar.cleanup
    inputImage.finishReader
  endif

  ;; DO NCAI is needed
  if prms.checkNCAI eq 1 then begin

    hdrSamples      = inputImage.getMeta('samples')
    hdrLines        = inputImage.getMeta('lines')
    hdrDescription  = file_basename(inputImage.getMeta('filename header')) + ': '
    hdrMapInfo      = inputImage.getMeta('map info')

    outputMask  = hubIOImgOutputImage(outDir + outFilename + '_NCAI_mask.dat')
    outputImage = hubIOImgOutputImage(outDir + outFilename + '_NCAI.dat')
    progressBar = hubProgressBar(TITLE = "Generate NCAI Mask File", GROUPLEADER = settings.hubGetValue('groupLeader'))
    progressBar.setInfo, 'Calculating the NCAI mask file'
    progressBar.setRange, [0, inputImage.getMeta('lines')]
    progressDone = 0

    ; set band index
    bandIndex = [inputImage.locateWavelength(2000), $
      inputImage.locateWavelength(2100), $
      inputImage.locateWavelength(2200)]

    ; initialize the input image reader
    inputImage.initReader, tileLines, /TILEPROCESSING, /SLICE, SUBSETBANDPOSITIONS = bandIndex

    ; initialize the soil product image writer
    ; initialize the soil product image writer
    outputImage.setMeta, 'samples', hdrSamples
    outputImage.setMeta, 'lines',   hdrLines
    outputImage.setMeta, 'bands',   1
    outputImage.setMeta, 'description', hdrDescription + 'Normalized Cellulose Absorption Index'
    outputImage.setMeta, 'header offset', 0
    outputImage.setMeta, 'file type', 'ENVI Standard'
    outputImage.setMeta, 'data type', 4
    outputImage.setMeta, 'interleave', 'BSQ'
    outputImage.setMeta, 'byte order', 0
    outputImage.setMeta, 'map info', hdrMapInfo
    outputImage.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')

    ; initialize the soil product mask writer
    outputMask.setMeta, 'samples', hdrSamples
    outputMask.setMeta, 'lines',   hdrLines
    outputMask.setMeta, 'bands',   1
    outputMask.setMeta, 'description', hdrDescription + 'Normalized Cellulose Absorption Index [MASK]'
    outputMask.setMeta, 'header offset', 0
    outputMask.setMeta, 'file type', 'ENVI Standard'
    outputMask.setMeta, 'data type', 4
    outputMask.setMeta, 'interleave', 'BSQ'
    outputMask.setMeta, 'byte order', 0
    outputMask.setMeta, 'map info', hdrMapInfo
    outputMask.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')

    ; calculate the mask
    while ~inputImage.tileProcessingDone() do begin
      idata = inputImage.getData()
      case size(iData, /TYPE) of
        2:    iData = float(iData) / 10000.
        12:   iData = float(iData) / 100000.
        else: iData = float(iData)
      endcase
      R2000 = reform(idata[0, *])
      R2100 = reform(iData[1, *])
      R2200 = reform(iData[2, *])      
      sub = 0.5 * (R2000 + R2200) - R2100
      sum = 0.5 * (R2000 + R2200) + R2100
      out = sub - sub
      ind = where(sum ne 0, cc)
      if cc ne 0 then out[ind] = sub[ind] / sum[ind]
      ; generate mask
      mask = fix(out - out)
      if cc ne 0 then mask[ind] = 1
      lim1 = -1.0
      lim2 = 0.03
      ind2 = where(mask eq 1 and out gt lim1 and out lt lim2, c2)
      mask *= 0
      if c2 ne 0 then mask[ind2] = 1
      outputImage.writeData, out
      outputMask.writeData, mask
      progressDone += tilelines
      progressBar.setProgress, progressDone
    endwhile
    ; cleanup object
    outputMask.cleanup
    outputImage.cleanup
    progressBar.cleanup
    inputImage.finishReader
  endif
  inputImage.cleanup
  
  ;; Do the soildominant mask file
  maskInputFileName = [$
    outDir + outFilename + '_water_mask.dat',$
    outDir + outFilename + '_NDVI_mask.dat', $
    outDir + outFilename + '_NCAI_mask.dat']
  
  fileTest = file_test(maskInputFileName)
  if fileTest[0] then inputNDRBI = hubIOImgInputImage(maskInputFileName[0])
  if fileTest[1] then inputNDVI  = hubIOImgInputImage(maskInputFileName[1])
  if fileTest[2] then inputNCAI  = hubIOImgInputImage(maskInputFileName[2])
 
  outputMask  = hubIOImgOutputImage(outDir + outFilename + '_soildom_mask.dat')
  progressBar = hubProgressBar(TITLE = "Generate Soil Dominant Mask File", GROUPLEADER = settings.hubGetValue('groupLeader'))
  progressBar.setInfo, 'Calculating the soil dominant mask file'
  progressBar.setRange, [0, nlines]
  progressDone = 0

  ; initialize the input image reader
  if fileTest[0] then inputNDRBI.initReader, tileLines, /TILEPROCESSING, /SLICE
  if fileTest[1] then inputNDVI.initReader,  tileLines, /TILEPROCESSING, /SLICE
  if fileTest[2] then inputNCAI.initReader,  tileLines, /TILEPROCESSING, /SLICE
  
  ; initialize the soil product mask writer
  outputMask.setMeta, 'samples', hdrSamples
  outputMask.setMeta, 'lines',   hdrLines
  outputMask.setMeta, 'bands',   1
  outputMask.setMeta, 'description', hdrDescription + 'Soil Dominant [MASK]'
  outputMask.setMeta, 'header offset', 0
  outputMask.setMeta, 'file type', 'ENVI Standard'
  outputMask.setMeta, 'data type', 4
  outputMask.setMeta, 'interleave', 'BSQ'
  outputMask.setMeta, 'byte order', 0
  outputMask.setMeta, 'map info', hdrMapInfo
  outputMask.initWriter, (fileTest[0] ? inputNDRBI : fileTest[1] ? inputNDVI : inputNCAI).getWriterSettings(SETBANDS = 1, SETDATATYPE='float')

  ; calculate the mask
  while ~(fileTest[0] ? inputNDRBI : fileTest[1] ? inputNDVI : inputNCAI).tileProcessingDone() do begin
    if fileTest[0] then i1 = inputNDRBI.getData() else i1 = 1
    if fileTest[1] then i2 = inputNDVI.getData()  else i2 = 1
    if fileTest[2] then i3 = inputNCAI.getData()  else i3 = 1
    ii = i1 * i2 * i3
    outputMask.writeData, ii
    progressDone += tilelines
    progressBar.setProgress, progressDone
  endwhile
  ; cleanup object
  outputMask.cleanup
  progressBar.cleanup
  if fileTest[0] then inputNDRBI.cleanup
  if fileTest[1] then inputNDVI.cleanup
  if fileTest[2] then inputNCAI.cleanup
  
  result = hash()
  return, result

end

pro test_EnSoMask_processing
  result = EnSoMask_processing(parameters, Title='EnSoMask', GroupLeader=groupLeader)
  print, result
end  
