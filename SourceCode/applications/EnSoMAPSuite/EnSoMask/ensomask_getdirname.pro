;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
function EnSoMask_getDirname
  
  result = filepath('EnSoMask', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnSoMask_getDirname

  print, EnSoMask_getDirname()
  print, EnSoMask_getDirname(/SourceCode)

end
