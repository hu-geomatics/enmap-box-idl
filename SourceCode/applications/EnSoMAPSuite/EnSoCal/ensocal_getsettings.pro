;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('EnSoCal.conf', ROOT=EnSoCal_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, EnSoCal_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function EnSoCal_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('EnSoCal.conf', ROOT=EnSoCal_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_EnSoCal_getSettings

  print, EnSoCal_getSettings()

end
