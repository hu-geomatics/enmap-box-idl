;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
function EnSoCal_GetPointFromCoordinate, xpos, ypos, map_info, dim, WinSize, VALID = valid, OPTION = option
  if keyword_set(option) then begin
    x = xpos - 1
    y = ypos - 1
  endif else begin
    x0    = map_info['pixelX']   ;float(map_info[1]) - 1
    y0    = map_info['pixelY']   ;float(map_info[2]) - 1
    xref  = map_info['easting']  ;float(map_info[3])
    yref  = map_info['northing'] ;float(map_info[4])
    xstep = map_info['sizeX']    ;float(map_info[5])
    ystep = map_info['sizeY']    ;float(map_info[6])
    x = abs(xpos - xref) / xstep + x0
    y = abs(ypos - yref) / ystep + y0
  endelse

  xmin = long64(x - WinSize/2) > 0
  xmax = long64(x + WinSize/2) < (dim[0]-1)
  ymin = long64(y - WinSize/2) > 0
  ymax = long64(y + WinSize/2) < (dim[1]-1)

  get = [xmin, xmax, ymin, ymax]

  ; test get
  test_get = PRODUCT([get[0] GE 0, get[1] LT dim[0], get[0] LE get[1], $
    get[2] GE 0, get[3] LT dim[1], get[2] LE get[3]])

  if not test_get then begin
    valid = 0
    return, -1
  endif
  valid = 1
  return, get
end

function EnSoCal_file_suffix, file
  name = file ; temporary variable in order not to modify the input file
  name = strsplit(name, '.', /extract, count = cc)
  if cc lt 1 then return, ''
  return, '.' + name[cc-1]
end

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`EnSoCal_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
;    parameters : in, required, type=hash
;    
;-
function EnSoCal_processing, parameters, settings
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  hdrSamples      = inputImage.getMeta('samples')
  hdrLines        = inputImage.getMeta('lines')
  hdrDescription  = file_basename(inputImage.getMeta('filename header')) + ': '
  hdrMapInfo      = inputImage.getMeta('map info')
  map_info = inputImage.getMeta('map info')
  nlines = inputImage.getMeta('lines')
  tileLines = 100

  ; set gain/offset
  case parameters['calParamFrame'] of
    0: begin
      gain   = float(parameters['calGain'])
      offset = float(parameters['calOff'])
    end
    1: begin
      template = ascii_template(parameters['refField'], CANCEL = cancel)
      if cancel then return, hash()
      if template.fieldCount lt 4 then begin
        txt = ['The csv file is not correct, at least 4 columns']
        tmp = dialog_message(txt, /ERROR, /CENTER)
        return, hash()
      endif
      data = read_ascii(parameters['refField'], TEMPLATE = template)
      xx = [0.0]
      yy = [0.0]
      for k = 0l, n_elements(data.field1) - 1 do begin
        box = EnSoCal_GetPointFromCoordinate(float(data.field2[k]), $
          float(data.field3[k]), map_info, $
          [inputImage.getMeta('samples'), inputImage.getMeta('lines')], $
          1ll, VALID = valid, OPTION = parameters['latlonXY'])
        if valid then begin
          inputImage.initReader, SubsetSampleRange = [box[0], box[1]], $
          SubsetLineRange = [box[2], box[3]], /CUBE
          point = inputImage.getData()
          inputImage.finishReader
          if finite(point) then begin
            xx = [xx, point]
            yy = [yy, float(data.field4[k])]
          endif
        endif
      endfor
      if n_elements(xx) le 3 then begin
        txt = ['Not enough points to estimage Gain and Offset', $
               '                                             ', $
              '                CANNOT CONTINUE                 ']
        tmp = dialog_message(txt, /CENTER, /INFO)
        return, hash()
      endif
      xx = xx[1:*]
      yy = yy[1:*]
      result = linfit(xx, yy, PROB = prob)
      if prob lt 0.1 then begin
        txt = ['Accuracy of the model parameter is questionnable', $
              '                                                ', $
              '                CANNOT CONTINUE                 ']
        tmp = dialog_message(txt, /CENTER, /INFO)
        return, hash()
      endif
      offset = float(result[0])
      gain   = float(result[1])
    end
  endcase
  
  ;; get the output filename
  path = file_dirname(parameters['inputImage'])
  name = file_basename(parameters['inputImage'], EnSoCal_file_suffix(parameters['inputImage'])) $
    + '_cal.dat'
  outputFileName = path + path_sep() + name
  
  outputImage = hubIOImgOutputImage(outputFilename)
  progressBar = hubProgressBar(TITLE = "Generate Calibration File", GROUPLEADER = settings.hubGetValue('groupLeader'))
  progressBar.setInfo, 'Calculating the calibration file'
  progressBar.setRange, [0, inputImage.getMeta('lines')]
  progressDone = 0
  inputImage.initReader, tileLines, /TILEPROCESSING, /VALUE, $
    SubsetSampleRange = [0, inputImage.getMeta('samples') - 1], $
    SubsetLineRange   = [0, inputImage.getMeta('lines')   - 1]
  
  outputImage.setMeta, 'samples', hdrSamples
  outputImage.setMeta, 'lines',   hdrLines
  outputImage.setMeta, 'bands',   1
  outputImage.setMeta, 'description', hdrDescription + 'Normalized Difference Blue Red Index'
  outputImage.setMeta, 'header offset', 0
  outputImage.setMeta, 'file type', 'ENVI Standard'
  outputImage.setMeta, 'data type', 4
  outputImage.setMeta, 'interleave', 'BSQ'
  outputImage.setMeta, 'byte order', 0
  outputImage.setMeta, 'map info', hdrMapInfo
  outputImage.initWriter, inputImage.getWriterSettings(SETBANDS = 1, SETDATATYPE='float')
  while ~inputImage.tileProcessingDone() do begin
    idata = inputImage.getData()
    idata = idata * gain + offset
    outputImage.writeData, idata
    progressDone += tilelines
    progressBar.setProgress, progressDone
  endwhile
  outputImage.cleanup
  progressBar.cleanup
  inputImage.finishReader

  result = hash()
  return, result

end

pro test_EnSoCal_processing
  result = EnSoCal_processing(parameters, Title='EnSoCal', GroupLeader=groupLeader)
  print, result
end  
