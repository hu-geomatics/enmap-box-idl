;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
pro ensocal_showHelp, input
  txt = [ 'The "Field Measurement Point" file corresponds to a     ', $
          ' 4 columns text file in ASCII format                    ', $
          '                                                        ', $
          'The system is able to detect the separation symbol but  ', $
          'You can preferably use the symbol: COMMA for separation ', $
          '                                                        ', $
          'The four first columns are:                             ', $
          '                                                        ', $
          ' POINT NAME, X_POS, Y_POS, VAL                          ', $
          '"NAME1",     12341, 12341, 0.4334                       ', $
          '"NAME2",     12311, 12311, 0.313                        ', $
          '                                                        ', $
          ' X_POS and Y_POS could be given in:                     ', $
          '    - Data Coordinates (LAT/LON, if georeferencement)   ', $
          '    - Image Coordinates (X/Y, if no georeferencement)   ', $
          '      (image coordinates start at [1,1], like in ENVI   ', $
          ' Do not forget to set the option!                       ', $
          '                                                        ', $
          'ATTENTION, IN THE WIZARD ASCII TEMPLATE SELECT:         ', $
          '          Data start at line:  2                        ', $
          'IF FIRST LINE IS A HEADER LINE OF THE ASCII FILE!!!     ', $
          '                                                        ', $
          'Please use the english decimal system: point            ']
  ok = dialog_message(txt, /INFORMATION, TITLE = 'EnSoCal - Help', /CENTER)
end

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;-
function EnSoCal_getParameters, settings
  
  ;-------------------------------------------------------
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program, groupLeader, TITLE = settings['title']
    hubAMW_label, 'EnSoCAL: Calibration with field data'
    ;hubAMW_button, TITLE = ' About ', /BAR
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "INPUT"
    hubAMW_inputImageFilename, 'inputImage', TITLE = 'Soil Product', SIZE=775
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "OUTPUT"
    hubAMW_outputDirectoryName, 'outputDir', TITLE = 'Output Directory'
  ;-------------------------------------------------------
  hubAMW_frame, TITLE = "PARAMETERS"
    hubAMW_subframe, 'calParamFrame', TITLE = 'Set gain and offset', /ROW, /SETBUTTON
      hubAMW_parameter, 'calGain', TITLE = 'Gain = ', /FLOAT, SIZE=5, IsGE = 0
      hubAMW_parameter, 'calOff',  TITLE = 'Offset = ', /FLOAT, SIZE=5
    hubAMW_subframe, 'calParamFrame', TITLE = 'Set field measurement file', /ROW
      hubAMW_pickfile,  'refField',    TITLE = 'Reference Point File      '
      hubAMW_checklist, 'latlonXY', VALUE = 0, LIST = ['Lat/Lon', 'X/Y'], TITLE = ''
      hubAMW_button, TITLE = 'i', EVENTHANDLER = 'ensocal_showHelp'

  ;-------------------------------------------------------
  ;-------------------------------------------------------

  parameters = hubAMW_manage(/Dictionary, BUTTONNAMEACCEPT='Launch')
  return, parameters
  
end

pro test_EnSoCal_getParameters
  parameters = EnSoCal_getParameters(Title='EnSoCal', GroupLeader=groupLeader)
  print, parameters
end  
