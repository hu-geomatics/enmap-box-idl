;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

function EnSoCal_getDirname
  
  result = filepath('EnSoCal', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnSoCal_getDirname

  print, EnSoCal_getDirname()
  print, EnSoCal_getDirname(/SourceCode)

end
