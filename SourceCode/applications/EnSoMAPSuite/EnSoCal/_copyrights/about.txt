EnSoCal version 1.0

Copyright: (c) 2015-2016 Stephane Guillaso. All rights reserved.

The software was developed by Stephane Guillaso.

This software is released under the LICENSE (e.g. BSD, GPL or GFZ open/close source license)
(see <EnMAP-Box installation folder>\enmapProject\application\EnSoCal\copyrights\license.txt).

Programming: Stephane Guillaso

Concept: Sabine Chabrillat/Stephane Guillaso

Homepage: None

Contact: Sabine Chabrillat (chabri@gfz-potsdam.de)

Disclaimer
The authors of this software tool accept no responsibility for errors or omissions in this work and shall not be liable for any damage caused by these.

Third Parties:

1. hubAPI library (hosted at http://indus.caf.dlr.de/forum/download) was developed by Humboldt-Universit�t zu Berlin, Geography Department, Geomatics Lab (http://www.hu-geomatics.de). 
hubAPI is released under the EnMap Open Source Licence (see <EnMAP-Box installation folder>\enmapProject\lib\hubAPI\copyrights\license.txt).

2. Source code documentation was created by using IDLdoc (http://idldoc.idldev.com) developed by Michael Galloy (http://michaelgalloy.com). IDLdoc is released under a BSD-type license 
(see <EnMAP-Box installation folder>\enmapProject\lib\IDLdoc\copyrights\license.txt).



