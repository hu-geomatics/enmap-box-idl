;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-
;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `EnSoCal_processing` function to this argument. 
;      
;-
pro EnSoCal_showReport, reportInfo, Title=title

  report = hubReport(Title=title)

  report.addHeading, 'you can use headings inside your report'
  report.addHeading, '... or subheadings', 2
  report.addHeading, '... or subsubheading', 3

  report.addHeading, 'you can insert paragraphs with running text'
  report.addParagraph, reportInfo['description']

  report.addHeading, 'you can insert monospaced text blocks'
  report.addMonospace, reportInfo['table']

  report.addHeading, 'you can insert image'
  report.addImage, reportInfo['image'], 'EnSoCal image caption'
  
  report.saveHTML, /Show

end
