;+
; :Author: Dr. Stephane Guillaso  (stephane.guillaso@gmail.com)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `EnSoCal_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `EnSoCal_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `EnSoCal_showReport`).
;
; :Keywords:
;    Title : in, optional, type=string
;      Application title.
;      
;    GroupLeader : in, optional, type=widget id
;      Group leader for widget programs.
;
;    Argument : in, optional, type=widget id
;      Argument given button description inside the enmap.men file.
;-
pro EnSoCal_application, applicationInfo
  settings = EnSoCal_getSettings()
  settings = settings + applicationInfo
  parameters = EnSoCal_getParameters(settings)
  if parameters['accept'] then $
    reportInfo = EnSoCal_processing(parameters, settings)
end

pro test_EnSoCal_application
  EnSoCal_application, Title='EnSoCal', GroupLeader=groupLeader, Argument=argument
end  
