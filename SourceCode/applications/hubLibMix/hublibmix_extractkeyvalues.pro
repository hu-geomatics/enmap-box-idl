;+
; :Description:
;    Returns an array of hash-values related to a set of hash keys.
;    The type of the returned array is equal to the type of the first hash value found.
;    
; :Params:
;    inputHash
;    hashKeys
;
;
;
;-
function hubLibMix_extractKeyValues, inputHash, hashKeys
  values = List()
  foreach key, hashKeys do begin
    if inputHash.hasKey(key) then values.add, inputHash[key]
  endforeach
  if n_elements(values) eq 0 then return, !NULL
  type = size(/TYPE, values[0])
  return, hubMathHelper.convertData(values.toArray(), size(values[0], /TYPE))
end