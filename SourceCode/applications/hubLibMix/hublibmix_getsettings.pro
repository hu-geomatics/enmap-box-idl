;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('hubLibMix.conf', ROOT=hubLibMix_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, hubLibMix_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function hubLibMix_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('hublibmix.conf', ROOT=hubLibMix_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_hubLibMix_getSettings

  print, hubLibMix_getSettings()

end
