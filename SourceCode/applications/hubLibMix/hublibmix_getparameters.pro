function hubLibMix_getParameters_fitStringLength, array, firstIndex, length=length
  i0 = isa(firstIndex) ? firstIndex : 0
  ie = isa(lastIndex) ? lastIndex : n_elements(array)-1
  nCharacters = isa(length) ? length : max(strlen(array[i0:ie]))
  format = '(%"%-'+strtrim(nCharacters,2)+'s")
  return, string(format=format, array)
end


function hubLibMix_getParameters_DialogFreeMixingCheck $
              , parameters, message=message, userInformation=userInformation
  msg = !NULL
 
  message = msg
  return, ~isa(msg)
end

function hubLibMix_getParameters_DialogFreeMixing, settings, parameters1
  classIndices = parameters1['classIndices']
  classNames = parameters1['classNames']
  nClasses = n_elements(classNames)
  
  hubAMW_program, settings.hubGetValue('groupLeader'), Title = settings['title']
 
  hubAMW_frame, Title = 'Spectral classes'
  table = list()
  table.add, 'Label | Name'
  table.add, '------+-------------------'
  for i=0, nClasses-1 do begin  
    table.add,string(format='(%"%2i    | %s")', classIndices[i], classNames[i] )
  endfor
  hubAMW_label, table.toArray()
  
  hubAMW_frame, Title = 'Mixing Ratios'
  
  
  hubAMW_label, ['Set number of mixing steps for each' $
                ,'classes combination']
  
  ;Column Layout
  columnBases = List()
   
  ;labelsLeft = string(format='(%"%i ")',classIndices) + classNames
  labelsLeft = string(format='(%"%i ")',classIndices)
  labelsLeft = hubLibMix_getParameters_fitStringLength(labelsLeft)
  
  labelsTop  = string(format='(%"%i")', classIndices)
  labelsTop  = hubLibMix_getParameters_fitStringLength(labelsTop)
  
  classCombinationKeys = List()
  
  ;1. Define Layout: create columns
  baseFrame = hubAMW_getCurrentBase()
  baseRow = widget_base(hubAMW_getCurrentBase(), /ROW)
  for i=0, nClasses-1 do begin
    columnBases.add, widget_base(baseRow, /COLUMN, FRAME=0) 
  endfor 
  
  ;2. Fill columns
  for iC = 0, n_elements(columnBases)-1 do begin
    hubAMW_setCurrentBase, columnBases[iC]
    
    iB = nClasses - 1 - iC
    classB = classIndices[iB]
    
    ;write header cell
    offset = 2
    if iC eq 0 then offset += max(strlen(labelsLeft))
    classBTopLabel = string(format='(%"%'+strtrim(offset,2)+'s%s")', '', labelsTop[iB])
    
    hubAMW_label, classBTopLabel
    
    for iA = 0, iB do begin
      classA = classIndices[iA]
      classALeftLabel = iC eq 0 ? labelsLeft[iA] : ''
      
      parameterKey = string(format='(%"classCombinationRatios,%i,%i")', classA, classB) 
      hubAMW_parameter, parameterKey, Title=classALeftLabel $
                      , /Integer, value=0, IsGE=0, IsLE=10, Size=1
      classCombinationKeys.add, parameterKey     
    endfor
  endfor
  
  
  userInformation = Hash()
  userInformation['classCombinationKeys'] = classCombinationKeys
 
  hubAMW_frame ,   Title =  'Output'
  hubAMW_checkbox, 'PureClassClassification' , Title='Show Pure Class Pixels', value=1b
  hubAMW_checkbox, 'inputPixelClassification', Title='Show Input Pixels', value=1b                
  hubAMW_list, 'fractionLabelClasses', title='Fraction Label Classes' $
                  , List= string(format='(%"%i ")',classIndices) + classNames $
                  , Extract = classIndices, /MULTIPLESELECTION $
                  , YSIZE=150, XSIZE=300
  
  hubAMW_checkbox      , 'keepUnused', Title='Add unused input spectra', value=parameters1.hubGetValue('keepUnused', default=1)
  hubAMW_outputFilename, 'outputFile'  , TITLE='Library', value = parameters1.hubGetValue('outputFile', default='mixedSpectra'), tsize=tsize
  hubAMW_combobox,       'outputMode'  , title='Format ' $
                 , list = parameters1['outputModeNames'] $
                 , Extract = parameters1['outputModes'] $
                 , value=1, tsize=tsize
  
  
  
   
  parameters = hubAMW_manage( $
        CONSISTENCYCHECKFUNCTION='hubLibMix_getParameters_DialogFreeMixingCheck' $
      , userInformation=userInformation)
      
  if parameters['accept'] then begin
     mix = hubLibMix_mixDescription()
     foreach key, classCombinationKeys do begin
      combination = strsplit(key,',',/Extract)
      classA = fix(combination[1])
      classB = fix(combination[2]) 
      number = parameters[key]
      if number ge 1 then begin   
        classFractionRatios = hubLibMix_getFractions(number)
        mix.addMixtures, classA, classB, classFractionRatios
      endif
     endforeach
     if parameters['keepUnused'] then mix.addPureClass, classIndices
     parameters['mixDescription'] = mix 
     parameters.hubRemove, classCombinationKeys
  endif
  return, parameters
 
end

function hubLibMix_getParameters_DialogIntraClassCheck $
    , parameters, message=message, userInformation=userInformation
  msg = !NULL
  hasKey = 0b
  hasSelectedKey = 0b
  foreach key, userInformation['classKeys'] do begin
    hasKey or= parameters.hubIsa(key)  
  endforeach
  
  if ~hasKey then begin
    msg = [msg, 'Select at least one class for inter class mixing']
  endif 
  
  message = msg
  return, ~isa(msg)
end


function hubLibMix_getParameters_DialogIntraClass, settings, parameters1
  classIndices = parameters1['classIndices']
  classNames = parameters1['classNames']
  nClasses = n_elements(classNames)
  
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
  hubAMW_frame, Title = 'Classes and Fractions'
  hubAMW_label, ['Select classes to mix and specify the mixing fractions' $
                 ,'e.g. 1 = 50%, 2 = 33% 66%, ... , 9 = 10% 20% ... 90%']
  classKeys = list()
  
  for i=0, nClasses-1 do begin
    key = strtrim(classIndices[i],2)
    hubAMW_parameter, key, TITLE=string(format='(%"%2i %s")', classIndices[i], classNames[i]) $
                    , OPTIONAL=2, VALUE=1, /Integer, IsGE=1 $
                    , size=3
                    
    classKeys.add, key
  endfor
  userInformation = Hash()
  userInformation['classKeys'] = classKeys
  
  hubAMW_frame ,   Title =  'Output'
  hubAMW_checkbox      , 'keepUnused', Title='Add unused input spectra', value=parameters1.hubGetValue('keepUnused', default=1)
  hubAMW_outputFilename, 'outputFile'  , TITLE='Library', value = parameters1.hubGetValue('outputFileIntra', default='intraClassMix'), tsize=tsize
  hubAMW_combobox,       'outputMode'  , title='Format ' $
                 , list=parameters1['outputModeNames'] $
                 , Extract=parameters1['outputModes'] $
                 , value=1, tsize=tsize
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubLibMix_getParameters_DialogIntraClassCheck' $
                            ,USERINFORMATION=userInformation)
                            
  if parameters['accept'] then begin
     mix = hubLibMix_mixDescription()
     foreach classKey, classKeys, i do begin
        if parameters.hubIsa(classKey) then begin
          steps = parameters[classKey]
          class = fix(classKey)
          mix.addMixtures, class, class, hubLibMix_getFractions(steps)
        endif
        
     endforeach
     parameters['mixDescription'] = mix
     if parameters['keepUnused'] then mix.addPureClass, classIndices
     parameters['Fraction100Classification'] = '_labels'
     parameters['DisjunctPixelMask'] = 1b
     parameters.hubRemove, ['keepUnused']
     parameters.hubRemove, classKeys.toArray()
     
  endif
  return, parameters
  
end

function hubLibMix_getParameters_DialogInterClassCheck, parameters, message=message
  msg = !NULL
  
  if isa(where(parameters['backgroundClasses'] eq parameters['targetClass'],/NULL)) then begin
    msg = [msg, 'Target class can not be a background class as well']
  endif
  
  message = msg
  return, ~isa(msg)
end
 
function hubLibMix_getParameters_DialogInterClass, settings, parameters1
  classIndices = parameters1['classIndices']
  classNames = parameters1['classNames']
  nClasses = n_elements(classNames)
    
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
    hubAMW_frame, Title = 'Select Spectral Classes'
    hubAMW_label, 'Target Class'
    hubAMW_combobox, 'targetClass',     Title='' $
               , LIST=classNames, Extract=classIndices
               
    hubAMW_list, 'backgroundClasses', Title='Background Classes' $
               , LIST=classNames, Extract=classIndices $
               , ALLOWEMPTYSELECTION=0, /MULTIPLESELECTION $
               , XSIZE=300, YSIZE = 150 
             
    hubAMW_frame, Title = 'Background Mixing Fractions'
    hubAMW_label, ['Specify mixing fractions by number of mixing steps' $
                  ,'e.g. 1 = 50%, 2 = 33% 66%, ... , 9 = 10% 20% ... 90%']
                  
    hubAMW_subframe, 'individualSteps' $
                   , Title='Same mixing for all background classes' $
                   , /SETBUTTON, /COLUMN
                   
    hubAMW_parameter, 'numberOfSteps', Title='Steps' $
                    , SIZE=1 $
                    , VALUE = 1 , /Integer $
                    , ISGE=1, ISLT=20
                  
    hubAMW_subframe, 'individualSteps' $
                   , TITLE='Specify steps individually' $
                   , /COLUMN 
      hubAMW_label, '(Classes not selected as background will be ignored)'
      classKeys = list()
      for i=0, nClasses-1 do begin
        key = strtrim(classIndices[i],2)
        hubAMW_parameter, key, TITLE=string(format='(%"%2i %s")', classIndices[i], classNames[i]) $
                        , OPTIONAL=0, VALUE=1, /Integer, IsGE=1 $
                        , Size=3
      endfor
      
    hubAMW_frame ,   Title =  'Output'
    hubAMW_checkbox      , 'keepUnused', Title='Add unused input spectra', value=parameters1.hubGetValue('keepUnused', default=1)
    hubAMW_outputFilename, 'outputFile'  , TITLE='Library' $
                         , value = parameters1.hubGetValue('outputFileInter', default='interClassMix') $
                         , tsize=tsize
                         
    hubAMW_combobox,       'outputMode'  , title='Format ' $
                   , list=parameters1['outputModeNames'] $
                   , Extract=parameters1['outputModes'] $
                   , value=1, tsize=tsize
   
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION = 'hubLibMix_getParameters_DialogInterClassCheck')
  if parameters['accept'] then begin
     targetClass   = parameters['targetClass']
     backgroundClasses = parameters['backgroundClasses']
     
    mix = hubLibMix_mixDescription()
    
    if parameters['individualSteps'] then begin
        foreach backgroundClass, backgroundClasses do begin
          key = strtrim(backgroundClass,2)
          fractions = hubLibMix_getFractions(parameters[key])
          mix.addMixtures, backgroundClass, targetClass, fractions
        endforeach    
     endif else begin
      fractions =  hubLibMix_getFractions(parameters['numberOfSteps'])
        foreach backgroundClass, backgroundClasses do begin
          mix.addMixtures, backgroundClass, targetClass, fractions
        endforeach 
     endelse
     
     if parameters['keepUnused'] then mix.addPureClass, classIndices
     parameters['mixDescription'] = mix 
     parameters['fractionLabelClasses'] = parameters['targetClass']
     parameters['InputSpectraClassification'] = 1b
     parameters['DisjunctPixelMask'] = 1b
     parameters.hubRemove, ['numberOfIntervals', 'backgroundClasses', 'targetClass']
     
  endif
  return, parameters
end

;+
; :Hidden:
;    Describe the procedure.
;
; :Params:
;    parameters
;
; :Keywords:
;    message
;
;-
function hubLibMix_getParameters_DialogBasicCheck, parameters, message=message
  msg = !NULL
  
  if parameters.hubIsa('inputMask') then begin
    inputImg  = hubIOImgInputImage((parameters['inputSampleSet'])['featureFilename'])
    inputMask = hubIOImgInputImage( parameters['inputMask'] )
    if ~inputImg.isCorrectSpatialSize(inputMask.getSpatialSize()) then begin
      msg = [msg, "Mask file must have same spatial size as SpecLib/Image"]
    endif
    inputImg.cleanup
    inputMask.cleanup
  endif
  
  message = msg
  return, ~isa(msg)
end

function hubLibMix_getParameters_DialogBasic, settings, parameters
  
  if ~isa(parameters) then parameters = Hash() + settings
 
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
    hubAMW_frame ,   Title =  'Input Files'
    
    tsize=100
    hubAMW_inputSampleSet, 'inputSampleSet', /CLASSIFICATION $
                         , TITLE = 'SpecLib/Image', REFERENCETITLE='Labels' $
                         , Value = parameters.hubGetValue('inputSpecLib') $
                         , tsize = tsize
    
    
    hubAMW_inputImageFilename, 'inputMask', TITLE='Mask' $
                             , OPTIONAL = parameters.hubIsa('inputMask') ? 1: 2 $
                             , value = parameters.hubgetValue('inputMask') $
                             , Tsize = tsize                     
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubLibMix_getParameters_DialogBasicCheck')
  
  if parameters['accept'] then begin
      parameters['inputSpecLib'] = (parameters['inputSampleSet'])['featureFilename']
      parameters['inputLabels']  = (parameters['inputSampleSet'])['labelFilename']
      
      parameters.hubRemove, 'inputSampleSet'
  endif
  return, parameters
end



;+
; :Description:
;    Checks whether a new spectral library or image would exceed the 
;    specific amount of disc space / memory defined by `maxSize`.
;    
;    If yes, a dialog will be shown and ask the user for agreement.
;    
; :Returns:
;   Returns `true` in case the file size will be lower than `maxSize` or the 
;   user confirmed the settings. Returns `false` otherwise.
;   
;      
;    
;
; :Params:
;    parameters: in, required, type=hash
;     Input parameter hash that contains at least the following keys::
;     
;       inputSpecLib    | string |
;       labelOccurrence | 
;       mixDescription
;       outputMode
;
; :Keywords:
;    maxSize: in, optional, type = double, default= 1 MB
;     Specify this value to set a maxium number of bytes. If exceeded, a dialog will be shown that asks
;     the user if he really want to create a file of the expected size.
;
;
;
;-
function hubLibMix_getParameters_checkTotalNumber, parameters, maxSize=maxSize


  requiredKeys = ['inputSpecLib','labelOccurrence','mixDescription','outputMode']
  if ~parameters.hubIsa(requiredKeys, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(requiredKeys[missing],', ')
  endif
  
  if ~isa(maxSize) then maxSize = 1.
  
  inputImg = hubIOImgInputImage(parameters['inputSpecLib'])
  nBands = inputImg.getMeta('bands')
  dataType = inputImg.getMeta('data type')
  inputImg.cleanup
  
  typeCodes = hubHelper.getDataTypeInfo(/TypeCodes)
  typeSizes = hubHelper.getDataTypeInfo(/TypeSizes)
  dataTypeSize = typeSizes[where(typeCodes eq dataType, /NULL)]

  
  info = hubLibMix_getFinalNumberOfProfiles( $
                      parameters['labelOccurrence'] $
                    , parameters['mixDescription'] $
                    , parameters['outputMode'] $
                    )
  
  fileSize = parameters['outputMode'] eq 'SL' ? $
            double(info.nProfiles) * nBands * dataTypeSize / (2l^20) : $
            double(info.nSamples) * info.nLines * nBands * dataTypeSize / (2l^20)
  
  answer = 'YES'
  if fileSize gt maxSize then begin          
    if parameters['outputMode'] eq 'SL' then begin
      text = ['Create spectral library with:' $
             ,string(format='(%"%i spectra in total")', info.nProfiles) $
             ,string(format='(%"Total Size %f MB ?")', fileSize ) $
             ]
    endif else begin
      text = ['Create image with:' $
             ,string(format='(%"%i spectra in total")', info.nProfiles) $
             ,string(format='(%"%i samples x %i lines x %i bands")', info.nSamples, info.nLines, nBands) $
             ,string(format='(%"Total Size %f MB ?")', fileSize ) $
             ]
    endelse
    answer = dialog_message(text, Title='Confirm Settings', /Question)
  endif 
  return, strcmp(answer, 'YES', /FOLD_CASE)
end

;+
; :Description:
;    Opens widget dialogs to collect parameters required to run `hubLibMix_processing`.
;
; :Params:
;    settings: in, required, type = hash
;
;
;
;-
function hubLibMix_getParameters, settings
  CANCELED = Hash('accept',0b)
  
  
  parameters = hub_getAppState('hubLibMix', 'state')
  
  parameters = hubLibMix_getParameters_DialogBasic(settings, parameters)  
  if ~parameters['accept'] then return, CANCELED
  
  hub_setAppState, 'hubLibMix', 'state', parameters 
  ;get label occurence
  labelOccurrence = hubLibMix_getClassLabelOccurrence(parameters, GroupLeader = GroupLeader, NoShow = NoShow)
  classIndices = where(labelOccurrence[*].occurrence gt 0 $
                   and labelOccurrence[*].index      gt 0, /NULL)
  parameters['labelOccurrence'] = labelOccurrence 
  parameters['classIndices'] = labelOccurrence[classIndices].index
  parameters['classNames'] = hubLibMix_getParameters_fitStringLength(labelOccurrence[classIndices].name)
   
  parameters['outputModeNames'] = ['Spectral Library','Image']
  parameters['outputModes'] = ['SL','Img']
      
  repeat begin
    checked = 0b
    case settings.hubGetValue('mode') of
      'interClassMixing'  : parameters += hubLibMix_getParameters_DialogInterClass(settings, parameters)
      'intraClassMixing'  : parameters += hubLibMix_getParameters_DialogIntraClass(settings, parameters)
      'freeMixing'        : parameters += hubLibMix_getParameters_DialogFreeMixing(settings, parameters)
      else : message, "unknown 'mode'"
    endcase
    if ~parameters['accept'] then continue
    
    checked = hubLibMix_getParameters_checkTotalNumber(parameters)
  endrep until checked || ~parameters['accept']
  
  
  if ~parameters['accept'] then return, CANCELED
  
  case settings.hubGetValue('mode') of
      'interClassMixing'  : parameters['outputFileInter'] = parameters['outputFile']
      'intraClassMixing'  : parameters['outputFileIntra'] = parameters['outputFile']
      else : ;nothing
  endcase
  parameters.hubRemove, ['outputFileInter','outputFileIntra']
  hub_setAppState, 'hubLibMix', 'state', parameters
  
  return, parameters
end

;+
; :hidden:
;-
pro test_hubLibMix_getParameters

  ; test your routine
  settings = hubLibMix_getSettings()
  
  settings['inputSpecLib'] = filepath("Roof", ROOT_DIR=hubLibMix_getDirname(), SUBDIRECTORY=settings['dirTestData'])
  settings['inputLabels']  = filepath("RoofClassification", ROOT_DIR=hubLibMix_getDirname(), SUBDIRECTORY=settings['dirTestData'])

  settings['inputSpecLib'] = hub_getTestSpeclib('ClassificationSpeclib')
  settings['inputLabels']  = hub_getTestSpeclib('ClassificationLabels')
  settings['outputFile'] = 'D:\TesthubLibMix'
  
  settings['mode'] = 'advanced'
  ;settings['mode'] = 'simple'
  
  parameters = hubLibMix_getParameters(settings)
  print, parameters
  if parameters['accept'] then begin 
    print, parameters['mixingRatios']
  endif
end  