;+
; :Description:
;    This program creates test data 
;
;
;
;
;
;-
pro hubLibMix_createTestData
  pathSpec   = enmapBox_getTestImage('AF_Image')
  pathClass  = enmapBox_getTestImage('AF_LC')
  
  nSamples = 5  ;spectra per class
  ;nClasses = 3 ;set max. number of classes. Default = classes in pathClass image
  threshold = 0.9
  
  prefix = 'Test'
  pathOutputDir = 'D:\Sandbox\'
  pathSL        = filepath(prefix+'SL', ROOT_DIR=pathOutputDir)      
  pathSLLabels  = filepath(prefix+'SLLabels', ROOT_DIR=pathOutputDir)
  pathImg       = filepath(prefix+'Img', ROOT_DIR=pathOutputDir) 
  pathImgLabels = filepath(prefix+'ImgLabels', ROOT_DIR=pathOutputDir)
  
  ;inputData
  inSpec = hubIOImgInputImage(pathSpec)
  div = inSpec.getMeta('data ignore value')
  inClass = hubIOImgInputImage(pathClass, /Classification)
  
  if ~inSpec.iscorrectSpatialSize(inClass.getSpatialSize()) then begin
    message, 'Source images must have same spatial size'
  endif
  
  nBands = inSpec.getMeta('bands')
  
  nClasses = isa(nClasses) ? nClasses : inClass.getMeta('classes')
  classValues = indgen(nClasses-1)+1
  
  ;structure to store the class spectra
  info = {spectra:make_array(nBands, nSamples) $ ;spectra
          , iClass:0b $ ;class values the spectra are related to
          , nSpectra:0 $ ;number of collected spectra
          }
  info = replicate(info, nClasses-1)
  info.iClass = indgen(nClasses-1)+1 
  
  info[1].iClass = 4
  info[3].iClass = 2
  
  tileLines = hubIOImg_getTileLines(IMAGE=inSpec)
  while ~(product(info[*].nSpectra eq nSamples)) do begin
  
    ;init readers
    inSpec.initReader, tileLines, /TileProcessing, /Slice
    inClass.initReader, tileLines, /TileProcessing, /Slice
    
    seed = systime(1)
    
    ;tile run
    while ~inClass.tileProcessingDone() do begin
      classData = inClass.getData()
      specData = inSpec.getData()
      prob = randomu(seed, n_elements(classData))
      foreach c, info[*].iClass, i do begin
        if info[i].nSpectra lt nSamples then begin 
          iValid = where(classData eq c and prob gt threshold, nClassPixels, /NULL)
          if isa(iValid) then begin
            ;add new class spectra
            foreach index, iValid do begin
              nOld = info[i].nSpectra
              info[i].Spectra[*, nOld-1] = specData[*, index]  
              info[i].nSpectra = nOld+1 
              if info[i].nSpectra eq nSamples then break
            endforeach 
          endif
        endif
      endforeach
      
      
    endwhile    
    
    ;finish readers
    inSpec.finishReader
    inClass.finishReader
  endwhile
  
  ;write the randomly collected data
  ;outSL = hubIOSLOutputSpeclib(pathSL)
  ;outSLLabels = hubIOOutputImage(pathSLLabels)
  
  outImg = hubIOImgOutputImage(pathImg)
  outImgLabels = hubIOImgOutputImage(pathImgLabels)
  
  outImg.copyMeta, inSpec, /CopyFileType, /CopyLabelInformation, /CopySpectralInformation
  outImgLabels.copyMeta, inClass, /CopyFileType, /CopyLabelInformation
  
  writerSettings = Hash()
  writerSettings['bands'] = inSpec.getMeta('bands')
  writerSettings['neighborhoodHeight'] = 1
  writerSettings['numberOfTiles'] = n_elements(info[*].iClass)
  writerSettings['lines'] = nClasses - 1 
  writerSettings['data type'] = inSpec.getMeta('data type')
  writerSettings['samples'] = nSamples
  writerSettings['tileProcessing'] = 1b
  writerSettings['dataFormat'] = 'slice'
  outImg.initWriter, writerSettings
  writerSettings['data type'] = inClass.getMeta('data type')
  writerSettings['bands'] = 1
  outImgLabels.initWriter, writerSettings
  
  
  if ~isa(div) then stop
  
  foreach class, info[*].iClass, iClass do begin
    dataSpectra = info[iClass].spectra
    dataLabels  = make_array(nSamples, /Byte, value=class)
    
    if iClass gt 0 then begin
      iMasked = nSamples - indgen(iClass)-1
      dataSpectra[*,iMasked] = div
      dataLabels[iMasked] = 0b
    endif
    ;add data ignore pixels
    
    outImg.writeData, dataSpectra
    outImgLabels.writeData, dataLabels
  endforeach
  
  outImg.cleanup
  outImgLabels.cleanup
  
  inSpec.cleanup
  inClass.cleanup

  ;generate SpecLib + labels from generated outImg + outImgLabels
  parameters = Hash()
  parameters['inputImage'] = pathImg
  parameters['inputLabels'] = pathImgLabels
  parameters['outputSL'] = pathSL
  parameters['outputSLLabels'] = pathSLLabels
  hubApp_dataImport_SLfromImg_processing, parameters
  
  print, 'Done!'
end