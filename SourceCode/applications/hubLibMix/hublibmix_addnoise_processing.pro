;+
; :hidden:
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universit�t zu Berlin, 2012.
;-


function hubLibMix_addNoise_processing_getNoise0, sigma, range, n
  ;TODO check sigma
  m = mean(range)
  rand = randomn(systime(1), n)
  rand *= sigma
  rand += m
  
  rand[rand lt range[0]] = range[0]
  rand[rand gt range[1]] = range[1]
  
  return,  rand
  
end

function hubLibMix_addNoise_processing_getNoise1, sigma, range, n
  ;TODO check sigma
  m = mean(range)
  rand = randomn(!NULL, n)
  rand *= sigma
  rand += m
  
  rand[rand lt range[0]] = m
  rand[rand gt range[1]] = m
  
  return,  rand
  
end

function hubLibMix_addNoise_processing_getNoise2, sigma, range, n
  ;TODO check sigma
  m = mean(range)
  
  noise = make_array(n, /Float)
  outOfRange = lindgen(n)
  repeat begin
  rand = randomn(!NULL, n_elements(outOfRange))
  rand *= sigma
  rand += m
  
  noise[outOfRange] = rand
  
  outOfRange = where(noise lt range[0] or noise gt range[1], /NULL, /L64)
  endrep until ~isa(outOfRange) 
  return,  noise
  
end


pro test_hubLibMix_processing_addNoise, spectra, SNR

  spec = sin(indgen(100000l)/25.)
  SNRs = [50,200]
  
  ;SNR = sig mean / sig stddev
  
  plot, spec, YRANGE=[0.95,1.]
  nBands = n_elements(spec)
  foreach SNR, SNRs, i do begin
    noise0 = hubLibMix_addNoise_processing_getNoise1(.5, [-1,1], nBands)
    noise1 = hubLibMix_addNoise_processing_getNoise1(.5, [-1,1], nBands)
    noise2 = hubLibMix_addNoise_processing_getNoise2(.5, [-1, 1], nBands)
    
    print, stddev(noise0),stddev(noise1), stddev(noise2)
   
    stop
  endforeach
  

  
end

 

pro hubLibMix_addNoise_processing, parameters
    hubApp_checkKeys, parameters, ['inputSL', 'outputSL', 'SNR']
    
    yMin = parameters.hubGetValue('yMin', default = 0.)
    yMax = parameters.hubGetValue('yMax', default = 1.)
    ySpan = yMax - yMin
    
    imgIn = hubIOImgInputImage(parameters['inputSL'])
    
    isSL = stregex(imgIn.getMeta('file type'), 'spectral library', /Boolean, /FOLD_CASE)
    if isSL then begin
      imgOut = hubIOSLOutputSpeclib(parameters['outputSL'])
      tags = ['spectra names']
      foreach tag, tags do begin
        if imgIn.hasMeta(tag) then imgOut.setMeta, tag, imgIn.getMeta(tag)
      endforeach
    endif else begin
      imgOut = hubIOImgOutputImage(parameters['outputSL'])
    endelse
     
    imgOut.setMeta, 'description', 'additive noise'
    
    toCopy = ['map info', 'band names', 'wavelength', 'wavelength units','fwhm']
    foreach tag, toCopy do begin
      if imgIn.hasMeta(tag) then imgOut.setMeta, tag, imgIn.getMeta(tag)
    endforeach 
    
    dataIgnoreValue = imgIn.getMeta('data ignore value')
    
    SNR = parameters['SNR']
    ;noise range and sigma from SNR???
    
    ;Ben Somers et al. 2012 "Automated Extraction of Image-Based Endmember Bundles for improved Spectra Unmixing"
;    noiseStdDev = 1.
;    noiseRange = [-1,1 ]
;    
;    ;more general formulation
;    noiseStdDev = yspan / 2.
;    noiseRange  = yspan
   ; 0.5 * noise / SNR
    
    nSamples = imgIn.getMeta('samples')
    nLines = imgIn.getMeta('lines')
    nBands = imgIn.getMeta('bands')
    
    tileLines = hubIOImg_getTileLines(IMAGE=imgIn)
   
    
    imgIn.initReader, tileLines, TILEPROCESSING=1, SLICE=1
    if ~isSL then imgOut.initWriter, imgIn.getWriterSettings()
    
    while ~imgIn.tileProcessingDone() do begin
      data = imgIn.getData()
      
      nValues = n_elements(data)
      iValid = isa(dataIgnoreValue)? where(data ne dataIgnoreValue, /NULL) : indgen(nValues)
      
      if isa(iValid) then begin
        ;truncated noise???
        
        ;noise = hubLibMix_addNoise_processing_getNoise(1, [0,1], nValues)
        
        ;formulation Ben Somers et al. 2012
;        noise = randomn(systime(1), nValues) ;mean = 0, stdDev = 1
        
;        noise *= 0.5 ;Ben Somers et al. 2012
;        noise = -1. > noise < 1.
;        noise /= SNR
        
        ;scale noise to range yMin yMax
        sigma = 0.5
        noise = hubLibMix_addNoise_processing_getNoise1(sigma, [-1., 1], nValues)
        noise = noise / SNR * sigma
        noise = noise * ySpan + yMin
        
        ;add noise to input spectra
        data[iValid] = data[iValid] + noise
        data[iValid] = yMin > data[iValid] < yMax
      endif
      imgOut.writeData, data
      
    endwhile
    imgIn.cleanup
    imgOut.cleanup
   
end


pro test_hubLibMix_addNoise_processing


end
