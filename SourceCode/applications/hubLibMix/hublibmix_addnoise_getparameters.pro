;+
; :hidden:
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-


;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    parameters
;    settings
;
;
;
; :Examples:
;
;-
function hubLibMix_addNoise_getParameters, parameters, settings

  if ~isa(parameters) then parameters = Hash()
  if ~isa(settings) then settings = Hash('title','Add noise')
  
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
    tsize=100
    hubAMW_frame ,   Title =  'Input'
    hubAMW_inputImageFilename, 'inputSL', TITLE='Image/SpecLib' $
                             , value = parameters.hubgetValue('inputSL') $
                             , tsize = tsize
    hubAMW_parameter, 'SNR', Title='SNR', /Integer, ISGt=0, tsize = tsize $
                           , value = parameters.hubgetValue('SNR', default=200)
                           
                           
    hubAMW_subframe, /Row
       hubAMW_parameter, 'yMin', Title = 'Data Range Min', /Float, value = 0.
       hubAMW_parameter, 'yMax', Title = 'Max', /Float, value = 1.
    
    hubAMW_frame ,   Title =  'Output'   
    hubAMW_outputFilename, 'outputSL', TITLE='Image/SpecLib' $
                             , value = parameters.hubgetValue('outputSL', default='SL_noise') $
                             
                             , tsize = tsize
    
  parameters = hubAMW_manage()
  
  if parameters['accept'] then begin
     range = [parameters['yMin'],parameters['yMax']]
     parameters['yMin'] = min(range)
     parameters['yMax'] = max(range)
     
  endif
  return, parameters

end
