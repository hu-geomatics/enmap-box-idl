;+
; :Description:
;    Return the fractions to mix spectra that are related with a certain fration number
;     
; :Params:
;    steps: in, required, type= integer
;     Number of steps to divide the range [0,1].
;     
;     In case `NumberOfIntervals` is set, it is interpreted as 
;     number of intervals to devide the range [0,1] into. 
;
; :Keywords:
;    NumberOfIntervals: in, optional, type = bool
;
;-
function hubLibMix_getFractions, steps, NumberOfIntervals=NumberOfIntervals
        
        if keyword_set(NumberOfIntervals) then begin
          if steps le 1 then return, !NULL
          stepWidth = 1. / (steps)
          return, (indgen(steps-1)+1) * stepWidth
        endif else begin
          if steps le 0 then return, !NULL
          stepWidth = 1. / (1 + steps)
          return, (indgen(steps)+1) * stepWidth
        endelse 
        
end

;+
; :Hidden:
;-
pro test_hubLibMix_getFractions
  nums = [0,1,2,3,4,5,6,7,8,9,10]
  foreach num, nums do begin
   ;print, num,':', hubLibMix_getFractions(num)
    print, num,':', hubLibMix_getFractions(num)
  endforeach
end