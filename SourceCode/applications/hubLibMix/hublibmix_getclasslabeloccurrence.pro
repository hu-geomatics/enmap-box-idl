;+
; :Description:
;    Returns the number of classification labels from each classification class. 
;
; :Params:
;    parameters: in, required, type = hash
;     The parameters hash returned from `hubLibMix_getParameters` and used as input for 
;     `hubLibMix_processing`. 
;
; :Keywords:
;    GroupLeader
;    NoShow
;
;-
function hubLibMix_getClassLabelOccurrence, parameters $
      , GroupLeader = GroupLeader $
      , NoShow = NoShow
  
  statsPar = Hash()
  statsPar['inputImage'] = parameters['inputLabels']
  statsPar['inputMask']  = parameters.hubGetValue('inputMask')
  statsPar['tileLines']  = parameters.hubGetValue('tileLines')
  
  labelStats = hubApp_imageStatistics_processing(statsPar, GroupLeader = GroupLeader, NoShow = NoShow)
  meta = labelStats['inputImageMetaInfo']
  bandStats = (labelStats['bandStatistics'])
  labelHistograms =  bandStats[0].classHistogram
  nClassOccurrence = labelHistograms['histogram']
  
  nClasses = meta['classes']
  info = replicate({name:'',index:-1,occurrence:0l}, nClasses)
  info[*].name = meta['class names']
  info[*].index = indgen(nClasses)
  info[*].occurrence = nClassOccurrence 
  return, info
end