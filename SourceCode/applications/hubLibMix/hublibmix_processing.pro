
;
;
;pro hubLibMix_processing_writeEmptyLine, outputFiles
;  files = n_elements(outputFiles) eq 1 ? [outputFiles] : outputFiles
;  foreach labelFile, files do begin
;    dataIgnoreValue = labelFile.getMeta('data ignore value')
;    if ~isa(dataIgnoreValue) then message, 'data ignore value undefined'
;    slice = make_array(labelFile.getMeta('bands'), labelFile.getMeta('samples'), value=dataIgnoreValue)
;    labelFile.writeData, slice
;  endforeach
;end


;+
; :Description:
;    Returns an 2D-array with combinations of pixel indices showing the mixture of source and
;    target spectra.
;    
;    Combinations are done according to following schema::
;     
;      s = source spectrum, t = target spectrum
;      n = number of source spectra
;      m = number of target spectra
;      
;      source class ne target class: s0:t0,..., s0:tm-1, s1:t0,..., s1:tm-1,...,..., sn-1:tm-1     
;      source class eq target class: s0:t1,..., s0:tm-1, s1:t2,..., s1:tm-1,...,..., sn-2:tm-1
;            which is equivalent to: s0:s1,..., s0:sn-1, s1:s2,..., s1:tn-1,...,..., sn-2:tn-1
;     
;     s != t     s == t
;     s0:t0      s0:t1
;      ...        ...
;     s0:tm      s0:tm
;     s1:t0      s1:t2
;      ...        ...
;     s1:tm      s1:tm
;      ...        ...
;      ...        ...
;     sn:tm      sn-2:tm-1
; :Params:
;    iSpectraSource: in, required, type = integer[]
;     Source spectra indices.
;      
;    iSpectraTarget: in, required, type = integer[]
;     Target spectra indices.
;     
;    equalClasses: in, required, type = boolean
;     Set this if source and target class are equal. In this case the result 
;     will not contain combinations of identic pixels.
;
;  :Returns:
;     2D-Array int[2][combination]
;     
;     `int[0,*] =` source spectra indices
;     
;     `int[1,*] =` target spectra indices
;-  
function hubLibMix_processing_getCombinations, iSpectraSource, iSpectraTarget, equalClasses 
      
      ;get combinations of pixel indices
      combiList = list()
      
      nSource = n_elements(iSpectraSource)
      nTarget = n_elements(iSpectraTarget)
      
      if equalClasses then begin
        for iS = 0, nSource - 1 do begin
          for iT = iS + 1, nTarget - 1 do begin
            combiList.add, [iSpectraSource[iS], iSpectraTarget[iT]]
          endfor
        endfor
      endif else begin
        for iS = 0, nSource - 1 do begin
          for iT = 0, nTarget - 1 do begin
            combiList.add, [iSpectraSource[iS], iSpectraTarget[iT]]
          endfor
        endfor
      endelse
      
      nSpectra = n_elements(combiList)
      
      if nSpectra gt 0 then begin
        iSource = make_array(nSpectra, /Integer, Value = 0)
        iTarget = make_array(nSpectra, /Integer, Value = 0)
        
        foreach combi, combiList, i do begin
          iSource[i] = combi[0]
          iTarget[i] = combi[1]
        endforeach 
        n = n_elements(iSource)
      endif else begin
        n = 0
        iSource = [-1]
        iTarget = [-1]
      endelse
                
      
   return, {sourceIndices:iSource $
           ,targetIndices:iTarget $
           ,n:n $
           }
end

function hubLibMix_processing_SpecLib, parameters, internals
  helper = hubHelper()
  
  mix = internals.mix
  nSamples = 1
  nLines   = 0
  nBands = internals.nBands
  dataIgnoreValueImage = (internals.inputImgSpec).getMeta('data ignore value')
  
  
  
  ;define output files
  outputSpeclib = hubIOSLOutputSpeclib(parameters['outputFile'])
  foreach tag, ['wavelength','wavelength units'] do begin
    outputSpeclib.setMeta, tag, (internals.inputImgSpec).getMeta(tag)
  endforeach
  outputSpeclib.setMeta, 'data ignore value', dataIgnoreValueImage
  
  
  outputLabelFiles = internals.outputLabelFiles
  labelWriterSettingsCat = (internals.inputImgSpec).getWriterSettings(SetBands=1, SetSamples = nSamples, SetLines = 9999, SetDataType='byte')
  labelWriterSettingsCat['tileProcessing'] = 0b
  labelWriterSettingsCont = (internals.inputImgSpec).getWriterSettings(SetBands=1, SetSamples = nSamples, SetLines = 9999, SetDataType='float')
  labelWriterSettingsCont['tileProcessing'] = 0b
  foreach key, outputLabelFiles.keys() do begin
    labelFile = outputLabelFiles[key]
    if total(strtrim(key,2) eq ['F100','mask','input']) then begin
      labelFile.initWriter, labelWriterSettingsCat
    endif else begin
      labelFile.initWriter, labelWriterSettingsCont
    endelse
  endforeach

  
  ;
  ; Write Spectra
  ;
  
  ;1a. write pure spectra
  nPure = n_elements(internals.inputSpectra[0,*])
  outputSpectraNamesList = list()
  inputSpectraNames = (internals.inputImgSpec).getMeta('spectra names')
  if ~isa(inputSpectraNames) then begin
    inputSpectraNames = strtrim(indgen(nPure) + 1 ,2)
  endif
  
  outputSpeclib.writeData, internals.inputSpectra
  outputSpectraNamesList.add, inputSpectraNames[internals.iSpectraOfInterest], /EXTRACT
  cntSpectraPure  = outputSpecLib.numberOfProfiles()
  
  ;1b. write labels related to pure spectra
  templateOutLabelsCont = make_array(cntSpectraPure, /float, value=internals.dataIgnoreValueFractions)
  templateOutLabelsCat  = make_array(cntSpectraPure, /float, value=.0)
  cntLabelsTotal = n_elements(templateOutLabelsCat)
  foreach labelKey, outputLabelFiles.keys() do begin
    labelFile = outputLabelFiles[labelKey]
    case labelKey of
      'input' : outputLabels = internals.inputClasses
      '100'   : outputLabels = internals.inputClasses
      else    : outputLabels = fix(labelKey) eq internals.inputClasses
    endcase
    labelFile.writeData, outputLabels
  endforeach
  
  ;2. Write Mixed Spectra (non-redundant)
  ;set to collect all used classes
  foreach targetClass, mix.getClasses(/Target) do begin 
    iSpectraT = where(internals.inputClasses eq targetClass, nSpectraT, /NULL)
    if ~isa(iSpectraT) then continue 
    
    foreach sourceClass, mix.getSourceClasses(targetClass) do begin
      iSpectraS = where(internals.inputClasses eq sourceClass, nSpectraS, /NULL)
      if ~isa(iSpectraS) then continue
     
      targetFractions = MIX.getMixtures(sourceClass, targetClass)
      nFractions = n_elements(targetFractions)
      equalClasses = sourceClass eq targetClass
      
      ;get combinations of pixel indices
      info = hubLibMix_processing_getCombinations(iSpectraS, iSpectraT, equalClasses)
      nCombi = info.n
      
      if nCombi eq 0 then continue
      
      
      nMixtures = nFractions * nCombi
      
      ;output data values with mixed spectra
      outputSpectra = make_array(nBands, nMixtures, /FLOAT, VALUE=dataIgnoreValueImage)
      outputSpectraNames = make_array(nMixtures , /String)
      
      ;stores indices of all positions related to a specific fraction
      fractionIndices = list()
      foreach frac, targetFractions, i do begin
        
        iFraction = indgen(nCombi) * nFractions + i
        fractionIndices.add, iFraction 
        
        ;calculate mixed spectra
        outputSpectra[*, iFraction] = (1.-frac) * internals.inputSpectra[*, info.sourceIndices] $ ;source spectra 
                                    +      frac * internals.inputSpectra[*, info.targetIndices]   ;target spectra 
        
        outputSpectraNames[iFraction] = string(format='(%"%i->%i %4.2f (")', sourceClass, targetClass, frac) $
                                         + inputSpectraNames[info.sourceIndices] + 'x' $
                                         + inputSpectraNames[info.targetIndices] + ')'                  
      endforeach
      
      
      
      outputSpeclib.writeData, outputSpectra
      outputSpectraNamesList.add, outputSpectraNames, /EXTRACT
      
      writtenMixedDataLines = n_elements(outputSpectra[0,*]) 
      
      ;write label data
      templateOutLabelsCont = make_array(nMixtures, /Float, value=dataIgnoreValueFractions)
      templateOutLabelsCat  = make_array(nMixtures, /Byte, value=0b)
      
      foreach key, outputLabelFiles.keys() do begin
        labelFile = outputLabelFiles[key]
        outLabels = []
        case key of
          'input': begin
                    outLabels = templateOutLabelsCat
                  end
          '100' : begin
                    outLabels = templateOutLabelsCat
                    if equalClasses then begin
                      foreach iFraction, fractionIndices, i  do begin
                        outLabels[iFraction] = sourceClass 
                      endforeach
                    endif
                  end
          'mask' : begin
                    outLabels = templateOutLabelsCat
                    outLabels[iFraction] = 1
                   end
          ;write a fraction layers
          else : begin
            outLabels = templateOutLabelsCont
            labelClass = fix(key)
            foreach iFraction, fractionIndices, i  do begin
              case 1b  of
                ;equal classes
                labelClass eq sourceClass && $
                labelClass eq targetClass : outLabels[iFraction] = 1.
                labelClass eq sourceClass : outLabels[iFraction] = 1. - targetFractions[i]
                labelClass eq targetClass : outLabels[iFraction] = targetFractions[i]
                else                      : outLabels[iFraction] = 0.
              endcase
            endforeach
          end 
        endcase
        labelFile.writeData, outLabels
      endforeach
      
      usedClasses = hubMathHelper.getUnion(usedClasses, [sourceClass, targetClass])
   
    endforeach
  endforeach
  
  nLines = outputspeclib.numberOfProfiles()
  outputspeclib.setMeta, 'spectra names', outputSpectraNamesList.toArray()
  foreach labelFile, outputLabelFiles do begin
    labelFile.setMeta, 'lines', nLines
    labelFile.finishWriter
    labelFile.checkFileSize
    labelFile.cleanup
  endforeach
  outputSpecLib.cleanup
  
  
end

function hubLibMix_processing_Image, parameters, internals
  helper = hubHelper()
  
  mix = internals.mix
  nSamples = 0
  nLines   = 0
  nBands = internals.nBands
  dataIgnoreValueImage = (internals.inputImgSpec).getMeta('data ignore value')
  
  foreach combi, mix.getClassCombinations() do begin
    fractions = mix.getMixtures(combi[0], combi[1])
    nSamples = max([nSamples, n_elements(fractions)])
    
  endforeach
  nSamples += 2 ;left and right 100% spectra
 
  outputImage = hubIOImgOutputImage(parameters['outputFile'])
  ;define output files
  foreach tag, ['wavelength','wavelength units'] do begin
    outputImage.setMeta, tag, (internals.inputImgSpec).getMeta(tag)
  endforeach
  outputImage.setMeta, 'data ignore value', dataIgnoreValueImage
  outputImage.copyMeta, internals.inputImgSpec, /COPYSPECTRALINFORMATION, /COPYLABELINFORMATION
  
  ;init image writer 
  imgWriterSettings = (internals.inputImgSpec).getWriterSettings(SetSamples=nSamples, SetLines=9999)
  imgWriterSettings['tileProcessing'] = 0b
  
  outputImage.initWriter, imgWriterSettings
  
  outputLabelFiles = internals.outputLabelFiles
  labelWriterSettingsCat = (internals.inputImgSpec).getWriterSettings(SetBands=1, SetSamples = nSamples, SetLines = 9999, SetDataType='byte')
  labelWriterSettingsCat['tileProcessing'] = 0b
  labelWriterSettingsCont = (internals.inputImgSpec).getWriterSettings(SetBands=1, SetSamples = nSamples, SetLines = 9999, SetDataType='float')
  labelWriterSettingsCont['tileProcessing'] = 0b
  foreach key, outputLabelFiles.keys() do begin
    labelFile = outputLabelFiles[key]
    if total(strtrim(key,2) eq ['F100','mask','input']) then begin
      labelFile.initWriter, labelWriterSettingsCat
    endif else begin
      labelFile.initWriter, labelWriterSettingsCont
    endelse
  endforeach

  
  
  ;set to collect all used classes
  usedClasses = [-1] ;pseudo value, because there is no NIL element
  
  
  nLines = 0ll
  foreach targetClass, mix.getClasses(/Target) do begin 
    iSpectraT = where(internals.inputClasses eq targetClass, nSpectraT, /NULL)
    if ~isa(iSpectraT) then continue 
    
    foreach sourceClass, mix.getSourceClasses(targetClass) do begin
      iSpectraS = where(internals.inputClasses eq sourceClass, nSpectraS, /NULL)
      if ~isa(iSpectraS) then continue
     
      targetFractions = MIX.getMixtures(sourceClass, targetClass)
      nFractions = n_elements(targetFractions)
      
      equalClasses = sourceClass eq targetClass
      
      ;get combinations of pixel indices
      info = hubLibMix_processing_getCombinations(iSpectraS, iSpectraT, equalClasses)
      nCombi = info.n
      if nCombi eq 0 then continue
      
      ;indices of pure spectra
      iPureSource = indgen(nCombi) * nSamples
      iPureTarget = iPureSource + nFractions + 1
      
      if (size(iPureSource, /Dimensions))[0] eq 0 then iPureSource = [iPureSource] 
      if (size(iPureTarget, /Dimensions))[0] eq 0 then iPureTarget = [iPureTarget] 
      
      ;output data values with mixed spectra
      outputSpectra = make_array(nBands, nCombi * nSamples, /FLOAT, VALUE=dataIgnoreValueImage)
      outputSpectra[*,iPureSource] = internals.inputSpectra[*, info.sourceIndices]
      outputSpectra[*,iPureTarget] = internals.inputSpectra[*, info.targetIndices]
      
      
      ;stores indices of all positions related to a specific fraction
      fractionIndices = list()
      foreach frac, targetFractions, i do begin
        
        iFraction = indgen(nCombi)* nSamples + i + 1
        fractionIndices.add, iFraction 
        
        ;calculate mixed spectra
        outputSpectra[*, iFraction] = (1.-frac) * internals.inputSpectra[*, info.sourceIndices] $ ;source spectra 
                                    +      frac * internals.inputSpectra[*, info.targetIndices]   ;target spectra 
                                      
      endforeach
      
      
      outputImage.writeData, outputSpectra
      
      writtenMixedDataLines = n_elements(outputSpectra[0,*]) / nSamples 
      
      ;write label data
      templateOutLabelsCont = make_array(nCombi * nSamples, /Float, value = internals.dataIgnoreValueFractions)
      templateOutLabelsCat  = make_array(nCombi * nSamples, /Byte, value = 0b)
      
      foreach key, outputLabelFiles.keys() do begin
        labelFile = outputLabelFiles[key]
        outLabels = []
        case key of
          'input': begin
                    outLabels = templateOutLabelsCat
                    outLabels[iPureSource] = sourceClass
                    outLabels[iPureTarget] = targetClass
                  end
          '100' : begin
                    outLabels = templateOutLabelsCat
                    outLabels[iPureSource] = sourceClass
                    outLabels[iPureTarget] = targetClass
                    if equalClasses then begin
                      foreach iFraction, fractionIndices, i  do begin
                        outLabels[iFraction] = sourceClass 
                      endforeach
                    endif
                  end
          'mask' : begin
                    outLabels = templateOutLabelsCat
                    foreach iFraction, fractionIndices do begin
                      outLabels[iFraction] = 1
                    endforeach
                    
                    ;indices for each pure spectrums' first occurence
                    if ~equalClasses then begin
                      if total(usedClasses eq sourceClass) eq 0 then $
                        outLabels[iPureSource[indgen(nSpectraS) * nSpectraT]] = 1
                      
                      if total(usedClasses eq targetClass) eq 0 then $
                        outLabels[iPureTarget[0:nSpectraT-1]] = 1
                        
                    endif else begin
                      if total(usedClasses eq sourceClass) eq 0 then begin
                        outLabels[iPureSource[0]] = 1
                        outLabels[iPureTarget[0:nSpectraS-2]] = 1
                      endif
                    endelse
                   
                    iTest = where(outLabels ne 0 and outLabels ne 1, /NULL)
                  end
          ;write a fraction layers
          else : begin
            outLabels = templateOutLabelsCont
            labelClass = fix(key)
            
            outLabels[iPureSource] = labelClass eq sourceClass ? 1. : 0.
            outLabels[iPureTarget] = labelClass eq targetClass ? 1. : 0.
            foreach iFraction, fractionIndices, i  do begin
              case 1b  of
                ;equal classes
                labelClass eq sourceClass && $
                labelClass eq targetClass : outLabels[iFraction] = 1.
                labelClass eq sourceClass : outLabels[iFraction] = 1. - targetFractions[i]
                labelClass eq targetClass : outLabels[iFraction] = targetFractions[i]
                else                      : outLabels[iFraction] = 0.
              endcase
            endforeach
          end 
        endcase
        labelFile.writeData, outLabels
      endforeach
      
      nLines += ncombi
      usedClasses = hubMathHelper.getUnion(usedClasses, [sourceClass, targetClass])
   
    endforeach
  endforeach
  
  ;add remaining "pure" spectra that are not part of any mixing
  foreach class, mix.getClasses(/Pure) do begin
    iSpectra = where(internals.inputClasses eq class, nSpectra, /NULL)
    if isa(iSpectra) then begin
       requiredLines = fix(nSpectra) / fix(nSamples) 
       if nSpectra mod nSamples ne 0 then requiredLines++
       outputSpectra = make_array(nBands, requiredLines * nSamples, /FLOAT, VALUE=dataignorevalueimage)
       
       iiSpectra = indgen(nSpectra) ;just a placeholder
       outputSpectra[*, iiSpectra] = internals.inputSpectra[*,iSpectra]
       outputImage.writeData, outputSpectra
       
       ;handle label images
       templateOutLabelsCont = make_array(requiredLines * nSamples, /float, value=dataIgnoreValueFractions)
       templateOutLabelsCat  = make_array(requiredLines * nSamples, /Byte, value=0)
       
       foreach key, outputLabelFiles.keys() do begin
        labelFile = outputLabelFiles[key]
        outLabels = []
        case key of
          'input' :begin 
                    outLabels = templateOutLabelsCat
                    outLabels[iiSpectra] = class
                  end
          '100'  :begin 
                    outLabels = templateOutLabelsCat
                    outLabels[iiSpectra] = class
                  end
          'mask': begin 
                    outLabels = templateOutLabelsCat
                    outLabels[iiSpectra] = 1b
                  end
          ;write a fraction layers
          else  : begin
                    outLabels = templateOutLabelsCont
                    outLabels[iiSpectra] = fix(key) eq class
                  end 
        endcase
        labelFile.writeData, outLabels
       endforeach
       nLines += requiredLines
    endif
  endforeach
  
  
  outputImage.setMeta,'lines',nLines
  outputImage.finishWriter
  outputImage.checkFileSize
  outputImage.cleanup
  
  foreach labelFile, outputLabelFiles do begin
    labelFile.setMeta, 'lines', nLines
    labelFile.finishWriter
    labelFile.checkFileSize
    labelFile.cleanup
  endforeach
  
  
  
end




;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    parameters: in, required, type = hash
;     Hash to provide the following input values/parameters (* = optional)::
;       
;       hash-key                     | type   | description
;       -----------------------------+--------+-------------
;         inputSpecLib               | string | file path of input speclib / image
;         inputLabels                | string | file path of input classification file 
;       * inputMask                  | string | file path of input mask. Useful to exclude redundant spectra 
;         mixDescription             | object | hubLibMix_mixDescription() Object that describes the mixing setup 
;         outputFile                 | string | file path of output speclib / image
;         outputMode                 | string | specifies the outputFile format
;                                    |        | 'Img' -> write an image
;                                    |        | 'SL'  -> write spectral library
;       * fractionLabelClasses       | int[]  | Array containing all classes to generate a
;                                    |        | class fraction image for. 
;                                    |        | file name will be <outputFile>_fracClass<class label>
;       * InputSpectraClassification | bool   | writes a classification showing the original input spectra
;                                    |        | file name will be <outputFile>_Input<class label> 
;       * Fraction100Classification  | bool   | writes a classification showing the original input spectra and all intra-class mixed pixels
;                                    |        | file name will be <outputFile>_F100
;       * DisjunctPixelMask          | bool   | writes a mask that shows the first occurrence of each spectrum
;                                    |        | file name will be <outputFile>_Mask
;       -----------------------------+--------+-------------
;       
;
; :Keywords:
;    GroupLeader: in, optional
;    NoShow: in, optional
;
;-
pro hubLibMix_processing, parameters, GroupLeader = GroupLeader, NoShow = NoShow
  ;check required parameters 
  required = ['inputSpecLib', 'inputLabels','mixDescription', 'outputFile','outputMode']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  
  outputMode = parameters['outputMode']
  if total(outputMode eq ['SL','Img']) ne 1 then begin
    message, "'outputMode' must be set to 'SL' or 'Img'"
  endif
  
  rootDir = file_dirname(parameters['outputFile'])
  inputImgSpec = hubIOImgInputImage(parameters['inputSpecLib'])
  inputImgClass = hubIOImgInputImage(parameters['inputLabels'], /Classification)
  
  if parameters.hubIsa('inputMask') then begin
    inputImgMask = hubIOImgInputImage(parameters['inputMask'], /Mask)
  endif else begin
    inputImgMask = !NULL
  endelse
    
  nSamples = inputImgSpec.getMeta('samples')
  nLines   = inputImgSpec.getMeta('lines')
  nBands   = inputImgSpec.getMeta('bands') 
  classNames = inputImgClass.getMeta('class names')

  tileLines = hubIOImg_getTileLines(nSamples, nBands + 4, 'DOUBLE')
  dataIgnoreValueInputSpecLib = inputImgSpec.getMeta('data ignore value')
  ;dataIgnoreValueImage = inputImgSpec.hasMeta('data ignore value') ? inputImgSpec.getMeta('data ignore value') : -1.
  dataIgnoreValueFractions = -1.
  
  MIX = parameters['mixDescription']
  
  ;check spatial size
  if ~inputImgSpec.isCorrectSpatialSize(inputImgClass.getSpatialSize()) then begin
    message, 'spectral library/image and class label file must have same spatial dimensions'
  endif

  
  ;get input label file statistics
  labelStats = hubLibMix_getClassLabelOccurrence(parameters, GroupLeader = GroupLeader, NoShow = NoShow)
  
  ;check class combination settings
  ;all classes used from input classification / that have to be in output image/SL
  classesOfInterest = MIX.getClasses()
  
  
  ;check 2: is there at least 1 pixel from each mixClass?
  iNotInClassFile = where(labelStats[classesOfInterest].occurrence eq 0, /NULL)
  if isa(iNotInClassFile) then begin
    names = classNames[classesOfInterest[iNotInClassFile]]
    message, string(format='(%"Label file %s does not contain any pixel of class type: %s ")' $
            , FILE_BASENAME(parameters['inputLabels']) $
            , strjoin(names,', '))
  endif  
  
  nFinal = hubLibMix_getFinalNumberOfProfiles(labelStats, mix, outputMode)
  ;order combination classes by decreasing number of
  ;combinations with other classes
  
  NoOrdering=1b
  if ~keyword_set(NoOrdering) then begin
    classCombiArray = classCombinations.toArray(Type=2)
    classCombiHistogram = make_array(n_elements(classesWithMixing), value=0, /Integer)
    foreach class, classesWithMixing, i do begin
      classCombiHistogram[i] = total(classCombiArray eq c, /Integer)
    endforeach
    classesWithMixing = reverse(classesWithMixing[sort(temporary(classCombiHistogram))])
  endif
  
  ;Define output label files
  outputLabelFiles = Hash()
  ;outputMapInfo = (hubIOImgMeta_mapInfo()).createEmptyMapInfoStruct()
  
  fractionLabelClasses = parameters.hubGetValue('fractionLabelClasses')
  foreach labelClass, fractionLabelClasses do begin
    labelClassString = strtrim(labelClass,2)
    filename = filepath(FILE_BASENAME(parameters['outputFile'])+'_fracClass'+labelClassString, ROOT_DIR=rootDir)
    outputLabelFile = hubIOImgOutputImage(filename)
    outputLabelFile.setMeta, 'file type', 'ENVI Standard'
    ;outputLabelFile.setMeta, 'map info', outputMapInfo
    outputLabelFile.setMeta, 'data ignore value', dataIgnoreValueFractions
    outputLabelFile.setMeta, 'data type', 'float'
    outputLabelFile.setMeta, 'band names', string(format='(%"Fractions Class %i: \"%s\"")', labelClass,classNames[labelClass] )
    outputLabelFiles[labelClassString] = outputLabelFile 
  endforeach
  
  ;Add class image showing all pure pixels
  if parameters.hubKeywordSet('InputSpectraClassification') then begin
    
    suffix = typename(parameters['InputSpectraClassification']) eq 'STRING' ? $
                      parameters['InputSpectraClassification'] : '_Input'
                  
    filename = filepath(FILE_BASENAME(parameters['outputFile']) + suffix, ROOT_DIR=rootDir)
    outputClassFile = hubIOImgOutputImage(filename)
    outputClassFile.copyMeta, inputimgclass, /COPYLABELINFORMATION, /COPYFILETYPE
    ;outputClassFile.setMeta, 'map info', outputMapInfo
    outputClassFile.setMeta, 'band names', 'Pure/Non-mixed spectra class value'
    outputClassFile.setMeta, 'description', string(format='(%"This shows the class values of all pure/non-mixed spectra in %s. Impure/mixed pixels are set to 0 / undefined")', parameters['outputFile'])  
    outputLabelFiles['input'] = outputClassFile 
  endif
  
  ;Add class image showing all pure/original and intra class mixed pixels
  if parameters.hubKeywordSet('Fraction100Classification') then begin
  
    suffix = typename(parameters['Fraction100Classification']) eq 'STRING' ? $
                      parameters['Fraction100Classification'] : '_F100'
    
    filename = filepath(FILE_BASENAME(parameters['outputFile']) + suffix, ROOT_DIR=rootDir)
    outputClassFile = hubIOImgOutputImage(filename)
    outputClassFile.copyMeta, inputimgclass, /COPYLABELINFORMATION, /COPYFILETYPE
    ;outputClassFile.setMeta, 'map info', outputMapInfo
    outputClassFile.setMeta, 'band names', 'Spectra with 100% Fractions'
    outputClassFile.setMeta, 'description', string(format='(%"This shows the class values of all pure or intra-class mixes spectra in %s. Impure/mixed pixels are set to 0 / undefined")', parameters['outputFile'])  
    outputLabelFiles['100'] = outputClassFile 
  endif
  
  if parameters['outputMode'] eq 'Img' then begin
    ;Add mask for disjunct pixels
    if parameters.hubKeywordSet('DisjunctPixelMask') then begin

      suffix = typename(parameters['DisjunctPixelMask']) eq 'STRING' ? $
                        parameters['DisjunctPixelMask'] : '_Mask'
                        
      filename = filepath(FILE_BASENAME(parameters['outputFile']) + suffix, ROOT_DIR=rootDir)
      outputMaskFile = hubIOImgOutputImage(filename)
      outputMaskFile.setMeta, 'file type', 'ENVI Standard'
      ;outputMaskFile.setMeta, 'map info', outputMapInfo
      outputMaskFile.setMeta, 'band names', 'Mask layer for disjunct pixel input'
      outputMaskFile.setMeta, 'description', string(format='(%"This file is related to the spectra in %s")', parameters['outputFile'])   
      outputLabelFiles['mask'] = outputMaskFile
    endif 
  endif
  
  
  ;init Data Readers: Spectra + Class Labels
  inputImgSpec.initReader , nLines, /TileProcessing, /SLICE
  inputImgClass.initReader, nLines, /TileProcessing, /SLICE, DATATYPE='byte'
  if isa(inputImgMask) then inputImgMask.initReader, nLines, /TileProcessing, /Slice, /Mask 
  
  
  ;extract spectra and classes labels of interest (spectraOI and classesOI) only
  inputSpectra = inputImgSpec.getData()
  inputClasses = inputImgClass.getData()
  if isa(inputImgMask) then inputMask = inputImgMask.getData()
  
  
  iSpectraOI = make_array(n_elements(inputClasses), /BYTE, value=0)
  
  foreach class, mix.getClasses() do begin
    if isa(inputMask) then begin
      iSpectraOI or= inputClasses eq class and inputMask ne 0
    endif else begin
      iSpectraOI or= inputClasses eq class
    endelse
  endforeach
  if isa(inputImgMask) then inputImgMask.cleanup
  
  ;exclude spectra with data ignore value in Spectral Input image
  if isa(dataIgnoreValueInputSpecLib) then begin
    iSpectraOI and= nBands gt 1 ?  byte(total(inputSpectra ne dataIgnoreValueInputSpecLib, 1) gt 1) : $
                                   byte(inputSpectra ne dataIgnoreValueInputSpecLib)
  endif
  iSpectraOI = where(iSpectraOI eq 1b, /NULL) ;get indices for spectra of interest
  
  ;extract spectra of interest, use them as inputSpectra
  inputSpectra = size(inputSpectra, /N_DIMENSIONS) eq 1 ? inputSpectra[iSpectraOI] : inputSpectra[*,iSpectraOI]
  inputClasses = inputClasses[iSpectraOI]
  
  internals = {inputClasses:inputClasses $
              ,inputSpectra:inputSpectra $
              ,inputImgSpec:inputImgSpec $
              ,iSpectraOfInterest:iSpectraOI $
              ,classNames:classNames $
              ,nBands:nBands $
              ,mix:mix $
              ,outputLabelFiles:outputLabelFiles $
              ,dataIgnoreValueFractions:dataIgnoreValueFractions $
              }
  
  case parameters['outputMode'] of
    'SL' : report = hubLibMix_processing_SpecLib(parameters, internals)
    'Img': report = hubLibMix_processing_Image(parameters,  internals)  
  endcase
  
  inputImgSpec.cleanup
  inputImgclass.cleanup
  if isa(inputImgMask) then inputImgMask.cleanup
  
end





;+
; :Hidden:
;-
pro test_hubLibMix_processing
  settings = hubLibMix_getSettings()
  
  parameters = hash() ;replace by own hash definition  
  rootDir = hubLibMix_getDirName()
  ;rootDir = 
  parameters['inputSpecLib'] = filepath('TestImg', root_dir=rootDir, SUBDIRECTORY=['resource','testData'])
  parameters['inputLabels']  = filepath('TestImgLabels', root_dir=rootDir, SUBDIRECTORY=['resource','testData'])
  
  ;parameters['inputSpecLib'] = filepath('miniSL', root_dir=rootDir, SUBDIRECTORY=['resource','testData'])
  ;parameters['inputLabels']  = filepath('miniSLLabels', root_dir=rootDir, SUBDIRECTORY=['resource','testData'])
  
  ;parameters['inputSpecLib'] = enmapBox_getTestSpeclib('AF_Speclib')
  ;parameters['inputLabels']  = enmapBox_getTestSpeclib('AF_Speclib_LC')
  
  ;parameters['inputSpecLib'] = enmapBox_getTestImage('AF_Image')
  ;parameters['inputLabels']  = enmapBox_getTestImage('AF_LC')
  
  
  parameters['outputFile'] = filePath('mixed', ROOT_DIR='/Users/benjaminjakimow/Documents/temp')
  parameters['outputMode'] = 'SL'
  ;parameters['outputMode'] = 'Img'
  parameters['InputSpectraClassification'] = 1b
  parameters['Fraction100Classification'] = 1b
  parameters['DisjunctPixelMask'] = 1b 
  
  mix = hubLibMix_mixDescription()
  mix.addMixtures, 2,2, [.2,.4,.6]
  ;mix.addMixtures, 2,4, [.2,.4,.6,.8]
  parameters['mixDescription'] = mix
  
  result = hubLibMix_processing(parameters)
  enmapBox_openImage, parameters['inputSpecLib']
  enmapBox_openImage, parameters['inputLabels']
  
  ;print, result

end  

pro test_hubLibMix_processing2
enmapbox
  fraction = [0.25, 0.75]
  mix = hubLibMix_mixDescription()
  mix.addMixtures, 1,2, fraction
  mix.addMixtures, 1,3, fraction
  mix.addMixtures, 2,3, fraction
  
  parameters = hash()
  parameters['inputSpecLib'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['inputLabels']  = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  parameters['mixDescription'] = mix
  parameters['outputFile'] = hub_getUserTemp('result')
  parameters['outputFile'] = 'g:\libmix\result'
  parameters['fractionLabelClasses'] = [1,2,3,4,5]
  parameters['outputMode'] = 'Img'
  hubLibMix_processing, parameters
  
end