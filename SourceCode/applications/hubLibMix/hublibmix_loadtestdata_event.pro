pro hubLibMix_loadTestData_event, event
  @huberrorcatch
  
  testDataDir = filepath('', root_dir=hubLibMix_getDirname(), SUBDIRECTORY=['resource','testData'])
  allHdrFiles = file_search(testdataDir,'*.hdr', /FOLD_CASE) 
  allHdrFiles = FILE_BASENAME(allHdrFiles, '.hdr',/FOLD_CASE)
  
  files = list()
  foreach f, allHdrFiles, i do begin
    mainFile1 = file_search(testDataDir, f)
    mainFile2 = file_search(testDataDir, f+'.[!h]?*') 
    if isa(mainFile1) && mainFile1 ne '' then files.add, mainFile1, /EXTRACT
    if isa(mainFile2) && mainFile2 ne '' then files.add, mainFile2, /EXTRACT

  endforeach 
  
  h = hubProEnvHelper()
  foreach f, files do begin
;    h.openFile, f[0]
    enmapBox_openImage, f[0]
  endforeach

end