;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `hubLibMix_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `hubLibMix_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `hubLibMix_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubLibMix_application, applicationInfo
  
  ; get global settings for this application
  settings = hubLibMix_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  settings['mode'] = applicationInfo.hubGetValue('argument') 
  
  parameters = Hash()
  ;possible modes:
  ;  intraClassMixing
  ;  interClassMixing
  ;  freeMixing
  ;  addNoise
  
  case settings['mode'] of
      'addNoise' : parameters = hubLibMix_addNoise_getParameters(parameters, settings)
      else : parameters = hubLibMix_getParameters(settings)
    endcase
  
  
  if parameters['accept'] then begin
    
    case settings['mode'] of
      'addNoise' : hubLibMix_addNoise_processing, parameters
      else : hubLibMix_processing, parameters
    endcase
   
 endif
  
end

;+
; :Hidden:
;-
pro test_hubLibMix_application
  applicationInfo = Hash()
  applicationInfo['inputSpecLib'] = enmapBox_getTestSpeclib('AF_Speclib')
  applicationInfo['inputLabels']  = enmapBox_getTestSpeclib('AF_Speclib_LC')
  applicationInfo['outputFile'] = 'D:\mixed'
  hubLibMix_application, applicationInfo

end  
