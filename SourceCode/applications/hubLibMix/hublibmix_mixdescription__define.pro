;+
; This object can be used to store mixing ratios between different label classes.
; 
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    Constructor of hubLibMix_mixDescription object.
;-
function hubLibMix_mixDescription::init  
  self.mixtures = Hash()
  self.classes = Hash()
  return,1b

end

;+
; :hidden:
;-
function hubLibMix_mixDescription::_overloadPrint

  result = list()
  
  foreach key, self.mixtures.keys() do begin
    result.add, key+':'+strjoin((self.mixtures)[key],' ')
  endforeach 
  
  if n_elements(result) gt 0 then begin
    return, transpose(result.toArray())
  endif 
  return, !NULL

end


;+
; :Description:
;    Adds mixing fraction(s) between source and target class.
;    Each fraction `f` is interpreted as fraction of target spectrum::
;     
;     S = source spectrum
;     T = target spectrum
;     
;     mixedSpectra = S * (1-f) + T * f 
;     
; :Params:
;    sourceClass: in, required, type = int
;    targetClass: in, required, type = int
;    fractions: in, required, type = float
;
;-
pro hubLibMix_mixDescription::addMixtures, sourceClass, targetClass, fractions
  self._checkClassIndex, sourceClass
  self._checkClassIndex, targetClass
  if isa(where(fractions le 0 or fractions ge 1, /NULL)) then begin
    message, 'fractions must be in range: 0.0 < fraction < 1.0'
  endif
  
  valueSet = fractions
  key = self._getKey(sourceClass, targetClass)
  
  if (self.mixtures).HasKey(key) then begin
    valueSet = [valueSet, (self.mixtures)[key]]
  endif
  valueSet = valueSet[uniq(valueSet, sort(valueSet))]
  (self.mixtures)[key] = valueSet
  (self.classes)[sourceClass] = 0
  (self.classes)[targetClass] = 0
end


;+
; :Description:
;    Adds a class index of a pure class. If not described by any fractions,
;    the spectra from this class will be used in it's pure/original values only.
;    
;
; :Params:
;    class: in, required, type = int
;       The class index.
;
;
;-
pro hubLibMix_mixDescription::addPureClass, class
  foreach c, class do begin
    self._checkClassIndex, c
    (self.classes)[c] = 0
  endforeach
end

pro hubLibMix_mixDescription::_checkClassIndex, class
  if n_elements(class) gt 1 then message, 'class index must be a scalar'
  if class le 0 then message, 'class index value must be >= 1' 
end

;+
; :Description:
;    Returns the distinct fraction(s) of a target class combined with a certain source class.
;    The fractions are ordered by increasing values.
;       
;    Please note that each target class can be defined as source class too.
;    
; :Params:
;    sourceClass: in, required, type=int
;     
;    targetClass: in, required, type=int
;     
;
;
;-
function hubLibMix_mixDescription::getMixtures, sourceClass, targetClass
  self._checkClassIndex, sourceClass
  self._checkClassIndex, targetClass
  
  
  key = self._getKey(sourceClass, targetClass)
  if ~self.mixtures.hasKey(key) then begin
    return, !NULL
  endif else begin
    return, (self.mixtures)[key] 
  endelse
end

;+
; :Description:
;    Returns the combinations of source and target classed described with one or more fractions.
;
; :Keywords:
;    sourceOfInterest: in, optional, type = boolean
;     Use this to limit the result on a specific source class.
;     
;    targetOfInterest: in, optional, type = boolean
;     Use this to limit the result on a target class.
;
;-
function hubLibMix_mixDescription::getClassCombinations $
      , sourceOfInterest=sourceOfInterest $
      , targetOfInterest=targetOfInterest
      
  combinations = (self.mixtures).Keys()
  
  combi = make_array(2,n_elements(combinations), /Byte)
  foreach key, combinations,i do begin
    combi[*,i] = fix(strsplit(key,',', /Extract)) 
  endforeach
  
  if isa(sourceOfInterest) then begin
    isSource = where(combi[0,*] eq sourceOfInterest, /NULL)
    if ~isa(isSource) then return, !NULL
    combi = combi[*,isSource]
  endif

  if isa(targetOfInterest) then begin
    isTarget = where(combi[1,*] eq targetOfInterest, /NULL)
    if ~isa(isTarget) then return, !NULL
    combi = combi[*,isTarget]
  endif
  
  result = List()
  for i=0, n_elements(combi[0,*])-1 do result.add, combi[*,i]
  return, result
end

;+
; :Description:
;    Creates the hash key for a specific source-target class combination.
; :Params:
;    sourceClass: in, required, type = integer
;     Class index of source class.
;    targetClass: in, required, type = integer
;     Class index of target class.
;-
function hubLibMix_mixDescription::_getKey, sourceClass, targetClass
  return, string(format='(%"%i,%i")', sourceClass, targetClass)
end


;+
; :Description:
;    Returns all source classes related to a target class. 
;    If class does not exist in this mixtures, this function returns !NULL.
; :Params:
;    targetClass: in, required, type=int
;     The class index you are searching the related source classes for.
;-
function hubLibMix_mixDescription::getSourceClasses, targetClass
  
  pairs = ((self.mixtures).keys()).toArray()
  
  sourceClasses = list()
  foreach pair, pairs do begin
    pairValues = fix(strsplit(pair,',', /Extract)) 
    if pairValues[1] eq targetClass then sourceClasses.add, pairValues[0]
  endforeach
  
  if n_elements(sourceClasses) eq 0 then return, 0
  
  sourceClasses = sourceClasses.toArray()
  sourceClasses = sourceClasses[sort(sourceClasses)]
  return, sourceClasses
end

;+
; :Description:
;    Returns class indices used in this mixing description object
;
; :Keywords:
;    Pure: in, optional, type = boolean
;     Set this to return class indices to be used as pure spectra only.
;     
;    Mix: in, optional, type = boolean
;     Set this to return the class indices to be used as mixed spectra only.
;
;    Target: in, optional, type = boolean
;     Set this to return class indices of target classes only.
;-
function hubLibMix_mixDescription::getClasses, Pure=Pure, Mix=Mix, Target=Target
  if total([ keyword_set(pure) $
           , keyword_set(mix) $
           , keyword_set(target) $
          ;, keyword_set(source) $
           ]) gt 1 then message, "Keywords 'Pure','Mix' or 'Target are exclusive"
   
  cSource = Hash()
  cTarget = Hash()
  foreach key, (self.mixtures).Keys(), i do begin
    combi = fix(strsplit(key,',', /Extract)) 
    cSource[combi[0]]=0
    cTarget[combi[1]]=0
  endforeach
  
  cSource = (cSource.Keys()).toArray()
  cTarget = (cTarget.Keys()).toArray()
  cAll    = (self.classes.Keys()).toArray()
  
  
  if keyword_set(target) then return, hubMathHelper.getSet(cTarget)
  if keyword_set(pure)   then return, hubMathHelper.getDifference(cAll, [cSource,cTarget])
  if keyword_set(mix)    then return, hubMathHelper.getUnion(cSource, cTarget) 
  return, hubMathHelper.getSet(cAll)
end


;+
; :hidden:
;-
pro hubLibMix_mixDescription__define
  struct = {hubLibMix_mixDescription $
    ,inherits IDL_Object $
    ,mixtures:hash() $
    ,classes:hash() $
  }
end



;+
; :hidden:
;-
pro test_hubLibMix_mixDescription
  
  mix = hubLibMix_mixDescription()

  mix.addMixtures, 1,1, [0.3]
  
  mix.addMixtures, 1,3, [0.3]
  mix.addMixtures, 1,2, [0.1,0.2]
  mix.addMixtures, 1,2, [0.2,0.1]
  mix.addMixtures, 2,2, [0.2,0.1]
  mix.addPureClass, 5
  print, 'All', mix.getClasses()
  print, 'Mix', mix.getClasses(/Mix)
  print, 'Pure', mix.getClasses(/Pure)
  print, 'Target', mix.getClasses(/Target)
  print, mix.getMixtures(1,1)
  print, mix.getClassCombinations(TARGETOFINTEREST=3)
  
end