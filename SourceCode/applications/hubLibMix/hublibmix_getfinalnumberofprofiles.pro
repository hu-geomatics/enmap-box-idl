function hubLibMix_getFinalNumberOfProfiles, stats, mixDescription, outputMode
  
  nFinal = 0ull
  nSamples = 0
  nLines = 0ull
  nCombinations = 0ull
  
  classIndices = mixDescription.getClasses()
  classCombinations = mixDescription.getClassCombinations()
  case outputMode of
    'SL': begin
          ;calculate all combinations (each combination is a line)
          foreach combi, mixDescription.getClassCombinations() do begin
            nCombi = 0ll
            classA = combi[0]
            classB = combi[1]
            if classA eq classB then begin
             n = stats[classA].occurrence
             for i=1, n do begin
                nCombi += n - i
             endfor
            endif else begin
              nCombi += product((stats[*].occurrence)[[classA,classB]],/Integer)
            endelse
            nCombi *=  n_elements(mixDescription.getMixtures(classA, classB))
            nCombinations += nCombi
          endforeach
          nLines = nCombinations 
          
          ;add pure pixels
          foreach class, mixDescription.getClasses(/Pure) do begin
            nLines += stats[class].occurrence           
          endforeach
          
          nFinal = nLines
          nSamples = 1
          
      end
      'Img' : begin
          ;calculate number of image samples
          foreach combi, mixDescription.getClassCombinations() do begin
            fractions = mixDescription.getMixtures(combi[0], combi[1])
            nSamples = max([nSamples, n_elements(fractions)])
          endforeach
          nSamples += 2 ;left and right 100%/pure spectra
          
          ;calculate all combinations (each combination is a line)
          foreach combi, mixDescription.getClassCombinations() do begin
            classA = combi[0]
            classB = combi[1]
            if classA eq classB then begin
             n = stats[classA].occurrence
             for i=1, n do begin
                nCombinations += n - i
             endfor
            endif else begin
              nCombinations += product((stats[*].occurrence)[[classA,classB]],/Integer)
            endelse
          endforeach
          nLines += nCombinations
          
          ;add non-mixed specra
          foreach class, mixDescription.getClasses(/Pure) do begin
            n = stats[class].occurrence
            lines = long(n) / nSamples
            if long(n) mod nSamples ne 0 then lines++
            nLines +=  lines           
          endforeach
          
          
          nFinal = nLines * nSamples
        end
     else: message, 'unknown outputMode'
   endcase
  
  return, {nProfiles:nFinal, nSamples:nSamples, nLines:nLines}
end


;+
; :Hidden:;-
pro test_hubLibMix_getFinalNumberOfProfiles
  
  n = 4
 
  nCombinations = 0
  for i=1, n do begin
     for j=i+1, n do begin
      nCombinations++
     endfor
  endfor
  print, nCombinations
  
  print, fix(factorial(n)-factorial(n-1))
end
