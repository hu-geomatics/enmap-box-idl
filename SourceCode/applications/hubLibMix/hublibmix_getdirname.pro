;+
; :Author: <author name> (<email>)
;-

function hubLibMix_getDirname
  
  result = filepath('hubLibMix', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_hubLibMix_getDirname

  print, hubLibMix_getDirname()
  print, hubLibMix_getDirname(/SourceCode)

end
