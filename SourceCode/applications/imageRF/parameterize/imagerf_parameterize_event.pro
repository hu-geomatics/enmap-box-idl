;+
; :Description:
;    This procedure is the event handler for a EnMAP-Box Menu button. 
;    It is called when clicking on the button, starts the 
;    `imageRF_parameterize_application` procedure and catches 
;    all errors that orrure during processing.
;    
; :Params:
;    event: in, required, type=button event structure
;
;-
pro imageRF_Parameterize_Event $
  ,event
  
  @huberrorcatch
    
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  imageRF_application,GroupLeader=event.top,rfType=menuEventInfo.argument, ACTION='train' 
  
end