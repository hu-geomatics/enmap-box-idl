pro imagerf_example


  ;A dictionary to store paramters.
  p = dictionary()  
  
  ;1. train the model
  p['numberOfFeatures'] = sqrt(114) 
  p['numberOfTrees'] = 50
  p['inputImage']  = hub_getTestImage('Hymap_Berlin-A_Image')
  p['inputLabels'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  p['model'] = FILEPATH('model.rfc',/TMP) ;output path
  imageRF_parameterize_processing,p
  
  ;2. use the model to classify the Hymap_Berlin-B_Image dataset
  p['inputImage']  = hub_getTestImage('Hymap_Berlin-B_Image') ;feature image to classify
  p['outputImgEst'] = FILEPATH('predictions.bsq',/TMP) ;output path of classified image
  imageRF_apply_imageProcessing,p
  print, 'Calculations done!'
  

end
