function imageRF_variableImportance_getReport_Plot $
  ,data $
  ,title $
  ,ytitle
  
  buffer = 1

  nVar = N_ELEMENTS(data)
  
  w = window(DIMENSIONS=[600 > nVar * 8,500], buffer=buffer, title=title)
  YRANGE=[0., max(data) * 1.05]
  


  ; label x-axis (no label with data type float)
  IF nVar le 10 THEN BEGIN
    xTickVal = LINDGEN(nVar)
    xminor=0
  ENDIF
  
  xvec = indgen(nVar)
  yvec = data > 0.
  
  !NULL = barplot(xvec-0.5,yvec,/HISTOGRAM $
              , XTITLE = 'Variable Index' $
              , xminor=-1 $
              , XSTYLE = 1, YSTYLE = 3 $
              , XTICKDIR = 1, YTICKDIR=1 $
              , CURRENT=w)
  
return, w
END

function imageRF_variableImportance_getReport, RESULTS, parameters
 
  RL = hubReportList(title =  'imageRF Variable Importance')
  R = RL.newReport(title='Model')
  R.addMonospace, ['filename: ' + parameters['model']]
  
  R = RL.newReport(title='Importance Plots')
  R.addHeading, 'Normalized Variable Importance', 2
  R.addImage, imageRF_variableImportance_getReport_Plot( $
        results['normalizedVariableImportance'] $
        ,'Normalized Variable Importance' $
        ,'Variable Importance' $
        )
        
  R.addHeading, 'Raw Variable Importance', 2      
  R.addImage, imageRF_variableImportance_getReport_Plot( $
         results['rawVariableImportance'] $
        ,'Raw Variable Importance' $
        ,'Variable Importance' $
        )
        
  
  R = RL.newReport(title='Importance Values')
;  headings = ['Variable', 'Raw'   , 'Normalized', 'StdDev', 'Band Names']
;  formats  = ['(%"%3i")', '(%"%f")', '(%"%f")','(%"%f")', '(%"%-s")']
  table = hubReportTable();headings, formats=formats)
  nVar = n_elements(results['stdDev'])
  
  table.addColumn, indgen(nVar)+1, NAME='Variable'
  table.addColumn, results['rawVariableImportance'], NAME='Raw'
  table.addColumn, results['normalizedVariableImportance'], NAME='Normalized'
  table.addColumn, results['stdDev'], NAME='StdDev'
  table.addColumn, results['band names'], NAME='Band Names'
  R.addHTML, table.getHTMLTable()
  
  return,  RL
end

pro test_imageRF_variableImportance_getReport
  RESOLVE_ROUTINE,'imageRF_parameterize_processing',/COMPILE_FULL_FILE 
  test_imageRF_parameterize_processing, model=model, rftype='RFC'
  
  ;train model
  parameters = Hash('model', model)
  results = imageRF_variableImportance_processing(parameters)
  report = imageRF_variableImportance_getReport(results, parameters) 
  report.saveHTML, /SHOW
end



