;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    Loads an RFC or RFR model and computes the raw and normalized
;    variable importance. Both will be stored in the file 
;    `<file name of your model>_VarImportance.csv` and plotted in a new 
;    window.
;    
; :Params:
;    parameters: in, required, type=Hash
;       A hash that contains the following parameters (* = optional)::
;       
;           key      | type   | description
;         -----------+--------+--------------------------------------------------
;           model    | string | file path of RFC or RFR model
;         * title    | string | title to be shown in the progress bar and 
;                    |        | the window that plots the variable importances.
;         -----------+--------+--------------------------------------------------
;
;    guiSettings : in, optional, type=hash
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          noShow      | bool | set this to avoid showing a progress bar
;          noOpen      | bool | set this to avoid opening the created image
;          ------------+------+-----------------
;-
function imageRF_variableImportance_processing, parameters, guiSettings
  if ~isa(guiSettings) then guiSettings = hash()
  
  HELPER = HubHelper()
  required = ['model']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  _title = parameters.hubIsa( 'title')? parameters['title'] : !NULL
  
  progressBar = hubProgressBar( $
        title = _title $
      , info = 'Model: ' + parameters['model'] $
      , GroupLeader = guiSettings.hubGetValue('groupLeader') $
      , NoShow = guiSettings.hubKeywordset('noShow'))
  
  IF ~(FILE_INFO(parameters['model'])).EXISTS THEN MESSAGE, "File doesn't exist!!"
  
  imageRF_loadModel $
    ,parameters['model'] $
    ,SETTINGS=settings $
    ,RFMODELSPLITPOINTPTRARR=rfModelSplitpointPtrArr $
    ,RFMODELSPLITVARIABLEPTRARR=rfModelSplitvariablePtrArr $
    ,RFMODELNODECLASSPTRARR=rfModelNodeclassPtrArr $
    ,OOBINDEX_ARR=oobIndex_arr $
    ,ERRORRATE=errorrate $
    ,FEATURES=features $
    ,DEPENDENTVARIABLE=dependentVariable $
    ,REFERENCECLASSES=referenceClasses
  
  ;---------------------
  ; Variable Importance
  ;---------------------
  oobArray = (ULONARR(settings.totalClassSamples,settings.numberOfClasses,settings.numberOfBands))
  varError = FLTARR(settings.numberOfTrees,settings.numberOfBands)
  diffVarError = FLTARR(settings.numberOfTrees,settings.numberOfBands)
  
  seed = SYSTIME(1)
  !NULL = RANDOMU(seed,1)
  
  
  FOR ntree=0,settings.numberOfTrees-1 DO BEGIN
    progressBar.setProgress, (ntree+1.)/settings.numberOfTrees
    
    FOR band=0,(settings.numberOfBands)-1 DO BEGIN
      varPixeldata = features
      varPixeldata[band,*] = features[band, SORT(RANDOMU(seed, settings.totalClassSamples))]
      ptr_pixeldata = PTR_NEW(varPixeldata)

      lfnr = 0
      imgsize = [N_ELEMENTS((*ptr_pixeldata)[0,*]),1]
      
      case settings.rfType of
        'RFR' : ptr_oobArray = PTR_NEW(DBLARR(imgsize[0], imgsize[1])) 
        'RFC' : ptr_oobArray = PTR_NEW(BYTARR(imgsize[0], settings.numberOfClasses))
      endcase
      
     imageRF_Ruling,lfnr,ptr_pixeldata,ptr_oobArray,*oobIndex_arr[ntree],settings.rfType, imgsize, settings.categoricalArray, rfModelSplitpointPtrArr[ntree], rfModelSplitvariablePtrArr[ntree], rfModelNodeclassPtrArr[ntree]
      case settings.rfType of
        'RFR' : varError[ntree,band] = imageRF_computeErrorRegression((*ptr_oobArray)[*oobIndex_arr[ntree]], dependentVariable[*oobIndex_arr[ntree]]) ; N_ELEMENTS(*oobIndex_arr[ntree]))  
        'RFC' : varError[ntree,band] = imageRF_computeErrorClassification((*ptr_oobArray)[*oobIndex_arr[ntree],*], referenceClasses[*oobIndex_arr[ntree]]) 
      endcase
      
      ; Aufräumen
      PTR_FREE, ptr_oobArray, ptr_pixeldata
    ENDFOR
    
    case settings.rfType of
      'RFR': diffVarError[ntree,*] = varError[ntree,*] - errorrate[ntree] 
      'RFC': diffVarError[ntree,*] = (100. - varError[ntree,*]) - (100. - errorrate[ntree])
    endcase
    
  ENDFOR
  results = Hash()
  results['stdDev'] = STDDEV(diffVarError,DIMENSION=1)
  results['rawVariableImportance'] = TOTAL(diffVarError,1) / settings.numberOfTrees
  results['normalizedVariableImportance'] = results['rawVariableImportance'] / results['stdDev']
  tagnames = tag_names(settings)
  hasBandNames = total(tagnames eq 'BANDNAMES') eq 1
  if hasBandNames then begin
    results['band names'] = settings.bandnames
  endif else begin
    results['band names'] = 'Band ' + strtrim(indgen(settings.numberOfBands)+1,2)
  endelse
  
  
  ; cleanup
  PTR_FREE $
    ,rfModelSplitpointPtrArr $
    ,rfModelSplitvariablePtrArr $
    ,rfModelNodeclassPtrArr $
    ,oobIndex_arr
 
  if isa(progressBar) then progressBar.cleanup
  return, results
END
