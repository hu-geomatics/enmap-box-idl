;+
; :hidden:
;-
;+
; :Private:
; :hidden_file:
; :Description:
;    Loads a RF model and checks the variables and settings.
;
; :Params:
;    fn_model : in, required, type=string
;                   Path and filename to model.
;
; :Keywords:
;    SETTINGS : out, optional, type=structure
;       Information and settings for the model.
;    RFMODELSPLITPOINTPTRARR : out, optional
;       Array of pointers with the split points to split for each tree.
;    RFMODELSPLITVARIABLEPTRARR : out, optional, type=int
;       Array of pointers with the variables to split for each tree.
;    RFMODELNODECLASSPTRARR : out, optional
;       Array of pointers with the class/estimation for the leaves of each tree.
;    OOBERROR : out, optional, type=double
;       Array of out-of-bag-errors for each tree.
;    OOBINDEX_ARR : out, optional, type=int
;       Array of pointers with the indices of the out-of-bag-data of each tree.
;    ERRORRATE : out, optional, type=double
;       Array of error-rates for each tree.
;    FEATURES : out, optional
;       The training data. Required to determine the variable importance.
;    DEPENDENTVARIABLE : out, optional
;       The dependent variable for regression.
;    REFERENCECLASSES : out, optional, type=byte
;       The class labels for classification.
;    MATRIX : out, optional
;       The confusion matrix for accurracy assessment (classification only).
;
; :Author: Carsten Oldenburg
;-
PRO imageRF_loadModel $
  ,fn_model $
  ,SETTINGS=settings $
  ,RFMODELSPLITPOINTPTRARR=rfModelSplitpointPtrArr $
  ,RFMODELSPLITVARIABLEPTRARR=rfModelSplitvariablePtrArr $
  ,RFMODELNODECLASSPTRARR=rfModelNodeclassPtrArr $
  ,OOBERROR=oobError $
  ,OOBINDEX_ARR=oobIndex_arr $
  ,ERRORRATE=errorrate $
  ,FEATURES=features $
  ,DEPENDENTVARIABLE=dependentVariable $
  ,REFERENCECLASSES=referenceClasses $
  ,MATRIX=matrix $
  ,hubOOBPerformances=hubOOBPerformances
  
  ;ON_ERROR,2

  model = IDL_Savefile(fn_model)
  model.restore, 'settings'

  tagsSettings = TAG_NAMES(settings)
  settingsNames = ['NUMBEROFTREES','NUMBEROFCLASSES','NAMESOFCLASSES','TOTALCLASSSAMPLES','LOOKUP','NUMBEROFFEATURES','NUMBEROFBANDS','IMPURITYFUNCTION','RFTYPE','INPUTIMAGE','INPUTLABELS','DATATYPE','DATATYPE_Y','CATEGORICALARRAY','DATAIGNOREVALUE']
  FOREACH tag,settingsNames,t DO BEGIN
    IF ~TOTAL(STRCMP(tagsSettings,settingsNames[t]),/INTEGER) THEN MESSAGE, 'Parameter '+tag+ ' missing!!'
  ENDFOREACH

  IF settings.numberOfTrees lt 1 OR settings.numberOfTrees gt 60000 THEN MESSAGE, 'Number of trees is not valid!!'
  IF settings.numberOfClasses lt 1 OR settings.numberOfClasses gt 255 THEN MESSAGE, 'Number of classes is not valid!!'
  IF settings.totalClassSamples lt 1 THEN MESSAGE, 'Number of total class samples not valid!!'
;  IF ~settings.rfType THEN BEGIN
;    IF N_ELEMENTS(settings.namesOfClasses) ne settings.numberOfClasses+1 THEN MESSAGE, "Names of classes don't match number of classes!!" 
;    IF N_ELEMENTS(settings.lookup) ne settings.numberOfClasses+1 THEN MESSAGE, "Lookup not valid!!"
;  ENDIF 
  IF ~TOTAL(settings.rfType eq ['RFR','RFC'],/INTEGER) THEN MESSAGE, 'Type of random forests is not valid!!'
  IF settings.rfType eq 'RFC' AND ~TOTAL(strcmp(settings.impurityFunction, ['gini','entropy'], /FOLD_CASE),/INTEGER) THEN MESSAGE, 'Impurity function is not valid!!'
  IF settings.numberOfBands lt 1 THEN MESSAGE, 'Model not valid!!'
  IF settings.numberOfFeatures lt 1 OR settings.numberOfFeatures gt settings.numberOfBands THEN MESSAGE, 'Model not valid!!'
  IF N_ELEMENTS(settings.categoricalArray) ne settings.numberOfBands THEN MESSAGE, 'Model not valid!!'

  IF ARG_PRESENT(RFMODELSPLITPOINTPTRARR) or ARG_PRESENT(RFMODELSPLITVARIABLEPTRARR) or ARG_PRESENT(RFMODELNODECLASSPTRARR) THEN BEGIN
  
    model.restore, 'rfModelSplitpointPtrArr'
    model.restore, 'rfModelSplitvariablePtrArr'
    model.restore, 'rfModelNodeclassPtrArr'
    
    IF N_ELEMENTS(rfModelSplitpointPtrArr) ne settings.numberOfTrees OR $
      N_ELEMENTS(rfModelSplitvariablePtrArr) ne settings.numberOfTrees OR $
      N_ELEMENTS(rfModelNodeclassPtrArr) ne settings.numberOfTrees THEN MESSAGE, "Arrays of the model don't have the same size!!"
      
    FOREACH element,rfModelSplitpointPtrArr,i DO BEGIN
      IF ~PTR_VALID(rfModelSplitpointPtrArr[i]) THEN MESSAGE, 'Array of the model is not valid!!'
      IF ~PTR_VALID(rfModelSplitvariablePtrArr[i]) THEN MESSAGE, 'Array of the model is not valid!!'
      IF ~PTR_VALID(rfModelNodeclassPtrArr[i]) THEN MESSAGE, 'Array of the model is not valid!!'
    ENDFOREACH
    
  ENDIF  
  
  IF ARG_PRESENT(OOBERROR) THEN BEGIN
    model.restore, 'oobError'
    IF N_ELEMENTS(oobError) ne settings.numberOfTrees THEN MESSAGE, 'Model not valid!!'
  ENDIF
  
  IF ARG_PRESENT(ERRORRATE) THEN BEGIN
    model.restore, 'errorrate'
    IF N_ELEMENTS(errorrate) ne settings.numberOfTrees THEN MESSAGE, 'Model not valid!!'
  ENDIF
  
  IF ARG_PRESENT(hubOOBPerformances) THEN BEGIN
    model.restore, 'hubOOBPerformances'
    ;IF N_ELEMENTS(oobConfusion) ne settings.numberOfTrees THEN MESSAGE, 'Model not valid!!'
  ENDIF
    
  IF ARG_PRESENT(OOBINDEX_ARR) THEN BEGIN
    model.restore, 'oobIndex_arr'
    IF N_ELEMENTS(oobIndex_arr) ne settings.numberOfTrees THEN MESSAGE, 'Model not valid!!'
  ENDIF
  
  IF ARG_PRESENT(FEATURES) THEN BEGIN
    model.restore, 'features'
    IF ~PRODUCT(SIZE(features,/DIMENSIONS) eq [settings.numberOfBands,settings.totalClassSamples]) THEN MESSAGE, 'Model not valid!!'
    FOREACH element,oobIndex_arr,e DO IF ~PTR_VALID(oobIndex_arr[e]) THEN MESSAGE, 'Array of the model is not valid!!'
  ENDIF  

  IF ARG_PRESENT(dependentVariable) or ARG_PRESENT(referenceClasses) THEN BEGIN
    IF settings.rfType eq 'RFR' THEN BEGIN
      model.restore, 'dependentVariable'
      IF N_ELEMENTS(dependentVariable) ne settings.totalClassSamples THEN MESSAGE, 'Model not valid!!'
    ENDIF ELSE BEGIN
      model.restore, 'referenceClasses'
      IF N_ELEMENTS(referenceClasses) ne settings.totalClassSamples THEN MESSAGE, 'Model not valid!!'
    ENDELSE
  ENDIF
  
  IF ARG_PRESENT(MATRIX) THEN BEGIN
    model.restore, 'matrix'
    IF ~PRODUCT(SIZE(matrix,/DIMENSIONS) eq [settings.numberOfClasses,settings.numberOfClasses]) THEN MESSAGE, 'Matrix is not valid!!'
  ENDIF
  
  model.cleanup

END