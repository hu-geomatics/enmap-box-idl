;+
; :hidden:
;-
;+
; :Private:
; 
; :Private_file:
;
; :Description:
;    Calls the function to compute the performance measures for regression.
;
; :Params:
;    meanOobArray
;    dependentVariable
;
;
;
; :Author: Carsten Oldenburg
;-
FUNCTION imageRF_computeErrorRegression $
  ,meanOobArray $
  , dependentVariable $
  , hubOOBPerformance=hubOOBPerformance
  

  !except=0
  hubOOBPerformance = hubMathRegressionPerformance(meanOobArray, dependentVariable)
  residuals = meanOobArray - dependentVariable
  hubOOBPerformance['variance'] = variance(mean(residuals))
  hubOOBPerformance['stdDeviation'] = sqrt(hubOOBPerformance['variance'])
   
  ;meanSquaredError = mean((meanOobArray - dependentVariable)^2)
  RETURN, hubOOBPerformance['meanSquaredError']
END