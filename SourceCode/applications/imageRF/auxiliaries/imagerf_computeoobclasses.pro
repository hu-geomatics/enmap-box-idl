;+
; :hidden:
;-
;+
; :Private:
; :hidden_file:
; :Description:
;    Computes the predicted classes of the out-of-bag-data and the index of the values.
;
; :Params:
;    oobArray
;
;
;
; :Author: Carsten Oldenburg
;-
FUNCTION imageRF_computeOobClasses $
  ,oobArray

  on_error,2

  !except=0

  valIndex = WHERE(TOTAL(oobArray,2,/PRESERVE_TYPE) ne 0L)
  !NULL = MAX(oobArray,maxIndex,DIMENSION=2)
  arr_index = ARRAY_INDICES(oobArray,maxIndex)
  oobClasses = (TRANSPOSE(arr_index))[*,1]
  oobClasses[valIndex]++
  result = { $
     ;oobClasses : oobClasses $
     oobClasses : byte(oobClasses) $
    ,valIndex : valIndex $
  }
  RETURN, result
END