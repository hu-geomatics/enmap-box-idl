;+
; :hidden:
;-
;+
; :Private:
; :hidden_file:
; :Description:
;    Builds the tree and assigns a class/value at the leaves.
;
; :Params:
;    lfnr
;    ptr_pixeldata
;       Features
;    ptr_classimg
;       Pointer to the rule image
;    index
;    rfType
;    imgsize
;    categoricalArray
;       For categorical variables
;    rfModelSplitpointPtrArr
;    rfModelSplitvariablePtrArr
;    rfModelNodeclassPtrArr
;
;
;
; :Author: Carsten Oldenburg
;-
PRO imageRF_Ruling $
  ,lfnr $
  ,ptr_pixeldata $
  ,ptr_classimg $
  ,index $
  ,rfType $
  ,imgsize $
  ,categoricalArray $
  ,rfModelSplitpointPtrArr $
  ,rfModelSplitvariablePtrArr $
  ,rfModelNodeclassPtrArr

  ON_ERROR,2
 
  IF FINITE((*rfModelNodeclassPtrArr)[lfnr],/NAN) THEN BEGIN
    IF index ne !NULL THEN BEGIN
      if (*rfModelSplitvariablePtrArr)[lfnr] eq -1 then stop
      IF categoricalArray[(*rfModelSplitvariablePtrArr)[lfnr]] THEN BEGIN
        ;index_l = WHERE((*ptr_pixeldata)[index,(*rfModelSplitvariablePtrArr)[lfnr]] eq $
        ;  (*rfModelSplitpointPtrArr)[lfnr], count, COMPLEMENT=index_r,  NCOMPLEMENT=ccount, /NULL)
        index_l = WHERE((*ptr_pixeldata)[(*rfModelSplitvariablePtrArr)[lfnr],index] eq $
          (*rfModelSplitpointPtrArr)[lfnr], count, COMPLEMENT=index_r,  NCOMPLEMENT=ccount, /NULL)
      ENDIF ELSE BEGIN
        ;index_l = WHERE((*ptr_pixeldata)[index,(*rfModelSplitvariablePtrArr)[lfnr]] le $
        ;  (*rfModelSplitpointPtrArr)[lfnr], count, COMPLEMENT=index_r,  NCOMPLEMENT=ccount, /NULL)
        index_l = WHERE((*ptr_pixeldata)[(*rfModelSplitvariablePtrArr)[lfnr],index] le $
          (*rfModelSplitpointPtrArr)[lfnr], count, COMPLEMENT=index_r,  NCOMPLEMENT=ccount, /NULL)
      ENDELSE
      leftIndex = index[index_l]
      rightIndex = index[index_r]
    ENDIF
    imageRF_Ruling,++lfnr, ptr_pixeldata, ptr_classimg, leftIndex, rfType, imgsize, categoricalArray, rfModelSplitpointPtrArr, rfModelSplitvariablePtrArr, rfModelNodeclassPtrArr
    imageRF_Ruling,++lfnr, ptr_pixeldata, ptr_classimg, rightIndex, rfType, imgsize, categoricalArray, rfModelSplitpointPtrArr, rfModelSplitvariablePtrArr, rfModelNodeclassPtrArr
  ENDIF ELSE BEGIN
    IF index ne !NULL THEN BEGIN
       CASE rfType of
        'RFR' : (*ptr_classimg)[index] = (*rfModelNodeclassPtrArr)[lfnr]
        'RFC' : BEGIN
            result = index + FIX((*rfModelNodeclassPtrArr)[lfnr]) * imgsize[0] * imgsize[1]
            (*ptr_classimg)[result] = 1 ; ++ geht nicht wegen Bootstrapdaten
                END
        ELSE : message, 'unknown rfType'
       ENDCASE
;      IF rfType THEN BEGIN
;        (*ptr_classimg)[index] = (*rfModelNodeclassPtrArr)[lfnr]
;      ENDIF ELSE BEGIN
;        result = index + FIX((*rfModelNodeclassPtrArr)[lfnr]) * imgsize[0] * imgsize[1]
;        (*ptr_classimg)[result] = 1 ; ++ geht nicht wegen Bootstrapdaten
;      ENDELSE
    ENDIF
  ENDELSE
END