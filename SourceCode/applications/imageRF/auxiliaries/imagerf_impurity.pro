;+
; :hidden:
;-
;+
; :Private:
; :hidden_file:
; :Description:
;    Computes the impurity and the probability for an node t.
;
; :Params:
;    n_jt
;    n_j
;    pi_j
;    impurityFunction
;    y
;    rfType
;
;
;-
FUNCTION imageRF_Impurity $
  ,n_jt $
  ,n_j $
  ,pi_j $
  ,impurityFunction $
  ,y $
  ,rfType

  ON_ERROR,2
 
  ; Wahrscheinlichkeit das eine Beobachtung in Klasse j und in Knoten t fällt
  p_jt = pi_j*n_jt/n_j
  ; Gesamtwahrscheinlichtkeit das Klassen in Knoten t zu fallen
  pt = TOTAL(p_jt)
  ; Impurity/Reinheit für den (Kind-)Knoten
  CASE rfType OF
    'RFR' : BEGIN ; Regression
              yCenter = (y - MEAN(y,/DOUBLE))
              i_t = MATRIX_MULTIPLY(yCenter,yCenter,/ATRANSPOSE) / n_jt ; s^2
            END
    'RFC' : BEGIN ; classification
            CASE impurityFunction OF
              'gini' : BEGIN ; Gini Index
                      ; Wahrscheinlichkeit das eine Beobachtung in Knoten t in Klasse j fällt
                      p_j_t = p_jt/pt
                      i_t = 1-MATRIX_MULTIPLY(p_j_t, p_j_t, /ATRANSPOSE)
                    END
              'entropy' : BEGIN ; Entropy
                      ; Wahrscheinlichkeit das eine Beobachtung in Knoten t in Klasse j fällt
                      p_j_t = p_jt/pt
                      index = WHERE(p_j_t gt 1.E-10)
                      i_t = - MATRIX_MULTIPLY(p_j_t[index], ALOG10(DOUBLE(p_j_t[index])), /ATRANSPOSE)
                    END
             ;'RFR' : ;do nothing
            ELSE: message, 'unknown type of impurity function: '+impurityFunction
            ENDCASE
          END
   ENDCASE
  RETURN, [i_t,pt]

END