;+
; :hidden:
;-
;+
; :Private:
; :hidden_file:
; :Description:
;    Split the data in the best split point.
;
; :Params:
;    data
;    ptr_index
;    splitpoint
;    splitvariable
;    isCategorical
;
; :Keywords:
;    INDEX_L
;    INDEX_R
;
; :Author: Carsten Oldenburg
;-
PRO imageRF_SplitData,INDEX_L=index_l,INDEX_R=index_r,data,ptr_index,splitpoint,splitvariable,isCategorical

 
  ;train = (*data)[ptr_index,*]
  train = (*data)[*,ptr_index]
  ;IF isCategorical THEN traindata_l = WHERE(train[*,splitvariable] eq splitpoint, COMPLEMENT=traindata_r) $
  ;  ELSE traindata_l = WHERE(train[*,splitvariable] le splitpoint, COMPLEMENT=traindata_r)
  if isCategorical then begin
    traindata_l = WHERE(train[splitvariable,*] eq splitpoint, COMPLEMENT=traindata_r) 
  endif else begin
    traindata_l = WHERE(train[splitvariable,*] le splitpoint, COMPLEMENT=traindata_r)
  endelse
  index_l = (ptr_index)[traindata_l]
  index_r = (ptr_index)[traindata_r]
END

;+
; :Private:
;
; :Description:
;    Determine random variables to find the best split in a node.
;
; :Params:
;    len
;    n_in
;
; :Keywords:
;    SEED
;
; :Author: by www.idlcoyote.com
;-
FUNCTION imageRF_generateRndVar,len,n_in,SEED=Seed

  
  inds = LONARR(n_in)
  n = n_in
  WHILE fix(n) GT 0 DO BEGIN
    inds[n_in-n] = LONG(RANDOMU(Seed, n)*len)
    u = UNIQ(inds)
    n = n_in - N_ELEMENTS(u)
    inds[0] = inds[u]
  ENDWHILE
  RETURN, inds
END

;+
; :Private:
;
; :Description:
;    Compute the frequencies of the training data in a node.
;
; :Params:
;    pixeldata
;    classes
;    band
;    bandsChosen
;    numberOfClasses
;
; :Keywords:
;    VALUES
;    CLASS_SAMPLES
;    N_VALUES
;
; :Author: Carsten Oldenburg
;-
FUNCTION imageRF_ComputeFreq,pixeldata,classes,band,bandsChosen,numberOfClasses $
    ,VALUES=values,CLASS_SAMPLES=class_samples, N_VALUES=n_values 
   
  pixel_temp = pixeldata[bandsChosen[band],*]
  index = UNIQ(pixel_temp, SORT(pixel_temp))
  n_values[band] = N_ELEMENTS(index)
  values = pixel_temp(index)
  data = UINTARR(n_values[band],numberOfClasses)

  minClass = MIN(classes, MAX=maxClass)
  classIndex = BINDGEN(maxClass+1-minClass)+minClass
  hist = histogram(isa(classes,/ARRAY)? classes : [classes] , MAX=maxClass, MIN=minClass, REVERSE_INDICES=R)
  
  FOREACH class, classIndex, c DO BEGIN
    class_samples[class-1] = hist[c]
    IF hist[c] gt 0 AND n_values[band] gt 1 THEN BEGIN
      ;class_samples[class-1] = hist[c]
      result = VALUE_LOCATE(values,pixel_temp[R[R[c]:R[c+1]-1]])
      data[0,class-1] = HISTOGRAM(result,MIN=0,MAX=n_values[band]-1)
    ENDIF
  ENDFOREACH
  
  RETURN, data 
    
END
  
;+
; :Private:
;
; :Description:
;    Calls the function to compute the impurities and probabilities
;    for all possible childnodes.
;
; :Params:
;    maxNumberOfValues
;    n_values
;    numberOfFeatures
;    data_arr
;    root_class_samples
;    aprioriProbs
;    numberOfClasses
;    class_samples
;    impurity_fkt
;    rfType
;    dependentVariable
;    categorical
;
; :Keywords:
;    IMPURITYLEFTCHILDNODE
;    IMPURITYRIGHTCHILDNODE
;    PROPABILITYLEFTCHILDNODE
;    PROPABILITYRIGHTCHILDNODE
;
; :Author: Carsten Oldenburg
;-
PRO imageRF_ImpurityMatrix,maxNumberOfValues,n_values,numberOfFeatures,data_arr,root_class_samples $
  ,aprioriProbs,numberOfClasses,class_samples,impurity_fkt,rfType,dependentVariable,categorical $
  ,IMPURITYLEFTCHILDNODE=impurityLeftChildnode,IMPURITYRIGHTCHILDNODE=impurityRightChildnode $
  ,PROPABILITYLEFTCHILDNODE=propabilityLeftChildnode,PROPABILITYRIGHTCHILDNODE=propabilityRightChildnode

 
  ; Arrays für die Reinheiten erstellen und mit NAN-Werten füllen,
  ; diese werden bei der MIN-Funktion nicht berücksichtigt
  impurityLeftChildnode = MAKE_ARRAY(maxNumberOfValues,numberOfFeatures, /DOUBLE, VALUE=!VALUES.F_NAN)
  impurityRightChildnode = MAKE_ARRAY(maxNumberOfValues,numberOfFeatures, /DOUBLE, VALUE=!VALUES.F_NAN)
  propabilityLeftChildnode = DBLARR(maxNumberOfValues,numberOfFeatures)
  propabilityRightChildnode = DBLARR(maxNumberOfValues,numberOfFeatures)
  
  FOR b=0, numberOfFeatures-1 DO BEGIN
    IF n_values[b] gt 1 THEN BEGIN
    
      IF categorical[b] THEN BEGIN
        upwards = *data_arr[b]
        lastIndexToCompute = 1 ; all categorical values
      ENDIF ELSE BEGIN
        upwards = TOTAL((*data_arr[b]),1,/CUMULATIVE,/DOUBLE)
        lastIndexToCompute = 2 ; no split at last value
      ENDELSE
      downwards = REBIN(TRANSPOSE(class_samples),n_values[b],numberOfClasses) - upwards

      FOR i=0ULL, n_values[b]-lastIndexToCompute DO BEGIN
        IF rfType eq 'RFR' THEN BEGIN
          dependentVariableL = dependentVariable[0:i]
          dependentVariableR = dependentVariable[i+1:*]
        ENDIF
        temp_l = imageRF_Impurity(upwards[i,*], root_class_samples, aprioriProbs, impurity_fkt, dependentVariableL,rfType)
        impurityLeftChildnode[i,b] = temp_l[0]
        propabilityLeftChildnode[i,b] = temp_l[1]
        temp_r = imageRF_Impurity(downwards[i,*], root_class_samples, aprioriProbs, impurity_fkt, dependentVariableR,rfType)
        impurityRightChildnode[i,b] = temp_r[0]
        propabilityRightChildnode[i,b] = temp_r[1]
      ENDFOR
    ENDIF
  ENDFOR
END

;+
; :Private:
;
; :Description:
;    Main routine for the trees of random forests.
;
; :Params:
;    impurityInNode
;    traindata
;    ptr_classes
;    dependentVariable
;    bootstrapIndex
;    rfSettings
;    ptr_rfModelSplitpoint
;    ptr_rfModelSplitvariable
;    ptr_rfModelNodeclass
;
;
; :Author: Carsten Oldenburg
;-
PRO imageRF_ComputeSplit,impurityInNode,traindata,ptr_classes,dependentVariable,bootstrapIndex,rfSettings $
  ,ptr_rfModelSplitpoint,ptr_rfModelSplitvariable,ptr_rfModelNodeclass

 
  ;pixeldata = (*traindata)[bootstrapIndex,*] ; Trainingsdaten im Knoten
  pixeldata = (*traindata)[*,bootstrapIndex] ; Trainingsdaten im Knoten
  IF rfsettings.rfType EQ 'RFR' THEN dependent = (*dependentVariable)[bootstrapIndex]
  class_samples = ULONARR(rfsettings.numberOfClasses) ; Anzahl Beobachtungen je Klasse im Knoten
  ; Pointer-Arrays für Häufigkeiten, Datenwerte
  data_arr = PTRARR(rfsettings.numberOfFeatures)
  value_arr = PTRARR(rfsettings.numberOfFeatures)
  n_values = UINTARR(rfsettings.numberOfFeatures)
  seed = rfsettings.randomSeed

  ; randomn input variables (without replacement)
  bandsChosen = imageRF_generateRndVar(rfsettings.numberOfBands, rfsettings.numberOfFeatures, SEED=seed)
  rfsettings.randomSeed = seed

  ; compute class frequencies
  FOR b=0,rfsettings.numberOfFeatures-1 DO BEGIN
    data_arr[b] = PTR_NEW(imageRF_ComputeFreq(pixeldata, (*ptr_classes)[bootstrapIndex], b, bandsChosen, $
      rfsettings.numberOfClasses, VALUES=values, CLASS_SAMPLES=class_samples, N_VALUES=n_values))
    value_arr[b] = PTR_NEW(values)
  ENDFOR
  maxNumberOfValues = MAX(n_values)

  ; Abbruchkriterien: mindestens n Beobachtungen, Grenzwert für Impurity
  ; und mehr Beobachtungen als Kanäle vorhanden -> Split ist möglich
  IF TOTAL(class_samples) ge rfsettings.minNumberOfSamples AND impurityInNode gt rfsettings.impurityThreshold $
    AND TOTAL(n_values[bandsChosen], /INTEGER) gt rfsettings.numberOfFeatures THEN BEGIN ;
    ; Reinheit für die Kindsknoten berechnen
    ;----------------------------------------
      categorical = rfsettings.categoricalArray[bandsChosen]
      imageRF_ImpurityMatrix, maxNumberOfValues, n_values, rfsettings.numberOfFeatures, $
        data_arr, rfsettings.numberOfClassSamples, rfsettings.aprioriProbs, rfsettings.numberOfClasses, $
        class_samples, rfsettings.impurityFunction, rfsettings.rfType, dependent, categorical $
        ,IMPURITYLEFTCHILDNODE=impurityLeftChildnode $
        ,IMPURITYRIGHTCHILDNODE=impurityRightChildnode $
        ,PROPABILITYLEFTCHILDNODE=propabilityLeftChildnode $
        ,PROPABILITYRIGHTCHILDNODE=propabilityRightChildnode

    impurities = impurityLeftChildnode * propabilityLeftChildnode + impurityRightChildnode * propabilityRightChildnode
    [] = MIN(impurities,maxIndex, /NAN)

      ; Umrechnung in 2D-Koord. -> Splitvariable und -punkt
      index_b = maxIndex/(maxNumberOfValues)
      index_i = maxIndex MOD (maxNumberOfValues)
      temp_value = *value_arr[index_b]
      
      indexMax = n_elements(temp_value) -1
      ; bei Wertebereichen mit Lücken wird der Splitpunkt in die Mitte verlagert
      IF categorical[index_b] THEN begin
        split_p = temp_value[index_i] 
      endif else begin
        split_p = (DOUBLE(temp_value[index_i])+DOUBLE(temp_value[index_i+1 < indexMax]))/2
      endelse

      *ptr_rfModelSplitpoint = [*ptr_rfModelSplitpoint,split_p]
      *ptr_rfModelSplitvariable = [*ptr_rfModelSplitvariable,bandsChosen[index_b]]
      *ptr_rfModelNodeclass = [*ptr_rfModelNodeclass,!VALUES.F_NAN] 
      
      ; Cleanup
      PTR_FREE, data_arr, value_arr
      ;*****************************
      ;* Erzeugung der Kindsknoten *
      ;*****************************
      imageRF_SplitData, INDEX_L=index_l, INDEX_R=index_r, traindata, bootstrapIndex, split_p, bandsChosen[index_b], $
        categorical[index_b]
        
      imageRF_ComputeSplit, impurityLeftChildnode[maxIndex], traindata, ptr_classes, dependentVariable, $
        index_l, rfSettings, ptr_rfModelSplitpoint, ptr_rfModelSplitvariable, ptr_rfModelNodeclass
      imageRF_ComputeSplit, impurityRightChildnode[maxIndex], traindata, ptr_classes, dependentVariable, $
        index_r, rfSettings, ptr_rfModelSplitpoint, ptr_rfModelSplitvariable, ptr_rfModelNodeclass

  ENDIF ELSE BEGIN
    CASE rfsettings.rfType OF
    'RFR' : BEGIN
              *ptr_rfModelSplitpoint = [*ptr_rfModelSplitpoint,!VALUES.F_NAN] 
              *ptr_rfModelSplitvariable = [*ptr_rfModelSplitvariable,-1] 
              *ptr_rfModelNodeclass = [*ptr_rfModelNodeclass,MEAN((*dependentVariable)[bootstrapIndex])]
            END
    'RFC' : BEGIN
              *ptr_rfModelSplitpoint = [*ptr_rfModelSplitpoint,!VALUES.F_NAN] 
              *ptr_rfModelSplitvariable = [*ptr_rfModelSplitvariable,-1] 
              !NULL = MAX(class_samples,maxIndex)
              *ptr_rfModelNodeclass = [*ptr_rfModelNodeclass,maxIndex]
            END
     ENDCASE
  ENDELSE
END

;+
; :Private:
;
; :Description:
;    Routine to start the tree growing.
;
; :Author: Carsten Oldenburg
;-
PRO imageRF_createrootnode, ptr_pixeldata,ptr_classes,ptr_dependentVariable,bootstrapIndex,rfSettings $
  ,ptr_rfModelSplitpoint,ptr_rfModelSplitvariable,ptr_rfModelNodeclass
    
  IF rfsettings.rfType EQ 'RFR' THEN dependentVariable = (*ptr_dependentVariable)[bootstrapIndex]
  
  result = imageRF_Impurity(rfSettings.totalClassSamples, $
    rfSettings.numberOfClassSamples, rfSettings.aprioriProbs, $
    rfSettings.impurityFunction, dependentVariable,rfsettings.rfType)
    
  imageRF_ComputeSplit, result[0], ptr_pixeldata, ptr_classes, ptr_dependentVariable, $
    bootstrapIndex, rfSettings, ptr_rfModelSplitpoint, ptr_rfModelSplitvariable, ptr_rfModelNodeclass
    
END