;+
; :hidden:
;-
;+
; :Private:
; 
; :Private_file:
; :Description:
;    Calls the function to compute the performance measures for classification.
;
; :Params:
;    oobArray, in
;    referenceClasses, in
;
; :Keywords:
;    CONFUSIONMATRIX
;
; :Author: Carsten Oldenburg
;-
FUNCTION imageRF_computeErrorClassification $
  ,oobArray $
  ,referenceClasses $
  ,CONFUSIONMATRIX=resultConfusionMatrix $
  ,hubOOBPerformance=hubOOBPerformance
  ;on_error,2
 
  !except=0
  
  resultOobClasses = imageRF_computeOobClasses(oobArray)
  idx = resultOobClasses.valindex
  numberOfClasses = (size(oobArray, /Dimensions))[1]-1
  
  
  ;performance = hubApp_accuracyAssessment_DataProcessing_Classification(referenceClasses[idx], resultOobClasses.oobClasses[idx], numberOfClasses)
  reference = referenceClasses[idx]
  estimation = resultOobClasses.oobClasses[idx]
  
  
  ;##
  numberOfSamples = n_elements(reference)
  if n_elements(estimation) ne numberOfSamples then begin
    message,  'reference and estimation  must have the same number of elements'
  endif
 
  if numberOfSamples gt 0 then begin
;    if isa(where(/NULL, estimation le 0 or estimation gt numberOfClasses)) then begin
;      message, string(format='(%"Estimation class labels out of range [1:%i].")', numberOfClasses)
;    endif
;    if isa(where(/NULL, reference le 0 or reference gt numberOfClasses)) then begin
;      message, string(format='(%"Reference class labels out of range [1:%i].")', numberOfClasses)
;    endif
    
    if typename(reference) ne 'BYTE' then begin
      message, 'Wrong type for reference.'
    endif  
    if typename(estimation) ne 'BYTE' then begin
      message, 'Wrong type for estimation.'
    endif  
    
    ; create confusion matrix
    confusionMatrix = hist_2d(reference,estimation,Min1=1,Min2=1,Max1=numberOfClasses,Max2=numberOfClasses)
    mainDiagonal = confusionMatrix[indgen(numberOfClasses),indgen(numberOfClasses)]
    overallAccuracy  = total(mainDiagonal)/numberOfSamples*100.

    ;provide the full bundle of hub classification performance metrics
    hubOOBPerformance = hubMathClassificationPerformance(reference, estimation,numberOfClasses)

  ;##
  endif else begin
    confusionMatriX = !NULL
    overallAccuracy = !NULL
    hubOOBPerformance = !NULL
  endelse
  resultConfusionMatrix = confusionMatrix
  RETURN, overallAccuracy
END