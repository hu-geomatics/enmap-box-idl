
;+
; :Description:
;    This function opens a widget dialog to select a RFC or RFR model, an image file
;    and a reference file containing data values for model validation.
;    The returned hash can be used as input argument for 
;   `imageRF_validate_processing`. 
;
; :Params:
; 
;    parameters : in, required, type=hash
;     A hash containing the following values::
;       
;       hash key | type   | description
;       ---------+--------+------------
;       rfType   | string | type of RF: RFC or RFR
;       ---------+--------+------------
;       
;     This hash can also be used to provide default values.
;     
;    GUISettings : in, optional, type=hash
;    
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          ------------+------+-----------------
;
;
;-
function imageRF_validate_getParameters, parameters, guiSettings
  
  if ~isa(guiSettings) then guiSettings = hash()
  rfType = parameters.hubgetValue('rfType')
  if ~isa(rfType) then message, 'Unknown rfType'
  groupLeader = guiSettings['groupLeader']
    
  title = 'imageRF: ' + rfType + ' Fast Accuracy Assessment'
  hubAMW_program, groupLeader, Title= title
  hubAMW_frame, Title='Input'
  case rfType of 
    'RFR' : begin
              title_model = 'RFR Model      '
              title_img   = 'Image          '
              title_ref   = 'Reference Areas'
              ext_model   = 'rfr' 
              flag_reg = 1
              flag_class = 0
            end
    'RFC' : begin
              title_model = 'RFC Model      '
              title_img   = 'Image          '
              title_ref =   'Reference Areas'
              ext_model = 'rfc' 
              flag_reg = 0
              flag_class = 1
            end
     else : message, 'rfType is unknown'
  endcase
      
  hubAMW_inputFilename ,'model'       ,Title=title_model, EXTENSION=ext_model $
          , Value = parameters.hubGetValue('model')
          
  hubAMW_inputSampleSet,'inputSampleSet' , Title=title_img, referenceTitle=title_ref $
                      , value=parameters.hubGetValue('inputImage') $
                      , classification=flag_class, regression=flag_reg 
  parameters = hubAMW_manage()
  
  
  if parameters['accept'] then begin 
    parameters['title'] = title
    parameters['inputImage'] = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputReference']  = (parameters['inputSampleSet'])['labelFilename']
    parameters.remove, ['inputSampleSet']
    parameters['rfType'] = rfType
  endif 
  
  return, parameters


end