
;+
; :Description:
;    This function returns a models accuracy after performing a Fast Accuracy Assessment.
;    First an estimation is made for all pixels that are defined in the reference file.
;    Then estimation and the reference file are subject of an Accuracy Assessment. This is 
;    done using the routine `hubApp_accuracyAssessment_ImageProcessing`, which returns the final
;    performance hash.
;    
;
; :Params:
;    parameters: in, required, type=hash
;      A hash that contains the following parameters (* = optional)::
; 
;           hash key           | type   | description
;         ---------------------+--------+----------------------------------------------------------------
;           'rfType'           | string | Type of random forest algorithm {'RFC' or 'RFR'}
;           'inputImage'       | string | Path of spectral image file
;           'inputReference'   | string | Path of reference file used to validate the model
;           'model'            | string | Path of random forest model (usually *.rfc or *.rfr)
;         * 'title'            | string | Title of the calling application
;         * 'outputErrorImage' | string | Path of error image that will be created.
;         ---------------------+--------+----------------------------------------------------------------
;         
;    guiSettings : in, optional, type=hash
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          noShow      | bool | set this to avoid showing a progress bar
;          noOpen      | bool | set this to avoid opening the created image
;          ------------+------+-----------------
;    
;-
function imageRF_validate_Processing, parameters, guiSettings
  if ~isa(guiSettings) then guiSettings = hash()
  
  if ~guiSettings.hubIsa('noOpen') then guiSettings['noOpen'] = 1b
  
  ;check parameters
  helper = hubHelper() 
  required = ['model', 'inputImage', 'inputReference','rfType']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  if total(parameters['rfType'] eq ['RFR','RFC']) ne 1 then begin
    message, "hash key 'rfType' must be 'RFR' or 'RFC'
  endif
  
  
  
  ;create path for temporary estimation file
  fn_est_tmp = string(format='(%"tmp_%s_%i")' $
      , file_basename(parameters['model'], '.*') $
      ;, file_basename((parameters['inputSampleSet'])['labelFilename']) $
      , systime(/seconds))
  fn_est_tmp = strjoin(strsplit(fn_est_tmp,':. ',/EXTRACT))
  
  processing_parameters = Hash()
  processing_parameters['writeProbabilityImage'] = 0
  processing_parameters['outputImgEst'] = filepath(fn_est_tmp, /TMP)
  processing_parameters['inputMask'] = parameters['inputReference']
  processing_parameters['model'] = parameters['model']
  processing_parameters['inputImage'] = parameters['inputImage']
  processing_parameters['title'] = parameters.hubIsa('title') ? parameters['title']:'imageRF: Fast Accuracy Assessment' 
  
  imageRF_apply_imageprocessing, processing_parameters, guiSettings
  
  accass_parameters = Hash()
  accass_parameters['inputReference']  = parameters['inputReference']
  accass_parameters['inputEstimation'] = processing_parameters['outputImgEst']
  
  ;rfType is given in the model implicitely and
  ;was delivered by imageRF_apply_imageprocessing routine
  case parameters['rfType'] of 
    'RFR' : accass_parameters['type'] = 'regression'
    'RFC' : accass_parameters['type'] = 'classification'
  endcase
  
  ;write an error image if required
  if parameters.hubIsa( 'outputErrorImage') then begin
     accass_parameters['outputErrorImage'] = parameters['outputErrorImage']
  endif
  performance = hubApp_accuracyAssessment_processing(accass_parameters, guiSettings)
  performance['model'] = parameters['model']
  case parameters['rfType'] of 
    'RFR' : performance['modelInfo'] = 'Random Forests for Regression Model' 
    'RFC' : performance['modelInfo'] = 'Random Forests for Classification Model'
  endcase
  if parameters.hubKeywordSet('deleteEstimationImage') then begin
    file_delete, processing_parameters['outputImgEst']  $
               , processing_parameters['outputImgEst'] + '.hdr', /ALLOW_NONEXISTENT
  endif
  return, performance
END

;;+
;; :Hidden:
;;-
;pro test
;  types=['RFC', 'RFR']
;  foreach type, types do begin
;    parameters = Hash()
;    parameters['accept'] = 1
;    parameters['rfType'] = type
;    case type of
;    'RFC' : begin
;              parameters['inputReference'] = 'D:\Dokumente\temp\data\classification\Hymap_Berlin-A_valid
;              parameters['inputImage'] = 'D:\Dokumente\temp\data\classification\Hymap_Berlin-A_Image'
;              parameters['model'] = 'D:\Dokumente\temp\data\classification\results\temp_model.rfc'
;              parameters['outputErrorImage'] = 'D:\Dokumente\temp\data\classification\results\temp_model_errorImage_fastacc'
;            end
;    'RFR' : begin
;              parameters['inputReference'] = 'D:\Dokumente\temp\data\regression\Hymap_Berlin-B_valid
;              parameters['inputImage'] = 'D:\Dokumente\temp\data\regression\Hymap_Berlin-B_Image'
;              parameters['model'] = 'D:\Dokumente\temp\data\regression\results\temp_model.rfr'
;              parameters['outputErrorImage'] = 'D:\Dokumente\temp\data\regression\results\temp_model_errorImage_fastacc'
;            end
;    endcase
;    
;    imageRF_validate_Processing, parameters 
;  endforeach
;  print, 'Test done'
;end