;+
; :Description:
;    This function returns a hash that contains the estimates of an RF model.
;    For RFC models it is possible to add the probabilities of each class by using the keyword `returnProbabilityImage`.
;    The result hash contains the following values (* = optional)::
;    
;          key              | type      | description
;        -------------------+-----------+------------------------------------------------
;          estimates        | number[]  | numeric[sample]                   
;        * probabilityImage | number[]  | Probabilities for each class     
;                           |           | float[class,sample] 
;        -------------------+-----------+------------------------------------------------
;
; :Params:
;    features: in, required, type=number[]"
;       The feature array that contains feature values for each sample. It has the
;       dimensions number[features,samples].
;       
;    settings: in, required, type=struct
;       The settings struct as restored from the RF model file.
;       
;    RFMODELSPLITPOINTPTRARR: in, required, type=Pointer[]
;       Array of pointers with the split points to split for each tree.
;       
;    RFMODELSPLITVARIABLEPTRARR: in, required, type=Pointer[]
;       Array of pointers with the variables to split for each tree.
;       
;    RFMODELNODECLASSPTRARR: in, required, type=Pointer[]
;        Array of pointers with the class/estimation for the leaves of each tree.
;
; :Keywords:
;    mask: in, optional, type=number[]
;     A mask used to hide pixels. Must have the same number of elements as samples defined
;     in `features`.
;    
;    returnProbabilityImage: in, optional, type=boolean, default=0
;     Use this keyword to return the probability image of an RF classification.
;
;-
function imageRF_apply_dataProcessing $
    ,features, settings $;required
    ,RFMODELSPLITPOINTPTRARR $;required, TODO
    ,RFMODELSPLITVARIABLEPTRARR $;required, TODO
    ,RFMODELNODECLASSPTRARR $;required, TODO
    ,mask=mask $
    ,returnProbabilityImage=returnProbabilityImage;optional
    
   
    if ~keyword_set(returnProbabilityImage) then returnProbabilityImage=0
    if n_elements(settings.categoricalarray) ne n_elements(features[*,0]) then $
        message, 'more/less features than the model was trained for!'
    ;if size(features, /type) ne 10  then message, 'ptr_feature must be a pointer on -> Numeric[features, pixel_index]'
    result = Hash()
    
    no_pixels = (size(features, /dimension))[1]
    imgsize = [1,no_pixels]
    ;get Mask Indizes
    if keyword_set(mask) then begin
      if n_elements(mask) ne no_pixels then message, 'Number of pixels in mask and featureset differs!'
      imageIndex = WHERE(mask ne 0, /NULL , unmaskedCount, complement=maskedIndex)
    endif else begin
      imageIndex = LINDGEN(no_pixels)
    endelse
    
    ;no pixels to process? --> return empty data
    if ~isa(imageindex) then begin
      case settings.rfType of 
        'RFR' : result['estimates'] = MAKE_ARRAY(no_pixels,1, TYPE=settings.datatype_y, value=settings.dataIgnoreValue)
        'RFC' : begin
           result['estimates'] = MAKE_ARRAY(no_pixels,1, TYPE=settings.datatype_y, value=0)
           if returnProbabilityImage then $
                result['probabilityImage'] = MAKE_ARRAY(no_pixels,settings.numberOfClasses - 1, TYPE=4,value=settings.dataIgnoreValue)
           end
      endcase
    endif else begin 
        ;Create the empty probabilityImage
        CASE settings.rfType OF
          'RFR' : BEGIN
                    probabilityImage = MAKE_ARRAY(no_pixels,1, TYPE=settings.datatype_y )
                  END
          'RFC' : BEGIN
                    probabilityImage = MAKE_ARRAY(no_pixels,settings.numberOfClasses - 1, TYPE=2)
                  END
        ENDCASE 
        ptr_features = PTR_NEW(features)
        FOR ntree=0,settings.numberOfTrees-1 DO BEGIN
            lfnr = 0
            case settings.rfType of 
              'RFR' : ptr_classimg = PTR_NEW(DBLARR(no_pixels)) 
              'RFC' : ptr_classimg = PTR_NEW(BYTARR(no_pixels,settings.numberOfClasses))
            endcase
            imageRF_Ruling,lfnr, ptr_features, ptr_classimg, imageIndex, settings.rfType, imgsize, settings.categoricalArray, $
              rfModelSplitpointPtrArr[ntree], rfModelSplitvariablePtrArr[ntree], rfModelNodeclassPtrArr[ntree]
            probabilityImage = TEMPORARY(probabilityImage) + *ptr_classimg
            ;print, transpose(probabilityImage)
            PTR_FREE, ptr_classimg
         ENDFOR
          
        case settings.rfType of 
          'RFR' : BEGIN
                    probabilityImage = TEMPORARY(probabilityImage) / settings.numberOfTrees
                    if isa(mask) then probabilityImage[maskedIndex] = settings.dataIgnoreValue
                    result['estimates'] = TEMPORARY(probabilityImage)
                  END
          'RFC' : BEGIN
                    !NULL = MAX(probabilityImage,index,DIMENSION=2) ; DIMENSION=2
                    indexSize = SIZE(index,/DIMENSIONS)
                    maxIndices = (TRANSPOSE(ARRAY_INDICES(probabilityImage,index)))[*,1]
                    
                    IF isa(mask) THEN maxIndices[imageIndex]++ ELSE maxIndices++
                    result['estimates'] = REFORM(maxIndices,indexSize)
                    
                    IF returnProbabilityImage THEN BEGIN
                      probabilityImage = transpose(TEMPORARY(FLOAT(probabilityImage))) / settings.numberOfTrees
                      if isa(mask) then probabilityImage[*,maskedIndex] = settings.dataIgnoreValue
                      result['probabilityImage'] = probabilityImage
                    ENDIF 
                  END
        endcase
        PTR_FREE, ptr_features
    endelse
  return, result
end