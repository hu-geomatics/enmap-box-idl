;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro imageRF_make, Cleanup=cleanup, Distribution=distribution
  codeDir = imageRF_getDirname(/SourceCode)
  appDir = imageRF_getDirname()
  
  if keyword_set(cleanup) then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  if ~keyword_set(CopyOnly) then begin
  SAVFile = filepath('imageRF.sav', ROOT=imageRF_getDirname(), SUBDIR='lib')
  hubDev_compileDirectory, codeDir, SAVFile, nolog=enmapboxmake.nolog()
    
    if enmapboxmake.NoIDLDoc() then begin
      docDir = filepath('', ROOT_DIR=imageRF_getDirname(), SUBDIR=['help','idldoc'])
      title='imageRF Documentation'
      hub_idlDoc, codeDir, docDir, title, NOSHOW=enmapboxmake.noshow() $
                , SUBTITLE='Random Forests for Classification and Regression' $
                , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLDOC')
    endif
    
      
    ; create distributen
    
    if isa(distribution) then begin
    
      if ~isa(distribution, 'string') then begin
        title = 'Select a directory for writing the distribution.'
        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
        if directory eq '' then return
      endif else begin
        directory = distribution
      endelse

      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
      file_copy, appDir, destination, /OVERWRITE, /RECURSIVE
      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
      file_copy, codeDir, destination, /OVERWRITE, /RECURSIVE
      
    endif
  endif
end