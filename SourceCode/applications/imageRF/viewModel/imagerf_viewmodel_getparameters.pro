;+
; :Description:
;    This function opens a widget dialog to select a RFC or RFR model.
;    The returned hash can be used as input argument for 
;   `imageRF_viewModel_processing`. 
;
; 
;-
;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    parameters : in, required, type=hash
;     A hash containing the following values::
;       
;       hash key | type   | description
;       ---------+--------+------------
;       rfType   | string | type of RF: RFC or RFR
;       ---------+--------+------------
;       
;     This hash can also be used to provide default values.
;     
;    GUISettings : in, optional, type=hash
;    
;      A hash containing GUI relevant information::
;          
;          hash key    | type | description
;          ------------+------+-----------------
;          groupLeader | int  | use this to specify the widget group leader
;          ------------+------+-----------------
;
;
;-
function imageRF_viewModel_getParameters, parameters, guiSettings
  
  if ~isa(guiSettings) then guiSettings = hash()
  
  rfType = parameters.hubGetValue('rfType')
  title = 'imageRF: View ' + rfType + ' Parameters'
  
  
  hubAMW_program ,Title= title ,guiSettings.hubGetValue('groupLeader')
  hubAMW_frame   ,Title='Input'
  case rfType of 
      'RFR' : hubAMW_inputFilename ,'model', Title='RFR Model '   $
                                   , Value = parameters.hubGetValue('model') $
                                   , EXTENSION='rfr'
                                   
      'RFC' : hubAMW_inputFilename ,'model', Title='RFC Model'  $
                                   , Value = parameters.hubGetValue('model') $
                                   , EXTENSION='rfc'

      else : message, 'unknown rfType'
  endcase
  parameters = hubAMW_manage()

  if parameters['accept'] then begin 
    parameters['title'] = title
    parameters['rfType'] = rfType
  endif 
  return, parameters
END