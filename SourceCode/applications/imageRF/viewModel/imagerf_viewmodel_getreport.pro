

function imageRF_viewModel_getReport_AAPlot, settings, Metric, yrange, yTitle
  w=window(window_title='Number of Trees', BUFFER=1, DIMENSIONS=[600,400])
  for iC = 1, settings.numberOfClasses - 1 do begin
    classColor = settings.lookup[*,iC]
    className = settings.namesOfClasses[iC]
    className = strjoin(strsplit(className, '/', /EXTRACT),'-')
    
    if iC eq 1 then begin
      p = plot(Metric[iC-1,*] $
        , POSITION=[.1, .1, .7,.9] $
        , YRANGE=yrange $
        ,XTITLE='Number of Trees' $
        ,YTITLE=yTitle, BACKGROUND_COLOR = 'White', /CURRENT, Color=classColor, name=className)
      leg = legend(POSITION=[.72,.9], /NORMAL $
        , HORIZONTAL_ALIGNMENT=0. $
        , /AUTO_TEXT_COLOR, SHADOW=0, THICK=0, FONT_SIZE = 10, linestyle=6)
    endif else begin
      p = plot(Metric[iC-1,*], /CURRENT, /OVERPLOT, COLOR=classColor, name=className)
      leg.add, p
    endelse
    
  endfor
  
  img = w.Read()
  img.getProperty, Data=imgData
  w.Close
  return, transpose(imgData, [1,2,0])
end

;+
; :Description:
;    This routine shows information about an RFC or RFR model.
;
; :Params:
;    parameters: in, required, type=Hash
;       A hash that contains the following parameters (* = optional)::
;       
;           key      | type   | description
;           ---------+--------+--------------------------------------------------
;           model | string | file path of RFC or RFR model
;         * title    | string | title to be shown in the progress bar and 
;                    |        | the window that plots the variable importances.
;           ---------+--------+-------------------------------------------------- 
;
; :Keywords:
;    GroupLeader: in, required, type = long
;       Use this keyword to specify the group leader for used widgets.
;
;-
function imageRF_viewModel_getReport, parameters, guiSettings, GroupLeader=groupLeader

  IF ~(FILE_INFO(parameters['model'])).EXISTS THEN MESSAGE, "File doesn't exist!!"
  
  RL = hubReportList(title = 'imageRF Model')
  
  imageRF_loadModel $
    ,parameters['model'] $
    ,SETTINGS=settings $
    ,OOBERROR=oobError
  
  R = RL.newReport(title = 'Model')
  R.addHeading, 'imageRF Model', 2
  R.addMonospace, 'filename: '+ parameters['model']
  R.addHeading, 'Training Data', 2
  R.addMonospace, ['  Image:           ' + settings.inputImage $
                  ,'  Reference Areas: ' + settings.inputLabels $
                  ]
  oModel = IDL_Savefile(parameters['model'])
  modelTag = oModel.restore('tag', /NULL)
  if isa(modelTag) && n_elements(modelTag) gt 0 then begin
    R.addHeading, 'Tagged values', 2
    
    R.addParagraph, 'Tagged value are not part of the RF model, but can be usefull for its further application context'
    lines = list()
    foreach key, modelTag.keys() do begin
      values = modelTag[key]
      nValues = n_elements(values) 
      if nValues le 1 then begin
        lines.add, string(format='(%"%s:%s")', key, strjoin(strtrim(string(values, /PRINT),2),','))
      endif else begin
        nMax = 15
        i = 0
        lines.add, string(format='(%"%s (%i values):")', key, nValues)
        foreach value, values do begin
          i++
          lines.add, '   '+strjoin(strtrim(string(value, /PRINT),2),',')
          if i eq nMax then begin
            lines.add, '   ...'
            break
          endif
        endforeach
        
      endelse
    endforeach
    R.addMonospace, lines.toArray()
  endif
  R = RL.newReport(title='Parameterization')
  ;R.addHeading, 'Number of ...', 3
  ;R.addMonospace,   ,'  Number of... ' $
  textString = [ $ 
       'Features/Bands:             ' + STRTRIM(settings.numberOfBands,2) $
      ,'Randomly Selected Features: ' + STRTRIM(settings.numberOfFeatures,2) $
      ,'Training Samples:           ' + STRTRIM(settings.totalClassSamples, 2) $
      ,'Trees:                      ' + STRTRIM(settings.numberOfTrees,2) $
      ]
 
  
  IF settings.rfType eq 'RFR' THEN BEGIN
    titleText = 'Learning Curve (Out-Of-Bag-Error)'
    yTitle = 'Root Mean Squared Error (RMSE)'
    oobError = SQRT(oobError)
    yrange=[FLOOR(MIN(oobError)),CEIL(MAX(oobError))] 
  ENDIF ELSE BEGIN
    CASE settings.impurityFunction OF
      'gini'    : impurityText = 'Gini-Coefficient'
      'entropy' : impurityText = 'Entropy'
      ELSE : message, 'unknown impurity function'
    ENDCASE
     textString = [textString $
                ,'Classes:                    ' + STRTRIM(settings.numberOfClasses-1,2) $
                ,'Impurity Function:          ' + impurityText $
                ] 
    
    titleText = 'Learning Curve (Out-Of-Bag-Accuracy)'
    yTitle = 'Overall Accuracy [%]'
    yrange = [0D,100D]
  ENDELSE   
  R.addMonospace, textString
   
  R = RL.newReport(title=titleText, shorttitle='Learning Curve')

  ; label x-axis (no label with data type float)
  nVar = N_ELEMENTS(oobError)
  IF nVar le 10 THEN BEGIN
    xTickVal = LINDGEN(nVar)
    xminor=0
  ENDIF  
  
  w=window(window_title='Number of Trees', BUFFER=1)
  p = plot(oobError, YRANGE=yrange $
           ,XTITLE='Number of Trees' $
           ,YTITLE=yTitle, BACKGROUND_COLOR = 'White', /CURRENT)
  
 
  
  img = w.Read()
  img.getProperty, Data=imgData
  imgData = transpose(imgData, [1,2,0])
  
  R.addImage, imgData
  
  imageRF_loadModel,parameters['model'],hubOOBPerformances=hubOOBPerformances
  nT = settings.numberOfTrees
  
    
    
  IF settings.rfType eq 'RFC' THEN BEGIN
    nC = settings.numberOfClasses - 1
    
    
    F1s = MAKE_ARRAY(nC, nT, value=0.0)
    EOs = MAKE_ARRAY(nC, nT, value=0.0)
    ECs = MAKE_ARRAY(nC, nT, value=0.0)
    UAs = MAKE_ARRAY(nC, nT, value=0.0)
    PAs = MAKE_ARRAY(nC, nT, value=0.0)
    
    table = hubReportTable();['#','Class name'])
    table.addColumn, name='#'
    table.addColumn, name='Class name'
    for i=0, nC do table.addRow, List(i, settings.namesOfClasses[i])
    
    R.addHTML, table.getHTMLTable() 
                    
    headings = ['#Tree',strtrim(indgen(nC)+1,2)+ ' ' + settings.namesOfClasses[1:*]]
    ;formats  = ['(%"%i")', make_array(nC, /String,value='(%"%-6.2f")')] 
    tableF1 = hubReportTable();headings, formats=formats)
    tableUser       = hubReportTable();headings), formats=formats)
    tableProducer   = hubReportTable();headings), formats=formats)
    ;tableOmission   = hubReportTable(headings)
    ;tableCommission = hubReportTable(headings)
    for i=0, nC do begin
      tableF1.addColumn, NAME=headings[i]
      tableUser.addColumn, NAME=headings[i]
      tableProducer.addColumn, NAME=headings[i]
    endfor

        
    for iT = 0, nT-1 do begin
;      if iT eq 0 then begin
;        !NULL = DIALOG_MESSAGE(string(((hubOOBPerformances[iT]).keys()).toArray()), /INFORMATION) 
;      endif
    
      F1s[*,iT] = (hubOOBPerformances[iT])['f1Accuracy']
      UAs[*,iT] = (hubOOBPerformances[iT])['userAccuracy']
      PAs[*,iT] = (hubOOBPerformances[iT])['producerAccuracy']
      ;EOs[*,iT] = (hubOOBPerformances[iT])['errorOfOmission']
      ;ECs[*,iT] = (hubOOBPerformances[iT])['errorOfCommission']
      
  
      
      tableF1.addRow,List(iT+1, (hubOOBPerformances[iT])['f1Accuracy'], /Extract)
      tableUser.addRow,List(iT+1, (hubOOBPerformances[iT])['userAccuracy'], /Extract)
      tableProducer.addRow,List(iT+1, (hubOOBPerformances[iT])['producerAccuracy'], /Extract)
      
      ;tableOmission.addRow,List(iT+1, (hubOOBPerformances[iT])['errorOfOmission'], /Extract)
      ;tableCommission.addRow,List(iT+1, (hubOOBPerformances[iT])['errorOfCommission'], /Extract)
      
    endfor
    
   
    
    R.addHeading, 'F1 Accuracies'
    yTitle = 'F1 Accuracy [%]'
    R.addImage, imageRF_viewModel_getReport_AAPlot(settings, F1s, yrange, yTitle) $
              , 'Classwise F1 Accuracies'
    
    
    R.addHTML, tableF1.getHTMLTable() 
                              
    R.addHeading, 'User Accuracies'
    yTitle = 'User Accuracy [%]'
    R.addImage, imageRF_viewModel_getReport_AAPlot(settings, UAs, yrange, yTitle) $
      , 'Classwise User Accuracies'

    R.addHTML, tableUser.getHTMLTable() 
    
    
    R.addHeading, 'Producer Accuracies'
    yTitle = 'Producer Accuracy [%]'
    R.addImage, imageRF_viewModel_getReport_AAPlot(settings, PAs, yrange, yTitle) $
      , 'Classwise Producer Accuracies'

    R.addHTML, tableProducer.getHTMLTable() 
    
     
  ENDIF 
  
  IF settings.rfType eq 'RFR' THEN BEGIN
    
    
    vMAE  = MAKE_ARRAY(nT, value=0.0)
    vRMSE = MAKE_ARRAY(nT, value=0.0)
    vSTD  = MAKE_ARRAY(nT, value=0.0)
    vR    = MAKE_ARRAY(nT, value=0.0)
    vR2   = MAKE_ARRAY(nT, value=0.0)
    
    for iT = 0, nT-1 do begin

      vMAE[iT]  = (hubOOBPerformances[iT])['meanAbsoluteError']
      vRMSE[iT] = (hubOOBPerformances[iT])['rootMeanSquaredError']
      vR[iT] = (hubOOBPerformances[iT])['pearsonCorrelation']
      vR2[iT] = (hubOOBPerformances[iT])['squaredPearsonCorrelation']
      
    endfor
    
;    if hubOOBPerformances.hasKey('standardDeviation') then begin
;      for iT = 0, nT-1 do begin
;        vSTD  = MAKE_ARRAY(nT, value=0.0)
;      endfor
;    endif
;    
;    headings = ['#Tree','MAE','RMSE','R','R2']
;    formats  = ['(%"%i")', make_array(4, /String,value='(%"%f")')] 
    tableResiduals = hubReportTable();headings, formats=formats)
    
    tableResiduals.addColumn, indgen(nT)+1,NAME='#Tree'
    tableResiduals.addColumn, vMAE, NAME='MAE'
    tableResiduals.addColumn, vRMSE, NAME='RMSE'
    tableResiduals.addColumn, vR, NAME='R'
    tableResiduals.addColumn, vR2, NAME='R2'
    
    R.addHTML, tableResiduals.getHTMLTable()
    
  ENDIF
  
  return, RL
END


;+
; :Hidden:
;-
pro TEST_IMAGERF_VIEWMODEL_GETREPORT
  parameters = hash()
  parameters['model'] = 'F:\SenseCarbonProcessing\BJ_ML\99_Temp\RFCModels\Model_for-pas-def.rfc'
  R = imageRF_viewModel_getReport(parameters)
  R.saveHTML, /Show
  hubReport
  stop
  ;build the model
  type = 'RFC'
  newModel = 0
  if type eq 'RFC' then begin
    pModel = FILEPATH('MyModel.rfc', /TMP)
    pModel = 'D:\temp\MyModel.rfc'
    if ~file_test(pModel) or  newModel then begin
      p = hash()
      p['numberOfFeatures'] = sqrt(114)
      p['inputImage']  = hub_getTestImage('Hymap_Berlin-A_Image')
      p['inputLabels'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
      p['model'] = pModel
      guiSettings = Hash('noShow', 1b, 'noOpen', 1b)
      imageRF_parameterize_processing, p, guiSettings
    endif
  endif
   
  if type eq 'RFR' then begin
    pModel = FILEPATH('MyModel.rfr', /TMP)
    pModel = 'D:\temp\MyModel.rfr'
    if ~file_test(pModel) or newModel then begin
      p = hash()
      p['numberOfFeatures'] = sqrt(114)
      p['inputImage']  = hub_getTestImage('Hymap_Berlin-B_Image')
      p['inputLabels'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
      p['model'] = pModel
      guiSettings = Hash('noShow', 1b, 'noOpen', 1b)
      imageRF_parameterize_processing, p, guiSettings
    endif
  endif
  
  parameters = hash()
  parameters['model'] = pModel 
  R = imageRF_viewModel_getReport(parameters)
  R.saveHTML, /Show
  
  
end

pro imageRF_BackwardCompatibility_test
  pathUpdate = 'D:\Sandbox\Mod6\EnMAP-Box_Patches'
  FILE_DELETE, 'D:\Sandbox\Mod6\EnMAP-Box_Patches\imageRFDistribution', /RECURSIVE, /QUIET
  FILE_DELETE, 'D:\Sandbox\Mod6\EnMAP-Box_win64\EnMAP-Box\enmapProject\applications\imageRF', /RECURSIVE, /QUIET
  imageRF_make, distribution = pathUpdate 
  
  FILE_COPY, 'D:\Sandbox\Mod6\EnMAP-Box_Patches\imageRFDistribution\EnMAP-Box' $
           , 'D:\Sandbox\Mod6\EnMAP-Box_win64', /OVERWRITE, /RECURSIVE

end
