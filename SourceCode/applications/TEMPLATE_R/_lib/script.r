# import packages
require(rjson)

# read input parameters
jsonFile <- commandArgs(TRUE)
jsonString <- readLines(jsonFile)
p <- fromJSON(jsonString)
r <- list()

### user defined code ###
 
print(p$inputText)
r$outputText <- "Hello World from R"

#########################

# write output parameters
fileConnection  <- file(p$scriptOutputFilename)
writeLines(toJSON(r), fileConnection)
close(fileConnection)
