pro otbBandMath_event, event
    
  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  groupLeader = applicationInfo['groupLeader']
  otbBandMathGUI = otbBandMathGUI(GroupLeader=groupLeader)
  otbBandMathGUI.startMainGUI

end
