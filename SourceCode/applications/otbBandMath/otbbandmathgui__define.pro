function otbBandMathGUI::init, GroupLeader=groupLeader
  self.groupLeader = ptr_new(groupLeader)
  self.title = 'OTB Band Math' 
  self.otb = hubOTBWrapper(/SHOWCOMMANDS)
  self.oldVariables = hash()
  self.functionDescription = hash()
  return, 1b
end

pro otbBandMathGUI::startMainGUI, defaultExpression

  defaultExpression = isa(defaultExpression) ? defaultExpression : ' '
  (hubGUIHelper()).setFixedWidgetFont
  self.idsMainGUI = hash()
  self.tlbMainGUI = widget_base(TITLE=self.title, /COLUMN, GROUP_LEADER=*self.groupLeader, UVALUE=self)
  
  baseMain = widget_base(self.tlbMainGUI, /ROW, XPAD=0,YPAD=0,SPACE=1)
  baseLeft =  widget_base(baseMain, /COLUMN, XPAD=0,YPAD=0,SPACE=1)
  base = widget_base(baseLeft, /COLUMN, XPAD=1,YPAD=1,SPACE=1)
  label = widget_label(base, Value='Input')
  text = widget_text(base, SCR_XSIZE=545, /EDITABLE, YSIZE=5, VALUE=[defaultExpression],UVALUE=self, /WRAP, /ALL_EVENTS)
  (self.idsMainGUI)['input'] = text

  base = widget_base(baseLeft, /COLUMN, XPAD=1,YPAD=1,SPACE=1)
  label = widget_label(base, Value='Output/Info')
  text = widget_text(base, SCR_XSIZE=545, /EDITABLE, YSIZE=10, VALUE=[''],UVALUE=self, /FRAME, /WRAP, /SCROLL)
  (self.idsMainGUI)['output'] = text
  base = widget_base(baseLeft, /ROW, XPAD=0,YPAD=0,SPACE=0)
  
  xsize = 50
  ysize1 = 50
  ysize2 = 25

  subbase = widget_base(base, /COLUMN, XPAD=1,YPAD=0,SPACE=0)

  subbase = widget_base(base, /COLUMN, XPAD=1,YPAD=0,SPACE=0)
  button = widget_button(subbase, VALUE=' && ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' || ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' <= ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' >= ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' != ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' == ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' > ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' < ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' ? : ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)


  subbase = widget_base(base, /COLUMN, XPAD=0,YPAD=0,SPACE=0)
  subbase = widget_base(subbase, /ROW, XPAD=0,YPAD=0,SPACE=0)
  subbase2 = widget_base(subbase, ROW=5, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase2, VALUE='(', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE=',', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE=')', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE='7', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='8', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='9', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='4', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='5', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='6', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='1', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='2', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='3', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='0', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='.', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)

  subbase2 = widget_base(subbase, ROW=5, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase2, VALUE='^', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE=string(247b), UNAME='/', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE=string(215b), UNAME='*', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='-', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='+', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  
  subbase2 = widget_base(subbase, ROW=5, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase2, VALUE='Backspace', UNAME='', SCR_XSIZE=xsize*2, SCR_YSIZE=ysize2, UVALUE=self)
  (self.idsMainGUI)['Backspace'] = button
  button = widget_button(subbase2, VALUE='Clear', SCR_XSIZE=xsize*2, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='=', SCR_XSIZE=xsize*2, SCR_YSIZE=ysize1*3, UVALUE=self)
  
  base = widget_base(baseLeft, /ROW, XPAD=0,YPAD=0,SPACE=0)
  subbase = widget_base(base)
  subbase = widget_base(subbase, /COLUMN, XPAD=0,YPAD=0,SPACE=0, /ALIGN_LEFT, /BASE_ALIGN_LEFT)
  label = widget_label(subbase, VALUE='Use imXbY variable name for specifying the Ys band of the Xs image.')  
  label = widget_label(subbase, VALUE='E.g. use im1b2 for the second band of the first image.')
  
  baseRight = widget_base(baseMain, /COLUMN, XPAD=0,YPAD=1,SPACE=1)
  label = widget_label(baseRight, VALUE='Functions')
  geoBaseLeft = widget_info(/GEOMETRY, baseLeft)
  (self.idsMainGUI)['tree'] = widget_tree(baseRight, SCR_XSIZE=300, SCR_YSIZE=geoBaseLeft.scr_ysize-20, /FOLDER)
  self.createFunctionTree

  (hubGUIHelper()).centerTLB, self.tlbMainGUI
  widget_control, self.tlbMainGUI, /REALIZE
  widget_control, (self.idsMainGUI)['input'], /INPUT_FOCUS
  xmanager, 'otbBandMathMainGUI', self.tlbMainGUI, /NO_BLOCK, EVENT_HANDLER='hubObjCallback_event'

end

pro otbBandMathGUI::createFunctionTree

  ; define bitmap for function tree nodes
  bitmap = bytarr(16,16,3)+255b
  x = 5
  bitmap[x:-x-1,x:-x-1,*] = 0b
  bitmap[x+1:-x-2,x+1:-x-2,*] = 255b
 
 ; insert functions to the tree
  self.functionDescription = hash()

  root = (self.idsMainGUI)['tree']
  
;---------------------------
; parse imageMath menu files

  ; - main menus init
  menu = hubGUIMenu()
  mainMenuFilename = filepath('otbbandmath.men', ROOT=otbBandMath_getDirname(), SUBDIR='resource')
  menu.extendMenu, mainMenuFilename
  menu.createMenuTree, root, UVALUE=self, BITMAP=bitmap
end

function otbBandMathGUI::startVariableGUI, expression

  str = expression
  digitNoZero = '1|2|3|4|5|6|7|8|9'
  digit       = '0|'+digitNoZero
  maxNumber = 0
  while (pos = stregex(str, 'im('+digitNoZero+')('+digit+')?', LENGTH=length)) ne -1 do begin
    number = fix(strmid(str, pos+2, length-2))
    maxNumber >= number
    str = strmid(str, pos+length)
  endwhile
  
  if maxNumber eq 0 then begin
    return, dictionary('filenames', [])
  endif
 
  variableNames = 'im'+strtrim(['1':maxNumber],2)

  ; create widget program

  hubAMW_program, self.tlbMainGUI, Title=self.title+': Enter Variables'
  hubAMW_frame, Title='Input'
  tSize = 100
  foreach variableName, variableNames, i do begin
    if self.oldVariables.hasKey(variableName) then begin
      oldFilename = (self.oldVariables)[variableName]
    endif else begin
      oldFilename = !null
    endelse

    hubAMW_inputImageFilename, variableName, Title=variableName, TSize=tsize, Value=oldFilename
    
  endforeach
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, '$resultFilename$', Title='Result Image', TSize=tsize, Value='otbBandMathResult'
  result = hubAMW_manage()
  accept = result['accept']

  ; save current selection for the next calculation
  if accept then begin
    self.oldVariables = self.oldVariables + result
    filenames = strarr(maxNumber)
    foreach variableName, variableNames, i do begin
      filenames[i] = result[variableName]
      (self.oldVariables)[variableName] = filenames[i]
    endforeach
    return, dictionary('filenames', filenames, 'outputFilename', result['$resultFilename$'])
  endif else begin
    return, dictionary('filenames', [])
  endelse
end

pro otbBandMathGUI::handleEvent, event

  ; get input text information
  selection = widget_info((self.idsMainGUI)['input'], /TEXT_SELECT)
  selectionStart = selection[0]
  selectionLength = selection[1]

  if isa(event, 'widget_button') then begin
  
    ; get button information
    buttonUname = widget_info(event.id, /UNAME)
    widget_control, event.id, GET_VALUE=buttonValue
    widget_control, (self.idsMainGUI)['input'], GET_VALUE=textValue

  ; handle '='
    if buttonValue eq '=' then begin
      self.setOutputText, ''
      self.calculate
      return
    endif
  
  ; handle 'Backspace'
    if buttonValue eq 'Backspace' then begin
      ; if nothing is selected, select the previous character and recall the event handler
      if selectionLength eq 0 then begin
        if selectionStart eq 0 then begin
          return
        endif else begin
          widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=[(selectionStart-1)>0,1]
          self.handleEvent, event
          return
        endelse
        
      ; otherwise delete the whole selection
      endif else begin
        ; nothing to do, will be handled by the following code
      endelse
     
    endif

    ; set new text in dependence to the button value
    case buttonValue of
      'Backspace' : newText = ''
      'Clear' : newText = ''
      else : newText = buttonUname ne '' ? buttonUname : buttonValue
    endcase
    
    ; special handling for functions
    isFunction = strmid(newText, strlen(newText)-2,2) eq '()'
    if isFunction then begin
      newText = strmid(newText, 0, strlen(newText)-1)
    endif
    
    ; update input text
    
    use_text_select = buttonValue ne 'Clear'
    no_newline =  1b
    widget_control, (self.idsMainGUI)['input'], SET_VALUE=newText, USE_TEXT_SELECT=use_text_select, NO_NEWLINE=no_newline
    newSelection = [selectionStart+strlen(newText), 0]
    widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=newSelection
    
    ; set input focus to input text
    widget_control, (self.idsMainGUI)['input'], /INPUT_FOCUS
    
    ; clean output text
;    widget_control, (self.idsMainGUI)['output'], SET_VALUE=''
    return
  endif
  
  ; handle character inputs
  if isa(event, 'widget_text_sel') then return ; ignore Delete key
  if isa(event, 'widget_text_del') then return ; ignore Backspace key
  if isa(event, 'widget_text_str') then return ; ignore Copy&Paste

  if isa(event, 'widget_text_ch') then begin
    if event.ch eq 61 then begin
      widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=[selectionStart-1,1]
      widget_control, (self.idsMainGUI)['input'], SET_VALUE='', /USE_TEXT_SELECT, /NO_NEWLINE
      self.calculate
    endif
    return
  endif
  
  ; handle tree event
  if isa(event, 'widget_tree_sel') then begin
    if widget_info(event.id, /TREE_FOLDER) then return
    ; insert function into input text widget
    if event.clicks eq 2 then begin
      functionName = widget_info(event.id, /UNAME)
      ; delete prefix
      if strcmp(functionName, 'imageMathFunction_', 18) then begin
        functionName = strmid(functionName, 18)
      endif
      newText = functionName+'('
      widget_control, (self.idsMainGUI)['input'], SET_VALUE=newText, /USE_TEXT_SELECT, /NO_NEWLINE
      newSelection = [selectionStart+strlen(newText), 0]
      widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=newSelection
      widget_control, (self.idsMainGUI)['input'], /INPUT_FOCUS
    endif
    ; show function description inside console text widget
    if event.clicks eq 1 then begin
      self.setOutputText, ''
    endif
    return
  endif
  if isa(event, 'widget_tree_expand') then begin
    return
  endif
  help, event

end

pro otbBandMathGUI::setInputText, inputText
  widget_control, (self.idsMainGUI)['input'], SET_VALUE=inputText, /NO_NEWLINE
end

function otbBandMathGUI::getInputText, Selection=selection
  
  widget_control, (self.idsMainGUI)['input'], GET_VALUE=inputText
  if keyword_set(selection) then begin
    selection = widget_info((self.idsMainGUI)['input'], /TEXT_SELECT)
    if selection[1] gt 0 then begin
      inputText = strmid(inputText, selection[0], selection[1])
    endif
  endif
  return, inputText
end

pro otbBandMathGUI::setOutputText, outputText
  widget_control, (self.idsMainGUI)['output'], SET_VALUE=outputText
end

function otbBandMathGUI::getOutputText
  widget_control, (self.idsMainGUI)['output'], GET_VALUE=outputText
  return, outputText
end

pro otbBandMathGUI::setVariables, variables
  self.calculator.setVariables, variables
end

pro otbBandMathGUI::calculate
  
  ; get user input text
  inputText = self.getInputText(/Selection)
  infixString = strtrim(strjoin(inputText, ' '),2)

  ; - if input text is empty, then return
  if infixString eq '' then begin
    self.setOutputText, ''
    return
  endif

  ; todo: parse for used variables

  ; get variables if not explicitly provided
  guiResult = self.startVariableGUI(infixString)
  filenames = guiResult.filenames
  if ~isa(filenames) then return
  outputFilename = guiResult.outputFilename

  IL  = '"' + filenames +'"';?&skipcarto=true'
  OUT = outputFilename+'.hdr?&gdal:of:ENVI'
  EXP = infixString
  
  progressBar = hubProgressBar(Title=self.title, GroupLeader=*self.groupLeader, Info='evaluating expression')
  otb = hubOTBWrapper(/SHOWCOMMANDS)
  catch, errorStatus
  if (errorStatus ne 0) then begin
    catch, /CANCEL
    self.setOutputText, !ERROR_STATE.MSG
    obj_destroy, progressBar
    return
  endif
  otb.BandMath,IL=IL, OUT=OUT, EXP=EXP
  catch, /CANCEL
  self.setOutputText, 'Calculation done.'
  obj_destroy, progressBar
  hubProEnvHelper.openImage, outputFilename
end

pro otbBandMathGUI::cleanup
  if widget_info(self.tlbMainGUI, /VALID_ID) then begin
    widget_control, /DESTROY, self.tlbMainGUI 
  endif  
end

pro otbBandMathGUI__define
  struct = {otbBandMathGUI $
    , inherits IDL_Object $
    , groupLeader : ptr_new() $
    , otb : obj_new() $
    , tlbMainGUI : 0l $
    , tlbVariableGUI : 0l $
    , idsMainGUI : hash() $
    , idsVariableGUI : hash() $
    , infoVariableGUI : hash() $
    , oldVariables : hash() $
    , functionDescription : hash() $
    , title : '' $
  }
end

pro test_otbBandMathGUI
  enmapBox
  enmapBox_openImage, hub_getTestImage('Hymap_Berlin-A_Image')
  enmapBox_openImage, hub_getTestImage('Hymap_Berlin-B_Image')
  otbBandMathGUI = otbBandMathGUI(GroupLeader=groupLeader)
  otbBandMathGUI.startMainGUI, 'im1b40 + im2b40'
end