;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

pro SyntMIX_processing, widget1, widget2, settings
  

  parametersLibMix = hash()
 
  parametersLibMix['outputMode'] = 'SL'
  parametersLibMix['inputSpecLib'] = widget1['speclib']
  parametersLibMix['inputLabels']  = widget1['speclibLabels']
  
  

  fractions = hubLibMix_getFractions(widget2['numberOfSteps'])
;  targetList = list()
  
    
  for i=1, n_elements(widget2['classNames'])-1 do begin
    if widget2['class'+strcompress(i,/REMOVE_ALL)] eq 0 then begin
      mix = hubLibMix_mixDescription()
      ;targetList.add, i
      ;parametersLibMix['outputFile'] = ((widget1['outputDirectory']+'firstMix')+strcompress(i,/REMOVE_ALL))[0]
      ;
      ;path = ((widget1['outputDirectory']+'TG_')+strcompress((widget2['classNames'])[i],/REMOVE_ALL))[0]
      basename = 'TG_' + strcompress((widget2['classNames'])[i],/REMOVE_ALL)
      path = SyntMIX_classname2filename(widget1['outputDirectory'], basename)
      ;replace invalid characters
      
      parametersLibMix['outputFile'] = path
      
      
      parametersLibMix['fractionLabelClasses'] = i
      for j=1, n_elements(widget2['classNames'])-1 do begin
        if i eq j then continue
        if widget2['class'+strcompress(j,/REMOVE_ALL)] ne 2 then mix.addMixtures, j,i, [fractions]
      endfor
      parametersLibMix['mixDescription'] = mix
      
      hubLibMix_processing, parametersLibMix
      mix.cleanup
      if widget2.haskey('snrValue') then begin
        parameters = hash()
        parameters['inputSL'] = parametersLibMix['outputFile']
        parameters['outputSL'] = parametersLibMix['outputFile']+'_noisy'
        parameters['SNR'] = widget2['snrValue']
        hubLibMix_addNoise_processing, parameters
      endif
      
    endif    
  endfor
  
  
end

;+
; :Hidden:
;-
pro test_SyntMIX_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = SyntMIX_getSettings()
  
  SyntMIX_processing, parameters, settings
  print, result

end  
