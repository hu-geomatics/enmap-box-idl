;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `SyntMIX_processing` function to this argument. 
;      
;    settings : in, optional, type=hash
;
;
;-
pro SyntMIX_showReport, reportInfo, settings

  report = hubReport(Title= 'synthMix-SVR Report')
  
  report.addHeading, 'General Information', 2
  report.addParagraph, 'Output Directory: ' + strcompress(reportInfo['wd'])
  report.addParagraph, 'Spectral Library: ' + strcompress(reportInfo['spec'])
  report.addParagraph, 'Labels: ' + strcompress(reportInfo['lab'])
  report.addParagraph, 'Image: ' + strcompress(reportInfo['img'])
  
  report.addHeading, 'Spectral Library Results' 
  
  report.addHeading, 'Chosen target classes: ', 3
  report.addParagraph, STRING((reportInfo['target']).toarray())
  report.addHeading, 'Chosen background classes: ', 3
  report.addParagraph, reportInfo['background'].toarray()
  report.addHeading, 'Mixing intervall: ', 3
  report.addParagraph, strcompress(reportInfo['mixingIntervall']) + ' (e.g. 1 = 50%, 2 = 33% 66%, ... , 9 = 10% 20% ... 90%)'
  report.addHeading, 'Total amount of spectra: ', 3
  report.addParagraph, strcompress(reportInfo['totalSpectra']) + ' spectra in ' + strcompress(reportInfo['totalLibaries']) + ' Libraries'
  report.addHeading, 'Added noise (SNR value): ', 3
  report.addParagraph, reportInfo['noise']

  report.addHeading, 'SVR model information'
  report.addHeading, 'Grid search results: ', 3
  
  table = hubReportTable()
  table.addColumn, reportInfo['target'], name = 'Target Classes'
  table.addColumn,  reportInfo['c'], NAME= 'C', FORMATSTRING='(F10.4)'
  table.addColumn, reportInfo['g'], NAME= 'G', FORMATSTRING='(F10.4)'
  table.addColumn,  reportInfo['e'], NAME= 'E', FORMATSTRING='(F10.4)'
  table.addColumn,  reportInfo['cv'], NAME= 'MAE', FORMATSTRING='(F10.4)'
  
  report.addHTML, table.getHTMLTable(ATTRIBUTESTABLE='border="1"')


  report.addHeading, 'Post-Processing'
  report.addHeading, 'Negative and super positive values set to 0 and 1: ', 3
  report.addParagraph, reportInfo['stretch']
  report.addHeading, 'Sum to unity: ', 3
  report.addParagraph, reportInfo['unity']
    
  report.saveHTML, /Show

end
