;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

pro validate_spectra, resultHash, userInformation=userInformation
   allProfiles=0UL
   nT=0
   parameters = hash()
   parameters['inputLabels'] = userInformation['speclibLabels']
   stats = hubLibMix_getClassLabelOccurrence(parameters)
   fractions = hubLibMix_getFractions(resultHash['numberOfSteps'])
   for i=1, n_elements(userInformation['classNames'])-1 do begin
       OriginalProfiles= 0UL
       if resultHash['class'+strcompress(i,/REMOVE_ALL)] eq 0 then begin
       nT++
       mix = hubLibMix_mixDescription()
       OriginalProfiles+= stats[i].OCCURRENCE
       for j=1, n_elements(userInformation['classNames'])-1 do begin
         if i eq j then continue
         if resultHash['class'+strcompress(j,/REMOVE_ALL)] ne 2 then begin
           mix.addMixtures, j,i, [fractions]
           OriginalProfiles+= stats[j].OCCURRENCE
         endif
       endfor 
     
       
         nProfiles = hubLibMix_getFinalNumberOfProfiles(stats, mix, 'SL')   
         allProfiles+= nProfiles.NPROFILES + OriginalProfiles
         mix.cleanup
      endif
   endfor
   ;nT =
    
   ok = dialog_message('You will create ' + strcompress(nT,/REMOVE_ALL) + ' synthetically mixed training data sets with a total of ' + strcompress(allProfiles,/REMOVE_ALL) + $
     ' spectra.',/INFORMATION)
end


function check_SyntMIX_getParameters_Widget2, resultHash, Message=msg
  
  target = list()
  background = list()
  nClasses = resultHash.haskey('snrValue') ? n_elements(resulthash)-3 : n_elements(resulthash)-2
  for i=1, nClasses do begin
    if resultHash['class'+strcompress(i,/REMOVE_ALL)] eq 0 then begin
      target.add, i
    endif
    if resultHash['class'+strcompress(i,/REMOVE_ALL)] eq 1 then begin
      background.add, i
    endif
  endfor
  isConsistent = n_elements(target) + n_elements(background) lt 2 OR n_elements(target) eq 0 ? 0 : 1
  
  msg = 'Inconsistent input. Select at least two target classes or one target and one background class.'

  return, isConsistent

end
function SyntMIX_getParameters_Widget2, settings, widget1
  
  labelImage = hubIOImgInputImage(widget1['speclibLabels'])
  classNames = labelimage.getMeta('class names')
  groupLeader = settings.hubGetValue('groupLeader')
  classes = strarr(n_elements(classNames))
  for i=1, n_elements(classNames)-1 do classes[i-1]= 'class'+strcompress(i,/REMOVE_ALL)

 
  hubAMW_program , groupLeader, Title = 'synthMix-SVR: Specify synthMix settings'
  hubAMW_label, 'Select target and background categories'
  tooltip = ['Choose nice combinations']
  list = list('Target','Background','Ignore')
  
  for i=1, n_elements(classNames)-1 do begin 
    hubAMW_label, classNames[i]
    hubAMW_checklist, 'class'+strcompress(i,/Remove_all), Title='', List=list, Value=2, Tooltip=tooltip,OPTIONAL=optional
  endfor
  
    
   hubAMW_frame, Title=''
   hubAMW_label, ['Specify mixing interval' $
                  ,'e.g. 1 = 50%, 2 = 33% 66%, ... , 9 = 10% 20% ... 90%']
                  
   hubAMW_parameter, 'numberOfSteps', Title='Steps' $
                   , SIZE=2 $
                   , VALUE = 4 , /Integer $
                   , ISGE=1, ISLT=20

   hubAMW_frame, Title=''   
   hubAMW_label, 'Add noise to spectral data'
   hubAMW_parameter, 'snrValue', Title='Signal to noise factor', Value=200, /Integer ,TSize=300, optional =2  
   hubAMW_button, Title='Evaluate' $
                , USERINFORMATION = hash('classNames',classNames)+widget1 $
                , TOOLTIP = 'Calculate number of mixed spectra' $
                , EVENTHANDLER='validate_spectra' $
                , RESULTHASHKEYS=['numberOfSteps',classes] $
                , /BAR 
             

   result = hubAMW_manage(ConsistencyCheckFunction='check_SyntMIX_getParameters_Widget2',/FLAT)
     if result['accept'] then begin
       parameters = result
       parameters['classNames'] = classNames 
     endif else begin
       parameters = !null
     endelse

 return, parameters

end

;+
; :Hidden:
;-
pro test_SyntMIX_getParameters_Widget2

  ; test your routine
  settings = SyntMIX_getSettings()
  parameters = SyntMIX_getParameters_Widget2(settings)
  print, parameters

end  
