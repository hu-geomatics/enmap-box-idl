;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function SyntMIX_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'This is '+settings['title']

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputIMageFilename, 'Spectral library', Title='Spectral Library' 
  hubAMW_inputIMageFilename, 'Labels', Title='Labels' 
    ; insert your widgets

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Output'
    ; insert your widgets
  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b

  parameters = hubAMW_manage()

  
  if parameters['accept'] then begin
    ; if required, perform some additional changes on the parameters hash
    
  endif
  return, parameters
end

;+
; :Hidden:
;-
pro test_SyntMIX_getParameters

  ; test your routine
  settings = SyntMIX_getSettings()
  parameters = SyntMIX_getParameters(settings)
  print, parameters

end  
