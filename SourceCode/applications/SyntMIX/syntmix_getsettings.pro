;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('SyntMIX.conf', ROOT=SyntMIX_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, SyntMIX_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function SyntMIX_getSettings
  ;read settings from configuration file
  settingsFilename = filepath(strlowcase('syntmix.conf'), ROOT=SyntMIX_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName'];+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_SyntMIX_getSettings

  print, SyntMIX_getSettings()

end
