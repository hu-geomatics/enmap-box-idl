;+
; :Author: <author name> (<email>)
;-

function SyntMIX_getDirname
  
  result = filepath('SyntMIX', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_SyntMIX_getDirname

  print, SyntMIX_getDirname()
  print, SyntMIX_getDirname(/SourceCode)

end
