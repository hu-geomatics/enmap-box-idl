;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

function check_SyntMIX_getParameters_Widget3, resultHash, msg
  
  ;TODO Check if anything is checked
  msg = 'Too few spectra'
  return, msg

end
function SyntMIX_getParameters_Widget3, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  
  hubAMW_program, Title='SVR Parameter Selection', groupLeader
  hubAMW_label, 'Default Values for the SVR grid search are used'
  hubAMW_frame, Title='Parameters', /Advanced
  hubAMW_label, 'Gaussian RBF Kernel Parameter Range'
  hubAMW_subframe, /Row
  hubAMW_parameter, 'minG', Title='   min(g)', Size=10, /Float, IsGT=0, Value=0.01
  hubAMW_parameter, 'maxG', Title=' max(g)', Size=10, /Float, IsGT=0, Value=1000.
  hubAMW_parameter, 'multiplierG', Title=' multiplier(g)', Size=10, /Float, IsGT=0, Value=10.
  hubAMW_subframe, /Column
  hubAMW_label, 'Regularization Parameter Range'
  hubAMW_subframe, /Row
  hubAMW_parameter, 'minC', Title='   min(C)', Size=10, /Float, IsGT=0, Value=0.1
  hubAMW_parameter, 'maxC', Title=' max(C)', Size=10, /Float, IsGT=0, Value=1000.
  hubAMW_parameter, 'multiplierC', Title=' multiplier(C)', Size=10, /Float, IsGT=0, Value=10.
  hubAMW_subframe, /Column
  hubAMW_label, 'Cross Validation'
  hubAMW_parameter, 'numberOfFolds',      Title='   number of folds                          ', Size=3, /Integer, IsGE=3, Value=3
  hubAMW_label, 'Termination criterion'
  hubAMW_parameter, 'stopGridSearch',     Title='   termination criterion for grid search    ', Size=10, /Float, IsGT=0, Value=0.1
  hubAMW_parameter, 'stopTraining',       Title='   termination criterion for final training ', Size=10, /Float, IsGT=0, Value=0.001
  hubAMW_label, 'Epsilon insensitive Loss Function Parameter'
  hubAMW_subframe, 'lossSelection', Title='automatic search', /ROW, /SetButton
  hubAMW_subframe, 'lossSelection', Title='user defined', /ROW
  hubAMW_parameter, 'lossUser',     Title='epsilon', Size=10, /Float, IsGE=0, Value='0.'
  hubAMW_subframe, /Column

 
 result = hubAMW_manage(/FLAT);CONSISTENCYCHECKFUNCTION='check_SyntMIX_getParameters_Widget2',/FLAT)
 
 if result['accept'] then begin
   parameters = result

 endif else begin
   parameters = !null
 endelse

 return, parameters

end

;+
; :Hidden:
;-
pro test_SyntMIX_getParameters_Widget3

  ; test your routine
  settings = SyntMIX_getSettings()
  parameters = SyntMIX_getParameters_Widget3(settings)
  print, parameters

end  
