;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`SyntMIX_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    
;
;-
function SyntMIX_postprocessing, widget1, widget2, widget4, parameters, i, Stretch = stretch, Unity = unity
  

if isa(stretch) then begin
  filenameResult = (widget1['outputDirectory']+'Final_Truncated_Stack')[0]
  filenameInput = (widget1['outputDirectory']+'Final_Fraction_Stack')[0]
  result = imageMath_evaluate('(Final_Truncated_Stack > 0) < 1', hash('Final_Truncated_Stack', imageMath_variable(filenameInput)), filenameResult)
  hubIOImg_copyMeta, filenameInput, filenameResult, /CopySpatialInformation
  return, filenameResult
endif


if isa(unity) then begin
  filenameResult = (widget1['outputDirectory']+'Final_Unity_Stack')[0]
  filenameInput = (widget1['outputDirectory']+'Final_Truncated_Stack')[0]
  result = imageMath_evaluate('Final_Unity_Stack / spectralSum(Final_Unity_Stack)', hash('Final_Unity_Stack', imageMath_variable(filenameInput)), filenameResult)
  hubIOImg_copyMeta, filenameInput, filenameResult, /CopySpatialInformation
  return, filenameResult
endif


end

;+
; :Hidden:
;-
pro test_SyntMIX_postprocessing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition

  
  SyntMIX_postprocessing, parameters
  print, result

end  