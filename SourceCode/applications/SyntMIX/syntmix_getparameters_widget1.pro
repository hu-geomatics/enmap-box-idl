;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

function check_SyntMIX_getParameters_Widget1, resultHash, msg
  
  ;TODO check nr of bands consistency
  
  msg = 'Too few spectra'
  return, msg

end

function SyntMIX_getParameters_Widget1, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  defaultValues = hub_getAppState('SyntMIX','stateHash_SyntMIX',default=hash())
 
  
  tsize = 120
  hubAMW_program , groupLeader, Title = 'synthMix-SVR: Manage Data Input and Output'
  

  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputDirectoryName, 'outputDirectory', TITLE='Specify folder',VALUE=defaultValues.hubGetValue('outputDirectory')
  hubAMW_frame ,   Title =  'Input Spectral Library'
  hubAMW_inputSampleSet, 'inputSampleSet', title='Spectral library', /CLASSIFICATION $
    , ReferenceTitle='Labels ' $
    , Value = defaultValues.hubGetValue('speclib') $
    , ReferenceValue = defaultValues.hubGetValue('speclibLabels') $
    , TSize=tsize
    
  hubAMW_frame ,   Title =  'Input Image'
  hubAMW_inputImageFilename, 'Image', title='Image',TSIZE=tsize, VALUE=defaultValues.hubGetValue('Image')

;  hubAMW_frame ,   Title =  'Output'
;
;  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b

  result = hubAMW_manage(/FLAT);CONSISTENCYCHECKFUNCTION='check_SyntMIX_getParameters_Widget1',/FLAT)
 
  if result['accept'] then begin
    parameters = result
    parameters['speclibLabels'] = parameters['inputSampleSet_labelFilename']
    parameters['speclib'] = parameters['inputSampleSet_featureFilename']

 

  endif else begin
    parameters = !null
  endelse
  return, parameters

end

;+
; :Hidden:
;-
pro test_SyntMIX_getParameters_Widget1

  ; test your routine
  settings = SyntMIX_getSettings()
  parameters = SyntMIX_getParameters_Widget1(settings)
  print, parameters

end  
