;+
; :Description:
;    Converts a class name ( = any string) into a valid file name that can be used to describe a file path.
;
; :Params:
;    classname, in, required, type=string
;
;
;
; :Author: geo_beja
;-
function SyntMIX_classname2filename, dirname, classname
  reg = '[<> ]' ;regular expression to find invalid characters
  bn = strjoin(strsplit(classname, reg, /EXTRACT, /REGEX),'_')
  return, FILEPATH(bn, ROOT_DIR=dirname)
end
