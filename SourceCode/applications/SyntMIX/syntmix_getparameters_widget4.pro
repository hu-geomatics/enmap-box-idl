;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-

function check_SyntMIX_getParameters_Widget4, resultHash, msg
  
  ;TODO Check if anything is checked
  msg = 'Too few spectra'
  return, msg

end
function SyntMIX_getParameters_Widget4, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  list = list('yes','no')
  
  hubAMW_program, Title='synthMix-SVR: Post-Processing', groupLeader
  hubAMW_subframe, 'postProcessing', Title='No post-processing', /Row, /SetButton 
  hubAMW_subframe, 'postProcessing', Title='Set fractions to 0 and 1', /Row 
  hubAMW_checklist, 'nounity', Title='Normalize set of fraction maps to unity', list = list ,TSize=300 , value=1
 
 result = hubAMW_manage(/FLAT);CONSISTENCYCHECKFUNCTION='check_SyntMIX_getParameters_Widget4',/FLAT)
 
 if result['accept'] then begin
   parameters = result

 endif else begin
   parameters = !null
 endelse

 return, parameters

end

;+
; :Hidden:
;-
pro test_SyntMIX_getParameters_Widget4

  ; test your routine
  settings = SyntMIX_getSettings()
  parameters = SyntMIX_getParameters_Widget4(settings)
  print, parameters

end  
