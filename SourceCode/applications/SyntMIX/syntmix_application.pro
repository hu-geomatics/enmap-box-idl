;+
; :Author: Matthias Held (matthias.held@geo.hu-berlin.de); Marcel Schwieder (marcel.schwieder@geo.hu-berlin.de)
;-



;+
; :Description:
;    This is the application main procedure. It queries user input (see `SyntMIX_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `SyntMIX_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `SyntMIX_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro SyntMIX_application, applicationInfo
   
  
  ; get global settings for this application
  settings = SyntMIX_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
 
  defaultValues = hub_getAppState('SyntMIX','stateHash_SyntMIX',default=hash())
 
  widget1 = SyntMIX_getParameters_Widget1(settings)
 
 
  
  if isa(widget1) then begin

    applyImage = hubIOImgInputImage(widget1['Image'])
    samples =  applyImage.getMeta('samples')
    lines =  applyImage.getMeta('lines')
    hub_setAppState, 'SyntMIX', 'stateHash_SyntMIX', widget1
    widget2 = SyntMIX_getParameters_Widget2(settings, widget1) 
    if isa(widget2) then begin

        target = list()
        background = list() 
        for i=1, n_elements(widget2['classNames'])-1 do begin
          if widget2['class'+strcompress(i,/REMOVE_ALL)] eq 0 then begin
            target.add, i
          endif
          if widget2['class'+strcompress(i,/REMOVE_ALL)] eq 1 then begin
            background.add, i
          endif  
        endfor
          nT = n_elements(target)
          nProfiles = 0
          targetNamesList = list()
          backgroundNamesList = list()
          gammaList = list()
          CList = list()
          epsilonList = list()
          maeList = list()
        
          SyntMIX_processing, widget1, widget2, settings
          finalStack = strarr(1,nT)
          for i=1, n_elements(widget2['classNames'])-1 do begin
            if widget2['class'+strcompress(i,/REMOVE_ALL)] eq 0 then begin
              
              
              ;speclib = hubIOImgInputImage((widget1['outputDirectory']+'TG_'+strcompress((widget2['classNames'])[i],/REMOVE_ALL))[0])
              basename = 'TG_'+strcompress((widget2['classNames'])[i],/REMOVE_ALL)
              path =  + SyntMIX_classname2filename(widget1['outputDirectory'], basename)
              
              speclib = hubIOImgInputImage(path)
              nProfiles = nProfiles + speclib.getMeta('lines')
              targetNamesList.add, (widget2['classNames'])[i]
            endif
            if widget2['class'+strcompress(i,/REMOVE_ALL)] eq 1 then begin
              backgroundNamesList.add, (widget2['classNames'])[i]
            endif 
          
          endfor
         
            widget4 = SyntMIX_getParameters_Widget4(settings)
            if isa(widget4) then begin
              widget3 = SyntMIX_getParameters_Widget3(settings)
        
              if isa(widget3) then begin
                j=0
                for i=1, n_elements(widget2['classNames'])-1 do begin
                  if widget2['class'+strcompress(i,/REMOVE_ALL)] eq 0 then begin
                     
                     parameters = widget3
                     parameters['modelFilename'] = SyntMIX_classname2filename(widget1['outputDirectory'],'svrModel_'+strcompress((widget2['classNames'])[i],/REMOVE_ALL)+'.svr')
                     parameters['svmType'] = 'svr'
                     parameters['labelFilename'] = SyntMIX_classname2filename(widget1['outputDirectory'],'TG_'+strcompress((widget2['classNames'])[i],/REMOVE_ALL)+'_fracClass'+strcompress(i,/REMOVE_ALL))
                     parameters['featureFilename'] = ~widget2.haskey('snrValue') $ 
                                                     ? SyntMIX_classname2filename(widget1['outputDirectory'], 'TG_'+strcompress((widget2['classNames'])[i],/REMOVE_ALL)) $
                                                     : SyntMIX_classname2filename(widget1['outputDirectory'], 'TG_'+strcompress((widget2['classNames'])[i],/REMOVE_ALL)+'_noisy')
    
                     imageSVM_parameterize_processing, parameters, Title=title, GroupLeader=groupLeader
                       
                       svrModel = imageSVM_loadModel( parameters['modelFilename'], SELECTEDPARAMETERPERFORMANCE=value)
                       maeList.add, (value['performance'])['meanAbsoluteError']
                       epsilonList.add, value['loss']
                       CList.add, value['c']
                       gammaList.add, value['g']
                     
                     parameters['featureFilename'] = widget1['Image']
                     parameters['outputLabelFilename'] = SyntMIX_classname2filename(widget1['outputDirectory'],strcompress((widget2['classNames'])[i],/REMOVE_ALL)+'_fraction')
                     imageSVM_apply_processing, parameters, Title=title, GroupLeader=groupLeader
                 
                       
                     finalStack[j] = SyntMIX_classname2filename(widget1['outputDirectory'],strcompress((widget2['classNames'])[i],/REMOVE_ALL)+'_fraction')
                     
                     j++ ; j=j+1
                   endif ;svm
                 endfor
                
              ; Write and stack the final fraction stack
                  parameters['inputImages'] = finalstack
                  parameters['outputImage'] = (widget1['outputDirectory']+string('Final_Fraction_Stack'))[0]
                    
                  hubApp_Stacking_processing, parameters
                    
                  if widget4['postProcessing'] eq 1 then begin
                     stretchedImage = SyntMIX_postprocessing(widget1, widget2, widget4, parameters, i, /Stretch)
                     if widget4['nounity'] eq 0 then begin
                        unityStack = SyntMIX_postprocessing(widget1, widget2, widget4, parameters, i,/Unity)
                     endif
                  endif
                                   
                
                
                ; Output report information
                ; SVR model information
                reportInfo = hash()
                      
                ;General information
                reportInfo['wd'] = widget1['outputDirectory']
                reportInfo['spec'] = widget1['speclib']
                reportInfo['lab'] = widget1['speclibLabels']
                reportInfo['img'] = widget1['Image']

                ; Spectral library results
                reportInfo['target'] = targetNamesList
                
                reportInfo['background'] = n_elements(backgroundNamesList) gt 0 ? backgroundNamesList : list('None')
                
                reportInfo['mixingIntervall'] = widget2['numberOfSteps']
                reportInfo['totalSpectra'] = nProfiles
                reportInfo['totalLibaries'] = nT
                
                ; Noise
                reportInfo['noise'] = widget2.haskey('snrValue')  ? widget2['snrValue'] : 'None'
            
                ; grid-search meta info
           
                reportInfo['cv'] = maeList
                reportInfo['c'] = CList
                reportInfo['e'] = epsilonList
                reportInfo['g'] = gammaList
             
                ; Post-Processing
                reportInfo['stretch'] = widget4['postProcessing'] eq 1 ? 'Yes' : 'No' 
                If widget4.haskey('nounity') then begin
                 reportInfo['unity'] = widget4['nounity'] eq 0 ? 'Yes' : 'No'
                endif else begin
                 reportInfo['unity'] = 'No'
                endelse
                
                syntmix_showReport, reportInfo, settings  
              
              endif ;widget3  
            endif ;widget4   
          endif
        endif
    
  
end

;+
; :Hidden:
;-
pro test_SyntMIX_application
  applicationInfo = Hash()
  SyntMIX_application, applicationInfo

end  
