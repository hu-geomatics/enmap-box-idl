pro SyntMix_loadTestData_event, event

  @huberrorcatch
 
  dirname = filepath('', root_dir=SyntMix_getDirname(), SUBDIRECTORY=['resource','testData'])
  filenames = hub_getDirectoryImageFilenames(dirname)
  foreach filename, filenames do begin
    enmapBox_openImage, filename
  endforeach

end