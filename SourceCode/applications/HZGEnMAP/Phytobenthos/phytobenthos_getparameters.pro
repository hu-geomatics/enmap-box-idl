;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, required, type=hash
;
;-
function Phytobenthos_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'Phytobenthos Index V. 1.0'
 
 hubAMW_frame 
  hubAMW_subframe , /row
   hubAMW_button,  Title=' About ', EventHandler='phytobenthos_button1'
   hubAMW_button,  Title='Contact', EventHandler='phytobenthos_button2'

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputImageFilename, 'inputImage', Title='Image'
    ; insert your widgets

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputFilename, 'outputImage', Title='PHI Image', value='phyto'
    ; insert your widgets
  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b

  parameters = hubAMW_manage()

  
  if parameters['accept'] then begin
    ; if required, perform some additional changes on the parameters hash
    
  endif
  return, parameters
end

;+
; :Hidden:
;-
pro test_Phytobenthos_getParameters

  ; test your routine
  settings = Phytobenthos_getSettings()
  parameters = Phytobenthos_getParameters(settings)
  print, parameters

end  
