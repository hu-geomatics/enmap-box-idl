;+
; :Author: <Ulrike Kleeberg> (<ulrike.kleeberg@hzg.de>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`Phytobenthos_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
function Phytobenthos_processing, parameters, settings

  ;Parameters: Input and output Image
  
  tileLines=100
  
  wavebd1= 635
  wavebd2= 750
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  outputImage = hubIOImgOutputImage(parameters['outputImage'])
  
 
  result = hash()
  
 ;;======= Wavelengths
   
myBandnames= 'PHI - Foster&Jesus'

   outputImage.setMeta, 'band names', myBandnames

phytoB1 = inputimage.locateWavelength(wavebd1)
phytoB2 = inputimage.locateWavelength(wavebd2)




inputimage.initReader, tilelines, /TILEPROCESSING, /SLICE, $
     SubsetbandPositions=[phytoB1,phytoB2], Datatype='float'
      
     
  
outputImage.copyMeta, inputimage, /COPYSPATIALINFORMATION  
  outputImage.initWriter, inputimage.getWriterSettings(SetBands=1)
  
  
;; Phytobenthos calculation

 ;row_counter = N_ELEMENTS(data[1,*])
  ;  phyto= MAKE_ARRAY(exportsize,row_counter) 
    
 ;   idx_counter = 0
 while ~inputImage.tileProcessingDone() do begin
    data = inputimage.getData()
    
      phytoValues= (data[1,*]-data[0,*]) / (data[1,*]+data[0,*])             
      outputImage.writeData, phytoValues
 endwhile
  
;cleanup objects
 inputImage.cleanup
 outputImage.cleanup
 
    
; store results to be reported inside a hash
  result = hash(); empty hash
  result += parameters.hubGetSubHash(['inputImage','outputImage'])
  return, result

end

;+
; :Hidden:
;-
pro test_Phytobenthos_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = Phytobenthos_getSettings()
  
  result = Phytobenthos_processing(parameters, settings)
  print, result

end  
