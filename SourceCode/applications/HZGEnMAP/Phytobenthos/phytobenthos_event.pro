;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `Phytobenthos_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro Phytobenthos_event, event
  
  ; set up a default error handler
  
  @huberrorcatch
  
  ; query information about the application (which is stored inside menu button)
  ;   - note that menu buttons are defined inside the menu file '.\Phytobenthos\_resource\enmap.men'
  ;   - the following information is returned as a hash:
  ;       applicationInfo['name'] = 'Phytobenthos Application'
  ;       applicationInfo['argument] = 'Phytobenthos_ARGUMENT'
  ;       applicationInfo['groupLeader'] = <EnMAP-Box top level base>
  
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  
  ; call the application main procedure
  ; if required, provide menu button information to it
 
  Phytobenthos_application, applicationInfo
  
end
