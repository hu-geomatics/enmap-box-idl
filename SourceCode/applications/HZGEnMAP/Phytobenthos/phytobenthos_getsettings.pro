;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('Phytobenthos.conf', ROOT=Phytobenthos_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, Phytobenthos_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function Phytobenthos_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('phytobenthos.conf', ROOT=Phytobenthos_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_Phytobenthos_getSettings

  print, Phytobenthos_getSettings()

end
