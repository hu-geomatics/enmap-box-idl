;+
; :Author: <author name> (<email>)
;-

function Phytobenthos_getDirname
  
  result = filepath('Phytobenthos', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_Phytobenthos_getDirname

  print, Phytobenthos_getDirname()
  print, Phytobenthos_getDirname(/SourceCode)

end
