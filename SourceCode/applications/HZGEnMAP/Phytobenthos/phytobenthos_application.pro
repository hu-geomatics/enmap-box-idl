;+
; :Author: <Ulrike Kleeberg> (<ulrike.kleeber@hzg.de>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `Phytobenthos_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `Phytobenthos_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `Phytobenthos_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro Phytobenthos_application, applicationInfo
  
  ; get global settings for this application
  settings = Phytobenthos_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = Phytobenthos_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = Phytobenthos_processing(parameters, settings)
    
;    if parameters['showReport'] then begin
;      Phytobenthos_showReport, reportInfo, settings
;    endif
;  
  endif
  
end

;+
; :Hidden:
;-
pro test_Phytobenthos_application
  applicationInfo = Hash()
  Phytobenthos_application, applicationInfo

end  
