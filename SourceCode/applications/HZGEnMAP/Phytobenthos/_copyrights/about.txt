PI � short description

The module Phytobenthos Index (PI) is a modified version of the NDVI [1] [2]. He is specifically designed for benthic diatoms, a dominant species on estuarine intertidal flats. The index uses the reflectance at 635 nm associated with chlorophyll c, a diagnostic absorption feature for this pigment [2] [3]. The NIR band represents the NIR plateau. 

PI = (R750 � R635)/(R750 + R635)


References:
[1] B. Jesus, C.R. Mendes, V. Brotas and D.M. Paterson "Effect of sediment type on microphytobenthos vertical distribution: Modelling the productive biomass and improving ground truth measurements";  Journal of Experimental Marine Biology and Ecology(332), 60-74 (2006).
[2]  L. Barill�, J-L. Mouget, V. M�l�der, P. Rosa and B. Jesus "Spectral response of benthic diatoms with different sediment backgrounds"; Remote Sens. Environ. (115), 1034-1042 (2011).
[3] V. M�l�der, P. Launeau, L. Barill� and L. Rince "Cartographie des peuplements du microphytobenthos per t�l�d�tection spatiale visible-infrarouge dans un �cosyst�me conchylicole"; C.R. Biol. (326),377-389 (2003).
