;+
; :Author: <Hongyan Xi> (hongyan.xi@hzg.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `OceanColorChlorophyll_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `OceanColorChlorophyll_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `OceanColorChlorophyll_showReport`).
;
;-
pro oceancolorchlorophyll_application, ApplicationInfo
  
  ; Note:
  ; use settings if needed
  settings = OceanColorChlorophyll_getSettings()
  ; use argument keyword if needed
  settings = settings + applicationInfo
  
  parameters = OceanColorChlorophyll_getParameters(settings)
  
  if parameters ['accept'] then begin
    reportInfo = OceanColorChlorophyll_processing(parameters, settings)
    
;    if parameters ['showReport'] then begin
;      OceanColorChlorophyll_showReport, reportInfo, settings
;    endif
  endif
end

pro test_OceanColorChlorophyll_application
  applicationInfo = Hash()
  OceanColorChlorophyll_application, applicationInfo
end  
