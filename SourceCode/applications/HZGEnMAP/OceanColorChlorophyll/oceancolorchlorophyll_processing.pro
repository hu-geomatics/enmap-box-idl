;+
; :Author: <Hongyan Xi> (<hongyan.xi@hzg.de>)
;-

;
;
;Algorithm Form:
;http://oceancolor.gsfc.nasa.gov/REPROCESSING/R2009/ocv6/
;
; Rrs1 = blue wavelength Rrs (e.g., 443, 490, or 510-nm)
; Rrs2 = green wavelength Rrs (e.g., 547, 555, or 565-nm)
; 
; OC4 form: X=log10((Rrs1>Rrs2>Rrs3)/Rrs4), Chl = 10^(a0 + a1*X + a2*X^2 + a3*X^3 + a4*X^4)
; a0 = 0.3272, a1 = -2.9940, a2 = 2.7218, a3 = -1.2259, a4 = -0.5683

function oceancolorchlorophyll_processing, parameters, settings

  ; check required parameters
  tileLines=100

  wavebd1 = 443
  wavebd2 = 489
  wavebd3 = 510
  wavebd4 = 555
  a0 = 0.3272
  a1 = -2.9940
  a2 = 2.7218
  a3 = -1.2259
  a4 = -0.5683
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  outputImage = hubIOImgOutputImage(parameters['outputImage'])
  
  ;create a progressBar
  progressBar = hubProgressBar(Title=settings.hubGetValue('title') $
    ;use GroupLeader if defined
    ,GroupLeader = settings.hubGetValue('groupLeader'))
  progressBar.setInfo, 'Calculating Chlorophyll...'
  progressBar.setRange, [0,inputImage.getMeta('lines')]
  progressDone = 0

  result = hash()

  ;;======= Wavelengths

  myBandnames= 'Chl-OC4'

  outputImage.setMeta, 'band names', myBandnames

  OC4B1 = inputimage.locateWavelength(wavebd1)
  OC4B2 = inputimage.locateWavelength(wavebd2)
  OC4B3 = inputimage.locateWavelength(wavebd3)
  OC4B4 = inputimage.locateWavelength(wavebd4)

  inputimage.initReader, tilelines, /TILEPROCESSING, /SLICE, $
    SubsetbandPositions=[OC4B1,OC4B2,OC4B3,OC4B4], Datatype='float'

  outputImage.copyMeta, inputimage, /COPYSPATIALINFORMATION
  outputImage.initWriter, inputimage.getWriterSettings(SetBands=1)

  while ~inputImage.tileProcessingDone() do begin
    
    data = inputimage.getData()
   
    R_ratio=alog10((data[0,*]>data[1,*]>data[2,*])/data[3,*])
    ChlConc= 10^(a0 + a1*R_ratio + a2*R_ratio^2 + a3*R_ratio^3 + a4*R_ratio^4)
   
    ;remove negative Rrs, replace invalid Chl with 0 
    ChlConc[where((data[0,*] le 0) or (data[1,*] le 0) or (data[2,*] le 0) or (data[3,*] le 0))] = 0
    ChlConc[where(~finite(R_ratio))] = 0
    ChlConc[where(ChlConc ge 200)] = 0
    outputImage.writeData, ChlConc
    
    progressDone += tileLines
    progressBar.setProgress, progressDone
    
  endwhile

  ;cleanup objects
  inputImage.cleanup
  outputImage.cleanup
  progressBar.cleanup


  ; store results to be reported inside a hash
  result = hash(); empty hash
  result += parameters.hubGetSubHash(['inputImage','outputImage'])
  return, result

  ; perform (image tile) processing
  ; if suitable, split your processing routine into individual image and data processing routines
  
  ; store results to be reported inside a hash
  
end

pro test_OceanColorChlorophyll_processing
  result = OceanColorChlorophyll_processing(parameters, Title='OceanColorChlorophyll', GroupLeader=groupLeader)
  print, result
end  
