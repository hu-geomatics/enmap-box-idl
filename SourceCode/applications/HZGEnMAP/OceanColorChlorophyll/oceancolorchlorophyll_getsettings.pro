;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('OceanColorChlorophyll.conf', ROOT=OceanColorChlorophyll_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, OceanColorChlorophyll_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function oceancolorchlorophyll_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('OceanColorChlorophyll.conf', ROOT=OceanColorChlorophyll_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_OceanColorChlorophyll_getSettings

  print, OceanColorChlorophyll_getSettings()

end
