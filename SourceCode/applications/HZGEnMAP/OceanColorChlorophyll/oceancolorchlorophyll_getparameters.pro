;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;-
function oceancolorchlorophyll_getParameters, settings

  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'OceanColorChlorophyll V. 1.0'
  
  ; frame for input options & parameters
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputImage', Title='Image'

  
;  hubAMW_frame, Title='Parameters'
;  hubAMW_label, 'Query application parameters here.'
;  
; frame for output options & parameters
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputImage', Title='Chlorophyll Prediction', value='Chloro'
  ; insert your widgets
  ;hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b

  parameters = hubAMW_manage()
  
  
  if parameters['accept'] then begin
    ; if required, perform some additional changes on the parameters hash
  endif
;  endif else begin
;    parameters = !null
;  endelse
  return, parameters
end

pro test_OceanColorChlorophyll_getParameters

  settings = OceanColorChlorophyll_getSettings()
  parameters = OceanColorChlorophyll_getParameters(settings)
  print, parameters
end  
