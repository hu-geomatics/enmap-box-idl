;+
; :Author: <author name> (<email>)
;-

function oceancolorchlorophyll_getDirname
  
  result = filepath('OceanColorChlorophyll', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_OceanColorChlorophyll_getDirname

  print, OceanColorChlorophyll_getDirname()
  print, OceanColorChlorophyll_getDirname(/SourceCode)

end
