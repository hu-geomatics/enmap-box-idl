;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `template_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro template_event, event
  
  ; set up a default error handler
  @huberrorcatch
  
  ; query information about the application (which is stored inside menu button)
  ;   - note that menu buttons are defined inside the menu file '.\TEMPLATE\_resource\enmap.men'
  ;   - the following information is returned as a hash:
  ;       applicationInfo['name'] = 'TEMPLATE Application'
  ;       applicationInfo['argument] = 'TEMPLATE_ARGUMENT'
  ;       applicationInfo['groupLeader'] = <EnMAP-Box top level base>
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)

  template_application, Title=applicationInfo['name'], GroupLeader=applicationInfo['groupLeader'], Argument=applicationInfo['argument']
end