;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;-
function template_getParameters, Title=title, GroupLeader=groupLeader
  
  hubAMW_program, groupLeader, Title=title

  hubAMW_frame, Title='Input'
  hubAMW_label, 'Query input images/files here.'

  hubAMW_frame, Title='Parameters'
  hubAMW_label, 'Query application parameters here.'

  hubAMW_frame, Title='Output'
  hubAMW_label, 'Query output images/files here.'
  parameters = hubAMW_manage(/Dictionary)
  
  if parameters.accept then begin
    ; if required, perform some additional changes on the parameters hash
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

pro test_template_getParameters
  parameters = template_getParameters(Title='template', GroupLeader=groupLeader)
  print, parameters
end  