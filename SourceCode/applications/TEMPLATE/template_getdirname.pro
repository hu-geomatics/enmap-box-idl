;+
; :Author: <author name> (<email>)
;-

function template_getDirname
  
  result = filepath('TEMPLATE', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_template_getDirname

  print, template_getDirname()
  print, template_getDirname(/SourceCode)

end