;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`template_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
;    parameters : in, required, type=hash
;    
;-
function template_processing, parameters, Title=title, GroupLeader=groupLeader

  ; check required parameters

  ; perform (image tile) processing
  ; if suitable, split your processing routine into individual image and data processing routines
  
  ; store results to be reported inside a hash
  
  result = hash()
  
  result['description'] = 'This application does nothing usefull. It is for demonstration only.'+ $
    strjoin(replicate('template text ', 100))

  result['table'] = [ $
    'template accuracy = 90%',$
    '',$
    'template 1 | template 2 | template 3',$
    '------------------------------------',$
    '      100% |       100% |       80% ',$
    '       70% |        50% |       80% ']

  result['image'] = bytscl(randomu(seed, 300, 300, 3))

  return, result

end

pro test_template_processing
  result = template_processing(parameters, Title='template', GroupLeader=groupLeader)
  print, result
end  
