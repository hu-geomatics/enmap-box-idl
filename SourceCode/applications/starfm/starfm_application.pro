pro starfm_application, Title=title, GroupLeader=groupLeader
  parameters = starfm_getParameters(Title=title, GroupLeader=groupLeader)
  if isa(parameters) then begin
    starfm_processingMultiBand, parameters, ShowLog=0, Title=title, GroupLeader=groupLeader
  endif
end

pro test_starfm_application
  starfm_application, Title='STARFM', GroupLeader=groupLeader
end  
