;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    parameters : in, required, type=dictionary
;      STARFM parameters::
;
;        Tag                      Type           Default   Description/Notes
;        -----------------------------------------------------------
;        NUM_IN_PAIRS             {1 | 2}        1         # number of input pairs, maximum = 2
;        IN_PAIR_MODIS_FNAME      filenames                # input MODIS files
;        IN_PAIR_MODIS_MASK       filename       'NONE'    # optional mask file, same order MODIS files and use NONE if not exist
;        IN_PAIR_LANDSAT_FNAME    filenames                # input Landsat files in the same order as MODIS
;        IN_PAIR_LANDSAT_MASK     filename       'NONE'    # optional mask file for Landsat
;        IN_PDAY_MODIS_FNAME      filename                 # MODIS input for the prediction date
;        IN_PDAY_MODIS_MASK       filename       'NONE'    # optional mask file for MODIS
;        OUT_PDAY_LANDSAT_FNAME   filename                 # output Landsat prediction file
;        RESOLUTION               float                    # spatial resolution
;        SCALE_FACTOR             float                    # scale factor for input reflectance file
;        LANDSAT_FILLV            integer                  # fill value for Landsat surface reflectance
;        LANDSAT_DATA_RANGE       integer[2]               # Landsat data range
;        LANDSAT_UNCERTAINTY      float          20        # uncertainty in the same unit as the scaled reflectance
;        MODIS_FILLV              integer                  # fill value for MODIS surface reflectance
;        MODIS_DATA_RANGE         integer[2]               # MODIS data range
;        MODIS_UNCERTAINTY        float          20        # uncertainty for MODIS 
;        USE_SPATIAL_FLAG         {'ON','OFF'}   'ON'      # spatial information flag, ON is strongly suggested
;        MAX_SEARCH_DISTANCE      float          1500      # maximum search distance for the spectral similar neighbor pixels
;        NUM_SLICE_PURE_TEST      integer        40        # number of slice for the spectral similar test (pure pixel)
;
; :Keywords:
;    NoOpen : in, optional, type=boolean
;      Set to not opening the image after processing.
;      
;    NoShow : in, optional, type=boolean
;      Set to hide progress bar.
;
;    ShowLog : in, optional, type=boolean
;      Set to show the output of StarFM.exe. Useful for debugging.
;
;    Title : in, optional, type=string
;      Title for GUI.
;      
;    GroupLeader : in, optional, type=int
;      Group leader for GUI.
;
; :Author: Andreas Rabe
;-
pro starfm_processingMultiBand, parameters, NoOpen=NoOpen, NoShow=noShow, ShowLog=showLog, Title=title, GroupLeader=groupLeader

  requiredKeys = ['LANDSAT_BANDS', 'MODIS_BANDS', 'IN_PAIR_MODIS_FNAME', 'IN_PAIR_LANDSAT_FNAME', 'IN_PDAY_MODIS_FNAME','OUT_PDAY_LANDSAT_FNAME','SCALE_FACTOR','LANDSAT_DATA_RANGE','MODIS_DATA_RANGE']
  if ~parameters.hubCheckKeys(requiredKeys, Missing=missing) then begin
    message, 'Incomplete parameter hash. Missing values for keys: '+strjoin(requiredKeys[missing],', ')
  endif

  workDir = file_dirname(parameters.OUT_PDAY_LANDSAT_FNAME)
  tempDir = filepath('', ROOT_DIR=workDir, SUBDIRECTORY='STARFMTemp')
  file_mkdir, workDir

  ; run single band starfm
  parametersSingle = parameters+dictionary()
  parametersSingle.IN_PAIR_MODIS_FNAME    = filepath('IN_PAIR_MODIS', ROOT_DIR=tempDir)
  parametersSingle.IN_PAIR_LANDSAT_FNAME  = filepath('IN_PAIR_LANDSAT', ROOT_DIR=tempDir)
  parametersSingle.IN_PDAY_MODIS_FNAME    = filepath('IN_PDAY_MODIS', ROOT_DIR=tempDir)
  image = hubIOImgInputImage(parameters.IN_PAIR_LANDSAT_FNAME)
  parametersSubsetting = hash()
  parametersSubsetting['sampleRange'] = [0,image.getMeta('samples')-1]
  parametersSubsetting['lineRange']   = [0,image.getMeta('lines')-1]
  parametersSubsetting['metaHash']    = hash('interleave', 'bsq')
  settingsSubsetting = hash('noShow', 1b, 'noOpen', 1b)
  resultImages = list()
  
  ; NOTE: Using "hubApp_ImageSubsetting_processing" is not the most efficient way to extract the single bands.
  ;       If needed, a faster routine could be implemented.

  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader, Range=[0,n_elements(parameters.LANDSAT_BANDS)], NoShow=noShow)
  for i=0,n_elements(parameters.LANDSAT_BANDS)-1 do begin
    progressBar.setInfo, 'Predict band '+strtrim(i+1,2)
    progressBar.setProgress, i+1

    ; subset IN_PAIR_MODIS
    parametersSubsetting['inputImage']  = parameters.IN_PAIR_MODIS_FNAME
    parametersSubsetting['outputImage'] = parametersSingle.IN_PAIR_MODIS_FNAME
    parametersSubsetting['bandSubset'] = (parameters.MODIS_BANDS)[i]
    hubApp_ImageSubsetting_processing, parametersSubsetting, settingsSubsetting

    ; subset IN_PAIR_LANDSAT
    parametersSubsetting['inputImage']  = parameters.IN_PAIR_LANDSAT_FNAME
    parametersSubsetting['outputImage'] = parametersSingle.IN_PAIR_LANDSAT_FNAME
    parametersSubsetting['bandSubset'] = (parameters.LANDSAT_BANDS)[i]
    hubApp_ImageSubsetting_processing, parametersSubsetting, settingsSubsetting

    ; subset IN_PDAY_MODIS
    parametersSubsetting['inputImage']  = parameters.IN_PDAY_MODIS_FNAME
    parametersSubsetting['outputImage'] = parametersSingle.IN_PDAY_MODIS_FNAME
    parametersSubsetting['bandSubset'] = (parameters.MODIS_BANDS)[i]
    hubApp_ImageSubsetting_processing, parametersSubsetting, settingsSubsetting

    parametersSingle.OUT_PDAY_LANDSAT_FNAME = filepath('OUT_PDAY_LANDSAT_', ROOT_DIR=tempDir)+strtrim(i,2)
    if keyword_set(showLog) then begin
      parametersSingle.logFilename = hub_getUserTemp('starfm_log_'+strtrim(i,2)+'.txt')
    endif
    starfm_processingSingleBand, parametersSingle, Title=title, GroupLeader=groupLeader, /NoOpen, ShowLog=showLog
    
    resultImages.add, parametersSingle.OUT_PDAY_LANDSAT_FNAME
  endfor
  progressBar.setProgress, /HIDE
  progressBar.setInfo, 'Stack single band predictions.'
  
  ; stack single band results
  parametersStacking = hash('inputImages',resultImages.toArray(), 'outputImage', parameters.OUT_PDAY_LANDSAT_FNAME);, 'metaHash', metaHash)
  hubApp_Stacking_processing, parametersStacking, hash('noShow', 1b, 'noOpen', noOpen)
  obj_destroy, progressBar
  
  ; delete tmp
  file_delete, filepath('', ROOT_DIR=workDir, SUBDIRECTORY='STARFMTemp'), /RECURSIVE

end

pro test_starfm_processingMultiBand
  enmapbox
  dataDir = 'E:\Andreas\Dropbox\starfm\'
  dataDir = 'D:\Dropbox\starfm\'

  parameters = starfm_getDefaultSettings()
  parameters.IN_PAIR_MODIS_FNAME    = dataDir+'basepair\stack_MOD09A1.A2011177.h17v05.005.2011202170550_utm_B02.dat'
  parameters.IN_PAIR_LANDSAT_FNAME  = dataDir+'basepair\stack_LT5_203034_20110624_TM_B02.dat'
  parameters.IN_PDAY_MODIS_FNAME    = dataDir+'prediction\stack_MOD09A1.A2011105.h17v05.005.2011117085616_utm_B02_prediction.dat'
  parameters.OUT_PDAY_LANDSAT_FNAME = hub_getUserTemp('starfm_prediction')
;  parameters.OUT_PDAY_LANDSAT_FNAME = 'D:\Users\Andreas\Desktop\STARFM_OUTPUT\starfm_prediction'
  parameters.SCALE_FACTOR           = 10000
  parameters.LANDSAT_DATA_RANGE     = [0,10000]
  parameters.MODIS_DATA_RANGE       = [0,10000]
  parameters.LANDSAT_BANDS          = [0,1]
  parameters.MODIS_BANDS            = [0,1]
  starfm_processingMultiBand, parameters, Title='STARFM', GroupLeader=groupLeader
end  
