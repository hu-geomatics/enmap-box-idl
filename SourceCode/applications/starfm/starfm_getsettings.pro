;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('starfm.conf', ROOT=starfm_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, starfm_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function starfm_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('starfm.conf'), ROOT=starfm_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_starfm_getSettings

  print, starfm_getSettings()

end
