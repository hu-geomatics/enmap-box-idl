function starfm_fixPOSIX, filename
  return, strjoin(strsplit(filename, '\', /EXTRACT, /PRESERVE_NULL),'/')
end
