;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    parameters : in, required, type=dictionary
;      STARFM parameters::
;
;        Tag                      Type           Default   Description/Notes
;        -----------------------------------------------------------
;        NUM_IN_PAIRS             {1 | 2}        1         # number of input pairs, maximum = 2
;        IN_PAIR_MODIS_FNAME      filenames                # input MODIS files
;        IN_PAIR_MODIS_MASK       filename       'NONE'    # optional mask file, same order MODIS files and use NONE if not exist
;        IN_PAIR_LANDSAT_FNAME    filenames                # input Landsat files in the same order as MODIS
;        IN_PAIR_LANDSAT_MASK     filename       'NONE'    # optional mask file for Landsat
;        IN_PDAY_MODIS_FNAME      filename                 # MODIS input for the prediction date
;        IN_PDAY_MODIS_MASK       filename       'NONE'    # optional mask file for MODIS
;        OUT_PDAY_LANDSAT_FNAME   filename                 # output Landsat prediction file
;        RESOLUTION               float                    # spatial resolution
;        SCALE_FACTOR             float                    # scale factor for input reflectance file
;        LANDSAT_FILLV            integer                  # fill value for Landsat surface reflectance
;        LANDSAT_DATA_RANGE       integer[2]               # Landsat data range
;        LANDSAT_UNCERTAINTY      float          20        # uncertainty in the same unit as the scaled reflectance
;        MODIS_FILLV              integer                  # fill value for MODIS surface reflectance
;        MODIS_DATA_RANGE         integer[2]               # MODIS data range
;        MODIS_UNCERTAINTY        float          20        # uncertainty for MODIS 
;        USE_SPATIAL_FLAG         {'ON','OFF'}   'ON'      # spatial information flag, ON is strongly suggested
;        MAX_SEARCH_DISTANCE      float          1500      # maximum search distance for the spectral similar neighbor pixels
;        NUM_SLICE_PURE_TEST      integer        40        # number of slice for the spectral similar test (pure pixel)
;        logFilename              filename                 optional log file
;        
; :Keywords:
;    NoOpen : in, optional, type=boolean
;      Set to not opening the image after processing. 
;      
;    ShowLog : in, optional, type=boolean
;      Set to show the output of StarFM.exe. Useful for debugging.
; 
;    Title : in, optional, type=string
;      Title for GUI.
;      
;    GroupLeader : in, optional, type=int
;      Group leader for GUI.
;
; :Author: Andreas Rabe
;-
pro starfm_processingSingleBand, parameters, NoOpen=NoOpen, ShowLog=showLog, Title=title, GroupLeader=groupLeader

  requiredKeys = ['IN_PAIR_MODIS_FNAME', 'IN_PAIR_LANDSAT_FNAME', 'IN_PDAY_MODIS_FNAME','OUT_PDAY_LANDSAT_FNAME','SCALE_FACTOR','LANDSAT_DATA_RANGE','MODIS_DATA_RANGE']
  if ~parameters.hubCheckKeys(requiredKeys, Missing=missing) then begin
    message, 'Incomplete parameter hash. Missing values for keys: '+strjoin(requiredKeys[missing],', ')
  endif

  file_mkdir,  file_dirname(parameters.OUT_PDAY_LANDSAT_FNAME)

  ; create settings file
  settingsFilename = hub_getUserTemp('starfm_settings.prm')
  starfm_createSettingsFile, parameters, settingsFilename
  
  ; create batch
  starfmExe = filepath('StarFM.exe', ROOT_DIR=starfm_getDirname(/SourceCode), SUBDIRECTORY='_resource') 
  batchText = [starfm_fixPOSIX(starfmExe)+' '+starfm_fixPOSIX(settingsFilename)]
  batchFilename = hub_getUserTemp('starfm_batch.bat')
  hubIOASCIIHelper.writeFile, batchFilename, batchText
  
  ; run batch
  logFilename = parameters.hubGetValue('logFilename', Default=hub_getUserTemp('starfm_log.txt', SubDirectory='STARFMTemp'))
  spawn, batchFilename+' > '+logFilename, /HIDE, /NOSHELL
  if keyword_set(showLog) then begin
    hubHelper.openFile, logFilename
  endif
  
  if ~keyword_set(noOpen) then begin
    hubIOImg_openImage, parameters.OUT_PDAY_LANDSAT_FNAME
  endif

end

pro test_starfm_processingSingleBand
  enmapbox
  parameters = starfm_getDefaultSettings()
  parameters.IN_PAIR_MODIS_FNAME    = 'C:\Users\janzandr\Desktop\starfm\basepair\MOD09A1.A2011177.h17v05.005.2011202170550_utm_B02.dat'
  parameters.IN_PAIR_LANDSAT_FNAME  = 'C:\Users\janzandr\Desktop\starfm\basepair\LT5_203034_20110624_TM_B02.dat'
  parameters.IN_PDAY_MODIS_FNAME    = 'C:\Users\janzandr\Desktop\starfm\prediction\MOD09A1.A2011105.h17v05.005.2011117085616_utm_B02_prediction.dat'
  parameters.OUT_PDAY_LANDSAT_FNAME = hub_getUserTemp('starfm_prediction')
  parameters.SCALE_FACTOR           = 10000
  parameters.LANDSAT_DATA_RANGE     = [0,10000]
  parameters.MODIS_DATA_RANGE       = [0,10000]
  starfm_processingSingleBand, parameters, Title='starfm', GroupLeader=groupLeader
end  
