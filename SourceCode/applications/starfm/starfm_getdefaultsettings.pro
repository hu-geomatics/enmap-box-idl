function starfm_getDefaultSettings
  s = dictionary()
  s['NUM_IN_PAIRS']           = 1
  s['IN_PAIR_MODIS_FNAME']    = !null
  s['IN_PAIR_MODIS_MASK']     = 'NONE'
  s['IN_PAIR_LANDSAT_FNAME']  = !null
  s['IN_PAIR_LANDSAT_MASK']   = 'NONE'
  s['IN_PDAY_MODIS_FNAME']    = !null
  s['IN_PDAY_MODIS_MASK']     = 'NONE'
  s['OUT_PDAY_LANDSAT_FNAME'] = !null
  s['NROWS']                  = !null
  s['NCOLS']                  = !null
  s['RESOLUTION']             = !null 
  s['SCALE_FACTOR']           = !null
  s['LANDSAT_FILLV']          = !null
  s['LANDSAT_DATA_RANGE']     = !null
  s['LANDSAT_UNCERTAINTY']    = 20.
  s['MODIS_FILLV']            = !null
  s['MODIS_DATA_RANGE']       = !null
  s['MODIS_UNCERTAINTY']      = 20.
  s['USE_SPATIAL_FLAG']       = 'ON'
  s['MAX_SEARCH_DISTANCE']    = 1500.
  s['NUM_SLICE_PURE_TEST']    = 40
  return, s
end