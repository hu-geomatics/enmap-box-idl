;+
; :Author: <author name> (<email>)
;-

function starfm_getDirname, SourceCode=sourceCode
  
  result = filepath('starfm', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result
  
end

;+
; :Hidden:
;-
pro test_starfm_getDirname

  print, starfm_getDirname()
  print, starfm_getDirname(/SourceCode)

end
