function starfm_getParameters1_consistencyCheck, resultHash, Message=message
 
  result = dictionary()+resultHash

  ; init images
  IN_PAIR_LANDSAT1 = hubIOImgInputImage(result.IN_PAIR_LANDSAT_FNAME1)
  IN_PAIR_MODIS1   = hubIOImgInputImage(result.IN_PAIR_MODIS_FNAME1)
  IN_PDAY_MODIS    = hubIOImgInputImage(result.IN_PDAY_MODIS_FNAME)
  if result.IN_PAIR_LANDSAT_MASK ne ''   then IN_PAIR_LANDSAT_MASK = hubIOImgInputImage(result.IN_PAIR_LANDSAT_MASK)
  if result.IN_PAIR_MODIS_MASK ne ''     then IN_PAIR_MODIS_MASK = hubIOImgInputImage(result.IN_PAIR_MODIS_MASK)
  if result.IN_PDAY_MODIS_MASK ne ''     then IN_PDAY_MODIS_MASK = hubIOImgInputImage(result.IN_PDAY_MODIS_MASK)
  if result.IN_PAIR_LANDSAT_FNAME2 ne '' then IN_PAIR_LANDSAT2 = hubIOImgInputImage(result.IN_PAIR_LANDSAT_FNAME2)
  if result.IN_PAIR_MODIS_FNAME2 ne ''   then IN_PAIR_MODIS2 = hubIOImgInputImage(result.IN_PAIR_MODIS_FNAME2)

  ; check second basepair
  if result.NUM_IN_PAIRS eq '2' then begin
    if ~isa(IN_PAIR_LANDSAT2) or ~isa(IN_PAIR_MODIS2) then begin
      message = 'Second basepair is undefined.'
      return, 0b
    endif
  endif
  
  ; check spatial sizes
  spatialSize = IN_PAIR_LANDSAT1.getSpatialSize()
  correctSpatialSize     = IN_PAIR_MODIS1.isCorrectSpatialSize(spatialSize)
  correctSpatialSize and= IN_PDAY_MODIS.isCorrectSpatialSize(spatialSize)
  if result.IN_PAIR_LANDSAT_MASK ne ''   then correctSpatialSize and= IN_PAIR_LANDSAT_MASK.isCorrectSpatialSize(spatialSize)
  if result.IN_PAIR_MODIS_MASK ne ''     then correctSpatialSize and= IN_PAIR_MODIS_MASK.isCorrectSpatialSize(spatialSize)
  if result.IN_PDAY_MODIS_MASK ne ''     then correctSpatialSize and= IN_PDAY_MODIS_MASK.isCorrectSpatialSize(spatialSize)
  if result.IN_PAIR_LANDSAT_FNAME2 ne '' then correctSpatialSize and= IN_PAIR_LANDSAT2.isCorrectSpatialSize(spatialSize)
  if result.IN_PAIR_MODIS_FNAME2 ne ''   then correctSpatialSize and= IN_PAIR_MODIS2.isCorrectSpatialSize(spatialSize)
  
  if ~correctSpatialSize then begin
    message = 'Inconsistent image spatial size.'
    return, 0b
  endif

  ; check INTERLEAVE and DATA TYPE? could be converted implicitly   

  return, 1b
end

function starfm_getParameters1, Title=title, GroupLeader=groupLeader
  
  oldParameters = hub_getAppState('starfm', 'parameters', Default=dictionary())

  ;fsize=750
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_checklist, 'NUM_IN_PAIRS',                    Title='NUM_IN_PAIRS          ', List=['1','2'], /Extract, Value=where(['1','2'] eq oldParameters.hubGetValue('NUM_IN_PAIRS', Default='1'))
  hubAMW_inputImageFilename, 'IN_PAIR_MODIS_FNAME1',   Title='IN_PAIR_MODIS_FNAME1  ', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PAIR_MODIS_FNAME1')) ? f : !null,    AllowEmptyImage=0
  hubAMW_inputImageFilename, 'IN_PAIR_MODIS_FNAME2',   Title='IN_PAIR_MODIS_FNAME2  ', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PAIR_MODIS_FNAME2')) ? f : !null,   /AllowEmptyImage
  hubAMW_inputImageFilename, 'IN_PAIR_MODIS_MASK',     Title='IN_PAIR_MODIS_MASK    ', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PAIR_MODIS_MASK')) ? f : !null,     /AllowEmptyImage
  hubAMW_inputImageFilename, 'IN_PAIR_LANDSAT_FNAME1', Title='IN_PAIR_LANDSAT_FNAME1', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PAIR_LANDSAT_FNAME1')) ? f : !null,  AllowEmptyImage=0
  hubAMW_inputImageFilename, 'IN_PAIR_LANDSAT_FNAME2', Title='IN_PAIR_LANDSAT_FNAME2', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PAIR_LANDSAT_FNAME2')) ? f : !null, /AllowEmptyImage
  hubAMW_inputImageFilename, 'IN_PAIR_LANDSAT_MASK',   Title='IN_PAIR_LANDSAT_MASK  ', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PAIR_LANDSAT_MASK')) ? f : !null,   /AllowEmptyImage
  hubAMW_inputImageFilename, 'IN_PDAY_MODIS_FNAME',    Title='IN_PDAY_MODIS_FNAME   ', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PDAY_MODIS_FNAME')) ? f : !null,     AllowEmptyImage=0
  hubAMW_inputImageFilename, 'IN_PDAY_MODIS_MASK',     Title='IN_PDAY_MODIS_MASK    ', SIZE=fsize, Value=(f=oldParameters.hubGetValue('IN_PDAY_MODIS_MASK')) ? f : !null,     /AllowEmptyImage
  hubAMW_frame, Title='Parameters'
  hubAMW_parameter, 'SCALE_FACTOR',                    Title='SCALE_FACTOR          ', Size=8, /Float, Value=oldParameters.hubGetValue('SCALE_FACTOR')
  hubAMW_parameter, 'LANDSAT_FILLV',                   Title='LANDSAT_FILLV         ', Size=8, /INTEGER, Value=oldParameters.hubGetValue('LANDSAT_FILLV'), /AllowEmptyValue
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'LANDSAT_DATA_RANGE1',             Title='LANDSAT_DATA_RANGE    ', Size=8, /Integer, Value=oldParameters.hubGetValue('LANDSAT_DATA_RANGE1')
  hubAMW_parameter, 'LANDSAT_DATA_RANGE2',             Title=' to', Size=8, /Integer, Value=oldParameters.hubGetValue('LANDSAT_DATA_RANGE2')
  hubAMW_subframe, /COLUMN
  hubAMW_parameter, 'LANDSAT_UNCERTAINTY',             Title='LANDSAT_UNCERTAINTY   ', Size=8, /Float, Unit='(Default=20)', Value=oldParameters.hubGetValue('movingWindowLength', Default=20.)
  hubAMW_parameter, 'MODIS_FILLV',                     Title='MODIS_FILLV           ', Size=8, /Integer, Value=oldParameters.hubGetValue('MODIS_FILLV'), /AllowEmptyValue
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'MODIS_DATA_RANGE1',               Title='MODIS_DATA_RANGE      ', Size=8, /Integer, Value=oldParameters.hubGetValue('MODIS_DATA_RANGE1')
  hubAMW_parameter, 'MODIS_DATA_RANGE2',               Title=' to', Size=8, /Integer, Value=oldParameters.hubGetValue('MODIS_DATA_RANGE2') 
  hubAMW_subframe, /COLUMN
  hubAMW_parameter, 'MODIS_UNCERTAINTY',               Title='MODIS_UNCERTAINTY     ', Size=8, /Float, Unit='(Default=20)', Value=oldParameters.hubGetValue('MODIS_UNCERTAINTY', Default=20.)
  hubAMW_checklist, 'USE_SPATIAL_FLAG',                Title='USE_SPATIAL_FLAG      ', List=['ON','OFF'], /Extract, Value=where(['ON','OFF'] eq oldParameters.hubGetValue('USE_SPATIAL_FLAG', Default='ON'))
  hubAMW_parameter, 'MAX_SEARCH_DISTANCE',             Title='MAX_SEARCH_DISTANCE   ', Size=8, /Float, Unit='(Default=1500)', Value=oldParameters.hubGetValue('MAX_SEARCH_DISTANCE', Default=1500)
  hubAMW_parameter, 'NUM_SLICE_PURE_TEST',             Title='NUM_SLICE_PURE_TEST   ', Size=8, /Integer, Unit='(Default=40)', Value=oldParameters.hubGetValue('NUM_SLICE_PURE_TEST', Default=40)

  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'OUT_PDAY_LANDSAT_FNAME',     Title='OUT_PDAY_LANDSAT_FNAME', SIZE=fsize, Value=oldParameters.hubGetValue('OUT_PDAY_LANDSAT_FNAME', Default='starfmPrediction')
  parameters = hubAMW_manage(/Dictionary, CONSISTENCYCHECKFUNCTION='starfm_getParameters1_consistencyCheck')
  
  if parameters.accept then begin
    hub_setAppState, 'starfm', 'parameters', parameters+hash()
    case parameters.NUM_IN_PAIRS of
      '1' : begin
        parameters.IN_PAIR_LANDSAT_FNAME = parameters.remove('IN_PAIR_LANDSAT_FNAME1')
        parameters.IN_PAIR_MODIS_FNAME   = parameters.remove('IN_PAIR_MODIS_FNAME1')
      end
      '2' : begin
        parameters.IN_PAIR_LANDSAT_FNAME = [parameters.remove('IN_PAIR_LANDSAT_FNAME1'),parameters.remove('IN_PAIR_LANDSAT_FNAME2')]
        parameters.IN_PAIR_MODIS_FNAME   = [parameters.remove('IN_PAIR_MODIS_FNAME1'),  parameters.remove('IN_PAIR_MODIS_FNAME2')]
      end
    endcase 
    parameters.LANDSAT_DATA_RANGE = [parameters.remove('LANDSAT_DATA_RANGE1'),parameters.remove('LANDSAT_DATA_RANGE2')]
    parameters.MODIS_DATA_RANGE   = [parameters.remove('MODIS_DATA_RANGE1'),  parameters.remove('MODIS_DATA_RANGE2')]

    ; delete empty parameters
    foreach value,parameters,key do begin
      if ~isa(value) || (isa(value, /SCALAR, 'string') && value eq '') then begin
        parameters.remove, key
      endif
    endforeach
    
    ; match landsat-modis bands dialog
    
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

pro test_starfm_getParameters1
  parameters1 = starfm_getParameters1(Title='STARFM', GroupLeader=groupLeader)
  parameters2 = starfm_getParameters2(parameters1, Title='STARFM', GroupLeader=groupLeader)
  parameters  = parameters1+parameters2
  print, parameters
end  
