function starfm_getParameters, Title=title, GroupLeader=groupLeader
  parameters1 = starfm_getParameters1(Title=title, GroupLeader=groupLeader)
  if isa(parameters1) then begin
    parameters2 = starfm_getParameters2(parameters1, Title=title, GroupLeader=groupLeader)
    if isa(parameters2) then begin
      parameters = parameters1+parameters2
    endif else begin
      parameters = !null
    endelse
  endif else begin
    parameters = !null
  endelse
  return, parameters
end  
