pro starfm_event, event

  @huberrorcatch
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)

  starfm_application, Title=applicationInfo['name'], GroupLeader=applicationInfo['groupLeader']
end
