pro starfm_createSettingsFile, parameters, filename

  s = starfm_getDefaultSettings()+parameters
  image = hubIOImgInputImage(parameters.IN_PAIR_MODIS_FNAME)
  s.NCOLS = image.getMeta('samples')
  s.NROWS = image.getMeta('lines')
  mapInfo = image.getMeta('map info')
  s.RESOLUTION = mean([mapInfo.SIZEX,mapInfo.SIZEY])

  settingsText = list()
  settingsText.add, 'STARFM_PARAMETER_START'
  settingsText.add, '# number of input pairs, maximum = 2'
  settingsText.add, 'NUM_IN_PAIRS = '+strtrim(s.NUM_IN_PAIRS, 2)
  settingsText.add, '# input MODIS files'
  settingsText.add, 'IN_PAIR_MODIS_FNAME = '+starfm_fixPOSIX(strtrim(s.IN_PAIR_MODIS_FNAME, 2))
  settingsText.add, '# optional mask file, same order MODIS files and use NONE if not exist'
  settingsText.add, 'IN_PAIR_MODIS_MASK = '+starfm_fixPOSIX(strtrim(s.IN_PAIR_MODIS_MASK, 2))
  settingsText.add, '# input Landsat files in the same order as MODIS'
  settingsText.add, 'IN_PAIR_LANDSAT_FNAME = '+starfm_fixPOSIX(strtrim(s.IN_PAIR_LANDSAT_FNAME, 2))
  settingsText.add, '# optional mask file for Landsat'
  settingsText.add, 'IN_PAIR_LANDSAT_MASK = '+starfm_fixPOSIX(strtrim(s.IN_PAIR_LANDSAT_MASK, 2))
  settingsText.add, '# optional classification map in the same order as input pairs'
  settingsText.add, '# classification map has to be generated from input Landsat data'
  settingsText.add, '# with as many separable classes as possible'
  settingsText.add, '#IN_PAIR_CLASSIFICATION_MAP ='
  settingsText.add, '# MODIS input for the prediction date'
  settingsText.add, 'IN_PDAY_MODIS_FNAME = '+starfm_fixPOSIX(strtrim(s.IN_PDAY_MODIS_FNAME, 2))
  settingsText.add, '# optional mask file for MODIS'
  settingsText.add, 'IN_PDAY_MODIS_MASK = '+starfm_fixPOSIX(strtrim(s.IN_PDAY_MODIS_MASK, 2))
  settingsText.add, '# output Landsat prediction file'
  settingsText.add, 'OUT_PDAY_LANDSAT_FNAME = '+starfm_fixPOSIX(strtrim(s.OUT_PDAY_LANDSAT_FNAME, 2))
  settingsText.add, '# number of rows'
  settingsText.add, 'NROWS = '+strtrim(s.NROWS, 2)
  settingsText.add, '# number of columns'
  settingsText.add, 'NCOLS = '+strtrim(s.NCOLS, 2)
  settingsText.add, '# spatial resolution'
  settingsText.add, 'RESOLUTION = '+strtrim(s.RESOLUTION, 2)
  settingsText.add, '# scale factor for input reflectance file'
  settingsText.add, 'SCALE_FACTOR = '+strtrim(s.SCALE_FACTOR, 2)
  settingsText.add, '# fill value for Landsat surface reflectance'
  settingsText.add, 'LANDSAT_FILLV = '+strtrim(s.hubGetValue('LANDSAT_FILLV', Default=''), 2)
  settingsText.add, '# Landsat data range'
  settingsText.add, 'LANDSAT_DATA_RANGE = '+strjoin(strtrim(s.LANDSAT_DATA_RANGE, 2),',')
  settingsText.add, '# uncertainty in the same unit as the scaled reflectance'
  settingsText.add, 'LANDSAT_UNCERTAINTY = '+strtrim(s.LANDSAT_UNCERTAINTY, 2)
  settingsText.add, '# fill value for MODIS surface reflectance'
  settingsText.add, 'MODIS_FILLV = '+strtrim(s.hubGetValue('MODIS_FILLV', Default=''), 2)
  settingsText.add, '# MODIS data range'
  settingsText.add, 'MODIS_DATA_RANGE = '+strjoin(strtrim(s.MODIS_DATA_RANGE, 2),',')
  settingsText.add, '# uncertainty for MODIS'
  settingsText.add, 'MODIS_UNCERTAINTY = '+strtrim(s.MODIS_UNCERTAINTY, 2)
  settingsText.add, '# spatial information flag, ON is strongly suggested'
  settingsText.add, 'USE_SPATIAL_FLAG = '+strtrim(s.USE_SPATIAL_FLAG, 2)
  settingsText.add, '# maximum search distance for the spectral similar neighbor pixels'
  settingsText.add, 'MAX_SEARCH_DISTANCE = '+strtrim(s.MAX_SEARCH_DISTANCE, 2)
  settingsText.add, '# number of slice for the spectral similar test (pure pixel)'
  settingsText.add, 'NUM_SLICE_PURE_TEST = '+strtrim(s.NUM_SLICE_PURE_TEST, 2)
  settingsText.add, '# optional replacement option for poor prediction (NEW to the paper)'
  settingsText.add, '# program will check and replace low quality prediction if this value is given'
  settingsText.add, '# this is useful for a small object to utlize temporal information from'
  settingsText.add, '# a same type large object that is beyond the searching distance'
  settingsText.add, '# define minimum acceptable percentage of samples within searching window'
  settingsText.add, 'MIN_SAMPLE_PERCENT ='
  settingsText.add, 'STARFM_PARAMETER_END'
  hubIOASCIIHelper.writeFile, filename, settingsText.toArray()
end