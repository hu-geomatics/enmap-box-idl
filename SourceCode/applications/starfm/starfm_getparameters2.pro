function starfm_getParameters2, parameters1, Title=title, GroupLeader=groupLeader
  compile_opt idl2
  
  oldParameters = hub_getAppState('starfm', 'parameters2', Default=dictionary())

  IN_PAIR_LANDSAT = hubIOImgInputImage((parameters1.IN_PAIR_LANDSAT_FNAME)[0])
  IN_PAIR_MODIS   = hubIOImgInputImage((parameters1.IN_PAIR_MODIS_FNAME)[0])
  bandnamesLandsat = IN_PAIR_LANDSAT.getMeta('band names')
  bandnamesModis   = IN_PAIR_MODIS.getMeta('band names')

  ndigits = strtrim(floor(alog10(n_elements(bandnamesLandsat)))+1,2)
  format = '(i'+ndigits+'.'+ndigits+')'
  hubAMW_program, Title=title, groupLeader
  hubAMW_label, 'Match corresponding Landsat and Modis Bands. 
  for i=0,n_elements(bandnamesLandsat)-1 do begin
    iStr = strtrim(i,2)
    hubAMW_subframe, /ROW
    hubAMW_combobox, 'landsat'+iStr, Title='Landsat Band '+   string(i+1, Format=format), Size=200, List=bandnamesLandsat, Value=oldParameters.hubGetValue('landsat'+iStr, Default=i)
    hubAMW_combobox, 'modis'+iStr,   Title='Modis Band '+     string(i+1, Format=format), Size=200, List=bandnamesModis,   Value=oldParameters.hubGetValue('modis'+iStr)
  endfor
  parameters = hubAMW_manage(/Dictionary)
  
  if parameters.accept then begin
    parameters.LANDSAT_BANDS = (parameters.hubGetValues('landsat'+strtrim([0:n_elements(bandnamesLandsat)-1],2))).toArray()
    parameters.MODIS_BANDS   = (parameters.hubGetValues('modis'+  strtrim([0:n_elements(bandnamesLandsat)-1],2))).toArray()
  endif else begin
    parameters = !null
  endelse
  return, parameters
end
