pro TimeStats_application, type, Title=title, GroupLeader=groupLeader

  case type of
    '' : timeStats, GROUP=groupLeader
    'basic_statistics' : begin
      parameters = TimeStats_basicStatistics_getParameters(Title=title, GroupLeader=groupLeader)
      if isa(parameters) then begin
        TimeStats_basicStatistics_processing, parameters.inputFilename, parameters.outputFilename, parameters.parameterList,$
          LagsForADFTest=parameters.lagsForADFTest, PeriodOfSeasonalComponent=parameters.periodOfSeasonalComponent, SignificanceTestOption=parameters.significanceTestOption,$
          Title=title, GroupLeader=groupLeader
      endif
    end
    'data_pre-processing' : begin
      parameters = TimeStats_dataPreprocessing_getParameters(Title=title, GroupLeader=groupLeader)
      if isa(parameters) then begin
        TimeStats_dataPreprocessing_processing, parameters.inputFilename, parameters.outputDirectory, parameters.preprocessingList,$
          LagForDifferences=parameters.lagForDifferences, $
          DetrendingParameters=parameters.detrendingParameters, $
          ThresholdForDifferenceImages=parameters.thresholdForDifferenceImages, $
          LagForDifferenceImages=parameters.lagForDifferenceImages, $
          RecordRange=parameters.recordRange, $
          Title=title, GroupLeader=groupLeader
      endif
    end
    'advanced_regression': begin
      parameters = TimeStats_advancedRegression_getParameters(Title=title, GroupLeader=groupLeader)
      if isa(parameters) then begin
        stop
;        TimeStats_advancedRegression_processing, parameters.inputFilename, parameters.outputDirectory, parameters.preprocessingList,$
;          LagForDifferences=parameters.lagForDifferences, $
;          DetrendingParameters=parameters.detrendingParameters, $
;          ThresholdForDifferenceImages=parameters.thresholdForDifferenceImages, $
;          LagForDifferenceImages=parameters.lagForDifferenceImages, $
;          RecordRange=parameters.recordRange, $
;          Title=title, GroupLeader=groupLeader
      endif
    end
    
    
    'load_test_data' : begin
      filenames = TimeStats_getTestImage()
      foreach filename, filenames do begin
        enmapBox_openImage, filename
      endforeach
    end
  endcase
end