function TimeStats_dialogSelectInputStack, groupLeader
  oldFilename = hub_getAppState('TimeStats', 'inputStack')
  hubAMW_program, groupLeader, Title='ENVI-stacked image file (BIL)'
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'filename', Title='ENVI-stacked image file (BIL)', VALUE=oldFilename
  result = hubAMW_manage()
  if result['accept'] then begin
    filename = result['filename']
    hub_setAppState, 'TimeStats', 'inputStack', filename
  endif else begin
    filename = ''
  endelse
  return, filename
end