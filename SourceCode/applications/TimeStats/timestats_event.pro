pro TimeStats_event, event
  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  TimeStats_application, applicationInfo['argument'], Title=applicationInfo['name'], GroupLeader=applicationInfo['groupLeader']
end
