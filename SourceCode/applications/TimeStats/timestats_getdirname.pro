function TimeStats_getDirname, SourceCode=sourceCode
  result = filepath('TimeStats', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result
end

pro test_TimeStats_getDirname
  print, TimeStats_getDirname()
  print, TimeStats_getDirname(/SourceCode)
end
