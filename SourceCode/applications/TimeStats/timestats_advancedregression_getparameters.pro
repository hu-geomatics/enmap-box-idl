function TimeStats_advancedRegression_getParameters, Title=title, GroupLeader=groupLeader

  oldParameters = hub_getAppState('TimeStats', 'advancedRegressionParameters', Default=DICTIONARY())

  ;fsize=750
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputFilename',            Title='Time Series Stack    ', SIZE=fsize, Value=oldParameters.hubGetValue('inputFilename')
  hubAMW_frame, Title='Parameters'
  hubAMW_checklist, 'compareRegression',                 Title='Compare a restricted and unrestricted regression model?', List=['no','yes'], Value=oldParameters.hubGetValue('compareRegression', Default=0) 
  hubAMW_checklist, 'restrictedRegressionParameters',    Title='Restricted regression parameters  ',   List=['t','t^2','t^3','t^4','t^5','X1','X2','X3','X4','X5','FP'], /MultipleSelection, Value=oldParameters.hubGetValue('restrictedRegressionParameters')
  hubAMW_checklist, 'unrestrictedRegressionParameters',  Title='Unrestricted regression parameters', List=['t','t^2','t^3','t^4','t^5','X1','X2','X3','X4','X5','FP'], /MultipleSelection, Value=oldParameters.hubGetValue('unrestrictedRegressionParameters')
  hubAMW_parameter, 'lagsY',                             Title='Lags: Y', Size=5, /Integer, Value=oldParameters.hubGetValue('lagsY')
  for i=1,5 do begin
    iString = strtrim(i,2)
    hubAMW_subframe, /ROW
    hubAMW_inputImageFilename, 'inputFilenameX'+iString, Title='External Regressor X'+iString, Value=oldParameters.hubGetValue('inputFilenameX'+iString)
    hubAMW_parameter, 'lagsX'+iString, Title=' Lags: X'+iString, Size=5, /Integer, Value=oldParameters.hubGetValue('lagsX'+iString)
  endfor
  hubAMW_subframe, /COLUMN
  hubAMW_checklist, 'glsParameterEstimation', Title='GLS parameter estimation', List=['no','yes'], Value=oldParameters.hubGetValue('glsParameterEstimation', Default=0)
  hubAMW_parameter, 'movingWindowLength', Title='Length of moving window for lin. windowed trend-analysis', Size=5, /Integer, Unit='(Default=-1)', Value=oldParameters.hubGetValue('movingWindowLength', Default=-1)  
  hubAMW_parameter, 'overlappingPoints', Title='Overlapping points', Size=5, /Integer, Unit='(Default=-1)', Value=oldParameters.hubGetValue('overlappingPoints', Default=-1)
  hubAMW_frame, Title='Fourier Polynomial Parameters'
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'lengthOfFourierPeriod1', Title='Length of Fourier period 1', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('lengthOfFourierPeriod1', Default=0)
  hubAMW_parameter, 'numberOfHarmonics1',     Title='   No. of harmonics',        Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('numberOfHarmonics1', Default=0)
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'lengthOfFourierPeriod2', Title='Length of Fourier period 2', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('lengthOfFourierPeriod2', Default=0)
  hubAMW_parameter, 'numberOfHarmonics2',     Title='   No. of harmonics',        Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('numberOfHarmonics2', Default=0)
  hubAMW_frame, Title='Output'
  hubAMW_outputDirectoryName, 'outputDirectory',         Title='Output Directory     ', SIZE=75, Value=oldParameters.hubGetValue('outputDirectory', Default='TimeStats')
  result = hubAMW_manage(/DICTIONARY)

  if result.accept then begin 
    parameters = result
    hub_setAppState, 'TimeStats', 'advancedRegressionParameters', parameters+hash()
    fourierParameters = dictionary()
    fourierParameters.lengthOfFourierPeriod1 = parameters.remove('lengthOfFourierPeriod1')
    fourierParameters.numberOfHarmonics1 = parameters.remove('numberOfHarmonics1')
    fourierParameters.lengthOfFourierPeriod2 = parameters.remove('lengthOfFourierPeriod2')
    fourierParameters.numberOfHarmonics2 = parameters.remove('numberOfHarmonics2')
    parameters['fourierParameters'] = fourierParameters.toStruct()
  endif else begin
    parameters = !NULL
  endelse
  return, parameters
end

pro test_TimeStats_advancedRegression_getParameters
  parameters = TimeStats_advancedRegression_getParameters()
  print,parameters
end
