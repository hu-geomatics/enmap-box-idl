function TimeStats_getTestImage, basename
  dirname = filepath('', ROOT=TimeStats_getDirname(), SUBDIR=['resource','testData'])
  filename = hub_getDirectoryImageFilename(dirname, basename)
  return, filename
end

pro test_TimeStats_getTestImage
  print, TimeStats_getTestImage()
end
