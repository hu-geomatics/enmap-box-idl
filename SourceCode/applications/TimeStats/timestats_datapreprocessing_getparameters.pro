function TimeStats_dataPreprocessing_getParameters, Title=title, GroupLeader=groupLeader

  preprocessingNames = ['No pre-processing','Anomalies from total mean','Standardized anomalies from total mean', $
    'Anomalies from  seasonal means','Standardized anomalies from seasonal means', $
    'Differences','De-trending','Prewhitening (ARMA-model)','Seasonal averaging (aggregation)','Maximum value compositing']
  significanceNames = ['save emp. t-scores (lin. reg) / emp. z-scores (MK, SMK, MSK)','save related twosided P-values']

  oldParameters = hub_getAppState('TimeStats', 'dataPreprocessingParameters', Default=DICTIONARY())

  ;fsize=750
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputFilename', Title='Time Series Stack          ', SIZE=fsize, Value=oldParameters.hubGetValue('inputFilename')
  hubAMW_frame, Title='Parameters'
  hubAMW_checklist, 'preprocessingList', Title='', List=preprocessingNames, /MULTIPLESELECTION, /SHOWBUTTONBAR, COLUMN=2, Value=oldParameters.hubGetValue('preprocessingList') 
  hubAMW_parameter, 'lagForDifferences',                Title='Lag for differences; length of seasonal/MVC period', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('lagForDifferences', Default=12)
  hubAMW_parameter, 'thresholdForDifferenceImages',     Title='Threshold (%) for difference images               ', Size=5, /Float, Unit='(Default=0)', Value=oldParameters.hubGetValue('thresholdForDifferenceImages', Default=0)
  hubAMW_parameter, 'lagForDifferenceImages',           Title='Choose lag                                        ', Size=5, /INTEGER, Unit='(Default=1)', Value=oldParameters.hubGetValue('thresholdForDifferenceImages', Default=1)
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'recordRange1',                     Title='Use record no. from                               ', Size=5, /INTEGER, Value=oldParameters.hubGetValue('recordRange1', Default=-1)
  hubAMW_parameter, 'recordRange2',     Title=' to', Size=5, /INTEGER, Unit='(Default= -1 to -1)', Value=oldParameters.hubGetValue('recordRange2', Default=-1)
  hubAMW_frame, Title='De-trending Parameters'
  hubAMW_checklist, 'detrendingIncludeLinearTrend', Title='Include linear trend component?', List=['yes','no'], Value=oldParameters.hubGetValue('detrendingIncludeLinearTrend', Default=0)
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'detrendingLengthOfFourierPeriod1', Title='Length of Fourier period 1', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('detrendingLengthOfFourierPeriod1', Default=0)
  hubAMW_parameter, 'detrendingNumberOfHarmonics1',     Title='   No. of harmonics', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('detrendingNumberOfHarmonics1', Default=0)
  hubAMW_subframe, /ROW
  hubAMW_parameter, 'detrendingLengthOfFourierPeriod2', Title='Length of Fourier period 2', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('detrendingLengthOfFourierPeriod2', Default=0)
  hubAMW_parameter, 'detrendingNumberOfHarmonics2',     Title='   No. of harmonics', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('detrendingNumberOfHarmonics2', Default=0)

  hubAMW_frame, Title='Output'
  hubAMW_outputDirectoryName, 'outputDirectory', Title='Output Directory ', SIZE=75, Value=oldParameters.hubGetValue('outputDirectory', Default='TimeStats')
  result = hubAMW_manage(/DICTIONARY)

  if result.accept then begin 
    parameters = result
    hub_setAppState, 'TimeStats', 'dataPreprocessingParameters', parameters+hash()
    detrendingParameters = dictionary()
    detrendingParameters.includeLinearTrend =  parameters.detrendingIncludeLinearTrend
    detrendingParameters.lengthOfFourierPeriod1 = parameters.remove('detrendingLengthOfFourierPeriod1')
    detrendingParameters.numberOfHarmonics1 = parameters.remove('detrendingNumberOfHarmonics1')
    detrendingParameters.lengthOfFourierPeriod2 = parameters.remove('detrendingLengthOfFourierPeriod2')
    detrendingParameters.numberOfHarmonics2 = parameters.remove('detrendingNumberOfHarmonics2')
    parameters['detrendingParameters'] = detrendingParameters.toStruct()
    parameters['recordRange'] = [parameters.remove('recordRange1'),parameters.remove('recordRange2')]
  endif else begin
    parameters = !NULL
  endelse
  return, parameters
end

pro test_TimeStats_dataPreprocessing_getParameters
  parameters = TimeStats_dataPreprocessing_getParameters()
  print,parameters
  help,parameters.outputDirectory
end
