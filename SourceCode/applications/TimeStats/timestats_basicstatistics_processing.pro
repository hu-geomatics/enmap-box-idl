;+
; :Description:
;    GUI-independent version of TimeStats "Basic statistics and trends" functionality.
;
; :Params:
;    inputFilename : in, required, type=filename
;      Filename to the temporal image-stack (BIL interleave).
;      
;    outputFilename : in, required, type=filename
;      Filename to the output regression parameters image. 
;      
;    parameterList : in, required, type=int[]
;      Array of regression parameters to be calculated. Specify a subset of the following indices::
;      
;        0=Reg. bias (b)
;        1=Reg. offset
;        2=Sigma
;        3=T-test (b)
;        4=r
;        5=RMS
;        6=RMS/mean
;        7=MaxResid
;        8=TimeMaxResid
;        9=MK test
;        10=K slope
;        11=SK test
;        12=SK slope
;        13=Mod.SK test
;        14=Stdev
;        15=Mean
;        16=Varkoeff
;        17=valid n
;        18=skewness
;        19=kurtosis
;        20=Abbe criterion
;        21=rho rank corr
;        22=tau rank korr
;        23=sig. rho
;        24=sig. tau
;        25=min
;        26=max
;        29=range
;        30=ADF test, incl. const./trend
;      
; :Keywords:
;    LagsForADFTest : in, optional, type=int, default=0
;      Lags for Augmented-Dickey-Fuller (ADF) test.
;      
;    PeriodOfSeasonalComponent : in, optional, type=int, default=12
;      Period of seasonal component.
;    
;    SignificanceTestOption : in, optional, type={0|1}, default=0
;      Significance test options are::
;        
;        0 = 'save emp. t-scores (lin. reg) / emp. z-scores (MK, SMK, MSK)'
;        1 = 'save related twosided P-values'      
;
;    Title : in, optional, type=string
;      Title for GUI.
;      
;    GroupLeader : in, optional, type=int
;      Group leader for GUI.
;
; :Author: Andreas Rabe
;-
pro TimeStats_basicStatistics_processing, inputFilename, outputFilename, parameterList,$
  LagsForADFTest=lagsForADFTest, $                      
  PeriodOfSeasonalComponent=periodOfSeasonalComponent, $
  SignificanceTestOption=significanceTestOption, $
  Title=title, GroupLeader=groupLeader

  ; check time stack interleave
  if ~strcmp(hubIOImg_getMeta(inputFilename, 'interleave'), 'bil', /FOLD_CASE) then begin
    message, 'The input time stack interleave must be BIL.'
  endif

  @timestats_initcommonblocks
  
  ; assign inputs to common block variables

  FILELIST         = inputFilename
  FILE             = inputFilename
  OFILE            = outputFilename
  WORKDIR          = file_dirname(FILE)
  FLAG[parameterList] = 1b
  if isa(lagsForADFTest) then lagsADF = lagsForADFTest
  if isa(periodOfSeasonalComponent) then quest.freq = periodOfSeasonalComponent
  if isa(significanceTestOption) then quest.sig = significanceTestOption
  BATCH            = 1 ; need to set this to 1 because of ENVI input I think(?)

  ; run TREND procedure
  TimeStats_runTrend, GroupLeader=groupLeader, Title=title

end

pro test_timestats_basicStatistics_processing
  inputFilename = TimeStats_getTestImage('timeStatsTestImage.bil')
  outputFilename = hub_getUserTemp('timeStatsOutput')
  parameterList = [0,1,5] ; calculate 3 parameters
  timestats_basicStatistics_processing, inputFilename, outputFilename, parameterList
end

pro test_timestats_basicStatistics_processing2
  filenames = file_search('D:\EnMAP-Box\enmapProject\applications\TimeStats\resource\testData\', '*.bil')
  foreach inputFilename,filenames do begin
    outputFilename = filepath(file_basename(inputFilename, '.bil')+'_TSOutput.bil', ROOT_DIR='d:\')
    parameterList = [0,1,5] ; calculate 3 parameters
    timestats_basicStatistics_processing, inputFilename, outputFilename, parameterList
  endforeach
end
