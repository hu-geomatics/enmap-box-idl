function bartlett,n
; adopted from barlett.m
temp=findgen(fix(n/2))
temp=temp*2
w = temp/(n-1);
if n mod 2 eq 0 then       w = [w, reverse(w[1:n_elements(w)-1]),0] $
    ; It's an even length sequence
else  w = [w, 1,reverse(w)]
    ; It's odd

return,w
end