function phase,x
  samples=n_elements(x)
  fkoeffs=fft(x)
  ; Phase
  phase=ATAN(fkoeffs, /PHASE) * (-1.)  ; Phasenspektrum, vorzeichen umdrehen
  temp=where(phase lt 0, count)
  if count gt 0 then phase[temp] =!pi + (phase[temp]+!pi) ; Phasenspektrum jetzt im Bereich 0-2pi
  ; tempor�re Skalierung in Monate (f�r Jahresgang!!!)
  ;phase=12. / (2 * !pi)*phase
  return,phase
end