function mvoutlier,pixelzeile,cols,zahl,mv,missvalue,outlier,olcorrmeth, olstddev, olthresh,freq
; f�hrt die Korrektur von Missing values und Ausrei�ern durch
  for k=0,cols-1 do begin
   temp=pixelzeile[k,*]
   if min(temp) ne max(temp) then begin   ; Maske!
        ; Missing Value Korrektur
     if missvalue gt 1 then begin
          interpolate_mv, zahl, temp, mv, missvalue,freq
          pixelzeile[k,*]=temp
     endif
        ; Ausreisser-Korrektur
     if outlier gt 0 then begin

         ; Erkennung �ber die Standardabweichung
          t=where((temp gt (mean(temp)+olstddev*stddev(temp))) or (temp lt (mean(temp)-olstddev*stddev(temp))),count)
          if count gt 0 then temp[t]=9999
          interpolate_mv, zahl, temp, 9999, olcorrmeth,freq
          pixelzeile[k,*]=temp
     endif ; ENDE AUSREISSERROUTINE
    endif
  endfor
return, pixelzeile
end
