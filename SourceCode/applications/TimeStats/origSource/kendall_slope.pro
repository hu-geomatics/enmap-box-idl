; Mann-Kenall-slope f�r alle Werte (nicht saisonal)
function Kendall_slope, data_vec, missvalue, mv
;calculate sen's slope estimator for input array
;getestet 3.3.03

n = n_elements(data_vec)
slp_vec = fltarr((n*(n-1))/2)
index = 0

c=0

for i=0,n_elements(data_vec)-2 do begin
    for j=i+1,n_elements(data_vec)-1 do begin
        if (missvalue eq 1) and ((data_vec[j] eq mv) or (data_vec[i] eq mv)) then slp_vec[index] = mv else $
       slp_vec[index] = (float(data_vec[j])-float(data_vec[i]))/(float(j)-float(i))
       index = index+1
    endfor
endfor

if missvalue eq 1 then begin
    temp=where(slp_vec ne mv, count)
    if count gt 0 then begin
       slp_vec_neu=fltarr(count)
       slp_vec_neu=slp_vec[temp]
    endif else slp_vec_neu=slp_vec
endif else slp_vec_neu=slp_vec

slp_vec_neu = slp_vec_neu(SORT(slp_vec_neu))
res = median(slp_vec_neu,/even)

return, res

end
