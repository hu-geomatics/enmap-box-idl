function wn,resid,alpha
  ; Test auf white noise: Schlittgen & Streitberg, 371
  resid=resid-mean(resid)
  resid1=periodogram(resid,'NO',1,'Hertz')
  residN=n_elements(resid)/2
  resid1=resid1[0:residN-1]
  resid1[0]=0
  Fresid=fltarr(residN)
  ; berechnen der Verteilungsfunktion
  sumresid=0
  sumresid=total(resid1)
  for i=0,residN-1 do begin
    temp=total(resid1[0:i])
    Fresid[i]=float(temp)/sumresid
    Fresid[i]=Fresid[i]-(float(i)/residN)
  end

  ; berechnen der Pr�fgr��e
  c=max(abs(Fresid))
  nom=sqrt(-0.5*alog(alpha/2))
  denom=sqrt(residN-1)+0.2+(0.68/sqrt(residN-1))
  cH0= nom/denom - 0.4/(residN-1)
  if cH0 gt c then sig=0 else sig=1
  return,sig
end