function periodogram,x,windType,Fs,units,NFFT=NFFT
; adopted from periodogram.m

;   PERIODOGRAM  Power Spectral Density (PSD) estimate via periodogram method.
;   Pxx = PERIODOGRAM(X) returns the PSD estimate of the signal specified
;   by vector X in the vector Pxx.  The signal X is windowed
;   with a window specified in windType of the same length as X.

;   Pxx is the distribution of power per unit frequency.PERIODOGRAM returns the two-sided PSD by default;
;   Note that a one-sided PSD contains the total power of the input signal.
;   units = 'Hertz' or 'rad/sample'
;   Fs = sampling intervall (in TimeStats always 1!!)
;   NFFT: if keyword set then the series is padded to length = NFFT

   x = reform(x)
   N = n_elements(x); ; Record the length of the data
   case windtype of
    'HANN':win=HANNING(N)
    'HAMM':win=HANNING(N,ALPHA = 0.54)
    'BARTLETT':win=barlett(N)
    'NO':win=fltarr(N)+1
   end

   ; Window the data
   xw = float(x)*win;

   ; Evaluate the window normalization constant
   ; A 1/N factor has been omitted since it will cancel below
   Utemp = transpose(win) # win; compute sum of squares of window
   ; U ist Array mit 1 Element, daher Umwandlung erforderlich!
   U=replicate(Utemp,N)
   ; Compute the periodogram power spectrum [Power] estimate
   ; A 1/N factor has been omitted since it cancels

   if KEYWORD_SET(NFFT) then begin
       xwNew = fltarr(NFFT)
       xwNew[NFFT-N:NFFT-1] = xw
       U=replicate(Utemp,NFFT)
       xw=xwNew
   end


   Sxx =(abs(fft(xw))^2) / U;
   ; to switch units between Hertz and rad/sample see computepsd.m
   ; here: units=power=PSD, since sample frequency is always 1 in TimeStats!!!
   if units eq 'Hertz' then begin
       Pxx = Sxx / Fs; % Scale by the sampling frequency to obtain the psd
       ;w = 2.*pi.*(0:N-1)./N;
       ;w = w.*Fs./(2.*pi);  Scale the frequency vector from rad/sample to Hz
       ;units = 'Hz';
   end else Pxx = Sxx/(2.*!pi); Scale the power spectrum by 2*pi to obtain the psd
   ;units = 'rad/sample'

   return,Sxx
end

