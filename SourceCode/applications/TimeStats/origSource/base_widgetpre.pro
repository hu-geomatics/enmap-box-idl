pro base_widgetPre_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of

; 'groupneu': WIDGET_CONTROL, _freq, GET_VALUE=freq
 'groupFrom':WIDGET_CONTROL, _from, GET_VALUE=recFrom
 'groupTo':  WIDGET_CONTROL, _to,   GET_VALUE=recTo
 'group_hs1': WIDGET_CONTROL, _thresh, GET_VALUE=thresh
 'group_hs2': begin
               WIDGET_CONTROL, _lag, GET_VALUE=lag
               quest.lag=lag
              end


 'FPpara'  : base_widgetFP1

 'groupa': begin
           if quest.normalize[ev2.value] eq 0 then quest.normalize[ev2.value] = 1 $
             else quest.normalize[ev2.value] = 0;no normalization
             case ev2.value of
                  0: begin
                ;no normalization
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  1: begin
                      ; differnces from total mean
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  2: begin
                       ; differnces from total mean/stddev
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  3: begin
                       ; long-term seasonal differences
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  4: begin
                       ; long-term seasonal differences/stddev
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                       if (quest.normalize[3] eq 1) and  (quest.normalize[4] eq 1) $
                          then quest.normalize[4] = 0
                     end
                  5: begin
                       ; Differences
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end

                  6: begin
                       ; FP
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=1
                     end
                  7: begin
                       ; prewhitening
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end

                  8: begin
                       ; seasonal averages
                       ; either seas. averages or MVC!!
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  9: begin
                       ; MVC
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                       if (quest.normalize[9] eq 1) and  (quest.normalize[8] eq 1) $
                          then quest.normalize[9] = 0
                     end
             end
            end
 'groupb':   WIDGET_CONTROL, _normFreq, GET_VALUE=normFreq
 'ok'  :    WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end


pro base_widgetPre, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
  values1a = ['No pre-processing','Anomalies from total mean','Standardized anomalies from total mean', $
             'Anomalies from  seasonal means','Standardized anomalies from seasonal means', $
             'Differences','De-trending','Prewhitening (ARMA-model)','Seasonal averaging (aggregation)','Maximum value compositing']
  values2 = ['no','yes']
  values5d= ['De-trending parameters']


  mvout=  WIDGET_BASE(TITLE='Data pre-processing',/COLUMN)


  bgroup1= CW_BGROUP(mvout,values1a,/COLUMN,SET_VALUE=quest.normalize,LABEL_TOP='Pre-processing',/FRAME,/NONEXCLUSIVE,UVALUE='groupa')
  sizebase1 = WIDGET_BASE(mvout, /row)
  _normFreq=   CW_FIELD(sizebase1,/INTEGER,/ALL_EVENTS,VALUE=normFreq, $
           TITLE='Lag for differences; length of seasonal/MVC period',UVALUE='groupb')



  FPp=WIDGET_BASE(mvout, /COLUMN)
  bgroupFP=CW_BGROUP(FPp,values5d,/FRAME,UVALUE='FPpara')

   hsbase=WIDGET_BASE(mvout, /column,/frame)
  _thresh=CW_FIELD(hsbase,/FLOAT,/ALL_EVENTS,VALUE=thresh, $
         TITLE='Threshold (%) for difference images',UVALUE='group_hs1')
  lag=quest.lag
  _lag=CW_FIELD(hsbase,/INTEGER,/ALL_EVENTS,VALUE=lag,TITLE='Choose lag                                                ',UVALUE='group_hs2')

 sizebase3 = WIDGET_BASE(mvout, /ROW)
 _from=   CW_FIELD(sizebase3,/INTEGER,/ALL_EVENTS,VALUE=recFrom,TITLE='Use record no. from',UVALUE='groupFrom')
 _to  =   CW_FIELD(sizebase3,/INTEGER,/ALL_EVENTS,VALUE=recTo,TITLE='to',UVALUE='groupTo')

  endbase = WIDGET_BASE(mvout, /ROW,/ALIGN_BOTTOM)
  ok = widget_button(endbase,VALUE='  ok  ', UVALUE='ok')


  WIDGET_CONTROL, sizebase1, /NO_COPY,/REALIZE
  WIDGET_CONTROL, FPp, /NO_COPY,/REALIZE

  for i=0,n_elements(quest.normalize)-1 do $
  case i of
     0:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     1:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     2:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     3:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=1
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     4:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=1
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     5:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     6:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=1
       end
     7:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=1
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     else:break
  end

 ; if integrals eq 1 then WIDGET_CONTROL, sizebase2, SENSITIVE=1 $
 ;   else WIDGET_CONTROL, sizebase2, SENSITIVE=0
  ;WIDGET_CONTROL, sizebase1, SENSITIVE=0
  XMANAGER,'base_widgetPre', mvout, GROUP=GROUP1
end
