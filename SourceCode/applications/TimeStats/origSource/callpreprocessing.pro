function callpreprocessing,x,normFreq,quest,fp,corrterm,nrPSmooth,mv
; x=col*samples; samples werden ver�ndert
      ; zuerst gl�tten falls ,nrPSmooth > 0
       if nrPSmooth ge n_elements(x) then nrPSmooth=0
       if nrPSmooth gt 0 then begin
          ; Ausrei�er nicht gl�tten, diese zuerst mit Mittelwert ersetzen
          nrPSmooth=round(nrPSmooth/2)
          if nrPSmooth le 2 then nrPSmooth = 3
          temp=where (x ne mv,count)
          temp1=where (x eq mv,count1)
          if count1 gt 0 then begin
            mittel=mean(x[temp])
            x[temp1]=mittel
            savgolFilter = SAVGOL(nrPSmooth,nrPSmooth, 0, 2)
            x=CONVOL(x, savgolFilter, /EDGE_TRUNCATE)
            ;x=smooth(x,nrPSmooth)
          ; jetzt Ausrei�er wieder zur�ckkopieren
            x[temp1]=mv
          end else begin
           ;  x=smooth(x,nrPSmooth)
                 savgolFilter = SAVGOL(nrPSmooth,nrPSmooth, 0, 2)
                 x=CONVOL(x, savgolFilter, /EDGE_TRUNCATE)
          endelse

        end

       for i=0,n_elements(quest.normalize)-1 do $
         if quest.normalize(i) eq 1 then $
         case i of
          1:  x=diffGobalMean(x)                                   ; Abweichungen vom globalen Mittelwert (var freq)
          2:  x=diffGobalMean(x, /STD)                            ; std. Abweichungen vom globalen Mittelwert (var freq)
          3:  x=diffSeasonMean(x,normFreq,81,83,84,85,OUT=1)       ; Abweichung von saisonalen Monatswerten
          4:  x=diffSeasonMean(x,normFreq,81,83,84,85,OUT=1,STD=1) ; Abweichung von saisonalen Monatswerten
          5:  x=differences(x,normFreq)
          6:  begin                                                ; pixelweise saisonale Korrektur �ber FP
               for k=0,n_elements(x[*,0])-1 do begin
                temp=x[k,*]
                if min(temp) ne max(temp) then begin
                 if quest.detrendLin eq 1 then $ ; keine lineare Trendkomponente
                 fourierPoly, temp, quest.freq1, quest.freq2,quest.NoHarmonics1, quest.NoHarmonics2, $
                   fp, corrterm, CORR=1 else $
                 fourierPoly, temp, quest.freq1, quest.freq2,quest.NoHarmonics1, quest.NoHarmonics2, $
                    fp, corrterm, CORR=1, LIN=1
                end
                x[k,*]=temp
               endfor
              end
          7:  for k=0,n_elements(x[*,0])-1 do begin                 ; prewhitening
                temp=x[k,*]
                prewhite,temp,AR,MA,86,87,OUT=1
                x[k,*]=temp
              end

          8:  x=saisonAvg(x,normFreq)                    ; saisonale Mittelwerte
          9:  x=MVC(x,normFreq)                         ; MVC
          else: break
        endcase

       return,x
end
