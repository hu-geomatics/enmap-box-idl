pro base_widget4b_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase
 lag=0
 widget_control, ev2.id, get_uvalue=uval
 case uval of
    ;'group00': case ev2.value of
    ;             0: quest.diff=0  ; use no differences
    ;             1: quest.diff=1  ; use first order ts- differences
    ;           endcase
    'group01': case ev2.value of
                 0: begin
                       quest.wald=0  ; no Wald F-Statsitik for two regressions
                       WIDGET_CONTROL, trendpar3, SENSITIVE=0
                    end
                 1: begin
                       quest.wald=1  ; otherwise
                       WIDGET_CONTROL, trendpar3, SENSITIVE=1
                    end
               endcase
    'grouppw': case ev2.value of
                 0: quest.prewhite=0  ; no pre-whitening
                 1: quest.prewhite=1  ; otherwise
               endcase

    'group1R':  begin
                 if flagR[ev2.value] eq 0 then flagR[ev2.value]=1 else flagR[ev2.value]=0
                end
    'group1UR': begin
                 if flagUR[ev2.value] eq 0 then begin
                                                 flagUR[ev2.value]=1
                                                 WIDGET_CONTROL, xy3,   SENSITIVE=1
                                                end else begin
                                                 flagUR[ev2.value]=0
                                                 WIDGET_CONTROL, xy3,   SENSITIVE=0
                                                end
                 if flagUR[5] eq 1 then WIDGET_CONTROL, parX1, SENSITIVE=1 else WIDGET_CONTROL, parX1, SENSITIVE=0
                 if flagUR[6] eq 1 then WIDGET_CONTROL, parX2, SENSITIVE=1 else WIDGET_CONTROL, parX2, SENSITIVE=0
                 if flagUR[7] eq 1 then WIDGET_CONTROL, parX3, SENSITIVE=1 else WIDGET_CONTROL, parX3, SENSITIVE=0
                 if flagUR[8] eq 1 then WIDGET_CONTROL, parX4, SENSITIVE=1 else WIDGET_CONTROL, parX4, SENSITIVE=0
                 if flagUR[9] eq 1 then WIDGET_CONTROL, parX5, SENSITIVE=1 else WIDGET_CONTROL, parX5, SENSITIVE=0
                 if flagUR[10] eq 1 then WIDGET_CONTROL, FPpara,SENSITIVE=1 else WIDGET_CONTROL, FPpara, SENSITIVE=0
                end

    'group1Y' :begin
                WIDGET_CONTROL, _lagsY,  GET_VALUE=lag
                lags.lagsY=lag
                if lag gt 0 then flagUR[7] = 1 else flagUR[7] = 0
               end
    'group1X1':begin
                WIDGET_CONTROL, _lagsX1, GET_VALUE=lag
                lags.lagsX1=lag
               end
    'group1X2':begin
                WIDGET_CONTROL, _lagsX2, GET_VALUE=lag
                lags.lagsX2=lag
               end
    'group1X3':begin
                WIDGET_CONTROL, _lagsX3, GET_VALUE=lag
                lags.lagsX3=lag
               end
    'group1X4':begin
                WIDGET_CONTROL, _lagsX4, GET_VALUE=lag
                lags.lagsX4=lag
               end
    'group1X5':begin
                WIDGET_CONTROL, _lagsX5, GET_VALUE=lag
                lags.lagsX5=lag
               end
    'groupX': case ev2.value of
                 0: quest.gls=0
                 1: quest.gls=1
               endcase

    'groupX1': extFiles.fileX1=DIALOG_PICKFILE(/READ,  FILTER = '*.bil', TITLE='X1 input file')
    'groupX2': extFiles.fileX2=DIALOG_PICKFILE(/READ,  FILTER = '*.bil', TITLE='X2 input file')
    'groupX3': extFiles.fileX3=DIALOG_PICKFILE(/READ,  FILTER = '*.bil', TITLE='X3 input file')
    'groupX4': extFiles.fileX4=DIALOG_PICKFILE(/READ,  FILTER = '*.bil', TITLE='X4 input file')
    'groupX5': extFiles.fileX5=DIALOG_PICKFILE(/READ,  FILTER = '*.bil', TITLE='X5 input file')
    'Fourier': base_widgetFP2
    'nowind': WIDGET_CONTROL, _nwindDiff, GET_VALUE=nwindDiff
    'noover': WIDGET_CONTROL, _noverlapDiff, GET_VALUE=noverlapDiff

    'ok'  :   begin
                temp1=where([extFiles.fileX1,extFiles.fileX2,extFiles.fileX3,extFiles.fileX4,extFiles.fileX5] ne '',count1)
                temp2=where(flagUR[5:9] eq 1,count2) ; time und fp ausschließen
                if (count2 eq 0) and (min(flagUR[0:4] eq 0)) and (flagUR[10] eq 0) $
                 and (lags.lagsY eq 0) then para[6]=0 else para[6]=1 ; Any regression parameters selected?
                if (count1 ne count2) then $
                    info=dialog_message(info[6],/INFORMATION,TITLE='Time-series analysis') $
                else WIDGET_CONTROL, ev2.top,/DESTROY
              end
  endcase
end


pro base_widget4b, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
  values0  = ['no','yes']
  values01 = ['t','t^2','t^3','t^4','t^5','X1','X2','X3','X4','X5','FP']
  valuesX1 = ['File name for 1. external regressor X1']
  valuesX2 = ['File name for 2. external regressor X2']
  valuesX3 = ['File name for 3. external regressor X3']
  valuesX4 = ['File name for 4. external regressor X4']
  valuesX5 = ['File name for 5. external regressor X5']


  para[6]=0

  trendpar=  WIDGET_BASE(TITLE='Advanced regression analysis',/COLUMN)
  trendpar1=  WIDGET_BASE(trendpar,/ROW)
  ;bgroup00  = CW_BGROUP(trendpar1,values0,/ROW,SET_VALUE=quest.diff, $
  ;                      LABEL_TOP='Difference time-series?',/FRAME, /EXCLUSIVE,UVALUE='group00')
  bgroup01  = CW_BGROUP(trendpar1,values0,/ROW,SET_VALUE=quest.wald, $
                        LABEL_TOP='Compare a restricted and unrestricted regression model?',/FRAME, /EXCLUSIVE,UVALUE='group01')
  ; restricted parameters
  trendpar3=  WIDGET_BASE(trendpar,/ROW)
  trendparR=  WIDGET_BASE(trendpar3,TITLE='Select regressors for restricted model',/COLUMN)

  bgroup1R= CW_BGROUP(trendparR,values01,/ROW, SET_VALUE=flagR, LABEL_TOP='Restricted regression parameters',/FRAME, /NONEXCLUSIVE,UVALUE='group1R')


  ; unrestricted parameters
  trendpar2=  WIDGET_BASE(trendpar,/ROW)
  trendparUR=  WIDGET_BASE(trendpar2,TITLE='Select regressors for unrestricted model',/COLUMN)
  bgroup1UR= CW_BGROUP(trendparUR,values01,/ROW,SET_VALUE=flagUR,LABEL_TOP='Unrestricted regression parameters',/FRAME, /NONEXCLUSIVE,UVALUE='group1UR')


  parY  = WIDGET_BASE(trendparUR, /COLUMN)
  _lagsY = CW_FIELD(parY,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsYR,TITLE='Lags: Y', UVALUE='group1Y')

  FPpara  = WIDGET_BASE(trendparUR,  /ROW)
  _FPpar= widget_button(FPpara, VALUE='Fourier polyn. parameter', UVALUE='Fourier')

  parX1  = WIDGET_BASE(trendparUR, /ROW)
  bgroupX1= CW_BGROUP(parX1,valuesX1,/COLUMN,UVALUE='groupX1')
  _lagsX1 = CW_FIELD(parX1,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsX1R,TITLE='Lags: X1',UVALUE='group1X1')

  parX2  = WIDGET_BASE(trendparUR,  /ROW)
  bgroupX2= CW_BGROUP(parX2,valuesX2,/COLUMN, UVALUE='groupX2')
  _lagsX2 = CW_FIELD(parX2,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsX2R,TITLE='Lags: X2',UVALUE='group1X2')

  parX3  = WIDGET_BASE(trendparUR,  /ROW)
  bgroupX3= CW_BGROUP(parX3,valuesX3,/COLUMN,UVALUE='groupX3')
  _lagsX3 = CW_FIELD(parX3,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsX3R,TITLE='Lags: X3',UVALUE='group1X3')

  parX4  = WIDGET_BASE(trendparUR,  /ROW)
  bgroupX4= CW_BGROUP(parX4,valuesX4,/COLUMN,UVALUE='groupX4')
  _lagsX4 = CW_FIELD(parX4,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsX4R,TITLE='Lags: X4',UVALUE='group1X4')

  parX5  = WIDGET_BASE(trendparUR,  /ROW)
  bgroupX5= CW_BGROUP(parX5,valuesX5,/COLUMN,UVALUE='groupX5')
  _lagsX5 = CW_FIELD(parX5,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsX5R,TITLE='Lags: X5',UVALUE='group1X5')


  bgroupX  = CW_BGROUP(trendparUR,values0,/ROW,SET_VALUE=quest.gls, $
                        LABEL_TOP='GLS parameter estimation',/FRAME, /EXCLUSIVE,UVALUE='groupX')

  bgroupX  = CW_BGROUP(trendparUR,values0,/ROW,SET_VALUE=quest.gls, $
                        LABEL_TOP='Automated model identification for multivariate distributed lag (DL-) models',/FRAME, /EXCLUSIVE,UVALUE='groupX')

  xy3= WIDGET_BASE(trendparUR, TITLE='Parameter for windowed trend analysis',/COLUMN)
  _nwindDiff=   CW_FIELD(xy3,/INTEGER,/ALL_EVENTS,VALUE=nwindDiff, $
     TITLE='Length of moving window for lin. windowed trend-analysis',UVALUE='nowind')
  _noverlapDiff=CW_FIELD(xy3,/INTEGER,/ALL_EVENTS,VALUE=noverlapDiff, $
     TITLE='Overlapping points',UVALUE='noover')

  endbase = WIDGET_BASE(trendpar, /ROW,/ALIGN_BOTTOM)
  ok= widget_button(endbase, VALUE='ok', UVALUE='ok')
  WIDGET_CONTROL, trendpar, /NO_COPY,/REALIZE
  WIDGET_CONTROL, parY, SENSITIVE=1

  if quest.wald eq 1 then WIDGET_CONTROL, trendpar3, SENSITIVE=1 else $
  WIDGET_CONTROL, trendpar3, SENSITIVE=0
  WIDGET_CONTROL, parX1, SENSITIVE=0
  WIDGET_CONTROL, parX2, SENSITIVE=0
  WIDGET_CONTROL, parX3, SENSITIVE=0
  WIDGET_CONTROL, parX4, SENSITIVE=0
  WIDGET_CONTROL, parX5, SENSITIVE=0
  WIDGET_CONTROL, xy3,   SENSITIVE=0
  if flagUR[10] eq 1 then WIDGET_CONTROL, FPpara,SENSITIVE=1 else WIDGET_CONTROL, FPpara, SENSITIVE=0

  XMANAGER,'base_widget4b', trendpar, GROUP=GROUP1
end
