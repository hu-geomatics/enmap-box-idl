function saisonAvg,x,freq
; computes saisonal averages in dependance from freq
; x=cols * time vector
     ;freq1=freq
     samples=floor(n_elements(x[0,*])/freq)  ; restliche Werte abschneiden
     buf=fltarr(n_elements(x[*,0]))
     ; if (samples eq 0) then begin
     ;     message,'Not enough samples for averaging!!, exit...'
     ;     stop
     ; end

     xNeu=x[*,0:samples-1]
     for i=0,samples-1 do begin
       for m=0, freq-1 do buf=buf+x[*,i*freq+m]
            buf=buf/freq  ; Mittelwert
            xNeu[*,i]=buf
            buf=buf*0
       end
         x=float(xNeu)
         return,x
end