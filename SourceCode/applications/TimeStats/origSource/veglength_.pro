function VegLength, y, yext, vegMA
 ; Zeitreihe glätten
 ;fourierPoly, y, 12, 0, 2, 0, fp, 0,/LIN,/FIT

 Maximum=max(y)
 Maxpeak=where(y eq Maximum)
 Maxpeak=Maxpeak[0]

 Minimum=min(y[0:Maxpeak])  ; Problem wenn zweites Minimum < erstes Minimum ist
 Minpeak=where(y eq Minimum)
 Minpeak=Minpeak[0]         ; Indizierung ab 0!

 y1=y[0:Maxpeak]              ; erster  Teil der Zeitreihe
 y2=y[Maxpeak:n_elements(y)-1]; zweiter Teil der Zeitreihe
 Minimum2=min(y2)
 Minpeak2=where(y2 eq Minimum2)
 Minpeak2=Minpeak2[0] + Maxpeak

 Amplitude = Maximum - Minimum

 b=TS_SMOOTH(yext, vegMA,/backward)
 b=b[vegMA:n_elements(b)-vegMA-1] ; Bereich der ursprünglichen Zeitreihe wieder ausschneiden
 diff=y1-b[0:Maxpeak]
 temp1th=where(diff ge 0,count)
 ;if count gt 0 then start=temp1th[0] else start = Minpeak  ; Suche nach 1. Schnittpunkt
 if count gt 0 then start=temp1th[count-1] else start = Minpeak  ; Suche nach 1. Schnittpunkt

 f=TS_SMOOTH(yext, vegMA,/forward)
 f=f[vegMA:n_elements(f)-vegMA-1] ; Bereich der ursprünglichen Zeitreihe wieder ausschneiden
 diff= y2 - f[Maxpeak:n_elements(y)-1]
 temp2th=where(diff le 0,count)
 ;if count gt 0 then ende=temp2th[0] + Maxpeak else ende = Minpeak2  ; Suche nach 1. Schnittpunkt
 if count gt 0 then ende=temp2th[count-1] + Maxpeak else ende = Minpeak2  ; Suche nach 1. Schnittpunkt

 vlength=ende-start
 ;plot,y,title=strjoin(['Länge: ',string(vlength),'  Phase: ', string(Maxpeak),'  Amplitude: ',string(Amplitude)])
 ;oplot,b
 ;oplot,f

 return,vlength
end