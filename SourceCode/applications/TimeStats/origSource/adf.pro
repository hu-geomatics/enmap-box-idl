pro ADF, x,y,lagsADF, ADFkoeff
   ; computes augmented Dickey-Fuller Test, using the model with trend and constant
   ; Code f�r ADF Test mit Konstante und Trendkomponente
   ; x: time
   ; y: time-series
   ; lagsADF: no. of lags
   ; ADFkoeffs: Array with regression coefficients
   ; Adopted from matlab code from Kanzler

         ; Compute total no. of observations in the sample and the series of the first differences
         obs=n_elements(x);
         dy =y[1:obs-1]-y[0:obs-2]

         ; Create the matrices for the dependent and independent variables
         xADF=fltarr(2+lagsADF,n_elements(y[lagsADF:obs-2]))

         if lagsADF gt 0 then for h=1,lagsADF do xADF[1+h,*]=dy[lagsADF-h:obs-2-h]
         noXvar=n_elements(xADF[*,0])
         xADF[0,*]=x[lagsADF:obs-2] ; time-variable
         xADF[1,*]=y[lagsADF:obs-2] ; time-series
         dy=dy[lagsADF:obs-2]       ; deltay
         result=regress(xADF,dy,Yfit=Yfit,CONST=ADFConst,SIGMA=ADFSigma,CORRELATION=ADFr)
         ADFkoeff[1:noXvar]=result
         ADFkoeff[0]=ADFConst

         ; Alterative Berechnung des Standardfehlers der Reg.koeff. und der Reg konstanten!!! (siehe adfreg.m von Kanzler)
         ;resid  = dy-ADFYfit
         ;rss    = transpose(resid) # resid ; residual sum of squares
         ;sgma2  = rss / (obs-2-lagsADF)
         ;sgma2  = sgma2[0] ;convert to scalar
         ;sigma  = sqrt(sgma2)
         ;ones   = transpose(REPLICATE(1, n_elements(xADF[0,*])))
         ;xadf   = [ones, xadf]        ; insert a vector
         ;se=sqrt(diag_matrix(sgma2 * invert(xADF # transpose(xADF))))
         ;t=ADFkoeff[0:n_elements(xADF[*,0])-1]/se
         t=ADFkoeff[1:noXvar]/ADFSigma

         ; perform unit-root significance test (coeff. from Kanzler) for model with constant and trend
         binf=[-3.9638, -3.4126, 3.1279]
         b1=  [-8.353, -4.039, -2.418]
         b2=  [-47.44, -17.83, -7.58]
         crit=binf + b1 / obs +b2 /obs^2
         case total(t[1] le crit) of
           0: ADFkoeff[noXvar+2] = 1;  Pos in ADFkoeff for sig. of yt-1
           1: ADFkoeff[noXvar+2] = 0.1;
           2: ADFkoeff[noXvar+2] = 0.05;
           3: ADFkoeff[noXvar+2] = 0.01;
         endcase
         ; Significance of trend-parameter
         df=obs-1-lagsADF
         ADFkoeff[noXvar+1] = min([1 - T_PDF(t[0], df), T_PDF(t[0],df)])*2
         if lagsADF gt 0 then $
          for h=0, lagsADF-1 do ADFkoeff[noXvar+3+h] = min([1 - T_PDF(t[2+h], df), T_PDF(t[2+h], df)])*2
         ; emp. t values for reg.parameter
         temp=n_elements(ADFkoeff)-1 - n_elements(t)
         ADFkoeff[temp:n_elements(ADFkoeff)-2]=t
         resid=dy-yfit
         ADFkoeff[n_elements(ADFkoeff)-1]=dwtest(resid)
    end

