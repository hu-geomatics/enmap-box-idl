pro arma,y,R,M,AR,MA
AR=0
MA=0
;y=[1,2,3,4,3,5,6,7,8,7,6,5,4,5,6,7,8,9,10,11,12,14,13,12,11,10,9,8,7,6,5,6,7,8,9,10,2,3,4,5,6,7,8,9,20,21,22,23,24,25]
;
; Estimate the AR coefficients of a general ARMA(R,M) model.
;

if R gt 0 then begin
   AR=fltarr(R)

;  Compute the auto-covariance sequence of the y(t) process. Note that the
;  variance (i.e, zeroth-lag auto-covariance) is found in the first element.
   lag=indgen(R+M+1)
   correlation =  a_correlate(y,lag)
   var         =  variance(y)
   covariance  =  correlation * var;       auto-covariance sequence.

;
;  In each case below, the matrix 'C' of covariances is
;  that outlined in equation A6.2.1 (page 220) of BJR.
;

   if M gt 0 then begin
;
;     For ARMA processes, the matrix C of covariances derived from the
;     estimated auto-covariance sequence is Toeplitz, but non-symmetric.
;     The AR coefficients are then found by solving the modified Yule-Walker
;     equations.
;
      i=indgen((M+1)-(M-R+2)+1)
      z=0
      for j=M+1,M-R+2,-1 do begin
         i[z]=M+1-z
         z=z+1
      end
      temp=where(i le 0,count)
      if count gt 0 then i[temp]=i[temp]+2
     ; covariance(k) = covariance(-k)
      C  =  toeplitz(covariance[M:M+R-1] , covariance[i-1]);
      if R eq 1 then $
         AR  =  covariance(M+1:M+R) ## invert(C) $;
      else $
         AR  =  invert(C) ## covariance(M+1:M+R);

   end else begin
   if R eq 1 then AR  =  correlation(1) $
      else begin

       ; For AR processes, the matrix C of covariances derived from the
       ; estimated auto-covariance sequence is Toeplitz and symmetric.
       ; The AR coefficients are found by solving the Yule-Walker equations.

         C   =  toeplitz(covariance[0:R-1],HERM=1)
         AR  =  invert(C) ## covariance(1:R)

      end
   endelse
 end else AR[*]  =  -999;

; print,ar

 ;
 ; Filter the ARMA(R,M) input series y(t) with the estimated AR coefficients
 ; to obtain a pure MA process. If the input moving-average model order M is
 ; zero (M = 0), then the filtered output is really just a pure innovations
 ; process (i.e., an MA(0) process); in this case the innovations variance
 ; estimate is just the sample variance of the filtered output. If M > 0, then
 ; compute the auto-covariance sequence of the MA process and continue.
 ;

 x        =  filterAR(y,ar);
 constant =  mean(x);

 if M eq 0 then begin
   var =  variance(x);
   MA       =  -999;
   return
 end

 MA=fltarr(M)
 lag=indgen(M+1)
 c  =  a_correlate(x,lag) * variance(x);     Covariance of an MA(M) process.

 ;
 ; Estimate the variance of the white noise innovations process e(t)
 ; and the MA coefficients of a general ARMA(R,M) model. The method of
 ; computation is that outlined in equation A6.2.4 (page 221) of BJR.
 ;

 MA      =  fltarr(M);   Initialize MA coefficients.
 MA1     =  fltarr(M)+1; Saved MA coefficients from previous iteration.
 counter =  1;           Iteration counter.
 tol     =  0.05;        Convergence tolerance.


repeat begin

;
;   Estimate the variance of the innovations process e(t).
    MA1  =  MA;
    var  =  c(0) ##  invert([1,MA] ## transpose([1,MA]))

    if abs(var) lt tol then  break
    for j = M,1,-1 do begin
;       Now estimate the moving-average coefficients. Note that
;       the MA coefficients are the negative of those appearing
;       in equation A6.2.4 (page 221) of BJR. This is due to the
;       convention of entering coefficient values, via GARCHSET,
;       exactly as the equation would be written.
      ; MA(j)  =  [c(j+1) ; -MA(1:M-j)]' * [1/variance ; MA(j+1:M)];
        if M-j-1 ge 0 then begin
           fact1=MA[0:M-j-1]*(-1)
           fact2=MA[j:M-1]
           MA[j-1]  =  [c[j], fact1] ## transpose([1/var, fact2]);
        end else MA[j-1]  =  c[j] ## transpose([1/var])



    endfor


    if n_elements(MA eq 1) then W=abs(MA-MA1) else  SVDC, transpose(MA-MA1), W, U, V
    counter=counter+1
    ;print,W
    endrep until (max(W) le tol) or (counter gt 100)
end  ; arma