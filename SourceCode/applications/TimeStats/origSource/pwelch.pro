 function pwelch,x,nwind,windtype,noverlap
 ; x: time-series
 ; nwind: length of the window, determines the no. of sections
 ; windtype: 'HANN','HAMM','BARTLETT', 'NO'
 ; noverlap: no. of overlapping points by moving window (default: 50% overlapping, if value < 0)

 ;PWELCH Power Spectral Density estimate via Welch's method.
 ;   Pxx = PWELCH(X) returns the Power Spectral Density (PSD) estimate,
 ;   Pxx, of a discrete-time signal vector X using Welch's averaged,
 ;   modified periodogram method. X is divided into several
 ;   sections, depending on the window length nwind and the no of overlapping points
 ;   default: 50%. Each section is windowed according to windtype.
 ;   The modified periodograms are computed and averaged. ;
 ;   If the number of sections is not a multiple of the length of X the default periodogram is calculated.
 ;   (that is nwind = lenght x)

   x = reform(x)
   M = float(n_elements(x));

   ; Obtain the necessary information to segment X

   if nwind gt M then begin
      print,'The length of the segments cannot be greater than the length of the input signal. Using default...'
      nwind=M ; Length of window equals lenght of x
   end
   if noverlap ge nwind then begin
      print, 'The number of samples to overlap must be less than the length of the segments. Using default...';
      noverlap=-1
   end

   if noverlap lt 0 then noverlap = fix(0.5 *nwind); use default:50% overlap

   ; Compute the number of segments
   k = (M-noverlap)/(nwind-noverlap);
   if k ne fix(k) then begin
      print, 'No. of segments is not a multiple from lenght(x). Using default...';
      nwind=M
      k=1
      noverlap=0
   end

   ; Compute the periodogram power spectrum of each segment and average
   ; always compute the twosided power spectrum, we force Fs = 1 to get
   ; a spectrum not a spectral density
   Sxx = fltarr(M); Initialize
   xindx = 1
   index=indgen(nwind)


   for i = 0,k-1 do begin
     Sxx = Sxx + periodogram(x[index],windType,1,'Hertz',NFFT=M);
     index = index + nwind - noverlap;
   end
   Sxx = Sxx / k; ; Average the sum of the periodograms
   return, Sxx
end


