function filterAR, y,ar
  ; filtert den AR process aus y heraus
  order=n_elements(ar)
  y=float(y)
  N=n_elements(y)
  x=y
  for i=1,order-1 do begin
    corr=0
    for j=0,i-1 do corr=corr+ar[j]*y[i-(j+1)]
    x[i]=y[i]-corr
  end

  for i=order,n-1 do begin
    corr=0
    for j=0,order-1 do corr=corr+ar[j]*y[i-(j+1)]
    x[i]=y[i]-corr
  end

  return,x
end