function differences, x,lags
   nobs=n_elements(x[0,*])
   for k=0,n_elements(x[*,0])-1 do begin
     temp=x[k,*]
     temp1=temp*0
     ;temp1[0:nobs-lags-1]=temp[lags:nobs-1]-temp[0:nobs-lags-1]
     temp1[lags:nobs-1]=temp[lags:nobs-1]-temp[0:nobs-lags-1]
     x[k,*]=temp1
   end
   return,x
end
