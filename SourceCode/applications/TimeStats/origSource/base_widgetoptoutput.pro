pro base_widgetOptOutput_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
 'group1': case ev2.value of

             0: begin
                 oldFilename = hub_getAppState('TimeStats', 'outputFilename0')
                 ofile=DIALOG_PICKFILE(/WRITE, /OVERWRITE_PROMPT, FILTER = '*.bil', TITLE='Select output image file with regression parameters', FILE=oldFilename)
                 if ofile ne '' then begin
                   para[1]=1
                   hub_setAppState, 'TimeStats', 'outputFilename0', ofile
                 endif
                 print, ofile
                 
                end
             1: begin
                 oldFilename = hub_getAppState('TimeStats', 'outputFilename1')
                 ofile_ori=DIALOG_PICKFILE(/WRITE, /OVERWRITE_PROMPT, FILTER = '*.bil', TITLE='Select stacked output image with original input information', FILE=oldFilename)
                 if ofile_ori ne '' then begin
                   para[2]=1
                   hub_setAppState, 'TimeStats', 'outputFilename1', ofile_ori
                 endif
                 print, ofile_ori
                end
             2: begin
                 oldFilename = hub_getAppState('TimeStats', 'outputFilename2')
                 hotspots=DIALOG_PICKFILE(/WRITE, /OVERWRITE_PROMPT, FILTER = '*.bil', TITLE='Select stacked output image with hot-spots', FILE=oldFilename)
                 if hotspots ne '' then begin
                   para[5]=1
                   hub_setAppState, 'TimeStats', 'outputFilename2', hotspots
                 endif
                 print, hotspots
                end
             3: begin
                  oldFilename = hub_getAppState('TimeStats', 'outputFilename3')
                  fourierfile=DIALOG_PICKFILE(/WRITE, /OVERWRITE_PROMPT, FILTER = '*.bil', TITLE='Output file for FFT-results', FILE=oldFilename)
                  if fourierfile ne '' then begin
                    para[4]=1
                    hub_setAppState, 'TimeStats', 'outputFilename3', fourierfile
                  endif
                  print, fourierfile
                end
           endcase
 'ok'  :    WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widgetOptOutput, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
 values1 = ['Regression parameters', $
            'Output stacked input images','Output stacked difference images','Output two-sided periodogram/PSD/ACF']

  sizebase = WIDGET_BASE(TITLE='Optional inputs',/COLUMN)
  bgroup1=  CW_BGROUP(sizebase,values1,/COLUMN, LABEL_TOP='Optional output filenames',/FRAME,UVALUE='group1')
  endbase = WIDGET_BASE(sizebase, /ROW,/ALIGN_BOTTOM)
  ok= widget_button(endbase, VALUE='ok', UVALUE='ok')

  WIDGET_CONTROL, sizebase, /NO_COPY,/REALIZE
  XMANAGER,'base_widgetOptOutput', sizebase, GROUP=GROUP1
end

