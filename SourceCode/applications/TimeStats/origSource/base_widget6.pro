pro base_widget6_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase


;if workDir eq '' then WIDGET_CONTROL, generic2, SENSITIVE=0


 widget_control, ev2.id, get_uvalue=uval
 WIDGET_CONTROL, ev2.TOP, GET_UVALUE=textwid
 case uval of
  'groupCol1':begin
                 WIDGET_CONTROL, _col1, GET_VALUE=temp
                 quest.colFirst=temp-1
                 if n_elements(filelist) gt 1 then begin
                    quest.colLast=temp
                    if quest.colLast lt quest.colFirst then quest.colLast=n_elements(filelist[0])
                    ;sortieren der Filelist
                    sortlist=filelist
                    for i=0,n_elements(filelist)-1 do sortlist[i]=strmid(filelist[i],quest.colFirst,quest.colLast)
                    writeFilelist,sortlist

                    if n_elements(filelist) eq 1 then WIDGET_CONTROL, textwid, SET_VALUE=file else $
                    WIDGET_CONTROL, textwid, SET_VALUE=filelist
                 end
              end
  'groupCol2':begin
                 WIDGET_CONTROL, _col2, GET_VALUE=temp
                 if n_elements(filelist) gt 1 then begin
                    quest.colLast=temp
                    if quest.colLast lt quest.colFirst then quest.colLast=n_elements(filelist[0])
                    ;sortieren der Filelist
                    sortlist=filelist
                    for i=0,n_elements(filelist)-1 do sortlist[i]=strmid(filelist[i],quest.colFirst,quest.colLast)

                    writeFilelist,sortlist
                    if n_elements(filelist) eq 1 then WIDGET_CONTROL, textwid, SET_VALUE=file else $
                    WIDGET_CONTROL, textwid, SET_VALUE=filelist
                  end
              end
  'group2_':begin
              list=DIALOG_PICKFILE(/READ,/MULTIPLE_FILES, FILTER = ['*.txt','*.bil','*.bsq','*.tif'], TITLE='Add files')
           if n_elements(list) gt 1 then ok_=1 else $
             if list ne '' then ok_=1 else ok_=0
              if ok_ eq 1 then begin

                file='files.txt'
                filelist=[filelist,list]
                WIDGET_CONTROL, generic3, SENSITIVE=1
                if n_elements(filelist) eq 1 then WIDGET_CONTROL, textwid, SET_VALUE=file else $
                WIDGET_CONTROL, textwid, SET_VALUE=filelist
               ;#######################
                     ;for v=0,n_elements(filelist)-1 do begin
                     ;   parts=STR_SEP(filelist[v],slash)
                     ;   filelist[v]=parts(n_elements(parts)-1)
                     ;end
                OPENW, 1, 'files.txt'
                      ;for i=0,n_elements(filelist)-1 do printf,1,strcompress(workDir+filelist[i])
                for i=0,n_elements(filelist)-1 do printf,1,filelist[i]
                close,1
              end
            end
  'group1_': case ev2.value of
             0: begin
                 workDir = ''
                 quest.tif =0
                 WIDGET_CONTROL, generic4, SENSITIVE=0
                 WIDGET_CONTROL, generic4b, SENSITIVE=0
                 
                 ;######################
                 ; EnMAP-Box integration
                 ; Choose from EnMAP-Box Filelist
                 
                 answer = dialog_message(/QUESTION, 'Select file from EnMAP-Box Filelist?')
                 if answer eq 'Yes' then begin
                   filelist = TimeStats_dialogSelectInputStack(ev2.TOP)
                 endif else begin
                   filelist=DIALOG_PICKFILE(/READ,/MULTIPLE_FILES, FILTER = ['*.txt','*.bil','*.bsq','*.tif'], TITLE='ENVI-stacked image file (BIL)/ASCII-Generic binaryimage list')
                 endelse
                 
                 ;######################


                if n_elements(filelist) gt 1 then ok_=1 else $
                   if filelist ne '' then ok_=1 else ok_=0
                   if ok_ eq 1 then begin

                    if n_elements(filelist) eq 1 then temp=filelist else temp=filelist[0]
                    if n_elements(STR_SEP(temp,'\')) gt 1 then slash='\' else slash='/'  ; DOS- oder UNIX-System

                    parts=STR_SEP(temp,slash)
                    for v=0,n_elements(parts)-2 do workDir=strcompress(workDir+parts(v)+slash)
                    print,'Working Directory: ',workDir
                    if workDir ne '' then begin
                      WIDGET_CONTROL, generic2, SENSITIVE=1
                      ofile=strcompress(workDir+'trend.bil')
                      ofile_ori=strcompress(workDir+'stack.bil')
                      hotspots=strcompress(workDir+'differences.bil')
                      fourierfile=strcompress(workDir+'fourier.bil')
                      para[1]=1 & para[2]=1 & para[5]=1 & para[4]=1
                      print,para
                      WIDGET_CONTROL,optionbase, SENSITIVE=1
                    end

                if n_elements(filelist) eq 1 then begin
                  WIDGET_CONTROL, generic5, SENSITIVE=0
                  file=filelist
                  parts=STR_SEP(file,slash)
                  parts=STR_SEP(parts[n_elements(parts)-1],'.')
                  if n_elements(parts) eq 2 then $ ; Dateiname mit Extention ?
                      test=strcompress(parts[n_elements(parts)-2]+'.hdr') $ ; pr�fen ob ein Headerfile vorhanden ist!!!
                     else test=strcompress(parts[n_elements(parts)-1]+'.hdr')
                     OPENR, 1, strcompress(workDir+test), ERROR = err
                     if err eq 0 then begin  ; Eingabefile ist ein Batchfile
                        close,1
                        batch=1 ; Batchfile
                        WIDGET_CONTROL, generic4, SENSITIVE=0
                        WIDGET_CONTROL, generic4b, SENSITIVE=0
                     end else begin
                        batch =0       ; Eingabefile ist kein Batchfile sondern enth�lt ASCII-Liste
                 ; pr�fen nach tif-files
                 openr,1,file
                 zeile=''
                 readf,1,zeile
                        parts=STR_SEP(zeile,'.')
                        if (STRUPCASE(parts[n_elements(parts)-1] eq 'tif')) or (STRUPCASE(parts[n_elements(parts)-1] eq 'tiff')) then $
                         begin
                           quest.tif=1
                           WIDGET_CONTROL, generic4, SENSITIVE=0
                           WIDGET_CONTROL, generic4b, SENSITIVE=0
                         end else begin
                           quest.tif =0
                           WIDGET_CONTROL, generic4, SENSITIVE=1
                           WIDGET_CONTROL, generic4b, SENSITIVE=1
                         endelse
                     endelse


                 end else begin
                    file='files.txt'
                 WIDGET_CONTROL, generic5, SENSITIVE=1
                    if n_elements(filelist) eq 1 then WIDGET_CONTROL, textwid, SET_VALUE=file else $
                    WIDGET_CONTROL, textwid, SET_VALUE=filelist
;#######################
                     ;for v=0,n_elements(filelist)-1 do begin
                     ;   parts=STR_SEP(filelist[v],slash)
                     ;   filelist[v]=parts(n_elements(parts)-1)
                     ;end
                     OPENW, 1, 'files.txt'
                     ;for i=0,n_elements(filelist)-1 do printf,1,strcompress(workDir+filelist[i])
                     for i=0,n_elements(filelist)-1 do printf,1,filelist[i]
                     close,1

                     batch=0 ; keine Batchfile, filelist
                     parts=STR_SEP(filelist[0],'.')
                     if (STRUPCASE(parts[n_elements(parts)-1] eq 'tif')) or (STRUPCASE(parts[n_elements(parts)-1] eq 'tiff')) then $
                         begin
                           quest.tif=1
                           WIDGET_CONTROL, generic4, SENSITIVE=0
                           WIDGET_CONTROL, generic4b, SENSITIVE=0
                         end else begin
                           quest.tif =0
                           WIDGET_CONTROL, generic4, SENSITIVE=1
                           WIDGET_CONTROL, generic4b, SENSITIVE=1
                         endelse
                 endelse

                 if n_elements(filelist) ne '' then para[0]=1
                 ;print, file


            if n_elements(filelist) eq 1 then WIDGET_CONTROL, textwid, SET_VALUE=file else $
            WIDGET_CONTROL, textwid, SET_VALUE=filelist

             end
             end
            endcase

    'group':   begin
                 case ev2.value of
                  0: quest.resid=-1;  no layer stack
                  1: quest.resid=0 ;  y
                  2: quest.resid=1 ;  resid
                  3: quest.resid=2 ;  yfit
                 endcase
              if quest.resid ge 0 then WIDGET_CONTROL, generic3, SENSITIVE=1 else $
              WIDGET_CONTROL, generic3, SENSITIVE=0
               end
    'groupout':  case ev2.value of
                  0: quest.outFormat=0
                  1: quest.outFormat=1
                  2: quest.outFormat=2
                end

    ;'group0': begin
    ;           case ev2.value of
    ;             0: begin
    ;                 lan=4 ;ENVI
    ;                 lan2=0
    ;                 WIDGET_CONTROL, sizebase, SENSITIVE=0
    ;                end
    ;             1: begin
    ;                 WIDGET_CONTROL, sizebase, SENSITIVE=1
    ;                 lan=3 ; Voreinstellung byte f�r generic binary
    ;                 lan2=1
    ;               end
    ;           endcase
    ;          end
    'groupGenBin':base_widgetGenBin
    'groupOptOutput':base_widgetOptOutput
    'group0_': begin
                case ev2.value of
                 0: begin ; GB float
                    lan=0
                    lan3=0  ; f�r GUI Kontrolle
                   end
                 1: begin
                    lan=2 ;GB integer
                    lan3=1  ; f�r GUI Kontrolle
                   end
                 2: begin
                    lan=3 ;GB byte
                    lan3=2  ; f�r GUI Kontrolle
                   end
                endcase
                end
    'group0__':begin
                case ev2.value of
                 0:  BOrder=0 ; INTEL
                 1:  BOrder=1 ; MOTOROLA
                endcase
                end
    'group01':   WIDGET_CONTROL, _cols, GET_VALUE=cols
    'group02':   WIDGET_CONTROL, _rows, GET_VALUE=rows
    'group03':   WIDGET_CONTROL, _imagebands, GET_VALUE=imagebands
    'group5':    WIDGET_CONTROL, _band, GET_VALUE=band
    'ok'  :      WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widget6, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
  values=   ['no','original/preprocessed','residuals','Yfit']
  ;values0 = ['ENVI (BIL)','Generic binary (BIL)']
  valuesb= ['byte','integer','float']

  values1_= ['ENVI (BIL,BSQ) temporal stack-file or single tif/generic binary image files']
  values1 = ['Regression parameters', $
            'Output stacked input images','Output stacked difference images','Output two-sided periodogram/PSD/ACF']
  genvalue= ['Generic binary options']
  opt=      ['Optional filenames to save results']

  values0_= ['float','integer','byte']
  values0__=['Intel','Motorola']
  values3  =['Add data']


  generic=  WIDGET_BASE(TITLE='Input/output file options',/COLUMN)
  bgroup1_= CW_BGROUP(generic,values1_,/COLUMN, LABEL_TOP='Input file',/FRAME,UVALUE='group1_')
  text = WIDGET_TEXT(bgroup1_, VALUE='',XSIZE=50,YSIZE=10,/SCROLL)

  generic5=  WIDGET_BASE(generic,/ROW)
  bgroup2_= CW_BGROUP(generic5,values3,/COLUMN, /FRAME,UVALUE='group2_')

  generic4=  WIDGET_BASE(generic,/ROW)
  ;_col1= CW_FIELD(generic4,/INTEGER,/ALL_EVENTS,VALUE=quest.colFirst,TITLE='Sort files from column',UVALUE='groupCol1')
  ;_col2= CW_FIELD(generic4,/INTEGER,/ALL_EVENTS,VALUE=quest.colLast,TITLE='To column',UVALUE='groupCol2')
  generic4b=  WIDGET_BASE(generic,/COLUMN)

  bgroup0_= CW_BGROUP(generic4b,values0_,/ROW,SET_VALUE=lan3,LABEL_TOP='Define generic binary format', $
                    /FRAME,/EXCLUSIVE,UVALUE='group0_')
  sizebase2 = WIDGET_BASE(generic4b, /row)
  _cols=CW_FIELD(sizebase2,/INTEGER,/ALL_EVENTS,  YSIZE=70, VALUE=cols,TITLE='cols',UVALUE='group01')
  _rows=CW_FIELD(sizebase2,/INTEGER,/ALL_EVENTS,VALUE=rows,TITLE='rows',UVALUE='group02')
  _imagebands=CW_FIELD(sizebase2,/INTEGER,/ALL_EVENTS,VALUE=imagebands,TITLE='bands',UVALUE='group03')
  _BOrder= CW_BGROUP(generic4b,values0__,/ROW,SET_VALUE=BOrder,LABEL_TOP='byte order',/FRAME,/EXCLUSIVE,UVALUE='group0__')
  sizebase3 = WIDGET_BASE(generic4b, /row)
  _band=   CW_FIELD(sizebase3,/INTEGER,/ALL_EVENTS,VALUE=band, $
           TITLE='Band number to be processed',UVALUE='group5')



  generic2=  WIDGET_BASE(generic,/COLUMN)
  bgroup =  CW_BGROUP(generic2,values,/ROW,SET_VALUE=quest.resid+1,LABEL_TOP='Create layer stack',/FRAME,/EXCLUSIVE,UVALUE='group')
  generic3=  WIDGET_BASE(generic,/COLUMN)

  bgroup2 = CW_BGROUP(generic3,valuesb,/ROW,SET_VALUE=quest.outFormat,LABEL_TOP='Output Data format (stack)',/FRAME,/EXCLUSIVE,UVALUE='groupout')
  ;bgroup0=  CW_BGROUP(generic2,values0,/ROW,SET_VALUE=lan2,LABEL_TOP='Input file format',/FRAME,/EXCLUSIVE,UVALUE='group0')
  bgroup00_= CW_BGROUP(generic2,opt,UVALUE='groupOptOutput')

  endbase = WIDGET_BASE(generic, /ROW,/ALIGN_BOTTOM)
  ok= widget_button(endbase, VALUE='ok', UVALUE='ok')
  WIDGET_CONTROL, generic, /NO_COPY,/REALIZE
  ;WIDGET_CONTROL, sizebase, SENSITIVE=0
  if quest.resid eq -1 then WIDGET_CONTROL, generic3, SENSITIVE=0 else WIDGET_CONTROL, generic3, SENSITIVE=1

 if workDir eq '' then WIDGET_CONTROL, generic2, SENSITIVE=0
 ;case lan2 of
 ;   0: begin ;ENVI
 ;       WIDGET_CONTROL, sizebase, SENSITIVE=0
 ;      end
 ;   1: begin ;GENERIC BINARY
 ;       WIDGET_CONTROL, sizebase, SENSITIVE=1
 ;      end
 ;endcase

 XMANAGER,'base_widget6', generic, GROUP=GROUP1
  WIDGET_CONTROL, generic, SET_UVALUE=text
  WIDGET_CONTROL, generic4b, SENSITIVE=0
  WIDGET_CONTROL, generic5, SENSITIVE=0
end

