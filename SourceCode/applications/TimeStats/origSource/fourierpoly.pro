pro fourierPoly, x, freq1, freq2, NoHarmonics1, NoHarmonics2, fp, corrterm, CORR=CORR, LIN=LIN, FIT=FIT
; defines Fourier polynomials to describe or eliminate non-linear ternds and seasonal components
; x=series
; freq1 = first seasonal component
; freq2 = second seasonal component
; NoHarmonics1 = no. of overtones to consider for freq1
; NoHarmonics2 = no. of overtones to consider for freq2
; CORR: if set eliminate saisonal terms from the series
; fp= (empty) vector for Fourier Polynomials
; CORR=detrending
; LIN=inclusion of linear time term

  N=n_elements(x)
  vec=findgen(N)
  fp=fltarr(1,N)

  temp=size(x)
  if temp(0) gt 1 then x=transpose(x)
  ; first components
  if freq1 gt 0 then begin
   fp=fltarr(NoHarmonics1*2,N) ; erste H�lfte Cosinus- (freq/2), zweite H�lfte Sinusterme
   for i=0,NoHarmonics1-1 do begin   ; belegen der Spalten mit fortlaufender Frequenznummer
     fp[i,*]=cos(2*!pi*((i+1.)/freq1)*vec)
     fp[i+NoHarmonics1,*]=sin(2*!pi*((i+1.)/freq1)*vec)
     ;if quest.normalize eq 5 then for x=0,quest.NoHarmonics*2-1 do fp[x,*]=autosc(fp[x,*]) ; evtl z-Transformation
   endfor
  end
  ; second components

  if freq2 gt 0 then begin
   fp2=fltarr(NoHarmonics2*2,N)
   for i=0,NoHarmonics2-1 do begin   ; belegen der Spalten mit fortlaufender Frequenznummer
      fp2[i,*]=cos(2*!pi*((i+1.)/freq2)*vec)
      fp2[i+NoHarmonics2,*]=sin(2*!pi*((i+1.)/freq2)*vec)
   endfor
   if freq1 gt 0 then  fp=[fp,fp2] else fp=fp2
  end

   ; linear term
  if keyword_set(LIN) then begin
     time=findgen(1,n_elements(x))+1
     if (freq1 gt 0) or (freq2 gt 0) then fp=[fp,time] else fp=time
  end

  if keyword_set(CORR) then begin ; Zeitreihe um saisonale Komponente bereinigen
     result=regress(fp,x,const=offset,yfit=yfit) ; bestimmen der Reg. koeff
     x=x-yfit+mean(x); bereinigen der Zeitreihe
  end
  if keyword_set(FIT) then begin ; saisonale Komponente beibehalten
     result=regress(fp,x,const=offset,yfit=yfit) ; bestimmen der Reg. koeff
     x=yfit; bereinigen der Zeitreihe
  end


end