pro olsReg4, Ols, x0,y0,lags,flag, X1,X2,X3,X4,X5,quest,fp,FromTo,nwindDiff,noverlapDiff
   ; computes ols regression, using variable no. of X variables and lags
   ;
   ; x: time
   ; y: time-series
   ; lags: Structure variable with no of lags for X variables: lags.lagsY (time), ..., lags.lagsX5
   ; flag: flags for X regressors flag[0]: time, flag[1], ..., flag[5]: X regressors
   ; X1, ..., X5: Time series of external X regressors
   ; delta: 1: use first differences
   ; Compute total no. of observations in the sample and the series of the first differences
   ; fp: if a matrix a Fourierpolynomial is considered in regression analysis
   ; residuals: 1: Resiuals are given back in Ols-structure
   ; quest. structure variable: quest.diff=1: use differenes
   ; in OLS

  ;dOffs=12 ; EXPERIMELL, sp�ter in �bergabe einbauen um saisonale Differenzen zu erm�glichen!!!!!
  ;dt=quest.diff
  ;ON_IOERROR,ioerror

   ; Establish error handler. When errors occur, the index of the
   ; error is returned in the variable Error_status:

    CATCH, Error_status
    IF Error_status NE 0 THEN BEGIN
       PRINT, 'Error index: ', Error_status
       PRINT, 'Error message: ', !ERROR_STATE.MSG
             ; Handle the error by extending A:
        ;     CATCH, /CANCEL
       return
    ENDIF
dt=0  ;DIFFERENZEN RAUSWERFEN!!!!!!
dOffs=1;
X1=float(X1) & X2=float(X2) & X3=float(X3) & X4=float(X4) & X5=float(X5)
x=float(x0) & y=float(y0)


oriLags=lags
tryLags=-1
X1_ori=X1  ; f�r prewhitening
X2_ori=X2
X3_ori=X3
X4_ori=X4
X5_ori=X5
y_ori=y

if quest.prewhite eq 1 then begin
   ; prewhitening of exogeneous variables
     y=y-mean(y)
     prewhite,X1,AR,MA
     prewhite,X2,AR,MA
     prewhite,X3,AR,MA
     prewhite,X4,AR,MA
     prewhite,X5,AR,MA
     X1preW=X1
     X2preW=X2
     X3preW=X3
     X4preW=X4
     X5preW=X5
end



repeat begin
X1=X1_ori  ; f�r prewhitening
X2=X2_ori
X3=X3_ori
X4=X4_ori
X5=X5_ori
y=y_ori
tryLags=tryLags+1
lags[1]=tryLags
lagstemp=tryLags ; call by reference! lags must not be altered!
OlsOld=Ols

    ; t=where(flag[5:9] eq 0,count)
    ; if count gt 0 then lagstemp[1+t]=0
    ; maxlag=max(lagstemp)
     nobs=n_elements(x);
     if dt eq 1 then begin
        x=x[dOffs:nobs-1]
        if min(flag[0:4] ne 0) then y  =y[dOffs:nobs-1]-y[0:nobs-dOffs-1]
        if flag[5] eq 1 then X1 =X1[dOffs:nobs-1]-X1[0:nobs-dOffs-1]
        if flag[6] eq 1 then X2 =X2[dOffs:nobs-1]-X2[0:nobs-dOffs-1]
        if flag[7] eq 1 then X3 =X3[dOffs:nobs-1]-X3[0:nobs-dOffs-1]
        if flag[8] eq 1 then X4 =X4[dOffs:nobs-1]-X4[0:nobs-dOffs-1]
        if flag[9] eq 1 then X5 =X5[dOffs:nobs-1]-X5[0:nobs-dOffs-1]
        nobs=nobs-dOffs
     end
  ; Create the matrices for the dependent and independent variables

   Ols.noVar=total(flag[0:9])+total(lagstemp) ; total no. of X-variables for regression



   if quest.prewhite eq 1 then begin
   ; prewhitening of exogeneous variables
     X1=X1preW
     X2=X2preW
     X3=X3preW
     X4=X4preW
     X5=X5preW
   end


   if Ols.noVar gt 0 then begin
        xOLS=dblarr(Ols.noVar,n_elements(y[lagstemp:nobs-1]))
       ; include Fourier-Polynomial-coefficients
       if flag[10] eq 1 then xols=[xols,fp[*,lagstemp:nobs-1]]
   end else xOls=fp[*,lagstemp:nobs-1] ; only Fourier Polynomials


   o=0

   if flag[0] eq 1 then begin; include time
                          xOLS[o,*]=x[lagstemp:nobs-1]
                          o=o+1
                        end
   ; include lag y if desired
   for h=0,Lags[0]-1 do begin
                          xOLS[o,*]=y[lagstemp-h-1:nobs-1-h-1]; y series will be shortened by maxlag later!!
                          o=o+1
                        end
   ; include external X
   firstX=-1
   lastX=-1
   xOLSGLS=xOLS ; for GLS xOLSGLS is a copy of xOLS except for the X variables!
   if flag[5] eq 1 then for h=0,Lags[1] do begin
                                 if firstX eq -1 then firstX=o      ; Index wird ben�tigt um X-Variablen sp�ter in xOLS zu finden
                                              xOLS[o,*]=X1[lagstemp-h:nobs-1-h]
                                              xOLSGLS[o,*]=X1_ori[lagstemp-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[6] eq 1 then for h=0,Lags[2] do begin
                                 if firstX eq -1 then firstX=o;
                                              xOLS[o,*]=X2[lagstemp-h:nobs-1-h]
                                              xOLSGLS[o,*]=X2_ori[lagstemp-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[7] eq 1 then for h=0,Lags[3] do begin
                                  if firstX eq -1 then firstX=o;
                                              xOLS[o,*]=X3[lagstemp-h:nobs-1-h]
                                              xOLSGLS[o,*]=X3_ori[lagstemp-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[8] eq 1 then for h=0,Lags[4] do begin
                                 if firstX eq -1 then firstX=o;
                                              xOLS[o,*]=X4[lagstemp-h:nobs-1-h]
                                              xOLSGLS[o,*]=X4_ori[lagstemp-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[9] eq 1 then for h=0,Lags[5] do begin
                                 if firstX eq -1 then firstX=o;
                                              xOLS[o,*]=X5[lagstemp-h:nobs-1-h]
                                              xOLSGLS[o,*]=X5_ori[lagstemp-h:nobs-1-h]
                                              o=o+1
                                             end
  if firstX gt -1 then lastX=o-1                                                     ; Index wird ben�tigt um X-Variablen sp�ter in xOLS zu finden

   ; jetzt die Polynome h�herer Ordnung
   for i=1,4 do $
   if flag[i] eq 1 then begin;
                          xOLS[o,*]=x[lagstemp:nobs-1]^(i+1)
                          xOLSGLS[o,*]=x[lagstemp:nobs-1]^(i+1)
                          o=o+1
                        end

   ynew=y[lagstemp:nobs-1]
   Ols.nobs=n_elements(ynew)
   result=regress(xOLS,ynew,Yfit=yfit,CONST=const,SIGMA=Sigma,FTEST=ftest,CORRELATION=r,MCORRELATION=Rmult)
   resid=ynew-yfit

  if (quest.prewhite eq 1) and (firstX gt -1) then begin
    prewhite,resid,AR,MA  ; ARMA Modell f�r Residuen, nach Liu and Hanssens (1982)
    ynew=preWhiteX(ynew,AR,MA)
    for k=firstX,lastX do xOLS[k,*]=preWhiteX(xOLS[k,*],AR,MA)
    result=regress(xOLS,ynew,Yfit=yfit,CONST=const,SIGMA=Sigma,FTEST=ftest,CORRELATION=r,MCORRELATION=Rmult)
    resid=ynew-yfit


  endif


    ; Test auf white noise: Schlittgen & Streitberg, 371
    resid=resid-mean(resid)
    resid1=periodogram(resid,'NO',1,'Hertz')
    residN=n_elements(resid)/2
    resid1=resid1[0:residN-1]
    resid1[0]=0
    Fresid=fltarr(residN)
    ; berechnen der Verteilungsfunktion
    sumresid=0
    sumresid=total(resid1)
    for i=0,residN-1 do begin
      temp=total(resid1[0:i])
      Fresid[i]=float(temp)/sumresid
      Fresid[i]=Fresid[i]-(float(i)/residN)
    end

    ; berechnen der Pr�fgr��e
    c=max(abs(Fresid))
    nom=sqrt(-0.5*alog(0.05/2))
    denom=sqrt(residN-1)+0.2+(0.68/sqrt(residN-1))
    cH0= nom/denom - 0.4/(residN-1)




;resid=ynew-(result[1]*xOLS[1,*])


   if dt eq 0 then index=lagstemp else index=lagstemp+dOffs
   case quest.resid of
      0:Ols.resid[index:n_elements(y0)-1]=ynew    ; build layerstack of y
      1:Ols.resid[index:n_elements(y0)-1]=resid  ; Layerstack of residuals
      2:Ols.resid[index:n_elements(y0)-1]=yfit   ; Layerstack of yfit
      else:Ols.resid=0
   endcase

   Ols.DW=dwtest(resid)
   Ols.F=ftest
   Ols.Rmult=Rmult
   Ols.Fsig=1-F_PDF(ftest,Ols.noVar,Ols.nobs-Ols.noVar-1)
   Ols.sige=transpose(resid) # resid / (Ols.nobs - Ols.noVar)
   t=result/Sigma ;t-emp
   df_t=Ols.nobs-1-lagstemp
   Ols.const=const
   Ols.FitY1=Yfit[0]
   Ols.FitYend=Yfit[Ols.nobs-1]
   o=0
   if flag[0] eq 1 then begin  ; time
                          Ols.b[0,0]=result[0]
                          Ols.t[0,0]=t[0]
                          Ols.tsig[0,0]=min([1 - T_PDF(t[0], df_t), T_PDF(t[0], df_t)])*2
                          ; Richtung der Sig. ber�cksichtigen!!
                          if t[0] lt 0 then Ols.tsig[0,0]=Ols.tsig[0,0]*(-1)
                          Ols.r[0,0]=r[0]
                          o=o+1
                        end

   if Lags[0] gt 0 then begin
    for h=0,Lags[0]-1 do begin ; lag y, if available
                          Ols.b[1,h]=result[o+h]
                          Ols.t[1,h]=t[o+h]
                          Ols.tsig[1,h]=min([1 - T_PDF(t[o+h], df_t), T_PDF(t[o+h], df_t)])*2
                          if t[o+h] lt 0 then Ols.tsig[1,h]=Ols.tsig[1,h]*(-1)
                          Ols.r[1,h]=r[o+h]
                         end
   o=o+h
   end

   for z=1,5 do $
     if flag[z+4] eq 1 then begin
       for h=0,Lags[z] do begin ;ext. X1, ... ,X3
                          Ols.b[z+1,h]=result[o+h]
                          Ols.t[z+1,h]=t[o+h]
                          Ols.tsig[z+1,h]=min([1 - T_PDF(t[o+h], df_t), T_PDF(t[o+h], df_t)])*2
                          if t[o+h] lt 0 then Ols.tsig[z+1,h]=Ols.tsig[z+1,h]*(-1)
                          Ols.r[z+1,h]=r[o+h]
       endfor
       o=o+h
     end
    ; h�here Polynomterme (2. - 5. Ordnung)
   for i=1,4 do $
    if flag[i] eq 1 then begin
                          Ols.t2to5[i-1]=result[o]
                          Ols.t2to5T[i-1]=t[o]
                          Ols.t2to5Tsig[i-1]=min([1 - T_PDF(t[o], df_t), T_PDF(t[o], df_t)])*2
                          if t[o] lt 0 then Ols.t2to5Tsig[i-1]= Ols.t2to5Tsig[i-1]*(-1)
                          Ols.t2to5r[i-1]=r[o]
                          o=o+1
                        end

    ; FP
    if flag[10]  eq 1 then begin
      noOfcoeffs=n_elements(result)
      noOfHarm=n_elements(fp[*,0])
      Ols.FP=result(noOfcoeffs-noOfHarm:noOfcoeffs-1)
      Ols.fpT=t(noOfcoeffs-noOfHarm:noOfcoeffs-1)
      for j=0,n_elements(Ols.fpT)-1 do Ols.fpTsig[j]=min([1 - T_PDF(Ols.fpT[j], df_t), T_PDF(Ols.fpT[j], df_t)])*2
    end


    if n_elements(FromTo) gt 1 then begin
     ; if Lags[0] gt 0 then begin ; erste Werte sind weggefallen wegen lags!!!!
     ;    temp=yfit[0]
     ;    for h=0,Lags[0]-2 do temp=[yfit[0], temp]
     ;    yfit= [temp, reform(yfit)]
     ; end
      ;Ols.ChangeFrTo=absDiffSegments(yfit,nwindDiff,noverlapDiff)
      ;Ols.ChangeFrTo=absDiffSegments(ynew,nwindDiff,noverlapDiff)
      Ols.ChangeFrTo=absDiffSegmentsTEMP(xOLS,ynew,nwindDiff,noverlapDiff)
    end
wald_F,Ols,OlsOld
;if ols.waldSig lt 0.05 then print,trylags
endrep until ((ols.waldSig gt 0.1) or (tryLags eq 4)) and (tryLags gt 0)
lags=orilags
Ols.FitY1=trylags
if cH0 gt c then Ols.FitYend=0 else Ols.FitYend=1
end


