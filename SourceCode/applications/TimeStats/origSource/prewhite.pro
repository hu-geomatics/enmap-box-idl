pro prewhite, y0, AR, MA,c1,c2,OUT=OUT
        ; hier code f�r Pre-whitening der y-Residuen----------------------------------
    n=n_elements(y0)
    if n gt 1 then begin
     y0=y0-mean(y0)
     ytemp=y0
     sigmaQ  =  1/sqrt(n) ; Annahme: Gaussian white noise, kein MA-Komponente (siehe Matlab autocorr.m)
     if wn(ytemp,0.05) eq 1 then $
    ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
        begin
        ; zuerst AR-1 Modell testen
        arma,y0,1,0,AR,MA
        ytemp[1:n-1]=y0[1:n-1]-AR[0]*y0[0:n-2]
        ; y(t) =  AR[0]*y0(t-1) + ... + AR(R)y(t-R) + e(t)
        ; Compute approximate confidence bounds using the Box-Jenkins-Reinsel
        ; approach, equations 2.1.13 and 6.2.2, on pages 33 and 188, respectively.
        ; hier: zweifache Std.abw. f�r 95% Sig.niveau
        ;if wn(ytemp,0.05) eq 1 then $
        if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
        begin
            ; AR(2)-Modell
            ytemp=y0
            arma,y0,2,0,AR,MA  ; AR(2)-Modell
            ytemp[2:n-1]=y0[2:n-1]-AR[0]*y0[1:n-2]-AR[1]*y0[0:n-3]
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; AR(3)-Modell
            ytemp=y0
            arma,y0,3,0,AR,MA  ; AR(3)-Modell
            ytemp[3:n-1]=y0[3:n-1]-AR[0]*y0[2:n-2]-AR[1]*y0[1:n-3]-AR[2]*y0[0:n-4]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; AR(4)-Modell
            ytemp=y0
            arma,y0,4,0,AR,MA  ; AR(4)-Modell
            ytemp[4:n-1]=y0[4:n-1]-AR[0]*y0[3:n-2]-AR[1]*y0[2:n-3]-AR[2]*y0[1:n-4]-AR[3]*y0[0:n-5]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
        end
        end
     y0=ytemp
   end
   if keyword_set(OUT) then begin; schreibe Statistik weg
      writeu,c1,byte(n_elements(AR)); AR-Ordnung
      ARkoeffs=fltarr(10); max. ber�cksichtigte AR-Ordnung: 10
      ARkoeffs(0:n_elements(AR)-1)=AR
      writeu,c2,ARkoeffs ; BIP
   end

end
