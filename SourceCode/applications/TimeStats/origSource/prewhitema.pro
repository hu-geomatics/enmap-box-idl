pro prewhiteMA, y0, AR, MA
        ; hier code f�r Pre-whitening der y-Residuen----------------------------------
    n=n_elements(y0)
    if n gt 1 then begin
     y0=y0-mean(y0)
     ytemp=y0
     sigmaQ  =  1/sqrt(n) ; Annahme: Gaussian white noise, kein MA-Komponente (siehe Matlab autocorr.m)
     if wn(ytemp,0.05) eq 1 then $
    ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
        begin
        ; zuerst AR-1 Modell testen
        arma,y0,1,0,AR,MA
        ytemp[1:n-1]=y0[1:n-1]-AR[0]*y0[0:n-2]
        ; y(t) =  AR[0]*y0(t-1) + ... + AR(R)y(t-R) + e(t)
        ; Compute approximate confidence bounds using the Box-Jenkins-Reinsel
        ; approach, equations 2.1.13 and 6.2.2, on pages 33 and 188, respectively.
        ; hier: zweifache Std.abw. f�r 95% Sig.niveau
        ;if wn(ytemp,0.05) eq 1 then $
        if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
        begin
            ; AR(2)-Modell
            ytemp=y0
            arma,y0,2,0,AR,MA  ; AR(2)-Modell
            ytemp[2:n-1]=y0[2:n-1]-AR[0]*y0[1:n-2]-AR[1]*y0[0:n-3]
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; AR(3)-Modell
            ytemp=y0
            arma,y0,3,0,AR,MA  ; AR(3)-Modell
            ytemp[3:n-1]=y0[3:n-1]-AR[0]*y0[2:n-2]-AR[1]*y0[1:n-3]-AR[2]*y0[0:n-4]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; AR(4)-Modell
            ytemp=y0
            arma,y0,4,0,AR,MA  ; AR(4)-Modell
            ytemp[4:n-1]=y0[4:n-1]-AR[0]*y0[3:n-2]-AR[1]*y0[2:n-3]-AR[2]*y0[1:n-4]-AR[3]*y0[0:n-5]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
        end
        end
     Yori=y0
     y0=ytemp

; jetzt MA parameter anpassen

        ; zuerst MA-1 Modell testen
        arma,Yori,0,1,AR,MA
        ytemp[1:n-1]=Yori[1:n-1]-MA[0]*y0[0:n-2]
        ; y(t) =  AR[0]*y0(t-1) + ... + AR(R)y(t-R) + e(t)
        ; Compute approximate confidence bounds using the Box-Jenkins-Reinsel
        ; approach, equations 2.1.13 and 6.2.2, on pages 33 and 188, respectively.
        ; hier: zweifache Std.abw. f�r 95% Sig.niveau
        ;if wn(ytemp,0.05) eq 1 then $
        if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
        begin
            ; MA(2)-Modell
            ytemp=y0
            arma,Yori,0,2,AR,MA  ; MA(2)-Modell
            ytemp[2:n-1]=Yori[2:n-1]-MA[0]*y0[1:n-2]-MA[1]*y0[0:n-3]
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(3)-Modell
            ytemp=y0
            arma,Yori,0,3,AR,MA  ; MA(3)-Modell
            ytemp[3:n-1]=Yori[3:n-1]-MA[0]*y0[2:n-2]-MA[1]*y0[1:n-3]-MA[2]*y0[0:n-4]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(4)-Modell
            ytemp=y0
            arma,Yori,0,4,AR,MA  ; MA(4)-Modell
            ytemp[4:n-1]=Yori[4:n-1]-MA[0]*y0[3:n-2]-MA[1]*y0[2:n-3]-MA[2]*y0[1:n-4]-MA[3]*y0[0:n-5]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(5)-Modell
            ytemp=y0
            arma,Yori,0,5,AR,MA  ; MA(5)-Modell
            ytemp[5:n-1]=Yori[5:n-1]-MA[0]*y0[4:n-2]-MA[1]*y0[3:n-3]-MA[2]*y0[2:n-4]-MA[3]*y0[1:n-5]-MA[4]*y0[0:n-6]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(6)-Modell
            ytemp=y0
            arma,Yori,0,6,AR,MA  ; MA(6)-Modell
            ytemp[6:n-1]=Yori[6:n-1]-MA[0]*y0[5:n-2]-MA[1]*y0[4:n-3]-MA[2]*y0[3:n-4]-MA[3]*y0[2:n-5]-MA[4]*y0[1:n-6]-MA[5]*y0[0:n-7]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(7)-Modell
            ytemp=y0
            arma,Yori,0,7,AR,MA  ; MA(7)-Modell
            ytemp[7:n-1]=Yori[7:n-1]-MA[0]*y0[6:n-2]-MA[1]*y0[5:n-3]-MA[2]*y0[4:n-4]-MA[3]*y0[3:n-5]-MA[4]*y0[2:n-6]-MA[5]*y0[1:n-7]-MA[6]*y0[0:n-8]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(8)-Modell
            ytemp=y0
            arma,Yori,0,8,AR,MA  ; MA(8)-Modell
            ytemp[8:n-1]=Yori[8:n-1]-MA[0]*y0[7:n-2]-MA[1]*y0[6:n-3]-MA[2]*y0[5:n-4]-MA[3]*y0[4:n-5]-MA[4]*y0[3:n-6]-MA[5]*y0[2:n-7]-MA[6]*y0[1:n-8]-MA[7]*y0[0:n-9]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(9)-Modell
            ytemp=y0
            arma,Yori,0,9,AR,MA  ; MA(9)-Modell
            ytemp[9:n-1]=Yori[9:n-1]-MA[0]*y0[8:n-2]-MA[1]*y0[7:n-3]-MA[2]*y0[6:n-4]-MA[3]*y0[5:n-5]-MA[4]*y0[4:n-6]-MA[5]*y0[3:n-7]-MA[6]*y0[2:n-8]-MA[7]*y0[1:n-9]-MA[8]*y0[0:n-10]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end
            if wn(ytemp,0.05) eq 1 then $
            ;if (a_correlate(ytemp,1) lt (-1)*sigmaQ*2) or  (a_correlate(ytemp,1) gt sigmaQ*2) then $
            begin
            ; MA(10)-Modell
            ytemp=y0
            arma,Yori,0,10,AR,MA  ; MA(10)-Modell
            ytemp[10:n-1]=Yori[10:n-1]-MA[0]*y0[9:n-2]-MA[1]*y0[8:n-3]-MA[2]*y0[7:n-4]-MA[3]*y0[6:n-5]-MA[4]*y0[5:n-6]-MA[5]*y0[4:n-7]-MA[6]*y0[3:n-8]-MA[7]*y0[2:n-9]-MA[8]*y0[1:n-10]-MA[9]*y0[0:n-11]
            ;if (a_correlate(y0,1) lt (-1)*sigmaQ*2) or  (a_correlate(y0,1) gt *sigmaQ*2) then $
            end

        end






   end
end
