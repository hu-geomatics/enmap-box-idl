function msk, data_vec, freq, missvalue, mv,sig
;modified seasonal kendall test for monotonic trends
;f�r variable Frequenzen freq, nicht vollst�ndige Zyklen werden verworfen
; berechnen Sig.niveau: TU, 29.06.05
; sig =0: R�ckgabe emp. z-score, 1: R�ckgabe 2 seitiges SigNiveau

if missvalue eq 1 then begin
   temp=where(data_vec eq mv,count)
   if count ne 0 then begin
      print,'!!!! Mod. seasonal Kendall-Test can not be computed with missing values. Select interpolation option !!!!
      return,0
   endif
endif

    sgn=0
    sgn_tot=0
    sgn_vec=indgen(n_elements(data_vec)-1)

s_matrix=datasplit(data_vec,freq)
;calculate sum of sgn and sum of variances for all seasons
;getestet 2.2.03
sum_sgn=0
sum_var=0
ny = fix(n_elements(data_vec)/freq)
for i=0,freq-1 do begin
    sum_sgn = sum_sgn + signum_(s_matrix[i,*],missvalue,mv)
    sum_var = sum_var + var(s_matrix[i,*], missvalue, mv)
endfor
;berechnen der geschaetzten kovarianzen f�r alle saisonpaare
sum_cov = 0
for i=0,freq-2 do begin
    for j=i+1,freq-1 do begin
       cov_arr = fltarr(2,ny)
       cov_arr[0,*] = s_matrix[i,*]
       cov_arr[1,*] = s_matrix[j,*]
       est_cov = mcov(cov_arr)
       sum_cov = sum_cov + est_cov
    endfor
endfor

    var_corr = sum_var + sum_cov
    sd_corr = sqrt(var_corr)
    if sd_corr eq 0 then sd_corr=0.0001

    ;continuity correction of overall test value
    case 1 of
       sum_sgn ge 0: z=float(sum_sgn-1)/sd_corr
       sum_sgn eq 0: z=0
       sum_sgn le 0: z=float(sum_sgn+1)/sd_corr
    endcase
if sig eq 1 then begin
   temp=min([1 - gauss_PDF(z), gauss_PDF(z)])*2
   if z lt 0 then z= temp * (-1) else z=temp
end

return, z
end
