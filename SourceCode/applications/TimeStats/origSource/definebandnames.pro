
function defineBandnames
common blk1
     bandnames=strarr(n_elements(flag)) ; normal trendparamters and ADF-testcoefficients
     bandnames[0]='Reg. koeff.  (b)' & bandnames[1]='Reg. constant' & bandnames[2]='Reg. sigma' & bandnames[3]='Ttest(b)'
     bandnames[4]='Correlation (r)' & bandnames[5]='RMS'&  bandnames[6]='Rel. error (RMS/mean)' & bandnames[7]='Max. residual'
     bandnames[8]='Time max. residual' & bandnames[9]='MK test' & bandnames[10]='MK slope'
     bandnames[11]='Seas. MK test' & bandnames[12]='Seas. MK slope' & bandnames[13]='Mod. seas. MK test'
     bandnames[14]='Stddev' & bandnames[15]='mean' & bandnames[16]='varkoeff' & bandnames[17]='valid n'
     bandnames[18]='Skewness' & bandnames[19]='kurtosis' & bandnames[20]='Abbe criterion'
     bandnames[21]='Spearman (rho) rank corr.' & bandnames[22]= 'Kendalls (tau) rank correlation'
     bandnames[23]='Sig. rho' & bandnames[24]='Sig.tau'
     bandnames[25]='Min'
     bandnames[26]='Max'
     bandnames[27]='Time Min->Max'
     bandnames[28]='Time Max->Min'
     bandnames[29]='Range'
     bandnames[30]='ADF: constant'
     bandnames=[bandnames,'ADF: Reg. koeff.:time']
     bandnames=[bandnames,'ADF: Reg. koeff.:t-1']
     for z=1,lagsADF do bandnames=[bandnames,'ADF:reg.koeff.:dt-'+STRCOMPRESS(string(z))]
     bandnames=[bandnames,'ADF: sig. time']
     bandnames=[bandnames,'ADF: sig. t-1 (ADF-Test)']
     for z=1,lagsADF do bandnames=[bandnames,'ADF:sig. :dt-'+STRCOMPRESS(string(z))]
     bandnames=[bandnames,'ADF:t emp. time']
     bandnames=[bandnames,'ADF:t emp. t-1(ADF-Test)']
     for z=1,lagsADF do bandnames=[bandnames,'ADF:t emp. :dt-'+STRCOMPRESS(string(z))]
     bandnames=[bandnames,'ADF:Durbin-Watson']
     ;    connecting restricted and unrestricted trend-coefficients
     ;    Parameter f�r restricted regression  (time X1 X2 X3 X4 X5)
     ;    Parameter f�r unrestricted regression(time X1 X2 X3 X4 X5)
     ;    lags.lagsY, ..., lagsX5
     return,bandnames
  end

