pro base_widgetClean_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
  'group4b': begin
             case ev2.value of
               0: begin
                    ;WIDGET_CONTROL, cyclebase, SENSITIVE=1
                    four_koeff_temp=1  ; tempor�re Schalter
                    IFFT_temp=1
                    missvalue = 0
                  end
               1: begin
                ;WIDGET_CONTROL, cyclebase, SENSITIVE=0
                    four_koeff_temp=0  ; tempor�re Schalter
                    IFFT_temp=0
                    missvalue = 1
                  end
               2: begin
                    missvalue = 2
                 ;   WIDGET_CONTROL, cyclebase, SENSITIVE=1
                    four_koeff_temp=1  ; tempor�re Schalter
                    IFFT_temp=1
                  end
               3: begin
                    missvalue = 3
                 ;   WIDGET_CONTROL, cyclebase, SENSITIVE=1
                    four_koeff_temp=1  ; tempor�re Schalter
                    IFFT_temp=1
                  end

               4: begin
                    missvalue = 4
                  ;  WIDGET_CONTROL, cyclebase, SENSITIVE=1
                    four_koeff_temp=1  ; tempor�re Schalter
                    IFFT_temp=1
                  end
             endcase
            end

  'group1': case ev2.value of
               0: olcorrmeth=2  ; average correction
               1: olcorrmeth=3  ; sais. average correction
               2: olcorrmeth=4  ; lin interpolation
            endcase
  'group': begin
             case ev2.value of
               0: begin  ; no outliers
                    WIDGET_CONTROL, olcorr1, SENSITIVE=0
                  ;  WIDGET_CONTROL, olcorr2, SENSITIVE=0
                    WIDGET_CONTROL, olcorr3, SENSITIVE=0
                    outlier = 0
                  end
               1: begin  ;outliers, detection with std.dev
                WIDGET_CONTROL, olcorr1, SENSITIVE=1
               ; WIDGET_CONTROL, olcorr2, SENSITIVE=0
                WIDGET_CONTROL, olcorr3, SENSITIVE=1
                    outlier = 1
                  end
   ;            2: begin ; outliers, detection with hotspots
   ;                 WIDGET_CONTROL, olcorr1, SENSITIVE=0
   ;                 WIDGET_CONTROL, olcorr2, SENSITIVE=1
   ;                 WIDGET_CONTROL, olcorr3, SENSITIVE=1
   ;                 outlier=2
   ;               end
             endcase
            end
  'groupmv':WIDGET_CONTROL, _mv, GET_VALUE=mv
  'groupSais':WIDGET_CONTROL, _mvSais, GET_VALUE=mvSais
  'groupSm':WIDGET_CONTROL, _smooth, GET_VALUE=nrPSmooth
  'group1b':begin
              WIDGET_CONTROL, _stddev, GET_VALUE=temp
              quest.confident=temp
              olstddev=sqrt(1./(1-temp/100.))
             print, olstddev
            end
  'group1c':WIDGET_CONTROL, _hs, GET_VALUE=olthresh
  'ok'  :   WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end


pro base_widgetClean, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
  values=['no outliers','outlier detection. --> Chebyshev''s Theorem ']
  values1=['outlier corr. --> global mean','outlier corr. --> seasonal mean','outlier corr. --> lin. interpolation']
  values4b =['no MV','no MV correction','MV --> global mean','MV --> seasonal mean','MV --> lin. interpolation']
  mvout=  WIDGET_BASE(TITLE='Data cleaning',/COLUMN)
  _mv    = CW_FIELD(mvout,/FLOAT,/ALL_EVENTS,VALUE=mv,TITLE='Missing Value code',UVALUE='groupmv')
  bgroup4b= CW_BGROUP(mvout,values4b,/ROW, SET_VALUE=missvalue,LABEL_TOP='Correction method for Missing Values',/FRAME,/EXCLUSIVE, UVALUE='group4b')
  bgroup= CW_BGROUP(mvout,values,/ROW, SET_VALUE=outlier,LABEL_TOP='Outlier correction',/FRAME,/EXCLUSIVE, UVALUE='group')
  olcorr1 = WIDGET_BASE(mvout, /COLUMN) ; f�r Std.dev
  _stddev= CW_FIELD(olcorr1,/FLOAT,/ALL_EVENTS,VALUE=quest.confident,TITLE='Confidence level',UVALUE='group1b')
  ;olcorr2 = WIDGET_BASE(mvout, /COLUMN) ; f�r Hotspots
  ;_hs= CW_FIELD(olcorr2,/FLOAT,/ALL_EVENTS,VALUE=olthresh,TITLE='Threshold for hotspots (Confirm with RETURN!)',UVALUE='group1c')
  olcorr3 = WIDGET_BASE(mvout, /COLUMN) ; f�r Hotspots
  bgroup1= CW_BGROUP(olcorr3,values1,/ROW, SET_VALUE=olcorrmeth-2,LABEL_TOP='Outlier correction method',/FRAME,/EXCLUSIVE, UVALUE='group1')
   _mvSais    = CW_FIELD(mvout,/FLOAT,/ALL_EVENTS,VALUE=mvSais,TITLE='Period',UVALUE='groupSais')
  _smooth=CW_FIELD(mvout,/FLOAT,/ALL_EVENTS,VALUE=nrPSmooth,TITLE='Length of Savitzky-Golay filter window',UVALUE='groupSm')
  endbase = WIDGET_BASE(mvout, /ROW,/ALIGN_BOTTOM)
  ok = widget_button(endbase,VALUE='  ok  ', UVALUE='ok')

  WIDGET_CONTROL, mvout, /NO_COPY,/REALIZE
  WIDGET_CONTROL, olcorr1, /NO_COPY,/REALIZE
  ;WIDGET_CONTROL, olcorr2, /NO_COPY,/REALIZE
  WIDGET_CONTROL, olcorr3, /NO_COPY,/REALIZE
  case outlier of
     0: begin  ; no outliers
          WIDGET_CONTROL, olcorr1, SENSITIVE=0
          ;WIDGET_CONTROL, olcorr2, SENSITIVE=0
          WIDGET_CONTROL, olcorr3, SENSITIVE=0
        end
     1: begin  ;outliers, detection with std.dev
          WIDGET_CONTROL, olcorr1, SENSITIVE=1
          ;WIDGET_CONTROL, olcorr2, SENSITIVE=0
          WIDGET_CONTROL, olcorr3, SENSITIVE=1
        end
     2: begin ; outliers, detection with hotspots
          WIDGET_CONTROL, olcorr1, SENSITIVE=0
          ;WIDGET_CONTROL, olcorr2, SENSITIVE=1
          WIDGET_CONTROL, olcorr3, SENSITIVE=1
        end
   endcase
 XMANAGER,'base_widgetClean', mvout, GROUP=GROUP1
end
