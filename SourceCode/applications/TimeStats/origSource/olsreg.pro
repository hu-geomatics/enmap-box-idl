pro olsReg, Ols, x0,y0,lags,flag, X1,X2,X3,X4,X5,quest,fp,FromTo,nwindDiff,noverlapDiff
   ; computes ols regression, using variable no. of X variables and lags
   ;
   ; x: time
   ; y: time-series
   ; lags: Structure variable with no of lags for X variables: lags.lagsY (time), ..., lags.lagsX5
   ; flag: flags for X regressors flag[0]: time, flag[1], ..., flag[5]: X regressors
   ; X1, ..., X5: Time series of external X regressors
   ; delta: 1: use first differences
   ; Compute total no. of observations in the sample and the series of the first differences
   ; fp: if a matrix a Fourierpolynomial is considered in regression analysis
   ; residuals: 1: Resiuals are given back in Ols-structure
   ; quest. structure variable: quest.diff=1: use differenes
   ; in OLS

  ;dOffs=12 ; EXPERIMELL, sp�ter in �bergabe einbauen um saisonale Differenzen zu erm�glichen!!!!!
  ;dt=quest.diff
 ; ON_IOERROR,ioerror

   ; Establish error handler. When errors occur, the index of the
   ; error is returned in the variable Error_status:

    CATCH, Error_status
    IF Error_status NE 0 THEN BEGIN
       PRINT, 'Error index: ', Error_status
       PRINT, 'Error message: ', !ERROR_STATE.MSG
             ; Handle the error by extending A:
       CATCH, /CANCEL
       return
    ENDIF

; double-prewhitening
;if quest.prewhite eq 1 then begin
;  prewhite, y0, AR, MA
   ; prewhitening of exogeneous variables
  ;  y=y-mean(y)

;     prewhite,X2,AR,MA
;     prewhite,X1,AR,MA
;    y=preWhiteX(y,AR,MA)
;     prewhite,y0,AR,MA
;     prewhite,X3,AR,MA
;     prewhite,X4,AR,MA
;     prewhite,X5,AR,MA
;end


;X=[transpose(X1),transpose(X2),transpose(X3)]

;X1 = transpose(PCOMP( X , NVARIABLES=1 , /STANDARDIZE ))
;flag[6]=0
;flag[7]=0



  dt=0  ;DIFFERENZEN RAUSWERFEN!!!!!!
  dOffs=1;
     X1=float(X1) & X2=float(X2) & X3=float(X3) & X4=float(X4) & X5=float(X5)
     x=float(x0) & y=float(y0)
     lagstemp=lags ; call by reference! lags must not be altered!
     t=where(flag[5:9] eq 0,count)
     if count gt 0 then lagstemp[1+t]=0
     maxlag=max(lagstemp)
     nobs=n_elements(x);
     ;if dt eq 1 then begin
     ;   x=x[dOffs:nobs-1]
     ;   if min(flag[0:4] ne 0) then y  =y[dOffs:nobs-1]-y[0:nobs-dOffs-1]
     ;   if flag[5] eq 1 then X1 =X1[dOffs:nobs-1]-X1[0:nobs-dOffs-1]
     ;   if flag[6] eq 1 then X2 =X2[dOffs:nobs-1]-X2[0:nobs-dOffs-1]
     ;   if flag[7] eq 1 then X3 =X3[dOffs:nobs-1]-X3[0:nobs-dOffs-1]
     ;   if flag[8] eq 1 then X4 =X4[dOffs:nobs-1]-X4[0:nobs-dOffs-1]
     ;   if flag[9] eq 1 then X5 =X5[dOffs:nobs-1]-X5[0:nobs-dOffs-1]
     ;   nobs=nobs-dOffs
     ;end
  ; Create the matrices for the dependent and independent variables

   Ols.noVar=total(flag[0:9])+total(lagstemp) ; total no. of X-variables for regression

   if Ols.noVar gt 0 then begin
       ; print,maxlag,nobs-1,n_elements(y)
        xOLS=dblarr(Ols.noVar,n_elements(y[maxlag:nobs-1]))

       ; include Fourier-Polynomial-coefficients
       if flag[10] eq 1 then xols=[xols,fp[*,maxlag:nobs-1]]
   end else xOls=fp[*,maxlag:nobs-1] ; only Fourier Polynomials

   o=0

   if flag[0] eq 1 then begin; include time
                          xOLS[o,*]=x[maxlag:nobs-1]
                          o=o+1
                        end
   ; include lag y if desired
   for h=0,Lags[0]-1 do begin
                          xOLS[o,*]=y[maxlag-h-1:nobs-1-h-1]; y series will be shortened by maxlag later!!
                          o=o+1
                        end
   ; include external X

   if flag[5] eq 1 then for h=0,Lags[1] do begin ;
                                              xOLS[o,*]=X1[maxlag-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[6] eq 1 then for h=0,Lags[2] do begin ;
                                              xOLS[o,*]=X2[maxlag-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[7] eq 1 then for h=0,Lags[3] do begin ;
                                              xOLS[o,*]=X3[maxlag-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[8] eq 1 then for h=0,Lags[4] do begin ;
                                              xOLS[o,*]=X4[maxlag-h:nobs-1-h]
                                              o=o+1
                                             end
   if flag[9] eq 1 then for h=0,Lags[5] do begin ;
                                              xOLS[o,*]=X5[maxlag-h:nobs-1-h]
                                              o=o+1
                                             end


   ; jetzt die Polynome h�herer Ordnung
   for i=1,4 do $
   if flag[i] eq 1 then begin;
                          xOLS[o,*]=x[maxlag:nobs-1]^(i+1)
                          o=o+1
                        end

   ynew=y[maxlag:nobs-1]
   Ols.nobs=n_elements(ynew)
   result=regress(xOLS,ynew,Yfit=yfit,CONST=const,SIGMA=Sigma,FTEST=ftest,CORRELATION=r,MCORRELATION=Rmult)



   if (quest.gls eq 1) then begin
     prewhite,resid,AR,MA  ; ARMA Modell f�r Residuen, nach Liu and Hanssens (1982)
     ynew=preWhiteX(ynew,AR,MA)
     for k=0,n_elements(xOLS[*,0])-1 do xOLS[k,*]=preWhiteX(xOLS[k,*],AR,MA)
     result=regress(xOLS,ynew,Yfit=yfit,CONST=const,SIGMA=Sigma,FTEST=ftest,CORRELATION=r,MCORRELATION=Rmult)
     resid=ynew-yfit
   endif



   resid=ynew-yfit

;resid=ynew-(result[1]*xOLS[1,*])


   if dt eq 0 then index=maxlag else index=maxlag+dOffs
   case quest.resid of
      0:Ols.resid[index:n_elements(y0)-1]=ynew    ; build layerstack of y
      1:Ols.resid[index:n_elements(y0)-1]=resid  ; Layerstack of residuals
      2:Ols.resid[index:n_elements(y0)-1]=yfit   ; Layerstack of yfit
      else:Ols.resid=0
   endcase

   Ols.DW=dwtest(resid)
   Ols.F=ftest
   Ols.Rmult=Rmult
   Ols.Fsig=1-F_PDF(ftest,Ols.noVar,Ols.nobs-Ols.noVar-1)
   Ols.sige=transpose(resid) # resid / (Ols.nobs - Ols.noVar)
   t=result/Sigma ;t-emp
   df_t=Ols.nobs-1-maxlag
   Ols.const=const
 ;  Ols.const=n_elements(AR)
   Ols.FitY1=Yfit[0]
   Ols.FitYend=Yfit[Ols.nobs-1]
   o=0
   if flag[0] eq 1 then begin  ; time
                          Ols.b[0,0]=result[0]
                          Ols.t[0,0]=t[0]
                          Ols.tsig[0,0]=min([1 - T_PDF(t[0], df_t), T_PDF(t[0], df_t)])*2
                          ; Richtung der Sig. ber�cksichtigen!!
                          if t[0] lt 0 then Ols.tsig[0,0]=Ols.tsig[0,0]*(-1)
                          Ols.r[0,0]=r[0]
                          o=o+1
                        end

   if Lags[0] gt 0 then begin
    for h=0,Lags[0]-1 do begin ; lag y, if available
                          Ols.b[1,h]=result[o+h]
                          Ols.t[1,h]=t[o+h]
                          Ols.tsig[1,h]=min([1 - T_PDF(t[o+h], df_t), T_PDF(t[o+h], df_t)])*2
                          if t[o+h] lt 0 then Ols.tsig[1,h]=Ols.tsig[1,h]*(-1)
                          Ols.r[1,h]=r[o+h]
                         end
   o=o+h
   end

   for z=1,5 do $
     if flag[z+4] eq 1 then begin
       for h=0,Lags[z] do begin ;ext. X1, ... ,X3
                          Ols.b[z+1,h]=result[o+h]
                          Ols.t[z+1,h]=t[o+h]
                          Ols.tsig[z+1,h]=min([1 - T_PDF(t[o+h], df_t), T_PDF(t[o+h], df_t)])*2
                          if t[o+h] lt 0 then Ols.tsig[z+1,h]=Ols.tsig[z+1,h]*(-1)
                          Ols.r[z+1,h]=r[o+h]
       endfor
       o=o+h
     end
    ; h�here Polynomterme (2. - 5. Ordnung)
   for i=1,4 do $
    if flag[i] eq 1 then begin
                          Ols.t2to5[i-1]=result[o]
                          Ols.t2to5T[i-1]=t[o]
                          Ols.t2to5Tsig[i-1]=min([1 - T_PDF(t[o], df_t), T_PDF(t[o], df_t)])*2
                          if t[o] lt 0 then Ols.t2to5Tsig[i-1]= Ols.t2to5Tsig[i-1]*(-1)
                          Ols.t2to5r[i-1]=r[o]
                          o=o+1
                        end

    ; FP
    if flag[10]  eq 1 then begin
      noOfcoeffs=n_elements(result)
      noOfHarm=n_elements(fp[*,0])
      Ols.FP=result(noOfcoeffs-noOfHarm:noOfcoeffs-1)
      Ols.fpT=t(noOfcoeffs-noOfHarm:noOfcoeffs-1)
      for j=0,n_elements(Ols.fpT)-1 do Ols.fpTsig[j]=min([1 - T_PDF(Ols.fpT[j], df_t), T_PDF(Ols.fpT[j], df_t)])*2
    end


    if n_elements(FromTo) gt 1 then begin
     ; if Lags[0] gt 0 then begin ; erste Werte sind weggefallen wegen lags!!!!
     ;    temp=yfit[0]
     ;    for h=0,Lags[0]-2 do temp=[yfit[0], temp]
     ;    yfit= [temp, reform(yfit)]
     ; end
      ;Ols.ChangeFrTo=absDiffSegments(yfit,nwindDiff,noverlapDiff)
      ;Ols.ChangeFrTo=absDiffSegments(ynew,nwindDiff,noverlapDiff)
      Ols.ChangeFrTo=absDiffSegmentsTEMP(xOLS,ynew,nwindDiff,noverlapDiff)
    end


end
