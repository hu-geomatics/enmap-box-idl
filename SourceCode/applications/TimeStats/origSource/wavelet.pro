;****************************************************************** WAVELET
FUNCTION wavelet,y1,dt, $   ;*** required inputs
	S0=s0,DJ=dj,J=j, $   ;*** optional inputs
	PAD=pad,MOTHER=mother,PARAM=param, $
	VERBOSE=verbose,NO_WAVE=no_wave,RECON=recon, $
	LAG1=lag1,SIGLVL=siglvl,DOF=dof,GLOBAL=global, $   ;*** optional inputs
	SCALE=scale,PERIOD=period,YPAD=ypad, $  ;*** optional outputs
	DAUGHTER=daughter,COI=coi, $
	SIGNIF=signif,FFT_THEOR=fft_theor, $
	OCT=oct,VOICE=voice   ;*** defunct inputs
	
	ON_ERROR,2
	r = CHECK_MATH(0,1)
	n = N_ELEMENTS(y1)
	n1 = n
	base2 = FIX(ALOG(n)/ALOG(2) + 0.4999)   ; power of 2 nearest to N

;....check keywords & optional inputs
	IF (N_ELEMENTS(s0) LT 1) THEN s0 = 2.0*dt
	IF (N_ELEMENTS(voice) EQ 1) THEN dj = 1./voice
	IF (N_ELEMENTS(dj) LT 1) THEN dj = 1./8
	IF (N_ELEMENTS(oct) EQ 1) THEN J = FLOAT(oct)/dj
	IF (N_ELEMENTS(J) LT 1) THEN J=FIX((ALOG(FLOAT(n)*dt/s0)/ALOG(2))/dj)  ;[Eqn(10)]
	IF (N_ELEMENTS(mother) LT 1) THEN mother = 'MORLET'
	IF (N_ELEMENTS(param) LT 1) THEN param = -1
	IF (N_ELEMENTS(siglvl) LT 1) THEN siglvl = 0.95
	IF (N_ELEMENTS(lag1) LT 1) THEN lag1 = 0.0
	lag1 = lag1(0)
	verbose = KEYWORD_SET(verbose)
	do_daughter = KEYWORD_SET(daughter)
	do_wave = NOT KEYWORD_SET(no_wave)
	recon = KEYWORD_SET(recon)
	IF KEYWORD_SET(global) THEN MESSAGE, $
		'Please use WAVE_SIGNIF for global significance tests'
		
;....construct time series to analyze, pad if necessary
	ypad = y1 - TOTAL(y1)/n    ; remove mean
	IF KEYWORD_SET(pad) THEN BEGIN   ; pad with extra zeroes, up to power of 2
		ypad = [ypad,FLTARR(2L^(base2 + 1) - n)]
		n = N_ELEMENTS(ypad)
	ENDIF

;....construct SCALE array & empty PERIOD & WAVE arrays
	na = J + 1                  ; # of scales
	scale = DINDGEN(na)*dj      ; array of j-values
	scale = 2d0^(scale)*s0      ; array of scales  2^j   [Eqn(9)]
	period = FLTARR(na,/NOZERO) ; empty period array (filled in below)
	wave = COMPLEXARR(n,na,/NOZERO)  ; empty wavelet array
	IF (do_daughter) THEN daughter = wave   ; empty daughter array

;....construct wavenumber array used in transform [Eqn(5)]
	k = (DINDGEN(n/2) + 1)*(2*!PI)/(DOUBLE(n)*dt)
	k = [0d,k,-REVERSE(k(0:(n-1)/2 - 1))]

;....compute FFT of the (padded) time series
	yfft = FFT(ypad,-1,/DOUBLE)  ; [Eqn(3)]

	IF (verbose) THEN BEGIN  ;verbose
		PRINT
		PRINT,mother
		PRINT,'#points=',n1,'   s0=',s0,'   dj=',dj,'   J=',FIX(J)
		IF (n1 NE n) THEN PRINT,'(padded with ',n-n1,' zeroes)'
		PRINT,['j','scale','period','variance','mathflag'], $
			FORMAT='(/,A3,3A11,A10)'
	ENDIF  ;verbose
	IF (N_ELEMENTS(fft_theor) EQ n) THEN fft_theor_k = fft_theor ELSE $
		fft_theor_k = (1-lag1^2)/(1-2*lag1*COS(k*dt)+lag1^2)  ; [Eqn(16)]
	fft_theor = FLTARR(na)
	
;....loop thru each SCALE
	FOR a1=0,na-1 DO BEGIN  ;scale
		psi_fft=CALL_FUNCTION(mother, $
			param,scale(a1),k,period1,coi,dofmin,Cdelta,psi0)
		IF (do_wave) THEN $
			wave(*,a1) = FFT(yfft*psi_fft,1,/DOUBLE)  ;wavelet transform[Eqn(4)]
		period(a1) = period1   ; save period
		fft_theor(a1) = TOTAL((ABS(psi_fft)^2)*fft_theor_k)/n
		IF (do_daughter) THEN $
			daughter(*,a1) = FFT(psi_fft,1,/DOUBLE)   ; save daughter
		IF (verbose) THEN PRINT,a1,scale(a1),period(a1), $
				TOTAL(ABS(wave(*,a1))^2),CHECK_MATH(0), $
				FORMAT='(I3,3F11.3,I6)'
	ENDFOR  ;scale

	coi = coi*[FINDGEN((n1+1)/2),REVERSE(FINDGEN(n1/2))]*dt   ; COI [Sec.3g]
	
	IF (do_daughter) THEN $   ; shift so DAUGHTERs are in middle of array
		daughter = [daughter(n-n1/2:*,*),daughter(0:n1/2-1,*)]

;....significance levels [Sec.4]
	sdev = (MOMENT(y1))(1)
	fft_theor = sdev*fft_theor  ; include time-series variance
	dof = dofmin
	signif = fft_theor*CHISQR_CVF(1. - siglvl,dof)/dof   ; [Eqn(18)]

	IF (recon) THEN BEGIN  ; Reconstruction [Eqn(11)]
		IF (Cdelta EQ -1) THEN BEGIN
			y1 = -1
			MESSAGE,/INFO, $
				'Cdelta undefined, cannot reconstruct with this wavelet'
		ENDIF ELSE BEGIN
			y1=dj*SQRT(dt)/(Cdelta*psi0)*(FLOAT(wave) # (1./SQRT(scale)))
			y1 = y1[0:n1-1]
		ENDELSE
	ENDIF
	
	RETURN,wave(0:n1-1,*)    ; get rid of padding before returning

END
