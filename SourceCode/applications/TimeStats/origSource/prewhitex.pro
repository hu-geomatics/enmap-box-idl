function preWhiteX,X,AR,MA
  lags=n_elements(AR)
  n=n_elements(X)
  X=X-mean(X)
  case lags of
    1:X[1:n-1]=X[1:n-1]-AR[0]*X[0:n-2]
    2:X[2:n-1]=X[2:n-1]-AR[0]*X[1:n-2]-AR[1]*X[0:n-3]
    3:X[3:n-1]=X[3:n-1]-AR[0]*X[2:n-2]-AR[1]*X[1:n-3]-AR[0]*X[0:n-4]
    0:X=X
  endcase
  return,X
end