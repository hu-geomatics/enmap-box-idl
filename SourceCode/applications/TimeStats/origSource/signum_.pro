function signum_, data_vec, missvalue, mv
; Mann-Kendall-Test (Autor: Tim Erbrecht)
;calculate sgn function for trend test
;getestet am 11.12.02
c=0
for i=0,n_elements(data_vec)-2 do begin
    for j=i+1,n_elements(data_vec)-1 do begin
       if (missvalue eq 0) or ((missvalue eq 1) and (data_vec[i] ne mv) and (data_vec[j] ne mv)) $
           then sgn = float(data_vec[j])-float(data_vec[i]) else sgn=0
       if sgn ne 0 then sgn=sgn/abs(sgn)
       c = c+sgn
    endfor
endfor

    return, c

end
