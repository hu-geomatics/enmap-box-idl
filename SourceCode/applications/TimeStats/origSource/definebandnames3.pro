function defineBandnames3,WFFTFromTo,WFFTtype
if WFFTtype eq 0 then text='Phase (start-end)' else text='Magnitude (start-end)'

if n_elements(WFFTFromTo) gt 0 then for z=0, n_elements(WFFTFromTo)-1 do $
   if z eq 0 then bandnames= strcompress(text + WFFTFromTo(z)) else $
       bandnames=[bandnames, strcompress(text + WFFTFromTo(z))]  ; Model. Differnzen

return,bandnames
end
