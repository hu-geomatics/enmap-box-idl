function mcov, data_mx
; Mann-Kendall-Test (Autor: Tim Erbrecht)
;calculation of covariances
;only valid if there are no missing values

n = n_elements(data_mx)/2

;calculation of K for two seasons
;getestet 2.2.03
K = 0
for i=0,n-2 do begin
    for j=i+1,n-1 do begin
       sgn = (float(data_mx[0,j])-float(data_mx[0,i]))*(float(data_mx[1,j])-float(data_mx[1,i]))
       if sgn ne 0 then sgn=sgn/abs(sgn) else sgn=0
       K = K + sgn
    endfor
endfor

;calculate rank matrix
;in case of ties midranks are calculated
;getestet 2.2.03
R_mx = fltarr(2,n)
temp= float(data_mx)
sort_index = fltarr(2,n)
for s=0,1 do sort_index[s,*] = sort(data_mx[s,*])
for s=0,1 do temp[s,*] = transpose(ranks(transpose(temp[s,sort(temp[s,*])])))
for s=0,1 do begin
    for o=0,n-1 do begin
       R_mx[s,sort_index[s,o]] = temp[s,o]
    endfor
endfor

;calculate sum of cross products
;getestet 2.2.03
rank_corr = 0
for i=0,n-1 do begin
    corr  = R_mx[0,i]*R_mx[1,i]
    rank_corr =    rank_corr +    corr
endfor

cov_est = (K + (4*rank_corr) - (n*(n+1)*(n+1))  )/3
return,cov_est
end

