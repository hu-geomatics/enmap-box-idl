function autosc,y
 y=float(y)
 std=stddev(y)
 if std gt 0 then y=(y-mean(y))/std
 return,y
end