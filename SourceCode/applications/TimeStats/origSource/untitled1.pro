pro PACF,x,dt,lags,pacfSig
  nobs=n_elements(x);
  nSTDs =  2;     ; Default is 2 standard errors (~95% condfidence interval).
  partialcorr=[1.,fltarr(lags)]
  if dt eq 1 then begin
      x=x[1:nobs-1]-x[0:nobs-2]
      nobs=nobs-1
  endif

  lags=min([lags,n_elements(x)-1])

  xOLS=fltarr(lags,n_elements(x[lags:nobs-1]))
  o=0;
  for h=0,Lags[0]-1 do begin
                          xOLS[o,*]=y[lags-h-1:nobs-1-h-1]; y series will be shortened by maxlag later!!
                          o=o+1
                        end
  y=x[maxlag:nobs-1]
  for i=1,lags do begin
     result=regress(xOLS,y)
     partialcorr[i]=result[i-1]
  endfor
  ;
  ; Compute approximate confidence bounds using the Box-Jenkins-Reinsel
  ; approach, equations 3.2.36 and 6.2.3, on pages 68 and 188, respectively.
  ;
  ; Note a subtle point here: The Pth autoregressive model 'fit' via OLS
  ; makes use of only the most recent (n - P) observations. Since the
  ; approximate confidence bounds for the hypothesized P is of interest
  ; only for lags > P, and the (P+1)th AR model uses (n - (P + 1) = n - p - 1
  ; observations, the 'n' in BJR equation 3.2.36 (i.e., the number of
  ; observations used in 'fitting') is taken to be (n - P - 1) rather than
  ; the original length of Series. Moreover, the effective number of
  ; observations used in 'fitting' each successive AR model will decrease
  ; by one observation for each lag. For even moderate sample sizes, this
  ; approximation should make little difference.
  ;

  bound  =  nSTDs / sqrt(n - 1)
  temp=where (partialcorr lt bound,count)
  if count gt 0 then pacfSig=temp(0)+1
end


