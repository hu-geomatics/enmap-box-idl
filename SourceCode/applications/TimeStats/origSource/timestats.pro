;####################################################################################################################################################
; Timeseries wertet Zeitreihen von Bilddaten aus, die endweder im LAN- oder BIL-format vorliegen.
; Bei dem ben�tigten File mit den Inputinfos handelt es sich um eine ASCII-Datei, die zeilenweise die Dateinamen (mit Pfad!) sowie eine sinnvolle
; numerische Zeitangabe f�r die Szene enth�lt. Das verlagte Format f�r jede Zeile dieser Datei ist "Dateiname <tab> Zeitangabe"
; Timeseries sch�tzt Zeitreihenparameter f�r die Zeitreihe und schreibt diese als ENVI-File weg.
; Werden keine Namen f�r die Outputfiles gew�hlt so werden dieser automatisch im Verzeichnis der Batchdatei angelegt.
; Die Bedeutung der Parameter ist folgende:
;
; Reg. bias (b): Regressionskoeff. der Trendgeraden
; Reg. offset:   Regressionskonstante der Trendgeraden
; Sigma:         Stdabw. von b
; T-test(b):     Testen der Signifikanz von b
; r:          Korrelationskoeffizient des Trends
; RMS:        RMS-Fehler des lin. Regressionsmodells
; RMS/mean
; MaxResid:      Maximales Residuum (f�r Aussreisser-Erkennung)
; TimeMaxResid:  Zeitpunkt des max. Residuums
; MK test:       Mann-Kenall Test
; K slope:    Kendall Slope
; SK test:    Seasonal Kendall Test
; SK slope:      Seasonal Kendall slope
; Mod. SK test:  Modified seasonal Kenall test
; Stddev
; Mean
; Varkoeff
; valid n:    Zahl der g�ltigen F�lle in einer Zeitreihe
; skewness
; kurtosis
; Abbe criterion: einfacher Test auf Homogenit�t der Zeitreihe

; Optional k�nnen missing values (MV) ersetzt werden, endweder aufgrund einer linearen Interpolation durch Nachbarwerte
; oder durch einsetzen des Mittwelwerts der Zeitreihe.
; Die Cross-v alidation Option (leave-one-out) ist nur bei sehr kurzen Zeitreihen sinnvoll, da sehr viele Ausgabefiles
; angelegt werden (entsprechend n Modelldurchg�ngen).
; Bei "Seasonal frequency for Kendall Tests" ist die saisonale Frequenz der (�quidistanten) Eingangsdaten anzugeben,
; z.B. bei Monatsdaten 12
; Es wird kanalweise ein Layerstack aller Orininal-Einzelbilder durchgef�hrt.
; Ein weiteres Feature ist die Hot-spot detection, die auf Differenzbilder beruht:
; Ist die Differenz bei einem Pixel gr��er als ein prozentualer Grenzwert wird an die
; entsprechende Stelle ins Ausgabebild eine 1 geschreiben, ansonsten eine 0.
; Als Option k�nnen zus�tzlich entweder die absoluten Betr�ge der Fourierkoeffizienten weggeschrieben werden,
; oder eine einfache Filterung im Fourierbereich erfolgen. Hierf�r wird die Angabe einer
; Startfrequenz (x/P, mit P= L�nge der Zeitreihe), sowie ein Grenzwert ben�tigt. Fourierkoeffizienten, die
; diesen Grenzwert �berschreiten werden 0 gesetzt und dadurch Teilschwingungen eliminiert.
; Optional kann die Berechnung der Zeitreihenparameter aufgrund der gefilterten Zeitreihe erfolgen
; Maskenpixel werden nicht als missing values gewertet sondern �bergangen.
; WICHTIG: �nderung numerischer Werte im Widget sind jeweils durch RETURN zu best�tigen!!
;
; Aufruf: main
;
;####################################################################################################################################################
pro timeStats, GROUP=group
  @timestats_initcommonblocks ; changed by Andreas Rabe: code excluded to seperate procedure for reusage
  base_widget, GROUP=GROUP            ; Aufruf GUI
end

