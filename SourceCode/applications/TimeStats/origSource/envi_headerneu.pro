pro envi_headerNEU,out,cols,rows,bands,DOS
 printf,out,'ENVI'
 printf,out,'description = {Regression}'
 printf,out,'samples = ', cols
 printf,out,'lines = ',rows
 printf,out,'bands = ', fix(bands)
 printf,out,'file type = ENVI Standard'
 printf,out,'data type = 4'
 printf,out,'interleave = bsq'
 if DOS eq 0 then printf,out,'byte order = 1' else printf,out,'byte order = 0'
end

