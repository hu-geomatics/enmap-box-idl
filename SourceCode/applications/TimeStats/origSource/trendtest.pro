function trendtest, flag, x, y,missvalue, mv, lagsADF, lagsOLS, $
                    flagUR, flagR, X1,X2,X3,X4,X5, quest,fp, FromTo,nwindDiff,noverlapDiff
; flags: bytarr(25): determines trend parameters to calculate from y
; x: fltarr: time
; y: fltarr: dependand variable
; missvalue: 1 = y may contain missing values. 0 = no missing values
; mv: Missing value code
; quest.freq: frequency component for Seasonal Kendall tests (e.g. 12 to account for the annual in monthly y-data )
; lagsADF: Lags to account for in the Augmented Dickey-Fuller test
; lags: Array with lags for max 6 independand external variables in ols-regression (lags[0]=time-lag)
; flagUR=bytarr(6); Parameters for unrestricted regression: flagUR(0)= 1: use X1=time, flagUR(1,...5)= use
; further external X variables (X2, ..., X6)
; flagUR=bytarr(6); Parameters for restricted regression
; X1, ..., X5: Optional time series of additional X varibles for regression
; quest.diff: 1: use differenced time-series for unrestricted/restricted regression



; Regressionsroutine zur pixelweisen Berechnung des Trends
; Pr�fen auf NaN
 temp=[reform(y), x,x1,x2,x3,x4,x5]
 temp1=WHERE(FINITE(Temp) EQ 0,count)
 if count gt 0 then NaN=1 else NaN=0
 maxlag=max(lagsOLS)
 if maxlag eq 0 then maxlag=1
  NoHarm=max([((quest.NoHarmonics1+quest.NoHarmonics2)*2),1])
  subOlsUR ={const:         -999.,                         $     ; offset
                 b:         REPLICATE(-999.,7,maxlag+1),   $     ; Max 6 externe Variablen (incl. time) + lagY
                 t:         REPLICATE(-999.,7,maxlag+1),   $     ; t-Statistik f�r Reg. koeff
                 tsig:      REPLICATE(-999.,7,maxlag+1),   $     ; Sig. von t
                 r:         REPLICATE(-999.,7,maxlag+1),   $     ; r
                 t2to5:     REPLICATE(-999.,4),            $     ; h�here Polynomterme
                 t2to5T:    REPLICATE(-999.,4),            $
                 t2to5Tsig: REPLICATE(-999.,4),            $
                 t2to5r:    REPLICATE(-999.,4),            $
                 F:         -999.,                         $     ; F-Statistik f�r Gesamtmodell
                 Fsig:      -999.,                         $     ; Sig F
                 DW:        -999.,                         $     ; Durbin-Watson Testgr��e
                 NoVar:     -999.,                         $
                 nobs:      -999.,                         $
                 sige:      -999.,                         $     ; e'*e/(noObs-noVar) f�r Wald-Test
                 WaldF:     -999.,                         $     ; Wald Statistik f�r 2 Regressionsmodelle (UR /R)
                 Waldsig:   -999.,                         $
                 fitY1:     -999.,                         $     ; erster und letzer gesch�tzter Wert
                 fitYend:   -999.,                         $
                 relGrowth: -999.,                         $     ; prozentualer j�hrlicher Zuwachs durch geometrisches Mittel
                 absGrowth: -999.,                         $     ; absoluter j�hrlicher Zuwachs durch geometrisches Mittel
                 Rmult:     -999.,                         $     ; multipler Korrelationskoeffizient
                 FP:        REPLICATE(-999.,NoHarm),       $     ; Cosinus und Sinusterme der FPs
                 fpT:       REPLICATE(-999.,NoHarm),       $     ; t-emp
                 fpTsig:    REPLICATE(-999.,NoHarm),       $     ; t-sig
                 ChangeFrTo:REPLICATE(-999.,n_elements(FromTo)),   $ ;moving windows f�r Trenddifferenzen
                 resid:     REPLICATE(-999.,n_elements(y))}      ; nimmt evtl Residuen auf

 subOlsR=subOlsUR
 ADFkoeff=fltarr(8+(lagsADF*3))

 trendpar={b:0., Const:0., Sigma:0., Ttest:0.,  R:0., MKslope:0.,  MKtest:0.,                       $
           Status :0., Maxwert:0., TimeMax:0., RMS:0., Stdabw:0., SMKtest:0., mittel:0.,            $
           SMKslope:0., MSMKtest:0., varkoeff:0., relerror:0.,skew:0., kurt:0., abbe:0.,            $
           rho:0., tau:0., sigrho:0., sigtau:0.,mini:0.,maxi:0.,MinToMax:0.,MaxToMin:0.,range:0.,   $
           ADFkoeff:fltarr(8+(lagsADF*3)),                                                          $
           olsUR: subOlsUR, olsR:subOlsR                                                            }
                      ; ADFkoeffs(0)=const,ADFkoeffs(1)=trend, ADFkoeffs(2)=Yt-1
                      ; ADFkoeffs(1... noXvar)= reg koeff Yt-1, dYt-1...,dYt-n
                      ; ADFkoeffs(noXvar+2)=sig. t Yt-1, ...
                      ; ADFkoeffs(noXvar+3...,noXvar+3+lagsADF-1)=sig.t temp, sig t dYt-1, ...
                      ; ADFkoeffs(noXvar+4+lagsADF-1...,end-1)=t emp
                      ; ADFkoeffs(end)=DW-Test


   ok=1 ; wenn ok =1 dann gen�gend WErte f�r Regression vorhanden
  ; missing values in der Zeitreihe abfangen und verbleibende Daten f�r Reg. analyse in neuen Vektor kopieren...
 trendpar.status=n_elements(y)
 yoMV=y ; f�r den Fall das Missing values ausgeschlossen werden sollen. VORSICHT: nur f�r normale Reg.analyse
 xoMV=x ; und f�r Statistiken, bei denen keine �quidistanten Werte vorausgesetzt werden!!!!
 if (missvalue eq 1) and (min(y) ne max(y)) then begin
   temp=where(y eq mv,count)
   trendpar.status=n_elements(y) - count  ; Anzahl g�ltiger F�lle
   if count gt 0 then begin
     temp=where(y ne mv)
     yoMV=y(temp)
     xoMV=x(temp)
   endif
   xoMV=transpose(xoMV)
 endif
 ; bei weniger als drei g�ltigen F�llen oder Masken keine Trendanalyse
 if (trendpar.Status gt 3) and (min(yoMV) lt max(yoMV)) and (NaN eq 0) $
  then begin
   ;Regressionsanalyse
   if (flag[0] eq 1) or (flag[1] eq 1) or (flag[2] eq 1) or (flag[3] eq 1) or $
      (flag[4] eq 1) or (flag[5] eq 1) or (flag[6] eq 1) or $
      (flag[7] eq 1) or (flag[8] eq 1) then begin
                                 yoMV=reform(yoMV)
                                 b=regress(xoMV,yoMV,Yfit=Yfit,CONST=const,SIGMA=sigma,CORRELATION=r)
                                 trendpar.b=b & trendpar.const=const & trendpar.sigma=sigma
                                 trendpar.r=r
   end
   ;t-Test des Reg. koeffizienten
   if flag[3] eq 1 then begin
      ;Ttest= sqrt(variance(y)-(b*b)*variance(x))/(stddev(x)*sqrt(n_elements(y)-2))
      ;Ttest=b/Ttest
      trendpar.Ttest=trendpar.b/trendpar.Sigma
      if quest.sig eq 1 then begin ; Voreichen bestimmt ob Sig.niveau positiv oder negativ ausgegeben wird!!
            temp = min([1 - T_PDF(trendpar.Ttest,trendpar.Status), T_PDF(trendpar.Ttest,trendpar.Status)])*2
            if trendpar.Ttest lt 0 then trendpar.Ttest= temp * (-1) else  trendpar.Ttest=temp
      end
   endif
   ; berechnen des RMS-Fehlers
   if (flag[5] eq 1) or (flag[6] eq 1) then begin
      temp=yoMV-yfit[*]
      temp=temp^2
      trendpar.RMS=sqrt(total(temp)/n_elements(temp))
     ;Yfit_1_n=yfit[n_elements(yfit)-1]-yfit[0]  ;Differenz erstes und letztes Jahr
   endif
   if flag[6] eq 1 then trendpar.relerror=trendpar.RMS/mean(yoMV)
   if (flag[7] eq 1) or (flag[8] eq 1) then begin
        temp=yoMV-yfit[*]
        pos = where(abs(temp) eq max(abs(temp))); finden der Position mit maximaler Abweichung von Trendgeraden
        trendpar.Maxwert=temp(pos[0])             ; maximale Abweichung (erster Wert!!, falls mehrere)
        trendpar.TimeMax=xoMV(pos[0])
              ; Differenzen max-abw. zum vorherigen/nachfolgenden Jahr

        ;TimeMax=(100./yfit[0]*yfit[n_elements(yfit)-1]) - 100.
        ;temp=where((TimeMax gt 100) or (TimeMax lt -100),count)
        ;if count gt 0 then TimeMax(temp)=0.

        ;TimeMax=(yfit[n_elements(yfit)-1] - yfit[0])/10000.
        ;TimeMax=y([n_elements(yfit)-1] - y[0]) / 10000.
        ;temp=where((TimeMax gt 1) or (TimeMax lt -1),count)
        ;if count gt 0 then TimeMax(temp)=0.



        ;if pos[0] gt 0 then diffvor =y[pos[0]]-y[pos[0]-1] else diffvor =0
        ;if pos[0] lt n_elements(y)-2 then diffnach=y[pos[0]]-y[pos[0]+1] else diffnach=0
   endif
   ; Mann-Kenall Test (nicht saisonal)
   if flag[9] eq 1 then trendpar.MKtest=mk(y,missvalue,mv,quest.sig)
   ; Mann-Kenall slope (nicht saisonal)
   if flag[10] eq 1 then trendpar.MKslope=Kendall_slope(y,missvalue,mv)
   ; Mann-Kenall Test (saisonal)
   if flag[11] eq 1 then trendpar.SMKtest=smk(y,quest.freq,missvalue,mv,quest.sig)
   ; Mann-Kenall slope (saisonal)
   if flag[12] eq 1 then trendpar.SMKslope=SKendall_slope(y,quest.freq,missvalue,mv)
   ; Mann-Kenall Test (modified saisonal, mit Ber�cksichtigung der Autokorrelation)
   if flag[13] eq 1 then trendpar.MSMKtest=msk(y,quest.freq,missvalue,mv,quest.sig)
   ; max. Residuum und dessen Zeitpunkt
   if flag[14] eq 1 then trendpar.Stdabw=stddev(yoMV)
   if flag[15] eq 1 then trendpar.mittel=mean(yoMV)
   if flag[16] eq 1 then trendpar.varkoeff=((stddev(yoMV)/mean(yoMV))/sqrt(n_elements(yoMV)))*100.
   ;if flag[16] eq 1 then trendpar.varkoeff=(stddev(yoMV)/mean(yoMV))*100.
   if flag[18] eq 1 then trendpar.skew=skewness(yoMV)
   if flag[19] eq 1 then trendpar.kurt=kurtosis(yoMV)
   ;if flag[20] eq 1 then trendpar.abbe=abbe_crit(y)
   if flag[20] eq 1 then begin
     prewhite, y, AR, MA
      ;trendpar.abbe=n_elements(AR)
      if n_elements(AR) gt 0 then trendpar.abbe=AR[0] else  trendpar.abbe=0
   end


   if (flag[21] eq 1) or (flag[23] eq 1) then begin
         temp=R_CORRELATE(xoMV, yoMV)
         trendpar.rho=temp[0]
         trendpar.sigrho=temp[1]
   end
   if (flag[22] eq 1) or (flag[24] eq 1) then begin
         temp=R_CORRELATE( xoMV, yoMV,/KENDALL)
         trendpar.tau=temp[0]
         trendpar.sigtau=temp[1]
   end
   if flag[25] eq 1 then trendpar.mini=min(yoMV)
   if flag[26] eq 1 then trendpar.maxi=max(yoMV)
   if flag[27] eq 1 then trendpar.MinToMax=Tmintomax(x,y)
   if flag[28] eq 1 then trendpar.MaxToMin=Tmintomax(reverse(x),reverse(y))
   if flag[29] eq 1 then trendpar.range=max(yoMV)-min(yoMV)
   if flag[30] eq 1 then begin
                           ADF, x,y,lagsADF, ADFkoeff
                           trendpar.ADFkoeff = ADFkoeff
                         end
   if max(flagR)  eq 1 then begin
                              olsReg, subOlsR,x,y,lagsOLS,flagR, X1,X2,X3,X4,X5,quest,fp, FromTo,nwindDiff,noverlapDiff
                              trendpar.OlsR=subOlsR
                            end
   if max(flagUR) eq 1 then begin
                       lagsOLS_temp=lagsOLS
                              olsReg, subOlsUR,x,y,lagsOLS_temp,flagUR, X1,X2,X3,X4,X5,quest,fp, FromTo,nwindDiff,noverlapDiff

                             if (quest.autoLAG eq 0) and (maxlag gt 0) and (max(flag[5:9]) gt 0) then begin  ; Automatische Anpassung der LAGS
                                subOlsUR.fitY1=-1  ; Voreinstellung, keine significante Korr. (nur f�r erste externe Variable!)
                                flagExt = 1        ; Voreindstellung gilt nur f�r erste Variable zur Unterscheidnung von sig. und nicht sig. lag0-Korrelation

                                temp=where(lagsOLS gt 0,count)
                                for k=0,count-1 do begin  ; Schleife �ber aller externe Variablen
                                  temp1=where(abs(subOlsUR.tsig[temp[k]+1,*]) lt 0.05,count1)  ; 10% Irrtumswahrscheinlichkeit

                                  ; Problem: h�here Lags werden manchmal per Zufall signifikant, daher wird gepr�ft ob auch der vorausgehende Lag (bis auf Lag 0) sig ist

                                  if (count1 gt 0) then begin
                                   if temp1[n_elements(temp1)-1] eq 0 then lagsOLS_temp[temp[k]]=0 else $ ; significant nur bei lag 0
                                   if temp1[n_elements(temp1)-1] eq 1 then lagsOLS_temp[temp[k]]=1 else $ ; significant bei lag 1 und evtl. bei lag 0

                                                                                                             ; mindestens zwei aufeinanderfolgende lags sind sig.
                                   if (temp1[n_elements(temp1)-1] - temp1[n_elements(temp1)-2] eq 1) then lagsOLS_temp[temp[k]]=temp1[n_elements(temp1)-1] else $
                                       begin
                                          lagsOLS_temp[temp[k]]=0                                      ; eliminieren von zuf�lliger h�herer Korr.
                                          if k eq 0 then flagExt = 0                                   ; nur f�r erste externe Variable!
                                       end
                                  end
                                endfor
                                olsReg, subOlsUR,x,y,lagsOLS_temp,flagUR, X1,X2,X3,X4,X5,quest,fp, FromTo,nwindDiff,noverlapDiff
                                if flagExt eq 1 then subOlsUR.fitY1=lagsOLS_temp[1]  ; f�r erste externe Variable Gesamtlags wegspeichern.
                            end

                            trendpar.OlsUR=subOlsUR

                            end
   if max(flagR) eq 1 then begin
                              wald_F,subOLsUR,subOlsR
                              trendpar.OlsR=subOlsR  ; Eintr�ge f�r Waldstatistik sind identisch in subOlsR/UR
                              trendpar.OlsUR=subOlsUR
                           end
  end

  return,trendpar
end
