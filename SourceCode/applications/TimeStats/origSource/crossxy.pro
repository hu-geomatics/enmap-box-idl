pro crossxy,x,y,nwind,windType,noverlap, co, qu, coh,p,amp
;COMMON blk1
;   adopted from Matlab function cohere.m

; compute PSD and CSD

;umwandeln in Zeilenvektoren, falls erforderlich
x=reform(x)
y=reform(y)
n = n_elements(x)         ;  Number of data points
; create window
case windType of
 'HANN':window=HANNING(nwind)
 'HAMM':window=HANNING(nwind,ALPHA = 0.54)
 'BARTLETT':window=barlett(nwind)
 'NO':window=fltarr(nwind)+1
end

if n lt nwind then begin            ;  zero-pad x , y if length is less than the window length
    xnew=fltarr(nwind)
    ynew=xnew
    x[0:n-1]=x
    y[0:n-1]=y
    x[n:nwind]=0
    y[n:nwind]=0
    n=nwind
    x=xnew
    y=ynew
end

k = fix((n-noverlap)/(nwind-noverlap));  Number of wind
                                      ; (k = fix(n/nwind) for noverlap=0)
index = indgen(nwind);

Pxx = fltarr(n) & Pxx2 = fltarr(n);
Pyy = fltarr(n) & Pyy2 = fltarr(n);
Pxy = fltarr(n) & Pxy2 = fltarr(n);

for i=0,k-1 do begin
    ;windowing and padding xw and yw with trailing zeros to length n
    xw = fltarr(n)
    yw = fltarr(n)
    xw[n-nwind:n-1] = window*x[index]
    yw[n-nwind:n-1] = window*y[index]
    index = index + (nwind - noverlap)


    Xx = fft(xw)
    Yy = fft(yw)
    Xx2 = abs(Xx)^2
    Yy2 = abs(Yy)^2
    Xy2 = Yy*conj(Xx)
    Pxx = Pxx + Xx2
    Pxx2 = Pxx2 + abs(Xx2)^2
    Pyy = Pyy + Yy2
    Pyy2 = Pyy2 + abs(Yy2)^2
    Pxy = Pxy + Xy2
    Pxy2 = Pxy2 + Xy2*conj(Xy2)
end
Amp=abs(Xy2)                           ;Amplitude spectrum
co = float(Xy2)                  ;Cospectrum
qu = imaginary(Xy2)                    ;Quatratur spectrum
Coh = (abs(Pxy)^2) / (Pxx*Pyy)         ;coherence function estimate

;Coh = abs(Pxy) / Pxx         ;gain function estimate

p=atan(xy2, /PHASE) * (-1.)            ; Phasenspektrum, vorzeichen umdrehen
temp=where(p lt 0, count)
;if count gt 0 then p[temp] =!pi + (p[temp]+!pi) ; Phasenspektrum jetzt im Bereich 0-2pi
;p=12. / (2 * !pi)*p
end