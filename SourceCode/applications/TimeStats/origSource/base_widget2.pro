pro base_widget2_event, ev1, GROUP=group
 COMMON WL2, _MorletP, _PaulP, _DOGP, _SIGLVL, _DT, _S0, _DJ, _NrScl, _scale_l, _scale_h
 COMMON blk1
 COMMON blkbase

  widget_control, ev1.id, get_uvalue=uval

  case uval of
    'group0' : case ev1.value of  ; Mother wavelet and optional wavlet parameters
           0:begin
               Mother='Morlet'
               MothWlNr=0
               WIDGET_CONTROL, baseMorlet, SENSITIVE=1
               WIDGET_CONTROL, basePaul, SENSITIVE=0
               WIDGET_CONTROL, baseDOG, SENSITIVE=0
              end
            1:begin
               Mother='Paul'
               MothWlNr=1
               WIDGET_CONTROL, baseMorlet, SENSITIVE=0
               WIDGET_CONTROL, basePaul, SENSITIVE=1
               WIDGET_CONTROL, baseDOG, SENSITIVE=0
              end
            2:begin
               Mother='DOG'
               MothWlNr=2
               WIDGET_CONTROL, baseMorlet, SENSITIVE=0
               WIDGET_CONTROL, basePaul, SENSITIVE=0
               WIDGET_CONTROL, baseDOG, SENSITIVE=1
              end
               endcase
      'group0_': WIDGET_CONTROL, _DT, GET_VALUE=DT
      'group01': WIDGET_CONTROL, _MorletP, GET_VALUE=MorletP
      'group02': WIDGET_CONTROL, _PaulP, GET_VALUE=PaulP
      'group03': WIDGET_CONTROL, _DOGP, GET_VALUE=DOGP
      'group1' : begin
                  case ev1.value of   ; Pad time series
                     0:PAD=0       ; no
                     1:PAD=1       ; yes
                  endcase
                 end
      'group2' : begin
                   case ev1.value of   ; SIGNIF Test
                     0:LAG1=1        ; based on lag 1
                     1:LAG1=0        ; based on global wavelet spectrum
                   endcase
                 end
      'group3': WIDGET_CONTROL, _SIGLVL, GET_VALUE=SIGLVL
      'group3a':WIDGET_CONTROL, _S0, GET_VALUE=S0
      'group3b':WIDGET_CONTROL, _DJ, GET_VALUE=DJ
      'group3c':WIDGET_CONTROL, _NrScl, GET_VALUE=NrScl
      'group3d':WIDGET_CONTROL, _scale_l, GET_VALUE=scale_l
      'group3e':WIDGET_CONTROL, _scale_h, GET_VALUE=scale_h
      'group4' :begin
                  case ev1.value of
                   0:VERBOSE=1   ; yes
                   1:VERBOSE=0   ; no
                  endcase
                end
      'done'  : WIDGET_CONTROL, ev1.top,/DESTROY
  endcase
end


pro base_widget2, GROUP=GROUP1
 COMMON blk1
 COMMON WL2
 COMMON blkbase

  values0=['Morlet','Paul','DOG (derivative of Gaussian)']
  values1=['no','yes']
  values2=['LAG 1 Autocorrelation','global wavelet spectrum']

  base2=WIDGET_BASE(TITLE='Wavelet analysis',/COLUMN)
  bgroup0= CW_BGROUP(base2,values0,/ROW, SET_VALUE=MothWlNr,LABEL_TOP='Select Mother Wavelet',/FRAME, /EXCLUSIVE,UVALUE='group0')

  baseMorlet= WIDGET_BASE(base2, /COLUMN,TITLE='Optional Morlet wavelet parameter')
  _MorletP=CW_FIELD(baseMorlet,/INTEGER,/ALL_EVENTS,VALUE=MorletP,TITLE='k0 (wavenumber)',UVALUE='group01')

  basePaul= WIDGET_BASE(base2, /COLUMN,TITLE='Optional Paul wavelet parameter')
  _PaulP=CW_FIELD(basePaul,/INTEGER,/ALL_EVENTS,VALUE=PaulP,TITLE='m (order)',UVALUE='group02')

  baseDOG= WIDGET_BASE(base2, /COLUMN,TITLE='Optional DOG wavelet parameter')
  _DOGP=CW_FIELD(baseDOG,/INTEGER,/ALL_EVENTS,VALUE=DOGP,TITLE='m (m-th derivative)',UVALUE='group03')

  _DT=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,VALUE=DT,TITLE='amount of time between each Y value, i.e. the sampling time.',UVALUE='group0_')

  bgroup1= CW_BGROUP(base2,values1,/ROW, SET_VALUE=PAD,LABEL_TOP='pad time series with zeroes',/FRAME, /EXCLUSIVE,UVALUE='group1')
  bgroup2= CW_BGROUP(base2,values2,/ROW, SET_VALUE=abs(1-LAG1),LABEL_TOP='SIGNIF level based on',/FRAME, /EXCLUSIVE,UVALUE='group2')

 _SIGLVL=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,VALUE=SIGLVL,TITLE='SIGLVL = significance level to use',UVALUE='group3')
 _S0=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,TITLE='smallest scale of the wavelet (Default:2*DT)',UVALUE='group3a')
 _DJ=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,VALUE=DJ,TITLE='the spacing between discrete scales',UVALUE='group3b')
 _NrScl=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,TITLE='# of scales minus one (Default:(LOG2(N DT/S0))/DJ) ',UVALUE='group3c')

 _scale_l=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,VALUE=scale_l,TITLE='smallest scale for scale averaging',UVALUE='group3d')
 _scale_h=CW_FIELD(base2,/FLOAT,/ALL_EVENTS,VALUE=scale_h,TITLE='largest scale for scale averaging',UVALUE='group3e')

  bgroup3= CW_BGROUP(base2,values1,/ROW, SET_VALUE=abs(1-VERBOSE),LABEL_TOP='Print out info for each analyzed scale',/FRAME, /EXCLUSIVE,UVALUE='group4')

  endbase = WIDGET_BASE(base2, /ROW,/ALIGN_BOTTOM)

  done= widget_button(endbase, VALUE='done', UVALUE='done')


  case MothWlNr of
   0:begin
       WIDGET_CONTROL, baseMorlet, SENSITIVE=1
       WIDGET_CONTROL, basePaul, SENSITIVE=0
       WIDGET_CONTROL, baseDOG, SENSITIVE=0
     end
   1:begin
       WIDGET_CONTROL, baseMorlet, SENSITIVE=0
       WIDGET_CONTROL, basePaul, SENSITIVE=1
       WIDGET_CONTROL, baseDOG, SENSITIVE=0
     end
   2:begin
       WIDGET_CONTROL, baseMorlet, SENSITIVE=0
       WIDGET_CONTROL, basePaul, SENSITIVE=0
       WIDGET_CONTROL, baseDOG, SENSITIVE=1
     end
  endcase


  WIDGET_CONTROL, base2, /NO_COPY,/REALIZE
  XMANAGER,'base_widget2', base2, GROUP=GROUP1

end
