pro WL,data_vec,scale_avg, zeit,MorletP,PaulP,DOGP, Mother, PAD,LAG1,SIGLVL, VERBOSE,DT,DJ,$
    S0,NrScl,scale_l, scale_h,LOCAL=local
    if S0 eq 0 then S0=2*DT ; Default Wavelet-Parameter
;   if NrScl eq 0 then NrScl = (LOG2(N* DT/S0))/DJ. ; Default
   if NrScl eq 0 then NrScl = 9./dj  ; this says do 9 powers-of-two with dj sub-octaves each
   if LAG1 eq 1 then    r1 = (A_CORRELATE(data_vec,1) + SQRT(A_CORRELATE(data_vec,2)))/2.
; normalize by standard deviation
   n=n_elements(data_vec)
   data_vec_ori=data_vec
   data_vec = transpose((data_vec - TOTAL(data_vec)/n))
   data_recon=data_vec

   case mother of
    'Morlet':WLpara=MorletP
    'Paul': WLpara=PaulP
    'DOG':WLpara=DOGP
   endcase
; Wavelet transform:
    wave = WAVELET(data_recon,DT,PERIOD=period,SCALE=scale,S0=s0,PAD=pad,COI=coi,DJ=dj,$
                   J=NrScl,MOTHER=mother,PARAM=WLpara,/RECON)
    power = (ABS(wave))^2  ; compute wavelet power spectrum
    global_ws = TOTAL(power,1)/n   ; global wavelet spectrum (GWS)

; GWS significance levels:
    dof = n - scale   ; the -scale corrects for padding at edges
    ;global_signif = WAVE_SIGNIF(data_vec,DT,scale,1,LAG1=0.0,DOF=dof,MOTHER=mother,PARAM=WLpara, CDELTA=Cdelta,PSI0=psi0)
    global_signif = WAVE_SIGNIF(data_vec,DT,scale,1,LAG1=0.0,DOF=dof,SIGLVL=siglvl, $
                                MOTHER=mother,PARAM=WLpara, CDELTA=Cdelta,PSI0=psi0)
; Significance levels, either assuming GWS or LAG1 background spectrum:
    J = N_ELEMENTS(scale) - 1
   if KEYWORD_SET(LOCAL) then begin
        if LAG1 eq 0 then signif = WAVE_SIGNIF(data_vec,dt,scale,0,GWS=global_ws,SIGLVL=siglvl,MOTHER=mother) $
        else signif = WAVE_SIGNIF(data_vec,dt,scale,0,LAG1=r1,SIGLVL=siglvl,MOTHER=mother)
       signif = REBIN(TRANSPOSE(signif),n,J+1)  ; expand signif --> (J+1)x(N) array
       signif = power/signif   ; where ratio > 1, power is significant
       time = FINDGEN(n)*dt + zeit[0]  ; construct time array
        xrange = [zeit[0],zeit[0]+DT*n]  ; plotting range
   endif
; check total variance (Parseval's theorem) [Eqn(14)]
       scale_avg = REBIN(TRANSPOSE(scale),n,J+1)  ; expand scale-->(J+1)x(N) array
       power_norm = power/scale_avg
       variance = (MOMENT(data_vec))(1)
       recon_variance = dj*dt/(Cdelta*n)*TOTAL(power_norm)  ; [Eqn(14)]

; Scale-average periods of scale_l- scale_h years
       avg = WHERE((scale GE scale_l) AND (scale LT scale_h))
       scale_avg = dj*dt/Cdelta*TOTAL(power_norm(*,avg),2)  ; [Eqn(24)]
       scaleavg_signif = WAVE_SIGNIF(data_vec,dt,scale,2, $
                   GWS=global_ws,SIGLVL=siglvl,DOF=[scale_l,scale_h],MOTHER=mother)

   if KEYWORD_SET(LOCAL) then begin
       IF (N_ELEMENTS(data_recon) GT 1) THEN BEGIN
         recon_variance = (MOMENT(data_recon))(1)
        ; RMS of Reconstruction [Eqn(11)]
         rms_error = SQRT(TOTAL((data_vec - data_recon)^2)/n)
         PRINT
         PRINT,'        ******** RECONSTRUCTION ********'
         PRINT,'original variance =',variance,' degC^2'
         PRINT,'reconstructed var =',FLOAT(recon_variance),' degC^2'
         PRINT,'Ratio = ',recon_variance/variance
         PRINT,'root-mean-square error of reconstructed sst = ',rms_error,' degC'
         PRINT
         IF (mother EQ 'DOG') THEN BEGIN
          PRINT,'Note: for better reconstruction with the DOG, you need'
          PRINT,'      to use a very small s0.'
         ENDIF
         PRINT
       ENDIF
;------------------------------------------------------ Plotting
       printfile = 0

       !P.FONT = -1
       !P.CHARSIZE = 1
       IF (printfile) THEN BEGIN
         SET_PLOT,'ps'
         DEVICE,/PORT,/INCH,XSIZE=6.5,XOFF=1,YSIZE=6,YOFF=3,/COLOR,BITS=8
         !P.FONT = 0
         !P.CHARSIZE = 0.75
       ENDIF ELSE WINDOW,0,XSIZE=600,YSIZE=600
       !P.MULTI = 0
       !X.STYLE = 1
       !Y.STYLE = 1
       LOADCT,6
;--- Plot time series
         pos1 = [0.1,0.75,0.7,0.95]
         PLOT,time,data_vec_ori,XRANGE=xrange, $
         XTITLE='Time',YTITLE='Value', $
        ; TITLE='a) normalized values', $
         TITLE='a) NDVI', $
         POSITION=pos1
       ;IF (N_ELEMENTS(data_recon) GT 1) THEN OPLOT,time,data_recon,COLOR=144
          XYOUTS,0.85,0.9,/NORMAL,ALIGN=0.5, $
          '!5WAVELET ANALYSIS!X'+$
          '!C!Ctime series';+$
               ;'!C!Cdots: reconst. time series'


;--- Contour plot wavelet power spectrum
         ;yrange = [64,0.5]   ; years
         yrange = [max(scale),min(scale)]

           ;levels =[mean(power)-2*stddev(power),mean(power)-stddev(power), $
            ;      mean(power),mean(power)+stddev(power),mean(power)+2*stddev(power)]
           levels =[mean(power)-2*stddev(power),mean(power)-1.5*stddev(power),mean(power)-1*stddev(power), mean(power)-0.5*stddev(power),$
                    mean(power),mean(power)+0.5*stddev(power),mean(power)+1*stddev(power),mean(power)+1.5*stddev(power), $
                    mean(power)+2*stddev(power)]


         ;colors = [16,32, 50,64,74,128,160,230,254]
         ;colors = [32, 64,128,208,254]
         period2 = FIX(ALOG(period)/ALOG(2))   ; integer powers of 2 in period
         ytickv = 2.^(period2(UNIQ(period2)))  ; unique powers of 2
         pos2 = [pos1(0),0.35,pos1(2),0.65]


       c1=convert(mean(power)-2*stddev(power),1)
       c2=convert(mean(power)-stddev(power),1)
       c3=convert(mean(power),2)
       c4=convert(mean(power)+stddev(power),1)
       c5=convert(mean(power)+2*stddev(power),1)

       label=convert(siglvl,2)
       label=strcompress(label)+'%'

       c=strcompress(c1)+' ' +strcompress(c2)+' '+strcompress(c3)+ ' '+strcompress(c4)+' '+strcompress(c5)

         CONTOUR,power,time,period,/NOERASE,POSITION=pos2, $
          XRANGE=xrange,YRANGE=yrange,/YTYPE, $
          YTICKS=N_ELEMENTS(ytickv)-1,YTICKV=ytickv, $
          LEVELS=levels,C_COLORS=colors,/FILL, $
          XTITLE='Time',YTITLE='Period', $
              ;TITLE='b) Power Spectrum (contours at '+ c + ')'
              TITLE='b) Power Spectrum'
       ;     TITLE='b) Wavelet Power Spectrum (contours at 0.5,1,2,4!Uo!NC!U2!N)'
       ; significance contour, levels at -99 (fake) and 1 (significant)
         CONTOUR,signif,time,period,/OVERPLOT,LEVEL=1,THICK=2, $
          C_LABEL=1,C_ANNOT='sig',C_CHARSIZE=1

       ; CONTOUR,signif,time,period,/OVERPLOT,LEVEL=1,THICK=2, $
       ;     C_LABEL=1,C_CHARSIZE=1
       ; cone-of-influence, anything "below" is dubious
         x = [time(0),time,MAX(time)]
         y = [MAX(period),coi,MAX(period)]
         color = 4
         POLYFILL,x,y,ORIEN=+45,SPACING=0.5,COLOR=color,NOCLIP=0,THICK=1
         POLYFILL,x,y,ORIEN=-45,SPACING=0.5,COLOR=color,NOCLIP=0,THICK=1
         PLOTS,time,coi,COLOR=color,NOCLIP=0,THICK=1

       ;--- Plot global wavelet spectrum
         pos3 = [0.74,pos2(1),95,pos2(3)]
         blank = REPLICATE(' ',29)
         PLOT,global_ws,period,/NOERASE,POSITION=pos3, $
          THICK=2,XSTYLE=10,YSTYLE=9, $
          YRANGE=yrange,/YTYPE,YTICKLEN=-0.02, $
          XTICKS=2,XMINOR=2, $
          YTICKS=N_ELEMENTS(ytickv)-1,YTICKV=ytickv,YTICKNAME=blank, $
          ;XTITLE='Power (!Uo!NC!U2!N)',TITLE='c) Global'
          XTITLE='Power',TITLE='c) Global'
         OPLOT,global_signif,period,LINES=1
         XYOUTS,1.7,60,label

;--- Plot 2--8 yr scale-average time series
         pos4 = [pos1(0),0.05,pos1(2),0.25]
         PLOT,time,scale_avg,/NOERASE,POSITION=pos4, $
          XRANGE=xrange,YRANGE=[0,MAX(scale_avg)*1.25],THICK=2, $
          XTITLE='Time',YTITLE='Avg variance', $
          TITLE='d) Scale-averaged time-Series'
         OPLOT,xrange,scaleavg_signif+[0,0],LINES=1
       IF (printfile) THEN DEVICE,/CLOSE
   endif
end
