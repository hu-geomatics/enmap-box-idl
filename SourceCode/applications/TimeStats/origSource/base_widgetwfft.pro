pro base_widgetWFFT_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
     'group6': begin
               case ev2.value of
                0: quest.WFFTType=0
                1: quest.WFFTType=1
               endcase
              end
     'WFFTnowind': WIDGET_CONTROL, _WFFTnwindDiff, GET_VALUE=WFFTnwindDiff
     'WFFTnoover': WIDGET_CONTROL, _WFFTnoverlapDiff, GET_VALUE=WFFTnoverlapDiff
     'WFFTperiod': begin
                     WIDGET_CONTROL, _WFFTPeriod, GET_VALUE=freq
                     quest.WFFTfreq=freq
                   end
     'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widgetWFFT, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
 values4 =['Phase','Spectra']

 ;case quest.WFFTType of
 ; 'PHASE'   :quest.WFFTType=0
 ; 'SPECTRA' :quest.WFFTType=1
 ;endcase

 WFFTbase=  WIDGET_BASE(TITLE='Windowed FFT',/COLUMN)
 WFFTbase1= WIDGET_BASE(WFFTbase, TITLE='Parameter for WFFT',/COLUMN)

 bgroup6= CW_BGROUP(WFFTbase1,values4,/ROW, SET_VALUE=quest.WFFTType,LABEL_TOP='Select target parameter for WFFT',/FRAME,/EXCLUSIVE, UVALUE='group6')

 WFFTbase2= WIDGET_BASE(WFFTbase1, TITLE='Parameter for windowed FFT',/COLUMN)
 _WFFTnwindDiff=   CW_FIELD(WFFTbase1,/INTEGER,/ALL_EVENTS,VALUE=WFFTnwindDiff, $
 TITLE='Length of moving window',UVALUE='WFFTnowind')
 _WFFTnoverlapDiff=CW_FIELD(WFFTbase1,/INTEGER,/ALL_EVENTS,VALUE=WFFTnoverlapDiff, $
 TITLE='Overlapping points',UVALUE='WFFTnoover')
 _WFFTPeriod=CW_FIELD(WFFTbase1,/INTEGER,/ALL_EVENTS,VALUE=quest.WFFTfreq, $
 TITLE='Target period',UVALUE='WFFTperiod')


 endbase = WIDGET_BASE(WFFTbase, /ROW,/ALIGN_BOTTOM)
 ok= widget_button(endbase, VALUE='done', UVALUE='done')

 WIDGET_CONTROL, WFFTbase, /NO_COPY,/REALIZE
 XMANAGER,'base_widgetWFFT', WFFTbase, GROUP=GROUP1
end