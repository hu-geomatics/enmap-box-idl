 function absDiffSegmentsTEMP,x,y,nwind,noverlap,CHECKNO=CHECKNO
 ;   x: time-series
 ;   nwind: length of the window, determines the no. of sections
 ;   noverlap: no. of overlapping points by moving window (default: 50% overlapping, if value < 0)

 ;   diff returns absolute differences in (overlapping) segments of a ts, if
 ;   CHECKNO is set a string array indicating beginning and end of each segment
 ;   X is divided into several
 ;   sections, depending on the window length nwind and the no of overlapping points
 ;   default: 50%.


   x = reform(x)
   M = n_elements(x);

   ; Obtain the necessary information to segment X

   if nwind gt M then begin
      print,'The length of the segments cannot be greater than the length of the input signal. Using default...'
      nwind=M ; Length of window equals lenght of x
   end
   if noverlap ge nwind then begin
      print, 'The number of samples to overlap must be less than the length of the segments. Using default...';
      noverlap=-1
   end

   if noverlap lt 0 then noverlap = fix(0.5 *nwind); use default:50% overlap

   ; Compute the number of segments
   k = fix((M-noverlap)/(nwind-noverlap));
   diff=fltarr(k)
   ; Compute differences in x for each segment

   xindx = 1
   index=indgen(nwind)
   if KEYWORD_SET(CHECKNO) then  begin  ; for file header definition
    diff=strarr(k)
    for i = 0,k-1 do begin
       diff[i]='No. '+strcompress(string(index[nwind-1])+1)+ '-'+ 'No. '+ strcompress(string(index[0]+1))
       index = index + nwind - noverlap
    end
   end else begin

     for i = 0,k-1 do begin
       xnew=x[index[0:nwind-1]]
       ynew=y[index[0:nwind-1]]
       ;fourierPoly, xnew, 12, 0, 1, 0, fp, corrterm, CORR=1
       ;fourierPoly, ynew, 12, 0, 1, 0, fp, corrterm, CORR=1
       ;crossxy,x,y,nwind,'HANN',noverlap, co, qu, coh,p,amp
       result=regress(xnew,ynew,Yfit=yfit,CONST=const,SIGMA=Sigma,FTEST=ftest,CORRELATION=r,MCORRELATION=Rmult)
       ;diff[i]=result/Sigma
       diff[i]=Yfit[nwind-1]-Yfit[0]
       ;diff[i]=p[1]
       index = index + nwind - noverlap
     end
   end
   return,diff
end