function smk, data_vec, freq, missvalue, mv,sig
;seasonal kendall test for monotonic trends
;f�r Datens�tze mit variabler Saison (freq), Nicht vollst�ndige Cyclen am Ende des Datensatzes
;werden verworfen
; berechnen Sig.niveau: TU, 29.06.05
; sig =0: R�ckgabe emp. z-score, 1: R�ckgabe 2 seitiges SigNiveau

s_matrix=datasplit(data_vec,freq)

;calculate sum of sgn and sum of variances for all seasons
;getestet 2.2.03

sum_sgn=0.
sum_var=0.

for i=0,freq-1 do begin
    sum_sgn = sum_sgn + signum_(s_matrix[i,*],missvalue,mv)
    sum_var = sum_var + var(s_matrix[i,*], missvalue, mv)
endfor


    sdev = sqrt(sum_var)
    if sdev eq 0 then sdev=0.00001
    ;continuity correction of overall test value
    case 1 of
       sum_sgn ge 0: z=float(sum_sgn-1)/sdev
       sum_sgn eq 0: z=0
       sum_sgn le 0: z=float(sum_sgn+1)/sdev
    endcase
    if sig eq 1 then begin
      temp=min([1 - gauss_PDF(z), gauss_PDF(z)])*2
      if z lt 0 then z= temp * (-1) else z=temp
    end

    return, z
end
