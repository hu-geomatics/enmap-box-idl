function datasplit,data_vec,freq
;zahl der Zyklen berechnen
ny = fix(n_elements(data_vec)/freq)

;------------------------------------------------------------------------------------------------------------------------------------------
;matrix der freq saisons bilden, werte zeilenweise angeordnet, und mit werten f�llen
s_matrix = fltarr(freq,ny)
for i=0,freq-1 do begin
    for j=0,ny-1 do begin
       s_matrix[i,j] = data_vec[i+freq*j]
    endfor
endfor
return, s_matrix
end