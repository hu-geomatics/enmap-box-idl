pro VegLength, metrics, nwind, y, quest, b, f
    CATCH, Error_status
    IF Error_status NE 0 THEN BEGIN
       PRINT, 'Error index: ', Error_status
       PRINT, 'Error message: ', !ERROR_STATE.MSG
       PRINT, Maxpeak
;             ; Handle the error by extending A:
       ;WIDGET_CONTROL, textwid, SET_VALUE='Error in main!'
       ;wait,3
       CATCH, /CANCEL
       return
    ENDIF

 ; Zeitreihe gl�tten

 ;if quest.vegMetrics lt 2 then fourierPoly, y, nwind, 0, 4, 0, fp, 0,/FIT
 n=n_elements(y)
 ;Maximum=max(y)
 ;Maxpeak=where(y eq Maximum)
 ;Maxpeak=Maxpeak[0]

 ;if Maxpeak eq n-1 then begin   ; wenn Maximum nicht mehr im laufenden Jahr liegt
    Maximum=max(y[0:n-n/4])
    Maxpeak=where(y[0:n-n/4] eq Maximum)
    Maxpeak=Maxpeak[0]
 ;end

 GlobalMin=min(y)   ; f�r Magnitude

 MinPreMax=min(y[0:Maxpeak])  ; Minimum vor Max
 TMinPreMax=where(y eq MinPreMax)
 TMinPreMax=TMinPreMax[0]         ; Indizierung ab 0!

 y1=y[0:Maxpeak]              ; erster  Teil der Zeitreihe
 y2=y[Maxpeak:n-1]; zweiter Teil der Zeitreihe

 MinPostMax=min(y2)        ; zweites Minimum
 TMinPostMax=where(y2 eq MinPostMax)
 TMinPostMax=TMinPostMax[0] + Maxpeak

 metrics.phase=Maxpeak + 1
 metrics.amp=Maximum - GlobalMin

 diff=y1-b[0:Maxpeak]
 temp1th=where(diff gt 0,count)  ; erster Schnittpunkt vor Max
 start=temp1th[0] ; default

 if count gt 0 then $
    if count eq Maxpeak+1 then begin
        if metrics.prevStart gt 0 then start = metrics.prevStart - n   ; kein Schnittpunkt!!
    end else begin
       diff(temp1th)=1
       temp1th=where(diff lt 0,count)
       if count gt 1 then diff(temp1th)=0
       c=diff-shift(diff,1)
       v=where(c eq 1,count2)
       if count2 gt 0 then start = v[n_elements(v)-1]
    endelse


 diff= y2 - f[Maxpeak:n-1] ; zweiter Schnittpunkt nach Max
 temp2th=where(diff le 0,count)
 if count gt 0 then ende=temp2th[0] + Maxpeak else ende = metrics.nextStart + n

 metrics.length=ende-start


; diff=y1-b[Maxpeak:n-1] ; suche Anfang eines zweiten Peaks (= Start Veg.periode im n�chsten Jahr)
; temp1th=where(diff ge 0,count)
; if count gt 0 then metrics.prevStart=temp1th[count-1] else metrics.prevStart = 0 ; Suche nach letztem Schnittpunkt (Start Veg. periode der n�chsten Jahres)


;if metrics.length gt 36 then begin

;       plot,y,title=strjoin(['L�nge: ',string(metrics.length),'  Phase: ', string(metrics.Phase),'  Amplitude: ',string(metrics.amp)])
;       oplot,b
;       oplot,f
;end

end