function diffSeasonMean,pixelzeile,freq,c1,c2,c3,c4,OUT=OUT,STD=STD
; Abweichungen von saisonalen Mittelwerten
; beachte: Korrektur erfolgt nur bis Ende der letzten vollen Saison (Jahres) in der Zeitreihe!!
; pixelzeile=float(pixelzeile)

   samples=n_elements(pixelzeile[0,*])
   cols=n_elements(pixelzeile[*,0])

   pixelzeileNeu=pixelzeile[*,0:freq-1]*0. ; Array mit Anfangswerten f�r jeden Monat
   pixelzeileStd=pixelzeileNeu
   pixelzeilePosAbw=fix(pixelzeileNeu[*,0])
   pixelzeileNegAbw=fix(pixelzeileNeu[*,0])
   ; Monatsmittelwerte
   for n=0, floor(samples/freq)-1 do pixelzeileNeu=pixelzeileNeu+pixelzeile[*,n*freq:n*freq+freq-1]
   pixelzeileNeu=pixelzeileNeu/floor(samples/freq)
   ; Monatsstandardabweichungen
   for n=0, floor(samples/freq)-1 do pixelzeileStd=pixelzeileStd+(pixelzeile[*,n*freq:n*freq+freq-1]-pixelzeileNeu)^2
   pixelzeileStd=sqrt(pixelzeileStd/(floor(samples/freq)-1))
   pixelzeileNegAbw=pixelzeileNegAbw*0
   pixelzeilePosAbw=pixelzeilePosAbw*0
   grenzePos=pixelzeileNeu + 1*pixelzeileStd
   grenzeNeg=pixelzeileNeu - 1*pixelzeileStd

   for n=0, floor(samples/freq)-1 do begin  ; Differenzen
     ;pos. Abweichungen
     for anf=0,n_elements(pixelzeile[*,0])-1 do begin
        temp=where(pixelzeile[anf,n*freq:n*freq+freq-1] gt grenzePos[anf,*],count)
        pixelzeilePosAbw[anf]=pixelzeilePosAbw[anf]+count
        temp=where(pixelzeile[anf,n*freq:n*freq+freq-1] lt grenzeNeg[anf,*],count)
        pixelzeileNegAbw[anf]=pixelzeileNegAbw[anf]+count
     end
     pixelzeile[*,n*freq:n*freq+freq-1]=(pixelzeile[*,n*freq:n*freq+freq-1]-pixelzeileNeu)
     ; ggf standardisieren
     if keyword_set(STD) then pixelzeile[*,n*freq:n*freq+freq-1]=pixelzeile[*,n*freq:n*freq+freq-1]/pixelzeileStd
   endfor
   if keyword_set(OUT) then begin; schreibe Statistik weg
      writeu,c1,pixelzeileNeu
      writeu,c2,pixelzeileStd
      writeu,c3,pixelzeilePosAbw
      writeu,c4,pixelzeileNegAbw
   end
   return,pixelzeile
end
