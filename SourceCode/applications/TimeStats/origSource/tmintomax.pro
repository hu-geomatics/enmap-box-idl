function Tmintomax,x,y
; funktion funktiniert nur f�r  Sinusform, nicht f�r Cosinusform!!!!!
    tempMax=max(y)
    v=where(y eq tempMax)
    Tmax =x[v[0]]
    tmaxYIndex=where(y eq tempMax)
    tmax=tmax[0]
     ;rechte Seite abschneiden,notwendig, falls rechts noch tieferes Min. liegt!

    tempMin=min(y[0:tmaxYIndex[0]])
    ; Grenzwert auf Minimum addieren
    x1=where(y eq tempMin)
    x1=x1[0]
    y[0:x1]=-999    ; Werte �berspringen
    tempMin=tempMin+(tempMin/100*10)  ; 10% Grenzwert
    ;tmin=x[where(y[0:tmaxYIndex[0]] eq tempMin)]
    i=where(y[0:tmaxYIndex[0]] gt tempMin,count)
    if count gt 0 then begin
      tmin=x[i]
      tmin=tmin[0]
      TminToTmax=Tmax-Tmin
    end else TminToTmax=0

return,TminToTmax
end