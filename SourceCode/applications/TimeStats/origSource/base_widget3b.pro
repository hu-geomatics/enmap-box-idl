

pro base_widget3b_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'groupFP1f': begin
                  WIDGET_CONTROL, _FPharmonics1, GET_VALUE=NoHarmonics
                  quest.NoHarmonics1=NoHarmonics
                 end

    'groupFP1': begin
                  WIDGET_CONTROL, _FPfreq1, GET_VALUE=freq
                  quest.freq1=freq
                end
  ;  'group2': begin
  ;             case ev2.value of
  ;              0: IFFT_reg= 1
  ;              1: IFFT_reg= 0
  ;             endcase
  ;            end

    'groupFP2f': begin
                  WIDGET_CONTROL, _FPharmonics2, GET_VALUE=NoHarmonics2
                  quest.NoHarmonics2=NoHarmonics2
                 end

    'groupFP2': begin
                  WIDGET_CONTROL, _FPfreq2, GET_VALUE=freq
                  quest.freq2=freq
                end
  ;  'group2': begin
  ;             case ev2.value of
  ;              0: IFFT_reg= 1
  ;              1: IFFT_reg= 0
  ;             endcase
  ;            end

    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
  endcase
end


pro base_widget3b, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase

 values1=  ['yes','no']
 fourier3=  WIDGET_BASE(TITLE='Parameters for fourier polynomial',/COLUMN)
 _FPfreq1=   CW_FIELD(fourier3,/INTEGER,/RETURN_EVENTS,VALUE=quest.freq1,TITLE='Length of Fourier period 1(Confirm with RETURN!)',UVALUE='groupFP1')
 _FPharmonics1= CW_FIELD(fourier3,/INTEGER,/RETURN_EVENTS,VALUE=quest.NoHarmonics1,TITLE='No. of harmonics (Confirm with RETURN!)',UVALUE='groupFP1f')

 _FPfreq2=   CW_FIELD(fourier3,/INTEGER,/RETURN_EVENTS,VALUE=quest.freq2,TITLE='Length of  Fourier period 2(Confirm with RETURN!)',UVALUE='groupFP2')
 _FPharmonics2= CW_FIELD(fourier3,/INTEGER,/RETURN_EVENTS,VALUE=quest.NoHarmonics2,TITLE='No. of harmonics (Confirm with RETURN!)',UVALUE='groupFP2f')


 ;bgroup8=   CW_BGROUP(fourier3,values1,/ROW, SET_VALUE=abs(1-IFFT_reg),LABEL_TOP='Use results for trend analysis ?',/FRAME,/EXCLUSIVE, UVALUE='group2')
 endbase = WIDGET_BASE(fourier3, /ROW,/ALIGN_BOTTOM)
 done= widget_button(endbase, VALUE='done', UVALUE='done')

  WIDGET_CONTROL, fourier3, /NO_COPY,/REALIZE
  XMANAGER,'base_widget3b', fourier3, GROUP=GROUP1
end

