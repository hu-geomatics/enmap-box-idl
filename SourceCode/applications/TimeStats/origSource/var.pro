function var, data_vec, missvalue, mv
; Mann-Kendall-Test (Autor: Tim Erbrecht)
;calculate tie corrected standard deviation for trend test
;getestet 31.1.03
    if missvalue eq 1 then mvIndex=where(data_vec eq mv, mvCount) else mvCount=-1
    if mvCount gt 0 then data_vec[mvIndex]=0
    test_vec = data_vec
    cor=0
    corr_tot = 0

    for i=0,n_elements(test_vec)-1 do begin
       if (test_vec[i] NE 0) then  begin
          index =  where(test_vec EQ test_vec[i], count)
          if (count gt 1) then begin
              cor = count*(count-1)*(2*count+5)
              test_vec[index] = 0
              corr_tot = corr_tot + cor
          endif else begin
              ;corr=0
          endelse
       endif
       ;corr_tot = corr_tot + corr
    endfor

    n = n_elements(data_vec)
    ; missing value adjustment
    if mvCount gt -1 then n=n-mvCount
    pvar = n*(n-1)*(2*n+5)
    cvar = pvar - corr_tot
    var= 1/18.*cvar
    ;sdev=sqrt(var)
    return,var
end
