pro trend,textwid
common blk1


;ON_IOERROR,ioerror

;
 ;   CATCH, Error_status
 ;   IF Error_status NE 0 THEN BEGIN
 ;      PRINT, 'Error index: ', Error_status
 ;      PRINT, 'Error message: ', !ERROR_STATE.MSG
;             ; Handle the error by extending A:
 ;      WIDGET_CONTROL, textwid, SET_VALUE='Error in main!'
 ;      wait,3
 ;      CATCH, /CANCEL
 ;      return
 ;   ENDIF

  header_img = {   hdword:    bytarr(6),     $     ; Definition Header LAN-file
                   ipack:     0,             $
                   nbands:    0,             $
                   unused1:   bytarr(6),     $
                   icols:     0L,            $
                   irows:     0L,            $
                   xstart:    0L,            $
                   ystart:    0L,            $
                   unused2:   bytarr(56),    $
                   maptyp:    0,             $
                   nclass:    0,             $
                   unused3:   bytarr(14),    $
                   iautyp:    0,             $
                   acre:      0.,            $
                   xmap:      0.,            $
                   ymap:      0.,            $
                   xcell:     0.,            $
                   ycell:     0.                  }
  ; kontrollieren der CWT parameter
  if (MorletP lt 6) or (MorletP gt 20) then MorletP=6
  if (PaulP lt 4) or (PaulP gt 20) then PaulP=6
  if (DOGP lt 2) or (DOGP gt 40) then DOGP=2


  band=band-1; Target Band
  FromTo=-1 ; f�r Differenzen in moving windows
  WFFTFromTo=-1; f�r Differenzen in WFFT moving windows
  xypixelzeile=0  ; vordefinieren xyzeile
  NoLags=bytarr(6) ; umkopieren des Lagstructures in ein Array f�r einfachere Auswertung
  NoLags[0]=lags.lagsY
  NoLags[1]=lags.lagsX1 & NoLags[2]=lags.lagsX2 & NoLags[3]=lags.lagsX3
  NoLags[4]=lags.lagsX4 & NoLags[5]=lags.lagsX5

  if quest.freq1 gt 0 then quest.NoHarmonics1=quest.NoHarmonics1+1 ; neben Oberschwingungen auch Hauptfrequenz!
  if quest.freq2 gt 0 then quest.NoHarmonics2=quest.NoHarmonics2+1 ; neben Oberschwingungen auch Hauptfrequenz!

  bname=''
  WFFTbname=''
  fp=0; Fourierpolynom may included in trend analysis (size(fp) is > 1 in that case)
;  ok=0  ; flag f�r Dateianzahl > 100
  zeile=''
  first=1
  interleave='BIL'
  if (quest.freq1 eq 0) and (quest.freq2 eq 0) then begin  ; kein FP gew�hlt, daher flags f�r OLSR ausschalten!
    flagUR[10]=0
    flagR[10]=0
  end

; pr�fen ob DOS oder UNIX-System!
  if n_elements(STR_SEP(!dir,"\")) eq 1 then dos = 0 else dos = 1
  for i=1,128 do close,i  ; vorsichtshalber schliessen aller verf�gbarer Dateikan�le
  for i=1,128 do free_lun,i


  if (flagxy[0] eq 1) and (xyfile ne '')  then IFFT=4
  X1Pixel=0
  X2Pixel=0
  X3Pixel=0
  X4Pixel=0
  X5Pixel=0
  ;if mv eq '' then missvalue = 0 else mv=float(mv)
  ; missvalue=0: keine MV, 1: MV aber keine Interpol, 2: MV durch Mittelwert ersetzen, 3: MV lin. interpolieren

; Dateinamen und Zeitvariable einlesen und ggf. ENVI-Header verarbeiten
  samples=0


 if quest.tif eq 1 then convertTiffToBSQ,file,batch,DOS,textwid


  openr,unit,file,/get_lun     ; Verarbeitungsfile
  if batch eq 0 then begin
     ; Verarbeitungsfile ist eine Stapeldatei
     while not eof(unit) do begin   ; wieviele samples?
        readf,unit,zeile
        samples=samples+1
     endwhile
     free_lun,unit
     openr,unit,file,/get_lun  ;wieder �ffnen
     zahl=fltarr(samples)
     datname=strarr(samples)

     for i=0,samples-1 do begin  ; Dateinamen und Zeitvariable werden eingelesen,
        readf,unit,zeile    ; Dateien �ffnen und die Header (nur bei LAN-Files) lesen
        result=strsplit(zeile,/REGEX,/EXTRACT)
        datname[i]=result[0]
        if n_elements(result) gt 1 then zahl[i]=result[1] else $; Zeitangabe vorhanden!
              zahl[i]=i   ; keine Zeitangabe vorhanden!

        if i eq 0 then $
           if lan eq 4 then begin  ; bei ENVI-Files Header auswerten
               parts=STR_SEP(datname[0],'.')
               name=''
               if n_elements(parts) gt 1 then begin
                   for j=0,n_elements(parts)-2 do name=STRCOMPRESS(name+'.'+parts[j])
                   name=STRMID(name,1)
               end else name=parts[0]
               readEnviHeader,STRCOMPRESS(name+'.hdr'),BOrder,cols,rows,imagebands,interleave,lan,mapInfo, wavelengthUnits
               if (imagebands gt 1) and (interleave ne 'bil') and (interleave ne 'Bil') then $
                 begin
                  WIDGET_CONTROL, textwid, SET_VALUE='No BIL-format, exit...'
                  message,'No BIL-format, exit...'
                  stop
                 end
           endif
     endfor
     zahl=reform(zahl)
     free_lun,unit

  endif else begin
      ; Verarbeitungsfile ist ein ENVI-Bil-File
      parts=STR_SEP(file,'.')   ; Stack Originalwerte
      readEnviHeader,STRCOMPRESS(parts[0]+'.hdr'),BOrder,cols,rows,imagebands,interleave,lan,mapInfo, wavelengthUnits; ENVI Header auswerten
      if (imagebands gt 1) and ( (STRUPCASE(interleave)  ne 'BIL') and (STRUPCASE(interleave)  ne 'BSQ')) then $
      begin
          WIDGET_CONTROL, textwid, SET_VALUE='No BIL-format, exit...'
          message,'Only ENVI-Files with BIL- or BSQ-datatypes are supported, exit...'
          stop
      end

      samples=imagebands
      zahl=findgen(samples)  ; Zeitvariable erzeugen
  endelse

    ; falls keine LAN-Files, Zeilen, Spalten und Kan�le definieren
  if lan ne 1 then begin
                     header_img.icols=cols
                     header_img.irows=rows
                     header_img.nbands=imagebands
                   end


  if (quest.normalize[3] eq 1) or (quest.normalize[4] eq 1) then $
   begin  ; ben�tigte Dateien f�r saisonale Statistiken
     openw,81,workDir+'m-mean.bil'
     openw,82,workDir+'m-mean.hdr'
     envi_header,82,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,quest.freq,1,dos,0
     close,82
     openw,83,workDir+'m-stddev.bil'
     openw,82,workDir+'m-stddev.hdr'
     envi_header,82,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,quest.freq,1,dos,0
     close,82
     openw,84,workDir+'CountsPosDiffMonthlyAvg.bil'
     openw,82,workDir+'CountsposDiffMonthlyAvg.hdr'
     envi_header,82,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,1,1,dos,0
     close,82
     openw,85,workDir+'CountsnegDiffMonthlyAvg.bil'
     openw,82,workDir+'CountsnegDiffMonthlyAvg.hdr'
     envi_header,82,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,quest.freq,1,dos,0
     close,82
   end

 if (quest.normalize[7] eq 1) then $   ; prewhitening
   begin
     openw,86,workDir+'AR-order.bil'
     openw,82,workDir+'AR-order.hdr'
     envi_header,82,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,1,1,dos,1
     close,82
     openw,87,workDir+'AR-koeffs.bil'
     openw,82,workDir+'AR-koeffs.hdr'
     envi_header,82,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,10,1,dos,0,BIP=1
     close,82
   end

  ; �ffnen der Dateien f�r restricted/unrestricted regression

  if flagUR[5] eq 1 then begin   ; optinal first external regressor file
     Result = STRSPLIT(extFiles.fileX1,'.',/extract)
     readEnviHeader,STRCOMPRESS(Result[0] + '.hdr'),xyBOrder,xycols,xyrows,xybands,xyinterleave,xylan,mapInfo, wavelengthUnits; ENVI Header auswerten
     checkEnviHeader, cols, rows, header_img.nbands, xycols, xyrows, xybands, xyinterleave,lan,xylan
     openr,unit11,extFiles.fileX1,/get_lun
  end
  if flagUR[6] eq 1 then begin   ; sec. external regressor file
     Result = STRSPLIT(extFiles.fileX2,'.',/extract)
     readEnviHeader,STRCOMPRESS(Result[0] + '.hdr'),xyBOrder,xycols,xyrows,xybands,xyinterleave,xylan,mapInfo, wavelengthUnits; ENVI Header auswerten
     checkEnviHeader, cols, rows, header_img.nbands, xycols, xyrows, xybands, xyinterleave, lan, xylan
     openr,unit13,extFiles.fileX2,/get_lun
  end
  if flagUR[7] eq 1 then begin   ; third external regressor file
     Result = STRSPLIT(extFiles.fileX3,'.',/extract)
     readEnviHeader,STRCOMPRESS(Result[0] + '.hdr'),xyBOrder,xycols,xyrows,xybands,xyinterleave,xylan,mapInfo, wavelengthUnits; ENVI Header auswerten
     checkEnviHeader, cols, rows, header_img.nbands, xycols, xyrows, xybands, xyinterleave, lan, xylan
     openr,unit15,extFiles.fileX3,/get_lun
  end
  if flagUR[8] eq 1 then begin   ; forth external regressor file
     Result = STRSPLIT(extFiles.fileX4,'.',/extract)
     readEnviHeader,STRCOMPRESS(Result[0] + '.hdr'),xyBOrder,xycols,xyrows,xybands,xyinterleave,xylan,mapInfo, wavelengthUnits; ENVI Header auswerten
     checkEnviHeader, cols, rows, header_img.nbands, xycols, xyrows, xybands, xyinterleave, lan, xylan
     openr,unit17,extFiles.fileX4,/get_lun
  end
  if flagUR[9] eq 1 then begin   ; fifth external regressor file
     Result = STRSPLIT(extFiles.fileX5,'.',/extract)
     readEnviHeader,STRCOMPRESS(Result[0] + '.hdr'),xyBOrder,xycols,xyrows,xybands,xyinterleave,xylan,mapInfo, wavelengthUnits; ENVI Header auswerten
     checkEnviHeader, cols, rows, header_img.nbands, xycols, xyrows, xybands, xyinterleave, lan, xylan
     openr,unit19,extFiles.fileX5,/get_lun
  end


  header_img.nbands=samples
  ; auswerten "von" "bis"
  if recFrom gt recTo then begin
   recFrom=1
   recTo=header_img.nbands
  end
  if (recFrom le 0) or (recFrom gt header_img.nbands) then recFrom=1
  if (recTo   le 0) or (recTo gt header_img.nbands)   then recTo=header_img.nbands
  samples=recTo-recfrom+1
  zahl=zahl[recFrom-1:recTo-1]

 ; �berpr�fen und initialisieren des zweiten Inputfiles f�r evtl. Kreuzspektralanalyse

  if IFFT eq 4 then begin   ; zweiter File f�r Kreuzspektralanalyse
      parts=STR_SEP(xyfile,'.')   ; Stack Originalwerte
      readEnviHeader,STRCOMPRESS(parts[0]+'.hdr'),xyBOrder,xycols,xyrows,xyimagebands,xyinterleave,xylan,mapInfo, wavelengthUnits; ENVI Header auswerten
      checkEnviHeader, cols, rows, header_img.nbands, xycols, xyrows, xyimagebands, xyinterleave,lan, xylan

      openr,unit9,xyfile,/get_lun

      ;Definieren der Ausgabedateien f�r Kreuzspektralanalyse
      if flagxy[0] eq 1 then begin   ; FFT
        xyfileFFT_head=workDir+'xyFFT.hdr'
        xyfileFFT=workDir+'xyFFT.bil'
        openw,unit10,xyfileFFT,/get_lun
        openw,unit11,xyfileFFT_head,/get_lun
        envi_header,unit11,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit11
      end

      if flagxy[1] eq 1 then begin   ; Cross spectrum
        xyfileCs_head=workDir+'xyCr.hdr'
        xyfileCs=workDir+'xyCr.bil'
        openw,unit12,xyfileCs,/get_lun
        openw,unit13,xyfileCs_head,/get_lun
        envi_header,unit13,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit13
      end

      if flagxy[2] eq 1 then begin   ; Co spectrum
        xyfileCo_head=workDir+'xyCo.hdr'
        xyfileCo=workDir+'xyCo.bil'
        openw,unit14,xyfileCo,/get_lun
        openw,unit15,xyfileCo_head,/get_lun
        envi_header,unit15,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit15
      end

      if flagxy[3] eq 1 then begin   ; Quadratur spectrum
        xyfileQu_head=workDir+'xyQu.hdr'
        xyfileQu=workDir+'xyQu.bil'
        openw,unit16,xyfileQu,/get_lun
        openw,unit17,xyfileQu_head,/get_lun
        envi_header,unit17,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit17
      end

      if flagxy[4] eq 1 then begin   ; Phase spectrum
        xyfilePs_head=workDir+'xyPs.hdr'
        xyfilePs=workDir+'xyPs.bil'
        openw,unit18,xyfilePs,/get_lun
        openw,unit19,xyfilePs_head,/get_lun
        envi_header,unit19,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit19
      end

      if flagxy[5] eq 1 then begin   ; Amplitude spectrum
        xyfileAs_head=workDir+'xyAs.hdr'
        xyfileAs=workDir+'xyAs.bil'
        openw,unit20,xyfileAs,/get_lun
        openw,unit21,xyfileAs_head,/get_lun
        envi_header,unit21,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit21
      end

      if flagxy[6] eq 1 then begin   ; Coherence spectrum
        xyfileCoh_head=workDir+'xyCoh.hdr'
        xyfileCoh=workDir+'xyCoh.bil'
        openw,unit22,xyfileCoh,/get_lun
        openw,unit23,xyfileCoh_head,/get_lun
        envi_header,unit23,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0
        free_lun,unit23
      end
  end


  if nwindDiff gt 0 then begin  ; f�r Differenzen in moving windows bei Trendanalyse
    temp=findgen(header_img.nbands)
    FromTo=absDiffSegments(temp,nwindDiff,noverlapDiff,CHECKNO=1)
  end
  if (WFFTnwindDiff gt 0) and ((IFFT eq 6) or (IFFT eq 7)) then begin  ; f�r Differenzen in WFFT
    temp=findgen(header_img.nbands)
    WFFTFromTo=absDiffSegmentsPM(temp,WFFTnwindDiff,WFFTnoverlapDiff,CHECKNO=1)
    WFFTbandnames=defineBandnames3(WFFTFromTo,quest.WFFTType)
    WFFTsizeBandname=n_elements(WFFTbandnames)

    for i=0, n_elements(WFFTbandnames)-1 do if i lt n_elements(WFFTbandnames)-1 then WFFTbname=WFFTbname+WFFTbandnames[i]+'  CH'+STRCOMPRESS(string(i+1))+',' else $
      WFFTbname=WFFTbname+WFFTbandnames[i]+'  CH'+STRCOMPRESS(string(i+1))

  end

;**************** keine Trendsegmente bei aggregierten Daten zulassen
  if (quest.normalize[8] eq 1) or  (quest.normalize[9] eq 1) then $
     begin
       FromTo = -1
       WFFTFromTo = -1
     end
;**************** keine Trendsegmente bei aggregierten Daten zulassen
  if (quest.normalize[8] eq 1) or  (quest.normalize[9] eq 1) then samplesTEMP=floor(samples/normFreq) else samplesTEMP=samples  ; f�r aggregierte Daten neue Bandnummer


  if max([flag, flagUR]) eq 1 then begin
     bandnames=defineBandnames()        ; f�r einfachs Trendmodul
     sizeBandname1=n_elements(bandnames) ; size of the bandname array till now
     bandnames=defineBandnames2(bandnames,FromTo); f�r resticted/unrestriced bands
     sizeBandname2=n_elements(bandnames) ; final size of the bandname array

     par=where(flag eq 1,count)  ;alles au�er ADF

     if count gt 0 then begin
                          erg=bandnames[par] ; normal trend parameters
                          if (max(par) eq 30) and (max(flag[1:29] eq 1)) then erg=[erg, bandnames[31:31+6+lagsadf*3]] else $; trend+ADF
                          if (max(par) eq 30) and (max(flag[1:29] eq 0)) then erg=bandnames[30:30+7+lagsadf*3] ;nur ADF
                          if max(flagUR) eq 1 then erg=[erg,bandnames[sizeBandname1:*]]
                        end  else erg=bandnames[sizeBandname1:*]
     for i=0, n_elements(erg)-1 do if i lt n_elements(erg)-1 then bname=bname+erg[i]+'  CH'+STRCOMPRESS(string(i+1))+ ', ' $
       else bname=bname+erg[i]+'  CH'+STRCOMPRESS(string(i+1))


     parts=STR_SEP(ofile,'.')
     if n_elements(parts) eq 1 then ofile=STRCOMPRESS(parts[0])+'.bil'
     ofile_head=STRCOMPRESS(parts[0])+'.hdr'
     openw,unit1,ofile,/get_lun       ; Ausgabefile f�r Regressionsparameter
     openw,unit2,ofile_head,/get_lun  ; ENVI Header-File
     envi_header,unit2,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,n_elements(erg),0,dos,0,bname
     _b=fltarr(header_img.icols)    ; Vektoren, die zeilenweise Regressionsparameter aufnehmen
     _SMKtest=_b & _Const=_b & _Sigma=_b & _Ttest=_b & _R =_b & _MKslope=_b
     _RMS=_b & _MKtest=_b & _Status=_b & _Max=_b & _TimeMax=_b & _Stdabw=_b
     _mean=_b & _SMKslope=_b & _MSMKtest=_b & _varkoeff=_b & _relerror=_b
     _skew=_b & _kurt=_b & _abbe=_b & _rho=_b & _tau=_b & _sigrho=_b & _sigtau=_b
     _Min=_b & _Maxi=_b & _MinToMax=_b & _MaxToMin=_b & _Range=_b
     _ADFkoeff=fltarr(header_img.icols,8+lagsADF*3) ; extra Array f�r Ausgaben des ADF-Tests
      ; Structure f�r unrestricted/restricted regression
      if sizeBandname2 gt sizeBandname1 then begin
       maxlags=max([lags.lagsY,lags.lagsX1,lags.lagsX2,lags.lagsX3,lags.lagsX4,lags.lagsX5])
       if maxlags eq 0 then maxlags=1
       NoHarm=max([((quest.NoHarmonics1+quest.NoHarmonics2)*2),1])



       olsUR ={const:     -999.,                         $     ; offset
               b:         REPLICATE(-999.,7,maxlags+1),  $     ; Max 10 externe Variablen (incl. time)
               t:         REPLICATE(-999.,7,maxlags+1),  $     ; t-Statistik f�r Reg. koeff
               tsig:      REPLICATE(-999.,7,maxlags+1),  $     ; Sig. von t
               r:         REPLICATE(-999.,7,maxlags+1),  $     ; r
               t2to5:     REPLICATE(-999.,4),            $     ; h�here Polynomterme
               t2to5T:    REPLICATE(-999.,4),            $
               t2to5Tsig: REPLICATE(-999.,4),            $
               t2to5r:    REPLICATE(-999.,4),            $
               F:         -999.,                         $     ; F-Statistik f�r Gesamtmodell
               Fsig:      -999.,                         $     ; Sig F
               DW:        -999.,                         $     ; Durbin-Watson Testgr��e
               NoVar:     -999.,                         $
               nobs:      -999.,                         $
               sige:      -999.,                         $     ; e'*e/(noObs-noVar) f�r Wald-Test
               WaldF:     -999.,                         $     ; Wald Statistik f�r 2 Regressionsmodelle (UR /R)
               Waldsig:   -999.,                         $
               Yfit1:     -999.,                         $     ; erster und letzer gesch�tzter Wert
               Yfitend:   -999.,                         $
               relGrowth: -999.,                         $     ; prozentualer j�hrlicher Zuwachs durch geometrisches Mittel
               absGrowth: -999.,                         $     ; absoluter j�hrlicher Zuwachs durch geometrisches Mittel
               Rmult:     -999.,                         $     ; multipler Korrelationskoeffizient
               FP:        REPLICATE(-999.,NoHarm),        $     ; Cosinus und Sinusterme der FPs
               fpT:       REPLICATE(-999.,NoHarm),        $     ; t-emp
               fpTsig:    REPLICATE(-999.,NoHarm),        $     ; t-sig
               ChangeFrTo:REPLICATE(-999.,n_elements(FromTo)), $ ;moving windows f�r Trenddifferenzen
               resid:     REPLICATE(-999.,samplesTEMP)}  ; nimmt Residuen auf
               ; maxlags+1,damit auch bei maxlag = 1 ein 2dim Array angelegt wird
       _olsUR=REPLICATE(olsUR, header_img.icols)
       _olsR=_olsUR
      end
  endif

  if quest.resid ge 0 then begin
    parts=STR_SEP(ofile_ori,'.')   ; Stack Originalwerte/Residuen/Yfit
    if file ne STRCOMPRESS(parts[0])+'.bil' then begin
        ofile_head_ori=STRCOMPRESS(parts[0])+'.hdr'
        ofile_ori=STRCOMPRESS(parts[0])+'.bil'
    endif else begin
        ofile_head_ori=STRCOMPRESS(parts[0])+'1'+'.hdr'
        ofile_ori=STRCOMPRESS(parts[0])+'1'+'.bil'
    endelse
    openw,unit4,ofile_head_ori,/get_lun
  end

  if thresh gt 0 then begin
    parts=STR_SEP(hotspots,'.')
    hotspots_head=STRCOMPRESS(parts[0])+'.hdr' ; Stack Hot-Spots
    hotspots=STRCOMPRESS(parts[0])+'.bil'
    openw,hshead,hotspots_head,/get_lun
  end

  if (IFFT eq 0) or (IFFT eq 1) or (IFFT eq 3) or (IFFT eq 5) or (IFFT eq 6) or (IFFT eq 7) then begin ; falls Fourier-Koeffizienten abgespeichert werden sollen
                      fourierfile_hdr=STRCOMPRESS(fourierfile)+'.hdr'
                      openw,unit5,fourierfile,/get_lun
                      openw,unit6,fourierfile_hdr,/get_lun
                      if (IFFT eq 6) or (IFFT eq 7) then $
                           envi_header,unit6,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,n_elements(WFFTbandnames),0,dos,0,WFFTbname else $
                           envi_header,unit6,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,0

  end


   if first eq 1 then begin
     if quest.resid ge 0 then openw,unit3,ofile_ori,/get_lun
     if thresh gt 0 then openw,hsstack,hotspots,/get_lun
   endif

   case lan of
    1:begin
        col=bytarr(header_img.icols,header_img.nbands)
         pixelzeile=col
         mv=byte(mv)
        offset=1  ; f�r point_lun
      end
    0:begin
        col=fltarr(header_img.icols,header_img.nbands)
        pixelzeile=col
        mv=float(mv)
        offset=4  ; f�r point_lun
      end
    2:begin
        col=intarr(header_img.icols,header_img.nbands)
        pixelzeile=col
        mv=fix(mv)
        offset=2  ; f�r point_lun
      end
    3:begin
        col=bytarr(header_img.icols,header_img.nbands)
        pixelzeile=col
        mv=byte(mv)
        offset=1  ; f�r point_lun
      end
   endcase

   xypixelzeile=pixelzeile  ; f�r Kreuzspektralanalyse

   if (IFFT eq 6) or (IFFT eq 7) then buffer=fltarr(n_elements(pixelzeile[*,0]),n_elements(WFFTbandnames)) else $
                     buffer=fltarr(n_elements(pixelzeile[*,0]),samples) ; Tempor�re Variable f�r FFT

   if max(flagxy) gt 0 then begin
     xybufferFFT=float(pixelzeile[*,RecFrom-1:recTo-1])*0
     xybufferXy2=float(pixelzeile[*,RecFrom-1:recTo-1])*0
     xybufferCo= float(pixelzeile[*,RecFrom-1:recTo-1])*0
     xybufferQu= float(pixelzeile[*,RecFrom-1:recTo-1])*0
     xybufferP=  float(pixelzeile[*,RecFrom-1:recTo-1])*0
     xybufferAmp=float(pixelzeile[*,RecFrom-1:recTo-1])*0
     xybufferCoh=float(pixelzeile[*,RecFrom-1:recTo-1])*0
   end




   if first eq 1 then begin
      case quest.outFormat of
        0: lan1=1
        1: lan1=2
        2: lan1=0
      endcase
      if quest.resid ge 0 then $
          if (quest.normalize[8] eq 1) or  (quest.normalize[9] eq 1) then $
          envi_header,unit4,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,floor(samples/normFreq),1,dos,lan1 else $ ; nur beim ersten Mal!
          envi_header,unit4,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samples,1,dos,lan1
          if thresh gt 0 then envi_header,hshead,mapInfo,wavelengthUnits,header_img.icols,header_img.irows,samplesTEMP,1,dos,lan,CLASS=1 ; kl�ren wie bei agg. Werten mit HotSpots zu verfahren ist!!!!!!!!!
   endif

   buf=fltarr(n_elements(pixelzeile[*,0]))
   pzbuff=pixelzeile ; notwendig bei batchfiles, wo sich Dimension von pixelzeile bei integrals =1 �ndert!

; Start der Prozessierungsschleife

corrtermVec=pixelzeile * 0. ; f�r FP


if IFFT eq 2 then begin  ;Wavelet Analyse: nicht mehr weiteren Schritte durchlaufen!!!!!!
   close, unit
   zahl=findgen(header_img.nbands)
   RoiWavelet,file, header_img.icols, header_img.irows, header_img.nbands, zahl   ; interaktive Wavelet-Analyse
end else $
begin ; alles andere

;header_img.irows=1139
pixelzeileOri=pixelzeile ; f�r batch = 0 erforderlich

for j=0,header_img.irows-1 do begin
WIDGET_CONTROL, textwid, SET_VALUE='row: '+ string(j+1)
pixelzeile=pixelzeileOri
    if batch eq 0 then begin
      for i=0,header_img.nbands-1 do begin
             openr,datei,datname[i],/get_lun
             if lan eq 1 then begin     ; Header nur einlesen, falls LAN-Files
               readu,datei,header_img;
               if dos eq 0 then header_img=swap_endian(header_img)
             end

             POINT_LUN, datei,ulong(j* header_img.icols*offset); Dateipointer setzen

             buf_=float(read_lan_file(datei,col[*,0],band,dos,BOrder,lan))
             if (missvalue gt 1) or (outlier gt 0) then $
                               buf_=mvoutlier(buf_,header_img.icols,zahl,mv,missvalue,outlier,olcorrmeth, olstddev, olthresh,mvSais)
             buf=buf+float(buf_)
             free_lun,datei
             pixelzeile[*,i]=buf
             buf=buf*0
      endfor
      temp=pixelzeile[*,recFrom-1:recTo-1]
      pixelzeile=temp
    end else begin     ; hier Code f�r stackfiles


       if (STRUPCASE(interleave)  eq 'BIL') then $
           pixelzeile = float(read_lan_file(unit, pzbuff, band, dos, BOrder, lan, ALL=1)) $
       else $   ; BSQ
          for m=0, header_img.nbands-1  do begin
            POINT_LUN, unit,ulong((j* header_img.icols*offset)+ m * header_img.irows * header_img.icols*offset); Dateipointer setzen
            pixelzeile[*,m] = float(read_lan_file(unit, pzbuff[*,0], 0, dos, BOrder, lan, ALL=1))
          endfor
          
; phasen verschieben
;pixelzeile=pixelzeile+2
;temp=where(pixelzeile gt 12,count)
;if count gt 0 then pixelzeile(temp)=pixelzeile(temp)-12
; phasen verschieben ENDE

;pixelzeile=pixelzeile*10000.
;pixelzeile=pixelzeile/10000.
;pixelzeile=(pixelzeile-128.)*0.008
         temp=pixelzeile[*,recFrom-1:recTo-1]
         pixelzeile=temp
          if (missvalue gt 1) or (outlier gt 0) then $
           pixelzeile=mvoutlier(pixelzeile,header_img.icols,zahl,mv,missvalue,outlier,olcorrmeth, olstddev, olthresh, mvSais)

         if IFFT eq 4 then $
         begin
             xypixelzeile=float(read_lan_file(unit9, pzbuff, band, dos, BOrder, lan, ALL=1))
             temp=xypixelzeile[*,recFrom-1:recTo-1]
             xypixelzeile=temp
             if (missvalue gt 1) or (outlier gt 0) then $
             xypixelzeile=mvoutlier(xypixelzeile,header_img.icols,zahl,mv,missvalue,outlier,olcorrmeth, olstddev, olthresh, mvSais)
             xypixelzeile=callpreprocessing(xypixelzeile,normFreq,quest,fp,corrterm,nrPSmooth,mv)
         end

         ; einlesen der Zeilen f�r externe Reg. variablen

        if flagUR[5] eq 1 then begin
                                 X1=float(readXFile(pixelzeile,unit11,pzbuff, band, dos, BOrder, lan,missvalue, $
                                                 outlier,header_img.icols,zahl,mv,olcorrmeth, olstddev, olthresh, recFrom,recTo))
                                 X1=callpreprocessing(X1,normFreq,quest,fp,corrterm,nrPSmooth,mv)
                               end
;X1=swap_endian(X1)
        if flagUR[6] eq 1 then begin
                                 X2=float(readXFile(pixelzeile,unit13,pzbuff, band, dos, BOrder, lan,missvalue, $
                                                 outlier,header_img.icols,zahl,mv,olcorrmeth, olstddev, olthresh, recFrom,recTo))
                                 X2=callpreprocessing(X2,normFreq,quest,fp,corrterm,nrPSmooth,mv)
                                 end
        if flagUR[7] eq 1 then begin
                                 X3=float(readXFile(pixelzeile,unit15,pzbuff, band, dos, BOrder, lan,missvalue, $
                                                 outlier,header_img.icols,zahl,mv,olcorrmeth, olstddev, olthresh, recFrom,recTo))
                                 X3=callpreprocessing(X3,normFreq,quest,fp,corrterm,nrPSmooth,mv)
                               end
        if flagUR[8] eq 1 then begin
                                 X4=float(readXFile(pixelzeile,unit17,pzbuff, band, dos, BOrder, lan,missvalue, $
                                                 outlier,header_img.icols,zahl,mv,olcorrmeth, olstddev, olthresh, recFrom,recTo))
                                 X4=callpreprocessing(X4,normFreq,quest,fp,corrterm,nrPSmooth,mv)
                               end
        if flagUR[9] eq 1 then begin
                                 X5=float(readXFile(pixelzeile,unit19,pzbuff, band, dos, BOrder, lan,missvalue, $
                                                 outlier,header_img.icols,zahl,mv,olcorrmeth, olstddev, olthresh, recFrom,recTo))
                                 X5=callpreprocessing(X5,normFreq,quest,fp,corrterm,nrPSmooth,mv)
                               end
      endelse

; tempor�re Offsetkorrekturen der Zeitreihe (Trishenko et al. 2002, RSE 81, 1-18)
;  pixelzeile=pixelzeile/10000.
;  ende=n_elements(pixelzeile[0,*])-1
;  pixelzeile[*,0:71]  =pixelzeile[*,0:71]- (0.00224+0.00428*pixelzeile[*,0:71]-0.00276*pixelzeile[*,0:71]^2)
;  pixelzeile[*,72:143]=pixelzeile[*,72:143]- (0.00003+0.01558*pixelzeile[*,72:143]-0.03521*pixelzeile[*,72:143]^2)
;  pixelzeile[*,144:ende]=pixelzeile[*,144:ende]- (-0.00138+0.08156*pixelzeile[*,144:ende]-0.02569*pixelzeile[*,144:ende]^2)

      ; Data-preprocessing
      pixelzeile=callpreprocessing(pixelzeile,normFreq,quest,fp,corrterm,nrPSmooth,mv)

      ; Schwingungsanalyse
      if (first eq 1) and (IFFT lt 8) then begin   ; evtl. Fourieranalyse

            for k=0,header_img.icols-1 do $
             if min(pixelzeile[k,*]) ne max(pixelzeile[k,*]) then begin
                xcorr=pixelzeile[k,*]-mean(pixelzeile[k,*])

                case IFFT of
                0: begin
                    ; Amplitude
                    ; buffer[k,*]= fkoeffs * conj(fkoeffs) / samples   ; wegschreiben des Power spectrums

              ;EMD
                     ;xcorr=pixelzeile[k,*]
                     ;temp=emd(transpose(xcorr))
                     ;if n_elements(temp[0,*]) lt 7 then  buffer[k,*]=0 else $
                     ;buffer[k,*]=temp[*,6]
                     ;buffer[k,*]=temp[*,n_elements(temp[0,*])-1] ; long-term trend
              ;ENDE EMD

              ; neue Variante
                     buffer[k,*]=periodogram(xcorr,windType,1,'Hertz')
;WL,pixelzeile[k,*],scale_avg,zahl,MorletP,PaulP,DOGP, Mother, $
;                           PAD,LAG1,SIGLVL,VERBOSE,DT,DJ,S0,NrScl,scale_l, scale_h;,LOCAL=1

;                    pz = ((pixelzeile[k,*] - TOTAL(pixelzeile[k,*])/samples))
;                    fkoeffs=fft(pz)
;                    power = (samples*(ABS(fkoeffs))^2);/2*variance(pz)
;                    r1 = (A_CORRELATE(pz,1) + SQRT(A_CORRELATE(pz,2)))/2.
;                    signif=fft_signif(pz,1.,0.95,LAG1=r1)
                    ; Gleichung 17 in Torrence and Compo

;                    buffer[k,*]=power/signif
;                    temp=where(buffer[k,*] gt 1, count) ; nur sig. Werte anzeigen
;                    if count gt 0 then buffer[k,temp]=1
;                    temp=where(buffer[k,*] le 1, count) ; nicht sig. Werte
;                    if count gt 0 then buffer[k,temp]=0


                   ; Phase
                   ;buffer[k,*]=phase(pixelzeile[k,*])
                   ; fkoeffs=fft(pixelzeile[k,*])
                   ; buffer[k,*]=ATAN(fkoeffs, /PHASE) * (-1.)  ; Phasenspektrum, vorzeichen umdrehen
                   ; temp=where(buffer[k,*] lt 0, count)
                   ; if count gt 0 then buffer[k,temp] =!pi + (buffer[k,temp]+!pi) ; Phasenspektrum jetzt im Bereich 0-2pi
                   ; tempor�re Skalierung in Monate (f�r Jahresgang!!!)
                   ; buffer[k,*]=12. / (2 * !pi)*buffer[k,*]


                   end
                1: begin  ;PSD
                     buffer[k,*]=pwelch(xcorr,nwind,windType,noverlap)
                   end
                ;5: begin
                ;     x=pixelzeile[k,*]
                ;     if flagUR[10] eq 0 then fourierPoly, x, quest.freq1, quest.freq2,quest.NoHarmonics1, quest.NoHarmonics2, fp, corrterm, CORR=1 $
                ;     else fourierPoly, pixelzeile[k,*], quest.freq1, quest.freq2,quest.NoHarmonics1, quest.NoHarmonics2, fp
                ;     pixelzeile[k,*]=x
                ;   end
                2: begin
                     ; scale_avg=fltarr(n_elements(pixelzeile[k,*]))
                      ; WL,pixelzeile[k,*],scale_avg,zahl,MorletP,PaulP,DOGP, Mother, $
                      ;     PAD,LAG1,SIGLVL,VERBOSE,DT,DJ,S0,NrScl,scale_l, scale_h;,LOCAL=1
                      ; buffer[k,*]=scale_avg
                     end
                3: buffer[k,0:samples-2]=A_CORRELATE(xcorr,indgen(samples-1)+1)
                4: begin
                        xypixelzeile[k,*]=xypixelzeile[k,*]-mean(xypixelzeile[k,*])
                        if x2y eq 0 then $
                          crossxy, xcorr, xypixelzeile[k,*],nwind,windType,noverlap, co, qu, coh,p,amp else $
                          crossxy, xypixelzeile[k,*], xcorr,nwind,windType,noverlap, co, qu, coh,p,amp
                        fkoeffs=fft(xypixelzeile[k,*])
                        xybufferFFT[k,*]= fkoeffs * conj(fkoeffs) / samples  ; wegschreiben des Power spectrums
                        xybufferXy2[k,*]=abs(xy2)
                        xybufferCo[k,*]=co
                        xybufferQu[k,*]=qu
                        xybufferP[k,*]=p
                        xybufferAmp[k,*]=amp
                        xybufferCoh[k,*]=coh
                    end
                5: begin ; Phasenspektrum
                      buffer[k,*]=phase(xcorr)
                   end
                6: begin ; WFFT
                      buffer[k,*]=absDiffSegmentsPM(pixelzeile[k,*],WFFTnwindDiff,WFFTnoverlapDiff,quest.WFFTtype,quest.WFFTfreq)
                   end
                7: begin ; Vegetation metrics
                      buffer[k,*]=callveglength(pixelzeile[k,*],WFFTnwindDiff,quest)
                   end


                endcase
             end else buffer[k,*]=0
         if (IFFT eq 0) or (IFFT eq 1) or (IFFT eq 3) or (IFFT eq 5) or (IFFT eq 6) or (IFFT eq 7) then writeu,unit5,buffer   ; Fourierkoeff. wegschreiben
         if IFFT eq 4 then begin
          if flagxy[0] eq 1 then writeu,unit10,xybufferFFT
          if flagxy[1] eq 1 then writeu,unit12,xybufferXy2
          if flagxy[2] eq 1 then writeu,unit14,xybufferCo
          if flagxy[3] eq 1 then writeu,unit16,xybufferQu
          if flagxy[4] eq 1 then writeu,unit18,xybufferP
          if flagxy[5] eq 1 then writeu,unit20,xybufferAmp
          if flagxy[6] eq 1 then writeu,unit22,xybufferCoh
         endif
       endif



       ; Trendanalyse
      pz=0
      mittelvek=fltarr(n_elements(pixelzeile[*,0]))  ; WEG
      stdvek=fltarr(n_elements(pixelzeile[*,0]))     ; WEG
      if max ([flag, flagUR]) eq 1 then $
       for i=0,header_img.icols-1 do $
       begin
        mittelvek[i]=mean(pixelzeile[i,*])
        stdvek[i]=stddev(pixelzeile[i,*])
        if flagUR[5] eq 1 then begin
          X1Pixel=reform(X1[i,*])
      ;   if quest.normalize eq 5 then X1Pixel=autosc(X1Pixel)
        end
        if flagUR[6] eq 1 then begin
          X2Pixel=reform(X2[i,*])
      ;    if quest.normalize eq 5 then X2Pixel=autosc(X2Pixel)
        end
        if flagUR[7] eq 1 then begin
          X3Pixel=reform(X3[i,*])
      ;    if quest.normalize eq 5 then X3Pixel=autosc(X3Pixel)
        end
        if flagUR[8] eq 1 then begin
          X4Pixel=reform(X4[i,*])
      ;    if quest.normalize eq 5 then X4Pixel=autosc(X4Pixel)
        end
        if flagUR[9] eq 1 then begin
          X5Pixel=reform(X5[i,*])
      ;    if quest.normalize eq 5 then X5Pixel=autosc(X5Pixel)
        end

        Y=float(pixelzeile[i,*])
        ;X=zahl[0:samples-1]
        if (quest.normalize[8] eq 1) or (quest.normalize[9] eq 1) then X=findgen(n_elements(pixelzeile[0,*])) $ ; bei saisonalen Mittelwerten
        else X=zahl[0:samples-1]

      ; Fourierpolynome berechnen
        temp=Y
        if flagUR[10] eq 1 then fourierPoly, Y, quest.freq1, quest.freq2,quest.NoHarmonics1, quest.NoHarmonics2, fp
        Y=temp

        trendpar=trendtest(flag, X, Y, missvalue, mv,lagsADF,NoLags,flagUR, flagR, X1Pixel, $
                           X2Pixel, X3Pixel, X4Pixel, X5Pixel, quest,fp, FromTo,nwindDiff,noverlapDiff)

       _b[i]=trendpar.b & _Const[i]=trendpar.Const & _Sigma[i]=trendpar.Sigma & _Ttest[i]=trendpar.Ttest
       _R[i]=trendpar.R & _MKslope[i]=trendpar.MKslope & _MKtest[i]=trendpar.MKtest
       _Status[i]=trendpar.Status & _Max[i]=trendpar.Maxwert & _TimeMax[i]=trendpar.TimeMax
       _RMS[i]=trendpar.RMS & _Stdabw[i]=trendpar.Stdabw & _SMKtest[i]=trendpar.SMKtest
       _mean[i]=trendpar.mittel & _SMKslope[i]=trendpar.SMKslope & _MSMKtest[i]=trendpar.MSMKtest
       _varkoeff[i]=trendpar.varkoeff & _relerror[i]=trendpar.relerror
       _skew[i]=trendpar.skew & _kurt[i]=trendpar.kurt & _abbe[i]=trendpar.abbe
       _rho[i]=trendpar.rho & _tau[i]=trendpar.tau & _sigrho[i]=trendpar.sigrho & _sigtau[i]=trendpar.sigtau
       _Min[i]=trendpar.mini & _Maxi[i]=trendpar.maxi & _MinToMax[i]=trendpar.MintoMax & _MaxToMin[i]=trendpar.MaxtoMin
       _Range[i]=trendpar.range
       _ADFkoeff[i,*]=trendpar.ADFkoeff
       if sizeBandname2 gt sizeBandname1 then begin
         _olsUR[i]=trendpar.olsUR & _olsR[i]=trendpar.olsR
       end
       ; bei Unresticeted regression Originalwerte/Residuen oder gefittet Y-Werte rausschreiben
       if max(flagUR) eq 1 then pixelzeile[i,*]=float(trendpar.olsUR.resid) else $
          pixelzeile[i,*]=float(pixelzeile[i,*])
       if quest.normalize[5] eq 1 then case quest.resid of
         1: pixelzeile[i,*]=pixelzeile[i,*] * stdvek[i]  ; bei Residuen auf Offset (Mittelwert) verzichten
         else: pixelzeile[i,*]=pixelzeile[i,*] * stdvek[i]+mittelvek[i] ; bei Original+Yfit offset verwenden
       endcase

       end   ; von icols


;tempor�re Umkehrung der Standardisierung
 ;for n=0, floor(samples/normFreq)-1 do begin  ; Differenzen
 ;   pixelzeile[*,n*normFreq:n*normFreq+normFreq-1]=pixelzeile[*,n*normFreq:n*normFreq+normFreq-1]*pixelzeileStd +pixelzeileneu
 ;endfor
 ;pixelzeile=pixelzeile+corrtermVec   ; Trend wieder addieren

      writeresults,pixelzeile,unit1,unit3,hsstack,_olsUR,_olsR, noLags,first,FromTo, $  ; Ergebnisse rausschreiben und ggf. Differenenbilder berechnen
       _b,_Const,_Sigma,_Ttest,_R,_RMS,_relerror,_Max,_TimeMax,_MKtest,_MKslope,_SMKtest,_SMKslope,_MSMKtest,_stdabw,_mean, $
       _varkoeff,_Status,_skew,_kurt,_abbe,_rho,_tau,_sigrho,_sigtau, stdvek,mittelvek, $
       _Min, _Maxi, _MinToMax, _MaxToMin, _Range, _ADFkoeff


endfor

    if samples-1 le 80 then for i=1,samples do close,i ; Schlie�en der Dateien
    if max([flag,flagUR]) eq 1 then free_lun,unit1, unit2
    if (first eq 1) and (thresh gt 0) then free_lun, hsstack, hshead
    if (first eq 1) and (quest.resid ge 0) then free_lun,unit3, unit4
    if (IFFT eq 0) or (IFFT eq 1) or (IFFT eq 3) or (IFFT eq 5) or (IFFT eq 6) or (IFFT eq 7) then free_lun,unit5,unit6


    if (flagxy[0] eq 1) and (xyfile ne '') then free_lun,unit10   ; FFT
    if (flagxy[1] eq 1) and (xyfile ne '') then free_lun,unit12   ; Cross spectrum
    if (flagxy[2] eq 1) and (xyfile ne '') then free_lun,unit14   ; Co spectrum
    if (flagxy[3] eq 1) and (xyfile ne '') then free_lun,unit16   ; Quadratur spectrum
    if (flagxy[4] eq 1) and (xyfile ne '') then free_lun,unit18   ; Phase spectrum
    if (flagxy[5] eq 1) and (xyfile ne '') then free_lun,unit20   ; Amplitude spectrum
    if (flagxy[6] eq 1) and (xyfile ne '') then free_lun,unit22   ; Coherence spectrum
    if flagUR[5] eq 1 then free_lun,unit11
    if flagUR[6] eq 1 then free_lun,unit13
    if flagUR[7] eq 1 then free_lun,unit15
    if flagUR[8] eq 1 then free_lun,unit17
    if flagUR[9] eq 1 then free_lun,unit19
    if (quest.normalize[3] eq 1) or (quest.normalize[4] eq 1)  then close,81,83,84,85
    if (quest.normalize[7] eq 1) then close,86,87   ; prewhitening
    first=0
print,'end'

endelse ; von Wavelet else

  ;######################
  ; EnMAP-Box integration
  ; Open output images
  if file_test(ofile) then enmapBox_openImage, ofile
  if (quest.normalize[3] eq 1) or (quest.normalize[4] eq 1) then begin 
    enmapBox_openImage, workDir+'m-mean.bil'
    enmapBox_openImage, workDir+'m-stddev.bil'
    enmapBox_openImage, workDir+'CountsPosDiffMonthlyAvg.bil'
    enmapBox_openImage, workDir+'CountsnegDiffMonthlyAvg.bil'
  endif
  if (quest.normalize[7] eq 1) then begin
    enmapBox_openImage, workDir+'AR-order.bil'
    enmapBox_openImage, workDir+'AR-koeffs.bil'
  endif
  ;######################

;ok=1     ; fehlerfrei durchgelaufen
; Fehlerbehandlung
;ioerror: if fehler eq 0 then begin
;           info=dialog_message(!ERROR_STATE.MSG,/INFORMATION,TITLE='TimeStats')
           ;WIDGET_CONTROL, textwid, SET_VALUE=!ERROR_STATE.MSG
;           stop
;         end


end

