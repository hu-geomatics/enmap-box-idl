function mvcorr,pixelzeile,cols,zahl,mv,missvalue
; f�hrt die Korrektur von Missing values und Ausrei�ern durch
  for k=0,cols-1 do begin
   temp=pixelzeile[k,*]
   if min(temp) ne max(temp) then begin   ; Maske!
        ; Missing Value Korrektur
     if missvalue gt 1 then begin
          interpolate_mv, zahl, temp, mv, missvalue
          pixelzeile[k,*]=temp
     endif
   endif
  endfor
return, pixelzeile
end
