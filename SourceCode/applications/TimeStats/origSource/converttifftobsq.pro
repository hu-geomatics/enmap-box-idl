pro convertTiffToBSQ,datei,batch,DOS,textwid
 WIDGET_CONTROL, textwid, SET_VALUE='Convert TIF...'
 openr,1,datei
 dateiout='stack-tif.BSQ'
 openw,2,dateiout
 openw,3,strcompress('stack-tif'+'.hdr')
 file=''
 i=0
 while not eof(1) do begin
  i=i+1
  readf,1,file
  result=float(read_tiff(file))
  writeu,2,result
 endwhile
 close,1,2
 batch=1
 datei=dateiout
 envi_headerNEU,3,n_elements(result[*,0]),n_elements(result[0,*]),i,DOS

 close,3
end


