 function absDiffSegmentsPM,x,nwind,noverlap,type,period,CHECKNO=CHECKNO
 ;   x: time-series
 ;   nwind: length of the window, determines the no. of sections
 ;   noverlap: no. of overlapping points by moving window (default: 50% overlapping, if value < 0)

 ;   diff returns absolute differences in (overlapping) segments of a ts, if
 ;   CHECKNO is set a string array indicating beginning and end of each segment
 ;   X is divided into several
 ;   sections, depending on the window length nwind and the no of overlapping points
 ;   default: 50%.
 ;   type: 'PHASE' for phase spectram, otherwise magnitude spectra
 ;   period: determines the target band (targetband = nwind/period)


   x = reform(x)
   M = n_elements(x);


   ; Obtain the necessary information to segment X

   if nwind gt M then begin
      print,'The length of the segments cannot be greater than the length of the input signal. Using default...'
      nwind=M ; Length of window equals lenght of x
   end
   if noverlap ge nwind then begin
      print, 'The number of samples to overlap must be less than the length of the segments. Using default...';
      noverlap=-1
   end

   if noverlap lt 0 then noverlap = 0;fix(0.5 *nwind); use default:50% overlap

   ; Compute the number of segments
   k = fix((M-noverlap)/(nwind-noverlap));
   diff=fltarr(k)
   ; Compute differences in x for each segment

   xindx = 1
   index=indgen(nwind)
   if KEYWORD_SET(CHECKNO) then  begin
    diff=strarr(k)
    for i = 0,k-1 do begin
       diff[i]=strcompress(string(index[0]+1))+ '-'+strcompress(string(index[nwind-1])+1)
       index = index + nwind - noverlap
    end
   end else begin
     pos=nwind / period
     for i = 0,k-1 do begin
       ;diff[i]=x[index[nwind-1]]-x[index[0]]
       x[index]=x[index]-mean(x[index])
       if type eq 0 then begin   ;Phase
         ;temp=periodogram(x[index],'HANN',1,'Hertz')
         phasensp=phase(x[index])
         diff[i]=phasensp[pos] ;12 Monate
         ;diff[i]=temp[1]
         ;diff[i]=mean(x[index])
       end else if type eq 1  then begin  ; Amplitude
        diff[i]=VegLength(x[index])
        ;spectra=periodogram(x[index],'HANN',1,'Hertz')
        ;diff[i]=spectra[pos]
       end
       index = index + nwind - noverlap
     end
   end
   return,diff
end