pro base_widgetFFT_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
     'group6': begin
               case ev2.value of
                0: windType='HANN'
                1: windType='HAMM'
                2: windType='BARTLETT'
                3: windType='NO'
               endcase
              end
    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widgetFFT, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
 values4 =['Hanning','Hamming', 'Bartlett','No']

 case windType of
  'HANN'    :wType=0
  'HAMM'    :wType=1
  'BARTLETT':wType=2
  'NO'      :wType=3
 endcase

 xy=  WIDGET_BASE(TITLE='Filter for leakage reduction',/COLUMN)
 xy3= WIDGET_BASE(xy, TITLE='Parameter for periodogram estimation',/COLUMN)

 bgroup6= CW_BGROUP(xy3,values4,/ROW, SET_VALUE=wType,LABEL_TOP='Select taper window function',/FRAME,/EXCLUSIVE, UVALUE='group6')

 endbase = WIDGET_BASE(xy, /ROW,/ALIGN_BOTTOM)
 ok= widget_button(endbase, VALUE='done', UVALUE='done')

 WIDGET_CONTROL, xy, /NO_COPY,/REALIZE
 XMANAGER,'base_widgetXy', xy, GROUP=GROUP1
end