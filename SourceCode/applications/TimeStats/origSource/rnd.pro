function rnd,number
; helper f�r Abbesches Kriterium: Autor Tim ERbrecht

    str = strcompress(string(number), /remove_all)
    strlength = strlen(str)
    num = strmid(str,0,strpos(str,'.')+3)
    num=float(num)
    test=fix(strmid(str,strpos(str,'.')+3,1))
    if test ge 5 then num = num+0.01
    return,num
end