pro base_widget4_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
  case uval of
    'signif': begin
               if ev2.value eq 0 then quest.sig = 0 else quest.sig = 1
             end
    'group3': begin
                if flag[ev2.value] eq 0 then flag[ev2.value]=1 else flag[ev2.value]=0
                if (flag[11] eq 1) or (flag[12] eq 1) or (flag[13] eq 1) then WIDGET_CONTROL,_MKfreq, SENSITIVE=1 $
                else WIDGET_CONTROL,_MKfreq, SENSITIVE=0
              end
    'group01':  WIDGET_CONTROL, _lagsADF, GET_VALUE=lagsADF
    'groupfreq':begin
                  WIDGET_CONTROL, _freq, GET_VALUE=freq
                  quest.freq=freq
                end
    ;'group2': base_widget4b
    'ok'  :   WIDGET_CONTROL, ev2.top,/DESTROY
  endcase
end


pro base_widget4, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
  values3 = ['Reg. bias (b)','Reg. offset','Sigma','T-test (b)','r','RMS','RMS/mean','MaxResid','TimeMaxResid', $
         'MK test','K slope','SK test','SK slope','Mod.SK test','Stdev','Mean','Varkoeff','valid n', $
         'skewness','kurtosis','Abbe criterion','rho rank corr','tau rank korr','sig. rho','sig. tau',$
         'min','max','time min->max','time max->min','range','ADF test, incl. const./trend']
  values =['save emp. t-scores (lin. reg) / emp. z-scores (MK, SMK, MSK)','save related twosided P-values']

  trendpar=  WIDGET_BASE(TITLE='Basic statistics and trend parameters',/COLUMN)
  bgroup3= CW_BGROUP(trendpar,values3,COLUMN=6, set_value=flag, $
         LABEL_TOP='Parameters to be extracted',/FRAME, /NONEXCLUSIVE,UVALUE='group3')

  parADF  = WIDGET_BASE(trendpar, /ROW)
  _lagsADF = CW_FIELD(parADF,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=lagsADF,TITLE='Lags for Augmented-Dickey-Fuller (ADF) test',UVALUE='group01')
  freq  = WIDGET_BASE(trendpar, /ROW)
  _MKfreq= CW_FIELD(freq,/INTEGER,/ALL_EVENTS,YSIZE=70, $
                      VALUE=quest.freq,TITLE='Period of seasonal component',UVALUE='groupfreq')


  sig= CW_BGROUP(trendpar,values,/ROW,SET_VALUE=quest.sig,LABEL_TOP='Significance test options',/FRAME,/EXCLUSIVE,UVALUE='signif')

;  regOpt  = WIDGET_BASE(trendpar, /ROW)
;  extRegOpt= widget_button(regOpt, VALUE='Extendet regression options', UVALUE='group2')

  endbase = WIDGET_BASE(trendpar, /ROW,/ALIGN_BOTTOM)
  ok= widget_button(endbase, VALUE='ok', UVALUE='ok')
  WIDGET_CONTROL, extRegOpt, SENSITIVE=1
  WIDGET_CONTROL, trendpar, /NO_COPY,/REALIZE
   if (flag[11] eq 1) or (flag[12] eq 1) or (flag[13] eq 1) then WIDGET_CONTROL,_MKfreq, SENSITIVE=1 $
       else WIDGET_CONTROL,_MKfreq, SENSITIVE=0

  XMANAGER,'base_widget4', trendpar, GROUP=GROUP1
end
