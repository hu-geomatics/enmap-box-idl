  function defineBandnames2,bandnames,FromTo
  common blk1
     ; evalute unrestricted flags
     ;sizeBandname1=n_elements(bandnames) ; size of the bandname array till now
     if max(flagUR) gt 0 then begin
      bandnames=[bandnames, 'UR const.']
      if flagUR[0] eq 1 then bandnames=[bandnames, 'UR reg.coff. time']
      if flagUR[1] eq 1 then bandnames=[bandnames, 'UR reg.coff. time^2']
      if flagUR[2] eq 1 then bandnames=[bandnames, 'UR reg.coff. time^3']
      if flagUR[3] eq 1 then bandnames=[bandnames, 'UR reg.coff. time^4']
      if flagUR[4] eq 1 then bandnames=[bandnames, 'UR reg.coff. time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('UR reg.coff. Yt-' + string(z))]   ; lags UR Y
      if flagUR[5] eq 1 then bandnames=[bandnames, 'UR reg.coff. X1']
      if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('UR reg.coff. X1t-' + string(z))]  ; lags UR X1
      if flagUR[6] eq 1 then bandnames=[bandnames, 'UR reg.coff. X2']
      if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('UR reg.coff. X2t-' + string(z))]  ; lags UR X2
      if flagUR[7] eq 1 then bandnames=[bandnames, 'UR reg.coff. X3']
      if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('UR reg.coff. X3t-' + string(z))]  ; lags UR X3
      if flagUR[8] eq 1 then bandnames=[bandnames, 'UR reg.coff. X4']
      if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('UR reg.coff. X4t-' + string(z))]  ; lags UR X4
      if flagUR[9] eq 1 then bandnames=[bandnames, 'UR reg.coff. X5']
      if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('UR reg.coff. X5t-' + string(z))]  ; lags UR X5

      if flagUR[0] eq 1 then bandnames=[bandnames, 'UR t-emp. time']
      if flagUR[1] eq 1 then bandnames=[bandnames, 'UR t-emp. time^2']
      if flagUR[2] eq 1 then bandnames=[bandnames, 'UR t-emp. time^3']
      if flagUR[3] eq 1 then bandnames=[bandnames, 'UR t-emp. time^4']
      if flagUR[4] eq 1 then bandnames=[bandnames, 'UR t-emp. time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('UR t-emp. Yt-' + string(z))]   ; lags UR Y
      if flagUR[5] eq 1 then bandnames=[bandnames, 'UR t-emp. X1']
      if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('UR t-emp. X1t-' + string(z))]  ; lags UR X1
      if flagUR[6] eq 1 then bandnames=[bandnames, 'UR t-emp. X2']
      if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('UR t-emp. X2t-' + string(z))]  ; lags UR X2
      if flagUR[7] eq 1 then bandnames=[bandnames, 'UR t-emp. X3']
      if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('UR t-emp. X3t-' + string(z))]  ; lags UR X3
      if flagUR[8] eq 1 then bandnames=[bandnames, 'UR t-emp. X4']
      if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('UR t-emp. X4t-' + string(z))]  ; lags UR X4
      if flagUR[9] eq 1 then bandnames=[bandnames, 'UR t-emp. X5']
      if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('UR t-emp. X5t-' + string(z))]  ; lags UR X5

      if flagUR[0] eq 1 then bandnames=[bandnames, 'UR t-sig. time']
      if flagUR[1] eq 1 then bandnames=[bandnames, 'UR t-sig. time^2']
      if flagUR[2] eq 1 then bandnames=[bandnames, 'UR t-sig. time^3']
      if flagUR[3] eq 1 then bandnames=[bandnames, 'UR t-sig. time^4']
      if flagUR[4] eq 1 then bandnames=[bandnames, 'UR t-sig. time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('UR t-sig. Yt-' + string(z))]   ; lags UR Y
      if flagUR[5] eq 1 then bandnames=[bandnames, 'UR t-sig. X1']
      if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('UR t-sig. X1t-' + string(z))]  ; lags UR X1
      if flagUR[6] eq 1 then bandnames=[bandnames, 'UR t-sig. X2']
      if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('UR t-sig. X2t-' + string(z))]  ; lags UR X2
      if flagUR[7] eq 1 then bandnames=[bandnames, 'UR t-sig. X3']
      if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('UR t-sig. X3t-' + string(z))]  ; lags UR X3
      if flagUR[8] eq 1 then bandnames=[bandnames, 'UR t-sig. X4']
      if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('UR t-sig. X4t-' + string(z))]  ; lags UR X4
      if flagUR[9] eq 1 then bandnames=[bandnames, 'UR t-sig. X5']
      if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('UR t-sig. X5t-' + string(z))]  ; lags UR X5

      if flagUR[0] eq 1 then bandnames=[bandnames, 'UR r time']
      if flagUR[1] eq 1 then bandnames=[bandnames, 'UR r time^2']
      if flagUR[2] eq 1 then bandnames=[bandnames, 'UR r time^3']
      if flagUR[3] eq 1 then bandnames=[bandnames, 'UR r time^4']
      if flagUR[4] eq 1 then bandnames=[bandnames, 'UR r time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('UR r Yt-' + string(z))]   ; lags UR Y
      if flagUR[5] eq 1 then bandnames=[bandnames, 'UR r X1']
      if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('UR r X1t-' + string(z))]  ; lags UR X1
      if flagUR[6] eq 1 then bandnames=[bandnames, 'UR r X2']
      if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('UR r X2t-' + string(z))]  ; lags UR X2
      if flagUR[7] eq 1 then bandnames=[bandnames, 'UR r X3']
      if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('UR r X3t-' + string(z))]  ; lags UR X3
      if flagUR[8] eq 1 then bandnames=[bandnames, 'UR r X4']
      if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('UR r X4t-' + string(z))]  ; lags UR X4
      if flagUR[9] eq 1 then bandnames=[bandnames, 'UR r X5']
      if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('UR r X5t-' + string(z))]  ; lags UR X5


      ; remaining parameters for total regression model
      bandnames=[bandnames, 'UR F-emp.']
      bandnames=[bandnames, 'UR F-sig.']
      bandnames=[bandnames, 'UR Durbin-Watson']
      bandnames=[bandnames, 'UR Yfit_start']
      bandnames=[bandnames, 'UR Yfit_end']
      bandnames=[bandnames, 'UR rel. growth[%]']
      bandnames=[bandnames, 'UR abs. growth']
      bandnames=[bandnames, 'UR Rmult']
      if flagUR[10] eq 1 then begin
        for z=1,quest.NoHarmonics1 do begin ; zuerst Cosinus- dann die Sinusterme, erste Schwingung entspricht der Hauptfreqzen
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. Fourier. polyn. cos']  ; die folgenden den Oberschwingungen
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-emp']
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-sig']
        end
        for z=1,quest.NoHarmonics1 do begin
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. Fourier. polyn. sin']
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-emp']
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-sig']
        end
        for z=1,quest.NoHarmonics2 do begin ; zuerst Cosinus- dann die Sinusterme, erste Schwingung entspricht der Hauptfreqzen
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. Fourier. polyn. cos']  ; die folgenden den Oberschwingungen
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-emp']
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-sig']
        end
        for z=1,quest.NoHarmonics2 do begin
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. Fourier. polyn. sin']
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-emp']
          bandnames=[bandnames,'UR ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-sig']
        end
      end
     end
     if n_elements(FromTo) gt 1 then for z=0, n_elements(FromTo)-1 do $
         bandnames=[bandnames, strcompress('Modelled abs. change (end-start) ' + FromTo(z))]  ; Model. Differnzen

     ; evalute restricted flags
     if max(flagR) gt 0 then begin
      bandnames=[bandnames, 'R const.']
      if flagR[0] eq 1 then bandnames=[bandnames, 'R reg.coff. time']
      if flagR[1] eq 1 then bandnames=[bandnames, 'R reg.coff. time^2']
      if flagR[2] eq 1 then bandnames=[bandnames, 'R reg.coff. time^3']
      if flagR[3] eq 1 then bandnames=[bandnames, 'R reg.coff. time^4']
      if flagR[4] eq 1 then bandnames=[bandnames, 'R reg.coff. time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('R reg.coff. Yt-' + string(z))]   ; lags R Y
      if flagR[5] eq 1 then begin
         bandnames=[bandnames, 'R reg.coff. X1']
         if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('R reg.coff. X1t-' + string(z))]  ; lags R X1
      end
      if flagR[6] eq 1 then begin
         bandnames=[bandnames, 'R reg.coff. X2']
         if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('R reg.coff. X2t-' + string(z))]  ; lags R X2
      end
      if flagR[7] eq 1 then begin
         bandnames=[bandnames, 'R reg.coff. X3']
         if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('R reg.coff. X3t-' + string(z))]  ; lags R X3
      end
      if flagR[8] eq 1 then begin
         bandnames=[bandnames, 'R reg.coff. X4']
         if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('R reg.coff. X4t-' + string(z))]  ; lags R X4
      end
      if flagR[9] eq 1 then begin
         bandnames=[bandnames, 'R reg.coff. X5']
         if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('R reg.coff. X5t-' + string(z))]  ; lags R X5
      end

      if flagR[0] eq 1 then bandnames=[bandnames, 'R t-emp. time']
      if flagR[1] eq 1 then bandnames=[bandnames, 'R t-emp. time^2']
      if flagR[2] eq 1 then bandnames=[bandnames, 'R t-emp. time^3']
      if flagR[3] eq 1 then bandnames=[bandnames, 'R t-emp. time^4']
      if flagR[4] eq 1 then bandnames=[bandnames, 'R t-emp. time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('R t-emp. Yt-' + string(z))]   ; lags R Y
      if flagR[5] eq 1 then begin
         bandnames=[bandnames, 'R t-emp. X1']
         if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('R t-emp. X1t-' + string(z))]  ; lags R X1
      end
      if flagR[6] eq 1 then begin
         bandnames=[bandnames, 'R t-emp. X2']
         if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('R t-emp. X2t-' + string(z))]  ; lags R X2
      end
      if flagR[7] eq 1 then begin
         bandnames=[bandnames, 'R t-emp. X3']
         if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('R t-emp. X3t-' + string(z))]  ; lags R X3
      end
      if flagR[8] eq 1 then begin
         bandnames=[bandnames, 'R t-emp. X4']
         if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('R t-emp. X4t-' + string(z))]  ; lags R X4
      end
      if flagR[9] eq 1 then begin
         bandnames=[bandnames, 'R t-emp. X5']
         if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('R t-emp. X5t-' + string(z))]  ; lags R X5
      end

      if flagR[0] eq 1 then bandnames=[bandnames, 'R t-sig. time']
      if flagR[1] eq 1 then bandnames=[bandnames, 'R t-sig. time^2']
      if flagR[2] eq 1 then bandnames=[bandnames, 'R t-sig. time^3']
      if flagR[3] eq 1 then bandnames=[bandnames, 'R t-sig. time^4']
      if flagR[4] eq 1 then bandnames=[bandnames, 'R t-sig. time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('R t-sig. Yt-' + string(z))]   ; lags R Y
      if flagR[5] eq 1 then begin
         bandnames=[bandnames, 'R t-sig. X1']
         if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('R t-sig. X1t-' + string(z))]  ; lags R X1
      end
      if flagR[6] eq 1 then begin
         bandnames=[bandnames, 'R t-sig. X2']
         if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('R t-sig. X2t-' + string(z))]  ; lags R X2
      end
      if flagR[7] eq 1 then begin
         bandnames=[bandnames, 'R t-sig. X3']
         if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('R t-sig. X3t-' + string(z))]  ; lags R X3
      end
      if flagR[8] eq 1 then begin
         bandnames=[bandnames, 'R t-sig. X4']
         if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('R t-sig. X4t-' + string(z))]  ; lags R X4
      end
      if flagR[9] eq 1 then begin
         bandnames=[bandnames, 'R t-sig. X5']
         if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('R t-sig. X5t-' + string(z))]  ; lags R X5
      end

      if flagR[0] eq 1 then bandnames=[bandnames, 'R r time']
      if flagR[1] eq 1 then bandnames=[bandnames, 'R r time^2']
      if flagR[2] eq 1 then bandnames=[bandnames, 'R r time^3']
      if flagR[3] eq 1 then bandnames=[bandnames, 'R r time^4']
      if flagR[4] eq 1 then bandnames=[bandnames, 'R r time^5']
      if lags.lagsY gt 0 then for z=1,lags.lagsY do bandnames  =[bandnames, strcompress('R t-sig. Yt-' + string(z))]   ; lags R Y
      if flagR[5] eq 1 then begin
         bandnames=[bandnames, 'R r X1']
         if lags.lagsX1 gt 0 then for z=1,lags.lagsX1 do bandnames=[bandnames, strcompress('R t-sig. X1t-' + string(z))]  ; lags R X1
      end
      if flagR[6] eq 1 then begin
         bandnames=[bandnames, 'R r X2']
         if lags.lagsX2 gt 0 then for z=1,lags.lagsX2 do bandnames=[bandnames, strcompress('R t-sig. X2t-' + string(z))]  ; lags R X2
      end
      if flagR[7] eq 1 then begin
         bandnames=[bandnames, 'R r X3']
         if lags.lagsX3 gt 0 then for z=1,lags.lagsX3 do bandnames=[bandnames, strcompress('R t-sig. X3t-' + string(z))]  ; lags R X3
      end
      if flagR[8] eq 1 then begin
         bandnames=[bandnames, 'R r X4']
         if lags.lagsX4 gt 0 then for z=1,lags.lagsX4 do bandnames=[bandnames, strcompress('R t-sig. X4t-' + string(z))]  ; lags R X4
      end
      if flagR[9] eq 1 then begin
         bandnames=[bandnames, 'R r X5']
         if lags.lagsX5 gt 0 then for z=1,lags.lagsX5 do bandnames=[bandnames, strcompress('R t-sig. X5t-' + string(z))]  ; lags R X5
      end

      ; remaining parameters for total regression model
      bandnames=[bandnames, 'R F-emp.']
      bandnames=[bandnames, 'R F-sig.']
      bandnames=[bandnames, 'R Durbin-Watson']
      bandnames=[bandnames, 'R Yfit_start']
      bandnames=[bandnames, 'R Yfit_end']
      bandnames=[bandnames, 'R rel. growth[%]']
      bandnames=[bandnames, 'R abs. growth']
      bandnames=[bandnames, 'R Rmult']
      if flagR[10] eq 1 then begin
        for z=1,quest.NoHarmonics1 do begin ; zuerst Cosinus- dann die Sinusterme, erste Schwingung entspricht der Hauptfreqzen
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. Fourier. polyn. cos']  ; die folgenden den Oberschwingungen
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-emp']
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-sig']
        end
        for z=1,quest.NoHarmonics1 do begin
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. Fourier. polyn. sin']
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-emp']
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-sig']
        end
        for z=1,quest.NoHarmonics2 do begin ; zuerst Cosinus- dann die Sinusterme, erste Schwingung entspricht der Hauptfreqzen
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. Fourier. polyn. cos']  ; die folgenden den Oberschwingungen
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-emp']
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. cos t-sig']
        end
        for z=1,quest.NoHarmonics2 do begin
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. Fourier. polyn. sin']
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-emp']
          bandnames=[bandnames,'R ' + STRCOMPRESS(string(z))+'. F.polyn. sin t-sig']
        end
      end
     ; if n_elements(FromTo) gt 1 then for z=0, n_elements(FromTo)-1 do $
     ;    bandnames=[bandnames,'R ' + strcompress('Abs. changes ' + FromTo(z))]  ; Model. Differnzen

      ; Wald statistics for two regressions
      bandnames=[bandnames, 'Wald-F']
      bandnames=[bandnames, 'Wald-sig']
    end
return,bandnames
end
