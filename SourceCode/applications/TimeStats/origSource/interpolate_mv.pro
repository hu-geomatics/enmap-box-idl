pro interpolate_mv, x, data_vec, mv, missvalue,freq
; ersetzt missing values durch den Mittelwert der Zeitreihe oder durch lin. Interpolation,
; ausser im Maskenbereich
missvalueORG=missvalue
temp_mv=where(data_vec eq mv, count)
if count gt 0 then begin
  if (n_elements(data_vec) - count eq 1) then missvalue = 2 ; zu wenig Daten
 case missvalue of
  2: begin     ; globales Mittel
       temp=where (data_vec ne mv,count)
       if count gt 0 then begin
         mittel=mean(data_vec[temp])
         data_vec[temp_mv]=mittel
       end
     end
  3: begin     ; saisonales Mittel
       saisonMean=SeasonMean(data_vec,freq,mv)
       j = temp_mv mod freq
       data_vec[temp_mv] = saisonMean[j]
     end
  4: begin  ; interpolation
       temp_nmv=where(data_vec ne mv, count)
       data_vec_new=data_vec[temp_nmv]
       x_new=x[temp_nmv]
       x_ip =x[temp_mv]
       data_vec_ip=interpol(data_vec_new,x_new,x_ip)
       data_vec[temp_mv]=data_vec_ip
     end
 endcase
endif
missvalue=missvalueORG
end
