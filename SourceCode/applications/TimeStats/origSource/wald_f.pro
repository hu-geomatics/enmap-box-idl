pro wald_F,OlsUR,OlsR
  ; PURPOSE: computes Wald F-test for two regressions
  ; adopted from wald.m from James P. LeSage

  ;  get nobs, nvar from unrestricted and restricted regressions
  nu = olsUR.nobs
  nr = olsR.nobs
  ku = olsUR.noVar
  kr = olsR.noVar
  ; recover residual sum of squares from .sige field of the ols structure
  epeu = olsUR.sige*(nu-ku)
  eper = olsR.sige*(nr-kr);
  numr = ku - kr ;find # of restrictions
  ddof = nu-ku   ;find denominator dof
  fstat1 = (eper - epeu)/numr;  numerator
  fstat2 = epeu/(nu-ku)      ;  denominator
  OlsUR.WaldF = fstat1/fstat2
  OlsUR.Waldsig=1-F_PDF(OlsUR.WaldF,numr,ddof)
  OlsR.WaldF=OlsUR.WaldF ; copy result also in olsR structure
  OlsR.Waldsig=OlsUR.Waldsig
end