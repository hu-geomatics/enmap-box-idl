function dWtest,resid
  ; calulated Durbin-Watson statistics from resid
  ediff=resid[1:n_elements(resid)-1]-resid[0:n_elements(resid)-2]
  DWdenom=transpose(resid) # resid
  DWnom =transpose(ediff) # ediff
  DWvalue=DWnom/DWdenom
  return, DWvalue
end
