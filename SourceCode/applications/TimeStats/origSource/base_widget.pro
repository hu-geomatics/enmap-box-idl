
pro base_widget_event, ev, GROUP=group
COMMON blk1
COMMON blkbase

WIDGET_CONTROL, ev.TOP, GET_UVALUE=textwid
widget_control, ev.id, get_uvalue=uval

case uval of
 'group0': base_widget6
 'group3': base_widget4
 'group3b':base_widget5
 'group3c':base_widgetPre
 'groupCl':base_widgetClean
 'group2': base_widget4b
 'done'  : widget_control, ev.top,/DESTROY
 'ok'    : begin
       para[3]=1; Dieser Schalter wird nicht mehr ben�tigt!
       if (xyfile ne '') and (max(flagxy) ne 0) then four_koeff=1
            WIDGET_CONTROL, base, /HOURGLASS ;  Display busy while computing
            ;if four_koeff eq 1 then para[3]=1
            if min(para) eq 1 then begin
              if (four_koeff_temp eq 0) and (four_koeff eq 1) and (max(flagxy) eq 0) then four_koeff=0
                                                           ; MV evtl. vorhanden, keine Fourieranalyse!!!
              if (IFFT_temp eq 0) and (IFFT eq 1) then IFFT = 0
              ;if (max([flag,flagR,flagUR] eq 1)) or (four_koeff eq 1) then begin
                 trend,textwid
                 widget_control, ev.top,/DESTROY
              ;endif else  info=dialog_message(info[3],/INFORMATION,TITLE='Time-series analysis')
            endif else begin
               temp=where(para eq 0)
               info=dialog_message(info[min(temp)],/INFORMATION,TITLE='Time-series analysis')
            endelse
           end
endcase

end


pro base_widget, GROUP=GROUP
 COMMON blk1
 COMMON blkbase

 ;values0 = ['Input/output file options     ']
 ;values  = ['Data cleaning                    ']
 ;values3c= ['Data pre-processing          ']
 ;values3 = ['Basic statistics and trends ']
 ;values3b =['Seasonal components       ']


 base=WIDGET_BASE(TITLE='TimeStats 1.0',/column)
 bgroup3= widget_button(base,VALUE='Input/output file options',UVALUE='group0')
 ;bgroup3= CW_BGROUP(base,values0,UVALUE='group0')
 optionbase=WIDGET_BASE(base, /COLUMN)

 ;bgroupCl= CW_BGROUP(optionbase,values,UVALUE='groupCl')
 ;bgroup3c= CW_BGROUP(optionbase,values3c,UVALUE='group3c')
 ;bgroup3= CW_BGROUP(optionbase,values3,UVALUE='group3')
 ;bgroup3b= CW_BGROUP(optionbase,values3b,UVALUE='group3b')
 bgroupCl= widget_button(optionbase,VALUE='Data cleaning', UVALUE='groupCl')
 bgroup3c= widget_button(optionbase,VALUE='Data pre-processing', UVALUE='group3c')
 bgroup3b= widget_button(optionbase,VALUE='Seasonal components', UVALUE='group3b')
 bgroup3=  widget_button(optionbase,VALUE='Basic statistics and trends', UVALUE='group3')
 extRegOpt=widget_button(optionbase,VALUE='Advanced regression analysis', UVALUE='group2')


 endbase = WIDGET_BASE(base, /ROW,/ALIGN_BOTTOM)
 text = WIDGET_TEXT(endbase, XSIZE=20)
 ok = widget_button(endbase,VALUE='  ok  ', UVALUE='ok')
 cancel= widget_button(endbase, VALUE='done', UVALUE='done')


 para[4]=1

 WIDGET_CONTROL, base, /NO_COPY,/REALIZE
 WIDGET_CONTROL, base, SET_UVALUE=text
 WIDGET_CONTROL,optionbase, SENSITIVE=0
 XMANAGER,'base_widget', base, GROUP=GROUP, NO_BLOCK=0
end
