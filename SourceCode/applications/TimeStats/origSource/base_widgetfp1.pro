

pro base_widgetFP1_event, ev2, GROUP=group
; Dialog f�r Detrending
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'groupFP1f': begin
                  WIDGET_CONTROL, _FPharmonics1, GET_VALUE=NoHarmonics
                  quest.NoHarmonics1=NoHarmonics
                 end

    'groupFP1': begin
                  WIDGET_CONTROL, _FPfreq1, GET_VALUE=freq
                  quest.freq1=freq
                end

    'groupFP2f': begin
                  WIDGET_CONTROL, _FPharmonics2, GET_VALUE=NoHarmonics2
                  quest.NoHarmonics2=NoHarmonics2
                 end

    'groupFP2': begin
                  WIDGET_CONTROL, _FPfreq2, GET_VALUE=freq
                  quest.freq2=freq
                end
    'group1' : begin
                  case ev2.value of   ; include lin. Trendkomponent
                     0:quest.detrendLin=0       ; yes
                     1:quest.detrendLin=1       ; no
                  endcase
                 end

    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
  endcase
end


pro base_widgetFP1, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase

 values1=  ['yes','no']
 fourier3=  WIDGET_BASE(TITLE='Parameters for fourier polynomial',/COLUMN)
 _FPlin= CW_BGROUP(fourier3,values1,/ROW, SET_VALUE=quest.detrendLin,LABEL_TOP='Include linear trend component',/FRAME, /EXCLUSIVE,UVALUE='group1')

 _FPfreq1=   CW_FIELD(fourier3,/INTEGER,/ALL_EVENTS,VALUE=quest.freq1,TITLE='Length of Fourier period 1',UVALUE='groupFP1')
 _FPharmonics1= CW_FIELD(fourier3,/INTEGER,/ALL_EVENTS,VALUE=quest.NoHarmonics1,TITLE='No. of harmonics',UVALUE='groupFP1f')

 _FPfreq2=   CW_FIELD(fourier3,/INTEGER,/ALL_EVENTS,VALUE=quest.freq2,TITLE='Length of  Fourier period 2',UVALUE='groupFP2')
 _FPharmonics2= CW_FIELD(fourier3,/INTEGER,/ALL_EVENTS,VALUE=quest.NoHarmonics2,TITLE='No. of harmonics',UVALUE='groupFP2f')


 endbase = WIDGET_BASE(fourier3, /ROW,/ALIGN_BOTTOM)
 done= widget_button(endbase, VALUE='done', UVALUE='done')

  WIDGET_CONTROL, fourier3, /NO_COPY,/REALIZE
  XMANAGER,'base_widgetFP1', fourier3, GROUP=GROUP1
end

