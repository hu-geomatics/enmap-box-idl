function abbe_crit, data_vec
;Autor: Tim Erbrecht
n = n_elements(data_vec)

mean_vec = mean(data_vec)
A = total((data_vec[*]-mean_vec)^2)
A = A - rnd((double(((data_vec[0]-mean_vec)^2)+((data_vec[n-1]-mean_vec)^2))/2))

B=0
for i=0,n-2 do begin
B = B + ( ( (rnd(float(data_vec[i]-mean_vec)))-(rnd(float(data_vec[i+1]-mean_vec)))  )^2 )
endfor

;calculate limiting values and test value
low=1-(1/sqrt(n-1))
high=1+(1/sqrt(n-1))
testval = rnd((2*A)/B)

;result of test
if (testval ge low) and (testval le high) then result=1 else result=0
return,result

end