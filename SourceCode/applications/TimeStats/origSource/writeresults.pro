
pro writeResults,pixelzeile,unit1,unit3,hsstack,_olsUR,_olsR, noLags, first,FromTo, $
       _b,_Const,_Sigma,_Ttest,_R,_RMS,_relerror,_Max,_TimeMax,_MKtest,_MKslope,_SMKtest,_SMKslope,_MSMKtest,_stdabw,_mean, $
       _varkoeff,_Status,_skew,_kurt,_abbe,_rho,_tau,_sigrho,_sigtau,stdvek,mittelvek,  $
       _Min,_Maxi, _MinToMax, _MaxToMin, _Range, _ADFkoeff
COMMON blk1
       if flag[0] eq 1 then writeu,unit1,_b
       if flag[1] eq 1 then writeu,unit1,_Const
       if flag[2] eq 1 then writeu,unit1,_Sigma
       if flag[3] eq 1 then writeu,unit1,_Ttest
       if flag[4] eq 1 then writeu,unit1,_R
       if flag[5] eq 1 then writeu,unit1,_RMS
       if flag[6] eq 1 then writeu,unit1,_relerror
       if flag[7] eq 1 then writeu,unit1,_Max
       if flag[8] eq 1 then writeu,unit1,_TimeMax
       if flag[9] eq 1 then writeu,unit1,_MKtest
       if flag[10] eq 1 then writeu,unit1,_MKslope
       if flag[11] eq 1 then writeu,unit1,_SMKtest
       if flag[12] eq 1 then writeu,unit1,_SMKslope
       if flag[13] eq 1 then writeu,unit1,_MSMKtest
       if flag[14] eq 1 then writeu,unit1,_stdabw
       if flag[15] eq 1 then writeu,unit1,_mean
       if flag[16] eq 1 then writeu,unit1,_varkoeff
       if flag[17] eq 1 then writeu,unit1,_Status
       if flag[18] eq 1 then writeu,unit1,_skew
       if flag[19] eq 1 then writeu,unit1,_kurt
       if flag[20] eq 1 then writeu,unit1,_abbe
       if flag[21] eq 1 then writeu,unit1,_rho
       if flag[22] eq 1 then writeu,unit1,_tau
       if flag[23] eq 1 then writeu,unit1,_sigrho
       if flag[24] eq 1 then writeu,unit1,_sigtau
       if flag[25] eq 1 then writeu,unit1,_Min
       if flag[26] eq 1 then writeu,unit1,_Maxi
       if flag[27] eq 1 then writeu,unit1,_MinToMax
       if flag[28] eq 1 then writeu,unit1,_MaxToMin
       if flag[29] eq 1 then writeu,unit1,_Range
       if flag[30] eq 1 then for x=0, n_elements(_ADFkoeff[0,*])-1 do writeu,unit1,_ADFkoeff[*,x]
       ;if flag[30] eq 1 then writeu,unit1,_ADFkoeff
       if max(flagUR) eq 1 then begin                ; unrestricted regression parameters
          writeu,unit1,_olsUR.const
          if flagUR[0] eq 1 then writeu,unit1,_olsUR.b[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagUR[y] eq 1 then writeu,unit1,_olsUR.t2to5[y-1,*]

          for z=0,NoLags[0]-1 do writeu,unit1,_olsUR.b[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagUR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsUR.b[y+1,z,*]

          if flagUR[0] eq 1 then writeu,unit1,_olsUR.t[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagUR[y] eq 1 then writeu,unit1,_olsUR.t2to5T[y-1,*]

          for z=0,NoLags[0]-1 do writeu,unit1,_olsUR.t[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagUR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsUR.t[y+1,z,*]

          if flagUR[0] eq 1 then writeu,unit1,_olsUR.tsig[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagUR[y] eq 1 then writeu,unit1,_olsUR.t2to5Tsig[y-1,*]
          for z=0,NoLags[0]-1 do writeu,unit1,_olsUR.tsig[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagUR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsUR.tsig[y+1,z,*]

          if flagUR[0] eq 1 then writeu,unit1,_olsUR.r[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagUR[y] eq 1 then writeu,unit1,_olsUR.t2to5r[y-1,*]
          for z=0,NoLags[0]-1 do writeu,unit1,_olsUR.r[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagUR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsUR.r[y+1,z,*]

          writeu,unit1,_olsUR.F
          writeu,unit1,_olsUR.Fsig
          writeu,unit1,_olsUR.DW
          if quest.normalize[4] eq 1 then begin                      ; evtl. f�r alle Falle formulieren!!!
                    _olsUR.Yfit1=_olsUR.Yfit1 * stdvek +mittelvek
                    _olsUR.Yfitend=_olsUR.Yfitend * stdvek +mittelvek
          end
          writeu,unit1,_olsUR.Yfit1
          writeu,unit1,_olsUR.Yfitend
          _olsUR.relGrowth=(_olsUR.Yfitend / _olsUR.Yfit1 -1)*100 ; siehe Sachs, S. 151!!
          writeu,unit1,_olsUR.relGrowth
          _olsUR.absGrowth=_olsUR.Yfitend - _olsUR.Yfit1
          writeu,unit1,_olsUR.absGrowth
          writeu,unit1,_olsUR.Rmult
          if flagUR[10] eq 1 then begin
            for z=0, quest.NoHarmonics1-1 do begin
               writeu,unit1,_olsUR.FP[z,*] ; Cosinus Terme
               writeu,unit1,_olsUR.fpT[z,*] ; Cosinus Terme
               writeu,unit1,_olsUR.fpTsig[z,*] ; Cosinus Terme
            end
            for z=quest.NoHarmonics1, quest.NoHarmonics1*2-1 do begin
               writeu,unit1,_olsUR.FP[z,*] ; Sinus Terme
               writeu,unit1,_olsUR.fpT[z,*]
               writeu,unit1,_olsUR.fpTsig[z,*]
            end
            for z=0, quest.NoHarmonics2-1 do begin
               writeu,unit1,_olsUR.FP[z,*] ; Cosinus Terme
               writeu,unit1,_olsUR.fpT[z,*] ; Cosinus Terme
               writeu,unit1,_olsUR.fpTsig[z,*] ; Cosinus Terme
            end
            for z=quest.NoHarmonics2, quest.NoHarmonics2*2-1 do begin
               writeu,unit1,_olsUR.FP[z,*] ; Sinus Terme
               writeu,unit1,_olsUR.fpT[z,*]
               writeu,unit1,_olsUR.fpTsig[z,*]
            end
          end
          if n_elements(FromTo) gt 1 then $
          for z=0,n_elements(FromTo)-1 do writeu,unit1,_olsUR.ChangeFrTo[z]
       end

       if max(flagR) eq 1 then begin                ; unrestricted regression parameters
          writeu,unit1,_olsR.const
          if flagR[0] eq 1 then writeu,unit1,_olsR.b[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagR[y] eq 1 then writeu,unit1,_olsR.t2to5[y-1,*]
          for z=0,NoLags[0]-1 do writeu,unit1,_olsR.b[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsR.b[y+1,z,*]

          if flagR[0] eq 1 then writeu,unit1,_olsR.t[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagR[y] eq 1 then writeu,unit1,_olsR.t2to5T[y-1,*]
          for z=0,NoLags[0]-1 do writeu,unit1,_olsR.t[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsR.t[y+1,z,*]

          if flagR[0] eq 1 then writeu,unit1,_olsR.tsig[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagR[y] eq 1 then writeu,unit1,_olsR.t2to5Tsig[y-1,*]
          for z=0,NoLags[0]-1 do writeu,unit1,_olsR.tsig[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsR.tsig[y+1,z,*]

          if flagR[0] eq 1 then writeu,unit1,_olsR.r[0,0,*] ;time regression parameter
          for y=1,4 do $
           if flagR[y] eq 1 then writeu,unit1,_olsR.t2to5r[y-1,*]

          for z=0,NoLags[0]-1 do writeu,unit1,_olsR.r[1,z,*] ;possible y lags
          for y=1,5 do $
           if flagR[y+4] eq 1 then $                           ; external X, including lag=0
            for z=0,NoLags[y] do writeu,unit1,_olsR.r[y+1,z,*]


          writeu,unit1,_olsR.F
          writeu,unit1,_olsR.Fsig
          writeu,unit1,_olsR.DW
          if quest.normalize[4] eq 1 then begin
                    _olsR.Yfit1=_olsR.Yfit1 * stdvek +mittelvek
                    _olsR.Yfitend=_olsR.Yfitend * stdvek +mittelvek
          end
          writeu,unit1,_olsR.Yfit1
          writeu,unit1,_olsR.Yfitend
          _olsR.relGrowth=(_olsR.Yfitend / _olsR.Yfit1 -1)*100 ; siehe Sachs, S. 151!!
          writeu,unit1,_olsR.relGrowth
          _olsR.absGrowth=_olsR.Yfitend - _olsR.Yfit1
          writeu,unit1,_olsR.absGrowth
          writeu,unit1,_olsR.Rmult
          if flagR[10] eq 1 then begin
            for z=0, quest.NoHarmonics1-1 do begin
               writeu,unit1,_olsR.FP[z,*] ; Cosinus Terme
               writeu,unit1,_olsR.fpT[z,*] ; Cosinus Terme
               writeu,unit1,_olsR.fpTsig[z,*] ; Cosinus Terme
            end
            for z=quest.NoHarmonics1, quest.NoHarmonics1*2-1 do begin
               writeu,unit1,_olsR.FP[z,*] ; Sinus Terme
               writeu,unit1,_olsR.fpT[z,*]
               writeu,unit1,_olsR.fpTsig[z,*]
            end
            for z=0, quest.NoHarmonics2-1 do begin
               writeu,unit1,_olsR.FP[z,*] ; Cosinus Terme
               writeu,unit1,_olsR.fpT[z,*] ; Cosinus Terme
               writeu,unit1,_olsR.fpTsig[z,*] ; Cosinus Terme
            end
            for z=quest.NoHarmonics2, quest.NoHarmonics2*2-1 do begin
               writeu,unit1,_olsR.FP[z,*] ; Sinus Terme
               writeu,unit1,_olsR.fpT[z,*]
               writeu,unit1,_olsR.fpTsig[z,*]
            end

          end
          if n_elements(FromTo) gt 1 then $
          for z=0,n_elements(FromTo)-1 do writeu,unit1,_olsR.ChangeFrTo[z]
          writeu,unit1,_olsR.WaldF
          writeu,unit1,_olsR.Waldsig
       end
       if first eq 1 then begin
         if quest.resid ge 0 then begin
            case quest.outFormat of
             0:writeu,unit3,byte(pixelzeile) ; schreibe Originalwerte/Residuen/Yfit im BIL-Format raus
             1:writeu,unit3,fix(pixelzeile)
             2:writeu,unit3,float(pixelzeile)
            end
         end
         if thresh gt 0 then begin  ;ggf. Hotspots
           hszeile=hotspotdetection(pixelzeile,thresh,quest.lag,missvalue,mv)
           writeu,hsstack,hszeile ; schreibe hot-spot-Klasse weg
         endif
      endif
end