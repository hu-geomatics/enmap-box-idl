pro base_widgetPSD_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'group4': WIDGET_CONTROL, _nwind, GET_VALUE=nwind
    'group5': WIDGET_CONTROL, _noverlap, GET_VALUE=noverlap
    'group6': begin
               case ev2.value of
                0: windType='HANN'
                1: windType='HAMM'
                2: windType='Bartlett'
                3: windType='No'
               endcase
              end
    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widgetPSD, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
 values4 =['Hanning','Hamming', 'Bartlett','No']
 case windType of
  'HANN'    :wType=0
  'HAMM'    :wType=1
  'Bartlett':wType=2
  'No'      :wType=3
 endcase
 xy=  WIDGET_BASE(TITLE='Parameter for power spectral density estimation',/COLUMN)
 xy3= WIDGET_BASE(xy, TITLE='Parameter for Welch PSD estimation',/COLUMN)
 _nwind=   CW_FIELD(xy3,/INTEGER,/ALL_EVENTS,VALUE=nwind,TITLE='Length of moving window',UVALUE='group4')
 _noverlap=CW_FIELD(xy3,/INTEGER,/ALL_EVENTS,VALUE=noverlap,TITLE='No. overlapping points',UVALUE='group5')
 bgroup6= CW_BGROUP(xy3,values4,/ROW, SET_VALUE=wType,LABEL_TOP='Select window function',/FRAME,/EXCLUSIVE, UVALUE='group6')

 endbase = WIDGET_BASE(xy, /ROW,/ALIGN_BOTTOM)
 ok= widget_button(endbase, VALUE='done', UVALUE='done')

 WIDGET_CONTROL, xy, /NO_COPY,/REALIZE
 XMANAGER,'base_widgetXy', xy, GROUP=GROUP1
end