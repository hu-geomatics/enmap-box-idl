function MVC,x,freq
; computes MVC
; x=matrix cols*samples
; freq: e.g. 3
     samples=floor(n_elements(x[0,*])/freq)  ; restliche Werte abschneiden
     cols=floor(n_elements(x[*,0]))
     ; if (samples eq 0) then begin
     ;     message,'Not enough samples for averaging!!, exit...'
     ;     stop
     ; end

     xNeu=x[*,0:samples-1]
     if max(xNeu) ne min(xNeu) then begin
         for z=0,cols-1 do $
            for i=0,samples-1 do xNeu[z,i]=max(x[z,i*freq:i*freq+freq-1])
     end
     return,xNeu
end