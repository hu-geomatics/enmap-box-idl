FUNCTION dog, $ ;*************************************************** DOG
  m,scale,k,period,coi,dofmin,Cdelta,psi0

  IF (m EQ -1) THEN m = 2
  n = N_ELEMENTS(k)
  expnt = -(scale*k)^2/2d
  dt = 2d*!PI/(n*k(1))
  norm = SQRT(2*!PI*scale/dt)*SQRT(1d/GAMMA(m+0.5))
  I = DCOMPLEX(0,1)
  gauss = -norm*(I^m)*(scale*k)^m*EXP(expnt > (-100d))*(expnt GT -100)
  fourier_factor = 2*!PI*SQRT(2./(2*m+1))
  period = scale*fourier_factor
  coi = fourier_factor/SQRT(2)
  dofmin = 1   ; Degrees of freedom with no smoothing
  Cdelta = -1
  psi0 = -1
  IF (m EQ 2) THEN BEGIN
    Cdelta = 3.541 ; reconstruction factor
    psi0 = 0.867325
  ENDIF
  IF (m EQ 6) THEN BEGIN
    Cdelta = 1.966 ; reconstruction factor
    psi0 = 0.88406
  ENDIF
  ;   PRINT,scale,n,norm,SQRT(TOTAL(ABS(gauss)^2,/DOUBLE))*SQRT(n)
  RETURN,gauss
END