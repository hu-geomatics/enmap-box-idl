 function callveglength,x,nwind,quest
 ;   x: time-series

  metrics=     {   phase: 0.,     $     ; 0: Zeitreihe nicht differenzieren, 1: Zeitreihe differenzieren
                   amp: 0.,       $
                   length:0.,     $
                   prevStart: 0., $
                   nextStart: 0.   }

   x = reform(x)
   M = n_elements(x);
   noverlap=0
   if nwind eq -1 then nwind = 12


   ; Obtain the necessary information to segment X

   if nwind gt M then begin
      print,'The length of the segments cannot be larger than the length of the input signal. Using default = 12...'
      nwind=12 ; Assuming monthly values
   end

   ; Compute the number of segments
   k = fix((M-noverlap)/(nwind-noverlap));
   diff=fltarr(k)
   ; Compute differences in x for each segment
   xindx = 1
   index=indgen(nwind)
   ; filter the series


   x1=x
   ;for i = 0,k-1 do begin
   ;  temp=x1[index]
   ;  fourierPoly, temp, nwind, 0, 4, 0, fp, 0,/FIT
   ;  x1[index]=temp
   ;  index = index + nwind - noverlap
   ;end
   x1=smooth(x,quest.vegMA)

   ;if quest.vegMetrics eq 2 then begin
     fac1=0.5 * quest.vegMA - 1.5
     fac2=fac1+2
     b=[x1[0:fac1],x1[0:n_elements(x1)-fac2]]
     b=smooth(b,quest.vegMA)

     fac1=0.5 * quest.vegMA - 0.5
     fac2=fac1-1
     f=[x1[fac1:n_elements(x1)-1],x1[0:fac2]]
     f=smooth(f,quest.vegMA)
   ;end

   index=indgen(nwind)
   for i = 0,k-1 do begin
     if i lt k-1 then begin
       ; f�r vorauslaufenden MA ersten Schnittpunkt im nachfolgeden Jahr bestimmen. Dies ist
       ; evtl der Endpunkt der Veg. periode des laufenden Jahres.
       indexTEMP = index + nwind - noverlap
       d= x1[indexTEMP] - f[indexTEMP] ; zweiter Schnittpunkt nach Max
       temp=where(d le 0,count)
       if count gt 0 then metrics.nextStart = temp[0] else metrics.nextStart = 0
     end else metrics.nextStart=0

     if i gt 0 then begin
       ; f�r nachlaufenden MA letzten Schnittpunkt im vorausgehenderm Jahr bestimmen. Dies ist
       ; evtl der Startpunkt der Veg. periode des laufenden Jahres.
       d= x1[indexOLD] - b[indexOLD] ; zweiter Schnittpunkt nach Max
       temp=where(d le 0,count)
       ; letzten Schnittpunkt suchen
       if count gt 0 then metrics.prevStart=temp[n_elements(temp)-1] else metrics.prevStart=0
       ;metrics.prevStart=0
       ;if count gt 0 then begin
       ;   c=temp-shift(temp,1)
       ;   v=where(c gt 1,count2)
       ;   if count2 gt 0 then metrics.prevStart = v[n_elements(v)-1]
       ;end
     end

     VegLength,metrics,nwind,x1[index],quest,b[index],f[index]
     case quest.vegMetrics of
          0: diff[i]=metrics.phase
          1: diff[i]=metrics.amp
          2: diff[i]=metrics.length
      endcase
        indexOLD=index
        index = index + nwind - noverlap
   end
   return,diff
end