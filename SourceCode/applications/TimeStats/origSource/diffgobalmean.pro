function diffGobalMean, x, STD=STD
; calculates global (standardized) differences from the global mean
 for n=0, n_elements(x[*,0])-1 do begin
  x[n,*]=x[n,*]-mean(x[n,*])

  ;x[n,*]= 100. / mean(x[n,*]) * x[n,*] -100

  if keyword_set(STD) then x[n,*]=x[n,*]/stddev(x[n,*])
 endfor
 return,x
end