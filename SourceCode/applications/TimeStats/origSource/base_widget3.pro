pro base_widget3_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'group8': WIDGET_CONTROL, _IFFT_A, GET_VALUE=IFFT_A
    'group9': WIDGET_CONTROL, _IFFT_L, GET_VALUE=IFFT_L
    'group11':begin
                case ev2.value of
                  0: IFFT_reg= 1
                  1: IFFT_reg= 0
                endcase
              end
    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
  endcase
end


pro base_widget3, GROUP=GROUP1
 COMMON blk1
 COMMON IFFTblk, _IFFT_A, _IFFT_L
 COMMON blkbase

 values7=  ['yes','no']
 fourier2=  WIDGET_BASE(TITLE='IFFT parameters',/COLUMN)

 _IFFT_A=   CW_FIELD(fourier2,/FLOAT,/ALL_EVENTS,VALUE=IFFT_A,TITLE='Eliminate frequencies with power spectra values greater',UVALUE='group8')
 _IFFT_L=   CW_FIELD(fourier2,/INTEGER,/ALL_EVENTS,VALUE=IFFT_L,TITLE='Remove frequencies beginning at x/P (Enter x)',UVALUE='group9')
 bgroup8=   CW_BGROUP(fourier2,values7,/ROW, SET_VALUE=abs(1-IFFT_reg),LABEL_TOP='Use IFFT results for trend analysis ?',/FRAME,/EXCLUSIVE, UVALUE='group11')
  endbase = WIDGET_BASE(fourier2, /ROW,/ALIGN_BOTTOM)


  done= widget_button(endbase, VALUE='done', UVALUE='done')

  WIDGET_CONTROL, fourier2, /NO_COPY,/REALIZE
  XMANAGER,'base_widget3', fourier2, GROUP=GROUP1
end

