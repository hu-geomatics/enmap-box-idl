function SeasonMean,data,freq,mv
; saisonale Mittelwerte mit missing values
; beachte: Korrektur erfolgt nur bis Ende der letzten vollen Saison (Jahres) in der Zeitreihe!!
; data=float(data)
   data=reform(data)
   samples=n_elements(data)
   noOfCycles=floor(samples/freq)
   denom=fltarr(freq)

   for i=0,freq-1 do denom[i]=noOfCycles


   dataNeu=data[0:freq-1]*0. ; Array mit Anfangswerten f�r jeden Monat

   ; Monatsmittelwerte
   for n=0, noOfCycles-1 do begin
      dataTEMP=data[n*freq:n*freq+freq-1]
      temp=where(dataTEMP eq mv,count)
      if count gt 0 then begin
         dataTEMP[temp] = 0  ; MV Null setzen
         j = temp mod freq   ; welche Saison?
         denom[j]=denom[j] - 1
      end
      dataNeu=dataNeu+dataTEMP
   end
   dataNeu=dataNeu/denom

    result=dataNeu
   return,result
end
