function toeplitz,x,y,HERM=HERM

  if keyword_set(HERM) then begin
    x[1] = conj(x[1])
    y = x
    x = conj(x);  set up for Hermitian Toeplitz
  end

  r=n_elements(x)
  c=n_elements(y)
  toep=fltarr(c,r)
  col=0
  for row=0,r-1 do begin
    toep[col:c-1,row]=y[0:c-col-1] ; erste Zeile
    col=col+1
  end

  row=0
  for col=0,c-1 do begin
    toep[col,row:r-1]=x[0:c-row-1] ; erste Zeile
    row=row+1
  end
  return,toep
end