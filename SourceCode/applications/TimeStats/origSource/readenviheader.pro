pro readEnviHeader,EnviHeader,BOrder,cols,rows,bands,interleave,lan,mapInfo, wavelengthUnits
  searchStrings=['samples','lines','bands','data type','interleave','byte order','map','wavelength']
  zeile=''
  wavelengthUnits=''
  mapInfo=''
  for i=0,7 do begin
   openr,eh,EnviHeader,/get_lun
   while not eof(eh) do begin
     readf,eh,zeile ; Dateien �ffnen und die Header (nur bei LAN-Files) lesen
     if strpos(zeile,searchStrings[i]) ge 0 then begin
       result=strsplit(zeile,/REGEX,/EXTRACT)
       case i of
        0: cols=long(result[n_elements(result)-1])
        1: rows=long(result[n_elements(result)-1])
        2: bands=long(result[n_elements(result)-1])
        3: begin
             lan=fix(result[n_elements(result)-1])
             if lan eq 1 then lan=3 else $  ;intern code f�r byte
             if lan eq 4 then lan=0        ;intern code f�r float
           end
        4: interleave=result[n_elements(result)-1]
        5: bOrder=fix(result[n_elements(result)-1])
        6: mapInfo=zeile
        7: wavelengthUnits=zeile
       endcase
     endif
   endwhile
   free_lun,eh
  endfor

end