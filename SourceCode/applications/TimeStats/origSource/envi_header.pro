pro envi_header,out,mapInfo,wavelengthUnits,cols,rows,bands,first,DOS,lan,bname,BSQ=BSQ,BIP=BIP,CLASS=CLASS
 printf,out,'ENVI'
 printf,out,'description = {Regression}'
 printf,out,'samples = ', cols
 printf,out,'lines = ',rows
 printf,out,'bands = ', fix(bands)
 if mapInfo ne '' then printf,out,mapInfo
 if wavelengthUnits ne '' then printf,out,wavelengthUnits

 if KEYWORD_SET(CLASS) then printf,out,'header offset = 0'
 if KEYWORD_SET(CLASS) then printf,out,'file type = ENVI Classification' $
  else printf,out,'file type = ENVI Standard'

 if  KEYWORD_SET(CLASS) then printf,out,'data type = 1' else $
 if (first eq 0 or lan eq 0) then printf,out,'data type = 4' else $
 if lan eq 2 then printf,out,'data type = 2' else $
 if (lan eq 3) or (lan eq 1) then printf,out,'data type = 1'

 if KEYWORD_SET(BIP) then printf,out,'interleave = bip' else $
 if KEYWORD_SET(BSQ) then printf,out,'interleave = bsq' else printf,out,'interleave = bil'
 if lan eq 1 then printf,out,'sensor type = Landsat MSS/TM'
 if KEYWORD_SET(CLASS) then begin
    printf,out,'classes = 3'
    printf,out,'class lookup = { 0,   0,   0, 255,   0,   0,   0, 255,   0}'
    printf,out,'class names = {no change, pos change, neg change}'
 endif
 if DOS eq 0 then printf,out,'byte order = 1' else printf,out,'byte order = 0'
 if first eq 0 then printf,out,'band names = {'+bname+'}'
 if KEYWORD_SET(BIP) then printf,out,'band names = {AR1,AR2,AR3,AR4,AR5,AR6,AR7,AR8,AR9,AR10}'
end

