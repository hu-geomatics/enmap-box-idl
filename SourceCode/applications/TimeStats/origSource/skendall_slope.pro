function SKendall_slope, data_vec, freq, missvalue, mv
;seasonal kendall slope estimator for variable frequencies

slopes=fltarr(freq)
s_matrix=datasplit(data_vec,freq)
;calculate slopes for all seasons

for i=0,freq-1 do slopes[i] = Kendall_slope(s_matrix[i,*],missvalue,mv)
    slopes = slopes(SORT(slopes))
    res = median(slopes,/even)
    return, res
end
