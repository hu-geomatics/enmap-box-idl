pro checkEnviHeader, refcols, refrows, refbands, c, r, b, interleave, reflan,l
; checks ENVI Headers for multiple time series analysis
  Result = STRLOWCASE(interleave)
  if (b gt 1) and (interleave ne 'bil') then $
  begin
      info=dialog_message('Only BIL ENVI-Files are supported, exit...',/INFORMATION,TITLE='TimeStats')
      stop
  end
  if (c ne refcols) or (r ne refrows) or (b ne refbands) $
     then begin
       info=dialog_message('Second input file does not match, exit...',/INFORMATION,TITLE='TimeStats')
       stop
     end

  if N_PARAMS() gt 6 then if (l ne reflan) $
     then begin
       info=dialog_message('Data type of second input file does not match reference file, exit...',/INFORMATION,TITLE='TimeStats')
       stop
     end
end