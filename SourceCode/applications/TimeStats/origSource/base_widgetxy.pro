pro base_widgetXy_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'group1': begin
               case ev2.value of
                0: begin
                     xyfile=DIALOG_PICKFILE(/READ, FILTER = '*.bil', TITLE='Input file (Envi-BIL) for cross correlation')
                parts=STR_SEP(xyfile,'.')
               test=strcompress(parts[0]+'.hdr') ; pr�fen ob ein Headerfile vorhanden ist!!!
                  OPENR, 1, test, ERROR = err
                  if err eq 0 then begin  ; Eingabefile ist ein Batchfile
                    close,1
                    ;WIDGET_CONTROL, xyspec2, SENSITIVE=1
                  end else begin
                     info=dialog_message('No Envi-File!',/INFORMATION,TITLE='Time-series analysis')
                     xyfile=''
                  endelse
                   end
                endcase
                 if xyfile ne '' then begin
                   WIDGET_CONTROL, xy2, SENSITIVE=1
                   WIDGET_CONTROL, xy3, SENSITIVE=1
                 end
                end

    'group2': x2y=ev2.value
    'group3': begin
                 if flagxy[ev2.value] eq 0 then flagxy[ev2.value]=1 else flagxy[ev2.value]=0
               end
    'group4': WIDGET_CONTROL, _nwind, GET_VALUE=nwind
    'group5': WIDGET_CONTROL, _noverlap, GET_VALUE=noverlap
    'group6': begin
               case ev2.value of
                0: windType='HANN'
                1: windType='HAMM'
                2: windType='BARTLETT'
                3: windType='NO'
               endcase
              end
     'groupX': case ev2.value of
                 0: quest.Xprocessing=0
                 1: quest.Xprocessing=1
               endcase
    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widgetXy, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
 values0 = ['no','yes']
 values1 = ['ENVI input file for spectral cross correlation']
 values2 = ['FFT (2. time series)','Cross spectrum','Cospectrum','Quadrature spectrum','Phase spectrum', $
         'Ampitude spectrum','Coherence spectum','Gain spectrum']
 values3 =['Input 1 --> Input 2','Input 1 <-- Input 2']
 values4 =['Hanning','Hamming', 'Bartlett','No']

 case windType of
  'HANN'    :wType=0
  'HAMM'    :wType=1
  'BARTLETT':wType=2
  'NO'      :wType=3
 endcase

 xy=  WIDGET_BASE(TITLE='Parameter for cross correlation',/COLUMN)
 bgroup1= CW_BGROUP(xy,values1,/COLUMN, LABEL_TOP='2. input file for cross correlation',/FRAME,UVALUE='group1')


 bgroup2= CW_BGROUP(xy,values3,/ROW, SET_VALUE=x2y,LABEL_TOP='Direction for Cross spectra?',/FRAME,/EXCLUSIVE, UVALUE='group2')
 bgroupX  = CW_BGROUP(xy,values0,/ROW,SET_VALUE=quest.Xprocessing, $
                        LABEL_TOP='Pre-process X as defined for Y ?',/FRAME, /EXCLUSIVE,UVALUE='groupX')
 xy2= WIDGET_BASE(xy, /COLUMN)
 bgroup3= CW_BGROUP(xy2,values2,COLUMN=2, $
         LABEL_TOP='Parameter for cross correlation',/FRAME, /NONEXCLUSIVE,UVALUE='group3')

 xy3= WIDGET_BASE(xy, /COLUMN)
 _nwind=   CW_FIELD(xy3,/INTEGER,/ALL_EVENTS,VALUE=nwind,TITLE='Length of spectral window',UVALUE='group4')
 _noverlap=CW_FIELD(xy3,/INTEGER,/ALL_EVENTS,VALUE=noverlap,TITLE='Overlapping points',UVALUE='group5')
 bgroup6= CW_BGROUP(xy3,values4,/ROW, SET_VALUE=wType,LABEL_TOP='Select window function',/FRAME,/EXCLUSIVE, UVALUE='group6')

 endbase = WIDGET_BASE(xy, /ROW,/ALIGN_BOTTOM)
 ok= widget_button(endbase, VALUE='done', UVALUE='done')

 if xyfile eq '' then begin
     WIDGET_CONTROL, xy2, SENSITIVE=0
     WIDGET_CONTROL, xy3, SENSITIVE=0
 end

 WIDGET_CONTROL, xy, /NO_COPY,/REALIZE
 XMANAGER,'base_widgetXy', xy, GROUP=GROUP1
end