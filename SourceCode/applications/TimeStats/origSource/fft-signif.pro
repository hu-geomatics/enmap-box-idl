FUNCTION fft_signif,y,dt,siglvl,LAG1=lag1

    period=reverse(findgen(N_ELEMENTS(y))+1)
    period=N_ELEMENTS(y)/period  ; Fourierperioden

    IF (N_ELEMENTS(y) EQ 1) THEN variance=y ELSE variance=(MOMENT(y))(1)
    lag1 = lag1(0)

    dofmin = 2 ; Degrees of freedom with no smoothing

;....significance levels [Sec.4]
    freq = dt/period  ; normalized frequency
    fft_theor = (1-lag1^2)/(1-2*lag1*COS(freq*2*!PI)+lag1^2)  ; [Eqn(16)]
    fft_theor = variance*fft_theor  ; include time-series variance
    signif = fft_theor

    dof = dofmin
    signif = fft_theor*CHISQR_CVF(1. - siglvl,dof)/dof   ; [Eqn(18)]
;       IF confidence THEN BEGIN
;         sig = (1. - siglvl)/2.
;         chisqr = dof/[CHISQR_CVF(sig,dof),CHISQR_CVF(1.-sig,dof)]
;         signif = fft_theor # chisqr
;       ENDIF
return,signif
END