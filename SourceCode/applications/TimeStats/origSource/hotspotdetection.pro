function hotspotdetection, zeile, thresh, lag, missvalue,mv
  hszeile=bytarr(n_elements(zeile[*,0]),n_elements(zeile[0,*]))
  for x=0,lag-1 do hszeile[*,x]=0  ; mit 0 am Anfang auffüllen
  for x=lag,n_elements(zeile[0,*])-1 do begin
    pos=where((zeile[*,x] ne 0) and ((100/(zeile[*,x]+0.000001) * zeile[*,x-lag]) le (100 - thresh)) )  ; pos. Veränderung
    if max(pos) gt  -1 then hszeile[pos,x]=1 ;Klassencodierung
    neg=where((zeile[*,x] ne 0) and ((100/(zeile[*,x]+0.000001) * zeile[*,x-lag]) ge (100 + thresh)) )  ; neg. Veränderung
    if max(neg) gt  -1 then hszeile[neg,x]=2
      ; evtl. Bereinigung von Missing values
      if missvalue eq 1 then begin
        temp=where(round(zeile[*,x] - mv) eq 0, count)
        if count gt 0 then begin
                      hszeile[temp,x]=0
                     ; hszeile[temp,x-lag]=0
                     end
        temp=where(round(zeile[*,x-lag]- mv) eq 0, count)
        if count gt 0 then hszeile[temp,x]=0
      endif
   endfor
  return, hszeile
end
