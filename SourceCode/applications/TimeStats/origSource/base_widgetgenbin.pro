pro base_widgetGenBin_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'group0_': begin
                case ev2.value of
                 0: begin ; GB float
                    lan=0
                    lan3=0  ; f�r GUI Kontrolle
                   end
                 1: begin
                    lan=2 ;GB integer
                    lan3=1  ; f�r GUI Kontrolle
                   end
                 2: begin
                    lan=3 ;GB byte
                    lan3=2  ; f�r GUI Kontrolle
                   end
                endcase
                end
    'group0__':begin
                case ev2.value of
                 0:  BOrder=0 ; INTEL
                 1:  BOrder=1 ; MOTOROLA
                endcase
                end
    'group01':   WIDGET_CONTROL, _cols, GET_VALUE=cols
    'group02':   WIDGET_CONTROL, _rows, GET_VALUE=rows
    'group03':   WIDGET_CONTROL, _imagebands, GET_VALUE=imagebands
    'group5':    WIDGET_CONTROL, _band, GET_VALUE=band

    'ok'  :    WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end

pro base_widgetGenBin, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase

  values0_= ['float','integer','byte']
  values0__=['Intel','Motorola']

  sizebase = WIDGET_BASE(TITLE='Generic binary file options',/COLUMN)
  bgroup0_= CW_BGROUP(sizebase,values0_,/ROW,SET_VALUE=lan3,LABEL_TOP='Define generic binary format', $
                    /FRAME,/EXCLUSIVE,UVALUE='group0_')
  sizebase2 = WIDGET_BASE(sizebase, /row)
  _cols=CW_FIELD(sizebase2,/INTEGER,/ALL_EVENTS,  YSIZE=70, VALUE=cols,TITLE='cols',UVALUE='group01')
  _rows=CW_FIELD(sizebase2,/INTEGER,/ALL_EVENTS,VALUE=rows,TITLE='rows',UVALUE='group02')
  _imagebands=CW_FIELD(sizebase2,/INTEGER,/ALL_EVENTS,VALUE=imagebands,TITLE='bands',UVALUE='group03')
  _BOrder= CW_BGROUP(sizebase,values0__,/ROW,SET_VALUE=BOrder,LABEL_TOP='byte order',/FRAME,/EXCLUSIVE,UVALUE='group0__')
  sizebase3 = WIDGET_BASE(sizebase, /row)
  _band=   CW_FIELD(sizebase3,/INTEGER,/ALL_EVENTS,VALUE=band, $
           TITLE='Band number to be processed',UVALUE='group5')
  endbase = WIDGET_BASE(sizebase, /ROW,/ALIGN_BOTTOM)
  ok= widget_button(endbase, VALUE='ok', UVALUE='ok')

  WIDGET_CONTROL, sizebase, /NO_COPY,/REALIZE
  XMANAGER,'base_widgetGenBin', sizebase, GROUP=GROUP1
end

