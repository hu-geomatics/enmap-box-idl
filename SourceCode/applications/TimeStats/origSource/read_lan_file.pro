function read_lan_file, unit, col, band,dos,BOrder,lan,ALL=all
  readu,unit,col                                 ; einlesen einer Zeile �ber alle B�nder
  if keyword_set(all) then row=col else row=col[*,band]
  if (BOrder eq 1) and (lan ne 1) then row=swap_endian(row)
  if dos eq 0 then  row=swap_endian(row) ; f�r SUNs
  return,row
end
