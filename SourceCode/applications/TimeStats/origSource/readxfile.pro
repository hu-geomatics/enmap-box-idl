function readXFile,pixelzeile,unit,pzbuff, band, dos, BOrder, lan,missvalue, $
                   outlier,icols,zahl,mv,olcorrmeth, olstddev, olthresh, recFrom, recTo
     ; helper for reading additional files for regression
     Xpixelzeile=pixelzeile
     Xpixelzeile=read_lan_file(unit, pzbuff, band, dos, BOrder, lan, ALL=1)

     if (missvalue gt 1) or (outlier gt 0) then $
        Xpixelzeile=mvoutlier(Xpixelzeile,icols,zahl,mv,missvalue,outlier,olcorrmeth, olstddev, olthresh)

        temp=Xpixelzeile[*,recFrom-1:recTo-1]
        Xpixelzeile=temp

     return,Xpixelzeile
end