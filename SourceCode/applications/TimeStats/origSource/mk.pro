function mk, data_vec, missvalue, mv,sig
; Mann-Kendall-Test (Autor: Tim Erbrecht)
;original tie-corrected mann-kendall test for monotonic trends
;this test does not adjust for serial dependance and seasonality

; berechnen Sig.niveau: TU, 29.06.05
; sig =0: R�ckgabe emp. z-score, 1: R�ckgabe 2 seitiges SigNiveau
sgn = signum_(data_vec, missvalue, mv)
variance = var(data_vec, missvalue, mv)
sdev = sqrt(variance)

    ;continuity correction of overall test value
    case 1 of
       sgn ge 0: z=float(sgn-1)/sdev
       sgn eq 0: z=0
       sgn le 0: z=float(sgn+1)/sdev
    endcase

if sig eq 1 then begin
   temp=min([1 - gauss_PDF(z), gauss_PDF(z)])*2
   if z lt 0 then z= temp * (-1) else z=temp
end
return, z
end

