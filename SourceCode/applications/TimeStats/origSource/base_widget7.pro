pro base_widget7_event, ev2, GROUP=group
 COMMON blk1
 ;COMMON blk2
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
  'group4c': begin
            case ev2.value of
              0: begin
                   integrals= 0
                   WIDGET_CONTROL, sizebase2, SENSITIVE=0
                 end
              1: begin
                   integrals= 1
                   WIDGET_CONTROL, sizebase2, SENSITIVE=1
                 end
            endcase
           end

 'groupneu': WIDGET_CONTROL, _freq, GET_VALUE=freq
 'groupFrom':WIDGET_CONTROL, _from, GET_VALUE=recFrom
 'groupTo':  WIDGET_CONTROL, _to,   GET_VALUE=recTo
 'group_hs1': WIDGET_CONTROL, _thresh, GET_VALUE=thresh
 'group_hs2': WIDGET_CONTROL, _lag, GET_VALUE=lag

 'FPpara'  : base_widget3b
 'groupa':   case ev2.value of
                  0: begin
                       quest.normalize=0;no normalization
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  1: begin
                       quest.normalize=1; differnces from total mean
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  2: begin
                       quest.normalize=2; differnces from total mean/stddev
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  3: begin
                       quest.normalize=3; long-term seasonal differences
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  4: begin
                       quest.normalize=4; long-term seasonal differences/stddev
                       WIDGET_CONTROL, sizebase1, SENSITIVE=1
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  5: begin
                       quest.normalize=5; autoscale
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=0
                     end
                  6: begin
                       quest.normalize=6; FP
                       WIDGET_CONTROL, sizebase1, SENSITIVE=0
                       WIDGET_CONTROL, FPp, SENSITIVE=1
                     end

                end

;'group1':   case ev2.value of
;               0: olcorrmeth=0  ; average correction
;               1: olcorrmeth=1  ; lin interpolation
;            endcase

;'group': begin
;             case ev2.value of
;               0: begin  ; no outliers
;                    WIDGET_CONTROL, olcorr1, SENSITIVE=0
;                    WIDGET_CONTROL, olcorr2, SENSITIVE=0
;                    WIDGET_CONTROL, olcorr3, SENSITIVE=0
;                    outlier = 0
;                  end
;               1: begin  ;outliers, detection with std.dev
;                WIDGET_CONTROL, olcorr1, SENSITIVE=1
;                WIDGET_CONTROL, olcorr2, SENSITIVE=0
;                WIDGET_CONTROL, olcorr3, SENSITIVE=1
;                    outlier = 1
;                  end
;               2: begin ; outliers, detection with hotspots
;                    WIDGET_CONTROL, olcorr1, SENSITIVE=0
;                    WIDGET_CONTROL, olcorr2, SENSITIVE=1
;                    WIDGET_CONTROL, olcorr3, SENSITIVE=1
;                    outlier=2
;                  end
;             endcase
;            end
 'groupb':   WIDGET_CONTROL, _normFreq, GET_VALUE=normFreq
 'ok'  :    WIDGET_CONTROL, ev2.top,/DESTROY
 endcase
end


pro base_widget7, GROUP=GROUP1
 COMMON blk1
; COMMON blk2
 COMMON blkbase
  values1a = ['no standardization','diff. from total average','diff. from total average/stddev', $
             'diff. from  seasonal average (e.g. month)','diff. from  seasonal average (e.g. month)/seasonal stddev','autoscale','Removal of cycles with Fourier P.']
  values2 = ['no','yes']
  values5d= ['Fourier polynomial parameter']


;  values=['no outliers','outlier detection. --> std.dev','outlier detection--> hotspots']
;  values1=['outlier corr. --> average','outlier corr. --> lin. interpolation']
;  values4b =['no MV','no MV correction','MV --> average','MV --> lin. interpolation']
  mvout=  WIDGET_BASE(TITLE='Data pre-processing',/COLUMN)


  bgroup1= CW_BGROUP(mvout,values1a,/COLUMN,SET_VALUE=quest.normalize,LABEL_TOP='Normalization',/FRAME,/EXCLUSIVE,UVALUE='groupa')
  sizebase1 = WIDGET_BASE(mvout, /row)
  _normFreq=   CW_FIELD(sizebase1,/INTEGER,/RETURN_EVENTS,VALUE=normFreq, $
           TITLE='Frequence used for seasonal normalization (Confirm each change with RETURN!)',UVALUE='groupb')


 ipbase=WIDGET_BASE(mvout, /COLUMN)
 bgroup4c=CW_BGROUP(ipbase,values2,/ROW, SET_VALUE=integrals,LABEL_TOP='Saisonal averaging',/FRAME,/EXCLUSIVE, UVALUE='group4c')
 ;bgroup4= CW_BGROUP(ipbase,values4,/ROW, SET_VALUE=1,LABEL_TOP='exclude Missing Values?',/FRAME,/EXCLUSIVE, UVALUE='group4')

 sizebase2 = WIDGET_BASE(mvout, /row)
 _freq=   CW_FIELD(sizebase2,/INTEGER,/RETURN_EVENTS,VALUE=freq,TITLE='Saisonal frequency (Confirm each change with RETURN!)',UVALUE='groupneu')

  FPp=WIDGET_BASE(mvout, /COLUMN)
  bgroupFP=CW_BGROUP(FPp,values5d,/FRAME,UVALUE='FPpara')

   hsbase=WIDGET_BASE(mvout, /column,/frame)
  _thresh=CW_FIELD(hsbase,/FLOAT,/RETURN_EVENTS,VALUE=thresh, $
         TITLE='Threshold (%) for difference images',UVALUE='group_hs1')
  _lag=CW_FIELD(hsbase,/INTEGER,/RETURN_EVENTS,VALUE=lag,TITLE='Choose lag                                                ',UVALUE='group_hs2')

 sizebase3 = WIDGET_BASE(mvout, /ROW)
 _from=   CW_FIELD(sizebase3,/INTEGER,/RETURN_EVENTS,VALUE=recFrom,TITLE='Use record no. "from"',UVALUE='groupFrom')
 _to  =   CW_FIELD(sizebase3,/INTEGER,/RETURN_EVENTS,VALUE=recTo,TITLE='to record no. "to"',UVALUE='groupTo')

;  _mv    = CW_FIELD(mvout,/FLOAT,/RETURN_EVENTS,VALUE=mv,TITLE='Missing Value code (Confirm with RETURN!)',UVALUE='groupmv')
;  bgroup4b= CW_BGROUP(mvout,values4b,/ROW, SET_VALUE=missvalue,LABEL_TOP='interpol. method for Missing Values',/FRAME,/EXCLUSIVE, UVALUE='group4b')
;  bgroup= CW_BGROUP(mvout,values,/ROW, SET_VALUE=outlier,LABEL_TOP='Outlier correction',/FRAME,/EXCLUSIVE, UVALUE='group')
;  olcorr1 = WIDGET_BASE(mvout, /COLUMN) ; f�r Std.dev
;  _stddev= CW_FIELD(olcorr1,/FLOAT,/RETURN_EVENTS,VALUE=olstddev              ,TITLE='Std. dev (Confirm with RETURN!)',UVALUE='group1b')
;  olcorr2 = WIDGET_BASE(mvout, /COLUMN) ; f�r Hotspots
;  _hs= CW_FIELD(olcorr2,/FLOAT,/RETURN_EVENTS,VALUE=olthresh,TITLE='Threshold for hotspots (Confirm with RETURN!)',UVALUE='group1c')
;  olcorr3 = WIDGET_BASE(mvout, /COLUMN) ; f�r Hotspots
;  bgroup1= CW_BGROUP(olcorr3,values1,/ROW, SET_VALUE=olcorrmeth,LABEL_TOP='Outlier correction method',/FRAME,/EXCLUSIVE, UVALUE='group1')
  endbase = WIDGET_BASE(mvout, /ROW,/ALIGN_BOTTOM)
  ok = widget_button(endbase,VALUE='  ok  ', UVALUE='ok')


;  WIDGET_CONTROL, mvout, /NO_COPY,/REALIZE
;  WIDGET_CONTROL, olcorr1, /NO_COPY,/REALIZE
;  WIDGET_CONTROL, olcorr2, /NO_COPY,/REALIZE
;  WIDGET_CONTROL, olcorr3, /NO_COPY,/REALIZE
  WIDGET_CONTROL, sizebase1, /NO_COPY,/REALIZE
  WIDGET_CONTROL, sizebase2, /NO_COPY,/REALIZE
  WIDGET_CONTROL, FPp, /NO_COPY,/REALIZE
;  case outlier of
;     0: begin  ; no outliers
;          WIDGET_CONTROL, olcorr1, SENSITIVE=0
;          WIDGET_CONTROL, olcorr2, SENSITIVE=0
;          WIDGET_CONTROL, olcorr3, SENSITIVE=0
;        end
;     1: begin  ;outliers, detection with std.dev
;          WIDGET_CONTROL, olcorr1, SENSITIVE=1
;          WIDGET_CONTROL, olcorr2, SENSITIVE=0
;          WIDGET_CONTROL, olcorr3, SENSITIVE=1
;        end
;     2: begin ; outliers, detection with hotspots
;          WIDGET_CONTROL, olcorr1, SENSITIVE=0
;          WIDGET_CONTROL, olcorr2, SENSITIVE=1
;          WIDGET_CONTROL, olcorr3, SENSITIVE=1
;        end
;  endcase

  case quest.normalize of
     0:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     1:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     2:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     3:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=1
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     4:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=1
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     5:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=0
       end
     6:begin
        WIDGET_CONTROL, sizebase1, SENSITIVE=0
        WIDGET_CONTROL, FPp, SENSITIVE=1
       end
  end

  if integrals eq 1 then WIDGET_CONTROL, sizebase2, SENSITIVE=1 $
    else WIDGET_CONTROL, sizebase2, SENSITIVE=0
  ;WIDGET_CONTROL, sizebase1, SENSITIVE=0
  XMANAGER,'base_widget7', mvout, GROUP=GROUP1
end
