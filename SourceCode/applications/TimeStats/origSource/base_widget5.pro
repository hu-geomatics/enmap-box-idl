pro base_widget5_event, ev2, GROUP=group
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'group7': begin
                case ev2.value of
                0: begin
                     IFFT= 0  ; FFT
                     IFFT2=0  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=1
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                   end
                1: begin
                     IFFT= 1  ; PSD
                     IFFT2=1  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=1
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                     end
;                2: begin
;                     IFFT= 5  ; Fourier-Polynom
;                     IFFT2=2  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
;                     WIDGET_CONTROL, FFTp, SENSITIVE=0
;                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
;                     WIDGET_CONTROL, wavel, SENSITIVE=0
;                     WIDGET_CONTROL, FPp, SENSITIVE=1
;                     WIDGET_CONTROL, xyspec, SENSITIVE=0
;                     end
                2: begin
                     IFFT= 2  ; Wavelet-Analyse
                     IFFT2=2  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=1
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                  end

                3: begin
                     IFFT= 3  ; ACF
                     IFFT2=3  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                  end

                4: begin; Kreuzspektralanalyse
                     IFFT= 4  ; Cross correlation
                     IFFT2=4  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     WIDGET_CONTROL, vegpar, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=1
                   end
                5: begin
                     IFFT= 5  ; FFT-Phase
                     IFFT2=5  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                     WIDGET_CONTROL, vegpar, SENSITIVE=0
                   end
                6: begin
                     IFFT= 6  ; WFFT
                     IFFT2=6  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=1
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                     WIDGET_CONTROL, vegpar, SENSITIVE=0
                   end
                7: begin
                     IFFT= 7  ; Veg.parameter
                     IFFT2=7  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                     WIDGET_CONTROL, vegpar, SENSITIVE=1
                  end
                8: begin
                     IFFT= 8  ; nichts
                     IFFT2=8  ; wird nur zur Steuerung des GUI ben�tigt, irgendwann �ndern!
                     WIDGET_CONTROL, WFFT, SENSITIVE=0
                     WIDGET_CONTROL, FFTp, SENSITIVE=0
                     WIDGET_CONTROL, IFFTp, SENSITIVE=0
                     ;WIDGET_CONTROL, FPp, SENSITIVE=0
                     WIDGET_CONTROL, wavel, SENSITIVE=0
                     WIDGET_CONTROL, xyspec, SENSITIVE=0
                     WIDGET_CONTROL, vegpar, SENSITIVE=0
                  end

                endcase
            end
    'WLpara'   : base_widget2
    'FFTpara'  : base_widgetFFT
    'WFFT_Para': base_widgetWFFT
    'PSDpara'  :  base_widgetPSD
;    'FPpara'  : base_widget3b
    'XYpara'   : base_widgetXy
    'VegPara'  : base_widgetVeg


;    'group10': begin
;                para[4]=1
;                if ev2.value eq 0 then $
;                     fourierfile=DIALOG_PICKFILE(/WRITE, FILTER = '*.bil', TITLE='Output file for Power spectrum (optional)')
;               end

    'ok'  : WIDGET_CONTROL, ev2.top,/DESTROY
  endcase
end


pro base_widget5, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase

  values4 = ['no','yes']
  values5 = ['Periodogram','PSD','CWA','ACF','Cross spectra','Phase','Windowed FFT','Veg.parameter','No']
  values5a= ['Periodogram parameter']
  values5b= ['Wavelet (CWA) parameter']
  values5c= ['PSD parameter']
  values5d= ['Fourier polynomial parameter']
  values5e= ['Cross correlation parameter']
  values5f= ['Windowed FFT parameter']
  values5g= ['Vegetation growth cycle parameter']
 ; values6 = ['File name for FFT/IFFT/ACF/Wavelet-coefficients']

  FFTpar=  WIDGET_BASE(TITLE='Extraction of cyclic component parameters',/COLUMN)
  fourier = WIDGET_BASE(FFTpar, /COLUMN)
  bgroup6= CW_BGROUP(fourier,values5,/ROW,SET_VALUE=IFFT2,/FRAME,/EXCLUSIVE, UVALUE='group7')

  FFTp=WIDGET_BASE(fourier, /COLUMN)
  bgroup6a=CW_BGROUP(FFTp,values5a,/FRAME,UVALUE='FFTpara')

  wavel=WIDGET_BASE(fourier, /COLUMN)
  bgroup6b=CW_BGROUP(wavel,values5b,/FRAME,UVALUE='WLpara')

  IFFTp=WIDGET_BASE(fourier, /COLUMN)
  bgroup6d=CW_BGROUP(IFFTp,values5c,/FRAME,UVALUE='PSDpara')  ; vormals IFFT, jetzt PSD!!

  ;FPp=WIDGET_BASE(fourier, /COLUMN)
  ;bgroup6c=CW_BGROUP(FPp,values5d,/FRAME,UVALUE='FPpara')

  xyspec=WIDGET_BASE(fourier, /COLUMN)
  bgroup6e=CW_BGROUP(xyspec,values5e,/FRAME,UVALUE='XYpara')

  WFFT=WIDGET_BASE(fourier, /COLUMN)
  bgroup6f=CW_BGROUP(WFFT,values5f,/FRAME,UVALUE='WFFT_Para')

  vegpar=WIDGET_BASE(fourier, /COLUMN)
  bgroup6g=CW_BGROUP(vegpar,values5g,/FRAME,UVALUE='VegPara')


  ;bgroup7= CW_BGROUP(fourier,values6,/COLUMN, LABEL_TOP='Output file',/FRAME,UVALUE='group10')

  endbase = WIDGET_BASE(FFTpar, /ROW,/ALIGN_BOTTOM)
  ok= widget_button(endbase, VALUE='ok', UVALUE='ok')
  WIDGET_CONTROL, FFTpar, /NO_COPY,/REALIZE

  WIDGET_CONTROL, fourier, SENSITIVE=1


  case IFFT2 of
    0: begin
         WIDGET_CONTROL, FFTp, SENSITIVE=1
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    1: begin
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=1
         WIDGET_CONTROL, wavel, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    ;2: begin
    ;     WIDGET_CONTROL, FFTp, SENSITIVE=0
    ;     WIDGET_CONTROL, IFFTp, SENSITIVE=0
    ;     WIDGET_CONTROL, wavel, SENSITIVE=0
    ;     WIDGET_CONTROL, FPp, SENSITIVE=1
    ;     WIDGET_CONTROL, xyspec, SENSITIVE=0
    ;   end
    2: begin
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=1
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    3: begin
         WIDGET_CONTROL, WFFT, SENSITIVE=0
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    4: begin
         WIDGET_CONTROL, WFFT, SENSITIVE=0
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=1
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    5: begin
         WIDGET_CONTROL, WFFT, SENSITIVE=0
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    6: begin
         WIDGET_CONTROL, WFFT, SENSITIVE=1
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
    7: begin ; Startwert
         WIDGET_CONTROL, WFFT, SENSITIVE=0
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=1
       end
    8: begin ; Startwert
         WIDGET_CONTROL, WFFT, SENSITIVE=0
         WIDGET_CONTROL, FFTp, SENSITIVE=0
         WIDGET_CONTROL, IFFTp, SENSITIVE=0
         WIDGET_CONTROL, wavel, SENSITIVE=0
         ;WIDGET_CONTROL, FPp, SENSITIVE=0
         WIDGET_CONTROL, xyspec, SENSITIVE=0
         WIDGET_CONTROL, vegpar, SENSITIVE=0
       end
  endcase
  XMANAGER,'base_widget5', FFTpar, GROUP=GROUP1
end