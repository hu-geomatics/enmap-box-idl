pro base_widgetVeg_event, ev2, GROUP=group
; Dialog f�r Vegetationsparameter
 COMMON blk1
 COMMON blkbase

 widget_control, ev2.id, get_uvalue=uval
 case uval of
    'groupVeg':  begin
                  WIDGET_CONTROL, _vegMA, GET_VALUE=vegMA
                  quest.vegMA=vegMA
                 end
    'WFFTnowind':begin
                  WIDGET_CONTROL, _WFFTnwindDiff, GET_VALUE=WFFTnwindDiff
                  WFFTnoverlapDiff=0
                 end
    'groupSel': case ev2.value of
                 0: quest.vegMetrics=0 ; Phase
                 1: quest.vegMetrics=1 ; Mag
                 2: quest.vegMetrics=2 ; Lenght
                endcase

    'done'  : WIDGET_CONTROL, ev2.top,/DESTROY
  endcase
end


pro base_widgetVeg, GROUP=GROUP1
 COMMON blk1
 COMMON blkbase
 values= ['Peaking time','Magnitude','Lenght vegetation growth cycle']
 values= ['Peaking time','Magnitude']

 veg=  WIDGET_BASE(TITLE='Parameters for vegetation metrics',/COLUMN)
 _vegMA=   CW_FIELD(veg,/INTEGER,/ALL_EVENTS,VALUE=quest.vegMA,TITLE='Length of moving average filter',UVALUE='groupVeg')

 _WFFTnwindDiff=   CW_FIELD(veg,/INTEGER,/ALL_EVENTS,VALUE=WFFTnwindDiff, $
 TITLE='Length of vegetation period',UVALUE='WFFTnowind')


 _vecSel= CW_BGROUP(veg,values,/ROW,SET_VALUE=quest.vegMetrics,LABEL_TOP='Select metric', $
                    /FRAME,/EXCLUSIVE,UVALUE='groupSel')

 endbase = WIDGET_BASE(veg, /ROW,/ALIGN_BOTTOM)
 done= widget_button(endbase, VALUE='done', UVALUE='done')

  WIDGET_CONTROL, veg, /NO_COPY,/REALIZE
  XMANAGER,'base_widgetVeg', veg, GROUP=GROUP1
end

