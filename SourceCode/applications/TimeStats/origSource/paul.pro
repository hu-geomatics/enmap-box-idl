FUNCTION paul, $ ;************************************************** PAUL
  m,scale,k,period,coi,dofmin,Cdelta,psi0

  IF (m EQ -1) THEN m = 4d
  n = N_ELEMENTS(k)
  expnt = -(scale*k)*(k GT 0.)
  dt = 2d*!PI/(n*k(1))
  norm = SQRT(2*!PI*scale/dt)*(2^m/SQRT(m*FACTORIAL(2*m-1)))
  paul = norm*((scale*k)^m)*EXP(expnt > (-100d))*(expnt GT -100)
  paul = paul*(k GT 0.)
  fourier_factor = 4*!PI/(2*m+1)
  period = scale*fourier_factor
  coi = fourier_factor*SQRT(2)
  dofmin = 2   ; Degrees of freedom with no smoothing
  Cdelta = -1
  IF (m EQ 4) THEN Cdelta = 1.132 ; reconstruction factor
  psi0 = 2.^m*FACTORIAL(m)/SQRT(!PI*FACTORIAL(2*m))
  ;   PRINT,scale,n,norm,SQRT(TOTAL(paul^2,/DOUBLE))*SQRT(n)
  RETURN,paul
END