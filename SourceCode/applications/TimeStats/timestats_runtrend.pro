pro TimeStats_runTrend, GroupLeader=groupLeader, Title=title
  tlb = widget_base(GROUP_LEADER=groupLeader, TITLE=title)
  textwid = widget_text(tlb, XSIZE=50, YSIZE=1)
  hubGUIHelper.centerTLB, tlb
  widget_control, tlb, /REALIZE
  trend,textwid
  widget_control, tlb, /DESTROY
end