;pro TimeStats_initCommonBlocks
;  
  COMMON blk1,workDir,para,flag,band, thresh,lag,hotspots,batch, integrals, normFreq, $
    ofile_ori,file,ofile,cols,rows,imagebands,lan,lan2,lan3, four_koeff,four_koeff_temp,fourierfile, $
    IFFT,IFFT2,IFFT_temp,IFFT_A,IFFT_L, nrPSmooth, mapinfo,wavelengthUnits, $
    nwind,windType,noverlap, nwindDiff, noverlapDiff,WFFTnwindDiff, WFFTnoverlapDiff, recFrom, recTo, $
    IFFT_reg,missvalue,mv,mvSais,BOrder, MorletP, PaulP,DOGP,dos, $
    Mother, PAD, LAG1, SIGLVL, VERBOSE, DT, S0, DJ, NrScl, scale_l, scale_h, $
    xyfile, flagxy, xy2, xy3,co, qu, p, amp, coh,x2y,outlier,olcorrmeth, olstddev,olthresh, lagsADF, $
    lags, extFiles, flagR, flagUR,  quest, info,filelist
  
  
  ; ANMERKUNG: IFFT, lan sollte mittelfristig ge�ndert werden damit temp. Variable IFFT2, lan2,lan3 nicht ben�tigt wird $
  ; in base_widget_5!!!!! und base_widget6
  
  COMMON blkbase,base,generic2, sizebase,fourier,wavel,fourier2,fbase,sizebase1,sizebase2, base2, baseMorlet, basePaul, baseDOG,ipbase,$
    FFTpar,FFTp, IFFTp, FPp, WFFT,tendbase, cyclebase, generic, olcorr1, olcorr2, olcorr3, xyspec,vegpar,_lagsADF, $
    _lagsY, _lagsX1, _lagsX2, _lagsX3, _lagsX4, _smooth, _vecSel, _lagsX5, _normFreq, _From, _To, MothWlNr, $
    trendparR, trendparUR, parY, parX1, parX2, parX3, parX4, parX5, harmonics, _FPharmonics1, _FPfreq1,  _FPharmonics2, _FPfreq2, $
    extRegOpt,trendpar3, _nwind, _noverlap,_vegMA, _WFFTnwind, _WFFTnoverlap,_nwindDiff, _WFFTPeriod,_noverlapDiff,_WFFTnwindDiff, _WFFTnoverlapDiff, optionbase, _MKfreq, $
    _band, _rows, _cols, _imagebands, _thresh, _lag, _freq, _mv, _mvSais, _hs, _stddev, generic3,FPpara,_col1,_col2,generic4, generic4b, generic5
  
  ;CD, 'D:\data\medokads\'
  ;CD, 'W:\database\SpatialData\ClimaticData\clima_eeza\SUBSETS\'
  ;cd,'/export/feut/ladamer/Nov2004'
  workDir=''
  mapinfo=''
  wavelengthUnits=''
  nrPSmooth=0 ; Anzahl der Datenpunkte f�r Gl�ttung
  info=strarr(7)
  info[0]='Batchfile is missing'
  info[1]='Outputfile is missing'
  info[2]='Outputfile for stacked input information is missing'
  info[3]='No trend parameter to extract'
  info[4]='Output file for Fourier coefficients is missing'
  info[5]='Output file for hot-spot detection is missing'
  info[6]='Missing file name for resticted/unrestricted regression!'
  band=1
  mvSais=12
  IFFT_reg=1
  imagebands=1
  IFFT_L=2
  recFrom = -1
  recTo = -1
  WFFTnwindDiff=-1
  WFFTnoverlapDiff=-1
  nwindDiff=-1
  noverlapDiff=-1
  MothWlNr=0 ; 0 f�r Morlet, 1 f�r PaulP, 2 f�r DOGP
  nwind=50; length of the window for spectral density estimation
  windType='HANN'
  noverlap=-1 ; 50% overlapping
  ;cols=6012
  ;rows=3928
  ;cols=1536
  ;rows=1600
  cols=839
  rows=379
  mv=999
  IFFT_A=0.15
  lan=3
  integrals=0  ; bei 1 werden Integrale gebildet �ber freq Zeitpunkte
  
  normFreq=12 ; f�r monatsweise (saisonale) Standardisierung
  BOrder=0
  IFFT=8
  IFFT2=8
  IFFT_temp=1
  ofile=''
  ofile_ori=''
  xyfile=''
  x2y=0
  hotspots=''
  thresh=0
  olstddev=2 ; nicht mehr Std.abweichung sondern Vertrauensbereich, gem�� Chebyshev's Theorem
  olthresh=50
  olcorrmeth=2
  lag=1
  lan2=0
  lan3=2  ; beides f�r GUI-kontrolle in base_widget6
  outlier=0             ; keine Ausreisseranalyse
  fourierfile=''
  missvalue=1
  four_koeff=0
  four_koeff_temp=1
  file=''
  flag=bytarr(31)        ; Array zur Auswahl der Trendparameter
  flagxy=bytarr(7)       ; Kreuzkorrelation
  flagR=bytarr(11)        ; Parameter f�r restricted regression
  flagUR=bytarr(11)       ; Parameter f�r unrestricted regression
  para=intarr(7)         ; f�r Fehlerkontrolle im GUI
  para[6]=1              ; Voreinstellung f�r resticted/unrestr. regression GUI
  batch=0                ; 0 kein Batchfile f�r Eingabe, 1 ENVI-Batchfile
  lagsADF=0              ; Anzahl der lags f�r ADF-Test
  ; wavelet parameters
  scale_l=0.9
  scale_h=1.05
  MorletP=6
  PaulP=4
  DOGP=2
  Mother='Morlet'
  PAD=1
  LAG1=0
  SIGLVL=0.95
  VERBOSE=0
  DT=0.083333333333333d  ; DT = amount of time between each Y value, i.e. the sampling time.
  ; Wert entspricht 1/12 des Jahresgangs (=Monatswerte)
  DJ=0.125
  S0=0; Default-Wert wird verwendet
  NrScl=0 ;Default-Wert wird verwendet
  
  ;    S0 = the smallest scale of the wavelet.  Default is 2*DT.
  ;
  ;    DJ = the spacing between discrete scales. Default is 0.125.
  ;         A smaller # will give better scale resolution, but be slower to plot.
  ;
  ;    NrScl = the # of scales minus one. Scales range from S0 up to S0*2^(J*DJ),
  ;        to give a total of (J+1) scales. Default is J = (LOG2(N DT/S0))/DJ.
  
  
  
  quest=     {   diff:       0b,      $     ; 0: Zeitreihe nicht differenzieren, 1: Zeitreihe differenzieren
    Xprocessing: 0b,       $     ; externe X Variable f�r Regression nicht wie Y vorverarbeiten, 1 sonst
    lag:        1,      $     ; f�r Hotspot-detection
    wald:       0b,      $     ; Wald-F Statistik f�r zwei Regressionen
    resid:      -1,       $     ; 0: Layerstack, 1: Residuen, 2: Yfit; -1: nichts
    freq:       12,      $     ; Periode f�r MSK, SK und SKS
    freq1:      0,      $      ; Periode f�r erstes Fp
    freq2:      0,      $      ; Periode f�r zweites Fp
    colFirst:   0,      $      ; Sortiere von colum 1
    colLast:    0,      $      ; bis colum 2
    WFFTfreq:   12,      $      ; Periode f�r WFFT
    vegMA:      5,      $      ; L�nge Moving Average Fenster f�r Vegetationsl�nge
    vegMetrics: 0,      $      ; 0:Phase, 1: Amplitude; 2: Lenght
    outFormat:  2,      $      ; 0 BYTE, 1 FIX, 2 FLOAT
    prewhite:   1,      $      ; 0 kein prewhitening, 1 sonst
    gls:        0,      $      ; 0 f�r OLS, 1 f�r GLS
    autoLAG:     0,      $      ; 0 f�r keine automatische Lagbestimmung bei DL-Models
    WFFTType:   0,      $      ; 0 f�r Phase, 1 f�r Magnitude
    sig:        0,      $       ; 0 t-emp, z-emp werden gespeichert, 1: 2seitige sig werden gespeichert
    confident:  95.,      $       ; gem�ss Chebyshev's Theorem  f�r Ausrei�er-Korrektur
    NoHarmonics1:0,      $     ; Anzahl Oberschwingungen erstes Freq.band
    NoHarmonics2:0,      $     ; Anzahl Oberschwingungen zweites Freq.band
    detrendLin:  0,      $      ; 0 lineare Komponente f�r detrending, 1 sonst
    tif:         0b,      $      ; 1 f�r tif-files
    normalize:  bytarr(10) }     ; 0: nicht standardisieren, 1: Abw. Jahresmittel,2: Abw. Jahresmittel/sttdev
  ; 3. Abw. vom Monatsmittel (bzw. �ber einzelne freq).,4. Abw. vom Monatsmittel/stddev,
  ; 5. Differenzen; 6. De-trending;  7: Prewhitening; 8. saisonale Mittelwerte; 9. MVC;
  quest.normalize[0]=0
  lags =     {lagsY:0, lagsX1:0, lagsX2:0, lagsX3:0, lagsX4:0, lagsX5:0 }  ; Lags f�r Zeitreihen in resticted und unrestricted Regression
  extFiles=  {fileX1:'',fileX2:'',fileX3:'',fileX4:'',fileX5:''}           ; Filennamen f�r externe Regressoren und Flags f�r

;end