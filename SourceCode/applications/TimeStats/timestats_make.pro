pro TimeStats_make

  codeDir = TimeStats_getDirname(/SourceCode)
  targetDir = TimeStats_getDirname()
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir

  SAVFile = filepath('TimeStats.sav', ROOT=TimeStats_getDirname(), SUBDIR='lib')
  compileFirst = filepath('timestats.pro', ROOT_DIR=codeDir, SUBDIRECTORY='origSource') ; needed for common blocks
  compileNot = filepath('timestats_initcommonblocks.pro', ROOT_DIR=codeDir)
  hubDev_compileDirectory, codeDir, SAVFile, CompileFirst=compileFirst, CompileNot=compileNot

  docDir = filepath('', ROOT_DIR=TimeStats_getDirname(), SUBDIR=['help','idldoc'])
  title='TimeStats Documentation'
  hub_idlDoc, codeDir, docDir, title

end