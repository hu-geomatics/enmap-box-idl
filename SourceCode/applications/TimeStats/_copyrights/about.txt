TimeStats Version 1.0

Author: Thomas Udelhoven
        Département Environnement et Agro-Biotechnologies
        41, rue du Brill
        L-4422 Belvaux
        Luxembourg

Contact: udelhove@lippmann.lu

TimeStats is freely redistributed for non-commercial use only.