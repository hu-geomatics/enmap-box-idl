;+
; :Description:
;    GUI-independent version of TimeStats "Data pre-processing" functionality.
;
; :Params:
;    inputFilename : in, required, type=filename
;      Filename to the temporal image-stack (BIL interleave).
;      
;    outputDirectory : in, required, type=filename
;      Filename to the output directory where the output files will be saved 
;      (`AR-koeffs.bil, AR-order.bil, CountsnegDiffMonthlyAvg.bil, CountsPosDiffMonthlyAvg.bil, m-mean.bil, m-stddev.bil, timeStatsTestImage.bil`)
;      
;    preprocessingList : in, required, type=int[]
;      Array of pre-processing options to be calculated. Specify a subset of the following indices::
;      
;        0=No pre-processing
;        1=Anomalies from total mean
;        2=Standardized anomalies from total mean
;        3=Anomalies from  seasonal means
;        4=Standardized anomalies from seasonal means (is not calculated when 3 is also choosen)
;        5=Differences
;        6=De-trending
;        7=Prewhitening (ARMA-model)
;        8=Seasonal averaging (aggregation) 
;        9=Maximum value compositing (is not calculated when 8 is also choosen)
;
; :Keywords:
;    LagForDifferences : in, optional, type=int, default=12
;      Lag for differences; length of seasonal/MVC period.
;    
;    DetrendingParameters : in, optional, type=structure
;      De-trending parameters is a structure with the following tags::
;        
;        Tag                      Type   Default   Description/Notes
;        -----------------------------------------------------------
;        includeLinearTrend       {0|1}  0         'Include linear trend component?'   0='yes', 1='no'
;        lengthOfFourierPeriod1   int    0         'Length of Fourier period 1'
;        lengthOfFourierPeriod2   int    0         'Length of Fourier period 2'
;        numberOfHarmonics1       int    0         'No. of harmonics 1'
;        numberOfHarmonics2       int    0         'No. of harmonics 2'
;    
;    ThresholdForDifferenceImages : in, optional, type=float, default=0
;      Threshold (%) for difference images.
;
;    LagForDifferenceImages : in, optional, type=int, default=1
;      Lag.
;    
;    RecordRange : in, optional, type=int, default=[-1 -1]
;      Use record no. 'from' und 'to'.
;    
;    Title : in, optional, type=string
;      Title for GUI.
;      
;    GroupLeader : in, optional, type=int
;      Group leader for GUI.
;
; :Author: Andreas Rabe
;-
pro TimeStats_dataPreprocessing_processing, inputFilename, outputDirectory, preprocessingList,$
  LagForDifferences=lagForDifferences, $                      
  DetrendingParameters=detrendingParameters, $
  ThresholdForDifferenceImages=thresholdForDifferenceImages, $
  LagForDifferenceImages=lagForDifferenceImages, $
  RecordRange=recordRange, $  
  Title=title, GroupLeader=groupLeader

  ; check time stack interleave
  if ~strcmp(hubIOImg_getMeta(inputFilename, 'interleave'), 'bil', /FOLD_CASE) then begin
    message, 'The input time stack interleave must be BIL.'
  endif

  ; create output directory if necessary
  if ~file_test(outputDirectory, /DIRECTORY) then begin
    file_mkdir, outputDirectory
  endif

  @timestats_initcommonblocks

  ; assign inputs to common block variables

  FILELIST         = inputFilename
  FILE             = inputFilename
  WORKDIR          = filepath('', ROOT_DIR=outputDirectory) ; ensure directory marker / or \ for correct behaviour of TREND 
  quest.normalize[preprocessingList] = 1b
  if (quest.normalize[3] eq 1) and (quest.normalize[4] eq 1) then quest.normalize[4] = 0
  if (quest.normalize[8] eq 1) and  (quest.normalize[9] eq 1) then quest.normalize[9] = 0
  if isa(lagForDifferences) then normFreq = lagForDifferences
  if isa(detrendingParameters) then begin
    quest.detrendLin = detrendingParameters.includeLinearTrend ; 0='yes', 1='no']
    quest.freq1 = detrendingParameters.lengthOfFourierPeriod1
    quest.freq2 = detrendingParameters.lengthOfFourierPeriod2
    quest.NoHarmonics1 = detrendingParameters.numberOfHarmonics1
    quest.NoHarmonics2 = detrendingParameters.numberOfHarmonics2
  endif
  if isa(thresholdForDifferenceImages) then thresh = thresholdForDifferenceImages
  if isa(lagForDifferenceImages) then quest.lag = lagForDifferenceImages
  if isa(recordRange) then begin
    recFrom = recordRange[0]
    recTo = recordRange[1]
  endif
  BATCH            = 1 ; need to set this to 1 because of ENVI input I think(?)

  ; run TREND procedure
  TimeStats_runTrend, GroupLeader=groupLeader, Title=title
end

pro test_timestats_dataPreprocessing_processing
  inputFilename = TimeStats_getTestImage('timeStatsTestImage.bil')
  outputDirectory = hub_getUserTemp(SubDirectory='TimeStats')
  preprocessingList = [0:9]
  timestats_dataPreprocessing_processing, inputFilename, outputDirectory, preprocessingList,$
    LagForDifferences=lagForDifferences, $                      
    DetrendingParameters=detrendingParameters, $
    ThresholdForDifferenceImages=thresholdForDifferenceImages, $
    LagForDifferenceImages=lagForDifferenceImages, $
    RecordRange=recordRange, $  
    Title=title, GroupLeader=groupLeader
  
  
end