function TimeStats_basicStatistics_getParameters, Title=title, GroupLeader=groupLeader

  parameterNames = ['Reg. bias (b)','Reg. offset','Sigma','T-test (b)','r','RMS','RMS/mean','MaxResid','TimeMaxResid', $
    'MK test','K slope','SK test','SK slope','Mod.SK test','Stdev','Mean','Varkoeff','valid n', $
    'skewness','kurtosis','Abbe criterion','rho rank corr','tau rank korr','sig. rho','sig. tau',$
    'min','max','time min->max','time max->min','range','ADF test, incl. const./trend']
  parameterNamesReduced = parameterNames[[[0:26],29,30]] ; exclude 'time min->max','time max->min'
  significanceNames = ['save emp. t-scores (lin. reg) / emp. z-scores (MK, SMK, MSK)','save related twosided P-values']

  oldParameters = hub_getAppState('TimeStats', 'basicStatisticsParameters', Default=DICTIONARY())

  fsize=750
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputFilename', Title='Time Series Stack          ', SIZE=fsize, Value=oldParameters.hubGetValue('inputFilename')
  hubAMW_frame, Title='Parameters'
  hubAMW_checklist, 'parameterListReduced', Title='', List=parameterNamesReduced, /MULTIPLESELECTION, /SHOWBUTTONBAR, COLUMN=6, Value=oldParameters.hubGetValue('parameterListReduced') 
  hubAMW_parameter, 'lagsForADFTest',            Title='Lags for Augmented-Dickey-Fuller (ADF) test', Size=5, /Integer, Unit='(Default=0)', Value=oldParameters.hubGetValue('lagsForADFTest', Default=0)
  hubAMW_parameter, 'periodOfSeasonalComponent', Title='Period of seasonal component               ', Size=5, /Integer, Unit='(Default=12)', Value=oldParameters.hubGetValue('periodOfSeasonalComponent', Default=12)
  hubAMW_checklist, 'significanceTestOption', Title='Significance test options', List=significanceNames, Value=oldParameters.hubGetValue('significanceTestOption', Default=0)
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputFilename', Title='Regression Parameter Image ', SIZE=fsize, Value=oldParameters.hubGetValue('outputFilename', Default='regressionParameters')
  result = hubAMW_manage(/DICTIONARY)

  if result.accept then begin 
    parameters = result
    hub_setAppState, 'TimeStats', 'basicStatisticsParameters', parameters+hash()
    parameterList = parameters.remove('parameterListReduced')
    parameterList[where(/NULL, parameterList gt 26)] += 2 
    parameters.parameterList = parameterList
  endif else begin
    parameters = !NULL
  endelse
  return, parameters
end

pro test_TimeStats_basicStatistics_getParameters
  parameters = TimeStats_basicStatistics_getParameters()
  print,parameters
end
