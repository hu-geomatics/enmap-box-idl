function imageMath_evaluate, expression, variables, filename, DataIgnoreValue=dataIgnoreValue, NoOpen=noOpen

  if isa(variables) then begin
  ;          'number'  : result[variableName] = {type:'number', filename:filename, sample:sample, line:line, band:band}
  ;          'band'    : result[variableName] = {type:'band', filename:filename,                           band:band}
  ;          'profile' : result[variableName] = {type:'profile', filename:filename, sample:sample, line:line}
  ;          'image'   : result[variableName] = {type:'image', filename:filename}
    if ~isa(variables, 'hash') then begin
      message, 'Argument must be a hash: variable', /NONAME
    endif
    if ~isa(filename) then begin
      message, 'Argument is undefined: filename', /NONAME
    endif
    variables['$resultFilename$'] = filename 
  endif else begin
    variables = hash()
  endelse
  calculator = imageMathCalculator()
  calculator.setInfixString, strjoin(expression, ' ')
  calculator.setVariables, variables
  calculator.setDataIgnoreValue, dataIgnoreValue
  result = calculator.execute(Status=status)
  obj_destroy, calculator  
  if status.error then begin
    message, status.message, /NONAME
  endif else begin
    if isa(result.imageMathArgument, 'imageMathArgumentNumber') then begin
      result = (result.imageMathArgument.getValue())[0]
    endif else begin
      filename = (result.imageMathArgument.getImage()).getMeta('filename data')
      if ~keyword_set(noOpen) then begin
        hubProEnvHelper.openImage, filename
      endif
      result = filename
    endelse
  endelse  
  
  return, result
  
end

pro test_imageMath_evaluate

  ; define the expression
  
  expression = 'imageMathFunction_spectralMean(abs(myImage-myProfile))'
  expression = 'spectralMean(abs(myImage-myProfile))'

  ; define the image arguments used inside the expression, and define the result image filename
  
  variables = hash()
  variables['myImage']    = {type:'image',   filename:hub_getTestImage('Hymap_Berlin-A_Image')}
  variables['myProfile']  = {type:'profile', filename:hub_getTestImage('Hymap_Berlin-A_Image'), sample:166, line:147}
  resultFilename = filepath('my_image', /TMP)

  ; evaluate the expression and show the result

  result = imageMath_evaluate(expression, variables, resultFilename)
  print, result
  (hubIOImgInputImage(resultFilename)).quickLook

end

pro test_imageMath_evaluate2

  ; define the expression

  expression = 'image1*image2'
  expression = 'image1*1'
  

  ; define the image arguments used inside the expression, and define the result image filename
  
  variables = hash('image1', imageMath_variable(hub_getTestImage('Hymap_Berlin-B_Image')),$
                   'image2', imageMath_variable(hub_getTestImage('Hymap_Berlin-Stratification-Image')))
  resultFilename = hub_getUserTemp('result')

  ; evaluate the expression and show the result

  result = imageMath_evaluate(expression, variables, resultFilename, DataIgnoreValue=-999)
  
  enmapBox
  enmapBox_openImage, resultFilename

end