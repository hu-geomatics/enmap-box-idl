;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `imageMath_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro imageMath_event, event
    
  @huberrorcatch

  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  groupLeader = applicationInfo['groupLeader']
  imageMathGUI = imageMathGUI(GroupLeader=groupLeader)
  imageMathGUI.startMainGUI

end
