.. title:: Implementing imageMath Routines

imageMath provides different mechanisms for integrating user-written routines
that can be applied to images without dealing with file IO issues.
The basic idea behind imageMath routines is, that the user simply defines the data processing
for image cube tiles, and imageMath cares about the file IO.

Value-Wise Routines
-------------------

A value-wise routine is a simple IDL function that is assumed to take one, two, three or more
input arrays of same size, do some calculations, and returns an output array of corresponding size.

When such a function is evaluated inside an imageMath expression, 
all possible combinations of input variables (i.e.` image`,` band`,` profile` and` number`)
are allowed. imageMath will pass image cube tiles to the function. Missing dimensions will be 
replicated to ensure that the function is always provided with consistent input data. 

A value-wise routine has the following structure::

  function myFunction, x1, x2, ..., Reflection=reflection

    if keyword_set(reflection) then begin
      description = ['Describe your function...','...']
      result = imageMathFunction_defineReflection(/ValueWise, Description=description)
      return, result
    endif
  
    result = ; calculate something
    return, result
  
  end

To communicate some information about itself, an imageMath function has to define the` Reflection` keyword. 
The `imageMathFunction_defineReflection` function can be used to create an appropriate result structure. 
Specify the` description` keyword to provide some useful information about the function, and set the` ValueWise` keyword.
imageMath will need this information to correctly call the function and pass data to it.

For details on how to implement such routines see the following examples:

Example: Value-Wise Conditional Operator (If-Then-Else Statement)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The conditional operator provides a way to write simple `If...Then...Else` statement in expression form.
The IDL syntax for the statement` If exp1 Then exp2 Else exp3` can be written as` exp1 ? exp2 : exp3`. 
The imageMath function `imageMathFunction_conditional` translates this behaviour to images. 
For all values where` exp1` evaluates to true, the result is taken from` exp2`, otherwise it is taken from` exp3`.

Have a look at the source code::

  .edit imageMathFunction_conditional

For example, we can extract some class regions from an image and set the rest to -1::

  expression = 'conditional(class eq 1, image, -1)'

  variables = hash()
  variables['class'] = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth'))
  variables['image'] = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Image'))
  resultFilename = filepath('my_image', /TMP)
 
  result = imageMath_evaluate(expression, variables, resultFilename)
  print, result
  (hubIOImgInputImage(resultFilename)).quickLook, /DefaultBands

IDL prints something like::

  C:\Users\janzandr\AppData\Local\Temp\my_image

.. image:: routine1.gif

Another example for conditional expressions is to build a composition of two images, 
e.g. where image A is place on the left side and image B is placed on the right side::

  expression = 'conditional( generateRampH(300,300,1) gt 150, imageA, imageB)'

  variables = hash()
  variables['imageA'] = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Image'))
  variables['imageB'] = imageMath_variable(hub_getTestImage('Hymap_Berlin-B_Image'))
  resultFilename = filepath('my_image', /TMP)
 
  result = imageMath_evaluate(expression, variables, resultFilename)
  print, result
  (hubIOImgInputImage(resultFilename)).quickLook, /DefaultBands

IDL prints something like::

  C:\Users\janzandr\AppData\Local\Temp\my_image

.. image:: routine2.gif

Example: Value-Wise Minimum and Maximum
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The imageMath functions `imageMathFunction_minimum` and `imageMathFunction_maximum` are returning
the minimum/maximum values for a set of inputs. 

Have a look at the source code::

  .edit imageMathFunction_minimum

Here is a simple examples with numbers only::

  expression = 'minimum(13, 42, 5, 7, 1001)'
  result = imageMath_evaluate(expression)
  print, result
  
IDL prints::

       5

Another, more complicated, example would be to calculate the minimum of 1) a horizontal ramp image, 
2) a vertical ramp image and 3) a constant number::

  expression = 'minimum(generateRampH(300,300,1), generateRampV(300,300,1), 150)'
  result = imageMath_evaluate(expression)
  print, result
  (hubIOImgInputImage(result)).quickLook

IDL prints something like::

  C:\Users\janzandr\AppData\Local\Temp\my_image

.. image:: routine3.gif

Image Processing Routines
-------------------------

An image processing routine is a simple IDL function that is assumed to take a three dimensional input array
and returns a three dimensional output array of corresponding spatial size (first two dimensions must match) and
arbitrary spectral size (third dimension).

When such a function is evaluated inside an imageMath expression, 
the first input variable must be an image and all other variables must be numbers.
imageMath will pass the image data as cube tiles. 

An image processing routine has the following structure::

  function myFunction, image, arg1, arg2, ..., Reflection=reflection

    if keyword_set(reflection) then begin
      description = ['Hello World', 'Describe your function...']
      functionClass = 'imageProcessing'
      neighborhoodHeight = 1
      reflection = {functionClass:functionClass, description:description,  neighborhoodHeight:neighborhoodHeight}
      return, reflection
    endif
  
    result = ; calculate something
    return, result
  
  end

To communicate some information about itself, an imageMath function has to define the` Reflection` keyword. 
The `imageMathFunction_defineReflection` function can be used to create an appropriate result structure. 
Specify the` description` keyword to provide some useful information about the function,
specify the` neighborhoodHeight` keyword if your routine uses a pixel neighborhood for the calculation, and
set the` ImageProcessing` keyword.
imageMath will need this information to correctly call the function and pass data to it.

For details on how to implement such routines see the following examples:

Example: Spatial Mean Filter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The imageMath functions `imageMathFunction_spatialMeanFilter` applies a moving average filter to the image.
The filter size can be defined via the` width` and` height` arguments. 

Filtering a random image::   

  expression = 'spatialMeanFilter(generateRandomU(300, 300, 3), 5, 5)'
  result = imageMath_evaluate(expression)
  print, result
  (hubIOImgInputImage(result)).quickLook, [0,1,2]
  
IDL prints::

  D:\Users\Andreas\AppData\Local\Temp\imageMath\imageMathTemp635507305

.. image:: routine4.gif

Note that for this function the` neighborhoodHeight` information is related to the` height` argument 
(or the` width` argument, if` height` is omitted). Let us inspect the source code section, dealing with that problem::

  .edit imageMathFunction_spatialMeanFilter
  
Find the passage::

    if isa(width) then begin
      neighborhoodHeight = isa(height) ? height : width
    endif else begin
      neighborhoodHeight = 1
    endelse

This conditional statement handles all possible situations of omitting one or both of the` height` and` width` keywords,
which has direct impact on the` neighborhoodHeight` information.

External Routines
-----------------

An external routine is a simple IDL function that is assumed to take one, two, three or more
inputs. In contradiction to other imageMath functions, not the image data is passed, but the corresponding filenames
(numbers are still passed as numbers). Use external routines to apply more complicated calculations, or 
to write wrappers for existing applications. 

When such a function is evaluated inside an imageMath expression, 
all possible combinations of input variables (i.e.` image`,` band`,` profile` and` number`)
are allowed. 

An external routine has the following structure::

  function myFunction, filename1, filename2, ..., Reflection=reflection, ResultFilename=resultFilename 

    if keyword_set(reflection) then begin
      description = ['Describe your function...','...']
      result = imageMathFunction_defineReflection(/External, Description=description [, /ReturnsNumber | /ReturnsProfile])
      return, result
    endif
  
    ; do some calculations
    ; write the result image to the filename provided by the resultFilename keyword  
    
    return, !null
  
  end

To communicate some information about itself, an imageMath function has to define the` Reflection` keyword. 
The `imageMathFunction_defineReflection` function can be used to create an appropriate result structure. 
Specify the` description` keyword to provide some useful information about the function, and set the` External` keyword.
Optionally set the` ReturnsNumber` or` ReturnsProfile` keywords to let imageMath interpret the result image as a number or a profile, instead of an image.
imageMath will need this information to correctly call the function and pass filenames and data to it.
Note that an external routine always returns !null,
the result image must be written to the filename provided by the` resultFilename` keyword.
  
For details on how to implement such routines see the following examples:

Example: Wrapper for the Savitzky-Golay Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Savitzky-Golay application can be started from the EnMAP-Box:
showing the following dialog:

.. image:: routine5.gif

To integrate the application into imageMath we have to write a wrapper (see `imageMathFunction_savitzkyGolayApplication`) that uses the Savitzky-Golay API.
We can programmatically replicate the above Savitzky-Golay settings by using the API::

  parameters = hash()
  parameters['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['outputFilename'] = filepath('my_image', /TMP)
  parameters['width'] = 5
  parameters['degree'] = 3
  parameters['order'] = 1
  hubApp_savitzkyGolay_imageProcessing, parameters
  (hubIOImgInputImage(parameters['outputFilename'])).quickLook, /DefaultBands
  (hubIOImgInputImage(parameters['outputFilename'])).quickLookProfile, 150,150
  
.. image:: routine6.gif

The above code snipped is the core part of the wrapper, for more details have a look at the source code::

  .edit imageMathFunction_savitzkyGolayApplication

