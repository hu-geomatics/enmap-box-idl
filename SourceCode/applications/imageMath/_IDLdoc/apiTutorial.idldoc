.. title:: imageMath API Tutorial

This tutorial shows examples for performing the imageMath calculations programmatically.

Evaluate a Simple Expression
----------------------------

Evaluate a simple expression without any input images::

  ; define the expression
  
  expression = '1+2'
  
  ; evaluate the expression and show the result
  
  result = imageMath_evaluate(expression)   
  help, result
  
IDL prints::

  RESULT          INT       =        3
  
Masking an Image
----------------

Masking an image by simply multiplying it with a mask band::

  ; define the expression
  
  expression = 'myImage*myMask'

  ; define the image variables used inside the expression, and define the result image filename
  
  variables = hash()
  variables['myImage'] = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Image'))
  variables['myMask']  = imageMath_variable(hub_getTestImage('Hymap_Berlin-B_Mask'), Band=0)
  resultFilename = filepath('my_image', /TMP)
    
  ; evaluate the expression and show the result
  
  result = imageMath_evaluate(expression, variables, resultFilename)
  print, result
  (hubIOImgInputImage(resultFilename)).quickLook, /DefaultBands

IDL prints something like::

  C:\Users\janzandr\AppData\Local\Temp\my_image

.. image:: api1.gif

Note that the` variables` hash keys must match the variable names used inside expression (i.e.` 'myImage'` and` 'myBand'` for the example above).
The corresponding hash values are returned by the` imageMath_variable` function.   

Calculating the Spectral Similarity between a Profile and all Image Profiles
----------------------------------------------------------------------------

Calculating the mean absolute error (MAE) between a vegetation profile and an image. 
The vegetation profile is taken from the same image (pixel location indices are x=166 and y=147)::

  ; define the expression
  
  expression = 'spectralMean(abs(myImage-myProfile))'

  ; define the image variables used inside the expression, and define the result image filename
  
  variables = hash()
  variables['myImage']    = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Image'))
  variables['myProfile']  = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Image'), Position=[166,147])
  resultFilename = filepath('my_image', /TMP)

  ; evaluate the expression and show the result

  result = imageMath_evaluate(expression, variables, resultFilename)
  print, result
  (hubIOImgInputImage(resultFilename)).quickLook

IDL prints something like::

  C:\Users\janzandr\AppData\Local\Temp\my_image

.. image:: api2.gif

Note that, instead of the mean absolute error,  you can also easily compute the root mean squared error (RMSE) by evaluating::
  
  expression = 'sqrt(spectralMean((myImage-myProfile)^2.))'
  result = imageMath_evaluate(expression, variables, resultFilename)
  (hubIOImgInputImage(resultFilename)).quickLook
  
The result is alsmost the same.


