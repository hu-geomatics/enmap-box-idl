;+
; :Author: <author name> (<email>)
;-

pro imageMath_make

  if enmapboxmake.clean() then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    
    appDir = imageMath_getDirname()
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ; find source code and application directory  
  
  codeDir =  file_dirname((routine_info('imageMath_make',/SOURCE)).path)
  appDir = imageMath_getDirname()
  
  ; check lower case *.pro filenames (important for auto-compiling under unix systems
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  if ~enmapboxmake.CopyOnly() then begin
     ; save routines inside SAVE file
     
    SAVFile = filepath('imageMath.sav', ROOT=appDir, SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, /NoLog
    
    if ~enmapboxmake.NoIDLDoc() then begin
      ; create IDLDOC documentation
      
      helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
    
      hub_idlDoc, codeDir, helpOutputDir, 'imageMath Documentation', NOSHOW=enmapboxmake.noShow() $
                , SUBTITLE=' ' $
                , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc')
    endif
  endif
end
