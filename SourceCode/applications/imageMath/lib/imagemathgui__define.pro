function imageMathGUI::init, GroupLeader=groupLeader
  self.groupLeader = ptr_new(groupLeader)
  self.title = 'ImageMath Calculator' 
  self.calculator = imageMathCalculator(GroupLeader=groupLeader)
  self.oldVariables = hash()
  self.functionDescription = hash()
  return, 1b
end

pro imageMathGUI::startMainGUI

  (hubGUIHelper()).setFixedWidgetFont
  self.idsMainGUI = hash()
  self.tlbMainGUI = widget_base(TITLE=self.title, /COLUMN, GROUP_LEADER=*self.groupLeader, UVALUE=self)
  
  baseMain = widget_base(self.tlbMainGUI, /ROW, XPAD=0,YPAD=0,SPACE=1)
  baseLeft =  widget_base(baseMain, /COLUMN, XPAD=0,YPAD=0,SPACE=1)
  base = widget_base(baseLeft, /COLUMN, XPAD=1,YPAD=1,SPACE=1)
  label = widget_label(base, Value='Input')
  text = widget_text(base, SCR_XSIZE=545, /EDITABLE, YSIZE=5, VALUE=[''],UVALUE=self, /WRAP, /ALL_EVENTS)
  (self.idsMainGUI)['input'] = text

  base = widget_base(baseLeft, /COLUMN, XPAD=1,YPAD=1,SPACE=1)
  label = widget_label(base, Value='Output/Info')
  text = widget_text(base, SCR_XSIZE=545, /EDITABLE, YSIZE=10, VALUE='',UVALUE=self, /FRAME, /WRAP, /SCROLL)
  (self.idsMainGUI)['output'] = text
  base = widget_base(baseLeft, /ROW, XPAD=0,YPAD=0,SPACE=0)
  
  xsize = 50
  ysize1 = 50
  ysize2 = 25

  subbase = widget_base(base, /COLUMN, XPAD=1,YPAD=0,SPACE=0)
  label = widget_label(subbase, VALUE='Bitwise', SCR_YSIZE=ysize2-5)
  button = widget_button(subbase, VALUE=' NOT ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' AND ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' OR ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' XOR ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  dummy = widget_base(subbase, XPAD=0,YPAD=0,SPACE=0, SCR_XSIZE=xsize, SCR_YSIZE=ysize2,/ROW)
  label = widget_label(dummy, VALUE=' Other',/ALIGN_CENTER)
  button = widget_button(subbase, VALUE='>', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE='<', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE='x^y', UNAME='^', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)

  subbase = widget_base(base, /COLUMN, XPAD=1,YPAD=0,SPACE=0)
  label = widget_label(subbase, VALUE='Logical', SCR_YSIZE=ysize2-5)
  button = widget_button(subbase, VALUE=' ~ ', UNAME=' ~', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' && ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' || ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' EQ ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' NE ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' LE ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' LT ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' GE ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase, VALUE=' GT ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
 
  subbase = widget_base(base, /COLUMN, XPAD=0,YPAD=0,SPACE=0)
  label = widget_label(subbase, VALUE='NumPad', SCR_YSIZE=ysize2-5)
  subbase = widget_base(subbase, /ROW, XPAD=0,YPAD=0,SPACE=0)
  subbase2 = widget_base(subbase, ROW=5, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase2, VALUE='(', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE=',', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE=')', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE='7', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='8', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='9', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='4', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='5', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='6', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='1', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='2', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='3', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='0', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='.', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)

  subbase2 = widget_base(subbase, ROW=5, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase2, VALUE=' mod ', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase2, VALUE=string(247b), UNAME='/', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE=string(215b), UNAME='*', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='-', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='+', SCR_XSIZE=xsize, SCR_YSIZE=ysize1, UVALUE=self)
  
  subbase2 = widget_base(subbase, ROW=5, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase2, VALUE='Backspace', UNAME='', SCR_XSIZE=xsize*2, SCR_YSIZE=ysize2, UVALUE=self)
  (self.idsMainGUI)['Backspace'] = button
  button = widget_button(subbase2, VALUE='Clear', SCR_XSIZE=xsize*2, SCR_YSIZE=ysize1, UVALUE=self)
  button = widget_button(subbase2, VALUE='=', SCR_XSIZE=xsize*2, SCR_YSIZE=ysize1*3, UVALUE=self)
  
  subbase = widget_base(base, /COLUMN, XPAD=1,YPAD=0,SPACE=0)
  label = widget_label(subbase, VALUE='Type Casting', SCR_YSIZE=ysize2-5)
  subbase2 = widget_base(subbase, /ROW, XPAD=0,YPAD=0,SPACE=0)
  subbase3 = widget_base(subbase2, /COLUMN, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase3, VALUE='byte()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='fix()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='uint()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='long()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='ulong()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='long64()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='ulong64()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='float()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='double()', SCR_XSIZE=xsize*1.5, SCR_YSIZE=ysize2, UVALUE=self)
  subbase3 = widget_base(subbase2, /COLUMN, XPAD=0,YPAD=0,SPACE=0)
  button = widget_button(subbase3, VALUE='0b', UNAME='b', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0s', UNAME='s', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0us', UNAME='us', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0l', UNAME='l', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0ul', UNAME='ul', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0ll', UNAME='ll', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0ull', UNAME='ull', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0.0', UNAME='.', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)
  button = widget_button(subbase3, VALUE='0.0d', UNAME='d', SCR_XSIZE=xsize, SCR_YSIZE=ysize2, UVALUE=self)

  baseRight = widget_base(baseMain, /COLUMN, XPAD=0,YPAD=1,SPACE=1)
  label = widget_label(baseRight, VALUE='Functions')
  geoBaseLeft = widget_info(/GEOMETRY, baseLeft)
  (self.idsMainGUI)['tree'] = widget_tree(baseRight, SCR_XSIZE=300, SCR_YSIZE=geoBaseLeft.scr_ysize-20, /FOLDER)
  self.createFunctionTree

  (hubGUIHelper()).centerTLB, self.tlbMainGUI
  widget_control, self.tlbMainGUI, /REALIZE
  widget_control, (self.idsMainGUI)['input'], /INPUT_FOCUS
  xmanager, 'imageMathMainGUI', self.tlbMainGUI, /NO_BLOCK, EVENT_HANDLER='hubObjCallback_event'

end

pro imageMathGUI::createFunctionTree

  ; define bitmap for function tree nodes
  bitmap = bytarr(16,16,3)+255b
  x = 5
  bitmap[x:-x-1,x:-x-1,*] = 0b
  bitmap[x+1:-x-2,x+1:-x-2,*] = 255b
 
 ; insert functions to the tree
  self.functionDescription = hash()

  root = (self.idsMainGUI)['tree']
  
;---------------------------
; parse imageMath menu files

  ; - main menus init
  imageMathMenu = hubGUIMenu()
  mainMenuFilename = filepath('imagemath.men', ROOT=imageMath_getDirname(), SUBDIR='resource')
  print, 'Parse imageMath main menu file: '+mainMenuFilename
  imageMathMenu.extendMenu, mainMenuFilename

  ; - user  menus
  userMenuFilenames = file_search(enmapBox_getDirname(/Applications), 'imagemath.men', Count=numberOfFiles)
  for i=0, numberOfFiles-1 do begin
    userMenuFilename = userMenuFilenames[i]
    if userMenuFilename eq mainMenuFilename then continue
    print, 'Parse imageMath user menu file: '+userMenuFilename
    imageMathMenu.extendMenu, userMenuFilename
  endfor

  imageMathMenu.createMenuTree, root, UVALUE=self, BITMAP=bitmap
end

pro imageMathGUI::startVariableGUI, Accept=accept, Result=result
    
  ; get user variables variables

  infixTokenStream = self.calculator.getInfixTokenStream()
  variableNames = hash()
  foreach token,infixTokenStream,i do begin
    if token.isVariable then begin
      variableNames[token.value] = !null
    endif
  endforeach

  if n_elements(variableNames) ne 0 then begin

    ; sort names
    variableNames = (variableNames.Keys()).toArray()
    variableNames = variableNames[sort(variableNames)]

    ; create widget program

    hubAMW_program, self.tlbMainGUI, Title=self.title+': Enter Variables'
    hubAMW_frame, Title='Input'
    tSize = 100
    foreach variableName, variableNames, i do begin
      if self.oldVariables.hasKey(variableName) then begin
        oldSettings = (self.oldVariables)[variableName]
        oldFilename = oldSettings['filename'] 
        oldBand = oldSettings['band']
        oldPixel = oldSettings['pixel']
      endif else begin
        oldFilename = !null
        oldBand = !null
        oldPixel = !null
      endelse

      hubAMW_inputImageFilename, variableName, Title=variableName, TSize=tsize $
        , /SelectBand, /SelectPixel, /AllowEmptyBand, /AllowEmptyPixel $
        , Value=oldFilename $
        , BandValue=oldBand $
        , PixelValue=oldPixel
    endforeach
    hubAMW_frame, Title='Output'
    hubAMW_outputFilename, '$resultFilename$', Title='Result Image', TSize=tsize, Value='imageMathResult'
;    hubAMW_parameter, '$resultDataIgnoreValue$', Title='Consider Data Ignore Values? Output Data Ignore Value', Value='', OPTIONAL=1, /STRING

    result = hubAMW_manage()
    accept = result['accept']


    if accept then begin
      ; save current selection for the next calculation
      self.oldVariables = self.oldVariables + result
      foreach variableName, variableNames do begin
        variableHash = result[variableName]
        filename = variableHash['filename']
        type = 'image'
        if isa(variableHash['band']) then begin
          type = 'band'
          band = variableHash['band']
        endif
        if isa(variableHash['pixel']) then begin
          type = 'profile'
          sample = (variableHash['pixel'])[0]
          line = (variableHash['pixel'])[1]
        endif
        if isa(variableHash['band']) and isa(variableHash['pixel']) then begin
          type = 'number'
        endif
        
        case type of
          'image'  : variable = {type:type, filename:filename}
          'band'   : variable = {type:type, filename:filename, band:band}
          'profile': variable = {type:type, filename:filename, sample:sample, line:line}
          'number' : variable = {type:type, filename:filename, sample:sample, line:line, band:band}
        endcase
        result[variableName] = variable
      endforeach
         
    endif 
  
  endif else begin
  
    accept = 1b
    result = hash()
    
  endelse
   

end

pro imageMathGUI::handleEvent, event

  ; get input text information
  selection = widget_info((self.idsMainGUI)['input'], /TEXT_SELECT)
  selectionStart = selection[0]
  selectionLength = selection[1]

  if isa(event, 'widget_button') then begin
  
    ; get button information
    buttonUname = widget_info(event.id, /UNAME)
    widget_control, event.id, GET_VALUE=buttonValue
    widget_control, (self.idsMainGUI)['input'], GET_VALUE=textValue

  ; handle '='
    if buttonValue eq '=' then begin
      self.setOutputText, ''
      self.calculate
      return
    endif
  
  ; handle 'Backspace'
    if buttonValue eq 'Backspace' then begin
      ; if nothing is selected, select the previous character and recall the event handler
      if selectionLength eq 0 then begin
        if selectionStart eq 0 then begin
          return
        endif else begin
          widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=[(selectionStart-1)>0,1]
          self.handleEvent, event
          return
        endelse
        
      ; otherwise delete the whole selection
      endif else begin
        ; nothing to do, will be handled by the following code
      endelse
     
    endif

    ; set new text in dependence to the button value
    case buttonValue of
      'Backspace' : newText = ''
      'Clear' : newText = ''
      else : newText = buttonUname ne '' ? buttonUname : buttonValue
    endcase
    
    ; special handling for functions
    isFunction = strmid(newText, strlen(newText)-2,2) eq '()'
    if isFunction then begin
      newText = strmid(newText, 0, strlen(newText)-1)
    endif
    
    ; update input text
    
    use_text_select = buttonValue ne 'Clear'
    no_newline =  1b
    widget_control, (self.idsMainGUI)['input'], SET_VALUE=newText, USE_TEXT_SELECT=use_text_select, NO_NEWLINE=no_newline
    newSelection = [selectionStart+strlen(newText), 0]
    widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=newSelection
    
    ; set input focus to input text
    widget_control, (self.idsMainGUI)['input'], /INPUT_FOCUS
    
    ; clean output text
;    widget_control, (self.idsMainGUI)['output'], SET_VALUE=''
    return
  endif
  
  ; handle character inputs
  if isa(event, 'widget_text_sel') then return ; ignore Delete key
  if isa(event, 'widget_text_del') then return ; ignore Backspace key
  if isa(event, 'widget_text_str') then return ; ignore Copy&Paste

  if isa(event, 'widget_text_ch') then begin
    if event.ch eq 61 then begin
      widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=[selectionStart-1,1]
      widget_control, (self.idsMainGUI)['input'], SET_VALUE='', /USE_TEXT_SELECT, /NO_NEWLINE
      self.calculate
    endif
    return
  endif
  
  ; handle tree event
  if isa(event, 'widget_tree_sel') then begin
    if widget_info(event.id, /TREE_FOLDER) then return
    ; insert function into input text widget
    if event.clicks eq 2 then begin
      functionName = widget_info(event.id, /UNAME)
      ; delete prefix
      if strcmp(functionName, 'imageMathFunction_', 18) then begin
        functionName = strmid(functionName, 18)
      endif
      newText = functionName+'('
      widget_control, (self.idsMainGUI)['input'], SET_VALUE=newText, /USE_TEXT_SELECT, /NO_NEWLINE
      newSelection = [selectionStart+strlen(newText), 0]
      widget_control, (self.idsMainGUI)['input'], SET_TEXT_SELECT=newSelection
      widget_control, (self.idsMainGUI)['input'], /INPUT_FOCUS
    endif
    ; show function description inside console text widget
    if event.clicks eq 1 then begin
      if widget_info(event.id, /TREE_FOLDER) then begin
        self.setOutputText, ''
      endif else begin
        functionName = widget_info(event.id, /UNAME)
        functionInfo = hubRoutineInfo(functionName, /FUNCTIONS)
        functionDescription = imageMathFunctionReflection(functionName, /Description)
        self.setOutputText, functionDescription
      endelse
    endif
    return
  endif
  if isa(event, 'widget_tree_expand') then begin
    return
  endif
  help, event

end

pro imageMathGUI::setInputText, inputText
  widget_control, (self.idsMainGUI)['input'], SET_VALUE=inputText, /NO_NEWLINE
end

function imageMathGUI::getInputText, Selection=selection
  
  widget_control, (self.idsMainGUI)['input'], GET_VALUE=inputText
  if keyword_set(selection) then begin
    selection = widget_info((self.idsMainGUI)['input'], /TEXT_SELECT)
    if selection[1] gt 0 then begin
      inputText = strmid(inputText, selection[0], selection[1])
    endif
  endif
  return, inputText
end

pro imageMathGUI::setOutputText, outputText
  widget_control, (self.idsMainGUI)['output'], SET_VALUE=outputText
end

function imageMathGUI::getOutputText
  widget_control, (self.idsMainGUI)['output'], GET_VALUE=outputText
  return, outputText
end

pro imageMathGUI::setVariables, variables
  self.calculator.setVariables, variables
end

pro imageMathGUI::calculate
  
  ; get user input text
  inputText = self.getInputText(/Selection)
  infixString = strtrim(strjoin(inputText, ' '),2)
   
  ; - if input text is empty, then return
  if infixString eq '' then begin
    self.setOutputText, ''
    return
  endif
  
  ; - set input text to calculator
  self.calculator.setInfixString, strjoin(inputText, ' ')
     
  ; get variables if not explicitly provided
  self.startVariableGUI, Accept=accept, Result=variables
  if accept then begin
    self.calculator.setVariables, variables
    if variables.hasKey('$resultDataIgnoreValue$') then begin
      self.calculator.setDataIgnoreValue, variables.remove('$resultDataIgnoreValue$')  
    endif
  endif else begin
    ; return silently
    return
  endelse
  
  widget_control,/HOURGLASS
  progressBar = hubProgressBar(Title=self.title, GroupLeader=*self.groupLeader, Info='evaluating expression')
  
  catch, errorStatus
   if (errorStatus ne 0) then begin
    catch, /CANCEL
    help, /LAST_MESSAGE
    self.setOutputText, 'Error: '+!error_state.msg
    obj_destroy, progressBar
    return
  endif
  result = self.calculator.execute(Status=status)

  if status.error then begin
    self.setOutputText, status.message
    obj_destroy, progressBar
    return
  endif else begin
    outputText = result.imageMathArgument._overloadPrint()
    outputText[0] = '= '+outputText[0]
      self.setOutputText, outputText
  endelse

  if ~isa(result.imageMathArgument, 'imageMathArgumentNumber') then begin
    filename = (result.imageMathArgument.getImage()).getMeta('filename data')
    hubProEnvHelper.openImage, filename
    widget_control, self.tlbMainGUI, /SHOW
  endif

  obj_destroy, progressBar
end

pro imageMathGUI::cleanup
  if widget_info(self.tlbMainGUI, /VALID_ID) then begin
    widget_control, /DESTROY, self.tlbMainGUI 
  endif  
  imageMath_cleanup
end

pro imageMathGUI__define
  struct = {imageMathGUI $
    , inherits IDL_Object $
    , groupLeader : ptr_new() $
    , calculator : obj_new() $
    , tlbMainGUI : 0l $
    , tlbVariableGUI : 0l $
    , idsMainGUI : hash() $
    , idsVariableGUI : hash() $
    , infoVariableGUI : hash() $
    , oldVariables : hash() $
    , functionDescription : hash() $
    , title : '' $
  }
end