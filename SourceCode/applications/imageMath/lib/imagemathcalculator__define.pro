function imageMathCalculator::init, GroupLeader=groupLeader
  self.variables = hash()
  self.groupLeader = ptr_new(groupLeader)
  return, 1b
end

pro imageMathCalculator::setVariables, variables
  self.variables = variables
end

pro imageMathCalculator::setDataIgnoreValue, dataIgnoreValue
  self.dataIgnoreValue = ptr_new(dataIgnoreValue)
end

function imageMathCalculator::getDataIgnoreValue
  if ptr_valid(self.dataIgnoreValue) then begin
    return, *self.dataIgnoreValue
  endif else begin
    return, !null
  endelse
end

pro imageMathCalculator::setInfixString, infixString
  self.infixString = infixString
end

function imageMathCalculator::getInfixString
  return, self.infixString
end

function imageMathCalculator::getInfixTokenStream, Status=status

  self.input = self.infixString
  tokenStream = list()
  while 1 do begin
   token = self.nextToken(Status=status)
   if status.error then begin
     return, !null
   endif
   if status.done then begin
     break
   endif
   tokenStream.add, token
  endwhile

; find function arity
  openBrackets = 0
  stack = list()
  ptrOutput = list()
  foreach token,tokenStream,i do begin
  ;  token = tokenStream[i]
    if token.isLeftBracket then begin
      openBrackets++
      continue
    endif
    if token.isFunction then begin
      ptrOutput.add, ptr_new(0)
      stack.add, ptrOutput[-1]
    endif
    if token.isComma then begin
      *(stack[-1])++
    endif
    if token.isRightBracket then begin
      ; If right bracket belongs to a function 
      if openBrackets eq n_elements(stack) then begin
        if ~(tokenStream[i-2]).isFunction then begin
          if n_elements(stack) ne 0 then begin
            *(stack[-1])++
          endif else begin
            status.error = 1
            status.message = 'Parse error.'
            return, !null
          endelse
        endif
        stack.remove
      endif
      openBrackets--
      continue
    endif    
  endforeach

  output = list()
  foreach ptr,ptrOutput,i do begin
 ;   ptr = ptrOutput[i]
    output.add, *ptr
  endforeach 

  ; set function and operator arity
  functionCounter = 0
  foreach token,tokenStream,i do begin
 ;   token = tokenStream[i]
    if token.isFunction then begin
      token.arity = output[functionCounter]
      tokenStream[i] = token
      functionCounter++
    endif
    if token.isOperator then begin
      ; the '-' or '+' operator is unary if
      if ((token.value eq '-') or (token.value eq '+')) && $
        ((i eq 0) || $                                           ; a) it is the first token on the stack
         ((tokenStream[i-1]).isComma) || $                       ; b) it follows an ',' token
         ((tokenStream[i-1]).isLeftBracket) || $                 ; c) it follows an '(' token
         ((tokenStream[i-1]).isOperator)) then begin             ; d) it follows an operator
        token.arity = 1
        token.value = '_'+token.value
        ; operator priority does not change! 
      endif else begin
        token.arity = self.getOperatorArity(token.value)
      endelse
      tokenStream[i] = token
    endif
  endforeach

  return, tokenStream
end

function imageMathCalculator::getReversePolishTokenStream, Status=status

  infixTokenStream = self.getInfixTokenStream(Status=status)
  if status.error then begin
    return, !null
  endif
  
  stack = list()
  output = list()
  
  foreach token, infixTokenStream,i do begin
 ;   token = infixTokenStream[i]
    status = {error:1b, message:''}

    ; If the token is a number token, then push it onto the stack.
    if token.isNumber then begin
      output.add, token
      status.error=0b
    endif
    ; If the token is a variable token, then push it onto the stack.
    if token.isVariable then begin
      output.add, token
      status.error=0b
    endif
    ; If the token is a function token, then push it onto the stack.
    if token.isFunction then begin
      stack.add, token
      status.error=0b
    endif
    ; If the token is a function argument separator (e.g., a comma):
    if token.isComma then begin
      correct = 0b
      while(~stack.hubIsEmpty()) do begin 
        stackToken = stack[-1]
        if stackToken.isLeftBracket then begin
          correct = 1b
          break
        endif else begin
          ; Until the token at the top of the stack is a left bracket,
          ; pop operators off the stack onto the output queue.
          output.add, stack.remove()
        endelse
      endwhile
      ; If no left parentheses are encountered, either the separator was misplaced
      ; or parentheses were mismatched.
      if ~correct then begin
        status.message = 'Error: separator or parentheses mismatched'
        return, !null
      endif
      status.error=0b
    endif
    ; If the token is an operator, op1, then:
    if token.isOperator then begin
      while ~stack.hubIsEmpty() do begin
        stackToken = stack[-1]
        ; While there is an operator token, o2, at the top of the stack
        ; op1 is left-associative and its priority is less than or equal to that of op2,
        ; or op1 has priority less than that of op2,
        ; Let + and ^ be right associative.
        ; Correct transformation from 1^2+3 is 12^3+
        ; The differing operator priority decides pop / push
        ; If 2 operators have equal priority then associativity decides.
        if stackToken.isOperator and $
           ( (token.leftAssociated and (token.priority le stackToken.priority)) or $
             (token.priority lt stackToken.priority)) then begin
          ; Pop o2 off the stack, onto the output queue;
          output.add, stack.remove()
        endif else begin
          break
        endelse
      endwhile
      ; push op1 onto the stack.
      stack.add, token
      status.error=0b
    endif
    ; If the token is a left bracket, then push it onto the stack.
    if token.isLeftBracket then begin
      stack.add, token
      status.error=0b
    endif
    ; If the token is a right bracket:
    if token.isRightBracket then begin
      correct = 0b
      ; Until the token at the top of the stack is a left bracket,
      ; pop operators off the stack onto the output queue
      while ~stack.hubIsEmpty() do begin
        stackToken = stack[-1]
        if stackToken.isLeftBracket then begin
          correct = 1b
          break
        endif else begin
          output.add, stackToken
          stack.remove, -1
        endelse
      endwhile
      ; If the stack runs out without finding a left bracket, then there are mismatched parentheses.
      if ~correct then begin
        status.message = 'Error: parentheses mismatched'
        return, !null
      endif
      ; Pop the left bracket from the stack, but not onto the output queue.
      stack.remove, -1
      ; If the token at the top of the stack is a function token, pop it onto the output queue.
      if ~stack.hubIsEmpty() then begin
        stackToken = stack[-1]
        if stackToken.isFunction then begin
          output.add, stackToken
          stack.remove, -1
        endif
      endif
      status.error=0b
    endif
    ; if token unknown
    if status.error then begin
      status.message = 'Error: unknown token: '+token.value
      return, !null
    endif
    
  endforeach
        
  ; When there are no more tokens to read:
  ; While there are still operator tokens in the stack:
  while ~stack.hubIsEmpty() do begin
    stackToken = stack[-1]
    if stackToken.isLeftBracket or stackToken.isRightBracket then begin
      status.message =  'Error: parentheses mismatched'
      return, !null
    endif
    output.add, stack.remove()
  endwhile

  return, output
end

function imageMathCalculator::emptyToken
  token = {imageMathToken,$
    type:'', value:'', imageMathArgument:obj_new(), $
    isNumber:0b, isVariable:0b, isFunction:0b, isOperator:0b, $
    isComma:0b, isLeftBracket:0b, isRightBracket:0b, $
    arity:0, priority:0, leftAssociated:1b, $
    isImageMathArgument:0b}
  return, token
end

function imageMathCalculator::parseToken, regex, Length=length
  substring = stregex(self.input, regex, /EXTRACT, /FOLD_CASE)
  if substring eq '' then begin
    return, !null
  endif else begin
    length = strlen(substring)
    substring = strtrim(substring, 2)
    return, substring
  endelse
end

function imageMathCalculator::nextToken, Status=status
  
  status = {done:0b, error:0b, message:''}
  
  ; remove leading withspace
  self.input = strtrim(self.input,1)

  ; return if end of input 
  if self.input eq '' then begin
    status.done = 1b
    return, !null
  endif

  digit = '(0|1|2|3|4|5|6|7|8|9)'
  integer = digit+'+'
  floating = integer+'\.'+digit+'*'
  
  operator = ['\^','--','\+\+','\*','/','MOD ','\+','-','<','>','NOT ','~','EQ ','NE ','LE ','LT ','GE ','GT ','AND ','OR ','XOR ','&&','\|\|']
  operator = '('+strjoin(operator,'|')+')'
  
  variableIdentifier = '(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z)(_|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|0|1|2|3|4|5|6|7|8|9)*
  functionIdentifier = variableIdentifier+'\('
  imageMathNumber = obj_new()
  
  ; try function argument separator
  value = self.parseToken(Length=length, '^,')
  if isa(value) then begin
    type = 'comma'
    goto, tokenFound
  endif
  ; try left bracket
  value = self.parseToken(Length=length, '^\(')
  if isa(value) then begin
    type = 'leftBracket'
    goto, tokenFound
  endif
  ; try right bracket
  value = self.parseToken(Length=length, '^\)')
  if isa(value) then begin
    type = 'rightBracket'
    goto, tokenFound
  endif
  ; try operator
  value = self.parseToken(Length=length, '^'+operator)
  if isa(value) then begin
    type = 'operator'
    goto, tokenFound
  endif
  ; try function
  value = self.parseToken(Length=length, '^'+functionIdentifier)
  if isa(value) then begin
    type = 'function'
    length--
    value = strmid(value, 0, length) ; keep the bracket
    goto, tokenFound
  endif
  ; try variable
  value = self.parseToken(Length=length, '^'+variableIdentifier)
  if isa(value) then begin
    type = 'variable'
    goto, tokenFound
  endif
  ; try floating double precision
  value = self.parseToken(Length=length, '^'+integer+'d')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(double(value))
    goto, tokenFound
  endif
  ; try floating double precision
  value = self.parseToken(Length=length, '^'+floating+'d')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(double(value))
    goto, tokenFound
  endif
  ; try floating single precision
  value = self.parseToken(Length=length, '^'+floating)
  if isa(value) then begin
    type = 'number_float'
    imageMathNumber = imageMathArgumentNumber(float(value))
    goto, tokenFound
  endif
  ; try byte integer
  value = self.parseToken(Length=length, '^'+integer+'b')
  if isa(value) then begin
    type = 'number_byte'
    imageMathNumber = imageMathArgumentNumber(byte(fix(value)))
    goto, tokenFound
  endif
  ; try long64 integer
  value = self.parseToken(Length=length, '^'+integer+'ll')
  if isa(value) then begin
    type = 'number_long64'
    imageMathNumber = imageMathArgumentNumber(long64(value))
    goto, tokenFound
  endif
  ; try long integer
  value = self.parseToken(Length=length, '^'+integer+'l')
  if isa(value) then begin
    type = 'number_long'
    imageMathNumber = imageMathArgumentNumber(long(value))
    goto, tokenFound
  endif
  ; try ulong64 integer
  value = self.parseToken(Length=length, '^'+integer+'ull')
  if isa(value) then begin
    type = 'number_ulong64'
    imageMathNumber = imageMathArgumentNumber(ulong64(value))
    goto, tokenFound
  endif
  ; try ulong integer
  value = self.parseToken(Length=length, '^'+integer+'ul')
  if isa(value) then begin
    type = 'number_ulong'
    imageMathNumber = imageMathArgumentNumber(ulong(value))  
    goto, tokenFound
  endif
  ; try ushort integer
  value = self.parseToken(Length=length, '^'+integer+'us')
  if isa(value) then begin
    type = 'number_uint'
    imageMathNumber = imageMathArgumentNumber(uint(value))
    goto, tokenFound
  endif
  ; try ushort integer (without explicit 's')
  value = self.parseToken(Length=length, '^'+integer+'u')
  if isa(value) then begin
    type = 'number_uint'
    imageMathNumber = imageMathArgumentNumber(uint(value))
    goto, tokenFound
  endif
  ; try short integer
  value = self.parseToken(Length=length, '^'+integer+'s')
  if isa(value) then begin
    type = 'number_int'
    imageMathNumber = imageMathArgumentNumber(fix(value))
    goto, tokenFound
  endif
  ; try integer and choose the smallest type (but not byte)
  value = self.parseToken(Length=length, '^'+integer)
  if isa(value) then begin
    value = long64(value)
    type = 'number_int'
    fixType = 2
    if value gt 32767 then begin
      type = 'number_long'
      fixType = 3
    endif
    if value gt 2147483647 then begin
      type = 'number_long64'
      fixType = 14
    endif
    imageMathNumber = imageMathArgumentNumber(fix(value,TYPE=fixType))
    goto, tokenFound
  endif
  ; try predefined constants 
  ; float PI
  value = self.parseToken(Length=length, '^!Pi')
  if isa(value) then begin
    type = 'number_float'
    imageMathNumber = imageMathArgumentNumber(!PI)
    goto, tokenFound
  endif
  ; double PI
  value = self.parseToken(Length=length, '^!dPi')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(!DPI)
    goto, tokenFound
  endif
  ; float degree to radians factor
  value = self.parseToken(Length=length, '^!D2R')
  if isa(value) then begin
    type = 'number_float'
    imageMathNumber = imageMathArgumentNumber(2.*!pi/360.)
    goto, tokenFound
  endif
  ; double degree to radians factor
  value = self.parseToken(Length=length, '^!dD2R')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(2d*!dpi/360d)
    goto, tokenFound
  endif
  ; float radians to degree factor
  value = self.parseToken(Length=length, '^!R2D')
  if isa(value) then begin
    type = 'number_float'
    imageMathNumber = imageMathArgumentNumber(360./2./!pi)
    goto, tokenFound
  endif
  ; double radians to degree factor
  value = self.parseToken(Length=length, '^!dR2D')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(360d/2d/!dpi)
    goto, tokenFound
  endif
  ; float infinity
  value = self.parseToken(Length=length, '^!Inf')
  if isa(value) then begin
    type = 'number_float'
    imageMathNumber = imageMathArgumentNumber(!VALUES.F_INFINITY)
    goto, tokenFound
  endif
  ; double infinity
  value = self.parseToken(Length=length, '^!dInf')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(!VALUES.D_INFINITY)
    goto, tokenFound
  endif
  ; float not a number
  value = self.parseToken(Length=length, '^!NaN')
  if isa(value) then begin
    type = 'number_float'
    imageMathNumber = imageMathArgumentNumber(!VALUES.F_NAN)
    goto, tokenFound
  endif
  ; double not a number
  value = self.parseToken(Length=length, '^!dNaN')
  if isa(value) then begin
    type = 'number_double'
    imageMathNumber = imageMathArgumentNumber(!VALUES.D_NAN)
    goto, tokenFound
  endif

  ; no token found, return error
  status.error = 1b
  status.message = 'Parse error at: "'+self.input+'"'
  return, !null
  
  ; if token found...
  tokenFound:
  ; update the remaining input string
  self.input = strmid(self.input, length)
  ; prepare result token
  token = self.emptyToken()
  token.type = type
  token.value = value
  if isa(imageMathNumber) then begin
    token.imageMathArgument = imageMathNumber
    token.isImageMathArgument = 1b
  endif
  ; define operator priority and associativity
  token.priority = -self.getOperatorPriority(token.value)
  token.leftAssociated =1b ; in IDL all Operators of equal priority are evaluated from left to right
  ; define some helpful flags
  token.isNumber = strcmp(type, 'number', 6)
  token.isVariable = strcmp(type, 'variable')
  token.isFunction = strcmp(type, 'function')
  token.isOperator = strcmp(type, 'operator')
  token.isComma = strcmp(type, 'comma')
  token.isLeftBracket = strcmp(type, 'leftBracket')
  token.isRightBracket = strcmp(type, 'rightBracket')

  return, token
end

function imageMathCalculator::getOperatorPriority, operator
  
  case strupcase(operator) of
    '^'  : priority = 3
    '++' : priority = 3
    '--' : priority = 3
    '*'  : priority = 3
    '/'  : priority = 4
    'MOD': priority = 4
    '+'  : priority = 5
    '-'  : priority = 5
    '<'  : priority = 5
    '>'  : priority = 5
    'NOT': priority = 5
    '~'  : priority = 5
    'EQ' : priority = 6
    'NE' : priority = 6
    'LE' : priority = 6
    'LT' : priority = 6
    'GE' : priority = 6
    'GT' : priority = 6
    'AND': priority = 7
    'OR' : priority = 7
    'XOR': priority = 7
    '&&' : priority = 8
    '||' : priority = 8
    else : priority = 0
  endcase 
  
  return, priority
end

function imageMathCalculator::getOperatorArity, operator
  
  case strupcase(operator) of
    '^'  : arity = 2
    '++' : arity = 1
    '--' : arity = 1
    '*'  : arity = 2
    '/'  : arity = 2
    'MOD': arity = 2
    '+'  : arity = 2
    '-'  : arity = 2
    '<'  : arity = 2
    '>'  : arity = 2
    'NOT': arity = 1
    '~'  : arity = 1
    'EQ' : arity = 2
    'NE' : arity = 2
    'LE' : arity = 2
    'LT' : arity = 2
    'GE' : arity = 2
    'GT' : arity = 2
    'AND': arity = 2
    'OR' : arity = 2
    'XOR': arity = 2
    '&&' : arity = 2
    '||' : arity = 2
    else : arity = 0
  endcase 
  
  return, arity
end

function imageMathCalculator::execute, Status=status
  
  ; get reverse polish token stream
  reversePolishTokenStream = self.getReversePolishTokenStream(Status=status)
  if status.error then begin
    return, !null
  endif
  
  status = {error:0b, message:''}
  
  ; replace variable indentifiers by imageMathArguments
  foreach token, reversePolishTokenStream, i do begin
  ;  token = reversePolishTokenStream[i]
    if token.isVariable then begin
      if ~(self.variables).hasKey(token.value) then begin
        status.error = 1b
        status.message = 'imageMath input argument not defined: '+token.value
        return, !null
      endif
      variable = (self.variables)[token.value]
      case variable.type of
        'image' : begin
          imageMathArgument = imageMathArgumentImage()
        end
        'band' : begin
          imageMathArgument = imageMathArgumentBand()
          imageMathArgument.setBand, variable.band
        end
        'profile' : begin
          imageMathArgument = imageMathArgumentProfile()
          imageMathArgument.setSample, variable.sample
          imageMathArgument.setLine, variable.line
        end
        'number' : begin
          imageMathArgument = imageMathArgumentNumber()
          imageMathArgument.setSample, variable.sample
          imageMathArgument.setLine, variable.line
          imageMathArgument.setBand, variable.band
        end
      endcase
      imageMathArgument.setImage, variable.filename
      imageMathArgument.setName, variable.type
      token.type = 'imageMathArgument'
      token.isVariable = 0b
      token.isNumber = 0b
      token.isImageMathArgument = 1b
      token.imageMathArgument = imageMathArgument
      reversePolishTokenStream[i] = token     
    endif
  endforeach
  
  ; execute expression
  stack = list()
  output = list()
  status = {error:1b, message:''}
;  foreach token, reversePolishTokenStream,i do begin
  for i=0, n_elements(reversePolishTokenStream)-1 do begin
    token = reversePolishTokenStream[i]
    ; If the token is a number or an variable
    if token.isImageMathArgument then begin
      ; Push it onto the stack.
      stack.add, token
    endif
    ; Otherwise, the token is an operator  (operator here includes both operators, and functions).
    if token.isOperator or token.isFunction then begin
      ; It is known a priori that the operator takes n arguments.
      nargs = token.arity
      ; If there are fewer than n values on the stack
      if n_elements(stack) lt nargs then begin
        ; (Error) The user has not input sufficient values in the expression.
        status.message = 'Error: insufficient input values for operator/function: '+token.value
        return, !null;
      endif
      ; Else, Pop the top n values from the stack.
      ; Evaluate the operator, with the values as arguments.
      inputs = list()
      for dummy=0,nargs-1 do begin
        inputToken = stack.remove()
        inputs.add, inputToken.imageMathArgument, 0
      endfor
      
      ; calculate the result
      if token.isOperator then begin
        operatorName = token.value
        outputImageMathArgument = self.executeOperator(operatorName, inputs)
      endif
      if token.isFunction then begin
        functionName = token.value
        outputImageMathArgument = self.executeFunction(functionName, inputs)
      endif
      outputImageMathArgument.setName, '<'+outputImageMathArgument.getType()+'>'
      
      ; create resultToken from output imageMathArgument
      resultToken = self.emptyToken()
      resultToken.isImageMathArgument = 1b
      resultToken.imageMathArgument = outputImageMathArgument
      resultToken.type = 'imageMathArgument'

      ; Push the returned result back onto the stack.
      stack.add, resultToken
    endif
    
  endfor
;  endforeach
 
  ; If there is only one value in the stack
  ; That value is the result of the calculation.
  if n_elements(stack) eq 1 then begin
    result = stack.remove()
    ; copy to result filename
    if ~isa(result.imageMathArgument, 'imageMathArgumentNumber') then begin
      image = result.imageMathArgument.getImage()
      filenameData = image.getMeta('filename data')
      filenameHeader = image.getMeta('filename header')
      
      if ~(self.variables).hasKey('$resultFilename$') then begin
        hubAMW_program, *self.groupLeader, Title='Image Math Calculator: Enter Variables'
        hubAMW_frame, Title='Output'
        hubAMW_outputFilename, '$resultFilename$', Title='Result Image', Value='imageMathResult'
        amwResult = hubAMW_manage()
        if ~amwResult['accept'] then return, !null
        (self.variables)['$resultFilename$'] = amwResult['$resultFilename$']
        ;(self.variables)['$resultFilename$'] = imageMath_getTemporaryFilename()
      endif
      
      resultFilenameData = (self.variables)['$resultFilename$']
      resultFilenameHeader = resultFilenameData+'.hdr'
      file_copy, filenameData, resultFilenameData, /NOEXPAND_PATH, /OVERWRITE
      file_copy, filenameHeader, resultFilenameHeader, /NOEXPAND_PATH, /OVERWRITE
      result.imageMathArgument.setImage, resultFilenameData, /SetName 
    endif
    status.error = 0b
    return, result
  endif else begin
    ; If there are more values in the stack
    ; (Error) The user input has too many values.
    status.message = 'Error: too many input values.'
    return, !null
  endelse
    
  ; delete temporary files
  tmpDir = file_dirname(imageMath_getTemporaryFilename())
  file_delete, tmpDir, /ALLOW_NONEXISTENT, /RECURSIVE

end

function imageMathCalculator::executeOperator, operator, inputs
  return, self.executeFunction(operator, inputs, /IsOperator)
end

function imageMathCalculator::executeFunction, functionName, inputs, IsOperator=isOperator

  ; if function is an operator, then the function class is "ValueWise"
  if keyword_set(isOperator) then begin
    functionClass = 'ValueWise'

  endif else begin
    
    ; throw error, if function is undefined
    functionInfo = hubRoutineInfo(functionName, /FUNCTIONS)
    if functionInfo.missing then begin
      ; check if function name is a short-cut
      functionInfo2 = hubRoutineInfo('imageMathFunction_'+functionName, /FUNCTIONS)
      if functionInfo2.missing then begin
        message, 'unknown function: '+functionName, /NONAME
      endif else begin
        functionName = 'imageMathFunction_'+functionName
      endelse
    endif  
    
    ; otherwise the function class is given by the function class reflection
    functionClass = imageMathFunctionReflection(functionName, /Class)
   
  endelse
  
  f = obj_new('imageMathFunction'+functionClass)
  f.setFunctionName, functionName
  f.setProcessingFunctionName, functionName, IsOperator=isOperator
  f.setNumberOfInputs, n_elements(inputs)
  for i=0,n_elements(inputs)-1 do begin
    f.setInput, inputs[i], i
  endfor
;  return, f.getResult(self.getDataIgnoreValue())
  return, f.getResult()
  
end

pro imageMathCalculator__define
  struct = {imageMathCalculator $
    , inherits IDL_Object $
    , infixString : '' $
    , input : '' $
    , variables : hash() $
    , dataIgnoreValue : ptr_new() $
    , groupLeader : ptr_new()}
end

pro test_imageMathCalculator, infixString
  
  variables = hash()
  variables['img1'] = {type:'image', filename:hub_getTestImage('Hymap_Berlin-A_Image')}
  variables['img2'] = {type:'image', filename:hub_getTestImage('Hymap_Berlin-B_Image')}
    
  t = imageMathCalculator()
  t.setInfixString, '1-1'
  t.setVariables, variables
    
  if isa(infixString) then begin
      t.setInfixString, infixString
  endif
  
  ; execute
  
  result = t.execute(Status=status)
  if status.error then begin
    print, status.message
  endif else begin
    print, 'Result:'
    if strcmp(typename(result.imageMathArgument), 'imageMathArgumentNumber', /FOLD_CASE) then begin
      value = result.imageMathArgument.getValue()
      print, t.getInfixString()+' = '+strtrim(value, 2)+' ('+strlowcase(typename(value))+')'
    endif else begin
      print, result.imageMathArgument
      result.imageMathArgument.quickLook
    endelse
  endelse

end