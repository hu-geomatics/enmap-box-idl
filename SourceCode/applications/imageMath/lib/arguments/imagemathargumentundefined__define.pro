function imageMathArgumentUndefined::init
  self.setName, '<undefined>'
  self.isSpatial = 0b
  self.isSpectral = 0b
  return, 1b
end

function imageMathArgumentUndefined::_overloadPrint
  result = self.getName()
  return, result
end

pro imageMathArgumentUndefined__define
  struct = {imageMathArgumentUndefined $
    , inherits imageMathArgument $
  }

end

pro test_imageMathArgumentUndefined
  arg = imageMathArgumentUndefined()
  print, arg
end
