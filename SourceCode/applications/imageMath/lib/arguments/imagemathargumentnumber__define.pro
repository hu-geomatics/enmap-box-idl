function imageMathArgumentNumber::init, value
  self.setName, '<number>'
  self.isSpatial = 0b
  self.isSpectral = 0b
  if isa(value) then begin
    self.setValue, value
  endif
  return, 1b
end

pro imageMathArgumentNumber::setValue, value
  ; create pseudo image
  filename = imageMath_getTemporaryFilename()
  hubIOImg_writeImage, reform(value,1,1,1), filename, /NoOpen
  self.setImage, filename
end

function imageMathArgumentNumber::getValue
  result = hubIOImg_readImage(self.image.getMeta('filename data'))
  result = result[self.samplePosition, self.linePosition, self.bandPosition]
  return, result
end

pro imageMathArgumentNumber::setBand, bandPosition
  self.bandPosition = bandPosition
end

function imageMathArgumentNumber::getBand
  return, self.bandPosition
end

pro imageMathArgumentNumber::quickLook
  xdisplayfile, TEXT=[string(self.getValue())], TITLE='Number'
end

function imageMathArgumentNumber::_overloadPrint
  value = self.getValue()
  if size(/TNAME, value) eq 'BYTE' then begin
    value = fix(value)
  endif
  result = strtrim(value, 2)
  return, result
end

pro imageMathArgumentNumber__define
  struct = {imageMathArgumentNumber $
    , inherits imageMathArgumentProfile $
    , bandPosition : 0ll $
  }

end

pro test_imageMathArgumentNumber
  input = imageMathArgumentNumber(1d)
  input.quickLook
  print, input
end