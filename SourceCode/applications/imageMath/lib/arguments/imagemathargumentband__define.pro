function imageMathArgumentBand::init
  self.setName, '<band>'
  self.isSpatial = 1b
  self.isSpectral = 0b
  return, 1b
end

pro imageMathArgumentBand::setBand, bandPosition
  self.bandPosition = bandPosition
end

function imageMathArgumentBand::getBand
  return, self.bandPosition
end

pro imageMathArgumentBand::quickLook
  self.imageMathArgumentImage::quickLook, self.bandPosition
end

pro imageMathArgumentBand__define
  struct = {imageMathArgumentBand $
    , inherits imageMathArgumentImage $
    , bandPosition : 0ll $
  }
end

pro test_imageMathArgumentBand

  input = imageMathArgumentBand()
  input.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName
  print, input
end