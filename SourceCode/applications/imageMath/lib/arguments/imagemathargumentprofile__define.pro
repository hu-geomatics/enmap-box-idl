function imageMathArgumentProfile::init
  self.setName, '<profile>'
  self.isSpatial = 0b
  self.isSpectral = 1b
  return, 1b
end

pro imageMathArgumentProfile::setSample, samplePosition
  self.samplePosition = samplePosition
end

function imageMathArgumentProfile::getSample
  return, self.samplePosition
end

pro imageMathArgumentProfile::setLine, linePosition
  self.linePosition = linePosition
end

function imageMathArgumentProfile::getLine
  return, self.linePosition
end

pro imageMathArgumentProfile::quickLook
  self.image.quickLookProfile, self.samplePosition, self.linePosition
end

pro imageMathArgumentProfile__define
  struct = {imageMathArgumentProfile $
    , inherits imageMathArgumentImage $
    , samplePosition : 0ll $
    , linePosition : 0ll $
  }
end

pro test_imageMathArgumentProfile

  input = imageMathArgumentProfile()
  input.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName
  input.setSample, 150
  input.setLine, 200
  print, input
end