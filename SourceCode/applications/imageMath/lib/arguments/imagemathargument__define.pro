function imageMathArgument::init
  self.setName, '<argument>'
  self.isSpatial = 0b
  self.isSpectral = 0b
  return, 1b
end

pro imageMathArgument::setName, name
  self.name = name
end

function imageMathArgument::getName
  return, self.name
end

function imageMathArgument::getSize
  message, 'Abstract methode.'
end

function imageMathArgument::getSizeString
  size = self.getSize()
  result = isa(size) ? strcompress(/REMOVE_ALL, size) : ['?','?','?']
  result = result[where(/NULL, [self.isSpatial, self.isSpatial, self.isSpectral])]
  result = '['+strjoin(result,',')+']'
  return, result
end

function imageMathArgument::isSpatial
  return,self.isSpatial
end

function imageMathArgument::isSpectral
  return,self.isSpectral
end

function imageMathArgument::isInitialized
  return, 0b
end

function imageMathArgument::getType
  result = strlowcase((strsplit(typename(self),'_',/EXTRACT))[-1])
  return, result
end

function imageMathArgument::_overloadPrint
  result = self.getName()+self.getSizeString()
  if isa(self, 'imageMathArgumentImage') then begin
    result = [result, self.image.getMeta('filename data')]
  endif
  return, result
end

pro imageMathArgument__define
  struct = {imageMathArgument $
    , inherits IDL_Object $
    , name: '' $
    , isSpatial : 0b $
    , isSpectral : 0b $
  }

end

pro test_imageMathArgument

  a = imageMathArgumentUndefined()
  print,a  

end