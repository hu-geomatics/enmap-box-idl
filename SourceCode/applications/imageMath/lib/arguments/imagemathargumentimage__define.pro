function imageMathArgumentImage::init
  self.setName, '<image>'
  self.isSpatial = 1b
  self.isSpectral = 1b
  return, 1b
end

pro imageMathArgumentImage::setImage, filename, SetName=setName
  self.image = hubIOImgInputImage(filename)
  if keyword_set(setName) then begin
    self.setName, file_basename(self.image.getMeta('filename data'))
  endif
end

function imageMathArgumentImage::getImage
  return, self.image
end

function imageMathArgumentImage::getSize
  if self.isInitialized() then begin
    ; get image size
    result = [self.image.getSpatialSize(), self.image.getSpectralSize()]
    ; mask dimensions
    dimensionMask = [0,0,0]
    dimensionMask or= self.isSpatial()  ? [1,1,0] : 0
    dimensionMask or= self.isSpectral() ? [0,0,1] : 0
    result[where(/NULL, ~dimensionMask)] = 1
  endif else begin
    result = !null
  endelse
  return, result
end

function imageMathArgumentImage::isInitialized
  return, isa(self.image, 'hubIOImgInputImage')
end

pro imageMathArgumentImage::quickLook, bandPositions
  image = self.getImage()
  if self.isInitialized() then begin
    if ~isa(bandPositions) then begin
      if image.hasMeta('default bands') then begin
        bandPositions = image.getMeta('default bands')
      endif else begin
        if image.hasMeta('wavelength') then begin
          bandPositions = image.locateWavelength(/TrueColor)
        endif else begin
          bandPositions = 0
        endelse
      endelse
    endif
    image.quickLook, bandPositions
  endif
end

pro imageMathArgumentImage::showHeader
  self.image.showHeader
end

pro imageMathArgumentImage__define
  struct = {imageMathArgumentImage $
    , inherits imageMathArgument $
    , image : obj_new() $
  }

end

pro test_imageMathArgumentImage

  inputImage = imageMathArgumentImage()
  inputImage.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName
  print, inputImage
  inputImage.quickLook,0
  inputImage.showHeader
end