function imageMathFunctionExternal::getArguments
  arguments = list()
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    if isa(input, 'imageMathArgumentNumber') then begin
      argument = (input.getValue())[0]
    endif else begin
      argument = (input.getImage()).getMeta('filename data')
    endelse
    arguments.add, argument
  endfor
  return, arguments
end

function imageMathFunctionExternal::getResult
  resultFilename = imageMath_getTemporaryFilename()
  arguments = self.getArguments()
  dummy = self.callProcessingFunction(arguments, ResultFilename=resultFilename)
  reflection = hash()+imageMathFunctionReflection(self.functionName)
  if reflection.hubKeywordSet(strupcase('returnsNumber')) then begin
    result = imageMathArgumentNumber()
  endif
  if reflection.hubKeywordSet(strupcase('returnsProfile')) then begin
    result = imageMathArgumentProfile()
  endif

  if ~isa(result) then begin
    result = imageMathArgumentImage()
  endif
  result.setImage, resultFilename, /SetName
  return, result
end

pro imageMathFunctionExternal__define
  struct = {imageMathFunctionExternal $
    , inherits imageMathFunction}
end

pro test_imageMathFunctionExternal

  image = imageMathArgumentImage()
  image.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName
  
  f = imageMathFunctionExternal()
  f.setFunctionName, 'Savitzky-Golay Filtering'
  f.setProcessingFunctionName, 'imageMathFunction_savitzkyGolayApplication'

  f.setNumberOfInputs, 4
  f.setInput, image, 0
  f.setInput, imageMathArgumentNumber(11), 1 ; width
  f.setInput, imageMathArgumentNumber(1),  2 ; order
  f.setInput, imageMathArgumentNumber(3),  3 ; degree

  result = f.getResult()

  print, result
  result.showHeader
  result.quickLook
  enmapbox
  enmapBox_openImage, (result.getImage()).getMeta('filename data')
  

end