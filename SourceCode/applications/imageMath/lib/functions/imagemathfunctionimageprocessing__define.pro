function imageMathFunctionImageProcessing::isConsistentInput, Message=message

  ; first input must be an image
  if ~strcmp(typename(self.getInput(0)), 'imageMathArgumentImage', /FOLD_CASE) then begin
    message = 'First argument must be an image.'
    return, 0b
  endif
  
  ; all other arguments must be numbers
  for i=1,self.getNumberOfInputs()-1 do begin
    if ~strcmp(typename(self.getInput(i)), 'imageMathArgumentNumber', /FOLD_CASE) then begin
      message = 'All arguments, except the first, must be numbers.'
      return, 0b
    endif
  endfor
  
  return, 1b
end

function imageMathFunctionImageProcessing::getSize
  if self.isConsistentInput() then begin
    input = self.getInput(0)
    result = input.getSize()
    result[2] = self.reflectSpectralSize()
  endif else begin
    result = !null
  endelse
  return, result
end

function imageMathFunctionImageProcessing::getNeighborhoodHeight
  result = imageMathFunctionReflection(self.processingFunctionName, self.getArguments(!null), /NeighborhoodHeight)
  return, result
end

function imageMathFunctionImageProcessing::getOneNeighborhoodCube
  input = self.getInput(0)
  inputImage = input.getImage()
  neighborhoodSize = [inputImage.getMeta('samples'), self.getNeighborhoodHeight()]
  bands = inputImage.getMeta('bands')
  oneNeighborhood = make_array(neighborhoodSize[0], neighborhoodSize[1], bands, VALUE=1, TYPE=inputImage.getMeta('data type'))
  !null = reform(/OVERWRITE, oneNeighborhood, neighborhoodSize[0], neighborhoodSize[1], bands)
  return, oneNeighborhood
end

function imageMathFunctionImageProcessing::reflectSpectralSize
  if self.isConsistentInput() then begin
    oneNeighborhoodCube = self.getOneNeighborhoodCube()
    processingResult = self.calculate(oneNeighborhoodCube, self.getKeywords())
    result = n_elements(processingResult[0,0,*])
  endif else begin
    result = !null
  endelse
  return, result
end

function imageMathFunctionImageProcessing::reflectDataType
  if self.isConsistentInput() then begin
    oneNeighborhoodCube = self.getOneNeighborhoodCube()
    processingResult = self.calculate(oneNeighborhoodCube, self.getKeywords())
    result = typename(processingResult)
  endif else begin
    result = !null
  endelse
  return, result
end

function imageMathFunctionImageProcessing::getArguments, data
  arguments = list(ptr_new(data, /NO_COPY))
  for i=1,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    number = (input.getValue())[0]
    arguments.add, number
  endfor
  return, arguments
end

function imageMathFunctionImageProcessing::getKeywords
  input = self.getInput(0)
  inputImage = input.getImage()
  inputHeader = inputImage.getHeader()
  outputHeader = hubIOImgHeader()
  keywords = {inputHeader:inputHeader, outputHeader:outputHeader}
  return, keywords
end

function imageMathFunctionImageProcessing::calculate, data, keywords

  inputDimensions = size(/DIMENSIONS, data)
  arguments = self.getArguments(data)
  result = self.callProcessingFunction(arguments, _EXTRA=keywords)

  ; repair potential dimension loss

  resultDimensions = size(/DIMENSIONS, result)
  case n_elements(resultDimensions) of
    3 : ; everything is ok, do nothing
    2 : !null = reform(/OVERWRITE, result, [resultDimensions, 1])
    1 : !null = reform(/OVERWRITE, result, [resultDimensions, 1, 1])
    else : message, 'unexpected calculation error'
  end
  resultDimensions = size(/DIMENSIONS, result)
  if ~hubEQ(/SCALAR, inputDimensions[0:1], resultDimensions[0:1]) then begin
    message, 'Resulting spatial dimension is inconsistent.' 
  endif
  return, result
end

function imageMathFunctionImageProcessing::getResult
  if ~self.isConsistentInput(Message=message) then begin
    message, self.getFunctionName()+': '+message, /NONAME
  endif
  
  
  input = self.getInput(0)
  inputImage = input.getImage()
  
  ; init readers
  numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)
  neighborhoodHeight = self.getNeighborhoodHeight()
  inputImage.initReader, numberOfTileLines, neighborhoodHeight, /Cube, /TileProcessing

  ; init writer
  size = self.getSize()
  dataType = self.reflectDataType()
  writerSettings = inputImage.getWriterSettings(SetDataType=dataType, SetSamples=size[0], SetLines=size[1], SetBands=size[2])
  outputImage = hubIOImgOutputImage(imageMath_getTemporaryFilename(), /NoOpen)
  outputImage.initWriter, writerSettings

  ; perform processing cube tile by cube tile
  keywords = self.getKeywords()
  while ~inputImage.tileProcessingDone() do begin  
    inputData = inputImage.getData()
    resultData = self.calculate(inputData, keywords)
    outputImage.writeData, resultData
  endwhile

  ; get result meta information
  ; - default meta
  metaNames = ['map info', 'coordinate system string']
  outputImage.copyMeta, inputImage, metaNames
  ; - user-defined meta
  (outputImage.getHeader()).copyMeta, keywords.outputHeader, keywords.outputHeader.getMetaNames()
    
  ; finish reader/writer
  inputImage.finishReader
  outputImage.finishWriter
  filename = outputImage.getMeta('filename data')
  obj_destroy, outputImage
  
  ; create result image math argument
  result = imageMathArgumentImage()
  result.setImage, filename, /SetName
  return, result
end

pro imageMathFunctionImageProcessing__define
  struct = {imageMathFunctionImageProcessing $
    , inherits imageMathFunction $
  }
end

pro test_imageMathFunctionImageProcessing

  image = imageMathArgumentImage()
  image.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName
  
  f = imageMathFunctionImageProcessing()
  f.setFunctionName, 'Savitzky-Golay Filtering'
  f.setProcessingFunctionName, 'imageMathFunction_savitzkyGolay'

  f.setNumberOfInputs, 4
  f.setInput, image, 0
  f.setInput, imageMathArgumentNumber(11), 1 ; width
  f.setInput, imageMathArgumentNumber(1),  2 ; order
  f.setInput, imageMathArgumentNumber(3),  3 ; degree

  result = f.getResult()

  print, result
  result.showHeader
  result.quickLook
  enmapbox
  enmapBox_openImage, (result.getImage()).getMeta('filename data')
  

end