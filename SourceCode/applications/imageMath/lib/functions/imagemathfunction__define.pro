function imageMathFunction::init
  self.name = 'result'
  return, 1b
end

pro imageMathFunction::setFunctionName, name
  self.functionName = name
end

function imageMathFunction::getFunctionName, name
  return, self.functionName
end

pro imageMathFunction::setprocessingFunctionName, functionName, IsOperator=isOperator
  
  self.isOperator = keyword_set(isOperator)
  if ~self.isOperator then begin
    routineInfo = hubRoutineInfo(functionName, /Function)
    if routineInfo.missing then begin
      message, 'Missing processing function: '+functionName, /NONAME, /NOPREFIX
    endif
  endif
  self.processingFunctionName = functionName
end

pro imageMathFunction::setNumberOfInputs, numberOfInputs
  inputs = list(imageMathArgumentUndefined(), LENGTH=numberOfInputs)
  self.inputs = inputs
end

function imageMathFunction::getNumberOfInputs
  return, n_elements(self.inputs)
end

pro imageMathFunction::setInput, input, index
  inputs = self.inputs
  for i=0,self.getNumberOfInputs()-1 do begin
    if input eq inputs[i] then begin
      message, 'Input arguments must be unique.'
    endif
  endfor
  inputs[index] = input
end

function imageMathFunction::getInput, index
  return, (self.inputs)[index]
end

function imageMathFunction::isConsistentInput
  return, 0b
end

function imageMathFunction::getSize
  return, !null
end

function imageMathFunction::getResult
  return, imageMathArgumentUndefined()
end

function imageMathFunction::callprocessingFunction, arguments, _EXTRA=_extra

  if n_elements(arguments) ge 1 then begin
    argument0 = arguments[0]
    if isa(argument0, 'pointer') then begin
      argument0 = reform(*argument0, size(/DIMENSIONS, *argument0))
    endif
  endif
  
  case n_elements(arguments) of
    0 : result = call_function(self.processingFunctionName, _EXTRA=_extra)
    1 : result = call_function(self.processingFunctionName, argument0, _EXTRA=_extra)
    2 : result = call_function(self.processingFunctionName, argument0, arguments[1], _EXTRA=_extra)
    3 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], _EXTRA=_extra)
    4 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], arguments[3], _EXTRA=_extra)
    5 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], arguments[3], arguments[4], _EXTRA=_extra)
    6 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], arguments[3], arguments[4], $
                                                        arguments[5], _EXTRA=_extra)
    7 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], arguments[3], arguments[4], $
                                                        arguments[5], arguments[6], _EXTRA=_extra)
    8 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], arguments[3], arguments[4], $
                                                        arguments[5], arguments[6], arguments[7], _EXTRA=_extra)
    9 : result = call_function(self.processingFunctionName, argument0, arguments[1], arguments[2], arguments[3], arguments[4], $
                                                        arguments[5], arguments[6], arguments[7], arguments[8], _EXTRA=_extra)
    else: message, 'Number of arguments is limited to 9.'
  endcase  
  
  return, result
end

function imageMathFunction::_overloadPrint, LeftSide=leftSide, RightSide=rightSide

  result = ''
  if ~keyword_set(rightSide) then begin
    result += self.imageMathArgument::_overloadPrint()
    result += ' = '
  endif
  
  if ~keyword_set(leftSide) then begin
    result += self.getFunctionName()+'('
    inputPrints = list()
    for i=0,self.getNumberOfInputs()-1 do begin
      input = self.getInput(i)
      if isa(input, 'imageMathFunction') then begin
        inputPrints.add, input._overloadPrint(/RightSide)
      endif else begin
        inputPrints.add, input._overloadPrint()
      endelse
    endfor
    if ~inputPrints.hubIsEmpty() then begin
      result += strjoin(inputPrints.ToArray(), ',')
    endif
    result += ')'
  endif
  return, result
end

pro imageMathFunction::cleanup
 
  if obj_valid(self.progressBar) then begin
    obj_destroy, self.progressBar 
  endif
  
end

pro imageMathFunction__define
  struct = {imageMathFunction $
    , inherits imageMathArgument $
    , functionName : '' $
    , processingFunctionName : '' $
    , isOperator : 0b $
    , inputs : list() $
    , output : obj_new() $
    , progressBar : obj_new() $
  }

end

pro test_imageMathFunction

  a = imageMathArgumentImage()
  a.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName

  b = imageMathArgumentImage()
  b.setImage, hub_getTestImage('Hymap_Berlin-B_Image'), /SetName

  tmp1 = imageMathFunctionValueWise()
  tmp1.setNumberOfInputs, 2
  tmp1.setName, 'tmp1'
  tmp1.setInput, a, 0
  tmp1.setInput, a, 1

  tmp2 = imageMathFunctionValueWise()
  tmp2.setNumberOfInputs, 2
  tmp2.setName, 'tmp2'
  tmp2.setInput, b, 0
  tmp2.setInput, b, 1
  
  f = imageMathFunctionValueWise()
  f.setNumberOfInputs, 2
  f.setName, 'result'
  f.setInput, tmp1, 0
  f.setInput, tmp2, 1
      
  c = f.getResult()
  print,f
  print,c
end