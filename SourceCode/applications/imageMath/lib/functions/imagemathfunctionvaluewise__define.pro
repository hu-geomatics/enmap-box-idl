function imageMathFunctionValueWise::isConsistentInput, Message=message
  
  message = !null
  sizeMaxima = self.getSizeMaxima()
  inputSizes = list()
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    
    ; return false if input is undefined
    if isa(input, 'imageMathArgumentUndefined') then begin
      message = 'Input argument is undefined.'
      return, 0b
    endif
    
    ; return false if input is a function with inconsistent input
    if isa(input, 'imageMath_function') then begin
      if ~input.isConsistentInput(Message=message) then begin
        return, 0b
      endif
    endif
    
    ; return false if sizes are not consistent
    size = input.getSize()
    for k=0,2 do begin
      if ~( (size[k] eq 1) or (size[k] eq sizeMaxima[k]) ) then begin
        message = 'Image dimensions do not match.' 
        return, 0b
      endif 
    endfor
  endfor
  
   ; return true otherwise
   return, 1b 
end

function imageMathFunctionValueWise::getSizeMaxima
  result = [1ll,1ll,1ll]
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    size = input.getSize()
    if isa(size) then begin
      result >=  size
    endif
  endfor
  return, result
end

function imageMathFunctionValueWise::getSize
  if self.isConsistentInput() then begin
    result = self.getSizeMaxima()
  endif else begin
    result = !null
  endelse
  return, result
end

function imageMathFunctionValueWise::getDataType
  if self.isConsistentInput() then begin
    ; collect ones in the images data type
    ones = list()
    for i=0,self.getNumberOfInputs()-1 do begin
      input = self.getInput(i)
      inputImage = input.getImage()
      ones.add, fix(1, TYPE=inputImage.getMeta('data type'))
    endfor
    ; apply the processing function to receive the result data type
    processingResult = self.calculate(ones)
    result = typename(processingResult)
  endif else begin
      result = !null
  endelse
  return, result
end

function imageMathFunctionValueWise::calculate, arguments
  
  if self.isOperator then begin
    operator = strupcase(self.processingFunctionName)
    case operator of
      '^'  : result = arguments[0] ^ arguments[1]
      '++' : result = arguments[0]+1b
      '--' : result = arguments[0]-1b
      '*'  : result = arguments[0] * arguments[1]
      '/'  : result = arguments[0] / arguments[1]
      'MOD': result = arguments[0] MOD arguments[1]
      '+'  : result = arguments[0] + arguments[1]
      '-'  : result = arguments[0] - arguments[1]
      '<'  : result = arguments[0] < arguments[1]
      '>'  : result = arguments[0] > arguments[1]
      'NOT': result = NOT arguments[0]
      '~'  : result = ~ arguments[0]
      'EQ' : result = arguments[0] EQ arguments[1]
      'NE' : result = arguments[0] NE arguments[1]
      'LE' : result = arguments[0] LE arguments[1]
      'LT' : result = arguments[0] LT arguments[1]
      'GE' : result = arguments[0] GE arguments[1]
      'GT' : result = arguments[0] GT arguments[1]
      'AND': result = arguments[0] AND arguments[1]
      'OR' : result = arguments[0] OR arguments[1]
      'XOR': result = arguments[0] XOR arguments[1]
      '&&' : result = (arguments[0] eq 1) AND (arguments[1] eq 1) ; workaround, because &&-operator is not vectorized
      '||' : result = (arguments[0] eq 1) OR (arguments[1] eq 1) ; workaround, because ||-operator is not vectorized
      '_-' : result = - arguments[0]
      '_+' : result = + arguments[0]
      else: message, 'Unknown operator: '+operator
    endcase
  endif else begin
    result = self.callProcessingFunction(arguments)
  endelse

  return, result
end

function imageMathFunctionValueWise::getResult, outputDataIgnoreValue
  if ~self.isConsistentInput(Message=message) then begin
    message, message, /NONAME
  endif
  
  ; init readers
  numberOfTileLines = 1
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    inputImage = input.getImage()
    numberOfTileLines >= hubIOImg_getTileLines(Image=inputImage)
  endfor
  
  inputDataIgnoreValues = list()
  neighborhoodHeight = 1
  numberOfTiles = 1
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    inputImage = input.getImage()
    subsetBandPositions = isa(input, 'imageMathArgumentBand') or isa(input, 'imageMathArgumentNumber') ? input.getBand() : !null
    subsetSampleRange = isa(input, 'imageMathArgumentProfile') or isa(input, 'imageMathArgumentNumber') ? replicate(input.getSample(), 2) : !null
    subsetLineRange = isa(input, 'imageMathArgumentProfile') or isa(input, 'imageMathArgumentNumber') ? replicate(input.getLine(), 2) : !null
    tileProcessing = input.isSpatial()
        
    inputImage.initReader, numberOfTileLines, /Cube, $
      TileProcessing=tileProcessing, $
      SubsetBandPositions=subsetBandPositions, SubsetSampleRange=subsetSampleRange, SubsetLineRange=subsetLineRange
    if tileProcessing then begin
      numberOfTiles >= (inputImage.getWriterSettings())['numberOfTiles']
    endif
    inputDataIgnoreValues.add, inputImage.getMeta('data ignore value')
  endfor

  ; init writer
  size = self.getSize()
  dataType = self.getDataType()
  writerSettings = hash()
  writerSettings['samples'] = size[0]
  writerSettings['lines'] = size[1]
  writerSettings['bands'] = size[2]
  writerSettings['data type'] = dataType
  writerSettings['dataFormat'] = 'cube'
  writerSettings['neighborhoodHeight'] = neighborhoodHeight
  writerSettings['tileProcessing'] = 1
  writerSettings['numberOfTiles'] = numberOfTiles

 ; writerSettings = inputImage.getWriterSettings(SetSamples=size[0], SetLines=size[1], SetBands=size[2], SetDataType=dataType)
 ; if writerSettings[
  outputImage = hubIOImgOutputImage(imageMath_getTemporaryFilename(), /NoOpen)
  outputImage.initWriter, writerSettings

  ; perform processing cube tile by cube tile
  for currentTile=1l,numberOfTiles do begin  
    ; read data 
    cubeTiles = list()
    cubeTileSizes = list()
    cubeTileSizeMaxima = [1l,1l,1l]
    for i=0,self.getNumberOfInputs()-1 do begin
      input = self.getInput(i)
      inputImage = input.getImage()
      cubeTile = inputImage.getData()
      cubeTileSizes. add, size(/DIMENSIONS, cubeTile)
      cubeTileSizeMaxima >= cubeTileSizes[i]
      cubeTiles.add, cubeTile, /NO_COPY
    endfor
    ; expand data
    for i=0,self.getNumberOfInputs()-1 do begin
      cubeTileLongSize = (size(cubeTiles[i]))[0:-3]
      if ~hubEQ( cubeTileLongSize, [3,cubeTileSizeMaxima], /SCALAR) then begin
        cubeTile = reform(/OVERWRITE, cubeTiles[i], cubeTileSizes[i])
        cubeTiles[i] = rebin(cubeTile, cubeTileSizeMaxima,/SAMPLE)
      endif
    endfor
    
    ; perform data processing
    resultCubeTile = self.calculate(cubeTiles)
    !null = reform(/OVERWRITE, resultCubeTile, cubeTileSizeMaxima)
    
    ; handle data ignore values
    if isa(outputDataIgnoreValue) then begin
      indices = []
      for i=0,self.getNumberOfInputs()-1 do begin
        if isa(inputDataIgnoreValues[i]) then begin
          indices = [indices, where(/NULL, resultCubeTile eq inputDataIgnoreValues[i])]
        endif
      endfor
      resultCubeTile[indices] = outputDataIgnoreValue
    endif
    
    ; write result
    outputImage.writeData, resultCubeTile                      
  endfor
 
  ;  finish readers
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    inputImage = input.getImage()
    inputImage.finishReader
  endfor
  
  ; get result meta information
  header = self.getResultHeader()
  outputHeader = outputImage.getHeader()
  outputHeader.copyMeta, header, header.getMetaNames()
  if isa(outputDataIgnoreValue) then begin
    outputHeader.setMeta, 'data ignore value', outputDataIgnoreValue
  endif
  
  ; finish writer
  outputImage.finishWriter
  filename = outputImage.getMeta('filename data')
  obj_destroy, outputImage
  
  ; create result image math argument
  result = obj_new(self.getResultClass())
  result.setImage, filename, /SetName
  
  return, result
end

function imageMathFunctionValueWise::getResultHeader
  
  ; copy all potential meta information
  subHeaders = list()
  spatialMetaNames = ['map info', 'coordinate system string']
  spectralMetaNames = ['default bands', 'wavelength units', 'wavelength', 'fwhm', 'spectra names']

  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    inputImage = input.getImage()
    inputHeader = inputImage.getHeader()
    subHeader = hubIOImgHeader()
    if input.isSpatial() then begin
      subHeader.copyMeta, inputHeader, spatialMetaNames
    endif
    if input.isSpectral() then begin
      subHeader.copyMeta, inputHeader, spectralMetaNames
    endif
    subHeaders.add, subHeader
  endfor
    
  ;filter consistent meta information: meta information is only used, if it is equal across all inputs
  resultHeader = hubIOImgHeader()
  metaNames = [spatialMetaNames, spectralMetaNames]
  isConsistents = hash(metaNames, replicate(1b, n_elements(metaNames)))
  for i=0,self.getNumberOfInputs()-1 do begin
    subHeader = subHeaders[i]
    for k=0,n_elements(metaNames)-1 do begin
      metaName = metaNames[k]
      subMeta = subHeader.getMetaObj(metaName)
      if ~subMeta.isDefined() then begin
        continue
      endif
      resultMeta = resultHeader.getMetaObj(metaName)
      if resultMeta.isDefined() then begin
        if ~resultMeta.isEqual(subMeta) then begin
          isConsistents[metaName] = 0b
        endif
      endif else begin
        resultHeader.copyMeta, subHeader, metaName
      endelse 
    endfor
  endfor

  for i=0,n_elements(metaNames)-1 do begin
    metaName = metaNames[i]
    if ~isConsistents[metaName] then begin
      resultHeader.setMeta, metaName, !null
    endif
  endfor
  return, resultHeader
end

function imageMathFunctionValueWise::getResultClass

  ; single image makes the result an image
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    if strcmp(typename(input), 'imageMathArgumentImage', /FOLD_CASE) then begin
      return, 'imageMathArgumentImage'
    endif
  endfor
  
  ; single band+profile compination makes the result an image
  bandPresent = 0b
  profilePresent = 0b
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    bandPresent or= strcmp(typename(input), 'imageMathArgumentBand', /FOLD_CASE)
    profilePresent or= strcmp(typename(input), 'imageMathArgumentProfile', /FOLD_CASE)
  endfor
  if bandPresent and profilePresent then begin
    return, 'imageMathArgumentImage'
  endif
  
  ; single band makes the result a band
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    if strcmp(typename(input), 'imageMathArgumentBand', /FOLD_CASE) then begin
      return, 'imageMathArgumentBand'
    endif
  endfor
  
  ; single profile makes the result a profile
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    if strcmp(typename(input), 'imageMathArgumentProfile', /FOLD_CASE) then begin
      return, 'imageMathArgumentProfile'
    endif
  endfor
  
  ; all numbers makes the result a number
  allNumbers = 1b
  for i=0,self.getNumberOfInputs()-1 do begin
    input = self.getInput(i)
    allNumbers and= strcmp(typename(input), 'imageMathArgumentNumber', /FOLD_CASE)
  endfor
  if allNumbers then begin
    return, 'imageMathArgumentNumber'
  endif
  
  ; otherwise somethink is wrong
  message, 'Can not determine the result type.'

end

pro imageMathFunctionValueWise__define
  struct = {imageMathFunctionValueWise $
    , inherits imageMathFunction $
  }
end

pro test_imageMathFunctionValueWise
  i1 = imageMathArgumentImage()
  i1.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName

  i2 = imageMathArgumentImage()
  i2.setImage, hub_getTestImage('Hymap_Berlin-B_Image'), /SetName

  b = imageMathArgumentBand()
  b.setImage, hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth'), /SetName
  b.setBand, 0
  
  p = imageMathArgumentProfile()
  p.setImage, hub_getTestImage('Hymap_Berlin-A_Image'), /SetName
  p.setSample, 200
  p.setLine, 145
  
;  b.quickLook
;  p.quickLook
;  return
  f = imageMathFunctionValueWise()
  f.setFunctionName, 'additon'
  f.setProcessingFunctionName, 'hubIDLValueWise_addition'
  f.setNumberOfInputs, 2
  f.setInput, i1, 0
  f.setInput, i2, 1
  c = f.getResult()

  print, c
  c.showHeader
  c.quickLook
  

end