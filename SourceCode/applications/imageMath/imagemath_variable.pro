function imageMath_variable, filename, Band=band, Position=position

  type = 'image'
  if isa(band) then begin
    type = 'band'
  endif
  if isa(position) then begin
    type = 'profile'
  endif
  if isa(band) && isa(position) then begin
    type = 'number'
  endif

  if isa(position) then begin
    sample = position[0]
    line = position[1]
  endif
  
  case type of
    'number'  : result = {type:'number',  filename:filename, sample:sample, line:line, band:band}
    'band'    : result = {type:'band',    filename:filename,                           band:band}
    'profile' : result = {type:'profile', filename:filename, sample:sample, line:line}
    'image'   : result = {type:'image',   filename:filename}
  endcase
  
  return, result
end