;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Private:
; :Description:
;    This is a helper routine for `imageMath_applicationWizard`. For a given ASCII file, it
;    replaces all occurrence of the string 'template', by the application name `appName`. 
;
; :Params:
;    filename : in, required, type=filepath
;      Filename of the ASCII file. 
;    appName : in, required, type=string
;      Application name.
;
;-
pro imageMath_applicationWizard_replaceTemplate, filename, appName

  lines = (hubIOASCIIHelper()).readFile(filename)
  foreach line, lines, i do begin
    lines[i] = strjoin(strsplit(line, 'template', /REGEX, /EXTRACT, /PRESERVE_NULL, /FOLD_CASE), appName)
  endforeach
  (hubIOASCIIHelper()).writeFile, filename, lines
  
end

;+
; :Description:
;    The Application Wizard is a widget program for creating an application skeleton 
;    that can be used as a starting point for writing your own EnMAP-Box application.
;
;-
pro imageMath_applicationWizard

  ; get application name

  title = 'imageMath Function Wizard'
  hubAMW_program, Title=title
  hubAMW_inputDirectoryName, 'codeRootDir', Title='Application Source Code Directory ', VALUE=enmapBox_getDirname(/Applications, /SourceCode)
  result = hubAMW_manage()
  if ~result['accept'] then return
  
  appCodeDir = result['codeRootDir']
  appName = file_basename(appCodeDir)

  ; copy template files
  templateCodeDir = filepath('TEMPLATE', ROOT=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY='imageMath')
  file_copy, filepath('_resource', ROOT_DIR=templateCodeDir), appCodeDir, /RECURSIVE, /OVERWRITE
  file_copy, filepath('imageMathFunction', ROOT_DIR=templateCodeDir), appCodeDir, /RECURSIVE, /OVERWRITE

  ; rename all PRO files
  proFilenames = file_search(filepath('imageMathFunction', ROOT_DIR=appCodeDir), '*.pro', COUNT=numberOfFiles)
  allFilenames = proFilenames
  
  for i=0,numberOfFiles-1 do begin
    targetFilename = filepath('imagemathfunction_'+strlowcase(appName)+strmid(file_basename(allFilenames[i]),26), ROOT=file_dirname(allFilenames[i]))
    file_move, allFilenames[i], targetFilename
  endfor

  ; replace "template" by <appName> string inside PRO, MEN, TXT, IDLDOC files
  allFilenames = [ $
    filepath('imagemath.men', ROOT_DIR=appCodeDir, SUBDIRECTORY='_resource'), $
    file_search(filepath('imageMathFunction', ROOT_DIR=appCodeDir), '*.pro', /TEST_REGULAR, COUNT=count2)]

  numberOfFiles = n_elements(allFilenames)
  
  for i=0,numberOfFiles-1 do begin
    imageMath_applicationWizard_replaceTemplate, allFilenames[i], appName
  endfor
  
  hubAMW_program, Title=title
  hubAMW_text, 'text', XSize=(get_screen_size())[0]*0.75, Value=[ $
    'imageMath function successfully created.',$
    '',$
    'menu file:', $
    allFilenames[0],$
    '',$
    'source code files:', $
    allFilenames[1:*]]
  result = hubAMW_manage()

end