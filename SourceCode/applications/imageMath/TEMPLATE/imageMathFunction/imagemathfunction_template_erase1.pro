function imageMathFunction_template_erase1, imageFilename $
  , ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      '***',$
      'This is an imageMath function of type: external',$
      '***',$
      '',$    
      'Returns an argument with all values set to zero.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_TEMPLATE_erase1(argument)',$
      '',$
      'argument = image | band | profile',$
      'result =   image | band | profile']
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(imageFilename) then message, 'Missing argument: image'

  inputImage = hubIOImgInputImage(imageFilename)
  outputImage = hubIOImgOutputImage(resultFilename)

  ; copy some metadata

  outputImage.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation, /CopySpectralInformation

  ; initialize for reading/writing image cube tiles

  numberOfTileLines = 100
  inputImage.initReader, numberOfTileLines, /Cube, /TileProcessing
  writerSettings = inputImage.getWriterSettings()
  outputImage.initWriter, writerSettings

  ; perform processing cube tile by cube tile
                                                                         
  while ~inputImage.tileProcessingDone() do begin                       
    cubeTileData = inputImage.getData()                                 
    resultTile =  cubeTileData*0
    outputImage.writeData, resultTile                             
  endwhile

  ; cleanup

  inputImage.cleanup
  outputImage.cleanup
 
  return, !null
end