function imageMathFunction_template_erase3, x, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      '***',$
      'This is an imageMath function of type: valueWise',$
      '***',$
      '',$      
      'Returns an argument with all values set to zero.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_TEMPLATE_erase3(argument)',$
      '',$
      'argument = image | band | profile | number',$
      'result =   image | band | profile | number]']
    reflection = {functionClass:'valueWise', description:description}
    return, reflection
  endif
  
  result = x*0
  return, result
  
end