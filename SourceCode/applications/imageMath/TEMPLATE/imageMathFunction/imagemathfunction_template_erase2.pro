function imageMathFunction_template_erase2, image, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      '***',$
      'This is an imageMath function of type: imageProcessing',$
      '***',$
      '',$    
      'Returns an image with all values set to zero.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_TEMPLATE_erase2(image)',$
      '',$
      'image = image',$
      'result = image']
    reflection = {functionClass:'imageProcessing', description:description,  neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  result = image*0
  return, result
end