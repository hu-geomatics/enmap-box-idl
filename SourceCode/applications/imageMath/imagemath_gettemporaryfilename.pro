function imageMath_getTemporaryFilename
  filename = filepath('imageMathTemp'+strtrim((randomn(seed, 1, /LONG))[0], 2), $
    ROOT_DIR=filepath('',/TMP), $
    SUBDIRECTORY='imageMath')
  return, filename
end