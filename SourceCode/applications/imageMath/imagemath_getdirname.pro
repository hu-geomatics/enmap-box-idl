;+
; :Author: <author name> (<email>)
;-

function imageMath_getDirname
  
  result = filepath('imageMath', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_imageMath_getDirname

  print, imageMath_getDirname()
  print, imageMath_getDirname(/SourceCode)

end
