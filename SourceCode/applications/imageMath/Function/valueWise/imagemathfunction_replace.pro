function imageMathFunction_replace, x, oldValue, newValue, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Replaces a specific value (oldValue) by a new one (newValue).',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_replace(x, oldValue, newValue)',$
      '',$
      'x =     image | band | profile | number',$
      'oldValue = number',$
      'newValue = number',$
      'result = image | band | profile | number']
    result = imageMathFunction_DefineReflection(/ValueWise, Description=description)
    return, result
  endif
  
  mask = x eq oldValue
  result = mask*newValue + (~mask)*x
  return, result
  
end