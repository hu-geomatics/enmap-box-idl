function imageMathFunction_insideRange, x, min, max, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Identifies wheter or not a given value x is inside the closed intervall [min, max].',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_insideRange(x, min, max)',$
      '',$
      'x =      image | band | profile | number',$
      'min =    image | band | profile | number',$
      'max =    image | band | profile | number',$
      'result = image | band | profile | number]']
    reflection = {functionClass:'valueWise', description:description}
    return, reflection
  endif
  
  result = (x ge min) * (x le max) 
  return, result
  
end