function imageMathFunction_maximum, x1, x2, x3, x4, x5, x6, x7, x8, x9, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the value-wise maximum of x1, x2, ..., x9.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_maximum(x1, x2 [, x3 [, x4 ...]])',$
      '',$
      'x1 =     image | band | profile | number',$
      'x2 =     image | band | profile | number',$
      'x3 =     image | band | profile | number (optional)',$
      '...',$
      'x9 =     image | band | profile | number (optional)',$
      'result = image | band | profile | number]']
    reflection = {functionClass:'valueWise', description:description}
    return, reflection
  endif
  
  result = x1
  result >= x2
  if isa(x3) then result >= x3 
  if isa(x4) then result >= x4 
  if isa(x5) then result >= x5 
  if isa(x6) then result >= x6 
  if isa(x7) then result >= x7 
  if isa(x8) then result >= x8 
  if isa(x9) then result >= x9 
    
  return, result
  
end