function imageMathFunction_conditional, x1, x2, x3, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the value-wise conditional x1 ? x2 : x3, which is a short-cut for an if-then-else statement.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_conditional(x1, x2, x3)',$
      '',$
      'x1 =     image | band | profile | number',$
      'x2 =     image | band | profile | number',$
      'x3 =     image | band | profile | number',$
      'result = image | band | profile | number]']
    result = imageMathFunction_DefineReflection(/ValueWise, Description=description)
    return, result
  endif
  
  false = x1 eq 0
  true  = ~false
  result = true*x2 + false*x3
  return, result
  
end