function imageMathFunction_spectralAnd, image, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a band with the profile-wise maximum values for an image.',$
      '',$
      'Syntax:',$
      'result = imageMathFunction_spectralMaximum(image)',$
      '',$
      'image = image',$
      'result = band']
    reflection = {functionClass:'imageProcessing', description:description,  neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  result = product(image, 3) eq 1
  return, result
  
end