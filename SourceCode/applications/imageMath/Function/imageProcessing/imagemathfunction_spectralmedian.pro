function imageMathFunction_spectralMedian, image, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a band with the profile-wise median values for an image.',$
      '',$
      'Syntax:',$
      'result = imageMathFunction_spectralMedian(image)',$
      '',$
      'image = image',$
      'result = band']
    reflection = {functionClass:'imageProcessing', description:description,  neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  result = median(image, DIMENSION=3)
  return, result
  
end