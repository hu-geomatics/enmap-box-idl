function imageMathFunction_spectralVariance, image, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a band with the profile-wise variance values for an image.',$
      '',$
      'Syntax:',$
      'result = imageMathFunction_spectralVariance(image)',$
      '',$
      'image = image',$
      'result = band']
    reflection = {functionClass:'imageProcessing', description:description,  neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  result = variance(image, DIMENSION=3)
  return, result
  
end