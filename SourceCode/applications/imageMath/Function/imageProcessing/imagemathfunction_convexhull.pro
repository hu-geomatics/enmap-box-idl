function imageMathFunction_convexHull, image, $
  InputHeader=inputHeader, OutputHeader=outputHeader, $
  Reflection=reflection
  
  if keyword_set(reflection) then begin

    description = [$
      'Returns an image with convex hull profiles.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_convexHull(image)',$
      '',$
      'image = image']
    result = imageMathFunction_DefineReflection(/ImageProcessing, Description=description)

  endif else begin
  
    outputHeader.copyMeta, inputHeader, /CopySpectralInformation
  
    result = make_array(size(/DIMENSIONS, image), TYPE=size(/TYPE, image))
    samples = n_elements(image[*,0,0])
    lines = n_elements(image[0,*,0])
    
    wavelength = inputHeader.getMeta('wavelength')
    for sample=0,samples-1 do begin
      for line=0,lines-1 do begin
        spectrum = reform(image[sample,line,*])
        !null = hubContinuumRemoval(spectrum, wavelength, HullSpectrum=hullSpectrum)
        result[sample,line,*] = hullSpectrum
      endfor
    endfor
  
  endelse
  return, result
  
end

pro test_imageMathFunction_convexHull

 @huberrorcatch

 expression = 'convexHull(a)'
 variables = hash()
 variables['a'] = imageMath_variable(hub_getTestImage('Hymap_Berlin-A_Image'))
 resultFilename = filepath('my_image', /TMP)
 result = imageMath_evaluate(expression, variables, resultFilename)
 print, result
 
end