function imageMathFunction_bandSequence, image, bandPosition, step,$
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns an spectral image subset consisting of the band given by the bandPosition argument from their on every n-th band, where n is given by the step argument.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_bandSequence(image, bandPosition, step)',$
      '',$
      'image = image',$
      'The input image.',$ 
      '',$
      'bandPosition = number',$
      'An integer specifying the band index',$
      '',$
      'step = number',$
      'An integer specifying the band index']
    reflection = {functionClass:'imageProcessing', description:description, neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  if ~isa(bandPosition) then message, 'Missing argument: bandPosition'
  if ~isa(step) then message, 'Missing argument: step'

  result = image[*,*,bandPosition:*:step]
  return, result
  
end