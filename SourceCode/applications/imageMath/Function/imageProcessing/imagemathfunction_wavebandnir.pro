function imageMathFunction_wavebandNIR, image, $
  InputHeader=inputHeader, $
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the image band nearest to the NIR wavelength at 860 nanometers.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_wavebandNIR(image)',$
      '',$
      'image = image',$
      'The input image.']
    reflection = {functionClass:'imageProcessing', description:description, neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'

  coloredInfraredBandPosition = inputHeader.locateWavelength(/ColoredInfrared)
  result = imageMathFunction_band(image, coloredInfraredBandPosition[0])
  return, result
  
end