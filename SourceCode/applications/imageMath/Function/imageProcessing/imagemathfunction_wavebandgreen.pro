function imageMathFunction_wavebandGreen, image, $
  InputHeader=inputHeader, $
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the image band nearest to the green wavelength at 550 nanometers.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_wavebandGreen(image)',$
      '',$
      'image = image',$
      'The input image.']
    reflection = {functionClass:'imageProcessing', description:description, neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'

  trueColorBandPosition = inputHeader.locateWavelength(/TrueColor)
  result = imageMathFunction_band(image, trueColorBandPosition[1])
  return, result
  
end