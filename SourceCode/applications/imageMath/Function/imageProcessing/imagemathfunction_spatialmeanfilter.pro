function imageMathFunction_spatialMeanFilter, image, width, height, $
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns an image with mean filtered bands.',$
      '',$
      'Syntax:',$
      'result = imageMathFunction_spatialMeanFilter(image, width [, height])',$
      '',$
      'image = image',$
      'The input image.',$ 
      '',$
      'width = number',$
      'The width of the two-dimensional neighborhood. This value must be an odd, positive integer.',$
      '',$
      'height = number (optional)',$
      'The height of the two-dimensional neighborhood. This value must be an odd, positive integer. If this value is omitted, height is set to width, resulting in a square neighborhood.',$
      'Note: Order must be less than or equal to the value specified for Degree.']
    if isa(width) then begin
      neighborhoodHeight = isa(height) ? height : width
    endif else begin
      neighborhoodHeight = 1
    endelse
    result = imageMathFunction_DefineReflection(/ImageProcessing, Description=description, NeighborhoodHeight=neighborhoodHeight)
    return, result
  endif

  if ~isa(image) then message, 'Missing argument: image'
  if ~isa(width) then message, 'Missing argument: width'
  if ~isa(height) then height = width
  
  ; result type is double if input image type is double, otherwise float
  type = typename(image) eq 'DOUBLE' ? 5 : 4
  imageSize = size(/DIMENSIONS, image)
  image = fix(image, TYPE=type)
  !null = reform(/OVERWRITE, image, imageSize)
  
  kernel = make_array(width, height, VALUE=1./(width*height), TYPE=type)
  !null = reform(/OVERWRITE, kernel, width, height, 1)
  result = convol(image, kernel, /CENTER)
  
  return, result
  
end