function imageMathFunction_waveband, image, wavelength, $
  InputHeader=inputHeader, $
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns an image band given by a specific wavelength in nanometers.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_waveband(image, wavelength)',$
      '',$
      'image = image',$
      'The input image.',$ 
      '',$
      'wavelength = number',$
      'A number specifying the wavelength in nanometers.']
    reflection = {functionClass:'imageProcessing', description:description, neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  if ~isa(wavelength) then message, 'Missing argument: wavelength'

  bandPosition = inputHeader.locateWavelength(wavelength)
  result = imageMathFunction_band(image, bandPosition)
  return, result
  
end