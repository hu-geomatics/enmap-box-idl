function imageMathFunction_spectralSpan, image, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a band with the profile-wise span (maximum-minimum) values for an image.',$
      '',$
      'Syntax:',$
      'result = imageMathFunction_spectralSpan(image)',$
      '',$
      'image = image',$
      'result = band']
    reflection = {functionClass:'imageProcessing', description:description,  neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  minImage = min(image, DIMENSION=3, MAX=maxImage)
  result = maxImage-minImage
  return, result
  
end