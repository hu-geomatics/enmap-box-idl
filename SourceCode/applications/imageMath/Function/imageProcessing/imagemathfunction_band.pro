function imageMathFunction_band, image, bandPosition, $
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a specific image band.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_band(image, bandPosition)',$
      '',$
      'image = image',$
      'The input image.',$ 
      '',$
      'bandPosition = number',$
      'An integer specifying the band index']
    reflection = {functionClass:'imageProcessing', description:description, neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  if ~isa(bandPosition) then message, 'Missing argument: bandPosition'

  result = image[*,*,bandPosition]
  return, result
  
end