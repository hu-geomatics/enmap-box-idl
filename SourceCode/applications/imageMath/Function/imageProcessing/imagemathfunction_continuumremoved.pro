function imageMathFunction_continuumRemoved, image, $
  InputHeader=inputHeader, OutputHeader=outputHeader, $
  Reflection=reflection
  
  if keyword_set(reflection) then begin

    description = [$
      'Returns an image with continuum-removed profiles.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_continuumRemoved(image)',$
      '',$
      'image = image']
    result = imageMathFunction_defineReflection(/ImageProcessing, Description=description, NeighborhoodHeight=1)

  endif else begin
    outputHeader.copyMeta, inputHeader, /CopySpectralInformation
    hubMath_mathErrorsOff, state
    result = image/float(imageMathFunction_convexHull(image, InputHeader=inputHeader, OutputHeader=outputHeader))
    hubMath_mathErrorsOn, state
    result[where(/NULL, ~finite(result))] = 1.
  endelse
  return, result
end