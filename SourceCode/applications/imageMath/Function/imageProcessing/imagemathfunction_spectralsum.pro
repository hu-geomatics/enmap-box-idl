function imageMathFunction_spectralSum, image, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a band with the profile-wise sum values for an image.',$
      '',$
      'Syntax:',$
      'result = imageMathFunction_spectralSum(image)',$
      '',$
      'image = image',$
      'result = band']
    reflection = {functionClass:'imageProcessing', description:description,  neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  result = total(image, 3)
  return, result
  
end