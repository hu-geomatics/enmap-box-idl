function imageMathFunction_savitzkyGolay, image, width, order, degree, $
  Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns an image with Savitzky-Golay filtered profiles.',$
      'The filter is defined as a weighted moving average with weighting given as a polynomial of a certain degree. The filter coefficients, when applied to a image profile, perform a polynomial least-squares fit within the filter window. This polynomial is designed to preserve higher moments within the data and reduce the bias introduced by the filter. The filter can use any number of points for this weighted average. Also see IDL SAVGOL function reference.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_savitzkyGolay(image, width, order, degree)',$
      '',$
      'image = image',$
      'The input image.',$ 
      '',$
      'width = number',$
      'An integer specifying the number of data points to include in the filter.',$
      'Note: Larger values produce a smoother result at the expense of flattening sharp peaks.',$
      '',$
      'order = number',$
      'An integer specifying the order of the derivative desired. For smoothing, use order 0. To find the smoothed first derivative of the signal, use order 1, for the second derivative, use order 2, etc.',$
      'Note: Order must be less than or equal to the value specified for Degree.',$
      '',$
      'degree = number',$
      'An integer specifying the degree of smoothing polynomial. Lower values will produce smoother results but may introduce bias, higher values will reduce the filter bias, but may overfit the data and give a noisier result.',$
      'Note: Degree must be less than the filter width.']
    reflection = {functionClass:'imageProcessing', description:description, neighborhoodHeight:1}
    return, reflection
  endif

  if ~isa(image) then message, 'Missing argument: image'
  if ~isa(width) then message, 'Missing argument: width'
  if ~isa(order) then message, 'Missing argument: order'
  if ~isa(degree) then message, 'Missing argument: degree'
  
  kernel = savgol(floor(width/2), floor(width/2), order, degree)
  !null = reform(/OVERWRITE, kernel, 1, 1, n_elements(kernel))
  result = convol(float(image), kernel)
  
  return, result
  
end