function imageMathFunction_redEdgeInflectionPoint, image, $
  InputHeader=inputHeader, OutputHeader=outputHeader, $
  Reflection=reflection
  
  if keyword_set(reflection) then begin

    description = [$
      'Returns a band with the red edge inflection point wavelength in nanometers for an input image.',$
      'Uses the red edge range between 680 and 730 nm.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_redEdgeInflectionPoint(image)',$
      '',$
      'image = image']
    result = imageMathFunction_DefineReflection(/ImageProcessing, Description=description)

  endif else begin
  
    ; prepare wavelength to nanometers
     
    wavelength = inputHeader.getMeta('wavelength')
    if strcmp(inputHeader.getMeta('wavelength units'), 'micrometers', /FOLD_CASE) then begin
      wavelength *= 1000.
    endif
      
    ; find red edge wavelengths

    startIndex = inputHeader.locateWavelength(680)
    endIndex = inputHeader.locateWavelength(730)
    if startIndex eq endIndex then begin
      message, 'Can not calculate result: red edge region contains only a single waveband.'
    endif
    
     ; subset to red edge region
    
    image = image[*,*,startIndex:endIndex]
    wavelength = wavelength[startIndex:endIndex]
    
    ; calculate gain
    
    valuesA = image[*,*,0:-2]
    valuesB = image[*,*,1:-1]
    deltaValues = valuesB - valuesA
    
    wlA = wavelength[0:-2]
    wlB = wavelength[1:-1]
    wlA = reform(/OVERWRITE, wlA, 1, 1, n_elements(wlA))
    wlB = reform(/OVERWRITE, wlB, 1, 1, n_elements(wlB))
    wlA = rebin(wlA, size(/DIMENSIONS, deltaValues))
    wlB = rebin(wlB, size(/DIMENSIONS, deltaValues))
    deltaWL = wlB - wlA 

    gain = deltaValues/deltaWL

    ; find red edge inflection point (i.e. waveband with maximum gain)
    
    !null = max(gain, maxIndex, DIMENSION=3)
    result = wlB[maxIndex]
  
  endelse
  return, result
  
end

pro test_imageMathFunction_RedEdgeInflectionPoint

 @huberrorcatch

 expression = 'imageMathFunction_RedEdgeInflectionPoint(a)'
 variables = hash()
 variables['a'] = imageMath_variable(enmapBox_getTestImage('AF_Image'))
 resultFilename = filepath('my_image', /TMP)
 result = imageMath_evaluate(expression, variables, resultFilename)
 print, result
 
end