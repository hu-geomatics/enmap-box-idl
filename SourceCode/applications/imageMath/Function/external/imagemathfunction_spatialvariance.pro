function imageMathFunction_spatialVariance, filename, maskFilename, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a profile with the band-wise variance values for an image.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_spatialVariance(image)',$
      '',$
      'image = image',$
      'mask = image',$
      'result = profile']
    reflection = {functionClass:'external', description:description, ReturnsProfile:1b}
    return, reflection
  endif

  if ~isa(filename) then message, 'Missing argument: image'
  
  imageMathFunction_incMathWrapper, filename, maskFilename, IncMathClass='hubMathIncVariance', ResultFilename=resultFilename, /CalculateMean
  return, !null
end