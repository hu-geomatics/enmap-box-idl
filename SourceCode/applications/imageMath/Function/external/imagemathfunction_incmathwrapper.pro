pro imageMathFunction_incMathWrapper, filename, maskFilename $
  ,ResultFilename=resultFilename, IncMathClass=incMathClass, CalculateMean=calculateMean, ImageWise=imageWise

  ; calculate
  ; - init input image
  inputImage = hubIOImgInputImage(filename)
  numberOfTileLines = 100
  inputImage.initReader, numberOfTileLines, /Cube, /TileProcessing
  
  ; - init mask image
  useMask = isa(maskFilename)
  if useMask then begin
    inputMask = hubIOImgInputImage(maskFilename)
    inputMask.initReader, numberOfTileLines, /Cube, /TileProcessing, /Mask
  endif
    
  ; - calculate statistic
  bands = inputImage.getMeta('bands')
  incMaths = list()

  if keyword_set(calculateMean) then begin
    if useMask then begin
      help,/TRACEBACK
      message, 'using a mask file is not yet implemented'
    endif
    if keyword_set(imageWise) then begin
      expression = 'imageMean(image)'
      variables = hash()
      variables['image'] = {type:'image', filename:filename}
      means = imageMath_evaluate(expression, variables, imageMath_getTemporaryFilename(), /NoOpen)
    endif else begin
      expression = 'spatialMean(image)'
      variables = hash()
      variables['image'] = {type:'image', filename:filename}
      meanFilename = imageMath_evaluate(expression, variables, imageMath_getTemporaryFilename(), /NoOpen)
      means = reform((hubIOImgInputImage(meanFilename)).getCube())
    endelse
   
  endif  
  
  if keyword_set(imageWise) then begin
  
    if keyword_set(calculateMean) then begin
      incMaths.add, obj_new(incMathClass, means[0])
    endif else begin
      incMaths.add, obj_new(incMathClass)
    endelse
  
  endif else begin
    
    for i=0, bands-1 do begin 
      if keyword_set(calculateMean) then begin
        incMaths.add, obj_new(incMathClass, means[i])
      endif else begin
        incMaths.add, obj_new(incMathClass)
      endelse
    endfor
    
  endelse
  
  while ~inputImage.tileProcessingDone() do begin                       
    cubeData = inputImage.getData()
    cubeDataSize = size(/DIMENSIONS, cubeData)
    
    if useMask then begin
      cubeMask = inputMask.getData()
      bandMask = ~product(reform(/OVERWRITE,~cubeMask, size(/DIMENSIONS,cubeMask)), 3)
    endif else begin
      bandMask = make_array(cubeDataSize[0], cubeDataSize[1], VALUE=1b)
    endelse
    
    indices = where(/NULL, bandMask)
    
    for i=0, bands-1 do begin
      vectorData = (cubeData[*,*,i])[indices]
      if keyword_set(imageWise) then begin
        (incMaths[0]).addData, vectorData    
      endif else begin
        (incMaths[i]).addData, vectorData
      endelse
    endfor
    
  endwhile
  
  result = list()
  for i=0, n_elements(incMaths)-1 do begin 
    result.add, (incMaths[i]).getResult()
    if ~isa(result[i]) then begin
      message, 'Can not calculate statistic, all values are masked.', /NONAME
    endif
  endfor
  
  result = result.toArray()
  !null = reform(/OVERWRITE, result, 1, 1, n_elements(result))
  
  ; - write result image 
  writerSettings = hash()
  writerSettings['samples'] = 1
  writerSettings['lines'] = 1
  writerSettings['bands'] = n_elements(result)
  writerSettings['data type'] = size(/TYPE, result)
  writerSettings['dataFormat'] = 'cube' 
  outputImage = hubIOImgOutputImage(resultFilename, /NoOpen)
  if ~keyword_set(imageWise) then begin
    outputImage.copyMeta, inputImage, ['wavelength', 'fwhm', 'wavelength units', 'default bands']
  endif
  outputImage.initWriter, writerSettings
  outputImage.writeData, result                            

  outputImage.cleanup
  inputImage.cleanup

end