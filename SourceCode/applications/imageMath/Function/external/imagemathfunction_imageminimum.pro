function imageMathFunction_imageMinimum, filename, maskFilename, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the minimum value for an image.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_imageMinimum(image)',$
      '',$
      'image = image',$
      'mask = image',$
      'result = number']
    reflection = {functionClass:'external', description:description, ReturnsNumber:1b}
    return, reflection
  endif

  if ~isa(filename) then message, 'Missing argument: image'
  
  imageMathFunction_incMathWrapper, filename, maskFilename, IncMathClass='hubMathIncMinimum', ResultFilename=resultFilename, /ImageWise
  return, !null
end