function imageMathFunction_imageSpan, filename, maskFilename, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the span (maximum-minimum) value for an image.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_imageSpan(image)',$
      '',$
      'image = image',$
      'mask = image',$
      'result = number']
    reflection = {functionClass:'external', description:description, ReturnsNumber:1b}
    return, reflection
  endif

  if ~isa(filename) then message, 'Missing argument: image'
  
  imageMathFunction_incMathWrapper, filename, maskFilename, IncMathClass='hubMathIncSpan', ResultFilename=resultFilename, /ImageWise
  return, !null
end