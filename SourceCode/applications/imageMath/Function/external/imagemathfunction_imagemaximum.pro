function imageMathFunction_imageMaximum, filename, maskFilename, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the maximum value for an image.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_imageMaximum(image)',$
      '',$
      'image = image',$
      'mask = image',$
      'result = number']
    reflection = {functionClass:'external', description:description, ReturnsNumber:1b}
    return, reflection
  endif

  if ~isa(filename) then message, 'Missing argument: image'
  
  imageMathFunction_incMathWrapper, filename, maskFilename, IncMathClass='hubMathIncMaximum', ResultFilename=resultFilename, /ImageWise
  return, !null
end