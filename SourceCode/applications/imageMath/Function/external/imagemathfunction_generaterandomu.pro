function imageMathFunction_generateRandomU, samples, lines, bands, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a random image of specified size [samples, lines, bands]. '+$
      'Random numbers are uniformly-distributed, floating-point, pseudo-random in the 0-1 range. '+$
      'Ramdom sequence seed is the current system time.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_generateRandomU(samples, lines, bands)',$
      '',$
      'samples  = number',$
      'lines    = number',$
      'bands    = number',$
      'result   = image']
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(samples) then message, 'Missing argument: samples'
  if ~isa(lines) then message, 'Missing argument: lines'
  if ~isa(bands) then message, 'Missing argument: bands'
  
  dataType = 4
  writerSettings = hash()
  writerSettings['samples'] = samples
  writerSettings['lines'] = lines
  writerSettings['bands'] = bands
  writerSettings['data type'] = dataType
  writerSettings['dataFormat'] = 'band'

  outputImage = hubIOImgOutputImage(resultFilename, /NoOpen)
  outputImage.initWriter, writerSettings
  
  seed = systime(1)
  for band=0,bands-1 do begin
    randomBand = randomu(seed, samples, lines)
    outputImage.writeData, randomBand
  endfor
  outputImage.cleanup
  
  return, !null
end