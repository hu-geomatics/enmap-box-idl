function imageMathFunction_generateConstant, samples, lines, bands, value, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a constant image of specified size [samples, lines, bands] and data type.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_generateConstant(samples, lines, bands, value)',$
      '',$
      'samples  = number',$
      'lines    = number',$
      'bands    = number',$
      'value    = number',$
      'result   = image']
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(samples) then message, 'Missing argument: samples'
  if ~isa(lines) then message, 'Missing argument: lines'
  if ~isa(bands) then message, 'Missing argument: bands'
  if ~isa(value) then message, 'Missing argument: value'
  
  writerSettings = hash()
  writerSettings['samples'] = samples
  writerSettings['lines'] = lines
  writerSettings['bands'] = bands
  writerSettings['data type'] = size(/TYPE, value)
  writerSettings['dataFormat'] = 'band'

  outputImage = hubIOImgOutputImage(resultFilename, /NoOpen)
  outputImage.initWriter, writerSettings
  
  constantBand = make_array(samples, lines, VALUE=value)
  for band=0,bands-1 do begin
    outputImage.writeData, constantBand
  endfor
  outputImage.cleanup
  
  return, !null
end