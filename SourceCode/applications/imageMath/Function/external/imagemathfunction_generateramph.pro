function imageMathFunction_generateRampH, samples, lines, bands, dataType, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a horizontal ramp image of specified size [samples, lines, bands] and data type.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_generateRampH(samples, lines, bands, dataType)',$
      '',$
      'samples  = number',$
      'lines    = number',$
      'bands    = number',$
      'dataType = number (optional, floating-point is default)',$
      'result   = image']
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(samples) then message, 'Missing argument: samples'
  if ~isa(lines) then message, 'Missing argument: lines'
  if ~isa(bands) then message, 'Missing argument: bands'
  if ~isa(dataType) then dataType = 4
  
  writerSettings = hash()
  writerSettings['samples'] = samples
  writerSettings['lines'] = lines
  writerSettings['bands'] = bands
  writerSettings['data type'] = dataType
  writerSettings['dataFormat'] = 'band'

  outputImage = hubIOImgOutputImage(resultFilename, /NoOpen)
  outputImage.initWriter, writerSettings
  
  ones = replicate(1., lines)
  ramp = findgen(samples)
  rampBand = ramp#ones
  for band=0,bands-1 do begin
    outputImage.writeData, rampBand
  endfor
  outputImage.cleanup
  
  return, !null
end