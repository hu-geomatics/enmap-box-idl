function imageMathFunction_imageVariance, filename, maskFilename, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the variance value for an image.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_imageVariance(image)',$
      '',$
      'image = image',$
      'mask = image',$
      'result = number']
    reflection = {functionClass:'external', description:description, ReturnsNumber:1b}
    return, reflection
  endif

  if ~isa(filename) then message, 'Missing argument: image'
  
  imageMathFunction_incMathWrapper, filename, maskFilename, IncMathClass='hubMathIncVariance', ResultFilename=resultFilename, /CalculateMean, /ImageWise
  return, !null
end