function imageMathFunction_spatialSpan, filename, maskFilename, $
  ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns a profile with the band-wise span (maximum-minimum) values for an image.',$
      '',$
      'Syntax:',$      
      'result = imageMathFunction_spatialSpan(image)',$
      '',$
      'image = image',$
      'mask = image',$
      'result = profile']
    reflection = {functionClass:'external', description:description, ReturnsProfile:1b}
    return, reflection
  endif

  if ~isa(filename) then message, 'Missing argument: image'
  
  imageMathFunction_incMathWrapper, filename, maskFilename, IncMathClass='hubMathIncSpan', ResultFilename=resultFilename
  return, !null
end