function imageMathFunction_defineReflection, $
  ImageProcessing=imageProcessing, ValueWise=valueWise, External=external,$
  Description=description,$
  NeighborhoodHeight=neighborhoodHeight
  
  if ~hub_isExclusiveKeyword(imageProcessing, ValueWise, external) then begin
    message, 'Set one of the exclusive keywords: imageProcessing, ValueWise, external.
  endif
  
  if keyword_set(imageProcessing) then functionClass = 'imageProcessing'
  if keyword_set(valueWise) then functionClass = 'valueWise'
  if keyword_set(external) then functionClass = 'external'
  neighborhoodHeight = isa(neighborhoodHeight) ? neighborhoodHeight : 1
  description = isa(description) ? description : ''
  result = {functionClass:functionClass, description:description, neighborhoodHeight:neighborhoodHeight}
  
  return, result
end