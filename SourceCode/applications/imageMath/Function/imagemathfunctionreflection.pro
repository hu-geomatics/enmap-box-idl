function imageMathFunctionReflection, functionName, arguments, _EXTRA=keywords, Description=description, Class=class, NeighborhoodHeight=neighborhoodHeight

  functionInfo = hubRoutineInfo(functionName, /Functions)

  if functionInfo.missing then begin
    functionClass = ''
    neighborhoodHeight = 1
    functionDescription = 'Warning: unknown function: '+functionName
  endif

  if functionInfo.system then begin
    functionClass = 'ValueWise'
    neighborhoodHeight = 1
    functionNames = ['sin',  'cos',    'tan',     'asin',    'acos',      'atan',       'sinh',            'cosh',              'tanh']
    longnames =     ['sine', 'cosine', 'tangent', 'arcsine', 'arccosine', 'arctangent', 'hyperbolic sine', 'hyperbolic cosine', 'hyperbolic tangent']
    functionIndex = where(/NULL, strlowcase(functionName) eq functionNames)
    if isa(functionIndex) then begin
      functionDescription = ['Returns the '+longnames[functionIndex]+'.', 'result = '+functionName+'(x)', '', 'x = image | band | profile | number', 'result = image | band | profile | number']    
    endif else begin
      functionNames = ['round','floor','ceil','alog','alog10','exp','sqrt','abs','finite']
      descriptions = [ $
      'Returns the integer closest to its argument.',$
      'Returns the closest integer less than or equal to its argument.',$
      'Returns the closest integer greater than or equal to its argument.',$
      'Returns the natural logarithm.',$
      'Returns the logarithm to the base 10.',$
      'Returns the natural exponential function.',$
      'Returns the square root.',$
      'Returns the absolute value.',$
      'Identifies whether or not a given value is finite.']
      functionIndex = where(/NULL, strlowcase(functionName) eq functionNames)
      functionDescription = [descriptions[functionIndex], 'result = '+functionName+'(x)', '', 'x = image | band | profile | number', 'result = image | band | profile | number']
    endelse
  endif

  if ~functionInfo.system && ~functionInfo.missing then begin
    case n_elements(arguments) of
      0 : reflection = call_function(functionName, _EXTRA=keyword, /Reflection)
      1 : reflection = call_function(functionName, arguments[0], _EXTRA=keyword, /Reflection)
      2 : reflection = call_function(functionName, arguments[0], arguments[1], _EXTRA=keyword, /Reflection)
      3 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], _EXTRA=keyword, /Reflection)
      4 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], arguments[3], _EXTRA=keyword, /Reflection)
      5 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], _EXTRA=keyword, /Reflection)
      6 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], $
                                                          arguments[5], _EXTRA=keyword, /Reflection)
      7 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], $
                                                          arguments[5], arguments[6], _EXTRA=keyword, /Reflection)
      8 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], $
                                                          arguments[5], arguments[6], arguments[7], _EXTRA=keyword, /Reflection)
      9 : reflection = call_function(functionName, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], $
                                                          arguments[5], arguments[6], arguments[7], arguments[8], _EXTRA=keyword, /Reflection)
      else: message, 'Number of arguments is limited to 10.'
    endcase  
  endif else begin
    reflection = {functionClass:functionClass, description:functionDescription,  neighborhoodHeight:neighborhoodHeight}
  endelse
  
  if keyword_set(description) then begin
    return, reflection.description
  endif
  if keyword_set(class) then begin
    return, reflection.functionClass
  endif
  if keyword_set(neighborhoodHeight) then begin
    return, reflection.neighborhoodHeight
  endif
  
  return, reflection
end