pro imageMath_cleanup
  tmpFolder = file_dirname(imageMath_getTemporaryFilename())
  file_delete, tmpFolder, /ALLOW_NONEXISTENT, /RECURSIVE
end