pro mybackup_iterativeSMA_unmixSlice, imageSlice, endmemberProfiles, selectionAlgorithm, selectionAlgorithmParameters, $
  FinalFractionsSlice=finalFractionsSlice, FinalResidualsSlice=finalResidualsSlice, FinalRMSESlice=finalRMSESlice, FinalModelledSlice=finalModelledSlice,$
  HistoryRMSESlice=historyRMSESlice, HistoryMinimalFractionsSlice=historyMinimalFractionsSlice, HistoryIndexSlice=historyIndexSlice, HistoryDarkFractionsSlice=historyDarkFractionsSlice

  numberOfEndmembers = (size(/DIMENSIONS, endmemberProfiles))[1]
  imageSliceSize = size(/DIMENSIONS, imageSlice)
  
  if arg_present(finalFractionsSlice) then finalFractionsSlice = make_array(numberOfEndmembers+1, imageSliceSize[1])
  if arg_present(finalResidualsSlice) then finalResidualsSlice = make_array(imageSliceSize)
  if arg_present(finalRMSESlice) then finalRMSESlice = make_array(1, imageSliceSize[1])
  if arg_present(finalModelledSlice) then finalModelledSlice = make_array(imageSliceSize)
  if arg_present(historyRMSESlice) then historyRMSESlice = make_array(numberOfEndmembers, imageSliceSize[1])
  if arg_present(historyMinimalFractionsSlice) then historyMinimalFractionsSlice = make_array(numberOfEndmembers, imageSliceSize[1])
  if arg_present(historyIndexSlice) then historyIndexSlice = make_array(numberOfEndmembers, imageSliceSize[1], /INTEGER)
  if arg_present(historyDarkFractionsSlice) then historyDarkFractionsSlice = make_array(numberOfEndmembers, imageSliceSize[1])

;  help, imageSlice, endmemberProfiles, finalResidualsSlice, finalRMSESlice, finalModelledSlice, historyRMSESlice, historyMinimalFractionsSlice, historyIndexSlice, historyDarkFractionsSlice
;  help, selectionAlgorithm

end