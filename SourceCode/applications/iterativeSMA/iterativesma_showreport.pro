pro iterativeSMA_showReport, parameters

  report = hubReport(Title='iterativeSMA')
  report.addHeading,    'Input', 1
  report.addMonospace,  ['Endmember SpecLib: '+parameters['inputEndmember'],$
                         'Image:             '+parameters['inputImage']]
  
  report.addHeading,    'Output', 1
  textBasename = 'Basename:                    '+parameters['outputBasename']

  textFinal =   'Final Model Outputs:         '
  options = list()
  if parameters.hubGetValue('outputFinalFractions', Default=1b) then options.add, 'Fractions'
  if parameters.hubGetValue('outputFinalResiduals', Default=1b) then options.add, 'Residuals'
  if parameters.hubGetValue('outputFinalRMSE', Default=1b) then options.add, 'RMSE'
  if parameters.hubGetValue('outputFinalModelled', Default=1b) then options.add, 'Modelled'
  textFinal += strjoin(options.toArray(),', ')
  
  textHistory = 'Elimination History Outputs: '
  options = list()
  if parameters.hubGetValue('outputHistoryRMSE', Default=1b) then options.add, 'RMSE'
  if parameters.hubGetValue('outputHistoryMinimalFractions', Default=1b) then options.add, 'Minimal Fractions'
  if parameters.hubGetValue('outputHistoryIndex', Default=1b) then options.add, 'Index'
  if parameters.hubGetValue('outputHistoryDarkFractions', Default=1b) then options.add, 'Dark Fractions' 
  textHistory += strjoin(options.toArray(),', ')
  
  report.addMonospace,  [textBasename, textFinal, textHistory]

  report.addHeading,    'Parameters', 1
  text = 'Selection Algorithm:  '+parameters['selectionAlgorithm']
  p = parameters['selectionAlgorithmParameters']
  case parameters['selectionAlgorithm'] of
    'threshold' :          text = [text, 'Threshold:            '+strtrim(p.th,2), 'Lagged Iterations:    '+strtrim(p.nl,2)]
    'trendlineThreshold' : text = [text, 'Threshold:            '+strtrim(p.th,2), 'Index Range:          '+strtrim(p.x0,2)+' to '+strtrim(p.x1,2)]
    'absolute' :           text = [text, 'Number of Endmembers: '+strtrim(-p,2)]
  endcase
  report.addMonospace,  text

  report.saveHTML, /Show

end
