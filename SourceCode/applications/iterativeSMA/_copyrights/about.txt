﻿iterativeSMA version 1.0

Copyright: © 2014 Wolfgang Mehl/Joachim Hill (University of Trier). All rights reserved.

The software was developed Wolfgang Mehl/Joachim Hill at the University of Trier.
This software is released under the LICENSE (e.g. BSD, GPL or GFZ open/close source license)
(see <EnMAP-Box installation folder>\enmapProject\application\iterativeSMA\copyrights\license.txt).

Programming: Wolfgang Mehl/Joachim Hill

Concept: Rogge, D.M., Rivard, B., Zhang, J., & Feng, J. (2006). Iterative spectral unmixing for optimizing per-pixel endmember sets. IEEE Transactions on Geoscience and Remote Sensing, 44, 3725-3736.

Homepage: www.feut.de

Contact: Wolfgang Mehl/Joachim Hill (wm@wammel.net/hillj@uni-trier.de)

Disclaimer
The authors of this software tool accept no responsibility for errors or omissions in this work and shall not be liable for any damage caused by these.

Third Parties:

1. hubAPI library was developed by Humboldt-Universit�t zu Berlin, Geography Department, Geomatics Lab (http://www.hu-geomatics.de). 
hubAPI is released under the EnMap Open Source Licence (see <EnMAP-Box installation folder>\enmapProject\lib\hubAPI\copyrights\license.txt).

2. Source code documentation was created by using IDLdoc (http://idldoc.idldev.com) developed by Michael Galloy (http://michaelgalloy.com). IDLdoc is released under a BSD-type license 
(see <EnMAP-Box installation folder>\enmapProject\lib\IDLdoc\copyrights\license.txt).



