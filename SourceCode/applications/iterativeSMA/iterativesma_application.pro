pro iterativeSMA_application, groupLeader
  parameters = iterativeSMA_getParameters(groupLeader)
  if isa(parameters) then begin
    hub_setAppState, 'iterativeSMA', 'inputEndmember', parameters['inputEndmember']
    hub_setAppState, 'iterativeSMA', 'inputImage', parameters['inputImage']

    iterativeSMA_processing, parameters, groupLeader
    iterativeSMA_showReport, parameters
  endif
end
