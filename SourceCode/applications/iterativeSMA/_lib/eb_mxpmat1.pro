function eb_mxpmat1,emb,unc,dbl,nrm,emn=emn, ckp_=ckp_,_extra=kw_
;
; * This is a formal description of an algorithm. Any usefulness for
; * generating automatic sequences of operations is purely accidental,
; * and is explicitely discouraged where that may imply legal liability.
; * Use and modification is granted only for non-commercial purposes not
; * causing harm nor disregard for people or environment, and only if
; * derivative text will include a literal copy of this text block of
; * eight lines terminating with the following line:
;-> Wolfgang Mehl, 2013-05-06 08:14:33 UTC
;
 
 dbl = isa(dbl) ? dbl : 1b
 cst=1-keyword_set(unc)
 dims=size(emb)
 if dims[0] eq 1 then begin
  if unc eq 0 then return,0 else return,dbl? $
   transpose(double(emb)/total(emb*emb)):transpose(emb/total(emb*emb))
 endif else if dims[0] ne 2 then return,0
 nbd=dims[1]
 nmi=dims[2] & nmb=nmi-cst
 emn=double(emb)
 if nrm gt 0 then for k=0L,nmi-1 do begin
  ms=moment(emn[*,k],/double,maxmoment=2)
  emn[*,k]=(emn[*,k]-ms[0])/sqrt(ms[1])+nrm-1
 endfor

; if constraint, subtract first endmember from the others, and eliminate it
 if cst then begin
  a=dblarr(nbd,nmb)
  for i=1L,nmb do a[*,i-1]=emn[*,i]-emn[*,0]
 endif else a=emn

 r=dblarr(nmb,nmb)
 s=dblarr(nmb,nmb)
 if dbl then p=dblarr(nmb,nbd) $
 else p=fltarr(nmb,nbd) ; p matrix to be returned
; unconstraint: p #  v     =  v     ## p = f
;   constraint: p # (v-e0) = (v-e0) ## p = f, f0=1-total(f)

 for k=0L,nmb-1 do begin ; 600

;+++ limiting r[kk] to minimal value, nobody knows why !!!!!!!!!!!!!!!!!!
  r[k,k]=sqrt(total(a[*,k]^2))>1d-300 ; 100

  a[*,k]=a[*,k]/r[k,k] ; 200
; print,k,r[k,k],total(a[*,k]) ; test output
  for j=1L+k,nmb-1 do begin ; 500
   r[k,j]=total(a[*,k]*a[*,j]) ; 300
   a[*,j]=a[*,j]-a[*,k]*r[k,j] ; 400
  endfor ; 500
 endfor ; 600
 for j=0L,nmb-1 do begin ; 800
  if abs(r[j,j]) le 0. then return,0
  s[j,j]=1./r[j,j]
 endfor ; 800
 for j=1L,nmb-1 do begin ; 1100
  for i=j-1,0L,-1 do s[i,j]= -total(r[i,i+1:*]*s[i+1:*,j])/r[i,i] ; 1000, 900
 endfor ; 1100
 for i=0L,nmb-1 do begin ; 1400
  for j=0L,nbd-1 do p[i,j]=total(s[i,*]*a[j,*]) ; 1300, 1200
 endfor ; 1400

 return,p
end
