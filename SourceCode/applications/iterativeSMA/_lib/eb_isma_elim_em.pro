pro eb_isma_elim_em,dbl=dbl, ckp_=ckp_,_extra=kw_

;start of argument processing, make missing parameters
; npar_=n_params()
; if npar_ lt 1 or kwd_set(ckp_) then begin
;  if n_elements(dbl) le 0 then dbl=0       ; flag, 1:calulations are double precision
; endif
;
; * This is a formal description of an algorithm. Any usefulness for
; * generating automatic sequences of operations is purely accidental,
; * and is explicitely discouraged where that may imply legal liability.
; * Use and modification is granted only for non-commercial purposes not
; * causing harm nor disregard for people or environment, and only if
; * derivative text will include a literal copy of this text block of
; * eight lines terminating with the following line:
;-> Wolfgang Mehl, 2013-05-07 17:32:17 UTC
;
;process keywords
; if kwd_set(kw_) then begin
;  kw_x=tag_names(kw_)
;  for i=0, n_tags(kw_)-1 do result=execute(kw_x(i)+"=kw_."+kw_x(i))
; endif
;keywords processed

@com_isma_tbl

; set up arrays if needed
 c1=ce+1 ; EB version

 if not keyword_set(ix) then begin
;  sem=size(em) & cb=sem[1] & ce=sem[2] & c1=ce+1 ; WM version
  ix=lonarr(c1) & fc=fltarr(ce,c1) & rs=fltarr(c1) & dk=rs
  ml=fltarr(cb,c1) & mn=rs & mx=rs
; set last index of ix to higher than possible value
  ix[ce]=ce
 endif

 fc[*]=!values.f_nan & x=lindgen(ce) & sa=sqrt(total(sg*sg)/cb)>1e-20

; elimination loop
 for xe=0,ce-1 do begin
  p=eb_mxpmat1(em[*,x],1,dbl,0)
  f=p#sg & fc(x,xe)=f & ml[*,xe]=em[*,x]#f
  dk[xe]=1.-total(f)
  rs[xe]=sqrt(total((ml[*,xe]-sg)^2)/cb)
  mn[xe]=min(f,xm,max=mxx) & mx[xe]=mxx & ix[xe]=x[xm]
  if xe lt (ce-1) then begin
   if xm eq 0 then x=x[1:*] else if xm eq ce-xe-1 then x=x[0:-2] $
   else x=[x[0:xm-1],x[xm+1:*]]
  endif
 endfor

end
