function not_void_string,x
 s=size(x)
 if s[0] gt 0 then return,1B
 if s[s[0]+1] ne 7 then return,1B
 return,strlen(x) ne 0
end
pro eb_isma_sel_em,sel,par,rng,xes=xes,xmb=xmb,rpp=rpp, ckp_=ckp_,_extra=kw_

;start of argument processing, make missing parameters
; npar_=n_params()
; if npar_ lt 5 or kwd_set(ckp_) then begin
;  if n_elements(sel) le 0 then sel=0       ; switch: selection algorithm
;  if n_elements(par) le 0 then par=0       ; parameter structure for selection parameters
;  if n_elements(rng) le 0 then rng=""      ; allowed range excesses for fractions and normalised RMS
;  if n_elements(xes) le 0 then xes=0       ; index of selected endmember set
;  if n_elements(xmb) le 0 then xmb=0       ; index list of retained endmembers
;  if n_elements(rpp) le 0 then rpp=0       ; flag, 1: range applied in post-processing
; endif
;
; * This is a formal description of an algorithm. Any usefulness for
; * generating automatic sequences of operations is purely accidental,
; * and is explicitely discouraged where that may imply legal liability.
; * Use and modification is granted only for non-commercial purposes not
; * causing harm nor disregard for people or environment, and only if
; * derivative text will include a literal copy of this text block of
; * eight lines terminating with the following line:
;-> Wolfgang Mehl, 2014-02-24 01:25:32 UTC
;
;process keywords
; if kwd_set(kw_) then begin
;  kw_x=tag_names(kw_)
;  for i=0, n_tags(kw_)-1 do result=execute(kw_x(i)+"=kw_."+kw_x(i))
; endif
;keywords processed

@com_isma_tbl

 rng=0 & rpp=1 ; EB only

 c1=ce-1

; apply fraction-range and RMS conditions
 if not_void_string(rng) then rnx=[rng,1e6,1e6,1e6]
 if keyword_set(rnx) and (rpp eq 0) then begin
  w=where(mn ge -rnx[0] and dk ge -rnx[1] and mx le (1.+rnx[2]) $
          and rs/sa le rnx[3],nw) & nw-- & n1=nw-1 & w=w[0:n1]
  if nw le 0 then begin & xes=ce & xmb=ce & return & end
 endif else begin
  nw=ce & n1=nw-1 & w=lindgen(nw)
 endelse
 n2=nw-2

; don't apply fraction range or RMS conditions
;  nw=ce & w=lindgen(nw)
 nocrit=0
 case sel of

  'threshold':begin
   if nw gt 3 then begin
    qr=1.-rs[w[0:n2]]/rs[w[1:n1]]
    for xe=n2,par.nl+1,-1 do begin
;     if qr[xe] le par then break
     if qr[xe] le par.th then begin
      if par.nl le 0 then break
      for i=1,par.nl do begin
       if qr[xe-i] gt par.th then break
      endfor
      if i gt par.nl then break
     endif
    endfor
    if xe le par.nl then nocrit=1 else xes=w[xe+1]
   endif
  end

  'trendlineThreshold':begin
; make regression
   nv=par.x1-par.x0+1 & u=lindgen(nv)+par.x0 & tu=nv*(nv-1+2*par.x0)/2
   tv=total(rs[par.x0:par.x1]) & tuu=total(u*u) & tuv=total(rs[par.x0:par.x1]*u)
   a=(nv*tuv-tu*tv)/(nv*tuu-tu*tu) & b=(tv-a*tu)/nv
;   tc=[a,b] & tl=(rs-(a*findgen(ce)+b))/par.th ; debugging
; apply regression condition
   xes=w[n1]
   for xe=n1,par.x0>1,-1 do begin
;    if (par.th*rs[w[xe]  ] gt (rsw[w[xe]  ]-(a* w[xe]   +b))) and $
;       (par.th*rs[w[xe]-1] gt (rsw[w[xe]-1]-(a*(w[xe]-1)+b))) then break
;    if (par.th*rs[w[xe  ]] gt (rsw[w[xe  ]]-(a*w[xe  ]+b))) and $
;       (par.th*rs[w[xe-1]] gt (rsw[w[xe-1]]-(a*w[xe-1]+b))) then break
    if (par.th*rs[w[xe]] gt (rs[w[xe]]-(a* w[xe]+b))) then break
    if xe le (par.x0-1) then nocrit=1 else xes=w[(xe+1)<n1]
   endfor
  end

  '2':begin
   if nw lt 3 then xes=w[0] else begin
    qr=rs[w[1:n1]]-rs[w[0:n2]]
    for xe=n2,1,-1 do begin
     if qr[xe-1]/qr[xe] le par then break
    endfor
    if xe le 0 then nocrit=1 else xes=w[xe+1]
   endelse
  end

  '3':begin
   if nw lt 3 then xes=w[0] else begin
    qr=rs[w[1:n1]]-rs[w[0:n2]]
    qt=qr[0:nw-3]/qr[1:n2]
    for xe=n2,1,-1 do begin
     if qt[xe-1] le par then begin
      if xe le 1 then break
      if qt[xe-2] gt qt[xe-1] then break
     endif
    endfor
    if xe le 0 then nocrit=1 else xes=w[xe+1]
   endelse
  end

  'absolute':begin
   xe=par ge 0?par:ce+par
; apply cutoff condition
   if w[0] ge xe then xes=xe else begin
    ww=where(w ge xe,nww)
    if nww le 0 then xes=xe else xes=w[ww[0]]
   endelse
  end
 endcase

 if nocrit then begin
  if keyword_set(rnx) and (rpp ne 0) then begin
   w=where(mn ge -rnx[0] and dk ge -rnx[1] and mx le (1.+rnx[2]) $
           and rs/sa le rnx[3],nw) & nw-- & w=w[0:n1]
   if nw le 0 then xes=0L else xes=w[0]
  endif else xes=w[0]
 endif

 xmb=ix[xes:c1>xes] & xmb=xmb[sort(xmb)]
 return

end
