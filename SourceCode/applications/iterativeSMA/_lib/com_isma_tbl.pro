common isma_com,cb,ce,em,sg,ix,fc,dk,rs,ml,mn,mx,sa,tc,tl
; cb: band count
; ce: endmember count
; em: input endmember matrix
; sg: signature to be analysed
; ix: output eliminated emb index vector
; fc: output fractions matrix
; dk: output dark fraction vector
; rs: output rms vector
; ml: output reconstructed signature matrix (models)
; mn: output minimal fraction vector (excluding shade)
; mx: output maximal fraction vector (excluding shade)
; sa: spectrum zero-RMS > 1e-20
; tc: test (regression) coefficients - debugging only
; tl: test (regression) line (debugging only)
