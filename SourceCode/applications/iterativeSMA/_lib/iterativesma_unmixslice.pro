 pro iterativeSMA_unmixSlice, imageSlice, endmemberProfiles, $
  selectionAlgorithm, selectionAlgorithmParameters, $
  FinalFractionsSlice=finalFractionsSlice, $
  FinalResidualsSlice=finalResidualsSlice, $
  FinalRMSESlice=finalRMSESlice, $
  FinalModelledSlice=finalModelledSlice, $
  HistoryRMSESlice=historyRMSESlice, $
  HistoryMinimalFractionsSlice=historyMinimalFractionsSlice, $
  HistoryIndexSlice=historyIndexSlice, $
  HistoryDarkFractionsSlice=historyDarkFractionsSlice

  @com_isma_tbl
 
  numberOfEndmembers = (size(/DIMENSIONS, endmemberProfiles))[1]
  imageSliceSize = size(/DIMENSIONS, imageSlice)

  if arg_present(FinalFractionsSlice)  or arg_present(finalResidualsSlice) or $
     arg_present(finalRMSESlice) or arg_present(finalModelledSlice) then begin
   selModel=1
   if arg_present(FinalFractionsSlice) then $
     FinalFractionsSlice = make_array(numberOfEndmembers+1, imageSliceSize[1])
   if arg_present(finalResidualsSlice) then $
     finalResidualsSlice = make_array(imageSliceSize)
   if arg_present(finalRMSESlice) then $
     finalRMSESlice = make_array(1, imageSliceSize[1])
   if arg_present(finalModelledSlice) then $
     finalModelledSlice = make_array(imageSliceSize)
  endif else selModel=0
  if arg_present(historyRMSESlice) then $
     historyRMSESlice = make_array(numberOfEndmembers, imageSliceSize[1])
  if arg_present(historyMinimalFractionsSlice) then $
     historyMinimalFractionsSlice = make_array(numberOfEndmembers, imageSliceSize[1])
  if arg_present(historyIndexSlice) then $
     historyIndexSlice = make_array(numberOfEndmembers, imageSliceSize[1], /INTEGER)
  if arg_present(historyDarkFractionsSlice) then $
     historyDarkFractionsSlice = make_array(numberOfEndmembers, imageSliceSize[1])

; Wolfgang's code -------------------------------------------------------------v
; selectionAlgorithm <- ['threshold','trendlineThreshold','absolute']
  cb=imageSliceSize[0] & npx=imageSliceSize[1] & ce=numberOfEndmembers
  em=endmemberProfiles
  for p=0L,npx-1 do begin
    
    ;#########################################
    ; bug fixing code by Andreas Rabe 7.8.2014
    ; ignore every problematic pixel
    catch, errorStatus
    if (errorStatus ne 0) then begin
      catch, /CANCEL
      message, /INFORMATIONAL, 'Catched unexpected error inside iterativeSMA.' 
      continue
    endif
    ;#########################################

    
   sg=imageSlice[*,p]
   eb_isma_elim_em,dbl=dbl
   if selModel then eb_isma_sel_em, $
      selectionAlgorithm, selectionAlgorithmParameters, xes=xes, xmb=xmb
   
   
   ;#########################################
   ; bug fixing code by Andreas Rabe 7.8.2014
   catch, /CANCEL
   ;#########################################
   
   if arg_present(FinalFractionsSlice) then $
     FinalFractionsSlice[[xmb,ce],p] = [fc[xmb,xes],dk[xes]]
   if arg_present(finalResidualsSlice) then $
     finalResidualsSlice[*,p] = sg-ml[*,xes]
   if arg_present(finalRMSESlice) then $
     finalRMSESlice[*,p] = rs[xes]
   if arg_present(finalModelledSlice) then $
     finalModelledSlice[*,p] = ml[*,xes]
   if arg_present(historyRMSESlice) then $
     historyRMSESlice[*,p] = rs[0:-2]
   if arg_present(historyMinimalFractionsSlice) then $
     historyMinimalFractionsSlice[*,p] = mn[0:-2]
   if arg_present(historyIndexSlice) then $
     historyIndexSlice[*,p] = ix[0:-2]
   if arg_present(historyDarkFractionsSlice) then $
     historyDarkFractionsSlice[*,p] = dk[0:-2]

  endfor
; Wolfgang's code -------------------------------------------------------------^
;  help, imageSlice, endmemberProfiles, finalResidualsSlice, finalRMSESlice, finalModelledSlice, historyRMSESlice, historyMinimalFractionsSlice, historyIndexSlice, historyDarkFractionsSlice
;  help, selectionAlgorithm

  return
end
