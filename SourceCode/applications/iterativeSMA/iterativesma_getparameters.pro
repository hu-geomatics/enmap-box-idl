function iterativeSMA_getParameters_check, resultHash, Message=message
  compile_opt strictarr 

  inputEndmember = hubIOImgInputImage(resultHash['inputEndmember'])
  numberOfEndmember = max(inputEndmember.getSpatialSize())
  inputImage = hubIOImgInputImage(resultHash['inputImage'])
  if inputEndmember.getSpectralSize() ne inputEndmember.getSpectralSize() then begin
    message = 'Number of bands in endmember image/speclib and image do not match.' 
    return, 0b
  endif
  
  selectionAlgorithm = iterativeSMA_getSelectionAlgorithm(resultHash['selectionAlgorithmIndex'])
  if selectionAlgorithm eq 'trendlineThreshold' then begin
    x0 = resultHash['x0']
    x1 = resultHash['x1']
    if (x0 ge numberOfEndmember) or (x1 ge numberOfEndmember) or (x0 gt x1) then begin
      message = 'inconsistent index range, must be inside 1 and number of endmembers minus 1.'
      return, 0b
    endif
  endif
  return, 1b
end

function iterativeSMA_getSelectionAlgorithm, index
  return, (['threshold','trendlineThreshold','absolute'])[index]
end

function iterativeSMA_getParameters, groupLeader
 
  hubAMW_program, groupLeader, Title = 'iterativeSMA'
  hubAMW_frame, Title='Input'
  tsize = 200
  hubAMW_inputImageFilename, 'inputEndmember', TSize=tsize, Title='Endmember SpecLib', Value=hub_getAppState('iterativeSMA', 'inputEndmember')
  hubAMW_inputImageFilename, 'inputImage', TSize=tsize, Title='Image to Unmix', Value=hub_getAppState('iterativeSMA', 'inputImage')

  hubAMW_frame, Title='Parameters'
  hubAMW_subframe, 'selectionAlgorithmIndex', Title='Select Number of retained Endmembers by Threshold', /Row, /SetButton
  hubAMW_parameter, 'th', Title='', /Float, IsGE=0, IsLE=1, Value=0.5
  hubAMW_parameter, 'nl', Title=' Number of lagged Iterations', /Integer, Value=0
  
  hubAMW_subframe, 'selectionAlgorithmIndex', Title='Select Number of retained Endmembers by Trendline Threshold', /Row
  hubAMW_parameter, 'th', Title='', /Float, IsGE=0, IsLE=1, Value=0.5
  hubAMW_parameter, 'x0', Title=' Index Range from ', /Integer, IsGE=1, Value=1
  hubAMW_parameter, 'x1', Title=' to ', /Integer, IsGE=1

  hubAMW_subframe, 'selectionAlgorithmIndex', Title='Set fixed Number of retained Endmembers', /Row
  hubAMW_parameter, 'numberOfSelectedEndmembers', Title='', /Integer, IsGE=1
       
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputBasename', Title='Basename', Value='isma', TSize=tsize
  hubAMW_checklist, 'outputFinal',   TITLE='Additional Output for Final Model:         ', LIST=['Fractions','Residuals','Modelled','RMSE'], /MultipleSelection, Value=[0,1,2,3]
  hubAMW_checklist, 'outputHistory', TITLE='Additional Output for Elimination History: ', LIST=['RMSE','Minimal Fractions','Index','Dark Fractions'], /MultipleSelection, Value=[0,1,2,3]
                                                                                                         
  result = hubAMW_manage(ConsistencyCheckFunction='iterativeSMA_getparameters_check')
  
  if result['accept'] then begin
    parameters = hash()
    parameters['inputEndmember'] = result['inputEndmember']
    parameters['inputImage'] = result['inputImage']
    parameters['outputBasename'] = result['outputBasename']

    parameters['selectionAlgorithm'] = iterativeSMA_getSelectionAlgorithm(result['selectionAlgorithmIndex'])
    case parameters['selectionAlgorithm'] of
      'threshold' : parameters['selectionAlgorithmParameters'] = {th:result['th'], nl:result['nl']}
      'trendlineThreshold' : parameters['selectionAlgorithmParameters'] = {x0:result['x0'], x1:result['x1'], th:result['th']}
      'absolute' : parameters['selectionAlgorithmParameters'] = -result.hubGetValue('numberOfSelectedEndmembers')
    endcase
    
    outputFinal = list(result['outputFinal'], /EXTRACT)
    parameters['outputFinalFractions'] = outputFinal.hubIsElement(0)
    parameters['outputFinalResiduals'] = outputFinal.hubIsElement(1)
    parameters['outputFinalModelled'] = outputFinal.hubIsElement(2)
    parameters['outputFinalRMSE'] = outputFinal.hubIsElement(3)

    outputHistory = list(result['outputHistory'], /EXTRACT)
    parameters['outputHistoryRMSE'] = outputHistory.hubIsElement(0)
    parameters['outputHistoryMinimalFractions'] = outputHistory.hubIsElement(1)
    parameters['outputHistoryIndex'] = outputHistory.hubIsElement(2)
    parameters['outputHistoryDarkFractions'] = outputHistory.hubIsElement(3)
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

pro test_iterativeSMA_getparameters
  print, iterativeSMA_getparameters()
end