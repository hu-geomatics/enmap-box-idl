;+
; :Author: <author name> (<email>)
;-

function iterativeSMA_getDirname
  
  result = filepath('iterativeSMA', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_iterativeSMA_getDirname

  print, iterativeSMA_getDirname()
  print, iterativeSMA_getDirname(/SourceCode)

end
