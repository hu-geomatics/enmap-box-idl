;+
; :Description:
;    Describ iterativeSVM...     
;
; :Params:
;    parameters: in, required, type=hash
;      This is the parameter hash used to provide required and optional parameters. 
;      (`*` = optional)::
;      
;          key                           | type            | description
;        --------------------------------+-----------------+-----------------------------------------------------------
;          inputEndmember                | string          | filename of endmember library
;          inputImage                    | string          | filename of image to be unmixed
;          outputBasename                | string          | file basename of results
;          selectionAlgorithm            | string          | {'threshold' | 'trendlineThreshold' | 'absolute'}
;          selectionAlgorithmParameters  | string | struct | number in case of 'threshold'
;                                                            structure = {x0:number, x1:number, th:number} in case of 'trendlineThreshold'
;                                                            number of endmember in case of 'absolute'                                                        
;        * outputFinalFractions          | boolean         | output image with fractions?
;        * outputFinalModelled           | boolean         | output image with modelled profiles?
;        * outputFinalResiduals          | boolean         | output image with band-wise residuals?        
;        * outputFinalRMSE               | boolean         | output image band with RMSE?
;        * outputHistoryRMSE'            | boolean         | output image with RMSE  history?
;        * outputHistoryMinimalFractions | boolean         | output image with minimal fraction history?
;        * outputHistoryIndex            | boolean         | output image with index history?
;        * outputHistoryDarkFractions    | boolean         | output image with dark fraction history?
;        --------------------------------+-----------------+----------------------------------------------------------
;   
;    groupLeader: in, optional, type=long
;       Use this keyword to specify the group leader for used widgets.
;       
; :Keywords:  
; 
;    NoShow: in, optional, type=boolean
;       Set this keyword to avoid showing GUI events, for instance a progress bar.
;
;-
pro iterativeSMA_processing, parameters, groupLeader, NoShow=NoShow

  ;check required parameters 
  required = ['inputEndmember','inputImage','outputBasename','selectionAlgorithm','selectionAlgorithmParameters']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. Missing values for keys: '+strjoin(required[missing],', ')
  endif
  outputFinalFractions = parameters.hubGetValue('outputFinalFractions', Default=1b)
  outputFinalResiduals = parameters.hubGetValue('outputFinalResiduals', Default=1b)
  outputFinalRMSE = parameters.hubGetValue('outputFinalRMSE', Default=1b)
  outputFinalModelled = parameters.hubGetValue('outputFinalModelled', Default=1b)
  outputHistoryRMSE = parameters.hubGetValue('outputHistoryRMSE', Default=1b)
  outputHistoryMinimalFractions = parameters.hubGetValue('outputHistoryMinimalFractions', Default=1b)
  outputHistoryIndex = parameters.hubGetValue('outputHistoryIndex', Default=1b)
  outputHistoryDarkFractions = parameters.hubGetValue('outputHistoryDarkFractions', Default=1b)

  progressBar = hubProgressBar(Title='iterativeSMA', GroupLeader=groupLeader, NoShow=NoShow)
  progressBar.setInfo, 'load endmembers'
  
  ; read endmember
  endmemberSpeclib = hubIOImgInputImage(parameters['inputEndmember'])
  endmemberProfiles = transpose(endmemberSpeclib.getCube(),[2,1,0])

  numberOfEndmembers = n_elements(endmemberProfiles[0,*])

  ; perform tile-processing
  ; - create image objects
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  if outputFinalFractions then outputFinalFractionsImage = hubIOImgOutputImage(parameters['outputBasename']+'Fractions')
  if outputFinalResiduals then outputFinalResidualsImage = hubIOImgOutputImage(parameters['outputBasename']+'Residuals')
  if outputFinalRMSE then outputFinalRMSEImage = hubIOImgOutputImage(parameters['outputBasename']+'RMSE')
  if outputFinalModelled then outputFinalModelledImage = hubIOImgOutputImage(parameters['outputBasename']+'Modelled')
  if outputHistoryRMSE then outputHistoryRMSEImage = hubIOImgOutputImage(parameters['outputBasename']+'HistoryRMSE')
  if outputHistoryMinimalFractions then outputHistoryMinimalFractionsImage = hubIOImgOutputImage(parameters['outputBasename']+'HistoryMinimalFractions')
  if outputHistoryIndex then outputHistoryIndexImage = hubIOImgOutputImage(parameters['outputBasename']+'HistoryIndex')
  if outputHistoryDarkFractions then outputHistoryDarkFractionsImage = hubIOImgOutputImage(parameters['outputBasename']+'HistoryDarkFractions')

  ; - set meta information
  endmemberNames = endmemberSpeclib.getMeta('spectra names')
  if outputFinalFractions then begin
    outputFinalFractionsImage.copyMeta, inputImage, /CopySpatialInformation
    outputFinalFractionsImage.setMeta, 'band names', [endmemberNames, 'dark fraction']
  endif
  if outputFinalResiduals then begin
    outputFinalResidualsImage.copyMeta, inputImage, /CopySpatialInformation, /CopySpectralInformation
  endif
  if outputFinalRMSE then outputFinalRMSEImage.copyMeta, inputImage, /CopySpatialInformation
  if outputFinalModelled then outputFinalModelledImage.copyMeta, inputImage, /CopySpatialInformation, /CopySpectralInformation
  historyBandNames = strtrim(numberOfEndmembers-indgen(numberOfEndmembers),2)+'-endmember model'
  if outputHistoryRMSE then begin
    outputHistoryRMSEImage.copyMeta, inputImage, /CopySpatialInformation
    outputHistoryRMSEImage.setMeta, 'band names', historyBandNames
  endif
  if outputHistoryMinimalFractions then begin
    outputHistoryMinimalFractionsImage.copyMeta, inputImage, /CopySpatialInformation
    outputHistoryMinimalFractionsImage.setMeta, 'band names', historyBandNames
  endif
  if outputHistoryIndex then begin
    outputHistoryIndexImage.copyMeta, inputImage, /CopySpatialInformation
    outputHistoryIndexImage.setMeta, 'band names', historyBandNames
  endif
  if outputHistoryDarkFractions then begin
    outputHistoryDarkFractionsImage.copyMeta, inputImage, /CopySpatialInformation
    outputHistoryDarkFractionsImage.setMeta, 'band names', historyBandNames
  endif

  ; - init reader
  ;numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)
  numberOfTileLines = 100 ; replace in EnMAP-Box 2.0 with above line   
  inputImage.initReader, numberOfTileLines, /Slice, /TileProcessing
  
  ; - init writer
  if outputFinalFractions then outputFinalFractionsImage.initWriter, inputImage.getWriterSettings(SetDataType='float', SetBands=numberOfEndmembers+1)
  if outputFinalResiduals then outputFinalResidualsImage.initWriter, inputImage.getWriterSettings(SetDataType='float')
  if outputFinalRMSE then outputFinalRMSEImage.initWriter, inputImage.getWriterSettings(SetDataType='float', SetBands=1)
  if outputFinalModelled then outputFinalModelledImage.initWriter, inputImage.getWriterSettings(SetDataType='float')
  if outputHistoryRMSE then outputHistoryRMSEImage.initWriter, inputImage.getWriterSettings(SetDataType='float', SetBands=numberOfEndmembers)
  if outputHistoryMinimalFractions then outputHistoryMinimalFractionsImage.initWriter, inputImage.getWriterSettings(SetDataType='float', SetBands=numberOfEndmembers)
  if outputHistoryIndex then outputHistoryIndexImage.initWriter, inputImage.getWriterSettings(SetDataType='int', SetBands=numberOfEndmembers)
  if outputHistoryDarkFractions then outputHistoryDarkFractionsImage.initWriter, inputImage.getWriterSettings(SetDataType='float', SetBands=numberOfEndmembers)

  progressBar.setInfo, 'unmix image'
  currentLine = 0
  progress = 0.
  numberOfTiles = (inputImage.getWriterSettings(SetDataType='float', SetBands=numberOfEndmembers))['numberOfTiles']
  while ~inputImage.tileProcessingDone() do begin
    progress++
    progressBar.setProgress, progress/numberOfTiles
    
    ; read data
    imageSlice = inputImage.getData()
    
    ; unmix each profile in the imageSlice
    ; ++++++++++++++++++++++++++++++++++++++++
    ; +++ Here comes the iterativeSMA code +++

 ;   myBackup_iterativeSMA_unmixSlice, imageSlice, endmemberProfiles, parameters['selectionAlgorithm'], parameters['selectionAlgorithmParameters'], $
    hubMath_mathErrorsOff, oldState
    iterativeSMA_unmixSlice, imageSlice, endmemberProfiles, parameters['selectionAlgorithm'], parameters['selectionAlgorithmParameters'], $
      FinalFractionsSlice=          outputFinalFractions          ? finalFractionsSlice : [],$
      FinalResidualsSlice=          outputFinalResiduals          ? finalResidualsSlice : [],$
      FinalRMSESlice=               outputFinalRMSE               ? finalRMSESlice : [],$
      FinalModelledSlice=           outputFinalModelled           ? finalModelledSlice : [],$
      HistoryRMSESlice=             outputHistoryRMSE             ? historyRMSESlice : [],$
      HistoryMinimalFractionsSlice= outputHistoryMinimalFractions ? historyMinimalFractionsSlice : [],$
      HistoryIndexSlice=            outputHistoryIndex            ? historyIndexSlice : [],$
      HistoryDarkFractionsSlice=    outputHistoryDarkFractions    ? historyDarkFractionsSlice : []
    hubMath_mathErrorsOn, oldState

    ; ++++++++++++++++++++++++++++++++++++++++
        
    ; write data
;    stop
    if outputFinalFractions then outputFinalFractionsImage.writeData, finalFractionsSlice
    if outputFinalResiduals then outputFinalResidualsImage.writeData, finalResidualsSlice
    if outputFinalRMSE then outputFinalRMSEImage.writeData, finalRMSESlice
    if outputFinalModelled then outputFinalModelledImage.writeData, finalModelledSlice
    if outputHistoryRMSE then outputHistoryRMSEImage.writeData, historyRMSESlice
    if outputHistoryMinimalFractions then outputHistoryMinimalFractionsImage.writeData, historyMinimalFractionsSlice
    if outputHistoryIndex then outputHistoryIndexImage.writeData, historyIndexSlice
    if outputHistoryDarkFractions then outputHistoryDarkFractionsImage.writeData, historyDarkFractionsSlice
  endwhile
  
  inputImage.cleanup
  if outputFinalFractions then outputFinalFractionsImage.cleanup
  if outputFinalResiduals then outputFinalResidualsImage.cleanup
  if outputFinalRMSE then outputFinalRMSEImage.cleanup
  if outputFinalModelled then outputFinalModelledImage.cleanup
  if outputHistoryRMSE then outputHistoryRMSEImage.cleanup
  if outputHistoryMinimalFractions then outputHistoryMinimalFractionsImage.cleanup
  if outputHistoryIndex then outputHistoryIndexImage.cleanup
  if outputHistoryDarkFractions then outputHistoryDarkFractionsImage.cleanup
  
  progressBar.cleanup
end

;+
; :hidden:
;-
pro test_iterativeSMA_processing
  
  parameters = hash($
    'inputEndmember', 'emb.slb',$
    'inputImage', 'fi',$
    'outputBasename','test',$
    'selectionAlgorithm',(['threshold','trendlineThreshold','absolute'])[2])
    
  case parameters['selectionAlgorithm'] of
    'threshold' : parameters['selectionAlgorithmParameters'] = 0.6667
    'trendlineThreshold' : parameters['selectionAlgorithmParameters'] = {x0:1,x1:2,th:0.4}
    'absolute' : parameters['selectionAlgorithmParameters'] = -3
  endcase

  iterativeSMA_processing, parameters
  iterativeSMA_showReport, parameters
end
