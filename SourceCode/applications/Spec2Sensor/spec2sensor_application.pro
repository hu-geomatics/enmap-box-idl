;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `Spec2Sensor_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `Spec2Sensor_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `Spec2Sensor_showReport`).
;
; :Keywords:
;    Title : in, optional, type=string
;      Application title.
;      
;    GroupLeader : in, optional, type=widget id
;      Group leader for widget programs.
;
;    Argument : in, optional, type=widget id
;      Argument given button description inside the enmap.men file.
;-
pro Spec2Sensor_application, Title=title, GroupLeader=groupLeader, Argument=argument
  
  ; Note:
  ; use settings if needed
  ;   settings = Spec2Sensor_getSettings()
  ; use argument keyword if needed
  
  parameters = Spec2Sensor_getParameters(Title=title, GroupLeader=groupLeader)
  
  if isa(parameters) then begin
    reportInfo = Spec2Sensor_processing(parameters, Title=title, GroupLeader=groupLeader)
    Spec2Sensor_showReport, reportInfo, Title=title
  endif
end

pro test_Spec2Sensor_application
  Spec2Sensor_application, Title='Spec2Sensor', GroupLeader=groupLeader, Argument=argument
end  
