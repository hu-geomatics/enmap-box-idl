# -*- coding: utf-8 -*-
import numpy as np
from rios import applier
import time

start_time = time.time() # Für Zeitberechnung: Start

oi = applier.OtherInputs() # Klasse für alle Inputs, die später in die RIOS-Funktion übergeben werden

# Input from IDL:

ImgIn = "C:\\Users\\geo_mahe\\Downloads\\Danner\\20120428_Neusling_AVIS" # File/evtl. mit Pfad für das Image-File
ImgOut = "C:\\Users\\geo_mahe\\Downloads\\Danner\\AVIS2EnMAP.bsq"        # Outputfile
oi.nodatval = -999               # Data Ignore Value for input and output
oi.sens_modus = 2                # Sensor-Modus (1 = Sentinel2, 2 = EnMAP)

if oi.sens_modus == 1:
    SRF_File = "S2_srf.rsp" # Dieses File muss im gleichen Verzeichnis liegen!
    oi.lambda_sensor = [442.25, 492.22, 560.31, 663.09, 703.96, 742.38, 781.72, 833.33, 865.82, 942.25,
    1373.67, 1609.43, 2193.89] # Zentrumswellenlängen von Sentinel-2

    fwhm = [18.00, 63.00, 34.00, 28.00, 14.00, 14.00, 18.00, 103.00, 22.00, 19.00,
    28.00, 87.00, 172.00] # Full Width Half Maxima von Sentinel-2

    line_length = [10, 3] # für den Output-Header (Items pro Zeile)

elif oi.sens_modus == 2:
    SRF_File = "C:\\Users\\geo_mahe\\Downloads\\Danner\\E_srf.rsp" # Dieses File muss im gleichen Verzeichnis liegen!
    oi.lambda_sensor = [423.00, 429.00, 434.00, 440.00, 445.00, 450.00, 455.00, 460.00, 464.00, 469.00,
    474.00, 479.00, 484.00, 488.00, 493.00, 498.00, 503.00, 508.00, 513.00, 518.00,
    523.00, 528.00, 533.00, 538.00, 543.00, 548.00, 553.00, 559.00, 564.00, 569.00,
    575.00, 580.00, 586.00, 592.00, 597.00, 603.00, 609.00, 615.00, 621.00, 627.00,
    633.00, 639.00, 645.00, 652.00, 658.00, 665.00, 671.00, 678.00, 685.00, 691.00,
    698.00, 705.00, 712.00, 719.00, 726.00, 733.00, 740.00, 748.00, 755.00, 762.00,
    770.00, 777.00, 785.00, 793.00, 800.00, 808.00, 816.00, 823.00, 831.00, 839.00,
    847.00, 855.00, 863.00, 871.00, 879.00, 887.00, 895.00, 903.00, 911.00, 920.00,
    928.00, 936.00, 944.00, 952.00, 961.00, 969.00, 977.00, 985.00, 905.00, 914.00,
    924.00, 934.00, 944.00, 954.00, 965.00, 975.00, 986.00, 996.00, 1007.00, 1018.00,
    1029.00, 1040.00, 1051.00, 1063.00, 1074.00, 1085.00, 1097.00, 1109.00, 1120.00, 1132.00,
    1144.00, 1155.00, 1167.00, 1179.00, 1191.00, 1203.00, 1215.00, 1227.00, 1239.00, 1251.00,
    1263.00, 1275.00, 1287.00, 1299.00, 1311.00, 1323.00, 1335.00, 1347.00, 1359.00, 1370.00,
    1382.00, 1394.00, 1406.00, 1418.00, 1430.00, 1441.00, 1453.00, 1465.00, 1476.00, 1488.00,
    1499.00, 1511.00, 1522.00, 1534.00, 1545.00, 1556.00, 1568.00, 1579.00, 1590.00, 1601.00,
    1612.00, 1623.00, 1634.00, 1645.00, 1656.00, 1667.00, 1678.00, 1689.00, 1699.00, 1710.00,
    1721.00, 1731.00, 1742.00, 1752.00, 1762.00, 1773.00, 1783.00, 1793.00, 1804.00, 1814.00,
    1824.00, 1834.00, 1844.00, 1854.00, 1864.00, 1874.00, 1884.00, 1893.00, 1903.00, 1913.00,
    1922.00, 1932.00, 1942.00, 1951.00, 1961.00, 1970.00, 1979.00, 1989.00, 1998.00, 2007.00,
    2016.00, 2026.00, 2035.00, 2044.00, 2053.00, 2062.00, 2071.00, 2080.00, 2089.00, 2098.00,
    2106.00, 2115.00, 2124.00, 2133.00, 2141.00, 2150.00, 2159.00, 2167.00, 2176.00, 2184.00,
    2193.00, 2201.00, 2209.00, 2218.00, 2226.00, 2234.00, 2243.00, 2251.00, 2259.00, 2267.00,
    2275.00, 2283.00, 2291.00, 2299.00, 2307.00, 2315.00, 2323.00, 2331.00, 2339.00, 2347.00,
    2355.00, 2363.00, 2370.00, 2378.00, 2386.00, 2393.00, 2401.00, 2409.00, 2416.00, 2424.00,
    2431.00, 2439.00]

    fwhm = [6.00,   6.00,   6.00,   6.00,   6.00,   4.00,   4.00,   4.00,   4.00,   4.00,
    4.00,   4.00,   4.00,   4.00,   4.00,   4.00,   4.00,   4.00,   4.00,   4.00,
    6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,
    6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,
    6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   6.00,   8.00,   8.00,   8.00,
    8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,
    8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,
    8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,
    8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,  10.00,  10.00,
    10.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,
    12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  14.00,  14.00,
    14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,
    14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  14.00,
    14.00,  14.00,  14.00,  14.00,  14.00,  14.00,  12.00,  12.00,  12.00,  12.00,
    12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,
    12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,
    12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,  12.00,
    12.00,  12.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,
    10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,
    10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,
    10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,  10.00,
    10.00,  10.00,  10.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,
    8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,
    8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,   8.00,
    8.00,   8.00] # Full Width Half Maximum von EnMAP

    line_length = [10]*24
    line_length.append(2) # 24x10 Items pro Zeile + 2 = 242 EnMAP-Kanäle

else:
    raise SystemExit # falscher Sensor-Modus?

infile = applier.FilenameAssociations()
infile.image = ImgIn

outfile = applier.FilenameAssociations()
outfile.outimage = ImgOut

# Find Header-File for Input-Image
Hdrfile = ImgIn.split('.')
Hdrfile = Hdrfile[0] + '.hdr'

with open(Hdrfile, 'r') as Header_In:
    lines = Header_In.readlines()

wave_flag = 0 # Flag für das Finden der Zeile mit den Wellenlängen im Input-Header
wave_convert = 0 # ggf. Umrechnung in Nanometer

for i, line in enumerate(lines):
    if line.lower().startswith("wavelength = "): # wenn die Zeile mit den Wellenlängen gefunden wird, Flag setzen und Startzeile speichern
        wave_flag = 1
        i_start = i
    if '}' in line and wave_flag == 1: # wenn der Bereich mit den Wellenlängen endet, Stopzeile speichern und loop abbrechen
        i_stop = i+1
        wave_flag = 0
    if "wavelength units" in line.lower(): # suche die Zeile mit der Information zur Einheit
        if "micrometers" in line.lower():
            wave_convert = 1000 # µm werden in Nanometer umgerechnet (Faktor 1000)
        elif "nanometers" in line.lower():
            wave_convert = 1 # nm bleiben Nanometer (Faktor 1)
        else:
            raise SystemExit

string_chaos = [lines[i_start + i].rstrip() for i in range(0,i_stop-i_start)] # \n abschneiden
string_chaos = ",".join(string_chaos) # Items zusammenfügen
string_chaos = string_chaos[string_chaos.index('{')+1:string_chaos.index('}')] # Auslesen aller Wellenlängen zwischen { & }
string_chaos = string_chaos.split(',') # Endprodukt: Alle Wellenlängen als Items einer String-Liste

oi.lambd = [int(float(item)*wave_convert) for item in string_chaos if not item==""] # Umwandlung der Strings in Integer (ganzzahlige Wellenlängen vorausgesetzt)

# Einlesen der SRFs (Spectral Response Functions)
with open(SRF_File, 'r') as SRF:
    lines = SRF.readlines()

# Get number of SRF-Bands per sensor band
srf_nbands = lines[0].split(' ')
srf_nbands = [int(srf_nbands[i]) for i in range(0,len(srf_nbands),2)]

del lines[0] # remove Header
srf = np.zeros((len(lines),(len(srf_nbands)*2))) # Erstellen der leeren Matrix für alle SRF-Werte

for line_num in range(len(lines)):
    line_ex = lines[line_num].split(' ') # Aufspalten der Items pro Zeile in Einzel-Werte
    line_ex = [oi.nodatval if "NA" in line_ex[i] else float(line_ex[i]) for i in range(len(line_ex))] # Umwandlung Str->Float, bzw. Setzen von NoDataValue
    srf[line_num,:] = line_ex # Einfügen in die fertige SRF-Matrix

srf = srf.reshape(len(lines),len(srf_nbands),2) # 1. Dim: SRF, 2. Dim: Sensor-Kanal, 3. Dimenson: [0] Wavelength und [1] SRF-Wert

### Angepasste SRF, mit Downgrade auf spektrale Auflösung des Input-Bilds (z.B. EnMAP)
oi.new_srf = np.copy(srf)
oi.new_srf.fill(oi.nodatval)

# Debug-Hilfe:
# srf_nbands: Anzahl an SRFs pro Output-Sensor-Kanal (z.B: 37, 87, 52 ... für Sentinel). len(srf_nbands) ist Kanalanzahl
# lambd: Wellenlängen der Kanäle des Input-Sensors (z.B. ASD oder EnMAP)

# Sprungmarker:
## sens_band: bei welchem Kanal des Output-Sensors befinden wir uns gerade (z.B. Sentinel: 1 bis 13)
## sens_lambd: bei welcher Wellenlänge des Input-Sensors befinden wir uns gerade
## srf_lambd: bei welcher Wellenlänge des Output-Sensors befinden wir uns gerade

oi.new_srf_nbands = []

for sens_band in range(len(srf_nbands)):
    mask = [] # setze Masken-Liste zurück

    for sens_lambd in range(len(oi.lambd)): # für jede Wellenlänge des Input-Sensors

        for srf_lambd in range(srf_nbands[sens_band]): # jeder Wert der SRF für die Wellenlänge des Input-Sensors

            if oi.lambd[sens_lambd] == int(srf[srf_lambd,sens_band,0]*1000): # Bei Treffer (SRF vorhanden: in nm!)
                mask.append(srf_lambd) # den Wert zur Maske hinzufügen

    for i in range(len(mask)): # die Maske in die neue SRF-Matrix ausschütten
        oi.new_srf[i,sens_band,:] = srf[mask[i],sens_band,:]

    oi.new_srf_nbands.append(len(mask)) # Update der srf_nbands (wie viele SRF-Werte pro Kanal, hier: im Output-Sensor)

### Ende New_SRF

lambd_full = range(oi.lambd[0], oi.lambd[len(oi.lambd)-1]+1)  # oi.lambd enthält nur die Wellenlängen des Sensors,
                                                              # lambd_full schließt alle Lücken dazwischen

controls = applier.ApplierControls()
controls.setOutputDriverName("ENVI")
controls.setCreationOptions(["INTERLEAVE=BSQ"])
controls.setStatsIgnore(oi.nodatval)

# die eigentliche Funktion, die Applier anwenden soll:
def spec2sensor(info, input, output, oi):

    lens =  [len(input.image[:,0,0]), len(input.image[0,:,0]), len(input.image[0,0,:])] # Dim1: bands, Dim2: rows, Dim3: cols
    spec_out = np.empty((len(oi.lambda_sensor),lens[1],lens[2])) # Output, der später in die Kanäle geschrieben wird
    spec_out.fill(oi.nodatval)

    for line in range(lens[1]):
        for col in range(lens[2]):

            spectrum = input.image[:,line,col]  # Array mit den Reflektanzen vom Input-File für das jeweilige Pixel
            hash_getspec = dict(zip(oi.lambd, spectrum)) # Erstelle eine Hashmap, die jeder Wellenlänge ein Spektrum zuweist

            if oi.nodatval in spectrum or np.isnan(spectrum).all(): # wenn im Spektrum das NoDataValue vorkommt
                spec_out[:,line,col] = [oi.nodatval]*len(oi.lambda_sensor) # alles auf NoData setzen
                continue

            spec_corr = [0] * len(oi.lambda_sensor) # temporäre Null-List zur Übertragung in die finale Matrix

            for sensor_band in range(len(oi.lambda_sensor)): # für jeden Kanal des Zielsensors...

                if oi.new_srf_nbands[sensor_band] == 0: # falls für einen Kanal des Zielsensors keine Input-Daten vorhanden sind,
                    spec_corr[sensor_band] = oi.nodatval # wird dieser Kanal auf NoDatVal oder 0 gesetzt
                    continue

                for srf_i in range(oi.new_srf_nbands[sensor_band]): # für jeden gültigen SRF-Wert im jeweiligen Kanal

                    wlambda = int(oi.new_srf[srf_i][sensor_band][0]*1000) # Wellenlänge des W-faktors, Umwandlung in nm und Integer
                    wfactor = oi.new_srf[srf_i][sensor_band][1] # Wichtungsfaktor
                    spec_corr[sensor_band] = spec_corr[sensor_band] + hash_getspec[wlambda]*wfactor # Aufsummieren der korrigierten Spektren

                spec_corr[sensor_band] = spec_corr[sensor_band]/sum(oi.new_srf[0:oi.new_srf_nbands[sensor_band],sensor_band,1]) # Teilen durch Summe der W-Faktoren

            spec_out[:,line,col] = spec_corr # Übertragen in finale Spectrum-Matrix

            if oi.sens_modus == 1: spec_out[10,line,col] = 0 # 11. Kanal ist bei synthetischen Sentinel2-Bildern immer 0 (cirrus mask)

    output.outimage = spec_out

applier.apply(spec2sensor, infile, outfile, controls=controls, otherArgs=oi)

Out_Hdr = ImgOut.split('.') # Output-Header finden
Out_Hdr = Out_Hdr[0] + '.hdr'

Hdr_deposit = [] # Header-Erweiterung als Liste generieren
Hdr_deposit.append('data ignore value = ' + str(oi.nodatval) + '\n')
Hdr_deposit.append('wavelength units = nanometers' + '\n')
Hdr_deposit.append('wavelength = {' + '\n')

# Wavelengths
for i in range(len(line_length)): # für jede neue Zeile (max. 10 Items!)
    Hdr_deposit.append(', '.join(map(str, oi.lambda_sensor[10*i:(10*i)+line_length[i]])) + ',\n') # 10 Wavelengths anfügen

# Letztes Komma durch } ersetzen:
temp = list(Hdr_deposit[-1]) # Umwandlung Str->List
Hdr_deposit.pop(-1) # letzte Hdr-Zeile abspalten
temp[-2] = "}" # Komma in } umschreiben
Hdr_deposit.append("".join(temp)) # Umwandlung List->Str und erneutes Anheften

# FWHM
Hdr_deposit.append('fwhm = {' + '\n')

for i in range(len(line_length)):
    Hdr_deposit.append(', '.join(map(str, fwhm[10*i:(10*i)+line_length[i]])) + ',\n')

temp = list(Hdr_deposit[-1])
Hdr_deposit.pop(-1)
temp[-2] = "}"
Hdr_deposit.append("".join(temp))

with open(Out_Hdr, 'a') as Header_Out:
    for header_line in Hdr_deposit:
        Header_Out.write(header_line) # neue Header-Elemente anfügen

print("---calculation took %s seconds ---" % (time.time() - start_time))