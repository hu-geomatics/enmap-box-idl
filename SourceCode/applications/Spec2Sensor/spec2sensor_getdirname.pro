;+
; :Author: <author name> (<email>)
;-

function Spec2Sensor_getDirname
  
  result = filepath('Spec2Sensor', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_Spec2Sensor_getDirname

  print, Spec2Sensor_getDirname()
  print, Spec2Sensor_getDirname(/SourceCode)

end
