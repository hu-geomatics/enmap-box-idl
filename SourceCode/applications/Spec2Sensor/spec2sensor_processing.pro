;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`Spec2Sensor_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;
;-
function Spec2Sensor_processing, parameters, Title=title, GroupLeader=groupLeader

  parameters['myArgument'] = 'Hello World from EnMAP-Box :-).'
  
  scriptFilename = filepath('Spec2Sensor.py', ROOT_DIR=Spec2Sensor_getDirname(), SUBDIRECTORY='lib')
  scriptResult = hubPython_runScript(scriptFilename, parameters, spawnResult, spawnError, Title=title, GroupLeader=groupLeader)
  myResult = scriptResult['myResult']

  ; store results to be reported inside a hash
  result = hash()

  return, result
end

;+
; :Hidden:
;-
pro test_Spec2Sensor_processing
  ; test your routine
  parameters = hash() ;replace by own hash definition

  
  result = Spec2Sensor_processing(parameters)
  print, result
end  
