; original R source code by Sebastian Schmidtlein
;
; adapted to IDL by Carsten Oldenburg

;+
; :Hidden:
;-
FUNCTION autoPLSR_findMaxima, vector

  ON_ERROR,2

  vec = vector
  nanIndex = WHERE(FINITE(vector,/NAN))
  vec[nanIndex] = 0

  vec0 = SHIFT(vec,-2)
  vec1 = SHIFT(vec,-1)
  vec3 = SHIFT(vec,1)
  vec4 = SHIFT(vec,2)
  vec0[-2:*] = 0
  vec1[-1] = 0
  vec3[0] = 0
  vec4[0:1] = 0

  maxima = (vec gt vec0) AND (vec ge vec1) AND (vec ge vec3) AND (vec gt vec4)
  RETURN, WHERE(maxima eq 1,/NULL)
END

;+
; :Hidden:
;-
FUNCTION autoPLSR_localMaximum, vector

  ON_ERROR,2

  ;Treat positive and negative values separately
  rcapos = vector
  rcaneg = vector
  rcaposFinite = WHERE(FINITE(rcapos))
  rcapos[rcaposFinite[WHERE(rcapos[rcaposFinite] le 0,/NULL)]] = !VALUES.F_NAN
  locpos = autoPLSR_findMaxima(rcapos)
  
  rcanegFinite = WHERE(FINITE(rcaneg))
  rcaneg[rcanegFinite[WHERE(rcaneg[rcanegFinite] ge 0,/NULL)]] = !VALUES.F_NAN
  rcaneg = ABS(rcaneg)
  locneg = autoPLSR_findMaxima(rcaneg)
  
  loc = [locpos,locneg]
  IF ISA(loc) THEN loc = loc(SORT(loc))
  
  RETURN, loc
END

;+
; :Hidden:
; Reduce autocorrelation
;-
FUNCTION autoPLSR_ac $
  , vector $
  , selcode $
  , subX $
  , POW=pow

  ON_ERROR,2

  IF (~ KEYWORD_SET(POW) ) THEN pow = 1d

  ; Compute correlation matrix of predictors
  correlationMatrix = CORRELATE(TRANSPOSE(subX),/DOUBLE)
  ; Mean nearest neighbor
  diag = LINDGEN((SIZE(correlationMatrix,/DIMENSIONS))[0])
  correlationMatrix[diag,diag] = -1
  nn = MAX(correlationMatrix,DIMENSION=1)
  mnn = MEDIAN(nn, /EVEN)
  
  thinout = vector
  thinout[WHERE(selcode eq 0,/NULL)] = !VALUES.F_NAN
  thin = !NULL
  REPEAT BEGIN
    ; Get local maxima from regression coefficients
    loc = autoPLSR_localMaximum(thinout)
    ; Add this selection to overall selection
    thin = [thin,loc]
    ; Correlation to local maxima larger than criterion?
    IF ISA(loc) THEN snn = correlationMatrix[loc,*] ge (mnn / pow) ELSE $
      snn = BYTARR((SIZE(correlationMatrix,/DIMENSIONS))[0])
    ; Remove such predictors
    IF N_ELEMENTS(loc) gt 1 THEN BEGIN
      thinout[WHERE(TOTAL(snn,1) gt 0,/NULL)] = !VALUES.F_NAN
    ENDIF ELSE thinout[WHERE(snn eq 1,/NULL)] = !VALUES.F_NAN
    ; Remove previous local maxima
;    IF ISA(loc) THEN 
    thinout[thin] = !VALUES.F_NAN ;ELSE thinout[thin] = 0
    ; Check if there are at least two predictors left
  ENDREP UNTIL ( N_ELEMENTS(WHERE(FINITE(thinout))) lt 3 ) $
    or ( ~ ISA(loc) ) 
  
  result = BYTARR(N_ELEMENTS(vector))
;  IF ISA(loc) THEN 
  result[thin] = 1
  RETURN, result
END

;+
; :Hidden:
;  Dynamic significance filter
;-
FUNCTION autoPLSR_dynamicp $
  , jkn $
  , stp

  idx = sort( jkn )
  vec = jkn lt jkn[idx[floor(stp)]]
  
  RETURN, vec
END

;+
; :Hidden:
;  Validation
;-
FUNCTION autoPLSR_checkout $
    , xdat $
    , selcode $
    , y $
    , maxnlv $
    , SCALE=scale $
    , PREP=prep
    
  ON_ERROR,2
  
  index = WHERE(selcode eq 1,/NULL)
  chkX = xdat[*,index]

  IF prep THEN chkX = autoPLSR_preprocessing(chkX)
  
  chk = autoPLSR_crossValidation(chkX,y,/LOO,SCALE=scale,RANDOM=random)
  reducedNumberOfComponents = chk.numberOfComponents
  
  ; scaling
  IF scale THEN BEGIN
    xScaled = autoPLSR_scaling(chkX)
  ENDIF ELSE xScaled = chkX
  model = autoPLSR_kernelPls(xScaled,y,reducedNumberOfComponents)
  chkRMSE = (autoPLSR_cvStats(model, chk, xScaled, y)).rmsep[1:*,1]
  chkBest = autoPLSR_getBest(chkRMSE)
  ; returns (1.) error at maxnlv, (2.) nlv where error minimizes and
  ; (3.) error at that point
  IF N_ELEMENTS(chkRMSE) ge maxnlv THEN BEGIN
    chkVal = [chkRMSE[maxnlv-1], chkBest, chkRMSE[chkBest]]
  ENDIF ELSE chkVal = [!VALUES.F_NAN, chkBest, chkRMSE[chkBest]]
  ; cleanup
  PTR_FREE, chk.model
  
  RETURN, chkVal
END

;+
; :Hidden:
;-
FUNCTION autoPLSR_predictorImportance $
  , subX $
  , y $
  , maxnlv $
  , regCoef $
  , vip $
  , jt $
  , NUMBEROFMODEL=minimum $
  , SCALE=scale $
  , ISSPECTRAL=isSpectral $
  , STINGY=stingy $
  , THOROUGH=thorough $
  , PREP=prep $
  , JUMP=jump $
  , COUNTER=counter

  ON_ERROR,2
  
  xDims = SIZE(subX,/DIMENSIONS)
  jtThresh = 0.1D
  vipThresh = 0.2D
  combinedThresh = -0.1D
  
  ; In shrinkmode, try to reduce drastically in the first iteration
  shrinkmode = 0
  IF ( (jump gt 0) and (counter eq 1) and (xDims[1] gt jump) ) THEN shrinkmode = 1
  
  IF ( shrinkmode ) THEN BEGIN
  
;    selcode = MAKE_ARRAY(xDims[1],TYPE=1,VALUE=1B)
    minimum = 0
    return, autoPLSR_dynamicp( jt, jump ) ; selcodeA0
    
  ENDIF ELSE BEGIN
  
    IF ( ~ thorough ) THEN BEGIN
      IF ( isSpectral ) THEN BEGIN
      
        ntests = 5
        selcode = MAKE_ARRAY(xDims[1],ntests,TYPE=1,VALUE=1B)
        comp = DBLARR(3,ntests)
        
        selcode[*,0] = jt le jtThresh ; selcodeA1
        selcode[*,1] = vip gt vipThresh ; selcodeA2
        selcode[*,2] = (vip - jt) gt combinedThresh ; selcodeA3
        selcode[*,3] = autoPLSR_dynamicp(jt, xDims[1] * 0.9) ; selcodeA4
        selcode[*,4] = autoPLSR_ac(jt, selcode[*,0], subX) ; selcodeB2
        
      ENDIF ELSE BEGIN ; not thorough and not spectral
      
        ntests = 4
        selcode = MAKE_ARRAY(xDims[1],ntests,TYPE=1,VALUE=1B)
        comp = DBLARR(3,ntests)
        
        selcode[*,0] = jt le jtThresh ; selcodeA1
        selcode[*,1] = vip gt vipThresh ; selcodeA2
        selcode[*,2] = (vip - jt) gt combinedThresh ; selcodeA3
        selcode[*,3] = autoPLSR_dynamicp( jt, xDims[1] * 0.9 ) ; selcodeA4
        
      ENDELSE
    ENDIF ELSE BEGIN
      IF isSpectral THEN ntests = 12 ELSE ntests = 5
      
      selcode = MAKE_ARRAY(xDims[1],ntests,TYPE=1,VALUE=1B)
      comp = DBLARR(3,ntests)
      
      selcode[*,0] = jt le jtThresh ; selcodeA1
      selcode[*,1] = vip gt vipThresh ; selcodeA2
      selcode[*,2] = (vip - jt) gt combinedThresh ; selcodeA3
      selcode[*,3] = autoPLSR_dynamicp( jt, xDims[1] * 0.9 ) ; selcodeA4            
      selcode[*,4] = autoPLSR_dynamicp( jt, xDims[1] * 0.75 ) ; selcodeA5
      
      IF isSpectral THEN BEGIN
        selcode[*,5] = autoPLSR_ac(regCoef, selcode[*,0], subX) ; selcodeB1
        selcode[*,6] = autoPLSR_ac(jt, selcode[*,0], subX) ; selcodeB2
        selcode[*,7] = autoPLSR_ac(vip, selcode[*,0], subX) ; selcodeB3
        selcode[*,8] = autoPLSR_ac(regCoef, selcode[*,1], subX) ; selcodeB4
        selcode[*,9] = autoPLSR_ac(jt, selcode[*,1], subX) ; selcodeB5
        selcode[*,10] = autoPLSR_ac(vip, selcode[*,1], subX) ; selcodeB6
        selcode[*,11] = autoPLSR_ac(regCoef, REPLICATE(1,xDims[1]), subX, POW=2) ; selcodeC1
      ENDIF
      
    ENDELSE
  ENDELSE
  
  ; Unique solutions
  corr = CORRELATE(TRANSPOSE(selcode))
  IF ( TOTAL( FINITE( corr ), /INTEGER ) gt 0 ) THEN BEGIN
    ind = TRANSPOSE(ARRAY_INDICES(corr,WHERE(corr eq 1.)))
    equalArr = ind[WHERE(ind[*,1] gt ind[*,0], /NULL),1]
  ENDIF ELSE equalArr = !NULL
  comp[*,equalArr] = 9999

  ; Avoid that all predictors are selected
  sum = TOTAL(selcode, 1, /INTEGER)
  sum[equalArr] = 0
  use = WHERE( sum lt xDims[1] and sum gt 1, /NULL $
      , COMPLEMENT=notUse, NCOMPLEMENT=nNotUse )
  comp[*,notUse] = 9999
  
  FOREACH element, use DO BEGIN
    comp[*,element] = autoPLSR_checkout(subX,selcode[*,element], y, maxnlv, SCALE=scale, PREP=prep)
  ENDFOREACH

  errors = comp[0,*] ; errors at maxnlv
  comps = FIX(comp[1,*]) ; nlv where errors in validation minimize
  errorsMin = comp[2,*] ; errors at minimum

  IF KEYWORD_SET(stingy) THEN BEGIN
    ; Scale errors
    scalederrors = errors / STDDEV(y)      
    ; Select predictor sets leading to a scaled error that does not exceed
    ; the minimum scaled error + x and remove models with higher error
    finite = WHERE(FINITE(scalederrors))
    lowerrors = finite[WHERE(scalederrors[finite] le (MIN(scalederrors, /NAN) + 0.025),count)]
    ; Get the models with the lowest nlv where error minimizes
    minima = WHERE(comps[lowerrors] eq MIN(comps[lowerrors]))
    !NULL = MIN(scalederrors[lowerrors[minima]], index, /NAN)
    ; Among these models, get the one with the lowest error
    minimum = lowerrors[minima[index]]
  ENDIF ELSE BEGIN
    ; Get the models with the lowest error
    minima = WHERE(errorsMin eq MIN(errorsMin, /NAN))
    ; Among these models, get the one with the lowest nlv
    !NULL = MIN(comps[minima], index, /NAN)
    minimum = minima[index]
  ENDELSE

  RETURN, selcode[*,minimum]
END

;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    Use this function to run a complete autoPLSR.
;
; :Params:
;    x : in, required, type=float
;      The predictor variables. 
;
;    y : in, required, type=float
;      The target variable.
;
;    parameters : in, required, type=hash
;      The parameters for the autoPLSR.
;
;    filenamepredictors : in, required, type=string
;      The filename of the predictor variables.
;
;    filenameresponse : in, required, type=string
;      The filename of the target variable.
;
;    settings : in, type=hash
;    
; :Returns:
;    Returns a hash with a short report about the regression process. 
;
;-
FUNCTION autoPLSR_calibrate $
  , x $
  , y $
  , parameters $
  , filenamepredictors $
  , filenameresponse $
  , settings

  ON_ERROR,2

  time=SYSTIME(1)

  IF parameters.HasKey('prep') THEN prep = parameters['prep']
  IF parameters.HasKey('scale') THEN scale = parameters['scale']
  IF parameters.HasKey('random') THEN random = parameters['random']
  IF parameters.HasKey('stingy') THEN stingy = parameters['stingy']
  IF parameters.HasKey('jump') THEN jump = parameters['jump']
  IF parameters.HasKey('thorough') THEN thorough = parameters['thorough']
  IF parameters.HasKey('no_backselect') THEN no_backselect = parameters['no_backselect']
  IF parameters.HasKey('loo') THEN loo = parameters['loo']

  title = settings.hubGetValue('title')+' - Calibrating autoPLSR model'
  info = 'Wait...'
;  progressBar = hubProgressBar(Title=title, Info=info, GroupLeader=groupLeader)

  text = [[''],[''],[info]]
  base = WIDGET_BASE(TITLE=title, /ALIGN_CENTER)
  textWid = WIDGET_TEXT(base,VALUE=text, /WRAP, XSIZE=80, YSIZE=4)
  geom = WIDGET_INFO(base, /GEOMETRY)
  xHalfSize = geom.Scr_XSize / 2
  yHalfSize = geom.Scr_YSize / 2
  WIDGET_CONTROL, base, /REALIZE, /HOURGLASS $
    , TLB_SET_XOFFSET=(Get_Screen_Size())[0]/2-xHalfSize $
    , TLB_SET_YOFFSET=(Get_Screen_Size())[1]/2-yHalfSize


  isSpectral = 1
  xDim = SIZE(x,/DIMENSIONS)
  maxnlv = CEIL(FLOAT(xDim[0]) * .1) ; 10 % of number of samples
  if ( maxnlv gt 5 ) THEN maxnlv = FLOOR( ALOG10( xDim[0] ) / ALOG10( 2 ) )
  maxnlv = maxnlv < (xDim[1] - 1) 
  
  iterStruct = { $
      model : PTR_NEW() $
    , numberOfLatentVariable : 0 $
    , selectedPredictors : BYTARR(xDim[1]) $
    , R2 : FLTARR(3) $
    , RMSE : FLTARR(3) $
    , cvStats : PTR_NEW() $
    , cvModel : PTR_NEW() $
    , xStdDev : PTR_NEW() $
    , jacktest : PTR_NEW() $
  }

  numberOfComponents = MIN([xDim[0]-1,xDim[1]])
  IF ( thorough ) THEN BEGIN
    modelName = ['A1','A2','A3','A4','A5','B1','B2','B3','B4','B5','B6','C1']
  ENDIF ELSE modelName = ['A1','A2','A3','A4','B2']
  
  metarmse = !NULL
  metaR2 = !NULL
  metanlv = !NULL
  report = !NULL

  numberOfModel=0
  counter = 1
  stopIteration = 0
  prevSelection = REPLICATE(1B,xDim[1])

  structArray = REPLICATE(iterStruct, 1)
  REPEAT BEGIN
  
    IF counter gt 1 THEN BEGIN
      prevSelection = selcode
      ; Construct new X
      indexPrevSelection = WHERE(prevSelection,count,/NULL)
      IF count lt 1 THEN BREAK
      subX = x[*,indexPrevSelection] ; selection

      newStructArray = REPLICATE(iterStruct, counter)
      STRUCT_ASSIGN, structArray, newStructArray
      structArray = newStructArray
    ENDIF ELSE subX = x
    
    ; --- Data preparation ----
    ; Preprocessing
;    IF prep THEN subX = autoPLSR_preprocessing(subX)
;
;    ; --- Run PLSR ----
;    ; cross-validation
;    cv = autoPLSR_crossValidation(subX,y,NCOMP=numberOfComponents,SCALE=scale,RANDOM=random,LOO=loo)
;    reducedNumberOfComponents = cv.numberOfComponents
;
;    ; scaling
;    IF scale THEN BEGIN
;      xScaled = autoPLSR_scaling(subX, STDDEV=xStdDev)
;    ENDIF ELSE xScaled = subX

    IF prep THEN cvsubX = autoPLSR_preprocessing(subX) $
      ELSE cvsubX = subX

    ; --- Run PLSR ----
    ; cross-validation
    cv = autoPLSR_crossValidation(cvsubX,y,NCOMP=numberOfComponents,SCALE=scale,RANDOM=random,LOO=loo)
    reducedNumberOfComponents = cv.numberOfComponents

    ; scaling
    IF scale THEN BEGIN
      xScaled = autoPLSR_scaling(cvsubX, STDDEV=xStdDev)
      subX = autoPLSR_scaling(subX)
    ENDIF ELSE xScaled = cvsubX

    ; calibrating
    model = autoPLSR_kernelPls(xScaled,y,reducedNumberOfComponents)

    ; Model parameters
    stats = autoPLSR_cvStats(model, cv, xScaled, y)
    rmsep = stats.rmsep[1:*,0:1]

    latentVariableIndex = autoPLSR_getBest(rmsep[*,1])

    regCoef = model.coefficients[*,latentVariableIndex]
    r2cal = stats.R2cal[latentVariableIndex+1] ; +1 -> index 0 is intercept
    r2val = stats.R2val[latentVariableIndex+1] ; +1 -> index 0 is intercept
    r2crop = stats.R2val[maxnlv] ; +1 -> index 0 is intercept
    jt = (autoPLSR_jackknife(cv,regCoef,latentVariableIndex)).pvalues
    vip = (autoPLSR_vip(model))[*,latentVariableIndex]
    vip = (vip - MIN(vip)) / MAX(vip - MIN(vip))
    

    ; ---- Reporting ------ 
    report = [[report],[ $
        STRING(counter, FORMAT='(I3)') + '  nX: ' + STRING(N_ELEMENTS(subX[0,*]), FORMAT='(I5)') $
      + '  LV: ' + STRING(latentVariableIndex+1, FORMAT='(I3)') $
      + '  R2val: ' + STRING(r2val, FORMAT='(1F0)') $
      + '  RMSEval: ' + STRING(rmsep[latentVariableIndex,1], FORMAT='(1F0)') $
    ]]
    IF (counter gt 1) THEN BEGIN
      IF ( jump gt 0 and counter eq 2 ) THEN BEGIN
        report[counter-1] = report[counter-1] $
          + '  Criterion: A0'
      ENDIF ELSE BEGIN
        report[counter-1] = report[counter-1] $
          + '  Criterion: ' + modelName[compIndex]
      ENDELSE
    ENDIF
    PRINT, report[counter-1]
    text = [[[text[*,-3]],[text[*,-2]],[text[*,-1]]],[report[counter-1]]]
;    text = [[[text[*,-2]],[text[*,-1]]],[report[counter-1]]]
    WIDGET_CONTROL, textWid, SET_VALUE=text
    
    IF ~ KEYWORD_SET(no_backselect) THEN BEGIN
      ; ---- Selection of predictors for subsequent runs ----
      selcode = autoPLSR_predictorImportance(subX, y, maxnlv, regCoef, vip, jt $
        , NUMBEROFMODEL=compIndex, SCALE=scale, ISSPECTRAL=isSpectral $
        , STINGY=stingy, THOROUGH=thorough, PREP=prep, JUMP=jump, COUNTER=counter)
    ENDIF ELSE selcode = 1
  
    ; Compute RMSE and R2  
    metarmse = [metarmse,[[rmsep[latentVariableIndex,*]],[rmsep[maxnlv-1,1]]]]
    metaR2 = [metaR2,[[r2cal],[r2val],[r2crop]]]
    metanlv = [metanlv,latentVariableIndex]
    
    ; cleanup
    PTR_FREE, cv.model

    IF counter gt 1 THEN BEGIN
      IF ( TOTAL(selcode, /INTEGER) eq SIZE(selcode, /DIMENSIONS) ) $
          THEN stopIteration = 1
    
      newSelection = prevSelection
      index = WHERE(prevSelection)
      newSelection[index] = selcode
      selcode = newSelection
      ; continue if the last model is not worse than "limit"
      limit = max(metaR2[*,1]) - 0.05D
      If ( (limit le 0 or metaR2[-1,1] le limit) $
        and (metanlv[-1] ge metanlv[-2]) ) THEN stopIteration = 1
    ENDIF

    structArray[counter-1].model = PTR_NEW(model)
    structArray[counter-1].numberOfLatentVariable = latentVariableIndex
    structArray[counter-1].selectedPredictors = prevSelection
    structArray[counter-1].R2 = metaR2[counter-1]
    structArray[counter-1].RMSE = metarmse[counter-1]
    structArray[counter-1].cvStats = PTR_NEW(stats)
    structArray[counter-1].xStdDev = PTR_NEW(xStdDev)
;    structArray[counter-1].prepX = PTR_NEW(prepX)
    structArray[counter-1].cvModel = PTR_NEW(cv)
    structArray[counter-1].jacktest = PTR_NEW(jt)
    
    numberOfModel++
    counter++
    info = 'Model: ' + STRING(counter)
;    progressBar.update, counter, 100 , Info=info

  ENDREP UNTIL TOTAL(selcode,/INTEGER) lt 10 OR stopIteration OR counter gt 100

  ; ---- Final model selection out of all iterations:
  metaError = metarmse[*,1]
  metaCrop = metarmse[*,2] 
               
  ; this is the first model with the global, minimum RMSEval:
  !NULL = MIN(metaError, errorsel)
        
  IF KEYWORD_SET(stingy) THEN BEGIN
    ; nlv in this model:
    bestNLV = structArray[errorsel].numberOfLatentVariable
    ; If the model with the lowest RMSEval has a nlv <= maxnlv take that:        
    IF (bestNLV le maxnlv-1) THEN BEGIN
      bestIndex = errorsel
      resRMSE = metaError[bestIndex]
      resR2 = metaR2[bestIndex,1]
    ENDIF ELSE BEGIN
      ; otherwise take the model with the lowest RMSE at maxnlv:        
      !NULL = MIN(metaCrop, bestIndex)
      resRMSE = metaCrop[bestIndex]
      resR2 = metaR2[bestIndex,2]
      bestNLV = maxnlv - 1
    ENDELSE
  ENDIF ELSE BEGIN
    ; nlv in this model:
    bestNLV = structArray[errorsel].numberOfLatentVariable
    bestIndex = errorsel
    resRMSE = metaError[bestIndex]
    resR2 = metaR2[bestIndex,1]
  ENDELSE

  ; ---- Reporting ----
  report = [[report],['---'],[ $
;      'Run ' + STRING(bestIndex+1, FORMAT='(I3)') $
;    + '  nX: ' + STRING(TOTAL(structArray[bestIndex].selectedPredictors, /INTEGER), FORMAT='(I5)') $
;    + '  LV: ' + STRING(structArray[bestIndex].numberOfLatentVariable+1, FORMAT='(I3)') $
;    + '  RMSEval: ' + STRING(metaError[bestIndex], FORMAT='(1F0)') $
;    + '  R2val: ' + STRING(metaR2[bestIndex,1], FORMAT='(1F0)') $
      'Predictors: ' + STRING(TOTAL(structArray[bestIndex].selectedPredictors, /INTEGER), FORMAT='(I5)') $
    + '  Observations: ' + STRING(xDim[0], FORMAT='(I5)') $
    + '  Latent Vectors: ' + STRING(structArray[bestIndex].numberOfLatentVariable+1, FORMAT='(I3)') $
    + '  Run: ' + STRING(bestIndex+1, FORMAT='(I3)') $
    ],[ $
      'RMSE in calibration: ' + STRING(metarmse[bestIndex,0], FORMAT='(1F0)') $
    + '  RMSE in validation: ' + STRING(metaError[bestIndex], FORMAT='(1F0)') $
    ],[ $
      'R2 in calibration: ' + STRING(metaR2[bestIndex,0], FORMAT='(1F0)') $
    + '  R2 in validation: ' + STRING(metaR2[bestIndex,1], FORMAT='(1F0)') $
  ]]
  PRINT, report[counter:*]
  
  IF structArray[bestIndex].numberOfLatentVariable+1 gt maxnlv AND KEYWORD_SET(stingy) THEN BEGIN
    printRMSE = (*structArray[bestIndex].cvStats).rmsep[bestNLV+1,1]
    printR2 = (*structArray[bestIndex].cvStats).r2val[bestNLV+1]

    report = [[report],[ $
        'This model with LV: ' + STRING(bestNLV+1, FORMAT='(I3)') $
      + '  RMSEval: ' + STRING(metaCrop[bestIndex], FORMAT='(1F0)') $
      + '  R2val: ' + STRING(metaR2[bestIndex,2], FORMAT='(1F0)') $
    ]]
    PRINT, report[counter+1]
  ENDIF

  time = SYSTIME(1)-time
  PRINT, time
  
  report = [[report],['---'],[ $
    '(processing time: ' + STRING(time, FORMAT='(1F0)') + ' sec)' $
  ]]
  ; ----
  ; creating structure for best model
  if ( loo ) then cvMethod = 'leave-one-out' else cvMethod = 'tenfold'
  
  modelStruct = { $
;      autoPLSR_Model, $
      model : structArray[bestIndex].model $
    , x : x $
    , y : y $
    , numberOfLatentVariable : bestNLV $
    , selectedPredictors : structArray[bestIndex].selectedPredictors $
    , cvStats : structArray[bestIndex].cvStats $
    , xStdDev : structArray[bestIndex].xStdDev $
;    , prepX : structArray[bestIndex].prepX $
    , cvModel : structArray[bestIndex].cvModel $
    , jacktest : structArray[bestIndex].jacktest $
    , scale : scale $
    , prep : prep $
    , random : random $
    , stingy : stingy $
    , no_backselect : no_backselect $
    , thorough : thorough $
    , cvMethod : cvMethod $
    , xdim : xdim $
    , filenamepredictors : filenamepredictors $
    , filenameresponse : filenameresponse $
  }
  
  ; reporting
  !NULL = DIALOG_MESSAGE(report,/INFORMATION,TITLE='autoPLSR')

  reportHash = autoPLSR_convertModel(modelStruct)

  reportHash['report'] = report

  ; saving best model
  SAVE, modelStruct, reportHash, DESCRIPTION='autoPLSR-Model', FILENAME=parameters['outputFilename']

  ; cleanup
  autoPLSR_cleanup, structArray
  IF ISA(newStructArray) THEN autoPLSR_cleanup, newStructArray

  ;  progressBar.finish
  WIDGET_CONTROL, base, /DESTROY

  PRINT, CHECK_MATH(/PRINT)
  
  return, reportHash

END