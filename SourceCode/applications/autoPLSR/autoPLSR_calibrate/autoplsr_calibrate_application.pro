;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `autoPLSR_getParameters`)
;    via a widget program. The user input is passed to the application processing routine.
;    Finally, all results are presented in a HTML report (see `autoPLSR_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro autoPLSR_calibrate_application, applicationInfo

  ON_ERROR,2
  
  ; get global settings for this application
  settings = autoPLSR_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = autoPLSR_calibrate_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = autoPLSR_calibrate_processing(parameters, settings)
    
    if parameters.HasKey('reportFilename') then filename = parameters['reportFilename']
    
    if parameters['reportShow'] or parameters.HasKey('reportFilename') then begin
      autoPLSR_showReport, reportInfo, settings, FILENAME=filename, SHOW=parameters['reportShow']
    endif
  
  endif
  
end