;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    Check if coordinates are present and valid if necessary.
;
; :Params:
;    resultHash : in, required, type=hash
;
;-
FUNCTION autoPLSR_calibrate_getParameters_consistencyCheck, resultHash, Message=message

  ON_ERROR,2

  CASE resultHash['InputSelect'] OF
    0 : BEGIN
      inputFilename = (resultHash['imageData'])['featureFilename']
      inputImage = hubIOImgInputImage(inputFilename)
      nBands = inputImage.getMeta('bands')

      IF resultHash['jump'] gt nBands THEN BEGIN
        message = 'Maximum number of predictors exceeds number of predictors!!'
      ENDIF ELSE RETURN, 1
    END
    1 : BEGIN
      inputFilename = resultHash['inputImage']
      target = resultHash['target']
      
      coordinates = autoPLSR_readASCII(target, /WITH_COORDINATES, MESSAGE=message)
      
      IF ISA(coordinates) THEN BEGIN
      
        inputImage = hubIOImgInputImage(inputFilename)
        map_info = inputImage.getMeta('map info')
        lines = inputImage.getMeta('lines')
        samples = inputImage.getMeta('samples')
        nBands = inputImage.getMeta('bands')
        inputImage.cleanup
        xmin = map_info.easting
        xmax = map_info.easting + samples * map_info.sizex
        ymin = map_info.northing - lines * map_info.sizey
        ymax = map_info.northing
        index = WHERE(coordinates[0,*] gt xmax OR coordinates[0,*] lt xmin $
          OR coordinates[1,*] gt ymax OR coordinates[1,*] lt ymin, /NULL)
        IF ISA(index) THEN BEGIN
          message = 'Some samples are located outside the boundaries of the reference image!!!'
        ENDIF ELSE BEGIN 
          IF resultHash['jump'] gt nBands THEN BEGIN
            message = 'Maximum number of predictors exceeds number of predictors!!'
          ENDIF ELSE RETURN, 1
        ENDELSE

;        IF ~ ISA(index) THEN RETURN, 1 $
;          ELSE message = 'Some samples are located outside the boundaries of the reference image!!!'
      ENDIF
    END
    2 : BEGIN
      target = resultHash['target']
      npredictors = autoPLSR_readASCII(target, MESSAGE=message, /NPREDICTORS)
      IF ISA(npredictors) THEN BEGIN
        IF resultHash['jump'] gt npredictors THEN BEGIN
          message = 'Maximum number of predictors exceeds number of predictors!!'
        ENDIF ELSE RETURN, 1
      ENDIF
    END
    ELSE :
  ENDCASE
  
END

;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION autoPLSR_calibrate_getParameters $
  , settings

  ON_ERROR,2

  groupLeader = settings.hubGetValue('groupLeader')
  
;  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - Calibrate Model'

  HUBAMW_FRAME, Title='Input'
  
  HUBAMW_SUBFRAME, 'InputSelect', Title='Predictor image data / Target image data', /SetButton, /COLUMN
  HUBAMW_INPUTSAMPLESET, 'imageData', /REGRESSION, ReferenceTitle='Target Image'

  HUBAMW_SUBFRAME, 'InputSelect', Title='Predictor image data / ASCII with target variable and coordinates', /COLUMN
  HUBAMW_INPUTIMAGEFILENAME, 'inputImage', Title='Input Image '
  HUBAMW_INPUTFILENAME, 'target', Title='Target      ', Extension='txt'

  HUBAMW_SUBFRAME, 'InputSelect', Title='ASCII with target variable and reflectance', /COLUMN
  HUBAMW_INPUTFILENAME, 'target', Title='Target + Reflectance', Extension='txt'
    
  HUBAMW_FRAME, Title='Settings'
  HUBAMW_CHECKBOX, 'prep', Title='Brightness normalization', Value=0
  HUBAMW_CHECKBOX, 'scale', Title='Data scaling', Value=1
  HUBAMW_CHECKBOX, 'loo', Title="Use leave-one-out instead of tenfold cross-validation", Value=0
  HUBAMW_CHECKBOX, 'no_backselect', Title="Compute PLS without backward selection of predictors", Value=0
  
  hubAMW_frame, Title='Advanced settings', /Advanced
  HUBAMW_CHECKBOX, 'thorough', Title="Use full set of selection filters (slower)", Value=0
  HUBAMW_CHECKBOX, 'random', Title='Random stratification for cross-validation', Value=1
  HUBAMW_CHECKBOX, 'stingy', Title="Prefer solutions with few latent vectors", Value=1
  hubAMW_parameter, 'jump', Title='Use ', Unit=' predictors at maximum' $
      , Value=-1, IsGE=-1, /Integer
  
  HUBAMW_FRAME, Title='Output'
  HUBAMW_OUTPUTFILENAME, 'outputFilename', Title='autoPLSR Model', Extension='plsr'

  HUBAMW_FRAME, Title='Report'
  HUBAMWGROUP_REPORT, 'report', SaveFileStyle=1
;  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b
; ReportShow, ReportFilename

  result = HUBAMW_MANAGE(ConsistencyCheckFunction='autoPLSR_calibrate_getParameters_consistencyCheck')

  RETURN, result

END

PRO testautoPLSR_calibrate_getParameters

  settings = autoPLSR_getSettings()
  parameters = autoPLSR_calibrate_getParameters(settings)
;  IF parameters.HasKey('imageData') THEN BEGIN
;    print, (parameters['imageData'])
;    print, (parameters['imageData'])['labelFilename']
;    print, (parameters['imageData'])['featureFilename']
;  ENDIF ELSE print, parameters
;  print, parameters

  if parameters['accept'] then begin
    reportInfo = autoPLSR_calibrate_processing(parameters, settings)  
  endif

END