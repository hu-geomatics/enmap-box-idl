;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application processing function. All parameters are
;    checked for validity. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`autoPLSR_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, required, type=hash
;    
;-
FUNCTION autoPLSR_calibrate_processing, parameters, settings

  IF ~ parameters.HasKey('outputFilename') THEN MESSAGE, "Parameters didn't contain filename for output image!!
    
  IF ~ parameters.HasKey('InputSelect') THEN MESSAGE, 'Parameters are not valid!!'
    
  CASE parameters['InputSelect'] OF
    0 : BEGIN ; predictors and target are image data
      IF parameters.HasKey('imageData') THEN BEGIN
        filenamepredictors = (parameters['imageData'])['featureFilename']
        filenameresponse = (parameters['imageData'])['labelFilename']
      ENDIF ELSE MESSAGE, "Parameters didn't contain filename for input!!!
      
      IF ~ FILE_TEST(filenamepredictors, /REGULAR) THEN MESSAGE, "Filename for features not valid!!"
      IF ~ FILE_TEST(filenameresponse, /REGULAR) THEN MESSAGE, "Filename for labels not valid!!"

      sampleSet = hubioimgsamplesetforregression(filenamepredictors, filenameresponse)
      indices = sampleSet.getIndices()
      x = TRANSPOSE(sampleSet.getFeatures(indices))
      y = sampleSet.getLabels(indices)
      sampleSet.cleanup
    END
    1 : BEGIN
      IF parameters.HasKey('inputImage') THEN BEGIN
        filenamepredictors = parameters['inputImage']
      ENDIF ELSE MESSAGE, "Parameters didn't contain filename for input image!!
      
      IF parameters.HasKey('target') THEN BEGIN
        filenameresponse = parameters['target']
        coordinates = autoPLSR_readASCII(filenameresponse, MESSAGE=message $
          , /WITH_COORDINATES, TARGET_VALUES=y)
        y = REFORM(y)
        IF ~ ISA(coordinates) THEN MESSAGE, message
      ENDIF ELSE MESSAGE, "Parameters didn't contain filename for target variable!!
      
      inputImage = hubIOImgInputImage(filenamepredictors)
      map_info = inputImage.getMeta('map info')
      nb = inputImage.getMeta('bands')
      datatype = inputImage.getMeta('data type')
      
      pixelCoords = [ULONG((coordinates[0,*] - map_info.easting) / map_info.sizex) $
        , ULONG((map_info.northing - coordinates[1,*]) / map_info.sizey)]

      ySortedIndex = SORT(pixelCoords[1,*])
      yUniqEnd = UNIQ(pixelCoords[1,ySortedIndex])
      yUniqStart = SHIFT(yUniqEnd,1) + 1
      yUniqStart[0] = 0
    
      x = MAKE_ARRAY(nb,N_ELEMENTS(ySortedIndex),TYPE=datatype)
      inputImage.initReader, /SLICE
      FOREACH element,yUniqStart,i DO BEGIN
        slice = inputImage.getData(pixelCoords[1,ySortedIndex[element]])
        FOR n=element,yUniqEnd[i] DO BEGIN
          x[*,ySortedIndex[n]] = slice[*,pixelCoords[0,ySortedIndex[n]]]
        ENDFOR
      ENDFOREACH
      inputImage.finishReader  
      inputImage.cleanup
      
      x = TRANSPOSE(x)
      
    END  
    2 : BEGIN
      IF parameters.HasKey('target') THEN BEGIN
        filenameresponse = parameters['target']
        filenamepredictors = filenameresponse
        
        x = TRANSPOSE(autoPLSR_readASCII(filenameresponse, MESSAGE=message $
          , /WITH_REFLECTANCE, TARGET_VALUES=y))
        IF ~ ISA(x) THEN MESSAGE, message
        y = REFORM(y)
        
      ENDIF ELSE MESSAGE, "Parameters didn't contain filename for target variable!!

    END
  ENDCASE
  
  IF SIZE(x, /N_DIMENSIONS) le 1 THEN MESSAGE, "Only one predictor variable found!!
  
  report = autoPLSR_calibrate( $
      x $
    , y $
    , parameters $
    , filenamepredictors $
    , filenameresponse $
    , settings $
  )

  return, report

END