;+
; :Hidden:
;-

FUNCTION autoPLSR_getBest, rmseVec

  ON_ERROR,2

  slope = - TS_DIFF(rmseVec,1)
  IF MAX(slope) gt 0 OR N_ELEMENTS(rmseVec) lt 4 THEN BEGIN
    ; Factor determining 'conservativism' of proposed nlv
    cnsv = 0.005
    criterion = cnsv * (MAX(rmseVec) - MIN(rmseVec))
    dif = slope + criterion
    !NULL = MAX(dif gt 0,latentVariableIndex)
  ENDIF ELSE BEGIN
    filter = [1., 2., 1.] ; [1., 1.5, 1.]
    ; Low-pass filter
    lp = CONVOL(rmseVec, filter)
    ; difference of the filtered data
    diff1 = TS_DIFF(lp,1) ; * (-1)
    ; Identify point were filter reaches minimum
    minfilter = (WHERE(diff1[1:-3] le 0., minfiltercount))[0] + 1
    IF minfiltercount eq 0 THEN latentVariableIndex = 0 ELSE BEGIN
      dwdw = TS_DIFF(rmseVec[minfilter-1:minfilter+1],1)
      latentVariableIndex = ([minfilter-1,minfilter,minfilter+1])[WHERE(dwdw lt 0., count)]
      IF count eq 0 THEN latentVariableIndex = 0
    ENDELSE
  ENDELSE
  
  RETURN, latentVariableIndex
END