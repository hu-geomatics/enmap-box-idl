; Author: Carsten Oldenburg
;
;+
; :Hidden:
;-

FUNCTION autoPLSR_setSegments, data, nSegments

  dims = SIZE(data,/DIMENSIONS)
  IF nSegments lt 1 THEN MESSAGE, "Number of segments not valid!!"
  IF dims[0] lt nSegments THEN MESSAGE, "Less observations than number of segments!!"

  binSize = dims[0] / nSegments
  modBins = dims[0] MOD nSegments
  segIndices = INDGEN(nSegments)

  segLenghtsStart = segIndices * binSize
  offset = segIndices < modBins
  segLenghtsStart = segLenghtsStart + offset
  segLenghtsEND = SHIFT(segLenghtsStart,-1) - 1
  segLenghtsEND[-1] = dims[0] - 1
  
  RETURN, [[segLenghtsStart],[segLenghtsEND]]
END