; Brightness normalization according to
;
; H. Feilhauer et al. (2010): Brightness-normalized Partial Least Squares Regression for
; hyperspectral data. In: Journal of Quantitative Spectroscopy & Radiative Transfer 111,
; 1947-1957.
; 
; Author: Carsten Oldenburg
;
;+
; :Hidden:
;-

FUNCTION autoPLSR_preprocessing, x, prepX=prepX

  ON_ERROR,2
  
  dim = SIZE(x,/DIMENSIONS)

;  prepX = SQRT(TOTAL(DOUBLE(x)^2,1))
;  result = x / REBIN(TRANSPOSE(prepX),dim[0],dim[1])

  prepX = SQRT(TOTAL(DOUBLE(x)^2,2))
  !null = where( prepX eq 0.D, count )
  if ( count gt 0 ) then begin
    text = [ "Problem with brightness normalization!! Division by zero." ]
    message, text
  endif
  result = x / REBIN(prepX,dim[0],dim[1])

  RETURN, result
END