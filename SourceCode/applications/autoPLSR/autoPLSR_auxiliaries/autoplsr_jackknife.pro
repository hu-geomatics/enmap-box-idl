; original R source code by Ron Wehrens and Bjørn-Helge Mevik
; released under the GPL-2
; 
; more information about the 'pls'-package
; URL: http://mevik.net/work/software/pls.html
; and
; URL: http://cran.r-project.org/web/packages/pls/
;
; adapted to IDL by Carsten Oldenburg
;

;+
; :Hidden:
;-
FUNCTION autoPLSR_jackknifeVariance, cvModel, numberOfCoef, latentVariableIndex

  ON_ERROR,2

  segDims = SIZE(cvModel.segLocations,/DIMENSIONS) 
  cvCoefficients = DBLARR(numberOfCoef,segDims[0])

  FOREACH element,cvModel.model,i DO BEGIN
    cvCoefficients[*,i] = (*element).coefficients[*,latentVariableIndex]
  ENDFOREACH
  
  cent = MEAN(cvCoefficients,DIMENSION=2)
  bDiffSq = (cvCoefficients - REBIN(cent,numberOfCoef,segDims[0]))^2
  result = (segDims[0] - 1) * MEAN(bDiffSq,DIMENSION=2)
  RETURN, result
END


;+
; :Hidden:
;-
; original R source code by Ron Wehrens and Bjørn-Helge Mevik
; released under the GPL-2
; 
; more information about the 'pls'-package
; URL: http://mevik.net/work/software/pls.html
; and
; URL: http://cran.r-project.org/web/packages/pls/
;
; adapted to IDL by Carsten Oldenburg
;
FUNCTION autoPLSR_jackknife, cvModel, modelCoefficients, latentVariableIndex

  ON_ERROR,2

  segDims = SIZE(cvModel.segLocations,/DIMENSIONS) 

  numberOfCoef = N_ELEMENTS(modelCoefficients)
  sdjack = SQRT(autoPLSR_jackknifeVariance(cvModel, numberOfCoef, latentVariableIndex))
  df = segDims[0] - 1
  tvals = modelCoefficients / sdjack
  pvals = 2 * (1D - T_PDF(ABS(tvals),df))
  
  result = { $
;      coefficients : modelCoefficients, $
      sd : sdjack $
    , tvalues : tvals $
    , pvalues : pvals $
    , df : df $
  }

  RETURN, result

END