;+
; :Hidden:
;-
 
; original R source code by Ron Wehrens and Bjørn-Helge Mevik
; released under the GPL-2
; 
; more information about the 'pls'-package
; URL: http://mevik.net/work/software/pls.html
; and
; URL: http://cran.r-project.org/web/packages/pls/
;
; adapted to IDL by Carsten Oldenburg
;
FUNCTION autoPLSR_cvStats, model, cv, x, y

  ON_ERROR,2
  
  predicted = x # model.coefficients + $
    REBIN(TRANSPOSE(model.intercept),N_ELEMENTS(y),model.numberOfComponents)
  residuals = predicted - REBIN(y,N_ELEMENTS(y),model.numberOfComponents) 

  ; Training
  train_SST = VARIANCE(y) * (N_ELEMENTS(y) - 1)
  train_SSE = TOTAL(residuals^2,1)

  ; cross-validation
  cv_SST = cv.PRESSO
  cv_SSE = cv.PRESS

  ; Mean Square Error of Prediction
  msepTrain = train_SSE / N_ELEMENTS(y)
  msepCV = cv_SSE / N_ELEMENTS(y)
  adjustedMsepCV = msepCV + msepTrain - cv.adj
  
  rmsep = SQRT([ $
      [train_SST / N_ELEMENTS(y),msepTrain] $ ; RMSEcal
    , [cv_SST / N_ELEMENTS(y),msepCV] $ ; RMSEval
    , [cv_SST / N_ELEMENTS(y),adjustedMsepCV] $
  ])

  R2train = 1 - [train_SST,train_SSE] / train_SST
  R2cv = 1 - [cv_SST,cv_SSE] / train_SST
; FIXME
;  if ( R2train lt 0. ) then R2train = 0.
;  if ( R2train gt 1. ) then R2train = 1.
;  if ( R2cv lt 0. ) then R2cv = 0.
;  if ( R2cv gt 1. ) then R2cv = 1.
    
  result = { $
      rmsep : rmsep $
    , R2cal : R2train $
    , R2val : R2cv $
    , predicted : predicted $
  }
  
  RETURN, result
END