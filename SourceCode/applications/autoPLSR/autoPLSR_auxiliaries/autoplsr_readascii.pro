; Author: Carsten Oldenburg
;
;+
; :Hidden:
;-

FUNCTION autoPLSR_readASCII $
  , filename $
  , MESSAGE=message $
  , TARGET_VALUES=target_values $
  , WITH_COORDINATES=with_coordinates $
  , WITH_REFLECTANCE=with_Reflectance $
  , NPREDICTORS=npredictors

  ON_ERROR,2

  IF ~ FILE_TEST(filename, /REGULAR) THEN MESSAGE, 'File not found!!'
  
  IF ~ KEYWORD_SET(with_coordinates) AND ~ KEYWORD_SET(with_Reflectance) THEN num_records = 1

  asciiStruct = READ_ASCII( $
      filename $
    , DELIMITER=' ' $
    , HEADER=header $
    , DATA_START=1 $
    , NUM_RECORDS=num_records $
    )
    
  header = STRSPLIT(header, /EXTRACT)
  
  valueIndex = WHERE(STRMATCH(header, 'values',/FOLD_CASE), /NULL)
  xIndex = WHERE(STRMATCH(header, 'x',/FOLD_CASE), /NULL)
  yIndex = WHERE(STRMATCH(header, 'y',/FOLD_CASE), /NULL)

  IF valueIndex ne 0 THEN BEGIN
    message = 'No values for target variable found in file!!'
    RETURN, !NULL ; 0
  ENDIF ELSE target_values = (asciiStruct.(0))[valueIndex,*]
  
  IF KEYWORD_SET(with_coordinates) THEN BEGIN
    IF xIndex ne 1 AND yIndex ne 2 THEN BEGIN
      message = 'No coordinates found in file!!'
      RETURN, !NULL ; 0
    ENDIF ELSE BEGIN
      coordinates = [(asciiStruct.(0))[xIndex,*],(asciiStruct.(0))[yIndex,*]]
      RETURN, coordinates
    ENDELSE
  ENDIF

  IF KEYWORD_SET(with_Reflectance) THEN BEGIN
    IF N_ELEMENTS(header) lt 3 THEN BEGIN
      message = 'No reflectance data found in file!!'
      RETURN, !NULL ; 0
    ENDIF ELSE BEGIN
      reflectance = (asciiStruct.(0))[1:*,*]
      RETURN, reflectance    
    ENDELSE
  ENDIF

  IF KEYWORD_SET(npredictors) THEN BEGIN
    IF N_ELEMENTS(header) lt 3 THEN BEGIN
      message = 'No predictors found in file!!'
      RETURN, !NULL ; 0
    ENDIF ELSE BEGIN
      RETURN, size( header, /DIMENSIONS )-1
    ENDELSE
  ENDIF
  
  RETURN, 1

END