; Author: Carsten Oldenburg
;
;+
; :Hidden:
;-

FUNCTION autoPLSR_scaling, data, STDDEV=dataStdDev

  nDims = SIZE(data,/N_DIMENSIONS)
  dataDim = SIZE(data,/DIMENSIONS)
  
  IF nDims eq 1 THEN BEGIN
    dataStdDev = STDDEV(data)
    RETURN, data / dataStdDev
  ENDIF ELSE BEGIN
    dataStdDev = STDDEV(data,DIMENSION=1)
    dataStdDevArray = REBIN(TRANSPOSE(dataStdDev),dataDim[0],dataDim[1])
    RETURN, data / dataStdDevArray
  ENDELSE
END