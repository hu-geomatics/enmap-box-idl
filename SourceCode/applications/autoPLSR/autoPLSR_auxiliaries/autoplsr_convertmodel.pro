;+
; :Hidden:
;-

FUNCTION autoplsr_convertmodel, modelStruct

  ON_ERROR,2

  coefficients = (*modelStruct.model).coefficients[*,modelStruct.numberoflatentvariable]
  intercept = (*modelStruct.model).intercept[modelStruct.numberoflatentvariable]

  ; scale data if model is scaled
  IF KEYWORD_SET(modelStruct.scale) THEN coefficients = coefficients / *modelStruct.xStdDev
  
  noMeansNo = ['No','Yes']
  
  report = HASH()
  
  report['description'] = 'Report of the autoPLSR application.'
  report['Predictors'] = modelStruct.filenamepredictors
  report['Response'] = modelStruct.filenameresponse
  report['NumberPredictors'] = STRING(modelStruct.xdim[1],FORMAT='(I5)')
  report['NumberUsedPredictors'] = STRING(TOTAL(modelStruct.selectedPredictors, /INTEGER),FORMAT='(I5)')
  report['Number of samples'] = STRING(modelStruct.xdim[0],FORMAT='(I5)')
  report['UsedPredictors'] = STRJOIN(STRING(WHERE(modelStruct.selectedPredictors)+1, FORMAT='(1I0)'),', ')
  report['NumberLV'] = STRING(modelStruct.numberOfLatentVariable+1,FORMAT='(I5)')
  report['scaling'] = noMeansNo[modelStruct.scale]
  report['dataPrep'] = noMeansNo[modelStruct.prep]
  report['random'] = noMeansNo[modelStruct.random]
  report['restrict'] = noMeansNo[modelStruct.stingy]
  report['no_backselect'] = noMeansNo[~ modelStruct.no_backselect]
  report['thorough'] = noMeansNo[modelStruct.thorough]
  report['cvMethod'] = modelStruct.cvMethod
  ; first index is SST -> numberoflatentvariable+1
  report['RMSEcal'] = STRING((*modelStruct.cvStats).rmsep[modelStruct.numberoflatentvariable+1,0], FORMAT='(1F0)') 
  report['RMSEval'] = STRING((*modelStruct.cvStats).rmsep[modelStruct.numberoflatentvariable+1,1], FORMAT='(1F0)')
  report['R2cal'] = STRING((*modelStruct.cvStats).r2cal[modelStruct.numberoflatentvariable+1,0], FORMAT='(1F0)')
  report['R2val'] = STRING((*modelStruct.cvStats).r2val[modelStruct.numberoflatentvariable+1,0], FORMAT='(1F0)')
  report['coefficients'] = [STRJOIN(STRING(coefficients, FORMAT='(1F0)'),', ')]
  report['intercept'] = [STRING(intercept, FORMAT='(1F0)')]
  report['predicted'] = [STRJOIN(STRING((*modelStruct.cvStats).predicted[*,modelStruct.numberoflatentvariable], FORMAT='(1F0)'),', ')]
  
  RETURN, report
  
END