
;
;+
; :Description:
;   original source code by Bhupinder. S. Dayal and John F. MacGregor
;   Dayal, B. S. and MacGregor, J. F. (1997), Improved PLS algorithms. Journal of Chemometrics, 11: 73–85
; 
;   adapted to IDL by Carsten Oldenburg
; 
;
; :Params:
;    x : in, required, type=float
;      The predictor variables. 
;
;    y : in, required, type=float
;      The target variable.
;
;    numberOfComponents : in, required, type=int
;      The maximum number of components to compute.
;    
; :Returns:
;    Returns a structure with the regression variables. 
;
;-
FUNCTION autoPLSR_kernelPls, x, y, numberOfComponents

  ON_ERROR,2
  
;  type = SIZE(y,/TYPE)
;  IF PRODUCT(type ne [1,2,3,4,5,12,13,14,15],/INTEGER) gt 0 THEN MESSAGE, "Datatype not valid!!"
;  IF TOTAL(type eq [1,2,3,4,12,13],/INTEGER) gt 0 THEN BEGIN
;    dataType = 4
;    isDouble = 0
;  ENDIF ELSE BEGIN
    dataType = 5; 5,14,15
    isDouble = 1
;  ENDELSE

  dims = SIZE(x,/N_DIMENSIONS)
  IF dims lt 2 THEN MESSAGE, "Dimensions of data must be greater than 1!!"

  xDim = SIZE(x,/DIMENSIONS)
  yDim = SIZE(y,/DIMENSIONS)

  w = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
  p = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
  q = MAKE_ARRAY(numberOfComponents,TYPE=dataType)
  r = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
  t = MAKE_ARRAY(xDim[0],numberOfComponents,TYPE=dataType)
  b = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
  b0 = MAKE_ARRAY(numberOfComponents,TYPE=dataType)
  
  ; center the data
  xMean = MEAN(x,DIMENSION=1,DOUBLE=isDouble)
  xMeanArray = REBIN(TRANSPOSE(xMean),xDim[0],xDim[1])
  xCentered = x - xMeanArray 
  
  yMean = MEAN(y,DOUBLE=isDouble)
  yCentered = y - yMean

  xy = MATRIX_MULTIPLY(xCentered,yCentered,/ATRANSPOSE)
  FOR a=0,numberOfComponents-1 DO BEGIN
    w_a = xy / (SQRT(MATRIX_MULTIPLY(xy,xy,/ATRANSPOSE)))[0]
    r_a = w_a
    r_a = r_a - r[*,0:a] # (MATRIX_MULTIPLY(p[*,0:a], w_a, /ATRANSPOSE))
    t_a = MATRIX_MULTIPLY(xCentered,r_a)
    tt = (MATRIX_MULTIPLY(t_a,t_a,/ATRANSPOSE))[0]
    p_a = MATRIX_MULTIPLY(xCentered,t_a,/ATRANSPOSE) / tt
    ; original code: q_a = TRANSPOSE(MATRIX_MULTIPLY(r_a,xy,/ATRANSPOSE)) / tt
    q_a = MATRIX_MULTIPLY(xy,r_a,/ATRANSPOSE) / tt
    xy = xy - MATRIX_MULTIPLY(p_a,q_a,/BTRANSPOSE) * tt
    
    w[*,a] = w_a ; weights W
    p[*,a] = p_a ; x-loadings P
    q[a] = q_a ; y-loadings Q
    r[*,a] = r_a ; weights R to compute scores T directly from original x
    t[*,a] = t_a ; scores T
    ; regression coefficients and intercept
    b[*,a] = MATRIX_MULTIPLY(r,q)
    b0[a] = yMean - MATRIX_MULTIPLY(xMean,b[*,a],/ATRANSPOSE)
  ENDFOR

  result = { $
      coefficients : b $
    , intercept : b0 $
    , xLoadings : p $
    , yLoadings : q $
    , scores : t $
    , weights : w $
    , xMean : xMean $
    , yMean : yMean $
    , numberOfComponents : numberOfComponents $
  }  
  
  RETURN, result
END

; modified
;FUNCTION autoPLSR_kernelPls, x, y, numberOfComponents
;
;  ON_ERROR,2
;  
;;  type = SIZE(y,/TYPE)
;;  IF PRODUCT(type ne [1,2,3,4,5,12,13,14,15],/INTEGER) gt 0 THEN MESSAGE, "Datatype not valid!!"
;;  IF TOTAL(type eq [1,2,3,4,12,13],/INTEGER) gt 0 THEN BEGIN
;;    dataType = 4
;;    isDouble = 0
;;  ENDIF ELSE BEGIN
;    dataType = 5; 5,14,15
;    isDouble = 1
;;  ENDELSE
;
;  dims = SIZE(x,/N_DIMENSIONS)
;  IF dims lt 2 THEN MESSAGE, "Dimensions of data must be greater than 1!!"
;
;  xDim = SIZE(x,/DIMENSIONS)
;  yDim = SIZE(y,/DIMENSIONS)
;
;  w = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
;  p = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
;  q = MAKE_ARRAY(numberOfComponents,TYPE=dataType)
;  r = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
;  t = MAKE_ARRAY(xDim[0],numberOfComponents,TYPE=dataType)
;  b = MAKE_ARRAY(xDim[1],numberOfComponents,TYPE=dataType)
;  b0 = MAKE_ARRAY(numberOfComponents,TYPE=dataType)
;  
;  ; center the data
;  xMean = MEAN(x,DIMENSION=1,DOUBLE=isDouble)
;  xMeanArray = REBIN(TRANSPOSE(xMean),xDim[0],xDim[1])
;  xCentered = x - xMeanArray 
;  
;  yMean = MEAN(y,DOUBLE=isDouble)
;  yCentered = y - yMean
;
;  xy = yCentered # xCentered
;  FOR a=0,numberOfComponents-1 DO BEGIN
;    w_a = REFORM(xy) / (SQRT(xy # REFORM(xy)))[0]
;    r_a = w_a
;    r_a = r_a - r[*,0:a] # (MATRIX_MULTIPLY(p[*,0:a], w_a, /ATRANSPOSE))
;    t_a = xCentered # r_a
;    tt = (MATRIX_MULTIPLY(t_a,t_a,/ATRANSPOSE))[0]
;    p_a = MATRIX_MULTIPLY(xCentered,t_a,/ATRANSPOSE) / tt
;    q_a = xy # r_a / tt
;    xy = xy - MATRIX_MULTIPLY(p_a,q_a,/BTRANSPOSE) * tt
;    
;    w[*,a] = w_a ; weights W
;    p[*,a] = p_a ; x-loadings P
;    q[a] = q_a ; y-loadings Q
;    r[*,a] = r_a ; weights R to compute scores T directly from original x
;    t[*,a] = t_a ; scores T
;    ; regression coefficients and intercept
;    b[*,a] = r # q
;    b0[a] = yMean - MATRIX_MULTIPLY(xMean,b[*,a],/ATRANSPOSE)
;  ENDFOR
;
;  result = { $
;      coefficients : b $
;    , intercept : b0 $
;    , xLoadings : p $
;    , yLoadings : q $
;    , scores : t $
;    , weights : w $
;    , xMean : xMean $
;    , yMean : yMean $
;    , numberOfComponents : numberOfComponents $
;  }  
;  
;  RETURN, result
;END