; Implementation of VIP (variable importance in projection)(*) for IDL
; Copyright © of original source code in R 2006,2007 by Bjørn-Helge Mevik
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License version 2 as
; published by the Free Software Foundation.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; A copy of the GPL text is available here:
; http://www.gnu.org/licenses/gpl-2.0.txt
;
; Contact info:
; Bjørn-Helge Mevik
; bhx6@mevik.net
; Rødtvetvien 20
; N-0955 Oslo
; Norway
;
; (*) As described in Chong, Il-Gyo & Jun, Chi-Hyuck, 2005, Performance of
; some variable selection methods when multicollinearity is present,
; Chemometrics and Intelligent Laboratory Systems 78, 103--112.
;
; VIP returns all VIP values for all variables and all number of components,
; as a number of comps x number of vars matrix.
;
; adapted to IDL by Carsten Oldenburg

;+
; :Hidden:
;-

FUNCTION autoPLSR_vip, structure

  q = structure.yLoadings
  t = structure.scores
  w = structure.weights
  numberOfComponents = N_ELEMENTS(q)
  
  ss = q^2 * DIAG_MATRIX(MATRIX_MULTIPLY(t,t,/ATRANSPOSE))
  wNorm2 = DIAG_MATRIX(MATRIX_MULTIPLY(w,w,/ATRANSPOSE))
  ssw = w^2 * REBIN(TRANSPOSE(ss / wNorm2),N_ELEMENTS(w[*,0]),numberOfComponents)
  vip = SQRT(N_ELEMENTS(w[*,0]) * TOTAL(ssw,2,/CUMULATIVE) / REBIN(TRANSPOSE(TOTAL(ss,/CUMULATIVE)),N_ELEMENTS(w[*,0]),numberOfComponents) )

  RETURN, vip
END