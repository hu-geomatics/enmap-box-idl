;+
; :Hidden:
;-
PRO autoPLSR_cleanup, structArray

  ON_ERROR,2

  FOREACH model, structArray DO BEGIN
    IF PTR_VALID(model.cvstats) THEN PTR_FREE, model.cvstats
    IF PTR_VALID(model.model) THEN PTR_FREE, model.model
    IF PTR_VALID(model.xStdDev) THEN PTR_FREE, model.xStdDev
    IF PTR_VALID(model.cvModel) THEN PTR_FREE, model.cvModel
    IF PTR_VALID(model.jacktest) THEN PTR_FREE, model.jacktest
  ENDFOREACH

END