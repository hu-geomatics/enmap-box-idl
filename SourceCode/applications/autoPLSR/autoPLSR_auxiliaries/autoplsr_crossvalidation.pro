; original R source code by Ron Wehrens and Bjørn-Helge Mevik
; released under the GPL-2
; 
; more information about the 'pls'-package
; URL: http://mevik.net/work/software/pls.html
; and
; URL: http://cran.r-project.org/web/packages/pls/
;
; adapted to IDL by Carsten Oldenburg
;

;+
; :Hidden:
;-

FUNCTION autoPLSR_crossValidation $
  , x $
  , y $
  , NCOMP=numberOfComponents $
  , NSEGMENTS=nSegments $
  , SCALE=scale $
  , RANDOM=random $
  , LOO=loo

  ON_ERROR,2

;  predictor = x
;  response = DOUBLE(y)
;
;  dims = SIZE(predictor,/DIMENSIONS)
;  segIndices = ULINDGEN(dims[0])

  predictor = x
  response = y

; XXX
;response=double(response)

  type = SIZE(response,/TYPE)
  IF PRODUCT(type ne [1,2,3,4,5,12,13,14,15],/INTEGER) gt 0 THEN MESSAGE, "Datatype not valid!!"
  IF TOTAL(type eq [1,2,3,4,12,13],/INTEGER) gt 0 THEN dataType = 4 ELSE dataType = 5; 5,14,15
  dims = SIZE(predictor,/DIMENSIONS)
  segIndices = ULINDGEN(dims[0])

  IF ~ ISA(numberOfComponents) THEN BEGIN
    numberOfComponents = MIN([dims[0]-1,dims[1]])
  ENDIF

  ; determine length of segments
  IF ~ KEYWORD_SET(loo) THEN BEGIN ; ten-fold cross-validation
    IF ~ KEYWORD_SET(nSegments) THEN nSegments = 10
    IF dims[0] lt nSegments THEN BEGIN
      loo = 1 ; Dataset to small, try with Leave-One-Out
    ENDIF ELSE BEGIN
      segLocations = autoPLSR_setSegments(predictor,nSegments)
    ENDELSE
  ENDIF ELSE BEGIN ; Leave-One-Out
    segLocations = [[segIndices],[segIndices]]
    nSegments = dims[0]
  ENDELSE

  IF KEYWORD_SET(random) THEN BEGIN
; TODO: evtl. seed nutzen
    rndIndex = SORT(RANDOMU(SYSTIME(1),dims[0]))
    predictor = predictor[rndIndex,*]
    response = response[rndIndex]
  ENDIF ELSE rndIndex = lindgen(dims[0])

  ; reduce number of components
  reducedNumberOfComponents = MIN([numberOfComponents, dims[0] - MAX(segLocations[*,1]-segLocations[*,0]+1) - 1], /NAN)

  model = PTRARR(nSegments)
;  cvpredicted = DBLARR(dims[0],reducedNumberOfComponents)
;  adj = DBLARR(reducedNumberOfComponents)
  cvpredicted = MAKE_ARRAY(dims[0],reducedNumberOfComponents,TYPE=dataType)
  adj = MAKE_ARRAY(reducedNumberOfComponents,TYPE=dataType)
    
  FOR i=0,nSegments-1 DO BEGIN
    
    currentValidationIndex = WHERE(segIndices ge segLocations[i,0] AND segIndices le segLocations[i,1], $
      COMPLEMENT=currentTrainIndex)
      
    IF KEYWORD_SET(scale) THEN BEGIN ; scaling
      predictorTrain = autoPLSR_scaling(predictor[currentTrainIndex,*],STDDEV=stddev)
      predictorTest = predictor / REBIN(TRANSPOSE(stddev),dims[0],dims[1])
    ENDIF ELSE BEGIN
      predictorTrain = predictor[currentTrainIndex,*]
      predictorTest = predictor
    ENDELSE
    responseTrain = response[currentTrainIndex]
    
    ; fitting
    model[i] = PTR_NEW(autoPLSR_kernelPls(predictorTrain,responseTrain,reducedNumberOfComponents))
    
    interceptArray = REBIN(TRANSPOSE((*model[i]).intercept),dims[0],reducedNumberOfComponents)
    predicted = predictorTest # (*model[i]).coefficients + interceptArray
    cvpredicted[currentValidationIndex,*] = predicted[currentValidationIndex,*]
    adj = adj + N_ELEMENTS(currentValidationIndex) * TOTAL((predicted - REBIN(response,dims[0],reducedNumberOfComponents))^2,1)

  ENDFOR
  ; Calculate validation statistics:
  PRESSO = VARIANCE(response) * dims[0]^2 / (dims[0] - 1)
  PRESS =  TOTAL((cvpredicted - REBIN(response,dims[0],reducedNumberOfComponents))^2,1)
  adj = adj / dims[0]^2

  result = { $
      model : model $
    , cvpredicted : cvpredicted[SORT(rndindex),*] $
    , adj : adj $
    , PRESSO : PRESSO $
    , PRESS : PRESS $
    , numberOfComponents : reducedNumberOfComponents $
    , segLocations : segLocations $
  }
  
  IF KEYWORD_SET(random) THEN result = CREATE_STRUCT(result,"rndIndex", rndIndex)
  
  RETURN, result
END