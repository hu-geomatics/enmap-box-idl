;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

pro autoPLSR_make, Distribution=distribution

  if enmapboxmake.clean() then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    
    appDir = autoPLSR_getDirname()
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ; find source code and application directory  
  
  codeDir =  file_dirname((routine_info('autoPLSR_make',/SOURCE)).path)
  appDir = autoPLSR_getDirname()
  
  ; check lower case *.pro filenames (important for auto-compiling under unix systems
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  file_copy, codeDir+path_sep()+"changelog.txt", appDir $
      , /NOEXPAND_PATH, /OVERWRITE, /REQUIRE_DIRECTORY;, /VERBOSE

  if ~enmapboxmake.CopyOnly() then begin
     ; save routines inside SAVE file
  
    SAVFile = filepath('autoPLSR.sav', ROOT=appDir, SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, /NoLog
    
    if ~enmapboxmake.NoIDLDoc() then begin
      ; create IDLDOC documentation

       helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])

       hub_idlDoc, codeDir, helpOutputDir, 'autoPLSR Documentation', NOSHOW=enmapboxmake.noShow() $
          , SUBTITLE='remote sensing made easy' $
          , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc')
      
    endif
    
    if isa(distribution) then begin
    
      if ~isa(distribution, 'string') then begin
        title = 'Select a directory for writing the distribution.'
        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
        if directory eq '' then return
      endif else begin
        directory = distribution
      endelse

      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
      file_copy, appDir, destination, /OVERWRITE, /RECURSIVE
      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
      file_copy, codeDir, destination, /OVERWRITE, /RECURSIVE
      
    endif
  endif
end
