autoPLSR version 1.11_test

License: GPL-2

Copyright: 2011-2012 Rheinische Friedrich-Wilhelms-Universitaet Bonn, Arbeitsgruppe Schmidtlein

Authors: Sebastian Schmidtlein (concept, R programming), Carsten Oldenburg (IDL programming)

Citation:
If you use this software, please cite it as: Schmidtlein, S., Oldenburg, C. (submitted): Autopls - automated model selection for partial least squares regression.
A published reference is: Schmidtlein, S., Bruelheide, H., Feilhauer, H. (2012). Mapping plant strategy types using remote sensing. Journal of Vegetation Science 23: 395-604.

Optional brightness normalization based on Feilhauer, H., Asner, G., Martin, R.E., Schmidtlein, S. (2010): Brightness-normalized partial least squares regression for hyperspectral data. Journal of Quantitative Spectroscopy and Radiative Transfer 111: 1947-1957.

Contact: s.schmidtlein@uni-bonn.de

Disclaimer
The authors of this software tool accept no responsibility for errors or omissions in this work and shall not be liable for any damage caused by these.

Third Parties

1. hubAPI library (hosted at http://indus.caf.dlr.de/forum/) developed by Humboldt-Universität zu Berlin, Geography Department, Geomatics Lab (http://www.hu-geomatics.de). 
see <EnMAP-Box installation folder>\enmapProject\lib\hubAPI\copyrights\license.txt

2. Source code documentation was created by using IDLdoc (http://idldoc.idldev.com) developed by Michael Galloy (http://michaelgalloy.com). IDLdoc is released under a BSD-type license 
(see <EnMAP-Box installation folder>\enmapProject\lib\IDLdoc\copyrights\license.txt).