;+
; :Author: Carsten Oldenburg
;-

;+
; :Hidden:
;-
PRO autoPLSR_viewInfluenceWidget_cleanup, event

  ON_ERROR,2

  WIDGET_CONTROL, event, GET_UVALUE=modelObject
  modelObject.cleanup

END

;+
; :Hidden:
;-
PRO autoPLSR_viewInfluenceWidget_event, event

  ON_ERROR,2

;  xsize = 750
;  ysize = 550
;
  eventName = WIDGET_INFO(event.ID, /UNAME)
  
  IF STRCMP(eventName,'bgroup') THEN BEGIN

    WIDGET_CONTROL, event.TOP, GET_UVALUE=modelObject

    WIDGET_CONTROL, event.TOP, SENSITIVE=0

    modelObject.plotInfluence, event

    WIDGET_CONTROL, event.TOP, SENSITIVE=1

  ENDIF

END

;+
; :Description:
;    Use this function to view the influence plots of an autoPLSR model.
;
; :Params:
;    modelObject : in, required, type=hash
;      The parameters for the autoPLSR.
;      
;    filenameFeatures : in, required, type=string
;      Filename of the predictor variables.
;    
;    filenameLabels : in, required, type=string
;      Filename of the target variable.
;    
;    ncomps : in, required, type=int
;      Number of components computed.
;    
;    nlv : in, required, type=int
;      Number of latent variable with lowest RMSE.
;
;    settings : in, type=hash
;
;-
PRO autoPLSR_viewInfluenceWidget $
  , modelObject $
  , filenameFeatures $
  , filenameLabels $
  , ncomps $
  , nlv $
  , settings

  ON_ERROR,2

  xsize = 750
  ysize = 550

  title = settings.hubGetValue('title')+' - Influence Plot'
  base = WIDGET_BASE(/COLUMN, UNAME='base', UVALUE=modelObject, title=title)
  WIDGET_CONTROL, base

  baseRow = WIDGET_BASE(base, /ROW)
  base2 = WIDGET_BASE(baseRow, /COLUMN)

  bNames = 'LV ' + STRING(UINDGEN(ncomps) + 1, FORMAT='(I0)')
  buttonListLV = CW_BGROUP(base2, bNames, /EXCLUSIVE, LABEL_TOP='# of LV' $
    , /RETURN_INDEX, /FRAME, Y_SCROLL_SIZE=550, /NO_RELEASE, UNAME='bgroup' $
    , SET_VALUE=nlv, XSIZE=65);, EVENT_FUNCT='autoPLSR_viewModel_event') ;  , X_SCROLL_SIZE=190 YSIZE=ysize, 

  text = [ $
      'Features: ' + filenameFeatures $
    , 'Labels:   ' + filenameLabels $
;    , '# of Latent Variables: ' + STRING(nlv+1, FORMAT='(I0)') $
;    , 'RMSE: ' + 
;    , 'R²:   ' + 
  ]
  widText = WIDGET_TEXT(base, VALUE=text, YSIZE=N_ELEMENTS(text))
    
  tab = WIDGET_TAB(baseRow, XSIZE=xsize)
;  tab = WIDGET_TAB(base)
  subBase1 = WIDGET_BASE(tab, TITLE='Y-Variance', UNAME='subBase1')
  subBase2 = WIDGET_BASE(tab, TITLE='X-Variance', UNAME='subBase2')
  subBase3 = WIDGET_BASE(tab, TITLE='3D-Plot', XSIZE=xsize, YSIZE=ysize, UNAME='subBase3')
  
  WIDGET_CONTROL, base, /REALIZE
  
  XMANAGER, 'autoPLSR_viewInfluenceWidget', base, CLEANUP='autoPLSR_viewInfluenceWidget_cleanup', /NO_BLOCK
  
  event = { $
      value : nlv $
    , top : base $
    , ID : buttonListLV $
  }
  
  autoPLSR_viewInfluenceWidget_event, event

END

PRO autoPLSR_viewInfluence, parameters, settings

  ON_ERROR,2

  IF parameters.HasKey('modelFilename') THEN modelFilename = parameters['modelFilename'] $
    ELSE MESSAGE, "Parameters didn't contain model filename!!"
    
  IF FILE_TEST(modelFilename, /REGULAR) THEN BEGIN
    saveObj = IDL_Savefile(modelFilename)
    names = saveObj.Names()
    struct = saveObj.Contents() 
    IF STRCMP(struct.description, 'autoPLSR-Model', /FOLD_CASE) THEN BEGIN
      saveObj.Restore, names
      saveObj.Cleanup
    ENDIF ELSE BEGIN
      saveObj.Cleanup
      MESSAGE, 'File is not a valid autoPLSR-Model!!'
    ENDELSE
  ENDIF ELSE MESSAGE, 'Model filename is not valid!!'

  filenamepredictors = modelStruct.filenamepredictors
  filenameresponse = modelStruct.filenameresponse
  ncomps = (*modelStruct.model).numberOfComponents
  nlv = modelStruct.numberOfLatentVariable

  modelObj = autoPLSR_viewInfluenceObj(modelStruct)

  autoPLSR_ViewInfluenceWidget $
    , modelObj $
    , filenamepredictors $
    , filenameresponse $
    , ncomps $
    , nlv $
    , settings
 
END