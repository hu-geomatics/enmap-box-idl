;+
; :Author: Carsten Oldenburg
;-

;+
; :Hidden:
;-
FUNCTION autoPLSR_viewInfluenceObj::INIT $
  , modelStruct

  ON_ERROR,2

;  IF ISA(modelStruct, 'autoPLSR_Model') THEN BEGIN
    self.modelStructure = PTR_NEW(modelStruct)

    self.pointObject_green = OBJ_NEW('orb', COLOR=[55, 155, 55])
    self.pointObject_blue = OBJ_NEW('orb', COLOR=[0, 0, 255])
    self.pointObject_orange = OBJ_NEW('orb', COLOR=[255, 111, 0])
    self.pointObject_red = OBJ_NEW('orb', COLOR=[255, 0, 0])
    self.orbObject = OBJ_NEW('orb', COLOR=[0, 0, 255])  
    
    RETURN, 1
;  ENDIF ELSE BEGIN
;    MESSAGE, 'File is not a valid autoPLSR-Model!!'
;  ENDELSE

END

;+
; :Hidden:
;-
PRO autoPLSR_viewInfluenceObj::plotInfluence, event

  ON_ERROR,2

  subBase1 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase1') 
  subBase2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase2') 
  subBase3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase3') 

  draw1 = WIDGET_WINDOW(subBase1, XSIZE=750, YSIZE=550)
  WIDGET_CONTROL, draw1, GET_VALUE=grWindow
  grWindow.Select
  
  ncomp = event.Value
  model = *(*self.modelStructure).model
  selectedpredictors = WHERE((*self.modelStructure).selectedpredictors)
  x = (*self.modelStructure).x[*,selectedpredictors]
  y = (*self.modelStructure).y
  
  coefficients = model.coefficients[*,ncomp]
  
  IF ( (*self.modelStructure).prep ) THEN BEGIN
;    x = x / REBIN(TRANSPOSE(*(*self.modelStructure).prepx),xDim[0],xDim[1])
    x = autoPLSR_preprocessing(x)
  ENDIF
  
  ; scale data if model is scaled
  IF ( (*self.modelStructure).scale ) THEN coefficients = coefficients / *(*self.modelStructure).xStdDev
      
; Samples-Leverage
  dims = SIZE(model.scores[*,0:ncomp],/DIMENSIONS)
  
;  tTt = DIAG_MATRIX(MATRIX_MULTIPLY(model.scores[*,0:ncomp], model.scores[*,0:ncomp], /ATRANSPOSE))
;
;  IF ncomp eq 1 THEN Hi = 1. / dims[0] + (model.scores[*,0:ncomp]^2) / tTt[0] $
;    ELSE Hi = 1. / dims[0] + TOTAL((model.scores[*,0:ncomp]^2) / REBIN(TRANSPOSE(tTt),dims),2)
    
  tTtInv = 1. / DIAG_MATRIX(MATRIX_MULTIPLY(model.scores[*,0:ncomp], model.scores[*,0:ncomp], /ATRANSPOSE))
    
  Hi = 1. / dims[0] + (model.scores[*,0:ncomp]^2) # tTtInv

  ; Y-Residuals and Influence Plot
  predicted = x # coefficients + model.intercept[ncomp]
  yresiduals = predicted - y

  Res2YSamples = yresiduals^2
  resYVar = MATRIX_MULTIPLY(yresiduals, yresiduals, /ATRANSPOSE) / dims[0]

  ; cal = 3.0; val = 2.6
  
  yleverageOutlierLimit = 3. * MEAN(Hi) ; 0.08 <- test
  ysamplesOutlierLimit = 3.^2 * resYVar[0] ; 0.5 <- test
  limitText = [ $
      "Leverage Limit: " + STRING(yleverageOutlierLimit) $
    , "Outlier Limit: " + STRING(ysamplesOutlierLimit) $
  ]
  
  ymaxX = MAX(Hi) * 1.1
  ymaxY = MAX(Res2YSamples) * 1.1
  
  ysamplesNoOutlier = WHERE(Res2YSamples le ysamplesOutlierLimit, ynoOutlierCount $
    , COMPLEMENT=youtlier, NCOMPLEMENT=yOutlierCount)

  IF ynoOutlierCount gt 0 THEN BEGIN
    ysamplesLessInfluence = WHERE(Hi[ysamplesNoOutlier] $
      le yleverageOutlierLimit, ylessInfluenceCount $
      , COMPLEMENT=ylargeInfluence, NCOMPLEMENT=ylargeInfluenceCount)
  ENDIF ELSE BEGIN
    ylessInfluenceCount = 0
    ylargeInfluenceCount = 0
  ENDELSE
    
  IF yOutlierCount gt 0 THEN BEGIN
    ydangerousOutlier = WHERE(Hi[youtlier] gt yleverageOutlierLimit, ydangerousOutlierCount $
      , COMPLEMENT=ylessInflOutlier, NCOMPLEMENT=ylessInflOutlierCount)
  ENDIF ELSE BEGIN
    ylessInflOutlierCount = 0
    ydangerousOutlierCount = 0
  ENDELSE
    
  IF ~ PTR_VALID(self.ploty1) THEN BEGIN
    self.ploty1 = PTR_NEW( $
      PLOT([0.,ymaxX], [0.,ymaxY], XRANGE=[0.,ymaxX], YRANGE=[0.,ymaxY] $
        , PSYM=1, BACKGROUND='FFFFFF'x, COLOR='000000'x $
        , XTITLE='Leverage', YTITLE='Residual Y-Variance' $ ; TITLE='Influence Plot', 
        , XSTYLE=1, YSTYLE=1, OVERPLOT=grWindow, /CURRENT, MARGIN=[0.26,0.2,0.05,0.1], /NODATA) $
    )
    
    !NULL = TEXT(0.5,0.84, 'Influence Plot', ALIGNMENT=0.5, TARGET=(*self.ploty1), FONT_SIZE=16)

    (*self.ploty1).Refresh, /DISABLE

    self.plotyOutlierLimit = PTR_NEW( $
      PLOT([0.,ymaxX], REPLICATE(ysamplesOutlierLimit,2), THICK=1 $
        , COLOR='2244cc'x, LINESTYLE=2, OVERPLOT=grWindow, /CURRENT) $
    )
          
    self.ploty5 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_green, OVERPLOT=grWindow, /CURRENT) $
    )
      
    self.ploty3 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_blue, OVERPLOT=grWindow, /CURRENT) $
    )
    
    self.plotyInfluenceLimit = PTR_NEW( $
      PLOT(REPLICATE(yleverageOutlierLimit,2), [0.,ymaxY], THICK=1 $
        , COLOR='4488dd'x, LINESTYLE=2, OVERPLOT=grWindow, /CURRENT) $
    )
      
    self.ploty6 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_orange, OVERPLOT=grWindow, /CURRENT) $
    )
      
    self.ploty7 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_red, OVERPLOT=grWindow, /CURRENT) $
    )
 
    xy = BYTARR(dims[0])
    label = STRING(INDGEN(dims[0]) + 1,FORMAT='(I0)')
    self.textyLabel = PTR_NEW( $
      TEXT(xy, xy, label, TARGET=(*self.ploty1), /DATA, FONT_SIZE=12) $
    )

    self.limityTextPlot = PTR_NEW( $
      TEXT([0.01,0.01],[0.05, 0.01], limitText, TARGET=(*self.ploty1), FONT_SIZE=12) $
    )

  ENDIF

  (*self.ploty1).Refresh, /DISABLE
  
  (*self.ploty1).SetProperty, XRANGE=[0.,ymaxX], YRANGE=[0.,ymaxY]
  
  (*self.plotyOutlierLimit).putData, TRANSPOSE([[0.,ymaxX], [REPLICATE(ysamplesOutlierLimit,2)]])
  IF yOutlierCount eq 0 THEN (*self.plotyOutlierLimit).Hide = 1 $
    ELSE (*self.plotyOutlierLimit).Hide = 0

  (*self.plotyInfluenceLimit).putData, TRANSPOSE([[REPLICATE(yleverageOutlierLimit,2)], [0.,ymaxY]])
  IF ylargeInfluenceCount eq 0 AND ydangerousOutlierCount eq 0 THEN (*self.plotyInfluenceLimit).Hide = 1 $
    ELSE (*self.plotyInfluenceLimit).Hide = 0
          
  IF ynoOutlierCount gt 0 AND ylessInfluenceCount gt 0 THEN BEGIN
    (*self.ploty5).Hide = 0
    (*self.ploty5).putData, TRANSPOSE([[-1,-2,Hi[ysamplesNoOutlier[ysamplesLessInfluence]]] $
      , [0,0,Res2YSamples[ysamplesNoOutlier[ysamplesLessInfluence]]]])
  ENDIF ELSE (*self.ploty5).Hide = 1
      
  IF ynoOutlierCount gt 0 AND ylargeInfluenceCount gt 0 THEN BEGIN
    (*self.ploty3).Hide = 0
    (*self.ploty3).putData, TRANSPOSE([[-1,-2,Hi[ysamplesNoOutlier[ylargeInfluence]]] $
      , [0,0,Res2YSamples[ysamplesNoOutlier[ylargeInfluence]]]])
  ENDIF ELSE (*self.ploty3).Hide = 1
      
  IF yOutlierCount gt 0 AND ylessInflOutlierCount gt 0 THEN BEGIN
    (*self.ploty6).Hide = 0
    (*self.ploty6).putData, TRANSPOSE([[-1,-2,Hi[youtlier[ylessInflOutlier]]] $
      , [0,0,Res2YSamples[youtlier[ylessInflOutlier]]]])
  ENDIF ELSE (*self.ploty6).Hide = 1
      
  IF yOutlierCount gt 0 AND ydangerousOutlierCount gt 0 THEN BEGIN
    (*self.ploty7).Hide = 0
    (*self.ploty7).putData, TRANSPOSE([[-1,-2,Hi[youtlier[ydangerousOutlier]]] $
        , [0,0,Res2YSamples[youtlier[ydangerousOutlier]]]])
  ENDIF ELSE (*self.ploty7).Hide = 1
  
  
  offsetX = ymaxX * 0.01
  offsetY = ymaxY * 0.01
  FOREACH textObj, (*self.textyLabel), i DO BEGIN
    textObj.SetProperty, LOCATIONS=[Hi[i]+offsetX,Res2YSamples[i]+offsetY]
  ENDFOREACH

  FOREACH textObj, (*self.limityTextPlot), i DO BEGIN
    textObj.putData, limitText[i]
  ENDFOREACH




;  ; X-Residuals and Influence Plot
  draw2 = WIDGET_WINDOW(subBase2, XSIZE=750, YSIZE=550)
  WIDGET_CONTROL, draw2, GET_VALUE=grWindow2
  grWindow2.Select
 
  xDim = SIZE(x, /DIMENSIONS)
  xMeanArray = REBIN(TRANSPOSE(model.xMean),xDim[0],xDim[1])
  
  ; scaling
  IF ( (*self.modelStructure).scale ) THEN BEGIN
    x = x / REBIN(TRANSPOSE(*(*self.modelStructure).xstddev),xDim[0],xDim[1])
  ENDIF
  
  xCentered = x - xMeanArray 
  
  calX = MATRIX_MULTIPLY(model.scores[*,0:ncomp], model.xloadings[*,0:ncomp], /BTRANSPOSE)
  
;  xCentered = xCentered - REBIN( TRANSPOSE(MEAN(calX,DIMENSION=1)) ,xDim[0],xDim[1])
;  xresiduals = TOTAL(xCentered - calX,2)
  
  xresiduals = xCentered - calX
  Res2XSamples = TOTAL(xresiduals^2,2) / xDim[1]

  resXVar = MATRIX_MULTIPLY(xresiduals, xresiduals, /ATRANSPOSE) / dims[0]

  ; cal = 3.0; val = 2.6
  xleverageOutlierLimit = 3. * MEAN(Hi) ; 0.08 <- test
  xsamplesOutlierLimit = 3.^2 * resXVar[0] ;  <- test
  limitxText = [ $
      "Leverage Limit: " + STRING(xleverageOutlierLimit) $
    , "Outlier Limit: " + STRING(xsamplesOutlierLimit) $
  ]
  
  xmaxX = MAX(Hi) * 1.1
  xmaxY = MAX(Res2XSamples) * 1.1
  
  xsamplesNoOutlier = WHERE(Res2XSamples le xsamplesOutlierLimit, xnoOutlierCount $
    , COMPLEMENT=xoutlier, NCOMPLEMENT=xoutlierCount)
    
  IF xnoOutlierCount gt 0 THEN BEGIN
    xsamplesLessInfluence = WHERE(Hi[xsamplesNoOutlier] $
      le xleverageOutlierLimit, xlessInfluenceCount $
      , COMPLEMENT=xlargeInfluence, NCOMPLEMENT=xlargeInfluenceCount)
  ENDIF ELSE BEGIN
    xlessInfluenceCount = 0
    xlargeInfluenceCount = 0
  ENDELSE
    
  IF xoutlierCount gt 0 THEN BEGIN
    xdangerousOutlier = WHERE(Hi[xoutlier] gt xleverageOutlierLimit, xdangerousOutlierCount $
      , COMPLEMENT=xlessInflOutlier, NCOMPLEMENT=xlessInflOutlierCount)
  ENDIF ELSE BEGIN
    xlessInflOutlierCount = 0
    xdangerousOutlierCount = 0
  ENDELSE 
  
  IF ~ PTR_VALID(self.plotx1) THEN BEGIN
  
    self.plotx1 = PTR_NEW( $
      PLOT([0.,xmaxX], [0.,xmaxY], XRANGE=[0.,xmaxX], YRANGE=[0.,xmaxY] $
        , PSYM=1, BACKGROUND='FFFFFF'x, COLOR='000000'x $
        , XTITLE='Leverage', YTITLE='Residual X-Variance' $ ; TITLE='Influence Plot', 
        , XSTYLE=1, YSTYLE=1, OVERPLOT=grWindow2, /CURRENT, MARGIN=[0.26,0.2,0.05,0.1], /NODATA) $
    )
    
    !NULL = TEXT(0.5,0.84, 'Influence Plot', ALIGNMENT=0.5, TARGET=(*self.plotx1), FONT_SIZE=16)

    (*self.plotx1).Refresh, /DISABLE

    self.plotxOutlierLimit = PTR_NEW( $
      PLOT([0.,xmaxX], REPLICATE(xsamplesOutlierLimit,2), THICK=1 $
        , COLOR='2244cc'x, LINESTYLE=2, OVERPLOT=grWindow2, /CURRENT) $
    )  
  
    self.plotx5 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_green, OVERPLOT=grWindow2, /CURRENT) $
    )

    self.plotx3 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_blue, OVERPLOT=grWindow2, /CURRENT) $
    )
    
    self.plotxInfluenceLimit = PTR_NEW( $
      PLOT(REPLICATE(xleverageOutlierLimit,2), [0.,xmaxY], THICK=1 $
        , COLOR='4488dd'x, LINESTYLE=2, OVERPLOT=grWindow2, /CURRENT) $
    )

    self.plotx6 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_orange, OVERPLOT=grWindow2, /CURRENT) $
    )
    
    self.plotx7 = PTR_NEW( $
      PLOT([-1,-2], [0,0], SYM_SIZE=0.5, LINESTYLE=6 $
        , SYM_OBJECT=self.pointObject_red, OVERPLOT=grWindow2, /CURRENT) $
    )   
         
    xy = BYTARR(dims[0])
    label = STRING(INDGEN(dims[0]) + 1,FORMAT='(I0)')
    self.textxLabel = PTR_NEW( $
      TEXT(xy, xy, label, TARGET=(*self.plotx1), /DATA, FONT_SIZE=12) $
    )

    self.limitxTextPlot = PTR_NEW( $
      TEXT([0.01,0.01],[0.05, 0.01], limitxText, TARGET=(*self.plotx1), FONT_SIZE=12) $
    )

  ENDIF
    
  (*self.plotx1).Refresh, /DISABLE

  (*self.plotx1).SetProperty, XRANGE=[0.,xmaxX], YRANGE=[0.,xmaxY]

  (*self.plotxOutlierLimit).putData, TRANSPOSE([[0.,xmaxX], [REPLICATE(xsamplesOutlierLimit,2)]])
  IF xOutlierCount eq 0 THEN (*self.plotxOutlierLimit).Hide = 1 $
    ELSE (*self.plotxOutlierLimit).Hide = 0

  (*self.plotxInfluenceLimit).putData, TRANSPOSE([[REPLICATE(xleverageOutlierLimit,2)], [0.,xmaxY]])
  IF xlargeInfluenceCount eq 0 AND xdangerousOutlierCount eq 0 THEN (*self.plotxInfluenceLimit).Hide = 1 $
    ELSE (*self.plotxInfluenceLimit).Hide = 0
  
  
  IF xnoOutlierCount gt 0 AND xlessInfluenceCount gt 0 THEN BEGIN
    (*self.plotx5).Hide = 0
    (*self.plotx5).putData, TRANSPOSE([[-1,-2,Hi[xsamplesNoOutlier[xsamplesLessInfluence]]] $
      , [0,0,Res2XSamples[xsamplesNoOutlier[xsamplesLessInfluence]]]])
  ENDIF ELSE (*self.plotx5).Hide = 1

  IF xnoOutlierCount gt 0 AND xlargeInfluenceCount gt 0 THEN BEGIN
    (*self.plotx3).Hide = 0
    (*self.plotx3).putData, TRANSPOSE([[-1,-2,Hi[xsamplesNoOutlier[xlargeInfluence]]] $
      , [0,0,Res2XSamples[xsamplesNoOutlier[xlargeInfluence]]]])
  ENDIF ELSE (*self.plotx3).Hide = 1
  
  IF xoutlierCount gt 0 AND xlessInflOutlierCount gt 0 THEN BEGIN
    (*self.plotx6).Hide = 0
    (*self.plotx6).putData, TRANSPOSE([[-1,-2,Hi[xoutlier[xlessInflOutlier]]] $
      , [0,0,Res2XSamples[xoutlier[xlessInflOutlier]]]])
  ENDIF ELSE (*self.plotx6).Hide = 1
   
  IF xoutlierCount gt 0 AND xdangerousOutlierCount gt 0 THEN BEGIN
    (*self.plotx7).Hide = 0
    (*self.plotx7).putData, TRANSPOSE([[-1,-2,Hi[xoutlier[xdangerousOutlier]]] $
      , [0,0,Res2XSamples[xoutlier[xdangerousOutlier]]]])
  ENDIF ELSE (*self.plotx7).Hide = 1
  
  offsetX = xmaxX * 0.01
  offsetY = xmaxY * 0.01
  FOREACH textObj, (*self.textxLabel), i DO BEGIN
    textObj.SetProperty, LOCATIONS=[Hi[i]+offsetX,Res2XSamples[i]+offsetY]
  ENDFOREACH

  FOREACH textObj, (*self.limitxTextPlot), i DO BEGIN
    textObj.putData, limitxText[i]
  ENDFOREACH




  ; 3D-Plot
  draw3 = WIDGET_WINDOW(subBase3, XSIZE=750, YSIZE=550)
  WIDGET_CONTROL, draw3, GET_VALUE=grwindow3
  grwindow3.Select

  IF ~ PTR_VALID(self.d3plot) THEN BEGIN
  
    self.d3plot = PTR_NEW( $
      PLOT3D([0.,xmaxX], [0.,xmaxY], [0.,ymaxY], XRANGE=[0.,xmaxX] $
        , YRANGE=[0.,xmaxY], ZRANGE=[0.,ymaxY], OVERPLOT=grWindow3 $
        , /CURRENT, LINESTYLE=6, XTITLE='Leverage', YTITLE='Residual X-variance' $
        , ZTITLE='Residual Y-variance', /NODATA) $
    )
    
    (*self.d3plot).Refresh, /DISABLE


    self.d3plotpoints = PTR_NEW( $
      PLOT3D([-1,-2], [0,0], [0,0], SYM_OBJECT=self.orbObject $
        , LINESTYLE=6, OVERPLOT=grWindow3, /CURRENT) $
    )
    
    label = STRING(INDGEN(dims[0]) + 1,FORMAT='(I0)')
    xy = BYTARR(dims[0])

    self.d3text = PTR_NEW( $
      TEXT(xy, xy, xy, label $
        , TARGET=(*self.d3plot), /DATA, FONT_SIZE=12) $
    )
    
  ENDIF

  (*self.d3plot).Refresh, /DISABLE

  (*self.d3plot).SetProperty, XRANGE=[0.,xmaxX], YRANGE=[0.,xmaxY], ZRANGE=[0.,ymaxY]
      
  (*self.d3plotpoints).putData, TRANSPOSE([[Hi],[Res2XSamples],[Res2YSamples]])
    
  offsetX = xmaxX * 0.01
  offsetY = xmaxY * 0.01
  offsetZ = ymaxY * 0.01

  FOREACH textObj, (*self.d3text), i DO BEGIN
    textObj.SetProperty, LOCATIONS=[Hi[i]+offsetX,Res2XSamples[i]+offsetY,Res2YSamples[i]+offsetZ]
  ENDFOREACH
  
  
  (*self.ploty1).Refresh
  (*self.plotx1).Refresh
  (*self.d3plot).Refresh


END

;+
; :Hidden:
;-
PRO autoPLSR_viewInfluenceObj::cleanup

  ON_ERROR,2

  IF OBJ_VALID(pointObject_green) THEN OBJ_DESTROY, pointObject_green
  IF OBJ_VALID(pointObject_blue) THEN OBJ_DESTROY, pointObject_blue
  IF OBJ_VALID(pointObject_orange) THEN OBJ_DESTROY, pointObject_orange
  IF OBJ_VALID(pointObject_red) THEN OBJ_DESTROY, pointObject_red
  IF OBJ_VALID(orbObject) THEN OBJ_DESTROY, orbObject

  IF PTR_VALID(self.modelStructure) THEN autoPLSR_cleanup, *self.modelStructure
  
  IF PTR_VALID(self.modelStructure) THEN PTR_FREE, self.modelStructure
  IF PTR_VALID(self.plotyOutlierLimit) THEN PTR_FREE, self.plotyOutlierLimit
  IF PTR_VALID(self.plotyInfluenceLimit) THEN PTR_FREE, self.plotyInfluenceLimit
  IF PTR_VALID(self.ploty1) THEN PTR_FREE, self.ploty1
  IF PTR_VALID(self.ploty3) THEN PTR_FREE, self.ploty3
  IF PTR_VALID(self.ploty5) THEN PTR_FREE, self.ploty5
  IF PTR_VALID(self.ploty6) THEN PTR_FREE, self.ploty6
  IF PTR_VALID(self.ploty7) THEN PTR_FREE, self.ploty7
  IF PTR_VALID(self.textyLabel) THEN PTR_FREE, self.textyLabel
  IF PTR_VALID(self.limityTextPlot) THEN PTR_FREE, self.limityTextPlot
  IF PTR_VALID(self.plotx1) THEN PTR_FREE, self.plotx1
  IF PTR_VALID(self.plotxOutlierLimit) THEN PTR_FREE, self.plotxOutlierLimit
  IF PTR_VALID(self.plotxInfluenceLimit) THEN PTR_FREE, self.plotxInfluenceLimit
  IF PTR_VALID(self.plotx3) THEN PTR_FREE, self.plotx3
  IF PTR_VALID(self.plotx5) THEN PTR_FREE, self.plotx5
  IF PTR_VALID(self.plotx6) THEN PTR_FREE, self.plotx6
  IF PTR_VALID(self.plotx7) THEN PTR_FREE, self.plotx7
  IF PTR_VALID(self.textxLabel) THEN PTR_FREE, self.textxLabel
  IF PTR_VALID(self.limitxTextPlot) THEN PTR_FREE, self.limitxTextPlot
  IF PTR_VALID(self.d3plot) THEN PTR_FREE, self.d3plot
  IF PTR_VALID(self.d3plotpoints) THEN PTR_FREE, self.d3plotpoints
  IF PTR_VALID(self.d3text) THEN PTR_FREE, self.d3text

END

;+
; :Hidden:
;-
PRO autoPLSR_viewInfluenceObj__define
  struct = { autoPLSR_viewInfluenceObj $
    , modelStructure : PTR_NEW() $
    , pointObject_green : OBJ_NEW() $
    , pointObject_blue : OBJ_NEW() $
    , pointObject_orange : OBJ_NEW() $
    , pointObject_red : OBJ_NEW() $
    , orbObject : OBJ_NEW() $
    , ploty1 : PTR_NEW() $
    , plotyOutlierLimit : PTR_NEW() $
    , plotyInfluenceLimit : PTR_NEW() $
    , ploty3 : PTR_NEW() $
    , ploty5 : PTR_NEW() $
    , ploty6 : PTR_NEW() $
    , ploty7 : PTR_NEW() $
    , textyLabel : PTR_NEW() $
    , limityTextPlot : PTR_NEW() $
    , plotx1 : PTR_NEW() $
    , plotxOutlierLimit : PTR_NEW() $
    , plotx5 : PTR_NEW() $
    , plotx3 : PTR_NEW() $
    , plotxInfluenceLimit : PTR_NEW() $
    , plotx6 : PTR_NEW() $
    , plotx7 : PTR_NEW() $
    , textxLabel : PTR_NEW() $
    , limitxTextPlot : PTR_NEW() $
    , d3plot : PTR_NEW() $
    , d3plotpoints : PTR_NEW() $
    , d3text : PTR_NEW() $
  }
END