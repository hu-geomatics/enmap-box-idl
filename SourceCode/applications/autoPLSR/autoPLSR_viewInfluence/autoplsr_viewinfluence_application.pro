;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input 
;    via a widget program. The user input is passed to the application
;    processing routine.
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro autoPLSR_viewInfluence_application, applicationInfo
  
  ; get global settings for this application
  settings = autoPLSR_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = autoPLSR_viewInfluence_getParameters(settings)
  
  if parameters['accept'] then begin
  
    autoPLSR_viewInfluence, parameters, settings
  
  endif
  
end