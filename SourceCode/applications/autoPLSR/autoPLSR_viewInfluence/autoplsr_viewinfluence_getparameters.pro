;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION autoPLSR_viewInfluence_getParameters $
  , settings

  ON_ERROR,2

  groupLeader = settings.hubGetValue('groupLeader')
  
;  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - View Influence Plot'

  HUBAMW_FRAME, Title='Input'
  HUBAMW_INPUTFILENAME, 'modelFilename', Title='Input Model', EXTENSION='plsr'

  result = HUBAMW_MANAGE()

  RETURN, result

END