;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('autoPLSR.conf', ROOT=autoPLSR_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, autoPLSR_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function autoPLSR_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('autoplsr.conf', ROOT=autoPLSR_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;;+
;; :Hidden:
;;-
;pro test_autoPLSR_getSettings
;
;  print, autoPLSR_getSettings()
;
;end
