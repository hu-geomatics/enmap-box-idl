;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function autoPLSR_getDirname
  
  result = filepath('autoPLSR', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;pro test_autoPLSR_getDirname
;
;  print, autoPLSR_getDirname()
;  print, autoPLSR_getDirname(/SourceCode)
;
;end
