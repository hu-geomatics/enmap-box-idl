;+
; :Author: Carsten Oldenburg
;-

;+
; :Hidden:
;-
FUNCTION autoPLSR_viewModelObj::INIT $
  , modelStruct

  ON_ERROR,2
;
;  IF ISA(modelStruct, 'autoPLSR_Model') THEN BEGIN
    self.modelStructure = PTR_NEW(modelStruct)
    RETURN, 1
;  ENDIF ELSE BEGIN
;    MESSAGE, 'File is not a valid autoPLSR-Model!!'
;  ENDELSE

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelObj::plotValues, event

  ON_ERROR,2

  draw1 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw1') 
  WIDGET_CONTROL, draw1, GET_VALUE=grWindow

  lv = event.value

  r2val = (*(*self.modelStructure).cvStats).r2val[1:*] ; -> index 0 is intercept
  r2cal = (*(*self.modelStructure).cvStats).r2cal[1:*] ; -> index 0 is intercept
;  lv = modelStructure.numberOfLatentVariable
  
  predicted = (*(*self.modelStructure).cvStats).predicted[*,lv]
  cvPredicted = (*(*self.modelStructure).cvModel).cvpredicted[*,lv]
  !NULL = LINFIT((*self.modelStructure).y, predicted, YFIT=yfitPredicted)
  !NULL = LINFIT((*self.modelStructure).y, cvPredicted, YFIT=yfitCvPredicted)

  IF ~ PTR_VALID(self.plotValues) THEN BEGIN
    observedMax = MAX((*self.modelStructure).y, MIN=observedMin)
    predictedMax = MAX([(*(*self.modelStructure).cvStats).predicted,(*(*self.modelStructure).cvModel).cvpredicted], MIN=predictedMin)
    IF observedMax gt 0 THEN observedMax = observedMax * 1.1 ELSE observedMax = observedMax / 1.2
    IF observedMin gt 0 THEN observedMin = observedMin / 1.2 ELSE observedMin = observedMin * 1.1
    IF predictedMax gt 0 THEN predictedMax = predictedMax * 1.1 ELSE predictedMax = predictedMax / 1.2
    IF predictedMin gt 0 THEN predictedMin = predictedMin / 1.2 ELSE predictedMin = predictedMin * 1.1

    self.plotValues = PTR_NEW( $
      PLOT([observedMin,observedMax], [predictedMin,predictedMax] $
        , XRANGE=[observedMin,observedMax], YRANGE=[predictedMin,predictedMax] $
        , BACKGROUND_COLOR='white', COLOR='black', TITLE='Predicted vs. observed values' $
        , XTITLE='Predicted', YTITLE='Observed', XSTYLE=1, YSTYLE=1, OVERPLOT=grWindow, /NODATA) $ ; OVERPLOT=grWindow
    )

    (*self.plotValues).Refresh, /DISABLE

    self.plotValues1 = PTR_NEW( $
      PLOT((*self.modelStructure).y, predicted, THICK=2, SYM_COLOR='red', NAME='Calibration' $
        , LINESTYLE=6, SYMBOL='o', OVERPLOT=grWindow) $ ;, /CURRENT
    )
    self.plotValues2 = PTR_NEW( $
      PLOT((*self.modelStructure).y, cvPredicted, THICK=2, SYM_COLOR='blue', NAME='Validation' $
        , LINESTYLE=6, SYMBOL='o', /SYM_FILLED, SYM_SIZE=0.8, OVERPLOT=grWindow, /CURRENT) $
    )
      
    self.plotValuesCurve1 = PTR_NEW( $
      PLOT((*self.modelStructure).y, yfitPredicted, THICK=1, COLOR='red', OVERPLOT=grWindow, LINESTYLE=0, /CURRENT) $
    )
    self.plotValuesCurve2 = PTR_NEW( $
      PLOT((*self.modelStructure).y, yfitCvPredicted, THICK=1, COLOR='blue', OVERPLOT=grWindow, /CURRENT) $
    )

    self.plotValuesLegend = PTR_NEW( $
      LEGEND(TARGET=[*self.plotValues1,*self.plotValues2], VERTICAL_ALIGNMENT='TOP', HORIZONTAL_ALIGNMENT='LEFT' $
        , POSITION=[0.175,0.81]) $
    )

    self.plotValuesText1 = PTR_NEW( $
      TEXT(0.63, 0.24, 'R² cal = '+STRING(r2cal[lv], FORMAT='(F5.3)'), 'r') $
    )
    self.plotValuesText2 = PTR_NEW( $
      TEXT(0.63, 0.2, 'R² val = '+STRING(r2val[lv], FORMAT='(F5.3)'), 'b') $
    )
  ENDIF ELSE BEGIN
    (*self.plotValues).Refresh, /DISABLE
  
    ; update data  
    (*self.plotValues1).putData, TRANSPOSE([[(*self.modelStructure).y], [predicted]])
    (*self.plotValues2).putData, TRANSPOSE([[(*self.modelStructure).y], [cvPredicted]])
    (*self.plotValuesCurve1).putData, TRANSPOSE([[(*self.modelStructure).y], [yfitPredicted]])
    (*self.plotValuesCurve2).putData, TRANSPOSE([[(*self.modelStructure).y], [yfitCvPredicted]])
    (*self.plotValuesText1).putData, 'R² cal = '+STRING(r2cal[lv], FORMAT='(F5.3)')
    (*self.plotValuesText2).putData, 'R² val = '+STRING(r2val[lv], FORMAT='(F5.3)')
  ENDELSE
   
  (*self.plotValues).Refresh

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelObj::plotRMSE, event ;, NLIMIT=nlimit, NLV=lv

  ON_ERROR,2

  lv = event.Value

  draw2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw2') 
  WIDGET_CONTROL, draw2, GET_VALUE=grWindow

  IF ~ PTR_VALID(self.plotRMSE) THEN BEGIN
  
    rmseCal = (*(*self.modelStructure).cvStats).rmsep[1:*,0] ; -> index 0 is intercept
    rmseVal = (*(*self.modelStructure).cvStats).rmsep[1:*,1] ; -> index 0 is intercept
    nVar = N_ELEMENTS((*(*self.modelStructure).model).coefficients[*,lv])
    ncomp = (*(*self.modelStructure).model).numberofcomponents
  
    xmax = ncomp ; MIN([ncomp,nVar])
    ymax = MAX([rmseCal,rmseVal]) * 1.1
    self.plotRMSEymax = ymax
    xTicks = UINDGEN(xmax) + 1
    pos = 95. / 750 ; xsize=750
  
    self.plotRMSE = PTR_NEW( $
      PLOT([0,xmax+1], [0.,ymax], XRANGE=[0,xmax+1], YRANGE=[0.,ymax] $
        , SYMBOL='+', BACKGROUND_COLOR ='white', COLOR='black' $
        , TITLE='RMSE vs. Number of latent vectors', XTITLE='Number of latent vectors', YTITLE='RMSE' $
        , XSTYLE=1, YSTYLE=1, OVERPLOT=grWindow, /NODATA, POSITION=[pos,0.15,1.-2*pos,0.8]) $
         ; , MARGIN=[0.2,0.1,0.4,0.1]
    )
    
    (*self.plotRMSE).Refresh, /DISABLE
    
    plot1 = BARPLOT(xTicks, rmseVal[0:xmax-1], THICK=2, FILL_COLOR='blue' $
      , NAME='Validation', OVERPLOT=grWindow, /CURRENT)
    plot2 = BARPLOT(xTicks, rmseCal[0:xmax-1], THICK=2, FILL_COLOR='red' $
      , NAME='Calibration', OVERPLOT=grWindow, /CURRENT)

    self.plotRMSE3 = PTR_NEW( $
      PLOT([lv+1,lv+1], [0,self.plotRMSEymax], THICK=1, COLOR='111111'x, LINESTYLE=2 $
        , NAME='Selected LV', OVERPLOT=grWindow, /CURRENT) $
    )
    target = [plot1,plot2,(*self.plotRMSE3)]
  
    IF ISA(nlimit) THEN BEGIN
      plot4 = PLOT([nlimit,nlimit], [0,ymax], THICK=1, COLOR='111111'x, LINESTYLE=':' $
        , OVERPLOT=grWindow, NAME='Restricted LV', /CURRENT)
      target = [target,plot4]
    ENDIF

    legend = LEGEND(TARGET=target, VERTICAL_ALIGNMENT='CENTER', HORIZONTAL_ALIGNMENT='LEFT', $
      POSITION=[xmax+1.3,ymax*0.5], /DATA) ; 
  ENDIF
  
  (*self.plotRMSE3).putData, TRANSPOSE([[lv+1,lv+1], [0,self.plotRMSEymax]])
  
  (*self.plotRMSE).Refresh

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelObj::plotCoefficients, event

  ON_ERROR,2

  lv = event.Value

  coefficients = (*(*self.modelStructure).model).coefficients[*,lv]
;  nVar = N_ELEMENTS(coefficients)
  nVar = N_ELEMENTS((*self.modelStructure).selectedpredictors)
  selectedpredictors = WHERE((*self.modelStructure).selectedpredictors)
;  nVarText = STRING(selectedpredictors + 1, FORMAT='(I0)')
;  maxCoef = MAX((*(*self.modelStructure).model).coefficients, MIN=minCoef)
  maxCoef = MAX(coefficients, MIN=minCoef)
  jt = *(*self.modelStructure).jacktest

  index1 = WHERE(jt lt 0.5 AND jt ge 0.1, /NULL)
  index2 = WHERE(jt lt 0.1 AND jt ge 0.05, /NULL)
  index3 = WHERE(jt lt 0.05 AND jt ge 0.01, /NULL)
  index4 = WHERE(jt lt 0.01, /NULL)

  subBase3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase3') 
  draw3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw3') 

  xsize = nVar*20
  IF xsize lt 750 THEN xsize = 750
  pos = 85. / xsize

  IF draw3 eq 0 THEN BEGIN
;    subsubBase3 = WIDGET_BASE(subBase3, XSIZE=1050, YSIZE=ysize, UNAME='subsubBase3', /SCROLL, X_SCROLL_SIZE=xsize)
    draw3 = WIDGET_WINDOW(subBase3, XSIZE=xsize, YSIZE=530, UNAME='draw3', /SCROLL, X_SCROLL_SIZE=749, Y_SCROLL_SIZE=550)
    ;, SENSITIVE=0) ; 
  ENDIF;
  draw3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw3') 
  WIDGET_CONTROL, draw3, GET_VALUE=grWindow

  ; Plot 3
  xmax = nVar
;  xTicks = UINDGEN(nVar) + 1
  xTicks = UINDGEN(xmax) + 1
  xcoefficients = fltarr(xmax)
  xcoefficients[selectedpredictors] = coefficients

  IF ~ PTR_VALID(self.plotCoefficients) THEN BEGIN
  
    self.plotCoefficients = PTR_NEW( $
      PLOT([0,xmax+1], [minCoef*1.05,maxCoef*1.05] $
;        , XRANGE=[1,xmax], YRANGE=[minCoef*1.05,maxCoef*1.05], PSYM=1 $
        , BACKGROUND_COLOR='white', COLOR='black', TITLE='Regression coefficients' $
        , XTITLE='# of Predictor variable', YTITLE='Regr. Coeff.', XSTYLE=1, YSTYLE=1, /NODATA $
        , OVERPLOT=grWindow, /CURRENT, POSITION=[pos,0.15,1.-2*pos,0.8] $
        , XMINOR=0, XTICKDIR=1, XTICKINTERVAL=5, XTICKLEN=0.025) $ XMAJOR=0, XTICKLEN=0.5, XTICKNAME=''
;        , XTICKNAME=STRING(nVarText, FORMAT='(I0)'), XTICKFONT_SIZE=7 $
;        , XMINOR=0, XTICKINTERVAL=1, XTICKVALUES=xTicks, MARGIN=[0.1,0.1,0.25,0.1]) $
    )
;    axisFont = OBJ_NEW('IDLgrFont', SIZE=7)
;    axisText = OBJ_NEW('IDLgrText',nVarText, FONT=axisFont)
;    self.barplotAxis = PTR_NEW( $
;      AXIS(0, LOCATION=[0.,0.], MINOR=0, MAJOR=0, TARGET=*self.plotCoefficients $ , TICKVALUES=xTicks
;      ) $
;    )

    (*self.plotCoefficients).Refresh, /DISABLE
    
    self.plotcurve = PTR_NEW( $
      BARPLOT(xTicks, xcoefficients, THICK=1, OVERPLOT=grWindow, WIDTH=0.25, COLOR='white '$
        , FILL_COLOR='gray') $
    )

;    self.plotcurve = PTR_NEW( $
;      PLOT(xTicks, coefficients, THICK=2, OVERPLOT=grWindow ) $
;    )

  ;  indexNull = WHERE(jt ge 0.5, /NULL)
  ;  IF ISA(indexNull) THEN plotNull = PLOT(xTicks[indexNull], coefficients[indexNull] $
  ;    , THICK=2, SYMBOL='o', SYM_TRANSPARENCY=100, NAME='p > 0.5', /OVERPLOT)
  
    target = !NULL
  
    IF ISA(index1) THEN BEGIN
      self.coefplot1 = PTR_NEW( $
        PLOT(xTicks[selectedpredictors[index1]], xcoefficients[selectedpredictors[index1]], LINESTYLE=6 $
          , THICK=2, SYMBOL='o', SYM_FILL_COLOR='white', NAME='p < 0.5', OVERPLOT=grWindow, /CURRENT) $
      )
      target = [target,*self.coefplot1]
    ENDIF
  
    IF ISA(index2) THEN BEGIN
      self.coefplot2 = PTR_NEW( $
        PLOT(xTicks[selectedpredictors[index2]], xcoefficients[selectedpredictors[index2]], LINESTYLE=6 $
          , THICK=2, SYMBOL='o', COLOR='yellow', /SYM_FILLED, NAME='p < 0.1', OVERPLOT=grWindow, /CURRENT) $
      )
      target = [target,*self.coefplot2]
    ENDIF
    
    IF ISA(index3) THEN BEGIN
      self.coefplot3 = PTR_NEW( $
        PLOT(xTicks[selectedpredictors[index3]], xcoefficients[selectedpredictors[index3]], LINESTYLE=6 $
          , THICK=2, SYMBOL='o', COLOR='orange', /SYM_FILLED, NAME='p < 0.05', OVERPLOT=grWindow, /CURRENT) $
      )
      target = [target,*self.coefplot3]
    ENDIF

    IF ISA(index4) THEN BEGIN
      self.coefplot4 = PTR_NEW( $
        PLOT(xTicks[selectedpredictors[index4]], xcoefficients[selectedpredictors[index4]], LINESTYLE=6 $
          , THICK=2, SYMBOL='o', COLOR='red', /SYM_FILLED, NAME='p < 0.01', OVERPLOT=grWindow, /CURRENT) $
      )
      target = [target,*self.coefplot4]
    ENDIF
    
    ; middle x-Axis
    !null = PLOT([0,xmax+1],[0.,0.], OVERPLOT=grWindow)
  
    legend = LEGEND(TARGET=target, VERTICAL_ALIGNMENT='CENTER', HORIZONTAL_ALIGNMENT='LEFT', $
      POSITION=[1.-1.95*pos,0.5])
    
  ENDIF ELSE BEGIN
  
    (*self.plotCoefficients).Refresh, /DISABLE
    
    
;    (*self.plotcurve).putData, TRANSPOSE([[xTicks],[coefficients]]) ; Plot
    (*self.plotcurve).putData, xcoefficients ; Barplot
    
    (*self.plotcurve).yRange = [minCoef*1.05,maxCoef*1.05]

;    (*self.barplotAxis).Location = [0.,0.]
    
    IF ISA(index1) THEN (*self.coefplot1).putData, TRANSPOSE([[-1,xTicks[selectedpredictors[index1]]], [0,xcoefficients[selectedpredictors[index1]]]]) ; -1 because if only 1 point to change
    IF ISA(index2) THEN (*self.coefplot2).putData, TRANSPOSE([[-1,xTicks[selectedpredictors[index2]]], [0,xcoefficients[selectedpredictors[index2]]]]) ; putData will reform array and plot
    IF ISA(index3) THEN (*self.coefplot3).putData, TRANSPOSE([[-1,xTicks[selectedpredictors[index3]]], [0,xcoefficients[selectedpredictors[index3]]]]) ; points at different variable
    IF ISA(index4) THEN (*self.coefplot4).putData, TRANSPOSE([[-1,xTicks[selectedpredictors[index4]]], [0,xcoefficients[selectedpredictors[index4]]]])
  
  ENDELSE
    
  (*self.plotCoefficients).Refresh

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelObj::cleanup

  ON_ERROR,2

  IF PTR_VALID(self.modelStructure) THEN autoPLSR_cleanup, *self.modelStructure

  IF PTR_VALID(self.modelStructure) THEN PTR_FREE, self.modelStructure
  IF PTR_VALID(self.plotValues) THEN PTR_FREE, self.plotValues
  IF PTR_VALID(self.plotValues1) THEN PTR_FREE, self.plotValues1
  IF PTR_VALID(self.plotValues2) THEN PTR_FREE, self.plotValues2
  IF PTR_VALID(self.plotValuesCurve1) THEN PTR_FREE, self.plotValuesCurve1
  IF PTR_VALID(self.plotValuesCurve2) THEN PTR_FREE, self.plotValuesCurve2
  IF PTR_VALID(self.plotValuesLegend) THEN PTR_FREE, self.plotValuesLegend
  IF PTR_VALID(self.plotValuesText1) THEN PTR_FREE, self.plotValuesText1
  IF PTR_VALID(self.plotValuesText2) THEN PTR_FREE, self.plotValuesText2
  IF PTR_VALID(self.plotRMSE) THEN PTR_FREE, self.plotRMSE
  IF PTR_VALID(self.plotRMSE3) THEN PTR_FREE, self.plotRMSE3
  IF PTR_VALID(self.plotRMSEymax) THEN PTR_FREE, self.plotRMSEymax
  IF PTR_VALID(self.plotCoefficients) THEN PTR_FREE, self.plotCoefficients
  IF PTR_VALID(self.plotcurve) THEN PTR_FREE, self.plotcurve
  IF PTR_VALID(self.coefplot1) THEN PTR_FREE, self.coefplot1
  IF PTR_VALID(self.coefplot2) THEN PTR_FREE, self.coefplot2
  IF PTR_VALID(self.coefplot3) THEN PTR_FREE, self.coefplot3
  IF PTR_VALID(self.coefplot4) THEN PTR_FREE, self.coefplot4
  IF PTR_VALID(self.plotCoefText) THEN PTR_FREE, self.plotCoefText

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelObj__define
  struct = { autoPLSR_viewModelObj $
    , modelStructure : PTR_NEW() $
    , plotValues : PTR_NEW() $
    , plotValues1 : PTR_NEW() $
    , plotValues2 : PTR_NEW() $
    , plotValuesCurve1 : PTR_NEW() $
    , plotValuesCurve2 : PTR_NEW() $
    , plotValuesLegend : PTR_NEW() $
    , plotValuesText1 : PTR_NEW() $
    , plotValuesText2 : PTR_NEW() $
    , plotRMSE : PTR_NEW() $
    , plotRMSE3 : PTR_NEW() $
    , plotRMSEymax : 0. $
    , plotCoefficients : PTR_NEW() $
    , plotcurve : PTR_NEW() $
    , coefplot1 : PTR_NEW() $
    , coefplot2 : PTR_NEW() $
    , coefplot3 : PTR_NEW() $
    , coefplot4 : PTR_NEW() $
    , plotCoefText : PTR_NEW() $
    , barplotAxis : PTR_NEW() $
  }
END