;+
; :Hidden:
;-
PRO autoPLSR_viewModelWidget_cleanup, event

  ON_ERROR,2

  WIDGET_CONTROL, event, GET_UVALUE=modelObject
  modelObject.cleanup

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelWidget_event, event
;
;  ON_ERROR,2

  xsize = 750
  ysize = 550

  eventName = WIDGET_INFO(event.ID, /UNAME)
  
  IF STRCMP(eventName,'bgroup') THEN BEGIN

    WIDGET_CONTROL, event.TOP, GET_UVALUE=modelObject
    subBase1 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase1') 
    subBase2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase2') 
    subBase3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='subBase3') 
    draw1 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw1') 
    draw2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw2') 
    draw3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw3') 
  ;  draw4 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='draw4') 

    ; disable user interaction
    WIDGET_CONTROL, event.TOP, SENSITIVE=0
    
    IF draw1 eq 0 THEN BEGIN
      draw1 = WIDGET_WINDOW(subBase1, XSIZE=xsize, YSIZE=ysize, UNAME='draw1')
    ENDIF
  
    modelObject.plotValues, event
  
    IF draw2 eq 0 THEN BEGIN
      draw2 = WIDGET_WINDOW(subBase2, XSIZE=xsize, YSIZE=ysize, UNAME='draw2')
    ENDIF
  
    modelObject.plotRMSE, event ;NLV=event.value
  
  ;  IF draw3 eq 0 THEN BEGIN
  ;;    subsubBase3 = WIDGET_BASE(subBase3, XSIZE=1050, YSIZE=ysize, UNAME='subsubBase3', /SCROLL, X_SCROLL_SIZE=xsize)
  ;    draw3 = WIDGET_WINDOW(subBase3, XSIZE=1050, YSIZE=ysize, UNAME='draw3', /SCROLL, X_SCROLL_SIZE=xsize)
  ;    ;, SENSITIVE=0) ; 
  ;  ENDIF;
  
    modelObject.plotCoefficients, event
    
  ;  draw4 = WIDGET_WINDOW(subBase4, XSIZE=xsize, YSIZE=ysize, UNAME='draw4')
  ;  WIDGET_CONTROL, draw4, GET_VALUE=window
  ;  window.Select
  ;  
  ;  autoPLSR_plotAllCoefficients, modelStruct

    ; enable user interaction
    WIDGET_CONTROL, event.TOP, SENSITIVE=1

  ENDIF

END

;+
; :Hidden:
;-
PRO autoPLSR_viewModelWidget $
  , modelObject $
  , filenameFeatures $
  , filenameLabels $
  , ncomps $
  , nlv $
  , settings

  ON_ERROR,2

  xsize = 750
  ysize = 550

  title = settings.hubGetValue('title')+' - Model'
  base = WIDGET_BASE(/COLUMN, UNAME='base', UVALUE=modelObject, title=title)
  WIDGET_CONTROL, base

  baseRow = WIDGET_BASE(base, /ROW)
  base2 = WIDGET_BASE(baseRow, /COLUMN)

  bNames = 'LV ' + STRING(UINDGEN(ncomps) + 1, FORMAT='(I0)')
  buttonListLV = CW_BGROUP(base2, bNames, /EXCLUSIVE, LABEL_TOP='# of LV' $
    , /RETURN_INDEX, /FRAME, Y_SCROLL_SIZE=550, /NO_RELEASE, UNAME='bgroup' $
    , SET_VALUE=nlv, XSIZE=65);, EVENT_FUNCT='autoPLSR_viewModel_event') ;  , X_SCROLL_SIZE=190 YSIZE=ysize, 

  text = [ $
      'Features: ' + filenameFeatures $
    , 'Labels:   ' + filenameLabels $
;    , '# of Latent Variables: ' + STRING(nlv+1, FORMAT='(I0)') $
;    , 'RMSE: ' + 
;    , 'R²:   ' + 
  ]
  widText = WIDGET_TEXT(base, VALUE=text, YSIZE=N_ELEMENTS(text))
    
  tab = WIDGET_TAB(baseRow, XSIZE=750)
;  tab = WIDGET_TAB(base)
  subBase1 = WIDGET_BASE(tab, TITLE='Values', UNAME='subBase1')
  subBase2 = WIDGET_BASE(tab, TITLE='RMSE', UNAME='subBase2');, /SCROLL, XSIZE=750, X_SCROLL_SIZE=650)  
  subBase3 = WIDGET_BASE(tab, TITLE='Reg. Coeffs.', XSIZE=xsize, YSIZE=ysize, UNAME='subBase3')
;  subBase4 = WIDGET_BASE(tab, TITLE='Reg. Coeffs.')
  
;  draw1 = WIDGET_WINDOW(subBase1, XSIZE=xsize, YSIZE=ysize, UNAME='draw1')
;  draw2 = WIDGET_WINDOW(subBase2, XSIZE=xsize, YSIZE=ysize, UNAME='draw2')
;  draw3 = WIDGET_WINDOW(subBase3, XSIZE=xsize, YSIZE=ysize, UNAME='draw3')
;;  draw4 = WIDGET_WINDOW(subBase4, XSIZE=xsize, YSIZE=ysize, UNAME='draw4')

  
  WIDGET_CONTROL, base, /REALIZE
  
  XMANAGER, 'autoPLSR_viewModelWidget', base, CLEANUP='autoPLSR_viewModelWidget_cleanup', /NO_BLOCK
  
  event = { $
      value : nlv $
    , top : base $
    , ID : buttonListLV $
  }
  
  autoPLSR_viewModelWidget_event, event

END

;+
; :Description:
;    Use this function to view the plots and to 
;    show the specific report of an autoPLSR model.
;
; :Params:
;    parameters : in, required, type=hash
;      The parameters for the autoPLSR.
;
;    settings : in, type=hash
;
;-
PRO autoPLSR_viewModel, parameters, settings

  ON_ERROR,2

  IF parameters.HasKey('modelFilename') THEN modelFilename = parameters['modelFilename'] $
    ELSE MESSAGE, "Parameters didn't contain model filename!!"
    
  IF FILE_TEST(modelFilename, /REGULAR) THEN BEGIN
    saveObj = IDL_Savefile(modelFilename)
    names = saveObj.Names()
    struct = saveObj.Contents()
    IF STRCMP(struct.description, 'autoPLSR-Model', /FOLD_CASE) THEN BEGIN
      saveObj.Restore, names
      saveObj.Cleanup
    ENDIF ELSE BEGIN
      saveObj.Cleanup
      MESSAGE, 'File is not a valid autoPLSR-Model!!'
    ENDELSE
  ENDIF ELSE MESSAGE, 'Model filename is not valid!!'

  if parameters['reportShow'] then begin
    autoPLSR_showReport, reportHash, settings, /SHOW
  endif

  filenamepredictors = modelStruct.filenamepredictors
  filenameresponse = modelStruct.filenameresponse
  ncomps = (*modelStruct.model).numberOfComponents
  nlv = modelStruct.numberOfLatentVariable

  modelObj = autoPLSR_viewModelObj(modelStruct)

  autoPLSR_viewModelWidget $
    , modelObj $
    , filenamepredictors $
    , filenameresponse $
    , ncomps $
    , nlv $
    , settings

END