;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program.
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION autoPLSR_viewModel_getParameters $
  , settings

  ON_ERROR,2

  groupLeader = settings.hubGetValue('groupLeader')
  
;  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - View Model'

  HUBAMW_FRAME, Title='Input'
  HUBAMW_INPUTFILENAME, 'modelFilename', Title='Input Model', EXTENSION='plsr'
  hubAMW_checkbox, 'reportShow', Title='Show Report', Value=1b

  result = HUBAMW_MANAGE()

  RETURN, result

END

PRO testautoPLSR_viewModel_getParameters

  settings = autoPLSR_getSettings()
  parameters = autoPLSR_viewModel_getParameters(settings)

  if parameters['accept'] then begin
    autoPLSR_viewModel, parameters, settings
  endif

END