;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `autoPLSR_processing` function to this argument. 
;      
;    settings : in, optional, type=hash
;
;
;-
pro autoPLSR_showReport $
  , reportInfo $
  , settings $
  , FILENAME=filename $
  , SHOW=show

  report = hubReport(Title=settings['title'])
  progress = report.getProgressBar()

  report.addParagraph, reportInfo['description']

  report.addHeading, 'autoPLSR-Model'
  report.addHeading, 'Input', 2
  
  text = [ $
      ['Predictors: ' + reportInfo['Predictors']] $
    , ['Response:   ' + reportInfo['Response']] $
  ]
  report.addParagraph, text
  
  report.addHeading, 'Arguments', 2

  text = [ $
      ['Scaling data:             ' + reportInfo['scaling']] $
    , ['Brightness normalization: ' + reportInfo['dataPrep']] $
    , ['Random stratification:    ' + reportInfo['random']] $
    , ["Keep no. of LV's low:     " + reportInfo['restrict']] $
    , ['Used full set of filters: ' + reportInfo['thorough']] $
    , ['Backward selection of predictors: ' + reportInfo['no_backselect']] $
    , ['Method for cross-validation: ' + reportInfo['cvMethod']] $
  ]
  report.addParagraph, text

  report.addHeading, 'Report'
  report.addMonospace, reportInfo['report']
  
  report.addHeading, 'Regression-Model'

  text = [ $
      [' Number of samples:              ' + reportInfo['Number of samples']] $
    , [' Number of predictors (overall): ' + reportInfo['NumberPredictors']] $
    , [' Number of predictors used:      ' + reportInfo['NumberUsedPredictors']] $
    , [' Used predictors: ' + reportInfo['UsedPredictors']] $
    , [' Number of latent variables:' + reportInfo['NumberLV']] $
  ]
  report.addParagraph, text

  report.addHeading, 'Statistics', 2

  text = [ $
      ['RMSE Calibration: ' + reportInfo['RMSEcal']] $ 
    , ['RMSE Validation:  ' + reportInfo['RMSEval']] $
    , ['R2 Calibration:   ' + reportInfo['R2cal']] $
    , ['R2 Validation:    ' + reportInfo['R2val']] $
  ]
  report.addParagraph, text
  
  report.addHeading, 'Model coefficients', 2
  report.addParagraph, reportInfo['coefficients']

  report.addHeading, 'Intercept', 2
  report.addParagraph, reportInfo['intercept']

  report.addHeading, 'Predicted', 2
  report.addParagraph, reportInfo['predicted']

;  report.addImage, reportInfo['image'], 'autoPLSR image caption'
  
  report.saveHTML, filename, SHOW=show
  report.cleanup

end
