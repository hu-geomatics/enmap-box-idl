;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input
;    via a widget program. The user input is passed to the application processing routine.
;    Finally, all results are presented in a HTML report (see `autoPLSR_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro autoPLSR_apply_application, applicationInfo
  
  ; get global settings for this application
  settings = autoPLSR_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = autoPLSR_apply_getParameters(settings)
  
  if parameters['accept'] then begin
  
    autoPLSR_apply_imageprocessing, parameters, settings
  
  endif
  
end