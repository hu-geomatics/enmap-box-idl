;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION autoPLSR_apply_getParameters $
  , settings

  ON_ERROR,2

  groupLeader = settings.hubGetValue('groupLeader')
  
  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - Apply model to image data'

  HUBAMW_FRAME, Title='Input'
  HUBAMW_INPUTFILENAME, 'modelFilename', Title='Input Model', EXTENSION='plsr'
  HUBAMW_INPUTSAMPLESET, 'inputFilename', Title='Input Image', /Masking, /ReferenceOptional
  
  HUBAMW_FRAME, Title='Output'
  HUBAMW_OUTPUTFILENAME, 'outputFilename', Title='Output image' ; , Value=''
  result = HUBAMW_MANAGE() ; ConsistencyCheckFunction='_getParameters_consistencyCheck'

  RETURN, result

END

PRO testautoPLSR_apply_getParameters

  settings = autoPLSR_getSettings()
  parameters = autoPLSR_apply_getParameters(settings)
;  IF parameters.HasKey('imageData') THEN BEGIN
;    print, (parameters['imageData'])
;    print, (parameters['imageData'])['labelFilename']
;    print, (parameters['imageData'])['featureFilename']
;  ENDIF ELSE print, parameters
;  print, parameters

  if parameters['accept'] then begin
    autoPLSR_apply_imageprocessing,parameters, settings  
  endif

END