; original R source code by Sebastian Schmidtlein
;
; adapted to IDL by Carsten Oldenburg
;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application processing function. All parameters are
;    checked for validity.
;
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, required, type=hash
;    
;-
PRO autoPLSR_apply_imageprocessing, parameters, settings

  ON_ERROR,2

  IF parameters.HasKey('inputFilename') THEN BEGIN
    maskFilename = (parameters['inputFilename'])['labelFilename']
    inputFilename = (parameters['inputFilename'])['featureFilename']
  ENDIF ELSE MESSAGE, "Parameters didn't contain input filename!!"

  IF parameters.HasKey('modelFilename') THEN modelFilename = parameters['modelFilename'] $
    ELSE MESSAGE, "Parameters didn't contain filename for model!!
  IF parameters.HasKey('outputFilename') THEN outputFilename = parameters['outputFilename'] $
    ELSE MESSAGE, "Parameters didn't contain filename for output image!!

  IF FILE_TEST(modelFilename, /REGULAR) THEN BEGIN
    saveObj = IDL_Savefile(modelFilename)
    names = saveObj.Names()
    struct = saveObj.Contents() 
    IF STRCMP(struct.description, 'autoPLSR-Model', /FOLD_CASE) THEN BEGIN
      saveObj.Restore, names
      saveObj.Cleanup
    ENDIF ELSE BEGIN
      saveObj.Cleanup
      MESSAGE, 'File is not a valid autoPLSR-Model!!'
    ENDELSE
  ENDIF

  inputImage = hubioimginputimage(inputFilename)
  IF ISA(maskFilename) THEN maskImage = hubioimginputimage(maskFilename)
  outputImage = hubioimgoutputimage(outputFilename)
  
  title = settings.hubGetValue('title')+' - Applying model'
  info = 'Wait...'
  progressBar = hubProgressBar(Title=title, Info=info $
    , GroupLeader=settings.hubGetValue('groupLeader'))
  progressBar.setRange, [0,inputImage.getMeta('lines')]
  counter = 0

  selectedPredictors = WHERE(modelStruct.selectedPredictors, countPredictors)
  coefficients = (*modelStruct.model).coefficients[*,modelStruct.numberoflatentvariable]
  intercept = (*modelStruct.model).intercept[modelStruct.numberoflatentvariable]

  ; scale data if model is scaled
  IF KEYWORD_SET(modelStruct.scale) THEN coefficients = coefficients / *modelStruct.xStdDev
  
  ; TILED Subset 
  ;--------------

  samples = inputImage.getMeta('samples')
  lines = inputImage.getMeta('lines')
  datatype = inputImage.getMeta('data type')
  outputImage.copyMeta, inputImage, /CopySpatialInformation
  nb = inputImage.getMeta('bands')
  
  IF modelStruct.xdim[1] ne nb THEN MESSAGE, 'Number of bands do not fit!!'

  CASE 1 OF
    datatype eq 1 : bytes = 1
    (datatype eq 2) OR (datatype eq 12) : bytes = 2
    (datatype eq 3) OR (datatype eq 13) OR (datatype eq 4) : bytes = 4
    (datatype eq 14) OR (datatype eq 15) OR (datatype eq 5) : bytes = 8
    ELSE : MESSAGE, 'Data type is not valid!!'
  ENDCASE

  numberOfTileLines = FLOOR(50e4 / (samples * countPredictors * bytes))
  IF numberOfTileLines le 0 THEN numberOfTileLines = 1
  numberOfTiles = CEIL(FLOAT(lines) / numberOfTileLines)
  inputImage.initReader, numberOfTileLines, /SLICE, /TileProcessing $
    , SubsetBandPositions=selectedPredictors

  ; set ignore value and init mask
  IF OBJ_VALID(maskImage) THEN BEGIN
    outputImage.setMeta, 'data ignore value', -9999
    maskImage.initReader, numberOfTileLines, /Slice, /TileProcessing, /Mask
  ENDIF ELSE BEGIN
    unmasked = lindgen(samples)
  ENDELSE

  writerSettings = inputImage.getWriterSettings(SetBands=1, SetDataType=5)
  outputImage.initWriter, writerSettings
  
  WHILE ~ inputImage.tileProcessingDone() DO BEGIN
    bandTileData = inputImage.getData()
    
    IF OBJ_VALID(maskImage) THEN BEGIN
      maskTileData = maskImage.getData()
      unmasked = WHERE(maskTileData, COMPLEMENT=maskIndices,/NULL)
    ENDIF      

    IF KEYWORD_SET(modelStruct.prep) and isa(unmasked) $
      THEN bandTileData[unmasked] = transpose(autoPLSR_preprocessing(transpose(bandTileData[unmasked])))

    ;-------------
    ; Apply Model
    ;-------------    
    predicted = coefficients # bandTileData + intercept
    
    IF ISA(maskIndices) THEN predicted[maskIndices] = -9999
    
    outputImage.writeData, predicted
    counter = counter + numberOfTileLines
    progressBar.setProgress, counter
  ENDWHILE
  
  ; cleanup
  outputImage.finishWriter
  inputImage.finishReader

  autoPLSR_cleanup, modelStruct
  inputImage.cleanup
  outputImage.cleanup
  IF OBJ_VALID(maskImage) THEN BEGIN
    maskImage.finishReader
    maskImage.cleanup
  ENDIF
  progressBar.cleanup

END