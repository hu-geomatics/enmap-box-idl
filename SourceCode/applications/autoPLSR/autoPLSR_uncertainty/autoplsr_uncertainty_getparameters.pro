;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program.
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION autoPLSR_uncertainty_getParameters $
  , settings

  ON_ERROR,2

  groupLeader = settings.hubGetValue('groupLeader')
  
;  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - Uncertainty'

  HUBAMW_FRAME, Title='Input'
  HUBAMW_INPUTFILENAME, 'modelFilename', Title='Input model  ', EXTENSION='plsr'
;  HUBAMW_INPUTIMAGEFILENAME, 'prediction', Title='Input image  '
  HUBAMW_INPUTSAMPLESET, 'inputFilename', Title='Input image  ', /Masking, /ReferenceOptional

  HUBAMW_FRAME, Title='Output'
  HUBAMW_OUTPUTFILENAME, 'outputFilename', Title='Output image ' ; , Value=''

;  hubAMW_checkbox, 'reportShow', Title='Show Report', Value=1b


  result = HUBAMW_MANAGE()

  RETURN, result

END

PRO testautoPLSR_uncertainty_getParameters

  settings = autoPLSR_getSettings()
  parameters = autoPLSR_uncertainty_getParameters(settings)
;  IF parameters.HasKey('imageData') THEN BEGIN
;    print, (parameters['imageData'])
;    print, (parameters['imageData'])['labelFilename']
;    print, (parameters['imageData'])['featureFilename']
;  ENDIF ELSE print, parameters
;  print, parameters

  if parameters['accept'] then begin
  
    autoPLSR_uncertainty, parameters, settings
    
  endif
END