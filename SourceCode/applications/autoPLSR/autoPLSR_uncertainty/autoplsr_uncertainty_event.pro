;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro autoPLSR_uncertainty_event, event
  
  ; set up a default error handler
  
  @huberrorcatch
  
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  
  ; call the application main procedure
  ; if required, provide menu button information to it
 
  autoPLSR_uncertainty_application, applicationInfo
  
end