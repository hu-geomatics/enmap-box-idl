;+
; :Author: Carsten Oldenburg
;-
;+
; :Description:
;    Use this function to compute the uncertainty 
;    of an autoPLSR predicted image.
;
; :Params:
;    parameters : in, required, type=hash
;      The parameters for the autoPLSR.
;
;    settings : in, type=hash
;
;-
PRO autoPLSR_uncertainty, parameters, settings

  ON_ERROR,2

  IF parameters.HasKey('modelFilename') THEN modelFilename = parameters['modelFilename'] $
    ELSE MESSAGE, "Parameters didn't contain model filename!!"
  IF parameters.HasKey('inputFilename') THEN BEGIN
    maskFilename = (parameters['inputFilename'])['labelFilename']
    predFname = (parameters['inputFilename'])['featureFilename']
  ENDIF
  IF parameters.HasKey('outputFilename') THEN outputFilename = parameters['outputFilename']
    
  IF FILE_TEST(modelFilename, /REGULAR) THEN BEGIN
    saveObj = IDL_Savefile(modelFilename)
    names = saveObj.Names()
    struct = saveObj.Contents()
    IF STRCMP(struct.description, 'autoPLSR-Model', /FOLD_CASE) THEN BEGIN
      saveObj.Restore, names
      saveObj.Cleanup
    ENDIF ELSE BEGIN
      saveObj.Cleanup
      MESSAGE, 'File is not a valid autoPLSR-Model!!'
    ENDELSE
  ENDIF ELSE MESSAGE, 'Model filename is not valid!!'

  inputImage = hubIOImgInputImage(predFname)
  IF ISA(maskFilename) THEN maskImage = hubioimginputimage(maskFilename)
  outputImage = hubioimgoutputimage(outputFilename)
  
  nBands = inputImage.getMeta('bands')
  samples = inputImage.getMeta('samples')
  lines = inputImage.getMeta('lines')
  xDims = size(modelStruct.x,/DIMENSIONS)
  
  IF ( nBands ne 1 ) and ( nBands ne xDims[1] ) $
    THEN MESSAGE, 'Image has too many bands, must be one for a predicted image!!'
  
  IF ( nBands eq 1 ) THEN BEGIN
    predicted = 1
    selectedPredictors = 1
  ENDIF ELSE BEGIN
    predicted = 0
    
    selectedPredictors = WHERE(modelStruct.selectedPredictors, countPredictors)
    coefficients = (*modelStruct.model).coefficients[*,modelStruct.numberoflatentvariable]
    intercept = (*modelStruct.model).intercept[modelStruct.numberoflatentvariable]

    ; scale data if model is scaled
    IF KEYWORD_SET(modelStruct.scale) THEN coefficients = coefficients / *modelStruct.xStdDev
  ENDELSE
  
  title = settings.hubGetValue('title')+' - Computing uncertainty'
  info = 'Wait...'
  progressBar = hubProgressBar(Title=title, Info=info $
    , GroupLeader=settings.hubGetValue('groupLeader'))
  progressBar.setRange, [0,inputImage.getMeta('lines')]
  counter = 0
  
    ; set ignore value and init mask
  IF OBJ_VALID(maskImage) THEN BEGIN
    outputImage.setMeta, 'data ignore value', -9999
    maskImage.initReader, 1, /Slice, /TileProcessing, /Mask
  ENDIF
  
  inputImage.initReader, 1, /SLICE, /TileProcessing $
    , SubsetBandPositions=selectedPredictors
  writerSettings = inputImage.getWriterSettings(SetBands=1, SetDataType=4)
  outputImage.initWriter, writerSettings

  original = modelStruct.y
  nObs = N_ELEMENTS(original)
  
  WHILE ~ inputImage.tileProcessingDone() DO BEGIN
    bandTileData = inputImage.getData()
    
    IF OBJ_VALID(maskImage) THEN BEGIN
      maskTileData = maskImage.getData()
      !NULL = WHERE(maskTileData, COMPLEMENT=maskIndices)
    ENDIF 
    
    IF ( ~ predicted ) THEN BEGIN
      IF KEYWORD_SET(modelStruct.prep) $
        THEN bandTileData = transpose(autoPLSR_preprocessing(transpose(bandTileData)))  
      bandTileData = coefficients # bandTileData + intercept
    ENDIF
    
    diff = abs( rebin(bandTileData, nObs, samples) $
      - rebin(original, nObs, samples) )
    minDiff = min(diff, DIMENSION=1)
    
    IF ISA(maskIndices) THEN minDiff[maskIndices] = -9999
    
    outputImage.writeData, minDiff
    counter++
    progressBar.setProgress, counter
  ENDWHILE
  
  ; cleanup
  outputImage.finishWriter
  inputImage.finishReader

  autoPLSR_cleanup, modelStruct

  inputImage.cleanup
  outputImage.cleanup
  IF OBJ_VALID(maskImage) THEN BEGIN
    maskImage.finishReader
    maskImage.cleanup
  ENDIF
  progressBar.cleanup  

END