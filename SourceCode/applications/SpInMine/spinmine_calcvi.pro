;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)
;-

;+
; :Description:
;    Calculates Spectral Indices. Avoids dividing by zero (outputs zero instead).
;
; :Params:
;     b1 : in, required
;       spectral values band 1
;       
;     b2 : in, required
;       spectral values band 2
;       
;     type : in, required
;       Code number of spectral index to be calculated (0-difference, 1-normalized diff., 2-ratio). 
;      
;    settings : in, optional, type=hash
;
;
;-
function SpInMine_calcVI, b1, b2, type
  case type of 
     ; 'Difference' : begin
     0: begin
      VI=float(b1)-float(b2);
      end
      ;'Normalized Difference' : begin
      1: begin
        VI = fltarr(N_elements(b2))
        index = where((float(b1)+float(b2)) ne 0)
        VI[index]=(float(b1[index])-float(b2[index]))/(float(b1[index])+float(b2[index]));
      end
      ;'Ratio' : begin
      2: begin
        VI = fltarr(N_elements(b2))
        index = where(b2 ne 0)
        VI[index] = float(b1[index])/float(b2[index]);
      end
  endcase
  return, VI
end