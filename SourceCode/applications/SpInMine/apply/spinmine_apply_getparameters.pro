;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)

;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function spinmine_apply_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'SpInMine ' + settings['title']

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Input'
  defaultModelFilename = hub_getAppState('spinMine', 'modelFilename')
  hubAMW_inputFilename ,      'fn_model'     ,Title='Model', Value=defaultModelFilename
  hubAMW_inputImageFileName , 'fn_InputImage' ,Title='Image'
    
    
  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputFileName, 'fn_OutputImage', Title = 'Result Image'
  hubAMW_parameter, 'NoDataValue', Title='No Data Value', Value=-1, /Integer
  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b

  parameters = hubAMW_manage()

  ; if required, perform some additional changes on the parameters hash
  
  return, parameters
end

;+
; :Hidden:
;-
pro test_apply_getParameters

  ; test your routine
  settings = spinmine_getSettings()
  parameters = spinmine_apply_getParameters(settings)
  print, parameters

end  
