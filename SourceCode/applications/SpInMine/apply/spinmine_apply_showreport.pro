;+

;-

;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `apply_processing` function to this argument. 
;      
;    settings : in, optional, type=hash
;
;
;-
pro spinmine_apply_showReport, reportInfo, settings

  ;model=read_csv(parameters['fn_model'])
  ;model_type=model.field1[0]
  ;model_band1=model.field1[1]
  ;model_band2=model.field1[2]
  ;model_coef1=model.field1[3]
  ;model_coef2=model.field1[4]
  index_type    = fix((reportInfo['model']).field1[0])
  bestacc_line = fix((reportInfo['model']).field1[1])
  bestacc_col  = fix((reportInfo['model']).field1[2])

  report = hubReport(Title=settings['title'])

  report.addHeading, 'SpInMine (Spectral Index Data Mining Tool) Apply Report'


  report.addHeading, 'Input',3
  report.addParagraph, 'Input Image: ' + reportInfo['fn_InputImage']
  report.addParagraph, 'Model File: ' + reportInfo['fn_model']
  
  report.addHeading, 'Output',3
  report.addParagraph, 'Output Image: ' + reportInfo['fn_OutputImage']
  
  report.addHeading, 'Model Used', 3
  report.addParagraph, 'Index Type: ' + (settings['index_types'])[index_type]
  
  report.addParagraph, 'Band 1:' + strtrim(bestacc_line)
  report.addParagraph, 'Band 2:' + strtrim(bestacc_col)
  ;report.addParagraph, 'Best Accuracy: ' +strtrim(reportInfo['bestacc_acc'])
  
  report.saveHTML, /Show

end
