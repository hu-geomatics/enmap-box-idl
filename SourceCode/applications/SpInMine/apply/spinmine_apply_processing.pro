;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)
;-

;+
; :Description:
;    Reads a SpInMine-Model from Spinmine/parameterize.
;    Applies Model to Input Image, Returns Output Image
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
function spinmine_apply_processing, parameters, settings

  ; check required parameters
  model=read_csv(parameters['fn_model'])
  model_type=model.field1[0]
  model_band1=model.field1[1]
  model_band2=model.field1[2]
  model_coef1=model.field1[3]
  model_coef2=model.field1[4]
  
  inputImage  = hubIOImgInputImage( parameters['fn_InputImage'])
  outputImage = hubIOImgOutputImage(parameters['fn_OutputImage'])

  ; prepare Output Image (Tile processing)
  tileLines = 100
  inputImage.initReader, tileLines, /TileProcessing, /Slice $
    , SubsetBandPositions=[model_band1, model_band2] , DataType='float'
  outputImage.copyMeta, inputImage, /CopySpatialInformation
  outputImage.setMeta, 'file type', 'EnMAP-Box Regression'   
  outputImage.setMeta, 'data ignore value', parameters['NoDataValue']
  outputImage.initWriter, inputImage.getWriterSettings(SetBands=1);
  
  ; calculate the Index Value
  while ~inputImage.tileProcessingDone() do begin
    data = inputImage.getData()
    ; Calculate Index in Tile Processing    
    VIvalues = SpInMine_calcVI(data[0,*], data[1,*], model_type)
    ; Transform VI image to Variable of interest
    VarValues = model_coef1 + model_coef2 * VIvalues
    outputImage.writeData, VarValues
  endwhile
  
  inputImage.cleanup
  outputImage.cleanup
  
  ; store results to be reported inside a hash
  result = hash() ;empty hash
  ;result += parameters.hubGetSubHash(['fn_InputImage','fn_OutputImage'])
  result['fn_InputImage'] = parameters['fn_InputImage']
  result['fn_OutputImage'] = parameters['fn_OutputImage']
  result['fn_model']=parameters['fn_model']
  result['model'] = model
  return, result

end

;+
; :Hidden:
;-
pro test_apply_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  parameters['fn_model'] = filepath(/TMP, 'SpInMine_Model.spm')
  parameters['fn_InputImage'] = hub_getTestImage('Hymap_Berlin-B_Image')
  parameters['fn_OutputImage'] = filepath(/TMP, 'SpInMine_test')
  parameters['spinmine_acc']=1
  parameters['NoDataValue']=-1
  settings = spinmine_getSettings()
  
  result = spinmine_apply_processing(parameters, settings)
  print, result

end  
