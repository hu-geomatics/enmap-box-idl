;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de), Andreas Rabe (janzandr@hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `apply_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `apply_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `apply_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro spinmine_apply_application, applicationInfo
  
  ; get global settings for this application
  settings = spinmine_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = spinmine_apply_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = spinmine_apply_processing(parameters, settings)
    
    if parameters['showReport'] then begin
      spinmine_apply_showReport, reportInfo, settings
    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_apply_application
  applicationInfo = Hash()
  spinmine_apply_application, applicationInfo
end  
