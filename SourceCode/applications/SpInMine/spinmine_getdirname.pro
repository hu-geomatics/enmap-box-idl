;+

;-

function SpInMine_getDirname, SourceCode=sourceCode
  
  result = filepath('SpInMine', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result
  
end

pro test_SpInMine_getDirname

  print, SpInMine_getDirname()
  print, SpInMine_getDirname(/SourceCode)

end
