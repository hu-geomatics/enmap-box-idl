;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)
;-

;+
; :Description:
;    This is the main procedure for parameterizing a SpInMine (Spectral Index Data Mining Tool) model. 
;    It queries user input (see `parameterize_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `parameterize_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `parameterize_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro spinmine_parameterize_application, applicationInfo
  
  ; get global settings for this application
  settings = spinmine_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = spinmine_parameterize_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = spinmine_parameterize_processing(parameters, settings)
    
    if parameters['showReport'] then begin
      spinmine_parameterize_showReport, reportInfo, settings
    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_parameterize_application
  applicationInfo = Hash()
  spinmine_parameterize_application, applicationInfo

end  
