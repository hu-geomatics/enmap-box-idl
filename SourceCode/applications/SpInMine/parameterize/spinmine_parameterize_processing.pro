;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)
;-

;+
; :Description:
;    SpInMine (Spectral Index Data Mining Tool):
;    Calculates spectral index (Difference, Normalized Difference or Ratio; see `spinmine_calcvi`) with all possible
;    spectral band combinations. Finds best index for estimating variable of interest according
;    to accuracy measure (R², RMSE or relative RMSE; see `spinmine_calcacc`).
;    
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
function spinmine_parameterize_processing, parameters, settings
  ; check required parameters

  
  ; create the input and output image objects
  sampleSet = hubIOImgSampleSetForRegression(parameters['fn_image'],parameters['fn_labels'])
     
   ; query pixel locations
  featureIndices = sampleSet.getIndices()
  features = sampleSet.getFeatures(featureIndices)
  data = sampleSet.getLabels(featureIndices)
  numberOfBands = sampleSet.getFeatureMeta('bands')
  numberOfFeatures = N_elements(featureindices)
  
  ;create a progressBar
  progressBar = hubProgressBar(Title=settings.hubGetValue('title') $
    ,GroupLeader = settings.hubGetValue('groupLeader'))
  progressBar.setInfo, 'Finding optimal index...'
  progressBar.setRange, [0, numberOfBands]
  ;progressDone = 0 
  
  ; Calculate Accuracy Matrix
  accuracy_matrix = fltarr(numberOfBands,numberOfBands)
  VI = fltarr(numberOfFeatures)
  FOR xx=0,numberOfBands-1 DO BEGIN
     FOR yy=0,numberOfBands-1 DO BEGIN      
        IF xx ne yy then begin ; don't calculate on diagonal
          VI = SpInMine_calcVI(features[xx,*], features[yy,*], parameters['spinmine_type'])
          predictions = SpInMine_calcPredictions(VI, data, Folds=0) ;
          accuracy_matrix [xx,yy] = SpInMine_calcACC(predictions, data, parameters['spinmine_acc'])
        ENDIF 
     ENDFOR
     progressDone = xx
     progressBar.setProgress, progressDone 
  ENDFOR
  
  ; Find Column and Line of best Accuracy
  case parameters['spinmine_acc'] of
    0: begin
      bestacc_index = where(accuracy_matrix eq max(accuracy_matrix), /NULL)
      bestacc_val = max(accuracy_matrix)
    end
    else: begin ; Find Min of acc., exclude zeros
      notnull =  where(accuracy_matrix eq 0)
      accuracy_matrix_notnull=accuracy_matrix
      accuracy_matrix_notnull[notnull] = max(accuracy_matrix)
      bestacc_index = where(accuracy_matrix_notnull eq min(accuracy_matrix_notnull), /NULL)
      bestacc_val = min(accuracy_matrix_notnull)
      accuracy_matrix = accuracy_matrix_notnull
    end
    
  endcase
  bestacc_line = bestacc_index/numberOfBands
  bestacc_col  = bestacc_index mod numberOfBands
  bestacc_VIvalues = SpInMine_calcVI(features[bestacc_line[0],*], features[bestacc_col[0],*], parameters['spinmine_type'])
  
  ; Regression
  noinf = where(finite(bestacc_VIvalues))
  regr_coeff = linfit(bestacc_VIvalues[noinf], data[noinf], YFIT=YFIT)
  
  ;inputImage = hubIOImgInputImage(parameters['inputImage'])
  ;outputImage = hubIOImgOutputImage(parameters['outputImage'])
  
  ; Additional Plots
  IF parameters['showAddGraphics'] then begin
    font=9
    meanfeat=mean(features, dimension=2)
    plotmax=max(meanfeat)*1.05
    bandindices = indgen(numberofbands)
    b1=bestacc_col[0]
    b2=bestacc_line[0]
    w=window(dimensions=[900,815], window_title='SpInMine Plots')
    pl1=plot(bandindices,meanfeat,/current, position=[0.35, 0.06, 0.90, 0.3], font_size=font, yrange=[0, plotmax], xrange=[0,numberOfBands], '-', xtitle='Band Number')
    ;pl1=plot(bandindices,features[*,0],/current, position=[0.35, 0.06, 0.90, 0.3], font_size=font, yrange=[0, max(features)], '-', xtitle='Band Number')
    ;for xx=1,numberOfFeatures-1 DO pl1=plot(bandindices,features[*,xx],/Overplot, font_size=font, '-')
    pl11=plot([b1,b1], [0, plotmax],'r2-',/Overplot, font_size=font)
    pl12=plot([b2,b2], [0, plotmax],'r2-',/Overplot, font_size=font)
    pl2=plot(meanfeat,bandindices, position=[0.06, 0.35, 0.30, 0.95], /current, font_size=font, xrange=[0, plotmax], yrange=[0,numberOfBands], '-', ytitle='Band Number')
    ;pl2=plot(features[*,0],bandindices, position=[0.06, 0.35, 0.30, 0.95], /current, font_size=font, xrange=[0, max(features)], '-', ytitle='Band Number')
    ;for xx=1, numberOfFeatures-1 DO pl1=plot(features[*,xx], bandindices, /Overplot, font_size=font, '-')
    pl21=plot([0, plotmax], [b1,b1], 'r2-',/Overplot, font_size=font)
    pl22=plot([0, plotmax], [b2,b2], 'r2-',/Overplot, font_size=font)
    titletext=(settings['acc_types'])(parameters['spinmine_acc'])+' Plot of '+(settings['index_types'])(parameters['spinmine_type'])+' indices'
    im=image(accuracy_matrix, position=[0.35, 0.35, 0.90, 0.95], /current, font_size=font+2, title=titletext)
    pol=polygon([b1-1, b1-1,b1+1, b1+1],[b2-1, b2+1, b2+1, b2-1],'r2-',/data, target=im, FILL_BACKGROUND=0)
    cb=colorbar(target=im, position=[0.9, 0.35, 0.93, 0.95], orientation=1, font_size=font, textpos=1)
    p4=plot( bestacc_VIvalues, data,'db', position=[0.06, 0.06, 0.3, 0.3], font_size=font, /current, ytitle='Variable', xtitle='Index')
    p4r=plot(bestacc_VIvalues, YFIT, 'r-',  font_size=font, /overplot)
    t=text(0.075, 0.1, 'y = ' + strtrim(string(regr_coeff[0], format='(F10.4)')) + strtrim(string(regr_coeff[1], format='(F+10.4)'))+' x', font_size=font)
    t=text(0.075, 0.08, (settings['acc_types'])(parameters['spinmine_acc'])+' = '+string(bestacc_val, format='(F9.3)'), font_size=font)
  
    img = w.Read()
    img.getProperty, Data=imgData
    imgData = transpose(imgData, [1,2,0])
    w.close
  
  ENDIF
  
  ;cleanup objects
  ;inputImage.cleanup
  progressBar.cleanup
  
  ; Prepare Output
  result = hash()
  
 ;result['description']     = 'SpInMine Result Text'
  result['bands']           = numberOfBands
  result['accuracy_matrix'] = accuracy_matrix
  result['bestacc_line']    = bestacc_line
  result['bestacc_col']     = bestacc_col
  result['bestacc_acc']     = accuracy_matrix[bestacc_col, bestacc_line]
  result['spinmine_acc']    = parameters['spinmine_acc']
  result['spinmine_type']   = parameters['spinmine_type']
  result['regr_coeff']      = regr_coeff
  result['bestacc_VIvalues']= bestacc_VIvalues
  result['data']            = data
  if isa(imgData) then begin
    result['AddGraphic'] = imgData
  endif 
  result['fn_model']        = parameters['fn_model']
  result['fn_image']        = parameters['fn_image']
  result['fn_labels']       = parameters['fn_labels']
  
 ; scatter=plot(data, bestacc_VIvalues, 'sb')
 ; regrplot=plot(data, regr_coeff[0]+regr_coeff[1]*data, '-r', OVERPLOT=1)
  
  modelhash=[parameters['spinmine_type'], bestacc_line[0], bestacc_col[0], regr_coeff[0],regr_coeff[1] ]
  write_csv, parameters['fn_model'], modelhash
  ;result['modelhash'] = modelhash
  return, result
end

;+
; :Hidden:
;-
pro test_parameterize_processing

  ; test the routine with example data
  parameters=hash()
  parameters['fn_image'] = hub_getTestImage('Hymap_Berlin-B_Image')
  parameters['fn_labels'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
  parameters['spinmine_type']=1
  parameters['spinmine_acc']=0
  parameters['showReport']=1
  parameters['showAddGraphics']=1
  parameters['fn_model']='C:\Users\buddenba\AppData\Local\Temp\SpInMine_Model.spm'
  settings = spinmine_getSettings()
  
  result = spinmine_parameterize_processing(parameters, settings)
  print, result

end  
