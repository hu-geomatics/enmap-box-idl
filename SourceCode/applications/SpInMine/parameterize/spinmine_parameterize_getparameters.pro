;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function spinmine_parameterize_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'Settings for ' + settings['title']

  ; frame for input options & parameters
  ; defaultValue = hash()
  ; defaultValue['featureFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
  ; defaultValue['regressionSample'] = hub_getTestImage('Hymap_Berlin-B_Image')
  ; defaultValue['labelFilename'] =   hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputSampleset, 'regressionSample', Title='Feature Image' $
    , /Regression, ReferenceTitle='Reference Areas'

    
  hubAMW_frame, title = 'Parameters'
  hubAMW_combobox, 'spinmine_type', Title='Spectral Index Type', List=settings['index_types']; List=['Difference', 'Normalized Difference', 'Ratio']
  hubAMW_combobox, 'spinmine_acc' , Title='Accuracy Measure'   , List=settings['acc_types']
  
  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputFilename,'fn_model', Title= 'SpInMine Model', EXTENSION='spm', VALUE='SpInMine_Model.spm'
  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1
  hubAMW_checkbox, 'showAddGraphics', Title='Show Additional Graphics', Value=0

  parameters = hubAMW_manage()
  
  if parameters['accept'] then begin
    ; if required, perform some additional changes on the parameters hash
    parameters['fn_image']  = (parameters['regressionSample'])['featureFilename']
    parameters['fn_labels'] = (parameters['regressionSample'])['labelFilename']
    parameters.remove, 'regressionSample'
    
  endif
  
  return, parameters
end

;+
; :Hidden:
;-
pro test_parameterize_getParameters

  ; test your routine
  settings = spinmine_getSettings()
  parameters = spinmine_parameterize_getParameters(settings)
  print, parameters

end  
