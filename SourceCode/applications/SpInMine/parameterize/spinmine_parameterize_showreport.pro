;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)

;-

;+
; :Description:
;    Report about SpInMine Model Parameterization.
;    Outputs relevant Information on Model in HTML report.
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `parameterize_processing` function to this argument. 
;      
;    settings : in, optional, type=hash
;
;
;-
pro spinmine_parameterize_showReport, reportInfo, settings
  report = hubReport(Title=settings['title'])
  
  ;report.addParagraph, reportInfo['description']
  report.addHeading, 'SpInMine (Spectral Index Data Mining Tool) model calibration report',2
  
  report.addHeading, 'Input Parameters', 3
  report.addParagraph, 'Index Type: ' + (settings['index_types'])(reportInfo['spinmine_type'])
  report.addParagraph, 'Accuracy Measure: ' + (settings['acc_types'])(reportInfo['spinmine_acc'])
  report.addParagraph, 'Number of Image bands considered: ' + strtrim(reportInfo['bands'])
  report.addParagraph, 'Input Image Filename: ' + reportInfo['fn_image']
  report.addParagraph, 'Labels Filename: ' + reportInfo['fn_labels']
  
  report.addHeading, 'Output', 3
  report.addParagraph, 'Model File Name: ' + reportInfo['fn_model']
  
  report.addHeading, 'Optimal Band Combination(s)', 3
  report.addParagraph, 'Band 1:' + strtrim(reportInfo['bestacc_line'])
  report.addParagraph, 'Band 2:' + strtrim(reportInfo['bestacc_col'])
  report.addParagraph, 'Best Accuracy: ' + (settings['acc_types'])(reportInfo['spinmine_acc'])+ ' = ' +strtrim(reportInfo['bestacc_acc'])
  report.addParagraph, 'Regression equation: Var = ' + strtrim((reportInfo['regr_coeff'])[0]) + ' + ' + strtrim((reportInfo['regr_coeff'])[1]) + ' * Index'

  if reportInfo.hubIsa('AddGraphic') then begin
    report.addImage, reportInfo['AddGraphic'], 'Map of accuracy measure'
  endif else begin

  scaled_image=255*reportInfo['accuracy_matrix']/max(reportInfo['accuracy_matrix'])
  report.addImage, byte(scaled_image), 'Map of accuracy measure'
  
  report.addHeading, 'Scatter plot of Index vs. Variable'
  data=reportInfo['data']
  bestacc_VIvalues=reportInfo['bestacc_VIvalues']
 ; pixmap = 1b
  window, xsize=600, ysize=400, /FREE, Pixmap=1b
  white=!D.N_COLORS-1
  black=!D.N_COLORS
  plot, data, bestacc_VIvalues, psym = 4, xtitle='Variable', ytitle='Index', background=white, color=black
  oplot, data, (reportInfo['regr_coeff'])[0]+(reportInfo['regr_coeff'])[1]*data, color=black
  ; read plot as RGB image (3D-Array)
  rgbImage = tvrd(true=3) ; 
  ;close Window
  WDELETE

  ;add RGB image to report
  report.addImage, rgbImage, 'Scatter Plot and Regression'
end
  report.saveHTML, /Show
  
end
