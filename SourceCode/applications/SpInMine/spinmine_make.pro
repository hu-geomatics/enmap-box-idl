;+
; :Private:
;-

pro spinmine_make
  
  if enmapboxmake.clean() then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    
    appDir = spinmine_getDirname()
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  codeDir = spinmine_getDirname(/SourceCode)
  targetDir = spinmine_getDirname()
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir
  
  if not enmapboxmake.copyOnly() then begin
    SAVFile = filepath('spinmine.sav', ROOT=spinmine_getDirname(), SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, nolog=enmapboxmake.noLog()
  
    docDir = filepath('', ROOT_DIR=spinmine_getDirname(), SUBDIR=['help','idldoc'])
    title='SpInMine Documentation'
    hub_idlDoc, codeDir, docDir, title, NoShow=enmapboxmake.noShow()
  endif

end