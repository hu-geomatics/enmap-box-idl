;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de), Andreas Rabe (janzandr@hu-berlin.de)
;-

function SpInMine_calcPredictions, VI, data, Folds=folds ; fold=0 is training performance

  if folds eq 0 then begin
    noinf = where(finite(VI))
    regr_coeff = linfit(VI[noinf], data[noinf], YFIT=predictions)
  ;  predictions = regr_coeff[0] + regr_coeff[1] * VI
  endif else begin
    numberOfSamples = n_elements(data)
    folds <= numberOfSamples
    partitionProportions = replicate(numberOfSamples, folds)
    partition = hubRandom_partitionProportional(partitionProportions, numberOfSamples)
    predictions = fltarr(numberOfSamples)
    for i=0,folds-1 do begin
      valIndices = where(partition eq i, /NULL, COMPLEMENT=trainIndices)
      trainIndices = trainIndices[where(/NULL, finite(VI[trainIndices]))]
      regr_coeff = linfit(VI[trainIndices], data[trainIndices])
      predictions[valIndices] = regr_coeff[0] + regr_coeff[1] * VI[valIndices]
    endfor
  endelse

  return, predictions
end