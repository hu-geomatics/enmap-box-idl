;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)
; 
; Calculates Accuracy Measures for training data und predicted values
;-

function SpInMine_calcACC, predictions, data, acc

  case acc of 
     0: begin ; R^2
        accuracy = CORRELATE(predictions, data)^2
      end
      
      1: begin ; RMSE
        accuracy = sqrt(total((predictions-data)^2) / n_elements(predictions))
      end
      
      2: begin; rel. RMSE
        accuracy = 100*sqrt(total((predictions-data)^2) / n_elements(predictions))/mean(data)
      end
      
      3: begin; mean absolute Error
        accuracy = mean(abs(predictions-data))
      end

  endcase
  return, accuracy
end
