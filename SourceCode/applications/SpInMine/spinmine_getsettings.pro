;+
; :Author: Henning Buddenbaum (buddenbaum@uni-trier.de)

;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('parameterize.conf', ROOT=parameterize_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, spinmine_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function spinmine_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('spinmine.conf', ROOT=SpInMine_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  ; additional settings
  ; ###########################################################################
  settings['index_types'] = ['Difference', 'Normalized Difference', 'Ratio']
  settings['acc_types'] =  ['R^2', 'RMSE', 'rel. RMSE', 'MAE']
  ; ###########################################################################
    
  return, settings

end

;+
; :Hidden:
;-
pro test_spinmine_getSettings

  print, spinmine_getSettings()

end
