function fmask_getDirname, SourceCode=sourceCode
  result = filepath('Fmask', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result
end

pro test_fmask_getDirname

  print, fmask_getDirname()
  print, fmask_getDirname(/SourceCode)

end
