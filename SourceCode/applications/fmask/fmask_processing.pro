pro fmask_processing, inputLandsatFolder, outputFmaskFolder, cloudProbabilityThresholds

  timeAll =hubTime()

  file_mkdir, outputFmaskFolder
  fmaskExe = filepath('Fmask.exe', ROOT_DIR=fmask_getDirname(/SourceCode), SUBDIRECTORY='_resource')
  fmaskBat = filepath('Fmask.bat' ,ROOT_DIR=outputFmaskFolder)
  
  cd, inputLandsatFolder, CURRENT=oldWorkDir
  foreach cloudProbabilityThreshold,cloudProbabilityThresholds do begin
    time =hubTime()
    cmd = fmaskExe+' 3 3 0 '+strtrim(cloudProbabilityThreshold,2)
    print, cmd
;    hubIOASCIIHelper.writeFile, fmaskBat, cmd
;    spawn, fmaskBat
;    fmaskDataFilename    = (file_search(inputLandsatFolder,'*MTLFmask'))[0]
;    fmaskHeaderFilename  = fmaskDataFilename+'.hdr'
;    targetDataFilename   = filepath(file_basename(fmaskDataFilename)+'_'+string(cloudProbabilityThreshold, Format='(i3.3)'), ROOT_DIR=outputFmaskFolder)
;    targetHeaderFilename = targetDataFilename+'.hdr'
;    file_move, fmaskDataFilename,   targetDataFilename+'.dat', /OVERWRITE
;    file_move, fmaskHeaderFilename, targetHeaderFilename, /OVERWRITE
    print, time.elapsed(/Report)
  endforeach
  cd, oldWorkDir
  
  fmaskFilenames = file_search(outputFmaskFolder,'*.dat')
 
  ; create Fmask overlay
  ;  0 => clear land pixel
  ;  1 => clear water pixel
  ;  2 => cloud shadow
  ;  3 => snow
  ;  4 => cloud
  ;  255 => no observation
  inputImage1 = hubIOImgInputImage(fmaskFilenames[0])
  nthreshold = n_elements(fmaskFilenames)
  size = inputImage1.getSpatialSize()
  fmasks = intarr(size, /NOZERO)
  fmaskOverlay = bytarr(size[0:1])
  for i=0,nthreshold-1 do begin
    fmask = hubIOImg_readImage(fmaskFilenames[i])
    fmaskOverlay[where(/NULL, fmask ge 2 and fmask le 4)] += 1
  endfor

  ; write results
  outputFilename = filepath('FmaskOverlay', ROOT_DIR=outputFmaskFolder)
  outputImage = hubIOImgOutputImage(outputFilename)
  outputImage.copyMeta, inputImage1, /CopySpatialInformation
  outputImage.setMeta, 'file type', 'envi classification'
  outputImage.setMeta, 'classes', nthreshold+1
  classLookup = bytarr(3, nthreshold+1)
  outputImage.setMeta, 'class lookup', classLookup
  outputImage.setMeta, 'class names', ['no observation',string(cloudProbabilityThresholds, FORMAT='(i3)')+'% cloud probability']
  outputImage.writeImage, fmaskOverlay
  print, 'done...'+time.elapsed(/Report)
end

pro test_fmask_processing
  inputLandsatFolder = 'G:\temp\temp_ar\fmask\LE70050662001128AGS00'
  outputFmaskFolder  = 'G:\temp\temp_ar\fmask\LE70050662001128AGS00\Fmask'
  cloudProbabilityThresholds = [0:50:5]
  fmask_processing, inputLandsatFolder, outputFmaskFolder, cloudProbabilityThresholds
end