;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('kMeans.conf', ROOT=kMeans_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, kMeans_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function kMeans_getSettings
  ;read settings from configuration file
  settingsFilename = filepath(strlowcase('kmeans.conf'), ROOT=kMeans_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_kMeans_getSettings

  print, kMeans_getSettings()

end
