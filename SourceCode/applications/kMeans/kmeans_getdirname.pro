;+
; :Author: <author name> (<email>)
;-

function kMeans_getDirname
  
  result = filepath('kMeans', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_kMeans_getDirname

  print, kMeans_getDirname()
  print, kMeans_getDirname(/SourceCode)

end
