;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `kMeans_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `kMeans_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `kMeans_showReport`).
;
;
;-
pro kMeans_application, GroupLeader=groupLeader
  
  title = 'kMeans Clustering'
  defaultValues = hub_getAppState('kMeans','stateHash_kMeans',default=hash())
  parameters = kMeans_getParameters(Title=title, GroupLeader=groupLeader)
  
  if isa(parameters) then begin
    hub_setAppState, 'kMeans', 'stateHash_kMeans', parameters
    reportInfo = kMeans_processing(parameters, Title=title, GroupLeader=groupLeader)
  endif
  
end

;+
; :Hidden:
;-
pro test_kMeans_application
  applicationInfo = Hash()
  kMeans_application, applicationInfo

end  
