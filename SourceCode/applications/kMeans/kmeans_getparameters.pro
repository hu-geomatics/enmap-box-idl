;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
;
;    
;
;-
function kMeans_getParameters, Title=title, GroupLeader=groupLeader
  
  tsize=75
  defaultValues = hub_getAppState('kMeans','stateHash_kMeans',default=hash())
  hubAMW_program, groupLeader, Title=title
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputFilePath', TITLE='Image', VALUE=defaultValues.hubGetValue('inputFilePath')
  hubAMW_frame, TITLE='Parameters'
  hubAMW_parameter, 'numberOfClusters',  Title='Number of Clusters', Value=6, SIZE=10,/INTEGER
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputFilePath', TITLE='Image',VALUE='kmeans'
  result = hubAMW_manage(/Flat);,CONSISTENCYCHECKFUNCTION='check_featureClustering_parameterize_getParameters')
;TODO no more clusters than bands
  if result['accept'] then begin
    parameters = result
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

;+
; :Hidden:
;-
pro test_kMeans_getParameters

  ; test your routine
  parameters = kMeans_getParameters(Title=title, GroupLeader=groupLeader)
  print, parameters

end  
