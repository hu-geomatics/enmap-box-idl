;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`kMeans_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;   
;
;-
function kMeans_processing, parameters, Title=title, GroupLeader=groupLeader

  scriptFilename = filepath('kMeans.R', ROOT_DIR=kMeans_getDirname(), SUBDIRECTORY='lib')
  hubR_runScript, scriptFilename, parameters, spawnResult, spawnError, Title=title, GroupLeader=groupLeader
  
  if isa(parameters) and parameters.hasKey('outputFilePath') then begin 
    enmapBox_openImage, parameters['outputFilePath']  
  endif
  ; store results to be reported inside a hash
  resultHash = hash('error', spawnError, 'result', spawnResult, 'inputfilename', parameters['inputFilePath'], 'outputfilename', parameters['outputFilePath'])
  return, resultHash
end

;+
; :Hidden:
;-
pro test_kMeans_processing
  parameters = hash()
  parameters['inputFilePath'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['numberOfClusters'] = 6
  parameters['outputFilePath'] = filepath(/TMP, 'kmeans')
  result = kMeans_processing(parameters)
  print, result
;  
;numberOfClusters
;  accept:        1
;  outputFilePath: D:\Users\Andreas\AppData\Local\Temp\kmeans
;  inputFilePath: E:\AndreasHU\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Image
  
end  
