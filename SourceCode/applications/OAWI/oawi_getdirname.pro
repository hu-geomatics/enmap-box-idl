;+
; :Author: <author name> (<email>)
;-

function OAWI_getDirname
  
  result = filepath('OAWI', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_OAWI_getDirname

  print, OAWI_getDirname()
  print, OAWI_getDirname(/SourceCode)

end
