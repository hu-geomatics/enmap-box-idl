pro oawi_button2, resultHash

  ok = dialog_message(['STANDARD SETTINGS: ' $
                     ,'' $
                     ,'Input-Image:' $
                     ,'Needs to be a hyperspectral image file of ENVI-type, covering at least the spectral domain between 668nm and 1100nm. A spectral resolution of 10nm or higher is recommended. ' $
                     ,'Output-Image:' $
                     ,'Will be created and overwritten if necessary. Output is a single band greyscale image of ENVI-type.' $ 
                     ,'NDVI-threshold:'$
                     ,'Exclude pixels with NDVI below this value. They receive a NoData Value instead. 0.2 by default. NoData Value: This value serves for both input and output image. Pixels containing this value will be excluded from the algorithm. Invalid iteration results (NDVI < threshold or non-converging pixels) will receive the same value. -999 by default.' $ 
                     ,'Maximum number of iterations per pixel:' $
                     ,'The algorithm stops after a given number of successful iteration steps. This may strongly affect computation time. 10 by default. ' $
                     ,'Automatic detection of target wavelength:' $
                     ,'Indicate whether or not you want the algorithm to look for the local minimum in the water absorption domain as the target wavelength for the iteration. Usually this improves the quality of the output, but for some instances it is known to cause invalid results (depending on your input data). On by default. If turned off, specify the target value manually (960nm by default).' $
                     ,'' $
                     ,'ADVANCED SETTINGS:' $
                     ,'' $
                     ,'Increment for algorithm:' $
                     ,'Standard increment for the iteration of d, i.e. the value that is added to d until the sign of the term R - R0 turns (exceeding the sought value). 0.05 by default.' $
                     ,'Multiplication factor for increment:' $
                     ,'After the sign of the term R - R0 turns (exceeding the sought value), the increment is reduced by the factor indicated here. Low factors cause longer computation time, high factors demand a greater number of maximum iteration in order to get close enough to the sought result. 0.2 by default.' $
                     ,'Improvement threshold for successful iteration: Consider an iteration successful if the sign of R - R0 turns (exceeding of the sought value) AND there is a model improvement greater than the factor specified here. This prevents the algorithm from getting stuck. 0.0005 (0.05 per cent) by default.' $
                     ,'Outer base points for iteration:' $
                     ,'A spectral subset is created between these two wavelengths. They are set to frame the considered water absorption band. Usually there is no need to change them, but if you have to, make sure that the target wavelength is contained in the subset.'] $
                     ,TITLE='OAWI - Help', /INFORMATION)

end
