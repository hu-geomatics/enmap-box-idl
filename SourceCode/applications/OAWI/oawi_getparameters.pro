function check_OAWI_getParameters, resultHash, Message=message

  msg=!NULL
  isConsistent = 1

  inputFilename = resultHash['inputFilename']
  inputImage = hubIOImgInputImage(inputFilename)
  bands = inputImage.getmeta('bands')
  
  if bands eq 1 then begin
    isConsistent = 0b
    msg = 'Input image must be a spectral image'
  endif

  if resultHash.haskey('lambda_target_input') then begin
    if resultHash['lambda_target_input'] lt resultHash['lambda_raw_min'] or resultHash['lambda_target_input'] gt resultHash['lambda_raw_max'] then begin
      isConsistent = 0b
      msg = 'target wavelength for iteration out of range (must be in range of outer base points for iteration)'
    endif
  endif

  message=msg
  return, isConsistent
end

;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided
;    by the` hubAPI` library).
;
;-
function OAWI_getParameters, Title=title, GroupLeader=groupLeader
  
  hubAMW_program, groupLeader, Title=title
  hubAMW_label, 'Retrieving the depth of optical active water in vegetation (OAWI)'
; 
  hubAMW_frame
  hubAMW_subframe , /row
  hubAMW_button,  Title=' About ',EVENTHANDLER='oawi_button1' 
  hubAMW_button,  Title=' Help  ', EventHandler='oawi_button2'
  hubAMW_button,  Title='Contact', EventHandler='oawi_button3'
  
  hubAMW_frame
  hubAMW_inputImageFilename, 'inputFilename', Title='Input-Image ',SIZE=400 
  hubAMW_outputFilename, 'outputFilename', Title='Output-Image',SIZE=400 

  hubAMW_frame, Title='Settings'
  hubAMW_parameter, 'NDVI_th',  Title='Set NDVI threshold for exclusion from processing:  ', Value='0.2',/FLOAT
  hubAMW_parameter, 'nodatval',  Title='NoData Value (for Input & Output):                 ', Value='-999', /FLOAT
  hubAMW_parameter, 'it_max',  Title='Maximal number of iterations per pixel:            ', Value='10', /INTEGER

  hubAMW_subframe, 'seek_target' $
    , Title = 'Automatic detection of target wavelength' $
    , /ROW, /SETBUTTON
  hubAMW_subframe, 'seek_target' $
  , Title = 'Set target wavelength for iteration' $
  , /ROW
  hubAMW_parameter, 'lambda_target_input', TITLE='', UNIT=' nm', Value='960', /FLOAT

  hubAMW_frame, Title='Advanced Settings', /ADVANCED
  
  hubAMW_parameter, 'step_init',  Title='Set increment for algorithm:                       ', Value='0.05',/FLOAT
  
  hubAMW_label, 'Set multiplication factor for increment after each successful iteration:'
  hubAMW_parameter, 'step_bd',  Title=' Factor: ', Value='0.2', /FLOAT
  
  hubAMW_parameter, 'improve_th',  Title='Set improvement threshold for successful iteration:', VALUE='0.0005', /FLOAT
  hubAMW_label, 'Specify outer base points for iteration (spectral subset):'
  hubAMW_parameter, 'lambda_raw_min',  Title='     min:', Value='900', /INTEGER, UNIT=' nm'
  hubAMW_parameter, 'lambda_raw_max',  Title='     max:', Value='1070', /INTEGER, UNIT=' nm'
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='check_OAWI_getParameters')
  
  if parameters['accept'] then begin
    if ~parameters.haskey('lambda_target_input') then parameters['lambda_target_input'] = !FALSE
    parameters['seek_target'] = parameters['seek_target'] eq 0 ? 1 : 0 
    parameters['lambda_raw'] = [parameters['lambda_raw_min'], parameters['lambda_raw_max']]
  endif else begin
    parameters = !null
  endelse
  print,parameters
  return, parameters
end

pro test_OAWI_getParameters
  parameters = OAWI_getParameters(Title='OAWI', GroupLeader=groupLeader)
  print, parameters
end  
