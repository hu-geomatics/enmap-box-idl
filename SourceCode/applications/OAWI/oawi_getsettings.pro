;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('OAWI.conf', ROOT=OAWI_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, OAWI_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function OAWI_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('OAWI.conf', ROOT=OAWI_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_OAWI_getSettings

  print, OAWI_getSettings()

end
