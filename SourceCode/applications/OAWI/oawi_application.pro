;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `OAWI_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `OAWI_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `OAWI_showReport`).
;
; :Keywords:
;    Title : in, optional, type=string
;      Application title.
;      
;    GroupLeader : in, optional, type=widget id
;      Group leader for widget programs.
;
;    Argument : in, optional, type=widget id
;      Argument given button description inside the enmap.men file.
;-
pro OAWI_application, Title=title, GroupLeader=groupLeader, Argument=argument
  
  ; Note:
  ; use settings if needed
  ;   settings = OAWI_getSettings()
  ; use argument keyword if needed
  
  parameters = OAWI_getParameters(Title=title, GroupLeader=groupLeader)
  
  if isa(parameters) then begin
    reportInfo = OAWI_processing(parameters, Title=title, GroupLeader=groupLeader)
;    OAWI_showReport, reportInfo, Title=title
  endif
end

pro test_OAWI_application
  OAWI_application, Title='OAWI', GroupLeader=groupLeader, Argument=argument
end  
