;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`OAWI_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;
;-
function OAWI_processing, parameters, Title=title, GroupLeader=groupLeader

 
  scriptFilename = filepath('OAWI.py', ROOT_DIR=OAWI_getDirname(), SUBDIRECTORY='lib')
  scriptResult = hubPython_runScript(scriptFilename, parameters, spawnResult, spawnError, Title=title, GroupLeader=groupLeader)

  if isa(parameters) and parameters.hasKey('outputFilename') then begin
    enmapBox_openImage, parameters['outputFilename']
  endif

   resultHash = hash('error', spawnError, 'result', spawnResult, 'inputfilename', parameters['inputFilename'], 'outputfilename', parameters['outputFilename'])

   return, resultHash
end

;+
; :Hidden:
;-
pro test_OAWI_processing
  ; test your routine
  parameters = hash() ;replace by own hash definition
  parameters['inputFilename'] = ""
  parameters['outputFilename'] = ""
  parameters['step_init'] = 0.05              
  parameters['step_bd'] = 0.2                 
  parameters['it_max'] = 10                   
  parameters['improve_th'] = 0.0005           
  parameters['nodatval'] = -999               
  parameters['lambda_raw'] = [900, 1070]      
  parameters['seek_target'] = !FALSE          
  parameters['lambda_target_input'] = 960     
  parameters['NDVI_th'] = 0.2
  
  result = OAWI_processing(parameters)
  print, result
end  
