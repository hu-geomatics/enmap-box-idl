# -*- coding: utf-8 -*-
# ^^ erlaubt deutsche Umlaute in den Kommentaren und Variablen ^^ #

## OAWI - Optical Active Water Iteration
# Findet aus einem Spektrum die Dicke der aktiven Wasserschicht 
# durch Iteration des Lambert-Beer'schen Gesetzes

import math
import numpy as np
from rios import applier
import sys
import json

# read input parameters
jsonFile = sys.argv[1]
fileObject = open(jsonFile, 'r')
jsonString = fileObject.read()
p = json.loads(jsonString)

# Von IDL übergeben:
ImgIn = p['inputFilename']
ImgOut = p['outputFilename']
#ImgIn = "C:\\Users\\geo_mahe\\Downloads\\Danner\\20120428_Neusling_AVIS"   # File/evtl. mit Pfad für das Image-File
#ImgOut = "C:\\Users\\geo_mahe\\Downloads\\Danner\\OAWI_AVISt.bsq"           # Outputfile

infile = applier.FilenameAssociations()
infile.image = ImgIn

outfile = applier.FilenameAssociations()
outfile.outimage = ImgOut

oi = applier.OtherInputs()

# Von IDL übergeben:
oi.step_init = p['step_init']    # Schrittweite der Iteration
oi.step_bd = p['step_bd']                    # Multipkationsfaktor für step nach abgeschlossener Iteration
oi.it_max = p['it_max']                      # maximale Anzahl an Iterationen
oi.improve_th = p['improve_th']            # Threshhold zur Verbesserung des Ergebnisses pro Iteration.
                                 # kann es 5x in Folge nicht erreicht werden, bricht das Skript ab
oi.nodatval = p['nodatval']                  # Data Ignore Value des Input-Bilds und für das Outputbild
oi.lambda_raw = p['lambda_raw']          # Stützstellen der Interpolation
oi.seek_target = p['seek_target']                # True: Minimum automatisch suchen; False: Vorgabe

oi.lambda_target_input = p['lambda_target_input']         # für seek_target == FALSE: Wellenlänge, auf die iteratiert wird (local minimum)
oi.NDVI_th = p['NDVI_th']                     # Threshold für NDVI

# Find Header-File for Input-Image
Hdrfile = ImgIn.split('.')
Hdrfile = Hdrfile[0] + '.hdr'

# Absorptionskoeffizienten für flüssiges Wasser nach PALMER & WILLIAMS (1974)
# in den Wellenlängen 725nm bis 2505nm (5nm Auflösung)

alpha_list = [0.013, 0.019, 0.024, 0.025, 0.027, 0.028, 0.028, 0.027, 0.026, 0.025,
              0.024, 0.023, 0.022, 0.022, 0.021, 0.02, 0.02, 0.019, 0.019, 0.02, 0.024,
              0.028, 0.032, 0.035, 0.038, 0.04, 0.041, 0.042, 0.044, 0.047, 0.05, 0.053,
              0.056, 0.059, 0.063, 0.067, 0.071, 0.078, 0.091, 0.103, 0.116, 0.136, 0.158,
              0.193, 0.238, 0.298, 0.367, 0.441, 0.483, 0.502, 0.511, 0.502, 0.486, 0.469,
              0.443, 0.416, 0.384, 0.351, 0.318, 0.285, 0.261, 0.236, 0.216, 0.198, 0.183,
              0.171, 0.162, 0.157, 0.153, 0.148, 0.153, 0.159, 0.164, 0.173, 0.183, 0.195,
              0.212, 0.229, 0.26, 0.295, 0.342, 0.436, 0.529, 0.678, 0.84, 0.985, 1.06,
              1.135, 1.188, 1.207, 1.226, 1.236, 1.243, 1.25, 1.25, 1.25, 1.25, 1.24, 1.23,
              1.22, 1.203, 1.187, 1.17, 1.15, 1.13, 1.11, 1.098, 1.085, 1.073, 1.085, 1.104,
              1.122, 1.167, 1.229, 1.291, 1.359, 1.456, 1.554, 1.651, 1.806, 1.977, 2.148,
              2.43, 2.88, 3.33, 3.78, 4.203, 4.618, 5.034, 5.45, 6.418, 7.387, 8.355, 9.781,
              13.04, 16.29, 19.55, 22.4, 24.67, 26.93, 29.19, 31.05, 31.3, 31.55, 31.47, 30.9,
              30.33, 29.76, 29.19, 28.61, 27.26, 25.72, 24.17, 22.63, 21.15, 19.76, 18.37,
              16.99, 15.6, 14.84, 14.08, 13.32, 12.56, 11.91, 11.42, 10.94, 10.45, 9.97, 9.546,
              9.21, 8.875, 8.54, 8.204, 7.923, 7.679, 7.435, 7.191, 6.947, 6.744, 6.603, 6.463,
              6.323, 6.182, 6.055, 5.98, 5.905, 5.83, 5.755, 5.68, 5.65, 5.65, 5.65, 5.65, 5.65,
              5.65, 5.75, 5.85, 5.95, 6.05, 6.15, 6.276, 6.504, 6.732, 6.961, 7.189, 7.417, 7.649,
              7.896, 8.143, 8.39, 8.637, 8.884, 9.131, 9.255, 9.349, 9.442, 9.473, 9.46, 9.447,
              9.52, 9.72, 9.92, 10.12, 10.32, 10.52, 10.72, 14.8, 21.47, 28.15, 34.82, 41.49,
              48.16, 54.83, 62.49, 70.8, 79.11, 87.43, 95.74, 104.1, 112.4, 120.7, 122.7, 120.5,
              118.3, 116.1, 113.9, 111.6, 109.4, 104.9, 99.82, 94.71, 89.61, 84.51, 79.41, 74.3,
              69.2, 66.25, 63.3, 60.35, 57.4, 54.44, 51.49, 48.54, 45.59, 43.67, 42.0, 40.33,
              38.67, 37.0, 35.33, 33.67, 32.0, 30.67, 29.85, 29.03, 28.2, 27.38, 26.56, 25.74,
              24.92, 24.09, 23.48, 23.17, 22.86, 22.56, 22.25, 21.94, 21.63, 21.33, 21.02, 20.71,
              20.41, 20.1, 19.79, 19.48, 19.4, 19.65, 19.89, 20.14, 20.39, 20.63, 20.88, 21.13,
              21.37, 21.62, 21.87, 22.11, 22.36, 22.61, 22.85, 23.24, 23.83, 24.43, 25.02, 25.62,
              26.21, 26.8, 27.4, 27.99, 28.59, 29.18, 30.21, 31.35, 32.48, 33.62, 34.75, 35.89,
              37.03, 38.16, 39.3, 40.44, 41.57, 43.3, 45.17, 47.04, 48.91, 50.78, 52.65, 54.52,
              56.39, 58.26, 60.13, 62.0, 64.03, 66.67, 69.31, 71.95, 74.59, 77.22, 79.86, 82.5,
              85.14, 87.78, 90.42, 93.06, 95.7]

range_alpha = range(725, 2505, 5)
hash_alpha = dict(zip(range_alpha, alpha_list))  # Hash_map weist jeder Wellenlänge zw. 720 und 2500nm einen Alpha-Wert zu

with open(Hdrfile, 'r') as Header_In:
    lines = Header_In.readlines()

wave_flag = 0
wave_convert = 0

for i, line in enumerate(lines):
    if line.lower().startswith("wavelength = "):
        wave_flag = 1
        i_start = i
    if '}' in line and wave_flag == 1:
        i_stop = i+1
        wave_flag = 0
    if "wavelength units" in line.lower(): # suche die Zeile mit der Information zur Einheit
        if "micrometers" in line.lower():
            wave_convert = 1000 # µm werden in Nanometer umgerechnet (Faktor 1000)
        elif "nanometers" in line.lower():
            wave_convert = 1 # nm bleiben Nanometer (Faktor 1)
        else:
            raise SystemExit # keine Wellenlängen-Info bekannt -> Ende Gelände

string_chaos = [lines[i_start + i].rstrip() for i in range(0,i_stop-i_start)]
string_chaos = ",".join(string_chaos)
string_chaos = string_chaos[string_chaos.index('{')+1:string_chaos.index('}')]
string_chaos = string_chaos.split(',')

oi.lambd = [int(float(item)*wave_convert) for item in string_chaos if not item==""]

controls = applier.ApplierControls()
controls.setOutputDriverName("ENVI")
controls.setCreationOptions(["INTERLEAVE=BSQ"])
controls.setStatsIgnore(oi.nodatval)

# die eigentliche Funktion, die Applier anwenden soll:
def d_iter(info, input, output, oi):
    def find_closest(input):    # Falls für eine bestimmte Wellenlänge keine Reflektanz vorliegt,
                                # wird diese Funktion gerufen um die nächstgelegene zu finden

        output = min(oi.lambd, key=lambda x: abs(x - input))
        return output

    lens = [len(input.image[:,0,0]), len(input.image[0,:,0]), len(input.image[0,0,:])] # Dim1: bands, Dim2: rows, Dim3: cols
    d_out = np.empty(lens[1:3])
    d_out.fill(oi.nodatval)

    for line in range(lens[1]):
        for col in range(lens[2]):

            spectrum = input.image[:,line,col]  # Array mit den Reflektanzen vom Input-File für das jeweilige Pixel
            hash_getspec = dict(zip(oi.lambd, spectrum)) # Erstelle eine Hashmap, die jeder Wellenlänge ein Spektrum zuweist

            lambda_out = [find_closest(oi.lambda_raw[0]), find_closest(oi.lambda_raw[1])]  # entsprechend der Vorgabe unter lambda_raw werden die tatsächlich vorhandenen Kanäle gesucht, die den angegebenen am nächsten liegen

            # Check Data NA and NDVI > NDVI threshold
            R827 = hash_getspec[find_closest(827)]
            R668 = hash_getspec[find_closest(668)]

            try:
                NDVI = float(R827-R668)/float(R827+R668)
            except ZeroDivisionError:
                NDVI = 0.0

            range_lamb = range(oi.lambd.index(lambda_out[0]),
                               oi.lambd.index(lambda_out[1]) + 1)  # Vorbereitung einer Range für die Erstellung der Subsets

            R_out = [hash_getspec[lambda_out[0]],
                     hash_getspec[lambda_out[1]]]  # Suche die Reflektanzen zu den lambda_out - Wellenlängen (über die Hashmap)

            m = (float(R_out[1])-float(R_out[0]))/(lambda_out[1]-lambda_out[0]) # Steigung des Spektrums innerhalb Lambda_Out

            sub_lambda = [oi.lambd[sub_lamb] for sub_lamb in range_lamb]  # Erstelle ein Subset an Wellenlängen zwischen den Stützstellen außen (lambda_out[0],lambda_out[1])
            sub_spectrum = [spectrum[sub_lamb] for sub_lamb in range_lamb]  # Erstelle ein Subset an Spektren zwischen den Stützstellen außen (lambda_out[0],lambda_out[1])
            sub_spectrum = [sub_spectrum[k]-(sub_lambda[k]-lambda_out[0])*m for k in range(len(sub_spectrum))] # Continuum removal - "Horizontalisierung" des Spektrums

            if oi.nodatval in sub_spectrum or np.isnan(sub_spectrum).any() or NDVI <= oi.NDVI_th: # NoData Output, falls NoData im Input, NaN gefunden oder NDVI unter Threshold
                d_out[line, col] = oi.nodatval
                continue

            if oi.seek_target:
                lambda_target = sub_lambda[sub_spectrum.index(min(sub_spectrum))]  # Target-Lambda (lokales Minimum) ist die zugehörige Wellenlänge zur minimalen Reflektanz im Subset
            else:
                lambda_target = find_closest(oi.lambda_target_input)  # Ziel-Wellenlänge auf die iteriert werden soll

            hash_getspec = dict(zip(sub_lambda, sub_spectrum)) # Update der Hashmap (korrigierte Spektren, gekippt)

            # interpoliere alpha zwischen linkem und rechtem Nachbarn
            try:
                alpha = hash_alpha[lambda_target]  # wenn er für die Ziel-Wellenlänge direkt einen Wert findet, braucht nicht interpoliert zu werden, ansonsten:
            except:
                alpha_diff = [range_alpha[i] - lambda_target for i in range(0, len(range_alpha))]  # Erstelle Abstands-Array
                least_diff = [max(i for i in alpha_diff if i < 0), min(i for i in alpha_diff if i > 0)]  # linker und rechter Nachbar (größter Wert < 0 und kleinster Wert > 0)
                alpha_pre = [hash_alpha[range_alpha[alpha_diff.index(least_diff[0])]], hash_alpha[range_alpha[
                    alpha_diff.index(least_diff[1])]]]  # zugehörige Alphawerte rechts und links von der gesuchten Position
                alpha = alpha_pre[0] + (alpha_pre[1] - alpha_pre[0]) * abs(least_diff[0]) / (least_diff[1] - least_diff[0])  # Interpolation zwischen linkem und rechten Nachbarn

            ## Initialisierung der Iteration
            step = oi.step_init # Step wird auf Startwert gesetzt
            d = 0  # anfänglicher Wassergehalt ist 0
            VZ = 1  # Boolean-Variable für das Vorzeichen
            th_fail = 0  # Count-Variable für Iterationen, die das improve_th nicht erreichen
            d_best = 0  # bislang bester d-Wert
            i = 1  # Count-Variable der Iteration
            j = 1  # Count-Variable für Iterationen ohne Verbesserung
            diff = []  # Array mit Differenzen zwischen linear optimierter Reflektanz und Reflektanz für aktuellen d-Wert
            diff.append(1)  # Startwert ist "1"

            while i <= oi.it_max and th_fail < 5:  # iteriere solange bis maximale Anzahl erreicht ist oder improve_th 5x nicht erreicht wurde

                if abs(d) > 5: break # Falls d extrem steigt, Abbruch (Minimum verpasst)
                d_best = d  # nach erfolgreicher Iterierung wird d_best aktualisiert
                d += VZ * step  # erhöhe d entsprechend der Schrittweite. VZ gibt an, ob d vorwärts oder rückwärts angepasst wird
                R_ohne = hash_getspec[lambda_target] / math.exp(-alpha * d)  # Berechne Reflektanz wenn Wassergehalt rausgerechnet ist (exp ist eine Funktion aus package "math")
                diff.insert(j, R_out[0] - R_ohne)  # speichere die Differenz zwischen diesen Reflektanzen im Diff-Array an der Stelle j

                if diff[j] * VZ < 0 and abs(diff[j]) < abs(diff[j - 1]):  # wenn die Differenz das Vorzeichen wechselt UND dabei einen kleineren Betrag hat als zuvor
                    step *= oi.step_bd  # Schrittweite anpassen (z.B. 50%)
                    VZ *= -1  # VZ wechseln
                    i += 1  # Iteration abgeschlossen, erhöhe um 1
                    j += 1  # Speicherposition im Diff-Array weiterzählen

                    if abs(d_best - d) < oi.improve_th * d_best:  # wenn die Diff zwar verbessert wurde, aber um weniger als improve_th, dann zähle den Fail-Counter hoch
                        th_fail += 1
                    else:
                        th_fail = 0  # wenn nicht, wird er auf 0 zurückgesetzt (erst beim 5. Mal in Folge setzt er aus)

                elif diff[j] * VZ < 0:  # wenn Vorzeichen gewechselt, aber KEINE Verbesserung
                    step *= oi.step_bd  # Schrittweite verringern
                    VZ *= -1  # Vorzeichen wechseln (Marschrichtung: zurück)
                    j += 1    # j wird erhöht (Speicherposition im Array), aber nicht i (da kein erfolgreicher Iterationsschritt)

            if d < 0 or d > 5: d = oi.nodatval # für den seltenen Fall, dass die Iteration ins Leere gelaufen ist

            d_out[line, col] = d # Schreibe den finalen d-Wert an die richtige Position im Bild

    d_out = np.expand_dims(d_out,0) # 3. Dimenson hinzufügen (nur 1 Kanal)
    output.outimage = d_out

applier.apply(d_iter, infile, outfile, controls=controls, otherArgs=oi)

Out_Hdr = ImgOut.split('.')
Out_Hdr = Out_Hdr[0] + '.hdr'

nodat_line = -1 # Initialisierung Nodat_line
Hdr_deposit = []

with open(Out_Hdr, 'r+') as Header_Out:
    for i, line in enumerate(Header_Out):

        if "Data Ignore Value" in line: # falls bereits ein Data Ignore Value feststeht:
            nodat_line = i # merke dir die Zeile, wo dies der Fall ist
            Hdr_deposit.append('Data Ignore Value = ' + str(oi.nodatval) + '\n') # füge da den neuen Wert ein
            continue
        Hdr_deposit.append(line) # speichere alle Zeilen des Headerfiles in den Container

    if nodat_line < 0: # wenn kein NoDatVal im Header gefunden wurde
        Hdr_deposit.append('Data Ignore Value = ' + str(oi.nodatval)) # hänge den Wert unten an das File an

    Header_Out.seek(0) # rewind

    for header_line in Hdr_deposit:
        Header_Out.write(header_line) # neuen Header überschreiben

