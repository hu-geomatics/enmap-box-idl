# import packages
import sys
import json

# read input parameters
jsonFile = sys.argv[1]
fileObject = open(jsonFile, 'r')
jsonString = fileObject.read()
p = json.loads(jsonString)
r = {}

### user defined code ###

#print(p['inputText'])
r['outputText'] = 'Hello World from Python'

#########################

# write output parameters
fileConnection = open(p['scriptOutputFilename'], 'w')
fileConnection.write(json.dumps(r))
fileConnection.close()
