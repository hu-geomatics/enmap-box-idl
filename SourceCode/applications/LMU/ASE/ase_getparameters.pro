function getParameters_Dialog1
  hubAMW_program , Title = 'ASE-image'
  
;   hubAMW_frame  
;  hubAMW_subframe , /row
;   hubAMW_button,  Title=' About ', EventHandler='ase_button1'
;   hubAMW_button,  Title=' Help  ', EventHandler='ase_button2'
;   hubAMW_button,  Title='Contact', EventHandler='ase_button3'
   
  hubAMW_frame ;,   Title =  'Input Data 1'
  hubAMW_inputImageFilename, 'inputImage', Title='Open Input Image 1   '

  hubAMW_frame ;,   Title =  'Input Data 2'
  hubAMW_inputImageFilename, 'inputImage2', Title='Open Input Image 2   ' 
  return, hubAMW_manage()

end

function getParameters_Dialog2, results1

  inputImage_temp = hubIOImgInputImage(results1['inputImage'])
;  help, inputImage_temp
;  inputImage_temp.initReader, tilelines, /SLICE, SubsetbandPositions=1, Datatype='float'
;  data2 = inputImage_temp.getData()
;  help, data2
  numberOfBands = inputImage_temp.getMeta('bands')
  if inputImage_temp.hasMeta('band names') then begin
    bandNames = inputImage_temp.getMeta('band names')
  endif else begin
    bandNames = 'Band '+ strtrim(indgen(numberOfBands)+1,2)
  endelse
  
  inputImage2_temp = hubIOImgInputImage(results1['inputImage2'])
  numberOfBands2 = inputImage2_temp.getMeta('bands')
  if inputImage2_temp.hasMeta('band names') then begin
    bandNames2 = inputImage2_temp.getMeta('band names')
  endif else begin
    bandNames2 = 'Band '+ strtrim(indgen(numberOfBands2)+1,2)
  endelse
    
  hubAMW_program , Title = 'ASE-image'
  
;     hubAMW_frame  
;  hubAMW_subframe , /row
;   hubAMW_button,  Title=' About ', EventHandler='ase_button1', TOOLTIP='About many interesting things'
;   hubAMW_button,  Title=' Help  ', EventHandler='ase_button2'
;   hubAMW_button,  Title='Contact', EventHandler='ase_button3'
 
  hubAMW_frame ,   Title =  'Band Settings'
  
  hubAMW_list, 'bandIndex', TITLE='Image 1: Select your Band of choice', LIST=bandNames, XSIZE=400, YSIZE=75
  hubAMW_list, 'bandIndex2', TITLE='Image 2: Select your Band of choice', LIST=bandNames2, XSIZE=400, YSIZE=75
  
  hubAMW_frame
  hubAMW_checkbox, 'nullvalue', Title='Ignore Zero in Statistics', Value=0b
  
;    hubAMW_frame  
;  hubAMW_subframe, TITLE='Main Indices Information' , /row
;   hubAMW_button,  Title='  RMSE  ', EventHandler='ase_button4'
;   hubAMW_button,  Title='  R^2   ', EventHandler='ase_button5'
;   hubAMW_button,  Title='  m/b   ', EventHandler='ase_button6'
;   hubAMW_button,  Title=' RRMSE  ', EventHandler='ase_button7'
;   hubAMW_button,  Title='  NSE   ', EventHandler='ase_button8'
;  
;  hubAMW_frame
;  hubAMW_subframe ,   Title =  'Error Indices', /row 
;  hubAMW_subframe, /row 
;     hubAMW_button,  Title='i', EventHandler='ase_button4', TOOLTIP='Root Mean Square Error'
;    hubAMW_checkbox, 'ase_rmse', Title='RMSE ', Value=1b;, TOOLTIP='Root Mean Square Error'
;    hubAMW_checkbox, 'ase_mae', Title='MAE  ', Value=0b;, TOOLTIP='Mean Absolute Error'
;    hubAMW_checkbox, 'ase_mbe', Title='MBE  ', Value=0b;, TOOLTIP='Mean Bias Error'
;    hubAMW_checkbox, 'ase_sb', Title='SB   ', Value=0b;, TOOLTIP='Simulation Bias'
;    hubAMW_checkbox, 'ase_sepc', Title='SEPC ', Value=0b;, TOOLTIP='Standard Error of Prediction'
;
;  hubAMW_frame
;  hubAMW_subframe ,   Title =  'Correlation-based Measures', /row   
;  hubAMW_subframe, /row   
;     hubAMW_button,  Title='i', EventHandler='ase_button5', TOOLTIP='Coefficient of Determination'
;    hubAMW_checkbox, 'ase_r2', Title='R^2  ', Value=1b; , TOOLTIP='Coefficient of Determination'
;    hubAMW_checkbox, 'ase_r', Title='r    ', Value=0b; , TOOLTIP='Pearson product-moment correlation coefficient'
;     hubAMW_button,  Title='i', EventHandler='ase_button6', TOOLTIP='Slope of Theil-Sen Regression'
;    hubAMW_checkbox, 'ase_m', Title='m    ', Value=1b;, TOOLTIP='Slope of Theil-Sen Regression'
;    hubAMW_checkbox, 'ase_b', Title='b    ', Value=1b;, TOOLTIP='Intercept of Theil-Sen Regression'
;
;  hubAMW_frame
;  hubAMW_subframe ,   Title =  'Dimensionless Evaluation', /row 
;  hubAMW_subframe, /row   
;     hubAMW_button,  Title='i', EventHandler='ase_button7'
;    hubAMW_checkbox, 'ase_rrmse', Title='RRMSE', Value=1b;, TOOLTIP='Relative Root Mean Square Error'
;    hubAMW_checkbox, 'ase_ndi', Title='NDI  ', Value=0b;, TOOLTIP='Non-dimensional Error Index'
;    hubAMW_checkbox, 'ase_nrmse', Title='NRMSE', Value=0b;, TOOLTIP='Normalized Root Mean Square Error'
;    hubAMW_checkbox, 'ase_rdp', Title='RDP  ', Value=0b;, TOOLTIP='Ratio of SD(obs) to Root Mean Square Error'
;    hubAMW_checkbox, 'ase_relb', Title='Rel.B', Value=0b;, TOOLTIP='Relative Bias'
;  hubAMW_subframe, /row   
;     hubAMW_button,  Title='i', EventHandler='ase_button8', TOOLTIP='Nash Sutcliffe Efficiency'
;    hubAMW_checkbox, 'ase_nse', Title='NSE  ', Value=1b;, TOOLTIP='Nash-Sutcliffe Efficiency'
;    hubAMW_checkbox, 'ase_rpe', Title='RPE  ', Value=0b;, TOOLTIP='Relative Percentage Error'
;    hubAMW_checkbox, 'ase_d', Title='d    ', Value=0b;, TOOLTIP='Willmott Index of Agreement'
;    hubAMW_checkbox, 'ase_ac', Title='AC   ', Value=0b;, TOOLTIP='Agreement Coefficient'

   
  ;parameters = hubAMW_manage()
  return, hubAMW_manage()

end

function ASE_getParameters, settings
;  groupLeader = settings.hubGetValue('groupLeader')
;  
;  hubAMW_program , groupLeader, Title = settings['title']
;  hubAMW_program , Title = 'ASE-image'
;  
;   hubAMW_frame  
;  hubAMW_subframe , /row
;   hubAMW_button,  Title=' About ', EventHandler='ase_button1'
;   hubAMW_button,  Title=' Help  ', EventHandler='ase_button2'
;   hubAMW_button,  Title='Contact', EventHandler='ase_button3'
;   
;  hubAMW_frame ;,   Title =  'Input Data 1'
;  hubAMW_inputImageFilename, 'inputImage', Title='Open Input Image 1   '
;
;;  hubAMW_frame ;,   Title =  'Input Data 2'
;;  hubAMW_inputImageFilename, 'inputImage2', Title='Open Input Image 2   ' 
;;parameters = hubAMW_manage() 
;  return, results1+results2 ;parameters ;hubAMW_manage()


  CANCELED = Hash('accept',0b)
  results1 = getParameters_Dialog1()
  if results1['accept'] eq 0 then return, CANCELED
  
   inputImage_temp = hubIOImgInputImage(results1['inputImage'])
;  help, inputImage_temp
;  inputImage_temp.initReader, tilelines, /SLICE, SubsetbandPositions=1, Datatype='float'
;  data2 = inputImage_temp.getData()
;  help, data2
;  print, 'hier'
   
  results2 = getParameters_Dialog2(results1)
  if results2['accept'] eq 0 then return, CANCELED
  
  return, results1+results2

end



;+
; :Hidden:
;-
pro test_ASE_getParameters

  ; test your routine
  settings = ASE_getSettings()
  parameters = ASE_getParameters(settings)
  print, parameters

end  
