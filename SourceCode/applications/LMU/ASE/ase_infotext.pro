PRO ase_infotext_event
cmd = WIDGET_INFO(event.id,/UNAME)  

  CASE cmd OF
'quit':  WIDGET_CONTROL,event.top,/DESTROY
   ELSE:
  ENDCASE      
END


PRO ase_infotext

base = WIDGET_BASE(row=3, MBAR = mbar, /TAB_MODE)

;-----a_base


   
   a_base = WIDGET_BASE(base, row=11, /FRAME)
   
   title1 = WIDGET_LABEL(a_base, VALUE = 'ASE - short description', /ALIGN_LEFT, XSIZE=200)
   
   txt1 = widget_text(a_base, value='The objective of the Advanced Statistical Evaluator (ASE) is to provide for remote sensing practitioners (i.e., non-statisticians) guidance for model evaluation. An optimal set of statistical measures is proposed for the quantitative assessment of model performance in the context of vegetation biophysical variable retrieval from Earth observation (EO) data.', $
   FONT='Times-Roman', xsize=110, ysize=3,/WRAP, uname='info1', UNITS=0) 
   
   txt2 = WIDGET_TEXT(a_base, value='The ASE module is based on a study [1] where the main terms and concepts were reviewed and an exhaustive literature review was carried out. Most common statistical measures used to evaluate model performances were summarized and analyzed supported by exemplary datasets. Finally, an "optimal" statistic set was defined, including root mean square error (RMSE) from the category of Error Indices; coefficient of determination (R^2), slope and intercept of Theil-Sen regression [2] from the category of Correlation-based Measures; relative RMSE (RRMSE), and Nash-Sutcliffe efficiency (NSE) index [3] from the category of Dimensionless Indices. This indicator set meets the following essential model validation criteria:',$
    FONT='Times-Roman', XSIZE=110, ysize=6,/WRAP, uname='info2') 
   
   txt3 = WIDGET_TEXT(a_base, value='(a) non-dimensionality, to avoid influence from the magnitude of the values',$
    FONT='Times-Roman', XSIZE=110, ysize=1,/WRAP, uname='info3') 
    
   txt4 = WIDGET_TEXT(a_base, value='(b) bounded, for effortless comprehension of its meaning (e.g., between 0-no agreement and 1-perfect agreement)',$
    FONT='Times-Roman', XSIZE=110, ysize=1,/WRAP, uname='info4')    

   txt5 = WIDGET_TEXT(a_base, value='(c) symmetry, i.e., data sets should be interchangeable since the assumption of "ground truth" is unrealistic in remote sensing applications',$
    FONT='Times-Roman', XSIZE=110, ysize=1,/WRAP, uname='info5') 

   txt6 = WIDGET_TEXT(a_base, value='(d) measurement of the actual difference in the data unit to understand the magnitude of the error',$
    FONT='Times-Roman', XSIZE=110, ysize=1,/WRAP, uname='info6') 

   txt7 = WIDGET_TEXT(a_base, value='(e) model prediction capability compared to measurement statistics (e.g., observed mean value)',$
    FONT='Times-Roman', XSIZE=110, ysize=1,/WRAP, uname='info7') 

   txt8 = WIDGET_TEXT(a_base, value='A wide acceptance and use of these statistics should enable a better intercomparison of scientific results, urgently needed in times of increasing model development activities that are carried out with respect to the upcoming EnMAP and other EO missions.',$
    FONT='Times-Roman', XSIZE=110, ysize=2,/WRAP, uname='info8')    
  
  b_base = WIDGET_BASE(base, row=4, /FRAME)
   title2 = WIDGET_LABEL(b_base, VALUE = 'References', /ALIGN_LEFT, XSIZE=200)
   
   txt9 = WIDGET_TEXT(b_base, value='[1] K. Richter, C. Atzberger, T. B. Hank, and W. Mauser, "Derivation of biophysical variables from Earth Observation data: validation and statistical measures," Journal of Applied Remote Sensing, accepted with minor revisions (2012).',$
    FONT='Times-Roman', XSIZE=110, ysize=2,/WRAP, uname='info9') 
    
   txt10 = WIDGET_TEXT(b_base, value='[2] R. Fernandes, and S. G. Leblanc, "Parametric (modified least squares) and non-parametric (Theil-Sen) linear regressions for predicting biophysical parameters in the presence of measurement errors," Remote Sens. Environ. 95(3), 303-316 (2005).',$
    FONT='Times-Roman', XSIZE=110, ysize=2,/WRAP, uname='info10') 
    
   txt11 = WIDGET_TEXT(b_base, value='[3] J. E. Nash, and J. V. Sutcliffe, "River flow forecasting through conceptual models part I - A discussion of principles," J. Hydrol. 10(3), 282-290 (1970).',$
    FONT='Times-Roman', XSIZE=110, ysize=2,/WRAP, uname='info11') 
    
 c_base = WIDGET_BASE(base, row=1)
  quit = WIDGET_BUTTON(c_base, VALUE='OK', UNAME='quit')
    
;; Ende einleiten
WIDGET_CONTROL, base, /REALIZE

XMANAGER, 'ase_infotext', base     ;; notwendig für die Bedienung der Knöpfe, ruft den Event-Manager hinzu

END