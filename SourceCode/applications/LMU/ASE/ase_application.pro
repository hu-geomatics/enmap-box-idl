;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `ASE_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `ASE_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `ASE_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro ASE_application, applicationInfo
  
  ; get global settings for this application
  settings = ASE_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = ASE_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = ASE_processing(parameters, settings)
    
;    if parameters['showReport'] then begin
;      ASE_showReport, reportInfo, settings
;    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_ASE_application
  applicationInfo = Hash()
  ASE_application, applicationInfo

end  
