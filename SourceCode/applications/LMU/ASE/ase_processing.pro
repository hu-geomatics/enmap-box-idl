
function ASE_processing, results1, results2, settings 
  
  tilelines=1000

  
 bdnr1 = results1['bandIndex']
 bdnr2 = results1['bandIndex2']
 help, bdnr1, bdnr2
  
  inputImage = hubIOImgInputImage(results1['inputImage'])
    inputimage.initReader,   tilelines, /TILEPROCESSING, /SLICE, $
     SubsetbandPositions=bdnr1, Datatype='float'
  data1 = inputimage.getData()
  help, data1

  inputImage2 = hubIOImgInputImage(results1['inputImage2'])
    inputimage2.initReader, tilelines, /TILEPROCESSING, /SLICE, $
         SubsetbandPositions=bdnr2, Datatype='float'
  data2 = inputimage2.getData()
  help, data2
  
;; ========== Abfangen der Nullwerte
if results1['nullvalue'] then begin 
 
  idx_1 = WHERE(data1 NE 0)
    data1_a = data1[idx_1]
    data2_a = data2[idx_1]

  idx_2 = WHERE(data2_a NE 0)
    data1_b = data1_a[idx_2]
    data2_b = data2_a[idx_2]

  data1 = data1_b
  data2 = data2_b

  help, data1
  help, data2
  print, min(data1)
  print, min(data2)

endif

;; ========== Filtern ungültiger Werte (NaN, Inf)

  idx_3 = WHERE(FINITE(data1),  count)
    data1_a = data1[idx_3]
    data2_a = data2[idx_3]

  idx_4 = WHERE(FINITE(data2_a),  count)
    data1_b = data1_a[idx_4]
    data2_b = data2_a[idx_4]

  data1 = data1_b
  data2 = data2_b
  
  data_struc = {dat1 : data1, dat2 : data2, comment : ''}
 
;ALTERNATIVE
;WHERE(/NULL, data1 NE 0 and data2 ne 0 and finite(data1) and finite(data2)) 

  
  ase_statistics, data_struc
  
  
  
  
;; ALT 03.07.2012
;  ;; Calculate Statistics
;            n_data = N_ELEMENTS(data1)
;         
;            ;; means
;            mean1 = mean(data1)
;            mean2 = mean(data2)
;            help, mean1, mean2
;            
;            ;; standard deviations
;            std1= STDDEV(data1)
;            std2 = STDDEV(data2)
;            help, std1, std2
;            
;            ;; RMSE
;            rmse = SQRT(TOTAL((data2-data1)^2)/n_data)
;
;;; I.
;
; if results1['ase_rmse'] then begin
;  print, 'RMSE: ', rmse                 
;  endif else begin
;  RMSE = 9999
;  endelse
; if results1['ase_mae'] then begin
;  mae = TOTAL(ABS(data2-data1))/n_data    
;  print, 'MAE: ', mae            
;  endif else begin
;  MAE = 9999
;  endelse  
; if results1['ase_mbe'] then begin
;  mbe = mean1-mean2  
;  print, 'MBE: ', mbe              
;  endif else begin
;  MBE = 9999
;  endelse 
; if results1['ase_sb'] then begin
;  sb = (mean1-mean2)^2 
;  print, 'SB: ', sb               
;  endif else begin
;  SB = 9999
;  endelse   
; if results1['ase_sepc'] then begin
;  sepc = sqrt(TOTAL((data2-data1-(mean2-mean1))^2)/n_data)
;  print, 'SEPC: ', sepc               
;  endif else begin
;  SEPC = 9999
;  endelse   
;
;;; II.
;
; if results1['ase_r2'] then begin
;  r_2 = (CORRELATE(data1,data2))^2
;  print, 'R^2: ', r_2               
;  endif else begin
;  r_2 = 9999
;  endelse   
; if results1['ase_r'] then begin
;  r = CORRELATE(data1,data2) 
;  print, 'R: ' , r               
;  endif else begin
;  r = 9999
;  endelse   
; if results1['ase_m'] then begin
;  mb_all = POLY_FIT(data1,data2,1) 
;              m = mb_all[1] 
;  print, 'm: ', m                          
;  endif else begin
;  m = 9999
;  endelse   
; if results1['ase_b'] then begin
;    mb_all = POLY_FIT(data1,data2,1) 
;              b = mb_all[0]
;  print, 'b: ', b                           
;  endif else begin
;  b = 9999
;  endelse   
;
;;; III.
;
; if results1['ase_rrmse'] then begin
;  RRMSE = RMSE/mean1 
;  print, 'RRMSE: ', rrmse               
;  endif else begin
;  RRMSE = 9999
;  endelse
; if results1['ase_ndi'] then begin
;  NDI = rmse/std1 
;  print, 'ND: ', ndi              
;  endif else begin
;  ndi = 9999
;  endelse
; if results1['ase_nrmse'] then begin
;  range = max(data1)-min(data1)
;  NRMSE = RMSE/range
;  print, 'NRMSE: ', nrmse               
;  endif else begin
;  NRMSE = 9999
;  endelse  
; if results1['ase_rdp'] then begin
;  RDP = std1/rmse
;  print, 'rdp: ', rdp               
;  endif else begin
;  rdp = 9999
;  endelse 
; if results1['ase_relb'] then begin
;  RelB = ((TOTAL(data1-data2))/n_data)/mean1*100 
;  print, 'relB: ', RelB               
;  endif else begin
;  relB = 9999
;  endelse 
; if results1['ase_nse'] then begin
;  NSE = 1-(TOTAL((data1-data2)^2)/TOTAL((data1-mean1)^2)) 
;  print, 'NSE: ', nse               
;  endif else begin
;  nse = 9999
;  endelse 
; if results1['ase_rpe'] then begin
;  RPE = mean((ABS(data2-data1))/data1*100)
;  print, 'RPE: ', rpe               
;  endif else begin
;  rpe = 9999
;  endelse 
; if results1['ase_d'] then begin
;  d = 1-(TOTAL((data1-data2)^2)/TOTAL((abs(data2-mean1)+ABS(data1-mean1))^2)) 
;  print, 'd: ', d               
;  endif else begin
;  d = 9999
;  endelse
; if results1['ase_ac'] then begin
;   AC = 1-(TOTAL((data1-data2)^2))/(TOTAL((ABS(mean1-mean2)+ABS(data1-mean1))*(ABS(mean1-mean2)+ABS(data2-mean2))))
;   print, 'AC: ', ac               
;  endif else begin
;  ac = 9999
;  endelse 
;  
;  ase_export, rmse, mae, mbe, sb, sepc, r_2, r, m, b, rrmse, ndi, nrmse, rdp, relb, nse, rpe, d, ac
;  print, 'Bier auf!'
         
end

;+
; :Hidden:
;-
pro test_ASE_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = ASE_getSettings()
  
  result = ASE_processing(parameters, settings)
  print, result

end  
