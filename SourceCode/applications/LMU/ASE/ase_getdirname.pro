;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function ASE_getDirname
  
  result = filepath('ASE', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_ASE_getDirname

  print, ASE_getDirname()
  print, ASE_getDirname(/SourceCode)

end
