;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('ASE.conf', ROOT=ASE_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, ASE_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function ASE_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('ase.conf', ROOT=ASE_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_ASE_getSettings

  print, ASE_getSettings()

end
