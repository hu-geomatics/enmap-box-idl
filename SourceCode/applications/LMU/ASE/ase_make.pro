;;+
;; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;;-
;
;pro ASE_make, Cleanup=cleanup
;
;  if keyword_set(cleanup) then begin
;    ; de-install application by deleting the application folder (NOT the source code!)
;    
;    appDir = ASE_getDirname()
;    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
;    return
;  endif
;  
;  ; find source code and application directory  
;  
;  codeDir =  file_dirname((routine_info('ASE_make',/SOURCE)).path)
;  appDir = ASE_getDirname()
;  
;  ; check lower case *.pro filenames (important for auto-compiling under unix systems
;  
;  enmapBoxDev_checkForUppercaseFilenames, codeDir
;  
;  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
;  
;  enmapBoxMake_copyDefaultDirs, codeDir, appDir
;  
;   ; save routines inside SAVE file
;   
;  SAVFile = filepath('ASE.sav', ROOT=appDir, SUBDIR='lib')
;  hubDev_compileDirectory, codeDir, SAVFile, /NoLog
;  
;  ; create IDLDOC documentation
;  
;  helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
;
;  idldoc, ROOT=codeDir $
;        , OUTPUT=helpOutputDir $
;        , TITLE='ASE Documentation' $
;        , SUBTITLE='remote sensing made easy' $
;        , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') $
;        , FORMAT_STYLE='rst' $
;        , /COLOR_OUTPUTLOG $
;        , /QUIET $
;        , USER=1
;
;  ; open documentation
;  
;  (hubHelper()).openFile, /HTML, filepath('index.html', ROOT_DIR=helpOutputDir)
;  
;end

pro ase_make
  ;definiere alle benötigten Ordner und Dateipfade (hier zentral, egal ob sie wirklich benötigt werden)
  appDir = ase_getDirname()
  SAVFile = filepath('ASE.sav', ROOT=appDir, SUBDIR='lib')
  codeDir =  file_dirname((routine_info('ase_make',/SOURCE)).path)
  helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  if enmapboxmake.clean() then begin
    ; de-install application by deleting the application folder (NOT the source code!)
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
  ;Hier wird alles kopiert. Wenn Deine Tabelle.sav in einem der "_xyz" Ordner liegt wird sie mit kopiert
  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  ;und wenn nicht CopyFileOnly gesetzt ist, dann wird der ganze Code
  ;als SAV file kompiliert, IDLDoc erstellt & geöffnet, etc.
  if ~enmapboxmake.CopyOnly() then begin
   ; save routines inside SAVE file
    hubDev_compileDirectory, codeDir, SAVFile, NoLog=enmapboxmake.NoLog()
    ; create IDLDOC documentation
    idldoc, ROOT=codeDir $
          , OUTPUT=helpOutputDir $
          , TITLE='ASE Documentation' $
          , SUBTITLE='remote sensing made easy' $
          , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') $
          , FORMAT_STYLE='rst' $
          , /COLOR_OUTPUTLOG $
          , /QUIET $
          , USER=1
  
    ; open documentation
    if ~enmapboxmake.noShow() then begin
      (hubHelper()).openFile, /HTML, filepath('index.html', ROOT_DIR=helpOutputDir)
    endif
  endif
  
end
