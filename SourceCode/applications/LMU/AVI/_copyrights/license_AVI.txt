AGRICULTURAL VEGETATION INDICES (AVI) 1.0

Copyright: � Ludwig-Maximilians University (LMU) 

The AVI software was developed at Ludwig-Maximilians University (LMU) Munich, Department of Geography, Physical Geography and Remote Sensing.

Programming: Matthias Locherer
Concept: Katja Richter, Tobias Hank and Matthias Locherer.

Homepage
http://www.geographie.uni-muenchen.de/department/fiona/department/fernerkundung/index.html

Contact: 
Matthias Locherer (m.locherer@iggf.geo.uni-muenchen.de)
Tobias Hank (tobias.hank@lmu.de)


Disclaimer
The authors of this software tool accept no responsibility for errors or omissions in this work and shall not be liable for any damage caused by these.

