;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `AVI_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `AVI_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `AVI_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro AVI_application, applicationInfo
  
  ; get global settings for this application
  settings = AVI_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = AVI_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = AVI_processing(parameters, settings)
    
;    if parameters['showReport'] then begin
;      AVI_showReport, reportInfo, settings
;    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_AVI_application
  applicationInfo = Hash()
  AVI_application, applicationInfo

end  
