;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function AVI_getDirname
  
  result = filepath('AVI', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_AVI_getDirname

  print, AVI_getDirname()
  print, AVI_getDirname(/SourceCode)

end
