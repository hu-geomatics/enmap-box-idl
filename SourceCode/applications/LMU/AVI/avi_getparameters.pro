;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function avi_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'AGRICULTURAL VEGETATION INDICES (AVI)'

 hubAMW_frame  
  hubAMW_subframe , /row
   hubAMW_button,  Title=' About ', EventHandler='avi_button1'
   hubAMW_button,  Title=' Help  ', EventHandler='avi_button2'
   hubAMW_button,  Title='Contact', EventHandler='avi_button3'
   ;hubAMW_button,  Title='References', EventHandler='avi_button4'

 hubAMW_frame ;,   Title =  'Input'
    hubAMW_inputImageFilename, 'inputImage', Title='Input-Image'
  
 hubAMW_frame
 

      hubAMW_checkbox, 'all_indices', Title='Select All', Value=0b
  
 hubAMW_frame
  hubAMW_subframe ,   Title =  'STRUCTURAL', /row

  ; hubAMW_subframe, Title = 'NDVI', /row 
   hubAMW_subframe, /row 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index1';;, TOOLTIP='hyperspectral Normalized Difference Vegetation Index, Oppelt (2002)'
    hubAMW_checkbox, 'avi_ndvi1', Title='hNDVI (Opp.)', Value=0b
     hubAMW_button,  Title='i', EventHandler='avi_button_Index2';, TOOLTIP='Normalized Difference Vegetation Index, Aparicio (2002)'
    hubAMW_checkbox, 'avi_ndvi2', Title='NDVI (Apr.) ', Value=0b
     hubAMW_button,  Title='i', EventHandler='avi_button_Index3';, TOOLTIP='Normalized Difference Vegetation Index, Datt (1998)'
    hubAMW_checkbox, 'avi_ndvi3', Title='NDVI (Dat.) ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index4';, TOOLTIP='Normalized Difference Vegetation Index, Haboudane (2004)'
    hubAMW_checkbox, 'avi_ndvi4', Title='NDVI (Hab.) ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index5';, TOOLTIP='Normalized Difference Vegetation Index, Zarco-Tejada (1999)'
    hubAMW_checkbox, 'avi_ndvi5', Title='NDVI (Zar.) ', Value=0b;, 
   hubAMW_subframe, /row   
     hubAMW_button,  Title='i', EventHandler='avi_button_Index6';, TOOLTIP='Modified Chlorophyll Absorption in Reflectance Index, Haboudane et. al. (2004)'
    hubAMW_checkbox, 'avi_str1', Title='MCARI1      ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index7';, TOOLTIP='Modified Chlorophyll Absorption in Reflectance Index, Haboudane et. al. (2004)'
    hubAMW_checkbox, 'avi_str2', Title='MCARI2      ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index8';, TOOLTIP='Improved SAVI with self-adjustment factor L, Qi et al. (1994)'
    hubAMW_checkbox, 'avi_str3', Title='MSAVI       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index9';, TOOLTIP='Modified Triangular Vegetation Index, Haboudane et al. (2004)'
    hubAMW_checkbox, 'avi_str4', Title='MTVI1       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index10';, TOOLTIP='Modified Triangular Vegetation Index, Haboudane et al. (2004)'
    hubAMW_checkbox, 'avi_str5', Title='MTVI2       ', Value=0b;, 
   hubAMW_subframe, /row 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index11';, TOOLTIP='Optimized Soil Adjusted Vegetation Index, Rondeaux et al. (1996)'
    hubAMW_checkbox, 'avi_str6', Title='OSAVI       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index12';, TOOLTIP='Renormalized Difference Vegetation Index, Rougean and Breon (1995)'
    hubAMW_checkbox, 'avi_str7', Title='RDVI        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index13';, TOOLTIP='Spectral Polygon Vegetation Index, Vincini et al. (2006)'
    hubAMW_checkbox, 'avi_str8', Title='SPVI        ', Value=0b;, 
  
  
;   hubAMW_frame
;  hubAMW_subframe ,   Title =  'LAI Derivation', /row
;
;  ; hubAMW_subframe, Title = 'NDVI', /row 
;   hubAMW_subframe, /row 
;     hubAMW_button,  Title='i', EventHandler='avi_button_Indexlai1';;, TOOLTIP='hyperspectral Normalized Difference Vegetation Index, Oppelt (2002)'
;    hubAMW_checkbox, 'avi_lai1', Title='LAI (MSAVI)', Value=0b
;     hubAMW_button,  Title='i', EventHandler='avi_button_Indexlai2';, TOOLTIP='Normalized Difference Vegetation Index, Aparicio (2002)'
;    hubAMW_checkbox, 'avi_lai2', Title='LAI (MTVI2) ', Value=0b
;     hubAMW_button,  Title='i', EventHandler='avi_button_Indexlai3';, TOOLTIP='Normalized Difference Vegetation Index, Datt (1998)'
;    hubAMW_checkbox, 'avi_lai3', Title='LAI (RDVI) ', Value=0b;, 
;     hubAMW_button,  Title='i', EventHandler='avi_button_Indexlai4';, TOOLTIP='Normalized Difference Vegetation Index, Haboudane (2004)'
;    hubAMW_checkbox, 'avi_lai4', Title='LAI (TVI) ', Value=0b;,   
    

  hubAMW_frame 
   hubAMW_subframe, Title =  'CHLOROPHYLL', /row  
     hubAMW_button,  Title='i', EventHandler='avi_button_Index14';, TOOLTIP='Carter Stress Index, Carter (1994)'
    hubAMW_checkbox, 'avi_chl1', Title='CSI1        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index15';, TOOLTIP='Carter Stress Index, Carter (1994)'
    hubAMW_checkbox, 'avi_chl2', Title='CSI2        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index16';, TOOLTIP='Greenness Index, Zarco-Tejada et al. (2005)'
    hubAMW_checkbox, 'avi_chl3', Title='G           ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index17';, TOOLTIP='Gitelson & Merzlyak Index, Gitelson and Merzlyak (1997)'
    hubAMW_checkbox, 'avi_chl4', Title='GM1         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index18';, TOOLTIP='Gitelson & Merzlyak Index, Gitelson and Merzlyak (1997)'
    hubAMW_checkbox, 'avi_chl5', Title='GM2         ', Value=0b;, 
   hubAMW_subframe, /row  
     hubAMW_button,  Title='i', EventHandler='avi_button_Index19';, TOOLTIP='green Normalized Difference Vegetation Index, Datt (1998)'
    hubAMW_checkbox, 'avi_chl6', Title='gNDVI       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index20';, TOOLTIP='Modified Chlorophyll Absorption in Reflectance Index, Daughtry et al. (2000)'
    hubAMW_checkbox, 'avi_chl7', Title='MCARI       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index21';, TOOLTIP='Normalized Phaeophytinization Index, Barnes (1992)'
    hubAMW_checkbox, 'avi_chl8', Title='NPQI        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index22';, TOOLTIP='Photochemical Reflectance Index, Gamon et al. (1992)'
    hubAMW_checkbox, 'avi_chl9', Title='PRI         ', Value=0b;,
     hubAMW_button,  Title='i', EventHandler='avi_button_Index23';, TOOLTIP='Red Edge Inflection Point, Guyot et al. (1988)'
    hubAMW_checkbox, 'avi_chl10', Title='REIP1      ', Value=0b;,
   hubAMW_subframe, /row  
     hubAMW_button,  Title='i', EventHandler='avi_button_Index24';, TOOLTIP='Red Edge Point, Dawson and Curran (1998)'
    hubAMW_checkbox, 'avi_chl11', Title='REP         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index25';, TOOLTIP='Simple Ratio, Datt (1998)'
    hubAMW_checkbox, 'avi_chl12', Title='SRchl       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index26';, TOOLTIP='Simple Ratio at 705 Index, Sims and Gamon (2002)'
    hubAMW_checkbox, 'avi_chl13', Title='SR705       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index27';, TOOLTIP='Transformed Chlorophyll Absorption in Reflectance Index, Haboudane et al. (2002)'
    hubAMW_checkbox, 'avi_chl14', Title='TCARI       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index28';, TOOLTIP='Triangular Vegetation Index, Broge and Leblanc (2000)'
    hubAMW_checkbox, 'avi_chl15', Title='TVI         ', Value=0b;, 
   hubAMW_subframe, /row 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index29';, TOOLTIP='Vogelmann Index, Vogelmann (1993)'
    hubAMW_checkbox, 'avi_chl16', Title='VOG1        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index30';, TOOLTIP='Vogelmann Index, Zarco-Tejada et al. (2001)'
    hubAMW_checkbox, 'avi_chl17', Title='VOG2        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index31';, TOOLTIP='Vogelmann Index, Zarco-Tejada et al. (2001)'
    hubAMW_checkbox, 'avi_chl18', Title='VOG3        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index32';, TOOLTIP='Zarco-Tejada & Miller, Zarco-Tejada et al. (2001)'
    hubAMW_checkbox, 'avi_chl19', Title='ZTM         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index33';, TOOLTIP='Simple Ratio Chlorophyll a, Datt (1998)'
    hubAMW_checkbox, 'avi_chl20', Title='SRa         ', Value=0b;, 
  hubAMW_subframe, /row
     hubAMW_button,  Title='i', EventHandler='avi_button_Index34';, TOOLTIP='Simple Ratio Chlorophyll b, Datt (1998)'
    hubAMW_checkbox, 'avi_chl21', Title='SRb         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index35';, TOOLTIP='Simple Ratio Chlorophyll b2, Datt (1998)'
    hubAMW_checkbox, 'avi_chl22', Title='SRb2        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index36';, TOOLTIP='Simple Ratio Chlorophyll total, Datt (1998)'
    hubAMW_checkbox, 'avi_chl23', Title='SRtot       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index37';, TOOLTIP='Pigment Specific Simple Ratio for Chl a, Blackburn (1998)'
    hubAMW_checkbox, 'avi_chl24', Title='PSSRa       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index38';, TOOLTIP='Pigment Specific Simple Ratio for Chl b, Blackburn (1998)'
    hubAMW_checkbox, 'avi_chl25', Title='PSSRb       ', Value=0b;, 
   hubAMW_subframe, /row  
     hubAMW_button,  Title='i', EventHandler='avi_button_Index39';, TOOLTIP='Leaf Chlorophyll Index, Datt (1999)'
    hubAMW_checkbox, 'avi_chl26', Title='LCI         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index40';, TOOLTIP='M to the Lo, do not Hassel the Hoff!'
    hubAMW_checkbox, 'avi_chl27', Title='MLO         ', Value=0b;, 

  hubAMW_frame 
  hubAMW_subframe,   Title =  'CAROTENOID' , /row  
     hubAMW_button,  Title='i', EventHandler='avi_button_Index41';, TOOLTIP='Anthocyanin Reflectance Index, Gitelson et al. (2002)'
    hubAMW_checkbox, 'avi_car1', Title='ARI         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index42';, TOOLTIP='Carotenoid Reflectance, Gitelson et al. (2002)'
    hubAMW_checkbox, 'avi_car2', Title='CRI         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index43';, TOOLTIP='Carotenoid Reflectance, Gitelson et al. (2002)'
    hubAMW_checkbox, 'avi_car3', Title='CRI2        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index44';, TOOLTIP='Pigment Specific Simple Ratio for Carotenoids, Blackburn (1998)'
    hubAMW_checkbox, 'avi_car4', Title='PSSRc       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index45';, TOOLTIP='Structural Independent Pigment Index, Penuelas and Filella (1998)'
    hubAMW_checkbox, 'avi_car5', Title='SIPI        ', Value=0b;, 

  hubAMW_frame 
  hubAMW_subframe,   Title = 'LEAF WATER'   , /row  
     hubAMW_button,  Title='i', EventHandler='avi_button_Index46';, TOOLTIP='Desease Water Stress Index, Galvão et al. (2005)'
    hubAMW_checkbox, 'avi_wat1', Title='DSWI        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index47';, TOOLTIP='Desease Water Stress Index, Apan et al. (2004)'
    hubAMW_checkbox, 'avi_wat2', Title='DWSI5       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index48';, TOOLTIP='Leaf Water Vegetation Index, Galvão et al. (2005)'
    hubAMW_checkbox, 'avi_wat3', Title='LWVI1       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index49';, TOOLTIP='Leaf Water Vegetation Index, Galvão et al. (2005)'
    hubAMW_checkbox, 'avi_wat4', Title='LWVI2       ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index50';, TOOLTIP='Moisture Stress Index, Hunt et al. (1989)'
    hubAMW_checkbox, 'avi_wat5', Title='MSI         ', Value=0b;, 
   hubAMW_subframe, /row
     hubAMW_button,  Title='i', EventHandler='avi_button_Index51';, TOOLTIP='Normalized Difference Water Index, Gao (1996)'
    hubAMW_checkbox, 'avi_wat6', Title='NDWI        ', Value=0b;,
     hubAMW_button,  Title='i', EventHandler='avi_button_Index52';, TOOLTIP='Plant Water Index, Penuelas et al. (1997)'
    hubAMW_checkbox, 'avi_wat7', Title='PWI         ', Value=0b;,
     hubAMW_button,  Title='i', EventHandler='avi_button_Index53';, TOOLTIP='Simple Ratio Water Index, Zarco-Tejada et al. (2003)'
    hubAMW_checkbox, 'avi_wat8', Title='SRWI        ', Value=0b;,
  
  hubAMW_frame 
  hubAMW_subframe,   Title = 'DRY MATTER'   , /row 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index54';, TOOLTIP='Shortwave Infrared Green Vegetation Index, Lobell et al. (2001)'
    hubAMW_checkbox, 'avi_dm1', Title='SWIRVI      ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index55';, TOOLTIP='Cellulose Absorption Index, Nagler et al. (2003)'
    hubAMW_checkbox, 'avi_dm2', Title='CAI         ', Value=0b ;,
     hubAMW_button,  Title='i', EventHandler='avi_button_Index56';, TOOLTIP='Normalized Difference Lignin Index, Serrano et al. (2002)'
    hubAMW_checkbox, 'avi_dm3', Title='NDLI        ', Value=0b ;,
     hubAMW_button,  Title='i', EventHandler='avi_button_Index57';, TOOLTIP='Normalized Difference Nitrogen Index, Serrano et al. (2002)'
    hubAMW_checkbox, 'avi_dm4', Title='NDNI        ', Value=0b ;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index58';, TOOLTIP='Blue Green Pigment Index, Zarco-Tejada et al. (2005)'
    hubAMW_checkbox, 'avi_dm5', Title='BGI         ', Value=0b ;,
   hubAMW_subframe, /row 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index59';, TOOLTIP='Blue Red Pigment Index, Zarco-Tejada et al. (2005)' 
    hubAMW_checkbox, 'avi_dm6', Title='BRI         ', Value=0b;,
     hubAMW_button,  Title='i', EventHandler='avi_button_Index60';, TOOLTIP='Red-Green Pigment Index, Zarco-Tejada et al. (2005)'
    hubAMW_checkbox, 'avi_dm7', Title='RGI         ', Value=0b ;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index61';, TOOLTIP='Simple Ratio Pigment Index, Penuelas et al. (1995)'
    hubAMW_checkbox, 'avi_dm8', Title='SRPI        ', Value=0b ;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index62';, TOOLTIP='Normalized Pigment Chlorophyll Index, Penuelas et al. (1994)'
    hubAMW_checkbox, 'avi_dm9', Title='NPCI        ', Value=0b ; , 
  
  hubAMW_frame 
  hubAMW_subframe,   Title = 'FLUORESCENCE'   , /row
     hubAMW_button,  Title='i', EventHandler='avi_button_Index63';, TOOLTIP='Curvature Index, Zarco-Tejada et al. (2000)'
    hubAMW_checkbox, 'avi_fl1', Title='CUR         ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index64';, TOOLTIP='Lichtenthaler Index, Lichtenthaler et al. (1996)'
    hubAMW_checkbox, 'avi_fl2', Title='LIC1        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index65';, TOOLTIP='Lichtenthaler Index, Lichtenthaler et al. (1996)'
    hubAMW_checkbox, 'avi_fl3', Title='LIC2        ', Value=0b;, 
     hubAMW_button,  Title='i', EventHandler='avi_button_Index66';, TOOLTIP='Lichtenthaler Index, Lichtenthaler et al. (1996)'
    hubAMW_checkbox, 'avi_fl4', Title='LIC3        ', Value=0b;, 

  hubAMW_frame ;,   Title =  'Output'
    hubAMW_outputFilename, 'outputImage', Title='Output-Image', value='AVI_Output'
    
    

  ;hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b
  

  parameters = hubAMW_manage()


  
  return, parameters
end

;+
; :Hidden:
;-
pro test_avi_getParameters

  ; test your routine
  settings = avi_getSettings()
  parameters = avi_getParameters(settings)
  print, parameters

end  
