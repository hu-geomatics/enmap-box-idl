
function avi_processing, parameters, settings

  tilelines =  100
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  outputImage = hubIOImgOutputImage(parameters['outputImage'])

;;======= 1. Number of Indices; necessary for Bandnames
  idx_counter = 0

if parameters['all_indices'] then begin
parameters['avi_ndvi1'] = 1
parameters['avi_ndvi2'] = 1
parameters['avi_ndvi3'] = 1
parameters['avi_ndvi4'] = 1
parameters['avi_ndvi5'] = 1

parameters['avi_str1'] = 1
parameters['avi_str2'] = 1
parameters['avi_str3'] = 1
parameters['avi_str4'] = 1
parameters['avi_str5'] = 1
parameters['avi_str6'] = 1
parameters['avi_str7'] = 1
parameters['avi_str8'] = 1

parameters['avi_chl1'] = 1
parameters['avi_chl2'] = 1
parameters['avi_chl3'] = 1
parameters['avi_chl4'] = 1
parameters['avi_chl5'] = 1
parameters['avi_chl6'] = 1
parameters['avi_chl7'] = 1
parameters['avi_chl8'] = 1
parameters['avi_chl9'] = 1
parameters['avi_chl10'] = 1
parameters['avi_chl11'] = 1
parameters['avi_chl12'] = 1
parameters['avi_chl13'] = 1
parameters['avi_chl14'] = 1
parameters['avi_chl15'] = 1
parameters['avi_chl16'] = 1
parameters['avi_chl17'] = 1
parameters['avi_chl18'] = 1
parameters['avi_chl19'] = 1
parameters['avi_chl20'] = 1
parameters['avi_chl21'] = 1
parameters['avi_chl22'] = 1
parameters['avi_chl23'] = 1
parameters['avi_chl24'] = 1
parameters['avi_chl25'] = 1
parameters['avi_chl26'] = 1
parameters['avi_chl27'] = 1

parameters['avi_car1'] = 1
parameters['avi_car2'] = 1
parameters['avi_car3'] = 1
parameters['avi_car4'] = 1
parameters['avi_car5'] = 1

parameters['avi_wat1'] = 1
parameters['avi_wat2'] = 1
parameters['avi_wat3'] = 1
parameters['avi_wat4'] = 1
parameters['avi_wat5'] = 1
parameters['avi_wat6'] = 1
parameters['avi_wat7'] = 1
parameters['avi_wat8'] = 1

parameters['avi_dm1'] = 1
parameters['avi_dm2'] = 1
parameters['avi_dm3'] = 1
parameters['avi_dm4'] = 1
parameters['avi_dm5'] = 1
parameters['avi_dm6'] = 1
parameters['avi_dm7'] = 1
parameters['avi_dm8'] = 1
parameters['avi_dm9'] = 1

parameters['avi_fl1'] = 1
parameters['avi_fl2'] = 1
parameters['avi_fl3'] = 1
parameters['avi_fl4'] = 1

endif

;;--- NDVI
    if parameters['avi_ndvi1'] then begin
    print, parameters['avi_ndvi1']
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi2'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi3'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi4'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi5'] then begin
      idx_counter = idx_counter + 1               
      endif 
;;--- LAI
;    if parameters['avi_lai1'] then begin
;      idx_counter = idx_counter + 1               
;    endif 
;       if parameters['avi_lai2'] then begin
;      idx_counter = idx_counter + 1               
;    endif 
;       if parameters['avi_lai3'] then begin
;      idx_counter = idx_counter + 1               
;    endif 
;       if parameters['avi_lai4'] then begin
;      idx_counter = idx_counter + 1               
;    endif  
    
;;--- Structural
    if parameters['avi_str1'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str2'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str3'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str4'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str5'] then begin
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str6'] then begin
      idx_counter = idx_counter + 1               
      endif
    if parameters['avi_str7'] then begin
      idx_counter = idx_counter + 1               
      endif
    if parameters['avi_str8'] then begin
      idx_counter = idx_counter + 1               
      endif
      
;;--- Chlorophyll
    if parameters['avi_chl1'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl2'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl3'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl4'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl5'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl6'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl7'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl8'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl9'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl10'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl11'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl12'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl13'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl14'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl15'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl16'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl17'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl18'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl19'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl20'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl21'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl22'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl23'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl24'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl25'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl26'] then begin
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl27'] then begin
      idx_counter = idx_counter + 1               
     endif
 
;;--- Carotenoid    
  if parameters['avi_car1'] then begin
      idx_counter = idx_counter + 1               
    endif     
  if parameters['avi_car2'] then begin
      idx_counter = idx_counter + 1               
    endif    
  if parameters['avi_car3'] then begin
      idx_counter = idx_counter + 1               
    endif    
  if parameters['avi_car4'] then begin
      idx_counter = idx_counter + 1 
    endif    
  if parameters['avi_car5'] then begin
      idx_counter = idx_counter + 1 
    endif  
    
   
;;--- Leaf Water Content
    if parameters['avi_wat1'] then begin
      idx_counter = idx_counter + 1               
    endif 
    if parameters['avi_wat2'] then begin
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat3'] then begin
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat4'] then begin
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat5'] then begin
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat6'] then begin
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat7'] then begin
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat8'] then begin
      idx_counter = idx_counter + 1               
    endif 
    
;;----- Dry Matter
if parameters['avi_dm1'] then begin
      idx_counter = idx_counter + 1               
     endif     
    if parameters['avi_dm2'] then begin
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_dm3'] then begin
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_dm4'] then begin
      idx_counter = idx_counter + 1 
     endif    
    if parameters['avi_dm5'] then begin
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm6'] then begin
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm7'] then begin
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm8'] then begin
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm9'] then begin
      idx_counter = idx_counter + 1 
     endif    

;;----- Fluorescence
    if parameters['avi_fl1'] then begin
      idx_counter = idx_counter + 1               
     endif     
    if parameters['avi_fl2'] then begin
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_fl3'] then begin
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_fl4'] then begin
      idx_counter = idx_counter + 1 
     endif 
     
         
;;======= 2. Array Bandnames
  myBandnames = MAKE_ARRAY(idx_counter,/STRING)
  idx_counter = 0

;;--- NDVI
    if parameters['avi_ndvi1'] then begin
      idx_name = 'hNDVI (OPPELT)' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_ndvi2'] then begin
      idx_name = 'NDVI (Aparicio)' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_ndvi3'] then begin
      idx_name = 'NDVI (Datt)' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_ndvi4'] then begin
      idx_name = 'NDVI (Haboudane)' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_ndvi5'] then begin
      idx_name = 'NDVI (Zarco-Tejada)' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
     
;;--- LAI
;    if parameters['avi_lai1'] then begin
;      idx_name = 'LAI (MSAVI)' 
;      myBandnames[idx_counter] = idx_name 
;      idx_counter = idx_counter + 1               
;     endif 
;    if parameters['avi_lai2'] then begin
;      idx_name = 'LAI (MTVI2)' 
;      myBandnames[idx_counter] = idx_name 
;      idx_counter = idx_counter + 1               
;     endif 
;    if parameters['avi_lai3'] then begin
;      idx_name = 'LAI (RDVI)' 
;      myBandnames[idx_counter] = idx_name 
;      idx_counter = idx_counter + 1               
;     endif 
;    if parameters['avi_lai4'] then begin
;      idx_name = 'LAI (TVI)' 
;      myBandnames[idx_counter] = idx_name 
;      idx_counter = idx_counter + 1               
;     endif

;;--- Structural
    if parameters['avi_str1'] then begin
      idx_name = '(Structural) MCARI1' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str2'] then begin
      idx_name = '(Structural) MCARI2' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str3'] then begin
      idx_name = '(Structural) MSAVI' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str4'] then begin
      idx_name = '(Structural) MTVI1' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str5'] then begin
      idx_name = '(Structural) MTVI2' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str6'] then begin
      idx_name = '(Structural) OSAVI' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str7'] then begin
      idx_name = '(Structural) RDVI' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
    if parameters['avi_str8'] then begin
      idx_name = '(Structural) SPVI' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
     endif 
          
;;--- Chlorophyll
    if parameters['avi_chl1'] then begin
      idx_name = '(Chlorophyll) CSI1' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
    if parameters['avi_chl2'] then begin
      idx_name = '(Chlorophyll) CSI2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl3'] then begin
      idx_name = '(Chlorophyll) G' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl4'] then begin
      idx_name = '(Chlorophyll) GM1' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl5'] then begin
      idx_name = '(Chlorophyll) GM2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif     
    if parameters['avi_chl6'] then begin
      idx_name = '(Chlorophyll) gNDVI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl7'] then begin
      idx_name = '(Chlorophyll) MCARI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl8'] then begin
      idx_name = '(Chlorophyll) NPQI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl9'] then begin
      idx_name = '(Chlorophyll) PRI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl10'] then begin
      idx_name = '(Chlorophyll) REIP' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl11'] then begin
      idx_name = '(Chlorophyll) REP' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl12'] then begin
      idx_name = '(Chlorophyll) SRchl' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl13'] then begin
      idx_name = '(Chlorophyll) SR705' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl14'] then begin
      idx_name = '(Chlorophyll) TCARI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl15'] then begin
      idx_name = '(Chlorophyll) TVI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl16'] then begin
      idx_name = '(Chlorophyll) VOG1' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl17'] then begin
      idx_name = '(Chlorophyll) VOG2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl18'] then begin
      idx_name = '(Chlorophyll) VOG3' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl19'] then begin
      idx_name = '(Chlorophyll) ZTM' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl20'] then begin
      idx_name = '(Chlorophyll) SRa' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl21'] then begin
      idx_name = '(Chlorophyll) SRb' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl22'] then begin
      idx_name = '(Chlorophyll) SRb2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl23'] then begin
      idx_name = '(Chlorophyll) SRtot' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl24'] then begin
      idx_name = '(Chlorophyll) PSSRa' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl25'] then begin
      idx_name = '(Chlorophyll) PSSRb' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl26'] then begin
      idx_name = '(Chlorophyll) LCI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl27'] then begin
      idx_name = '(Chlorophyll) MLO' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif

;;--- Carotenoid    
    if parameters['avi_car1'] then begin
      idx_name = '(Carotenoid) ARI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif     
    if parameters['avi_car2'] then begin
      idx_name = '(Carotenoid) CRI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_car3'] then begin
      idx_name = '(Carotenoid) CRI2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_car4'] then begin
      idx_name = '(Carotenoid) PSSRc' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_car5'] then begin
      idx_name = '(Carotenoid) SIPI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
     endif
          
;;--- Leaf Water Content
    if parameters['avi_wat1'] then begin
      idx_name = '(Leaf Water) DSWI' 
      myBandnames[idx_counter] = idx_name 
      idx_counter = idx_counter + 1               
    endif 
    if parameters['avi_wat2'] then begin
      idx_name = '(Leaf Water) DSWI5' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat3'] then begin
      idx_name = '(Leaf Water) LWVI1' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat4'] then begin
      idx_name = '(Leaf Water) LWVI2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat5'] then begin
      idx_name = '(Leaf Water) MSI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat6'] then begin
      idx_name = '(Leaf Water) NDWI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat7'] then begin
      idx_name = '(Leaf Water) PWI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 
     if parameters['avi_wat8'] then begin
      idx_name = '(Leaf Water) SRWI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif 

;;----- Dry Matter
    if parameters['avi_dm1'] then begin
      idx_name = '(Dry Matter) SWIRVI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif     
    if parameters['avi_dm2'] then begin
      idx_name = '(Dry Matter) CAI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif    
    if parameters['avi_dm3'] then begin
      idx_name = '(Dry Matter) NDLI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif   
    if parameters['avi_dm4'] then begin
      idx_name = '(Dry Matter) NDNI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif      
    if parameters['avi_dm5'] then begin
      idx_name = '(Dry Matter) BGI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif       
    if parameters['avi_dm6'] then begin
      idx_name = '(Dry Matter) BRI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif        
    if parameters['avi_dm7'] then begin
      idx_name = '(Dry Matter) RGI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif        
    if parameters['avi_dm8'] then begin
      idx_name = '(Dry Matter) SRPI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif        
    if parameters['avi_dm9'] then begin
      idx_name = '(Dry Matter) NPCI' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif   

;;----- Fluorescence
    if parameters['avi_fl1'] then begin
      idx_name = '(Fluorescence) CUR' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif     
    if parameters['avi_fl2'] then begin
      idx_name = '(Fluorescence) LIC1' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif    
    if parameters['avi_fl3'] then begin
      idx_name = '(Fluorescence) LIC2' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif        
    if parameters['avi_fl4'] then begin
      idx_name = '(Fluorescence) LIC3' 
      myBandnames[idx_counter] = idx_name
      idx_counter = idx_counter + 1               
    endif
    
;;======= 3. Wellenlängen
   

   outputImage.setMeta, 'band names', myBandnames

aviB1 = inputimage.locateWavelength(415.00)
aviB2 = inputimage.locateWavelength(420.00)
aviB3 = inputimage.locateWavelength(430.00)
aviB4 = inputimage.locateWavelength(435.00)
aviB5 = inputimage.locateWavelength(440.00)
aviB6 = inputimage.locateWavelength(445.00)
aviB7 = inputimage.locateWavelength(450.00)
aviB8 = inputimage.locateWavelength(500.00)
aviB9 = inputimage.locateWavelength(510.00)
aviB10  = inputimage.locateWavelength(528.00)
aviB11  = inputimage.locateWavelength(531.00)
aviB12  = inputimage.locateWavelength(547.00)
aviB13  = inputimage.locateWavelength(550.00)
aviB14  = inputimage.locateWavelength(554.00)
aviB15  = inputimage.locateWavelength(567.00)
aviB16  = inputimage.locateWavelength(570.00)
aviB17  = inputimage.locateWavelength(650.00)
aviB18  = inputimage.locateWavelength(668.00)
aviB19  = inputimage.locateWavelength(670.00)
aviB20  = inputimage.locateWavelength(672.00)
aviB21  = inputimage.locateWavelength(675.00)
aviB22  = inputimage.locateWavelength(677.00)
aviB23  = inputimage.locateWavelength(680.00)
aviB24  = inputimage.locateWavelength(682.00)
aviB25  = inputimage.locateWavelength(683.00)
aviB26  = inputimage.locateWavelength(690.00)
aviB27  = inputimage.locateWavelength(695.00)
aviB28  = inputimage.locateWavelength(700.00)
aviB29  = inputimage.locateWavelength(705.00)
aviB30  = inputimage.locateWavelength(708.00)
aviB31  = inputimage.locateWavelength(710.00)
aviB32  = inputimage.locateWavelength(715.00)
aviB33  = inputimage.locateWavelength(720.00)
aviB34  = inputimage.locateWavelength(724.00)
aviB35  = inputimage.locateWavelength(726.00)
aviB36  = inputimage.locateWavelength(734.00)
aviB37  = inputimage.locateWavelength(740.00)
aviB38  = inputimage.locateWavelength(747.00)
aviB39  = inputimage.locateWavelength(745.00)
aviB40  = inputimage.locateWavelength(750.00)
aviB41  = inputimage.locateWavelength(760.00)
aviB42  = inputimage.locateWavelength(774.00)
aviB43  = inputimage.locateWavelength(780.00)
aviB44  = inputimage.locateWavelength(800.00)
aviB45  = inputimage.locateWavelength(802.00)
aviB46  = inputimage.locateWavelength(820.00)
aviB47  = inputimage.locateWavelength(827.00)
aviB48  = inputimage.locateWavelength(850.00)
aviB49  = inputimage.locateWavelength(858.00)
aviB50  = inputimage.locateWavelength(860.00)
aviB51  = inputimage.locateWavelength(900.00)
aviB52  = inputimage.locateWavelength(970.00)
aviB53  = inputimage.locateWavelength(983.00)
aviB54  = inputimage.locateWavelength(1094.00)
aviB55  = inputimage.locateWavelength(1205.00)
aviB56  = inputimage.locateWavelength(1240.00)
aviB57  = inputimage.locateWavelength(1510.00)
aviB58  = inputimage.locateWavelength(1600.00)
aviB59  = inputimage.locateWavelength(1657.00)
aviB60  = inputimage.locateWavelength(1660.00)
aviB61  = inputimage.locateWavelength(1680.00)
aviB62  = inputimage.locateWavelength(2015.00)
aviB63  = inputimage.locateWavelength(2090.00)
aviB64  = inputimage.locateWavelength(2106.00)
aviB65  = inputimage.locateWavelength(2195.00)
aviB66  = inputimage.locateWavelength(2208.00)
aviB67  = inputimage.locateWavelength(2210.00) 
  
  inputimage.initReader, tilelines, /TILEPROCESSING, /SLICE, $
     SubsetbandPositions=[aviB1,aviB2,aviB3,aviB4,aviB5,aviB6,aviB7,aviB8,aviB9,aviB10,$
                 aviB11,aviB12,aviB13,aviB14,aviB15,aviB16,aviB17,aviB18,aviB19,aviB20,$
                 aviB21,aviB22,aviB23,aviB24,aviB25,aviB26,aviB27,aviB28,aviB29,aviB30,$
                 aviB31,aviB32,aviB33,aviB34,aviB35,aviB36,aviB37,aviB38,aviB39,aviB40,$
                 aviB41,aviB42,aviB43,aviB44,aviB45,aviB46,aviB47,aviB48,aviB49,aviB50,$
                 aviB51,aviB52,aviB53,aviB54,aviB55,aviB56,aviB57,aviB58,aviB59,aviB60,$
                 aviB61,aviB62,aviB63,aviB64,aviB65,aviB66,aviB67], Datatype='float'
    ;SubsetbandPositions=[  0   ,  1   ,  2   ,  3   ,  4   ]
  
  outputImage.copyMeta, inputimage, /COPYSPATIALINFORMATION  
  outputImage.initWriter, inputimage.getWriterSettings(SETBANDS=idx_counter) ; SETBANDS= idx_counter
  
    ;; ----- progressbar2
    progressBar = hubProgressBar(Title=settings.hubGetValue('title') $
      , GroupLeader = settings.hubGetValue('groupLeader'))
    progressBar.setInfo, 'Calculating Indices...'
    progressBar.setRange, [0,inputImage.getMeta('lines')]
    progressDone=0
  ;; ----- end progressbar2

;============ IV.0 Abfrage der Statistik

  median_data = AVI_imagestats(parameters) 
  help, median_data
  
;============ IV. CALCULATING
  exportsize = idx_counter
;help, avi_indices  
  WHILE ~inputimage.tileProcessingDone() DO BEGIN
  
    data = inputimage.getData()
    
;; === image statistics check
 if (median_data LT 10000) and (median_data GT 1000) then begin
  data=TEMPORARY(data)/10000
 endif else begin
 ;---
  if (median_data LT 1000) and (median_data GT 100) then begin
   data=TEMPORARY(data)/1000
  endif else begin
   ;--
   if (median_data LT 100) and (median_data GT 10) then begin
   data=TEMPORARY(data)/100
   endif else begin
    ;-
    if (median_data LT 10) and (median_data GT 1) then begin
   data=TEMPORARY(data)/10
    endif else begin
    endelse
    ;-
   endelse
   ;--
  endelse
 ;---   
 endelse
;; === check end     
    
    row_counter = N_ELEMENTS(data[1,*])
    avi_indices = MAKE_ARRAY(exportsize,row_counter)   
    
    idx_counter = 0
  

;;=== NDVI
 
    if parameters['avi_ndvi1'] then begin
      avi_indices[idx_counter,*] = (data[46,*]-data[17,*]) / (data[46,*]+data[17,*])
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi2'] then begin
      avi_indices[idx_counter,*] = (data[50,*]-data[22,*]) / (data[50,*]+data[22,*])
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi3'] then begin
      avi_indices[idx_counter,*] = (data[43,*]-data[22,*]) / (data[43,*]+data[22,*])
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi4'] then begin
      avi_indices[idx_counter,*] = (data[43,*]-data[18,*]) / (data[43,*]+data[18,*])
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_ndvi5'] then begin
      avi_indices[idx_counter,*] = (data[41,*]-data[21,*]) / (data[41,*]+data[21,*])
      idx_counter = idx_counter + 1               
      endif 

;;=== Structure
    if parameters['avi_str1'] then begin
         mcari1_a = 2.5*(data[43,*]-data[18,*])
         mcari1_b = 1.3*(data[43,*]-data[12,*])
      avi_indices[idx_counter,*] = 1.2*(mcari1_a - mcari1_b)
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str2'] then begin
         mcari2_a = 2.5*(data[43,*]-data[18,*])
         mcari2_b = 1.3*(data[43,*]-data[12,*])
         mcari2_c = 1.5*(mcari2_a - mcari2_b) 
         mcari2_d = ((2*data[43,*])+1)^2 
         mcari2_e = (6*data[43,*])-(5*data[18,*])
         mcari2_f = SQRT(mcari2_d - mcari2_e - 0.5)  
      avi_indices[idx_counter,*] = mcari2_c / mcari2_f
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str3'] then begin
         msavi_a = (2*data[43,*])+1
         msavi_b = msavi_a^2
         msavi_c = 8*(data[43,*]-data[18,*])
         msavi_d = SQRT(msavi_b - msavi_c)         
      avi_indices[idx_counter,*] = 0.5*(msavi_a - msavi_d)
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str4'] then begin
         mtvi1_a = 1.2*(data[43,*] - data[12,*])
         mtvi1_b = 2.5*(data[18,*] - data[12,*])
      avi_indices[idx_counter,*] = 1.2*(mtvi1_a - mtvi1_b)
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str5'] then begin
         mtvi2_a = 1.2*(data[43,*] - data[12,*])
         mtvi2_b = 2.5*(data[18,*] - data[12,*])
         mtvi2_c = 1.5*(mtvi2_a - mtvi2_b)
         mtvi2_d = ((2*data[43,*])+1)^2
         mtvi2_e1 = 6*data[43,*]
         mtvi2_e2 = 5*SQRT(data[18,*])
         mtvi2_e = mtvi2_e1 - mtvi2_e2
         mtvi2_f = SQRT(mtvi2_d - mtvi2_e - 0.5)
      avi_indices[idx_counter,*] = mtvi2_c / mtvi2_f
      idx_counter = idx_counter + 1               
      endif 
    if parameters['avi_str6'] then begin
         osavi_a = 1.16*(data[43,*] - data[18,*])
         osavi_b = data[43,*] + data[18,*] + 0.16
      avi_indices[idx_counter,*] = osavi_a / osavi_b
       idx_counter = idx_counter + 1               
      endif
    if parameters['avi_str7'] then begin
         rdvi_a = data[43,*] - data[18,*]
         rdvi_b = SQRT(data[43,*] + data[18,*])
      avi_indices[idx_counter,*] = rdvi_a / rdvi_b
      idx_counter = idx_counter + 1               
      endif
    if parameters['avi_str8'] then begin
         spvi_a = 3.7*(data[43,*] - data[18,*])
         spvi_b = 1.2*(data[10,*] - data[18,*])
      avi_indices[idx_counter,*] = 0.4*(spvi_a - spvi_b)
      idx_counter = idx_counter + 1               
      endif
      
;;=== LAI  
;    if parameters['avi_lai1'] then begin
;         msavi_a = (2*data[43,*])+1
;         msavi_b = msavi_a^2
;         msavi_c = 8*(data[43,*]-data[18,*])
;         msavi_d = SQRT(msavi_b - msavi_c)         
;         msavi = 0.5*(msavi_a - msavi_d)
;        avi_indices[idx_counter,*] = 0.1663*exp(4.2731*MSAVI)
;      idx_counter = idx_counter + 1               
;       endif 
;    if parameters['avi_lai2'] then begin
;        aaa = 1.2*(data[43,*]-data[12,*])
;        aab = 2.5*(data[18,*]-data[12,*])
;        aa = 1.5*(aaa-aab)
;        bba = ((2*data[43,*])+1)^2
;        bbb = (6*data[43,*])-(5*SQRT(data[18,*]))
;        bb = SQRT((bba-bbb)-0.5)
;        MTVI2 = aa / bb
;      avi_indices[idx_counter,*] = 0.2227*exp(3.6566*MTVI2)
;      idx_counter = idx_counter + 1               
;       endif 
;    if parameters['avi_lai3'] then begin
;         RDVI_a = data[43,*]-data[18,*]
;         RDVI_b = data[43,*]+data[18,*] 
;         RDVI = RDVI_a / (SQRT(RDVI_b))
;      avi_indices[idx_counter,*] = 0.0918*EXP(6.0002*RDVI)
;      idx_counter = idx_counter + 1               
;       endif 
;    if parameters['avi_lai4'] then begin
;        TVI_a = 120*(data[39,*]-data[12,*])
;        TVI_b = 200*(data[18,*]-data[12,*])
;        TVI = 0.5*(TVI_a - TVI_b)
;      avi_indices[idx_counter,*] = 0.1817*EXP(4.1469*TVI)
;      idx_counter = idx_counter + 1               
;    endif        

;;=== Chlorophyll
    
  if parameters['avi_chl1'] then begin
      avi_indices[idx_counter,*] = data[26,*] / data[1,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl2'] then begin
      avi_indices[idx_counter,*] = data[26,*] / data[40,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl3'] then begin
      avi_indices[idx_counter,*] = data[13,*] / data[21,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl4'] then begin
      avi_indices[idx_counter,*] = data[13,*] / data[21,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl5'] then begin
      avi_indices[idx_counter,*] = data[39,*] / data[27,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl6'] then begin
      avi_indices[idx_counter,*] = (data[39,*] - data[12,*]) / (data[39,*] + data[12,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl7'] then begin
      avi_indices[idx_counter,*] = ((data[27,*]-data[18,*]) - 0.2*(data[27,*]-data[12,*]))*(data[27,*]/data[18,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl8'] then begin
      avi_indices[idx_counter,*] = (data[0,*] - data[3,*]) / (data[0,*] + data[3,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl9'] then begin
      avi_indices[idx_counter,*] = (data[9,*] - data[14,*]) / (data[9,*] + data[14,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl10'] then begin
      avi_indices[idx_counter,*] = 700+(740/700) * (((data[42,*] / data[18,*]) - data[14,*])/(data[36,*] + data[27,*]))
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl11'] then begin
      avi_indices[idx_counter,*] = 700+ (40*((((data[18,*]+data[42,*])/2)-data[27,*])/(data[36,*]+data[27,*])))
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl12'] then begin
      avi_indices[idx_counter,*] = data[19,*] / (data[12,*]*data[29,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl13'] then begin
      avi_indices[idx_counter,*] = data[39,*] / data[28,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl14'] then begin
      avi_indices[idx_counter,*] = 3*((data[27,*]-data[18,*]) - 0.2*(data[27,*]-data[12,*]))*(data[27,*]/data[18,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl15'] then begin
      avi_indices[idx_counter,*] = 0.5*(120*(data[39,*]-data[12,*]) - 200*(data[18,*]-data[12,*]))
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl16'] then begin
      avi_indices[idx_counter,*] = data[36,*] / data[32,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl17'] then begin
      avi_indices[idx_counter,*] = (data[35,*] - data[37,*]) / (data[31,*] + data[32,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl18'] then begin
      avi_indices[idx_counter,*] = (data[35,*] - data[37,*]) / (data[31,*] + data[34,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl19'] then begin
      avi_indices[idx_counter,*] = data[39,*] / data[30,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl20'] then begin
      avi_indices[idx_counter,*] = data[20,*] / data[27,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl21'] then begin
      avi_indices[idx_counter,*] = data[20,*] / (data[16,*]*data[27,*])
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl22'] then begin
      avi_indices[idx_counter,*] = data[19,*] / data[29,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl23'] then begin
      avi_indices[idx_counter,*] = data[40,*] / data[12,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl24'] then begin
      avi_indices[idx_counter,*] = data[43,*] / data[20,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl25'] then begin
      avi_indices[idx_counter,*] = data[43,*] / data[16,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl26'] then begin
      avi_indices[idx_counter,*] = (data[47,*]-data[30,*]) / data[47,*]+data[22,*]
      idx_counter = idx_counter + 1               
     endif
    if parameters['avi_chl27'] then begin
      avi_indices[idx_counter,*] = data[38,*] / data[33,*]
      idx_counter = idx_counter + 1               
     endif
    
;;=== CAROTENOIDS    
  if parameters['avi_car1'] then begin
     avi_indices[idx_counter,*] = (1/data[12,*]) - (1/data[27,*])
      idx_counter = idx_counter + 1           
    endif   
  if parameters['avi_car2'] then begin
     avi_indices[idx_counter,*] = (1/data[8,*]) - (1/data[12,*])
      idx_counter = idx_counter + 1           
    endif  
  if parameters['avi_car3'] then begin
     avi_indices[idx_counter,*] = (1/data[8,*]) - (1/data[27,*])
      idx_counter = idx_counter + 1           
    endif  
  if parameters['avi_car4'] then begin
     avi_indices[idx_counter,*] = data[43,*] / data[7,*]
      idx_counter = idx_counter + 1           
    endif
  if parameters['avi_car5'] then begin
     avi_indices[idx_counter,*] = (data[5,*]-data[43,*]) / (data[22,*]-data[43,*])
      idx_counter = idx_counter + 1           
    endif

;;=== LEAF WATER
   
  if parameters['avi_wat1'] then begin
      avi_indices[idx_counter,*] = (data[44,*]-data[11,*]) / (data[58,*]+data[23,*])
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat2'] then begin
      avi_indices[idx_counter,*] = (data[43,*]+data[12,*]) / (data[59,*]+data[22,*])
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat3'] then begin
      avi_indices[idx_counter,*] = (data[53,*]-data[52,*]) / (data[53,*]+data[52,*])
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat4'] then begin
      avi_indices[idx_counter,*] = (data[53,*]-data[54,*]) / (data[53,*]+data[54,*])
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat5'] then begin
      avi_indices[idx_counter,*] = data[57,*]/data[45,*]
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat6'] then begin
      avi_indices[idx_counter,*] = (data[49,*]-data[55,*]) / (data[49,*]+data[55,*])
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat7'] then begin
      avi_indices[idx_counter,*] = data[51,*]/data[50,*]
      idx_counter = idx_counter + 1               
    endif 
  if parameters['avi_wat8'] then begin
      avi_indices[idx_counter,*] = data[48,*]/data[55,*]
      idx_counter = idx_counter + 1               
    endif    

;;=== Dry MAtter
    if parameters['avi_dm1'] then begin
       avi_indices[idx_counter,*] = (37.27*(data[66,*]+data[62,*])) + (26.27*(data[65,*]-data[62,*])) - 0.57
      idx_counter = idx_counter + 1               
     endif     
    if parameters['avi_dm2'] then begin
       avi_indices[idx_counter,*] = (0.5*(data[61,*]+data[64,*])) - data[63,*]
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_dm3'] then begin
       avi_indices[idx_counter,*] = ((1/ALOG(data[53,*]))-(1/ALOG(data[54,*]))) / ((1/ALOG(data[53,*]))+(1/ALOG(data[54,*])))
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_dm4'] then begin
       avi_indices[idx_counter,*] = ((1/ALOG(data[56,*]))-(1/ALOG(data[60,*]))) / ((1/ALOG(data[56,*]))+(1/ALOG(data[60,*])))
      idx_counter = idx_counter + 1 
     endif    
    if parameters['avi_dm5'] then begin
       avi_indices[idx_counter,*] = data[6,*] / data[12,*]
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm6'] then begin
       avi_indices[idx_counter,*] = data[6,*] / data[25,*]
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm7'] then begin
       avi_indices[idx_counter,*] = data[25,*] / data[12,*]
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm8'] then begin
       avi_indices[idx_counter,*] = data[2,*] / data[22,*]
      idx_counter = idx_counter + 1 
     endif
    if parameters['avi_dm9'] then begin
       avi_indices[idx_counter,*] = (data[22,*]-data[2,*]) / (data[22,*]+data[2,*])
      idx_counter = idx_counter + 1 
     endif

;;=== FLUORESCENCE
    if parameters['avi_fl1'] then begin
       avi_indices[idx_counter,*] = (data[20,*]*data[12,*]) / (data[24,*])^2
      idx_counter = idx_counter + 1               
     endif     
    if parameters['avi_fl2'] then begin
       avi_indices[idx_counter,*] = (data[43,*]-data[22,*]) / (data[43,*] + data[22,*])
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_fl3'] then begin
       avi_indices[idx_counter,*] = data[4,*] / data[25,*]
      idx_counter = idx_counter + 1               
     endif    
    if parameters['avi_fl4'] then begin
       avi_indices[idx_counter,*] = data[4,*] / data[36,*]
      idx_counter = idx_counter + 1 
     endif 

  outputImage.writeData, avi_indices
  
  ;; ----- progressbar
      progressDone += tilelines
      progressBar.setProgress, progressDone
  ;; ----- end progressbar
    
  ENDWHILE

;============  V. CLEANUP
  inputImage.cleanup
  outputImage.cleanup
  progressBar.cleanup
  
  
  
;;==================================================================================================  


end
;
;;+
;; :Hidden:
;;-
pro test_avi_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = avi_getSettings()
  
  result = avi_processing(parameters, settings)
  print, result

end  
