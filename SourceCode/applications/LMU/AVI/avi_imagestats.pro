function AVI_imagestats, parameters

wl_NIR = 750.00
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  NIR = inputImage.locateWavelength(wl_NIR)

  inputImage.initReader, /CUBE, $
   SubsetbandPositions=NIR, Datatype='float'
   data_stat = inputImage.getData()
   
     help, data_stat
     uni_data = data_stat[UNIQ(data_stat)]
     help, uni_data
     med_data = MEDIAN(uni_data)
     help, med_data 
    
 return, med_data    


end