;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('AVI.conf', ROOT=AVI_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, AVI_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function AVI_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('avi.conf', ROOT=AVI_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_AVI_getSettings

  print, AVI_getSettings()

end
