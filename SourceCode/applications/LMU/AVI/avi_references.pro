PRO avi_references_event
cmd = WIDGET_INFO(event.id,/UNAME)  

  CASE cmd OF
'quit':  WIDGET_CONTROL,event.top,/DESTROY
   ELSE:
  ENDCASE      
END


PRO avi_references

base = WIDGET_BASE(row=3, MBAR = mbar, /TAB_MODE)

;-----a_base


   
   a_base = WIDGET_BASE(base, row=51, /FRAME, Y_SCROLL_SIZE=400, /SCROLL, YSIZE=2400)
   
   ;title1 = WIDGET_LABEL(a_base, VALUE = 'ASE - short description', /ALIGN_LEFT, XSIZE=200)
   
   txt1 = widget_text(a_base, value='APAN, A.; HELD, A.; PHINN, S.; MARKLEY, J. Detecting sugarcane orange rust disease using EO-1 Hyperion hyperspectral imagery. Int. J. Remote Sens. 2004, 25, 489-498.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info1', UNITS=0) 
   txt2 = widget_text(a_base, value='APARICIO, N., VILLEGAS, D., ARAUS, J. L., CASADESUS, J., & ROYO, C. (2002). Relationship between growth traits and spectral vegetation indices in Durum Wheat. Crop Science, 42, 1547-1555. ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info2', UNITS=0)
      txt = widget_text(a_base, value='BARET, F.; GUYOT, G.; MAJOR, D.J. TSAVI: a vegetation index which minimizes soil brightness effects on LAI and APAR estimation. In Proceedings of Quantitative Remote Sensing: An Economic Tool for the Nineties, IGARSS 1989 and 12th Canadian Symposium on Remote Sensing, Vancouver, Canada, 1989; pp. 1355-1358.', $
   FONT='Helvetica', xsize=100, ysize=3,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='BARNES, J.D. 1992. A reappraisal of the use of DMSO for the extraction and determination of chlorophylls a and b in lichens and higher plants. Environ. Exp. Bot. 2:85-100.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='BLACKBURN, G. A. 1998. Spectral indices for estimating photosynthetic pigment concentrations: a test using senescent tree leaves. Int. J. Remote Sens. 19: 657D675.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='BROGE, N.H., and E. LEBLANC. 2000. Comparing prediction power and stability of broadband and hyperspectral vegetation indices for estimation of green leaf area index and canopy chlorophyll density. Remote Sens. Environ. 76:156-172.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='CARTER, G.A. Ratios of leaf reflectances in narrow wavebands as indicators of plant stress. Int. J. Remote Sens. 1994, 15, 697-704.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='CHEN, J. 1996. Evaluation of vegetation indices and modified simple ratio for boreal applications. Can. J. Remote Sens. 22:229-242.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='DATT, B., 1999. A new reflectance index for remote sensing of chlorophyll content in higher plants: tests using Eucalyptus leaves. J. Plant Physiol., 154, 30-36.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
      txt = widget_text(a_base, value='DATT, B.: "Remote sensing of chlorophyll a, chlorophyll b, chlorophyll a+b and total carotenoid content in eucalyptus leaves", in "Remote sensing of the environment" 66,p. 111-121, 1998.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='DAUGHTRY, C.S.T., C.L. WALTHALL, M.S. KIM, E. BROWN DE COLSTOUN, and J.E. McMURTREY III. 2000. Estimating corn leaf chlorophyll concentration from leaf and canopy reflectance. Remote Sens. Environ. 74:229-239.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='DAWSON, T.P. and CURRAN, P.J., 1998. A new technique for interpolating red edge position. International Journal of Remote Sensing, 19(11): 2133-2139.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='GALVAO, L. S., FORMAGGIO, A. R., and TISOT, D. A., 2005. Discrimination of sugarcane varieties in Southeastern Brazil with EO-1 Hyperion data. Remote Sensing of Environment, 94, 523-534.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='GAMON, J.A., J. PENUELAS, and C.B. FIELD. 1992. A narrow-waveband spectral index that tracks diurnal changes in photosynthetic efficiency. Remote Sens. Environ. 41:35-44.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='GAO, B.-C. 1996. NDWI - a normalized difference water index for remote sensing of vegetation liquid water from space. Remote Sens. Environ. 58:257-266.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='GITELSON, A.A., ZUR, Y., CHIVKUNOVA, O.B. and MERZLYAK, M.N., 2002. Assessing carotenoid content in plant leaves with reflectance spectroscopy, Photochemistry and Photobiology 75, pp. 272-281.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='GITELSON, A. A., & MERZLYAK, M. N. (1997): Remote estimation of chlorophyll content in higher plant leaves. International Journal of Remote Sensing 18, 2691-2697.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='GUYOT, G.; BARET, F.; MAJOR, D.J. High spectral resolution: determination of spectral shifts between the red and the near infrared. Int. Arch. Photogramm Remote Sen. 1988, 11, 750-760.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='HABOUDANE, D., J.R. MILLER, N. TREMBLAY, P.J. ZARCO-TEJADA, and L. DEXTRAZE. 2002. Integration of hyperspectral vegetation indices for prediction of crop chlorophyll content for application to precision agriculture. Remote Sens. Environ. 81(2-3):416-426.', $
   FONT='Helvetica', xsize=100, ysize=3,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='HABOUDANE, D., MILLER, J. R., PATTEY, E., ZARCO-TEJADA, P. J., & STRACHAN, I. (2004): Hyperspectral vegetation indices and novel algorithms for predicting green LAI of crop canopies: Modeling and validation in the context of precision agriculture. Remote Sensing of Environment 90(3), 337-352.', $
   FONT='Helvetica', xsize=100, ysize=3,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='HUETE, A.R. 1988. A soil-adjusted vegetation index (SAVI). Remote Sens. Environ. 25:295-309.   ', $
   FONT='Helvetica', xsize=100, ysize=1,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='HUNT, J.; RAYMOND, E.; ROCK, B.N. Detection of changes in leaf water content using Near- and Middle-Infrared reflectances. Remote Sens. Environ. 1989, 30, 43-54.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='JORDAN, C.F. 1969. Derivation of leaf area index from quality of light on the forest floor. Ecology 50:663-666.         ', $
   FONT='Helvetica', xsize=100, ysize=1,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='LICHTENHALER, H. K., LANG, M., SOWINSKA, M., HEISEL, F., & MIEH, J. A. (1996): Detection of vegetation stress via a new high resolution fluorescence imaging system. Journal of Plant Physiology 148, 599-612.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='LOBELL, D.B.; ASNER, G.P.; LAW, B.E.; TREUHAFT, R.N. Subpixel canopy cover estimation of coniferous forests in Oregon using SWIR imaging spectrometry. J. Geophys. Res. D. Atmos. 2001, 106, 5151-5160.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='MAJOR, D.J.; BARET, F.; GUYOT, G. A vegetation index adjusted for soil brightness. Int. J. Remote Sens. 1990, 11, 722-740.         ', $
   FONT='Helvetica', xsize=100, ysize=1,/WRAP, uname='info', UNITS=0) 
         txt = widget_text(a_base, value='NAGLER, P.L.; INOUE, Y.; GLENN, E.P.; RUSS, A.L.; DAUGHTRY, C.S.T. Cellulose absorption index (CAI) to quantify mixed soil-plant litter scenes. Remote Sens. Environ. 2003, 87, 310-325.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='OPPELT, N., MAUSER, W. (2003): The Use of an Integral Approach to derive the Pigment Content of Agricultural Plants. 3rd EARSeL Workshop on Imaging Spectroscopy, 13-16 May 2003 Herrsching, Germany         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='PENUELAS, J., and FILELLA, I., 1998. Visible and near-infrared reflectance techniques for diagnosing plant physiological status. Trends in Plant Science, 3, 151-156.', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='PENULEAS, J., GAMON, J.A., FREDEEN, A.L., MERINO, J., and FIELD, C.B., 1994. Refelctance indices associated with physiological changes in nitrogen- and water-limited sunflower leaves. Remote Sens. Environ., 48, 135-146.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='PENUELAS, J., I. FILELLA, P. LLORET, F. MUNOZ, and M. VILAJELIU. 1995. Reflectance assessment of mite effects on apple trees. Int. J. Remote Sens. 16:2727-2733.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='PENUELAS, J., J. PINOL, R. OGAYA, and I. FILELLA. 1997. Estimation of plant water concentration by the reflectance Water Index (R900/R970). Int. J. Remote Sens. 18:2869-2875.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='PENUELAS, J., J.A. GAMON, A.L. FREDEEN, J. MERINO, and C.B. FIELD. 1994. Reflectance indices associated with physiological changes in nitrogen- and water-limited sunflower leaves. Remote Sens. Environ. 48:135-146.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='QI, J., A. CHEHBOUNI, A.R. HUETE, Y.H. KEER, and S. SOROOSHIAN. 1994. A modified soil vegetation adjusted index. Remote Sens. Environ. 48:119-126.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='RONDEAUX, G., M. STEVEN, and F. BARET. 1996. Optimization of soil-adjusted vegetation indices. Remote Sens. Environ. 55:95-107.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ROUGEAN, J.-L., and F.M. BREON. 1995. Estimating PAR absorbed by vegetation from bidirectional reflectance measurements. Remote Sens. Environ. 51:375-384.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ROUSE, J.W., R.H. HAAS, J.A. SCHELL, D.W. DEERING, and J.C. HARLAN. 1974. Monitoring the vernal  advancements and retrogradation of natural vegetation. NASA/GSFC, Greenbelt, MD.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='SERRANO, L.; PENUELAS, J.; USTIN, S.L. Remote sensing of nitrogen and lignin in Mediterranean vegetation from AVIRIS data: Decomposing biochemical from structural signals. Remote Sens. Environ. 2002, 81, 355-364.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='SIMS, D.A.; GAMON, J.A. Relationships between leaf pigment content and spectral reflectance across a wide range of species, leaf structures and developmental stages. Remote Sens. Environ. 2002, 81, 337-354.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='VINCINI, M., FRAZZI, E. and D ALESSIO, P. Angular dependence of maize and sugar beet Vis from directional CHRIS/PROBA data, 4th ESA CHRIS PROBA Workshop, ESRIN, Frascati, Italy (2006), pp. 19-21.Vincini et al. (2006)         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='VOGELMANN, J. E., ROCK, B. N., & MOSS, D. M. (1993). Red edge spectral measurements from sugar maple leaves. International Journal of Remote Sensing, 14, 1563-1575.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ZARCO-TEJADA, P. J., MILLER, J. R., MOHAMMED, G. H., & NOLAND, T. L. (2000): Chlorophyll fluorescence effects on vegetation apparent reflectance: I. Leaflevel measurements and simulation of reflectance and transmittance spectra. Remote Sensing of Environment, 74(3), 582-595.         ', $
   FONT='Helvetica', xsize=100, ysize=3,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ZARCO-TEJADA, P. J., MILLER, J. R., MOHAMMED, G. H., NOLAND, T. L., & SAMPSON, P. H. (2001): Scaling-up and model inversion methods with narrow-band optical indices for chlorophyll content estimation in closed forest canopies with hyperspectral data. IEEE Transactions on Geoscience and Remote Sensing, 39(7), 1491-1507.         ', $
   FONT='Helvetica', xsize=100, ysize=3,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ZARCO-TEJADA, P. J., WHITING, M., & USTIN, S. L. (2005): Temporal and spatial relationships between within-field yield variability in cotton and highspatial hyperspectral remote sensing imagery. Agronomy Journal, Vol.97 (No. 3), 641-653.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ZARCO-TEJADA, P.J., C.A. RUEDA, and S.L. USTIN. 2003. Water content estimation in vegetation with MODIS reflectance data and model inversion methods. Remote Sens. Environ. 85(1):109-124.         ', $
   FONT='Helvetica', xsize=100, ysize=2,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ZARCO-TEJADA, P. J., MILLER, J. R., MOHAMMED, G. H., NOLAND, T. L., & SAMPSON, P. H. 1999. Canopy optical indices from infinite reflectance and canopy reflectance models for forest condition monitoring: Application to hyperspectral CASI data. In IEEE 1999 Int. Geosci. and Remote Sensing Symp., IGARSS 1999, Hamburg, Germany. 28 June-2 July 1999. Proc. of the IEEE, Piscataway, NJ.         ', $
   FONT='Helvetica', xsize=100, ysize=4,/WRAP, uname='info', UNITS=0)
         txt = widget_text(a_base, value='ZARCO-TEJADA, P.J.; BERJON, A.; LOPEZ-LOZANO, R.; MILLER, J.R.; MARTIN, P.; CACHORRO, V.; GONZALES, M.R.; de FRUTOS, A. Assessing vineyard condition with hyperspectral indices: Leaf and canopy reflectance simulation in a row-structured discontinuous canopy. Remote Sens. Environ. 2005, 99, 271-287.         ', $
   FONT='Helvetica', xsize=100, ysize=3,/WRAP, uname='info', UNITS=0) 

   
     
  
 
    
menu = WIDGET_BUTTON(mbar, /MENU, VALUE='Menu')
  quit = WIDGET_BUTTON(menu, VALUE='Quit', UNAME='quit')
    
;; Ende einleiten
WIDGET_CONTROL, base, /REALIZE

XMANAGER, 'avi_references', base     ;; notwendig für die Bedienung der Knöpfe, ruft den Event-Manager hinzu

END