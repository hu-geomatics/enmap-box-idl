;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function ASE2_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = 'ASE-point'
  hubAMW_label, 'ADVANCED STATISTICAL EVALUATOR (ASE)'
  hubAMW_label, 'point to point data'

 hubAMW_frame  
  hubAMW_subframe , /row
   hubAMW_button,  Title=' About ', EventHandler='ase2_button1'
   hubAMW_button,  Title=' Help  ', EventHandler='ase2_button2'
   hubAMW_button,  Title='Contact', EventHandler='ase2_button3'

  parameters = hubAMW_manage()
  return, parameters
  
end

;+
; :Hidden:
;-
pro test_ASE2_getParameters

  ; test your routine
  settings = ASE2_getSettings()
  parameters = ASE2_getParameters(settings)
  print, parameters

end  
