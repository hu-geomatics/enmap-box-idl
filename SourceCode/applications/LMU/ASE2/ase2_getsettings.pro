;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('ASE2.conf', ROOT=ASE2_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, ASE2_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function ASE2_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('ase2.conf', ROOT=ASE2_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_ASE2_getSettings

  print, ASE2_getSettings()

end
