;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`ASE2_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
function ASE2_processing, parameters, settings

  ase2_statistics

end

;+
; :Hidden:
;-
pro test_ASE2_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = ASE2_getSettings()
  
  result = ASE2_processing(parameters, settings)
  print, result

end  
