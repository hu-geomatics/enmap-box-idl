PRO ase2_statistics_event, event

cmd = WIDGET_INFO(event.id,/UNAME)

  CASE cmd OF
  'open1': BEGIN
            COMMON share9, obs, est, path_obs, path_est
            
            path_obs = dialog_pickfile(filter='*.txt')               
            txt_id=widget_info(event.top, find_by_uname='txt_obs')
            widget_control, txt_id, set_value=path_obs

            
            obs = READ_ASCII(path_obs)
            obs = [obs.field1]
                                    
           END

  'open2': BEGIN
            COMMON share9, obs, est, path_obs, path_est
            
            path_est = dialog_pickfile(filter='*.txt')
            txt_id=widget_info(event.top, find_by_uname='txt_est')
            widget_control, txt_id, set_value=path_est
            
            est = READ_ASCII(path_est)
            est = [est.field1]
                                  
           END

  'scat': BEGIN  
                        
              both_arrays = [obs,est]
              lowest_value = min(both_arrays)
              highest_value = max(both_arrays) 
              ranges = (highest_value - lowest_value) / 10  
              line1 = [lowest_value-ranges,highest_value+ranges]
              line2 = [lowest_value-ranges,highest_value+ranges]
 
            ow = window(dimensions=[450,450])
               
            scplot = PLOT(obs, est, XTITLE=file_basename(path_obs, '.txt'), XRANGE=[lowest_value-ranges, highest_value+ranges], $
                                  YTITLE=file_basename(path_est, '.txt'), YRANGE=[lowest_value-ranges, highest_value+ranges], $
                        LINESTYLE=6, SYMBOL='o', SYM_COLOR='purple', SYM_THICK=2, SYM_SIZE=0.6, FONT_NAME='arial', FONT_STYLE=1, position=[0.1,0.1,0.9,0.9],/current)
            scplot = PLOT(line1, line2, COLOR='dark blue', /OVERPLOT) 

          END
  'hist10': BEGIN
            
            data1_hist = obs
            data2_hist = est
            
            mean_temp1 = mean(data1_hist)
            mean_temp2 = mean(data2_hist)
             
            max_data1 = max(obs)
            min_data1 = min(obs)
            
            max_data2 = max(est)
            min_data2 = min(est)             
            
            hist_obs = HISTOGRAM(data1_hist, LOCATIONS=location_obs, min=min_data1, max=max_data1, NBINS=10)
            hist_est = HISTOGRAM(data2_hist, LOCATIONS=location_est, min=min_data2, max=max_data2, NBINS=10)

            his_1 = BARPLOT(location_obs, hist_obs, index=0, NBARS=2, NAME=file_basename(path_obs, '.txt'), FILL_COLOR='purple', TITLE='Histogram (10 bins)', FONT_NAME='arial', FONT_STYLE=1) ;;+' | '+file_basename(path_est, '.txt')
            his_2 = BARPLOT(location_est, hist_est, index=1, NBARS=2, NAME=file_basename(path_est, '.txt'), FILL_COLOR='light blue', /OVERPLOT)
            legende = LEGEND(TARGET=[his_1,his_2], POSITION=[0.4,0.85], font_size=10, FONT_NAME='arial')
            
          END
 'hist100': BEGIN
            
            data1_hist = obs
            data2_hist = est
            
            mean_temp1 = mean(data1_hist)
            mean_temp2 = mean(data2_hist)
            
            max_data1 = max(obs)
            min_data1 = min(obs)
            
            max_data2 = max(est)
            min_data2 = min(est)
            
            hist_obs = HISTOGRAM(data1_hist, LOCATIONS=location_obs, min=min_data1, max=max_data1, NBINS=100)
            hist_est = HISTOGRAM(data2_hist, LOCATIONS=location_est, min=min_data1, max=max_data2, NBINS=100)
            
            his_1 = BARPLOT(location_obs, hist_obs, index=0, NBARS=2, NAME=file_basename(path_obs, '.txt'), COLOR='purple', FILL_COLOR='purple', TITLE='Histogram (100 bins)', FONT_NAME='arial', FONT_STYLE=1) ;;+' | '+file_basename(path_est, '.txt')
            his_2 = BARPLOT(location_est, hist_est, index=1, NBARS=2, NAME=file_basename(path_est, '.txt'), COLOR='light blue', FILL_COLOR='light blue', /OVERPLOT)
            legende = LEGEND(TARGET=[his_1,his_2], POSITION=[0.4,0.85], font_size=10, FONT_NAME='arial')
            
          END

          
  'quit':  WIDGET_CONTROL,event.top,/DESTROY
  
  'about': ok = dialog_message('The objective of the Advanced Statistical Evaluator (ASE) is to provide for remote sensing practitioners (i.e., non-statisticians) guidance for model evaluation. An optimal set of statistical measures is proposed for the quantitative assessment of model performance in the context of vegetation biophysical variable retrieval from Earth observation (EO) data.                                                                                                                    The ASE module is based on a study [1] where the main terms and concepts were reviewed and an exhaustive literature review was carried out. Most common statistical measures used to evaluate model performances were summarized and analyzed supported by exemplary datasets. Finally, an "optimal" statistic set was defined, including root mean square error (RMSE) from the category of Error Indices; coefficient of determination (R^2), slope and intercept of Theil-Sen regression [2] from the category of Correlation-based Measures; relative RMSE (RRMSE), and Nash-Sutcliffe efficiency (NSE) index [3] from the category of Dimensionless Indices. This indicator set meets the following essential model validation criteria:                                                (a) non-dimensionality, to avoid influence from the magnitude of the values;                                                                                                                   (b) bounded, for effortless comprehension of its meaning (e.g., between 0-no agreement and 1-perfect agreement);                                                    (c) symmetry, i.e., data sets should be interchangeable since the assumption of "ground truth" is unrealistic in remote sensing applications;                                                                                                    (d) measurement of the actual difference in the data unit to understand the magnitude of the error;                                                                              (e) model prediction capability compared to measurement statistics (e.g., observed mean value).                                                                             A wide acceptance and use of these statistics should enable a better intercomparison of scientific results, urgently needed in times of increasing model development activities that are carried out with respect to the upcoming EnMAP and other EO missions.                                                                                                                                                            References:                                                                                                          [1] K. Richter, C. Atzberger, T. B. Hank, and W. Mauser, "Derivation of biophysical variables from Earth Observation data: validation and statistical measures," Journal of Applied Remote Sensing, accepted with minor revisions (2012).                                                                                       [2] R. Fernandes, and S. G. Leblanc, "Parametric (modified least squares) and non-parametric (Theil-Sen) linear regressions for predicting biophysical parameters in the presence of measurement errors," Remote Sens. Environ. 95(3), 303-316 (2005).                                                              [3] J. E. Nash, and J. V. Sutcliffe, "River flow forecasting through conceptual models part I - A discussion of principles," J. Hydrol. 10(3), 282-290 (1970).     ',$
             TITLE='ASE - short description', /INFORMATION)
  'help':  ok = dialog_message('Please note that compared files must be ascii with .txt-ending.              In addition to the calculation of Error Indices you can visualise scatterplot and histogram of your data.',TITLE='Help', /INFORMATION)
  'contact': ok = dialog_message('Authors:                                                                                                                                                                                                                                 Matthias Locherer (m.locherer@iggf.geo.uni-muenchen.de)                 Katja Richter (k.richter@iggf.geo.uni-muenchen.de)                             Tobias Hank (tobias.hank@lmu.de)                                                                                                                                                                                           Deptartment of Geography, Ludwigs-Maximilians-University Munich', $
             TITLE='Contact', /INFORMATION)
  ;'exp' :  message = DIALOG_MESSAGE('Under Construction!')
  'but1': ok = dialog_message('Root Mean Square Error (RMSE)                                                 --------------------------------------------------------------------------- Recommended value / range:                                                            Generally: Close to 0!                                                                                      e.g.:  Leaf Area Index (LAI):                                                                                     Excellent: RMSE < 0.5;                                                                                         Good: 0.5 < RMSE < 1.0                     ---------------------------------------------------------------------------   Characteristics: (+ advantage / - disadvantage)                                           +    indicates magnitude of error (in variable units)                                      +    symmetry is provided (i.e., data are interchangeable)                          +    frequently used in literature (thus comparability is guaranteed)          -     depending on data unit, range, and distribution of the variable value -     not bounded'                   ,$
               TITLE='RMSE', /INFORMATION)
  'but6': ok = dialog_message('Coefficient of Determination  (R^2)                                                 --------------------------------------------------------------------------- Recommended value / range:                                                            Generally: Close to 1!                                                                                               Excellent: R^2 > 0.9;                                                                                            Good: 0.5 < R^2 < 0.9                      ---------------------------------------------------------------------------   Characteristics: (+ advantage / - disadvantage)                                           +    reflects spatial (temporal) patterns                                                            +    dimensionless & bounded (i.e., easily to understand)                           +    symmetry is provided (i.e., data are interchangeable)                             +    frequently used in literature (thus comparability is provided)              -     does not measure actual differences, therefore may impose                       apparently  good/high model`s accuracy'                   ,$
               TITLE='R^2', /INFORMATION) 
  'but8': ok = dialog_message('Slope (m) / Intercept (b)                                                 --------------------------------------------------------------------------- Recommended value / range:                                                                        m: Generally: Close to 1!                                                                                           Excellent: 0.95 < m < 1.05 ;                                                                                 Good: 0.80 < m < 1.20                                                                               b:  Generally close to 0          ---------------------------------------------------------------------------   Characteristics: (+ advantage / - disadvantage)                                           +    indicates model under/overestimation for low/ high variables             +    indicates linear model biases /error                                                           -    asymmetric for traditional Ordinary Least Square (OLS) regression forms, thus observed data are assumed error-free (which is not true!). Use of Theil-Sen or reduced major axis (RMA) forms is recommended.'                   ,$
               TITLE='m/b', /INFORMATION) 
  'but9': ok = dialog_message('Slope (m) / Intercept (b)                                                 --------------------------------------------------------------------------- Recommended value / range:                                                                        m: Generally: Close to 1!                                                                                           Excellent: 0.95 < m < 1.05 ;                                                                                 Good: 0.80 < m < 1.20                                                                               b:  Generally close to 0          ---------------------------------------------------------------------------   Characteristics: (+ advantage / - disadvantage)                                           +    indicates model under/overestimation for low/ high variables             +    indicates linear model biases /error                                                           -    asymmetric for traditional Ordinary Least Square (OLS) regression forms, thus observed data are assumed error-free (which is not true!). Use of Theil-Sen or reduced major axis (RMA) forms is recommended.'                   ,$
               TITLE='m/b', /INFORMATION) 
  'but12': ok = dialog_message('Relative Root Mean Square Error (RRMSE)                                           --------------------------------------------------------------------------- Recommended value / range:                                                            Generally: Close to 0!                                                                                                 Excellent: RRMSE < 10% ;                                                                                    Good: 10% < RRMSE < 20%                     ---------------------------------------------------------------------------   Characteristics: (+ advantage / - disadvantage)                                           +    suitable for comparisons between different variables or different               ranges                                                                                                           +    insensitive to magnitude of values                                                             +    less sensitive to outliers                                                                                -     not bounded'                   ,$
                TITLE='RRMSE', /INFORMATION)    
  'but16': ok = dialog_message('Nash Sutcliffe Efficiency Index (NSE)                                                 --------------------------------------------------------------------------- Recommended value / range:                                                            Generally: Close to 1!                                                                                                Excellent: NSE > 0.9 ;                                                                                           Good: 0.5 < NSE < 0.8                                                                                         Useful: 0.1 < NSE < 0.4                      ---------------------------------------------------------------------------   Characteristics: (+ advantage / - disadvantage)                                           +    indicates the predictive power of models                                                +    tells if model performs at least as accurate as the mean of observed          data                                                                                                                -     not bounded in negative direction'                   ,$
                TITLE='NSE', /INFORMATION)     
  'calc':  BEGIN
            prelist1 = WIDGET_INFO(event.top, FIND_BY_UNAME='grp1')
            WIDGET_CONTROL, prelist1, GET_VALUE=list_a
            prelist2 = WIDGET_INFO(event.top, FIND_BY_UNAME='grp2')
            WIDGET_CONTROL, prelist2, GET_VALUE=list_b
            prelist3 = WIDGET_INFO(event.top, FIND_BY_UNAME='grp3')
            WIDGET_CONTROL, prelist3, GET_VALUE=list_c

            ;; Calculate Statistics
            n_data = N_ELEMENTS(obs)
         
            ;; means
            mean1 = mean(obs)
            mean2 = mean(est)
            
            ;; standard deviations
            std1 = STDDEV(obs)
            std2 = STDDEV(est)
            
            ;; RMSE
            rmse = SQRT(TOTAL((est-obs)^2)/n_data)        
                       
            if where(list_a[0]) ne 0 then begin & endif else begin
               
              tx=widget_info(event.top, find_by_uname='text_a1')
              widget_control, tx, set_value=STRING(rmse, format='(f7.2)')
            endelse
            if where(list_a[1]) ne 0 then begin & endif else begin & mae = TOTAL(ABS(est-obs))/n_data 
              tx=widget_info(event.top, find_by_uname='text_a2')
              widget_control, tx, set_value=string(mae, format='(f7.2)')
            endelse
            if where(list_a[2]) ne 0 then begin & endif else begin & mbe = mean2-mean1 
              tx=widget_info(event.top, find_by_uname='text_a3')
              widget_control, tx, set_value=STRING(mbe, format='(f7.2)')
            endelse
            if where(list_a[3]) ne 0 then begin & endif else begin & sb = (mean1-mean2)^2 
              tx=widget_info(event.top, find_by_uname='text_a4')
              widget_control, tx, set_value=STRING(sb, format='(f7.2)')
            endelse
            if where(list_a[4]) ne 0 then begin & endif else begin & sepc = sqrt(TOTAL((est-obs-(mean2-mean1))^2)/n_data) 
              tx=widget_info(event.top, find_by_uname='text_a5')
              widget_control, tx, set_value=STRING(sepc, format='(f7.2)')
            endelse
            
            if where(list_b[0]) ne 0 then begin & endif else begin & r_2 = (CORRELATE(obs,est))^2 
              tx=widget_info(event.top, find_by_uname='text_b1')
              widget_control, tx, set_value=STRING(r_2, format='(f7.2)')
            endelse
            if where(list_b[1]) ne 0 then begin & endif else begin & r = CORRELATE(obs,est) 
              tx=widget_info(event.top, find_by_uname='text_b2')
              widget_control, tx, set_value=STRING(r, format='(f7.2)')
            endelse
            if where(list_b[2]) ne 0 then begin & endif else begin 
              m_all = POLY_FIT(obs,est,1) 
              b = m_all[0]
              m = m_all[1]
              ;y = m*obs+b 
              tx=widget_info(event.top, find_by_uname='text_b3')
              widget_control, tx, set_value=STRING(m, format='(f7.2)')
            endelse
            
            if where(list_b[3]) ne 0 then begin & endif else begin 
              m_all = POLY_FIT(obs,est,1) 
              b = m_all[0]
              m = m_all[1]
              ;y = m*obs+b 
              tx=widget_info(event.top, find_by_uname='text_b4')
              widget_control, tx, set_value=STRING(b, format='(f7.2)')
            endelse
            
            if where(list_c[0]) ne 0 then begin & endif else begin 
              NDI = rmse/std1 
              tx=widget_info(event.top, find_by_uname='text_c1')
              widget_control, tx, set_value=STRING(ndi, format='(f7.2)')
              endelse
            if where(list_c[1]) ne 0 then begin & endif else begin 
              range = max(obs)-min(obs)
              NRMSE = RMSE/range 
              tx=widget_info(event.top, find_by_uname='text_c2')
              widget_control, tx, set_value=STRING(nrmse, format='(f7.2)')
              endelse
            if where(list_c[2]) ne 0 then begin & endif else begin 
              RRMSE = RMSE/mean1 
              tx=widget_info(event.top, find_by_uname='text_c3')
              widget_control, tx, set_value=STRING(rrmse, format='(f7.2)')
              endelse
            if where(list_c[3]) ne 0 then begin & endif else begin 
              RDP = std1/rmse 
              tx=widget_info(event.top, find_by_uname='text_c4')
              widget_control, tx, set_value=STRING(rdp, format='(f7.2)')
              endelse
            if where(list_c[4]) ne 0 then begin & endif else begin & RelB = ((TOTAL(obs-est))/n_data)/mean1*100 
              tx=widget_info(event.top, find_by_uname='text_c5')
              widget_control, tx, set_value=STRING(relb, format='(f7.2)')
            endelse
            if where(list_c[5]) ne 0 then begin & endif else begin & RPE = mean((ABS(est-obs))/obs*100) 
              tx=widget_info(event.top, find_by_uname='text_c6')
              widget_control, tx, set_value=STRING(rpe, format='(f7.2)')
            endelse
            if where(list_c[6]) ne 0 then begin & endif else begin & NSE = 1-(TOTAL((obs-est)^2)/TOTAL((obs-mean1)^2)) 
              tx=widget_info(event.top, find_by_uname='text_c7')
              widget_control, tx, set_value=STRING(nse, format='(f7.2)')
            endelse
            if where(list_c[7]) ne 0 then begin & endif else begin
              d = 1-(TOTAL((obs-est)^2)/TOTAL((abs(est-mean1)+ABS(obs-mean1))^2)) 
              tx=widget_info(event.top, find_by_uname='text_c8')
              widget_control, tx, set_value=STRING(d, format='(f7.2)')
            endelse
            if where(list_c[8]) ne 0 then begin & endif else begin 
              AC = 1-(TOTAL((obs-est)^2))/(TOTAL((ABS(mean1-mean2)+ABS(obs-mean1))*(ABS(mean1-mean2)+ABS(est-mean2))))
              tx=widget_info(event.top, find_by_uname='text_c9')
              widget_control, tx, set_value=STRING(ac, format='(f7.2)')
            endelse

                      
           END
          
        
  ELSE:
  ENDCASE
END

;; =========================WIDGET============================================

PRO ase2_statistics

base = WIDGET_BASE(row=4, MBAR = mbar, /TAB_MODE)

;-----a_base


   
   a_base = WIDGET_BASE(base, column=2, /FRAME, XSIZE=630)
    note_obs = WIDGET_BUTTON(a_base, VALUE='Open Point Data 1', xsize = 150, UNAME = "open1")
    txt_obs = widget_text(a_base, value='Path...', xsize = 65, /editable, uname='txt_obs') 
    
   
;-----b_base    


   
   b_base = WIDGET_BASE(base, column=2, /FRAME, XSIZE=630)
     note_est = WIDGET_BUTTON(b_base, VALUE='Open Point Data 2', xsize = 150, UNAME = "open2")
     txt_est = widget_text(b_base, value='Path...', xsize = 65, /editable, uname='txt_est')
   
  
;-----c_base: Schaltflächen

  c_base = WIDGET_BASE(base, row=2, /FRAME, XSIZE=630)
   
    c_base_1 = WIDGET_BASE(c_base, column=3)
      title1 = WIDGET_LABEL(c_base_1, VALUE = 'Error Indices', /ALIGN_CENTER, XSIZE=200)
      title2 = WIDGET_LABEL(c_base_1, VALUE = 'Correlation-based Measures', /ALIGN_CENTER, XSIZE=200)
      title3 = WIDGET_LABEL(c_base_1, VALUE = 'Dimensionless Evaluation Indices', /ALIGN_CENTER, XSIZE=200)
    
    c_base_2 = WIDGET_BASE(c_base, column=3)
      
      subbase_1 = WIDGET_BASE(c_base_2, column=3, /FRAME, XSIZE=204)
        base_but1  =WIDGET_BASE(subbase_1, row=5)
          but1 = WIDGET_BUTTON(base_but1, VALUE='i', Uname='but1', YSIZE=27, TOOLTIP='Root Mean Square Error')
          but2 = WIDGET_BUTTON(base_but1, VALUE='i', Uname='but2', YSIZE=27,TOOLTIP='Mean Absolute Error')
          but3 = WIDGET_BUTTON(base_but1, VALUE='i', Uname='but3', YSIZE=27,TOOLTIP='Mean Bias Error')
          but4 = WIDGET_BUTTON(base_but1, VALUE='i', Uname='but4', YSIZE=27,TOOLTIP='Simulation Bias')
          but5 = WIDGET_BUTTON(base_but1, VALUE='i', Uname='but5', YSIZE=27,TOOLTIP='Standard Error of Prediction')
        ind1 = ['RMSE','MAE','MBE','SB','SEPC']
        grp1 = CW_BGROUP(subbase_1, ind1, /COLUMN, /NONEXCLUSIVE,  XSIZE=91, YSIZE=300, UNAME='grp1', SET_VALUE=[1,0,0,0,0])
        base_txt1 = WIDGET_BASE(subbase_1, row=5)
          text_a1 = WIDGET_TEXT(base_txt1, UNAME='text_a1', xsize=7, SCR_YSIZE=27)
          text_a2 = WIDGET_TEXT(base_txt1, UNAME='text_a2', xsize=7, SCR_YSIZE=27)
          text_a3 = WIDGET_TEXT(base_txt1, UNAME='text_a3', xsize=7, SCR_YSIZE=27)
          text_a4 = WIDGET_TEXT(base_txt1, UNAME='text_a4', xsize=7, SCR_YSIZE=27)
          text_a5 = WIDGET_TEXT(base_txt1, UNAME='text_a5', xsize=7, SCR_YSIZE=27)
          
      subbase_2 = WIDGET_BASE(c_base_2, column=3,  /FRAME, XSIZE=204)
          base_but2  = WIDGET_BASE(subbase_2, row=4)
          but1 = WIDGET_BUTTON(base_but2, VALUE='i', Uname='but6', YSIZE=27, TOOLTIP='Coefficient of Determination')
          but2 = WIDGET_BUTTON(base_but2, VALUE='i', Uname='but7', YSIZE=27,TOOLTIP='Pearson product-moment correlation coefficient')
          but3 = WIDGET_BUTTON(base_but2, VALUE='i', Uname='but8', YSIZE=27,TOOLTIP='Slope of Theil-Sen Regression')
          but4 = WIDGET_BUTTON(base_but2, VALUE='i', Uname='but9', YSIZE=27,TOOLTIP='Intercept of Theil-Sen Regression')
        ind2 = ['R^2','r','m','b']
        grp2 = CW_BGROUP(subbase_2, ind2, /COLUMN, /NONEXCLUSIVE, XSIZE=91, YSIZE=300, UNAME='grp2', SET_VALUE=[1,0,1,1])
        base_txt2 = WIDGET_BASE(subbase_2, row=4)
          text_b1 = WIDGET_TEXT(base_txt2, UNAME='text_b1', xsize=7, SCR_YSIZE=27)
          text_b2 = WIDGET_TEXT(base_txt2, UNAME='text_b2', xsize=7, SCR_YSIZE=27)
          text_b3 = WIDGET_TEXT(base_txt2, UNAME='text_b3', xsize=7, SCR_YSIZE=27)
          text_b4 = WIDGET_TEXT(base_txt2, UNAME='text_b4', xsize=7, SCR_YSIZE=27)
          
      subbase_3 = WIDGET_BASE(c_base_2, column=3, /FRAME, XSIZE=204)
        base_but3  = WIDGET_BASE(subbase_3, row=9)
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but10', YSIZE=27, TOOLTIP='Non-dimensional Error Index')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but11', YSIZE=27, TOOLTIP='Normalized Root Mean Square Error')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but12', YSIZE=27, TOOLTIP='Relative Root Mean Square Error')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but13', YSIZE=27, TOOLTIP='Ratio of SD(obs) to Root Mean Square Error')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but14', YSIZE=27, TOOLTIP='Relative Bias')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but15', YSIZE=27, TOOLTIP='Relative Percentage Error')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but16', YSIZE=27, TOOLTIP='Nash-Sutcliffe Efficiency')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but17', YSIZE=27, TOOLTIP='Willmott Index of Agreement')
          but1 = WIDGET_BUTTON(base_but3, VALUE='i', Uname='but18', YSIZE=27, TOOLTIP='Agreement Coefficient')
          
        ind3 = ['NDI','NRMSE','RRMSE','RDP','Rel.B','RPE','NSE','d','AC']
        grp3 = CW_BGROUP(subbase_3, ind3, /COLUMN, /NONEXCLUSIVE,  XSIZE=91, YSIZE=300, UNAME='grp3', SET_VALUE=[0,0,1,0,0,0,1,0,0]) 
        base_txt3 = WIDGET_BASE(subbase_3, row=9)
          text_c1 = WIDGET_TEXT(base_txt3, UNAME='text_c1', xsize=7, SCR_YSIZE=27)
          text_c2 = WIDGET_TEXT(base_txt3, UNAME='text_c2', xsize=7, SCR_YSIZE=27)
          text_c3 = WIDGET_TEXT(base_txt3, UNAME='text_c3', xsize=7, SCR_YSIZE=27)
          text_c4 = WIDGET_TEXT(base_txt3, UNAME='text_c4', xsize=7, SCR_YSIZE=27)
          text_c5 = WIDGET_TEXT(base_txt3, UNAME='text_c5', xsize=7, SCR_YSIZE=27)
          text_c6 = WIDGET_TEXT(base_txt3, UNAME='text_c6', xsize=7, SCR_YSIZE=27)
          text_c7 = WIDGET_TEXT(base_txt3, UNAME='text_c7', xsize=7, SCR_YSIZE=27)
          text_c8 = WIDGET_TEXT(base_txt3, UNAME='text_c8', xsize=7, SCR_YSIZE=27)
          text_c9 = WIDGET_TEXT(base_txt3, UNAME='text_c9', xsize=7, SCR_YSIZE=27)

 
;-----d_base, Ausführung

d_base = WIDGET_BASE(base, COLUMN=4, /FRAME, xsize=630)

 d_base_1 = WIDGET_BASE(d_base, row=1);, XSIZE=210)
  scat = WIDGET_BUTTON(d_base_1, VALUE='Scatterplot', XSIZE=150 ,UNAME='scat')
  
 d_base_2 = WIDGET_BASE(d_base, row=1);, XSIZE=105) 
  hist10 = WIDGET_BUTTON(d_base_2, VALUE='Histogram 10 bins', XSIZE=150, UNAME='hist10')
  
 d_base_3 = WIDGET_BASE(d_base, row=1);, XSIZE=105) 
  hist100 = WIDGET_BUTTON(d_base_2, VALUE='Histogram 100 bins', XSIZE=150, UNAME='hist100')
  
 d_base_3 = WIDGET_BASE(d_base, row=1);, XSIZE=210) 
  calc = WIDGET_BUTTON(d_base_3, VALUE='Calculate',  XSIZE=150,UNAME='calc')

;-----Top-Menu

menu = WIDGET_BUTTON(mbar, /MENU, VALUE='Menu')
   
   ;exp =  WIDGET_BUTTON(menu, VALUE='Export Calculated Data', UNAME='exp')
   quit = WIDGET_BUTTON( menu, VALUE = "Quit", UNAME = 'quit')

info = WIDGET_BUTTON(mbar, /MENU, VALUE='Information')

   about = WIDGET_BUTTON(info, VALUE='About', UNAME='about')
   help = WIDGET_BUTTON(info, VALUE='Help', UNAME='help')
   contact = WIDGET_BUTTON(info, VALUE='Contact', UNAME='contact')


;; Ende einleiten
WIDGET_CONTROL, base, /REALIZE

XMANAGER, 'ase2_statistics', base     ;; notwendig für die Bedienung der Knöpfe, ruft den Event-Manager hinzu

END







