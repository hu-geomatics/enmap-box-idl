PRO statistics_event, event

cmd = WIDGET_INFO(event.id,/UNAME)

  CASE cmd OF
  'open1': BEGIN
            COMMON share1, obs, est
            
            path_obs = dialog_pickfile(filter='*.txt')               
            txt_id=widget_info(event.top, find_by_uname='txt_obs')
            widget_control, txt_id, set_value=path_obs

            
            obs = READ_ASCII(path_obs)
            obs = [obs.field1]
                                    
           END
  'open1b':BEGIN
            COMMON share2, obs_sd, est_sd
            
            path_obs_sd = dialog_pickfile(filter='*.txt')               
            txt_id=widget_info(event.top, find_by_uname='txt_obs_sd')
            widget_control, txt_id, set_value=path_obs_sd

            
            obs_sd = READ_ASCII(path_obs_sd)
            obs_sd = [obs_sd.field1]
  
           END
  'open2': BEGIN
            COMMON share1, obs, est
            
            path_est = dialog_pickfile(filter='*.txt')
            txt_id=widget_info(event.top, find_by_uname='txt_est')
            widget_control, txt_id, set_value=path_est
            
            est = READ_ASCII(path_est)
            est = [est.field1]
                                  
           END
  'open2b':BEGIN
            COMMON share2, obs_sd, est_sd
            
            path_est_sd = dialog_pickfile(filter='*.txt')               
            txt_id=widget_info(event.top, find_by_uname='txt_est_sd')
            widget_control, txt_id, set_value=path_est_sd

            
            est_sd = READ_ASCII(path_est_sd)
            est_sd = [est_sd.field1]
  
           END
  'scat': BEGIN  
                        
            plot = ERRORPLOT(obs, est, obs_sd, est_sd, XTITLE="Observed Data", $
            YTITLE="Estimated Data", $
            LINESTYLE=6)
            ; Set some properties
            
            plot.MIN_VALUE = 0
            ;plot.THICK=2
            plot.SYM_INDEX="X"
            ;plot.SYM_COLOR ="cornflower"
            plot.ERRORBAR_COLOR="indian_red"
            plot.ERRORBAR_CAPSIZE=0.2

          END
  'quit':  WIDGET_CONTROL,event.top,/DESTROY
  
  'about': message = DIALOG_MESSAGE('Proposed optimal statistical set with recommended desirable ranges/values, characteristics/advantages as well as shortcomings/disadvantages.',/INFORMATION, TITLE='Proposed optimal statistical set', RESOURCE_NAME='test') ;funktioniert noch nicht
  'help':  message = DIALOG_MESSAGE('Under Construction!')
  'exp' :  message = DIALOG_MESSAGE('Under Construction!')
  'calc':  BEGIN
            prelist1 = WIDGET_INFO(event.top, FIND_BY_UNAME='grp1')
            WIDGET_CONTROL, prelist1, GET_VALUE=list_a
            prelist2 = WIDGET_INFO(event.top, FIND_BY_UNAME='grp2')
            WIDGET_CONTROL, prelist2, GET_VALUE=list_b
            prelist3 = WIDGET_INFO(event.top, FIND_BY_UNAME='grp3')
            WIDGET_CONTROL, prelist3, GET_VALUE=list_c
         
            ;; Calculate Statistics
            ntv = N_ELEMENTS(obs)
         
            ;; means
            mobs = mean(obs)
            mest = mean(est)
            
            ;; standard deviations
            sdobs = STDDEV(obs)
            sdest = STDDEV(est)
            
            ;; RMSE
            rmse = SQRT(TOTAL((est-obs)^2)/ntv)        
            
            print, ntv, mobs, mest, sdobs, sdest
            
            if where(list_a[0]) ne 0 then begin & endif else begin 
              tx=widget_info(event.top, find_by_uname='text_a1')
              widget_control, tx, set_value=STRING(rmse)
            endelse
            if where(list_a[1]) ne 0 then begin & endif else begin & mae = TOTAL(ABS(est-obs))/ntv 
              tx=widget_info(event.top, find_by_uname='text_a2')
              widget_control, tx, set_value=STRING(mae)
            endelse
            if where(list_a[2]) ne 0 then begin & endif else begin & mbe = mest-mobs 
              tx=widget_info(event.top, find_by_uname='text_a3')
              widget_control, tx, set_value=STRING(mbe)
            endelse
            if where(list_a[3]) ne 0 then begin & endif else begin & sb = (mobs-mest)^2 
              tx=widget_info(event.top, find_by_uname='text_a4')
              widget_control, tx, set_value=STRING(sb)
            endelse
            if where(list_a[4]) ne 0 then begin & endif else begin & sepc = sqrt(TOTAL((est-obs-(mest-mobs))^2)/ntv) 
              tx=widget_info(event.top, find_by_uname='text_a5')
              widget_control, tx, set_value=STRING(sepc)
            endelse
            
            if where(list_b[0]) ne 0 then begin & endif else begin & r_2 = (CORRELATE(obs,est))^2 
              tx=widget_info(event.top, find_by_uname='text_b1')
              widget_control, tx, set_value=STRING(r_2)
            endelse
            if where(list_b[1]) ne 0 then begin & endif else begin & r = CORRELATE(obs,est) 
              tx=widget_info(event.top, find_by_uname='text_b2')
              widget_control, tx, set_value=STRING(r)
            endelse
            if where(list_b[2]) ne 0 then begin & endif else begin 
              m_all = POLY_FIT(obs,est,1) 
              b = m_all[0]
              m = m_all[1]
              ;y = m*obs+b 
              tx=widget_info(event.top, find_by_uname='text_b3')
              widget_control, tx, set_value=STRING(m)
            endelse
            
            if where(list_b[3]) ne 0 then begin & endif else begin 
              m_all = POLY_FIT(obs,est,1) 
              b = m_all[0]
              m = m_all[1]
              ;y = m*obs+b 
              tx=widget_info(event.top, find_by_uname='text_b4')
              widget_control, tx, set_value=STRING(b)
            endelse
            
            if where(list_c[0]) ne 0 then begin & endif else begin 
              NDI = rmse/sdobs 
              tx=widget_info(event.top, find_by_uname='text_c1')
              widget_control, tx, set_value=STRING(ndi)
              endelse
            if where(list_c[1]) ne 0 then begin & endif else begin 
              range = max(obs)-min(obs)
              NRMSE = RMSE/range 
              tx=widget_info(event.top, find_by_uname='text_c2')
              widget_control, tx, set_value=STRING(nrmse)
              endelse
            if where(list_c[2]) ne 0 then begin & endif else begin 
              RRMSE = RMSE/mobs 
              tx=widget_info(event.top, find_by_uname='text_c3')
              widget_control, tx, set_value=STRING(rrmse)
              endelse
            if where(list_c[3]) ne 0 then begin & endif else begin 
              RDP = sdobs/rmse 
              tx=widget_info(event.top, find_by_uname='text_c4')
              widget_control, tx, set_value=STRING(rdp)
              endelse
            if where(list_c[4]) ne 0 then begin & endif else begin & RelB = ((TOTAL(obs-est))/ntv)/mobs*100 
              tx=widget_info(event.top, find_by_uname='text_c5')
              widget_control, tx, set_value=STRING(relb)
            endelse
            if where(list_c[5]) ne 0 then begin & endif else begin & RPE = mean((ABS(est-obs))/obs*100) 
              tx=widget_info(event.top, find_by_uname='text_c6')
              widget_control, tx, set_value=STRING(rpe)
            endelse
            if where(list_c[6]) ne 0 then begin & endif else begin & NSE = 1-(TOTAL((obs-est)^2)/TOTAL((obs-mobs)^2)) 
              tx=widget_info(event.top, find_by_uname='text_c7')
              widget_control, tx, set_value=STRING(nse)
            endelse
            if where(list_c[7]) ne 0 then begin & endif else begin
              d = 1-(TOTAL((obs-est)^2)/TOTAL((abs(est-mobs)+ABS(obs-mobs))^2)) 
              tx=widget_info(event.top, find_by_uname='text_c8')
              widget_control, tx, set_value=STRING(d)
            endelse
            if where(list_c[8]) ne 0 then begin & endif else begin 
              AC = 1-(TOTAL((obs-est)^2))/(TOTAL((ABS(mobs-mest)+ABS(obs-mobs))*(ABS(mobs-mest)+ABS(est-mest))))
              tx=widget_info(event.top, find_by_uname='text_c9')
              widget_control, tx, set_value=STRING(ac)
            endelse

                      
           END
          
  'hist': BEGIN
            hist_obs = HISTOGRAM(obs)
            hist_est = HISTOGRAM(est)
            help, hist_obs, hist_est, obs, est
            print, hist_obs, hist_est
            his_1 = BARPLOT(hist_obs, index=0, NBARS=2, FILL_COLOR='green')
            his_2 = BARPLOT(hist_est, index=1, NBARS=2, FILL_COLOR='red', /OVERPLOT)
            
            text08 = TEXT(3,15,'Observed Data', /CURRENT, COLOR='green', /DATA)
            text09 = TEXT(3,13,'Estimated Data', /CURRENT, COLOR='red', /DATA)
            
          END
          
  ELSE:
  ENDCASE
END

;; =========================WIDGET============================================

PRO statistics

base = WIDGET_BASE(row=4, MBAR = mbar, /TAB_MODE)

;-----a_base

  a_base = WIDGET_BASE(base, row=2, /FRAME)
   
   a_base_1 = WIDGET_BASE(a_base, column=2)
    note_obs = WIDGET_BUTTON(a_base_1, VALUE='Observed Data', xsize = 90, UNAME = "open1")
    txt_obs = widget_text(a_base_1, value='Path...', xsize = 78, /editable, uname='txt_obs') 
    
   a_base_2 = WIDGET_BASE(a_base, column=2)
    note_obs_sd = WIDGET_BUTTON(a_base_2, VALUE='Std-Dev', xsize = 90, UNAME = "open1b")
    txt_obs_sd = widget_text(a_base_2, value='Path...', xsize = 78, /editable, uname='txt_obs_sd') 
   
;-----b_base    

  b_base = WIDGET_BASE(base, row=2, /FRAME)
   
   b_base_1 = WIDGET_BASE(b_base, column=2)
     note_est = WIDGET_BUTTON(b_base_1, VALUE='Estimated Data', xsize = 90, UNAME = "open2")
     txt_est = widget_text(b_base_1, value='Path...', xsize = 78, /editable, uname='txt_est')
   b_base_2 = WIDGET_BASE(b_base, column=2)
    note_est_sd = WIDGET_BUTTON(b_base_2, VALUE='Std-Dev', xsize = 90, UNAME = "open2b")
    txt_est_sd = widget_text(b_base_2, value='Path...', xsize = 78, /editable, uname='txt_est_sd')
  
;-----c_base: Schaltflächen

  c_base = WIDGET_BASE(base, row=2)
   
    c_base_1 = WIDGET_BASE(c_base, column=3)
      title1 = WIDGET_LABEL(c_base_1, VALUE = 'Error Indices', /ALIGN_CENTER, XSIZE=190)
      title2 = WIDGET_LABEL(c_base_1, VALUE = 'Correlation-based Measures', /ALIGN_CENTER, XSIZE=190)
      title3 = WIDGET_LABEL(c_base_1, VALUE = 'Dimensionless Evaluation Indices', /ALIGN_CENTER, XSIZE=190)
    
    c_base_2 = WIDGET_BASE(c_base, column=3)
      
      subbase_1 = WIDGET_BASE(c_base_2, column=2, /FRAME)
        ind1 = ['RMSE','MAE','MBE','SB','SEPC']
        grp1 = CW_BGROUP(subbase_1, ind1, /COLUMN, /NONEXCLUSIVE,  XSIZE=91, YSIZE=250, UNAME='grp1', SET_VALUE=[1,0,0,0,0])
        base_txt1 = WIDGET_BASE(subbase_1, row=5)
          text_a1 = WIDGET_TEXT(base_txt1, UNAME='text_a1', XSIZE=12, SCR_YSIZE=25)
          text_a2 = WIDGET_TEXT(base_txt1, UNAME='text_a2', XSIZE=12, SCR_YSIZE=25)
          text_a3 = WIDGET_TEXT(base_txt1, UNAME='text_a3', XSIZE=12, SCR_YSIZE=25)
          text_a4 = WIDGET_TEXT(base_txt1, UNAME='text_a4', XSIZE=12, SCR_YSIZE=25)
          text_a5 = WIDGET_TEXT(base_txt1, UNAME='text_a5', XSIZE=12, SCR_YSIZE=25)
          
      subbase_2 = WIDGET_BASE(c_base_2, column=2, /FRAME)
        ind2 = ['R^2','r','m','b']
        grp2 = CW_BGROUP(subbase_2, ind2, /COLUMN, /NONEXCLUSIVE, XSIZE=91, YSIZE=250, UNAME='grp2', SET_VALUE=[1,0,1,1])
        base_txt2 = WIDGET_BASE(subbase_2, row=4)
          text_b1 = WIDGET_TEXT(base_txt2, UNAME='text_b1', XSIZE=12, SCR_YSIZE=25)
          text_b2 = WIDGET_TEXT(base_txt2, UNAME='text_b2', XSIZE=12, SCR_YSIZE=25)
          text_b3 = WIDGET_TEXT(base_txt2, UNAME='text_b3', XSIZE=12, SCR_YSIZE=25)
          text_b4 = WIDGET_TEXT(base_txt2, UNAME='text_b4', XSIZE=12, SCR_YSIZE=25)
          
      subbase_3 = WIDGET_BASE(c_base_2, column=2, /FRAME)
        ind3 = ['NDI','NRMSE','RRMSE','RDP','Rel.B','RPE','NSE','d','AC']
        grp3 = CW_BGROUP(subbase_3, ind3, /COLUMN, /NONEXCLUSIVE,  XSIZE=91, YSIZE=250, UNAME='grp3', SET_VALUE=[0,1,0,0,0,0,1,0,0]) 
        base_txt3 = WIDGET_BASE(subbase_3, row=9)
          text_c1 = WIDGET_TEXT(base_txt3, UNAME='text_c1', XSIZE=12, SCR_YSIZE=25)
          text_c2 = WIDGET_TEXT(base_txt3, UNAME='text_c2', XSIZE=12, SCR_YSIZE=25)
          text_c3 = WIDGET_TEXT(base_txt3, UNAME='text_c3', XSIZE=12, SCR_YSIZE=25)
          text_c4 = WIDGET_TEXT(base_txt3, UNAME='text_c4', XSIZE=12, SCR_YSIZE=25)
          text_c5 = WIDGET_TEXT(base_txt3, UNAME='text_c5', XSIZE=12, SCR_YSIZE=25)
          text_c6 = WIDGET_TEXT(base_txt3, UNAME='text_c6', XSIZE=12, SCR_YSIZE=25)
          text_c7 = WIDGET_TEXT(base_txt3, UNAME='text_c7', XSIZE=12, SCR_YSIZE=25)
          text_c8 = WIDGET_TEXT(base_txt3, UNAME='text_c8', XSIZE=12, SCR_YSIZE=25)
          text_c9 = WIDGET_TEXT(base_txt3, UNAME='text_c9', XSIZE=12, SCR_YSIZE=25)

 
;-----d_base, Ausführung

d_base = WIDGET_BASE(base, row = 1, /FRAME)

  scat = WIDGET_BUTTON(d_base, VALUE='Scatterplots', XSIZE=190 ,UNAME='scat')
  hist = WIDGET_BUTTON(d_base, VALUE='Histogram', XSIZE=190, UNAME='hist')
  calc = WIDGET_BUTTON(d_base, VALUE='Calculate',  XSIZE=190,UNAME='calc')

;-----Top-Menu

menu = WIDGET_BUTTON(mbar, /MENU, VALUE='Menu')
   
   help = WIDGET_BUTTON(menu, VALUE='Help', UNAME='help')
   exp =  WIDGET_BUTTON(menu, VALUE='Export Calculated Data', UNAME='exp')
   quit = WIDGET_BUTTON( menu, VALUE = "Quit", UNAME = 'quit')

info = WIDGET_BUTTON(mbar,  /MENU, VALUE='Info')
   about = WIDGET_BUTTON(info, VALUE='About Indices', UNAME='about')
   ;tester = WIDGET_TABLE(about)



;; Ende einleiten
WIDGET_CONTROL, base, /REALIZE

XMANAGER, 'statistics', base     ;; notwendig für die Bedienung der Knöpfe, ruft den Event-Manager hinzu

END







