;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function ASE2_getDirname
  
  result = filepath('ASE2', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_ASE2_getDirname

  print, ASE2_getDirname()
  print, ASE2_getDirname(/SourceCode)

end
