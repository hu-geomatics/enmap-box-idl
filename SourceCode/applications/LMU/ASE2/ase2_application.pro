;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `ASE2_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `ASE2_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `ASE2_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro ASE2_application, applicationInfo
  
  ; get global settings for this application
  settings = ASE2_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = ASE2_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = ASE2_processing(parameters, settings)
    
;    if parameters['showReport'] then begin
;      ASE2_showReport, reportInfo, settings
;    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_ASE2_application
  applicationInfo = Hash()
  ASE2_application, applicationInfo

end  
