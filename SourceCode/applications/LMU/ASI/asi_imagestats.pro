function ASI_imagestats, parameters

wl_NIR = 750.00
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  NIR = inputImage.locateWavelength(wl_NIR)

  inputImage.initReader, /CUBE, $
   SubsetbandPositions=NIR, Datatype='float'
   data_stat = inputImage.getData()
   

     uni_data = data_stat[UNIQ(data_stat)]

     med_data = MEDIAN(uni_data)

    
 return, med_data    

;return, 

end
