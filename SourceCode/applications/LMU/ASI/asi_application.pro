;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `ASI_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `ASI_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `ASI_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro ASI_application, applicationInfo
  
  ; get global settings for this application
  settings = ASI_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = ASI_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = ASI_processing(parameters, settings)
    
;    if parameters['showReport'] then begin
;      ASI_showReport, reportInfo, settings
;    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_ASI_application
  applicationInfo = Hash()
  ASI_application, applicationInfo

end  
