;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function ASI_getDirname
  
  result = filepath('ASI', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_ASI_getDirname

  print, ASI_getDirname()
  print, ASI_getDirname(/SourceCode)

end
