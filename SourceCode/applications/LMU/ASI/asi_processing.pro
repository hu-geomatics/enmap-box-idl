
function ASI_processing, parameters, settings


;============ I. check required parameters

tileLines = 10
wl1 = parameters['asi1']
wl2 = parameters['asi2']

  inputImage = hubIOImgInputImage(parameters['inputImage'])
  outputImage = hubIOImgOutputImage(parameters['outputImage'])
  
;; ----- progressbar
    progressBar = hubProgressBar(Title=settings.hubGetValue('title') $
      , GroupLeader = settings.hubGetValue('groupLeader'))
    progressBar.setInfo, 'Calculating Spectral Integral for wavelength'
    progressBar.setRange, [0,inputImage.getMeta('lines')]
    progressDone=0
;; ----- end progressbar

  band1 = inputImage.locateWavelength(wl1)
  band2 = inputImage.locateWavelength(wl2)
  
;;------ Identification of all bands
     band1a = band1[0]
     band2a = band2[0]
     band_count = (band2a-band1a)+1
     allbands = MAKE_ARRAY(band_count, /INT, VALUE=0)

        incr = 0
     FOR i = band1a, band2a, 1 DO BEGIN
        allbands[incr] = band1a+incr
        incr++
     ENDFOR
;;------ End of Identification
 
  inputImage.initReader, tilelines, /TILEPROCESSING, /SLICE, $
   SubsetbandPositions=allbands, Datatype='float'

  myBandnames = ['Spectral Integral']
   outputImage.setMeta, 'band names', myBandnames ;;;;;
     
  outputImage.copyMeta, inputImage, /COPYSPATIALINFORMATION  
  outputImage.initWriter, inputImage.getWriterSettings(SETBANDS=1)

;============ II. parameters for hullcurve and spectral integral
  
  fwhm = inputImage.getMeta('fwhm')

  cut_fwhm = fwhm[band1a:band2a]
        
;============ III.0 Abfrage der Statistik
  print, 'step'
  median_data = ASI_imagestats(parameters) 
  help, median_data
;============ III. MAIN - CALCULATING IMAGE

WHILE ~inputImage.tileProcessingDone() do begin
    
     data = inputImage.getData()

;; === image statistics check
 if (median_data LT 10000) and (median_data GT 1000) then begin
  data=TEMPORARY(data)/10000
 endif else begin
 ;---
  if (median_data LT 1000) and (median_data GT 100) then begin
   data=TEMPORARY(data)/1000
  endif else begin
   ;--
   if (median_data LT 100) and (median_data GT 10) then begin
   data=TEMPORARY(data)/100
   endif else begin
    ;-
    if (median_data LT 10) and (median_data GT 1) then begin
   data=TEMPORARY(data)/10
    endif else begin
    endelse
    ;-
   endelse
   ;--
  endelse
 ;---   
 endelse
;; === checke end     
     
      row_counter = N_ELEMENTS(data[0,*])
      index = indgen(band_count) ;; generiert Index-Array für die Berechnung der Hüllkurve
      hull = MAKE_ARRAY(band_count, row_counter, /float)  ;; Array, in das die Werte der Schleife eingeschrieben werden sollen
     integral = MAKE_ARRAY(1,row_counter) 
     rep_arr = MAKE_ARRAY(1,row_counter, value=1, /BYTE)
       rep_data = cut_fwhm # rep_arr
     
     data=TEMPORARY(data)/10000    ;Routine zum Abfragen des Formats der Spektren?????????????????????????????????????????????????????????????????????

      spc = data*rep_data   
;;-------- Begin of Integral

   hull_diff = spc[band_count-1,*]-spc[0,*]   ;; Differenz zwischen Min und Max

   FOR i= 0, band_count-1, 1 DO BEGIN

      hull[i,*] = spc[0,*] + (hull_diff*(index[i])/band_count)
 
   ENDFOR
  
  sum_spc = TOTAL(spc, 1)
  sum_hull = TOTAL(hull, 1)
  integral[0,*] = (sum_hull-sum_spc)/sum_hull
  
;;-------- End of Interal
      
    outputImage.writeData, integral
    
    ;; ----- progressbar
      progressDone += tilelines
      progressBar.setProgress, progressDone
    ;; ----- end progressbar
     
ENDWHILE


;============  V. CLEANUP

  inputImage.cleanup
  outputImage.cleanup
  progressBar.cleanup

end

;+
; :Hidden:
;-
pro test_ASI_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = ASI_getSettings()
  
  result = ASI_processing(parameters, settings)
  print, result

end  
