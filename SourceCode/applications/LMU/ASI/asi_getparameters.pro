
function ASI_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'ANALYZE SPECTRAL INTEGRAL (ASI)'
  
 hubAMW_frame  
  hubAMW_subframe , /row
   hubAMW_button,  Title=' About ', EventHandler='asi_button1'
   hubAMW_button,  Title=' Help  ', EventHandler='asi_button2'
   hubAMW_button,  Title='Contact', EventHandler='asi_button3'
  
  hubAMW_frame ;,   Title =  'Input'
    hubAMW_inputImageFilename, 'inputImage', Title='Input-Image'

  hubAMW_frame , title = 'Set range of Integral'
 
     hubAMW_parameter, 'asi1', title='Enter lower wavelength (nm):', value=550 $
      , Unit='(400-2499 nm)', /Float, ISGE=400, IsLE=2499, Size=4, TOOLTIP='Set wavelength where Spectral Integral begins. Value must be lower than in the other field.'
     hubAMW_parameter, 'asi2', title='Enter upper wavelength (nm):', value=760 $
      , Unit='(401-2500 nm)', /Float, IsGE=401, IsLE=2500, Size=4, TOOLTIP='Set wavelength where Spectral Integral ends. Value must be higher than in the other field.'
  hubAMW_frame ;,   Title =  'Output'
    hubAMW_outputFilename, 'outputImage', Title='Output-Image', value='ASI_Output'


  parameters = hubAMW_manage()
  return, parameters
end

;+
; :Hidden:
;-
pro test_ASI_getParameters

  ; test your routine
  settings = ASI_getSettings()
  parameters = ASI_getParameters(settings)
  print, parameters

end  
