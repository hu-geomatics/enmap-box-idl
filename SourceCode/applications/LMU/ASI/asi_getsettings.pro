;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('ASI.conf', ROOT=ASI_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, ASI_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function ASI_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('asi.conf', ROOT=ASI_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_ASI_getSettings

  print, ASI_getSettings()

end
