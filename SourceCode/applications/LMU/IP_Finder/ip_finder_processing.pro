
function IP_Finder_processing, parameters, settings

t0 = SYSTIME(1)

  tilelines =  1
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  outputImage = hubIOImgOutputImage(parameters['outputImage'])

  ndvi_threshold = parameters['ndvit']
  thr_low = parameters['lower_brd']
  thr_high = parameters['higher_brd']

;;------ Identification of all necessary bands
;     band1a = inputimage.locateWavelength(thr_low)
;     band2a = inputimage.locateWavelength(thr_high)
;     band_count = (band2a-band1a)+1
;     allbands = MAKE_ARRAY(band_count, /INT)
;
;        incr = 0
;     FOR i = band1a, band2a, 1 DO BEGIN
;        allbands[incr] = band1a+incr
;        incr++
;     ENDFOR
;;------ End of Identification
  
  inputImage.initReader, tilelines, /TILEPROCESSING, /SLICE, $
   SubsetbandPositions=allbands, Datatype='float'

  myBandnames = ['REIP']
   outputImage.setMeta, 'band names', myBandnames ;;;;;
     
  outputImage.copyMeta, inputImage, /COPYSPATIALINFORMATION  
  outputImage.initWriter, inputImage.getWriterSettings(SETBANDS=1)  

  ndvi_band1 = inputimage.locateWavelength(678.00)
  ndvi_band2 = inputimage.locateWavelength(820.00)
  
     ;; =====  Einlesen der Wellenlänge als xarr ====  

  wl_bands = inputImage.getMeta('wavelength')
  
  if inputImage.getMeta('wavelength units') EQ 'micrometers' then begin  
     wl_bands = wl_bands*1000  
  endif 

;; ===== Eliminieren von evtl Meta-Daten
  
        wl_count = N_ELEMENTS(wl_bands)

            if wl_bands[wl_count-1] GT 2500 then begin
               wl_bands = wl_bands[0:wl_count-2]  ;; Azimuth
            endif

        wl_count = N_ELEMENTS(wl_bands)   
  
            if wl_bands[wl_count-1] GT 2500 then begin
               wl_bands = wl_bands[0:wl_count-2]  ;; Zenith
            endif
 
        wl_count = N_ELEMENTS(wl_bands)  
;bin = VALUE_LOCATE(
;; ===== End Eliminieren

  wl_bands_cut = wl_bands[WHERE(wl_bands GT 670.00 and wl_bands LT 800.00)]
            
     ;; ===== Definieren des Splines ====  

          xarr_spline = FINDGEN(13000)/100                       ;; Spline in 0.01 nm Schritten von 670.00-799.99 nm 
          xarr_spline = xarr_spline + 670.00 ;wl_bands[0]        ;; Spline beginnt bei Startwellenlänge der vorhandenen Bänder           
          
;; ----- progressbar
       progressBar = hubProgressBar(Title=settings.hubGetValue('title') $
         , GroupLeader = settings.hubGetValue('groupLeader'))
       progressBar.setInfo, 'Calculating REIP...'
       progressBar.setRange, [0,inputImage.getMeta('lines')]
       progressDone=0
;; ----- end progressbar

t1 = SYSTIME(1)

WHILE ~inputImage.tileProcessingDone() do begin
    
     data = inputImage.getData()    
     row_counter = N_ELEMENTS(data[1,*])
     ndvi = MAKE_ARRAY(row_counter) 
     nullstelle = MAKE_ARRAY(row_counter)
     
     ndvi[*,*] = (data[ndvi_band2,*]-data[ndvi_band1,*]) / (data[ndvi_band2,*]+data[ndvi_band1,*])

   FOR i=0,row_counter-1,1 DO BEGIN
ta = SYSTIME(1)    
    
    IF ndvi[i] GT ndvi_threshold THEN BEGIN
    
     ;; ===== 1. Ableitung (Steigung) ===== data[Refl,Pixel]
     
          d1_data = deriv(data[*,i])
     
     ;; ===== 2. Ableitung (Krümmung) =====
     
          d2_data = deriv(d1_data)
     
     ;; ===== Smoothing with SavGol ====
     
          filter = SAVGOL(5,5,0,3)
          d2_data_sm = CONVOL(d2_data, filter)     

          d2_data_sm = d2_data_sm[WHERE(wl_bands GT 670.00 and wl_bands LT 800.00),*]
     
     ;; ===== Using Cubic Spline ====      
          
tb = SYSTIME(1) 
          
          d2_splined = SPLINE(wl_bands_cut, d2_data_sm, xarr_spline)     
                              
tc = SYSTIME(1)           
 
;if i EQ 0 then begin
;     e1 = plot(wl_bands_cut, d2_data_sm) 
;     e1 = plot(xarr_spline, d2_splined, /overplot, color='red')
;endif 
          
         ;; 2. Ableitung auf ABS setzen, um mit MIN-Funktion Nullstelle zu finden 
         
          ; --- alte Methode         
          ;          d2_splined = abs(d2_splined)
          ;          nullstelle[i] = xarr_spline[WHERE(d2_splined EQ min(d2_splined))]   
          ; ---

          loc = VALUE_LOCATE(d2_splined, 0)

             CASE 1 OF
                loc EQ -1: nullstelle[i] = 0
                loc EQ (N_Elements(d2_splined)-1): nullstelle[i] = 0
                0 GE d2_splined[loc+1]: IF Abs(d2_splined[loc]) GT Abs(d2_splined[loc+1]) THEN nullstelle[i] = xarr_spline[loc+1] ELSE nullstelle[i] = xarr_spline[loc] 
                ELSE:  nullstelle[i] = 0
             ENDCASE 
          
          if nullstelle[i] LT thr_low OR nullstelle[i] GT thr_high then begin
             nullstelle[i] = 0
          endif   

    ENDIF ELSE BEGIN
          
          nullstelle[i] = 0
          
    ENDELSE      
              
td = SYSTIME(1) 
 
;if i EQ 5 then begin
;     print, 'a Time: ' +STRING(tb-ta)
;     print, 'b Time: ' +STRING(tc-tb)
;     print, 'c Time: ' +STRING(td-tc)
;endif
   
   ENDFOR                     
;; ========== Ausschreiben der Position der Nullstelle und Beenden ==========

    outputImage.writeData, nullstelle
    
    ;; ----- progressbar
         progressDone += tilelines
         progressBar.setProgress, progressDone
    ;; ----- end progressbar
     
ENDWHILE

t2 = SYSTIME(1)
print, '1. Time: ' +STRING(t1-t0)
print, '2. Time: ' +STRING(t2-t1)
;============  V. CLEANUP

  inputImage.cleanup
  outputImage.cleanup
  progressBar.cleanup

end












;+
; :Hidden:
;-
;pro test_IP_Finder_processing
;
;  ; test your routine
;  
;  parameters = hash() ;replace by own hash definition
;  settings = IP_Finder_getSettings()
;  
;  result = IP_Finder_processing(parameters, settings)
;  print, result
;
;end  
