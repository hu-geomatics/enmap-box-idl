
function IP_Finder_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'Identifying Red Edge Inflection Point (iREIP)'

 hubAMW_frame  
  hubAMW_subframe , /row
   hubAMW_button,  Title=' About ', EventHandler='reip_button1'
   hubAMW_button,  Title=' Help  ', EventHandler='reip_button2'
   hubAMW_button,  Title='Contact', EventHandler='reip_button3'  
  
  hubAMW_frame ;,   Title =  'Input'
    hubAMW_inputImageFilename, 'inputImage', Title='Input-Image'

 hubAMW_frame, title = 'Thresholds'
    hubAMW_parameter, 'ndvit', Title='Set NDVI threshold for exclusion from processing:', Value=0.2 , /float
    hubAMW_parameter, 'lower_brd', Title='Set lower border of valid REIP-value (Nanometers):', Value=690 , /float
    hubAMW_parameter, 'higher_brd', Title='Set upper border of valid REIP-value (Nanometers):', Value=790 , /float

 hubAMW_frame
    hubAMW_outputFilename, 'outputImage', Title='Output-Image', value='REIP' ;value='E:\EnMAP\REIP'

  parameters = hubAMW_manage()
  
  return, parameters
end

;+
; :Hidden:
;-
;pro test_IP_Finder_getParameters
;
;  ; test your routine
;  settings = IP_Finder_getSettings()
;  parameters = IP_Finder_getParameters(settings)
;  print, parameters
;
;end  
