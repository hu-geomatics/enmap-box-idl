
pro IP_Finder_make
                            ;definiere alle benötigten Ordner und Dateipfade (hier zentral, egal ob sie wirklich benötigt werden)
  appDir = IP_Finder_getDirname()
  SAVFile = filepath('IP_Finder.sav', ROOT=appDir, SUBDIR='lib')
  codeDir =  file_dirname((routine_info('IP_Finder_make',/SOURCE)).path)
  helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
  
  enmapBoxDev_checkForUppercaseFilenames, codeDir
  
  if enmapboxmake.clean() then begin
                              ; de-install application by deleting the application folder (NOT the source code!)
    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
    return
  endif
  
                              ;Hier wird alles kopiert. Wenn Deine Tabelle.sav in einem der "_xyz" Ordner liegt wird sie mit kopiert
                              ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
                              ;und wenn nicht CopyFileOnly gesetzt ist, dann wird der ganze Code
                              ;als SAV file kompiliert, IDLDoc erstellt & geöffnet, etc.
  if ~enmapboxmake.CopyOnly() then begin
                               ; save routines inside SAVE file
    hubDev_compileDirectory, codeDir, SAVFile, NoLog=enmapboxmake.NoLog()
                                ; create IDLDOC documentation
    idldoc, ROOT=codeDir $
          , OUTPUT=helpOutputDir $
          , TITLE='IP_Finder Documentation' $
          , SUBTITLE='remote sensing made easy' $
          , OVERVIEW=filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') $
          , FORMAT_STYLE='rst' $
          , /COLOR_OUTPUTLOG $
          , /QUIET $
          , USER=1
  
    if ~enmapboxmake.NoShow() then begin
      (hubHelper()).openFile, /HTML, filepath('index.html', ROOT_DIR=helpOutputDir)
    endif
  endif
  
end




;pro IP_Finder_make, Cleanup=cleanup, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, Distribution=distribution
;
;  if keyword_set(cleanup) then begin
;    ; de-install application by deleting the application folder (NOT the source code!)
;    
;    appDir = IP_Finder_getDirname()
;    file_delete, appDir, /ALLOW_NONEXISTENT, /RECURSIVE
;    return
;  endif
;  
;  ; find source code and application directory  
;  
;  codeDir =  file_dirname((routine_info('IP_Finder_make',/SOURCE)).path)
;  appDir = IP_Finder_getDirname()
;  
;  ; check lower case *.pro filenames (important for auto-compiling under unix systems
;  
;  enmapBoxDev_checkForUppercaseFilenames, codeDir
;  
;  ; copy default folders ('_copyrights','_help','_lib','_resource') to application folder
;  
;  enmapBoxMake_copyDefaultDirs, codeDir, appDir
;  
;  if ~keyword_set(CopyOnly) then begin
;     ; save routines inside SAVE file
;     
;    SAVFile = filepath('IP_Finder.sav', ROOT=appDir, SUBDIR='lib')
;    hubDev_compileDirectory, codeDir, SAVFile, /NoLog
;    
;    if ~keyword_set(noIDLDoc) then begin
;      ; create IDLDOC documentation
;      
;      helpOutputDir = filepath('', ROOT_DIR=appDir, SUBDIR=['help','idldoc'])
;      hub_idlDoc, codeDir, helpOutputDir, 'IP_Finder Documentation' $
;                , Subtitle = 'IP_Finder Subtitle' $
;                , Overview = filepath('overview',ROOT_DIR=codeDir, SUBDIR='_IDLdoc') 
;     
;    endif
;    
;    ; create distributen
;    
;    if isa(distribution) then begin
;    
;      if ~isa(distribution, 'string') then begin
;        title = 'Select a directory for writing the distribution.'
;        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
;        if directory eq '' then return
;      endif else begin
;        directory = distribution
;      endelse
;
;      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
;      file_copy, appDir, destination, /OVERWRITE, /RECURSIVE
;      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
;      file_copy, codeDir, destination, /OVERWRITE, /RECURSIVE
;      
;    endif
;    
;  endif
;end
