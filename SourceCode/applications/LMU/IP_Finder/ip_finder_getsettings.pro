;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('IP_Finder.conf', ROOT=IP_Finder_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, IP_Finder_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function IP_Finder_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('IP_Finder.conf', ROOT=IP_Finder_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = 'iREIP v.1.0' ;settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_IP_Finder_getSettings

  print, IP_Finder_getSettings()

end
