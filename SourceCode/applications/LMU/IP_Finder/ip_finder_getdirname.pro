;+
; :Author: <author name> (<email>)
;-

function IP_Finder_getDirname
  
  result = filepath('IP_Finder', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_IP_Finder_getDirname

  print, IP_Finder_getDirname()
  print, IP_Finder_getDirname(/SourceCode)

end
