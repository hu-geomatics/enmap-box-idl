;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `IP_Finder_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `IP_Finder_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `IP_Finder_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro IP_Finder_application, applicationInfo
  
  ; get global settings for this application
  settings = IP_Finder_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = IP_Finder_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = IP_Finder_processing(parameters, settings)
    
;    if parameters['showReport'] then begin
;      IP_Finder_showReport, reportInfo, settings
;    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_IP_Finder_application
  applicationInfo = Hash()
  IP_Finder_application, applicationInfo

end  
