pro reip_button1, resultHash

ok = dialog_message('The Red Edge Inflection Point (REIP) is considered a highly accurate indicator for the nutrient status of plants. In nutrient deficiency, the REIP moves towards lower frequencies (Red Shift), while on a nutrient oversupply on the other hand, it moves to higher frequencies / lower wavelengths (Blue Shift).                                                                                                                                                                                                          Using a cubic spline on the second derivative of the reflectance signal allows the precise identification of the Red Edge Inflection Point in the data. The output image contains the exact wavelength of the REIP for each pixel.',$
 TITLE='REIP Finder - short description', /INFORMATION)
 


end