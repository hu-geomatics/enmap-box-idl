pro applications_makeExternals

  ;in case it is needed...
  CopyOnly=enmapboxmake.copyOnly()
  NoIDLDoc=enmapboxmake.noIDLDoc()
  nolog=enmapboxmake.nolog()

  imageSVM_make
  imageRF_make
  imageMath_make

  SyntMIX_make
  kMeans_make
  hublibmix_make
  
  spinmine_make
  autoPLSR_make
;  MaxEntWrapper_make, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc, nolog=nolog
  lmu_make
  oawi_make
  EnWaterMAP_make
  ensomapsuite_make
  engeomap_make
;  Phytobenthos_make, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc, nolog=nolog
;  oceancolorchlorophyll_make, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, nolog=nolog
;  imageIVM_make
  iterativeSMA_make
;  otbApplications_make, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, nolog=nolog
;  otbBandMath_make, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, nolog=nolog
;  starfm_make, CopyOnly=copyOnly, NoIDLDoc=noIDLDoc, nolog=nolog
;  TimeStats_make
  ; not released
  ;gfzEnMAP_make, CopyOnly=copyOnly, NoIDLDoc = noIDLDoc, nolog=nolog


end