function timesat_06_createTIMESATSettingsFile_getSettings, settings

  default = orderedHash()
  default['%Job_name (no blanks)']                                      = ''
  default['%Image /series mode (1/0)']                                  = '1'
  default['%Trend (1/0)']                                               = '0'
  default['%Use mask data (1/0)']                                       = '1'
  default['%Data file list/name']                                       = ''
  default['%Mask file list/name']                                       = ''
  default['%Image file type']                                           = '2'
  default['%Byte order (1/0)']                                          = '0'
  default['%File dimension (nrow ncol)']                                = ''
  default['%Processing window (start row stop row start col stop col)'] = ''
  default['%No. years and no. points per year']                         = ''
  default['%Valid data range (lower upper)']                            = '-10000 10000'
  default['%Mask range 1 and weight']                                   = ''
  default['%Mask range 2 and weight']                                   = ''
  default['%Mask range 3 and weight']                                   = ''
  default['%Amplitude cutoff value']                                    = '0'
  default['%Print functions and weights (1/0)']                         = '0'
  default['%Output files (1/0 1/0 1/0)']                                = '1 1 0'
  default['%Use land cover (1/0)']                                      = '0'
  default['%Name of landcover file']                                    = ' '
  default['%Spike method']                                              = '0'
  default['%Spike value']                                               = '2'
  default['%No. of landcover classes']                                  = '1'
  default['************']                                               = ' '
  default['%Land cover code for class  1']                              = '1'
  default['%Season parameter']                                          = '1'
  default['%No. of envelope iterations (1-3)']                          = '1'
  default['%Adaptation strength (1-10)']                                = '2'
  default['%Force minimum (1/0) and value']                             = '0 -99999'
  default['%Fitting method (1-3)']                                      = '3'
  default['%Weight update method']                                      = '1'
  default['%Window size for Sav-Gol.']                                  = '4'
  default['%Season start method']                                       = '1'
  default['%Season start / stop values']                                = '0.5 0.5'

  return, default+settings

end

pro timesat_06_createTIMESATSettingsFile, p, settings, calculate

  if ~calculate then return
  
  settingsAll = timesat_06_createTIMESATSettingsFile_getSettings(settings)
  settingsAll['%Job_name (no blanks)']       = p.tile
  settingsAll['%File dimension (nrow ncol)'] = strjoin(strtrim((p.headerENVIFullTile).getSpatialSize(),2),' ')
  numberOfYears = strtrim(fix(strmid(p.doyEnd,0,4))-fix(strmid(p.doyStart,0,4))+1,2)
  settingsAll['%No. years and no. points per year']                         = numberOfYears+' 46'
  processingWindow = strjoin(strtrim([p.processingLineRange+1, p.processingSampleRange+1],2),' ')
  settingsAll['%Processing window (start row stop row start col stop col)'] = processingWindow
  settingsText = (settingsAll.values()).toArray()+' '+(settingsAll.keys()).toArray()

  filenames = timesat_getFilenames(p)
  hubIOASCIIHelper.writeFile, filenames.settingsFile, settingsText

  print, '*** _06_createTIMESATSettingsFile ***'
  print, '  '+filenames.settingsFile
  print, 'done'
  print, ''
end