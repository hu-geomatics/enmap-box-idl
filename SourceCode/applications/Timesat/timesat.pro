pro timesat,tile
  
  ; define inputs and settings
  
  parameters = dictionary()
  parameters.pathPython27     = 'C:\Python27\ArcGISx6410.2'
  parameters.pathTimesat      = 'O:\timesat'
  parameters.pathInputMODIS   = 'O:\00_Datasets\MODIS 13Q1'
  parameters.pathOutputMODIS  = 'O:\01_Processing\Processed_Data\MODIS\MODIS13Q1'
  parameters.tiles            = tile ;(['h25v06','h25v07']
  parameters.sensors          = ['MOLA', 'MOLT']
  parameters.doyStart         = '2001001'
  parameters.doyEnd           = '2013365'
  parameters.subsetSampleRange = [3000,3009]
  parameters.subsetLineRange   = [1000,1009]
  parameters.useSubset         = 0b
  parameters.processingSampleRange = parameters.subsetSampleRange
  parameters.processingLineRange   = parameters.subsetLineRange
  parameters.seasonalParameters        = ['','SoS', 'EoS', 'LoS', 'BV', 'MoS', 'Mfit', 'Amp', 'LDer', 'RDer', 'LInt', 'SInt']
  parameters.seasonalParametersIndices = [4,6];,7,8,9]
  parameters.seasonStartDOY  = '001'
  parameters.seasonSavety = 10

  ; TIMESAT settings

  tsSettings = orderedHash()
  tsSettings['%Valid data range (lower upper)']                            = '-10000 10000'
  tsSettings['%Mask range 1 and weight']                                   = '0 0 1'
  tsSettings['%Mask range 2 and weight']                                   = '1 1 0.5'
  tsSettings['%Mask range 3 and weight']                                   = '2 3 0'
  tsSettings['%Spike method']                                              = '0'
  tsSettings['%Spike value']                                               = '2'
  tsSettings['%Season parameter']                                          = '1'
  tsSettings['%No. of envelope iterations (1-3)']                          = '1'
  tsSettings['%Adaptation strength (1-10)']                                = '2'
  tsSettings['%Fitting method (1-3)']                                      = '3'
  tsSettings['%Season start method']                                       = '1'
  tsSettings['%Season start / stop values']                                = '0.5 0.5'

  ; turn processing steps on=1 or off=0
  
  calculations = dictionary()
  calculations._01_downloadMODIS             = 0b ; not implemented yet
  calculations._02_convertHDF                = 0b
  calculations._03_spatialSubset             = 0b
  calculations._04_createTIMESATMissingDummy = 1b
  calculations._05_createTIMESATFilelist     = 1b
  calculations._06_createTIMESATSettingsfile = 0b
  calculations._07_createTIMESATModel        = 0b
  calculations._08_extractSeasonalParameters = 0b
  
  ; run 
   
  timesat_00_main, parameters, tsSettings, calculations
 
end
