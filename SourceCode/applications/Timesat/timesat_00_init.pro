pro timesat_00_init, parameters

  ; prepare full tile and subsetted tile meta information
  
  parameters.tile = (parameters.tiles)[0]
  parameters.sensor = (parameters.sensors)[0] 
  dirnames = timesat_getDirnames(parameters)
  firstFilename = (file_search(dirnames.envi,'*pixel_reliability_16bit.dat'))[0]
  fullHeader = (hubIOImgInputImage(firstFilename)).getHeader()
  subsetHeader = (hubIOImgInputImage(firstFilename)).getHeader()
  mapInfo = fullHeader.getMeta('map info')+dictionary()
  mapInfo.PIXELX = mapInfo.PIXELX - (parameters.processingSampleRange)[0]
  mapInfo.PIXELY = mapInfo.PIXELY - (parameters.processingLineRange)[0]
  subsetHeader.setMeta, 'map info',                 mapInfo
  subsetHeader.setMeta, 'coordinate system string', 'PROJCS["Sinusoidal",GEOGCS["GCS_unnamed ellipse",DATUM["D_unknown",SPHEROID["Unknown",6371007.181,0]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Sinusoidal"],PARAMETER["central_meridian",0],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]'
  fullHeader.setMeta,   'coordinate system string', 'PROJCS["Sinusoidal",GEOGCS["GCS_unnamed ellipse",DATUM["D_unknown",SPHEROID["Unknown",6371007.181,0]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Sinusoidal"],PARAMETER["central_meridian",0],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]'
  parameters.headerENVIFullTile = fullHeader
  parameters.headerENVISubsetTile = subsetHeader

end
