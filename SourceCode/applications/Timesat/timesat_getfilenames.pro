function timesat_getFilenames, p
  
  dirnames = timesat_getDirnames(p)
  filenames = dictionary()
  if p.hubIsa('tile') then begin
    filenames.dataFilelistMerged =    filepath(p.tile+'_VI.files.merged.txt', ROOT_DIR=dirnames.timesat.job )
    filenames.maskFilelistMerged =    filepath(p.tile+'_PR.files.merged.txt', ROOT_DIR=dirnames.timesat.job)
    filenames.dataFilelistComposite = filepath(p.tile+'_VI.files.composite.txt', ROOT_DIR=dirnames.timesat.job)
    filenames.maskFilelistComposite = filepath(p.tile+'_PR.files.composite.txt', ROOT_DIR=dirnames.timesat.job)
    filenames.settingsFile = filepath(p.tile+'_settings.set',      ROOT_DIR=dirnames.timesat.model)
    filenames.missingFile  = filepath('missing.dat',               ROOT_DIR=dirnames.outputMODISTile)
  endif
  return, filenames
end
