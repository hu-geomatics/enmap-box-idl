pro timesat_ar
  
  ; define inputs and settings
  
  parameters = dictionary()
  parameters.pathPython27     = 'C:\Python27\ArcGISx6410.2'
  parameters.pathTimesat      = 'O:\timesat'
  parameters.pathInputMODIS   = 'Y:\DAR_Data\00_Datasets\MODIS 13Q1'
  parameters.pathOutputMODIS  = 'Y:\DAR_Data\01_Processing\Processed_Data\MODIS\MODIS13Q1'
  parameters.tiles            = ['h26v06']
  parameters.sensors          = ['MOLA', 'MOLT']
  parameters.logFile          = hub_getUserTemp('timesat_ar')
  parameters.logShow          = 1b
  parameters.doyStart         = '2001001'
  parameters.doyEnd           = '2013365'
  
  parameters.processingSampleRange = [3000,3009] ; zero based indices
  parameters.processingLineRange   = [1000,1009] ; zero based indices
  parameters.seasonalParameters           = ['','SoS','EoS','LoS','BV','MoS','Mfit','Amp','LDer','RDer','LInt','SInt']
                                  ; indices are:   1     2     3    4     5      6     7      8      9     10     11 
  parameters.seasonalParametersIndices = [1:11]
  parameters.seasonStartDOY  = '001'
  parameters.seasonSavety = 10
  parameters.useNearestNeighbourComposite = 1b
  parameters.fillMissingMode = (['precursor','missing'])[0] ; 0=precursor 1=missing
  

  ; TIMESAT settings

  tsSettings = orderedHash()
  tsSettings['%Valid data range (lower upper)']                            = '-10000 10000'
  tsSettings['%Mask range 1 and weight']                                   = '0 0 1'
  tsSettings['%Mask range 2 and weight']                                   = '1 1 0.5'
  tsSettings['%Mask range 3 and weight']                                   = '2 3 0'
  tsSettings['%Spike method']                                              = '0'
  tsSettings['%Spike value']                                               = '2'
  tsSettings['%Season parameter']                                          = '1'
  tsSettings['%No. of envelope iterations (1-3)']                          = '1'
  tsSettings['%Adaptation strength (1-10)']                                = '2'
  tsSettings['%Fitting method (1-3)']                                      = '3'
  tsSettings['%Season start method']                                       = '1'
  tsSettings['%Season start / stop values']                                = '0.5 0.5'

  ; turn processing steps on=1 or off=0
  
  calculations = dictionary()
  ; the following calculations are part of the preprocessing
  calculations._01_downloadMODIS             = 0b ; not implemented yet
  calculations._02_convertHDF                = 0b
  calculations._03_createComposite           = 1b
  calculations._04_createTIMESATMissingDummy = 0b
  ; the following results are settings-specific and will be stored in a subfolder defined by parameters.jobName 
  parameters.jobName = 'mySettings2'    ; must be valid folder name without whitespaces
  calculations._05_createTIMESATFilelist     = 0b
  calculations._06_createTIMESATSettingsfile = 0b
  calculations._07_createTIMESATModel        = 0b
  calculations._08_extractSeasonalParameters = 0b
  
  ; run 
   
  timesat_00_main, parameters, tsSettings, calculations
 
end
