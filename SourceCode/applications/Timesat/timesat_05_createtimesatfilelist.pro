pro timesat_05_createTIMESATFilelist, p, tsSettings, calculate

  filenames = timesat_getFilenames(p)
  if p.useNearestNeighbourComposite then begin
    tsSettings['%Data file list/name'] = filenames.dataFilelistComposite
    tsSettings['%Mask file list/name'] = filenames.maskFilelistComposite
  endif else begin
    tsSettings['%Data file list/name'] = filenames.dataFilelistMerged
    tsSettings['%Mask file list/name'] = filenames.maskFilelistMerged
  endelse
  
  if ~calculate then return

  timesat_05a_createTIMESATFilelist, p
  timesat_05b_createTIMESATFilelist, p


  print, '*** _05_createTIMESATFilelist ***'
  print, 'a) merge sensors
  print, '    '+filenames.dataFilelistMerged
  print, '    '+filenames.maskFilelistMerged
  print, 'b) sensor composite (nearest neighbour resampling)
  print, '    '+filenames.dataFilelistComposite
  print, '    '+filenames.maskFilelistComposite
  print, 'done'
  print, ''
  
end
