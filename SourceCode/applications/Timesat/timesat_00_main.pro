pro timesat_00_main, parameters, tsSettings, calculations

  startTime = hubTime()

;  timesat_00_init, parameters

  foreach tile, parameters.tiles do begin
    foreach sensor, parameters.sensors do begin
      parameters.tile = tile
      parameters.sensor = sensor
      timesat_02_convertHDF,    parameters, calculations._02_convertHDF
    endforeach
    timesat_03_createComposite, parameters, calculations._03_createComposite
  endforeach
  
  foreach tile, parameters.tiles do begin
    parameters.tile = tile
    timesat_04_createTIMESATMissingDummy, parameters,             calculations._04_createTIMESATMissingDummy
    timesat_05_createTIMESATFilelist,     parameters, tsSettings, calculations._05_createTIMESATFilelist
    timesat_06_createTIMESATSettingsFile, parameters, tsSettings, calculations._06_createTIMESATSettingsfile
    timesat_07_createTIMESATModel,        parameters,             calculations._07_createTIMESATModel
    timesat_08_extractSeasonalParameters, parameters,             calculations._08_extractSeasonalParameters
  endforeach
  
  print, '*** TIMESAT finished ***'
  print, 'Overall processing time: '+startTime.elapsed(/Report)

end
