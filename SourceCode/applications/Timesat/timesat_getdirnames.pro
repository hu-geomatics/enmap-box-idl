function timesat_getDirnames, p
  
  dirnames = dictionary()
  dirnames.inputMODIS = p.pathInputMODIS
  dirnames.outputMODIS = p.pathOutputMODIS
  dirnames.timesat = dictionary()
  dirnames.timesat.root            = filepath('', ROOT_DIR=dirnames.outputMODIS,     SUBDIRECTORY='TIMESAT')
  
  if p.hubIsa('tile') then begin
    dirnames.timesat.job            = filepath('', ROOT_DIR=dirnames.timesat.root,         SUBDIRECTORY=[p.tile, p.jobName])
    dirnames.timesat.model         = filepath('', ROOT_DIR=dirnames.timesat.job,           SUBDIRECTORY='model')
    dirnames.timesat.metricsStacks = filepath('', ROOT_DIR=dirnames.timesat.job,           SUBDIRECTORY='metricsStacks')
    dirnames.timesat.metricsBands  = filepath('', ROOT_DIR=dirnames.timesat.job,           SUBDIRECTORY='metricsBands')
    dirnames.outputMODISTile       = filepath('', ROOT_DIR=dirnames.outputMODIS,           SUBDIRECTORY=p.tile)
    dirnames.composite             = filepath('', ROOT_DIR=dirnames.outputMODISTile,       SUBDIRECTORY='COMPOSITE')
  end
  
  if p.hubIsa(['sensor','tile'], /EvaluateAND) then begin
    dirnames.hdf                   = filepath('', ROOT_DIR=dirnames.inputMODIS,            SUBDIRECTORY=[p.sensor,p.tile])
    dirnames.envi                  = filepath('', ROOT_DIR=dirnames.outputMODIS,           SUBDIRECTORY=[p.tile,p.sensor])
  endif
  
  return, dirnames
end
