pro timesat_07_createTIMESATModel, p, calculate
  
  if ~calculate then return

  ; create batch

  dirnames = timesat_getDirnames(p)
  filenames = timesat_getFilenames(p)
  exeFilename = filepath('TSF_process.exe', ROOT_DIR=p.pathTimesat, SUBDIRECTORY=['timesat_fortran','main'])
  batchText = [exeFilename+' '+filenames.settingsFile]
  batchFilename = filepath(p.tile+'_TSF_process_batch.bat', ROOT_DIR=dirnames.timesat.model)
  hubIOASCIIHelper.writeFile, batchFilename, batchText

  ; run batch
  cd, dirnames.timesat.model, CURRENT=oldWorkDir 
  spawn, batchFilename, spawnResult, SpawnError

  if p.logShow then begin
    logFile = p.logFile+'_07.txt'
    hubIOASCIIHelper.writeFile, logFile, [spawnResult, SpawnError]
    hubHelper.openFile, logFile
  endif
  cd, oldWorkDir
  
  print, '*** _07_createTIMESATModel ***'
  print, 'Result Folder: '+dirnames.timesat.model
  print, 'done'
  print, ''
  
end