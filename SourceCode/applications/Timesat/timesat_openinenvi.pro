pro timesat_openInENVI

  ; init timeseries
  timeseriesFolder = 'Y:\DAR_Data\01_Processing\Processed_Data\MODIS\MODIS13Q1\h26v06'
  timeseries = hubRSTimeseriesModisPR(timeseriesFolder)
  
  ; temporal subset
  timeseries = timeseries.subset(hubTime(2011),hubTime(2012))
  
  
;  histos = timeseries.getCloudFractions(/Plot)
;  return
;  
  ; timeseries =  timeseries.subset(hubTime(2011), hubTime(2013))

  ul = [8895604.1573329996, 3335851.5589999999]
  pixelSize = [231.65636, 231.65636]
  subset=[ul,ul+[+1,-1]*pixelSize*[300,300]]
 ; timeseries.openInENVI, 'ndvi', 'mask eq 0 or mask eq 1', -999, Subset=subset
  timeseries.openInENVI, 'abs(doy-TARGETDOY[i])', 'mask eq 0 or mask eq 1', -1, Subset=subset, TARGETDOY=timeseries.scenes.time.doy


end