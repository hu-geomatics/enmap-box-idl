folder <- "Z:/01_Processing/Processed_Data/MODIS/MODIS13Q1/TIMESAT/h27v05_set_01/"
setwd(folder)

files <- c(
  files.s1 <- dir(pattern="s1$"),
  files.s2 <- dir(pattern="s2$"),
  files.ns <- dir(pattern="nseas$"))

data.path <- "Z:/01_Processing/Processed_Data/MODIS/MODIS13Q1/h27v05/MOLT/"
xmin <- 1
xmax <- 4800
ymin <- 2000
ymax <- 4800

source("Z:/01_Processing/R_Scripts/TIMESAT_MODIS_Subset.R")
sub.extent <- MOD.extent(xmin,xmax,ymin,ymax,data.path)

samples       <- xmax-xmin+1
lines         <- ymax-ymin+1
bands         <- 1
header.offset <- 0
file.type     <- "ENVI Standard"
data.type     <- 2
interleave    <- "bsq"
byte.order    <- 0
map.info      <- paste("{Sinusoidal, 1, 1, ",sub.extent[[1]]@xmin,", ", sub.extent[[1]]@ymax, ", ", sub.extent[[2]], ",", sub.extent[[3]], "}", sep="")
cs.string     <- "{PROJCS[\"Sinusoidal\",GEOGCS[\"GCS_unnamed ellipse\",DATUM[\"D_unknown\",SPHEROID[\"Unknown\",6371007.181,0]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Sinusoidal\"],PARAMETER[\"central_meridian\",0],PARAMETER[\"false_easting\",0],PARAMETER[\"false_northing\",0],UNIT[\"Meter\",1]]}"
band.names    <- "{Band 1}"


for (f in files){
header.file <- paste(folder,f,".hdr",sep="")
file.create(header.file)
    
    cat(paste("ENVI",
              "description = {",
              paste(folder,f,"}",sep=""),
              paste("samples = ",samples),
              paste("lines = ",lines),
              paste("bands = ",bands),
              paste("header offset = ",header.offset),
              paste("file type = ", file.type), 
              paste("data type = ", data.type), 
              paste("interleave = ", interleave),
              paste("byte order = ", byte.order), 
              paste("map info = ", map.info),
              paste("coordinate system string = ", cs.string),
              paste("band names = ", band.names),
              sep="\n"),
              file=header.file)
  }
