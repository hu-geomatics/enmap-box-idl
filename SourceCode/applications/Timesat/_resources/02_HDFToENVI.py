#import modules
import arcpy

hdfFilenames  = p['hdfFilenames']
enviFilenames = p['enviFilenames']
layerNames    = p['layerNames']
layerIndices  = p['layerIndices']
 
for i in range(len(hdfFilenames)):

  hdfFilename = hdfFilenames[i]
  enviFilename = enviFilenames[i]
  
  for layer in range(len(layerNames)):
    arcpy.ExtractSubDataset_management(hdfFilename, enviFilename+layerNames[layer], layerIndices[layer])