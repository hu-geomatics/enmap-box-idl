pro timesat_05b_createTIMESATFilelist, p

  
  yearStart = fix(strmid(p.doyStart,0,4))
  yearEnd = fix(strmid(p.doyEnd,0,4))
    
  years = [yearStart:yearEnd]
  doys = [1:365:8]
  nyears = n_elements(years)
  ndoys = n_elements(doys)
  yeardoy = rebin(transpose(years),ndoys,nyears)*1000l + rebin(doys,ndoys,nyears)
  times = hubTime(yeardoy, /DOY)
  syeardoy = times.syear+times.sdoy

  basenames = 'COMPOSITE.A'+syeardoy+'.'+p.tile+'.005_250m_08_days_'
  
  dirnames = timesat_getDirnames(p)  
  allDataText = filepath(basenames+'NDVI.dat', ROOT_DIR=dirnames.composite)
  allMaskText = filepath(basenames+'pixel_reliability_16bit.dat', ROOT_DIR=dirnames.composite)

  filenames = timesat_getFilenames(p)
  hubIOASCIIHelper.writeFile, filenames.dataFilelistComposite, [strtrim(nyears*ndoys,2), allDataText]
  hubIOASCIIHelper.writeFile, filenames.maskFilelistComposite, [strtrim(nyears*ndoys,2), allMaskText]
end
