pro timesat_08_extractSeasonalParameters, p, calculate
  
  if ~calculate then return

  ; create batch file

  yearStart = fix(strmid(p.doyStart,0,4))
  yearEnd = fix(strmid(p.doyEnd,0,4))
  numberOfYears = yearEnd-yearStart+1
  
  dirnames = timesat_getDirnames(p)
  tpaFilename = filepath(p.tile+'_TS.tpa', ROOT_DIR=dirnames.timesat.model)
  exeFilename = filepath('TSF_seas2img.exe', ROOT_DIR=p.pathTimesat, SUBDIRECTORY=['timesat_fortran','tools'])
  dataIgnoreValue = '-10000'
  failValue = dataIgnoreValue
  NAValue   = dataIgnoreValue
  dataTypeTIMESAT = '2'  ; 1=8-bit unsigned integer, 2=16-bit signed integer, 3=32-bit real
  case dataTypeTIMESAT of
    '1' : dataTypeIDL  = '1' ; byte
    '2' : dataTypeIDL  = '2' ; short integer
    '3' : dataTypeIDL  = '4' ; 32-bit float
  endcase
  seasonStartIndex = round(fix(p.seasonStartDOY)/8.)+1
  batchText = list()
  for year=yearStart,yearEnd do begin
    startFile = strtrim((seasonStartIndex+(year-yearStart)*46-p.seasonSavety) > 1  ,2)
    endFile   = strtrim((startFile+46-1+p.seasonSavety) < (numberOfYears*46), 2)
    foreach i, p.seasonalParametersIndices do begin
      resultFilename = filepath((p.seasonalParameters)[i]+'_'+strtrim(year,2)+'_'+p.tile, ROOT_DIR=dirnames.timesat.metricsBands)
      batchText.add, exeFilename+' '+tpaFilename+' '+strtrim(i,2)+' '+startFile+' '+endFile+' '+failValue+' '+NAValue+' '+resultFilename+' '+dataTypeTIMESAT
    endforeach
  endfor 
  batchText = batchText.toArray()
  
  batchFilename = filepath(p.tile+'_TSF_seas2img_batch.bat', ROOT_DIR=dirnames.timesat.model)
  hubIOASCIIHelper.writeFile, batchFilename, batchText
  
  ; run batch
  file_mkdir, dirnames.timesat.metricsBands
  spawn, batchFilename, spawnResult, SpawnError
  
  if p.logShow then begin
    logFile = p.logFile+'_08.txt'
    hubIOASCIIHelper.writeFile, logFile, [spawnResult, SpawnError]
    hubHelper.openFile, logFile 
  endif

  dirnames = timesat_getDirnames(p)
  firstFilename = (file_search(dirnames.envi,'*pixel_reliability_16bit.dat'))[0]
  spatialSize = hubIOImg_getMeta(firstFilename, ['samples','lines'])

  header = hubIOImgHeader()
  header.setMeta, 'samples', hubMathSpan(p.processingSampleRange)+1
  header.setMeta, 'lines', hubMathSpan(p.processingLineRange)+1
  header.setMeta, 'bands',1
  header.setMeta, 'file type', 'ENVI Standard'
  header.setMeta, 'data type', dataTypeIDL
  header.setMeta, 'interleave', 'bsq'
  header.setMeta, 'byte order', 0
  
  ; create stack
  metaHash = hash()
  metaHash['band names']               = 'Year ' + string(format='(I4.4)',[yearStart:yearEnd])
  metaHash['data ignore value']        = dataIgnoreValue
  metaHash['map info']                 = (p.headerENVISubsetTile).getMeta('map info')
  metaHash['coordinate system string'] = (p.headerENVISubsetTile).getMeta('coordinate system string')

  foreach seasonalParameter, (p.seasonalParameters)[p.seasonalParametersIndices] do begin
    foreach outputType,['nseas','s1','s2'] do begin
      filenames = file_search(dirnames.timesat.metricsBands,seasonalParameter+'*_'+outputType)
      foreach filename,filenames do begin
        header.writeFile, filename+'.hdr'
      endforeach
      stackFilename = filepath(seasonalParameter+'_'+p.tile+'_'+strmid(p.doyStart,0,4)+'-'+strmid(p.doyEnd,0,4)+'_'+outputType+'_stack.bsq', ROOT_DIR=dirnames.timesat.metricsStacks)
      parameters = hash('inputImages',filenames, 'outputImage', stackFilename, 'metaHash', metaHash)
      hubApp_Stacking_processing, parameters, settings
    endforeach
  endforeach
  
  ; delete single band images
  file_delete, dirnames.timesat.metricsBands, /ALLOW_NONEXISTENT, /RECURSIVE
  
  print, '*** _08_extractSeasonalParameters ***'
  print, '  batch file: '+batchFilename
  print, '  results:    '+dirnames.timesat.metricsStacks
  print, 'done'
  print, ''
  
end