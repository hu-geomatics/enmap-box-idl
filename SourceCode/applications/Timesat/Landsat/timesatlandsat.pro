pro timesatLandsat

;  resolve_all ; prevents most of the "% Compiled module" messages 

  timeseries = hubRSTimeseriesLandsat('G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014')
;  print, timeseries.cloudCover()
;  print, timeseries.doyResidual()

  timeseries.openInENVI, 'doy-tDoy', Subset=subset
  
  ul = [173400.00, -1763100.0]
  subset=[ul,ul+[+30,-30]*[300,300]]
  timeseries.openInENVI, 'ndvi*(fmask eq 0)', Subset=subset
  
;  timeseries = timeseries.subset(hubTime(2001),hubTime(2002))
;  timeseries = timeseries.subset(hubTime(2001),hubTime(2001020))
  
;  timeseries.openInENVI, 'ndvi*(fmask eq 0)'
 ; timeseries.openInENVI, ['ndvi','fmask'], [0,0], 'ndvi*(fmask eq 0)', Subset=subset

  ndviExpression = '(float(toa[3]-toa[2])/float(toa[3]+toa[2])) * (fmask eq 0)'    ; (nir-red)/(nir+red)
;  timeseries.openInENVI, ndviExpression, Subset=subset

  
    
  return
;  timeseries.scenes.parseMTL
;  print, transpose(timeseries.scenes.mtl.LANDSAT_SCENE_ID)+' '+transpose(timeseries.scenes.mtl.CORNER_UL_PROJECTION_X_PRODUCT)+' '+transpose(timeseries.scenes.mtl.CORNER_UL_PROJECTION_Y_PRODUCT)
;
;  
;  return
;  
;  
;  eastings = list()
;  northings =  list()
;  foreach filename, filenames do begin
;    mapInfo = hubIOImg_getMeta(filename, 'map info')
;    eastings.add, mapInfo.EASTING
;    northings.add, mapInfo.NORTHING
;  endforeach
;  eastings = eastings.toArray()
;  northings = northings.toArray()
;  stop
;  print, transpose(filenames)
;  
;  
  
;  timeseries = hubRSTimeseriesLandsat('G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014')
;  timeseries = timeseries.subset(hubTime(2001),hubTime(2001100))
;  filenames = timeseries.scenes.path.toa
;  pathVRT = 'G:\temp\temp_ar\myBand1.vrt'
;  vb = VRTBuilder(SHOWCOMMANDS=1)
;  foreach path, filenames do vb.addVRTRasterBand, path, 0, DESCRIPTION=FILE_BASENAME(path)
;  vb.writeVRT, pathVRT
;
;  
 
;  - "Rohdaten" Zukunft nach LEDAPS (incl. Wolkenmaske) in G:\Rohdaten\Brazil\Raster\LANDSAT\2000_2014
  ; warten aus LEDAPS für LC8
  
  
  ;- FMASK generieren (vorangegangene Sensitivitätsanalyse)
  ;timeseries.scenes.calculateFmask, 3, 3, 0, 10, DataType=2

  ; spatial subset

  ;- Veg. index berechnen (und Wolken (FMASK) maskieren)
  timeseries.scenes.calculateNDVI, Scale=1000, DataType=2
  timeseries.scenes.calculateNBR, Scale=1000, DataType=2
  timeseries.scenes.calculateTCG, DataType=2
  timeseries.scenes.calculateTCW, DataType=2
  timeseries.scenes.calculateTCB, DataType=2
  

;  timesat = hubTimesat(timeseries, 'G:\temp\temp_ar\Timesat')
;  timesat.createFilelist, 'ndvi', 'fmask'


 
  ;- Create file list for timesat with dummy files for missing observations
;  missing = 'missing.dat'
;  filelist = timeseries.getFilenames('ndvi', Missing=missing)
  

;  timesat = hubRSTimesat(timeseries)
  

  
;  - TIMESAT processing
;  - TSF Routine fit2img
;  - fitted values mit header ausschreiben und image stack bauen

end
