pro timesat_03_createComposite_singleDOY, p, targetYear, targetDoy, Info=info

  validDoyRange = [-7,7]+targetDoy   ; do not enlarge the range! Would require changes in the below code! Call Andreas
  candidateDoys = [-2:1]*8+targetDoy ; could be derived from validDoyRange
  
  ; include doy 1 of next year if needed
  ndaysThisYear = fix(julday(1, 1, targetYear+1, 0, 0, 0)-julday(1, 1, targetYear, 0, 0, 0))
  if targetDoy+7 gt ndaysThisYear then begin
    candidateDoys  = [candidateDoys,1]
    candidateYears = [replicate(targetYear, n_elements(candidateDoys)), targetYear+1]
  endif else begin
    candidateYears =  replicate(targetYear, n_elements(candidateDoys))
  endelse
  isNextYear = (candidateYears eq targetYear+1) or (targetDoy eq 361)
  
  ; include doy 353, 361 of last year if needed
  ndaysLastYear = fix(julday(1, 1, targetYear, 0, 0, 0)-julday(1, 1, targetYear-1, 0, 0, 0))
  if min(candidateDoys) lt 1 then begin
    candidateDoys = [353, 361, candidateDoys[where(candidateDoys ge 1, count)]]
    candidateYears = [targetYear-1,targetYear-1,replicate(targetYear, count)]
  endif else begin
    candidateYears =  replicate(targetYear, n_elements(candidateDoys))
  endelse
  isLastYear = candidateYears eq targetYear-1
  
  stargetDoy = string(targetDoy, FORMAT='(i3.3)')
  scandidateDoys = string(candidateDoys, FORMAT='(i3.3)')
  scandidateYears = string(candidateYears, FORMAT='(i4.4)')
  stargetYear = string(targetYear, FORMAT='(i4.4)')
  
  ; get candidate filenames
  dirnames = timesat_getDirnames(p)
  root = filepath('', ROOT_DIR=dirnames.outputMODIS, SUBDIRECTORY=p.tile)
  postfixDOY = 'composite_DOY.dat'
  postfixVI = 'NDVI.dat'
  postfixMask = 'pixel_reliability_16bit.dat'
  
  MOLABasenames = 'MYD13Q1.A'+scandidateYears+scandidateDoys+'.'+p.tile+'.005_250m_16_days_'
  MOLAFilenames = filepath(MOLABasenames , ROOT_DIR=root+'MOLA')
  MOLTBasenames = 'MOD13Q1.A'+scandidateYears+scandidateDoys+'.'+p.tile+'.005_250m_16_days_'
  MOLTFilenames = filepath(MOLTBasenames , ROOT_DIR=root+'MOLT')
  
  indices = where(/NULL, file_test([MOLAFilenames,MOLTFilenames]+postfixDOY))
  doyCandidateFilenames = ([MOLAFilenames,MOLTFilenames]+postfixDOY)[indices]
  viCandidateFilenames = ([MOLAFilenames,MOLTFilenames]+postfixVI)[indices]
  maskCandidateFilenames = ([MOLAFilenames,MOLTFilenames]+postfixMask)[indices]
  isLastYear = ([isLastYear,isLastYear])[indices]
  isNextYear = ([isNextYear,isNextYear])[indices]
  
  ; read data
  header = hubIOImg_getMeta(doyCandidateFilenames[0])
  size = [header.getMeta('samples'), header.getMeta('lines'), n_elements(doyCandidateFilenames)]
  doyCandidatesOrig = intarr(size, /NOZERO)
  doyCandidates = intarr(size, /NOZERO)
  viCandidates = intarr(size, /NOZERO)
  maskCandidates = intarr(size, /NOZERO)
  for i=0,size[2]-1 do begin
    
    doyCandidatesI = hubIOImg_readImage(doyCandidateFilenames[i])
    doyCandidatesOrig[0,0,i] = doyCandidatesI
    if isLastYear[i] then begin
      whereLastYear = where(/NULL, doycandidatesI ge 353)
      doyCandidatesI[whereLastYear]  = -(ndaysLastYear-doyCandidatesI[whereLastYear]) ; create negative doys for last years observations
    endif
    if isNextYear[i] then begin
      whereNextYear = where(/NULL, doycandidatesI lt 353)
      doyCandidatesI[whereNextYear] += ndaysThisYear ; create doys with 365/366 offsets for next years observations
    endif
    doyCandidates[0,0,i] = doyCandidatesI

    
    viCandidates[0,0,i]   = hubIOImg_readImage(viCandidateFilenames[i])
    maskCandidates[0,0,i] = hubIOImg_readImage(maskCandidateFilenames[i])
  endfor
  
  ; code -1 (invalid pixel) to 3 (cloudy pixel) -> needed for TIMESATs '%Mask range and weight' setting 
  maskCandidates[where(/NULL, maskCandidates eq -1)] = 3
  
  ; mask data (doy for invalid pixels is set to -10000)
  dataIgnoreValue = -10000
  invalid = (maskCandidates ge 2) or (doyCandidates lt validDoyRange[0]) or (doyCandidates gt validDoyRange[1]) 
  doyCandidatesMasked = invalid*dataIgnoreValue + (~invalid)*doyCandidates

  ; find nearest neighbour for target year
  distance = abs(doyCandidatesMasked-targetDoy)
  !null = min(hub_fix3d(distance), nnIndices, DIMENSION=3)
  
  ; create composite
  doyComposite  = reform(doyCandidatesOrig[nnIndices],  size[0:1])
  viComposite   = reform(viCandidates[nnIndices],   size[0:1])
  maskComposite = reform(maskCandidates[nnIndices], size[0:1])
  
  ; write results
  outputFolder = filepath('', ROOT_DIR=dirnames.outputMODIS, SUBDIRECTORY=[p.tile,'COMPOSITE'])
  outputFilename = filepath('COMPOSITE.A'+stargetYear+stargetDoy+'.'+p.tile+'.005_250m_08_days_', ROOT_DIR=outputFolder)
  metaNames = ['map info','coordinate system string']
  metaValues = list(header.getMeta(metaNames[0]),header.getMeta(metaNames[1]))
  hubIOImg_writeImage, doyComposite,  outputFilename+postfixDOY,  MetaNames=metaNames, MetaValues=metaValues, /NoOpen
  hubIOImg_writeImage, viComposite,   outputFilename+postfixVI,   MetaNames=metaNames, MetaValues=metaValues, /NoOpen
  hubIOImg_writeImage, maskComposite, outputFilename+postfixMask, MetaNames=metaNames, MetaValues=metaValues, /NoOpen
  
end

pro timesat_03_createComposite, p, calculate

  if ~calculate then return

  print, '*** _03_createComposite ***'

  yearStart = fix(strmid(p.doyStart,0,4))
  yearEnd = fix(strmid(p.doyEnd,0,4))
  nYears = yearEnd-yearEnd+1
  
  targetYears = [yearStart:yearEnd]
  targetDoys   = [1:365:8]
 
  foreach targetYear,targetYears,i do begin
    foreach targetDoy,targetDoys do begin
      timesat_03_createComposite_singleDOY, p, targetYear, targetDoy  
      print, '  '+strtrim(targetYear,2)+' '+strtrim(targetDoy,2)+' done'
    endforeach
  endforeach
 

  print, 'done'
  print, ''
  
end