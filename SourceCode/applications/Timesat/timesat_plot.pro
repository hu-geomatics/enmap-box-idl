pro timesat_plot, p

  parameters = dictionary()
  parameters.pathPython27     = 'C:\Python27\ArcGISx6410.2'
  parameters.pathTimesat      = 'O:\timesat'
  parameters.pathInputMODIS   = 'O:\00_Datasets\MODIS 13Q1'
  parameters.pathOutputMODIS  = 'O:\01_ProcessingSubset\Processed_Data\MODIS\MODIS13Q1'
  parameters.tiles            = ['h26v06']
  parameters.sensors          = ['MOLA', 'MOLT']
  parameters.doyStart         = '2001001'
  parameters.doyEnd           = '2013365'
  parameters.subsetSampleRange = [3000,3009]
  parameters.subsetLineRange   = [1000,1009]
  parameters.useSubset         = 0b
  parameters.processingSampleRange = parameters.subsetSampleRange
  parameters.processingLineRange   = parameters.subsetLineRange
  parameters.seasonalParameters        = ['','SoS', 'EoS', 'LoS', 'BV', 'MoS', 'Mfit', 'Amp', 'LDer', 'RDer', 'LInt', 'SInt']
  parameters.seasonalParametersIndices = [4,6];,7,8,9]
  parameters.seasonStartDOY  = '001'
  parameters.seasonSavety = 10
  p = parameters

  pixelIndex = 6000
  p.tile = (p.tiles)[0]
  filenames = timesat_getFilenames(p)
  
  dataFiles = (hubIOASCIIHelper.readFile(filenames.dataFilelist))[1:*]
  maskFiles = (hubIOASCIIHelper.readFile(filenames.maskFilelist))[1:*]
  numberOfFiles = n_elements(dataFiles)
  profile = make_array(numberOfFiles)
  foreach dataFile,dataFiles,i do begin
    profile[i] = hubIOImg_readProfiles(dataFile, pixelIndex)
  endforeach
  profile[where(/NULL, profile eq 2)] = !VALUES.F_NAN


  stop
  

end
