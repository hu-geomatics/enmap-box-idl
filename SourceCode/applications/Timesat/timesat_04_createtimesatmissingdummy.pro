pro timesat_04_createTIMESATMissingDummy, p, calculate

  if ~calculate then return
  
  filenames = timesat_getFilenames(p)
  dirnames = timesat_getDirnames(p)
  
  ; create data/mask dummy file (use one image with all values equal to  2 for both data and mask dummy)
  firstMaskFilename = (file_search(dirnames.envi,'*pixel_reliability_16bit.dat'))[0]
  variables = hash('image', imageMath_variable(firstMaskFilename))
  result = imageMath_evaluate('image*0+2', variables, filenames.missingFile, /NOOPEN)
  
  print, '*** _04_createTIMESATMissingDummy ***'
  print, '  '+filenames.missingFile
  print, 'done'
  print, ''

end
