pro timesat_02_convertHDF, p, calculate

  ; convert HDF to ENVI (uses a Python Script)
  dirnames = timesat_getDirnames(p)
  file_mkdir, dirnames.envi
      
  hdfFilenames = file_search(dirnames.hdf,'*.hdf')
  p.enviFilenames = filepath(strmid(file_basename(hdfFilenames),0,27), ROOT_DIR=dirnames.envi)+'_250m_16_days_'
  
  if ~calculate then return
     
  layerNames = ['NDVI.dat', 'EVI.dat', 'composite_DOY.dat', 'pixel_reliability.dat']
  layerIndices = ['0', '1', '10', '11']
  scriptFilename = filepath('02_HDFToENVI.py', ROOT_DIR=enmapBox_getDirname(/Applications, /SourceCode), SUBDIRECTORY=['Timesat','_resources'])
  parameters = hash('hdfFilenames', hdfFilenames, 'enviFilenames', p.enviFilenames, 'layerNames', layerNames, 'layerIndices', layerIndices)
  hubPython_runCode, scriptFilename, parameters, NoReport=0, PathPython=p.pathPython27, File=scriptFilename, ShowScript=0
  
  ; delete all xml and ovr files
  file_delete, file_search(dirnames.envi,'*.xml')
  file_delete, file_search(dirnames.envi,'*.ovr')
 
  ; convert pixel reliability image to 16bit
  foreach reliabilityFilename, p.enviFilenames+'pixel_reliability.dat' do begin
    variables = hash('image', imageMath_variable(reliabilityFilename))
    reliability16bitFilename = filepath(file_basename(reliabilityFilename, '.dat')+'_16bit.dat', ROOT_DIR=file_dirname(reliabilityFilename))
    !null = imageMath_evaluate('long(image)', variables, reliability16bitFilename, /NOOPEN)
  endforeach

  ; delete old 8bit images
  ;file_delete, reliabilityFilename

end
