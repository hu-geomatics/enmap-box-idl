pro timesat_05a_createTIMESATFilelist, p

  filenames = timesat_getFilenames(p)

  doyFilenames = []
  pixelReliabilityFilenames = []
  ndviFilenames = []
  foreach sensor, p.sensors do begin
    p.sensor = sensor
    dirnames = timesat_getDirnames(p)
    sensorsDoyFilenames = file_search(dirnames.envi, '*composite_DOY.dat')
    doyFilenames = [doyFilenames, sensorsDoyFilenames]
    pixelReliabilityFilenames = [pixelReliabilityFilenames, strmid(sensorsDoyFilenames, 0, strlen(dirnames.envi)+41)+'pixel_reliability_16bit.dat']
    ndviFilenames =             [ndviFilenames,             strmid(sensorsDoyFilenames, 0, strlen(dirnames.envi)+41)+'NDVI.dat']
  endforeach
  doys = strmid(file_basename(doyFilenames),9,7)
  validIndices = where(/NULL, doys ge p.doyStart and doys le p.doyEnd)
  sortedIndices = validIndices[sort(doys[validIndices])]
  doys = doys[sortedIndices]
  pixelReliabilityFilenames = pixelReliabilityFilenames[sortedIndices]
  ndviFilenames = ndviFilenames[sortedIndices]
  
  yearStart = fix(strmid(p.doyStart,0,4))
  yearEnd = fix(strmid(p.doyEnd,0,4))

  allDoys = list()
  foreach year, [yearStart:yearEnd] do begin
    allDoys.add, strtrim(year,2)+string(format='(I3.3)',[1:365:8]), /EXTRACT
  endforeach
  
  ; fill up missing files
  ; - init all dates with missing
  allMissingText = orderedHash(allDoys, filenames.missingFile)

  ; - insert available dates
  allDataText = allMissingText+orderedHash(doys, ndviFilenames)
  allMaskText = allMissingText+orderedHash(doys, pixelReliabilityFilenames)

  ; - handle missing dates
  case p.fillMissingMode of
    'precursor' : begin
      foreach doy,allDoys,i do begin
        if i eq 0 then continue
        if allDataText[doy] eq filenames.missingFile then begin
          precursorDoy = allDoys[i-1]
          allDataText[doy] = allDataText[precursorDoy]
          allMaskText[doy] = allMaskText[precursorDoy]
        endif
      endforeach
    end 
    'missing'   : ; do nothing 
  endcase

  ; create timesat txt files
  
  hubIOASCIIHelper.writeFile, filenames.maskFilelistMerged, [strtrim(n_elements(allDoys),2), (allMaskText.values()).toArray()]
  hubIOASCIIHelper.writeFile, filenames.dataFilelistMerged, [strtrim(n_elements(allDoys),2), (allDataText.values()).toArray()]

  print, '*** _05_createTIMESATFilelist ***'
  print, 'Number of missing scenes: '+strtrim(n_elements(allDoys)-n_elements(doys),1)+' (filled up with "missing.dat" file)'
  print, '  '+filenames.dataFilelistMerged
  print, '  '+filenames.maskFilelistMerged
  print, 'done'
  print, ''
  
end
