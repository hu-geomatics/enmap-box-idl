;+
; :Hidden:
;-

;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `maxentwrapper_maxentWidgetModel`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `MaxEntWrapperObj::createModelStart`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
PRO MaxEntWrapper_createModel_application, applicationInfo

  ON_ERROR,2
  
  ; get global settings for this application
  settings = MaxEntWrapper_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo

  appDir = MaxEntWrapper_getDirname()

  maxent = FILEPATH('maxent.jar', ROOT_DIR=appDir, SUBDIRECTORY=['java'])
  IF ~ FILE_TEST(maxent, /REGULAR) THEN BEGIN 
    MESSAGE, 'maxent.jar not found.', /NONAME
    RETALL
  ENDIF

  obj = MaxEntWrapperObj()
  dataObj = MaxEntWrapperImgObj()
  ioObj = MaxEntWrapperIO()

  objStruct = { $ 
      MaxEntObj : obj $
    , dataObj : dataObj $
    , ioObj : ioObj $
  }

; _getParameters function
  arguments = maxentwrapper_maxentWidgetModel(objStruct, settings)

  IF ~ arguments.canceled THEN BEGIN
  
    PRINT, 'starting...'
; _imageProcessing procedure
    obj.createModelStart, arguments, dataObj, ioObj
    PRINT, '...finished'
  
  ENDIF

  MaxEntWrapper_cleanup, arguments, obj, dataObj, ioObj

END
