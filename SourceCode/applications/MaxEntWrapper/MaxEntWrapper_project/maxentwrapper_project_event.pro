;+
; :Hidden:
;-


;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `MaxEntWrapper_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro MaxEntWrapper_project_event, event
  
  ; set up a default error handler
  
  @huberrorcatch
  
  ; query information about the application (which is stored inside menu button)
  ;   - note that menu buttons are defined inside the menu file '.\MaxEntWrapper\_resource\enmap.men'
  ;   - the following information is returned as a hash:
  ;       applicationInfo['name'] = 'MaxEntWrapper Application'
  ;       applicationInfo['argument] = 'MaxEntWrapper_ARGUMENT'
  ;       applicationInfo['groupLeader'] = <EnMAP-Box top level base>
  
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  
  ; call the application main procedure
  ; if required, provide menu button information to it
 
  MaxEntWrapper_project_application, applicationInfo
  
end


;PRO MaxEntWrapper_project_event $
;  , event
;
;  CATCH, Error_status
;  IF Error_status ne 0 THEN BEGIN
;    CATCH, /CANCEL
;    HELP, /LAST_MESSAGE, OUTPUT=output
;    PRINT, output
;    errorMessage = [ $
;;        'Error index: ' + STRING(Error_status) $
;      'Error message: ', output $
;    ]
;    CASE Error_status OF
;      -364 : BEGIN
;        errorMessage = [errorMessage,"Perhaps JAVA can't be found!!","Make sure JAVA is in your PATH."]
;      END
;      ELSE : 
;    ENDCASE
;    !NULL = DIALOG_MESSAGE(errorMessage, /ERROR, TITLE='MaxEnt-Wrapper')
;
;    RETURN
;  ENDIF
;
;  MaxEntWrapper_project_application, event.top
;
;END

;PRO test_project
;
;  base = WIDGET_BASE()
;  event = {TOP : base}
;  MaxEntWrapper_project_event, event
;
;END
