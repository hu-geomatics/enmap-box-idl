;+
; :Hidden:
;-


;+
; :Author: Carsten Oldenburg
;-

PRO MaxEntWrapper_project_application, applicationInfo

  ON_ERROR,2

  ; get global settings for this application
  settings = MaxEntWrapper_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  appDir = MaxEntWrapper_getDirname()

  maxent = FILEPATH('maxent.jar', ROOT_DIR=appDir, SUBDIRECTORY=['java'])
  IF ~ FILE_TEST(maxent, /REGULAR) THEN BEGIN 
    MESSAGE, 'MaxEnt not found!!', /NONAME
    RETALL
  ENDIF

  obj = MaxEntWrapperObj()
  dataObj = MaxEntWrapperImgObj()
  ioObj = MaxEntWrapperIO()

  objStruct = { $
      MaxEntObj : obj $
    , dataObj : dataObj $
    , ioObj : ioObj $
  }

; _getParameters function
  arguments = maxentwrapper_maxentWidgetProject(objStruct, settings)

  IF ~ arguments.canceled THEN BEGIN
  
    PRINT, 'starting...'
; _imageProcessing procedure
    obj.projectStart, arguments, dataObj, ioObj
    PRINT, '...finished'
  
  ENDIF

  MaxEntWrapper_cleanup, arguments, obj, dataObj, ioObj

END
