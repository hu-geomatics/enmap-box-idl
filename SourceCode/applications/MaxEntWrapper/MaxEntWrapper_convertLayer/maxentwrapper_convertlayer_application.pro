;+
; :Hidden:
;-

;
;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `MaxEntWrapper_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `MaxEntWrapper_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `MaxEntWrapper_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro MaxEntWrapper_convertLayer_application, applicationInfo
  
  ; get global settings for this application
  settings = MaxEntWrapper_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
;  obj = MaxEntWrapperObj()
;  dataObj = MaxEntWrapperImgObj()
;  ioObj = MaxEntWrapperIO()
;
;  objStruct = { $
;      MaxEntObj : obj $
;    , dataObj : dataObj $
;    , ioObj : ioObj $
;  }
;
;; _getParameters function
;  arguments = maxentwrapper_createEnvLayerWidget(objStruct, settings['groupLeader'])
;
;  IF ~ arguments.canceled THEN BEGIN
;  
;    PRINT, 'starting...'
;; _imageProcessing procedure
;    dataObj.writeLayers,ioObj, arguments.outputdir, arguments.nodata
;    PRINT, '...finished'
;  
;  ENDIF
;
;  MaxEntWrapper_cleanup, arguments, obj, dataObj, ioObj

  
  parameters = MaxEntWrapper_convertLayer_getParameters(settings)
  
  if parameters['accept'] then begin
  
    MaxEntWrapper_convertLayer_processing, parameters, settings
    
  endif
 
end