;+
; :Hidden:
;-

;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`MaxEntWrapper_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
pro MaxEntWrapper_convertLayer_processing, parameters, settings

;  ON_ERROR,2

  extension = 'bil'
  
  IF parameters.HasKey('inputfilename') THEN inputfilename = (parameters['inputfilename'])['featureFilename']
  IF (parameters['inputfilename']).HasKey('labelFilename') THEN maskfilename = (parameters['inputfilename'])['labelFilename']
  IF parameters.HasKey('outputdir') THEN outputdir = parameters['outputdir']
  IF parameters.HasKey('nodata') THEN nodata = parameters['nodata'] ELSE nodata = -9999

  IF ~ FILE_TEST(outputdir, /DIRECTORY) THEN MESSAGE, 'Directory for output not valid!!', /NONAME

  IF ~ FILE_TEST(outputdir+PATH_SEP()+'layer', /DIRECTORY) THEN FILE_MKDIR, outputdir+PATH_SEP()+'layer'

  ioObj = MaxEntWrapperIO()
  inputimage = hubioimginputimage(inputfilename)
  IF ISA(maskfilename) THEN maskobj = hubIOImgInputImage(maskfilename, /Mask)

  nb = inputimage.getMeta('bands')
  numberoflines = inputimage.getMeta('lines')
  numberofsamples = inputimage.getMeta('samples')
  pos = LINDGEN(nb)
  interleave = inputimage.getMeta('interleave')
  datatype = inputimage.getMeta('data type')
  bandNames = MaxEntWrapper_correctString(inputimage.getMeta('band names'))
  IF ~ ISA(bandNames) THEN bandNames = STRING(INDGEN(nb)+1, FORMAT='(I0)')
  map_info = inputimage.getMeta('map info')
  
  string = ['Input file: '+inputfilename,'Output Directory: '+outputdir+PATH_SEP()+'layer']
  title = ['Create environmental layers']
  info = 'Please wait...'

  progressBar = hubProgressBar(Title=title, /Cancel)
  progressBar.setInfo, info

  CASE 1 OF
    datatype eq 1 : bytes = 1
    (datatype eq 2) OR (datatype eq 12) : bytes = 2
    (datatype eq 3) OR (datatype eq 13) OR (datatype eq 4) : bytes = 4
    (datatype eq 14) OR (datatype eq 15) OR (datatype eq 5) : bytes = 8
    ELSE : MESSAGE, 'Data type is not valid!!'
  ENDCASE
  ; tiled processing
  maxCount = FLOOR(5e7 / (numberofsamples * nb * bytes))

  bytetype = maxentwrapper_bytetype(datatype)

  header = STRARR(15)

  header[0] = 'BYTEORDER     I'
  header[1] = 'LAYOUT        BIL'
  header[2] = 'NROWS         ' + STRING(numberoflines, FORMAT='(I0)')
  header[3] = 'NCOLS         ' + STRING(numberofsamples, FORMAT='(I0)')
  header[4] = 'NBANDS        1'
  header[5] = 'NBITS         ' + STRING(bytetype.nbits, FORMAT='(I0)')
  header[6] = 'PIXELTYPE     ' + STRING(bytetype.pixeltype, FORMAT='(I0)')
  header[7] = 'BANDROWBYTES  ' + STRING(numberofsamples * bytetype.nbits, FORMAT='(I0)')
  header[8] = 'TOTALROWBYTES ' + STRING(numberofsamples * bytetype.nbits, FORMAT='(I0)')
  header[9] = 'BANDGAPBYTES  0'
  header[10] = 'NODATA     ' + STRING(nodata)
  header[11] = 'ULXMAP        ' + STRING(map_info.easting + 0.5 * map_info.sizex, FORMAT='(D0.3)')
  header[12] = 'ULYMAP        ' + STRING(map_info.northing - 0.5 * map_info.sizey, FORMAT='(D0.3)')
  header[13] = 'XDIM          ' + STRING(map_info.sizex, FORMAT='(D0.3)')
  header[14] = 'YDIM          ' + STRING(map_info.sizey, FORMAT='(D0.3)')
  
  IF maxCount lt numberoflines THEN BEGIN
    remains = numberoflines mod maxCount
    IF remains gt 0 THEN num = numberoflines / maxCount + 1 ELSE num = numberoflines / maxCount
    firstCount = UINDGEN(num) * maxCount
    lastCount = SHIFT(firstCount,-1)-1
    lastCount[-1] = numberoflines-1
  ENDIF ELSE BEGIN
    num = 1
    firstCount = 0
    lastCount = numberoflines-1
  ENDELSE

  IF OBJ_VALID(maskobj) THEN BEGIN
    maskobj.initReader, /BAND
    mask = maskobj.getData(0)
    maskIndex = WHERE(mask eq 0,count,/NULL)
    maskobj.finishReader 
    indices = maskIndex
  ENDIF
  
  reportStatus = 0UL
  progressBar.setRange, [0,nb*num]
  FOREACH nBand, pos, n DO BEGIN

    outFilename = outputdir + PATH_SEP() + 'layer' + PATH_SEP() + bandnames[nBand] +'.' + extension
    unit = ioObj.openFile(outFilename)
    
    headerFilename = outputdir + PATH_SEP() + 'layer' + PATH_SEP() + bandnames[nBand] +'.hdr'
    headerLun = ioObj.openFile(headerFilename)
    
    FOREACH line,header DO BEGIN
      ioObj.writeFile, headerLun, line
    ENDFOREACH
    
    ioObj.closeFile, headerLun
  
    inputimage.initReader, (lastCount[0]-firstCount[0]+1), /BAND, /TILEPROCESSING $
      , SubsetBandPositions=nBand
    i = 0
    WHILE ~ inputimage.tileProcessingDone() DO BEGIN ; Tile

      IF OBJ_VALID(maskobj) THEN BEGIN
        tiledMask = mask[*,firstCount[i]:lastCount[i]]
        tiledMaskIndex = WHERE(tiledMask eq 0,count,/NULL)
      ENDIF

      bandTile = inputimage.getData()
      
      IF OBJ_VALID(maskobj) THEN BEGIN
        bandTile[tiledMaskIndex] = nodata
      ENDIF

      ioObj.writeBinaryFile, unit, bandTile

      Info='Wait'+strcompress(nb-nband-1)+' bands left'
      progressBar.setInfo, info
      progressBar.setProgress, reportStatus++

      i++
    ENDWHILE
    inputimage.finishReader
    
    ioObj.closeFile, unit
    print, "Band: ", nBand + 1
    
  ENDFOREACH

  ; Write Map-Info to disk
  SAVE, map_info, /COMPRESS, DESCRIPTION='Map-Info' $
    , FILENAME=outputdir+PATH_SEP()+'layer'+PATH_SEP()+'mapinfo'
    
  OBJ_DESTROY, progressBar

end

;+
; :Hidden:
;-
pro test_MaxEntWrapper_processing

  inputfilename = hash( $
      'labelFilename', 'D:\Arbeit\IDLWorkspace80\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Mask' $
    , 'featureFilename', 'D:\Arbeit\IDLWorkspace80\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-B_Image' $
  )
  
  parameters = hash( $
      'outputdir', 'D:\Carsten\temp\out\' $
    , 'accept', 1 $
    , 'inputfilename', inputfilename $
    , 'nodata', -9999.0000 $
  )
  settings = MaxEntWrapper_getSettings()
  MaxEntWrapper_convertLayer_processing, parameters, settings

end  
