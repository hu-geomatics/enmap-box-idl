;+
; :Hidden:
;-

;
;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION MaxEntWrapper_convertLayer_getParameters, settings

  ON_ERROR,2
  
  groupLeader = settings.hubGetValue('groupLeader')

;  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - Convert environmental layers'

  HUBAMW_FRAME, Title='Input'
  HUBAMW_INPUTSAMPLESET, 'inputfilename', /MASKING, ReferenceTitle='Input Mask', /ReferenceOptional
  HUBAMW_PARAMETER, 'nodata', Title='No data value', Value=-9999, /Float
  
  HUBAMW_FRAME, Title='Output'
  HUBAMW_OUTPUTDIRECTORYNAME, 'outputdir', Title='Directory'

  parameters = HUBAMW_MANAGE() ; ConsistencyCheckFunction='_getParameters_consistencyCheck'

  RETURN, parameters

END