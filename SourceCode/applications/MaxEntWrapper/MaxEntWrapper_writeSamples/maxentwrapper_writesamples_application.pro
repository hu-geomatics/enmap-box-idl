;+
; :Hidden:
;-


;+
; :Author: Carsten Oldenburg
;-

PRO MaxEntWrapper_writeSamples_application, applicationInfo

  ON_ERROR,2

  ; get global settings for this application
  settings = MaxEntWrapper_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  appDir = MaxEntWrapper_getDirname()

  obj = MaxEntWrapperObj()
  dataObj = MaxEntWrapperImgObj()
  ioObj = MaxEntWrapperIO()

  objStruct = { $
      MaxEntObj : obj $
    , dataObj : dataObj $
    , ioObj : ioObj $
  }

; _getParameters function
  arguments = maxentwrapper_writeSamplesWidget(objStruct, settings)

  IF ~ arguments.canceled THEN BEGIN
  
    PRINT, 'starting...'
; _imageProcessing procedure
    obj.createSamples, arguments, dataObj, ioObj
    PRINT, '...finished'
  
  ENDIF

  MaxEntWrapper_cleanup, arguments, obj, dataObj, ioObj

END
