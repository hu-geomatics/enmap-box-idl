;+
; :Hidden:
;-

FUNCTION maxentwrapper_initArguments

  arguments = { $
      responsecurves : 0b $
    , pictures : 1b $
    , jackknife : 0b $
    , outputformat : 'Logistic' $
    , outputdirectory : '' $ ; -o
    , projectionlayers : '' $ ; -j
    , samplesfile : '' $ ; -s
    , environmentallayers : '' $ ; -e
;    , outputfiletype : 'bil' $ ; 'asc' $
;    , randomseed : 0 $
;    , logscale : 1 $
    , warnings : 1 $
;    , tooltips : 1 $
;    , askoverwrite : 1 $
;    , skipifexists : 0 $
;    , removeduplicates : 1 $
;    , writeclampgrid : 1 $
;    , writemess : 1 $
    , randomtestpoints : 25 $ ; 0
    , betamultiplier : 1. $
    , maximumbackground : 10000L $
    , biasfile : '' $
    , testsamplesfile : '' $
;    , replicates : 1 $
    , replicatetype : 'Crossvalidate' $ ; crossvalidate, bootstrap, subsample
    , perspeciesresults : 0 $
;    , writebackgroundpredictions : 0 $
;    , responsecurveexponent : 0 $
    , linear : 1 $
    , quadratic : 1 $
    , product : 1 $
    , threshold : 1 $
    , hinge : 1 $
    , addsamplestobackground : 1 $
    , addallsamplestobackground : 0 $
    , autorun : 1 $
    , writeplotdata : 0 $
    , fadebyclamping : 0 $
    , extrapolate : 1 $
;    , visible : 1 $
    , autofeature : 1 $
;    , doclamp : 1 $
    , outputgrids : 1 $
;    , plots : 1 $
;    , appendtoresultsfile : 0 $
    , maximumiterations : 500 $
    , convergencethreshold : 0.00001 $
;    , adjustsampleradius : 0 $
    , threads : !CPU.TPOOL_NTHREADS $ ; 1
    , lq2lqptthreshold : 80 $
    , l2lqthreshold : 10 $
    , hingethreshold : 15 $
    , beta_threshold : -1. $
    , beta_categorical : -1. $
    , beta_lqp : -1. $
    , beta_hinge : -1. $
;    , logfile : 'maxent.log' $
    , cache : 0 $
    , defaultprevalence : 0.5 $
;    , applythresholdrule : '' $
    , togglelayertype : PTR_NEW('') $ ; -t 'continuous' | 'categorical'
    , togglespeciesselected : PTR_NEW('') $
    , togglelayerselected : PTR_NEW('') $
;    , verbose : 0 $
    , allowpartialdata : 0 $
;    , prefixes : 1 $
    , nodata : -9999 $
; user
    , outputfile : '' $
    , speciesselected : PTR_NEW(/ALLOCATE_HEAP) $
    , layerselected : PTR_NEW(/ALLOCATE_HEAP) $
    , layertypes : PTR_NEW(/ALLOCATE_HEAP) $
    , speciesthreshold : 0. $
    , lambdasfilename : PTR_NEW('') $
    , outputGeoTiff : 0B $
    , createlayers : 0 $
    , sampleswithdata : 0 $
    , inputfilename : '' $
    , outputdir : '' $
    , workingEnvironment : '' $
; canceled by user
    , canceled : 1 $
  }
  
  RETURN, arguments
  
END
