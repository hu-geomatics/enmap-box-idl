;+
; :Hidden:
;-

FUNCTION MaxEntWrapperObj::init

  appDir = MaxEntWrapper_getDirname()

  maxent = FILEPATH('maxent.jar', ROOT_DIR=appDir, SUBDIRECTORY='java')
  self.pathMaxEnt = maxent
  
  self.writelayers = 1B
  
  RETURN, 1
END


PRO MaxEntWrapperObj::createModelStart, arguments, dataObj, ioObj

  ON_ERROR,2

  title = 'MaxEnt-Wrapper in progress...'
  info = 'Generating background data...'

  ; create progress bar with CANCEL button
  progressBar = hubProgressBar(Title=title, /Cancel)
  progressBar.setInfo, info
  
  self.createBackground, arguments, dataObj, ioObj

  info = 'Writing samples data...'
  progressBar.setInfo, info
  progressBar.setProgress, 0.5

  self.createSamples, arguments, dataObj, ioObj

  info = 'Collecting arguments...'
  progressBar.setInfo, info
  progressBar.setProgress, 1.
    
  command = self.collectArguments(arguments, /MODEL)

  progressBar.setInfo, ['finish processing']
  OBJ_DESTROY, progressBar

  PRINT, command
  self.executeWrapper, command

END

;----------------------
; Environmental Layers
;----------------------
PRO MaxEntWrapperObj::createBackground, arguments, dataObj, ioObj

  ON_ERROR,2

  dataObj.GetProperty, INPUTFILENAME=inputfilename

  filename = FILE_BASENAME(inputfilename)
  fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))

  CASE 1 OF
    ; ENVI-Data
    STRCMP(fileArr[-1],'hdr') : BEGIN
        dataObj.GetProperty $
          , DIMS=dims $
          , BANDNAMES=bandnames $
          , INPUTFILENAME=inputfilename $
          , POS=pos
          
        dataObj.randomBackgroundPoints, arguments.maximumbackground
        background = dataObj.readBackgroundData(arguments.maximumbackground)
        species = MAKE_ARRAY(arguments.maximumbackground, $
          TYPE=7, VALUE='background')
        
        arguments.inputfilename = inputfilename
        arguments.environmentallayers = arguments.outputdirectory + 'background.csv'
        
        ioObj.writeSamplesData $
          , arguments.environmentallayers $
          , species $
          , background.coordinates $
          , DATA=background.background $
          , VARS=bandnames[pos]
    ;      , VARS='Var' + STRING(pos, FORMAT='(I03)')
    END
    STRCMP(fileArr[-1],'csv') : BEGIN
      arguments.environmentallayers = inputfilename
    END
  ENDCASE

END

;---------------
; Samples layer
;---------------
PRO MaxEntWrapperObj::createSamples, arguments, dataObj, ioObj

  ON_ERROR,2

  filename = FILE_BASENAME(arguments.samplesfile)
  fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))
  

  IF ~ arguments.sampleswithdata THEN BEGIN
  
    dataObj.GetProperty $
      , SAMPLESDATASTRUCT=samplesDataStruct $
      , BANDNAMES=bandnames $
      , POS=pos
    
    ID = !NULL
    coordinates = !NULL
    data = !NULL
  
    arguments.samplesfile = arguments.outputdirectory + 'samples.csv'

    IF N_ELEMENTS(samplesDataStruct.samplesdata) le 1 $
      THEN speciesselected = samplesDataStruct.header[3:*] $
      ELSE speciesselected = UINDGEN(N_ELEMENTS(*arguments.speciesselected))

    FOREACH species,speciesselected DO BEGIN
      
      IF N_ELEMENTS(samplesDataStruct.samplesdata) le 1 THEN BEGIN
        selectedIndex = WHERE(samplesDataStruct.ids eq species, count)
        IF count gt 0 THEN selectedID = MAKE_ARRAY(N_ELEMENTS(selectedIndex), TYPE=7, VALUE=species) $
          ELSE selectedID = !NULL
      ENDIF ELSE BEGIN
        thresh = arguments.speciesthreshold
        IF thresh ge 100. THEN selectedIndex = WHERE(samplesDataStruct.samplesdata[species,*] ge thresh, count) $
          ELSE selectedIndex = WHERE((samplesDataStruct.samplesdata[species,*] - thresh) gt 4e-6, count)
        If count gt 0 THEN selectedID = MAKE_ARRAY(count, TYPE=7 $
          , VALUE=samplesDataStruct.header[species+3]) $
          ELSE selectedID = !NULL
      ENDELSE
      
      IF count gt 0 THEN BEGIN
        selectedCoords = samplesDataStruct.coordinates[*,selectedIndex]
        trainData = dataObj.readTrainData(selectedCoords)
      ENDIF ELSE BEGIN
        selectedCoords = !NULL
        trainData = !NULL
      ENDELSE
      
      ID = [ID,selectedID]
      coordinates = [[coordinates],[selectedCoords]]
      data = [[data],[trainData]]      
    ENDFOREACH
    
    ioObj.writeSamplesData $
      , arguments.samplesfile $
      , ID $
      , coordinates $
      , DATA=data $
      , VARS=bandnames[pos]
  ENDIF ; else nothing to do
END

FUNCTION MaxEntWrapperObj::checkArgumentsModel, arguments, dataObj

  ON_ERROR,2

  IF ~ FILE_TEST(dataObj.getInputfilename(), /REGULAR) THEN BEGIN
    MESSAGE, 'Input file is not valid!!', /NONAME
    RETURN, 0
  ENDIF
  
  IF STRCMP(arguments.samplesfile,'') OR ~ FILE_TEST(arguments.samplesfile, /REGULAR) THEN BEGIN
    MESSAGE, 'File with samples is not valid!!', /NONAME
    RETURN, 0
  ENDIF

  message = "Can't retrieve training data from background file. Please select an ENVI file for input!!"
  layersFilename = FILE_BASENAME(dataObj.getInputfilename())
  layersArr = STRLOWCASE(STRSPLIT(layersFilename,'.',/EXTRACT))
  IF STRCMP(layersArr[-1],'csv') AND ~ arguments.sampleswithdata $
    THEN MESSAGE, message, /NONAME
    
  IF ~ arguments.sampleswithdata THEN BEGIN
    IF ~ dataObj.checkCoordinates() $
      THEN MESSAGE, "Map info of species and environmental layers don't fit!!", /NONAME
  ENDIF
  
;  IF ~ hubMathHelper.getOr(STRMATCH(['Logistic','Cumulative','Raw'],arguments.outputformat)) THEN BEGIN
;    MESSAGE, 'Output format is not valid!!', /NONAME
;    RETURN, 0
;  ENDIF

  IF ~ FILE_TEST(arguments.outputdirectory, /DIRECTORY, /WRITE) THEN BEGIN ; 
    MESSAGE, 'Directory for writing output is not valid!!', /NONAME
    RETURN, 0
  ENDIF

  IF ~ PTR_VALID(arguments.speciesselected) THEN BEGIN
    MESSAGE, 'Problem with species!!', /NONAME
    RETURN, 0    
  ENDIF
  
  IF ~ hubMathHelper.getOr(*arguments.layerselected) THEN BEGIN
    MESSAGE, 'Minimum of one layer must be selected!!', /NONAME
    RETURN, 0    
  ENDIF

  IF ~ hubMathHelper.getOr(*arguments.speciesselected) THEN BEGIN
    MESSAGE, 'Minimum of one species must be selected!!', /NONAME
    RETURN, 0    
  ENDIF

  IF arguments.randomtestpoints lt 0 OR arguments.randomtestpoints gt 100 THEN BEGIN
    MESSAGE, 'error6'
    RETURN, 0    
  ENDIF

  IF arguments.betamultiplier lt 0 THEN BEGIN
    MESSAGE, 'error7'
    RETURN, 0    
  ENDIF

  IF arguments.maximumbackground lt 1L OR arguments.maximumbackground gt 2000000000L THEN BEGIN
    MESSAGE, 'error8'
    RETURN, 0    
  ENDIF

  IF ~ STRCMP(arguments.biasfile,'') AND ~ FILE_TEST(arguments.biasfile, /REGULAR) THEN BEGIN
    MESSAGE, 'Bias file is not valid!!', /NONAME
    RETURN, 0
  ENDIF

  IF ~ STRCMP(arguments.testsamplesfile,'') AND ~ FILE_TEST(arguments.testsamplesfile, /REGULAR) THEN BEGIN
    MESSAGE, 'File with test samples is not valid!!', /NONAME
    RETURN, 0
  ENDIF

  IF ~ hubMathHelper.getOr(STRMATCH(['Crossvalidate','Bootstrap','Subsample'],arguments.replicatetype)) THEN BEGIN
    MESSAGE, 'Type of replicating is not valid!!', /NONAME
    RETURN, 0
  ENDIF

  IF arguments.maximumiterations lt 1L OR arguments.maximumiterations gt 500000L THEN BEGIN
    MESSAGE, 'error12'
    RETURN, 0    
  ENDIF
  
  IF arguments.convergencethreshold lt 0D THEN BEGIN
    MESSAGE, 'error13'
    RETURN, 0    
  ENDIF

  IF arguments.lq2lqptthreshold lt 1 THEN BEGIN
    MESSAGE, 'error14'
    RETURN, 0    
  ENDIF
  
  IF arguments.l2lqthreshold lt 1 THEN BEGIN
    MESSAGE, 'error15'
    RETURN, 0    
  ENDIF
  
  IF arguments.hingethreshold lt 1 THEN BEGIN
    MESSAGE, 'error16'
    RETURN, 0    
  ENDIF

; TODO : check arguments
;  , beta_threshold : 1D $
;  , beta_categorical : 1D $
;  , beta_lqp : 1D $
;  , beta_hinge : 1D $

;; self
  IF ~ FILE_TEST(self.pathMaxEnt, /REGULAR) THEN BEGIN ; 
    MESSAGE, 'MaxEnt not found!!', /NONAME
    RETURN, 0
  ENDIF

  RETURN, 1
END




FUNCTION MaxEntWrapperObj::checkArgumentsProject, arguments, dataObj

  ON_ERROR,2

  IF STRCMP(arguments.outputfile,'') OR ~ FILE_TEST(FILE_DIRNAME(arguments.outputfile), /DIRECTORY) THEN BEGIN
    MESSAGE, 'File for writing output is not valid!!', /NONAME
    RETURN, 0
  ENDIF

  IF PTR_VALID(arguments.lambdasfilename) THEN BEGIN
  
    FOREACH lambda, *arguments.lambdasfilename DO BEGIN
      IF STRCMP(lambda,'') OR ~ FILE_TEST(lambda, /REGULAR) THEN BEGIN
        MESSAGE, 'Model file '+ lambda + ' is not valid!!', /NONAME
        RETURN, 0
      ENDIF
    ENDFOREACH
  ENDIF ELSE BEGIN
    MESSAGE, 'Model file is not valid!!', /NONAME
    RETURN, 0  
  ENDELSE
  
  IF ~ FILE_TEST(dataObj.getInputfilename(), /REGULAR) THEN BEGIN
    IF ~ FILE_TEST(dataObj.getLayersdir(), /DIRECTORY) THEN BEGIN
      MESSAGE, 'Directory with environmental layers is not valid!!', /NONAME
      RETURN, 0
    ENDIF ELSE self.writelayers = 0
  ENDIF
  
  IF ~ hubMathHelper.getOr(STRMATCH(['Logistic','Cumulative','Raw'],arguments.outputformat)) THEN BEGIN
    MESSAGE, 'Output format is not valid!!', /NONAME
    RETURN, 0
  ENDIF
  
  IF ~ FILE_TEST(self.pathMaxEnt, /REGULAR) THEN BEGIN
    MESSAGE, 'MaxEnt not found!!', /NONAME
    RETURN, 0
  ENDIF

  RETURN, 1
END


FUNCTION MaxEntWrapperObj::collectArguments $
  , arguments $
  , MODEL=model $
  , PROJECT=project $
  , LAMBDA=lambda
  
  ON_ERROR,2

  IF KEYWORD_SET(model) THEN BEGIN
    maxMemory = maxentwrapper_memtest()
;    if !VERSION.ARCH eq "x86_64" THEN BEGIN
;      IF maxMemory gt 2400L THEN maxMemory = 2400L
;    endif else begin
      IF maxMemory gt 1500L THEN maxMemory = 1500L
;    endelse
    IF maxMemory lt 512L THEN maxMemory = 512L

    command = "java -Xms512m -Xmx" + STRING(maxMemory,FORMAT='(6I0)') + "m -jar "
    
    IF (FILE_INFO(self.pathMaxEnt)).EXISTS $
      THEN command = command + '"' + self.pathMaxEnt + '"' $
      ELSE MESSAGE, 'Error'
    ; mandatory arguments
    ;---------------------
    ; outputdirectory
    command = command + ' -o "' + strmid(arguments.outputdirectory,0,strlen(arguments.outputdirectory)-1) + '"' ; Fix problem with last directory seperator
    ; environmentallayers
;    command = command + ' -e "' + arguments.environmentallayers + '"'
    command = command + ' -e "' + arguments.environmentallayers + '"'
    ; samplesfile
    command = command + ' -s "' + arguments.samplesfile + '"'
; FIXME : autorun
    command = command + " -a" ; autorun
    command = command + " redoifexists"
    
    ; Optional arguments
    ;--------------------
    IF ~ arguments.autofeature THEN BEGIN
      command = command + " -A"
      IF ~ arguments.linear THEN command = command + " -l"
      IF ~ arguments.quadratic THEN command = command + " -q"
      IF ~ arguments.product THEN command = command + " -p"
      IF ~ arguments.threshold THEN command = command + " threshold=false"
      IF ~ arguments.hinge THEN command = command + " -h"
    ENDIF
    
    IF arguments.responsecurves THEN command = command + " -P"
;    IF ~ arguments.pictures THEN command = command + " pictures=false"
    IF arguments.jackknife THEN command = command + " -J"
;    IF ~ STRCMP(arguments.outputformat, 'Logistic') $
;      THEN command = command + " outputformat=" + arguments.outputformat
    IF ~ arguments.warnings THEN command = command + " warnings=false"
    
    IF arguments.randomtestpoints ne 0 THEN command = command + " -X " $
      + STRING(arguments.randomtestpoints,FORMAT='(3I0)')
    IF arguments.betamultiplier ne 1d THEN command = command + " -b " $
      + STRING(arguments.betamultiplier,FORMAT='(6D0)')
    IF arguments.maximumbackground ne 10000 THEN command = command $
      + " maximumbackground=" + STRING(arguments.maximumbackground,FORMAT='(6I0)')
    
    IF ~ STRCMP(arguments.biasfile,'') THEN command = command + ' biasfile="' + arguments.biasfile + '"'
    IF ~ STRCMP(arguments.testsamplesfile,'') THEN command = command + ' -T "' + arguments.testsamplesfile + '"'

    IF ~ STRCMP(arguments.replicatetype, 'Crossvalidate') THEN command = command $
      + " replicatetype=" + arguments.replicatetype
      
    IF arguments.perspeciesresults THEN command = command + " perspeciesresults=true"

    IF ~ arguments.addsamplestobackground THEN command = command + " -d"
    IF arguments.addallsamplestobackground THEN command = command + " addallsamplestobackground=true"
    IF arguments.writeplotdata THEN command = command + " writeplotdata=true"
    IF arguments.fadebyclamping THEN command = command + " fadebyclamping=true"
    IF ~ arguments.extrapolate THEN command = command + " extrapolate=false"
    IF ~ arguments.outputgrids THEN command = command + " -x"
    
    IF arguments.maximumiterations ne 500L THEN command = command + " -m " $
      + STRING(arguments.maximumiterations, FORMAT='(16I0)')
    IF arguments.convergencethreshold ne 0.00001D THEN command = command $
      + " -c " + STRING(arguments.convergencethreshold, FORMAT='(12D0)')
      
    IF ~ arguments.cache THEN command = command + " cache=false"
    
    IF arguments.allowpartialdata THEN command = command + " allowpartialdata=true"
    
    IF arguments.lq2lqptthreshold ne 80L THEN command = command + " lq2lqptthreshold=" $
      + STRING(arguments.lq2lqptthreshold, FORMAT='(16I0)')   
    IF arguments.l2lqthreshold ne 10L THEN command = command + " l2lqthreshold=" $
      + STRING(arguments.l2lqthreshold, FORMAT='(16I0)')  
    IF arguments.hingethreshold ne 15L THEN command = command + " hingethreshold=" $
      + STRING(arguments.hingethreshold, FORMAT='(16I0)')
      
    IF arguments.beta_threshold ne -1D THEN command = command $
      + " beta_threshold=" + STRING(arguments.beta_threshold, FORMAT='(12D0)')
    IF arguments.beta_categorical ne -1D THEN command = command $
      + " beta_categorical=" + STRING(arguments.beta_categorical, FORMAT='(12D0)')
    IF arguments.beta_lqp ne -1D THEN command = command $
      + " beta_lqp=" + STRING(arguments.beta_lqp, FORMAT='(12D0)')
    IF arguments.beta_hinge ne -1D THEN command = command $
      + " beta_hinge=" + STRING(arguments.beta_hinge, FORMAT='(12D0)')
      
    ; Environmental layers
    IF ~ PRODUCT(STRCMP(*arguments.togglelayerselected, ''), /INTEGER) $
      THEN command = command + STRJOIN(' -N "' + *arguments.togglelayerselected + '"', /SINGLE)
      
    ; Species
    IF ~ PRODUCT(STRCMP(*arguments.togglespeciesselected, ''), /INTEGER) $
      THEN command = command + STRJOIN(' -E "' + *arguments.togglespeciesselected + '"', /SINGLE)

    IF ~ PRODUCT(STRCMP(*arguments.togglelayertype , ''), /INTEGER) $
      THEN command = command + STRJOIN(' -t "' + *arguments.togglelayertype + '"', /SINGLE)

;    command = command + " prefixes=false"
;    command = command + " outputfiletype=" + arguments.outputfiletype
    command = command + ' threads=' + STRING(arguments.threads,FORMAT='(I1)')

    RETURN, command
  ENDIF
  
  ;------------
  ; Projection
  ;------------
  IF KEYWORD_SET(project) THEN BEGIN
    maxMemory = maxentwrapper_memtest()
;    if !VERSION.ARCH eq "x86_64" THEN BEGIN
;      IF maxMemory gt 2400L THEN maxMemory = 2400L
;    endif else begin
      IF maxMemory gt 1500L THEN maxMemory = 1500L
;    endelse
    IF maxMemory lt 512L THEN maxMemory = 512L

    command = "java -Xms512m -Xmx" + STRING(maxMemory,FORMAT='(6I0)') + "m -cp"

    IF (FILE_INFO(self.pathMaxEnt)).EXISTS $
      THEN command = command + ' "' + self.pathMaxEnt + '"' + " density.Project" $
      ELSE MESSAGE, 'MaxEnt not found!!', /NONAME

    outputBasename = FILE_BASENAME(lambda,'.lambdas',/FOLD_CASE)

    command = command + ' "' + lambda + '"' ; *arguments.lambdasfilename
    command = command + ' "' + arguments.projectionlayers + '"'
    command = command + ' "' + arguments.outputfile + PATH_SEP() + outputBasename + '"' ; 
    command = command + ' threads=' + STRING(arguments.threads,FORMAT='(I1)')

    IF arguments.defaultprevalence ne 0.5 THEN command = command $
      + " defaultprevalence=" + STRING(arguments.defaultprevalence, FORMAT='(12D0)')
    IF ~ STRCMP(arguments.outputformat, 'Logistic') $
      THEN command = command + " outputformat=" + arguments.outputformat
;    command = command + " cache=false"
    
    RETURN, command
  ENDIF

  RETURN, 'error'  

END

PRO MaxEntWrapperObj::projectStart, arguments, dataObj, ioObj

  CATCH, ProjectError_status
  IF ProjectError_status NE 0 THEN BEGIN
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    
    IF STRCMP(!ERROR_STATE.MSG, 'Canceled by user') THEN BEGIN
      RETURN
    ENDIF ELSE BEGIN
      warn = [ $
          'Error index: ' + STRING(ProjectError_status) $
        , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
      ]
      dialog = DIALOG_MESSAGE(warn, /ERROR)
    ENDELSE
    
;    CATCH, /CANCEL
    RETURN
  ENDIF

  IF self.writelayers THEN BEGIN
    outputdirectory = arguments.outputfile
    dataObj.writeLayers, ioObj, outputdirectory, arguments.nodata
    arguments.projectionlayers = outputdirectory + PATH_SEP() + 'layer'
  ENDIF ELSE arguments.projectionlayers = dataObj.getLayersdir()

  progressBar = hubProgressBar(Title='Projecting', /Cancel)
  progressBar.hideBar

  dataObj.GetProperty $
    , MAP_INFO=map_info $
    , MASKOBJ=maskobj $
    , INDICES=indices
  
  FOREACH lambda, *arguments.lambdasfilename, i DO BEGIN
  
    outputBasename = FILE_BASENAME(lambda,'.lambdas',/FOLD_CASE)

    progressBar.setInfo, ['Projecting is in progress...' $
      ,'Species no.'+STRING(i+1, FORMAT='(I0)')+':'+outputBasename]

    command = self.collectArguments(arguments, /PROJECT, LAMBDA=lambda)

    PRINT, command
    self.executeWrapper, command

    data = ioObj.readBil(arguments.outputfile+PATH_SEP()+outputBasename+'.asc')
    dims = SIZE(data, /DIMENSIONS)

    IF OBJ_VALID(maskobj) THEN data[indices] = 0. ELSE data[WHERE(data eq arguments.nodata)] = 0. ; apply mask
    
    ; FIXME : Problem with hub-Routine and scalar strings
    IF ~ STRCMP(arguments.outputfile[-1],PATH_SEP()) THEN arguments.outputfile = arguments.outputfile + PATH_SEP()
    outputObj = hubioimgoutputimage((arguments.outputfile+outputBasename)[0]) ; PATH_SEP()+
    
    writerSettings = hash()
    outputObj.setMeta, 'map info', map_info
    writerSettings['samples'] = dims[0]
    writerSettings['lines'] = dims[1]
    writerSettings['bands'] = 1
    writerSettings['data type'] = 'float'
    writerSettings['dataFormat'] = 'band'
    outputObj.initWriter, writerSettings
    outputObj.writeData, data
    outputObj.finishWriter
;    outputObj.setMeta, 'map info', map_info

  ENDFOREACH
  
  OBJ_DESTROY, progressBar

END

PRO MaxEntWrapperObj::executeWrapper, command

  ON_ERROR,2

  IF STRCMP(command,'error') THEN MESSAGE, 'Command not valid!!', /NONAME

  CASE !VERSION.OS_FAMILY OF
    'unix' : SPAWN, command, result, /NOSHELL, EXIT_STATUS=status
    'Windows' : SPAWN, command, result, /NOSHELL, /HIDE, EXIT_STATUS=status; , NULL_STDIN ;, /STDERR
    ELSE : SPAWN, command, result, /NOSHELL, EXIT_STATUS=status
  ENDCASE
  
  IF status THEN BEGIN
    appDir = MaxEntWrapper_getDirname()
    logfile = FILEPATH('wrapper.log', ROOT_DIR=appDir, SUBDIRECTORY='lib')
    OPENW, unit, logfile, /GET_LUN
    PRINTF, unit, command
    CLOSE, unit
    FREE_LUN, unit
  ENDIF
  
END

PRO MaxEntWrapperObj::Cleanup
  ; DESTROY_OBJ, self.javaObj
END

PRO MaxEntWrapperObj__define
  struct = { MaxEntWrapperObj $
    , pathMaxEnt : '' $
    , writelayers : 0B $
  }
END
