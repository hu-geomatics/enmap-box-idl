;+
; :Hidden:
;-

FUNCTION maxentwrapperImgObj::Init

  self.pos = PTR_NEW(/ALLOCATE_HEAP)
  self.bandNames = PTR_NEW(/ALLOCATE_HEAP)
  self.map_info = PTR_NEW(/ALLOCATE_HEAP)
  self.indices = PTR_NEW(/ALLOCATE_HEAP)
  self.samplesdatastruct = PTR_NEW(/ALLOCATE_HEAP)
  
  RETURN, 1
END

FUNCTION maxentwrapperImgObj::bytetype

  ON_ERROR,2

  CASE self.datatype OF
    1 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 8
    END
    2 : BEGIN
      pixeltype = 'SIGNEDINT'
      nbits = 16
    END
    3 : BEGIN
      pixeltype = 'SIGNEDINT'
      nbits = 32
    END
    4 : BEGIN
      pixeltype = 'FLOAT'
      nbits = 32
    END
    5 : BEGIN
      pixeltype = 'FLOAT'
      nbits = 64
    END
    12 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 16
    END
    13 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 32
    END
    14 : BEGIN
      pixeltype = 'SIGNEDINT'
      nbits = 64
    END
    15 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 64
    END
  ENDCASE
  
  result = { $
      pixeltype : pixeltype $
    , nbits : nbits $
  }
  
  RETURN, result
  
END

FUNCTION maxentwrapperImgObj::openDirectory, parent, ioObj

  ON_ERROR,2

  title = 'Select directory with environmental layers'
  dir = DIALOG_PICKFILE(DIALOG_PARENT=parent, TITLE=title, /DIRECTORY)
  
  IF FILE_TEST(dir, /DIRECTORY) THEN BEGIN
    pattern = ['*.asc','*.bil','*.gri','*.mxe']
    IF PRODUCT(STRCMP(FILE_SEARCH(dir+pattern, /FOLD_CASE, /TEST_REGULAR),''), /INTEGER) $ ; +PATH_SEP()
      THEN MESSAGE, 'No environmental layers in directory found!!', /NONAME
    IF FILE_TEST(dir + 'mapinfo', /REGULAR) $ ; +PATH_SEP()
      THEN RESTORE, dir + 'mapinfo' ; +PATH_SEP()
    self.LAYERSDIR = STRMID(dir, 0, STRLEN(dir)-1) ; delete last (back-)slash
    *self.map_info = map_info
    RETURN, 1
  ENDIF ELSE RETURN, 0

END

PRO maxentwrapperImgObj::openENVIdata, imgObj, pos, dims

  ON_ERROR,2

  self.imgobj = imgobj
  self.nb = imgobj.getMeta('bands')
  self.numberoflines = imgobj.getMeta('lines')
  self.numberofsamples = imgobj.getMeta('samples')
;        self.dims = dims
;        *self.pos = pos imgobj.getMeta('')
  self.inputfilename = fname
  self.interleave = imgobj.getMeta('interleave')
  self.datatype = imgobj.getMeta('data type')
  *self.bandNames = MaxEntWrapper_correctString(imgobj.getMeta('band names'))
  self.map_info = imgobj.getMeta('map info')
    
END

FUNCTION maxentwrapperImgObj::openMask, parent

  ON_ERROR,2

  filter = '*.hdr;*.HDR'
  maskFilename = DIALOG_PICKFILE(DIALOG_PARENT=parent, FILTER=filter $
    , /MUST_EXIST, /READ, TITLE='Select mask file')

  IF FILE_TEST(maskFilename, /REGULAR, /READ) THEN BEGIN
    maskFilename = STRJOIN((STRLOWCASE(STRSPLIT(maskFilename,'.',/EXTRACT)))[0:-2])
    maskObj = hubioimginputimage(maskFilename)
    IF self.NUMBEROFSAMPLES eq maskObj.getMeta('samples') $
      AND self.NUMBEROFLINES eq maskObj.getMeta('lines') THEN BEGIN

      self.maskfilename = maskFilename
      self.maskObj = maskObj
      self.mpos = 0
      RETURN, 1
    ENDIF ELSE MESSAGE, "Image and mask doesn't fit!!", /NONAME
  ENDIF ELSE RETURN, 0
END

FUNCTION maxentwrapperImgObj::openData, parent, filter, title, IOOBJ=ioObj

  ON_ERROR,2

  fname = DIALOG_PICKFILE(DIALOG_PARENT=parent, FILTER=filter, /MUST_EXIST $
    , TITLE=title)

  filename = FILE_BASENAME(fname)
  dirname = FILE_DIRNAME(fname, /MARK_DIRECTORY)
  fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))

  CASE 1 OF
    ; ENVI data file
    STRCMP(fileArr[-1],'hdr') : BEGIN

      imgobj = hubioimginputimage(dirname+STRJOIN(fileArr[0:-2],'.'))

      IF OBJ_VALID(imgobj) THEN BEGIN 
    
        self.imgObj = imgobj
        self.nb = imgobj.getMeta('bands')
        self.numberoflines = imgobj.getMeta('lines')
        self.numberofsamples = imgobj.getMeta('samples')
;        self.dims = dims
        *self.pos = LINDGEN(self.nb)
        self.inputfilename = fname
        self.interleave = imgobj.getMeta('interleave')
        self.datatype = imgobj.getMeta('data type')
        *self.bandNames = MaxEntWrapper_correctString(imgobj.getMeta('band names'))
        *self.map_info = imgobj.getMeta('map info')

        RETURN, 1
      ENDIF ELSE RETURN, 0
    END
    ; ASCII-Data
    STRCMP(fileArr[-1],'csv') : BEGIN
      header = ioObj.readCsv(fName, /ONLY_HEADER)
      self.nb = N_ELEMENTS(header) - 3 ; subtract fields: species, x, y
      *self.pos = LINDGEN(self.nb)
      self.inputfilename = fname      
      *self.bandNames = (header)[3:*]

      RETURN, 1
    END
    ELSE : RETURN, 0
  ENDCASE
END



FUNCTION maxentwrapperImgObj::readTrainData, coordinates

  ON_ERROR,2

  xmin = *self.map_info.easting
  xmax = *self.map_info.easting + self.numberOfSamples * *self.map_info.sizex
  ymin = *self.map_info.northing - self.numberOfLines * *self.map_info.sizey
  ymax = *self.map_info.northing
  index = WHERE(coordinates[0,*] gt xmax OR coordinates[0,*] lt xmin $
    OR coordinates[1,*] gt ymax OR coordinates[1,*] lt ymin, /NULL)
  IF ISA(index) THEN MESSAGE, 'Some samples are located outside the boundaries of the image!!', /NONAME

  pixelCoords = [FLOOR((coordinates[0,*] - xmin) / *self.map_info.sizex), $
    FLOOR((ymax - coordinates[1,*]) / *self.map_info.sizey)]

  ySortedIndex = SORT(pixelCoords[1,*])
  yUniqEnd = UNIQ(pixelCoords[1,ySortedIndex])
  yUniqStart = SHIFT(yUniqEnd,1) + 1
  yUniqStart[0] = 0

  trainPlots = MAKE_ARRAY(self.nb,N_ELEMENTS(ySortedIndex),TYPE=self.datatype)
  self.imgobj.initReader, /SLICE
  FOREACH element,yUniqStart,i DO BEGIN
    slice = self.imgobj.getData(pixelCoords[1,ySortedIndex[element]])
    FOR n=element,yUniqEnd[i] DO BEGIN
      trainPlots[*,ySortedIndex[n]] = slice[*,pixelCoords[0,ySortedIndex[n]]]
    ENDFOR
  ENDFOREACH
  self.imgobj.finishReader  

  RETURN, trainPlots

END

PRO maxentwrapperImgObj::randomBackgroundPoints $
  , numberOfBackgroundPoints

  ON_ERROR,2

  IF OBJ_VALID(self.maskobj) THEN BEGIN
  
    self.maskobj.initReader, /BAND
    mask = self.maskobj.getData(self.mpos)
    unmaskIndex = WHERE(mask ne 0,count,/NULL)
    self.maskobj.finishReader

    IF count gt numberOfBackgroundPoints THEN BEGIN
      r = RANDOMU(SYSTIME(1),count)
      *self.indices = (unmaskIndex)[(SORT(r))[0:numberOfBackgroundPoints-1]]
    ENDIF ELSE *self.indices = unmaskIndex
  ENDIF ELSE BEGIN
    numberOfPixels = self.numberoflines * self.numberofsamples

    IF numberOfPixels gt numberOfBackgroundPoints THEN BEGIN
      r = RANDOMU(SYSTIME(1),numberOfPixels)
      *self.indices = (SORT(r))[0:numberOfBackgroundPoints-1]
    ENDIF ELSE *self.indices = UINDGEN(numberOfPixels)

  ENDELSE
END

FUNCTION maxentwrapperImgObj::checkCoordinates

  ON_ERROR,2

  speciesCoords = (*self.SAMPLESDATASTRUCT).coordinates

  map_info = *self.map_info

  xmin = map_info.easting
  xmax = map_info.easting + self.numberofsamples * map_info.sizex
  ymin = map_info.northing - self.numberoflines * map_info.sizey
  ymax = map_info.northing
  index = WHERE(speciesCoords[0,*] gt xmax OR speciesCoords[0,*] lt xmin $
    OR speciesCoords[1,*] gt ymax OR speciesCoords[1,*] lt ymin, /NULL)
  IF ~ ISA(index) THEN RETURN, 1 $
    ELSE RETURN, 0
    ;message = 'Some species samples are located outside the boundaries of the environme image!!!'

END  

FUNCTION maxentwrapperImgObj::readBackgroundData $
  , numberOfBackgroundPoints

  ON_ERROR,2

  pixelCoords = ARRAY_INDICES([self.numberofsamples,self.numberoflines],*self.indices,/DIMENSIONS)

  coordinates = [pixelCoords[0,*] * map_info.sizex + map_info.easting , $
    map_info.northing - pixelCoords[1,*] * map_info.sizey]

  background = MAKE_ARRAY(self.nb,numberOfBackgroundPoints,TYPE=self.datatype)

  IF ~ STRCMP(self.interleave,'bsq') THEN BEGIN 
    ySortedIndex = SORT(pixelCoords[1,*])
    yUniqEnd = UNIQ(pixelCoords[1,ySortedIndex])
    yUniqStart = SHIFT(yUniqEnd,1) + 1
    yUniqStart[0] = 0
    
    self.imgobj.initReader, /SLICE
    FOREACH element,yUniqStart,i DO BEGIN
      slice = self.imgobj.getData(pixelCoords[1,ySortedIndex[element]])
      FOR n=element,yUniqEnd[i] DO BEGIN
        background[*,n] = slice[*,pixelCoords[0,ySortedIndex[n]]]
      ENDFOR
    ENDFOREACH
    self.imgobj.finishReader
    
    coordinates = coordinates[*,ySortedIndex]
  ENDIF ELSE BEGIN
    self.imgobj.initReader, /BAND
    FOREACH element,*self.pos,i DO BEGIN
      band = self.imgobj.getData(element)
      background[i,*] = band[*self.indices]
    ENDFOREACH 
    self.imgobj.finishReader
  ENDELSE
  
  result = { $
      coordinates : coordinates $
    , background : background $
  }

  RETURN, result

END

PRO maxentwrapperImgObj::writeLayers, ioObj, outFname, nodata

  ON_ERROR,2

  extension = 'bil'

  IF ~ FILE_TEST(outFname, /DIRECTORY) THEN MESSAGE, 'Directory for output not valid!!', /NONAME

  IF ~ FILE_TEST(outFname+PATH_SEP()+'layer', /DIRECTORY) THEN FILE_MKDIR, outFname+PATH_SEP()+'layer'
  
  IF TOTAL(FILE_TEST(outFname+PATH_SEP()+'layer'+PATH_SEP() $
    +*self.bandnames+'.'+extension), /INTEGER) gt 0 THEN BEGIN

    msgText = [ $
        'Files for environmental layers exist!!' $
      , 'Overwrite layers??' $
    ]
    res = DIALOG_MESSAGE(msgText, /CENTER, /QUESTION, TITLE='Overwrite layers?' )
    CASE res OF
      'Yes' : 
      'No' : BEGIN
        self.maskobj = self.maskobj.cleanup
        RETURN ; MESSAGE, 'Canceled by user', /NONAME
      END
      ELSE : MESSAGE, 'ErrorLay1'
    ENDCASE    
  ENDIF
  
  string = ['Input file: '+self.inputfilename,'Output Directory: '+outFname+PATH_SEP()+'layer']
  title = ['Create environmental layers']
  info = 'Please wait...'

  progressBar = hubProgressBar(Title=title, /Cancel)
  progressBar.setInfo, info

  CASE 1 OF
    self.datatype eq 1 : bytes = 1
    (self.datatype eq 2) OR (self.datatype eq 12) : bytes = 2
    (self.datatype eq 3) OR (self.datatype eq 13) OR (self.datatype eq 4) : bytes = 4
    (self.datatype eq 14) OR (self.datatype eq 15) OR (self.datatype eq 5) : bytes = 8
    ELSE : MESSAGE, 'Data type is not valid!!'
  ENDCASE
  ; tiled processing
  maxCount = FLOOR(5e7 / (self.numberofsamples * self.nb * bytes))

  bytetype = self.bytetype()

  header = STRARR(15)

  header[0] = 'BYTEORDER     I'
  header[1] = 'LAYOUT        BIL'
  header[2] = 'NROWS ' + STRING(self.numberoflines)
  header[3] = 'NCOLS ' + STRING(self.numberofsamples)
  header[4] = 'NBANDS        1'
  header[5] = 'NBITS         ' + STRING(bytetype.nbits)
  header[6] = 'PIXELTYPE     ' + STRING(bytetype.pixeltype)
  header[7] = 'BANDROWBYTES  ' + STRING(self.numberofsamples * bytetype.nbits)
  header[8] = 'TOTALROWBYTES ' + STRING(self.numberofsamples * bytetype.nbits)
  header[9] = 'BANDGAPBYTES  0'
  header[10] = 'NODATA     ' + STRING(nodata)
  header[11] = 'ULXMAP        ' + STRING(*self.map_info.easting + 0.5 * *self.map_info.sizex, FORMAT='(D0.3)')
  header[12] = 'ULYMAP        ' + STRING(*self.map_info.northing - 0.5 * *self.map_info.sizey, FORMAT='(D0.3)')
  header[13] = 'XDIM          ' + STRING(*self.map_info.sizex, FORMAT='(D0.3)')
  header[14] = 'YDIM          ' + STRING(*self.map_info.sizey, FORMAT='(D0.3)')
  
  IF maxCount lt self.numberoflines THEN BEGIN
    remains = self.numberoflines mod maxCount
    IF remains gt 0 THEN num = self.numberoflines / maxCount + 1 ELSE num = self.numberoflines / maxCount
    firstCount = UINDGEN(num) * maxCount
    lastCount = SHIFT(firstCount,-1)-1
    lastCount[-1] = self.numberoflines-1
  ENDIF ELSE BEGIN
    num = 1
    firstCount = 0
    lastCount = self.numberoflines-1
  ENDELSE
  
  !NULL = WHERE(self.datatype eq [2,3,12,13,14,15], icount)
  !NULL = WHERE(self.datatype eq [4,5], fcount)

  IF OBJ_VALID(self.maskobj) THEN BEGIN
    self.maskobj.initReader, /BAND
    mask = self.maskobj.getData(self.mpos)
    maskIndex = WHERE(mask eq 0,count,/NULL)
    self.maskobj.finishReader 
    *self.indices = maskIndex
  ENDIF
  
  reportStatus = 0UL
  progressBar.setRange, [0,self.nb*num]
  FOREACH nBand,*self.pos, n DO BEGIN

    outFilename = outFname + PATH_SEP() + 'layer' + PATH_SEP() + (*self.bandnames)[nBand] +'.' + extension
    unit = ioObj.openFile(outFilename)
    
    headerFilename = outFname + PATH_SEP() + 'layer' + PATH_SEP() + (*self.bandnames)[nBand] +'.hdr'
    headerLun = ioObj.openFile(headerFilename)
    
    FOREACH line,header DO BEGIN
      ioObj.writeFile, headerLun, line
    ENDFOREACH
    
    ioObj.closeFile, headerLun
  
    self.imgobj.initReader, (lastCount[0]-firstCount[0]+1), /BAND, /TILEPROCESSING $
      , SubsetBandPositions=nBand
    i = 0
    WHILE ~ self.imgobj.tileProcessingDone() DO BEGIN ; Tile

      IF OBJ_VALID(self.maskobj) THEN BEGIN
        tiledMask = mask[*,firstCount[i]:lastCount[i]]
        tiledMaskIndex = WHERE(tiledMask eq 0,count,/NULL)
      ENDIF

      bandTile = self.imgobj.getData()
      
      IF OBJ_VALID(self.maskobj) THEN BEGIN
        bandTile[tiledMaskIndex] = -9999
      ENDIF

      ioObj.writeBinaryFile, unit, bandTile

      Info='Wait'+strcompress(self.nb-nband-1)+' bands left'
      progressBar.setInfo, info
      progressBar.setProgress, reportStatus++

      i++
    ENDWHILE
    self.imgobj.finishReader
    
    ioObj.closeFile, unit
    print, "Band: ", nBand + 1
    
  ENDFOREACH

  ; Write Map-Info to disk
  map_info = *self.MAP_INFO
  SAVE, map_info, /COMPRESS, DESCRIPTION='Map-Info' $
    , FILENAME=outFname+PATH_SEP()+'layer'+PATH_SEP()+'mapinfo'
    
  OBJ_DESTROY, progressBar

END

PRO maxentwrapperImgObj::GetProperty $
  , IMGOBJ=imgobj $
  , NB=nb $
  , POS=pos $
  , DIMS=dims $
  , MAP_INFO=map_info $
  , INPUTFILENAME=inputfilename $
  , MASKFILE=maskfile $
  , LAYERSDIR=layersdir $
  , BANDNAMES=bandnames $
  , MASKOBJ=maskobj $
  , SAMPLESDATASTRUCT=samplesdatastruct $
  , NUMBEROFLINES=numberOfLines $
  , NUMBEROFSAMPLES=numberOfSamples $
  , INDICES=indices
  
  imgobj = self.imgobj
  nb = self.nb
  IF PTR_VALID(self.pos) THEN pos = *self.pos
  dims = self.DIMS
  IF PTR_VALID(self.map_info) THEN map_info = *self.map_info
  inputfilename = self.inputfilename
  maskfile = self.maskfilename
  layersdir = self.layersdir
  IF PTR_VALID(self.bandnames) THEN bandnames = *self.bandnames
  maskobj = self.maskobj
  IF PTR_VALID(self.samplesdatastruct) THEN samplesdatastruct = *self.samplesdatastruct
  numberOfLines = self.numberOfLines
  numberOfSamples = self.numberOfSamples
  indices = *self.indices
   
END

PRO maxentwrapperImgObj::SetProperty $
  , INPUTFILENAME=inputfilename $
  , MASKFILE=maskfile $
  , NUMBEROFLINES=numberOfLines $
  , NUMBEROFSAMPLES=numberOfSamples $
  , NUMBEROFBANDS=numberofbands $
  , DIMS=dims $
  , POS=pos $
  , INTERLEAVE=interleave $
  , DATATYPE=datatype $
  , BANDNAMES=bandNames $
  , MAPINFO=mapinfo $
  , NUMBEROFBACKGROUNDPOINTS=numberOfBackgroundPoints $
  , SAMPLESDATASTRUCT=samplesdatastruct

  IF ISA(inputfilename) THEN self.inputfilename = inputfilename
  IF ISA(maskfile) THEN self.maskfilename = maskfile

  IF ISA(numberOfLines) THEN self.numberOfLines = numberOfLines
  IF ISA(numberOfSamples) THEN self.numberOfSamples = numberOfSamples
  IF ISA(numberofbands) THEN self.nb = numberofbands
  IF ISA(dims) THEN self.dims = dims
  IF ISA(pos) THEN *self.pos = pos
  IF ISA(interleave) THEN self.interleave = interleave
  IF ISA(datatype) THEN self.datatype = datatype

  IF ISA(bandNames) THEN *self.bandNames = MaxEntWrapper_correctString(bandNames)
  
  IF ISA(map_info) THEN *self.map_info = mapinfo

  IF ISA(samplesdatastruct) THEN *self.samplesdatastruct = samplesdatastruct
  
  IF ISA(numberOfBackgroundPoints) THEN self.NUMBEROFBACKGROUNDPOINTS = numberOfBackgroundPoints
  
END

PRO maxentwrapperImgObj::setInputfilename, string
  self.inputfilename = string
END

FUNCTION maxentwrapperImgObj::getInputfilename
  RETURN, self.inputfilename
END

FUNCTION maxentwrapperImgObj::getLayersdir
  RETURN, self.layersdir
END

FUNCTION maxentwrapperImgObj::getSamplesfilename
  RETURN, self.samplesfilename
END




PRO maxentwrapperImgObj::Cleanup

  IF OBJ_VALID(self.imgobj) THEN self.imgobj.cleanup
  IF OBJ_VALID(self.maskobj) THEN self.maskobj.cleanup
  IF PTR_VALID(self.POS) THEN PTR_FREE, self.POS
  IF PTR_VALID(self.MAP_INFO) THEN PTR_FREE, self.MAP_INFO
  IF PTR_VALID(self.BANDNAMES) THEN PTR_FREE, self.BANDNAMES
  IF PTR_VALID(self.INDICES) THEN PTR_FREE, self.INDICES
  IF PTR_VALID(self.SAMPLESDATASTRUCT) THEN PTR_FREE, self.SAMPLESDATASTRUCT

END

PRO maxentwrapperImgObj__define
  struct = { maxentwrapperImgObj $
    , IMGOBJ : OBJ_NEW() $ ; EnMap-Box ImgObj
    , MASKOBJ : OBJ_NEW() $
    , POS : PTR_NEW() $
    , MPOS : 0 $
    , INPUTFILENAME : '' $
    , MASKFILENAME : '' $
    , LAYERSDIR : '' $
    , SAMPLESFILENAME : '' $
    , MAP_INFO : PTR_NEW() $
    , BANDNAMES : PTR_NEW() $
    , NB : 0UL $
    , NUMBEROFLINES : 0UL $
    , NUMBEROFSAMPLES : 0UL $
    , DIMS : LONARR(5) $
    , INTERLEAVE : '' $
    , DATATYPE : 0 $
    , INDICES : PTR_NEW() $
    , SAMPLESDATASTRUCT : PTR_NEW() $
  }
END
