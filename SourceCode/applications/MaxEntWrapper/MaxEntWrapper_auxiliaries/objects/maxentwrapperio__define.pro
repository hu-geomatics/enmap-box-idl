;+
; :Hidden:
;-

FUNCTION MaxEntWrapperIO::Init
  RETURN, 1
END

;FUNCTION MaxEntWrapper_IDmatch, dataStruct, coordsStruct
;
;  ON_ERROR,2
;
;  IDCoordsSortedIndex = SORT(coordsStruct.IDs)
;  IDDataSortedIndex = SORT(dataStruct.IDs)
;  
;  IDCoordsSorted = coordsStruct.IDs[IDCoordsSortedIndex]
;  IDDataSorted = dataStruct.IDs[IDDataSortedIndex]
;  
;  header = [dataStruct.header[0],'x','y',dataStruct.header[1:*]]
;  
;  IF ARRAY_EQUAL(IDCoordsSorted,IDDataSorted) THEN BEGIN
;    result = { $
;        header : header $
;      , IDs : (dataStruct.IDs)[*,IDDataSortedIndex] $
;      , coordinates : (coordsStruct.coordinates)[*,IDCoordsSortedIndex] $
;      , samplesData : (dataStruct.samplesdata)[*,IDDataSortedIndex] $
;    }
;  
;  ENDIF ELSE BEGIN
;  
;    hist = HIST_2D(IDCoordsSorted,IDDataSorted)
;  
;    index = ARRAY_INDICES(hist, WHERE(hist eq 1))
;    
;    matchedCoordsIndex = IDCoordsSortedIndex[index[1,*]]
;    matchedDataIndex = IDDataSortedIndex[index[0,*]]
;  
;    result = { $
;        header : header $
;      , IDs : (dataStruct.IDs)[*,matchedDataIndex] $
;      , coordinates : (coordsStruct.coordinates)[*,matchedCoordsIndex] $
;      , samplesData : (dataStruct.samplesdata)[*,matchedDataIndex] $
;    }
;
;  ENDELSE
;  
;  RETURN, result
;
;END

FUNCTION MaxEntWrapperIO::openFile, outFname

  ON_ERROR,2

  OPENW, unit, outFname, /GET_LUN
  
  RETURN, unit
END

PRO MaxEntWrapperIO::writeFile, unit, data

  ON_ERROR,2

  PRINTF, unit, data
END

PRO MaxEntWrapperIO::writeBinaryFile, unit, data

  ON_ERROR,2

  WRITEU, unit, data
END

PRO MaxEntWrapperIO::closeFile, unit

  ON_ERROR,2

  CLOSE, unit
  FREE_LUN, unit
END

FUNCTION MaxEntWrapperIO::readBil, file, HEADER=header

  ON_ERROR,2

  IF ~ FILE_TEST(file, /REGULAR) THEN MESSAGE, 'Projected file not found!!'
  
  OPENR, unit, file, /GET_LUN

  header = STRARR(6)
  READF, unit, header
  ncols = LONG((STRSPLIT(header[0],' ',/EXTRACT))[1])
  nrows = LONG((STRSPLIT(header[1],' ',/EXTRACT))[1])
  nodata = LONG((STRSPLIT(header[5],' ',/EXTRACT))[1])
  data = FLTARR(ncols,nrows)
  
  READF, unit, data

  CLOSE, unit
  FREE_LUN, unit
  
  RETURN, data

END

;PRO MaxEntWrapperIO::convertAsciiGrid2Binary $
;  , inFile $
;  , outFile $
;  , MAP_INFO=map_info $
;  , MFID=mfid $
;  , INDICES=indices
;
;  ON_ERROR,2
;
;  IF ~ FILE_TEST(inFile, /REGULAR) THEN MESSAGE, 'Projected file not found!!'
;  
;  OPENR, inLun, inFile, /GET_LUN
;  OPENW, outLun, outFile, /GET_LUN
;
;  header = STRARR(6)
;  READF, inLun, header
;  ncols = LONG((STRSPLIT(header[0],' ',/EXTRACT))[1])
;  nrows = LONG((STRSPLIT(header[1],' ',/EXTRACT))[1])
;  nodata = LONG((STRSPLIT(header[5],' ',/EXTRACT))[1])
;  data = FLTARR(ncols)
;  
;  REPEAT BEGIN
;    line=''
;    READF, inLun, line
;    data = FLOAT(STRSPLIT(line,' ',/EXTRACT))
;    WRITEU, outLun, data
;  ENDREP UNTIL EOF(inLun)
;
;  IF mfid ne -1 THEN BEGIN
;    data = FLTARR(ncols,nrows)
;    assocData = ASSOC(outLun, data)
;    (assocData[0])[indices] = 0.
;  ENDIF
;
;  CLOSE, inLun, outLun
;  FREE_LUN, inLun, outLun
;
;  ENVI_SETUP_HEAD, DATA_IGNORE_VALUE=nodata, DATA_TYPE=4, DESCRIP='MaxEnt projection' $
;    , FILE_TYPE=0, FNAME=outFile, INTERLEAVE=0, MAP_INFO=map_info, NB=1, NL=nrows $
;    , NS=ncols, /WRITE
;
;; [, BNAMES=string array]
;
;END





FUNCTION MaxEntWrapperIO::readAsciiGrid, fName, HEADER=header

  ON_ERROR,2

  IF ~ (FILE_INFO(fName)).EXISTS THEN MESSAGE, 'File ' + fName + ' not existing!!'

  asciiStruct = READ_ASCII( $
      fName $
    , DELIMITER=' ' $
    , HEADER=header $
;    , MISSING_VALUE=-9999 $
    , DATA_START=6 $
    )
    
  RETURN, asciiStruct.(0)

END

FUNCTION MaxEntWrapperIO::readCoordinates, fName, HEADER=header

  ON_ERROR,2

  IF ~(FILE_INFO(fName)).EXISTS THEN MESSAGE, 'File ' + fName + ' not existing!!'

  asciiStruct = READ_ASCII( $
      fName $
    , DELIMITER=' ' $
    , HEADER=header $
;    , MISSING_VALUE=-9999 $
    , DATA_START=1 $
    )
    
  IF SIZE(asciiStruct.(0),/N_DIMENSIONS) ne 2 AND $
    (SIZE(asciiStruct.(0),/DIMENSIONS))[0] lt 3 THEN $
    MESSAGE, "File with coordinates not valid!!"
    
  IDs = REFORM(FIX((asciiStruct.(0))[0,*]))
  coordinates = (asciiStruct.(0))[1:2,*]
  
  result = { $
      IDs : IDs $
    , coordinates : DOUBLE(coordinates) $
  }
  
  RETURN, result
 
END

FUNCTION MaxEntWrapperIO::readShape, fName

  ON_ERROR,2

  shape = OBJ_NEW('IDLffShape', fName)

  IF shape.SHPTYPE ne 1 THEN MESSAGE, 'Shapefile must be of type point!!'

  attributes = shape.GetAttributes(/ALL)

  shape.GetProperty $
    , N_ENTITIES=numberOfEntities $
    , ATTRIBUTE_NAMES=attribute_names $
    , N_ATTRIBUTES=numberOfAttributes
  
  type = SIZE((attributes[0]).(0), /TYPE)
  
  IDs = MAKE_ARRAY(numberOfEntities, TYPE=type)
  coordinates = DBLARR(2,numberOfEntities)
  data = DBLARR(numberOfAttributes-3,numberOfEntities)

  FOR ent=0, numberOfEntities-1 DO BEGIN
    IDs[ent] = (attributes[ent]).(0)
    coordinates[*,ent] = [(attributes[ent]).(1),(attributes[ent]).(2)]
    FOR nattr=3, numberOfAttributes-1 DO BEGIN
      data[nattr-3,ent] = (attributes[ent]).(nattr)
    ENDFOR
  ENDFOR
      
  result = { $
      header : attribute_names $
    , IDs : IDs $
    , coordinates : coordinates $
    , samplesData : data $
  }

  RETURN, result

END

FUNCTION MaxEntWrapperIO::readCsv $
  , fName $
  , ONLY_HEADER=only_header

  ON_ERROR,2

  IF KEYWORD_SET(only_header) THEN num_records = 1

  csvStruct = READ_CSV( $
      fName $
    , HEADER=header $
    , NUM_RECORDS=num_records $
;    , MISSING_VALUE=value $
;    , RECORD_START=1 $
  )
  
  IF KEYWORD_SET(only_header) THEN RETURN, header
  
  xIndex = WHERE(STRMATCH(header, 'x',/FOLD_CASE) eq 1)
  yIndex = WHERE(STRMATCH(header, 'y',/FOLD_CASE) eq 1)
  IF xIndex ne 1 AND yIndex ne 2 THEN MESSAGE, 'No coordinates found in file '+ fName +'!!', /NONAME
  
  IDs = csvStruct.(0)
  coordinates = [TRANSPOSE(csvStruct.(1)),TRANSPOSE(csvStruct.(2))]
  
  CASE 1 OF
    N_ELEMENTS(header) eq 3 : BEGIN
      data = 0
      withdata = 0
    END
    N_ELEMENTS(header) gt 3 : BEGIN
      data = !NULL
      FOR i=3,N_TAGS(csvStruct)-1 DO BEGIN
        data = [data,csvStruct.(i)]
      ENDFOR
      withdata = 1
    END
    ELSE : MESSAGE, "File with species not valid!!"
  ENDCASE
  
  result = { $
      header : header $
    , IDs : IDs $
    , coordinates : coordinates $
    , samplesData : data $
    , withdata : withdata $
  }

  RETURN, result

END

FUNCTION MaxEntWrapperIO::readAscii, fName

  ON_ERROR,2

  asciiStruct = READ_ASCII( $
      fName $
    , DELIMITER=' ' $
    , HEADER=header $
;    , MISSING_VALUE=-9999 $
    , DATA_START=1 $
    )
    
  IF SIZE(asciiStruct.(0),/N_DIMENSIONS) ne 2 AND $
    (SIZE(asciiStruct.(0),/DIMENSIONS))[0] lt 2 THEN $
    MESSAGE, "File with species not valid!!", /NONAME

  header = STRSPLIT(header, /EXTRACT)
    
  xIndex = WHERE(STRMATCH(header, 'x',/FOLD_CASE) eq 1)
  yIndex = WHERE(STRMATCH(header, 'y',/FOLD_CASE) eq 1)
  IF xIndex ne 1 AND yIndex ne 2 THEN MESSAGE, 'No coordinates found in file '+ fName +'!!', /NONAME
    
  IDs = FIX((asciiStruct.(0))[0,*])
  coordinates = DOUBLE((asciiStruct.(0))[1:2,*])
  data = (asciiStruct.(0))[3:*,*]

  result = { $
      header : header $
    , IDs : IDs $
    , coordinates : coordinates $
    , samplesData : data $
  }

  RETURN, result
   
END

FUNCTION MaxEntWrapperIO::readENVI, fName

  ON_ERROR,2;

  dirname = FILE_DIRNAME(fName, /MARK_DIRECTORY)
  filename = FILE_BASENAME(fName)
  fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))
  
  filename = FILEPATH(STRJOIN(fileArr[0:-2],'.'), ROOT_DIR=dirname)

  samplesImage = hubioimginputimage(filename)
  speciesNames = samplesImage.getMeta('band names')
  nb = samplesImage.getMeta('bands')
  lines = samplesImage.getMeta('lines')
  samples = samplesImage.getMeta('samples')
  map_info = samplesImage.getMeta('map info')
  datataype = samplesImage.getMeta('data type')
  
  plotArr = PTRARR(nb)
  plotIDs = !NULL
  plotDataArr = PTRARR(nb)

  samplesImage.initReader, /Band
  FOR i=0, nb-1 DO BEGIN
     data = samplesImage.getData(i)
     tmp = WHERE(data gt 0, /NULL)
     IF ISA(tmp) THEN BEGIN
       plotArr[i] = PTR_NEW(tmp)
       plotDataArr[i] = PTR_NEW(data[tmp])
       plotIDs = [plotIDs,tmp]
     ENDIF
  ENDFOR
  sortedPlotIDs = plotIDs[SORT(plotIDs)]
  uniqPlotIDs = sortedPlotIDs[UNIQ(sortedPlotIDs)]
  IDs = ULINDGEN(N_ELEMENTS(uniqPlotIDs))
  pixelCoords = ARRAY_INDICES([samples,lines], uniqPlotIDs, /DIMENSIONS)

  coordinates = [pixelCoords[0,*] * map_info.sizex + map_info.easting , $
    map_info.northing - pixelCoords[1,*] * map_info.sizey]
  
  samplesData = MAKE_ARRAY(N_ELEMENTS(speciesNames), N_ELEMENTS(uniqPlotIDs), TYPE=datataype)
  FOR i=0, nb-1 DO BEGIN
    IF PTR_VALID(plotArr[i]) THEN BEGIN
      index = VALUE_LOCATE(uniqPlotIDs, *plotArr[i])
      samplesData[i,index] = *plotDataArr[i]
    ENDIF
  ENDFOR

  OBJ_DESTROY, samplesImage

  result = { $
      header : ['','','',speciesNames] $
    , IDs : IDs $
    , coordinates : coordinates $
    , samplesData : samplesData $
    , sampleswithdata : 0 $
  }

  RETURN, result

END

FUNCTION MaxEntWrapperIO::readSamplesData, fName

  ON_ERROR,2

  IF ~ (FILE_INFO(fName)).EXISTS THEN MESSAGE, 'File '+ fName +' not existing!!'

  filename = FILE_BASENAME(fName)
  fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))
  
  CASE 1 OF
    ; ASCII-Data
    STRCMP(fileArr[-1],'txt') : BEGIN
      dataStruct = self.readAscii(fName)
      
      index = WHERE(TOTAL(dataStruct.samplesdata,2) gt 0)
      IF ISA(index) THEN BEGIN
        result = { $
            header : [dataStruct.header[0:2],dataStruct.header[index+3]] $
          , IDs : dataStruct.IDs $
          , coordinates : dataStruct.coordinates $
          , samplesData : dataStruct.samplesdata[index,*] $
          , sampleswithdata : 0 $
        }      
        RETURN, result
      ENDIF ELSE MESSAGE, 'No species present!! All samples have 0 presence!!'
    END
    ; CSV-Data
    STRCMP(fileArr[-1],'csv') : BEGIN
      dataStruct = self.readCsv(fName)
      
      header = [(dataStruct.header)[0:2],STRING( $
        (dataStruct.ids)[UNIQ(dataStruct.ids, SORT(dataStruct.ids))]) ]
      if ( size( dataStruct.ids, /type ) ne 7 ) then $
        header = strcompress( header, /remove_all )
      
      result = { $
          header : header $
        , IDs : dataStruct.IDs $
        , coordinates : dataStruct.coordinates $
        , samplesData : dataStruct.samplesdata $
        , sampleswithdata : dataStruct.withdata $
      }

      RETURN, result
    END
    
    ; ENVI-Data
    STRCMP(fileArr[-1],'hdr') : BEGIN
      dataStruct = self.readENVI(fName)
      RETURN, dataStruct    
    END
    ; SHAPE-File
    STRCMP(fileArr[-1],'shp') : BEGIN
      dataStruct = self.readShape(fName)
      
      result = { $
          header : dataStruct.header $
        , IDs : dataStruct.IDs $
        , coordinates : dataStruct.coordinates $
        , samplesData : dataStruct.samplesdata $
        , sampleswithdata : 0 $
      }
      
      RETURN, result    
    END
    ELSE : RETURN, 0
  ENDCASE
END

PRO MaxEntWrapperIO::writeSamplesData $
  , outFname $
  , ID $
  , coordinates $
  , DATA=data $
  , VARS=vars

  ON_ERROR,2

  dims = SIZE(coordinates,/DIMENSIONS)
  
  OPENW, unit, outFname, /GET_LUN
  
  ; caption
  IF KEYWORD_SET(data) THEN BEGIN
    dimsData = SIZE(data,/DIMENSIONS)
;    vars = "Var" + STRING(FORMAT='(I03)',INDGEN(dimsData[0])+1)
    caption = "Species, X, Y, " + STRJOIN(vars,", ")

    datatype = SIZE(data, /TYPE)
    !NULL = WHERE(datatype eq [2,3,12,13,14,15], icount)
    !NULL = WHERE(datatype eq [4,5], fcount)
    CASE 1 OF
      datatype : data = TEMPORARY(STRTRIM(FIX(data), 2))
      icount : data = TEMPORARY(STRTRIM(data, 2))
;      fcount : data = TEMPORARY(STRTRIM(STRING(data, FORMAT='(D)'),2))
      fcount : data = TEMPORARY(STRTRIM(data,2))
;      fcount : data = TEMPORARY(REFORM(STRTRIM(STRING(data, FORMAT='(D0)'),2),dimsData))
      ELSE : MESSAGE, 'Datatype not valid!!'
    ENDCASE
    
  ENDIF ELSE caption = "Species, X, Y"; + STRJOIN(vars,", ")
  PRINTF, unit, caption

  coordinates = TEMPORARY(STRTRIM(coordinates,2))
  
  FOR i=0,dims[1]-1 DO BEGIN
    IF KEYWORD_SET(data) THEN BEGIN
      line = ID[i] + "," + STRJOIN(coordinates[*,i],",") + "," + STRJOIN(data[*,i],",")
    ENDIF ELSE line = ID[i] + "," + STRJOIN(coordinates[*,i],",")
    PRINTF, unit, line
  ENDFOR

  CLOSE, unit
  FREE_LUN, unit

END



PRO MaxEntWrapperIO__define
  struct = { MaxEntWrapperIO, $
    dummy : 0 $
  }
END
