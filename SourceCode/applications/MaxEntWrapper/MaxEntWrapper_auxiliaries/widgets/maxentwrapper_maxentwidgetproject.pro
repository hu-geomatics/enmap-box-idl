;+
; :Hidden:
;-

; 
; FIXME : GeoTiff  
;FUNCTION maxentwrapper_toggleTiffProject, event
;
;  ON_ERROR,2
;
;  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
;    
;  (*parameter).arguments.outputgeotiff = event.SELECT
;
;END

PRO maxentwrapper_setInputProject, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseInput')
  maskID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='childBaseMask')
  
  CASE event.INDEX OF
    0 : BEGIN ; File
      filter = [['*.hdr;*.HDR'],['ENVI file']]
      title = 'Select ENVI file or CSV-formatted file'
      IF (*parameter).dataObj.openData(event.TOP, filter, title, IOOBJ=(*parameter).ioObj) THEN BEGIN
    
        (*parameter).dataObj.GetProperty $
          , NB=nb $
          , POS=pos $
          , INPUTFILENAME=inputfilename $
          , BANDNAMES=bandnames
    
        WIDGET_CONTROL, textID, SET_VALUE=inputfilename
        WIDGET_CONTROL, maskID, SENSITIVE=1
        
      ENDIF ELSE BEGIN
        WIDGET_CONTROL, textID, SET_VALUE=''
        WIDGET_CONTROL, maskID, SENSITIVE=0
      ENDELSE    
    END
    1 : BEGIN ; Directory
      IF (*parameter).dataObj.openDirectory(event.TOP, (*parameter).ioObj) THEN BEGIN
      
        (*parameter).dataObj.GetProperty $
          , LAYERSDIR=layersdir
      
        WIDGET_CONTROL, textID, SET_VALUE=layersdir
        WIDGET_CONTROL, maskID, SENSITIVE=0
      ENDIF
    END
    ELSE : print, 'ErrorProj2'
  ENDCASE
END

PRO maxentwrapper_setMaskProject, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseMask')
  
  IF (*parameter).dataObj.openMask(event.TOP) THEN BEGIN
    
    (*parameter).dataObj.GetProperty $
      , MASKFILE=maskfile
    
    WIDGET_CONTROL, textID, SET_VALUE=maskfile
        
  ENDIF ELSE BEGIN
    WIDGET_CONTROL, textID, SET_VALUE=''
  ENDELSE    
END

PRO maxentwrapper_setLambdaProject, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
 
  filter = ['*.lambdas;*.LAMBDAS']
  lambda = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, FILTER=filter $
    , /MUST_EXIST, /READ, TITLE='Select model', /MULTIPLE_FILES)
    
  lambda = lambda[SORT(lambda)]

  IF PTR_VALID((*parameter).arguments.lambdasfilename) THEN BEGIN
    *(*parameter).arguments.lambdasfilename = lambda
  ENDIF ELSE (*parameter).arguments.lambdasfilename = PTR_NEW(lambda)

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseLambda')
  Widget_CONTROL, textID, SET_VALUE=lambda
  
END

PRO maxentwrapper_setOutputDirProject, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  outdir = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, /WRITE $
    , TITLE='Select output directory', /DIRECTORY)

  (*parameter).arguments.outputfile = outdir

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textOutDir')
  Widget_CONTROL, textID, SET_VALUE=outdir
  
END

PRO maxentwrapper_runWidgetProject, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  
  CATCH, Error_status
  IF Error_status NE 0 THEN BEGIN
    PRINT, 'Error index: ', Error_status
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    warn = [ $
        'Error index: ' + STRING(Error_status) $
      , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
    ]
    dialog = DIALOG_MESSAGE(warn, /ERROR)
    
    CATCH, /CANCEL
    RETURN
  ENDIF
  
  IF (*parameter).obj.checkArgumentsProject((*parameter).arguments, (*parameter).dataObj) THEN BEGIN 
    (*parameter).arguments.canceled = 0
    WIDGET_CONTROL, event.TOP, /DESTROY
  ENDIF
END

PRO maxentwrapper_exitWidget, event
  WIDGET_CONTROL, event.TOP, /DESTROY
END

PRO maxentwrapper_maxentWidgetProject_event, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  uname = WIDGET_INFO(event.ID, /UNAME)

  CASE uname OF
    'defaultprevalence' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value

      IF value gt 1. THEN BEGIN
        value = 1.
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
      IF value lt 0. THEN BEGIN
        value = 0.
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.defaultprevalence = value
    END
    ELSE :
  ENDCASE

END

FUNCTION maxentwrapper_maxentWidgetProject, objStruct, appinfo

  ON_ERROR,2

  arguments = maxentwrapper_initArguments()

  params = { $
      obj : objStruct.MaxEntObj $
    , dataObj : objStruct.dataObj $
    , ioObj : objStruct.ioObj $
    , arguments : arguments $
  }
  globalParameters = PTR_NEW(params)
  
  labelXSize = 120
  
  group_leader = appinfo.hubGetValue('groupLeader')

  base = WIDGET_BASE(title=appinfo.hubGetValue('title')+' - Project environmental layers',/COLUMN $
    , UVALUE=globalParameters, GROUP_LEADER=group_leader)

  base2 = WIDGET_BASE(base, /COLUMN, /ALIGN_TOP)
  
  labelBaseInputTop = WIDGET_LABEL(base2, VALUE='Input', /ALIGN_CENTER)
  childBaseInput = WIDGET_BASE(base2, /ROW, /ALIGN_LEFT)
  labelBaseInput = WIDGET_LABEL(childBaseInput, VALUE='Input file:', XSIZE=labelXSize)
  textBaseInput = WIDGET_TEXT(childBaseInput, /NO_NEWLINE, UNAME='textBaseInput', $
     XSIZE=53) 
  value = ['Select ENVI file','Select environmental layers']
  buttonBaseInput = WIDGET_DROPLIST(childBaseInput, EVENT_PRO='maxentwrapper_setInputProject', /FLAT $
    , VALUE=value)

  childBaseMask = WIDGET_BASE(base2, /ROW, /ALIGN_LEFT, SENSITIVE=0, UNAME='childBaseMask')
  labelBaseMask = WIDGET_LABEL(childBaseMask, VALUE='Mask file:', XSIZE=labelXSize)
  textBaseMask = WIDGET_TEXT(childBaseMask, UNAME='textBaseMask', /NO_NEWLINE $
    , XSIZE=53) ; , /SCROLL, YSIZE=7
  buttonBaseMask = WIDGET_BUTTON(childBaseMask, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setMaskProject') ; SCR_YSIZE=1, 


  labelBaseLambdaTop = WIDGET_LABEL(base2, VALUE='Model', /ALIGN_CENTER)
  childBaseLambda = WIDGET_BASE(base2, /ROW, /ALIGN_LEFT)
  labelBaseLambda = WIDGET_LABEL(childBaseLambda, VALUE='Lambda file:', XSIZE=labelXSize)
  textBaseLambda = WIDGET_TEXT(childBaseLambda, UNAME='textBaseLambda', /NO_NEWLINE $
    , XSIZE=50, /SCROLL, YSIZE=7)
  buttonBaseLambda = WIDGET_BUTTON(childBaseLambda, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setLambdaProject') ; , SCR_YSIZE=1

  defaultprevalenceBox = WIDGET_BASE(base2, /ROW, UNAME='defaultprevalenceBox')
  defaultprevalence = CW_FIELD(defaultprevalenceBox, /ALL_EVENTS, /FLOATING, TITLE='Default prevalence: ' $
  , UNAME='defaultprevalence', UVALUE=globalParameters, VALUE=params.arguments.defaultprevalence, XSIZE=10)

  outputFormats = [ $
      'Logistic' $
    , 'Cumulative' $
    , 'Raw' $
  ]
  childBaseOutputFormat = WIDGET_BASE(base2, /ROW, /ALIGN_LEFT)
  labelOutputFormat = WIDGET_LABEL(childBaseOutputFormat, VALUE='Output format:      ')
  outputFormat = WIDGET_COMBOBOX(childBaseOutputFormat, /FLAT, VALUE=outputFormats, $
    EVENT_PRO='maxentwrapper_setOutputFormat')

  labelBaseOutDirTop = WIDGET_LABEL(base2, VALUE='Output', /ALIGN_CENTER)
  childBaseOutDir = WIDGET_BASE(base2, /ROW, /ALIGN_LEFT)
  labelOutDir = WIDGET_LABEL(childBaseOutDir, VALUE='Output directory: ', XSIZE=labelXSize)
  textOutDir = WIDGET_TEXT(childBaseOutDir, /NO_NEWLINE, UNAME='textOutDir', $
    XSIZE=53) 
  buttonOutDir = WIDGET_BUTTON(childBaseOutDir, /FLAT, VALUE='Browse', $
    EVENT_PRO='maxentwrapper_setOutputDirProject')


  baseInteract = WIDGET_BASE(base, /ROW, /ALIGN_RIGHT)
  
  buttonStart = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Run', $
    EVENT_PRO='maxentwrapper_runWidgetProject', XSIZE=55)
    
  buttonExit = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Exit', $
    EVENT_PRO='maxentwrapper_exitWidget', XSIZE=55)

  WIDGET_CONTROL, base, /REALIZE

  CATCH, Error_widget
  IF Error_widget NE 0 THEN BEGIN
    PRINT, 'Error index: ', Error_widget
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    warn = [ $
        'Error index: ' + STRING(Error_widget) $
      , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
    ]
    dialog = DIALOG_MESSAGE(warn, /ERROR)
    
;    CATCH, /CANCEL
  ENDIF

  XMANAGER, 'maxentwrapper_maxentWidgetProject', base ;, /NO_BLOCK
  
  RETURN, (*globalParameters).arguments

END