;+
; :Hidden:
;-

FUNCTION maxentwrapper_setExperimental, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CASE event.VALUE OF
    0 : (*parameter).arguments.perspeciesresults = event.SELECT
    1 : (*parameter).arguments.fadebyclamping = event.SELECT
    2 : (*parameter).arguments.allowpartialdata = event.SELECT
    ELSE : MESSAGE, 'ErrorXper01'
  ENDCASE
END

FUNCTION maxentwrapper_setAdvanced, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CASE event.VALUE OF
    0 : (*parameter).arguments.addsamplestobackground = event.SELECT
    1 : (*parameter).arguments.addallsamplestobackground = event.SELECT
    2 : (*parameter).arguments.writeplotdata = event.SELECT
    3 : (*parameter).arguments.extrapolate = event.SELECT
    4 : (*parameter).arguments.outputgrids = event.SELECT
    5 : (*parameter).arguments.cache = event.SELECT
    ELSE : MESSAGE, 'ErrorAdv01'
  ENDCASE
END

FUNCTION maxentwrapper_setBasic, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CASE event.VALUE OF
    0 : (*parameter).arguments.warnings = event.SELECT
    ELSE : MESSAGE, 'ErrorBasic01'
  ENDCASE  
END

PRO maxentwrapper_settingsWidget_event, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  uname = WIDGET_INFO(event.ID, /UNAME)

  CASE uname OF
 ;-------
 ; Basic
 ;-------
    'randomTestpoints' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
  
      IF value gt 100 THEN BEGIN
        value = 100
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
      IF value lt 0 THEN BEGIN
        value = 0
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.randomTestpoints = value
    END
    'betaMultiplier' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value

      IF value lt 0. THEN BEGIN
        value = 0.
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.betaMultiplier = value
    END  
    'maximumBackground' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      IF value le 0L THEN BEGIN
        value = 1L
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.maximumBackground = value
    END
 ;----------
 ; Advanced
 ;----------
    'maximumiterations' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      IF value gt 32000 THEN BEGIN
        value = 32000
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
      IF value le 0 THEN BEGIN
        value = 1
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.maximumiterations = value
    END
    'convergencethreshold' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      IF value lt 0. THEN BEGIN
        value = (*parameter).arguments.convergencethreshold
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.convergencethreshold = value
    END

 ;-------------
 ; Xperimental
 ;-------------
    'lq2lqptthreshold' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value

      IF value lt 0 THEN BEGIN
        value = (*parameter).arguments.lq2lqptthreshold
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.lq2lqptthreshold = value
    END 
    'l2lqthreshold' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value

      IF value lt 0 THEN BEGIN
        value = (*parameter).arguments.l2lqthreshold
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
   
      (*parameter).arguments.l2lqthreshold = value
    END  
    'hingethreshold' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value

      IF value lt 0 THEN BEGIN
        value = (*parameter).arguments.hingethreshold
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
    
      (*parameter).arguments.hingethreshold = value
    END   
    'beta_threshold' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      (*parameter).arguments.beta_threshold = value
    END 
    'beta_categorical' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      (*parameter).arguments.beta_categorical = value
    END  
    'beta_lqp' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      (*parameter).arguments.beta_lqp = value
    END  
    'beta_hinge' : BEGIN
      WIDGET_CONTROL,event.ID, GET_VALUE=value
    
      (*parameter).arguments.beta_hinge = value
    END
    ELSE :     
  ENDCASE
 
END


PRO maxentwrapper_setReplicateType, event
  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  (*parameter).arguments.replicatetype = event.STR
END




PRO maxentwrapper_setBiasFile, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBiasFile')

  filter = ['*.mxe;*.MXE','*.asc;*.ASC','*.grd;*.GRD','*.bil;*.BIL']
  biasFname = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, FILTER=filter $
    , /MUST_EXIST, /READ, TITLE='Select bias file')

  IF FILE_TEST(biasFname, /REGULAR) THEN BEGIN
    (*parameter).arguments.biasfile = biasFname
    WIDGET_CONTROL, textID, SET_VALUE=biasFname
  ENDIF ELSE BEGIN
    (*parameter).arguments.biasfile = ''
    WIDGET_CONTROL, textID, SET_VALUE=''
  ENDELSE
END

PRO maxentwrapper_setTestsamples, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textTestsamplesFile')

  filter = ['*.txt;*.TXT','*.csv;*.CSV']
  testsamplesFname = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, FILTER=filter $
    , /MUST_EXIST, /READ, TITLE='Select file with test samples')

  IF FILE_TEST(testsamplesFname, /REGULAR) THEN BEGIN
    (*parameter).arguments.testsamplesfile = testsamplesFname
    WIDGET_CONTROL, textID, SET_VALUE=testsamplesFname
  ENDIF ELSE BEGIN
    (*parameter).arguments.testsamplesfile = ''
    WIDGET_CONTROL, textID, SET_VALUE=''
  ENDELSE
END

PRO maxentwrapper_settingsWidget, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  base = WIDGET_BASE(title='MaxEnt-Wrapper Settings', $
    GROUP_LEADER=event.TOP, UVALUE=parameter)  

  baseTab = WIDGET_TAB(base)

  baseSettings = WIDGET_BASE(baseTab, title='Basic',/COLUMN)  
  baseSettings2 = WIDGET_BASE(baseTab, title='Advanced', /COLUMN)
  baseSettings3 = WIDGET_BASE(baseTab, title='Experimental', /COLUMN)

  ; Basic-Tab
  ;-----------
  basicText = [ $
      'Give visual warnings' $
  ]
  
  basicValues = [ $
      (*parameter).arguments.warnings $
  ]
  
  bgroupBasic = CW_BGROUP(baseSettings, basicText, UNAME ='bgroupWarnings',/NONEXCLUSIVE, $
      SET_VALUE=basicValues, EVENT_FUNCT='maxentwrapper_setBasic') 

  randomTestpoints = CW_FIELD(baseSettings, /ALL_EVENTS, /INTEGER $
    , TITLE='Random test percentage:          ', UNAME='randomTestpoints' $
    , VALUE=(*parameter).arguments.randomtestpoints, XSIZE=8)
  betaMultiplier = CW_FIELD(baseSettings, /ALL_EVENTS, /FLOATING $
    , TITLE='Regularization multiplier:       ', UNAME='betaMultiplier' $
    , VALUE=(*parameter).arguments.betamultiplier, XSIZE=8)    
  maximumBackground = CW_FIELD(baseSettings, /ALL_EVENTS, /LONG $
    , TITLE='Max number of background points: ', UNAME='maximumBackground' $
    , VALUE=(*parameter).arguments.maximumbackground, XSIZE=8)    
    
  replicateTypes = [ $
      'Crossvalidate' $
    , 'Bootstrap' $
    , 'Subsample' $
  ]
  childBaseReplicateType = WIDGET_BASE(baseSettings, /ROW, /ALIGN_LEFT)
  labelOutputFormat = WIDGET_LABEL(childBaseReplicateType, VALUE='Replicated run type:', $
    XSIZE=180)
  replicateType = WIDGET_COMBOBOX(childBaseReplicateType, /FLAT, VALUE=replicateTypes, $
    EVENT_PRO='maxentwrapper_setReplicateType')
    
  childBaseTestsamplesFile = WIDGET_BASE(baseSettings, /ROW, /ALIGN_LEFT)
  labelTestsamplesFile = WIDGET_LABEL(childBaseTestsamplesFile, VALUE='Test sample file')
  textTestsamplesFile = WIDGET_TEXT(childBaseTestsamplesFile, /NO_NEWLINE, UNAME='textTestsamplesFile', $
     XSIZE=50) 
  buttonTestsamplesFile = WIDGET_BUTTON(childBaseTestsamplesFile, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setTestsamples')

  ; Advanced-Tab
  ;--------------
  advancedText = [ $
      'Add samples to background' $
    , 'Add all samples to background' $
    , 'Write plot data' $
    , 'Extrapolate' $
    , 'Write output grids' $
    , 'Cache ascii files' $
  ]
  advancedValues = [ $
      (*parameter).arguments.addsamplestobackground $
    , (*parameter).arguments.addallsamplestobackground $
    , (*parameter).arguments.writeplotdata $
    , (*parameter).arguments.extrapolate $
    , (*parameter).arguments.outputgrids $
    , (*parameter).arguments.cache $
  ]
  
  bgroupAdvanced = CW_BGROUP(baseSettings2, advancedText, UNAME ='bgroupAdvanced',/NONEXCLUSIVE, $
      SET_VALUE=advancedValues, EVENT_FUNCT='maxentwrapper_setAdvanced') 

  maximumiterations = CW_FIELD(baseSettings2, /ALL_EVENTS, /INTEGER $
    , TITLE='Maximum iterations:    ', UNAME='maximumiterations' $
    , VALUE=(*parameter).arguments.maximumiterations, XSIZE=10)
  convergencethreshold = CW_FIELD(baseSettings2, /ALL_EVENTS, /FLOATING $
    , TITLE='Convergence threshold: ', UNAME='convergencethreshold' $
    , VALUE=(*parameter).arguments.convergencethreshold, XSIZE=10)

  childBaseBiasFile = WIDGET_BASE(baseSettings2, /ROW, /ALIGN_LEFT)
  labelBiasFile = WIDGET_LABEL(childBaseBiasFile, VALUE='Bias file')
  textBiasFile = WIDGET_TEXT(childBaseBiasFile, /NO_NEWLINE, UNAME='textBiasFile', $
     XSIZE=50) 
  buttonBiasFile = WIDGET_BUTTON(childBaseBiasFile, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setBiasFile')

  ; Experimental-Tab
  ;--------------
  xperimentalText = [ $
      'Per species results' $
    , 'Fade by clamping' $
    , 'Use samples with some missing data' $
  ]
  
  xperimentalValues = [ $
      (*parameter).arguments.perspeciesresults $
    , (*parameter).arguments.fadebyclamping $
    , (*parameter).arguments.allowpartialdata $
  ]
  
  bgroupXperimental = CW_BGROUP(baseSettings3, xperimentalText, UNAME ='bgroupXperimental',/NONEXCLUSIVE, $
      SET_VALUE=xperimentalValues, EVENT_FUNCT='maxentwrapper_setExperimental') 

  lq2lqptthreshold = CW_FIELD(baseSettings3, /ALL_EVENTS, /INTEGER $
    , TITLE='Lq to lqp threshold:    ', UNAME='lq2lqptthreshold' $
    , VALUE=(*parameter).arguments.lq2lqptthreshold, XSIZE=8)
  l2lqthreshold = CW_FIELD(baseSettings3, /ALL_EVENTS, /INTEGER $
    , TITLE='Linear to lq threshold: ', UNAME='l2lqthreshold' $
    , VALUE=(*parameter).arguments.l2lqthreshold, XSIZE=8)  
  hingethreshold = CW_FIELD(baseSettings3, /ALL_EVENTS, /INTEGER $
    , TITLE='Hinge threshold:        ', UNAME='hingethreshold' $
    , VALUE=(*parameter).arguments.hingethreshold, XSIZE=8)
  beta_threshold = CW_FIELD(baseSettings3, /ALL_EVENTS, /FLOATING $
    , TITLE='Beta threshold:         ', UNAME='beta_threshold' $
    , VALUE=(*parameter).arguments.beta_threshold, XSIZE=8)
  beta_categorical = CW_FIELD(baseSettings3, /ALL_EVENTS, /FLOATING $
    , TITLE='Beta categorical:       ', UNAME='beta_categorical' $
    , VALUE=(*parameter).arguments.beta_categorical, XSIZE=8)
  beta_lqp = CW_FIELD(baseSettings3, /ALL_EVENTS, /FLOATING $
    , TITLE='Beta lqp:               ', UNAME='beta_lqp' $
    , VALUE=(*parameter).arguments.beta_lqp, XSIZE=8)
  beta_hinge = CW_FIELD(baseSettings3, /ALL_EVENTS, /FLOATING $
    , TITLE='Beta hinge:             ', UNAME='beta_hinge' $
    , VALUE=(*parameter).arguments.beta_hinge, XSIZE=8)

  WIDGET_CONTROL, base, /REALIZE
  XMANAGER, 'maxentwrapper_settingsWidget', base, /NO_BLOCK  
  
END