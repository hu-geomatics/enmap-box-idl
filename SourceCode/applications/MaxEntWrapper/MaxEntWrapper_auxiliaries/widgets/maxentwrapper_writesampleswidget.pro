PRO maxentwrapper_setSamplesFile, event

  ON_ERROR,2
  
  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  filter = ['*.txt;*.TXT']
  samplesFname = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, FILTER=filter $
    , /MUST_EXIST, /READ, TITLE='Select samples')

  samplesDataStruct = (*parameter).ioObj.readSamplesData(samplesFname)

  IF SIZE(samplesDataStruct,/TYPE) eq 8 THEN BEGIN

;    bgroupID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupSamples')
;    IF bgroupID ne 0 THEN WIDGET_CONTROL, bgroupID, /DESTROY
;
    names = samplesDataStruct.HEADER[3:*]
    value = MAKE_ARRAY(N_ELEMENTS(names),TYPE=1,VALUE=1)
;    bgroupSamples = CW_BGROUP(baseID2, names, UNAME ='bgroupSamples', $
;      SET_VALUE=value, /NONEXCLUSIVE, EVENT_FUNCT='maxentwrapper_toogleSpecies')
;      
;    WIDGET_CONTROL, baseID, MAP=1
;
    *(*parameter).arguments.speciesselected = value
    (*parameter).arguments.samplesfile = samplesFname
    (*parameter).arguments.sampleswithdata = samplesDataStruct.sampleswithdata
    (*parameter).dataObj.SetProperty, SAMPLESDATASTRUCT=samplesDataStruct
    
    textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseSamples')
    WIDGET_CONTROL, textID, SET_VALUE=samplesFname
      
;    filename = FILE_BASENAME(samplesFname)
;    fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))
;    
;    CASE fileArr[-1] OF
;      'csv' : WIDGET_CONTROL, baseID3, SENSITIVE=0
;      'txt' : WIDGET_CONTROL, baseID3, SENSITIVE=1
;      'hdr' : BEGIN
;        type = SIZE(samplesDataStruct.samplesdata, /TYPE)
;        IF type eq 1 THEN WIDGET_CONTROL, baseID3, SENSITIVE=0 $
;          ELSE WIDGET_CONTROL, baseID3, SENSITIVE=1
;      END
;; TODO : shape
;      'shp' : WIDGET_CONTROL, baseID3, SENSITIVE=1
;      ELSE : WIDGET_CONTROL, baseID3, SENSITIVE=0
;    ENDCASE
    
      

  ENDIF ELSE BEGIN
;    WIDGET_CONTROL, baseID, MAP=0
;    WIDGET_CONTROL, baseID3, SENSITIVE=0
    bgroupID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupSamples')
    IF bgroupID ne 0 THEN WIDGET_CONTROL, bgroupID, /DESTROY
    *(*parameter).arguments.speciesselected = 0
    (*parameter).arguments.samplesfile = ''
  ENDELSE
END



PRO maxentwrapper_setImageDir, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  
;  baseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='envLayerBox')
;  baseID2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='envLayerBox2')

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseEnvLayer')
  
  filter = [['*.hdr;*.HDR'],['ENVI file']]
  title = 'Select ENVI file'
  IF (*parameter).dataObj.openData(event.TOP, filter, title, IOOBJ=(*parameter).ioObj) THEN BEGIN
  
    (*parameter).dataObj.GetProperty $
      , NB=nb $
      , POS=pos $
      , INPUTFILENAME=inputfilename $
      , BANDNAMES=bandnames
  
    WIDGET_CONTROL, textID, SET_VALUE=inputfilename

    *(*parameter).arguments.layerselected = MAKE_ARRAY(nb, TYPE=0, VALUE=1)
    *(*parameter).arguments.layertypes = MAKE_ARRAY(nb, TYPE=0, VALUE=1)

;    rowBaseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='rowBase')
;    IF rowBaseID ne 0 THEN WIDGET_CONTROL, rowBaseID, /DESTROY
;    rowBase = WIDGET_BASE(baseID2, /ROW, UNAME='rowBase')
    
;    bgroupLayers = CW_BGROUP(rowBase, bandnames, UNAME ='bgroupLayers', $
;      SET_VALUE=(*(*parameter).arguments.layerselected)[pos], XSIZE=320, $
;      /NONEXCLUSIVE, EVENT_FUNCT='maxentwrapper_toogleLayers') ; YSIZE=250
;
;    types = MAKE_ARRAY(nb, TYPE=7, VALUE='continuous')
;
;    bgroupLayersType = CW_BGROUP(rowBase, types, UNAME ='bgroupLayersType', $
;      SET_VALUE=(*(*parameter).arguments.layerselected)[pos], XSIZE=130, $
;      /NONEXCLUSIVE, EVENT_FUNCT='maxentwrapper_toogleLayersType') ;, XSIZE=250, YSIZE=250
;
;  
;    WIDGET_CONTROL, baseID, MAP=1

  ENDIF ELSE BEGIN
    WIDGET_CONTROL, textID, SET_VALUE=''
    (*parameter).dataObj.SetProperty, MASKFILE=''
;    WIDGET_CONTROL, baseID, MAP=0
;    rowBaseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='rowBase')
;    IF rowBaseID ne 0 THEN WIDGET_CONTROL, rowBaseID, /DESTROY
    *(*parameter).arguments.layerselected = 0
    
  ENDELSE
  
END

; exists in maxentwrapper_maxentwidgetmodel.pro
;PRO maxentwrapper_setOutputDir, event
;
;  ON_ERROR,2
;
;  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
;
;  outdir = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, /DIRECTORY $
;    , /MUST_EXIST, /WRITE, TITLE='Select output directory')
;
;  (*parameter).arguments.outputdirectory = outdir
;
;  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textOutDir')
;  WIDGET_CONTROL, textID, SET_VALUE=outdir
;  
;END

FUNCTION testInput, parameter

  IF ~ FILE_TEST((*parameter).dataObj.getInputfilename(), /REGULAR) THEN BEGIN
    MESSAGE, 'Input file is not valid!!', /NONAME
    RETURN, 0
  ENDIF
  
  IF STRCMP((*parameter).arguments.samplesfile,'') $
    OR ~ FILE_TEST((*parameter).arguments.samplesfile, /REGULAR) THEN BEGIN
    
    MESSAGE, 'File with samples is not valid!!', /NONAME
    RETURN, 0
  ENDIF
  
  IF ~ FILE_TEST((*parameter).arguments.outputdirectory, /DIRECTORY, /WRITE) THEN BEGIN ; 
    MESSAGE, 'Directory for writing output is not valid!!', /NONAME
    RETURN, 0
  ENDIF
  
  RETURN, 1

END

PRO maxentwrapper_writeSamples, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CATCH, Error_status
  IF Error_status NE 0 THEN BEGIN
    PRINT, 'Error index: ', Error_status
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    warn = [ $
        'Error index: ' + STRING(Error_status) $
      , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
    ]
    dialog = DIALOG_MESSAGE(warn, /ERROR)
    
    CATCH, /CANCEL
    RETURN
  ENDIF

  IF testInput( parameter ) THEN BEGIN
    (*parameter).arguments.canceled = 0
    WIDGET_CONTROL, event.TOP, /DESTROY
  ENDIF
END

; exists in maxentwrapper_maxentwidgetmodel.pro
;PRO maxentwrapper_exitWidget, event
;  WIDGET_CONTROL, event.TOP, /DESTROY
;END



FUNCTION maxentwrapper_writeSamplesWidget, objStruct, appinfo

  ON_ERROR,2

  arguments = maxentwrapper_initArguments()
  
  params = { $
      obj : objStruct.MaxEntObj $
    , dataObj : objStruct.dataObj $
    , ioObj : objStruct.ioObj $
    , arguments : arguments $
  }
  globalParameters = PTR_NEW(params)
  
  group_leader = appinfo.hubGetValue('groupLeader')
  
  buttonXsize = 75

  base = WIDGET_BASE(title=appinfo.hubGetValue('title')+' - Write samples with data file',/COLUMN, GROUP_LEADER=group_leader $
    , UVALUE=globalParameters)
  
  baseSettings = WIDGET_BASE(base,/ROW)
  
  childBase = WIDGET_BASE(baseSettings, /COLUMN, /ALIGN_TOP)
  
  childBaseSamples = WIDGET_BASE(childBase, /ROW, /ALIGN_LEFT)
  labelBaseSamples = WIDGET_LABEL(childBaseSamples, VALUE='Samples file', XSIZE=120)
  textBaseSamples = WIDGET_TEXT(childBaseSamples, /NO_NEWLINE, UNAME='textBaseSamples', $
     XSIZE=50) 
  buttonBaseSamples = WIDGET_BUTTON(childBaseSamples, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setSamplesFile', XSIZE=buttonXsize)

  
  childBaseEnvLayer = WIDGET_BASE(childBase, /ROW, /ALIGN_LEFT)
  labelBaseEnvLayer = WIDGET_LABEL(childBaseEnvLayer, VALUE='Image file', XSIZE=120)
  textBaseEnvLayer = WIDGET_TEXT(childBaseEnvLayer, /NO_NEWLINE, UNAME='textBaseEnvLayer', $
     XSIZE=50) 
  buttonBaseEnvLayer = WIDGET_BUTTON(childBaseEnvLayer, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setImageDir', XSIZE=buttonXsize)

;  envLayerBox = WIDGET_BASE(childBase, XSIZE=475, /FRAME, $ ;YSIZE=50, 
;    /COLUMN, YSIZE=260, UNAME='envLayerBox')
;  WIDGET_CONTROL, envLayerBox, MAP=0
;
;  envLayerBox2 = WIDGET_BASE(envLayerBox, XSIZE=450, /FRAME, $ ;YSIZE=50, 
;    /SCROLL, Y_SCROLL_SIZE=220, UNAME='envLayerBox2')
;  
;  envLayerBoxButtons = WIDGET_BASE(envLayerBox, /ROW, /FRAME, UNAME='envLayerBoxButtons')
;;  buttonRange = WIDGET_BUTTON(samplesBoxButtons, /FLAT, VALUE='Select Range', $
;;    EVENT_FUNC='maxentwrapper_toogleSpecies', XSIZE=buttonXsize, UNAME='speciesRange')
;  layerAll = WIDGET_BUTTON(envLayerBoxButtons, /FLAT, VALUE='Select All', $
;    EVENT_FUNC='maxentwrapper_toogleLayers', XSIZE=buttonXsize, UNAME='layerAll')
;  layerNone = WIDGET_BUTTON(envLayerBoxButtons, /FLAT, VALUE='Clear', $
;    EVENT_FUNC='maxentwrapper_toogleLayers', XSIZE=buttonXsize, UNAME='layerClear')
  

  childBaseOutDir = WIDGET_BASE(childBase, /ROW, /ALIGN_LEFT)
  labelOutDir = WIDGET_LABEL(childBaseOutDir, VALUE='Output directory', XSIZE=120)
  textOutDir = WIDGET_TEXT(childBaseOutDir, /NO_NEWLINE, UNAME='textOutDir', $
    XSIZE=50) 
  buttonOutDir = WIDGET_BUTTON(childBaseOutDir, /FLAT, VALUE='Browse', $
    EVENT_PRO='maxentwrapper_setOutputDir', XSIZE=buttonXsize)
  

  baseInteract = WIDGET_BASE(base, /ROW, /ALIGN_RIGHT)
  
  buttonStart = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Run', $
    EVENT_PRO='maxentwrapper_writeSamples', XSIZE=buttonXSize)
    
  buttonExit = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Exit', $
    EVENT_PRO='maxentwrapper_exitWidget', XSIZE=buttonXSize)

  WIDGET_CONTROL, base, /REALIZE

  CATCH, Error_widget
  IF Error_widget NE 0 THEN BEGIN
    PRINT, 'Error index: ', Error_widget
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    warn = [ $
        'Error index: ' + STRING(Error_widget) $
      , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
    ]
    dialog = DIALOG_MESSAGE(warn, /ERROR)
    
  ENDIF
  
  XMANAGER, 'maxentwrapper_maxentWidgetModel', base ;, /NO_BLOCK

  CATCH, /CANCEL

  RETURN, (*globalParameters).arguments
END