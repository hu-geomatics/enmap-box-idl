;+
; :Hidden:
;-

PRO maxentwrapper_maxentWidgetModel_event, event

  ON_ERROR,2
  
  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  uname = WIDGET_INFO(event.ID, /UNAME)
  
  CASE uname OF
    'speciesThreshold' : BEGIN
      WIDGET_CONTROL, event.ID, GET_VALUE=value
  
      (*parameter).dataObj.GetProperty, SAMPLESDATASTRUCT=samplesdatastruct
      data = TRANSPOSE(samplesdatastruct.samplesdata)
      dims = SIZE(data, /DIMENSIONS)
      
      ; toogle species in widget
      IF value ge 100. THEN selectedIndex = WHERE(data ge value, count) $
        ELSE selectedIndex = WHERE((data - value) gt 4e-6, count)
     
      (*(*parameter).arguments.speciesselected)[*] = 0b
      IF count gt 0 THEN BEGIN
        indarr = ARRAY_INDICES(dims ,selectedIndex, /DIMENSIONS)
        uniqInd = REFORM(indarr[1,UNIQ(indarr[1,*])])      
        (*(*parameter).arguments.speciesselected)[uniqInd] = 1b
        ; toggle species argument for maxent
        index = WHERE(~ (*(*parameter).arguments.speciesselected)[uniqInd],ncount)
        IF ncount gt 0 THEN *(*parameter).arguments.togglespeciesselected $
          = samplesdatastruct.HEADER[uniqInd[index]+3] ; 0-2 : ID, x, y
      ENDIF
      speciesWid = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupSamples')
      WIDGET_CONTROL, speciesWid, SET_VALUE=(*(*parameter).arguments.speciesselected)
    
      ; check treshold
      IF value gt 100d THEN BEGIN
        value = 100d
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
      IF value lt 0d THEN BEGIN
        value = 0d
        WIDGET_CONTROL, event.ID, SET_VALUE=value
      ENDIF
      (*parameter).arguments.speciesthreshold = value
    END
    ELSE :
  ENDCASE
  
  

END

PRO maxentwrapper_setSamples, event

  ON_ERROR,2
  
  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  baseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='samplesBox')
  baseID2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='samplesBoxSpecies')
  baseID3 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='speciesThresholdParamBox')

  filter = ['*.txt;*.TXT','*.csv;*.CSV','*.shp;*.SHP','*.hdr','*.HDR']
  samplesFname = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, FILTER=filter $
    , /MUST_EXIST, /READ, TITLE='Select samples')

  samplesDataStruct = (*parameter).ioObj.readSamplesData(samplesFname)

  IF SIZE(samplesDataStruct,/TYPE) eq 8 THEN BEGIN

    bgroupID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupSamples')
    IF bgroupID ne 0 THEN WIDGET_CONTROL, bgroupID, /DESTROY

    names = samplesDataStruct.HEADER[3:*]
    value = MAKE_ARRAY(N_ELEMENTS(names),TYPE=1,VALUE=1)
    bgroupSamples = CW_BGROUP(baseID2, names, UNAME ='bgroupSamples', $
      SET_VALUE=value, /NONEXCLUSIVE, EVENT_FUNCT='maxentwrapper_toogleSpecies')
      
    WIDGET_CONTROL, baseID, MAP=1

    *(*parameter).arguments.speciesselected = value
    (*parameter).arguments.samplesfile = samplesFname
    (*parameter).arguments.sampleswithdata = samplesDataStruct.sampleswithdata
    (*parameter).dataObj.SetProperty, SAMPLESDATASTRUCT=samplesDataStruct
    
    textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseSamples')
    WIDGET_CONTROL, textID, SET_VALUE=samplesFname
      
    filename = FILE_BASENAME(samplesFname)
    fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))
    
    CASE fileArr[-1] OF
      'csv' : WIDGET_CONTROL, baseID3, SENSITIVE=0
      'txt' : WIDGET_CONTROL, baseID3, SENSITIVE=1
      'hdr' : BEGIN
        type = SIZE(samplesDataStruct.samplesdata, /TYPE)
        IF type eq 1 THEN WIDGET_CONTROL, baseID3, SENSITIVE=0 $
          ELSE WIDGET_CONTROL, baseID3, SENSITIVE=1
      END
; TODO : shape
      'shp' : WIDGET_CONTROL, baseID3, SENSITIVE=1
      ELSE : WIDGET_CONTROL, baseID3, SENSITIVE=0
    ENDCASE
    
      

  ENDIF ELSE BEGIN
    WIDGET_CONTROL, baseID, MAP=0
    WIDGET_CONTROL, baseID3, SENSITIVE=0
    bgroupID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupSamples')
    IF bgroupID ne 0 THEN WIDGET_CONTROL, bgroupID, /DESTROY
    *(*parameter).arguments.speciesselected = 0
    (*parameter).arguments.samplesfile = ''
  ENDELSE
END

FUNCTION maxentwrapper_toogleSpecies, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  eventUname = WIDGET_INFO(event.ID, /UNAME)
  bgroupSamples = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupSamples')
  
  (*parameter).dataObj.GetProperty, SAMPLESDATASTRUCT=samplesdatastruct

  speciesThreshold = WIDGET_INFO(event.TOP, FIND_BY_UNAME='speciesThreshold')
  WIDGET_CONTROL, speciesThreshold, GET_VALUE=thresh

  IF thresh gt 0. THEN BEGIN
    data = TRANSPOSE(samplesdatastruct.samplesdata)
    dims = SIZE(data, /DIMENSIONS)
    IF SIZE(data, /N_DIMENSIONS) eq 1 THEN MESSAGE, 'Data not valid, not enough samples!!'

    ; toogle species in widget
    IF thresh ge 100. THEN selectedIndex = WHERE(data ge thresh, threshcount) $
      ELSE selectedIndex = WHERE((data - thresh) gt 4e-6, threshcount)

    IF threshcount gt 0 THEN BEGIN
      indarr = ARRAY_INDICES(dims ,selectedIndex, /DIMENSIONS)
      uniqInd = REFORM(indarr[1,UNIQ(indarr[1,*])])
    ENDIF
  ENDIF ELSE BEGIN
    dims = SIZE(samplesdatastruct.samplesdata, /DIMENSIONS)
    uniqInd = LINDGEN(dims[0])
  ENDELSE
  
  CASE eventUname OF
    'bgroupSamples' : BEGIN

      IF thresh gt 0. THEN BEGIN

        IF event.SELECT THEN BEGIN
       
          IF thresh ge 100. THEN !NULL = WHERE(data[*,event.VALUE] ge thresh, count) $
            ELSE !NULL = WHERE((data[*,event.VALUE] - thresh) gt 4e-6, count)
     
          IF count gt 0 THEN BEGIN
            (*(*parameter).arguments.speciesselected)[event.VALUE] = event.SELECT

            ; toogle species in widget
            IF threshcount gt 0 THEN BEGIN
              index = WHERE(~ (*(*parameter).arguments.speciesselected)[uniqInd], togglecount)
              IF togglecount gt 0 THEN *(*parameter).arguments.togglespeciesselected = samplesdatastruct.HEADER[uniqInd[index]+3] $ ; 0-2 : ID, x, y
                ELSE *(*parameter).arguments.togglespeciesselected = ''
            ENDIF ELSE BEGIN
              (*(*parameter).arguments.speciesselected)[event.VALUE] = 0b
              WIDGET_CONTROL, event.ID, SET_VALUE=*(*parameter).arguments.speciesselected
            ENDELSE
          ENDIF ELSE BEGIN
            (*(*parameter).arguments.speciesselected)[event.VALUE] = 0b
            WIDGET_CONTROL, event.ID, SET_VALUE=*(*parameter).arguments.speciesselected
          ENDELSE
        
        ENDIF ELSE BEGIN

          (*(*parameter).arguments.speciesselected)[event.VALUE] = event.SELECT

          ; toogle species in widget
          IF threshcount gt 0 THEN BEGIN
            index = WHERE(~ (*(*parameter).arguments.speciesselected)[uniqInd], togglecount)
            IF togglecount gt 0 THEN *(*parameter).arguments.togglespeciesselected = samplesdatastruct.HEADER[uniqInd[index]+3] ; 0-2 : ID, x, y
          ENDIF
        
        ENDELSE
      ENDIF ELSE BEGIN
        (*(*parameter).arguments.speciesselected)[event.VALUE] = event.SELECT
        index = WHERE(~ *(*parameter).arguments.speciesselected) + 3 ; 0-2 : ID, x, y
        *(*parameter).arguments.togglespeciesselected = samplesdatastruct.HEADER[index]
      ENDELSE
    END
    'speciesRange' : BEGIN
    END
    'speciesAll' : BEGIN
      IF thresh gt 0. THEN BEGIN
        IF threshcount gt 0 THEN BEGIN
          *(*parameter).arguments.togglespeciesselected = ''
          (*(*parameter).arguments.speciesselected)[uniqInd] = 1b
          WIDGET_CONTROL, bgroupSamples, SET_VALUE=*(*parameter).arguments.speciesselected
        ENDIF ELSE BEGIN
          *(*parameter).arguments.togglespeciesselected = samplesdatastruct.HEADER[uniqInd+3]
          (*(*parameter).arguments.speciesselected)[event.VALUE] = 0b
          WIDGET_CONTROL, bgroupSamples, SET_VALUE=*(*parameter).arguments.speciesselected
        ENDELSE
      ENDIF ELSE BEGIN
        (*(*parameter).arguments.speciesselected)[*] = 1b
        *(*parameter).arguments.togglespeciesselected = ''
        WIDGET_CONTROL, bgroupSamples, SET_VALUE=*(*parameter).arguments.speciesselected
      ENDELSE
    END
    'SpeciesClear' : BEGIN
      (*(*parameter).arguments.speciesselected)[*] = 0b
      *(*parameter).arguments.togglespeciesselected = samplesdatastruct.HEADER[uniqInd+3]
      WIDGET_CONTROL, bgroupSamples, SET_VALUE=(*(*parameter).arguments.speciesselected)
    END
    ELSE :
  ENDCASE
END

FUNCTION maxentwrapper_toggleAutofeature, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  sensID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='childBaseFeatures')
  WIDGET_CONTROL, sensID, SENSITIVE=~event.SELECT
  (*parameter).arguments.autofeature = event.SELECT
END

FUNCTION maxentwrapper_setFeature, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CASE event.VALUE OF
    0 : (*parameter).arguments.linear = event.SELECT
    1 : (*parameter).arguments.quadratic = event.SELECT
    2 : (*parameter).arguments.product = event.SELECT
    3 : (*parameter).arguments.threshold = event.SELECT
    4 : (*parameter).arguments.hinge = event.SELECT
    ELSE : MESSAGE, 'Error W1'
  ENDCASE
END

FUNCTION maxentwrapper_setOutSet, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CASE event.VALUE OF
    0 : (*parameter).arguments.responsecurves = event.SELECT
    1 : (*parameter).arguments.jackknife = event.SELECT
;    1 : (*parameter).arguments.pictures = event.SELECT
;    2 : (*parameter).arguments.jackknife = event.SELECT
    ELSE : MESSAGE, 'Error W2'
  ENDCASE
END

PRO maxentwrapper_setOutputFormat, event
  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  (*parameter).arguments.outputformat = event.STR
END

PRO maxentwrapper_setLayerDir, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter
  
  baseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='envLayerBox')
  baseID2 = WIDGET_INFO(event.TOP, FIND_BY_UNAME='envLayerBox2')
  maskBase = WIDGET_INFO(event.TOP, FIND_BY_UNAME='maskBox')

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseEnvLayer')
  textMaskID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseMask')
  
  filter = [['*.hdr;*.HDR','*.csv;*.CSV'],['ENVI file','CSV-Format (SWD)']]
  title = 'Select ENVI file or CSV-formatted file'
  IF (*parameter).dataObj.openData(event.TOP, filter, title, IOOBJ=(*parameter).ioObj) THEN BEGIN
  
    (*parameter).dataObj.GetProperty $
      , NB=nb $
      , POS=pos $
      , INPUTFILENAME=inputfilename $
      , BANDNAMES=bandnames
  
    WIDGET_CONTROL, textID, SET_VALUE=inputfilename

    (*parameter).dataObj.SetProperty, MASKFILE=''
    WIDGET_CONTROL, textMaskID, SET_VALUE=''
        
    filename = FILE_BASENAME(inputfilename)
    fileArr = STRLOWCASE(STRSPLIT(filename,'.',/EXTRACT))
    IF STRCMP(fileArr[-1],'hdr') THEN BEGIN
      WIDGET_CONTROL, maskBase, SENSITIVE=1
    ENDIF ELSE BEGIN
      WIDGET_CONTROL, maskBase, SENSITIVE=0
    ENDELSE
    
    *(*parameter).arguments.layerselected = MAKE_ARRAY(nb, TYPE=0, VALUE=1)
    *(*parameter).arguments.layertypes = MAKE_ARRAY(nb, TYPE=0, VALUE=1)

    rowBaseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='rowBase')
    IF rowBaseID ne 0 THEN WIDGET_CONTROL, rowBaseID, /DESTROY
    rowBase = WIDGET_BASE(baseID2, /ROW, UNAME='rowBase')
    
    bgroupLayers = CW_BGROUP(rowBase, bandnames, UNAME ='bgroupLayers', $
      SET_VALUE=(*(*parameter).arguments.layerselected)[pos], XSIZE=320, $
      /NONEXCLUSIVE, EVENT_FUNCT='maxentwrapper_toogleLayers') ; YSIZE=250

    types = MAKE_ARRAY(nb, TYPE=7, VALUE='continuous')

    bgroupLayersType = CW_BGROUP(rowBase, types, UNAME ='bgroupLayersType', $
      SET_VALUE=(*(*parameter).arguments.layerselected)[pos], XSIZE=130, $
      /NONEXCLUSIVE, EVENT_FUNCT='maxentwrapper_toogleLayersType') ;, XSIZE=250, YSIZE=250


;    bgroupLayers = CW_BGROUP(baseID2, bandnames, UNAME ='bgroupLayers', $
;      SET_VALUE=(*(*parameter).arguments.layerselected)[pos], /NONEXCLUSIVE, EVENT_FUNCT='toogleLayers') ;, XSIZE=250, YSIZE=250
  
    WIDGET_CONTROL, baseID, MAP=1

  ENDIF ELSE BEGIN
    WIDGET_CONTROL, textID, SET_VALUE=''
    WIDGET_CONTROL, maskBase, SENSITIVE=0
    (*parameter).dataObj.SetProperty, MASKFILE=''
    WIDGET_CONTROL, baseID, MAP=0
    rowBaseID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='rowBase')
    IF rowBaseID ne 0 THEN WIDGET_CONTROL, rowBaseID, /DESTROY
    *(*parameter).arguments.layerselected = 0
    
  ENDELSE
  
END

FUNCTION maxentwrapper_toogleLayers, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  eventUname = WIDGET_INFO(event.ID, /UNAME)
  bgroupSamples = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupLayers')
  
  (*parameter).dataObj.GetProperty $
    , BANDNAMES=bandnames $
    , NB=nb $
    , POS=pos
  
  CASE eventUname OF
    'bgroupLayers' : BEGIN
      (*(*parameter).arguments.layerselected)[event.VALUE] = event.SELECT
      index = WHERE(~ *(*parameter).arguments.layerselected)
      *(*parameter).arguments.togglelayerselected = bandnames[index]
    END
    'layersRange' : BEGIN
    END
    'layerAll' : BEGIN
      (*(*parameter).arguments.layerselected)[*] = 1
      *(*parameter).arguments.togglelayerselected = ''
      WIDGET_CONTROL, bgroupSamples, SET_VALUE=*(*parameter).arguments.layerselected
    END
    'layerClear' : BEGIN
      (*(*parameter).arguments.layerselected)[*] = 0
      *(*parameter).arguments.togglelayerselected = bandnames
      WIDGET_CONTROL, bgroupSamples, SET_VALUE=*(*parameter).arguments.layerselected
    END
    ELSE :
  ENDCASE

;  pos = WHERE(*(*parameter).arguments.layerselected eq 1, numberOfBands)
;
;  (*parameter).dataObj.setProperty, NUMBEROFBANDS=numberOfBands, POS=pos

END

FUNCTION maxentwrapper_toogleLayersType, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  eventUname = WIDGET_INFO(event.ID, /UNAME)
  bgroupSamples = WIDGET_INFO(event.TOP, FIND_BY_UNAME='bgroupLayersType')
  
  (*parameter).dataObj.GetProperty $
    , BANDNAMES=bandnames $
    , NB=nb $
    , POS=pos
  
  (*(*parameter).arguments.layertypes)[event.VALUE] = event.SELECT
  index = WHERE(~ *(*parameter).arguments.layertypes)
  *(*parameter).arguments.togglelayertype = bandnames[index]

END

PRO maxentwrapper_setMask, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textBaseMask')

  IF (*parameter).dataObj.openMask(event.TOP) THEN BEGIN
    (*parameter).dataObj.GetProperty, MASKFILE=mask
    WIDGET_CONTROL, textID, SET_VALUE=mask
  ENDIF ELSE BEGIN
    WIDGET_CONTROL, textID, SET_VALUE=''  
  ENDELSE
  
END

PRO maxentwrapper_setOutputDir, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  outdir = DIALOG_PICKFILE(DIALOG_PARENT=event.TOP, /DIRECTORY $
    , /MUST_EXIST, /WRITE, TITLE='Select output directory')

  (*parameter).arguments.outputdirectory = outdir

  textID = WIDGET_INFO(event.TOP, FIND_BY_UNAME='textOutDir')
  WIDGET_CONTROL, textID, SET_VALUE=outdir
  
END

PRO maxentwrapper_runWidget, event

  ON_ERROR,2

  WIDGET_CONTROL, event.TOP, GET_UVALUE=parameter

  CATCH, Error_status
  IF Error_status NE 0 THEN BEGIN
    PRINT, 'Error index: ', Error_status
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    warn = [ $
        'Error index: ' + STRING(Error_status) $
      , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
    ]
    dialog = DIALOG_MESSAGE(warn, /ERROR)
    
    CATCH, /CANCEL
    RETURN
  ENDIF
  
  IF (*parameter).obj.checkArgumentsModel((*parameter).arguments, (*parameter).dataObj) THEN BEGIN
    (*parameter).arguments.canceled = 0
    WIDGET_CONTROL, event.TOP, /DESTROY
  ENDIF
END

PRO maxentwrapper_exitWidget, event
  WIDGET_CONTROL, event.TOP, /DESTROY
END



FUNCTION maxentwrapper_maxentWidgetModel, objStruct, appinfo

  ON_ERROR,2

  arguments = maxentwrapper_initArguments()
  
  params = { $
      obj : objStruct.MaxEntObj $
    , dataObj : objStruct.dataObj $
    , ioObj : objStruct.ioObj $
    , arguments : arguments $
  }
  globalParameters = PTR_NEW(params)
  
  group_leader = appinfo.hubGetValue('groupLeader')
  
  buttonXsize = 75

  base = WIDGET_BASE(title=appinfo.hubGetValue('title')+' - Determine model',/COLUMN, GROUP_LEADER=group_leader $
    , UVALUE=globalParameters)
  
  baseSettings = WIDGET_BASE(base,/ROW)
  
  childBaseL = WIDGET_BASE(baseSettings, /COLUMN, /ALIGN_TOP)
  childBaseR = WIDGET_BASE(baseSettings, /COLUMN, /ALIGN_TOP)
  
  labelBaseSamplesTop = WIDGET_LABEL(childBaseL, VALUE='Samples', /ALIGN_CENTER)
  childBaseSamples = WIDGET_BASE(childBaseL, /ROW, /ALIGN_LEFT)
  labelBaseSamples = WIDGET_LABEL(childBaseSamples, VALUE='Samples file')
  textBaseSamples = WIDGET_TEXT(childBaseSamples, /NO_NEWLINE, UNAME='textBaseSamples', $
     XSIZE=50) 
  buttonBaseSamples = WIDGET_BUTTON(childBaseSamples, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setSamples', XSIZE=buttonXsize)

  samplesBox = WIDGET_BASE(childBaseL, XSIZE=450, /FRAME, $ ;YSIZE=50, 
    /COLUMN, YSIZE=260, UNAME='samplesBox')
  WIDGET_CONTROL, samplesBox, MAP=0
  
  samplesBoxSpecies = WIDGET_BASE(samplesBox, XSIZE=450, /FRAME, $ ;YSIZE=50, 
    /SCROLL, Y_SCROLL_SIZE=220, UNAME='samplesBoxSpecies')
  
  samplesBoxButtons = WIDGET_BASE(samplesBox, /ROW, /FRAME, UNAME='samplesBoxButtons')
;  buttonRange = WIDGET_BUTTON(samplesBoxButtons, /FLAT, VALUE='Select Range', $
;    EVENT_FUNC='maxentwrapper_toogleSpecies', XSIZE=buttonXsize, UNAME='speciesRange')
  buttonAll = WIDGET_BUTTON(samplesBoxButtons, /FLAT, VALUE='Select All', $
    EVENT_FUNC='maxentwrapper_toogleSpecies', XSIZE=buttonXsize, UNAME='speciesAll')
  buttonNone = WIDGET_BUTTON(samplesBoxButtons, /FLAT, VALUE='Clear', $
    EVENT_FUNC='maxentwrapper_toogleSpecies', XSIZE=buttonXsize, UNAME='SpeciesClear')  

  speciesThresholdParamBox = WIDGET_BASE(childBaseL, /ROW, UNAME='speciesThresholdParamBox', $
    SENSITIVE=0)
    
  speciesThresholdParam = CW_FIELD(speciesThresholdParamBox, /FLOATING, /ALL_EVENTS $
    , TITLE='Threshold for species appearance [0..100]: ', VALUE=arguments.speciesthreshold $
    , UNAME='speciesThreshold') ; ;, FRAME=1
  
  labelBaseEnvLayerTop = WIDGET_LABEL(childBaseR, VALUE='Image or background data', /ALIGN_CENTER)
  childBaseEnvLayer = WIDGET_BASE(childBaseR, /ROW, /ALIGN_RIGHT)
  labelBaseEnvLayer = WIDGET_LABEL(childBaseEnvLayer, VALUE='Image file')
  textBaseEnvLayer = WIDGET_TEXT(childBaseEnvLayer, /NO_NEWLINE, UNAME='textBaseEnvLayer', $
     XSIZE=50) 
  buttonBaseEnvLayer = WIDGET_BUTTON(childBaseEnvLayer, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setLayerDir', XSIZE=buttonXsize)

  envLayerBox = WIDGET_BASE(childBaseR, XSIZE=475, /FRAME, $ ;YSIZE=50, 
    /COLUMN, YSIZE=260, UNAME='envLayerBox')
  WIDGET_CONTROL, envLayerBox, MAP=0

  envLayerBox2 = WIDGET_BASE(envLayerBox, XSIZE=450, /FRAME, $ ;YSIZE=50, 
    /SCROLL, Y_SCROLL_SIZE=220, UNAME='envLayerBox2')
  
  envLayerBoxButtons = WIDGET_BASE(envLayerBox, /ROW, /FRAME, UNAME='envLayerBoxButtons')
;  buttonRange = WIDGET_BUTTON(samplesBoxButtons, /FLAT, VALUE='Select Range', $
;    EVENT_FUNC='maxentwrapper_toogleSpecies', XSIZE=buttonXsize, UNAME='speciesRange')
  layerAll = WIDGET_BUTTON(envLayerBoxButtons, /FLAT, VALUE='Select All', $
    EVENT_FUNC='maxentwrapper_toogleLayers', XSIZE=buttonXsize, UNAME='layerAll')
  layerNone = WIDGET_BUTTON(envLayerBoxButtons, /FLAT, VALUE='Clear', $
    EVENT_FUNC='maxentwrapper_toogleLayers', XSIZE=buttonXsize, UNAME='layerClear')
  
;  labelBaseMaskTop = WIDGET_LABEL(childBaseR, VALUE='Mask file', /ALIGN_CENTER)
  childBaseMask = WIDGET_BASE(childBaseR, /ROW, /ALIGN_RIGHT, SENSITIVE=0, UNAME='maskBox')
  labelBaseMask = WIDGET_LABEL(childBaseMask, VALUE='Mask file')
  textBaseMask = WIDGET_TEXT(childBaseMask, /NO_NEWLINE, UNAME='textBaseMask', $
     XSIZE=50) 
  buttonBaseMask = WIDGET_BUTTON(childBaseMask, /FLAT, VALUE='Browse' $
    , EVENT_PRO='maxentwrapper_setMask', XSIZE=buttonXsize)

  childBaseFeatures = WIDGET_BASE(childBaseL, /COLUMN, /ALIGN_LEFT, $
    UNAME='childBaseFeatures', SENSITIVE=0)
  featureNames = [ $
      'Linear features' $
    , 'Quadratic features' $
    , 'Product features' $
    , 'Threshold features' $
    , 'Hinge features' $
  ]
  
  featureValues = [ $
      arguments.linear $
    , arguments.quadratic $
    , arguments.product $
    , arguments.threshold $
    , arguments.hinge $
  ]
  features = CW_BGROUP(childBaseFeatures, featureNames, /NONEXCLUSIVE, $
    SET_VALUE=featureValues, UNAME='button_features', EVENT_FUNCT='maxentwrapper_setFeature')
  autoFeatures = CW_BGROUP(childBaseL, 'Auto features', /NONEXCLUSIVE, $
    SET_VALUE=arguments.autofeature, UNAME='button_autofeatures', UVALUE='button_features', $
    EVENT_FUNCT='maxentwrapper_toggleAutofeature')


  childBaseOutSet = WIDGET_BASE(childBaseR, /COLUMN, /ALIGN_RIGHT)
  outSetNames = [ $
      'Create response curves' $
;    , 'Make pictures of predictions' $
    , 'Do jackknife to measure variable importance' $
  ]
  outSetValues = [ $
      arguments.responsecurves $
;    , arguments.pictures $
    , arguments.jackknife $
  ]
  outSet = CW_BGROUP(childBaseOutSet, outSetNames, /NONEXCLUSIVE, $
    SET_VALUE=outSetValues, UNAME='button_outset', EVENT_FUNCT='maxentwrapper_setOutSet')
  
;  outputFormats = [ $
;      'Logistic' $
;    , 'Cumulative' $
;    , 'Raw' $
;  ]
;  childBaseOutputFormat = WIDGET_BASE(childBaseR, /ROW, /ALIGN_RIGHT)
;  labelOutputFormat = WIDGET_LABEL(childBaseOutputFormat, VALUE='Output format')
;  outputFormat = WIDGET_COMBOBOX(childBaseOutputFormat, /FLAT, VALUE=outputFormats, $
;    EVENT_PRO='maxentwrapper_setOutputFormat')

  childBaseOutDir = WIDGET_BASE(childBaseR, /ROW, /ALIGN_RIGHT)
  labelOutDir = WIDGET_LABEL(childBaseOutDir, VALUE='Output directory')
  textOutDir = WIDGET_TEXT(childBaseOutDir, /NO_NEWLINE, UNAME='textOutDir', $
    XSIZE=45) 
  buttonOutDir = WIDGET_BUTTON(childBaseOutDir, /FLAT, VALUE='Browse', $
    EVENT_PRO='maxentwrapper_setOutputDir', XSIZE=buttonXsize)
  

  baseInteract = WIDGET_BASE(base, /ROW, /ALIGN_RIGHT)
  
  buttonStart = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Run', $
    EVENT_PRO='maxentwrapper_runWidget', XSIZE=buttonXSize)
    
  buttonSettings = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Settings', $
    EVENT_PRO='maxentwrapper_settingsWidget', XSIZE=buttonXSize)

  buttonExit = WIDGET_BUTTON(baseInteract, /ALIGN_CENTER, /FLAT, VALUE='Exit', $
    EVENT_PRO='maxentwrapper_exitWidget', XSIZE=buttonXSize)

  WIDGET_CONTROL, base, /REALIZE

  CATCH, Error_widget
  IF Error_widget NE 0 THEN BEGIN
    PRINT, 'Error index: ', Error_widget
    PRINT, 'Error message: ', !ERROR_STATE.MSG
    warn = [ $
        'Error index: ' + STRING(Error_widget) $
      , 'Error message: ' + STRING(!ERROR_STATE.MSG) $
    ]
    dialog = DIALOG_MESSAGE(warn, /ERROR)
    
  ENDIF
  
  XMANAGER, 'maxentwrapper_maxentWidgetModel', base ;, /NO_BLOCK

  CATCH, /CANCEL

  RETURN, (*globalParameters).arguments
END