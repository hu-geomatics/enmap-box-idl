;+
; :Hidden:
;-

FUNCTION MaxEntWrapper_correctString, stringArray

  ON_ERROR,2

  expression = ':<>\*?"' ; characters not allowed in filenames
  FOREACH string, stringArray, i DO BEGIN
    string = STRJOIN(STRSPLIT(string, expression, /EXTRACT, /PRESERVE_NULL), '_')
    stringArray[i] = string
  ENDFOREACH

  RETURN, stringArray
  
END