;+
; :Hidden:
;-

PRO MaxEntWrapper_cleanup $
  , arguments $
  , obj $
  , dataObj $
  , ioObj

  ON_ERROR,2

  IF SIZE(arguments, /TYPE) eq 8 THEN BEGIN
    IF PTR_VALID(arguments.togglelayertype) THEN PTR_FREE, arguments.togglelayertype
    IF PTR_VALID(arguments.togglespeciesselected) THEN PTR_FREE, arguments.togglespeciesselected
    IF PTR_VALID(arguments.togglelayerselected) THEN PTR_FREE, arguments.togglelayerselected
    IF PTR_VALID(arguments.speciesselected) THEN PTR_FREE, arguments.speciesselected
    IF PTR_VALID(arguments.layerselected) THEN PTR_FREE, arguments.layerselected
    IF PTR_VALID(arguments.layertypes) THEN PTR_FREE, arguments.layertypes
    IF PTR_VALID(arguments.lambdasfilename) THEN PTR_FREE, arguments.lambdasfilename
  ENDIF
  IF OBJ_VALID(obj) THEN OBJ_DESTROY, obj
  IF OBJ_VALID(dataObj) THEN OBJ_DESTROY, dataObj
  IF OBJ_VALID(ioObj) THEN OBJ_DESTROY, ioObj
END
