;+
; :Hidden:
;-

FUNCTION maxentwrapper_bytetype, datatype

  ON_ERROR,2

  CASE datatype OF
    1 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 8
    END
    2 : BEGIN
      pixeltype = 'SIGNEDINT'
      nbits = 16
    END
    3 : BEGIN
      pixeltype = 'SIGNEDINT'
      nbits = 32
    END
    4 : BEGIN
      pixeltype = 'FLOAT'
      nbits = 32
    END
    5 : BEGIN
      pixeltype = 'FLOAT'
      nbits = 64
    END
    12 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 16
    END
    13 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 32
    END
    14 : BEGIN
      pixeltype = 'SIGNEDINT'
      nbits = 64
    END
    15 : BEGIN
      pixeltype = 'UNSIGNEDINT'
      nbits = 64
    END
  ENDCASE
  
  result = { $
      pixeltype : pixeltype $
    , nbits : nbits $
  }
  
  RETURN, result
  
END