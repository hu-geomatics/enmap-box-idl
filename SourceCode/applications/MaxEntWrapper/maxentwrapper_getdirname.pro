;+
; :Hidden:
;-


;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

function MaxEntWrapper_getDirname
  
  result = filepath('MaxEntWrapper', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

pro test_MaxEntWrapper_getDirname

  print, MaxEntWrapper_getDirname()
  print, MaxEntWrapper_getDirname(/SourceCode)

end
