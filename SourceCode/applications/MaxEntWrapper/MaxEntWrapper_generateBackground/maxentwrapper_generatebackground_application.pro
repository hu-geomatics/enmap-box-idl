;+
; :Hidden:
;-

;
;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `MaxEntWrapper_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `MaxEntWrapper_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `MaxEntWrapper_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro MaxEntWrapper_generateBackground_application, applicationInfo
  
  ; get global settings for this application
  settings = MaxEntWrapper_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
;  groupLeader = settings.hubGetValue('groupLeader')
;
;  obj = MaxEntWrapperObj()
;  dataObj = MaxEntWrapperImgObj()
;  ioObj = MaxEntWrapperIO()
;
;  objStruct = { $
;      MaxEntObj : obj $
;    , dataObj : dataObj $
;    , ioObj : ioObj $
;  }
;
;; _getParameters function
;  arguments = maxentwrapper_bgpointsWidget(objStruct, groupLeader)
;
;  IF ~ arguments.canceled THEN BEGIN
;  
;    dataObj.GetProperty, INDICES=indices
;    
;    IF ~ ISA(indices) THEN dataObj.randomBackgroundPoints, arguments.maximumbackground
;    dataObj.GetProperty, BANDNAMES=bandnames, POS=pos
;    background = dataObj.readBackgroundData(arguments.maximumbackground)
;    species = MAKE_ARRAY(arguments.maximumbackground, $
;      TYPE=7, VALUE='background')
;    
;    ioObj.writeSamplesData $
;      , arguments.outputfile $
;      , species $
;      , background.coordinates $
;      , DATA=background.background $
;      , VARS=bandnames[pos]
;;      , VARS='Var' + STRING(pos, FORMAT='(I03)')
;  
;  ENDIF
;
;  MaxEntWrapper_cleanup, arguments, obj, dataObj, ioObj
  
  parameters = MaxEntWrapper_generateBackground_getParameters(settings)
  
  if parameters['accept'] then begin
  
    MaxEntWrapper_generateBackground_processing,parameters, settings
  
  endif
  
end