;+
; :Hidden:
;-

;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
FUNCTION MaxEntWrapper_generateBackground_getParameters, settings

  ON_ERROR,2
  
  groupLeader = settings.hubGetValue('groupLeader')

;  ; auto-managed widget program
  HUBAMW_PROGRAM, groupLeader, Title=settings.hubGetValue('title')+' - Generate background points'

  HUBAMW_FRAME, Title='Input'
  HUBAMW_INPUTSAMPLESET, 'samplesFilename', /MASKING, ReferenceTitle='Input Mask', /ReferenceOptional
  
  HUBAMW_PARAMETER, 'maximumBackground', Title='Max number of background points: ' $
    , Value=10000L, IsGE=1L, /Integer
    
  HUBAMW_FRAME, Title='Output'
  HUBAMW_OUTPUTFILENAME, 'outputFilename', Title='Output File', EXTENSION='csv'
  HUBAMW_CHECKBOX, 'quicklook', Title='Show quicklook', Value=1
  parameters = HUBAMW_MANAGE()

  RETURN, parameters
END