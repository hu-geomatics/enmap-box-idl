;+
; :Hidden:
;-

;+
; :Author: Carsten Oldenburg
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`MaxEntWrapper_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
pro MaxEntWrapper_generateBackground_processing, parameters, settings

  ioObj = MaxEntWrapperIO()

  IF (parameters['samplesFilename']).HasKey('labelFilename') $
    THEN maskFilename = (parameters['samplesFilename'])['labelFilename']
  imageFilename = (parameters['samplesFilename'])['featureFilename']
  outputFilename = parameters['outputFilename']
  numberOfBackgroundPoints = parameters['maximumBackground']
  showQuicklook = parameters['quicklook']

  inputimage = hubioimginputimage(imageFilename)
  IF ISA(maskFilename) THEN maskimage = hubioimginputimage(maskFilename)

  IF ~ OBJ_VALID(inputimage) THEN MESSAGE, 'Problem with image!!' 

  nb = inputimage.getMeta('bands')
  numberoflines = inputimage.getMeta('lines')
  numberofsamples = inputimage.getMeta('samples')
  pos = LINDGEN(nb)
  interleave = inputimage.getMeta('interleave')
  datatype = inputimage.getMeta('data type')
  bandNames = MaxEntWrapper_correctString(inputimage.getMeta('band names'))
  IF ~ ISA(bandNames) THEN bandNames = STRING(INDGEN(nb)+1, FORMAT='(I0)')
  map_info = inputimage.getMeta('map info')
  
  message_Text = 'Are the background points correct?'

  REPEAT BEGIN

    IF OBJ_VALID(maskimage) THEN BEGIN
    
      maskimage.initReader, /BAND
      mask = maskimage.getData(0)
      unmaskIndex = WHERE(mask ne 0,count,/NULL)
      maskimage.finishReader
  
      IF count gt numberOfBackgroundPoints THEN BEGIN
        r = RANDOMU(SYSTIME(1),count)
        indices = (unmaskIndex)[(SORT(r))[0:numberOfBackgroundPoints-1]]
      ENDIF ELSE indices = unmaskIndex
    ENDIF ELSE BEGIN
      numberOfPixels = numberoflines * numberofsamples
  
      IF numberOfPixels gt numberOfBackgroundPoints THEN BEGIN
        r = RANDOMU(SYSTIME(1),numberOfPixels)
        indices = (SORT(r))[0:numberOfBackgroundPoints-1]
      ENDIF ELSE indices = UINDGEN(numberOfPixels)
  
    ENDELSE
  
    IF showQuicklook THEN BEGIN
      bgdata = BYTARR(numberOfSamples, numberOfLines)
      bgdata[indices] = 1B
      
      writerSettings = hash()
      writerSettings['samples'] = numberOfSamples
      writerSettings['lines'] = numberOfLines
      writerSettings['bands'] = 1
      writerSettings['data type'] = 'byte'
      writerSettings['dataFormat'] = 'band'
      
      tempfile = FILEPATH('bgimage', /TMP)
      bgimage = hubioimgoutputimage(tempfile)
      bgimage.initWriter, writerSettings
      bgimage.writeData, bgdata
      bgimage.cleanup

      bgimage = hubioimginputimage(tempfile)
      bgimage.quickLook, [0] ; , Stretch=0
      bgimage.cleanup

      bool = DIALOG_MESSAGE(message_Text, /CENTER, /QUESTION, TITLE='Background points')

    ENDIF ELSE bool = "yes"
    
  ENDREP UNTIL ~ showQuicklook OR STRCMP(STRLOWCASE(bool),'yes')

  pixelCoords = ARRAY_INDICES([numberofsamples,numberoflines],indices,/DIMENSIONS)

  coordinates = [pixelCoords[0,*] * map_info.sizex + map_info.easting , $
    map_info.northing - pixelCoords[1,*] * map_info.sizey]

  background = MAKE_ARRAY(nb,numberOfBackgroundPoints,TYPE=datatype)

  IF ~ STRCMP(interleave,'bsq') THEN BEGIN 
    ySortedIndex = SORT(pixelCoords[1,*])
    yUniqEnd = UNIQ(pixelCoords[1,ySortedIndex])
    yUniqStart = SHIFT(yUniqEnd,1) + 1
    yUniqStart[0] = 0
    
    inputimage.initReader, /SLICE
    FOREACH element,yUniqStart,i DO BEGIN
      slice = inputimage.getData(pixelCoords[1,ySortedIndex[element]])
      FOR n=element,yUniqEnd[i] DO BEGIN
        background[*,n] = slice[*,pixelCoords[0,ySortedIndex[n]]]
      ENDFOR
    ENDFOREACH
    inputimage.finishReader
    
    coordinates = coordinates[*,ySortedIndex]
  ENDIF ELSE BEGIN
    inputimage.initReader, /BAND
    FOREACH element,pos,i DO BEGIN
      band = inputimage.getData(element)
      background[i,*] = band[indices]
    ENDFOREACH 
    inputimage.finishReader
  ENDELSE
  
  species = MAKE_ARRAY(numberOfBackgroundPoints, TYPE=7, VALUE='background')
    
  ioObj.writeSamplesData $
    , outputFilename $
    , species $
    , coordinates $
    , DATA=background $
    , VARS=bandnames[pos]
;    , VARS='Var' + STRING(pos, FORMAT='(I03)')

  ioObj.cleanup

end

;+
; :Hidden:
;-
pro test_MaxEntWrapper_processing

  samplesFilename = hash( $
;    'labelFilename', 'D:\Arbeit\IDLWorkspace80\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Mask', $
    'featureFilename', 'D:\Arbeit\IDLWorkspace80\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Image' $
  )
    
  parameters = hash( $
      'samplesFilename', samplesFilename $
    , 'outputFilename', 'D:\Carsten\temp\out\background.csv' $
    , 'accept', 1 $
    , 'quicklook', 1 $
    , 'maximumBackground', 10000 $
  )

  settings = MaxEntWrapper_getSettings()
  
  MaxEntWrapper_generateBackground_processing, parameters, settings

end  
