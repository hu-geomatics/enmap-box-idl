# MaxEnt-Wrapper changelog
#--------------------------

1.4 - 2012.11.27
  - Adapted to EnMap-Box 1.4

1.3 - 2012.11.14
  - Fixed useless output format (moved to project widget)
  - Fixed problems with whitespaces during projection
  - Fixed bug with numerical species names
  - Added tool for creating samples with data files (swd)

1.2.2
  - Fixed problem with skalar string array during projection process

1.2.1
  - Fixed problem with mapinfo after projecting

1.2.0
  - Fixed problems with species threshold
  - Adapted MaxEnt-Wrapper to EnMapBox v.1.3
  - changed textboxes to cw_field in GUIs
  - Several minor bugfixes and modifications

1.1.0
  - Added prevalence to apply model widget
  - Removed prevalence from create model widget

1.0.6
  Fixed
  - JAVA arguments: problem with last directory seperator

1.0.5
  Fixed
  - EnMap-Box: Problem with scalar string

  Added
  - Copyright

1.0.4
  Fixed
  - Widget for Create Model: Problem with mask
  - Widget for Create Model: invisible continuous/categorical checkbox
  - Widget for Create Model: Problem with different map info
                             (species and environmental layers)

1.0.3
  Fixed
  - size of buttons and label for the background points widget

1.0.2
  Fixed
  - exclude species where presence-absence-data is zero (txt-file)

1.0.1 
  Fixed
  - Problem with ENVI-data of type Float
  - Problem with samples lying outside the boundaries of an ENVI-image

  Added
  - Changed Progress window for creating background and samples

1.0.0 Final Version released