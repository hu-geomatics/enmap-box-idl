;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

;+
; :Description:
;    This function 
;    
;    (1) creates a stratified mask and 
;    
;    (2) returns a hash containing some basic file information and statistic measures on the input image and the creates stratified mask.
;        
;    1. Stratified mask 
;    ~~~~~~~~~~~~~~~~~~
;    
;    The aims of the stratified mask are to show which pixels of an input image are valid pixels 
;    which stratum (or class of an classification file) they belong to.
;    
;    The stratified mask is a combination of the following masks:
;    
;     - The internal mask of a single image file, which 'masks' all pixels showing 
;        the data ignore value. In case a data ignore value is not defined the internal mask 
;        of an image file contains no masked pixels.   
;    
;     - An explicit defined mask file having values of zero, where a pixel is to be masked or non-zero for valide pixels.
;     
;     - A classification or stratification file showing the areas that belong to a specific stratum (or regions of interest).
;        
;        
;     Pixels in the final stratified mask will get a value of zero in case
;      
;     - they got masked by the images data ignore value, or
;     
;     - they got optionally masked an explicit mask file, or
;     
;     - they are not assigned to any stratum / class in the stratification file.  
;
;     Otherwise they will get the value give in the stratification file. In case no stratification file is used
;     their value is set to one. 
;    
;     2. File Information
;     ~~~~~~~~~~~~~~~~~~~
;     
;     During the creation of the stratified mask some file information and statistic measures are collected.
;     This avoids re-reading the image files. See `Outputs` for detailes.
;     
; :Returns: 
;     This functions returns a hash providing the following information::
;     
;       key                | type       | content
;       -------------------+------------+---------------------------------------
;       inputImgRange      | numeric[2] | input image data range
;       inputImgFileType   | byte       | input image file type 
;       inputImgDIV        | numeric    | input image data ignore value  
;       stratificationMask | string     | path to final strata mask
;       strataCounts       | long[nS]   | number of samples per each stratum
;       strataLookup       |            | class lookup as copied from inputStratification
;       strataNames        | string[nS] | the name of each stratum, e.g. ['masked', 'stratum 1', ... ] 
;       nStrata            | integer    | nS = the total number of final strata
;                          |            | can be within range 2 <= nS <= 255
;       -------------------+------------+-----------------------------------------
;       (nS = number of strata)
; :Params:
;    parameters, in, required, type=hash
;     This has contains the required parameters for this functions::
;     
;       key                 | type   | value
;       --------------------+--------+-------------------------
;       inputImage          | string | filepath to input image
;       fn_outputStrataMask | string | filepath of result
;       inputStratification | string | filepath to stratification file
;       inputMask           | string | filepath to mask file
;       ignoreValueHandling | string | how to handle data ignore values in inputImage?
;
;     The value for `ingoreValueHandling` controls how the internal mask of the input image with more than one band is interpreted:
;     
;       `ingoreValueHandling = 0` - an input images pixel gets masked if ANY band is value equal the data ignore value
;       
;       `ingoreValueHandling = 1` - an input images pixel gets masked if ALL band values are equal the data ignore value
;       
;       `ingoreValueHandling = 2` - do not care about the input images data ingore value and use the information defined by
;                                   the external masks defined in `inputMask` and `inputStratification` only.
;                                   
; :Keywords:
;    GroupLeader: in, optional, type=long
;      Use this keyword to specify the group leader for used widgets.
;      
;    NoShow: in, optional, type=boolean
;      Set this keyword to avoid showing a progressbar.
;-
function hubApp_randomSampling_getStrataMask, parameters, GroupLeader=groupLeader, NoShow=NoShow
  required = ['inputImage', 'deleteStrataMask']
  optional = ['inputMask','ignoreValueHandling','stratificationMask','inputStratification']
  hubHelper = hubHelper()
 
  ;default: 
  if ~parameters.hubIsa(required, /EvaluateAND, IndicesFalse=missing) then begin
     message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  fn = hubHelper.filepath('temp_strataMask')
  result = dictionary()
  result['stratificationMask'] = parameters.hubGetValue('stratificationMask' $
                            , default = hubHelper.filepath(/TMP, 'temp_strataMask'))

  
  
  ;0 => mask if ANY band is DIV
  ;1 => mask if ALL bands are DIV  
  ;2 => use external mask only, do not care about internal mask
  ;3 => map external mask on internal mask, 
  ;     only available in combination with /PreserveDimensions 
  ;see documentation of `hubHelper.getImageMask()`
  
  case parameters.hubGetValue('ignoreValueHandling', default='ALL') of
    'ALL' : maskMode = 1
    'ANY' : maskMode = 0
    else : message, "unknown value for key 'ingnoreValueHandling'"
  endcase
  
  imgInput = hubIOImgInputImage(parameters['inputImage'])
  
  if imgInput.isClassification() then begin
    imgInputDIV = 0
  endif else begin
    imgInputDIV = imgInput.getMeta('data ignore value')
  endelse
  
  tileLines = hubIOImg_getTileLines(imgInput.getMeta('samples') $
                                  , imgInput.getMeta('bands')+1 $
                                  , imgInput.getMeta('data type'))
  
  result['inputImgFileType'] = imgInput.getMeta('file type')
  result['inputImgDIV'] = imgInputDIV
  if not keyword_set(NoShow) then begin
    progressBar = hubProgressBar(Title='Random Sampling', Info='Create Image Statistics', GroupLeader=groupLeader)
    progressDone = 0
    progressMAX = float(imgInput.getMeta('lines') / tileLines) 
    progressBar.setRange, [progressDone, progressMax]
  endif
  
  
  imgInput.initReader, tileLines, slice=1, /TileProcessing ;, Mask=maskMode

  ;inputMode => which input images are involved?
  ;inputMode = 0 - image only
  ;inputMode = 1 - image + mask
  ;inputMode = 2 - image + stratification
  ;inputMode = 3 - image + mask + stratification
  inputMode = 0
  nStrata = 2
  
  if parameters.hubIsa('inputMask') then begin
      inputMode += 1
      imgMask = hubIOImgInputImage(parameters['inputMask'], /Mask)
      imgMask.initReader, tileLines, slice=1, /TileProcessing, /Mask
  endif
  
  if parameters.hubIsa('inputStratification') then begin
      inputMode += 2
      imgStrat = hubIOImgInputImage(parameters['inputStratification'], /Classification)
      imgStrat.initReader, tileLines, slice=1, /TileProcessing
      result['strataNames'] = imgStrat.getMeta('class names')
      nStrata = imgStrat.getMeta('classes')
      result['strataLookup'] = imgStrat.getMeta('class lookup')
      strataNames = imgStrat.getMeta('class names', default='stratum '+string(indgen(nStrata)+1))
  endif
  
  ;set mask strata names according to input mode
  case inputMode of
    0 : result['strataNames'] = ['ignored','valid']
    1 : result['strataNames'] = ['ignored/masked','valid']
    2 : result['strataNames'] = ['ignored/unclassified', strataNames[1:*]] ;[1.*] because [0] is class unclassified
    3 : result['strataNames'] = ['ignored/masked/unclassified', strataNames[1:*]]
  endcase
  result['nStrata'] = nStrata
  
  ;histogram to count pixel occurrence for each stratum
  incHist = hubMathIncHistogram([0, nStrata-1], nStrata)
  ;data range of total image
  incRange = hubMathIncRange()
  ;output image
  imgOutput = hubIOImgOutputImage(result['stratificationMask'] $
              ;do not show the file in the file list if it will get delated after the random
              ;sampling routine
              , NoOpen = parameters.hubKeywordset('deleteStrataMask'))
  imgOutput.copyMeta, imgInput, /CopySpatialInformation
  imgOutput.setMeta, 'classes', nStrata
  imgOutput.setMeta, 'class names', result['strataNames']
  colorseed = systime(1)
  ;imgOutput.setMeta, 'class lookup', bytscl(randomu(colorseed,3,nStrata), min=0, max=1, top=255)
  imgOutput.setMeta, 'file type', 'ENVI Classification'
  imgOutput.setMeta, 'class lookup', isa(imgStrat) ? imgStrat.getMeta('class lookup') : [[0,0,0],[255,255,255]]
  imgOutput.initWriter, imgInput.getWriterSettings(SetBands=1, SetDataType='byte')
 
  
  ;
  ;TILE RUN over the input image
  ;
  while ~imgInput.tileProcessingDone() do begin
    ;get tile pixels
    imageData = imgInput.getData()
    ;get the mask defined in the image pixels, as defined by the data ignore value 
    imageDataMask = hubHelper.getImageMask(imageData, maskMode, imageDIV=imgInputDIV)
    iNotMasked = where(imageDataMask ne 0, /NULL)
    if isa(iNotMasked) then incRange.addData, imageData[*, iNotMasked]
    
    case inputMode of 
      0 : outputLabels = imageDataMask
      1 : outputLabels = imageDataMask * imgMask.getData()
      2 : outputLabels = imageDataMask * imgStrat.getData()
      3 : outputLabels = imageDataMask * imgMask.getData() * imgStrat.getData()
    endcase 
    
    incHist.addData, outputLabels     ;count stratum pixels
    imgOutput.writeData, outputLabels ;write classification image
    if isa(progressBar) then progressBar.setProgress, ++progressDone
  endwhile
  hist = incHist.getResult()
  
  result['strataCounts'] = hist['histogram']
  result['inputImgRange'] = incRange.getResult() 
  
  incHist.cleanup
  incRange.cleanup
  
  imgInput.cleanup
  imgOutput.cleanup
  if isa(imgMask) then imgMask.cleanup
  if isa(imgStrat) then imgStrat.cleanup
  if isa(progressBar) then progressBar.cleanup

  return, result

end

;+
; :hidden:
;-
pro test_hubApp_randomSampling_getStrataMask
  base = 'D:\Dokumente\EnMAP-Box_resources\installer\IDL80\products\enmapBox\data\'
  parameters = Hash()
  ;required = ['fn_input', 'outputSamples','fn_class', 'classSamples', 'seed']
  ;parameters['inputImage'] = base + 'Hymap_Berlin-B_Regression-Estimation'
  ;parameters['inputImage'] = base + 'Hymap_Berlin-B_Image'
  ;parameters['divInput'] = -1
  ;parameters['inputStratification'] = base + 'Hymap_Berlin-A_Classification-GroundTruth'
  ;parameters['inputMask'] = base + 'Hymap_Berlin-A_Mask'
  
  base = 'D:\Sandbox\BigData\' 
  parameters['inputImage']           = base + 'HyMap_ownSub'
  ;parameters['inputStratification']  = base + 'SOIL_CLASS'
  ;parameters['inputMask']            = base + 'GSM_interp'
  
  parameters['fn_outputStrataMask'] = 'D:\Sandbox\Test\test'
  
;         DIVHandling |                 
;         ------------+---------+---------------------------------------------------------
;         PreserveImageDimension not set
;               0     | [1,nP]  | OR  - ignore pixel if ANY band value is set to DIV
;               1     | [1,nP]  | AND - ignore pixels if ALL band values are set to DIV
;               2     | [1,nP]  | use restrictions from external mask only (ignore DIV)
;         PreserveImageDimension is set
;               0     | [nB,nP] | OR  - ignore pixel if ANY band value is set to DIV
;               1     | [nB,nP] | AND - ignore pixel if ALL band values are set to DIV
;               2     | [nB,nP] | use restrictions from external mask only (ignore DIV)              
;               3     | [nB,nP] | band DIV values + external mask                
;         ------------+---------+---------------------------------------------------------
;
  parameters['ignoreValueHandling'] = 2
  res = hubApp_randomSampling_getStrataMask(parameters)
  print, res

end