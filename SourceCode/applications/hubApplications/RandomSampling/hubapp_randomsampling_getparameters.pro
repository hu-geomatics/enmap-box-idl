;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    This function returns the final number of samples to get selected from each 
;    stratum according to the used random sampling strategy.
;    
; :Params:
;    result: in, required, type=hash
;       This is the result hash returned by calling `hubAMW_manage(...)`.
;       As specified in dialog 2 this hash provides the following keys::
;       
;          key                       | type     | values
;        ----------------------------+----------+-------------
;         samplingType               | int      | 0 = equal sampling
;                                    |          | 1 = proportional sampling
;                                    |          | 2 = disproportional sampling / self defined
;         nStrata                    | int      | number of strata in strata mask
;                                    |          | is >= 2 
;         strataCounts               | long[]   | vector with number of counts per stratum of strata mask
;         sampleSizePercent          | float    | percent of samples to choose form each stratum != 0
;         stratumSampleSizeParameter | string[] | the hash-key name for each stratum  
;         -----------------------------------------------------------------------------
;         
;       
;
; :Keywords:
;    iLessThanRequested: out, optional, type=int[]
;       This vector returns the strata indicies of strata where less pixels are 
;       available for sampling that requested to choose from.  
;       
;    iZeroAvailable: out, optional, type=int[]
;       This vector informs you about empty strata. This can be the case when for example
;       all pixels within a stratum are masked by the data ignore value or
;       an external mask.  
;       
;-
function hubApp_randomSampling_getParameters_getSamplesPerStratum $
    , result $
    , iLessThanRequested=iLessThanRequested $
    , iZeroAvailable=iZeroAvailable 
    
    
    samplesRequested = []
    case result['samplingType'] of
      ;equal
      0 : begin
            samplesRequested = [0, replicate(result['sampleSize'], result['nStrata']-1)]
          end
      ;proportional
      1 : begin
            counts = float((result['strataCounts'])[1:*])
            samplesRequested = [0, fix(counts / 100 * result['sampleSizePercent'], type=13)]
          end
      ;disproportional
      2 : begin
            ;collect parameter values
            samples = [0] ;no sampling from class 0
            foreach amwParameterName, (result['stratumSampleParameterNames']) do begin
              samples = [samples, result[amwParameterName]]
            end
            samplesRequested = samples
          end
    endcase
    
    samplesFinal = samplesRequested
    samplesAvailable = result['strataCounts']
    
    iZeroAvailable = where(samplesAvailable[1:*] eq 0, /NULL, /L64)
    if isa(iZeroAvailable) then begin
      iZeroAvailable += 1
    endif
    iLessThanRequested = where(samplesAvailable lt samplesRequested, /NULL,/L64)
    if isa(iLessThanRequested) then begin
      ;use only the maximum number of available pixels
      samplesFinal[iLessThanRequested] = samplesAvailable[iLessThanRequested]
    endif 
    return, samplesFinal
end



;+
; :Description:
;    Callback function to verify the inputs of the first dialog.
;
; :Params:
;    resultHash
;
; :Keywords:
;    Message
;    UserInformation
;-
function hubApp_randomSampling_getParameters_Dialog1Check, resultHash, Message=message $
   , UserInformation=userInformation 
   msg = !NULL
   isConsistent = 1
   
   img1 = hubIOImgInputImage((resultHash['inputSampleSet'])['featureFilename'])
   
   if resultHash.hasKey('inputStratification') then begin
      
      imgStrat = hubIOImgInputImage(resultHash['inputStratification']) 
      
      if ~imgStrat.isClassification() then begin
        msg = [msg, 'Stratification image is not a classification']
      endif
      
      if ~img1.isCorrectSpatialSize(imgStrat.getSpatialSize()) then begin
        msg = [msg, 'Spatial sizes of input images mismatch'] 
      endif
      
   endif
   
   message = msg
   return, ~isa(msg) 
end
 
 
 
;+
; :Description:
;    Callback function to verify the inputs of the second dialog.
;    
; :Params:
;    resultHash: in, required, type=hash
;
; :Keywords:
;    Message
;    UserInformation
;-
function hubApp_randomSampling_getParameters_Dialog2Check $
    , resultHash, Message=message, UserInformation=userInformation
    
   msg = !NULL
   
   nStrata = n_elements(userInformation['strataCounts'])-1
   ;required to add the informations resulting form the first dialog
   ;e.g. strataCounts, strataNames etc. ...
   resultHash = resultHash + userInformation
   samplesFinal = hubApp_randomSampling_getParameters_getSamplesPerStratum( $
          resultHash $
        , iLessThanRequested=iLessThanRequested $
        , iZeroAvailable=iZeroAvailable)
   
   if total(samplesFinal) eq 0 then begin
      message = 'Total number of samples must be > zero'
      return, 0b
   endif

   if ~isa(msg) then begin
     info = List()
  
     iLessThanRequested = hubmathHelper.getDifference(iLessThanRequested, iZeroAvailable)
     if isa(iZeroAvailable) then begin 
      info.add, 'Strata without any (non-masked) pixel:'
      info.add, '  '+(userInformation['strataNames'])[iZeroAvailable], /Extract
      if ~isa(iLessThanRequested) then begin
        info.add, 'Sampling without these strata?'
      endif
     endif
     
     
     if isa(iLessThanRequested) then begin
      info.add, string(format='(%"Strata with less than %i pixels to choose from:")', samplesFinal['sampleSize'])
      foreach i, iLessThanRequested do begin
        info.add, string(format='(%"    %s (max. %i pixels)")' $
                  , (userInformation['strataNames'])[i] $
                  , (userInformation['strataCounts'])[i]) 
      endforeach
      info.add, ''
      info.add, 'Select all pixels available from them?'
     endif
  
     isOK = n_elements(info) eq 0 ? 'Yes' : dialog_message(info.toArray(), /Question, title='Allocation')
     if ~strcmp(isOk,'YES', /FOLD_CASE) then begin
      msg = ''
     endif
   endif 
   message = msg
   return, ~isa(msg)
 end


;
;Dialog 2: How many samples are to choose from each stratum?
;
function hubApp_randomSampling_getParameters_Dialog2, settings, result1
  hubHelper = hubHelper()
  isStratified = result1.hubIsa('inputStratification')
  strataCounts = result1['strataCounts']
  strataNames  = result1['strataNames']
    
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
  
    ;
    ;1. Show General File Information
    ; 
    hubAMW_Frame, title='Selected Inputs'
    maxFilepathLength= 35
    info = List()
    info.add,                                             'Image: '+hubHelper.getShortenedString(result1['inputImage'], maxFilepathLength)
    if result1.hubIsa('inputMask') then info.add,           'Mask:  '+hubHelper.getShortenedString(result1['inputMask'], maxFilepathLength)
    if result1.hubIsa('inputStratification') then info.add, 'Stratification: ' + hubHelper.getShortenedString(result1['inputStratification'], maxFilepathLength-9)
    
    info.add, 'Pixel Counts:'
    info.add, string(format='(%"  Total:          %i")', total(strataCounts))
    info.add, string(format='(%"  Masked/Ignored: %i")', strataCounts[0])
    info.add, string(format='(%"  For Sampling:   %i")', total(strataCounts[1:*])) 
    hubAMW_label, info.toArray() 
    
    ;
    ;2. Select Sampling Strategy
    ;
    hubAMW_frame, title='Sampling Strategy'
    
    tsize = isStratified ? 175 : 100
    ;2.1 Equalized Sampling - same number of pixels from each stratum
    hubAMW_subframe, 'samplingType' $
            , Title= isStratified ? 'Equalized Sampling' : 'Absolute Sampling' $
            , /SetButton, /ROW
      hubAMW_Parameter, 'sampleSize' $
                      , Title= isStratified ? 'Stratum Sample Size' : 'Sample Size' $
                      , IsGE=1, IsLE=max(strataCounts[1:*]) $
                      , Unit='Pixels', /Integer $
                      , TSize=tsize
                      
    ;2.2 Proportional Sampling - same percentage of pixels from each stratum
    hubAMW_subframe, 'samplingType' $
            , Title = isStratified ? 'Proportional Sampling' : 'Relative Sampling' $
            , /ROW
      hubAMW_Parameter, 'sampleSizePercent' $
                      , Title='Pixels' + (isStratified ? ' from each stratum' : '') $
                      , IsGE=0, IsLE=100, /Float $
                      , Unit = '%' $
                      , TSize=tsize
   
    ;2.3 Disproportional Sampling - exact number of samples fore each stratum
    ;    (only available for stratified sampling)
    if isStratified then begin
        hubAMW_subframe, 'samplingType', Title='Disproportional Sampling', /Column   
          ;a list that contains all hash-keys that refer to a stratum sample value gt 0
          
          ;rigth-aligned description
          maxLength = max(strlen(string(strataNames[1:*])))
          
          ;list the parameternames to be returned in the result hash
          stratumSampleParameterNames = List()
          for i=1, n_elements(strataNames) - 1 do begin
            parameterName  = string(format='(%"dispParameter%i")', i)
            stratumSampleParameterNames.add, parameterName 
            parameterTitle = string(format='(%"%'+string(maxLength)+'s")', strataNames[i])
            parameterUnit  = string(format='(%"of %i Pixels")', strataCounts[i])
            hubAMW_parameter, parameterName, title=parameterTitle $
                            , Unit = parameterUnit $
                            , IsGE=0, IsLE=strataCounts[i], /Integer 
          endfor
          result1['stratumSampleParameterNames'] = stratumSampleParameterNames
     endif
     

     
    hubAMW_frame ,   Title =  'Output'
     ;Define data ignore value (divOutput) if 
     ;  1) the input image is not an ENVI Classification (ENVI Classification -> data ignore value = 0)
     ;  2) the input images data ingore value is not defined
     if ~strcmp(result1['inputImgFileType'], 'ENVI Classification', /FOLD_CASE) then begin
      if result1.hubIsa('inputImgDIV') then begin
        divOutput = result1['inputImgDIV'] 
      endif else begin
        hubAMW_Parameter, 'divOutput', title='Data Ignore Value', /FLOAT
      endelse
     endif
     
    hubAMW_outputFilename, 'outputSamples',    title='Random Sample' $
                         , tsize=125, Optional=0, value='sample'
    hubAMW_outputFilename, 'outputComplement', title='Complement' $
                         ,tsize=125,  Optional=2, value='sampleComplement'
     
     result = hubAMW_manage(UserInformation = result1, ConsistencyCheckFunction='hubApp_randomSampling_getParameters_Dialog2Check')
     if result['accept'] then begin
       ;extend result hash with information
       result = result +result1
       if isa(divOutput) then result['divOutput'] = divOutput 
       result['samplesPerStratum'] = hubApp_randomSampling_getParameters_getSamplesPerStratum(result+result1)
     endif
     
     return, result
end


;
;Dialog 1: Get basic parameters
;
function hubApp_randomSampling_getParameters_Dialog1, settings, defaultValues
  hubHelper = hubHelper()
  seedChoiceNames  = ['Current System Time', 'Constant']
  seedChoiceValues = ['systime', 'constant']
  ignoreValueHandlingChoiceValues = ['ALL','ANY']
  tsize=125
  
  hubAMW_program , settings.hubgetValue('groupLeader'), Title = settings['title']
  hubAMW_frame ,   Title =  'Input'
    hubAMW_inputSampleSet, 'inputSampleSet', title='Image', /Masking $
                         , ReferenceTitle='Mask ', /ReferenceOptional $
                         , Value = defaultValues.hubGetValue('inputImage') $
                         , ReferenceValue = defaultValues.hubGetValue('inputMask') $
                         , TSize=tsize ;, SIZE=625
    hubAMW_inputImageFilename ,'inputStratification',Title='Stratification   ' $
                              , optional=2 $ ;/Classification
                              , value = defaultValues.hubGetValue('inputStratification') $
                              , TSize=tsize
  
    hubAMW_frame ,   Title =  'Parameters', /Advanced
    hubAMW_CheckList, 'seedChoice', Title='Random Sequence Seed: ' $
                    , List=seedChoiceNames $
                    , value=0, AllowEmptySelection = 0

    ;hubAMW_label, 'Exclude all Pixels where'
    hubAMW_CheckList, 'ignoreValueHandling' $
                    , Title='Exclude all Pixels where pixel value = data ignore value in:' $
                    , List=['ALL bands' $ ;index 0 => ALL bands
                          , 'ANY band' $  ;index 1 => ANY band
                          ] $
                          ;, 'Use all pixels'] $     ;index 2 => ignore data ignore values (but use the mask file)
                    , value = 0 $
                    , AllowEmptySelection = 0
                    
  result = hubAMW_manage(ConsistencyCheckFunction='hubApp_randomSampling_getParameters_Dialog1Check')
  if result['accept'] then begin
    result['ignoreValueHandling'] = ignoreValueHandlingChoiceValues[result['ignoreValueHandling']] 
    result['seedChoice'] = seedChoiceValues[result['seedChoice']]
    result['inputImage'] = (result['inputSampleSet'])['featureFilename']
    result['inputMask'] = (result['inputSampleSet'])['labelFilename']
    
    ;remove keys that are not required anymore
    result.hubRemove, ['inputSampleSet']
  endif
  return, result
end

;+
; :Description:
;     This function creates a widget to collect the parameters required to 
;     run the `hubApp_randomSampling_processing` routine.
;
; :Params:
;    settings: in, required, type=hash
;       Use this hahs to provide settings and default values for 
;       `hubApp_randomSampling_getParameters_Dialog1` and `hubApp_randomSampling_getParameters_Dialog2`
;
;       Use hash-key `groupLeader` to define the to level widget base.
;-
function hubApp_randomSampling_getParameters, settings, defaultValues
  if ~isa(defaultValues) then defaultValues = hash()

  result1 = hubApp_randomSampling_getParameters_Dialog1(settings, defaultValues)
  if result1['accept'] then begin
    ;get strataMask and return path, strataCounts and strataNames
    ;each stratum results from the intersected input files
    result1['deleteStrataMask'] = ~settings.hubKeywordSet('DEBUG')
    strataMaskInfo = hubApp_randomSampling_getStrataMask(result1, GroupLeader=settings.hubGetValue('groupLeader'))
    result2 = hubApp_randomSampling_getParameters_Dialog2(settings, result1 + strataMaskInfo) 
  endif
  
  if isa(result2) && result2['accept'] then begin
      result = result1 + result2
      result += settings.hubGetSubHash(['DEBUG'])
  endif else begin
      result = Hash('accept', 0b)
  endelse
  return, result 
end


;+
; :hidden:
;-
pro test_hubApp_randomSampling_getParameters 
 settings = hash()
 settings['title'] = 'Random Sampling'
 settings['tileLines'] = 100
 f = hubApp_randomSampling_getParameters(settings)
 if isa(f) then print, f
end