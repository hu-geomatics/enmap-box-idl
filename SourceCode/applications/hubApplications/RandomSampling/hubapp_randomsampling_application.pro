;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
;
; :Description:
;    This procedure is the main routine to perform a random sampling. It opens a widget dialog to collect 
;    the required parameters from the user and then starts the random sampling procedure implemented
;    in `hubApp_randomSampling_processing`.
;
; :Keywords:
;    GroupLeader: in, required, type=long
;       Use this keyword to specify the group leader for used widgets.
;-
pro hubApp_randomSampling_application, GroupLeader=groupLeader
  ; get global settings for this application
  settings = hubApp_getSettings()
  settings['title'] = 'Random Sampling'
  settings['groupLeader'] = groupLeader
  
  ;restore state from previous application calls to retrieve default values
  defaultValues = hub_getAppState('hubApp', 'stateHash_RandomSampling')
  
  
  settings['DEBUG'] = 0
  
  parameters = hubApp_randomSampling_getParameters(settings,defaultValues)
  if parameters['accept'] then begin
     
    ;save parameters as new session state hash
    hub_setAppState, 'hubApp', 'stateHash_RandomSampling', parameters
    
    hubApp_randomSampling_processing, parameters, GroupLeader=groupLeader
  endif
end