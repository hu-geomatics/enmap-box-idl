;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

;+
; :Description:
;    This procedure creates a random sample of an image. This is done in a tile-processing way
;    to allow sampling from very large images while having small memory capabilities only. 
;    
;    A stratification image `stratificationMask` is required to describe the areas from where a specific number
;    of samples is to choose randomly. This stratification image is usually a temporary product only, 
;    which is created during the use of `hubapp_randomsampling_getparameters.pro` that can be used to
;    parameterize this routine. By default it gets deleted after the sampling process has
;    finished. Use keyword `KeepStrataMask` to change this. 
;    
;    The non-sampled pixels of the final image in `outputSamples` will have a value equal to the data ingnore value.
;    In case of classification images this is `0` by default. For other image types this value must be defined
;    in the header file of the input image `inputImage` or explicitely using the hash-key `divOutput` (see `parameters`).
;    
;
; :Params:
;    parameters: in, required, type=hash
;      This hash contains the obligatory and optional (*) parameters to control the random sampling::
;      
;         key                | type     | description
;       ---------------------+----------+-------------------------------------------------------
;         inputImage         | string   | filepath of input image
;         outputSamples      | string   | filepath of output image
;       * outputComplement   | string   | filepath of complement of output image
;         stratificationMask | string   | filepath to strata mask
;         deleteStrataMask   | bool     | set to 1b to delete the strata mask 
;         samplesPerStratum  | long[]   | number of samples to choose from each of the 
;                            |          | strata defined as class in stratificationMask
;       * tileLines          | int      | number of tiles lines
;       * divOutput          | numeric  | data ignore value to mark non-sampled values 
;                            |          | of the sampled output image. Must be set in case 
;                            |          | the input image is not a classification file and does not provide the header taf 'data ignore value'
;       * seedChoice         | string   | defines the initial seed 
;                            |          | 'systime'  = use system time (default)
;                            |          | 'constant' = use fixed value of 1
;       ---------------------+----------+------------------------------------------------------- 
;   
; :Keywords:
;    NoShow: in, optional, type=boolean
;       Use this keyword to avoid showing GUI events, e.g. a progress bar. 
;    
;    GroupLeader: in, optional, type=long
;       Set this keyword to specify the group leader for used widgets.
;
;-
pro hubApp_randomSampling_processing, parameters, NoShow=NoShow, GroupLeader=GroupLeader
  ;check required parametersameters 
  required = ['inputImage', 'outputSamples','stratificationMask', 'samplesPerStratum', 'deleteStrataMask']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter-hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  ;for debugging only
  DEBUG = parameters.hubKeywordSet('DEBUG')
  
  imgStrataMask = hubIOImgInputImage(parameters['stratificationMask'], /Classification)
  

  nSamples = imgStrataMask.getMeta('samples')
  nLines = imgStrataMask.getMeta('lines')
  nStrata = imgStrataMask.getMeta('classes')
  strataNames = imgStrataMask.getMeta('class names')
  if n_elements(parameters['samplesPerStratum']) ne nStrata then begin
    message, 'number of random samples must be defined for each stratum'
  endif 
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  
  ;define the data ignore value of output image(s)
  if inputImage.isClassification() then begin
    divOutput = 0b 
  endif else begin
    if ~parameters.hubIsa('divOutput') then begin
      divOutput = inputImage.getMeta('data ignore value')
      if ~isa(divOutput) then message, "Data ignore value for output image required. Use parameter 'divOutput'"
    endif else begin
      divOutput = parameters['divOutput']
    endelse 
  endelse
  
  tileLines = hubIOImg_getTileLines(nSamples, inputImage.getMeta('bands')+2 $
                                  , inputImage.getMeta('data type'))
  
  ;define progess bar
  if ~keyword_set(NoShow) then begin
    refinementSteps = 0
    progressInc = fix(imgStrataMask.getMeta('samples') * imgStrataMask.getMeta('lines') / tileLines)
    progressDone = 0.                   
    progressBar = hubProgressBar(Title='Random Sampling' $
      , Info = 'Add thresholds' $
      , GroupLeader=groupLeader $
      , NoShow=noShow)
    progressBar.setRange, [progressDone, 1.]
  endif
  
  ;define the output image(s)
  outputImage = hubIOImgOutputImage(parameters['outputSamples'])
  outputImage.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation, /CopySpectralInformation
  outputImage.setMeta, 'data ignore value', divOutput
  
  if parameters.hubIsa('outputComplement') then begin
    imgComplement = hubIOImgOutputImage(parameters['outputComplement'])
    imgComplement.copyMeta, inputImage, /CopyFileType, /CopyLabelInformation, /CopySpatialInformation, /CopySpectralInformation
  endif
  
  
  ;Random Sampling Settings (RS)
  ;
  
  RS_NBINS = 2u^16-1 ;number of bins = 65535 
  useLONG = (long(nSamples) * long(nLines)) le 2l^31-2 
  ;useLong = 1b ;actually this does not work for long values & hubIncHistogram 
  if useLong then begin
    ;default: use long values (32-Bit) as random numbers
    RS_MAX = 2l^31-1
    RS_MIN = 0l
    RS_SPLIT_VALUE = -1d ;TODO: change this when hubMathIncHistogram returns Integer boundaries
    RS_DOUBLE = 0b
    RS_LONG = 1b
  endif else begin
    ;default: use double values (64-Bit) as random numbers
    RS_MAX = 1d
    RS_MIN = 0d
    RS_SPLIT_VALUE = -1d
    RS_DOUBLE = 1b
    RS_LONG = 0b
  endelse
  ;set the initial seed. this is the same for each iterative refinement loop,
  ;so do never overwrite variable 'initial_seed' !
    
  initialSeed = parameters.hubGetValue('initialSeed')
  if ~isa(initialSeed) then begin
    initialSeed = systime(1) ;use current systime as default values
    if parameters.hubIsa('seedChoice') then begin
      case parameters['seedChoice'] of
        'systime'  : initialSeed = systime(1) ;current systime
        'constant' : initialSeed = 1 ;constant
        else : message, "unknown value for parameter 'seedChoice'"
      endcase
    endif 
  endif
  
  strataOfInterest = where(parameters['samplesPerStratum'] gt 0, /NULL, nStrataOfInterest)
  if DEBUG then print, 'Strata of interest', strataOfInterest  
  
  ;IDEA: 
  ; - for each stratum we are looking for nS random sampled pixel
  ; - each non-masked pixel gets a random sampling value out of range [0,1]
  ; - using a constant seed we can ensure to assing always the same random number to a single pixel in a repeated tile loop
  ; - for each stratum there exists a "split value". This value splits the range [0, 1] into 
  ;   n pixels that have a random number below the split value and
  ;   m pixels that have a random number higher the split value
  ; - we can put all random values into a histogram 
  ; - the split value of a stratum is within the range of a histogram's bin whose cumulative counts 
  ;   are equal to nS
  ; - if this bin contains one value only, this one is our split value
  ; - since we use a histogram and do not know about the split value exactly,
  ;   we can use the right boundary of this histogram bin instead to get an alternative split value 
  ; - to avoid floating point arithmetic errors etc. we refine the histogram until we got at least
  ;   two neighored bins with a cumulative histogram equal the required number of samples. Using the
  ;   second bin's center value gives the split value 

  ;for each stratum we are looking for the threshold value that splits the range [0,1] into 
  info = replicate({binRange:[RS_MIN,RS_MAX] $   ;range of recent bin. starts with [0,1] and gets thinner (e.g. [0.1,0.3]) 
                   ,cumOffset:0l $       ;offset of cumulative values
                   ,foundSplitValue:0b $ ;flag if split value was found: 0 - not found, 1 - found
                   ,SplitValue:RS_SPLIT_VALUE $ ;the initial split value
                   ,samplesRequired:0l $ ;number of random samples to choose
                   ,samplesSelected:0l $ ;number of random samples selected (for final test)
                   ,samplesMax:-1l $     ;number of samples at maximum available in this stratum
                   ,stratumValue: -1 $   ;the value that denotes the stratum
                   ,refinementSteps:1 $  ;number of iterative refinement steps required to get the split value
                   ,incHist:OBJ_NEW() $  ;a pointer for the hubMathIncHistogram Object
                    }, n_elements(strataOfInterest))
  info[*].samplesRequired = (parameters['samplesPerStratum'])[strataOfInterest];
  info[*].stratumValue = strataOfInterest
  
  if DEBUG then print, format='(%"initial range: [%d,%d], nbins=%i")', rs_min, rs_max, rs_nbins
  ;=================================================
  ;PART I: get the treshold-values for each stratum
  ;================================================
  ;the treshold value is the random number between 0 and 1 that spli of a specific pixel, 
  ;use iterative refinement to get a histogram with a bin where
  ; 1. the number of cumulative samples is exactly equal to the number of required samples, or
  ; 2. the number of cumulative samples is higher that this number
  
  ;hRandomSequences = Hash()
  ;hRandomSequences['final'] = List()
  
  repeat begin ;repeat until the threshold was found for each class
    if isa(progressBar) then begin
      progressDone = 0
      progressBar.setRange, [0, nLines]
      progressBar.setInfo, string(format='(%"Iterative Refinement %i")', max(info[*].refinementsteps))  
      progressBar.setProgress, progressDone
    endif
    
    ;initialize histograms
    for i=0, nStrataOfInterest-1 do begin
      ;#check
      if info[i].binRange[0] ge info[i].binRange[1] then message, 'programming error: dRange=zero'
      if ~info[i].foundSplitValue then begin
         
        info[i].incHist = hubMathIncHistogram(info[i].binRange, RS_NBINS)
      end 
    endfor

    ;fill histograms with random sampled number assigned to each pixel
    seed = initialSeed
    imgStrataMask.initReader, tileLines, /slice, /TileProcessing
    
    
    ;loop over all image/stratum mask tiles
    while ~imgStrataMask.tileProcessingDone() do begin
        ;get stratum mask data
        strataLabels = imgStrataMask.getData()
        
        ;assign a random value to each pixel 
        randomscores = randomu(seed, n_elements(strataLabels), DOUBLE=RS_DOUBLE, LONG=RS_LONG)
        
        ;add the random values to the histogram of each stratum
        for i=0, nStrataOfInterest-1 do begin
          if ~info[i].foundSplitValue then begin
            stratumPixelIndices = where(strataLabels eq info[i].stratumValue, /NULL)
            if isa(stratumPixelIndices) then begin
              (info[i].incHist).addData, randomscores[stratumPixelIndices] 
            endif
          endif
        endfor  
        
        if isa(progressBar) then begin
          progressDone += tileLines
          progressBar.setProgress, progressDone
        endif
        
    endwhile    
    imgStrataMask.finishReader
    
    
    ;read histograms
    for i=0, nStrataOfInterest-1 do begin
       
      ;if split value was not already found...
      if ~info[i].foundSplitValue then begin
        
        ;First Tile -> initialize some things
        if info[i].refinementSteps eq 1 then begin 
          
          ;save the number of total samples available for each stratum of interest
          info[i].samplesMax = (info[i].incHist).getNumberOfSamples()
          
          ;#check
          if info[i].samplesMax lt info[i].samplesRequired then begin
            message, string(format='(%"not enough samples available in stratum %s")', strataNames[i]) 
          endif 
        endif
        
        ;get a short object reference...
        hist = (info[i].incHist).getResult() 
        
        ;cumulative histogram
        newCumHist = hist['cumulativeHistogram'] + info[i].cumOffset
        
        ;match 1 is true when there are two neighboured bins with cumulative sum equal the required samples 
        match1 =  where(newCumHist eq info[i].samplesRequired, /NULL)
        if ~isa(match1) or n_elements(match1) lt 2 then begin
          iSplitBin = (where(newCumHist ge info[i].samplesRequired, /NULL))[0]
          
          ;define the next range & cumulative sum offset 
          newRange   = [(hist['binStart'])[iSplitBin] $
                       ,(hist['binStart'])[iSplitBin] + 1.5d * hist['binSize'] $
                       ] 
          newCumOffset = (iSplitBin eq 0) ? info[i].cumOffset : newCumHist[iSplitBin-1]
          
          ;#checks
          if newCumOffset ge info[i].samplesRequired then message, 'programming error: newCumOffset > required samples'
          oldDiff = info[i].binRange[1] - info[i].binRange[0] 
          newDiff = newRange[1] - newRange[0]
          if oldDiff le newDiff then message, 'programming error: no change in histogram range' 
          
          
          ;set next range and cumulative sum offset
          info[i].refinementSteps++
          info[i].cumOffset = newCumOffset 
          info[i].binRange = newRange 
        endif else begin
          ;this is the split value and the right neighbor bin contains not a single count
          info[i].foundSplitValue = 1b
          info[i].splitValue = (hist['binCenter'])[match1[1]] 
          
        endelse
      endif
    endfor  
   
  endrep until product(info[*].foundSplitValue) eq 1
  
 
  inputImage.initReader, tileLines, slice=1, /TileProcessing
  imgStrataMask.initReader, tileLines, slice=1, /TileProcessing  
  outputImage.initWriter, inputImage.getWriterSettings()
  if isa(imgComplement) then begin
    imgComplement.initWriter, inputImage.getWriterSettings()
  endif
  
  if isa(progressBar) then begin
    progressDone = 0
    progressBar.setProgress, progressDone
    progressBar.setInfo, 'Write random samples'
  endif
  
  
  seed = initialSeed
  while ~imgStrataMask.tileProcessingDone() do begin
        strataLabels = imgStrataMask.getData()
        randomscores = randomu(seed, n_elements(strataLabels), DOUBLE=RS_DOUBLE, LONG=RS_LONG)
        
        values = inputImage.getData()
        sampledValues = make_array(size(values, /DIMENSIONS) $
                                  , value=divOutput $
                                  , type=size(values, /TYPE))
         
       for i=0, nStrataOfInterest-1 do begin
          iSampled = where(strataLabels eq info[i].stratumValue and $ 
                          randomscores lt info[i].SplitValue, nPixelsFound, /NULL)
          info[i].samplesSelected += nPixelsFound 
          if isa(iSampled) then begin
            sampledValues[*,iSampled] = values[*,iSampled]
          endif
       endfor  
       
       ;write the output data
       outputImage.writeData, sampledValues
        
       ;write output data complement (all not sampled and not masked pixels)
        if isa(imgComplement) then begin
          ;samples pixels are masked in the complement
          values[where(sampledValues ne divOutput, /NULL)] = divOutput
          
          ;masked pixels are masked in the complement
          iMasked = where(strataLabels eq 0, /NULL)
          if isa(iMasked) then values[*,iMasked] = divOutput
          
          ;write
          imgComplement.writeData, values
        endif
        
        if isa(progressBar) then begin
            progressDone += tileLines
            progressBar.setProgress, progressDone
        endif
       
  endwhile
  
  stratFiles = [imgStrataMask.getMeta('filename data') $
              ,imgStrataMask.getMeta('filename header')]
  imgStrataMask.cleanup
  inputImage.cleanup
  outputImage.cleanup
  if isa(imgComplement) then imgComplement.cleanup
  
  if DEBUG then begin
    print, 'initial Seed', initialseed
    print, 'Refinement Steps:', info[*].refinementSteps
    print, 'Final min:', info[*].binRange[0]
    print, 'Final max:', info[*].binRange[1]
;     headings = ['i','required','selected','steps','splitValue']
;      formats = ['(%"%i")', '(%"%i")','(%"%i")','(%"%i")','(%"%25f")'  ]
      table = hubReportTable();headings, Formats=formats)
      table.addColumn, NAME='i'
      table.addColumn, NAME='required'
      table.addColumn, NAME='selected'
      table.addColumn, NAME='steps'
      table.addColumn, NAME='splitValue'
      for i=0, n_elements(info)-1 do begin
        table.addRow, list(i, info[i].samplesRequired, info[i].samplesSelected $
          , info[i].refinementSteps, info[i].splitValue)
      endfor
    print, table.getFormatedASCII(/TRIM)
 endif 
 
 ;#check - number of sampled pixels
 if product(info[*].samplesSelected eq info[*].samplesRequired) ne 1 then begin
    message, 'Programming error: uncorrect sampling'
 endif 
   
  if parameters.hubKeywordSet('deleteStrataMask') then begin
    FILE_DELETE, stratFiles, /ALLOW_NONEXISTENT 
  endif
  if isa(progressBar) then progressBar.cleanup


end



;+
; :hidden:
;-
pro test_hubApp_randomSampling_processing
parameters = Hash()

;parameters[strataCounts:                      0                 37769                 60845 ...
parameters['samplesPerStratum'] =  [0,100]

;parameters['samplesPerStratum'] =  [0,199,199]
;parameters['samplesPerStratum'] =  [0, 6791289, 2266358]
;parameters['samplesPerStratum'] =  [0, 1, 1]

;parameters['stratificationMask'] = hub_getTestImage('Hymap_Berlin-A_Image')
parameters['inputImage']      = hub_getTestImage('Hymap_Berlin-A_Image')
parameters['outputSamples'] = FILEPATH('samples', /TMP)
;parameters['seedChoice'] = 'systime'
;inputImgRange:   0.000125000     0.976050
;inputImgType: ENVI Standard
;sampleSizePercent:        50.000000
;strataNames: ignored/masked/unclassified soils & manmade water coniferous forest meadow ...
;parameters['divOutput'] =  -99.000000
parameters['tileLines'] = 1000
parameters['initialSeed'] = systime(1)
parameters['deleteStrataMask'] = 0b
parameters['DEBUG'] = 0b
;report = hubapp_imageStatistics_getReport(stats)
;report.saveHTML, /Show

; # |   class name |   counts | cumCounts | probDens | cumDistr
;==============================================================
; 0 | Unclassified | 13844095 |  13844095 |     0.60 |     0.60
; 1 |          For |  6791289 |  20635384 |     0.30 |     0.90
; 2 |          Def |  2266358 |  22901742 |     0.10 |     1.00

t1 = systime(1)
trials = 3
times = make_array(trials+1, value=0, /DOUBLE)
times[0] = systime(1)
for i=1, n_elements(times)-1 do begin
  hubApp_randomSampling_processing, parameters, NoShow=NoShow, GroupLeader=GroupLeader
  times[i] = systime(1)
endfor

for i=1, n_elements(times)-1 do begin
  print, format='(%"T%2i: %f")',i, times[i]-times[i-1]
endfor


end


