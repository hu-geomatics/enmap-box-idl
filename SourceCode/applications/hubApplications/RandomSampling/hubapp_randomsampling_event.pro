;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    Use this event handler to call the hubApp_randomSampling_application from an EnMAP-Box Menu button.
;
; :Params:
;    event: in, required, type=button event structure
;
;
;-
pro hubApp_randomSampling_event, event
  @huberrorcatch
  hubApp_randomSampling_Application, GroupLeader=event.top
end