;+
; :Description:
;    Callback check function for `hubApp_reClassify_getParameters_Dialog1b`. 
;-
function hubApp_reClassify_getParameters_specifyClassesCheck, resultHash, Message=message, UserInformation=userInformation
  msg = !NULL
  isConsistent = 1
  
  
  classTitles = UserInformation['parNameTitles']
  parNameKeys = UserInformation['parNameKeys']
  classNames = sindgen(n_elements(parNameKeys))
  classNameOccurence = bindgen(n_elements(parNameKeys))
  foreach parName, parNameKeys, i do begin
    className = resultHash[parName]
    classNames[i] = className
    !NULL = where(classNames eq className, cnt)
    classNameOccurence[i] = cnt
  endforeach
  
  if total(stregex(classNames, ',',/BOOLEAN)) ne 0 then begin
    msg = [msg, 'Class names are not allowed to contain a comma "," ']
    isConsistent *= 0b
  endif 
  
  
  iRedundant = where(classNameOccurence gt 1, /NULL)
  if isa(iRedundant) then begin
    msg = [msg, 'New classnames must be unique']
    s = classTitles[iRedundant]+' "'+classNames[iRedundant]+'"'
    foreach i, iRedundant do begin
      msg = [msg, string(format='(%"  %s \"%s\"")',classTitles[i], classNames[i])]
    endforeach
    isConsistent *= 0b 
  endif 
  
  message = msg
  return, isConsistent
end

;+
; :Description:
;    This dialog is used to specify class names and class colors of a new classification.
;
; :Params:
;    settings: in,  required, type=hash
;     The settings hash. Must contain the key `title`.
;     
;    hubHelper: in, required, type=hubHelper()
;     The hubHelper object, as defined in `hubApp_reClassify_getParameters`.
;     
;    nClasses: in, required, type=integer
;     The number of (non-unclassified) classe you want to get names and colors for.
;
; :Keywords:
;    seed: in, optional, type=numeric
;     Use this to set a constant seed that is used to generate default class colors randomly. 
;    
;    defaultValues: in, optional, type=Hash
;     Use this to provide default values for ´class lookup´ or ´class names´.
;     If they are defined for more than nClasses, only the values for nClasses will be used.
;     
; :Author: geo_beja
;-
function hubApp_reClassify_getParameters_specifyClasses, settings, hubHelper, nClasses, seed=seed, defaultValues=defaultValues
  default = isa(defaultValues)? defaultValues : Hash() ;empty hash 
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_frame, TITLE='Specify Classes'
  
  parPrefixes = string(format='(%"class%2.2i")', indgen(nClasses+1))
  parNameKeys=parPrefixes+'name'
  parNameTitles = string(format='(%"Class %-2i")', indgen(nClasses+1))
  parNameTitles[0] = 'Unclassified'
  parNameValues = make_array(nClasses+1, value='', /STRING) 
  parNameValues[0] = 'unclassified'
  
  parColorKeys=parPrefixes+'color'
  if ~keyword_set(seed) then seed = systime(1)
  parColorValues=bytscl(randomu(seed, 3, nClasses+1))
  parColorValues[*,0]= 0
  
  ;use default values if defined
  if default.hubIsa('class lookup') then begin
    n = nClasses < n_elements((default['class lookup'])[0,*])
    for i=0, n-1 do begin
      parColorValues[*, i] = (default['class lookup'])[*,i]
    endfor
  endif
  
  if default.hubIsa('class names') then begin
    n = nClasses < n_elements((default['class names'])[0,*])
    for i=0, n-1 do begin
      parColorValues[*, i] = (default['class names'])[*,i]
    endfor
  endif
  

  ;right-aligned class titles
  sizeNameTitles = max(strlen(string(parNameTitles[*])))
  sizeNameValueFields = 25
  str1 = strtrim(sizeNameTitles,2)
  str2 = strtrim(sizeNameValueFields,2)
  parNameTitles=string(format='(%"%'+str1+'s")', parNameTitles)
  hubAMW_label, string(format='(%"%'+str1+'s %-'+str2+'s   Color    ")',' ', 'Name')
  for i=0, n_elements(parPrefixes) - 1 do begin
    hubAMW_subframe, /ROW
    hubAMW_parameter, parNameKeys[i], Title=parNameTitles[i], value=parNameValues[i] $
                    , size=sizeNameValueFields, /STRING 
    hubAMW_color, parColorKeys[i], TITLE='',VALUE=parColorValues[*,i]
  endfor
  
  result = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_reClassify_getParameters_specifyClassesCheck' $
                        ,USERINFORMATION=Hash('parNameKeys',parNameKeys ,'parNameTitles',parNameTitles))
  if result['accept'] then begin
    for i=0, n_elements(parPrefixes)-1 do begin
      parNameValues[i]  = result[parNameKeys[i]]
      parColorValues[*,i] = result[parColorKeys[i]]
    endfor 
    result.hubRemove, parNameKeys
    result.hubRemove, parColorKeys
    result['classes'] = nClasses+1
    result['class names'] = parNameValues
    result['class lookup'] = parColorValues
  endif

  return, result
end

;+
; :hidden:
;-
pro test_hubApp_reClassify_getParameters_specifyClasses
  settings = hubApp_getSettings()
  settings['title'] = 'ReClassify'
  seed= systime(1)
  print, seed
  print, hubApp_reClassify_getParameters_specifyClasses(settings, hubHelper(), 4, seed=seed)
  
end