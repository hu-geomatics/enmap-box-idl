;+
; :Author: Benjamin Jakimow
;-


function hubApp_reClassify_getParameters_nameMatchIndex, names, matchWords
  maxMatches = n_elements(matchWords)
  matches = 0.
  foreach n, names do begin
    foreach w, matchWords do begin
      matches += total(STRCMP(n,w, min(strlen([w,n])), /FOLD_CASE))
    endforeach
  endforeach
  return, matches / maxMatches
end

pro test_hubApp_reClassify_getParameters_nameMatchIndex
  names = ['arb','arb']
  matchWords = ['arb','arb']
  print, hubApp_reClassify_getParameters_nameMatchIndex(names, matchWords)
end

;+
; :Description:
;    Callback check function for `hubApp_reClassify_getParameters_Dialog2`.
;-
function hubApp_reClassify_getParameters_Dialog2Check $
        , resultHash, Message=message, UserInformation=userInformation
   msg = !NULL
   isConsistent = 1
   parameterKeys = userInformation['parameterKeys']
   
   allTheSame = 1b
   noClassChange = 1b
   keyvalue1 = resultHash[parameterKeys[0]]
   for i=0, n_elements(parameterKeys)-1 do begin
      allTheSame *= resultHash[parameterKeys[i]] eq keyvalue1
      noClassChange *= resultHash[parameterKeys[i]] eq i
   endfor
   if allTheSame then begin
      msg = [msg, 'Need at least two different output classes']
      isConsistent *= 0b
   endif
;   if noClassChange then begin
;      msg = [msg, 'No difference compared to input classification']
;      isConsistent *= 0b
;   endif
   message = msg
   return, isConsistent
end

;+
; :Description:
;    This dialog is used to specify which class in classification A should get expressed as
;    a class from classification B. 
;    It returns a hash with key `mapArray`. This array contains the class indices to choose in the 
;    targeted classification, e.g. as followed::
;      mapArray[0] eq 0  -> map class 0 (unclassified) to class 
;      mapArray[1] eq 7  -> map class 1 to class 7
;     
;    or more generally spoken::
;      mapArray[n] eq m  -> map class n of the source classification to class m in the final classification 
;
; :Params:
;    settings: in, required, type=hash
;      Overall Settings from `hubApp_getSettings`.
;      
;    hubHelper: in, required, type=hubHelper()
;      Helper object defined in `hubApp_reClassify_getParameters`.
;      
;    parameters: in, required, type=hash
;      Parameter hash a modified in `hubApp_reClassify_getParameters`.
;      Contains the information returned from `hubApp_reClassify_getParameters_Dialog1` and
;      , if required, `hubApp_reClassify_getParameters_Dialog1b`.
;
;-
function hubApp_reClassify_getParameters_Dialog2, settings, hubHelper, parameters
  
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
      inputClassification  = hubIOImgInputImage(parameters['inputClassification'])
      inputClasses    = inputClassification.getMeta('classes')
      inputClassNames = inputClassification.getMeta('class names')
      
      inputClassification.cleanup    
      
      
      ;array with lookup table. Index 0 -> class 0 is mapped to array[0] etc.
      mapArray = bindgen(inputClasses)
      ;parameter names to read the map value 
      parameterKeys  = 'class'+strtrim(sindgen(inputClasses),2)
      
    hubAMW_frame, Title='Specify Class Mapping'
    strSource = 'From'
    strTarget = 'To'
    maxLength = strtrim(max([strlen(inputClassNames),strlen(strSource)]),2)
      
    targetClassNames = parameters['targetClassNames']
    splitRegex = '[ ,;]'
    targetClassNameWords = strsplit(targetClassNames, splitRegex, /REGEX,/EXTRACT)
    inputClassNameWords = strsplit(inputClassNames, splitRegex, /REGEX,/EXTRACT)
    
    hubAMW_label, string(format=             '(%"%-' + maxLength + 's    %s")', strSource, strTarget)
    for i=0, inputClasses - 1 do begin
      hubAMW_subframe, /ROW 
      
      matchIndex = make_array(n_elements(targetClassNames))
      for iMatch = 0, n_elements(targetClassNames) - 1 do begin
        matchIndex[iMatch] = hubApp_reClassify_getParameters_nameMatchIndex(inputClassNameWords[i], targetClassNameWords[imatch])
      endfor
      maxMatchIndex = max(matchIndex, value)
      if maxMatchIndex eq 0 then value = 0
      hubAMW_combobox, parameterKeys[i] $
                     , title = string(format='(%"%-' + maxLength + 's ->")', inputClassNames[i]) $
                     , List  = targetClassNames $
                     , value = value
                      
    endfor
    
    hubAMW_frame , Title = 'Output'
    hubAMW_outputFilename, 'outputClassification', TITLE='Classification', VALUE='reclassified' 
                       
    result = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_reClassify_getParameters_Dialog2Check' $
                          , USERINFORMATION=Hash('parameterKeys',parameterKeys))
    
    if result['accept'] then begin
      for i=0, inputClasses - 1 do begin
        mapArray[i] = result[parameterKeys[i]]
      endfor
      result['mapArray'] = mapArray
      result.hubRemove, parameterKeys
      
    endif 
    return, result
end



;+
; :Description:
;    Consistency Check for `hubApp_reClassify_getParameters_Dialog1`.  
;
;-
function hubApp_reClassify_getParameters_Dialog1Check, resultHash, Message=message
   
   msg = !NULL
   isConsistent = 1
   
   inputClassification = hubIOImgInputImage((resultHash['inputSampleSet'])['featureFilename'])
   if ~inputClassification.isClassification() then begin
          isConsistent = isConsistent and 0
          msg = [msg, 'Input File is not of file type "ENVI Classification"']
   endif
   
   if resultHash['classificationType'] eq 0 then begin
      targetClassification = hubIOImgInputImage(resultHash['targetClassificationFile'])
      if ~targetClassification.isClassification() then begin
          isConsistent = isConsistent and 0
          msg = [msg, 'Target File is not of file type "ENVI Classification"']
      endif
      targetClassification.cleanup
   endif else begin
    ;check new target classification settings
   
   endelse
   inputClassification.cleanup
   
   message = msg
   return, isConsistent
 
end


;+
; :Description:
;    First AMW based dialog to collect basci input parameters.
;    Returns a hash containing the following keys::
;    
;       Key                       | type    | description
;       --------------------------+---------+----------------------------------
;       inputSampleSet            | hash    | input sample set with input classification and 
;                                 |         | optional mask file
;       classificationType        | byte    | 0 = use existing classification
;                                 |         | 1 = create new classification
;       targetClassificationFiles | string  | filepath of an existing classification 
;                                 |         | is required when classificationType = 0
;       nNewClasses               | integer | number of new classes
;                                 |         | is required when classificationType = 1
;       --------------------------+---------+----------------------------------- 
;       
;    
; :Params:
;    settings: in, required, type=hash
;      Overall Settings from `hubApp_getSettings`.
;      
;    hubHelper: in, required, type=hubHelper()
;      Helper object defined in `hubApp_reClassify_getParameters`. 
;      
;-
function hubApp_reClassify_getParameters_Dialog1, settings, hubHelper
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
  
  tsize=101
  hubAMW_frame, title='Input'
  hubAMW_inputSampleSet, 'inputSampleSet', TITLE='Classification', /MASKING $
                       , REFERENCETITLE='Mask', /REFERENCEOPTIONAL $
                       , TSIZE=tsize

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Target Classification'
  hubAMW_subframe, 'classificationType', Title='Use existing classification', /COLUMN, /SETBUTTON
  hubAMW_inputImageFilename, 'targetClassificationFile' $
                           , TITLE = 'Target Classification' $
                           , SIZE = 451
    
  hubAMW_subframe, 'classificationType', Title='Create new classification', /COLUMN
  hubAMW_parameter, 'nNewClasses', title='Classes', IsGT=0, IsLT=256, /INTEGER, SIZE=3
 
  return, hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_reClassify_getParameters_Dialog1Check')
end



;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function hubApp_reClassify_getParameters, settings
  ;DEBUG SETTINGS
  if 1 then begin
    DEBUG_OUTPUTFILE = 'D:\Sandbox\temp\test'
  endif
  
  groupLeader = settings.hubGetValue('groupLeader')
  hubHelper = hubHelper()
  CANCELED = Hash('accept', 0b)
 
  
  results1 = hubApp_reClassify_getParameters_Dialog1(settings, hubHelper)
  
  if ~results1.hasKey('accept') then stop
  if ~results1['accept'] then return, CANCELED
  ;1. get classificatin file info
  parameters = Hash('accept', 1b)
  parameters['inputClassification'] = (results1['inputSampleSet'])['featureFilename']
  parameters['inputMask'] = (results1['inputSampleSet'])['labelFilename']
  
      
  ;2. get target classification info
  if results1['classificationType'] eq 0 then begin
    ;2.1. use classification from exising file
    targetClassificationFile = hubIOImgInputImage(results1['targetClassificationFile'])
    parameters['targetClasses']    = targetClassificationFile.getMeta('classes')
    parameters['targetClassLookup']= targetClassificationFile.getMeta('class lookup')
    parameters['targetClassNames'] = targetClassificationFile.getMeta('class names')
    targetClassificationFile.cleanup
    
  endif else begin
    ;2.2. use new defined classification
    results1b = hubApp_reClassify_getParameters_specifyClasses(settings, hubhelper, results1["nNewClasses"])
    if ~results1b['accept'] then return, CANCELED
    parameters['targetClasses'] = results1b['classes']
    parameters['targetClassLookup'] = results1b['class lookup']
    parameters['targetClassNames'] = results1b['class names']  
  endelse
      
  ;open dialog 2 -> specify mapping from input to target classification
  results2 = hubApp_reClassify_getParameters_Dialog2(settings, hubHelper, parameters)
  if ~results2['accept'] then return, CANCELED
  parameters['mapArray'] = results2['mapArray']
  parameters['outputClassification'] = results2['outputClassification']
  return, parameters
end


;+
; :Hidden:
;-
pro test_hubApp_reClassify_getParameters

  ; test your routine
  settings = hubApp_getSettings()
  settings['title'] = 'Re-Classify'
  parameters = hubApp_reClassify_getParameters(settings)
  print, parameters

end  
