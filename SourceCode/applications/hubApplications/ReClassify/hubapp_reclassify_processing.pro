;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-


;+
; :Description:
;   This routine is used to convert an input classification by
;   using the class names, (binary) class values and class colors of an target classification.
;   
;   
;   
; :Returns:
;   Nothing. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;       Use the following hash-keys (n = number of classe in target classification)::
;         key                   | type     | description
;         ----------------------+----------+------------------
;         inputClassification   | string   | path of input classification file
;         outputClassification  | string   | path of output classification file
;         mapArray              | int[]    | array that shows which class of input classification 
;                               |          | belong to which class in the target classification
;         targetClassification  | string   | path of target classificaiton file             
;         targetClassNames      | string[] | class names of target classification
;         targetClassLookup     | byte[3,n]| array with RGB values for each class in the target classification
;         ----------------------+----------+------------------
;      It is required to specified either `targetClassification` or `targetClassNames` and `targetClassLookup`
;      In all cases max(mapArray) must be in range [0, n-1]  
;      
;    settings : in, optional, type=hash
;      The settings hash as defined in `hubApp_reClassify_application`. 
;-
pro hubApp_reClassify_processing, parameters, settings

  ; check required parameters
  
  required = ['inputClassification','mapArray','outputClassification']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  if ~(parameters.hubIsa(['targetClassification']) or $
       parameters.hubIsa(['targetClasses', 'targetClassNames','targetClassLookup'])) then begin
    message, 'incomplete parameter hash. need key "targetClassification" or ' $
           + 'keys "targetClasses", "targetClassNames" and "targetClassLookup"'    
    
  endif
  
  
  
  
  
  ;define input files
  inputClassification = hubIOImgInputImage(parameters['inputClassification'], /Classification)
  inputClasses = inputClassification.getMeta('classes')
  if parameters.hubIsa('inputMask') then begin
    inputMask = hubIOImgInputImage(parameters['inputMask'], /Mask)
  endif 
  
  tileLines = hubIOImg_getTileLines(image = inputClassification)
  
  ;define progress bar
  if ~settings.hubKeywordSet('noShow') then begin
    progressBar = hubProgressBar(TITLE = settings.hubGetValue('title') $
                                , groupLeader = settings.hubGetValue('groupLeader') $
                                , info = 'write new classifcation...')
    progressBar.setRange, [0,inputClassification.getMeta('lines')] 
    progressDone = 0l
  endif
  
  mapArray = parameters['mapArray']
  if ~parameters.hubIsa('targetClassification') then begin
    targetClasses     = n_elements(parameters['targetClassNames'])
    targetClassNames  = parameters['targetClassNames']
    targetClassLookup = parameters['targetClassLookup']
  endif else begin
    targetClassification = hubIOImgInputImage(parameters['targetClassification'], /Classification)
    targetClasses     = targetClassification.getMeta('classes')
    targetClassNames  = targetClassification.getMeta('class names')
    targetClassLookup = targetClassification.getMeta('class lookup')
  endelse
  
  ;define output file
  outputClassification = hubIOImgOutputImage(parameters['outputClassification'])
  outputClassification.copyMeta, inputClassification, /COPYFILETYPE, /COPYSPATIALINFORMATION 
  outputClassification.setMeta, 'classes', targetClasses
  outputClassification.setMeta, 'class names', targetClassNames
  outputClassification.setMeta, 'class lookup', targetClassLookup
  
  ;init readers and writers
  inputClassification.initReader, tileLines, /Slice, /TILEPROCESSING
  outputClassification.initWriter, inputClassification.getWriterSettings()
  if isa(inputMask) then begin
    inputMask.initReader, tileLines, /Slice, /TILEPROCESSING, /Mask
  endif
  
  ;do the tile processing
  while ~inputClassification.TileProcessingDone() do begin
    data = inputClassification.getData()
    mappedData = data * 0b
    
    for classValue = 0, n_elements(parameters['mapArray'])-1 do begin
      mappedData[where(data eq classValue, /NULL)] = (parameters['mapArray'])[classValue]
    endfor
    
    if isa(inputMask) then begin
      maskedData = inputMask.getData()
      mappedData[where(maskedData eq 0, /NULL)] = 0
    endif
    
    outputClassification.writeData, mappedData
    
    if isa(progressBar) then begin
      progressDone += tileLines
      progressBar.setProgress, progressDone
    endif 
  endwhile
  
  ;cleanup objects
  inputClassification.cleanup
  if isa(inputMask) then inputMask.cleanup
  outputClassification.cleanup
  if isa(progressBar) then progressBar.cleanup
end

;+
; :Hidden:
;-
pro test_hubApp_reClassify_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = hubApp_getSettings()
  settings['title'] = 'ReClassify'
  parameters['inputMask']='D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Mask'
  parameters['inputClassification'] = 'D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Classification-Estimation'
  parameters['targetClasses']= 6
  parameters['targetClassNames']= ['Unclassified','vegetation','built-up','impervious','pervious','water']
  parameters['targetClassification']='D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Classification-GroundTruth'
  parameters['mapArray']= [0,   2,   2,   1,   1,   0]
  parameters['accept']= 1
  parameters['outputClassification'] = 'D:\Sandbox\temp\test'
  
  hubApp_reClassify_processing, parameters, settings
  print, 'test done#'

end  
