;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;-

;+
; :Description:
;    This application can be used to change a classification images classification scheme into a
;    scheme from an existing classification file or a new classification.
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubApp_reClassify_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  settings['title'] = 'Reclassify Image'
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = hubApp_reClassify_getParameters(settings)
  
  if parameters['accept'] then begin
  
    hubApp_reClassify_processing, parameters, settings
  
  endif
  
end

;+
; :Hidden:
;-
pro test_hubApp_reClassify_application
  applicationInfo = Hash()
  hubApp_reClassify_application, applicationInfo

end  
