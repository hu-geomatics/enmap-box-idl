;+
; :hidden:
;-
;+
; :hidden:
;-
function hubApp_getDirname, SourceCode=sourceCode
  
  return, filepath('hubApplications', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  
end

;+
; :hidden:
;
;
;
;
;-
pro test_hubApp_getDirname

  print, hubApp_getDirname()

end

