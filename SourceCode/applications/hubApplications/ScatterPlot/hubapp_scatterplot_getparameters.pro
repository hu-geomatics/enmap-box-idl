;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; 
; :Description:
;   A GUI to visualize scatter / density plots.
;   
;   Last modifications:
;       
;       2015-06-02: fixed an error with IDL's internal histogram function
;                   The first two bands of X and Y image are now selected by default
;                   
;                   
;   Future modification / wishlist:
;       
;       - a button to swap axes / images
;       
;       - more precice scale
;       
;       - choice to show scatter plot or 2D histogram (required direct linkage to read the image)
; 
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2015.
;-




;+
; :Description:
;    Consistency check function called by `hubAMW_manage` in `hubApp_scatterPlot_getParameters_Dialog1`.
;
; :Params:
;    resultHash: in, required, type=hash
;      
; :Keywords:
;    Message: out, required, type=string[]
;       String-Array with error messages in case the function returns false.
;
;-
function hubApp_scatterPlot_getParameters_Dialog1Check, resultHash, Message=message
   
   msg = !NULL
   isConsistent = 1b
   
   image1 = hubIOImgInputImage(resultHash['image1'])
   image2 = hubIOImgInputImage(resultHash['image2'])
   
   spatialSize = image1.getSpatialSize()
   if ~image2.isCorrectSpatialSize(spatialSize) then begin
    msg = [msg, 'Images do not have same spatial size']
    isconsistent *= 0b
   endif
   
   if resultHash.hubIsa('mask') then begin
      maskImage = hubIOImgInputImage(resultHash['mask'])
      if ~maskImage.isCorrectSpatialSize(spatialSize) then begin
        msg = [msg, 'Mask has different spatial size']
        isconsistent *= 0b
      endif
      if ~maskImage.isMask() then begin
        msg = [msg, string(format='(%"\"%s\" can not be used as mask.")' $
                          , file_basename(resultHash['mask']))]
        isconsistent *= 0b
      endif
   endif
   
   if resultHash.hubIsa('stratification') then begin
      statificationImage = hubIOImgInputImage(resultHash['stratification'])
      if ~statificationImage.isCorrectSpatialSize(spatialSize) then begin
        msg = [msg, 'Stratification image has different spatial size']
        isconsistent *= 0b
      endif
      if ~statificationImage.isClassification() then begin
        msg = [msg, string(format='(%"\"%s\" is not a valid classification image.")' $
                          , file_basename(resultHash['stratification']))]
        isconsistent *= 0b
      endif
   endif
   
   message = msg
   return, isConsistent
end
 
 


;+
; :Description:
;    This dialog is used to collect the images that are required to draw a scatter plot.
;
; :Params:
;    settings
;
;
;
;-
function hubApp_scatterPlot_getParameters_Dialog1, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title']
    ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Input'
  tsize = 125
  hubAMW_inputImageFilename, 'image1', TITLE='Image/Band 1 (X)', value=settings.hubGetValue('image1'), tsize=tsize
  hubAMW_inputImageFilename, 'image2', TITLE='Image/Band 2 (Y)', value=settings.hubGetValue('image2'), tsize=tsize
  hubAMW_inputImageFilename, 'mask', TITLE='Mask', OPTIONAL=2, value=settings.hubGetValue('mask'), tsize=tsize
  hubAMW_inputImageFilename, 'stratification', TITLE='Stratification', OPTIONAL=2, value=settings.hubGetValue('stratification'), tsize=tsize
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_scatterPlot_getParameters_Dialog1Check')
  return, parameters
end

function hubApp_scatterPlot_getParameters_Dialog2CreateInitialScreen, dimensions
  
  w = window(Buffer=1, dimensions=dimensions)
  t1 = TEXT(0.5, 0.5, /NORMAL $
        , ALIGNMENT = 0.5 $
        , 'Please select a band combination!Cand press "Draw!"' $
        , FONT_SIZE = 20 $
        , FONT_COLOR = [125,125,125] $
        , current=w)
  return, w.CopyWindow(resolution=300)
end

pro hubApp_scatterPlot_getParameters_Dialog2ShowPlotAsReport, resultHash, userInformation=userInformation
  lastPlotInfo = userInformation['lastPlotInfo']
  if ~isa(lastPlotInfo) then begin
    !NULL = DIALOG_MESSAGE('Please draw a plot first', /INFORMATION)
  endif else begin
      report = hubReport(Title='Scatter Plot')
      report.addHeading, 'Files'
      info = list()
      info.add, 'Image1 (X-Axis) :'+userInformation['image1']
      info.add, 'Image2 (Y-Axis) :'+userInformation['image2']
      if userInformation.hubIsa('mask') then begin
        info.add, 'Mask           :'+userInformation['mask']
      endif
      if userInformation.hubIsa('stratification') then begin
        info.add, 'Stratification :'+userInformation['stratification']
      endif
      report.addMonospace, info.toArray()
      report.addHeading, 'Scatter Plot'
     
      report.addImage, lastPlotInfo.rgb
      subtitles = lastPlotInfo.subtitles
      if n_elements(subtitles) gt 0 then begin
        report.addMonospace, subtitles.toArray()
      endif
      report.saveHTML, /Show
  
  endelse 

end


;+
; :Private:
; :Hidden:
;   
;-
pro hubApp_scatterPlot_getParameters_Dialog2PlotToClipboard, resultHash, userInformation=userInformation

  

  lastPlotInfo = userInformation['lastPlotInfo']
  if ~isa(lastPlotInfo) then begin
    !NULL = DIALOG_MESSAGE('Please draw a plot first', /INFORMATION)
  endif else begin
    
    
    imageData = lastPlotInfo.rgb
    imageSize = (size(/DIMENSIONS, imageData))[0:1]
    
    ; create object graphic hierarchy
    oImage = IDLgrImage(imageData, INTERLEAVE=2)
    oView = IDLgrView(VIEWPLANE_RECT=[0,0,imageSize])
    oModel = IDLgrModel()
    oView.add, oModel
    oModel.add, oImage
    
    ; draw to clipboard
    oClipboard = IDLgrClipboard(DIMENSIONS=imageSize)
    oClipboard.draw, oView
    obj_destroy, oClipboard
  endelse

end



;+
; :Private:
; :Hidden:
;    This routine is not used any more. Maybee in future again
;-
pro hubApp_scatterPlot_getParameters_Dialog2AddToPlotList, resultHash, userInformation=userInformation
  
  ;add the plot to the plot info list 
  plotInfoList = userInformation['plotInfoList']
  lastPlotInfo = userInformation['lastPlotInfo']
  
  if ~isa(lastPlotInfo) then begin
    !NULL = DIALOG_MESSAGE('Please draw a plot first', /INFORMATION)
  endif 
  
  if ~plotInfoList.HasKey(lastPlotInfo.id) then begin
    lastPlotInfo.nAdded = n_elements(plotInfoList)
    plotInfoList[lastPlotInfo.id] = lastPlotInfo
    nPlots = n_elements(plotInfoList)
  endif
end

;+
; :Description:
;    This routine draws a scatterplot. It is called by the `Draw Band Combination` button in `hubApp_scatterPlot_getParameters_Dialog2`
;    and calls the `hubApp_scatterPlot_processing` function to a 2D histogram. This histogram is used as
;    scatter plot.
; 
; :Params:
;    resultHash:in, required, type=Hash()
;     Contains the hash-keys specified as 'RESULTHASHKEYS' at the 'hubAMW_button' definition.
;
; :Keywords:
;    userInformation: in, required, type=hash()
;     Contains user defined hash values that are not retrieved from one of the hubAMW widgets.
;
;-
pro hubApp_scatterPlot_getParameters_Dialog2DrawEvent, resultHash, userInformation=userInformation
  
  parameters = Hash()
  parameters = parameters + resultHash.hubGetSubHash(['bandIndex1','bandIndex2','nBins1','nBins2','stratumIndices'])
  parameters = parameters + userInformation.hubGetSubhash(['image1','image2','mask','stratification','bandNames1','bandNames2'])  
  
  iBand1 = resultHash['bandIndex1']
  iBand2 = resultHash['bandIndex2']
  bandName1 = (parameters['bandNames1'])[iBand1]
  bandName2 = (parameters['bandNames2'])[iBand2]
  nBins1 = resultHash['nBins1']
  nBins2 = resultHash['nBins2']
  fileName1 = FILE_BASENAME(parameters.hubGetValue('image1'))
  fileName2 = FILE_BASENAME(parameters.hubGetValue('image2'))
  
  results = hubApp_scatterPlot_processing(parameters, userInformation['settings'] )
  
  title = string(format='(%"Band %i vs. Band %i")', iBand1+1,iBand2+1)
  
  subtitles = list()
  if ~results['hasSamples'] then begin
      subtitles.add, 'No Pixels found' 
  endif
  if results.hubIsa('mask') then begin
      subtitles.add, 'Mask: '+FILE_BASENAME(results['mask'])
  endif  
  
  stratumIndices = []
  if results.hubIsa('stratification') then begin
     subtitles.add, 'Stratification: '+FILE_BASENAME(results['stratification'])
      stratumIndices = resultHash.hubGetValue('stratumIndices')
      nStrataIndices = n_elements(stratumIndices)
      stratumString = 'Selected Stratum'
      if nStrataIndices le 3 then begin
        strataNames = (userInformation['strataNames'])[stratumIndices]
        stratumString += ': ' + strjoin(strtrim(strataNames,2),', ')
      endif else begin
        stratumString += ' Indices: ' + strjoin(strtrim(stratumIndices,2),',')
      endelse 
     subtitles.add,stratumString 
  endif

  w = resultHash['drawWindow']
  ;clear window
  w.erase
  w.title = title

  
  if results['hasSamples'] then begin
    hist = hist_equal(results['histogram2D']) 
    idx_zero = where(hist eq 0, /NULL)
    red   = (userInformation['R'])[hist]
    green = (userInformation['G'])[hist] 
    blue  = (userInformation['B'])[hist] 
    red[idx_zero] = 255 & green[idx_zero] = 255 & blue[idx_zero] = 255
    rgb = [[[temporary(red)]],[[temporary(green)]],[[temporary(blue)]]]
    
    ;define the color bar image
    steps = min([nBins1 * nBins2, 255]) 
    steps = 24
    ;count values to show
    cBarValues = ul64indgen(steps+1) * (double(max(results['histogram2D'])) / steps)
    cBarValueRange = [0, max(cBarValues)]
    cBarValues2D = transpose(hist_equal(cBarValues))
    cBarValues2D = [cBarValues2D,cBarValues2D]
    
    ;the RGB image for the color bar
    cBarRGB = make_array(2, steps+1, 3, value=0b)
    cBarRGB[*,*,0] = (userInformation['R'])[cBarValues2D]
    cBarRGB[*,*,1] = (userInformation['G'])[cBarValues2D]
    cBarRGB[*,*,2] = (userInformation['B'])[cBarValues2D]
    cBarRGB[*,0,*] = 255 ;no counts -> white
    
    if 1 then begin
      ;vertical color bar
      positionImagePlot = [0.25, 0.1, 0.95, 0.80]
      positionColorBar =  [0.10, 0.1, 0.15, 0.80]
    endif else begin
      ;horizontal color bar
      positionImagePlot = [0.38, 0.2, 0.98, 0.8]
      positionColorBar =  [0.15, 0.2, 0.23, 0.8]
      cBarRGB = transpose(cBarRGB, [1,0,2]) 
    endelse
    
    ;no image size of dim=3 possible
    
    XRANGE = results['rangeX'] 
    YRANGE = results['rangeY'] 
    
    
    ;subtitles.add, string(format='(%"1: [%0.2f, %0.2f], StdDev , Mean")', XRANGE[0], XRANGE[1])
    ;rgb = byte(rgb*0.75)
    XTICKVALUES = !NULL
    YTICKVALUES = !NULL
    XMINOR = -1
    YMINOR = -1
    if nBins1 lt 25 then begin
     ; XTICKVALUES = results['binCenterX']   
      XMINOR = 0
    endif
    if nBins2 lt 25 then begin
     ; YTICKVALUES = results['binCenterY']
      YMINOR = 0
    endif

    
    img = image(rgb, results['binCenterX'], results['binCenterY'] $
                  , AXIS_STYLE=2 $
                  , RGB_TABLE=39, FONT_SIZE=9 $
                  , CURRENT=w $
                  , BACKGROUND_COLOR='white' $
                  , position=positionImagePlot $
                  , ASPECT_RATIO=0 $
                  , XTICKVALUES =xtickvalues, XMINOR=XMINOR $
                  , YTICKVALUES =ytickvalues, YMINOR=yMINOR $
                  ;, XRANGE = xrange $
                  ;, YRANGE = yrange $
                  , XSTYLE=3, YSTYLE=3 $
                  , XTITLE = string(format = '(%"%s \"%s\" (%i) ")', fileName1, bandName1, iBand1+1) $
                  , YTITLE = string(format = '(%"%s \"%s\" (%i) ")', fileName2, bandName2, iBand2+1) $
                  ;, image_location = [Xrange[0], YRange[0]] $
                  ;, image_dimensions = [Xrange[1]-XRange[0], YRange[1] - YRange[0]] $
                  )
                  
     !NULL = image(cBarRGB, current=w, POSITION=positionColorBar $
                  ;, IRREGULAR=1 $
                  , AXIS_STYLE=1 $
                  , YRANGE=cbarvaluerange $
                  , XMAJOR=0, XMINOR=1, XTHICK=0 $
                  , title='Counts' $
                  , YSTYLE=0 $
                  , ASPECT_RATIO=0 $
                  , IMAGE_DIMENSIONS=[10,max(cBarValueRange)] $
                 ; , image_location = [0,0] $
                 ; , image_dimensions = [1,max(cBarValueRange)] $
                  )


  endif 
  
  if n_elements(subtitles) gt 0 then begin
  !NULL = Text( .1, .87, strjoin(subtitles.toArray(),"!C") $
    , alignment=0, /Normal, FONT_SIZE=9 $
    , font_color='black' $
    )
  endif
  
  ;id to identify a plot by combination of bands and bins
  id = string(format='(%"%i,%i:%i,%i")', iBand1, iBand2, nBins1, nBins2)
  if isa(stratumIndices) then begin
    id += ':' + strjoin(stratumIndices,',')
  endif
  plotInfo = {id:id $
             ,nAdded:0 $
             ,rgb:transpose(w.copyWindow(), [1,2,0]) $
             ,subtitles:subtitles $
             }
  ;cb = IDLgrClipboard()

  userInformation['lastPlotInfo'] = plotInfo
 
  
end

;+
; :Description:
;    This consistency check function is used to validate the hubAMW widgets that are required to run 
;    a `hubApp_scatterPlot_getParameters_Dialog2DrawEvent`. 
;
; :Params:
;    resultHash: in, required, type=Hash
;
; :Keywords:
;    userInformation: in, required, type=hash()
;     Contains user defined hash values that are not retrieved from one of the hubAMW widgets.
;
;    message: out, required, type=String[]
;     String-Array with error messages in case the function returns false.
;
;-
function hubApp_scatterPlot_getParameters_Dialog2DrawCheck, resultHash, userInformation=userInformation, message=message
   
   msg = !NULL
   isConsistent = 1b
   
   
   if ~resultHash.hubIsa('bandIndex1') then begin
    msg = [msg, 'Please select a band from image 1']
   endif
   if ~resultHash.hubIsa('bandIndex2') then begin
    msg = [msg, 'Please select a band from image 2']
   endif
   
   nBins1 = resultHash['nBins1']
   nBins2 = resultHash['nBins2']
   
   message = msg
   isConsistent = ~isa(msg)
   return, isConsistent
end

;+
; :Description:
;    Consistency check function called by `hubAMW_manage` in `hubApp_scatterPlot_getParameters_Dialog2`.
;
; :Params:
;    resultHash: in, required, type=hash
;
; :Keywords:
;    Message: out, required, type=string[]
;     String-Array for error messages in case the function returns false.
;-
function hubApp_scatterPlot_getParameters_Dialog2Check, resultHash, Message=message
   
   msg = !NULL
   isConsistent = 1b
   
   message = msg
   return, isConsistent
end

;+
; :Description:
;    This dialog is used to select the bands that are to be compared and to draw the scatter plot. 
;
; :Params:
;    settings: in, required, type=hash
;     The application settings hash as defined in `hubApp_sctterPlot_application`.
;    results1: in, required, type=hash
;     This is the result hash returned from `hubApp_scatterPlot_getParameters_Dialog1`.
;
;
;
;-
function hubApp_scatterPlot_getParameters_Dialog2, settings, results1
  groupLeader = settings.hubGetValue('groupLeader')
  image1 = hubIOImgInputImage(results1['image1'])
  image2 = hubIOImgInputImage(results1['image2'])
  nSamples = image1.getMeta('lines')
  nLines   = image1.getMeta('samples')
  
  ;some general information to be used in other routines
  drawUserInfo = Hash('initialScreen',1b)
  drawUserInfo = drawUserInfo + results1.hubgetSubHash(['image1','image2','mask','stratification'])
  drawUserInfo['bandNames1'] = image1.hasMeta('band names') ? image1.getMeta('band names') : 'Band ' + strtrim(indgen(image1.getMeta('bands'))+1,2)
  drawUserInfo['bandNames2'] = image2.hasMeta('band names') ? image2.getMeta('band names') : 'Band ' + strtrim(indgen(image2.getMeta('bands'))+1,2) 
  drawUserInfo['settings'] = settings
  ;drawUserInfo['plotInfoList'] = Hash() 
  
  drawUserInfo['lastPlotInfo'] = !NULL 
  image1.cleanup
  image2.cleanup
  if results1.hubIsa('stratification') then begin
    stratification =  hubIOImgInputImage(results1['stratification'], /CLASSIFICATION)
    nStrata = stratification.getMeta('classes')
    strataNames = stratification.getMeta('class names')
    drawUserInfo['strataNames'] = strataNames
    strataNames = strtrim(indgen(nStrata),2) + ' '+strataNames
    stratification.cleanup
  endif
  
    
  loadct,13
  tvlct,r,g,b,/GET
  drawUserInfo['R'] = r
  drawUserInfo['G'] = g
  drawUserInfo['B'] = b
  
  hubAMW_program , groupLeader, Title = settings['title'], /HIDEBUTTONS
  
  hubAMW_frame, title='Input files'
  hubAMW_label, string(format='(%"Image 1: %s")', results1['image1'])
  hubAMW_label, string(format='(%"Image 2: %s")', results1['image2'])
  if results1.hubIsa('mask') then begin
      hubAMW_label, string(format='(%"Mask:    %s")', results1['mask'])
  endif
  if results1.hubIsa('stratification') then begin
      hubAMW_label, string(format='(%"Stratification:    %s")', results1['stratification'])
  endif
  hubAMW_label, string(format='(%"Spatial Size (samples x lines): %i x %i pixel")', nSamples, nLines)
  
  hubAMW_frame, title='Settings'
  baseFrame = widget_base(hubAMW_getCurrentBase(), /ROW, Frame=0, SPACE=0)
  baseFrameInfoContainer = widget_base(baseFrame, /COLUMN, FRAME=0, SPACE=1)
  baseFrameDraw          = widget_base(baseFrame, /COLUMN, FRAME=0, SPACE=0)
  baseFrameInfoTop  = widget_base(baseFrameInfoContainer, /ROW, FRAME=1)
  baseFrameInfo1 = widget_base(baseFrameInfoTop, /COLUMN, FRAME=0)
  baseFrameInfo2 = widget_base(baseFrameInfoTop, /COLUMN, FRAME=0)
  ;baseFrameInfoR = widget_base(baseFrame, /COLUMN, /FRAME)
  

  hubAMW_setCurrentBase, baseFrameInfo1
  listXSize = 125
  listYSize= 350
  parNBinsSize = 5
  parNBinsMin = 2
  parNBinsMax = 30000
  hubAMW_list, 'bandIndex1', TITLE='Band from Image 1', list=drawUserInfo['bandNames1'] $
             , MULTIPLESELECTION=0b, XSIZE=listXSize, YSIZE=listYSize, value=0
  hubAMW_parameter, 'nBins1', TITLE='#Bins', /Integer, SIZE=parNBinsSize $
                  , value=255, ISGE=parNBinsMin, ISLE=parNBinsMax 
  
  hubAMW_setCurrentBase, baseFrameInfo2
  hubAMW_list, 'bandIndex2', TITLE='Band from Image 2', list=drawUserInfo['bandNames2'] $
             , MULTIPLESELECTION=0b, XSIZE=listXSize, YSIZE=listYSize, value=0     
  hubAMW_parameter, 'nBins2', TITLE='#Bins', /Integer, SIZE=parNBinsSize $
                  , value=255, ISGE=parNBinsMin, ISLE=parNBinsMax
  
  if isa(strataNames) then begin
    baseFrameInfoDown = widget_base(baseFrameInfoContainer, /COLUMN, FRAME=1)
    hubAMW_setCurrentBase, baseFrameInfoDown
    ;hubAMW_label, 'Select Stratum'                 
    ;hubAMW_combobox, 'stratumIndices', Title='',LIST=strataNames,Value=0
    hubAMW_list, 'stratumIndices', Title='Select Stratum', List=strataNames $
               , Value=0, YSIZE=95, XSIZE=260, /MultipleSelection  
  endif
  
  
  xsize = 700
  ysize = 600
  initialImage = hubApp_scatterPlot_getParameters_Dialog2CreateInitialScreen([xsize,ysize]) 
  
  hubAMW_setCurrentBase, baseFrameDraw
  hubAMW_draw, 'drawWindow', XSIZE=xsize, YSIZE=ysize, /IDLObjectGraphics, IMAGE=initialImage
  hubAMW_subframe, targetBase=baseFrameDraw, /Row
  hubAMW_button, Title='Draw', USERINFORMATION=drawUserInfo $
        ;, TOOLTIP = 'Draw a new scatter plot with the selected band combination.' $
        , RESULTHASHKEYS=['drawWindow','bandIndex1','bandIndex2','nBins1','nBins2','stratumIndices'] $ 
        , CONSISTENCYCHECKFUNCTION='hubApp_scatterPlot_getParameters_Dialog2DrawCheck' $
        , EVENTHANDLER='hubApp_scatterPlot_getParameters_Dialog2DrawEvent'
  
  
  
  hubAMW_button, Title='Copy to clipboard', USERINFORMATION=drawUserInfo $
    ;, TOOLTIP = 'Copy this plot to the systems clipbord' $
    , EVENTHANDLER='hubApp_scatterPlot_getParameters_Dialog2PlotToClipboard'


  ;hubAMW_button, Title='Add to Report', USERINFORMATION=drawUserInfo $
  ;      , EVENTHANDLER='hubApp_scatterPlot_getParameters_Dialog2AddToPlotList'
  
  hubAMW_button, Title='Get HTML Report', USERINFORMATION=drawUserInfo $
        ;, TOOLTIP = 'Create a HTML file containing this plot and auxiliary information' $
        , EVENTHANDLER='hubApp_scatterPlot_getParameters_Dialog2ShowPlotasReport'
  
  
  parameters = hubAMW_manage()
  if parameters['accept'] then begin
    ;nothing
  endif
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_scatterPlot_getParameters_Dialog2
 
  settings = hubApp_getSettings()
  settings['title'] = 'Scatter Plot'
  results1 = hash()
  results1['image1'] = enmapBox_getTestImage('AF_Image')
  results1['image2'] = enmapBox_getTestImage('AF_Image')
  results1['mask']   = enmapBox_getTestImage('AF_Mask')
  results1['stratification'] = enmapBox_getTestImage('AF_AdminBorders')
  
  print, hubApp_scatterPlot_getParameters_Dialog2(settings, results1)
end

;+
; :Description:
;    Main getParameters Dialog. 
;
; :Params:
;    settings: in, required, type = hash
;
;
;
;-
function hubApp_scatterPlot_getParameters, settings
  
  ;restore state from previous application calls to retrieve default values
  stateHash = hub_getAppState('hubApp', 'stateHash_scatterPlot')
  if isa(stateHash) then settings += stateHash
  
  groupLeader = settings.hubGetValue('groupLeader')
  groupLeader = !NULL
  CANCELED = hash('accept', 0b)
  results1 = hubApp_scatterPlot_getParameters_Dialog1(settings)
  if ~results1['accept'] then return, CANCELED
  
    
  results2 = hubApp_scatterPlot_getParameters_Dialog2(settings, results1)
  
  state = results1
  if results2['accept'] then state += results1
  ;save results  as new session state hash
  hub_setAppState, 'hubApp', 'stateHash_scatterPlot', state
  
  return,results2
end

;+
; :Hidden:
;-
pro test_hubApp_scatterPlot_getParameters

  ; test your routine
  settings = hubApp_getSettings()
  settings['title'] = 'Scatter Plot'
  settings['image1'] = enmapBox_getTestImage('AF_Image')
  settings['image2'] = enmapBox_getTestImage('AF_Image')
  ;settings['mask']   = enmapBox_getTestImage('AF_MaskVegetation')
  ;settings['stratification'] = enmapBox_getTestImage('AF_LC')
  
  settings['image1'] = hub_getTestImage('Hymap_Berlin-A_Image')
  settings['image2'] = hub_getTestImage('Hymap_Berlin-A_Image')
  settings['stratification'] = hub_getTestImage('Hymap_Berlin-A_Classification-Estimation')
  
  
  settings = hubApp_getSettings()
  settings['title'] = 'Scatter Plot'
  settings['image1'] = 'G:\temp\temp_bj\malicious_testdata\svrEstimation_marcel_masked.bsq'
  settings['image2'] = 'G:\temp\temp_bj\malicious_testdata\label_fractions30m_masked.bsq'
  
  parameters = hubApp_scatterPlot_getParameters(settings)
  print, parameters

end  
