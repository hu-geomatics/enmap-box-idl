;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-


;+
; :Description:
;    This is the application main procedure. It queries user input (see `hubApp_scatterPlot_getParameters`)
;    via a widget program. The user input is passed to the applications processing routine
;    (see `hubApp_scatterPlot_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `hubApp_scatterPlot_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;-
pro hubApp_scatterPlot_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  settings['title'] = 'Scatter Plot'
  parameters = hubApp_scatterPlot_getParameters(settings)
  
  if parameters['accept'] then begin 
    if parameters.hubIsa('reportList')then begin
      reportList = parameters['reportList']
      reportList.saveHTML, /Show 
    endif
  endif
  
end

;+
; :Hidden:
;-
pro test_hubApp_scatterPlot_application
  
  settings = hubApp_getSettings()
  settings['title'] = 'Scatter Plot'
  settings['image1'] = enmapBox_getTestImage('AF_Image')
  settings['image2'] = enmapBox_getTestImage('AF_Image')
  settings['mask']   = enmapBox_getTestImage('AF_Mask')
  settings['stratification'] = enmapBox_getTestImage('AF_AdminBorders')
  hubApp_scatterPlot_application, settings

end  
