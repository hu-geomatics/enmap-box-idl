;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-


;+
; :Description:
;    This function returns the valid pixel values from the intersection of `image1`, `image2`, `mask1` and `mask2`.
;    when using this function it is required that the data readers of all images are initialized in the same way and 
;    for same spatial dimension only.
;    
; :Params:
;    image1: in, required, type=hubIOImgInputImage()
;    image2: in, required, type=hubIOImgInputImage()
;
; :Keywords:
;    mask: in, required, type=hubIOImgInputImage(/Mask)
;    stratification: in, required, type=hubIOImgInputImage(/Classification)
;
;-
function hubApp_scatterPlot_processing_getDataVectors, image1, image2 $
      , mask=mask $
      , stratification=stratification $
      , stratumIndices=stratumIndices 
      
  div1 = image1.getMeta('data ignore value')
  div2 = image2.getMeta('data ignore value')
 
  data1 = image1.getData()
  data2 = image2.getData()
  
  dims = size(data1, /DIMENSIONS)
  
  nPixels = n_elements(data1)
  valid = make_array(dims, value=1b, /BYTE)
  
  if isa(div1) then valid and= data1 ne div1
  if isa(div2) then valid and= data2 ne div2
  if isa(mask) then begin
    maskData = mask.getData()
    if product(dims eq size(maskData, /DIMENSIONS)) ne 1 then message, 'mask vector has wrong dimensions' 
    valid and= temporary(maskData) ne 0b
  endif
  
  if isa(stratification) then begin
    stratMask = make_array(dims, value=0b, /BYTE)
    stratData = stratification.getData()
    if product(dims eq size(stratData, /DIMENSIONS)) ne 1 then message, 'stratification vector has wrong dimensions'
    foreach stratIndex, stratumIndices do begin
      stratMask or= stratData eq stratIndex 
    endforeach
    valid and= stratMask
  endif
  
  is_valid = where(valid eq 1b, /NULL)
  data1 = data1[is_valid]
  data2 = data2[is_valid]
  return, Dictionary('data1',data1,'data2',data2, 'hasValues', isa(data1))
end


;+
; :Description:
;    This function returns a 2D Histogram that can be used/interpreted as scatter plot between
;    two image bands. 
;    
; :Returns:
;   The following hash is returned::
;     
;      key | type | description
;      ----+------+---------------
;      
;      
;      ----+------+--------------
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;     The following hash keys are used to control this function::
;
;      key        | type      | description
;      -----------+-----------+-----------------
;       / required hash keys /
;      image1     | string    | filepath image1
;      image2     | string    | filepath image2
;      bandIndex1 | int       | band index image1 (zero based)
;      bandIndex2 | int       | band index image2 (zero based)
;       / optional hash keys /
;      nBins      | int       | number of bins for nBins1 or nBins, 
;                 |           | in case not specified explicitely
;                 |           | default = 256
;      nBins1     | int       | number of bins for band 1
;      nBins2     | int       | number of bins for band 2
;      range1     | double[2] | value range of interest in band 1, default=[min(band1), max(band1)]
;      range2     | double[2] | value range of interest in band 2, default=[min(band1), max(band1)]  
;      -----------+-----------+------------------
;      
;      
;     
;       
;    
;    settings : in, optional, type=hash
;     A settings hash, e.g. as defined in `hubApp_scatterPlot_applicaton`. The following
;     keys are used by this function:: 
;     
;      key       | type | description
;      ----------+------+---------------
;      NOSHOW    | bool | if set, the progressbar will not appear
;      ----------+------+--------------
;     
;
;-
function hubApp_scatterPlot_processing, parameters, settings

  ; check required parameters
  hubApp_checkKeys, parameters, ['image1','image2','bandIndex1','bandIndex2']
  
  
  image1 = hubIOImgInputImage(parameters['image1'])
  image2 = hubIOImgInputImage(parameters['image2'])
  
  if ~image1.isCorrectSpatialSize(image2.getSpatialSize()) then begin
    message,'image1 and image2 must have same spatial size'
  endif
  
  tileLines = hubIOImg_getTileLines(image1.getMeta('samples'),3, 'double')
  
  initBandSubsets = [parameters['bandIndex1'], parameters['bandIndex2']]  
  initImages      = [image1, image2]
  initDataTypes   = ['double','double']
  
  mask = []
  if parameters.hubIsa('mask') then begin
    mask = hubIOImgInputImage(parameters['mask'], /MASK)
    
    if ~image1.isCorrectSpatialSize(mask.getSpatialSize()) then begin
      message,'images and mask must have same spatial size'
    endif
  endif

  stratification = []
  if parameters.hubIsa('stratification') then begin
    if ~parameters.hubIsa('stratumIndices') then message, "Hash-Key 'stratumIndices' required when stratification file is given"
    stratification = hubIOImgInputImage(parameters['stratification'], /Classification)
    initBandSubsets = [initBandSubsets, 0]
    initImages = [initImages, stratification]
    initDataTypes = [initDataTypes, 'byte']
    if ~image1.isCorrectSpatialSize(stratification.getSpatialSize()) then begin
      message,'images and stratification must have same spatial size'
    endif
  endif
  
  ; check & set optional parameters
  nBins  = parameters.hubGetValue('nBins', default=256)
  nBins1 = parameters.hubGetValue('nBins1', default=nBins)
  nBins2 = parameters.hubGetValue('nBins2', default=nBins)
  
  if nBins  lt 1 then message, 'nBins must be >= 1'
  if nBins1 lt 1 then message, 'nBins1 must be >= 1' 
  if nBins2 lt 1 then message, 'nBins2 must be >= 1'
  
  range1 = parameters.hubGetValue('range1')
  range2 = parameters.hubGetValue('range1')
  
  if isa(settings) then settings = hash() 
  
  if isa(settings) && ~settings.hubKeywordSet('noShow') then begin
    progressBar = hubProgressBar(Title='Scatter Plot' $
                      , groupLeader=parameters.hubGetValue('groupLeader') $
                      , info='Calculate band histogram...' $
                      )
    linesProcessed = 0l
    progressBar.setRange, [0, float(image1.getMeta('lines')) / tileLines]
    progressBar.setProgress, linesProcessed
  endif
  
  
  
  ;init readers
  hubApp_manageReaders, initImages, tileLines $
              , SUBSETBANDPOSITIONS=initBandSubsets $
              , DataType=initDataTypes $
              , maskImages=[mask] $
              , ForegroundMaskValues = initMaskForegroundValues $
              , /INIT, /BAND
              
  results = Dictionary()
  
  
  ;1. get range in case it was not defined before
  if ~isa(range1) or ~isa(range2) then begin
    mathRange1 = hubMathIncRange()
    mathRange2 = hubMathIncRange()
  
    while ~image1.TileProcessingDone() do begin
      dataHash = hubApp_scatterPlot_processing_getDataVectors(image1, image2 $
          , mask = mask $
          , STRATIFICATION = stratification $
          , StratumIndices = parameters['stratumIndices'] )
      if dataHash.hubIsa('data1') then begin
        mathRange1.addData, temporary(dataHash['data1'])
        mathRange2.addData, temporary(dataHash['data2'])
      endif
      
      if isa(progressBar) then begin
        linesProcessed += tileLines 
        progressBar.setProgress, linesProcessed
      endif
    endwhile
    
    ;finish readers  
    hubApp_manageReaders, initImages, maskImages=[mask], /FINISH
    
    results['numberOfSamples'] = mathRange1.getNumberOfSamples() 
    results['hasSamples'] = results['numberOfSamples'] gt 0 
    if ~isa(range1) then range1 = (results['hasSamples']) ? mathRange1.getResult() : [0,1]
    if ~isa(range2) then range2 = (results['hasSamples']) ? mathRange2.getResult() : [0,1]
   endif 
  
  ;2. get 2D-histogram in case there is at least one pixel availables
  hist2D = hubMathIncHistogram2D(range1, range2, nBins1, nBins2)
  if results['hasSamples'] then begin
    ;re-init readers
    hubApp_manageReaders, initImages, tileLines $
              , SUBSETBANDPOSITIONS=initBandSubsets $
              , DataType=initDataTypes $
              , maskImages=[mask] $
              , ForegroundMaskValues = initMaskForegroundValues $
              , /INIT, /BAND
  
    while ~image1.TileProcessingDone() do begin
      dataHash = hubApp_scatterPlot_processing_getDataVectors(image1, image2 $
                            , mask = mask $
                            , STRATIFICATION = stratification $
                            , StratumIndices = parameters['stratumIndices'] )
                            
      if dataHash.hubIsa('data1') then begin
        if product(FINITE(dataHash.data1)) eq 0 or $
           product(FINITE(dataHash.data2)) eq 0 $
           then begin
            s = ""
           endif 
        ;hist2D.addData, temporary(dataHash.data1), temporary(dataHash.data2)
        hist2D.addData, dataHash.data1, dataHash.data2
        ;p = SCATTERPLOT(dataHash.data2, dataHash.data1, SYMBOL='.', /SYM_FILLED, RGB_TABLE=7)
        ;!NULL = temporary(dataHash)
      endif
      if isa(progressBar) then begin
        progressBar.setProgress, ++linesProcessed
      endif
    endwhile
  endif

  results += hist2D.getResult() 
  results += parameters
  
  ;cleanup inputImage objects
  hubApp_manageReaders, initImages, maskImages=[mask], /CLEANUP
  
  if isa(progressBar) then progressBar.cleanup
  
  return, results
end

;+
; :Hidden:
;-
pro test_hubApp_scatterPlot_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = hubApp_getSettings()
  parameters['image1'] = 'D:\EnMAP-Box\enmapProject\lib\hubAPI\resource\testData\image\Hymap_Berlin-A_Image'
  parameters['image2'] = 'D:\Sandbox\BigData\NN_scaled_01_ETB.bin'
  
  parameters['image1'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['image2'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['stratification'] = hub_getTestImage('Hymap_Berlin-A_Classification-Estimation')
  parameters['stratumIndices'] = 2
  ;parameters['image2'] = parameters['image1']
  parameters['bandIndex1'] = 16
  parameters['bandIndex2'] = 95
  parameters['nBins1'] = 10
  parameters['nBins2'] = 10
  
  ;si = hubIOImgInputImage(parameters['stratification'], /Classification)
  ;si.initReader, 2, /TileProcessing 
  
  result = hubApp_scatterPlot_processing(parameters, settings)
  ;print, result
  help, result['histogram2D']
  print, result['histogram2D']
  
end  
