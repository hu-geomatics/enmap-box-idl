;+
; :Author: Andreas
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `hubApp_kernelPCA_viewmodel_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro hubapp_kernelpca_viewmodel_event, event
  
  @huberrorcatch
    
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  
  hubapp_kernelpca_viewmodel_application, Title='Kernel PCA',  GroupLeader=event.top, applicationInfo
  
end
