;+
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro hubApp_kernelPCA_viewmodel_application, Title=title, GroupLeader=groupLeader, applicationInfo

  settings = applicationInfo
  settings['title'] = 'KernelPCA'
  defaultValues = hub_getAppState('hubApp_kernelPCA','stateHash_kPCA',default=hash())
  parameters =  hubApp_kernelPCA_viewmodel_getParameters(settings, defaultValues, GroupLeader=groupLeader)
  if isa(parameters) then begin
    hubApp_kernelPCA_viewmodel_processing, parameters
  endif
end
