function hubApp_kernelPCA_viewmodel_getParameters, settings, defaultValues, GroupLeader=groupLeader
  
  hubAMW_program, Title=settings['title']+': View Model', GroupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputFilename,'modelFilename', Title='KPC File', Value=defaultValues.hubGetValue('modelFilename')
  result = hubAMW_manage()
  
  if result['accept'] then begin
    parameters = result
  endif else begin
    parameters = !NULL
  endelse
  return, parameters
  
end

;+
; :Hidden:
;-
pro test_hubApp_kernelPCA_viewmodel_getParameters

  settings = hash()
  settings['title'] = 'Kernel PCA'
  defaultValues = hash()
  result = hubApp_kernelPCA_viewmodel_getParameters(settings, defaultvalues)
  print, result

end  
