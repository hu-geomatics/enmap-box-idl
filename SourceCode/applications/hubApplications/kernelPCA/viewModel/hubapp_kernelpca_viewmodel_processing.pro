;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    useful results to the user in the form of text, images or tables. 
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        keys                      | type     | description
;        --------------------------+----------+--------------------------------------------------
;        modelFilename             | string   | file path of KPC model
;
;-
pro hubApp_kernelPCA_viewmodel_processing, parameters

  restore, FILENAME=parameters['modelFilename']
 
  lambda = model['lambda'] 
  num_pcs = model['num_pcs']
  report = hubReport(title = 'View kernelPC Model Parameters')
  parameters['inputMask'] = isa(parameters['inputMask']) ? parameters['inputMask'] : ' '

  report.addHeading, 'Input File', 1
  report.addMonospace, ['  Image:           ' + parameters['inputFilename'], $
                        '  Reference Areas: ' + parameters['inputMask'], $
                        '  Model Filename:  ' + parameters['modelFilename']]
  report.addHeading, 'Model Parameters', 1
  if model['kernel'] eq 1 then begin
    report.addMonospace, ['  kernel:            ' + 'non-linear', $
                          '  gamma:             ' + strcompress(/Remove_all,model['gma']), $
                          '  number of samples: ' + strcompress(/Remove_all,parameters['m']), $
                          '  nscale:            ' + strcompress(/Remove_all,parameters['nscale']) ]
  endif                        
  
  if model['kernel'] eq 0 then begin
    report.addMonospace, ['  kernel:            ' + 'linear', $
                          '  number of samples: ' + strcompress(/Remove_all,parameters['m'])]
  endif
  
  ;Table 1
  lambdaList = list()
  for i=1, num_pcs do begin 
    lambdaList.add, lambda[i-1]  
  endfor
  
  expVariance = lambda / total(lambda) * 100
  
  expVarList = list()
  for i=1, num_pcs do begin 
    expVarList.add, expVariance[i-1] 
  endfor
  
  expVarCum = expVariance
  for i=1, num_pcs-1 do begin
    expVarCum[i] = total(expVariance[0:i]) 
  endfor
  
  expVarCumList = list()
  for i=1, num_pcs do begin
    expVarCumList.add, expVarCum[i-1] 
  endfor
  
  componentsList = list()
  for i=1, num_pcs do componentsList.add, i

  report.addHeading, 'Results', 1
  report.addHeading, 'Eigenvalues', 2
  
  table1 = hubReportTable()
  table1.addColumn, componentsList, NAME='Components'
  table1.addColumn, lambdaList, NAME='Eigenvalue'
  table1.addColumn, expVarList, NAME='Explained variance [%]'
  table1.addColumn, expVarCumList, NAME='Explained cumulated variance [%]'
  report.addHTML, table1.getHTMLTable(ATTRIBUTESTABLE='border="1"')                       
  
  ;Plots
  leftMargin = 250
  rightMargin = 150
  windowWidth = 1200
  w = window(DIMENSIONS=[windowWidth,500],/BUFFER)
  if n_elements((indgen(num_pcs)+1)[0:(where(fix(expVarCum) le 95))[-1]+1]) le 20 then begin
    xtickinterval = 1
    xminor = 0
  endif

  eigenPlot = plot((indgen(num_pcs)+1)[0:(where(fix(expVarCum) le 95))[-1]+1], (lambda[0:num_pcs-1])[0:(where(fix(expVarCum) le 95))[-1]+1] $
    ,XTITLE='# Components' $
    ,YTITLE='Eigenvalue' $
    ,TITLE='Components explaining 95% variance' $
    ,AXIS_STYLE=1 $
    ,MARGIN = [leftMargin, 50, rightMargin, 50] $
    ,THICK=2 $
    ,LAYOUT=[2,1,1] $
    ,/DEVICE $
    ,/CURRENT $
    ,/BUFFER $
    ,xtickformat='(I)' $
    ,xtickinterval=xTickinterval $
    ,xminor=xMinor)

  cumVarPlot = plot((indgen(num_pcs)+1)[0:(where(fix(expVarCum) le 95))[-1]+1], (expVarCum[0:num_pcs-1])[0:(where(fix(expVarCum) le 95))[-1]+1] $
    ,AXIS_STYLE=0 $
    ,MARGIN = [leftMargin, 50, rightMargin, 50] $
    ,THICK=2 $
    ,LAYOUT=[2,1,1] $
    ,/DEVICE $
    ,/CURRENT $
    ,COLOR='r' $
    ,YRANGE=[0,100])

  yAxis1 = axis('Y',TEXTPOS=1,TARGET=cumvarplot,LOCATION=[max(cumVarPlot.xrange),0,0],TITLE='Explained cumulated variance [%]',COLOR='r')

  eigenFullPlot = plot((indgen(num_pcs)+1), lambda[0:num_pcs-1] $
    ,XTITLE='# Components' $
    ,YTITLE='Eigenvalue' $
    ,TITLE='All components' $
    ,AXIS_STYLE=1 $
    ,MARGIN = [leftMargin, 50, rightMargin, 50] $
    ,THICK=2 $
    ,LAYOUT=[2,1,2] $
    ,/DEVICE $
    ,/CURRENT $
    ,/BUFFER)

  cumVarFullPlot = plot((indgen(num_pcs)+1) ,expVarCum[0:num_pcs-1] $
    ,AXIS_STYLE=0 $
    ,MARGIN = [leftMargin, 50, rightMargin, 50] $
    ,THICK=2 $
    ,LAYOUT=[2,1,2] $
    ,/DEVICE $
    ,/CURRENT $
    ,/BUFFER $
    ,COLOR='r' $
    ,YRANGE=[0,100])

  yAxis2 = axis('Y',TARGET=cumVarFullPlot,LOCATION='right',TITLE='Explained cumulated variance [%]',TICKDIR=0,COLOR='r')

  imageContent = transpose(w.CopyWindow(BORDER=0,width=windowWidth), [1,2,0])
  w.Close

  report.addImage, imageContent
  
  if model['kernel'] eq 0 then begin
  ;Table 2
    table2 = hubReportTable() 
    eigenVectorArray = objarr(num_pcs)
    for i=0, num_pcs-1 do begin
      eigenVectorArray[i]=list()
      for j=0, num_pcs-1 do begin
        (eigenVectorArray[i]).add, (model['alpha'])[i,j]
      endfor
      table2.addColumn, eigenVectorArray[i], NAME=strcompress(i+1,/remove_all)
    endfor
    report.addHeading, 'Eigenvectors', 2
    report.addHTML, table2.getHTMLTable(ATTRIBUTESTABLE='border="1"')           
    endif
    
  report.saveHTML, /Show

end

;+
; :Hidden:
;-
pro test_hubApp_kernelPCA_viewmodel_processing
  
  ;train a small model
  p = hash()
  settings = hash()
  settings['title'] = 'KernelPCA'
  p['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  p['modelFilename'] = filepath('test.kpc',/TMP) 
  p['nscale'] = 1.
  p['randomSamples'] = 1000
  p['inputMask'] = !NULL;hub_getTestImage('Hymap_Berlin-A_Mask')
  p['kernelChoice'] = 0
  hubapp_kernelpca_parameterize_processing, p, settings
  
  ;view model
  hubApp_kernelPCA_viewmodel_processing, p
  
end
