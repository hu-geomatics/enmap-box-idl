;           
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro hubApp_kernelPCA_parameterize_application, Title=title, GroupLeader=groupLeader, applicationInfo

  settings = applicationInfo
  settings['title'] = 'KernelPCA'
  defaultValues = hub_getAppState('hubApp_kernelPCA','stateHash_kPCA',default=hash())

  parameters = hubApp_kernelPCA_parameterize_getParameters(settings, defaultValues, GroupLeader=groupLeader)
  
  if isa(parameters) then begin
    hub_setAppState, 'hubApp_kernelPCA', 'stateHash_kPCA', parameters
;    stopWatch = hubProgressStopWatch()
    hubApp_kernelPCA_parameterize_processing, parameters, settings, Title=title, GroupLeader=groupLeader
    hubApp_kernelPCA_viewmodel_processing, parameters
   
;    stopWatch.showResults, title=settings['title'], description='Parameterization completed'
    ok = dialog_message('Do you want to apply the model to an image?', /QUESTION, TITLE=settings['title'])
      if ok eq 'Yes' then begin
         hubApp_kernelPCA_apply_application, Title=title, GroupLeader=groupLeader, applicationInfo
      endif
    
  endif 
end
