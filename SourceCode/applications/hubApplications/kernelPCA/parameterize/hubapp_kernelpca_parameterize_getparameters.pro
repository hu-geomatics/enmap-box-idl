;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, required, type=hash
;
;-
function hubApp_kernelPCA_parameterize_getParameters, settings, defaultValues, GroupLeader=groupLeader
      
  tsize=75
  defaultValues = hub_getAppState('hubApp_kernelPCA','stateHash_kPCA',default=hash())
  hubAMW_program, groupLeader, Title=settings['title']+': Parameterize'
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleSet, 'inputSampleSet', title='Image', /Masking $
    , ReferenceTitle='Mask ', /ReferenceOptional $
    , Value = defaultValues.hubGetValue('inputFilename') $
    , ReferenceValue = defaultValues.hubGetValue('inputMask') $
    , TSize=tsize
  
  hubAMW_frame, Title='Parameters'
  hubAMW_parameter, 'randomSamples', Title='Number of random samples', Value=1000, IsGE=10, IsLE=10000, /Integer, Optional=1
  hubAMW_subframe, 'kernelChoice', Title='Linear PCA', /Row, /SetButton
  hubAMW_subframe, 'kernelChoice', Title='Non-linear kernelPCA', /Row 
  hubAMW_parameter, 'nscale',  Title='NSCALE Parameter', Value=1.0, /FLOAT,SIZE=10
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'modelFilename', Title= 'KPC Model', Extension=extension, TSize=tSize, Value='kpcModel.kpc'
  result = hubAMW_manage(/Flat)
    
  if result['accept'] then begin
    parameters = result
    parameters['inputMask'] = parameters['inputSampleSet_labelFilename']
    parameters['inputFilename'] = parameters['inputSampleSet_featureFilename']
    if ~parameters.haskey('randomSamples') and ~isa(parameters['inputMask']) then begin
      ok = dialog_message('Do you really want to calculate the statistics from the entire image cube?', /QUESTION, TITLE=settings['title'])
      if ok eq 'No' then begin
        parameters = !null  
      endif
    endif
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_kernelPCA_parameterize_getParameters

  settings = hash()
  settings['title'] = 'kernelPCA'
  parameters = hubApp_kernelPCA_parameterize_getParameters(settings)
  print, parameters

end  
