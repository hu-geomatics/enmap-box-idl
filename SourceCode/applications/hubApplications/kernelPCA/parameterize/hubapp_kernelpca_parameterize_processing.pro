function covarianceInc, filename
  
  inputImage = hubIOImgInputImage(filename)
  num_bands = inputImage.getMeta('bands')
  num_lines = inputImage.getMeta('lines')
  
  numberOfTileLines = hubIOImg_getTileLines(image=inputImage)
  inputImage.initReader, numberOfTileLines, /SLICE, /TILEPROCESSING
  ;initialize 1-d hubMathIncMean object array
  incMeans = objarr(num_bands)
  for i=0, num_bands-1 do incMeans[i] = hubMathIncMean()
  while ~inputImage.tileProcessingDone() do begin
    data = inputImage.getData()
    for i=0, num_bands-1 do (incMeans[i]).addData, data[i,*]
  endwhile
  ;initialize 2-d hubMathIncCovariance object array with band means
  incCovs = objarr(num_bands,num_bands)
  for i=0, num_bands-1 do begin
    for j=i, num_bands-1 do begin
      incCovs[i,j] = hubMathIncCovariance((incMeans[i]).getResult(),(incMeans[j]).getResult())
    endfor
  endfor
  bandMeans = dblarr(num_bands)
  ;fill object with data
  for i=0, num_bands-1 do bandMeans[i] = (incMeans[i]).getResult()  
  inputImage.cleanup
  
  progressBar = hubProgressBar(Title='Calculating Covariances', /Cancel)
  TilesDone = 0
  progressBar.setRange, [0, num_lines]
  inputImage = hubIOImgInputImage(filename)
  inputImage.initReader, numberOfTileLines, /SLICE, /TILEPROCESSING
  
  while ~inputImage.tileProcessingDone() do begin
    data = inputImage.getData()
    data = data - rebin(bandMeans, num_bands, n_elements(data[0,*])) ;center data
    for i=0, num_bands-1 do begin
      for j=i, num_bands-1 do begin
        (incCovs[i,j]).addData, data[i,*], data[j,*]
      endfor
    endfor
    TilesDone += numberOfTileLines
    TilesDone <= num_lines
    progressBar.setProgress, TilesDone
  endwhile
  inputImage.cleanup
  ;build cov-array
  covMatrix = dblarr(num_bands,num_bands)
  for i=0, num_bands-1 do begin
    for j=i, num_bands-1 do begin
      covMatrix[i,j] = (incCovs[i,j]).getResult()
      if i eq j then continue
      covMatrix[j,i] = (incCovs[i,j]).getResult()
    endfor
  endfor
  progressBar.cleanup
  resultList = list()
  resultList.add, covMatrix
  resultList.add, bandMeans 

  return, resultList
end

;+
; :Description:
;    Use this procedure to parameterize a KPC model.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;
;        keys               | type     | description
;        -------------------+----------+--------------------------------------------------
;        inputFilename      | string   | file path of feature image
;        modelFilename      | string   | file path of created KPC model
;        nscale             | number   | in case of non-linear kernel, scaling parameter (default = 1.0) 
;        kernelChoice       | number   | 0 = linear kernel
;                           |          | 1 = non-linear kernel
;        
;        optional keys      | type     | description
;        -------------------+----------+--------------------------------------------------
;        randomSamples      | number   | number of samples drawn from input image
;        inputMask          | string   | file path of mask image. Set this to !NULL if not used
;        -------------------+----------+--------------------------------------------------
;
;    settings: in, required, type=Hash
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro hubapp_kernelpca_parameterize_processing, parameters, settings, Title=title, GroupLeader=groupLeader

  inputImage = hubIOImgInputImage(parameters['inputFilename'])
  numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)
  ;inputFilename = parameters['inputFilename']
  num_cols = inputImage.getMeta('samples')
; num_rows = numberOfTileLines
  num_bands = inputImage.getMeta('bands')
  if parameters['kernelChoice'] eq 1 then nscale = float(parameters['nscale'])
   
  progressbar = hubProgressBar( $
    GroupLeader=settings.hubGetValue('groupLeader') $
    , Info='Processing...' $
    , Title=settings['title'])
 
  parameters['deleteStrataMask']=1b
  parameters['inputImage']=parameters['inputFilename']
  parameters['ignoreValueHandling'] = 'ANY'
  strataMask = hubApp_randomSampling_getStrataMask(parameters)

  if parameters.HasKey('randomSamples') then begin
    m = parameters['randomSamples']
    parameters['inputImage']=strataMask['stratificationMask']
    parameters['stratificationMask']=strataMask['stratificationMask']
    parameters['outputSamples']=FILEPATH('outputSamples',/TMP)
    if isa(parameters['inputMask']) then begin 
      if (strataMask['strataCounts'])[1] lt m then m = (strataMask['strataCounts'])[1] 
    endif
    parameters['samplesPerStratum']=[0,m]
    hubApp_randomSampling_processing,parameters,NoShow=NoShow
    randomPoints = hubIOImgSampleSetForDensity(parameters['inputFilename'],parameters['outputSamples'])
  endif else begin
    randomPoints = hubIOImgSampleSetForDensity(parameters['inputFilename'],strataMask['stratificationMask']) 
    m = (strataMask['strataCounts'])[1]
  endelse
  
  if isa(parameters['inputMask']) or parameters.HasKey('randomSamples') or parameters['kernelChoice'] eq 1 then begin
    G = (randomPoints.getSample()).FEATURES  
  endif

  if parameters['kernelChoice'] eq 1 then begin
    num_pcs = num_bands < m
    ; centered radial basis kernel matrix
    K = kernel_matrix(G,gma=gma,nscale=nscale)
    K = center(K)
    progressbar.setInfo, 'Diagonalizing...'
    ; eigenvalues and eigenvectors of centered kernel matrix
    lambda = la_eigenql(K,/double,eigenvectors=V,range=[m-num_pcs,m-1])
    idx = reverse(sort(lambda))
    lambda = lambda[idx]
    V = V[*,idx]
    progressbar.cleanup
    ; dual variables (normalized eigenvectors)
    alpha = float(diag_matrix(1/sqrt(lambda))##V)
    
    model = hash()
    model['alpha'] = alpha
    model['gma'] = gma
    model['lambda'] = lambda
    model['G'] = G
  endif

  if parameters['kernelChoice'] eq 0 then begin

    Tol = 1.0d-12
    model = hash()
    if ~isa(parameters['inputMask']) and ~parameters.HasKey('randomSamples') then begin
      resultList = covarianceInc(parameters['inputFilename'])
      eigenvalues = EIGENQL(resultList[0], EIGENVECTORS=eigenvectors, /DOUBLE)
      iss = where(abs(Eigenvalues) le Tol, nss) ;Check for ~zero~ eigenvalues.
      if nss ne 0 then Eigenvalues[iss] = 0.0
      model['means'] = resultList[1]
    endif else begin 
      ;Remove the mean from each variable.
     
      means = total(double(G), 2)/m ;use double precision!
      sampleCentered = G - rebin(means, num_bands, m)
      covMatrix = double(sampleCentered # transpose(sampleCentered)) / (m-1)
      eigenValues = EIGENQL(covMatrix, EIGENVECTORS=eigenvectors, /DOUBLE)
   
      iss = where(abs(Eigenvalues) le Tol, nss) ;Check for ~zero~ eigenvalues.
      if nss ne 0 then Eigenvalues[iss] = 0.0
      
      model['means'] = means
    endelse
    ;Scale each eigenvector by the SQRT of its corresponding eigenvalue.
    eigenVectors = Eigenvectors * (replicate(1.0, m) # sqrt(Eigenvalues))

    num_pcs = num_bands
    model['lambda'] = eigenValues
    model['alpha'] = eigenVectors
  endif
  progressbar.cleanup
  model['num_cols'] = num_cols
 ; model['num_rows'] = num_rows
  model['num_pcs'] = num_pcs
  model['num_bands'] = num_bands
  model['kernel'] = parameters['kernelChoice']
  
  parameters['m'] = m
  parameters['modelImage'] = parameters['inputFilename']
  
  save, model, parameters, FILENAME=parameters['modelFilename']
  enmapBox_openFile, parameters['modelFilename']
end
  
;+
; :Hidden:
;-
  
pro test_hubapp_kernelpca_parameterize_processing
  ;train a small model
  p = hash()
  settings = hash()
  settings['title'] = 'KernelPCA'
  p['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  p['modelFilename'] = filepath('test.kpc',/TMP)
  p['nscale'] = 1.
  p['randomSamples'] = 500
  p['inputMask'] = !NULL
  p['kernelChoice'] = 1 ; 0=linear 1=non-linear
  
  hubapp_kernelpca_parameterize_processing, p, settings
  print, 'Parameterization Complete.'
  
end