;+
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro hubApp_kernelPCA_apply_application, Title=title, GroupLeader=groupLeader, applicationInfo

  settings = applicationInfo
  settings['title'] = 'KernelPCA'
  defaultValues = hub_getAppState('hubApp_kernelPCA','stateHash_kPCA',default=hash())
  result = hubApp_kernelPCA_apply_getParameters(settings, defaultValues, GroupLeader=groupLeader)
  if isa(result) then begin
    hub_setAppState, 'hubApp_kernelPCA', 'stateHash_kPCA', result
    stopWatch = hubProgressStopWatch()
    hubApp_kernelPCA_apply_processing, result, settings, Title=title, GroupLeader=groupLeader
    stopWatch.showResults, title=settings['title'], description='Calculations completed.'
  endif 
  
end
