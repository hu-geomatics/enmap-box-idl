;+
; :Description:
;    Use this procedure to apply a KPC model to an image.
;
; :Params:
;    result: in, required, type=Hash
;      A hash that contains the following parameters::
;      
;        keys                      | type     | description
;        --------------------------+----------+--------------------------------------------------
;        modelFilename             | string   | file path of KPC to be applied
;        inputFilename             | string   | file path of feature image
;        outputFilename            | string   | file path of created KPC image
;        pcs                       | number   | principal components to be extracted
;        exclusiveSubframeGroup    | number   | number of output pcs chosen 
;                                  |          | 0 = by defined number
;                                  |          | 1 = by explained variance
;                                  |          | 2 = all
;
;        optional keys             | type     | description
;        --------------------------+----------+--------------------------------------------------
;        inputMask                 | string   | file path of mask image
;        --------------------------+----------+-------------------------------------------------- 
;  
;    settings: in, required, type=Hash
;    
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro hubApp_kernelPCA_apply_processing, result, settings, Title=title, GroupLeader=groupLeader

  restore, FILENAME=result['modelFilename']
  
  ct = 1b
  num_pcs = model['num_pcs']
  ; defined number of PCs
  if result['exclusiveSubframeGroup'] eq 0 then num_pcs = result['pcs']
  ; number of PCs by explained variance
  if result['exclusiveSubframeGroup'] eq 1 then begin
    i = 0
    arr = 0
    while arr le result['expVariance'] do begin
      arr+= (model['lambda'] / total(model['lambda']) * 100)[i]
      i++
    endwhile
    num_pcs = i
  endif
  ; all pcs = num_pcs

  alpha = temporary((model['alpha'])[*,0:num_pcs-1])
  progressbar = hubProgressBar( $
    GroupLeader=settings.hubGetValue('groupLeader') $
    , Info='Processing...' $
    , Title='PCA Rotation')
    
  inputImage = hubIOImgInputImage(result['inputFilename'])
  inputDIV = inputImage.getmeta('data ignore value')
  num_cols = inputImage.getmeta('samples')
  modelImage = hubIOImgInputImage(parameters['modelImage'])
  numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)

  ; initialize tile processing
  outputImage = hubIOImgOutputImage(result['outputFilename'])
  outputImage.copyMeta, inputImage, /CopySpatialInformation
  if model['kernel'] eq 0 then begin
    outputImage.setMeta, 'band names', 'PC '+strtrim(lindgen(num_pcs)+1,2)
    outputImage.setMeta, 'description', 'PCA: '+file_basename(result['inputFilename'])
  endif else begin
    outputImage.setMeta, 'band names', 'kernel PC '+strtrim(lindgen(num_pcs)+1,2)
    outputImage.setMeta, 'description', 'kernel PCA: '+file_basename(result['inputFilename'])
  endelse
  
  inputImage.initReader, numberOfTileLines, /Slice, /TileProcessing, DataType='float'
   if isa(result['inputMask']) then begin
    inputMask = hubIOImgInputImage(result['inputMask'])
    inputMask.initReader, numberOfTileLines, /SLICE, /MASK, /TILEPROCESSING
   endif
  writerSettings = inputImage.getWriterSettings(SetBands=num_pcs)
  outputImage.initWriter, writerSettings
 
 outputDIV = 0
 
 if isa(inputDIV) then begin
  outputDIV = inputDIV
  outputImage.setMeta, 'data ignore value', outputDIV
 endif

 if result.haskey('outputDIV') then begin
  outputDIV = result['outputDIV']
  outputImage.setMeta, 'data ignore value', outputDIV
 endif
    
  while ~inputImage.tileProcessingDone() do begin
    sliceTileData = inputImage.getData()
    if isa(result['inputMask']) then begin
      maskSliceTile = inputMask.getData()
      notValidMaskPixels = where(maskSliceTile eq 0)
    endif
    num_rows = n_elements(sliceTileData[0,*]) / num_cols
    
    if model['kernel'] eq 0 then begin
      centeredSliceTile = sliceTileData - REBIN(model['means'], n_elements(slicetiledata[*,0]), n_elements(slicetiledata[0,*]))
      
;      no = n_elements(slicetiledata[0,*])      ;# of observations
;      means = model['means']
;      xstd = slicetiledata - (means # replicate(1,No))       ;Deviations from means
;      stdev = sqrt(total(xstd^2, 2)/(No-1))
;      sampleCentered = xstd * ((1./stdev) # replicate(1, No))
      
;      slicetileKPC = TRANSPOSE(model['alpha']) # sampleCentered
      slicetileKPC = (transpose(model['alpha'] ## Transpose(centeredSliceTile)))[0:num_pcs-1,*]
    endif
    
    if model['kernel'] eq 1 then begin
      if not gpuKernelProject(alpha,  model['gma'], model['G'], sliceTileData, num_cols, num_rows, sliceTileKPC, center_train=ct) then $
        message, 'projection aborted'   
      sliceTileKPC = transpose(reform(temporary(slicetileKPC), num_cols*num_rows,num_pcs))
    endif
    
    if isa(result['inputMask']) then begin
      sliceTileKPC[*, notValidMaskPixels] = outputDIV
    endif
    outputImage.writeData, sliceTileKPC     
  endwhile

  inputImage.cleanup
  outputImage.cleanup
  progressBar.cleanup
  
end

;+
; :Hidden:
;-
pro test_hubApp_kernelPCA_apply_processing

  ; train a small model
  p = hash()
  settings = hash()
  settings['title'] = 'KernelPCA'
  p['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  p['modelFilename'] = filepath('test.kpc',/TMP)
  p['nscale'] = 1.
  p['randomSamples'] = 150
  p['inputMask'] = hub_getTestImage('Hymap_Berlin-A_Mask')
  p['kernelChoice'] = 0

  
  hubapp_kernelpca_parameterize_processing, p, settings

  ; apply the model
  parameters = hash()
  parameters['outputFilename'] = filepath('kernelPC',/TMP)
  parameters['pcs'] = 10
  parameters['inputFilename'] = p['inputFilename']
  parameters['inputMask'] = p['inputMask']
  parameters['modelFilename'] = p['modelFilename']
  parameters['exclusiveSubframeGroup'] = 0 ;extract defined nr of pcs
 
  hubApp_kernelPCA_apply_processing, parameters, settings
  
  print, 'KPC Image written to ', parameters['outputFilename']
end  
