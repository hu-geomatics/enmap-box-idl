function check_hubApp_kernelPCA_apply_getParameters, resultHash, Message=message

  msg=!NULL
  isConsistent = 1
  restore, FILENAME=resultHash['modelFilename']
 
  ;spectral dimension
  applyImage = hubIOImgInputImage((resultHash['inputSampleSet'])['featureFilename'])
  modelBands = model['num_bands']
  if applyImage.isCorrectSpectralSize(modelBands) ne 1 then begin
    msg = 'Spectral dimensions of the model and the image do not match'
    isConsistent *= 0b
  endif
  
  ;#pc le modelBands?
  if resultHash['exclusiveSubframeGroup'] eq 0 then begin
    num_pcs = resultHash['pcs']
    if num_pcs gt modelBands then begin
      isConsistent*=0b
      msg = '#Principal components > number of bands'
    endif
  endif
  
  message=msg
  return, isConsistent
end

;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided
;    by the` hubAPI` library).
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function hubApp_kernelPCA_apply_getParameters, settings, defaultValues, GroupLeader=groupLeader
  
  tsize=70
  defaultValues = hub_getAppState('hubApp_kernelPCA','stateHash_kPCA',default=hash())
  hubAMW_program, Title='Kernel PCA: Apply Model', groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleSet, 'inputSampleSet', title='Image', /Masking $
  , ReferenceTitle='Mask ', /ReferenceOptional $
  , Value = defaultValues.hubGetValue('inputFilename') $
  , ReferenceValue = defaultValues.hubGetValue('inputMask') $
  , TSize=tsize
  hubAMW_inputFilename,'modelFilename', Title='KPC File', tsize=tsize, Value=defaultValues.hubGetValue('modelFilename')
 
  hubAMW_frame, Title='Parameters'
 
  hubAMW_subframe, 'exclusiveSubframeGroup', Title='Choose number of', /Row, /SetButton 
  hubAMW_parameter, 'pcs', Title='principal components', Value=3, IsGE=1,   IsLE=1000, Size=3,  /Integer 
  hubAMW_subframe, 'exclusiveSubframeGroup', Title='Choose PCs by explained variance', /Row 
  hubAMW_parameter, 'expVariance', Title='variance = ', Value=95, Unit='%', /Integer, Size=3, IsGT=0, IsLE=100
  hubAMW_subframe, 'exclusiveSubframeGroup', Title='Choose all PCs', /Row
    
  hubAMW_frame, Title='Output'
  hubAMW_parameter, 'outputDIV', Title = 'Data Ignore Value', OPTIONAL=2, /Float
  hubAMW_outputFilename, 'outputFilename', Title='Image',$
  Value=filepath('kernelPCs', /TMP), TSize=tSize
 
  amwResult = hubAMW_manage(/FLAT, CONSISTENCYCHECKFUNCTION='check_hubApp_kernelPCA_apply_getParameters')

  if amwResult['accept'] then begin 
    result = amwResult
    result['inputMask']=result['inputSampleSet_labelFilename']
    result['inputFilename']=result['inputSampleSet_featureFilename']
  endif else begin
    result = !NULL
  endelse

  return, result
  
end

;+
; :Hidden:
;-

pro test_hubApp_kernelPCA_apply_getParameters

  settings = hash()
  settings['title'] = 'kernelPCA'
  parameters = hubApp_kernelPCA_apply_getParameters(settings)
  print, parameters

end  
