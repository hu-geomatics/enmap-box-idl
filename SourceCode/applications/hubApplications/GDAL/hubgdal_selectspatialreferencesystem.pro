function hubGDAL_selectSpatialReferenceSystem, settings, defaultValues, GroupLeader=groupLeader
  
  if ~isa(defaultValues) then defaultValues = hash()
  
  gdalWrapper = defaultValues.hubGetValue('gdalWrapper', default=hubGDALWrapper(dirGDAL=defaultValues.hubGetValue('dirGDAL')))
  
  SRSInfo = gdalWrapper.getSpatialReferenceSystems()
  ProjInfo = gdalWrapper.getProjectionsTransformList()
  
  
  
  infoList = list()
  infoLUT = list()
  foreach SRSType, SRSInfo.keys() do begin
    ;TODO: differentiate between GCS, PCS etc.
    n = n_elements(SRSInfo[SRSType])
    lName = max(strlen((SRSInfo[SRSType]).COORD_REF_SYS_NAME))
    lCode = max(strlen((SRSInfo[SRSType]).COORD_REF_SYS_CODE))
    
    
    formatString = '(%"%'+strtrim(lName,2) + 's  %'+strtrim(lCode,2)+'s")'
    
    foreach srs, SRSInfo[SRSType] do begin
      infoList.add, [SRSType, string(format=formatString, srs.COORD_REF_SYS_NAME, srs.COORD_REF_SYS_CODE)]
      infoLUT.add, ptr_new(srs, /NO_COPY)
    endforeach
  endforeach
  
  hubAMW_program,TITLE='Select your SRS'
  input = infoList.toArray(/NO_COPY, /TRANSPOSE)
  hubAMW_treelist, 'index', List=input[0:1,*], Title='My TreeList', Value=1, XSIZE=500, YSIZE=300, Extract=extract, /ShowFilter
  result = hubAMW_manage()
  if result['accept'] then begin
    srS = input[*,result['index']]

  
  
  endif
  
  


end


pro test_hubGDAL_selectSpatialReferenceSystem
 results =  hubGDAL_selectSpatialReferenceSystem()
 print, results
 stop

end