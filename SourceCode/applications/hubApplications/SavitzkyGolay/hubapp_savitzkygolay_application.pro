;+
; :Description:
;    This procedure is the main procedure for the
;    `User-Application: Savitzki-Golay Filter` application.
;    It is implicitly called by the `hubApp_savitzkyGolay_event` event handler procedure,
;    calls the `hubApp_savitzkyGolay_getParameters` function,
;    and the `hubApp_savitzkyGolay_imageProcessing` procedure.
;    
;    For details also see the example section in: `Image Processing Applications <../image-processing-applications.html>`
;
; :Params:
;    groupLeader : in, optional, type=int
;       Use this keyword to specify the group leader for used widgets.
;       
;-
pro hubApp_savitzkyGolay_application $
  , groupLeader

  parameters = hubApp_savitzkyGolay_getParameters(groupLeader)
  if ~isa(parameters) then return
  
  hubApp_savitzkyGolay_imageProcessing, parameters

end
