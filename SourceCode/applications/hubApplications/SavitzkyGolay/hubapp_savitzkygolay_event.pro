;+
; :Description:
;    This procedure is the event handler for the EnMAP-Box Menu button named 
;    `User-Application: Savitzki-Golay Filter` under `Applications -> Digital Filter`.
;    It is implicitly called when clicking on the button, 
;    calls the `hubApp_savitzkyGolay_application` procedure,
;    and catches all errors that orrure during processing.
;    
;    For details also see the example section in: `Image Processing Applications <../image-processing-applications.html>`
;
; :Params:
;    event : in, required, type=button event structure
;
;-
pro hubApp_savitzkyGolay_event $
  ,event
  
  @huberrorcatch

  hubApp_savitzkyGolay_application, event.top

end