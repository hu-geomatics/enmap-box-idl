;+
; :Description:
;    This function is the auto-managed widget program consistency check function used inside
;    the `hubApp_savitzkyGolay_getParameters` function.
;
; :Params:
;    resultHash : in, required, type=hash
;
; :Keywords:
;    Message : out, required, type=string[]
;-
function hubApp_savitzkyGolay_getParameters_consistencyCheck $
  , resultHash $
  , Message=message

  message = list()

  ; check consistency

  ; - 'width' must be odd
 
  if ~(resultHash['width'] mod 2 eq 1) then begin
    message.add, 'Filter width must be an odd number.'
  endif
  
  ; - 'order' must be lower equal 'degree'
  
  if ~(resultHash['order'] le resultHash['degree']) then begin
    message.add, 'Derivative order must be lower than or equal to the polynomial degree.'
  endif
  
  ; - 'degree' must be lower 'width'
  
  if ~(resultHash['degree'] lt resultHash['width']) then begin
    message.add, 'Polynomial degree must be lower than the filter width.'
  endif
  
  ; - 'width must be lower equal number of image bands
  
  inputImage = hubIOImgInputImage(resultHash['inputFilename'])
  if ~(resultHash['width'] le inputImage.getMeta('bands')) then begin
    message.add, 'Filter width must be lower than or equal to the number of image bands.'
  endif

  message = message.toArray()
  isConsistent = n_elements(message) eq 0
  return, isConsistent
  
end

;+
; :Description:
;    This function returns a hash variable that can be used as argument for the `hubApp_savitzkyGolay_imageProcessing` procedure.
;
; :Params:
;    groupLeader: in, optional, type=long
;       Use this keyword to specify the group leader for used widgets.
;
;-
function hubApp_savitzkyGolay_getParameters $
  , groupLeader
  
  ; auto-managed widget program
  
  hubAMW_program, groupLeader, Title='Savitzky Golay Filter'
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputFilename', Title='Image       '
  hubAMW_frame, Title='Filter Parameters'
  hubAMW_parameter, 'width',  Title='filter width      ', Value=5, /Integer, IsGE=3
  hubAMW_parameter, 'degree', Title='polynomial degree ', Value=3, /Integer, IsGE=0
  hubAMW_parameter, 'order',  Title='derivative order  ', Value=1, /Integer, IsGE=0
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputFilename', Title='Filtered Image', Value='savitzky_golay'
  result = hubAMW_manage(ConsistencyCheckFunction='hubApp_savitzkyGolay_getParameters_consistencyCheck')

  ; prepare parameters

  parameters = result['accept'] ? result : !null
  
  return, parameters

end

