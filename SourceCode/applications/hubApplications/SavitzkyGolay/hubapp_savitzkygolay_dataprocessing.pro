;+
; :Description:
;    This function applies Savitzky Golay Filtering to the input data and returns the result. 
;    It is simply a wrapper to the IDL `convol` function.
;
; :Params:
;    data : in, required, type=number[][]
;       Use this argument to specify the input data.
;      
;    kernel : in, required, type=number[]
;       Use this argument to specify the Savitzky Golay filter kernel.
;
;-
function hubApp_savitzkyGolay_dataProcessing $
  , data $
  , kernel

  return, convol(data, kernel, /NORMALIZE)

end