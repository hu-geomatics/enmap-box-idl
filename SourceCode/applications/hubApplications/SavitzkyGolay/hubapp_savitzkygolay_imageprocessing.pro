;+
; :Description:
;    This procedure is used to perform Savitzki-Golay Filtering to an input image and writing the result to an output image.
;    
;    For details also see the example section in: `Image Processing Applications <../image-processing-applications.html>`
;
; :Params:
;    parameters : in, required, type=hash
;      Use this argument to specify the input parameters::
;      
;        hash key          | type   | description
;        ------------------|---------------------------------
;        'inputFilename'   | string | input image filename
;        'outputFilename'  | string | output image filename
;        'width'           | int    | filter kernel size, must be an odd number
;        'degree'          | int    | polynomial degree, must be lower than the filter width
;        'order'           | int    | derivative order, must be lower than or equal to the polynomial degree
;
;-
pro hubApp_savitzkyGolay_imageProcessing $
  , parameters

  ; check parameters

  requiredParameters = ['width', 'order', 'degree', 'inputFilename', 'outputFilename']
  
  if ~isa(parameters.hubIsa(requiredParameters, /EvaluateAND, IndicesFalse=missingParameters)) then begin
    message, 'Missing parameters: '+strjoin(requiredParameters[missingParameters],', ')
  endif

  ; create savitzky golay filter kernel
  
  kernel = savgol(floor(parameters['width']/2), floor(parameters['width']/2), parameters['order'], parameters['degree'])

  ; image processing
  ; - create objects

  inputImage = hubIOImgInputImage(parameters['inputFilename'])
  outputImage = hubIOImgOutputImage(parameters['outputFilename'], NoOpen=parameters.hubGetValue('noOpen', Default=0))

  ; - prepare metadata

  outputImage.copyMeta, inputImage, ['wavelength', 'wavelength units', 'default bands', 'map info']
  outputImage.setMeta, 'description', ['Savitzky Golay Parameters:', reform(parameters._overloadPrint())]

  ; - initialize for reading/writing

  numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)
  inputImage.initReader, numberOfTileLines, /Slice, /TileProcessing, DataType='float'
  outputImage.initWriter, inputImage.getWriterSettings()

  ; - apply filter
                                                                                             
  while ~inputImage.tileProcessingDone() do begin                                        
    sliceTileData = inputImage.getData()
    sliceTileFiltered = hubApp_savitzkyGolay_dataProcessing(sliceTileData, kernel)
    outputImage.writeData, sliceTileFiltered                                             
  endwhile

 ; cleanup

 inputImage.cleanup
 outputImage.cleanup

end