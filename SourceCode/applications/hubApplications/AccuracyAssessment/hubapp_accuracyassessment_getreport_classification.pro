


function hubApp_accuracyAssessment_getReport_Classification_getClassDetailsTable $
  , classNamesN, unit=unit $
  , performanceMean=performanceMean $
  , performanceStdDev=performanceStdDev $
  , performanceStdErr=performanceStdErr $
  , confidenceIntervals=confidenceIntervals
  
  if ~isa(unit) then unit = '%'
  
;  head0 = list()
;  head1 = list('Map Class')
;  formats = list('(%"%s")')
;  floatFormat = '(%"%0.2f")'
;  
;  if isa(performanceMean) then begin
;    head1.add, 'Mean ['+unit+']'
;    formats.add, floatFormat
;  endif
;  
;  if isa(performanceStdDev) then begin
;    head1.add, 'Standard Deviation ['+unit+']'
;    formats.add, floatFormat
;  endif
;  
;  if isa(performanceStdErr) then begin
;    head1.add, 'Standard Error['+unit+']'
;    formats.add, floatFormat
;  endif
;  
;  head0.add,
;  
;  if isa(confidenceIntervals) then begin
;    
;    
;    
;    foreach intervalName, intervalNames do begin
;      head1.add, intervalName
;      head1.add, 2
;    endforeach
;    formats.add, replicate(floatFormat,nIntervals*2), /EXTRACT
;    
;  endif
  
 
  accTable = hubReportTable()
  accTable.addColumn, classNamesN, name='Class'
  if isa(performanceMean) then accTable.addColumn, performanceMean, name= 'Estimate ['+unit+']'
  if isa(performanceStdDev) then accTable.addColumn, performanceStdDev, name = 'Standard Deviation ['+unit+']'
  if isa(performanceStdErr) then accTable.addColumn, performanceStdErr, name = 'Standard Error['+unit+']'
  
  if isa(confidenceIntervals) then begin
    alphas = (confidenceIntervals.keys()).toArray()
    nIntervals = n_elements(alphas)
    intervalNames = string(format='(%"%i \%")', (1.-alphas)*100)

    head0 = (accTable.getHeader())[-1]

    head1 = list(replicate('', accTable.getColumnCount()), /EXTRACT)
    head1.add, 'Confidence Intervals ['+unit+']'
    head1.add, nIntervals*2
    
    foreach alpha, alphas do begin
        values = confidenceIntervals[alpha]
        head0.add, string(format='(%"%i \% Interval")', 100-round(alpha*100))
        head0.add, 2
        accTable.addColumn, values.intervalStart
        accTable.addColumn, values.intervalEnd
    endforeach
    
    accTable.setHeader, list(head1, head0)
  endif
  return, accTable
end


function hubApp_accuracyAssessment_getReport_Classification_getErrorMatrixTable, classNames, confusionMatrix, relative=relative, floats=floats
  ;nClasses = performances['classes'] - 1
  
  nClasses = n_elements(classNames)
  
  errorTable = hubReportTable() 
  headings = list(list(' '        ,'Reference Class', nclasses,'') $
                 ,list('Map class', string(format='(%"(%i)")', indgen(nClasses)+1), 'Sum', /EXTRACT))

  formatsINTEGER =  ['(%"%s")', replicate('(%"%i")', nClasses), '(%"%i")']
  formatsFLOAT100 = ['(%"%s")', replicate('(%"%0.2f")', nClasses), '(%"%0.2f")']
  formatsFLOATINF = ['(%"%s")', replicate('(D0.2)', nClasses), '(D0.2)']
  ;formatsFLOATINF = ['(%"%s")', replicate('(D)', nClasses), '(D)']
  case 1b of 
    keyword_set(relative) : formats = formatsFLOAT100
    keyword_set(floats)   : formats = formatsFLOATINF
    else : formats = formatsINTEGER
  endcase
  errorTable.addEmptyColumns, nClasses+2, formatStrings = formats                
  errorTable.setHeader, headings
  
  sumAll = total(confusionMatrix)
  
  Matrix = keyword_set(relative) ? confusionMatrix / sumAll * 100 : confusionMatrix
  sumEst = total(Matrix,1)
  sumRef = total(Matrix,2)
  
  
  for i=0, nClasses-1 do begin
    errorTable.addRow, List(classNames[i] $
      , Matrix[*,i] $
      , sumEst[i] $
      , /Extract)
  endfor
  errorTable.addSeparator, '-'
  errorTable.addRow, List('Sum', sumRef, total(sumRef), /Extract)
  
  return, errorTable
end

;+
; :Description:
;    Returns a hubReport object that can be used to present the classification performance as returned
;    from a `hubMathIncClassificationPerformance()` object. 
;
; :Params:
;    classificationPerformance: in, required, type = hash
;      Hash with classification performance measures as returned 
;      from `hubMathIncClassificationPerformance().getResult()`
;
; :Keywords:
;    title
;    shortTitle
;    _REF_EXTRA
;
; :Examples:
;
;-
function hubApp_accuracyAssessment_getReport_Classification, classificationPerformance $
  , title=title $
  , shortTitle=shortTitle $
  ,_REF_EXTRA = _extra

  if ~isa(title) then title='Results'
  if ~isa(shortTitle) then shortTitle=title  
  
  htmlHelper = hubReportHTMLHelper()
  report = hubReport(title=title, shortTitle=shortTitle,  _EXTRA=_extra)
  nClasses = classificationPerformance['numberOfClasses']
  nSamples = classificationPerformance['numberOfSamples']
  classNames = classificationPerformance['classNames']
  classNamesN = string(format='(%"(%i) ")', indgen(nClasses)+1) + classNames
  confusionMatrix = classificationPerformance['confusionMatrix']
  sumEst = classificationPerformance['sumEstimation']
  sumRef = classificationPerformance['sumReference']
  
  hasAdjustedSamplingBias   = classificationPerformance['adjustedSamplingBias']
  ;hasAdjustedAreaEstimation = classificationPerformance['adjustedAreaEstimation']
  ;iserrorAdjusted = hasAdjustedSamplingBias
  
  if nSamples gt 0 then begin
    
    CIntervals = classificationPerformance['confidenceIntervals']
    alphas = CIntervals['significanceLevels']
    alphas = alphas[sort(alphas)]
    nIntervals = n_elements(alphas)
    intervalNames = string(format='(%"%i \%")', round((1.-alphas)*100))
    
    report.addHeading, 'Quick Overview', 2
    
    ;QUICK OVERVIEW: overall measures
    
    ;heading = 
    ;formats = ['(%"%s")', '(%"%0.2f")', '(%"%0.2f")','(%"%0.2f")','(%"%0.2f")']
    alpha = 0.05
    accTable = hubReportTable()
    accTable.addEmptyColumns, 5;, formatStrings = ['(%"%s")', '(%"%0.2f")', '(%"%0.2f")','(%"%0.2f")','(%"%0.2f")'] 
    accTable.setHeader, list('Measure','Estimate [%]', 'Standard Error [%]', '95 % Confidence Interval [%]',2)
    keys      = ['overallAccuracy','kappaAccuracy']
    keysTitle = ['Overall Accuracy','Kappa Accuracy']
    foreach key, keys, iKey do begin
       
    accTable.addRow, list(keysTitle[iKey] $
          , classificationPerformance[key] $
          , classificationPerformance[key+'StandardError'] $
          , ((CIntervals[key])[alpha]).intervalStart $
          , ((CIntervals[key])[alpha]).intervalEnd    $
              )   
    endforeach
    report.addHTML, accTable.getHTMLTable(ATTRIBUTESTABLE='border="1"') 
    report.addHTML, '</br>'
    
      
    ;QUICK Overview: class-wise measures (UA / PA)
    keys     = ['userAccuracy','producerAccuracy']
    keyTitle = ["User's Accuracy [%]","Producer's Accuracy [%]"]

    head0 = list(' ')
    head1 = list('Map class')
    
    foreach key, keys, i do begin
      head0.add, keyTitle[i]
      head0.add, 4
      head1.add, list('Estimate', 'Standard Error', '95 % Interval',2), /EXTRACT
    endforeach
    
    accTable = hubReportTable()
    accTable.addColumn, classNamesN
    foreach key, keys, i do begin
      accTable.addColumn, classificationPerformance[key]
      accTable.addColumn, classificationPerformance[key+'StandardError']
      CI = (CIntervals[key])[alpha]
      accTable.addColumn, CI.intervalStart
      accTable.addColumn, CI.intervalEnd
    endforeach
    accTable.setHeader, list(head0, head1)
    
    report.addHTML, accTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
    
        
    ;REFERENCE SAMPLING DESIGN    

    report.addHeading, 'Reference Sampling Design', 2
    if hasAdjustedSamplingBias then begin
      headings = ['DN', 'Stratum', 'Stratum Proportion', 'Sample Proportion', 'Adjustment Weight'] ;n Ref, a/n , Stratum Sample Proportion, Adjustment Weights
      
      
      strataNames = classificationPerformance['strataNames']
      nStrata = n_elements(strataNames)
      
      table = hubReportTable()
      table.addColumn, indgen(nStrata)+1, formatString='(%"%3i")'
      table.addColumn, strataNames
      table.addColumn, classificationPerformance['strataProportions']
      table.addColumn, classificationPerformance['strataSampleProportions']
      table.addColumn, classificationPerformance['strataWeights']
      table.setHeader, headings
      report.addHTML, table.getHTMLTable()
      table.cleanup

    endif else begin
      ;TODO???
    endelse

    
    ;Error Matrices
    report.addHeading, 'Confusion/Error Matrices', 2
    report.addHeading, 'Sample Counts' + (hasAdjustedSamplingBias ? ' (unadjusted)' : ''), 3    
    report.addHTML, string(format='(%"<span style=\"font-size:small\">number of reference samples: %i</br></span>")', nSamples)

    errorTable = hubApp_accuracyAssessment_getReport_Classification_getErrorMatrixTable(classNamesN, classificationPerformance['confusionMatrix'])
    report.addHTML, errorTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')

    if hasAdjustedSamplingBias then begin
      report.addHeading, 'Sample Counts (adjusted for sampling bias)', 3
      errorTable = hubApp_accuracyAssessment_getReport_Classification_getErrorMatrixTable(classNamesN, classificationPerformance['adjustedConfusionMatrix'], /FLOATS)
      report.addHTML, errorTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
    endif
    
    
    report.addHeading, 'Estimated Map Areas', 2
    report.addHeading, 'Map Area Proportions [%]' + (hasAdjustedSamplingBias ? ' (adjusted for map proportions)' : ''), 3
    
    errorTable = hubApp_accuracyAssessment_getReport_Classification_getErrorMatrixTable(classNamesN, classificationPerformance['adjustedConfusionMatrix'], /RELATIVE)
    report.addHTML, errorTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
    
    ;
    ;Areas Matrix relative to total area
    ;
    mapInfo = classificationPerformance.hubGetValue('mapInfo')
    if isa(mapInfo) then begin
      totalArea = classificationPerformance['totalArea']
      pixelArea = mapInfo.sizeX * mapInfo.sizeY
      nMapPixels = totalArea / pixelArea 
      
      
      unitL = IDLUNIT(mapInfo.units, /DECOMPOSE)
      unitA = unitL^2
      
      unitStrL = (unitL.terms)[0].symbol ne '' ? (unitL.terms)[0].symbol : (unitL.terms)[0].name 
      unitStrA = hubReportHTMLHelper.maskHTMLCharacters(unitStrL+'^2')
      unitStrL = hubReportHTMLHelper.maskHTMLCharacters(unitStrL)
      
      
      report.addHeading, 'Map Areas'+(hasAdjustedSamplingBias ? ' (adjusted for map proportions)' : ''), 3
      
      report.addHTML, '<span style="font-size:small">' $
          + string(format='(%"number of map pixels: %i</br>")', nMapPixels) $
          + string(format='(%"pixel size X: %0.2f %s</br>")', mapInfo.sizeX, unitStrL ) $
          + string(format='(%"pixel size Y: %0.2f %s</br>")', mapInfo.sizeY, unitStrL ) $
          + string(format='(%"pixel area: %0.2f %s </br>")', pixelArea, unitStrA) $
          + string(format='(%"map area: %0.2f %s </br>")', totalArea, unitStrA ) $ 
          + '</span>'
      
      ;keys = (classificationPerformance.keys()).toarray()
      ;keys = transpose(keys[sort(keys)])
      
      matrix = classificationPerformance['adjustedConfusionMatrix']
      matrix /= float(total(matrix)) ;relative adjusted confusion matrix
      matrix *= totalArea
      errorTable = hubApp_accuracyAssessment_getReport_Classification_getErrorMatrixTable(classNamesN, matrix , /floats)
      report.addHTML, errorTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
  
      report.addHeading, 'Map Class Areas '+(hasAdjustedSamplingBias ? '(adjusted for map proportions)' : ''), 3
   
        emptyColumn = list(LENGTH=nClasses)
        accTable = hubApp_accuracyAssessment_getReport_Classification_getClassDetailsTable(classNamesN  $
          , unit = unitStrA $
          , CONFIDENCEINTERVALS = CIntervals.hubGetValue('classArea') $
          , PerformanceMean     = classificationPerformance.hubGetValue('classArea', default=emptyColumn) $
         ; , PerformanceStdDev   = classificationPerformance.hubGetValue('classAreaStandardDeviation', default=emptyColumn) $ 
          , PerformanceStdErr   = classificationPerformance.hubGetValue('classAreaStandardError', default=emptyColumn) $ 
          )
          
        report.addHTML, accTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
        report.addHTML, string(format='(%"<span style=\"font-size:small\">n = %i</span>")', nSamples)
      

    endif
    report.addHeading, 'Performance Measures', 2
   
    report.addHeading, 'Overall', 3
   
    
    accTable = hubReportTable()
    head0 = list('',4,'Confidence Intervals [%]', nIntervals*2)
    head1 = list('Measure','Estimate [%]','Standard Deviation [%]', 'Standard Error [%]')
    foreach intervalName, intervalNames do begin
      head1.add, intervalName
      head1.add, 2
    endforeach
    accTable.addEmptyColumns, 4 + n_elements(intervalNames)*2
    accTable.setHeader, list(head0, head1) 
    
    oaMKeys   = ['overallAccuracy', 'kappaAccuracy', 'averageF1Accuracy']
    oaMNames  = ['Overall Accuracy', 'Kappa Accuracy', 'Avg. F1 Accuracy']
    ;oaMSuffix = ['%','','%']
    oaMSuffix = ['','','']
    
    foreach oaMKey, oaMKeys, iOAM do begin
      rowValues = List()
      rowValues.add, oaMNames[iOAM]
      rowValues.add, string(format='(%"%0.2f %s")', classificationPerformance[oaMKey], oaMSuffix[iOAM])
      
      rowValues.add, classificationPerformance.hubgetValue(oaMKey+'StandardDeviation', default='')
      rowValues.add, classificationPerformance.hubgetValue(oaMKey+'StandardError', default='')
      
      oaMConfidenceIntervals = cintervals.hubgetValue(oaMKey)
      foreach alpha, alphas do begin
        if isa(oaMConfidenceIntervals) && oaMConfidenceIntervals.hasKey(alpha) then begin
          oaMCIntervals = oaMConfidenceIntervals[alpha]
          rowValues.add, oaMCIntervals.intervalStart
          rowValues.add, oaMCIntervals.intervalEnd
        endif else begin
          rowValues.add, !NULL
          rowValues.add, !NULL
     
        endelse
        ;rowValues.add, isa(oaMConfidenceIntervals) ? oaMConfidenceIntervals[alpha]: list(!NULL, !NULL), /EXTRACT 
      endforeach
      
      accTable.addRow, rowValues 
    endforeach
    
    report.addHTML, accTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')

    ;CLASS-WISE 
    ;CLASS-WISE OVERVIEW
    report.addHeading, 'Class-wise Overview', 3
    
    head0 = list('','Errors [%]',2, 'Accuracies [%]',4)
    head1 = list('Class','Commission', 'Omission',"User's","Producer's",'F1','Conditional Kappa')
    
    accTable = hubReportTable()
    accTable.addColumn, classNamesN
    accTable.addColumn, classificationPerformance['errorOfCommission']
    accTable.addColumn, classificationPerformance['errorOfOmission']
    accTable.addColumn, classificationPerformance['userAccuracy']
    accTable.addColumn, classificationPerformance['producerAccuracy']
    accTable.addColumn, classificationPerformance['f1Accuracy']
    accTable.addColumn, classificationPerformance['conditionalKappaAccuracy']
    accTable.setHeader, list(head0,head1)
    report.addHTML, accTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
    report.addHTML, '</br>'
    
    ;CLASS-WISE Detailed
    report.addHeading, 'Class-wise Details', 3
    
    classwiseMetricTitles =['Error of Commission','Error of Omission',"User's Accuracy","Producer's Accuracy", 'F1 Accuracy', 'Conditional Kappa Accuracy']
    classwiseMetricKeys =  ['errorOfCommission','errorOfOmission','userAccuracy','producerAccuracy', 'f1Accuracy', 'conditionalKappaAccuracy']
    
    foreach mk, classwiseMetricKeys, iM do begin 
      mkT = classwiseMetricTitles[iM]
      report.addHeading, mkT, 4
      
      accTable = hubApp_accuracyAssessment_getReport_Classification_getClassDetailsTable(classNamesN  $
        ,CONFIDENCEINTERVALS=CIntervals.hubGetValue(mk) $
        ,PerformanceMean   = classificationPerformance[mk] $
        ,PerformanceStdDev = classificationPerformance.hubGetValue(mk+'StandardDeviation') $
        ,PerformanceStdErr = classificationPerformance.hubGetValue(mk+'StandardError') $
        )
        
      report.addHTML, accTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
      report.addHTML, string(format='(%"<span style=\"font-size:small\">n = %i</span>")', nSamples)
   endforeach
    
    
  endif else begin
    report.addMonospace, ' -- no values available for this stratum -- '
  endelse

  return, report
end

;+
; :Hidden:
;-
pro test_hubApp_accuracyAssessment_getReport_Classification
  nClasses = 4
  nSamplesPerLoop = 25
  nLoops = 4
  
  cInc = hubMathIncClassificationPerformance(nClasses)
  for l = 1, nLoops do begin
    cInc.addData,1 > fix((randomu(seed, nSamplesPerLoop)*10) mod nClasses) $
                ,1 > fix((randomu(seed, nSamplesPerLoop)*10) mod nClasses)    
  endfor
  result = cInc.getResult()
  report = hubApp_accuracyAssessment_getReport_Classification(result)
  report.saveHTML, /Show
end