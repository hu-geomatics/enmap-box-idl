
;+
; :Description:
;    This function returns a `hubReportList` showing the performance measures returned from `hubApp_accuracyAssessment_processing`. 
;
; :Params:
;    performances: in, required, type=hash
;     The hash containing performance measures as returned from `hubApp_accuracyAssessment_processing`.
;      
;    settings: in, optional, type = hash
;     Use this hash to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       ------------+--------+---------------
;     
;
; :Keywords:
;    GroupLeader: in, optional, type = widget identifier
;     <DEPRICATED> Please see `settings['groupLeader']` 
;      
;    NoShow: in, optional, type = bool
;     <DEPRICATED> Please see `settings['noShow']`
;     
;    createPlots: in, optional, type = bool
;     Set this to add plots related to the type of accuracy accessment presented in the `performance` hash.
;
;     In case of `performance['accuracyType'] eq 'regression'` a scatter plot and a residual histogram is added to the report.
;     
;     
;
;-
function hubApp_accuracyAssessment_getReport, performances, settings $
    , createPlots=createPlots, GroupLeader=groupLeader, NoShow=noShow
    
  if ~isa(settings) then settings = Hash() 
  
  accuracyType = performances.hubGetValue('accuracyType')
  
  title = performances.hubIsa('inputROIs')? 'Stratified ' : '' 
  title += 'Accuracy Assessment'
  case accuracyType of
    'regression'    : title += ' for Regression'
    'classification': title += ' for Classification'
  endcase
  
  ;restore meta information of input files
  refMeta = performances.hubGetValue('referenceMetaInfo')
  estMeta = performances.hubGetValue('estimationMetaInfo')
   
  reportList = hubReportList(Title=title $
                , GroupLeader=settings.hubgetValue('groupLeader', default=groupLeader))
  title      = 'File Information'
  shorttitle = 'File Information'
  
  if ~settings.hubKeywordSet('noShow') || keyword_set(noshow) then begin
    pBar = reportList.getProgressBar()
    pBar.setTitle, 'Accuracy Assessment'
    pBar.setInfo, 'Create Report...' 

    progress = 2
    if performances.hubIsa('stratifiedPerformances') then progress += n_elements(performances['stratifiedPerformances'])
    pBar.setRange, [0, progress]
    progress = 0
    pBar.setProgress, progress
  endif
  
  rep = reportList.newReport(Title=title, ShortTitle=shorttitle, /NoShow)
  ;rep.addHeading, 'Reference & Estimation File', 1
  
  if performances.hubIsa('model') then begin
        rep.addHeading, 'Model'
        block = List()
        block.add, string(format= '(%"Model: %s")', performances['model'])
        if performances.hubIsa('modelInfo') then begin
          block.add, performances['modelInfo'], /Extract
        endif
        rep.addMonospace, block.toArray()
  endif
  
  rep.addHeading, 'Estimation', 3
  rep.addMonospace, hubApp_getFileInfoBlock(estMeta)


  rep.addHeading, 'Reference', 3
  rep.addMonospace, hubApp_getFileInfoBlock(refMeta)
  
  if performances.hubIsa('referenceStratificationMetaInfo') then begin
    rep.addHeading, 'Reference Sampling Stratification', 3
    rep.addMonospace, hubApp_getFileInfoBlock(performances['referenceStratificationMetaInfo'])
    
  endif
  if performances.hubIsa('maskMetaInfo') then begin
    rep.addHeading, 'Mask File', 3
    rep.addMonospace, hubApp_getFileInfoBlock(performances['maskMetaInfo'])
  endif

  if performances.hubIsa('inputROIs') then begin
    nROIS = n_elements(performances['roiPerformances'])
    rep.addHeading, 'ROI File', 3
    rep.addMonospace, hubApp_getFileInfoBlock(performances['roiMetaInfo'])
  endif  
  
  if accuracyType eq 'classification' then begin
    report = reportList.newReport(Title='Classification Label Overview' $
                                , ShortTitle='Classes', /NoShow)
    
    table = hubReportTable()
    table.addColumn, indgen(refMeta['classes']), name='DN', FORMATSTRING='(%"%3i")'
    table.addColumn, refMeta['class names'], name='Reference', FORMATSTRING='(%"%s")'
    table.addColumn, estMeta['class names'], name='Map / Prediction', FORMATSTRING='(%"%s")'
    report.addHTML, table.getHTMLTable()
    table.cleanup


    if total(refMeta['class names'] ne estMeta['class names']) ne 0 then begin
      report.addHTML, ['<div style="color:red">Please note: </br>' $
                      ,'Difference between estimation and reference class label names.</br>' $
                      ,'All accuracy metrics are calculated by comparing class label numbers.' $
                      ,'Similar or identical classes need to have the same label number.</div>']
    endif
    
    if performances.hubIsa('inputReferenceStratification') then begin
      report.addHTML, '</br>'
      report.addHeading, 'Reference Sampling Design', 4
      report.addHTML, ['<div style="">' $
                      ,'Reference samples were selected using a stratified sampling design.' $
                      ,'This accuracy assessment was adjusted for the sampling probability of each reference value.' $
                      ,'A detailed description can be found in <a href="http://dx.doi.org/10.1016/j.rse.2012.10.031">Olofsson et al. 2013: Making better use of accuracy data in land change studies: Estimating accuracy and area and quantifying uncertainty using stratified estimation</a>.' $
                      ,'</div></br>']
                       

      
      stratMeta = performances['referenceStratificationMetaInfo']
      stratProp = performances['strataProportions']
      strataNames = (stratMeta['class names'])[1:*]

      table = hubReportTable()
      table.addColumn, indgen(stratMeta['classes']-1)+1, name='DN', formatString='(%"%3i")'
      table.addColumn, strataNames, name='Stratum', formatString='(%"%s")'
      table.addColumn, stratProp, name='Stratum Proportion', formatString='(%"%6.2f")'
      report.addHTML, table.getHTMLTable()
      table.cleanup
  
      
    endif

    
  endif
  
  if isa(pBar) then pBar.setProgress, progress++
  
  
  addPlots = keyword_set(createPlots) || keyword_set(addPlots)
 
;  
  ;add standard accuracy assessment results 
  title = 'Results'
  shorttitle = 'Results'
  report = !NULL
  case accuracyType of
    'regression'      : report = hubApp_accuracyAssessment_getReport_Regression(performances['imagePerformance'], Title=title, ShortTitle=shorttitle, /NoShow, AddPlots=AddPlots)
    'classification'  : report = hubApp_accuracyAssessment_getReport_Classification(performances['imagePerformance'], Title=title, ShortTitle=shorttitle, /NoShow)
  endcase
  reportList.addReport, report
  
  if isa(pBar) then pBar.setProgress, progress++
  
  ;normal case -> normal accuracy assessment without stratification
  if performances.hubIsa('roiPerformances') then begin
    iROI = 1
    foreach roiPerformance, performances['roiPerformances'] do begin
      roiName = roiPerformance['roiName']
      shorttitle = string(format='(%"Results ROI %i")', iROI)
      title      = string(format='(%"Results for region of interest (ROI) %i: \"%s\"")', iROI, roiName)
      
      report = !NULL
      case accuracyType of
        'regression'    : report = hubApp_accuracyAssessment_getReport_Regression(roiPerformance, Title=title, ShortTitle=shorttitle, /NoShow)
        'classification': report = hubApp_accuracyAssessment_getReport_Classification(roiPerformance, Title=title, ShortTitle=shorttitle, /NoShow)
        else: message, 'not implemented'
      endcase
      
      reportList.addReport, report
      
      if isa(pBar) then pBar.setProgress, progress++
      
      iROI++
    endforeach
  endif
  
  if isa(pBar) then pBar.cleanup
  
  return, reportList
end

;+
; :Hidden:
;-
pro TEST_HUBAPP_ACCURACYASSESSMENT_GETREPORT
  @huberrorcatch
  p= Hash()
  
  mode = 'classification'
 ; mode = 'regression'
  
  case mode of 
    'classification': begin
        p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-A_Classification-Estimation')
        p['inputReference'] = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth')
      end
    'regression' : begin
        p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-B_Regression-Estimation')
        p['inputReference'] = hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth')
      end
  endcase  
  
  p['inputMask'] = hub_getTestImage('Hymap_Berlin-B_Mask')
  p['inputROIs'] = hub_getTestImage('Hymap_Berlin-Stratification-Image')
  ;p['outputErrorImage'] = 'D:\Sandbox\testErrorImage'
  
  results = hubApp_accuracyAssessment_processing(p)
  
  report = hubApp_accuracyAssessment_getReport(results, /CreatePlots)
  report.SaveHTML, /Show, selectedReports=2
end  
