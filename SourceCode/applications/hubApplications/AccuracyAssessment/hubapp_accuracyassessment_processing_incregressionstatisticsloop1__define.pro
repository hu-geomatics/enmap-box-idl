;+
; This object is used for the incremental calculation of mean values and the range of continuous
;   1. estimated values, 2. their corresponding reference calues and 3. the errors/residuals between
;   estimated and reference values. 
;   
;
; 
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;      
;-
;+
; :Description:
;    Parameter-free constructor.  
;
;-
function hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1::init
  
  self.meanRef = hubMathIncMean()
  self.meanEst = hubMathIncMean()
  self.meanErr = hubMathIncMean()
  
  self.rangeRef = hubMathIncRange()
  self.rangeEst = hubMathIncRange()
  self.rangeErr = hubMathIncRange()
  
  return, 1
end


;+
; :Description:
;    Add estimated values an their corresponding reference values.
;
; :Params:
;    reference: in, required, type=numeric
;    estimation: in, required, type=numeric
;
;-
pro hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1::addData $
  , reference, estimation
  
  errors = reference - estimation
  
  self.hubMathInc::addData, reference, estimation
  
  (self.meanRef).addData, reference
  (self.meanEst).addData, estimation
  (self.meanErr).addData, errors
  
  (self.rangeRef).addData, reference
  (self.rangeEst).addData, estimation
  (self.rangeErr).addData, errors

  
end


;+
; :Description:
;    Returns a hash containing the mean values and ranges of 
;    the reference values, estimation values and the errors between. 
;
;    
;-
function hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1::getResult
  
  results = Dictionary()
  results['numberOfSamples'] = self.hubMathInc::getNumberOfSamples()
  results['meanRef'] = self.meanRef.getResult()
  results['meanEst'] = self.meanEst.getResult()
  results['meanErr'] = self.meanErr.getResult()

  results['rangeRef'] = self.rangeRef.getResult()
  results['rangeEst'] = self.rangeEst.getResult()
  results['rangeErr'] = self.rangeErr.getResult()
  results['rangeRefEst'] = [min([results['rangeRef'], results['rangeEst']], max=v), v]
  return, results
  
end




;+
; :hidden:
;-
pro hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1__define

  struct = {hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1 $
    , inherits hubMathInc $
    , meanRef : obj_new() $
    , meanEst : obj_new() $
    , meanErr : obj_new() $
    , rangeRef: obj_new() $
    , rangeEst: obj_new() $
    , rangeErr: obj_new() $
  }
end

;+
; :hidden:
;-
pro test_hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1
    o = hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1()
    o.addData, [1,2,3,4], [2,3,4,5]
    print, o.getResult()
    print, o.getNumberOfSamples()
end
