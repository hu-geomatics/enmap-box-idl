;+
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-

;+
; :Description:
;    This procedure is the main routine to perform an accuracy assessment. 
;    It opens a widget dialog to collect the required 
;    parameters from the user and starts the accuracy assessment routine 
;    `hubApp_accuracyAssessment_processing`. The results are finally used 
;    to generate a report with `hubApp_accuracyAssessment_getReport`.
;
; :Keywords:
;    GroupLeader: in, required, type=long
;       Use this keyword to specify the group leader for used widgets.
;       
;    Type: in, required, type=string
;     This string defines the type of accuracy assessment that is to perform.
;     The value fo type can be::
;       
;           value        | description
;       -----------------+-------------------------------
;       'regression'     | to compare continuous data
;       'classification' | to compare categorical data
;       -------------------------------------------------
;        
;    
;     
;-
pro hubApp_accuracyAssessment_application $ 
  ,GroupLeader=groupLeader $
  ,Type=type
  settings = Hash('groupLeader',groupLeader)
  ;restore state from previous application calls to retrieve default values
  defaultValues = hub_getAppState('hubApp', 'stateHash_AccuracyAssessment')
  
  parameters = hubApp_accuracyAssessment_getParameters(type, defaultValues, settings)
  if parameters['accept'] then begin
    ;save parameters as new session state hash
    hub_setAppState, 'hubApp', 'stateHash_AccuracyAssessment', parameters

    performance = hubApp_accuracyAssessment_processing(parameters, settings)
    report = hubApp_accuracyAssessment_getReport(performance, settings, /createPlots)
    report.SaveHTML, /show;, selectedReports=[2]
  endif
end

