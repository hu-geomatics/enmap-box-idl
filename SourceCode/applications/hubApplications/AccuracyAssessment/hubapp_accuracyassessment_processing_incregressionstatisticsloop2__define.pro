;+
; :hidden:
; :Description:
;    This object is used to calculate histograms and correlation statistics on
;    contiuous estimated values and their reference.
;
; :Params:
;    Loop1Results: in, required, type=Hash
;       The result hash a returned from `hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2` object.
;
;    nBins: in, optional, type=integer, default=100
;     Number of bins to be used for each histogram.
;
; :Keywords:
;    range: in, optional, type=numeric[2]
;      Min-max range that will be used for the histograms of reference and estimated values.
;
;    nBinsRef: in, optional, type=numeric[2]
;      Min-max range of reference values histogram.
;    
;    nBinsEst: in, optional, type=numeric[2]
;      Min-max range of estimation values histogram.
;
; :Examples:
;
;-
function hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2::init $
      , Loop1Results, nBins $
      , range=range, nBinsRef=nBinsRef, nBinsEst=nBinsEst $
      , savePairs = savePairs
  
    if ~isa(nBins) then nBins = 100
    if ~isa(nBinsRef) then nBinsRef = nBins
    if ~isa(nBinsEst) then nBinsEst = nBinsRef
    if ~isa(nBinsErr) then nBinsErr = nBins
    
    self.returnSamples = keyword_set(savePairs)
    self.dataEst = list()
    self.dataRef = list()
    if isa(range) then begin
      rangeRef = range
      rangeEst = range
    endif else begin
      rangeRef = Loop1Results['rangeRef']
      rangeEst = Loop1Results['rangeEst']
    endelse
    
    self.performance = hubMathIncRegressionPerformance(Loop1Results['meanRef'], Loop1Results['meanEst'])
    self.histRefEst  = hubMathIncHistogram2D(rangeRef, rangeEst $
                                           , nBinsRef, nBinsEst)
    self.histErr     = hubMathIncHistogram(Loop1Results['rangeErr'], nBinsErr)
    
  return, 1
end

;+
; :Description:
;    Add estimated values an their corresponding reference values.
;
; :Params:
;    reference: in, required, type=numeric
;    estimation: in, required, type=numeric
;
;-
pro hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2::addData $
  , reference, estimation
  
  errors = reference - estimation
  
  self.hubMathInc::addData, reference, estimation
  
  (self.performance).addData, reference, estimation
  (self.histRefEst).addData, reference, estimation
  (self.histErr).addData, errors
  
  if self.returnSamples then begin
    self.dataEst.add, estimation
    self.dataRef.add, reference
  endif
  
end

;+
; :Description:
;    Returns a hash with descriptive information on estimation-, reference- and error values::
;     
;     key                 |  type | description
;     --------------------+-------+-----------------
;     errorHistogram      | hash  | histogram hash for error/residual values. See `hubMathIncHistogram().getResult()`
;     RefEstHistogram2D   | hash  | 2D histogram hash for reference and estimation values. See `hubMathIncHistogram2D().getResult()`
;     referenceHistogram  | hash  | histogram hash for reference values. See `hubMathIncHistogram().getResult()`
;     estimationHistogram | hash  | histogram hash for estimated values. See `hubMathIncHistogram().getResult()`
;-
function hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2::getResult

  nSamples = self.hubMathInc::getNumberOfSamples()
  if nSamples le 0 then begin
    results = Dictionary('numberOfSamples', nSamples)
  endif else begin
    results = self.performance.getResult()
    results['errorHistogram'] = self.histErr.getResult()
    results['RefEstHistogram2D'] = self.histRefEst.getResult()
    results['referenceHistogram'] = self.histRefEst.getHistogramX()
    results['estimationHistogram'] = self.histRefEst.getHistogramY()
    if self.returnSamples then begin
      results['dataEst'] = self.dataEst
      results['dataRef'] = self.dataRef
    endif
  endelse
  return, results
  
end




;+
; :hidden:
;-
pro hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2__define

  struct = {hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2 $
    , inherits hubMathInc $
    , performance : obj_new() $
    , histRefEst: obj_new() $
    , histErr : obj_new() $
    , noSamples:1b $
    , returnSamples:0b $
    , dataRef : obj_new() $
    , dataEst : obj_new() $
  }
end

;+
; :hidden:
;-
pro test_hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2
  o = hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2( $
      5, 50 $
    , [0,10], [0,100], [-100,100] $
    , 100 $
  )
  ;o.addData, [1,2,3,4], [2,50,4,70]
  ;o.addData, [2,0,5,2], [9,3,20,5]
  print, o.getResult()
end
