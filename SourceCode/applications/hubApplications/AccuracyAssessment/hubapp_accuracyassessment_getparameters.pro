;+
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
 ;+
; :Description:
;    Widget callback function. Checks if the input of the widget dialog is correct.
;
; :Params:
;    resultHash: in, required, type=hash
;       Hash with output collected from the sub-widget.
;
; :Keywords:
;    Message: out, optional, type=string[]
;     Set this to provide information on occured errors.
;-
;+
           ; :Description:
           ;    Describe the procedure.
           ;
           ; :Params:
           ;    resultHash
           ;
           ; :Keywords:
           ;    Message
           ;    UserInformation
           ;
           ; :Author: geo_beja
           ;-
function hubApp_accuracyAssessment_getParameters_Check, resultHash, Message=message, UserInformation=userInformation
   msg = !NULL
   
   pathRef = resultHash['inputReference']
   pathEst = resultHash['inputEstimation']
   
   imgRef = hubIOImgInputImage(pathRef)
   imgEst = hubIOImgInputImage(pathEst)
   
   ;CHECK REF / EST
   ;check type, number of bands in case of classification is checked implicitely
   
   if userInformation['type'] eq 'classification' then begin
     isClassRef = imgRef.isClassification()
     isClassEst = imgEst.isClassification()
     if ~isClassRef then begin
       msg = [msg, 'Reference file must be of file type "ENVI Classification"']
     endif 
     if ~isClassEst then begin
        msg = [msg, 'Estimation file must be of file type "ENVI Classification"']
     endif
                                    
     if isClassRef and isClassEst then begin
       if imgRef.getMeta('classes') ne imgEst.getMeta('classes') then begin
         msg = [msg, 'Estimation and reference file must have same number of classes.']
       endif 
     endif
   endif

   if imgRef.getMeta('bands') ne 1 OR imgEst.getMeta('bands') ne 1 then begin
    msg = [msg, 'Number of bands must be 1']
   endif
   
   if ~imgRef.isCorrectSpatialSize(imgEst.getSpatialSize()) then begin
    msg = [msg, 'Estimation and reference must have same spatial extent.']
   endif
   
   if userinformation.type eq 'classification' then begin 
     if ~imgRef.isClassification() then  msg = [msg, 'Reference file needs to be of file type "ENVI Classification"']
     if ~imgEst.isClassification() then  msg = [msg, 'Estimated file needs to be of file type "ENVI Classification"']
   endif 
   
   if userinformation.type eq 'regression' then begin
    info  =!NULL
    bnRef = FILE_BASENAME(pathRef)
    bnEst = FILE_BASENAME(pathEst)
    if ~imgRef.isRegression() and ~imgEst.isRegression() then begin
      
      info = ['Please note that neither "'+bnRef+'" nor "'+bnEst+'" has a specified data ignore value (e.g. -9999).' $
             ,'This can lead to errors and wrong image interpretations']
    endif
    if isa(info) then begin
      info = strjoin(info,string(13b))
      print, info
      !NULL = DIALOG_MESSAGE(info, INFORMATION=1)
    endif
   endif
   
   if resultHash.hubIsa('inputMask') then begin
      imgMask = hubIOImgInputImage(resultHash['inputMask'])
      if ~imgMask.isMask() then msg = [msg, 'The mask image must be a valid mask file']        
      if ~imgMask.isCorrectSpatialSize(imgRef.getSpatialSize()) then msg = [msg, 'Mask image has different spatial size than reference image.']
      imgMask.cleanup
   endif

   if resultHash.hubIsa('inputROIs') then begin
      imgROI = hubIOImgInputImage(resultHash['inputROIs'])
      if ~imgROI.isClassification() then begin
        msg = [msg, 'The stratification image must be a classification file']
      endif
      if ~imgROI.isCorrectSpatialSize(imgRef.getSpatialSize()) then msg = [msg, 'Stratification image has different spatial than reference image']
      imgROI.cleanup
   endif
   
   
   imgRef.cleanup
   imgEst.cleanup


   message = msg
   return, ~isa(msg)
 end

function hubApp_accuracyAssessment_getParameters_dialog2_check, results, message=message
  return, 1b
end

function hubApp_accuracyAssessment_getParameters_dialog2, resultHash, settings $
    , title=title 
  
  
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings.hubGetValue('title')
  
  getStrataProportions = resultHash.hubIsa('inputReferenceStratification')
 
  if getStrataProportions then begin
    hubAMW_frame, title='Specify stratification used for stratified sampling of reference values'
    hubAMW_label, ['Reference Stratification: ', resultHash['inputReferenceStratification']]
    
    hubAMW_subframe, 'x', Title='Stratified sampling was done one this stratification.', /SETBUTTON, /COLUMN
    hubAMW_subframe, 'x', Title='Stratified sampling was done on a superset of this stratification.', /COLUMN
    hubAMW_inputImageFilename, 'inputRefSamplingStratificationMap', title='Sampling stratification
  endif
  
  temp = hubAMW_manage(ConsistencyCheckFunction='hubApp_accuracyAssessment_getParameters_dialog2_check')
  
  if ~temp['accept'] then return, Hash('accept', 0b)
  
  results = Hash()
  
;  if estimateClassAreas then begin
;    
;    stop
;  endif
  
  if getStrataProportions then begin
    inputRefSamplingStratificationMap = resultHash['inputReferenceStratification']
    inputRefSamplingStratificationSrc = temp.hubGetValue('inputRefSamplingStratificationMap' $
                                             ,default=resultHash['inputReferenceStratification'])
    
    imgStrataMap = hubIOImgInputImage(inputRefSamplingStratificationMap, /CLASSIFICATION)
  
    imgStrataSrc = hubIOImgInputImage(inputRefSamplingStratificationSrc, /CLASSIFICATION)
    
    nStrataMap = imgStrataMap.getMeta('classes') - 1
    nStrataSrc = imgStrataSrc.getMeta('classes') - 1
    
    strataNamesMap = (imgStrataMap.getMeta('class names'))[1:*]
    strataNamesSrc = (imgStrataSrc.getMeta('class names'))[1:*]
    
    imgStrataMap.cleanup
    
    LUT = replicate({labelMap:-1, labelSrc:-1, count:0l}, nStrataMap)
    LUT.labelMap = indgen(nStrataMap)+1
    foreach labelMap, LUT.labelMAP, i do begin
      mapName = strataNamesMap[i]
      iMatch = where(strcmp(strataNamesSrc, mapName, /FOLD_CASE) eq 1, nMatches, /NULL)
      if nMatches eq 0 then message, string(format='(%"Stratum \"%s\" does not exist in %s")', mapName, inputRefSamplingStratificationSrc)
      if nMatches gt 1 then message, string(format='(%"Redundant stratum/class names in %s")', inputRefSamplingStratificationSrc)
      
      LUT[i].labelSrc = iMatch[0]+1 
      
    endforeach
    
    ;TODO calculate the proportions
    
    totalArea = 0ll
    imgStrataSrc.initReader, hubIOImg_getTileLines(image=imgStrataSrc), /tileProcessing, /SLICE
    while ~imgStrataSrc.tileprocessingDone() do begin
      stratData = imgStrataSrc.getData()
      totalArea += total(stratData gt 0)
      for i=0, nStrataMap-1 do begin
        LUT[i].count = LUT[i].count + total(stratData eq LUT[i].labelSrc)
      endfor
    endwhile
    imgStrataSrc.finishReader
    imgStrataSrc.cleanup
    results['strataProportions'] = double(LUT.count) / totalArea
  endif
  
  return, results
end

;+
; :Description:
;    This function creates a widget that collects parameters required to 
;    perform an accuracy assessment. 
;    
;    The `type` parameter determines whether 
;    the reference and estimation files contain discrete/categorical data (classification results) 
;    or continuous numerical values (regression results).
;
;    The function returns a hash with following keys::
;    
;          key              | type   | description
;       --------------------+--------+---------------------------------------------------
;       inputEstimation     | string | filename of estimated image
;       inputReference      | string | filename of image with reference/grount truth data
;       type                | string | type of assessment to perform. 
;                           |        | can be 'regression' or 'classification'
;       inputMask           | string | filename of a mask image 
;       inputROIs           | string | filename of stratification image 
;       --------------------+--------+-------------------------------------------------
;
;     
; :Params:
;    type: in, required, type=string
;       Use this argument to specify the type of accuracy assessment. 
;       Valide type values are `classification` or `regression`.
;       
;    p: in, optional, type=hash
;       Hash containing widget default values.
;        
; :Keywords:
;    GroupLeader: in, required, type = long
;       Use this keyword to specify the group leader for used widgets.
;-
function hubApp_accuracyAssessment_getParameters,type, p, settings, GroupLeader=groupLeader
  if ~isa(settings) then settings = hash() 
  if ~isa(p) then p = hash() 
  
  if ~settings.hubIsa('title') then begin
    case type of
      'regression'    : settings['title'] = 'Accuracy Assessment for Regression'
      'classification': settings['title'] = 'Accuracy Assessment for Classification'  
    endcase
  endif
  tsize=200
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings.hubGetValue('title')
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputImageFilename, 'inputEstimation', Title='Estimation', tsize=tsize $
                           , value = p.hubGetValue('inputEstimation') 
  hubAMW_inputImageFilename, 'inputReference', Title='Reference', tsize=tsize $
                           , value = p.hubGetValue('inputReference')                                                                             
  hubAMW_inputImageFilename ,'inputMask', Title='Mask', Optional=2, tsize=tsize $
                            , value = p.hubGetValue('inputMask')                                            
  hubAMW_inputImageFilename ,'inputROIs', Title='Regions of Interest', Optional=2, tsize=tsize $
                            , value = p.hubGetValue('inputROIs')
  
  if type eq 'classification' then begin
    ;hubAMW_subframe, 'estimateArea',
    hubAMW_frame, Title='Correct for sampling bias', /ADVANCED
    ;adjustment = p.hubIsa('inputReferenceStratification') ? 1 : 0
    adjustment = 0
    hubAMW_subframe, 'adjustment', Title='Reference Sample is unbiased (e.g. simple random / proportional stratified)', /COLUMN, SETBUTTON=adjustment eq 0
    hubAMW_subframe, 'adjustment', Title='Reference Sample is biased by disproportional stratified random sampling', /COLUMN, SETBUTTON=adjustment eq 1
    hubAMW_inputImageFilename, 'inputReferenceStratification', Title='Reference Stratification', value=p.hubGetValue('inputReferenceStratification')
  end

  hubAMW_frame, Title='Advanced Settings', /Advanced
  if type eq 'regression' then begin
    
    hubAMW_parameter, 'numberOfBins', Title='Number of Histogram Bins' $
                    , IsGE=2, /Integer $
                    , value=p.hubGetValue('numberOfBins', default=256) 
    hubAMW_checkbox,  'equalBins'   , Title='Use same histogram range for all strata' $
                   , value=p.hubGetValue('equalBins', default=1b)
  endif 

  hubAMW_outputFilename, 'outputErrorImage', title='Save Errors', Optional=2 $
                       ,SIZE=521, tsize=tsize $
                       , value=p.hubGetValue('outputErrorImage', default='errorImage')
              
  
  result = hubAMW_manage(ConsistencyCheckFunction='hubApp_accuracyAssessment_getParameters_Check' $
                         , UserInformation = Dictionary('type',type))
  if result['accept'] then begin                          
    if result.hubIsa('inputReferenceStratification') then begin
      result += hubApp_accuracyAssessment_getParameters_dialog2(result, settings)
    endif
    
    result['type'] = type
  endif 
  return, result
  
end

;+
; :Hidden:
;-
pro test_hubapp_accuracyassessment_getparameters
 type= 'regression'
 ;type= 'classification'
 p = Hash()
 
 if type eq 'classification' then begin
   p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-A_Classification-Estimation')
   p['inputReference'] = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth')
   ;p['inputReferenceStratification'] = hub_getTestImage('Hymap_Berlin-Stratification-Image')
   p['inputROIs'] = hub_getTestImage('Hymap_Berlin-Stratification-Image')
   ;p['inputEstimation'] = 'D:\temp\HM\map'
   ;p['inputReference'] = 'D:\temp\HM\validation'
   ;p['inputReferenceStratification'] = 'D:\temp\HM\map'

 endif else begin
   if 0b then begin 
     p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-B_Regression-Estimation')
     p['inputReference'] = hub_getTestImage('Hymap_Berlin-B_Regression-Validation-Sample')
     p['inputROIs'] = hub_getTestImage('Hymap_Berlin-Stratification-Image')
   endif
   
   if 1b then begin
     p['inputEstimation'] = 'G:\temp\temp_bj\malicious_testdata\label_fractions30m_masked.bsq'
     p['inputReference' ] = 'G:\temp\temp_bj\malicious_testdata\svrEstimation_marcel_masked.bsq'

   endif


 endelse
  res = hubApp_accuracyAssessment_getParameters(type, p)
  results = hubApp_accuracyAssessment_processing(res)
  report = hubApp_accuracyAssessment_getReport(results)
  report.saveHTML, savePath, /SHOW ;, SELECTEDREPORTS=[2]
  print, res
end