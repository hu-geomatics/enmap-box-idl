;+
; This routine can be used to perform an accuracy assessment for classification of regression results. 
; 
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-

;+
; :Hidden:
; :Description:
;    Routine to read an image slice-wise. Returns image data by output-keywords.
;
; :Params:
;    imgList: in, required, type=hubIOImgInputImageList
;     Set of images to read the data from-
;
; :Keywords:
;    dataRef: out, type=numeric[]
;     Reference images data values
;     
;    dataEst: out, type=numeric[]
;     Estimation images data values
;     
;    dataMask: out, type=byte[]
;     Mask image data values. All pixels are set to zero where imgRef, imgEst or imgMask is masked by a
;      data ignore value.  
;     
;    dataROIMask: out, type=byte[]
;     Mask as intersection of dataMask and the imgROI data.
;     
;    dataStratification: out, type=byte[]
;     Stratification image values in case imgStrat is specified.
;
;
;-
pro hubApp_accuracyAssessment_processing_readTileData $
  ;input
  , imgList $
  ;output
  , dataRef=dataRef, dataEst=dataEst $
  , dataMask=dataMask $
  , dataROIMask = dataROIMask $
  , dataStratification = dataStratification
   
  data = imgList.getData()
  imgRef = imgList.getImage('imgRef')
  isClassification = imgRef.isClassification()
  dataRef = data.remove('imgRef')
  dataEst = data.remove('imgEst')
  
  nPixels = n_elements(dataRef[0,*])
  dataMask = make_array(nPixels,value=1b, /BYTE)
  
  if data.hasKey('imgMask') then dataMask *= data.remove('imgMask') 
  
  ;consider internal data ignore values
  imgRef = imgList.getImage('imgRef')
  imgEst = imgList.getImage('imgEst')
  divRef = isClassification ? 0 : imgRef.getMeta('data ignore value')
  divEst = isClassification ? 0 : imgEst.getMeta('data ignore value')

  ;consider reference sampling stratification
  if data.hasKey('imgStrat') then begin
    dataStratification = data['imgStrat']

  endif
    
  if isa(divRef) then dataMask *= (dataRef ne divRef)
  if isa(divEst) then dataMask *= (dataEst ne divEst)
  
  dataMask *= FINITE(dataMask)
  dataMask *= FINITE(dataRef)
  dataMask *= FINITE(dataEst)
  
  ;consider ROIs
  if data.hasKey('imgROI') then begin
    dataROIMask = dataMask * data.remove('imgROI')
  endif
  

end


;+
; :hidden:
; :Description:
;    This function runs the accuracy assessment for classification.
;
; :Params:
;    parameters: in, required, type=hash
;     
;    imgList: in, required, type=hubIOImgInputImageList
;
; :Keywords:
;    imgErr: in, optional, type=hubIOImgOutputImage
;     Error image to write the errors to.
;
;
;-
function hubApp_accuracyAssessment_processing_getAccClass, parameters $
  , imgList, imgErr=imgErr
  
  tileLines = parameters['tileLines']
  
  imgEst = imgList.getImage('imgEst')
  imgRef = imgList.getImage('imgRef')
  nClasses = imgRef.getMeta('classes') - 1
  classNames = (imgRef.getMeta('class names'))[1:*]
  classColors = imgRef.getMeta('class lookup')
  
  hasROIs = imgList.hasImage('imgROI')
  if hasROIs then begin
    imgROI = imgList.getImage('imgROI')
    nROIs  = imgROI.getMeta('classes') -1
    roiNames = (imgROI.getMeta('class names'))[1:*]  
  endif
  
  mapInfo = imgEst.getMeta('map info')
  adjustAreaEstimation = 0b
  if isa(mapInfo) then begin
    adjustAreaEstimation = 1b
    mapPixelSize = float(mapInfo.sizeX) * mapInfo.sizeY
    mapUnit = mapInfo.units
  endif
  adjustSamplingBias = imgList.hasImage('imgStrat') 
  if adjustSamplingBias || adjustAreaEstimation then begin
    if adjustSamplingBias then begin
      strataProportions = parameters['strataProportions']
      strataSamplingProportions = strataProportions * 0d
      imgStrat = imgList.getImage('imgStrat')
      nStrata = imgStrat.getMeta('classes') - 1
      strataNames = imgStrat.getMeta('class names')
      if isa(strataNames) then strataNames = strataNames[1:*]
      incHistStrat = hubMathIncHistogram([1, nStrata], nStrata, /INTEGER)
    endif
    
    if adjustAreaEstimation then begin
      incHistMap   = hubMathIncHistogram([1, nClasses], nClasses, /INTEGER)
      if hasROIs then begin
        incHistROIMapList = list()
        for iR = 1, nROIs do begin
          incHistROIMapList.add, hubMathIncHistogram([1, nClasses], nClasses, /INTEGER)
        endfor
    endif
    
    endif
    imgList.initReader, tileLines, /Slice, /TileProcessing
    
    
    
    while ~imgList.tileprocessingDone() do begin
      data = imgList.getData()
      
      if adjustSamplingBias then begin
        dataStrat = data['imgStrat'] * (data['imgRef'] ne 0)
        incHistStrat.addData, dataStrat
      endif
      
      if adjustAreaEstimation then begin
        incHistMap.addData, data['imgEst']  
        if hasROIs then begin
          for iR = 1, nROIs do begin
            dataROI = data['imgEst'] * (data['imgROI'] eq iR)
            (incHistROIMapList[iR-1]).addData, dataROI
          endfor
        endif
      endif
      
    endwhile
    imgList.finishReader
    
    if adjustSamplingBias then begin
      temp = incHistStrat.getResult()
      strataSampleProportions = temp['probabilityDensity']
    endif
    
    if adjustAreaEstimation then begin
      temp = incHistMap.getResult()
      MapClassProportions = temp['probabilityDensity']
      MapTotalArea = temp['numberOfSamples']  * mapPixelSize
      if hasROIs then begin
        ROITotalAreas = list()
        ROIMapClassProportions = list()
        for iR = 1, nROIs do begin
          temp = (incHistROIMapList[iR-1]).getResult()
          ROITotalAreas.add,  temp['numberOfSamples'] * mapPixelSize
          ROIMapClassProportions.add, temp['probabilityDensity']
          
        endfor
      endif
    endif
  endif
  
  
  
  
  incPerf = hubMathIncClassificationPerformance(nClasses $
    , AdjustSamplingBias=adjustSamplingBias, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions, strataNames=strataNames $
    , AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions=mapClassProportions $
    , classNames=classNames, classColors=classColors)
  

  if hasROIs then begin
    incStratPerf = list()
    for i= 1, nROIs do begin
      incStratPerf.add, hubMathIncClassificationPerformance(nClasses, classNames=classNames, classColors=classColors $
        , AdjustSamplingBias=adjustSamplingBias, StrataProportions=strataProportions, StrataSampleProportions=strataSampleProportions, strataNames=strataNames $
        , AdjustAreaEstimation=adjustAreaEstimation, MapClassProportions= adjustAreaEstimation ? ROIMapClassProportions[i-1] : !NULL $
        )
    endfor
  endif
  

  imgList.initReader, tileLines, /SLICE, /TileProcessing
  
  if isa(imgErr) then begin
    writerSettings = imgRef.getWriterSettings(SetBands=1, SetDataType = imgErr.getMeta('data type'))
    imgErr.initWriter, writersettings
  endif
  
  while ~imgList.tileProcessingDone() do begin
    
    
    hubApp_accuracyAssessment_processing_readTileData, imgList $
      , dataRef=dataRef, dataEst=dataEst $ ;output classifcation values
      , dataMask=dataMask, dataROIMask = dataROIMask $;output mask values
      , dataStratification = dataStratification
      
    ;get classification values for non-masked pixels
    iValues = ~adjustSamplingBias? where(dataMask ne 0, /NULL) $
                       : where(dataMask ne 0 and dataStratification ne 0, /NULL) 
                       
    if isa(iValues) then begin
      if ~adjustSamplingBias then begin
        incPerf.addData, dataRef[iValues], dataEst[iValues]
      endif else begin
        incPerf.addData, dataRef[iValues], dataEst[iValues], dataStratification[iValues]
      endelse
    
      if hasROIs then begin
        for iROI = 0, nROIs - 1 do begin
          
          iValues = ~adjustSamplingBias? where(dataROIMask eq (iROI+1), /NULL) $  
                             : where(dataROIMask eq (iROI+1) and dataStratification ne 0, /NULL)
          
          if isa(iValues) then begin
            if ~adjustSamplingBias then begin
              (incStratPerf[iROI]).addData, dataRef[iValues], dataEst[iValues]
            endif else begin
              (incStratPerf[iROI]).addData, dataRef[iValues], dataEst[iValues], dataStratification[iValues]
            endelse
          endif 
        endfor
      endif
    endif

      
    ;write error image
    if isa(imgErr) then begin
      errorData = dataMask ne 0 and dataRef ne dataEst 
      imgErr.writeData, errorData
    endif
      
  endwhile
  
  ;finish Reader and Writers
  imgList.finishReader
  ;hubApp_manageReaders, imgReaders, tileLines, MASKIMAGES=imgMask, /FINISH
  if isa(imgErr) then imgErr.finishWriter
  
  ;read performances
  performances = Hash()
  performances['imagePerformance'] = incPerf.getResult(totalArea = maptotalarea)
  if isa(mapInfo) then (performances['imagePerformance'])['mapInfo'] = mapInfo
  if hasROIs then begin
    roiPerformances = list()
    for iROI = 0, nROIs -1 do begin
      roiTotalArea = adjustAreaEstimation ? ROITotalAreas[iROI] : !NULL
      roiPerformance = incStratPerf[iROI].getResult(totalArea = roiTotalArea)
      roiPerformance['roiName'] = roiNames[iROI]
      if isa(mapInfo) then roiPerformance['mapInfo'] = mapInfo
      roiPerformances.add, roiPerformance
      
    endfor
    performances['roiPerformances'] = roiPerformances
  endif 
    
  return, performances
end



;+
; :hidden:
; :Description:
;    Uses all input images to run the accuracy assessment for regression.
;    
; :Params:
;    parameters
;    imgList
;
; :Keywords:
;    imgErr
;
;-
function hubApp_accuracyAssessment_processing_getAccRegr, parameters $
  , imgList, imgErr=imgErr

  tileLines = parameters['tileLines']
  hasROIs = imgList.hasImage('imgROI')
  
  pBar = parameters.hubGetValue('progressbar')
  if isa(pBar) then begin
    pBarProgress = 0
    pBar.setRange, [0, parameters['lines'] / tileLines * 2] 
    
  endif
  
  if hasROIs then begin
    imgROI = imgList.getImage('imgROI')
    nROIs = imgROI.getMeta('classes') -1
    roiNames = (imgROI.getMeta('class names'))[1:*]
  endif 
  
  ;
  ;LOOP 1: get basic statistics for reference, estimation and errors + write error image
  ;
  incStatsLoop1 = hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1()
  if hasROIs then begin
    incStratifiedStatsLoop1 = list()
    for i=1, nROIs do begin
      incStratifiedStatsLoop1.add, hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop1()
    endfor
  endif
  
  ;init readers & writers for the first loop
  imgList.initReader, tileLines, /Slice
  if isa(imgErr) then begin
    writerSettings = imgRef.getWriterSettings(SetBands=1, SetDataType = imgErr.getMeta('data type'))
    imgErr.initWriter, writersettings
  endif
  
  
  
  while ~imgList.tileProcessingDone() do begin
      
      hubApp_accuracyAssessment_processing_readTileData, imgList $
        , dataRef=dataRef, dataEst=dataEst $ ;output classifcation values
        , dataMask=dataMask, dataROIMask = dataROIMask ;output mask values

      iValues = where(dataMask ne 0, /NULL)
      if isa(iValues) then begin
        incStatsLoop1.addData, dataRef[iValues], dataEst[iValues]
      endif
      
      ;get values for stratified pixels
      if hasROIs then begin
        for iROI = 0, nROIs - 1 do begin
          iValues = where(dataROIMask eq (iROI+1), /NULL)
          if isa(iValues) then (incStratifiedStatsLoop1[iROI]).addData, dataRef[iValues], dataEst[iValues]
        endfor
      endif 
        
      ;write error values
      if isa(imgErr) then begin
        errorData = (dataEst - dataRef) * dataMask
        imgErr.writeData, errorData
      endif
      
      if isa(pBar) then pBar.setProgress, pBarProgress++
  endwhile
  
  ;finish Reader and Writers
  imgList.finishReader
  ;hubApp_manageReaders, imgReaders, tileLines, MASKIMAGES=imgMask, /FINISH
  if isa(imgErr) then imgErr.finishWriter
  
  
  
  ;
  ;LOOP 2: get regression performance and histograms for reference, estimationa and error values
  ;
  nBins = parameters.hubgetValue('numberOfBins', default = 256) > 5
  resultsLoop1 = incStatsLoop1.getResult()
  
  ;define histogram ranges
  useEqualBins = parameters.hubKeywordSet('equalBins') 
  returnData = parameters.hubGetValue('returnData', default = resultsLoop1['numberOfSamples'] le 5.000)
  incStatsLoop2 = hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2( $
                      resultsLoop1, nBins $
                    , savePairs= savePairs $
                    )
  
  if hasROIs then begin
    incStratifiedStatsLoop2 = list()
    for i=0, nROIs -1 do begin
        stratumResultsLoop1 = (incStratifiedStatsLoop1[i]).getResult()
        incStratifiedStatsLoop2.add, hubApp_AccuracyAssessment_processing_IncRegressionStatisticsLoop2( $
             stratumResultsLoop1, nBins $
           , savePairs= savePairs $
           , range = useEqualBins ?   resultsLoop1['rangeRefEst'] : stratumResultsLoop1['rangeRefEst'] $
           )
    endfor
  endif
  
  ;run TILE_LOOP 2
  ;hubApp_manageReaders, imgReaders, tileLines, MASKIMAGES=imgMask, /INIT, /SLICE
  imgList.initReader, tileLines, /Slice
  while ~imgList.tileProcessingDone() do begin

    hubApp_accuracyAssessment_processing_readTileData, imgList $
      , dataRef=dataRef, dataEst=dataEst $ ;output classifcation values
      , dataMask=dataMask, dataROIMask = dataROIMask ;output mask values


    ;get values for unmasked pixels
    iValues = where(dataMask ne 0, /NULL)
    if isa(iValues) then incStatsLoop2.addData, dataRef[iValues], dataEst[iValues]
    
    ;get values for stratified pixels
    if hasROIs then begin
      for iROI = 0, nROIs - 1 do begin
        iValues = where(dataROIMask eq (iROI+1), /NULL)
        if isa(iValues) then (incStratifiedStatsLoop2[iROI]).addData, dataRef[iValues], dataEst[iValues]
      endfor
    endif    
    
    if isa(pBar) then pBar.setProgress, pBarProgress++
    
  endwhile
  
  imgList.finishReader
  
  ;create empty result hash
  results = Hash()
  results['imagePerformance'] = incStatsLoop2.getResult()
  if hasROIs then begin
    roiPerformances = list()
    for iROI = 0, nROIs -1 do begin
      roiPerformance = (incStratifiedStatsLoop2[iROI]).getResult()
      roiPerformance['roiName'] = roiNames[iROI]
      roiPerformances.add, roiPerformance
    endfor
    results['roiPerformances'] = roiPerformances
  endif 
  
  return, results
end



;+
; :Description:
;    This function performs an accuracy assessment. 
;
; :Params:
;    parameters: in, required, type=hash
;     This has is used for parameterization of the function::
;     
;       key                   | type    | description
;       ----------------------+---------+--------------------
;         inputReference      | string  | filename of reference image 
;         inputEstimation     | string  | filename of estimated image
;       * inputMask           | string  | filename of mask image
;       * inputROIs           | string  | filename of stratification image
;       * outputErrorImage    | string  | filename of an image that shows the estimation errors
;       * equalBins           | boolean | set on true to use euqal bins and ranges for all histogram 
;                             |         | accross all strata
;       * numberOfBins        | int     | number of bins per variable to produce the 2D histogram of errors
;                             |         | default: 256
;       * returnData          | boolean | set on true to return the sample pairs of estimation and reference
;       ----------------------+---------+----------------------
;       * = optional
;    
;    settings: in, optional, type=hash
;     Use this hash to provide GUI relevant parameters.
; 
; :Returns: type=hash
;   The returned hash contains the following information (mask / stratification is optional)::
;   
;     key                    | type   | description
;     -----------------------+--------+-----------------
;     inputEstimation        | string | estimation image path   
;     inputReference         | string | reference image path
;     accuracyType           | string | 'regression' or 'classification'
;     performance            | hash   | classification/regression performance hash
;     referenceMetaInfo      | hash   | meta infos related to inputReference
;     estimationMetaInfo     | hash   | meta infos related to inputEstimation
;     -----------------------|--------|---------------------------
;     inputMask              | string | mask image path
;     maskMetaInfo           | hash   | meta infos related to inputMask
;     -----------------------|--------|---------------------------------
;     inputROIs              | string | stratification image path
;     roiMetaInfo            | hash   | meta infos related to inputROIs
;     roiPerformances        | list   | list of classification/regression hashes, one for each stratum
;     -----------------------+--------+-----------------------------
;     
;  Depending on the accuracy type, a single performance hash contains the values returned by
;  the `hubMathIncClassificationPerformance()` or `hubMathIncRegressionPerformance()` object, respectively.
;  
;  In case of regressions accuracy assessments, additionall following values are provided::
;
;     key                 |  type | description
;     --------------------+-------+-----------------
;     errorHistogram      | hash  | histogram hash for error/residual values. See `hubMathIncHistogram().getResult()`
;     RefEstHistogram2D   | hash  | 2D histogram hash for reference and estimation values. See `hubMathIncHistogram2D().getResult()`
;     referenceHistogram  | hash  | histogram hash for reference values. See `hubMathIncHistogram().getResult()`
;     estimationHistogram | hash  | histogram hash for estimated values. See `hubMathIncHistogram().getResult()`
;  
;
; :Keywords:
;    noShow: in, optional, type=boolean
;     <DEPRICATED, use `settings`> 
;     
;     Set this keyword to avoid showing a progressbar while the accuracy assessment is performed.
;     
;     Use this keyword to specify the group leader for used widgets.
;-
function hubApp_accuracyAssessment_processing, parameters, settings $
    , noShow=noShow

  ;check required parameters 
  hubApp_checkKeys, parameters, ['inputReference', 'inputEstimation']
  
  ;copy parameters hash for internal use
  par = Hash()+parameters
  if ~isa(settings) then settings = hash()
  par['returnData'] = parameters.hubGetValue('returnData', default=0)
  
  ;init the progressbar
  if ~settings.hubKeywordSet('noShow') || ~keyword_set(noShow) then begin
    progressbar = hubProgressbar(title='Accuracy Assessment' $
                  , GroupLeader=settings.hubgetValue('groupLeader') $
                  , info='Please wait...')
    par['progressbar'] = progressbar
  endif
  
  
  imgList = hubIOImgInputImageList()
  imgRef = imgList.addImage(par['inputReference'] , 'imgRef')
  imgEst = imgList.addImage(par['inputEstimation'], 'imgEst')

  if imgRef.isClassification() ne imgEst.isClassification() then message, 'Reference and estimation must have same file type'
  accuracyType = imgRef.isClassification() ? 'classification' : 'regression'
  par['lines'] = imgRef.getMeta('lines')
  par['samples'] = imgRef.getMeta('samples')
  
  if accuracyType eq 'classification' && par.hubIsa('inputReferenceStratification') then begin
    imgStrat = imgList.addImage(par['inputReferenceStratification'], 'imgStrat', /CLASSIFICATION)
    hubApp_checkKeys, parameters, ['strataProportions'] 
  endif
  
  if par.hubIsa('inputMask') then imgMask = imgList.addImage(par['inputMask'], 'imgMask', /Mask)
  if par.hubIsa('inputROIs') then begin
    imgROI = imgList.addImage(par['inputROIs'], 'imgROI' , /Classification)
    par['nROIs']     = imgROI.getMeta('classes')-1
    par['roi names'] = (imgROI.getMeta('class names'))[1:*]
  endif
  
  nBands = 0
  foreach k, imgList.ImageKeys() do begin
    img = imgList.getImage(k)
    
    nBands += img.getMeta('bands')
  endforeach
  
  if ~par.hubIsa('tileLines') then begin
    par['tileLines'] = hubIOImg_getTileLines( $
                          imgRef.getMeta('samples') $
                        , nBands $
                        , imgRef.getMeta('data type'))
  endif
  

  
  if parameters.hubIsa('outputErrorImage') then begin
    imgErr = hubIOImgOutputImage(par['outputErrorImage'])
    imgErr.copyMeta, imgRef, /CopySpatialInformation
    imgErrDataType = accuracyType eq 'classification' ? 1 : 4
    imgErr.setMeta, 'data type', imgErrDataType
  endif
  
  performances = Hash() + parameters
  case accuracytype of
      'classification' : begin
            performances += hubApp_accuracyAssessment_processing_getAccClass(par $
                              , imgList, imgErr=imgErr)          
                         end
      'regression' : begin
        ;add some default settings
        par['numberOfBins'] = parameters.hubGetValue('numberOfBins', default=256)
        par['equalBins'] = parameters.hubGetValue('equalBins', default=0)
        
        performances += hubApp_accuracyAssessment_processing_getAccRegr(par $
          , imgList, imgErr=imgErr)
          end
  endcase

 
  ;provide additional information
  performances['accuracyType'] = accuracyType
  toCopy =  ['samples', 'lines','data type', 'file type', 'class names', 'classes','class colors'] 
  
  
  performances['referenceMetaInfo'] = imgRef.getMeta()
  performances['estimationMetaInfo'] = imgEst.getMeta()
  if isa(imgStrat) then begin
    performances['referenceStratificationMetaInfo'] = imgStrat.getMeta()
    (performances['referenceStratificationMetaInfo'])['strataProportions'] = parameters['strataProportions']
  endif
  if isa(imgMask) then performances['maskMetaInfo'] = imgMask.getMeta()
  if isa(imgROI) then performances['roiMetaInfo'] = imgROI.getMeta()
  
  imgList.cleanup
  if isa(progressbar) then progressbar.cleanup
  
  
  return, performances
end

;+
; :hidden:
;-
pro test_hubApp_accuracyAssessment_processing
 @huberrorcatch
  mode = 'classification'
  mode = 'regression'
  stratified = 1b
  p= Hash()

  if mode eq 'classification' then begin
    p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-A_Classification-Estimation') 
    p['inputReference' ] = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth')
    p['inputReference' ] = hub_getTestImage('Hymap_Berlin-A_Classification-Validation-Sample')
    if stratified then begin
      p['inputReferenceStratification' ] = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth');hub_getTestImage('Hymap_Berlin-Stratification-Image')
      p['strataProportions']             = [0.6, 0.1, 0.1, 0.1, 0.1]
    endif
    ;p['strataProportions']             = [0.25, 0.6, 0.15]
    
  endif else begin
    
    if 0b then begin
      p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-B_Regression-Estimation')
      p['inputReference' ] = hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth')
      if stratified then p['inputReferenceStratification' ] = hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth');hub_getTestImage('Hymap_Berlin-Stratification-Image')
      p['equalBins']=1;
    endif
    
    if 1b then begin
      p['inputEstimation'] = 'G:\temp\temp_bj\malicious_testdata\label_fractions30m_masked.bsq'
      p['inputReference' ] = 'G:\temp\temp_bj\malicious_testdata\svrEstimation_marcel_masked.bsq'
      
    endif
  endelse
  
 ;p['inputMask'] = hub_getTestImage('Hymap_Berlin-Stratification-Image')
  
  ;p['numberOfBins'] = 256
 
  
  
  results = hubApp_accuracyAssessment_processing(p)
  
  report = hubApp_accuracyAssessment_getReport(results)
 ; savePath = DIALOG_PICKFILE(TITLE="Save to...",FILTER="*.html", FILE="AAReport.html")
  report.saveHTML, savePath, /SHOW, SELECTEDREPORTS=[2]

end  
  