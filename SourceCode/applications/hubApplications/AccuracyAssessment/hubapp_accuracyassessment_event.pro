;+
; :hidden:
; :Description:
;    Use this event handler to call the accuracy assessment application from an EnMAP-Box menu button.
;    Do not forget to provide the type of accuracy assessment when calling this event.
;    
; :Params:
;    event: in, required, type=button event structure
; 
; :EXAMPLES:
;   This is how an entry in your enmap.men file could look like::
;     
;     ;<level> {<Button Name>}    {<arguments>}     {<event handler>}
;     0 {My Sample Button}
;     1 {Classification Accuracy} {classification}  {hubApp_accuracyAssessment_Event}
;     1 {Regression Accuracy}     {regression}      {hubApp_accuracyAssessment_Event}
; 
;-
pro hubApp_accuracyAssessment_Event, event
  
  @huberrorcatch
  menueEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  hubApp_accuracyAssessment_application,GROUPLEADER=event.top, TYPE=menueEventInfo.argument
end