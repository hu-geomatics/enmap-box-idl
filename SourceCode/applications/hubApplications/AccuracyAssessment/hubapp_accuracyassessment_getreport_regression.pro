;+
; :Author: geo_beja
; 
; 
;-
;+
; :Description:
;    Returns a hubReport object that can be used to present the regression performance as returned
;    from a `hubMathIncRegressionPerformance()` object. 
;
; :Params:
;    regressionPerformance: in, required, type=hash
;       Hash with regression performance measures as returned from `hubMathIncRegressionPerformance().getResult()` 
;       
;       
;
; :Keywords:
;    title: in, optional, type=string
;       Title of the report.
;       
;    shortTitle: in, optional, type=string
;       Short form of title to be used in a reports navigation bar.
;       
;    _REF_EXTRA: in, optional
;       Optional arguments.
;
; :Examples:
;
;-
function hubApp_accuracyAssessment_getReport_Regression, regressionPerformance $
  , title=title $
  , shortTitle=shortTitle $
  ,_REF_EXTRA = _extra
  
  if ~isa(title) then title='Results'
  if ~isa(shortTitle) then shortTitle=title
  
  buffer = 1b
  report = hubReport(title=title, shortTitle=shortTitle, _EXTRA=_extra)
  
  nSamples = regressionPerformance['numberOfSamples']
  
  
  metricTable = hubReportTable()
  metricTable.addColumn, name='Metric', FORMATSTRING='(%"%s")'
  metricTable.addColumn, name='Abbr.', FORMATSTRING='(%"%s")'
  metricTable.addColumn, name='Value', FORMATSTRING='(%"%s")'
  metricTable.addRow, List('Number of sample pairs'    ,'n', strtrim(nSamples,2))
  

  if nSamples gt 0 then begin
    metricTable.addRow, List('Mean absolute error'    ,'MAE', strtrim(regressionPerformance['meanAbsoluteError'],2))
    metricTable.addRow, List('Mean squared error'     ,'MSE', strtrim(regressionPerformance['meanSquaredError'],2))
    metricTable.addRow, List('Root mean squared error','RMSE', strtrim(regressionPerformance['rootMeanSquaredError'],2))
    metricTable.addRow, List('Pearson correlation'    ,'r', string(format='(%"%6.2f")', regressionPerformance['pearsonCorrelation']))
    metricTable.addRow, List('Squared pearson correlation'    ,'r^2', string(format='(%"%6.2f")', regressionPerformance['squaredPearsonCorrelation']))
    metricTable.addRow, List('Nash-Sutcliffe efficiency'    ,'NSE', string(format='(%"%6.2f")', regressionPerformance['nashSutcliffeEfficiency']))
  endif else begin
    metricTable.addRow, List('Mean absolute error'    ,'MAE', '---')
    metricTable.addRow, List('Mean squared error'     ,'MSE', '---')
    metricTable.addRow, List('Root mean squared error','RMSE', '---')
    metricTable.addRow, List('Pearson correlation'    ,'r', '---')
    metricTable.addRow, List('Squared pearson correlation'    ,'r^2', '---')
    metricTable.addRow, List('Nash-sutcliffe efficiency'    ,'NSE', '---')    
  endelse
  
  
  
  report.addHTML, metricTable.getHTMLTable(ATTRIBUTESTABLE='border="1"', caption='Performance metrics')

  nSamples = regressionPerformance['numberOfSamples']
  
  if nSamples eq 0 then begin
    report.addHTML, '<p style="color:red">No valid pixels-pairs found.</p>'
  endif
  
  if nSamples gt 0 and regressionPerformance.hubCheckKeys( $
        ['referenceHistogram' $
        ,'estimationHistogram' $
        ,'RefEstHistogram2D']) then begin
          
        histRef = regressionPerformance['referenceHistogram']
        histEst = regressionPerformance['estimationHistogram']
        hist2D  = regressionPerformance['RefEstHistogram2D']
        
        table = hubReportTable()
        table.addColumn, name='Axis', FORMATSTRING='(%"%s")'
        ;table.addColumn, name='Image', FORMATSTRING='(%"%s")'
        table.addColumn, name='Bins', FORMATSTRING='(%"%s")'
        table.addColumn, name='Min', FORMATSTRING='(%"%s")'
        table.addColumn, name='Max', FORMATSTRING='(%"%s")'
        ;table.addRow, List('Number of sample pairs'    ,'n', strtrim(nSamples,2))
        table.addRow, List('X - Reference', histRef['numberOfBins'], (histRef['range'])[0], (histRef['range'])[1])
        table.addRow, List('Y - Estimation', histEst['numberOfBins'], (histEst['range'])[0], (histEst['range'])[1])
        report.addHTML, '<p>'
        report.addHTML, table.getHTMLTable(ATTRIBUTESTABLE='border="1"', caption='Histogram settings')
        report.addHTML, '</p>'
        width = 700
        height= 700
        
        info = list()
        

        
        plotPosMain = [0.25 ,0.1 ,0.75 , 0.6 ] ;main plot
        plotPosRef  = [0.25 ,0.65,0.75 , 0.8 ] ;reference density
        plotPosEst  = [0.80 ,0.1 ,0.95,0.6 ] ;estimation density
        plotPoscBar = [0.1  ,0.1 ,0.15,0.5]  ;color bar
   
        
        cntMax = max([histEst['histogram'],histRef['histogram']])
        rangeXY = [min([hist2D['rangeX'], hist2D['rangeY']], max=m), m]
        

        w = WINDOW(WINDOW_TITLE="Regression Accuracy" $
                  , DIMENSIONS=[width,height] $
                  , BUFFER = buffer)

        p = BARPLOT(histRef['binStart'], histRef['histogram'] $
            , position=plotPosRef $
            , /HISTOGRAM $
            ;, YRANGE=[0, cntMax] $
            , XRANGE=rangeXY $
            , AXIS_STYLE=1 $
            , COLOR='dark blue', FILL_COLOR='blue' $
            , XSTYLE=1, YSTYLE=0 $
            , YMAJOR = 3, YMINOR = -1,  YTICKFORMAT='(%"%i")' $
            , XTICKDIR=1 $
            , XTICKLAYOUT = 1 $
            , XSHOWTEXT=0 $
            , /CURRENT $
            )

        !NULL = BARPLOT(histEst['binStart'], histEst['histogram'] $
          , position=plotPosEst $
          ;, XRANGE=[0,cntMax] $
          , YRANGE=rangeXY $
          , XSTYLE=0, YSTYLE=1 $
          , XMAJOR=3, XMINOR=-1, XTICKFORMAT='(%"%i")' $
          , COLOR='dark blue', FILL_COLOR='blue' $
          , YSHOWTEXT = 0 $
          , YTICKDIR=1 $
          , YTICKLAYOUT = 1 $
          , /CURRENT, /HORIZONTAL $
          , AXIS=1 $
          )

       
        ctable = COLORTABLE(13, /TRANSPOSE)
        ctable[*,0] = 255b
        
        data2D = hist2D['histogram2D']
        
        vMin = min(data2D[where(data2D ne 0)], max=vMax)
        img = image(data2D $
          , hist2D['binStartX'] $
          , hist2D['binStartY'] $
          , RGB_TABLE=ctable $
          , AXIS_STYLE = 1 $
          , XRANGE=rangeXY, YRANGE=rangeXY $
          , POSITION=plotPosMain $
          , INTERPOLATE = 0, CLIP=0 $
          , /CURRENT $
          )
          
        img.POSITION = plotPosMain
        cBar = COLORBAR(TARGET = img $
                        , ORIENTATION = 1 $
                        , TITLE='Counts' $
                        , POSITION = plotPosCBar)
        !NULL = plot(rangeXY,rangeXY, color='black' $
                    , /CURRENT, /OVERPLOT, AXIS_STYLE=2 $
                    , XTITLE='Reference' $
                    , YTITLE='Estimation')

        report.addImage, w, 'Histogram Reference vs. Estimation'
                       
   

  endif
  
  if nSamples gt 0 and regressionPerformance.hubCheckKeys('errorHistogram') then begin
    w = WINDOW(WINDOW_TITLE="Regression Residuals" $
      , DIMENSIONS=[700,700] $
      , BUFFER = buffer)
    plotPos = [0.2 ,0.2 ,0.8 , 0.8 ] ;main plot
    errorHist = regressionPerformance['errorHistogram']
    
    maxRange = max(abs([ min(errorHist['binStart']) $
                       , max(errorHist['binEnd'])  $
                       ])) * [-1,1]
    bp = BARPLOT(errorHist['binCenter'], errorHist['histogram']  $
      , XRANGE=maxRange $
      , COLOR='dark blue', FILL_COLOR='blue' $
      , XTITLE='Residuals' $
      , YTITLE='Counts' $
      , XTICKFORMAT='(%"%i")', YTICKFORMAT='(%"%i")' $
      , Position=plotPos $
      , /CURRENT $
      )
     
     !NULL = plot([0,0],bp.YRANGE, color='black',LINESTYLE='-', /OVERPLOT, /CURRENT)
     report.addImage, w, 'Histogram Residuals'

  endif
   

  return, report
end

;+
; :Hidden:
;-
pro test_hubApp_accuracyAssessment_getReport_Regression
  nSamplesPerLoop = 25
  nLoops = 4
  
  cInc = hubMathIncRegressionPerformance(0., 0.)
  for l = 1, nLoops do begin
    cInc.addData, randomu(seed, nSamplesPerLoop) $
                , RANDOMN(seed, nSamplesPerLoop)
  endfor
  result = cInc.getResult()
  report = hubApp_accuracyAssessment_getReport_Regression(result, /ADDPLOTS)
  report.saveHTML, /Show

end

;+
; :Hidden:
;-
pro test_hubApp_accuracyAssessment_getReport_Regression2
  @huberrorcatch
  mode = 'regression'
  p= Hash()
  
  p['inputEstimation'] = hub_getTestImage('Hymap_Berlin-B_Regression-Estimation')
  p['inputReference' ] = hub_getTestImage('Hymap_Berlin-B_Regression-GroundTruth')
  p['equalBins']=1;
  
  
  img = hubIOImgInputImage(p['inputEstimation'])
  m = img.getMeta()
  
  results = hubApp_accuracyAssessment_processing(p)
  
  report = hubApp_accuracyAssessment_getReport(results)
  report.saveHTML, /SHOW

end