;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `hubApp_ChangeClassColors_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `hubApp_ChangeClassColors_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `hubApp_ChangeClassColors_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubApp_ChangeClassColors_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  settings['title'] = 'Change Class Colors'
  state = hub_getAppState('hubApp', 'stateHash_ChangeClassColors')
  default = isa(state) ? state : Hash() 
  parameters = hubApp_ChangeClassColors_getParameters(default, settings)
  
  if parameters['accept'] then begin
    hub_setAppState, 'hubApp', 'stateHash_ChangeClassColors', parameters
    hubApp_ChangeClassColors_processing, parameters, settings
    
  endif
  
end

;+
; :Hidden:
;-
pro test_hubApp_ChangeClassColors_application
  applicationInfo = Hash()
  hubApp_ChangeClassColors_application, applicationInfo

end  
