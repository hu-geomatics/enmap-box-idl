;+
; :Author: Benjamin Jakimow
;-
function hubApp_ChangeClassColors_getParameters2, parameters1, settings
  imgClassOld = hubIOImgInputImage(parameters1['inputClassification'])
  nClassesOld = imgClassOld.getMeta('classes')
  classNamesOld = imgClassOld.getMeta('class names') 
  classLookupOld = imgClassOld.getMeta('class lookup')
  
  classLookupNew = classLookupOld
  
  if parameters1.hubIsa('referenceClassification') then begin
    imgClassRef = hubIOImgInputImage(parameters1['referenceClassification'])
    nClassesRef = imgClassRef.getMeta('classes')
    classNamesRef = imgClassRef.getMeta('class names')
    classLookupRef = imgClassRef.getMeta('class lookup')
    
    found = make_array(nClassesOld, /Byte, value=0b)
    foreach n, classNamesOld, i do begin
      iNew = where(strcmp(n, classNamesRef,/FOLD_CASE) eq 1, /NULL)
      if isa(iNew) then begin
        found[i] = 1b
        classLookupNew[*,i] = classLookupRef[*,iNew[0]]
      endif  
    endforeach
  endif
  
  nMax = strtrim(max(STRLEN(classNamesOld)),2)
  classNamesOld = string(format='(%"%-'+nMax+'s")',classNamesOld)
  
  
  
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
  hubAMW_frame,TITLE='Specify class color change' 
   ;right-aligned class titles
  if isa(found) &&  total(found) ne 0 then begin
    hubAMW_label, '(1) = class name not found in reference classifcation'
  endif
  pNames = List()
  
  for i=0, nClassesOld - 1 do begin
    hubAMW_subframe, /ROW
    
    pNameOld = 'pOldColor'+strtrim(i,2)
    pNameNew = 'pNewColor'+strtrim(i,2)
    hubAMW_color, pNameOld, VALUE=classLookupOld[*,i], Editable=0b $
                , Title = classNamesOld[i]$
                , SUFFIXTITLE='->'
    
    suffixTitle=''
    if isa(found) then begin
      if found[i] eq 0 then suffixTitle='(1)'
    endif 
    
    hubAMW_color, pNameNew, VALUE=classLookupNew[*,i], Editable=1b $
            , Title = ''$
            , SUFFIXTITLE=suffixTitle
    pNames.add, pNameNew
  endfor
  
  parameters = hubAMW_manage()
  
  if parameters['accept'] then begin
    nClasses = n_elements(pNames)
    classLookup = make_array(3, nClasses , /Byte)
    foreach pName, pNames, i do begin
      classLookup[*,i] = parameters[pName]
      parameters.remove, pName
    endforeach
    parameters['class lookup'] = classLookup
  endif
  return, parameters

end



function hubApp_ChangeClassColors_getParameters1Check, results, message=message
  message = []
  
  imgClass = hubIOImgInputImage(results['inputClassification'])
  if ~imgClass.isClassification() then begin
    message = [message, 'Classification image must have the file type ENVI Classification']
  endif
  
  if results.hubIsa('referenceClassification') then begin
    imgRef = hubIOImgInputImage(results['inputClassification'])
    if ~imgRef.isclassification() then begin
      message = [message, 'Reference image must have the file type ENVI Classification']
    endif else begin
      
      
    endelse
    
  endif
  imgClass.cleanup
  return, ~isa(message)
end

function hubApp_ChangeClassColors_getParameters1, default, settings
   
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_inputImageFilename, 'inputClassification', TITLE='Classification' $
                           , VALUE=default.hubGetValue('inputClassification')
  
  choice = default.hubKeywordSet('copyClassification')
  hubAMW_subframe, 'copyClassification',/ROW, SETBUTTON = choice eq 0b $
                 , Title = 'Set colors manually'
  hubAMW_subframe, 'copyClassification',/ROW, SETBUTTON = choice eq 1b $
                 , Title = 'Copy colors'
  hubAMW_inputImageFilename, 'referenceClassification', Title='Reference' $
                           ,value=default.hubGetValue('referenceClassification')
  
  
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_ChangeClassColors_getParameters1Check')

  
  if parameters['accept'] then begin
    
  endif
  return, parameters

end 

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function hubApp_ChangeClassColors_getParameters, default, settings
  if ~isa(default) then default = hash()
  if ~isa(settings) then settings = hash('title','ChangeClassColors')
  
  parameters = hubApp_ChangeClassColors_getParameters1(default, settings)
  
  if parameters['accept'] then begin
    parameters += hubApp_ChangeClassColors_getParameters2(parameters, settings)
    
  endif
  
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_ChangeClassColors_getParameters

  ; test your routine
  defPar = Hash()
  defPar['inputClassification'] = enmapBox_getTestImage('AF_LC')
  parameters = hubApp_ChangeClassColors_getParameters(defPar)
  print, parameters

end  
