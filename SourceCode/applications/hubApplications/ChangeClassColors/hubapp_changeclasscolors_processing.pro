;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`hubApp_ChangeClassColors_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;     
;       key                     | type        | description
;       ------------------------+-------------+------------
;       inputClassification     | string      | filepath of classification file that is to change
;       class lookup            | byte[3][nC] | 2D array with RGB values for each of nC classes 
;       referenceClassification | string      | filepath of classification file with reference colors
;       ------------------------+-------------+------------
;    
;    settings : in, optional, type=hash
;
;-
pro hubApp_ChangeClassColors_processing, parameters, settings
  if ~isa(settings) then settings = Hash()

  hubApp_checkKeys, parameters, ['inputClassification']
  if ~parameters.hubIsa(['class lookup','referenceClassification'], /EvaluateOR) then begin
    message, 'Set either keyword "class lookup" or "referenceClassification"' 
  endif
  
  foreach path, parameters['inputClassification'] do begin
    imgClass = hubIOImgInputImage(path, /Classification)
    classLookupOld = imgClass.getMeta('class lookup')
    nClasses = imgClass.getMeta('classes')
    pathHeaderFile = imgClass.getMeta('filename header')
    imgClass.cleanup
    
    if ~parameters.hubIsa('class lookup') then begin
      refImg = hubIOImgInputImage(parameters['referenceClassification'], /Classification)
      classLookupNew = refImg.getMeta('class lookup')
      classNamesNew = refImg.getMeta('class names')
      refImg.cleanup
    endif else begin
      classLookupNew = parameters['class lookup']
    endelse
    
    ;copy existing colors only, specify for existing classes only
    nNew = n_elements(classLookupNew[0,*])
    classLookupOld[*, 0: ((nClasses-1) < (nNew - 1))] = classLookupNew
    
    imgHdr = hubIOImgHeader()
    imgHdr.parseFile, pathHeaderFile 
    imgHdr.setMeta, 'class lookup', classLookupOld[*, 0:(nClasses-1)]
    imgHdr.writeFile, pathHeaderFile
    imgHdr.cleanup
    if ~settings.hubKeywordSet('noOpen') then enmapBox_openImage, path
  endforeach
end


;+
; :Hidden:
;-
pro test_hubApp_ChangeClassColors_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  parameters['inputClassification'] = enmapBox_getTestImage('AF_LC')
  parameters['class lookup'] = bytscl(randomu(systime(), 3, 5))
  settings = hubApp_getSettings()
  hubApp_ChangeClassColors_processing, parameters, settings
  
end  
