pro hubApp_spectralResampling_application, applicationInfo
  
 
  ; save application info to settings hash
  settings = applicationInfo
  settings['title'] = 'Spectral Resampling'
  
  defaultValues = hub_getAppState('hubApp', 'stateHash_spectralResampling')
  parameters = hubapp_spectralResampling_getParameters(settings,defaultValues)
  if isa(parameters) then begin
    hub_setAppState, 'hubApp', 'stateHash_spectralResampling', parameters
    hubApp_spectralResampling_processing, parameters, groupLeader=applicationInfo.hubGetValue('groupLeader')
  endif
end
