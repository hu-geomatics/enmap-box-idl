pro hubApp_spectralResampling_processing, parameters, GroupLeader=GroupLeader
 

  ; check required parameters 
  required = ['inputImageFilename','outputImageFilename','responseSLIFilename']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  ; init image objects
  inputImage = hubIOImgInputImage(parameters['inputImageFilename'])
  outputImage = hubIOImgOutputImage(parameters['outputImageFilename'])
  responseSLI = hubIOImgInputImage(parameters['responseSLIFilename'])
  
  if parameters.haskey('filterType') then begin
    if parameters['filterType'] eq 1 then parameters['fwhm'] = 1
  endif

  if ~parameters.hubKeywordSet('fwhm') and ~responseSLI.isSpectralLibrary() then begin 
    message, 'ResponseSLIFilename must be the path to a Spectral Library file containing the target sensor spectral response functions or the path to an image containing the target sensor FWHM information.'
  endif

  if parameters.hubKeywordSet('fwhm') and ~responseSLI.hasMeta('fwhm') then begin
    message, 'ResponseSLIFilename must be the path to an image containing the target sensor FWHM information.'
  endif
  
  if parameters.hubGetValue('fwhm') then begin
    targetBands = parameters.hubGetValue('targetBands', Default=indgen(responseSLI.getMeta('bands'))) ; default is number of FWHM image/speclib
  endif else begin
    targetBands = parameters.hubGetValue('targetBands', Default=indgen(responseSLI.getMeta('lines'))) ; default is number of target wavelength in speclib  
  endelse

  ; prepare some metainfos
  samples = inputImage.getMeta('samples')
  lines = inputImage.getMeta('lines')
  bands = n_elements(targetBands) ; number of target wavelength
  case inputImage.getMeta('wavelength units') of
    'nanometers'  : wavelengthInput = inputImage.getMeta('wavelength')
    'micrometers' : wavelengthInput = inputImage.getMeta('wavelength')*1000
  endcase

  ; read or create response functions
  ; - create response functions from FWHM 
  if parameters.hubGetValue('fwhm') then begin
    
    case responseSLI.getMeta('wavelength units') of
      'nanometers'  : begin
        fwhm = responseSLI.getMeta('fwhm')
        wavelength = responseSLI.getMeta('wavelength')
      end
      'micrometers' : begin
        fwhm = responseSLI.getMeta('fwhm')*1000
        wavelength = responseSLI.getMeta('wavelength')*1000
      end
    endcase

    ; FWHM = 2 sqrt(2 ln 2) sigma = 2.3548 sigma 
    sigmas = fwhm/2.3548

    ; calculate wavelength range
    wavelengthResponse = [floor(wavelength[0]-2*sigmas[0]):ceil(wavelength[-1]+2*sigmas[-1])] ; use 2-sigma-regions of all responses
    
    ; create response functions
    responseFunctions = list()

    for i=0,responseSLI.getMeta('bands')-1 do begin
      sigma = sigmas[i]
      wlCenter = wavelength[i]
      wl = [floor(wlCenter-2*sigma):ceil(wlCenter+2*sigma)] ; use 2-sigma-region
      weights = make_array(n_elements(wavelengthResponse))  ; full array filled with zeroes 
      weights[(where(/NULL, wavelengthResponse eq wl[0]))[0]] =  exp(-0.5/sigma^2*(wlCenter-wl)^2) ; insered non-zero weights for the 2-sigma-region; very small weights (outside the 2s-range) are excluded
      responseFunctions.add, weights
    endfor
    responseFunctions = responseFunctions.toArray()
    
    outputImage.setMeta, 'band names', (responseSLI.getMeta('band names'))[targetBands]
    outputImage.copyMeta, responseSLI, 'fwhm'

  endif else begin
    ; - response functions given as speclib
    responseFunctions = hubIOImg_readImage(parameters['responseSLIFilename'], /SLICE)
    case responseSLI.getMeta('wavelength units') of
      'nanometers'  : wavelengthResponse = responseSLI.getMeta('wavelength')
      'micrometers' : wavelengthResponse = responseSLI.getMeta('wavelength')*1000
    endcase
    outputImage.setMeta, 'band names', (responseSLI.getMeta('spectra names'))[targetBands]
    
    ; calculate center wavelength
    wavelength = list()
    foreach targetBand, targetBands do begin
      ; estimate center wavelength of target sensor (should the user provide this information?)
      range = where(/NULL, responseFunctions[targetBand, *] ne 0)
      wavelength.add, mean(wavelengthResponse[range])
    endforeach
    wavelength = wavelength.toArray()
  endelse

  ; set metainfos to result image 
  outputImage.copyMeta, inputImage, /CopySpatialInformation 
  case inputImage.getMeta('wavelength units') of
    'nanometers'  : outputImage.setMeta, 'wavelength', wavelength
    'micrometers' : outputImage.setMeta, 'wavelength', wavelength/1000.
  endcase
  outputImage.setMeta, 'wavelength units', inputImage.getMeta('wavelength units')
  if inputImage.isSpectralLibrary() then begin
    outputImage.setMeta, 'file type', 'ENVI Spectral Library'
    outputImage.copyMeta, inputImage, 'spectra names'
  endif

  ; resample image

  tileLines = hubIOImg_getTileLines(image=inputImage)
  tileLines = 100
  inputImage.initReader, tileLines, /Slice, /TileProcessing
 
  outputDataType = hubMathHelper.resolveDataTypeAlias(parameters.hubGetValue('outputDataType', default=4))
  outputImage.initWriter, inputImage.getWriterSettings(SetDataType=outputDataType, SetBands=bands)
  
  
  while ~inputImage.tileProcessingDone(Progress=progress) do begin
    
    ; read input data
    inputData = inputImage.getData()
    
    ; interpolate input data to response function wavelengths
    dims = [size(/DIMENSIONS, wavelengthResponse), (size(/DIMENSIONS, inputData))[1]]
    inputDataInterpolated = make_array(dims, type=5)
    for i=0,dims[1]-1 do begin
      inputSpectrum = inputData[*,i]
      inputSpectrumInterpolated = interpol(inputSpectrum, wavelengthInput, wavelengthResponse)
      inputDataInterpolated[0,i] = inputSpectrumInterpolated
    endfor
    
    ; convolute with response functions
    outputData = hub_fix2d(make_array(bands, dims[1], type=5))
    foreach targetBand, targetBands, i do begin
      ; find start and end wavelength of the response function
      range = where(/NULL, responseFunctions[targetBand, *] ne 0)
      data = inputDataInterpolated[range[0]:range[-1], *]
      weightsVector = (responseFunctions[targetBand, *])[range[0]:range[-1]]
      weightsVector = weightsVector/total(weightsVector) ; normalize weights to sum up to 1
;      weights = rebin(weightsVector, n_elements(range), dims[1])
      weights = rebin(weightsVector, n_elements(weightsVector), dims[1])

      outputData[i,*] = total(data*weights, 1)
    endforeach
    
    outputImage.writeData, temporary(outputData)

;    pBar.setProgress,  pBarLinesDone

  endwhile
  
  ;pBar.cleanup
  inputImage.cleanup
  outputImage.cleanup

end


;+
; :hidden:
;-
pro test_hubApp_spectralResampling_processing

  ; This example uses Landsat 8 sensor response functions stored as a spectral library. Target bands are subsetted to band 2 (Blue) to 7 (SWIR2).  
  p = dictionary()
  p.inputImageFilename = hub_getTestSpeclib('ClassificationSpeclib') ; image/speclib to be resampled
  p.responseSLIFilename = 'C:\Program Files\Exelis\ENVI52\resource\filterfuncs\landsat8_oli.sli' ; response functions are stored here
  p.targetBands = [1:6] ; subset target bands
  p.outputImageFilename = 't:\resampled'
  hubApp_spectralResampling_processing, p

  ; This example uses HyMAP sensor response functions given as FWHM metadata. Target bands are subsetted to band 2 (Blue) to 7 (SWIR2).
  p = dictionary()
  p.inputImageFilename = hub_getTestSpeclib('ClassificationSpeclib') ; image/speclib to be resampled
  p.fwhm = 1b ; use FWHM info from "responseSLIFilename" image/speclib
  p.responseSLIFilename = hub_getTestImage('Hymap_Berlin-A_Image') ; image/speclib with FWHM information
  p.outputImageFilename = 't:\resampled'
  hubApp_spectralResampling_processing, p

end
