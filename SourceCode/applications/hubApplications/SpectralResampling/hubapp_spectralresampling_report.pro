function hubapp_spectralresampling_report_check, resulthash, Message=message

  isConsistent = 1
  inputFileType = hubIOImg_getMeta(resulthash['responseSLIFilename'],'file type')
  if ~strcmp(inputFileType, 'envi spectral library',FOLD_CASE=1) then begin
    message='Response Functions File is not of type ENVI Spectral Library'
    isConsistent=0
  endif

  return, isConsistent

end

pro hubapp_spectralresampling_report_inputSRF_channels, resultHash
  spectraNames = hubIOImg_getMeta(resultHash['responseSLIFilename'],'spectra names')
  hubAMW_program, groupLeader, TITLE='Select Channels to be used from the input reponse functions'
  hubAMW_list, 'inputChannels', List=spectraNames, /MultipleSelection, /ShowButtonBar, XSIZE=500, YSIZE=500,VALUE=indgen(n_elements(spectraNames))
  result = hubAMW_manage()
  if ~result['accept'] then return
  
  hubIOImg_subsetSpeclib $
    , resultHash['responseSLIFilename'] $
    , filepath(/TMP,'inputSLI_reduced') $
    , result['inputChannels']
  
end

pro hubapp_spectralresampling_report, resultHash, UserInformation=userInformation

  targetBands = resultHash['targetBands']
  if ~userInformation.haskey('defaultValues') then defaultValues = hash()
  hasFWHMInfo = isa(hubIOImg_getMeta(userInformation['inputImageFilename'],'fwhm'))
  groupLeader = userInformation['groupLeader']
  
  p = dictionary()
    
  if hasFWHMInfo then begin
    ;create input file response functions
    p.inputFilename = userInformation['inputImageFilename']
    p.outputFilename = FILEPATH(/TMP,'inputreponses')
    hubApp_spectralresampling_createResponseFunctionSLI, p
  endif else begin
    hubAMW_program, groupLeader, Title = 'Select Response Functions File'
    hubAMW_Label, ['For creating spectral resampling examples, the band wavelengths + FWHM information are needed.',$
                   'However, this information is not provided inside the input image header file.',$
                   'Alternatively, you can now select the Spectral Response Functions corresponsing to the input spectra.']
    hubAMW_inputImageFilename, 'responseSLIFilename', title='Spectral Response Functions',VALUE=UserInformation.hubgetvalue('defaultValues')
    
    result = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubapp_spectralresampling_report_check')
   
    if result['accept'] then begin
      userInformation['defaultValues'] = result['responseSLIFilename']
      spectraNames = hubIOImg_getMeta(result['responseSLIFilename'],'spectra names')
      hubAMW_program, groupLeader, TITLE='Select Channels to be used from the input reponse functions'
      hubAMW_list, 'inputChannels', List=spectraNames, /MultipleSelection, /ShowButtonBar, XSIZE=500, YSIZE=500,VALUE=indgen(n_elements(spectraNames))
      result2 = hubAMW_manage()
      if ~result2['accept'] then return

      hubIOImg_subsetSpeclib $
        , result['responseSLIFilename'] $
        , filepath(/TMP,'inputSLI_reduced') $
        , result2['inputChannels']
    endif else return

;    p.outputFilename = result['responseSLIFilename']
    p.outputFilename = filepath(/TMP,'inputSLI_reduced')
  endelse
      
  inputResponsesFile = p.outputFilename
  
  exampleNames = hubIOImg_getMeta(filepath('USGSDigitalSpectralLibrary_splib06a_3examples.sli',root=hubApp_getDirname(),SUBDIRECTORY=['resource','testData','usgs_speclib']),'spectra names')
  hubAMW_program, groupLeader, TITLE='Select surface type for spectral resampling example'
  hubAMW_list, 'surfaceType', List=exampleNames, /ShowButtonBar, XSIZE=400, YSIZE=200,VALUE=2
  hubAMW_label, ['Source: ',$
                'R. N. Clark, G. A. Swayze, R. Wise, K. E. Livo,', $
                'T. M. Hoefen, R. F. Kokaly, and S. J. Sutley, 2007,', $ 
                'USGS Digital Spectral Library splib06a,', $
                'U.S. Geological Survey, Data Series 231.']
  result3 = hubAMW_manage()
  if ~result3['accept'] then return
  
  if UserInformation['filterType'] eq 1 then begin
    p.inputFilename = userInformation['responseSLIFilename']
    p.outputFilename = FILEPATH(/TMP,'filterreponses')
    hubApp_spectralresampling_createResponseFunctionSLI, p
    outputResponsesFile = p.outputFilename
  endif else begin
    outputResponsesFile = userInformation['responseSLIFilename']
  endelse
    
  inputResponsesSLI = hubIOImg_readImage(inputResponsesFile)
  outputResponsesSL = hubIOImg_readImage(outputResponsesFile)
  
  inputResponsesObject = hubIOImgInputImage(inputResponsesFile)
  outputResponsesObject = hubIOImgInputImage(outputResponsesFile)
  
  wLinput = inputResponsesObject.getMeta('wavelength')
  wLoutput = outputResponsesObject.getMeta('wavelength')
  
  wLinputUnit = inputResponsesObject.getMeta('wavelength units')
  wLoutputUnit = outputResponsesObject.getMeta('wavelength units')
  
  inputBands = inputResponsesObject.getMeta('bands')
  outputBands = outputResponsesObject.getMeta('bands')
  
  inputLines = inputResponsesObject.getMeta('lines')
  outputLines = outputResponsesObject.getMeta('lines')
  
  if UserInformation['filterType'] eq 0 then begin
    bandnames = outputResponsesObject.getMeta('spectra names')
  endif else begin
    if outputResponsesObject.isSpectralLibrary() then begin
       bandnames = outputResponsesObject.getMeta('spectra names')     
    endif else begin
       bandnames = outputResponsesObject.getMeta('band names')
    endelse
  endelse
  
  case wLinputUnit of 
    'nanometers' : wLinput = inputResponsesObject.getMeta('wavelength')
    'micrometers' : wLinput = inputResponsesObject.getMeta('wavelength')*1000.
  endcase
  
  case wLoutputUnit of
    'nanometers' : wLoutput = outputResponsesObject.getMeta('wavelength')
    'micrometers' : wLoutput = outputResponsesObject.getMeta('wavelength')*1000.
  endcase
  
  minWL = min([wLinput,wloutput])
  maxWL = max([wLinput,wloutput])
  
  inputResized = fltarr(1,inputLines,maxWL-minWL+1)
  outputResized = fltarr(1,outputLines,maxWL-minWL+1)

  ;if WLinput is ne WLoutput, resize the input arrays to minWL-maxWL range
  inputResized[*,*,where(fix([minWL:maxWL]) eq fix(min(wLinput))):inputBands-1+where(fix([minWL:maxWL]) eq fix(min(wLinput)))] = inputResponsesSLI
  outputResized[*,*,where(fix([minWL:maxWL]) eq fix(min(wloutput))):outputBands-1+where(fix([minWL:maxWL]) eq fix(min(Wloutput)))] = outputResponsesSL
  
  ;set WL flagging, 1= extrapolation zone, 2=interpolation zone (default initialization number), 3=response function definition zone 

  inputFlags = intarr(n_elements(inputResized[0,0,*]))+2
  
  isPlotAble = 0
  if inputLines < n_elements(targetBands) lt 100 then isPlotAble = 1
 
  if isPlotAble then inputRSP = plot(/NODATA, [0,0],name='input response functions',Dimensions=[1000,500],margin=[.1,.25,.1,.1],xtitle='wavelength [nm]',ytitle='Responsivity',WINDOW_TITLE='Response Functions Plot')
  
  for i=0, inputLines-1 do begin
    bandDefZone = where(inputResized[*,i,*] ne 0)

    ;    inputRSP = plot([minWL:maxWL],name='input response functions',inputResized[0,i,*],Dimensions=[1000,500],/Overplot, /fill_background,fill_transparency=30,margin=[.1,.25,.1,.1],xtitle='wavelength [nm]',WINDOW_TITLE='Response Functions Plot')
    if isPlotAble then inputRSP  = plot([minWL:maxWL],name='input response functions',inputResized[0,i,*],/fill_background,fill_transparency=50,/Overplot,thick=2)
    maximum   = (max(inputResized[0,i,bandDefZone]))[0]
    maximum_i = (where(inputResized[0,i,bandDefZone] eq maximum))[0]
    lowerThanFwhm = where(inputResized[*,i,bandDefZone] le maximum/2)
    ;plot fwhm zone
    ;    !null = plot(([minWL:maxWL])[bandDefZone[(lowerThanFwhm[where(lowerThanFwhm le maximum_i)])[-1]]:bandDefZone[(lowerThanFwhm[where(lowerThanFwhm gt maximum_i)])[0]]],replicate(.5,n_elements([bandDefZone[(lowerThanFwhm[where(lowerThanFwhm le maximum_i)])[-1]]:bandDefZone[(lowerThanFwhm[where(lowerThanFwhm gt maximum_i)])[0]]])),/Overplot,thick=5,color='red')
    ;plot definition zone (ne 0)
    ;     !null = plot(([minWL:maxWL])[bandDefZone],replicate(.3,n_elements(bandDefZone)),/overplot,color='green',thick=5)
    bandFWHMZone = [bandDefZone[(lowerThanFwhm[where(lowerThanFwhm le maximum_i)])[-1]]:bandDefZone[(lowerThanFwhm[where(lowerThanFwhm gt maximum_i)])[0]]]
    inputFlags[bandFWHMZone] = 3
  endfor
    
  
  table = hubReportTable()
  table.addColumn, name='band name'
  table.addColumn, name='in fwhm zone'
  table.addColumn, name='interpolated';,FORMATSTRING='(%"%-20s")'
  table.addColumn, name='extraploated'
  
  ; calculates for each channel of the filter sensor how many values of 1-3 of input interpolation are contained
  foreach i,targetbands do begin

    if isPlotAble then outputRSP = plot([minWL:maxWL],name='output response functions',outputResized[0,i,*],/overplot,Dimensions=[1000,500],color='red',/fill_background,fill_color='red',fill_transparency=70,thick=2,yrange=[0,1])
    if isPlotAble then !null     = legend(target=[inputRSP,outputRSP],POSITION=[1,0],/NORMAL,VERTICAL_ALIGNMENT='bottom',LINESTYLE=6)

    bandDefZone = where(outputResized[*,i,*] ne 0)
    percentages = fltarr(3)

    maximum = (max(outputResized[0,i,bandDefZone]))[0]
    maximum_i = (where(outputResized[0,i,bandDefZone] eq maximum))[0]
    lowerThanFwhm = where(outputResized[*,i,bandDefZone] le maximum/2)

    ;   !null = plot(([minWL:maxWL])[bandDefZone[(lowerThanFwhm[where(lowerThanFwhm le maximum_i)])[-1]]:bandDefZone[(lowerThanFwhm[where(lowerThanFwhm gt maximum_i)])[0]]],replicate(.4,n_elements([bandDefZone[(lowerThanFwhm[where(lowerThanFwhm le maximum_i)])[-1]]:bandDefZone[(lowerThanFwhm[where(lowerThanFwhm gt maximum_i)])[0]]])),/Overplot,thick=5,color='red')

    bandFWHMZone = [bandDefZone[(lowerThanFwhm[where(lowerThanFwhm le maximum_i)])[-1]]:bandDefZone[(lowerThanFwhm[where(lowerThanFwhm gt maximum_i)])[0]]]

    qualityFlagsInZone = (inputflags[bandFWHMZone])[UNIQ(inputflags[bandFWHMZone], SORT(inputflags[bandFWHMZone]))]

    ;calculate contained flag values for the defined zone for one channel
    hist = (histogram(inputFlags[bandFWHMZone]))

    ; if histogram calculated for values [1,1,3,3], it will contain a zero [2, 0 , 2], this case is treated separately here
    if min((histogram(inputFlags[bandFWHMZone]))) eq 0 then begin
      percentages[0:2] = hist*100/total(hist)
    endif

    if min((histogram(inputFlags[bandFWHMZone]))) ne 0 then begin
      percentages[qualityFlagsInZone-1] = hist*100/total(hist)
    endif

    table.addRow, list(bandnames[i],percentages[2],percentages[1],percentages[0])
    ;    print, bandnames[i], ' - extrapolated: ',strcompress(/REMOVE_ALL,percentages[0]),' %', ' interpolated : ',strcompress(/REMOVE_ALL,percentages[1]),' %', ' in fwhm zone: ',strcompress(/REMOVE_ALL,percentages[2]), ' %'

  endforeach
  
  ;define extrapolation zone
  inputFlags[0:(where(inputFlags eq 3))[0]-1] = 1
  inputFlags[(where(inputFlags eq 3))[-1]+1:n_elements(inputFlags)-1] = 1
 
  
;  print, (table.getFormatedASCII())[0,0:1]
;  print, (table.getFormatedASCII())[0,2:-1]
  
  p = dictionary()
  p.inputImageFilename = filepath('USGSDigitalSpectralLibrary_splib06a_3examples.sli',root=hubApp_getDirname(),SUBDIRECTORY=['resource','testData','usgs_speclib'])
  p.responseSLIFilename = inputResponsesFile ; response functions are stored here
;  p.targetBands = targetBands ; subset target bands
  p.outputImageFilename = FILEPATH(/TMP,'USGS_to_inputsensor')
  true_input_name = p.outputImageFilename
  
  hubApp_spectralResampling_processing, p
  
  true_input = hubIOImg_readImage(true_input_name)
  true_input_object = hubIOImgInputImage(true_input_name)
  true_input_WL = true_input_object.getMeta('wavelength')
  
  p = dictionary()
  p.inputImageFilename = filepath('USGSDigitalSpectralLibrary_splib06a_3examples.sli',root=hubApp_getDirname(),SUBDIRECTORY=['resource','testData','usgs_speclib'])
  p.responseSLIFilename = outputResponsesFile ; response functions are stored here
  p.targetBands = targetBands ; subset target bands
  p.outputImageFilename = FILEPATH(/TMP,'USGS_to_outputsensor')
  true_output_name = p.outputImageFilename
  hubApp_spectralResampling_processing, p

  true_output = hubIOImg_readImage(true_output_name)
  true_output_object = hubIOImgInputImage(true_output_name)
  true_output_WL = true_output_object.getMeta('wavelength')
  
  p = dictionary()
  p.inputImageFilename = true_input_name
  p.responseSLIFilename = outputResponsesFile
  p.targetBands = targetBands ; subset target bands
  p.outputImageFilename = FILEPATH(/TMP,'input_to_outputsensor')
  true_input_to_output_name = p.outputImageFilename
  hubApp_spectralResampling_processing, p
  
  true_input_to_output = hubIOImg_readImage(true_input_to_output_name)
  true_input_to_output_object = hubIOImgInputImage(true_input_to_output_name)
  true_input_to_output_WL = true_input_to_output_object.getMeta('wavelength')

  ;plot ground truth from USGS
  groundTruthUSGS = hubIOImg_readImage(filepath('USGSDigitalSpectralLibrary_splib06a_3examples.sli',root=hubApp_getDirname(),SUBDIRECTORY=['resource','testData','usgs_speclib']))
  groundTruthUSGS_WL = hubIOImg_getMeta(filepath('USGSDigitalSpectralLibrary_splib06a_3examples.sli',root=hubApp_getDirname(),SUBDIRECTORY=['resource','testData','usgs_speclib']),'wavelength')
 
  gTruth = plot(groundTruthUSGS_WL,groundTruthUSGS[0,result3['surfaceType'],*], name='  example surface, 1-nm-spectrum', color='grey',transparency=60,margin=[.1,.23,.1,.1],xtitle='wavelength [µm]',ytitle='Reflectance',WINDOW_TITLE='Spectral Resampling Example',dimensions=[800,500],title='Surface Type: '+exampleNames[result3['surfaceType']])  
  a = plot(true_input_WL,true_input[0,result3['surfaceType'],*],NAME='  example surface, input sensor',SYM_COLOR='grey',SYMBOL='tu',SYM_SIZE=1.6,sym_thick=2,LINESTYLE=6,/overplot)
  d = plot(true_input_WL,true_input[0,result3['surfaceType'],*],NAME='  example surface, input sensor 1-nm resampled',/overplot,transparency=60,linestyle='dotted')
  b = plot(true_output_WL,true_output[0,result3['surfaceType'],*],NAME='  example surface, output sensor',SYM_COLOR='grey',sym_thick=2,SYM_SIZE=1.6,SYMBOL='s',LINESTYLE=6,/OVERPLOT)
  c = plot(true_input_to_output_WL,true_input_to_output[0,result3['surfaceType'],*],NAME='  output spectrum after linear resampling',SYM_COLOR='red',sym_thick=2,sym_filled=1,fill_color='red',sym_transparency=20,SYM_SIZE=1.2,SYMBOL='o',LINESTYLE=6,/OVERPLOT)
  !null = legend(target=[b,c],/NORMAL,HORIZONTAL_ALIGNMENT='right',VERTICAL_ALIGNMENT='bottom',LINESTYLE=6,POSITION=[1,0],SHADOW=0,HORIZONTAL_SPACING=.02)
  !null = legend(target=[gTruth,a,d],/NORMAL,POSITION=[0,0],VERTICAL_ALIGNMENT='bottom',HORIZONTAL_ALIGNMENT='left',LINESTYLE=6,SHADOW=0,HORIZONTAL_SPACING=.04)
end