;+
; :Description:
;    Widget callback function. Checks whether the input is correct.
;
; :Params:
;    resultHash: in, required, type=hash
;       Hash with output collected form the sub-widget.
;       
; :Keywords:
;    Message: out, optional, type= string[]
;     Set this to provide an information on occured errors.
;
;-
function hubapp_spectralResampling_dialog1_Check, resultHash, Message=message
   

   isConsistent = 1
   inputImage = hubIOImgInputImage(resultHash['inputImageFilename'])
   inputResponse = hubIOImgInputImage(resultHash['responseSLIFilename'])
   if ~isa(inputImage.getMeta('wavelength')) or ~isa(inputImage.getMeta('wavelength units')) then begin
    message='wavelength or wavelength units not defined in image/speclib'
    isConsistent=0
   endif
   
   if ~isa(inputResponse.getMeta('wavelength')) or ~isa(inputResponse.getMeta('wavelength units')) then begin
     message='wavelength or wavelength units not defined in filter function'
     isConsistent=0
   endif
   if resultHash['filterType'] eq 0 then begin
     if ~strcmp(inputResponse.getMeta('file type'), 'envi spectral library',FOLD_CASE=1) then begin
       message='filter function is not of type ENVI Spectral Library'
       isConsistent=0
     endif 
   endif
      
   return, isConsistent

end

function hubapp_spectralResampling_dialog1, settings, defaultValues
  
   if ~isa(defaultValues) then defaultValues = hash()
  groupLeader = settings['groupLeader']
  tsize = 100
  xsize = 550
  
  ;name of data type as shown in the combobox
  helper = hubHelper()

  datatype_names  = helper.getDataTypeInfo(/TYPEDESCRIPTIONS)
  datatype_values = helper.getDataTypeInfo(/TypeCodes)
  
  hubAMW_program, groupLeader, Title = 'Spectral Resampling'
  hubAMW_frame, Title =  'Input'
  hubAMW_inputImageFilename, 'inputImageFilename', title='Image/Speclib',VALUE=defaultValues.hubGetValue('inputImageFilename'), TSIZE=tsize+20,size=xsize
 
  hubAMW_frame, Title =  'Resampling Parameters'
  hubAMW_subframe, 'filterType' $
    , Title = 'Spectral Response Function' $
    , /ROW
  hubAMW_inputImageFilename, 'responseSLIFilename', title='Speclib', VALUE=defaultValues.hubGetValue('responseSLIFilename'), TSIZE=tsize,size=xsize

  hubAMW_subframe, 'filterType' $
    , Title = 'Band Wavelength + FWHM (from header file)' $
    , /ROW
  hubAMW_inputImageFilename, 'responseSLIFilename', TITLE='Image/Speclib', VALUE=settings.hubGetValue('inputFilterFunction'), TSIZE=tsize,size=xsize
  
  hubAMW_frame, Title =  'Output'
  hubAMW_combobox,       'outputDataType', Title='Data Type   ', Value=3, List= datatype_names, Extract=datatype_values, TSIZE=tsize+70
  hubAMW_outputFilename, 'outputImageFilename', Title='Resampled Image/Speclib', TSIZE=tsize+70, Value=filepath('resampledImage', /TMP)

  result = hubAMW_manage(/FLAT,CONSISTENCYCHECKFUNCTION='hubapp_spectralResampling_dialog1_Check')

  return, result
end

;function check_hubapp_spectralResampling_preview, resultHash, Message=message, Userinformation=userinformation
;  
;  isConsistent = 1
;  inputImage = hubIOImgInputImage(userinformation['inputImageFilename'])
;  if ~isa(inputImage.getmeta('fwhm')) then begin
;    message = 'Preview not possible: Plotting would require FWHM information in header file.'
;    isConsistent = 0
;  endif
;
;  return, isConsistent
;
;end

function hubapp_spectralResampling_dialog2, settings, result1

  groupLeader = settings['groupLeader']
  responseFilename = result1['responseSLIFilename']
 
  ;todo what if FWHM case speclib given (samples = bands) vs image given (bands = bands) , TIP: i dont need bands, when i can create via band/spectranames/wavelength
  responseImage = hubIOImgInputImage(responseFilename)
  if result1['filterType'] eq 0 then begin
    bands = responseImage.getMeta('lines')
    bandnames = responseImage.getMeta('spectra names')
  endif else begin
    ; todo what if fwhm + spectlib, then name info from wavelength
    bands = responseImage.getMeta('bands')
    bandnames = responseImage.getMeta('band names')
  endelse

  if ~isa(bandnames) then bandnames = 'band ' + strcompress(indgen(bands)+1,/REMOVE_ALL) 

  userInformation = result1

  hubAMW_program, groupLeader, TITLE='Select Output Bands'
  hubAMW_list, 'targetBands', List=bandNames, /MultipleSelection, /ShowButtonBar, XSIZE=500, YSIZE=500,VALUE=indgen(bands) 
  hubAMW_button, Title='Preview', EventHandler='hubapp_spectralresampling_report',/Bar,RESULTHASHKEYS=['targetBands'],USERINFORMATION=userInformation;, CONSISTENCYCHECKFUNCTION='check_hubapp_spectralResampling_preview'
  
  userInformation['groupLeader'] = groupLeader
  
  result = hubAMW_manage(USERINFORMATION=userInformation)

  return, result

end


;+
; :Description:
;    This function creates a widget to collect the parameters required for 
;    parameterization of `hubapp_spectralResampling_processing` routine.
;
; :Params:
; 
;     
;     settings: in, optional, type=hash
;       A hash providing default values, e.g. from a previous call of this dialog.
;        
;-
function hubapp_spectralResampling_getparameters, settings,defaultValues
 
  ;restore state from previous application calls to retrieve default values
  stateHash = hub_getAppState('hubApp', 'stateHash_spectralResampling')
  if isa(stateHash) then settings += stateHash
 
  groupLeader = settings.hubgetValue('groupLeader')
  
  result1 = hubapp_spectralResampling_dialog1(settings,defaultValues)
  if result1['accept'] eq 0 then return, !null
  
  result2 = hubapp_spectralResampling_dialog2(settings,result1)
  if result2['accept'] eq 0 then return, !null
  parameters = result1 + result2
  return, parameters                  
end

pro test_hubapp_spectralResampling_getparameters
  settings = hash()
  settings['title'] = 'Spectral Resampling'
  settings['groupLeader'] = settings.hubgetValue('groupLeader')
  f = hubapp_spectralResampling_getparameters(settings)
  print, f
end




