pro hubApp_spectralresampling_createResponseFunctionSLI, parameters

  ; check required parameters 
  required = ['inputFilename','outputFilename']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  ; init image objects
  inputImage = hubIOImgInputImage(parameters['inputFilename'])
  outputImage = hubIOImgOutputImage(parameters['outputFilename'])

  ; create response functions from FWHM 
   
  case inputImage.getMeta('wavelength units') of
    'nanometers'  : begin
      fwhm = inputImage.getMeta('fwhm')
      wavelength = inputImage.getMeta('wavelength')
    end
    'micrometers' : begin
      fwhm = inputImage.getMeta('fwhm')*1000
      wavelength = inputImage.getMeta('wavelength')*1000
    end
  endcase

  ; FWHM = 2 sqrt(2 ln 2) sigma = 2.3548 sigma 
  sigmas = fwhm/2.3548

  ; calculate wavelength range
  wavelengthResponse = [floor(wavelength[0]-2*sigmas[0]):ceil(wavelength[-1]+2*sigmas[-1])] ; use 2-sigma-regions of all responses
  
  ; create response functions
  responseFunctions = list()

  for i=0,inputImage.getMeta('bands')-1 do begin
    sigma = sigmas[i]
    wlCenter = wavelength[i]
    wl = [floor(wlCenter-2*sigma):ceil(wlCenter+2*sigma)] ; use 2-sigma-region
    weights = make_array(n_elements(wavelengthResponse))  ; full array filled with zeroes 
    weights[(where(/NULL, wavelengthResponse eq wl[0]))[0]] =  exp(-0.5/sigma^2*(wlCenter-wl)^2) ; insered non-zero weights for the 2-sigma-region; very small weights (outside the 2s-range) are excluded
    responseFunctions.add, weights
  endfor
  responseFunctions = transpose(responseFunctions.toArray())

  ; set meta infos
  outputImage.setMeta, 'wavelength', wavelengthResponse
  outputImage.setMeta, 'wavelength units', 'nanometers'
  outputImage.setMeta, 'file type', 'ENVI Spectral Library'

  ; write speclib
  size = size(/DIMENSIONS, responseFunctions)
  writerSettings = hash()
  writerSettings['samples'] = 1
  writerSettings['lines'] = size[1]
  writerSettings['bands'] = size[0]
  writerSettings['data type'] = size(/TYPE, responseFunctions)
  writerSettings['dataFormat'] = 'cube'
  outputImage.initWriter, writerSettings
  outputImage.writeData, responseFunctions

  ; cleanup  
  inputImage.cleanup
  outputImage.cleanup

end


;+
; :hidden:
;-
pro test_hubApp_spectralresampling_createResponseFunctionSLI
  
  p = dictionary()
  p.inputFilename = hub_getTestImage('Hymap_Berlin-A_Image')
  p.outputFilename = 't:\HyMAP_responses.sli'

;  p.inputFilename = 't:\SpecLib_APEX2013_Brussels_ROIl_merged'
;  p.outputFilename = 't:\Apex_responses.sli'
 
  hubApp_spectralresampling_createResponseFunctionSLI, p
  print, 'Done!'

end
