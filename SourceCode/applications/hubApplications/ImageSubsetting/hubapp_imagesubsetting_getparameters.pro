;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;-


;+
; :Description:
;    This dialog is used to specifiy the image and, optionally, a mask file.
;
; :Params:
;    settings: in, required, type=hash
;     The applications settings hash as defined in `hubApp_ImageSubsetting_application`.
;      
;    hubHelper: in, required, type=hubHelper()
;     The hubHelper Object defined in ;
;
;
;-
function hubApp_ImageSubsetting_getParametersDialog1, settings, hubHelper
    ;Dialog 1: collect input and output file names 
    hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
    ; frame for output options & parameters
    hubAMW_frame ,   Title =  'Input'
    hubAMW_inputSampleSet, 'sampleSet', title='Image', /Masking $
                         , value = settings.hubGetValue('inputImage') $
                         , referenceValue = settings.hubGetValue('maskImage') $
                         , /ReferenceOptional
  
      
    
    results = hubAMW_manage()
    if results['accept'] then begin
        results['inputImage'] = (results['sampleSet'])['featureFilename']
        results['maskImage']  = (results['sampleSet'])['labelFilename']
        results.hubRemove, 'sampleSet'
    endif
    return, results
end


;+
; :Description:
;    This function reads the result hash and returns the desired spatial subset in 
;    zero-based pixel coordinates.  
;
; :Params:
;    resultHash: in, required, type=hash
;     Must contain the following keys returned by the widgets::
;       
;       key             | type   | description
;       ----------------+--------+---------------------------------------
;       coordinateType  | byte   | 0 -> use (one-based) pixel coordinates
;                       |        | 1 -> use geo coordinates 
;       --- if coordinateType = 0 ------------------------------------
;       samplesStart    | int    | first one-based pixel in X-direction
;       samplesEnd      | int    | last  one-based pixel in X-direction
;       linesStart      | int    | first one-based pixel in Y-direction 
;       linesEnd        | int    | last  one-based pixel in Y-direction
;       
;       --- if coordinateType = 1 ------------------------------------
;       gcEastingStart  | double | 
;       gcEastingEnd    | double |
;       gcNorthingStart | double |
;       gcNorthingEnd   | double |
;       ----------------+--------+--------------------------------------
;       
;    geoHelper: in, required, type=hubGeoHelper()
;      The hubGeoHelper as once defined in `hubApp_ImageSubsetting_getParametersDialog2`.
;      It is required to get the a geo-coordinates corresponding pixel coordinate. 
;
;
;
;-
function hubApp_ImageSubsetting_getParametersDialog2PixelRanges, resultHash, geoHelper
    ranges = Hash()
    
    if resultHash['coordinateType'] eq 0 then begin
      lineRange = [resultHash['linesStart'],resultHash['linesEnd']]-1
      sampleRange = [resultHash['samplesStart'],resultHash['samplesEnd']]-1
      ranges['lineRange'] = [min(lineRange), max(lineRange)]
      ranges['sampleRange'] = [min(sampleRange), max(sampleRange)]
    endif else begin
      if ~geoHelper.isInitialized() then begin
        imageDimension = geoHelper.getDimensions() 
        ranges['sampleRange'] = [0,imageDimension.samples]
        ranges['lineRange'] = [0,imageDimension.lines]
      endif else begin
        geoSubset = geoHelper.createGeometry([resultHash['gcEastingStart'],resultHash['gcEastingEnd']] $ 
                                            ,[resultHash['gcNorthingStart'],resultHash['gcNorthingEnd']])
        pixelRange = geoHelper.convertCoordinate2PixelIndex(geoSubset, Truncate=2)     
       
        ranges['sampleRange'] = hubRange(pixelRange.x)
        ranges['lineRange'] = hubRange(pixelRange.y)
      endelse
    endelse
    return, ranges 
end


;+
; :Description:
;    Check function for Dialog2. (I'm not sure if we realy need this).
;
; :Params:
;    resultHash: in, required, type=hash
;
; :Keywords:
;    message: out, required, type=string
;    userInformation: in, required, type=hash
;
;-
function hubApp_ImageSubsetting_getParametersDialog2Check, resultHash $
      , message=message, userInformation=userInformation
  isValid = 1b
  msg = []
;  if resultHash['coordinateType'] eq  0 then begin
;    ;pixel coordinates
;    
;    
;  endif else begin
;  ;geo-coordinates
;  
;  endelse
  
  message = msg
  return, isValid
end

;+
; :Description:
;    This is the event handler function of the 'Preview' button widget. It renders the preview
;    to the `previewImageDraw` widget and shows the spatial size of the desired image subset.
;
; :Params:
;    resultHash: in, required, type=hash
;     This hash contains the values of those widgets that are provided Dialog2's 
;     preview button argument ResultHashKeys
;
; :Keywords:
;    userInformation: in, required, type=hash
;     This hash contains the variables that are not returned by widgets.
;
;-
pro hubApp_ImageSubsetting_getParametersDialog2DrawPreview, resultHash, userInformation=userInformation
  
  w = resultHash['previewImageDraw']
  img = userInformation['previewImage']
  geoHelper = userInformation['geoHelper']
  previewImagePositionX = userInformation['pixelPositionsX']
  previewImagePositionY = userInformation['pixelPositionsY']
  ranges = hubApp_ImageSubsetting_getParametersDialog2PixelRanges(resultHash, geoHelper)
  xRange = hubRange(previewImagePositionX)
  yRange = hubRange(previewImagePositionY) 
  
  ns = userInformation['ns']
  nl = userInformation['nl']
  
;  if n_elements(previewImagePositionX) gt 1 then begin
;    subXRangeIndices = value_locate(previewImagePositionX, ranges['sampleRange'])
;    subXRange = previewImagePositionX[subXRangeIndices]
;    subXVec = subXRange[[0,1,1,0,0]]
;  endif else begin
;    ;image has more than one pixel
;    subXRangeIndices = [0,1]
;    subXVec = make_array(5,value=previewImagePositionX)
;    subXVec += [0,1,1,0,0]
;  endelse
;  
;  if n_elements(previewImagePositionY) gt 1 then begin
;    subYRangeIndices = value_locate(previewImagePositionY, ranges['lineRange'])
;    subYRange = previewImagePositionY[subYRangeIndices]
;    subYVec = max(previewImagePositionY) - subYRange[[0,0,1,1,0]]
;  endif else begin
;    subYRangeIndices = [0,1]
;    subYVec =  make_array(5,value=previewImagePositionY)
;    subYVec += [0,0,1,1,0]
;  endelse
  
  imgPosition=[0.,0.,1.,1.]

  subRangeX = ranges['sampleRange']
  subRangeY = ranges['lineRange']
  
  wi = image(hub_fix3d(img), previewImagePositionX, previewImagePositionY, ORDER=1 $
            , POSITION=imgPosition $
            , CURRENT=w, ASPECT_RATIO=1)

  vX = [0, subRangeX, ns-1]
  vY = [0, subRangeY, nl-1]
  
  ;grey excluded regions
  
;  if subRangeX[0] ne 0 then begin
;    !NULL = polygon(vX[[0,1,1,0]], vY[[0,1,2,3]],FILL_TRANSPARENCY=50,FILL_BACKGROUND=[0,0,0],linestyle = '', /DATA, TARGET=wi )
;  endif
;  if subRangeX[1] ne ns-1 then begin
;    !NULL = polygon(vX[[2,3,3,2]], vY[[1,0,3,2]],FILL_TRANSPARENCY=50,FILL_BACKGROUND=[0,0,0],linestyle = '', /DATA, TARGET=wi )
;  endif

  
  !NULL = polygon(vX[[0,3,3,0,0,1,1,2,2,1]] + [0,1,1,0,0,0,0,1,1,0] $
                 ,nl - (vY[[0,0,3,3,0,1,2,2,1,1]] + [0,0,1,1,0,0,1,1,0,0]) $
                 ,FILL_TRANSPARENCY=50,FILL_BACKGROUND=[0,0,0] $
                 ,linestyle = '', /DATA, TARGET=wi)

  ;red region of interest
  
  xVec = (vX[[1,2,2,1]] + [0,1,1,0])
  yVec = nl -(vY[[1,1,2,2]] + [0,0,1,1]) 
  !NULL = polygon(xVec, yVec  $
                 ,FILL_TRANSPARENCY=100,COLOR='red' $
                 ,linestyle = '-', /DATA, TARGET=wi, THICK=2)
;  

end

;+
; :Description:
;    This dialog is used to define the spatial and spectral subset.
;
; :Params:
;    settings: in, required, type=hash
;     The applications settings hash as defined in `hubApp_ImageSubsetting_application`.
;      
;    hubHelper: in, required, type=hubHelper()
;     The hubHelper Object.
;     
;    results1: in, required, type=hash
;     This is the result hash as returned by `hubApp_ImageSubsetting_getParametersDialog1`.
;
;-
function hubApp_ImageSubsetting_getParametersDialog2, settings, hubHelper, results1
    results2 = Hash()
    inputImage = hubIOImgInputImage(results1['inputImage'])
    inputDataType = inputImage.getMeta('data type')
    inputSamples = inputImage.getMeta('samples')
    inputLines = inputImage.getMeta('lines')
    inputInterleave = inputImage.getMeta('interleave')
    inputDataIgnoreValue = inputImage.getMeta('data ignore value')
    geoHelper = !NULL
    if inputImage.hasMeta('map info') then begin
      geoHelper = hubGeoHelper(inputImage)
      imgBoundaries = geoHelper.getImageBoundaries()
    endif
    
    if results1.hubIsa('maskImage') then begin
      maskImage = hubIOImgInputImage(results1['maskImage'], /MASK)
    endif
    
    plotSizeX = 300
    plotSizeY = 300
    
    isClassification = inputImage.isClassification()
    previewImageInfo = Hash()
    previewImageInfo['ns'] = inputsamples
    previewImageInfo['nl'] = inputLines
    previewImageInfo += hubApp_getPreviewImage(inputImage, plotSizeX*2, plotSizeY*2, inputMask=maskImage , TRUECOLOR=1)
    previewImageInfo['geoHelper'] = geoHelper
    
    ;define lists for list selections
    interleaves = ['BSQ','BIL', 'BIP']
    
    
    hubAMW_program , groupLeader, Title = 'ImageSubsetting';settings['title']
    
    hubAMW_frame ,   Title =  'Input Info'
    info = List()
    info.add, 'Image:' + hubHelper.getShortenedString(results1['inputImage'], 50)
    info.add, 'File Type: ' + inputImage.getMeta('file type')

    info.add, 'Data Type: ' + hubHelper.getDataTypeInfo(inputImage.getMeta('data type'),/TypeDescriptions)
    info.add, string(format='(%"Samples x Lines x Bands: %i x %i x %i")' $
            , inputSamples, inputLines, inputImage.getMeta('bands'))
    if isa(geoHelper) then begin
      mapInfo = geoHelper.getMapInfo()
      miUnits = mapInfo.hubGetValue('units', default='')
      info.add, string(format='(%"Spatial Size: %f x %f %s")', imgBoundaries.x[1] - imgBoundaries.x[0], abs(imgBoundaries.y[0] - imgBoundaries.y[1]), miUnits)
    endif
    info.add, 'Interleave: ' + inputImage.getMeta('interleave')
    info.add, 'Data Ignore Value: '+ (isa(inputDataIgnoreValue) ? strtrim(inputDataIgnoreValue,2) : '<not defined>')
    hubAMW_label, info.ToArray()
    
    hubAMW_tabFrame
    hubAMW_tabPage ,   Title =  'Spatial Subset'
    baseFrame = widget_base(hubAMW_getCurrentBase(), /ROW)
    baseFrameLeft = widget_base(baseFrame, /COLUMN, /FRAME)
    baseFrameRight = widget_base(baseFrame, /COLUMN, /FRAME)
    
    hubAMW_setCurrentBase, baseFrameLeft
    
    hubAMW_subframe, 'coordinateType', /COLUMN, TITLE='Pixel Coordinates', /SETBUTTON, targetBase=baseFrameLeft
    left1 = widget_base(hubAMW_getCurrentBase(), /Column)
    hubAMW_subframe, /ROW, targetBase=left1
    hubAMW_parameter,'samplesStart', title = 'Samples', Size=8, /INTEGER, ISGE=1, ISLE=inputSamples, value=1
    hubAMW_parameter,'samplesEnd',   title = 'to'     , Size=8, /INTEGER, ISGE=1, ISLE=inputSamples , value = inputSamples
    hubAMW_subframe, /ROW, targetBase=left1
    hubAMW_parameter,'linesStart', title = 'Lines  ', Size=8, /INTEGER, ISGE=1, ISLE=inputLines, value=1
    hubAMW_parameter,'linesEnd',   title = 'to'     , Size=8, /INTEGER, ISGE=1, ISLE=inputLines, value=inputLines

    hubAMW_subframe, 'coordinateType', /COLUMN, TITLE='Geo Coordinates', targetBase=baseFrameLeft
    left2 = widget_base(hubAMW_getCurrentBase(), /Column)
  
    if isa(geoHelper) then begin
      mapInfo = geoHelper.getMapInfo()
      minX = min(imgBoundaries.x,  max=maxX)
      minY = min(imgBoundaries.y,  max=maxY)
      unitsFormat = '(F-15.4)'
      if isa(mapInfo.units) then begin
        case strlowcase(mapInfo.units) of
          'meters'    : unitsFormat = '(F-15.4)' 
          'kilometers': unitsFormat = '(F-15.10)'
          'yards'     : unitsFormat = '(F-15.4)'
          'miles'     : unitsFormat = '(F-15.10)'
          else : 
        endcase
      endif 
      hubAMW_subframe, /ROW, targetBase=left2
      hubAMW_parameter,'gcEastingStart', title = ' Easting from', Size=14, /FLOAT, ISGE=minX, ISLE=maxX, value=string(minX, format=unitsFormat)
      hubAMW_subframe, /ROW, targetBase=left2
      hubAMW_parameter,'gcEastingEnd',   title = '           to', Size=14, /FLOAT, ISGE=minX, ISLE=maxX, value=string(maxX, format=unitsFormat)
      hubAMW_subframe, /ROW, targetBase=left2
      hubAMW_parameter,'gcNorthingStart',title = 'Northing from', Size=14, /FLOAT, ISGE=minY, ISLE=maxY, value=string(maxY, format=unitsFormat)
      hubAMW_subframe, /ROW, targetBase=left2
      hubAMW_parameter,'gcNorthingEnd',  title = '           to', Size=14, /FLOAT, ISGE=minY, ISLE=maxY, value=string(minY, format=unitsFormat)
    endif else begin
      hubAMW_subframe, /Column, targetBase=left2
      hubAMW_label, ['','<This image is not georeferred>']
    endelse
    
    hubAMW_setCurrentBase, baseFrameRight
    hubAMW_draw, 'previewImageDraw', XSIZE=plotSizeX, YSIZE=plotSizeY, /IDLOBJECTGRAPHICS
    hubAMW_button, Title= 'Preview' $
                 , ConsistencyCheckFunction='hubApp_ImageSubsetting_getParametersDialog2Check' $
                 , ResultHashKeys = ['previewImageDraw','coordinateType' $
                                    ,'gcEastingStart','gcEastingEnd','gcNorthingStart','gcNorthingEnd' $
                                    ,'samplesStart','samplesEnd','linesStart','linesEnd'] $
                 , USERINFORMATION=previewImageInfo $
                 , EVENTHANDLER='hubApp_ImageSubsetting_getParametersDialog2DrawPreview', /PRESS
    
   
   
    hubAMW_tabPage ,   Title =  'Spectral Subset'
    nBands = inputImage.getMeta('bands')
    sig = fix(alog10(nBands))+1
    bandText = string(format='(%"%'+strtrim(sig,2)+'i")', indgen(nBands)+1) + ':'
    if inputImage.hasMeta('bnames') then begin
      bandText += inputImage.getMeta('bnames')
    endif else begin
      bandText += 'Band '+strtrim((indgen(inputImage.getMeta('bands'))+1),2)
    endelse
    if inputImage.hasMeta('wavelength') then begin
      bandText += ' ('+ strtrim(inputImage.getMeta('wavelength'),2) + ')'
    endif
    
    hubAMW_list, 'bandSubset', title='Select Bands', List=bandText $
      , value= indgen(nBands) $
      , /MultipleSelection, XSize= 300, YSize=300
    
    
      ; frame for output options & parameters
    hubAMW_frame,   Title =  'Output'
    
    if results1.hubIsa('maskImage') then begin
      if isa(inputDataIgnoreValue) then begin
        results2['outputDataIgnoreValue'] = inputDataIgnoreValue
      endif else begin 
        hubAMW_parameter, 'outputDataIgnoreValue', title='Data Ignore Value', /FLOAT
      endelse
    endif 
    hubAMW_outputFilename, 'outputImage', title='Image' $
                         , value = settings.hubGetValue('outputImage', default='subset')
    
   
    results2 = results2 + hubAMW_manage()
    inputImage.cleanup
    
    if results2['accept'] then begin
      results2 = results2 + hubApp_ImageSubsetting_getParametersDialog2PixelRanges(results2, geoHelper)
      
     
      results2.hubRemove, ['linesStart','linesEnd','samplesStart','samplesEnd' $
                           , 'gcEastingStart','gcEastingEnd','gcNorthingStart','gcNorthingEnd']
    endif   
  return, results2
    
end



;+
; :Hidden:
;-
pro test_hubApp_ImageSubsetting_getParametersDialog2
  settings = hubApp_getSettings()
  settings['title'] = 'Dialgo 2 Test'
  par = Hash()
  par['accept'] =  1
  par['inputImage'] = enmapBox_getTestImage('AF_Image') 
  par['maskImage'] = enmapBox_getTestImage('AF_MaskVegetation')
  par['outputImage']='C:\Users\geo_beja\AppData\Local\Temp\subset'
  print, hubApp_ImageSubsetting_getParametersDialog2(settings, hubHelper(), par)

end
;+
; :Description:
;    This is the applications user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;     The applications settings hash as defined in `hubApp_ImageSubsetting_application`.
;      
;-
function hubApp_ImageSubsetting_getParameters, settings, defaultValues
  if ~isa(defaultValues) then defaultValues = hash()
  
  internalSettings = settings + defaultValues
  hubHelper = hubHelper()
  CANCELED = Hash('accept', 0b)
  results1 = hubApp_ImageSubsetting_getParametersDialog1(internalSettings, hubHelper)
  if ~results1['accept'] then return, CANCELED
  
  results2 = hubApp_ImageSubsetting_getParametersDialog2(internalSettings, hubHelper, results1)
  
  if ~results2['accept'] then return, CANCELED
  parameters = results1 + results2
  parameters.hubRemove, ['previewImageDraw','coordinateType']
  
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_ImageSubsetting_getParameters

  ; test your routine
  defaultValues = Hash()
  defaultValues['inputImage'] = hub_getTestImage('Hymap_Berlin-A_Image')
  settings = hubApp_getSettings()
  settings['title'] = 'Testsubsetting'
  parameters = hubApp_ImageSubsetting_getParameters(settings, defaultValues)
  print, parameters

end  
