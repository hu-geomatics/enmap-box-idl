function imageMathFunction_spatialSubset, image $
            , sampleStart, sampleEnd, lineStart, lineEnd $
            , ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns the spatial subset of an image',$
      '',$
      'Syntax:',$      
      'result = spatialSubset(image, sampleStart, sampleEnd, lineStart, lineEnd )',$
      '', $
      '']
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(sampleStart) then message, 'sampleStart is undefined'
  if ~isa(sampleEnd) then message, 'sampleEnd is undefined'
  if ~isa(lineStart) then message, 'lineStart is undefined'
  if ~isa(lineEnd) then message, 'lineEnd is undefined'
  
  lineRange = [lineStart, lineEnd]
  sampleRange = [sampleStart, sampleEnd]
  lineRange = lineRange[sort(lineRange)]
  sampleRange = sampleRange[sort(sampleRange)]
  
  parameters = hash()
  parameters['inputImage'] = image
  parameters['outputImage'] = resultFilename
  parameters['sampleRange'] = sampleRange
  parameters['lineRange']   = lineRange
  
  settings = hash('noOpen',1)
  hubApp_ImageSubsetting_processing, parameters, settings 
  return, !null




end