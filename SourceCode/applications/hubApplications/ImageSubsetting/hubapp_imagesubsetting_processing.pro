;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;-

;+
; :Description:
;   This routine creates a spectral and/or spatial subset for a given input image.
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;     Use a hash to provide the following parameters::
;     
;       key               | type     | description
;       ------------------+----------+---------------
;         inputImage      | string[] | array with filepaths of input images 
;         outputImage     | string   | filepath of output image
;         sampleRange     | int[2]   | range of sample indices, e.g [0, 9] for the first 10 samples 
;         lineRange       | int[2]   | range of line indices, e.g [0, 9] for the first 10 lines
;       * maskImage       | string   | filepath of a mask image
;       * bandSubset      | int[]    | array with band indices showing the bands to keep
;                         |          | by default all bands are kept in the output image
;       ------------------+----------+---------------     
;    
;    settings : in, optional, type=hash
;      Use this hash to provide optional GUI relevant parameters::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       noOpen      | bool   | set on true to avoid opening the outputImage in the file list
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+---------------
;-
pro hubApp_ImageSubsetting_processing, parameters, settings
  
   
  helper = hubHelper()
  if ~isa(settings) then settings=hash()
  
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressBar(title=settings.hubGetValue('title') $
                         , groupLeader=settings.hubGetValue('groupLeader'))
    pBar.setInfo, 'Create Image Subset...'
  endif
  
  ;checks
  hubApp_checkKeys, parameters, ['inputImage','outputImage','sampleRange','lineRange']
  
  sampleRange = parameters['sampleRange']
  lineRange = parameters['lineRange']
  
  if n_elements(sampleRange) ne 2 then message, 'sampleRange must contain two values: [min,max]' 
  if n_elements(lineRange) ne 2 then message, 'lineRange must contain two values: [min,max]'
  
  ;create readers & writers...  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  nSamples = inputImage.getMeta('samples')
  nLines = inputImage.getMeta('lines')
  tileLines =  hubIOImg_getTileLines(image=inputImage)
  
  ;check sample/line range against existing ranges from input image
  if sampleRange[0] lt 0 || sampleRange[1] ge nSamples || $
     sampleRange[0] gt sampleRange[1] then begin
     message, string(format='(%"sample range [a,b] must be within [0,%i] and a <= b")',nSamples-1)
  endif
  if lineRange[0] lt 0 || lineRange[1] ge nLines|| $
     lineRange[0] gt lineRange[1] then begin
     message, string(format='(%"line range [a,b] must be within [0,%i] and a <= b")',nLines-1)
  endif
  
  
  inputDataIgnoreValue = inputImage.getMeta('data ignore value')
  inputBands = inputImage.getMeta('bands')
  bandSubset = parameters.hubGetValue('bandSubset', default=indgen(inputBands))
  
  if parameters.hubIsa('maskImage') then maskImage = hubIOImgInputImage(parameters['maskImage'], /Mask) 
  
  outputImage = hubIOImgOutputImage(parameters['outputImage'] $
              , noOpen = settings.hubKeywordSet('noOpen'))
  
  
  
  ;set output meta tags
  outputImage.copyMeta, inputImage, /COPYLABELINFORMATION, /COPYFILETYPE 
    
  ;correct map info 
  if inputImage.hasMeta('map info') then begin
    mapInfo = inputImage.getMeta('map info')
    mapInfo.pixelX = mapInfo.pixelX - sampleRange[0] 
    mapInfo.pixelY = mapInfo.pixelY - lineRange[0]
    outputImage.setMeta, 'map info', mapInfo
  endif
  
  
  bandRelatedTags = ['band names', 'wavelength', 'fwhm', 'data gains values', 'data offset values']
  foreach tag, bandRelatedTags do begin
    if inputImage.hasMeta(tag) then outputImage.setMeta, tag, (inputImage.getMeta(tag))[bandSubset]
  endforeach
  

  ;correct default bands
  if inputImage.getMeta('bands') eq n_elements(bandSubset) then begin
    outputImage.setMeta, 'default bands', inputImage.getMeta('default bands')
  endif else begin
    outputImage.setMeta, 'default bands', [0,0,0]
  endelse

  
  ;set user input specific tag values
  outputDataIgnoreValue = parameters.hubGetValue('outputDataIgnoreValue', default=inputImage.getMeta('data ignore value'))
  outputImage.setMeta, 'data ignore value',outputDataIgnoreValue
  outputDataType = parameters.hubGetValue('outputDataType', default=inputImage.getMeta('data type')) 
  
 
  
  ;init readers & writers...
  inputImage.initReader, tileLines, /Slice, /TILEPROCESSING $
        , SubsetBandPositions = bandSubset $
        , SubsetSampleRange = sampleRange $
        , SubsetLineRange = lineRange
        
  if isa(maskImage) then begin
    maskImage.initReader, tileLines, /SLICE, /TILEPROCESSING, /MASK $
                        , SubsetSampleRange = sampleRange $
                        , SubsetLineRange = lineRange
  endif
  writerSettings = inputImage.getWriterSettings(SetDataType= outputDataType)
  outputImage.initWriter, writerSettings
  
  ;set range of progress bar
  if isa(pBar) then pBar.setRange, [0, writerSettings['lines']*writerSettings['samples']]
        
  ;start subsetting...
  progress = 0l
  while  ~inputImage.tileProcessingDone() do begin
    imageData = inputImage.getdata()
    if isa(maskImage) then begin
      maskData = maskImage.getdata() 
    endif
    
    ;masked values and/or image internal data ignore values
    if isa(maskData) or isa(inputDataIngoreValue) then begin
      maskData = helper.getImageMask(imageData, 1 $
                          , IMAGEDIV=inputDataIngoreValue $
                          , EXTERNALMASK=maskData, /PreserveDimensions)
      
      ;Explicit Casting:
      ;Example: inputData = integer + inputDataIngoreValue = -1
      ;        outputData = float + data ingore value = 23.99
      ;the .99 would be cutted in case the image data gets not casted first                       
      imageData = hubmathHelper.convertData(imageData, outputDataType, /NoCopy)
      
      ;set masked or ignored values to outputDataIgnoreValue
      if isa(outputDataIgnoreValue) then imageData[where(maskData eq 0, /NULL)] = outputDataIgnoreValue     
    end
    if isa(pBar) then begin
      progress += n_elements(imageData[0,*])
      pBar.setProgress, progress
    endif
    outputImage.writeData, imageData
  endwhile
  
  ;copy all remaining tags, except those that are already defined
  optionalTags = (inputImage.getMeta()).keys()

  foreach tag, optionalTags do begin
    if ~outputImage.hasMeta(tag) then outputImage.setMeta, tag, inputImage.getMeta(tag)
  endforeach

  if parameters.hasKey('metaHash') then begin
    header = outputImage.getHeader()
    header.setMetaList, (parameters['metaHash']).keys(), (parameters['metaHash']).values()
  endif
  
  ;cleanup everything
  inputImage.cleanup
  if isa(maskImage) then maskImage.cleanup
  
  outputImage.finishWriter
  outputImage.cleanup
  if isa(pBar) then pBar.cleanup
  
end

;+
; :Hidden:
;-
pro test_hubApp_ImageSubsetting_processing
  ; test your routine
  parameters = hash() ;replace by own hash definition
  settings = hubApp_getSettings()
  
  parameters['inputImage'] = enmapBox_getTestImage('AF_Image')
  parameters['maskImage'] = enmapBox_getTestImage('AF_LC_Validation')
  parameters['lineRange'] = [0,266]
  parameters['sampleRange'] = [0,432]
  parameters['bandSubset'] = [0]

  parameters['outputDataType'] = 4
  parameters['outputImage'] = FILEPATH('output.img',/tmp)
  parameters['outputInterleave'] = 'BSQ'
  parameters['noShow']=0b
  parameters['outputDataIgnoreValue'] =  -1
  hubApp_ImageSubsetting_processing, parameters, settings
  print, 'test done'
end  
