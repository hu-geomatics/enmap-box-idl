;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `hubApp_ImageSubsetting_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `hubApp_ImageSubsetting_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `hubApp_ImageSubsetting_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubApp_ImageSubsetting_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  settings['title'] = 'Image Subsetting'
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  ;restore state from previous application calls to retrieve default values
  defaultValues = hub_getAppState('hubApp', 'stateHash_ImageSubsetting')
  parameters = hubApp_ImageSubsetting_getParameters(settings, defaultValues)
  
  if parameters['accept'] then begin
    ;save parameters as new session state hash
    hub_setAppState, 'hubApp', 'stateHash_ImageSubsetting', parameters
;    watch = hubProgressStopWatch()
    hubApp_ImageSubsetting_processing, parameters, settings
;    watch.showResults, title=settings['title'] $
;                     , description=['Subsetting done','No errors occured']
  endif
end

;+
; :Hidden:
;-
pro test_hubApp_ImageSubsetting_application
  applicationInfo = Hash()
  hubApp_ImageSubsetting_application, applicationInfo

end  
