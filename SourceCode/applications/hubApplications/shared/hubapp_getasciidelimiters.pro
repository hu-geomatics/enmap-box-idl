;+
; :Description:
;    Use this function to get some common ASCII value delimiters or information about.
;    
; :Examples:
;    
;   Print the delimiters::
;     print, transpose(hubApp_getASCIIdelimiters())
;   
;   Print delimiter descriptions::
;     print, transpose(hubApp_getASCIIdelimiters(/DelimiterDescriptions))
;     
; :Keywords:
;    DelimiterIsRegex: in, optional, type=bool
;     Set this on true to return a boolean array containing a flag whether 
;     a delimiter is defined as regular expression or not. 
;     
;    DelimiterDescriptions: in, optional, type=bool
;     Set this to return a description for each delimiter.
;
;-
function hubApp_getASCIIdelimiters $
    , DelimiterIsRegex=DelimiterIsRegex $
    , DelimiterDescriptions=DelimiterDescriptions

  if keyword_set(DelimiterIsRegex) then begin
    return, [0b ,0b        ,0b ,1b]
  endif
  
  if keyword_set(DelimiterDescriptions) then begin
    return, ['[;]   Semicolon' $
            ,'[\t]  Tabulator' $
            ,'[,]   Comma' $
            ,'[  ]  Multiple whitespace characters' $
            ]
  endif
  
  ;else
  return, [';',string(9b),',','[ ]+']
end