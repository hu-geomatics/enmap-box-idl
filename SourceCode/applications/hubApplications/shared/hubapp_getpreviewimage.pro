;+
; :Description:
;    This function calculates a preview from a large raster image.
;
; :Returns: 
;    The function returns the following hash::
;    
;           key         | type                       | description
;      -----------------+----------------------------+-----------------------------------
;       previewImage    | byte[sizeX][sizeY][nBands] | grey values for each requested band
;       pixelPositionsX | int[sizeX]                 | vector with corresponding pixel indices in 
;                       |                            | X-direction from the large raster image 
;       pixelPositionsY | int[sizeY]                 | vector with corresponding pixel indices in
;                       |                            | Y-directionfrom the large raster image
;       ----------------+----------------------------+-----------------------------------
;       
;       with: nBands = number of returned Bands
;             sizeX = pixels in X direction
;             sizeY = pixels in Y direction
;
; :Params:
;    inputImage: in, required, type=hubIOImgInputImage()
;    maxSizeX: in, required, type=int
;     The preview images maximum number of pixels in X direction.
;      
;    maxSizeY: in, required, type=int
;     The preview images maximum number of pixels in Y direction.
;     
; :Keywords:
;    inputMask: in, optional, type=hubIOImgInputImage()
;     A mask used to hide parts of the inputImage.
;     
;    TrueColor: in, optional, type=bool
;     Set this to return a preview image showing the bands in TrueColor representation.
;     
;    ColoredInfrared: in, optional, type=bool
;     Set this to return a preview image showing the bands in Colored Infra Red representation.
;     
;    DefaultBands: in, optional, type=bool
;     Set this to return a preview image showing the default bands of the input image.
;     
;    bandPositions: in, optional, type=int[]
;     Use this to specify the bands used for the preview image explicitely.
;
; :Examples:
;    Here you can see how to get an 200 by 200 pixel preview::
;    
;        inputImg = hubIOImgInputImage(enmapBox_getTestImage('AF_Image'))
;        maskImg  = hubIOImgInputImage(enmapBox_getTestImage('AF_MaskVegetation'))
;        preview =  hubApp_getPreviewImage(inputImg, 200, 200, /TRUECOLOR $
;                , inputMask=maskImg, COLOREDINFRARED=0)
;        inputImg.cleanup
;        
;        cgImage, preview['previewImage']
;
;-
function hubApp_getPreviewImage, inputImage, maxSizeX, maxSizeY $
      , inputMask=inputMask $
      , TrueColor=trueColor $
      , ColoredInfrared=ColoredInfrared $
      , DefaultBands=defaultBands $
      , bandPositions=bandPositions 
      
  hubHelper = hubHelper()
  tileLines = 200
  lines  = inputImage.getMeta('lines')
  samples = inputImage.getMeta('samples')
  dataType = inputImage.getMeta('data type')
  dataIgnoreValue = inputImage.getMeta('data ignore value')
  
  if isa(inputMask) && ~inputImage.isCorrectSpatialSize(inputMask.getSpatialSize()) then begin
    message, 'inputImage and inputMask must have same spatial size'
  endif
  
  maxSize = samples gt lines ? maxSizeX : maxSizeY
  stepX = float(samples) /maxSizeX
  stepY = float(lines) / maxSizeY
  pixelStep = max([1d, stepX, stepY])
  pixelPositionsX = indgen((samples /pixelStep)) * pixelStep
  pixelPositionsY = indgen((lines   /pixelStep)) * pixelStep
  
  pixelPositionsX = round(pixelPositionsX)
  pixelPositionsY = round(pixelPositionsY)
  
  pixelPositionsX = pixelPositionsX[where(pixelPositionsX lt samples, /NULL)]
  pixelPositionsY = pixelPositionsY[where(pixelPositionsY lt lines, /NULL)]
  
  
  
  ;set first band, if bandPositions argument was not defined
  if ~isa(bandPositions) then begin
    ; find RGB or CIR or Default
    if inputImage.hasMeta('wavelength') and inputImage.hasMeta('wavelength units') then begin
      if keyword_set(trueColor) then bandPositions = inputImage.locateWavelength(/TrueColor)
      if keyword_set(coloredInfrared) then bandPositions = inputImage.locateWavelength(/ColoredInfrared)
    endif 
    ; set default bands, if bandPositions argument not defined
    if keyword_set(defaultBands) then bandPositions = inputImage.getMeta('default bands')
  endif
  if ~isa(bandPositions) then bandPositions = [0]
  
  
  ; set class colors
  isClassification = inputImage.isClassification()
  if isClassification then begin
    classLookup = inputImage.getMeta('class lookup')
  endif 
  
  ; set data stretching range in %
  if ~isa(stretchingRange) then begin
    stretchingRange = [2,98]
  endif else begin
    if min(stretchingRange) lt 0 or max(stretchingRange) gt 100 then begin
      message,'stretching range must be within range [0, 100]'
    endif
  endelse
  
  if n_elements(stretchingRange) eq 1 then begin
    stretchingRange = [stretchingRange, 100-stretchingRange]
  endif
  
  
  nBands = n_elements(bandPositions)
  if ~(nBands eq 1 or nBands ge 3) then message, 'number of band positions must be 1 or >= 3'
    
  inputImage.initReader, /SLICE, SubsetBandPositions=bandPositions
  if isa(inputMask) then inputMask.initReader, /SLICE, /Mask
  
  imageValues = make_array(n_elements(pixelPositionsX), n_elements(pixelPositionsY) $
                          , nBands, value=0, type=dataType)
  
  rgbDefault = 0b ;black
  
  ;3D Array with RGB Values
  ;if nBands =  1 -> x,y,3
  ;if nBands >= 3 -> x,y,nBands 
  ;this allows you to get RGB Values for mor than 3 bands and at least grey values
  imageRGB = make_array(n_elements(pixelPositionsX), n_elements(pixelPositionsY) $
             , nBands eq 1? 3 : nBands $
             , value=rgbDefault, /BYTE)
             
 
  
  mask = make_array(n_elements(pixelPositionsX), n_elements(pixelPositionsY), value=1b, /BYTE)
  
  
  ;get the data values
  foreach iLine, pixelPositionsY, i do begin
    imageLine = i
    lineData = inputImage.getData(iLine)
    lineData = lineData[*,pixelPositionsX]
    
    imageValues[*,imageLine,*] = transpose(hub_fix3d(lineData), [1,0])
    
    ;set mask
    if isa(inputMask) then begin
      maskData = inputMask.getData(iLine)
      maskData = maskData[pixelPositionsX]
    endif
    maskLine = hubHelper.getImageMask(lineData, 1, IMAGEDIV=dataIgnoreValue, EXTERNALMASK=maskData) 
    mask[*,imageLine] = maskLine
  endforeach
  
  
  ;set the RGB color values
  iValid = where(mask ne 0b, /NULL)
  
  bandIndices = (nBands eq 1) ? [0,0,0] : indgen(nBands)
  
  if isa(iValid) then begin
    foreach iBand, bandIndices, iRGB do begin
        channelValues = imageValues[*,*,iBand]
        channelRGB = imageRGB[*,*,iRGB]
        if isClassification then begin
          channelRGB[iValid] = classLookup[iRGB,channelValues[iValid]] 
        endif else begin
          
        
          ;set scaled continuous colors
          hist = histogram(float(channelValues[iValid]), locations=locations,nBins = 255)
          cumHistogram = total(hist, /PRESERVE_TYPE, /CUMULATIVE)
          cumDistribution = (100d*cumHistogram / cumHistogram[-1])
          stretchMin = (where(cumDistribution ge stretchingRange[0]))[0]
          stretchMax = (where(cumDistribution le stretchingRange[1]))[-1]
          
          channelRGB[iValid] = bytscl(channelValues[iValid] , min=locations[stretchMin] $
                                                            , max=locations[stretchMax])
        endelse
        imageRGB[*,*,iRGB] = channelRGB
    endforeach
  endif
  
   results = Hash()
   results['previewImage'] = imageRGB
   results['pixelPositionsX'] = pixelPositionsX
   results['pixelPositionsY'] = pixelPositionsY
  return, results
end

;+
; :hidden:
;-
pro test_hubAPP_getPreviewImage

  inputImg = hubIOImgInputImage(enmapBox_getTestImage('AF_Image'))
  maskImg  = hubIOImgInputImage(enmapBox_getTestImage('AF_MaskVegetation'))
  preview =  hubApp_getPreviewImage(inputImg, 200, 200, /TRUECOLOR $
          , inputMask=maskImg, COLOREDINFRARED=0)
  inputImg.cleanup
  
  cgImage, preview['previewImage']

  
  
end