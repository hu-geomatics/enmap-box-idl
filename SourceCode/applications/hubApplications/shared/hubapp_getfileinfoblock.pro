;+
; :Description:
;    Returns file meta information as formats string list
;
; :Params:
;    metaInfo: in, type = hash
;
;
;-
function hubApp_getFileInfoBlock, metaInfo
  
  block = list()
  block.add, string(format='(%"file path: %s")', metaInfo['filename data'])
  
  ns = metaInfo['samples']
  nl = metaInfo['lines']
  nb = metaInfo['bands']
  ft = metaInfo['file type']
  
  block.add, string(format='(%"dimension: %i x %i x %i (samples x lines x bands)")', ns, nl, nb )
  block.add, string(format='(%"           %i pixel in total")', ns * nl )
  block.add, string(format='(%"data type: %i = %s")', metaInfo['data type'] $
           , hubHelper.getDataTypeInfo(metaInfo['data type'], /TYPEDESCRIPTIONS))
  block.add, string(format='(%"file type: %s")', ft)
  
  if metaInfo.hubIsa('data ignore value') then begin
    block.add, string(format='(%"data ignore value: %s")', strtrim(metaInfo['data ignore value'],2)) 
  endif else begin
    if not stregex('classification', ft, FOLD_CASE=1, /BOOLEAN) then begin 
      block.add, 'data ignore value: -- unspecified (!) --'
    endif
  endelse
  
  if metaInfo.hubIsa('classes') then begin
  block.add, string(format='(%"classes  : %i (%s)")', metaInfo['classes'], strjoin(metaInfo['class names'],', ', /SINGLE))
  endif
  
  block.add, ''
  
  primary = ['samples','lines','bands','file type','filename data','data type' $
            ,'data ignore value', 'classes', 'class names','class lookup']
  foreach key, metaInfo.keys() do begin
    value = metaInfo[key]
    if ~total(primary eq STRLOWCASE(key)) $
      and stregex(typename(value), '(STRING|INT|LONG|BYTE|DOUBLE|FLOAT)', /BOOLEAN) then begin
      block.add, string(format='(%"%s: %s")', STRLOWCASE(key), strjoin(strtrim(metaInfo[key],2), ','))
    endif
  endforeach
     
  return, block.toArray()
end
