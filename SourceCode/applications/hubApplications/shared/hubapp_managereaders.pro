;+
; :Description:
;    This routine can be used to initialize, finish or cleanup the data readers of multiple 
;    hubIOImgInputImage objects.  
;
; :Params:
;    images: in, required, type = Array or List of hubIOImgInputImage object
;    tileLines: in, optional, type=int
;     Number of tile lines to be used in case of tiled data reading. 
;     Using this parameter leads will call the underying `/TileProcessing` keyword
;     of each single initReader() method.
;     
;
; :Keywords:
;    SubsetBandPositions: in, optional, type=List()
;    SubsetSampleRanges: in, optional, type=List()
;    SubsetLineRanges: in, optional, type=List()
;    DataTypes: in, optional, type=string[] or int[] or list
;    BAND: in, optional, type=boolean
;    SLICE: in, optional, type=boolean
;    CUBE: in, optional, type=boolean
;    VALUE: in, required, type=boolean
;    INIT: in, required, type=boolean
;    FINISH:in, required, type=boolean
;    CLEANUP:in, required, type=boolean
;    maskImages:in, optional, type=Array or List of hubIOImgInputImage objects
;    ForegroundMaskValues: in, optional, type=numeric[]
;    BackgroundMaskValues:in, optional, type=numeric[]
;
;-
pro hubApp_manageReaders $
      , images $
      , tileLines $
      , SubsetBandPositions = subsetBandPositions $
      , SubsetSampleRanges = subsetSampleRanges $
      , SubsetLineRanges = subsetLineRanges $
      , DataTypes = dataTypes $
      , BAND = BAND, SLICE=SLICE, CUBE=CUBE, VALUE=VALUE $
      , INIT=INIT, FINISH=FINISH, CLEANUP=CLEANUP $
      , maskImages = maskImages $
      , ForegroundMaskValues = ForegroundMaskValues $
      , BackgroundMaskValues = BackgroundMaskValues
  
  ;Initial checks
  key = total([keyword_set(finish),keyword_set(init), keyword_set(cleanup)])
  if key eq 0 then begin
      message, 'Set one of the exclusive keywords INIT, FINISH or CLEANUP'
  endif
  if key gt 1 then begin
      message, 'Keywords INIT, FINISH and CLEANUP are exclusive'
  endif
  if keyword_set(init) and $
     total([keyword_set(BAND),keyword_set(SLICE), keyword_set(CUBE), keyword_set(VALUE)]) ne 1 then begin
      message, 'Exclusive Keyword BAND, SLICE, CUBE or VALUE must be set for initializing reading'
  endif
  
  imageList = list()
  maskList  = list()
  if isa(images) then imageList.add, images, /Extract
  if isa(maskImages) then maskList.add, maskImages, /Extract
  
  ;remove !NULL entries
  iIsNullImg = where(imageList eq !NULL, /NULL)
  if isa(iIsNullImg) then imageList.remove, iIsNullImg
  
  iIsNullMask = where(maskList eq !NULL, /NULL)
  if isa(iIsNullMask) then maskList.remove, iIsNullMask
  
  nImages = n_elements(imageList)
  nMasks = n_elements(maskList)
  nFGValues = n_elements(ForegroundMaskValues)
  nBGValues = n_elements(BackgroundMaskValues)
  
  if isa(dataTypes) && n_elements(dataTypes) ne nImages then message, 'Output data type required for each input image'
  if isa(ForegroundMaskValues) && nMasks ne nFGValues then message, string(format='(%"%i mask images but %i foreground values.")', nMasks, nFGValues)
  if isa(BackgroundMaskValues) && nMasks ne nBGValues then message, string(format='(%"%i mask images but %i background values.")', nMasks, nBGValues)
  if isa(maskForegroundValues) and isa(BackgroundMaskValues) then begin
    for i=0, nFGValues do begin
      if isa(ForegroundMaskValues[i]) and isa(BackgroundMaskValues[i]) then message, 'Set either a foreground or background value for a mask image'
    endfor
  endif
  
  ;init images
  if keyword_set(init) then begin 
    isTileProcessing = keyword_set(tileLines)
    
    foreach inputImage, imageList, i do begin
        inputImage.initReader, tileLines $ 
                      , BAND=BAND, SLICE=SLICE, CUBE=CUBE, VALUE=VALUE $
                      , TileProcessing = isTileProcessing $
                      , SubsetSampleRange = isa(SubsetSampleRanges)? SubsetSampleRanges[i] : !NULL $
                      , SubsetBandPositions = isa(SubsetBandPositions) ? SubsetBandPositions[i] : !NULL $
                      , SubsetLineRange = isa(SubsetLineRanges) ? SubsetLineRanges[i] : !NULL $ 
                      , DataType = isa(DataTypes)? DataTypes[i] : !NULL
    endforeach
    
    foreach maskImage, maskList, i do begin
      maskImage.initReader, tileLines, BAND=BAND, SLICE=SLICE, CUBE=CUBE, VALUE=VALUE $
                     , TileProcessing = isTileProcessing $
                     , /Mask $
                     , ForegroundMaskValue = isa(ForegroundMaskValues)? ForegroundMaskValues[i] : !NULL $
                     , BackgroundMaskValue = isa(BackgroundMaskValues)? BackgroundMaskValues[i] : !NULL
    endforeach
  endif 
    
  if keyword_set(finish) then begin
    foreach inputImage, imageList do inputImage.finishReader
    foreach maskImage, maskList do maskImage.finishReader
  endif
  
  if keyword_set(cleanup) then begin
    foreach inputImage, imageList do inputImage.cleanup
    foreach maskImage, maskList do maskImage.cleanup
  endif
  
end


;+
; :hidden:
; :Description:
;    Describe the procedure.
;
;
;
;
;
;-
pro test_hubApp_manageReaders

  image1 = hubIOImgInputImage(hub_getTestImage('Hymap_Berlin-A_Image'))
  image2 = hubIOImgInputImage(hub_getTestImage('Hymap_Berlin-A_Image'))
  mask = hubIOImgInputImage(hub_getTestImage('Hymap_Berlin-A_Mask'))
  strat = hubIOImgInputImage(hub_getTestImage('Hymap_Berlin-A_Classification-GroundTruth'))
  
  tileLines= 100
  hubApp_manageReaders, /INIT, [image1, image2, !NULL], tileLines $
          , SubsetBandPositions=List([2,45], [33,44,55]) $
          ;, DataTypes=['double','float'] $
          , /SLICE $
          , maskImages=[mask, strat] 
         ; , ForegroundMaskValues=List(!NULL, 3)
          
  while ~image1.tileProcessingDone() do begin
    help, image1.getData()
    help, image2.getData()
    help, mask.getData()
    help, strat.getData()
    print, '----'
  endwhile
  
  hubApp_manageReaders, /FINISH, [image1, image2], maskImages=[mask,strat]
  hubApp_manageReaders, /CLEANUP, [image1, image2], maskImages=[mask,strat]
  
  print, 'Done'
end

