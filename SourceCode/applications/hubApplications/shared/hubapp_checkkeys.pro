;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

;+
; :Description:
;    Checks if all values in `requiredKeys` exists as Hash-Keys in 
;    `parameters` with an value unequal !NULL. 
;
; :Params:
;    parameters: in, required, type=hash
;     The Hash you want to check for the keys with values unequal !NULL.
;
;    requiredKeys: in, required, type=array
;     The Array with hash-keys to check.
;
;  :Examples:
;     
;     This throws an error because key `B` does not exist in hash `parameters`::
;     
;       parameters = Hash('A',1,'C',42.23)
;       hubApp_checkKey, parameters, ['A','B','C']
;
;-
pro hubApp_checkKeys, parameters, requiredKeys
  if ~parameters.hubIsa(requiredKeys, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(requiredKeys[missing],', ')
  endif
end