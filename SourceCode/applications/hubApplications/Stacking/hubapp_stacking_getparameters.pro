
function hubApp_Stacking_getParametersCheck, parameters, message=message
  msg = !NULL
  images = parameters['inputImages']
  nImages = n_elements(images) 
  if nImages lt 2 then begin
    msg = [msg, 'Select at least two images']
  endif else begin
    correctSize = make_array(nImages, /BYTE, value=1b)
    image1 = hubIOImgInputImage(images[0])
    for i=1, nImages - 1 do begin
      image2 = hubIOImgInputImage(images[1])
      correctSize[i] = image1.iscorrectSpatialSize(image2.getSpatialSize())
      image2.cleanup
    endfor
    
    if product(correctSize) eq 0 then begin
      msg = [msg, 'Selected images must have same spatial size']
    endif
  endelse

  message = msg
  return, ~isa(msg)
end

function hubApp_Stacking_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  helper = hubHelper()
  
  dataTypeCodes        = [-1, helper.getDataTypeInfo(/TypeCode)] 
  dataTypeDescriptions = ['<from image with largest data type size>', helper.getDataTypeInfo(/TypeDescription)]
  
  
  imageFileList = hubProEnvHelper.getFileNames(/Image)
  parameters = Hash('accept', 0b)
  
  if n_elements(imageFileList) eq 0 then begin
    !NULL = Dialog_message('Please load images to your file list first.', /Information)  
  endif else begin
  
    hubAMW_program , groupLeader, Title = settings.hubGetValue('title')
    hubAMW_frame ,   Title =  'Input'
    
    hubAMW_list, 'inputImages', Title='Select images to stack' $
               , LIST    = file_basename(imageFileList) $
               , Extract = imageFileList $
               , ALLOWEMPTYSELECTION = 0 $
               , MULTIPLESELECTION   = 1 $
               , XSIZE=400, YSIZE = n_elements(imageFileList) * 50 < 300
    
    ; frame for output options & parameters
    hubAMW_frame ,   Title =  'Output'
    hubAMW_parameter, 'dataIgnoreValue', Title='Data Ignore Value', /FLOAT, Optional=2
    hubAMW_combobox, 'outputDataType' $
            , Title='Data Type  ' $
            , List = dataTypeDescriptions $
            , Extract = dataTypeCodes  
            
    
    hubAMW_outputFilename, 'outputImage' $
            , Title='Stack Image' $
            , Value='imageStack'
    
    parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_Stacking_getParametersCheck')
    
    if parameters['accept'] then begin
      if parameters['outputDataType'] eq -1 then begin
        parameters.hubRemove, 'outputDataType'
      endif
    endif
  
  endelse
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_Stacking_getParameters

  ; test your routine
  settings = hubApp_getSettings()
  parameters = hubApp_Stacking_getParameters(settings)
  print, parameters

end  
