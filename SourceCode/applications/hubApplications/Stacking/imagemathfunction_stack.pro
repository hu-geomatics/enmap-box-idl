function imageMathFunction_stack, img0, img1, img2, img3, img4, img5,img6,img7,img8,img9 $
            , ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'Returns an image stack of up to 10 input images',$
      '',$
      'Syntax:',$      
      'result = stack(image1, image2, ..., image10)',$
      '']
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(img0) then message, 'Missing argument: image1'
  if ~isa(img1) then message, 'Missing argument: image2'
  
  images = [img0, img1]
  if isa(img2) then images = [images, img2]
  if isa(img3) then images = [images, img3]
  if isa(img4) then images = [images, img4]
  if isa(img5) then images = [images, img5]
  if isa(img6) then images = [images, img6]
  if isa(img7) then images = [images, img7]
  if isa(img8) then images = [images, img8]
  if isa(img9) then images = [images, img9]
  
  parameters = hash()
  parameters['inputImages'] = images
  parameters['outputImage'] = resultFilename
  
  settings = hash('noOpen',1)
  hubApp_Stacking_processing, parameters, settings 
  return, !null



end