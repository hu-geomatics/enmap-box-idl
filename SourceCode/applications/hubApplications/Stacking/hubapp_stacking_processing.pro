
;+
; :Description:
;    This procedure performs a simple stacking of different images with same spatial 
;    pixel dimensions.
;
; :Params:
;    parameters: in, required, type=hash()
;     Use this hash to provide the following parameters::
;     
;       key               | type     | description
;       ------------------+----------+---------------
;         inputImages     | string[] | array with filepaths of input images 
;         outputImage     | string   | filepath of output image
;       * dataIgnoreValue | numeric  | the output images data ignore value
;       * outputDataType  |          | the IDL data type of the final image
;                         |          | can be given as string or byte value
;                         |          | default: largest data type of all input images
;       * metaHash        | hash     | meta hash, keys/values are the metadata names/values
;       ------------------+----------+---------------     
;     
;      In case the `dataIgnoreValue` is defined for the output image and a certain 
;      input image has a defined ignore value too, all ignored input pixels will 
;      be mapped to `dataIgnoreValue`.
;    
;    settings: in, optional, type = hash
;     Use this hash to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       noOpen      | bool   | set on true to avoid opening the outputImage in the file list
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+---------------
;
;
;-
pro hubApp_Stacking_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputImages','outputImage']
  
  _settings = isa(settings) ? settings : Hash('noShow', 1b)
  
  if ~_settings.hubKeywordSet('noShow') then begin
    ;init the progressBar
    progressBar = hubProgressbar( $
          title=_settings.hubGetValue('title', default='Image Stacking') $
        , GroupLeader=_settings.hubGetValue('groupLeader') $
        , info='Please wait...')
    progressDone = 0l
    
  endif
  
  
  
  hubHelper = hubHelper()
  
  inputImages = parameters['inputImages']
  nImages = n_elements(inputImages)
  
  outputDIV = parameters.hubGetValue('dataIgnoreValue')
  
  image1 = hubIOImgInputImage(inputImages[0])
  nSamples1 = image1.getMeta('samples')
  nLines1   = image1.getMeta('lines')
  nBands1   = image1.getMeta('bands')
  
  nBandsTotal = nBands1

  
  dataTypes   = make_array(nImages, /BYTE, value=1b)
  nBands       = make_array(nImages, /Integer, value=0b)
  nSamples     = make_array(nImages, /Long, value=0b)
  nLines       = make_array(nImages, /Long, value=0b)
  mapInfos = list()
  defaultBands = !NULL
  ;get informations for all images
  foreach imgPath, inputImages, i do begin
    img = hubIOImgInputImage(imgPath) 
    
    if ~isa(defaultBands) && img.hasMeta('default bands') then begin
      defaultBands = img.getMeta('default bands') + total(nBands[0:i])
    endif
    
    nSamples[i] = img.getMeta('samples')
    nLines[i]   = img.getMeta('lines')
    nBands[i]   = img.getMeta('bands')
    dataTypes[i] = img.getMeta('data type')
    if img.hasMeta('map info') then mapInfos.add,  img.getMeta('map info')
    
    img.cleanup
  endforeach
  
  ;check spatial size 
  if isa(where(nSamples ne nSamples[0] or nLines ne nLines[0], /NULL)) then begin
    message, 'All Images must have same spatial size.'
  endif 
  
  nSamples = nSamples[0]
  nLines = nLines[0]
  nBandsOutput = total(nBands)
  nBandsMax = max(nBands)
  
  outputDataType = parameters.hubGetValue('outputDataType', default=!NULL)
  if ~isa(outputDatatype) then begin
    case 1b of   
      total(dataTypes eq 5) ne 0: outputDataType = 5 ;double
      total(dataTypes eq 4) ne 0: outputDataType = 4 ;float
      else : begin
        outputDatatype = max(dataTypes)
        end
    endcase
  endif
  outputDataType = hubHelper.getDataTypeInfo(outputDataType,TYPECODES=1)
  
  
  
  tileLines = hubIOImg_getTileLines(nSamples, nBandsMax, outputDataType)
  
  
  outputImage = hubIOImgOutputImage(parameters['outputImage'] $
                , NoOpen=_settings.hubKeywordSet('noOpen'))
                
  outputImage.setMeta, 'samples',nSamples
  outputImage.setMeta, 'lines', nLines
  outputImage.setMeta, 'bands',nBandsOutput
  if isa(defaultBands) then outputImage.setMeta, 'default bands', defaultBands
  if n_elements(mapInfos) gt 0 then begin
    outputImage.setMeta, 'map info',  mapInfos[0]
  endif
  outputImage.setMeta, 'data type', outputDatatype
  outputBandNames = list()
  
  if isa(progressBar) then begin
    progressBar.setRange, [0, nBandsOutput * nLines]
  endif
  
  foreach inputImagePath, inputImages do begin
    inputImage = hubIOImgInputImage(inputImagePath)
    bandNames = inputImage.getMeta('band names')
    if isa(bandNames) then begin
      outputBandNames.Add, bandNames, /EXTRACT
    endif else begin
      outputBandNames.add, sindgen(inputImage.getMeta('bands'))
    endelse
    
    if inputImage.isClassification() then begin
      inputDIV = 0b
    endif else begin
      inputDIV = inputImage.getMeta('data ignore value')
    endelse
    inputImage.initReader, tileLines, /BAND, /TileProcessing
    
    if ~isa(writerSettings) then begin
      writerSettings =  inputImage.getWriterSettings( $
                                 SetBands=nBandsOutput $
                                ,SetDataType=outputdatatype)
      
      writerSettings['tileProcessing'] = 0b ;write as stream
      outputImage.initWriter, writerSettings
    endif
    
    while ~inputImage.tileProcessingDone() do begin
      data = inputImage.getData()
      
      bandMask = !NULL
      if isa(inputDIV) && isa(outputDIV) then begin
        bandMask = data ne inputDIV 
      endif 
      
      ;convert to target type
      data = hubmathHelper.convertData(data, outputDatatype, /NoCopy)
      
      ;set ignored pixels to output data ignore values
      if isa(bandMask) then begin
        data = data * bandMask + (~bandMask) * outputDIV
        
      endif
      
      ;write data
      outputImage.writeData, data
      
      if isa(progressBar) then begin
        progressDone += tileLines
        progressBar.setProgress, progressDone
      endif
      
    endwhile
    inputImage = !NULL
  endforeach
  
  outputImage.setMeta,'band names', outputBandNames.toArray()
  
  if parameters.hasKey('metaHash') then begin
    header = outputImage.getHeader()
    header.setMetaList, (parameters['metaHash']).keys(), (parameters['metaHash']).values()
  endif
  outputImage = !NULL
  if isa(progressBar) then progressBar.cleanup
  heap_gc
end


;+
; :Hidden:
;-
pro test_hubApp_Stacking_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = hubApp_getSettings()
  parameters['inputImages'] = [enmapBox_getTestImage('AF_LAI_Training') $
                              ,enmapBox_getTestImage('AF_LC') $
                              ,enmapBox_getTestImage('AF_LC_Training') $ 
                              ]
  parameters['dataIgnoreValue'] = -999
  parameters['outputImage'] = 'D:\testStackedImage'
  hubApp_Stacking_processing, parameters, settings
  
  print, 'done!'

end  
