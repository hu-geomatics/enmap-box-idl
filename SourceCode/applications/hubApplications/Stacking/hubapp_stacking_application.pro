;+
; :Description:
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubApp_Stacking_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  settings['title'] = 'Stack Images'
  parameters = hubApp_Stacking_getParameters(settings)
  
  if parameters['accept'] then begin
    hubApp_Stacking_processing,parameters, settings
  endif
  
end

;+
; :Hidden:
;-
pro test_hubApp_Stacking_application
  applicationInfo = Hash()
  hubApp_Stacking_application, applicationInfo

end  
