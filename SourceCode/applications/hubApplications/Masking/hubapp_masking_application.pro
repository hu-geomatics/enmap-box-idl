;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `hubApp_Masking_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `hubApp_Masking_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `hubApp_Masking_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro hubApp_Masking_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  settings['title'] = 'Apply Mask'
  parameters = hubApp_Masking_getParameters(settings)
  if parameters['accept'] then begin
    hubApp_Masking_processing, parameters, settings  
  endif
end

;+
; :Hidden:
;-
pro test_hubApp_Masking_application
  applicationInfo = Hash()
  hubApp_Masking_application, applicationInfo

end  
