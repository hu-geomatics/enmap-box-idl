;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    This function performs an explicit consistency check of user defined 
;    input parameters. It is called by `hubApp_Masking_getParameters`. 
;-
function hubApp_Masking_getParametersCheck, parameters, message=message
  msg = !NULL
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  inputMask  = hubIOImgInputImage(parameters['inputMask'])
  if ~inputImage.isCorrectSpatialSize(inputMask.getspatialSize()) then begin
    msg = [msg, 'Input image and input mask must have same spatial size']
  endif
  
  if ~parameters.hubIsa('dataIgnoreValue') then begin
    if ~inputImage.isClassification() &&  $
       ~inputImage.hasMeta('data ignore value') then begin
      msg = [msg, "Input image does not specify a 'data ignore value'. Please set explicitely."]
    endif
  endif
  
  if ~inputMask.isMask() then begin
    msg = [msg, FILE_BASENAME(parameters['inputMask']) + ' is not a valide mask file']
  endif
  inputImage.cleanup
  inputMask.cleanup
  
  message = msg
  return, ~isa(msg)
end

;+
; :Description:
;    Use this routine to collect the parameters required for running the 
;    `hubAPP_masking_processing` routine. 
;
; :Params:
;    settings: in, optional, type=hash
;     Use this hash to provide following basic application infos::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | this applications title 
;       ------------+--------+---------------  
;
;-
function hubApp_Masking_getParameters, settings
  if ~isa(settings) then settings = hash()
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings.hubGetValue('title')
  
  tsize=90
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputImageFilename, 'inputImage', TITLE='Image', tsize=tsize, value=settings.hubGetValue('inputImage')
  hubAMW_inputImageFilename, 'inputMask' , TITLE='Mask ', tsize=tsize, value=settings.hubGetValue('inputMask')
  
  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputFilename, 'outputImage'   , TITLE='Masked Image', tsize=tsize, value=settings.hubGetValue('outputImage', default='maskedImage')
  
  hubAMW_label, 'Mask value / data ignore value'
  hubAMW_subframe,'useDIV', /COLUMN, TITLE='Choose from input image', /SETBUTTON
  hubAMW_subframe,'useDIV', /COLUMN, TITLE='Specify new value'
  hubAMW_parameter, 'dataIgnoreValue', Title='', /FLOAT
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_Masking_getParametersCheck')
  
  if parameters['accept'] then begin
    inputImage = hubIOImgInputImage(parameters['inputImage'])
    if ~parameters.hubIsa('dataIgnoreValue') then begin
      case 1b of
        inputImage.hasMeta('data ignore value') : parameters['dataIgnoreValue'] = inputImage.getMeta('data ignore value')
        inputImage.isClassification() : parameters['dataIgnoreValue'] = 0b
      endcase
    endif 
    
    inputImage.cleanup
  endif
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_Masking_getParameters

  ; test your routine
  settings = hubApp_getSettings()
  settings['inputImage'] = hub_getTestImage('Hymap_Berlin-A_Image')
  settings['inputMask'] = hub_getTestImage('Hymap_Berlin-A_Mask')
  settings['title'] = 'Masking'
  parameters = hubApp_Masking_getParameters(settings)
  print, parameters

end  
