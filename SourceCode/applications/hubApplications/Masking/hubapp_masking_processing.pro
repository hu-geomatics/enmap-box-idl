;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    This routine combines a mask with an image. The resulting output image
;    will have all masked or non-data values having set to the data ignore value.
;    
;    Note that it is required to use an inputImage with `data ignore value` is defined, 
;    or to set the `outputDataIgnoreValue` explicitely.
;      
;      
;      
;      
;
; :Params:
;    parameters: in, required, type=hash
;     This is the parameter hash containing the following values::
;     
;       key             | type    | description
;       ----------------+---------+---------------
;       inputImage      | string  | filepath of input image
;       inputMask       | string  | filepath of mask image 
;       outputImage     | string  | filepath of final masked image
;       dataIgnoreValue | numeric | data ignore value to mask pixels in the output image
;       ----------------+---------+---------------       
;     
;     Two important optional and exclusive parameters are `foregroundMaskValue` and `BackgroundMaskValue`.
;    
;     Use `foregroundMaskValue` to specify a single pixel value that marks all valid pixels. 
;     
;     Use `backgroundMaskValue` to specify a single pixel value that marks all invalid / masked pixels 
;     
;       
;    
;    settings: in, optional, type = hash
;     This hash can be used to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+---------------
;     
;
;-
pro hubApp_Masking_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputImage','inputMask','outputImage']
  
  _settings = isa(settings) ? settings : Hash()

  helper = hubHelper()
  
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  nBands = inputImage.getMeta('bands')
  
  inputMask  = hubIOImgInputImage(parameters['inputMask'], /Mask)
  
  tileLines = hubIOImg_getTileLines(inputImage.getMeta('samples') $
                                  , inputImage.getMeta('bands') + 1 $
                                  , inputImage.getMeta('data type'))
                                    
  if ~_settings.hubKeywordset('noShow') then begin
    ;init the progressBar
    progressBar = hubProgressbar( $
          title=_settings.hubGetValue('title', default='Masking') $
        , GroupLeader=_settings.hubGetValue('groupLeader') $
        , info='Please wait...')
    progressDone = 0l
    progressBar.setRange, [0, inputImage.getMeta('lines')]  
  endif
    
  if inputImage.isClassification() then begin
    inputDIV = !NULL
    outputDIV = 0
  endif else begin
    inputDIV = inputImage.getMeta('data ignore value')
    outputDIV = parameters.hubGetValue('dataIgnoreValue', default=inputDIV)
    if ~isa(outputDIV) then begin
      message, "missing data ignore value for the output file. Use 'dataIgnoreValue' or an inputImage having 'data ignore value' defined."
    endif
  endelse
  
  if ~inputMask.isMask() then begin
    message, 'Can not use inputMask as mask image'
  endif
  
  outputImage = hubIOImgOutputImage(parameters['outputImage'])
  outputImage.copyMeta, inputImage, /COPYFILETYPE, /COPYLABELINFORMATION, /COPYSPATIALINFORMATION, /COPYSPECTRALINFORMATION
  
  inputImage.initReader, tileLines, /Slice, /TileProcessing
  inputMask.initReader, tileLines, /Slice, /TileProcessing, /Mask $
    , ForegroundMaskValue=parameters.hubGetValue('foregroundMaskValue') $
    , BackgroundMaskValue=parameters.hubGetValue('backgroundMaskValue')
    
  outputImage.initWriter, inputImage.getWriterSettings()
  
  ;copy all meta values that are not defined until now
  foreach tag, (inputImage.getMeta()).keys() do begin
    if ~outputImage.hasMeta(tag) then begin
      outputImage.setMeta, tag, inputImage.getMeta(tag)
    endif
  endforeach
  
  maskInitialized = 0b
  while ~inputImage.tileProcessingdone() do begin
    data = inputImage.getData()
    mask = inputMask.getData()
    if isa(inputDIV) then begin
      for iB = 0, nBands-1 do begin
        data[iB, where(data[iB,*] eq inputDIV, /NULL)] = outputDIV
      endfor
    endif
    data[*,where(mask eq 0, /NULL)] = outputDIV
    outputImage.writeData, data
    
    if isa(progressBar) then begin
      progressDone += tileLines
      progressBar.setProgress, progressDone
    endif
    
  endwhile

  inputImage.cleanup
  inputMask.cleanup
  outputImage.cleanup
  if isa(progressBar) then progressBar.cleanup
end

;+
; :Hidden:
;-
pro test_hubApp_Masking_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  parameters['inputImage'] = enmapBox_getTestImage('AF_Image')
  parameters['inputMask'] = enmapBox_getTestImage('AF_LC_Validation')
  parameters['outputImage'] = 'D:\TestMasked'
  hubApp_Masking_processing, parameters
  print, 'Done!'

end  
