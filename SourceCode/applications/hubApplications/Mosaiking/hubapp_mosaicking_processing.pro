;+
; :Description:
;    This procedure performs pixel-based image mosaicking. Currently, all images are outlayed in a column.
;
; :Params:
;    parameters: in, required, type=hash()
;     Use this hash to provide the following parameters::
;     
;       key               | type     | description
;       ------------------+----------+---------------
;         inputFilenames  | string[] | input image filenames 
;         outputFilename  | string   | output image filename
;         outputDataType  |          | the IDL data type of the final image
;                         |          | can be given as string or byte value.
;                         |          | Default is the data type of the first image.
;       * dataIgnoreValue | numeric  | the output images data ignore value
;       ------------------+----------+---------------     
;     
;      In case the `dataIgnoreValue` is defined for the output image and a certain 
;      input image has a defined ignore value too, all ignored input pixels will 
;      be mapped to `dataIgnoreValue`.
;    
;-
pro hubApp_mosaicking_processing, parameters, Title=title, GroupLeader=groupLeader, NoOpen=noOpen, NoShow=noShow
  hubApp_checkKeys, parameters, ['inputFilenames','outputFilename']
  
  progressBar = hubProgressbar(Title=parameters.hubGetValue('title', Default='Image Mosaicking'), GroupLeader=groupLeader, NoShow=keyword_set(noShow))
  
  inputImages = list()
  foreach filename, parameters['inputFilenames'] do begin
    inputImages.add, hubIOImgInputImage(filename)
  endforeach
  
  samples = (inputImages[0]).getMeta('samples')
  lines = 0ul
  bands = (inputImages[0]).getMeta('bands')
  dataType = parameters.hubGetValue('outputDataType', Default=(inputImages[0]).getMeta('data type'))
  foreach inputImage, inputImages do begin 
    lines += inputImage.getMeta('lines')
    if (samples ne inputImage.getMeta('samples')) or (bands ne inputImage.getMeta('bands')) then begin
      message, 'Number of samples must match for all images.'
    endif
  endforeach

  writerSettings = hash()
  writerSettings['samples'] = samples
  writerSettings['lines'] = lines
  writerSettings['bands'] = bands
  writerSettings['data type'] = dataType
  writerSettings['dataFormat'] = 'slice'
  writerSettings['tileProcessing'] = 1
  outputImage = hubIOImgOutputImage(parameters['outputFilename'], NoOpen=noOpen)
  outputImage.initWriter, writerSettings

  numberOfTileLines = hubIOImg_getTileLines(samples, bands, dataType)
  foreach inputImage, inputImages, i do begin
    progressBar.setProgress, float(i+1)/inputImages.count()

    inputImage.initReader, numberOfTileLines, /Slice, /TileProcessing
    while ~inputImage.tileProcessingDone() do begin
      data = inputImage.getData()
      outputImage.writeData, data
    endwhile
    inputImage.cleanup
  endforeach
  progressBar.cleanup
  outputImage.cleanup
  
end

;+
; :Hidden:
;-
pro test_hubApp_mosaicking_processing
  enmapbox
  parameters = hash()
  parameters['inputFilenames'] = replicate(hub_getTestImage('Hymap_Berlin-A_Image'), 2) 
  parameters['outputFilename'] = filepath('mosaic', /TMP)
  hubApp_mosaicking_processing, parameters
end  
