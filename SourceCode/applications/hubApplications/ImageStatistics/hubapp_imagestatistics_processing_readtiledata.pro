;+
; :Copyright:
; :hidden:
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-


;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    image
;
; :Keywords:
;    imgROI
;    imgMask
;    dataImg
;    dataMask
;    dataStratifiedMask
;
; :Examples:
;
;-
pro hubApp_imageStatistics_processing_readTileData $
  ;input
  , image $
  ;input optional
  , imgROI = imgROI, imgMask = imgMask $
  ;output
  , dataImg=dataImg, dataMask=dataMask, dataStratifiedMask = dataStratifiedMask
  
  
  dataImg = image.getData()
  
  nPixels = n_elements(dataImg[0,*])
  nBands  = n_elements(dataImg[*,0])
  dataMask = make_array(nBands, nPixels,value=1b, /BYTE)
  
  div = image.getMeta('data ignore value')
  
  
  ;consider internal data ignore values
  if isa(div) then dataMask *= (dataImg ne div)
 ; if nBands gt 1  then dataMask = product(dataMask,1)
  
  ;consider external/explizit mask
  if isa(imgMask) then begin
    imgMaskData = imgMask.getData()
    for i = 0, nBands - 1 do begin
      dataMask[i,*] = dataMask[i,*] * imgMaskData
    endfor
  endif
  
  ;consider stratification
  if isa(imgROI) then begin
    dataStratifiedMask = dataMask
    imgROIData = imgROI.getData() 
    for i = 0, nBands -1 do begin
      dataStratifiedMask[i, *] = dataStratifiedMask[i, *] * imgROIData
    endfor
         
  endif
end