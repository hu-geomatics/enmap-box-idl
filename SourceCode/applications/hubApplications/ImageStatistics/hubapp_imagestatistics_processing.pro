pro hubApp_imageStatistics_processing_initReader, imageList, tileLines, targetBands 
  foreach key, imageList.ImageKeys() do begin
    img = imageList.getImage(key)
    case key of
      'inputImage': img.initReader, tileLines, /TileProcessing, /Slice, SubsetBandposition=targetBands
      'inputMask' : img.initReader, tileLines, /TileProcessing, /Slice, /Mask
      'inputROIs' : img.initReader, tileLines, /TileProcessing, /Slice
      else : message, 'Unknown image key : '+key
    endcase
  endforeach

end

;+
;
; :Hidden:
; 
; :Description:
;    This routine iterates over an image.
;
; :Params:
;    imgList
;
; :Keywords:
;    bandStats
;    roiBandStats
;    loop1
;    loop2
;    progressBar
;
;-
pro hubApp_imageStatistics_processing_fillIncStats, imgList $
    , bandStats=bandStats $
    , oabStats = oabStats $
    , roiBandStats=roiBandStats $
    , loop1 = loop1 $
    , loop2 = loop2 $
    , progressBar = progressBar 
  
  img = imgList.getImage('inputImage')
  
  nLines = img.getMeta('lines')
  nSamples = img.getMeta('samples')
  
  divInputImage = img.getMeta('data ignore value')

  hasDIV = isa(divInputImage)
  hasROIs = isa(roiBandStats)
  hasProgressBar = isa(progressbar)
  
  addLoop1 = keyword_set(loop1)
  addLoop2 = keyword_set(loop2)
  
  if hasROIs then begin
    nROIs = n_elements(roiBandStats[0,*])
  endif
    
  while ~img.tileProcessingDone() do begin
    
    data = imgList.getData()
    imageData = data['inputImage']
    nPixel = n_elements(imageData[0,*])
    
    mask = make_array(nPixel, /BYTE, /NOZERO)
    
    foreach targetBandIndex, bandstats[*].targetBandindex, iB do begin
      mask[*] = 1b
      if hasDIV then mask *= imageData[iB,*] ne divinputimage
      
      ;standard image band statistic
      iValid = where(mask ne 0, /NULL)
      if isa(iValid) then begin
        if addLoop1 then begin
          oabStats.loop1.addData, imageData[iB, iValid]
          bandStats[iB].loop1.addData, imageData[iB, iValid]
        endif
        if addLoop2 then begin
          oabStats.loop2.addData, imageData[iB, iValid]
          bandStats[iB].loop2.addData, imageData[iB, iValid]
        endif
      endif
      
      ;stratified image band statistic
      if hasROIs then begin
        for iROI = 0, nROIs - 1 do begin
            iValid = where(data['inputROIs'] eq iROI+1 and mask ne 0, /NULL)
            if isa(iValid) then begin
              if addLoop1 then roiBandStats[iB, iROI].loop1.addData, imageData[iB, iValid]
              if addLoop2 then roiBandStats[iB, iROI].loop2.addData, imageData[iB, iValid]
            endif
        endfor
      endif
    endforeach
    
    if hasProgressBar then begin
      progressBar.setProgress, progressBar.getProgress() + fix(nPixel / nSamples)
    endif
  endwhile
end


;+
; :Description:
;    Calculates statistical descriptions of an image.  
;
; :Params:
;    parameters: in, required, type = Hash
;     Use this hash to specify the following parameters (* = optional)::
;       
;       --------------+--------+----------------------
;         key         | type   | description
;       --------------+--------+----------------------
;         inputImage  | string | path of input image
;       * inputMask   | string | path of mask file
;       * inputROIs   | string | path of ENVI Classification file that specifies ROIs (Regions of interest)
;       * targetBands | int[]  | band indices (0 to n bands - 1) of all bands the statistics is to calculated for
;                     |        | default = indices of all inputImage bands
;      ---------------+--------+----------------------
;      
;    settings: in, optional, type = Hash
;     This hash provides parameters to control the programm flow.
;     
;     
;
; :Keywords:
;    GroupLeader: in, optional
;     Widget base id for GUI programming.
;     
;    NoShow: in, optional
;     Depricated
;
; :Examples:
;
;-
function hubApp_imageStatistics_processing, parameters, settings, GroupLeader=GroupLeader, NoShow=NoShow
    
  if ~isa(settings) then settings = Hash()
  internalParams = Hash() + parameters 
  results = Hash()

  imgList = hubIOImgInputImageList()
  imgList.addImage, parameters['inputImage'], 'inputImage', imageReference=inputImage
  nBands = inputImage.getMeta('bands')
  
  targetBands = parameters.hubGetValue('targetBands', default=indgen(nBands))
  
  if max(targetBands) gt nBands - 1 then message, 'targetBand must be band indices in range of [0, nBands-1]'
  
  nTargetBands = n_elements(targetBands)
  
  results['inputImageMetaInfo'] = inputImage.getMeta()
  nClasses = inputImage.isClassification() ? inputImage.getMeta('classes')-1 : !NULL
  
  if ~keyword_set(NoShow) && ~settings.hubKeywordSet('noShow') then begin
    progressBar = hubProgressBar(Title='Image Statistics', Info='Calculate...' $
      , GroupLeader=groupLeader, NoShow=noShow)
    progressBar.setRange, [0, inputImage.getMeta('lines')*2]
    progressBar.setProgress, 0
  endif

  
  if parameters.hubIsa('inputMask') then begin
    imgList.addImage, parameters['inputMask'], 'inputMask', /MASK, imageReference=inputMask
    results['inputMaskMetaInfo'] = inputMask.getMeta()
  endif
  
  hasROIs = parameters.hubIsa('inputROIs')
  if hasROIs then begin
    imgList.addImage, parameters['inputROIs'], 'inputROIs', /Classification, imageReference=inputROIs
    nROIs = inputROIs.getMeta('classes') - 1
    roiNames = (inputROIs.getMeta('class names'))[1:*]
    results['inputROIMetaInfo'] = inputROIs.getMeta()
  endif
  

  tileLines = hubIOImg_getTileLines(image=inputImage)
  
  ;
  ;Loop 1: get basic statistics for each band
  ;
  template  = {loop1:obj_new(), loop2:obj_new(),stats:hash() }
  bandStats = replicate(CREATE_STRUCT({targetBandIndex:-1}, template), nTargetBands)
  oabStats  =           CREATE_STRUCT({nTargetBands:nTargetBands}, template)
   
  oabStats.loop1 = hubApp_ImageStatistics_processing_IncStatisticsLoop1(nClasses)
  foreach targetBandIndex, targetBands, i do begin
    bandStats[i].targetBandIndex = targetBandIndex
    bandStats[i].loop1 = hubApp_ImageStatistics_processing_IncStatisticsLoop1(nClasses)
    ;initialization of loop2 see below 
  endforeach
  
  if hasROIs then begin
    roiBandStats = replicate(bandStats[0], nTargetBands, nROIs)
    foreach targetBandIndex, targetBands, i do begin
      for r=0, nROIs - 1 do begin
        roiBandStats[i,r].targetBandIndex = targetBandIndex
        roiBandStats[i,r].loop1 = hubApp_ImageStatistics_processing_IncStatisticsLoop1(nClasses)
      endfor
    endforeach
  endif
  
  ;do loop 1
  hubApp_imageStatistics_processing_initReader, imgList, tileLines, targetBands
  hubApp_imageStatistics_processing_fillIncStats $
      , imgList $
      , oabStats=oabStats $
      , bandStats=bandStats $
      , roiBandStats=roiBandStats $
      , progressBar=progressbar $
      , /LOOP1
  imgList.finishReader
  
  
  ;
  ;Loop 2: get variance statistics
  ;
  nBins = parameters.hubgetValue('nBins', default=256)
  
  oabStats.stats = oabStats.loop1.getResult()
  for iB = 0, nTargetBands - 1 do begin
    bandStats[iB].stats = bandStats[iB].loop1.getResult()
    if hasROIs then begin
      for iROI = 0, nROIs - 1 do begin
        rstats = roiBandStats[iB, iROI].loop1.getResult()
        rstats['roiName'] = roiNames[iROI]
        roiBandStats[iB, iROI].stats = rstats
      endfor
    endif
  endfor
  
  ;if FINITE(oabStats.stats.mean) then begin
  if 1b then begin
    stats = oabStats.stats
    oabStats.loop2 = hubApp_ImageStatistics_processing_IncStatisticsLoop2(stats['range'], stats['mean'] , nBins)  
    for iB = 0, nTargetBands - 1 do begin
      stats = bandStats[iB].stats
      bandStats[iB].loop2 = hubApp_ImageStatistics_processing_IncStatisticsLoop2(stats['range'], stats['mean'] , nBins) 
      if hasROIs then begin
        for iROI = 0, nROIs - 1 do begin
          rstats = roiBandStats[iB, iROI].stats
          if rstats.numberOfSamples gt 0  then begin
            roiBandStats[iB, iROI].loop2 = hubApp_ImageStatistics_processing_IncStatisticsLoop2(stats['range'], stats['mean'], nBins)  
          endif
        endfor
      endif
    endfor
      
    ;do loop 2!
    hubApp_imageStatistics_processing_initReader, imgList, tileLines, targetBands
    hubApp_imageStatistics_processing_fillIncStats $
        , imgList $
        , oabStats=oabStats $
        , bandStats=bandStats $
        , roiBandStats=roiBandStats $
        , progressBar = progressBar $
        , /LOOP2
    imgList.finishReader
    
    ;
    ;Loop 2: get variance statistics
    ;
    oabStats.stats += oabStats.loop2.getResult()
    
    for iB = 0, nTargetBands - 1 do begin
      stats = bandStats[iB].loop2.getResult()
      if stats.numberOfSamples ne bandStats[iB].stats.numberOfSamples then message, 'different number of samples' 
      bandStats[iB].stats += stats 
      
      if hasROIs then begin
        for iROI = 0, nROIs - 1 do begin
          if roiBandStats[iB, iROI].stats.numberOfSamples gt 0 then begin
            stats = roiBandStats[iB, iROI].loop2.getResult()
            if stats.numberOfSamples ne roiBandStats[iB, iROI].stats.numberOfSamples then message, 'different number of roi samples'
            roiBandStats[iB, iROI].stats += stats
          endif
        endfor
      endif
    endfor
    
  endif
  
  ;collect results
    
  keys = bandStats[0].stats.keys()
  template = bandStats[0].stats.toStruct()
  for i = 0, N_ELEMENTS(keys)-1 do begin 
      case typename(template.(i)) of
        'FLOAT' : template.(i) = !VALUES.F_NAN
        'DOUBLE': template.(i) = !VALUES.D_NAN 
        'DICTIONARY': template.(i) = DICTIONARY()
        'HASH': template.(i) = Hash()
        'OBJECT': template.(i) = obj_new()
        'POINTER': template.(i) = ptr_new()
        'LIST' : template.(i) = list()
        'STRING': template.(i) = ''
        'UNDEFINED': ;nothing
        else : template.(i) = 0b
      endcase      
  endfor
  resultOABStats = CREATE_STRUCT(template, {nTargetBands:nTargetBands})
  template       = CREATE_STRUCT(template, {bandIndex:-1}) 
  
  resultBandStats = replicate(template, ntargetbands)
  
  tags = TAG_NAMES(template)
  
  
  resultBandStats[*].bandIndex = targetbands
  foreach key, keys, iKey do begin
    iTag = where(strcmp(tags, key, /FOLD_CASE) eq 1, /NULL)
    if isa(iTag) then begin
      
      resultOABStats.(iTag) = oabStats.stats[key]
      
      for iB = 0, ntargetbands-1 do begin
        if bandStats[iB].stats.hasKey(key) then begin 
          resultBandStats[iB].(iTag) = bandStats[iB].stats[key]
        endif    
      endfor
    endif
  endforeach
  
  results['oabStatistics'] = resultOABStats
  results['bandStatistics'] = resultBandStats
  
  if hasROIs then begin
    resultROIBandStats = replicate(template, nTargetbands, nROIs)
    ;resultROIBandStats[*].bandIndex = targetbands
    foreach key, keys, iKey do begin
      iTag = where(strcmp(tags, key, /FOLD_CASE) eq 1, /NULL)
      if isa(iTag) then begin
        for iR=0, nROIs-1 do begin
          resultROIBandStats[*,iR].bandIndex = targetBands
          for iB = 0, nTargetbands - 1 do begin
            if roiBandStats[iB,iR].stats.hasKey(key) then begin
              resultROIBandStats[iB,iR].(iTag) = roiBandStats[iB,iR].stats[key]
            endif
          endfor
        endfor
      endif
    endforeach
    
    results['roiStatistics'] = resultROIBandStats
  endif
  
  if isa(progressBar) then progressBar.cleanup
  return, results

end

;+
; :hidden:
;-
pro test_hubApp_imageStatistics_processing
  p= Hash()
  p['inputImage'] = enmapBox_getTestImage('AF_Image')
  ;p['inputImage'] = hub_getTestImage('Hymap_Berlin-A_Mask')
  
  ;p['inputImage'] = enmapBox_getTestImage('AF_LC')
  p['inputMask'] = enmapBox_getTestImage('AF_Mask')
  p['inputROIs'] = enmapBox_getTestImage('AF_AdminBorders')
  p['equalBins'] = 0
  ;p['targetBands']=2
  
  
  p = dictionary()
  
  p.inputImage = 'D:\Temp\EB2\nanImage' 
  
  ;p.targetBands = [6]
  result = hubApp_imageStatistics_processing(p)
  
  settings = Hash('createPlots',0b)
  report = hubApp_imageStatistics_getReport(result,settings=settings) 
  report.saveHTML, /SHOW
end