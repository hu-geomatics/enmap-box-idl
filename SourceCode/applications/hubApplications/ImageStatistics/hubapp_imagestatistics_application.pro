;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-
;+
; :Description:
;    This procedure is the main routine to calculate and show the image statistic. 
;    It opens a widget dialog to collect the required parameters from the user and 
;    then starts the random sampling procedure implemented
;    in `hubApp_imageStatistics_processing`. 
;
;
; :Keywords:
;    GroupLeader: in, optional, type=long
;       Use this keyword to specify the group leader for used widgets.
;-
pro hubApp_imageStatistics_application $
  ,GroupLeader=groupLeader
  
  ;restore state from previous application calls to retrieve default values
  defaultValues = hub_getAppState('hubApp', 'stateHash_ImageStatistics')
  
  parameters = hubapp_imageStatistics_getParameters(defaultValues, GROUPLEADER=groupLeader)
  if parameters['accept'] then begin
    ;save parameters as new session state hash
    hub_setAppState, 'hubApp', 'stateHash_ImageStatistics', parameters
      
    statisticResults = hubapp_imageStatistics_Processing(parameters)
    

    
    settings = Hash()
    createPlots = parameters.hubKeywordSet('createPlots')
    settings['createPlots'] = createPlots   
    settings['noShow'] =  0
    
    nBands = n_elements(statisticResults['bandStatistics'])
    
    if nBands gt 20 && createPlots then begin
      question = string(format='(%"Do you really want to create plot images for %i bands? This might take some time")', nBands)
      answer = DIALOG_MESSAGE(question, /QUESTION, /DEFAULT_NO)
      if ~answer then createPlots = 0b
    endif
    report = hubapp_imageStatistics_getReport(temporary(statisticResults) $
        , SETTINGS=settings $
        , GROUPLEADER=groupLeader $
        )
    report.saveHTML, /show
  endif

end

pro test_hubApp_imageStatistics_application
  groupLeader = widget_base()
  hubApp_imageStatistics_application $
    , GROUPLEADER=groupLeader
  widget_control,group_leader,/DESTROY

end

pro test_amw_output
   @huberrorcatch
  hubAMW_program , Title = 'Test'
  hubAMW_outputFilename, 'fn', title='Save File', Optional=2
  result = hubAMW_manage()
end