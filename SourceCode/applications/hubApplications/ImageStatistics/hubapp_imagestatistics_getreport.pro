;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-


function hubApp_imageStatistics_getReport_getHistogramTable, hist
  nBins = hist['numberOfBins']
  table = hubReportTable()
  table.addColumn, indgen(nBins)+1, name='#bin'
  table.addColumn, hist['binStart'], name='binStart'
  table.addColumn, hist['binEnd'], name='binEnd'
  table.addColumn, hist['histogram'], name='counts'
  table.addColumn, hist['cumulativeHistogram'], name='cum. counts'
  table.addColumn, hist['probabilityDensity'], name='prob. density'
  table.addColumn, hist['cumulativeDistribution'], name='cum. distribution'
  return, table
end

function hubApp_imageStatistics_getReport_getBandOverviewTable, bandNumbers $
  , numberOfPixels=numberOfPixels $
  , numberOfIgnored=numberOfIgnored $
  , numberOfUsed=numberOfUsed $
  ;, rangeValues=rangeValues $
  , minValues=minValues $
  , maxValues=maxValues  $
  , meanValues=meanValues $
  , stddevValues=stddevValues $ 
  , tabletitle=tabletitle $
  , classNames=classNames $
  , classCounts = classCounts 

  
  headings = list()
  if isa(tabletitle) then headings.add, list(tableTitle, nColumns) 

  
  
  table = hubReportTable()
  table.addColumn, bandNumbers, name='Band'
  if isa(numberOfPixels)  then table.addColumn, numberOfPixels, name='n total'
  if isa(numberOfIgnored) then table.addColumn, numberOfIgnored, name='n ignored'
  if isa(numberOfUsed) then table.addColumn, numberOfUsed, name='n used'
  if isa(minValues) then table.addColumn, minValues, name='min', formatString='(%"%0.2f")'
  if isa(maxValues) then table.addColumn, maxValues, name='max', formatString='(%"%0.2f")'
  if isa(meanValues) then table.addColumn, meanValues, name='mean', formatString='(%"%0.2f")'
  if isa(stddevValues) then table.addColumn, stddevValues, name='stddev', formatString='(%"%0.2f")'
  if isa(classCounts) then begin
    nb = n_elements(classCounts[0,*])
    nc = n_elements(classCounts[*,0])
    if isa(classNames) then begin
      cn = classNames
    endif else begin
      cn = 'class '+strtrim(indgen(nc),2)
      cn[0] = 'unclassified'
    endelse
     
    foreach className, cn, iClass do begin
      table.addColumn, classCounts[iClass,*], name=className, formatString='(%"%i")'
    endforeach
  endif
  return, table
end

;+
; :Description:
;    This function takes the image statistics returned from `hubApp_imageStatistics_Processing` and presents them 
;    in a hubReport object. The report provides histogram tables and plots of the image value distribution.
;
; :Params:
;    resultHash: in, required, type=hash
;       This hash contains the statistical information derived from `hubApp_imageStatistics_Processing`.
;
; :Keywords:
;    settings: in, optional, type=hash
;     Hash to provide GUI related options.
;     
;     
;    GroupLeader: in, optional
;     Group leader of calling widget
;-
function hubApp_imageStatistics_getReport, resultHash, settings=settings, GroupLeader=groupLeader 
  
  if ~isa(settings) then settings = Hash()
  
  ;check required parameters
  required = ['bandStatistics', 'inputImageMetaInfo']
  if ~resultHash.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  createPlots = settings.hubKeywordSet('createPlots')
  
  imageMeta = resultHash['inputImageMetaInfo'] 
  
  ns = imageMeta['samples']
  nl = imageMeta['lines']
  nb = imageMeta['bands']
  np = ulong(ns)*nl
  
  isClassification = imageMeta.hasKey('classes')
  if isClassification then begin
     nClasses = imageMeta['classes']
     classNames = imageMeta.hubGetValue('class names')
     classColors = imageMeta.hubgetValue('class lookup')
     if ~isa(classNames) then begin
      classNames = 'class '+strtrim(indgen(nClasses),2)
      classNames[0] = 'unclassified'
      ;classColors = 
     endif
  endif
  
  
  bandStatistics = resultHash['bandStatistics']
  
  
  targetBands = bandStatistics[*].bandIndex
  nTargetBands = n_elements(targetBands)
  
  hasROIs = resultHash.hubIsa('roiStatistics')
  nROIs = 0
  if hasROIs then begin
    roiStatistics = resultHash['roiStatistics']
    nROIs = n_elements((resultHash['roiStatistics'])[0,*])
    inputROIMetaInfo = resultHash['inputROIMetaInfo']
    roiNames = (inputROIMetaInfo['class names'])[1:*]
  endif
  
  RepList = hubReportList(title = 'Image Statistics', /NoShow )
  
  if ~settings.hubkeywordset('noShow') then begin
    progressbarMax = nTargetBands + 1
    if hasROIS then progressbarMax += (nTargetBands+1) * nROIs 
    progressbarDone = 0
    progressbar = hubProgressBar(title='Image Statistics HTML Report', info='Create Report', GroupLeader=GroupLeader, NoShow=NoShow)
    progressbar.setRange, [progressbarDone, progressBarMax]
  endif
  
  ;
  ;GENERAL INFORMATIONS ON USED FILES
  ;
  ;RepList.updateProgressBar, progressbarDone++, progressbarMax
  rep = RepList.newReport(title='File Information', /NoShow)
  rep.addHeading, 'Image'
  inputImageMeta = resultHash['inputImageMetaInfo']
  bandNames = inputImageMeta.hubGetValue('band names')
  
  rep.addMonospace, hubApp_getFileInfoBlock(inputImageMeta)
  
  if resultHash.hubIsa('maskMetaInfo') then begin
    rep.addHeading, 'Mask'
    rep.addMonospace, hubApp_getFileInfoBlock(resultHash['inputMaskMetaInfo'])
  endif
  
  if resultHash.hubIsa('roiMetaInfo') then begin
    rep.addHeading, 'Region of Interests'
    rep.addMonospace, hubApp_getFileInfoBlock(resultHash['inputROIMetaInfo'])
  endif

  ;
  ; Add statistical overview
  ;
  bandNumbers = bandStatistics[*].bandIndex + 1
  rep = RepList.newReport(title='Overview', /NoShow)
  rep.addHeading, 'Band values', 2
  
  if isa(progressBar) then progressBar.setInfo, 'Add overviews statistics...'  
  
  if isclassification then begin
    classCounts = make_array(nClasses, nTargetBands, /LONG, value=0)
    for iB = 0, nTargetBands - 1 do begin
      classCounts[*,iB] = (bandstatistics[iB].classHistogram)['histogram']
    endfor
  endif else begin
    classCounts = !NULL
  endelse
  
  ;
  ;OVERVIEW Table
  ;
  
  minValues = list()
  maxValues = list()
  avgValues = list()
  stdValues = list()
  
  foreach bandStatistic, bandStatistics do begin
    if bandstatistic.numberOfSamples gt 0 then begin
      minValues.add, bandstatistic.range[0]
      maxValues.add, bandstatistic.range[1]
      avgValues.add, bandstatistic.mean
      stdValues.add, bandstatistic.STANDARDDEVIATION
    endif else begin
      minValues.add, !NULL
      maxValues.add, !NULL
      avgValues.add, !NULL
      stdValues.add, !NULL
    endelse
  endforeach
  
  pixelPerBand = make_array(n_elements(bandNumbers), value=np)
  table = hubApp_imageStatistics_getReport_getBandOverviewTable(bandNumbers $
    , NUMBEROFPIXELS=pixelPerBand  $
    , numberOfIgnored=pixelPerBand - bandstatistics[*].numberOfSamples $
    , NUMBEROFUsed= bandstatistics[*].numberOfSamples $
    , minValues= minValues $
    , maxValues= maxValues $
    , meanValues = avgValues $
    , stddevValues = stdValues $
    , classNames=classNames $
    , classCounts = classCounts $
    )
   
    rep.addHTML, table.getHTMLTable()
  
  if isa(progressBar) then progressBar.setProgress, ++progressbarDone
  
  if hasROIs then begin
    for iROI = 0, nROIs - 1 do begin
      rep.addHeading, 'ROI: '+roiNames[iROI], 2
        if isclassification then begin
          classCounts = make_array(nClasses, nTargetBands, /LONG, value=0)
          for iB = 0, nTargetBands - 1 do begin
            classCounts[*,iB] = (roistatistics[iB, iROI].classHistogram)['histogram']
          endfor
        endif else begin
          classCounts = !NULL
        endelse
      
      table = hubApp_imageStatistics_getReport_getBandOverviewTable(bandNumbers $
      , NUMBEROFUSED= roistatistics[*,iROI].numberOfSamples $
      , minValues= min(roistatistics[*,iROI].range,dimension=1, max=temp) $
      , maxValues= temp $
      , meanValues = roistatistics[*,iROI].mean $
      , stddevValues = roistatistics[*,iROI].STANDARDDEVIATION $
      , classNames = classNames $
      , classCounts = classCounts $
      )  
      rep.addHTML, table.getHTMLTable()
      
      if isa(progressBar) then progressBar.setProgress, ++progressbarDone
    endfor
    
  endif
  
  foreach tB, targetBands, iB do begin
    
    if isa(progressBar) then progressBar.setInfo, string(format='(%"Add details band %i (%i/%i)...")', tB+1,iB+1, nTargetBands)
    
    ltitle = string(format='(%"Band %i")', tB+1)
    stitle = ltitle
    if isa(bandNames) && ~strcmp(bandNames[tB], stitle, /FOLD_CASE) then begin
      ltitle += ': "'+bandNames[tB]+'"'
      stitle = bandNames[tB] 
    endif
    
    rep = RepList.newReport(title=ltitle, shottitle=stitle, /NoShow)
    
    bandStats = bandstatistics[iB]
    if isClassification then begin
      rep.addHeading, 'Categorical values', 2
      classCounts = bandStats.classHistogram['histogram']
      
      table = hubReportTable()
      table.addColumn, indgen(nClasses), name='DN'
      table.addColumn, classNames, name='class name'
      table.addColumn, classCounts, name='counts'
      table.setHeader, list(list('Classification scheme',3), (table.getHeader())[-1])
      rep.addHTML, table.getHTMLTable()
        
      if createPlots then begin
        
        pClassHist = plot([0,nClasses], [0, max(classCounts[1:*])] $ 
            , /NODATA, BUFFER=1 $
            ;, XTICKINTERVAL = 1 $
            , XMINOR=0, XMAJOR=nClasses+1 $
            ;, XTICKNAME = ['', classNames[1:*],'']+'  ' $
            , XTICKNAME = ['', classNames[1:*],'']+'  ' $
            , XTEXT_ORIENTATION = 30 $
            , XTICKDIR = 1 $
            , XTICKLEN = 0 $
            , YTICKFORMAT='(%"%i")' $
            , TITLE = 'Pixel per class' $
            , YTITLE = 'counts' $
            
             
            )            
        for iC = 1, nClasses-1 do begin
          xvec = 0.3* [-1,1,1,-1] + iC
          yvec = [0,0,1,1] * classCounts[iC]
          !NULL = POLYGON(xvec, yvec,FILL_COLOR=classColors[*,iC], /DATA)
        endfor
        
        rep.addImage, pClassHist
      endif
    endif 
    
    rep.addHeading, 'Histogram', 2
    hist = bandStats.histogram
    
    nmasked = np - bandStats.numberofSamples
    info = [string(format='(%"number of bins: %i")', hist['numberOfBins']) $
           ,string(format='(%"bin size: %f")', hist['binSize']) $
           ,string(format='(%"range: [%f, %f)")', (hist['binStart'])[0], (hist['binEnd'])[-1]) $
           ,string(format='(%"number of pixels:      %i")', np - bandStats.numberofSamples) $
           ,string(format='(%"number of pixels used: %i (%0.2f\%)")', bandStats.numberofSamples, 100. * bandStats.numberofSamples / np) $
           ,string(format='(%"ignored/masked pixels: %i (%0.2f\%)")', nmasked, 100. * nmasked /np ) $
           ]
    rep.addMonospace, info
    
    if createPlots then begin
      ;add normal historgram
      buffer = 1
      pHist = plot(hist['binStart'], hist['histogram'] $
        , BUFFER=buffer $
        ;, XTICKINTERVAL = 1 $
        , FILL_COLOR='blue' $
        , /FILL_BACKGROUND $
        , XMINOR=-1, XMAJOR=-1 $
        , /HISTOGRAM $
        , YTICKFORMAT='(%"%i")' $
        , YTICKDIR=0, XTICKDIR=0 $
        , XTITLE = 'value' $
        , YTITLE = 'counts' $
        )
        
      rep.addImage, pHist
      
      if nROIs gt 0 then begin
        roiNames = inputROIMetaInfo['class names']
        roiColors = inputROIMetaInfo['class lookup']
        
        
        for iROI = 0, nROIs-1 do begin
          
          roiStats = roistatistics[iB,iROI]
          roiHist = roiStats.histogram
           
          color = roiColors[*,iROI+1]
          
          if iROI eq 0 then begin
            xvals = roiHist.binCenter
            yvals = roiHist.histogram
            pHist = BARPLOT(xvals,yvals, BOTTOM_COLOR=color, buffer=buffer)
          endif else begin
            if total(xvals ne roiHist.binCenter) then message, 'Error: unequal bin positions'
            yvals = ybottom + roiHist.histogram
            !NULL = BARPLOT(xvals, yvals, BOTTOM_COLOR=color, BOTTOM_VALUES=ybottom, /OVERPLOT, Buffer=buffer)
          endelse
          ybottom = yvals
        endfor
        
        rep.addImage, pHist
      endif
      ;if stratified, add stratified histogram
      
      
    endif
    
    table = hubApp_imageStatistics_getReport_getHistogramTable(hist)
    rep.addHTML, table.getHTMLTable()
    if isa(progressBar) then progressBar.setProgress, ++progressbarDone
    
    ;TODO add histogram image
    ;bandstatistics[iB].classHistogram

    if hasROIs then begin
      rep.addHeading, 'Regions of interest', 2
      for iROI = 0, nROIs - 1 do begin
        rep.addHeading, '"'+roiNames[iROI]+'"', 3
        
        if isclassification then begin
          x = indgen(nClasses)+1
          
        endif
        
        if roistatistics[iB, iROI].numberOfSamples gt 0 then begin
          hist = roistatistics[iB, iROI].histogram
          table = hubApp_imageStatistics_getReport_getHistogramTable(hist)
          rep.addHTML, table.getHTMLTable()
        endif else begin
          rep.addMonospace, '- There are no valid/unmasked pixels for this ROI -'
        endelse
        if isa(progressBar) then progressBar.setProgress, ++progressbarDone
      endfor
      
    endif
    
  endforeach
  
  ;TODO
  if isa(progressbar) then progressbar.cleanup
  return, RepList
end

;+
; :Hidden:
;-
pro test_hubApp_imageStatistics_getReport
  p= Hash()
  
  
  p['inputImage'] = enmapBox_getTestImage('AF_LAI')
  p['inputROIs'] = enmapBox_getTestImage('AF_AdminBorders')

  p['inputImage'] = 'Y:\BJ_NOC\03_Settings\01_FireTests\03_Settings\BurFFS\SpecLib100\sli.2r1.samples.bsq'
  p['inputROIs'] = 'Y:\BJ_NOC\03_Settings\01_FireTests\03_Settings\BurFFS\SpecLib100\sli.2r1.samples.toi.bsq'

  p['equalBins']  = 1
  
  ;p['createPlots'] = 1
  ;p['targetBands'] = [1,3,7,9]
  settings = Hash()
  settings['createPlots'] = 0
  settings['noShow'] =  0
  result = hubApp_imageStatistics_processing(p)
  tic
  report = hubApp_imageStatistics_getReport(result,SETTINGS= settings)
  toc
  report.saveHTML, /SHOW
  
  p= Hash()
  p['inputImage'] = hub_getTestImage('Hymap_Berlin-A_Mask')
  p['inputROIs'] = hub_getTestImage('Hymap_Berlin-Stratification-Image')
  result = hubApp_imageStatistics_processing(p)
  settings = Hash('createPlots',1b)
  report = hubApp_imageStatistics_getReport(result,settings=settings)
  report.saveHTML, /SHOW
end
