;+
;
;
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;
;-

;+
; :Hidden:
; :Description:
;    Constructor
;
; :Params:
;    histogramRange: in, required, type=double[]
;     Range eof daa values.
;     
;    mean: in, required, type=double
;     Data mean.
;     
;    nBins: in, required, type=int
;     Number of histogram bins.
;
;
;-
function hubApp_ImageStatistics_processing_IncStatisticsLoop2::init, histogramRange, mean, nBins

  self.histogram  = hubMathIncHistogram(histogramRange, nBins)
  self.variance   = hubMathIncVariance(mean)
  
  return, 1
end


;+
; :Description:
;    Adds data values.
;
; :Params:
;    data: in, required, type=numeric
;
;-
pro hubApp_ImageStatistics_processing_IncStatisticsLoop2::addData $
  , data
  
  
  self.hubMathInc::addData, data
  
  (self.histogram).addData, data
  (self.variance).addData, data
  
  
end


;+
; :Description:
;    Returns a hash containing the mean values and ranges of
;    the reference values, estimation values and the errors between.
;
;
;-
function hubApp_ImageStatistics_processing_IncStatisticsLoop2::getResult

  results = DICTIONARY()
  nSamples = self.hubMathInc::getNumberOfSamples()
  results['numberOfSamples'] = nSamples
  if nSamples gt 0 then begin
    results['histogram'] = self.histogram.getResult()
    results['variance'] = self.variance.getResult()
    results['standardDeviation'] = sqrt(results['variance']) 
  endif
  return, results
  
end


function hubApp_ImageStatistics_processing_IncStatisticsLoop2::_overloadPrint
  info = ['hubApp_ImageStatistics_processing_IncStatisticsLoop2' $
         ,[string(self.getResult(), /PRINT)]  ]
  
  return, strjoin(info, string(13b))
end

;+
; :hidden:
;-
pro hubApp_ImageStatistics_processing_IncStatisticsLoop2__define

  struct = {hubApp_ImageStatistics_processing_IncStatisticsLoop2 $
    , inherits hubMathInc $
    , histogram : obj_new() $
    , variance: obj_new() $ 
  }
end

;+
; :hidden:
;-
pro test_hubApp_ImageStatistics_processing_IncStatisticsLoop2
  o = hubApp_ImageStatistics_processing_IncStatisticsLoop2([0,10], 4)
  o.addData, [1,2,3,4]
  print, o
  print, o.getResult()
  print, o.getNumberOfSamples()
end
