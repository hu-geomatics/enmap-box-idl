;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-
;+
; :Description:
;    Use this event handler to call the hubApp_imageStatistics_Application from an 
;    EnMAP-Box menu button.
;
; :Params:
;    event: in, required, type=button event structure
;    
;-
pro hubApp_imageStatistics_event $
  ,event
 
  @huberrorcatch
  hubApp_imageStatistics_Application, GroupLeader=event.top

end