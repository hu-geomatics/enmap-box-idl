;+
;
;
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;
;-

;+
; :Description:
;    Constructor
;
; :Params:
;    nClasses: in, optional, type=integer
;     Number of classes (except 0 = undefined) in case the data is taken from an classification data set.
;     
;-
function hubApp_ImageStatistics_processing_IncStatisticsLoop1::init, nClasses
  
  self.mean  = hubMathIncMean()
  self.range = hubMathIncRange()
  
  self.nClasses = isa(nClasses) ? nClasses : 0
  if self.nClasses gt 0 then begin
    self.outOfValidClassRange = 0l
    self.classHistogram = hubMathIncHistogram([0,nClasses], nClasses+1)
  endif
  
  return, 1
end


;+
; :Description:
;    Adds data values.
;
; :Params:
;    data: in, required, type=numeric
;
;-
pro hubApp_ImageStatistics_processing_IncStatisticsLoop1::addData $
  , data
  
  
  self.hubMathInc::addData, data
  
  (self.mean).addData, data
  (self.range).addData, data
  
  if isa(self.classHistogram) then begin
    iValid = where(data ge 0 and data le self.nClasses, /NULL, nComplement=nOutliers)
    self.outOfValidClassRange += nOutliers
    if isa(iValid) then (self.classHistogram).addData, data[iValid]
  endif
  
  
end


;+
; :Description:
;    Returns a hash containing the mean values and ranges of
;    the reference values, estimation values and the errors between.
;
;
;-
function hubApp_ImageStatistics_processing_IncStatisticsLoop1::getResult

  results = DICTIONARY()
  results['numberOfSamples'] = self.hubMathInc::getNumberOfSamples()
  results['mean'] = self.mean.getResult()
  results['range'] = self.range.getResult()

  if isa(self.classHistogram) then begin
    results['classHistogram'] = self.classHistogram.getResult()
    results['outOfValidClassRange'] = self.outOfValidClassRange
  endif
  
  return, results
  
end

function hubApp_ImageStatistics_processing_IncStatisticsLoop1::_overloadPrint
  info = ['hubApp_ImageStatistics_processing_IncStatisticsLoop1' $
    ,[string(self.getResult(), /PRINT)]  ]

  return, strjoin(info, string(13b))
end




;+
; :hidden:
;-
pro hubApp_ImageStatistics_processing_IncStatisticsLoop1__define

  struct = {hubApp_ImageStatistics_processing_IncStatisticsLoop1 $
    , inherits hubMathInc $
    , mean : obj_new() $
    , range: obj_new() $
    , classHistogram: obj_new() $
    , outOfValidClassRange: 0l $
    , nClasses : 0 $
  }
end

;+
; :hidden:
;-
pro test_hubApp_ImageStatistics_processing_IncStatisticsLoop1
  o = hubApp_ImageStatistics_processing_IncStatisticsLoop1(4)
  o.addData, [1,2,3,4,10,0]
  print, o.getResult()
  print, o.getNumberOfSamples()
end
