;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-
;+
; :Description:
;    Callback function used to verify the AMW dialog.
;
; :Params:
;    resultHash: in, required, type=Hash
;     This hash is provided by the callback function.
;     
; :Keywords:
;    Message: out, required, type=String[]
;     This is the message-array used to give explanations on occured errors.
;-
function hubApp_imageStatistics_getParameters_Check, resultHash, Message=message
   
   msg = !NULL
   isConsistent = 1
   
   ;check whether the provided stratification file is of type ENVI Classification
   if resultHash.hubIsa('inputROIs') then begin
      imgROI = hubIOImgInputImage(resultHash['inputROIs'])
      if ~imgROI.isClassification() then begin
        msg = [msg, 'The stratification image must be a classification file']
        isConsistent = isConsistent and 0         
      endif
      imgROI.cleanup
   endif
   
  
   
   message = msg
   return, isConsistent
 end
 
 
;+
; :Description:
;     This function creates a widget to collect the parameters required to 
;     run the `hubApp_imageStatistics_processing` routine.
; 
; :Params:
;    defaultValues: in, optional, type = hash
;     A hash with default values.
; 
; :Keywords:
;    GroupLeader: in, required, type=long
;       Use this keyword to specify the group leader for used widgets.
;-
function hubApp_imageStatistics_getParameters $
  , defaultValues, GroupLeader=groupLeader
  
  if ~isa(defaultValues) then defaultValues = hash()
   
  title = 'Image Statistics'
  hubAMW_program , groupLeader, Title = title
  hubAMW_frame ,   Title =  'Input'
hubAMW_inputSampleSet, 'inputSampleSet' $
        , Title='Image' $
        , /Masking $
        , SELECTSPECTRALSUBSET = 1 $ ;defaultValues.hubGetValue('targetBands', default=1) $
        , /ReferenceOptional $
        , value = defaultValues.hubGetValue('inputImage') $
        , ReferenceValue = defaultValues.hubGetValue('inputMask')                                            
  
  hubAMW_inputImageFilename ,'inputROIs' $
    , Title='Stratification' $
    , optional= defaultValues.hubIsa('inputROIs') ? 1 : 2  $
    , size=400 $
    , VALUE=defaultValues.hubGetValue('inputROIs')
  

  ;hubAMW_frame ,   Title =  'Output'
  ;hubAMWGroup_report, 'report', ADDPLOTSSTYLE=2, SAVEFILEVALUE='imageStatistics', SAVEFILESIZE=420
  ;hubAMW_Subframe, /Column
  ;hubAMW_checkbox, 'equalBins', Title='Equal bins for all bands & stratum', Value=0
  hubAMW_checkbox, 'createPlots', Title=' Create histogram plots', Value=defaultValues.hubGetValue('createPlots', default=1)
  result = hubAMW_manage(ConsistencyCheckFunction='hubApp_imageStatistics_getParameters_Check')
  if result['accept'] then begin                          
    ;make clean result hash -> remove non-required hash-keys
    sampleSetResult = result['inputSampleSet']
    result['targetBands'] = sampleSetResult.hubgetValue('spectralSubset') 
    result['inputImage']  = sampleSetResult['featureFilename']
    result['inputMask'] = sampleSetResult.hubGetValue('labelFilename')
    
    result.hubRemove, ['inputSampleSet','sub']
    
  endif 
  return, result
end


;+
; :hidden:
; :private:
;-
pro test_hubApp_imageStatistics_getParameters
  f = hubApp_imageStatistics_getParameters()
  print, f
end
