;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-

;+
; :Description:
;    This procedure is used to scale images from an input to an output range. This can be done using the mode `range` to perform a liner 
;    scaling between an input and output range, or using the mode `zTransformation`.     
;
;     1. Linear scaling between input and output ranges 
;     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;        In this mode (`parameters['mode']='range'`) a scaling is performed considering an input range `[Imin,Imax]` and an output range `[Omin,Omax]`. 
;        The values of input image `x` are scaled to the value `y` according to::
;        
;           gain    = (Omax - Omin) / (Imax - Imin)
;           offset  = -Imin * gains + Omin
;           y = x * gain + offset
;        
;        It is obligatory to define the outputRange of interest using the hash-key `outputRange`. The input range can be 
;        defined:
;        
;        a) implicitely the input range will be the min/max range of the input image (`inputImage`)
;                 
;        b) explicitely using `inputRange` to define `Imin` and `Imax`
;        
;        c) explicitely using `inputRange` and setting `percentiles` on true. This interpretes values in `inputRange`
;           as lower and upper percentile ranks which then are used to calculate `Imin` and `Imax` from the input statistic.
;        
;        The keywords `inputRange` and `outputRange` are 2-D arrays of type `numeric[2,1]` or `numeric[2,<number of bands>]`. This 
;        allows to use a range for all bands or to specify a range for each single band. For instance
;        `parameters['outputRange'] = [[0,1], [0,100]` defines that the first band should be scaled between 0 and 1 
;        and the second band between 0 and 100. 
;        
;        
;                 
;        
;    2. Z-Transformation / Standardization 
;    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;       This mode uses `parameters['mode']='zTransformation'` and performs a z-Transformation of the input image, according to::
;       
;         mean = mean value of all input values x
;         stdDev = standard deviation of all input values x
;         
;       The scaling from x to y is done by::
;         
;         y = (x - mean) / stdDev
;       
;       The values or mean and standard deviation can be performed:
;       
;       a) implicitely by calculating the image statistic. This is the default behaviour in case `mean` and `stdDeviation` are not set.
;       
;       b) explicitely using the keywords `mean` and `stdDeviation`.
;       
;       
;    Further Notes
;    ~~~~~~~~~~~~~
;    
;    By default both modes described above will calculate the statistical measures using the image statistic
;    and consider values from all bands. Setting `bandwise` on true will change this behaviour 
;    to use the band statistic instead. For instance the default result in 
;    mode `zTransformation` (without using other options) will be an image with a mean value of zero 
;    accross _all_ images. Using `bandwise` will create a scaled image where each 
;    band has an mean value of zero.     
;    
;    Furthermore it might be of interest to use statistical measures based on other images, for instance 
;    to use the same scaling factors for different input images. Use the hash-key `inputStatisticImg` to define an image that 
;    is used to calculate the image statistic instead of `inputImage`.  
;
; :Params:
;    parameters: in, required, type=hash
;      This is the parameter hash used to provide obligatory and optional parameters. 
;      (`*` = optional, `B` can be `1` or equal to the number of bands):
;      
;      Hash-keys used in all modes::
;      
;          key             | type         | description
;        ------------------+--------------+-----------------------------------------------------------
;          inputImage      | string       | filename of input image
;          outputImage     | string       | filename of scaled output image 
;          mode            | string       | mode of image scaling. can be 'range' or 'zTransformation'
;        * inputStatsImage | string       | filename of image that is used alternatively to the input image to 
;                          |              | calculate the image or band statistics
;        * outputDIV       | numeric      | data ingore value of output image
;        ------------------+--------------+----------------------------------------------------------
;        
;      Specific hash-keys for mode 'range'::
;      
;          key            | type          | description
;        -----------------+---------------+----------------------------------------------------------
;          outputRange    | numeric[2,B]  | output data range, e.g. [0.0, 1.0]
;        * inputRange     | numeric[2,B]  | input data range, e.g. [0,10000], or 
;                         |               | range of input percentile ranks, e.g [0,100], in case of /percentiles
;        * percentiles    | boolean       | use this to interpreted the values in inputRange as percentile ranks
;        * bandwise       | boolean       | set this to use the statistic of each single band instead of total image statistic 
;        * trimOutliers   | boolean       | set this to trim values out of the outputRange  
;        -----------------+---------------+----------------------------------------------------------
;
;      Specific hash-keys for mode 'zTransformation'::
;    
;          key            | type          | description
;        -----------------+---------------+----------------------------------------------------------
;        * mean           | numeric[2,B]  | mean value(s) used for z-Transformation 
;        * stdDeviation   | numeric[2,B]  | stdDeviation value(s) used for z-Transformation
;                         |               | range of input percentile ranks, e.g [0,100], in case of /percentiles
;        * percentiles    | boolean       | use this to interpreted the values in inputRange as percentile ranks
;        * bandwise       | boolean       | set this to use the statistic of each single band instead of total image statistic 
;        - - - - - - - - -+- - - - - - - -+- - - - - - - - - - - - - - - -
;        * outputRange    | numeric[2,B]  | use both keywords to trim the scaled values 
;        * trimOutliers   | boolean       |
;        -----------------+---------------+-----------------------------------------------------------
; 
; :Examples:
;   1. Scaling an image of choice to the range [0.0,1.0]:: 
;     
;     parameters=Hash()
;     parameters['inputImage'] = enmapBox_getTestImage('AF_Image')
;     parameters['outputImage'] = FILEPATH('ScaledImage', /TMP)
;     parameters['mode'] = 'range'
;     parameters['outputRange'] = [0.,1.]
;     hubApp_imageScaling_processing, parameters
;     
;   2. Scaling an image of choice to the range [0.0,1.0], but setting the lower and upper 5 percent of all values to 0 and 1, respectively::
;     
;     parameters=Hash()
;     parameters['inputImage'] = enmapBox_getTestImage('AF_Image')
;     parameters['outputImage'] = FILEPATH('ScaledImage90', /TMP)
;     parameters['mode'] = 'range'
;     parameters['usePercentiles'] = 1 ;use percentiles
;     parameters['inputRange'] = [5,95] ;set the percentile ranks
;     parameters['outputRange'] = [0.,1.]
;     hubApp_imageScaling_processing, parameters
;   
;     
;   
; :Keywords:
;    GroupLeader: in, optional, type=long
;       Use this keyword to specify the group leader for used widgets.
;       
;    NoShow: in, optional, type=boolean
;       Set this keyword to avoid showing GUI events, for instance a progress bar.
;
;-
pro hubApp_imageScaling_processing, parameters, settings, GroupLeader=GroupLeader, NoShow=NoShow
  if ~isa(settings) then settings = hash()
  if keyword_set(groupLeader) then settings['groupLeader'] = GroupLeader
  if keyword_set(noShow) then settings['noShow'] = NoShow
  
  
  hubHelper = hubHelper()
  
  
  ;check required parameters 
  required = ['inputImage','mode','outputImage']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  
  if parameters['mode'] eq 'range' then begin
     if parameters.hubKeywordSet('usePercentiles') and $
       ~parameters.hubKeywordSet('inputRange') then $
         message, "variable 'inputRange' is undefined"
     if ~parameters.hubKeywordSet('outputRange') then $
         message, "variable 'outputRange' is undefined"
  endif
  
  if parameters['mode'] eq 'zTransformation' then begin
    ;checks
    ;mean and stdDeviation must be set together 
    required = ['mean','stdDeviation']
    if parameters.hubIsa(required, /EvaluateXOR, indicesFalse=missing) then begin
      message, 'incomplete parameter hash. missing values for key: '+strjoin(required[missing],', ')
    endif
  endif
  
  
  nine=1   ;output data ignore values? are autogenerated using a -9, -99, -999... schema
  outputDIV = parameters.hubGetValue('outputDIV')  
  
  
  ;image Objects
  imgInput = hubIOImgInputImage(parameters['inputImage'])
  imgDIV   = imgInput.getMeta('data ignore value') 
  nBands = imgInput.getMeta('bands')
  nLines = imgInput.getMeta('lines')
  
  if imgInput.isClassification() then begin
    message, "Can not scale Classification images'"
  endif
  
  tileLines = hubIOImg_getTileLines(imgInput.getMeta('samples') $
                                  , nBands $
                                  , imgInput.getMeta('data type'))
                                  
  if parameters.hubIsa('inputMask') then begin
    imgMask = hubIOImgInputImage(parameters['inputMask'], /MASK)
  endif
  imgOutput = hubIOImgOutputImage(parameters['outputImage'])
  imgOutput.copyMeta, imgInput, /CopySpatialInformation, /CopySpectralInformation,/COPYFILETYPE 
  toCopy = ['spectra names', 'wavelength', 'band names','wavelength units', 'fwhm']
  foreach tag, toCopy do begin
    if imgInput.hasMeta(tag) then imgOutput.setMeta, tag, imgInput.getMeta(tag)
  endforeach
  
  
  ;use band-statistics of external image?
  ;=> check whether inputImage and inputStatisticImg have the same number of bands
  if parameters.hubKeywordSet(['inputStatisticImg','bandwise'], /EvaluateAND) then begin
    imgExtStatistics = hubIOImgInputImage(parameters['inputStatisticImg'])
    nBandsExternalImage = imgExtStatistics.getMeta('bands')
    imgExtStatistics.cleanup
    if nBands ne nBandsExternalImage then message, 'inputImage and inputStatisticImg must have the same number of bands'
  endif

  intPar = Hash()
  ;if set, use the external image to derive the statistic for getting scaling factors etc
  intPar['inputImage'] = parameters.hubIsa('inputStatisticImg')? $
                            parameters['inputStatisticImg'] :  $
                            parameters['inputImage']
  intPar['inputMask'] = parameters.hubGetValue('inputMask')
  
  if ~parameters.hubKeywordSet('inputRange') or $
     parameters.hubKeywordSet('usePercentiles')  then begin
     imageStats = hubApp_imageStatistics_processing(intPar, settings) 
     bandStats = imageStats['bandStatistics']
     oabStats = imageStats['oabStatistics']
  endif
 
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressBar(title=settings.hubGetValue('title', default='Image Scaling') $
                         , groupLeader = settings.hubGetValue('groupLeader') $
                         )
                         
    pBarLinesDone = 0l
    pBar.setRange, [0, nLines]
  
  endif
 
  ;define output ranges
  if parameters.hubIsa('outputRange') then begin
    rangesOut = make_array(2,nBands, /Double)
    rangesOut[0,*] = (parameters['outputRange'])[0,*]
    rangesOut[1,*] = (parameters['outputRange'])[1,*]
  endif
  
  ;define ranges to trim
  if parameters.hubKeywordSet('trimOutliers') then begin
      if ~parameters.hubIsa('outputRange') then message, "keyword 'trimOutliers' requires a definition of 'outputRange'"
      trimRanges = rangesOut
  endif    
 
  
  ; Prepare parameters for linear scaling
  if parameters['mode'] eq 'range' then begin
    ;image statisic =statistics of input image (default) or (inputStatisticImg is set) of external image
    ;rangeModes (internal use only):
    ;0 : inputRange(s) in data units 
    ;    -> userdefined 'inputRange'. If not defined use min/max from image statistic  
    ;1 : inputRange = percentileRanks in 'inputRange' 
    ;    -> use percentiles fo percentileRank defined in inputRange based on total image statistic
    ;2 : inputRange(s) in data units, bandwise 
    ;    -> userdefined 'inputRange' / from input band statistics if undefined
    ;3 : inputRange = percentileRanks in 'inputRange' -> use percentiles from band statistic
    
    rangeMode = 0
    if parameters.hubKeywordSet('usePercentiles') then rangeMode += 1
    if parameters.hubKeywordSet('bandwise') then rangeMode += 2
    
    ;the final input range
    rangesIn = make_array(2, nBands, /Double)
    case rangeMode of 
      0 : begin ;data unit, image statistic
            if parameters.hubIsa('inputRange') then begin
              rangesIn[0,*] = (parameters['inputRange'])[0]
              rangesIn[1,*] = (parameters['inputRange'])[1]
            endif else begin
              rangesIn[0,*] = oabStats.range[0]
              rangesIn[1,*] = oabStats.range[1]
            endelse
          end
      1 : begin ;percentiles, image statistic
              hist = oabStats.histogram
              dataValues    =  [0,hist['binEnd']]
              dataOccurence =  [0, hist['cumulativeDistribution']] * 100
              percentiles = interpol(dataValues, dataOccurence, float(parameters['inputRange']))
              rangesIn[0,*] = percentiles[0,*]
              rangesIn[1,*] = percentiles[1,*]
          end
      2 : begin ;data units, bandwise
            if parameters.hubIsa('inputRange') then begin
              rangesIn[0,*] = (parameters['inputRange'])[0]
              rangesIn[1,*] = (parameters['inputRange'])[1]
            endif else begin 
              rangesIn[0,*] = bandStats[*].range[0]
              rangesIn[1,*] = bandStats[*].range[1]
            endelse 
          end
      3 : begin ;percentiles, bandwise
            ;input ranges are percentile rank ranges
            percentileRanks = make_array(2, nBands, /Double)
            percentileRanks[0,*] = (parameters['inputRange'])[0,*]
            percentileRanks[1,*] = (parameters['inputRange'])[1,*]
            
            for iBand=0, nBands - 1 do begin
              ;get histogram for un-masked values in band iBand
              hist = bandStats[iBand].histogram
              dataValues    =  [0, hist['binEnd']]
              dataOccurence =  [0, hist['cumulativeDistribution']] * 100
              rangesIn[*,iBand] = interpol(dataValues, dataOccurence, percentileRanks[*,iBand])
            endfor
          end
    endcase
    
    if ~isa(outputDIV) then begin
     outputDIV = hubHelper.getDataIgnoreValueSuggestion(parameters['outputRange'], Nine=Nine)
    endif
    
   
    ;calculate gains and offsets for each band    
    gains   = (rangesOut[1,*] - rangesOut[0,*]) / (rangesIn[1,*]-rangesIn[0,*])
    offsets = -rangesIn[0,*] * gains + rangesOut[0,*] 
  endif
  
  
  ;
  ; prepare parameters for Z-Tranformation / Standard Score Scaling
  ;
  if parameters['mode'] eq 'zTransformation' then begin
   
    means = make_array(nBands, /Double)
    stdDevs = make_array(nBands, /FLOAT)
    
    ;values for stdDev and mean are given directly
    if parameters.hubIsa(['stdDeviation', 'mean'], /EvaluateOR) then begin
     ;parameters['mean'] or parameters['stdDeviation'] can be 
     ;(1) a scalar or 
     ;(2) a vector containing a value for each band 
     ; -> keyword bandwise does not matter 
      means[*] = parameters['mean']
      stdDevs[*] = parameters['stdDeviation']
    endif else begin
    ;values for stdDev and mean must be derived from an image statistic
      if parameters.hubKeywordSet('bandwise') then begin
        
        means[*]    =  bandStats[*].mean
        stdDevs[*]  =  bandStats[*].standardDeviation
      endif else begin
        means[*]    =  oabStats.mean
        stdDevs[*]  =  oabStats.standardDeviation
      endelse
    endelse
    
;    if ~isa(outputDIV) then begin
;      ;use band statistics of each band with respect to ignored/masked values
;      maxScore = max(((imageStats['maxValues'])[*,1] - means) / stdDevs)
;      minScore = min(((imageStats['minValues'])[*,1] - means) / stdDevs)
;      outputDIV = hubHelper.getDataIgnoreValueSuggestion([minScore, maxScore], Nine=Nine)
;    endif
  endif
  
  ;Tile-loop
  imgInput.initReader, tileLines, /Slice, /TileProcessing
  if isa(imgMask) then imgMask.initReader, tileLines, /Slice, /TileProcessing, /Mask
  
  outputDataType = parameters.hubGetValue('outputDataType', default=4)
  if isa(outputDIV) then imgOutput.setMeta, 'data ignore value', outputDIV
  imgOutput.initWriter, imgInput.getWriterSettings(SetDataType=outputDataType)
    
  while ~imgInput.tileProcessingDone() do begin
    imageData = imgInput.getData()
    if isa(imgMask) then externalMask = imgMask.getData() else externalMask = !NULL
  
    imageDataMask = hubHelper.getImageMask( imageData, 1, imageDIV=imgDIV $
                                          , externalMask = externalMask $
                                          , /PreserveDimensions)
   
    
    ;create output array with outputDIV as default value
    outputData = make_array(size(imageData, /DIMENSION) $
                       , value=outputDIV $
                       , type=isa(outputDataType)? outputDatatype : 4) ;default: float output
    
    ;add scaled data to the output array
    for iBand = 0, nBands - 1 do begin
      iBandPixels = where(imageDataMask[iBand,*] ne 0, /NULL)
      if isa(iBandPixels) then begin
        case parameters['mode'] of
          'range'           : outputData[iBand, iBandPixels] = imageData[iBand, iBandPixels] * gains[iBand] + offsets[iBand]
          'zTransformation' : outputData[iBand, iBandPixels] = (imageData[iBand, iBandPixels] - means[iBand]) / stdDevs[iBand] 
        endcase
        
        ;trim values to the range given in trimRange
        if isa(trimRanges) then begin
          ;outputData[iBand, where(imageDataMask[iBand, *] ne 0 and outputData[iBand, *] lt trimRanges[0,iBand], /NULL)] = trimRanges[0,iBand]
          ;outputData[iBand, where(imageDataMask[iBand, *] ne 0 and outputData[iBand, *] gt trimRanges[1,iBand], /NULL)] = trimRanges[1,iBand]
          
          outputData[iBand, *] >= trimRanges[0,iBand]
          outputData[iBand, *] <= trimRanges[1,iBand] 
        endif
      endif
    endfor
    imgOutput.writeData, temporary(outputData)
    if isa(pBar) then begin
        pBarLinesDone += tileLines
        pBarLinesDone <= nLines
        pBar.setProgress,  pBarLinesDone
    endif 
  endwhile
  
  if isa(pBar) then pBar.cleanup
  imgInput.cleanup
  imgOutput.cleanup
  if isa(imgMask) then imgMask.cleanup

end


;+
; :hidden:
;-
pro test_hubApp_imageScaling_processing
  t = Hash()
  ;t['outputRange'] = [-100,0]
  
  t['accept']= 1
  t['mode'] = 'range'
  t['usePercentiles'] = 1
  t['inputRange']=[25,75]
  t['outputRange'] = [0.,1]
  
;  t['inputRange'] = [0,10000]
;  t['inputPercentileRange'] = [25,75]
;  t['inputRangeMode']='dataValues'
  ;t['inputRangeMode']='imagePercentiles'
  ;t['inputRangeMode']='bandPercentiles' 
  base = 'D:\Programme\IDL81\IDL\IDL81\products\enmapBox\data\'
  t['inputImage']= enmapBox_getTestImage('AF_Image')
  ;t['inputImage']= base + 'Hymap_Berlin-B_Image'
  t['inputMask']= enmapBox_getTestImage('AF_Mask')
  t['outputImage']= FILEPATH('testScaledImage', /TMP)
  ;t['outputDataType']= 4
  ;t['mode'] = 'zTransformation'
  hubApp_imageScaling_processing, t

  print, 'test done'
end
