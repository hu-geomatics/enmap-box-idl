;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-
;+
; :Description:
;    Use this event handler to call the imageScaling application from an EnMAP-Box menu button.
;    
; :Params:
;    event: in, required, type=button event structure
;-
pro hubApp_imageScaling_event $
  ,event
  
  @huberrorcatch
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  hubApp_imageScaling_application, applicationInfo

end