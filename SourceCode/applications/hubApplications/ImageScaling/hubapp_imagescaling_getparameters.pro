;+
; :Description:
;    Widget callback function. Checks whether the input is correct.
;
; :Params:
;    resultHash: in, required, type=hash
;       Hash with output collected form the sub-widget.
;       
; :Keywords:
;    Message: out, optional, type= string[]
;     Set this to provide an information on occured errors.
;
;-
function hubApp_imageScaling_getParameters_Check, resultHash, Message=message, UserInformation=userInformation
   
   msg = !NULL
   
   inputImage = hubIOImgInputImage((resultHash['inputSampleSet'])['featureFilename'])
   isClassiciation = inputImage.isClassification()
   inputImage.cleanup
   if isClassiciation then begin
    msg =  [msg, "Input Image can not be a Classification Image'"] 
   endif
   
   if strcmp(userInformation['mode'], 'range', /Fold_case) then begin 
     if resultHash.hubIsa('inputRangeMin') then begin
       if resultHash['inputRangeMin'] ge resultHash['inputRangeMax'] then begin
        msg = [msg, 'Input range minimum is not less than input range maximum.']
       endif
     endif
     
     if resultHash.hubIsa('inputPercentileRangeMin') then begin
       if resultHash['inputPercentileRangeMin'] ge resultHash['inputPercentileRangeMax'] then begin
        msg = [msg, 'Input percentile range minimum is not less than input range maximum.']
       endif
     endif
     
     if resultHash.hubIsa('outputRangeMin') then begin
       if resultHash['outputRangeMin'] ge resultHash['outputRangeMax'] then begin
        msg = [msg, 'Output range minimum is not less than output range maximum.']
       endif
     endif
     ;TODO
     ;if keyword bandwise is set and an external statistic file is given 
     ;=> check whether nBands != nBands of input image
   endif 
   
   if strcmp(userInformation['mode'], 'zTransformation', /Fold_case) then begin
     ;if set manually, both, mean and stdDev must be given 
     if resultHash.hubIsa(['stdDeviation', 'mean'], /EvaluateXOR) then begin
      msg = [msg, 'Set Standard Deviation and Mean.']
     endif 
     ;TODO
     ;check whether keyword  /bandwise is set and an external statistic file 
     ;having nBands != nBands of input image
   endif
   message = msg
   return, ~isa(msg)


end


;+
; :Description:
;    This function creates a widget to collect the parameters required for 
;    parameterization of `hubApp_ImageScaling_processing` routine.
;
; :Params:
;     applicationInfo: in, required, type=Hash
;     
;     defaultValues: in, optional, type=hash
;       A hash providing default values, e.g. from a previous call of this dialog.
;        
;-
function hubapp_imageScaling_getparameters, applicationInfo, defaultValues
  if ~isa(defaultValues) then defaultValues = Hash()
  
  groupLeader = applicationInfo.hubgetValue('groupLeader')
  mode = applicationInfo.hubgetValue('mode', default=applicationInfo.hubgetValue('argument', default='range'))
  title = 'Image Scaling: '
   case mode of 
    'range' : title += 'From Input Range to Output Range'
    'zTransformation': title += 'Standardize Data via Z-Transformation'
    else : message, 'scale mode is undefined'
  endcase

  ;name of data type as shown in the combobox
  helper = hubHelper()
  
  datatype_names  = helper.getDataTypeInfo(/TYPEDESCRIPTIONS)
  datatype_values = helper.getDataTypeInfo(/TypeCodes)
 
  hubAMW_program, groupLeader, Title = title
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputSampleSet, 'inputSampleSet', title='Image', /Masking $
                       , ReferenceTitle=       'Mask ' $
                       , Value = defaultValues.hubGetValue('inputImage') $
                       , /ReferenceOptional $
                       , ReferenceValue = defaultValues.hubGetValue('inputMask')
                         
                         
  if strcmp(mode,'range', /fold_case) then begin
    hubAMW_frame ,   Title = 'Input Range' 
    usePercentiles = defaultValues.hubKeywordSet('usePercentiles')
    hubAMW_subframe, 'usePercentiles', title='by Data Values',/Row, SetButton=~usePercentiles
      ;hubAMW_subframe, /Row
      def = ~usePercentiles ? defaultValues.hubGetValue('inputRange') : !NULL
      hubAMW_parameter, 'inputRangeMin', title='Min', /Float $
                      , value = isa(def) ? def[0] : !NULL
      hubAMW_parameter, 'inputRangeMax', title='Max', /Float $
                      , value = isa(def) ? def[1] : !NULL
    hubAMW_subframe, 'usePercentiles', title='by Percentiles', /Row, SetButton=usePercentiles
      def = usePercentiles ? defaultValues.hubGetValue('inputRange', default=[5,95]) $
                           : [5,95]
      hubAMW_parameter, 'inputPercentileRangeMin', title='Min', IsGE=0, IsLT=100, Unit='%  ', /Float, Size=5 $
                      , value=def[0] 
      hubAMW_parameter, 'inputPercentileRangeMax', title='Max', IsGT=0, IsLE=100, Unit='%', /Float, Size=5 $
                      , value=def[1]
    
    hubAMW_frame, title='Output Range'
    hubAMW_subframe, /Row
       def = defaultValues.hubGetValue('outputRange', default=[0,1])
       hubAMW_parameter, 'outputRangeMin', title='Min', /Float $
                       , value=def[0] 
       hubAMW_parameter, 'outputRangeMax', title='Max', /Float $
                       , value=def[1]
                       
    hubAMW_subframe, /Column
       hubAMW_checkbox,  'trimOutliers', title='  Trim Outliers to Min/Max' $
                      , value=defaultValues.hubGetValue('trimOutliers', default=1b)

    hubAMW_frame, Title='Advances Settings', /Advanced
      hubAMW_checklist, 'bandwise', title='Calculate percentiles' $
                      , list=['from full image', 'bandwise'] $
                      , value=defaultValues.hubGetValue('bandwise', default=1b) 
                      
      def = defaultValues.hubGetValue('inputStatisticImg')
      hubAMW_inputImageFilename, 'inputStatisticImg', Title='from external image' $
                               , value = def $
                               , Optional= isa(def)? 1 : 2 
        
  endif
    
  if strcmp(mode,'zTransformation', /fold_case) then begin
    hubAMW_frame ,   Title =  'Parameters', /Advanced
    ;scoreModeNames = ['imageStatistics','bandStatistics','manually']
    setManually = defaultValues.hubIsa(['mean','stdDeviation'], /EvaluateAND)
    hubAMW_subframe, 'scoreMode' $
                   , title = 'Use Mean and Standard Deviation from Image Statistic' $
                   , /Column, SetButton = ~setManually
      
    hubAMW_subframe, 'scoreMode', title='Set Mean and Standard Deviation manually' $
                   , /ROW, SetButton = setManually
                   
      hubAMW_parameter, 'mean', title='Mean', /Float $
                      , value=defaultValues.hubGetValue('mean', default=0.)
      hubAMW_parameter, 'stdDeviation', title='Standard Deviation', /Float $
                      , value=defaultValues.hubGetValue('stdDeviation', default=1.)
    
    hubAMW_subframe,/COLUMN 
      hubAMW_checklist, 'bandwise', title='Calculate Mean and Standard Deviation', value=1 $
                      , list=['from full image', 'bandwise']
      hubAMW_inputImageFilename, 'inputStatisticImg', Title='from external image', optional=2
              
  endif
       
  hubAMW_frame ,  Title =  'Output'
    hubAMW_parameter,      'outputDIV', Title = 'Data Ignore Value', OPTIONAL=2, /Float
    hubAMW_combobox,       'outputDataType', Title='Data Type   ', Value=3, List= datatype_names, Extract=datatype_values, /ADVANCED
    hubAMW_outputFilename, 'outputImage'   , Title='Scaled Image', Value='scaled'
 
  result = hubAMW_manage(ConsistencyCheckFunction='hubapp_imageScaling_getparameters_Check', UserInformation=Hash('mode', mode))
  
  if result['accept'] then begin
      if strcmp(mode,'range', /fold_case) then begin
            result['mode'] = 'range'
            result['inputRange'] = result['usePercentiles'] ? $
                          [result['inputPercentileRangeMin'], result['inputPercentileRangeMax']] $
                         :[result['inputRangeMin'], result['inputRangeMax']]
            
            result['outputRange'] = [result['outputRangeMin'], result['outputRangeMax']]
            ;delete non-required keys
            result.hubRemove, ['inputPercentileRangeMin','inputPercentileRangeMax' $
                               ,'inputRangeMin','inputRangeMax' $
                               ,'outputRangeMin','outputRangeMax']
      endif
      
      if strcmp(mode,'zTransformation', /fold_case) then begin
          result['mode'] = 'zTransformation'
          ;delete non-required keys
          if result['scoreMode'] eq 0 then result.hubRemove, ['stdDeviation', 'mean']
          result.hubRemove, ['scoreMode']
      endif
      
      result['inputImage'] = (result['inputSampleSet'])['featureFilename']
      result['inputMask']  = (result['inputSampleSet'])['labelFilename']
      
      ;delete non-required keys
      result.hubRemove,'inputSampleSet'
  endif
  return, result
end

;+
; :Description:
;    Describe the procedure.
;
; :hidden:
; :private:
;
;
; :Author: geo_beja
;-
pro test_hubapp_imageScaling_getparameters
  ai = Hash('mode', 'zTransformation')
  r = hubapp_imageScaling_getparameters(ai)
  print, r
end