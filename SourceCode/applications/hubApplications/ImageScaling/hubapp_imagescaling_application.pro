;+
; :Copyright:
;
; :Author: Benjamin Jakimow <benjamin.jakimow@geo.hu-berlin.de>
;-
;+
; :Description:
;    This procedure is the main routine to perform an image scaling operation. It opens a widget dialog to collect the required 
;    parameters form the user and then starts the scaling procedure.
;
;-
pro hubApp_imageScaling_application, applicationInfo
  
  ;restore state from previous application calls to retrieve default values
  defaultValues = hub_getAppState('hubApp', 'stateHash_ImageScaling')
  

  parameters = hubapp_imageScaling_getParameters(applicationInfo, defaultValues)
  if parameters['accept'] then begin
    ;save parameters as new session state hash
    hub_setAppState, 'hubApp', 'stateHash_ImageScaling', parameters
    
    time1 = systime(1)
    hubApp_imageScaling_processing, parameters, groupLeader=applicationInfo.hubGetValue('groupLeader')
    sec = strcompress(ulong64(systime(1)-time1),/REMOVE_ALL)
    log = ['Scaling completed.' $
          ,'(time needed: '+sec+' sec)' ]
    ok = dialog_message(log,/INFORMATION,TITLE='Image Scaling')
  endif
end
