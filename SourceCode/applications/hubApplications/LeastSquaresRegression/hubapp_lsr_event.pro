;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

pro hubApp_LSR_event, event
  
  ; set up a default error handler
  
  @huberrorcatch
  
  ; query information about the application (which is stored inside menu button)
  ;   - note that menu buttons are defined inside the menu file '.\hubApp_ImageSubsetting\_resource\enmap.men'
  ;   - the following information is returned as a hash:
  ;       applicationInfo['name'] = 'hubApp_ImageSubsetting Application'
  ;       applicationInfo['argument] = 'hubApp_ImageSubsetting_ARGUMENT'
  ;       applicationInfo['groupLeader'] = <EnMAP-Box top level base>
  
  applicationInfo = enmapBoxUserApp_getApplicationInfo(event)
  
  ; call the application main procedure
  ; if required, provide menu button information to it
 
  hubApp_LSR_application, applicationInfo
  
end