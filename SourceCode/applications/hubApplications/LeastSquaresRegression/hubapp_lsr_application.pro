;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-

;+
; :Description:
;    This routine controls the main program flow
;    
; :Params:
;    applicationInfo: in, required, type=hash
;     This hash must contain at least the key `argument`, which can have one 
;     of the following values::
;     
;       parameterize -> train an new model
;       apply        -> estimate values using an existing model
;       view         -> view a models parameterization
;       validate     -> validate a model
;       
;
;
;
;-
pro hubApp_LSR_application, applicationInfo
  
  
  settings = hubApp_getSettings()
  settings += applicationInfo
  settings['title'] = 'Least Squares Regression'
  
  ;restore state from previous application calls to retrieve default values
  stateHash = hub_getAppState('hubApp', 'stateHash_LSR')
  if isa(stateHash) then settings += stateHash
  
  case settings['argument'] of
    'apply'        : parameters = hubApp_LSR_apply_getParameters(settings)
    'parameterize' : parameters = hubApp_LSR_parameterize_getParameters(settings)
    'view'         : parameters = hubApp_LSR_viewModel_getParameters(settings)
    'validate'     : parameters = hubApp_LSR_validate_getParameters(settings)
  endcase
  
  if parameters['accept'] then begin
    ;save parameters as new session state hash
    hub_setAppState, 'hubApp', 'stateHash_LSR', parameters
    
    case settings['argument'] of
      'parameterize'    : begin 
                            hubApp_LSR_parameterize_processing, parameters
                            report = hubApp_LSR_viewModel_getReport(parameters)
                            report.SaveHTML, /Show
                          end
                          
      'view'            : begin
                            report = hubApp_LSR_viewModel_getReport(parameters)
                            report.saveHTML, /Show
                          end
                          
      'apply'           : hubApp_LSR_apply_processing, parameters
      
      'validate'        : begin
                            performance = hubApp_LSR_validate_processing(parameters, settings)
                            report = hubApp_accuracyAssessment_getReport(performance, /createPlots, GroupLeader=GroupLeader)
                            report.SaveHTML, /Show
                            file_delete, performance['inputEstimation']  $
                                       , performance['inputEstimation'] + '.hdr', /ALLOW_NONEXISTENT
                          end
    endcase
  endif
end