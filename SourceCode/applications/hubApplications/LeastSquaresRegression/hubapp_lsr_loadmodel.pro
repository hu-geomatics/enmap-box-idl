;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
;   Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    Loads a least-squares regression model and returns it after checking it for consistency. 
;
; :Params:
;    modelPath: in, required, type = string
;     The file path of the model.
;
; :Keywords:
;    ErrorMessage: out, optional, type = string
;     In case the function is not able to load the model, it 
;     will write an error message to this variable. This error
;     message can be used by a calling routine. 
;
;-
function hubApp_lsr_loadModel, modelPath, ErrorMessage=ErrorMessage

  ErrorMessage = []
  model = !NULL
  ;check if file exist
  fileInfo = file_info(modelPath)
  if ~fileInfo.exists then begin
    ErrorMessage = "File '"+modelPath+"' does not exist."
  endif else begin
    savFile = IDL_Savefile(modelPath)
    if total(savFile.Names() eq 'MODEL') ne 1 then begin
      ErrorMessage = 'SAV File contains no model information'
    endif else begin
      savFile.restore, 'MODEL'
      ;TODO Check MODEL content
    endelse
    savFile.cleanup
  endelse
  return, model
end