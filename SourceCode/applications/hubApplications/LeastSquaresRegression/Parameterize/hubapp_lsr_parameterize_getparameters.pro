;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    Collects and returns parameterization setting related to the regression type 
;    specified in dialog `hubApp_LSR_parameterize_getParameters1`.
;
; :Params:
;    parameters1: in, required, type=hash
;     This is the parameter hash returned from `hubApp_LSR_parameterize_getParameters1`. 
;
;    settings: in, required, type=hash
;     This is the settings hash used to describe GUI relevant parameters.
;     
;
;
;
;-
function hubApp_LSR_parameterize_getParameters2,parameters1, settings 
  
  type = parameters1['type'] 
  case type of
    'ULSR' : title = 'Univariate ' + settings['title']
    'MLSR' : title = 'Multivariate ' + settings['title']
  endcase
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']

  inputImage = hubIOImgInputImage(parameters1['inputImage'])
  nBands = inputImage.getMeta('bands')
  bandNames = inputImage.getMeta('band names')
  if ~isa(bandNames) then begin
    bandNames = 'band'+strtrim(indgen(nBands)+1,2)
  endif
  inputImage.cleanup

  hubAMW_frame ,   Title =  'Parameters'
;  case type of
;    'ULSR' : hubAMW_label, '$y = a + b_1*x^1 + ... + b_d*x^d$' 
;    'MLSR' : hubAMW_label, 'y = a + a*x^1 + ... + a*x^d'
;  endcase
  
  isULRS = type eq 'ULSR' 
    if isULRS then begin
      hubAMW_parameter, 'degree' $
            , Title='Polynomial Degree' $
            , value = parameters1.hubGetValue('degree', default=1) $
            , IsGE=0, /Integer
    endif 
    
    hubAMW_list, 'featureIndices' $
               , Title= 'Select Input '+ ((isULRS) ? 'Feature' : 'Features for 1st degree polynomial' )$
               , List=bandNames $
               , ALLOWEMPTYSELECTION=0b $
               , MULTIPLESELECTION= ~isULRS $
               , XSIZE=300, YSIZE= 200 < nBands * 50
    
    ; frame for output options & parameters
    hubAMW_frame ,   Title =  'Output'
  
    defModel = settings.hubGetValue('model', default='model.lsr')
    hubAMW_outputFilename, 'model',TITLE='Model',value=defModel, tsize=tsize
    parameters = hubAMW_manage()
    if parameters['accept'] then begin
  
    endif
  return, parameters
end

;+
; :Description:
;    Explicit consistency check function called by `hubApp_LSR_parameterize_getParameters1`.
;-
function hubApp_LSR_parameterize_getParameters1Check, results, message=message
  msg = []
  
  inImg = hubIOImgInputImage((results['inputSampleSet'])['featureFilename'])
  if results.hubIsa('inputMask') then begin
    inMask = hubIOImgInputImage(results['inputMask'])
    if ~inImg.isCorrectSpatialSize(inMask.getSpatialSize()) then begin
      msg = [msg, 'Mask and Image must have same spatial size']
    endif
    inMask.cleanup
  endif
  inImg.cleanup
  
  message = msg
  return, ~isa(msg)
end

;+
; :Description:
;    This function collects file pathes of images used to parameterize a LSR model and the
;    model type (ULSR or MLSR).
;
; :Params:
;    settings: in, required, type=hash
;     This is the settings hash used to describe GUI relevant parameters.
;     
;-
function hubApp_LSR_parameterize_getParameters1, settings
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title']
  ; frame for output options & parameters
  
  tsize = 70
  hubAMW_frame ,   Title =  'Input'
    hubAMW_inputSampleSet, 'inputSampleSet' $
          , value = settings.hubGetValue('inputImage') $
          , Title          ='Image    ' $
          , REFERENCETITLE ='Reference' $
          , /REGRESSION, tsize=tsize
    hubAMW_inputImageFilename,'inputMask',TITLE='Mask ',OPTIONAL=2, tsize=tsize
    
  hubAMW_frame, title='Parameters'
  modeNames = ['Univariate Least Squares','Multivariate Least Squares'] 
  modes     = ['ULSR','MLSR']
  hubAMW_combobox, 'type', Title='Regression Model Type', List=modeNames, EXTRACT=modes
  
    
 
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_LSR_parameterize_getParameters1Check')
  if parameters['accept'] then begin
    parameters['inputImage'] = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputLabels'] = (parameters['inputSampleSet'])['labelFilename']
    parameters.hubRemove, ['inputSampleSet']
  endif
  return, parameters
end

;+
; :Description:
;    Describe the procedure.
;
; :returns:
;    If the user input is valid, the function returns a hash containg the following
;    values for using them as input in `hubApp_LSR_parameterize_processing`::
;     
;       key            | type   | description
;       ---------------+--------+---------------
;       type           | string | type of LSR. Can be set to
;                      |        |  ULSR = Univariate LSR
;                      |        |  MLSR = Multivariate LSR
;       inputImage     | string | filepath of image with input data
;       inputLabels    | string | filepath of image with training reference labels
;       model          | string | filepath to store the final model
;       featureIndices | int[]  | indices of feature(s) to use train the model on
;                      |        | ULSR = one 1 band in inputImage
;                      |        | MLSR = one up to all bands in inputImage
;       degree         | int    | In case type = ULSR: the polynomial degree or
;                      |        | number of coefficients to derive 
;       ---------------+--------+---------------   
;
;   
; :Params:
;    settings: in, optional, type=hash
;     Use this hash to provide basic application, GUI infos or default values::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | application title (required) 
;       ------------+--------+---------------  
;
;
;-
function hubApp_LSR_parameterize_getParameters, settings
  CANCEL = Hash('accept',0b)
  
  parameters = hubApp_LSR_parameterize_getParameters1(settings)
  if ~parameters['accept'] then return, CANCEL
  
  parameters += hubApp_LSR_parameterize_getParameters2(parameters, settings)
  if ~parameters['accept'] then return, CANCEL

  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_LSR_parameterize_getParameters

  ; test your routine
  settings = hubApp_getSettings()
  settings['type'] = 'ULSR'
  settings['type'] = 'MLSR'
  settings['title'] = 'Test-Mode: '+settings['type']
  parameters = hubApp_LSR_parameterize_getParameters(settings)
  print, parameters

end  
