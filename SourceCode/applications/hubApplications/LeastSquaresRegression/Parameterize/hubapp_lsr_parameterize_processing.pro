;+
; :Description:
;    This procedure parameterizies a new least squares regression (LSR) model.
;
; :Params:
;    parameters: in, required, type=hash()
;     Use this hash to provide following values::
;    
;       key            | type   | description
;       ---------------+--------+---------------
;       type           | string | type of LSR. Can be set to
;                      |        |  ULSR = Univariate LSR
;                      |        |  MLSR = Multivariate LSR
;       inputImage     | string | filepath of image with input data
;       inputLabels    | string | filepath of image with training reference labels
;       model          | string | filepath to store the final model
;       featureIndices | int[]  | indices of feature(s) to use train the model on
;                      |        | ULSR = one 1 band in inputImage
;                      |        | MLSR = one up to all bands in inputImage
;       degree         | int    | In case type = ULSR: the polynomial degree or
;                      |        | number of coefficients to derive 
;       ---------------+--------+---------------  
;       
;       
;    settings: in, optional, type=hash
;     Use this hash to provide following basic application infos::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | this applications title 
;       ------------+--------+---------------  
;
;
;-
pro hubApp_LSR_parameterize_processing, parameters, settings
  hubApp_checkKeys, parameters, ['type', 'inputImage', 'inputLabels','model','featureIndices']
  if ~isa(settings) then settings = hash()
  
  helper = hubHelper()
  
  type = parameters['type']
  if ~isa(where(type eq ['ULSR','MLSR'], /NULL)) then begin
    message, "Unknown LSR Type: '"+ type +"'" 
  endif
  
  
  inFeatures = hubIOImgInputImage(parameters['inputImage'])  
  nSamples = inFeatures.getMeta('samples')
  nLines   = inFeatures.getMeta('lines')
  nBands   = inFeatures.getMeta('bands')
  
  featureIndices =  parameters['featureIndices']
  if min(featureIndices) lt 0 or max(featureIndices) gt nBands -1 then begin
    message, "'featureIndices' value(s) must be in range [0, number of bands -1]"
  endif
  
  if type eq 'ULSR' then begin
    hubApp_checkKeys, parameters, ['degree']
    degree = parameters['degree']
    if degree lt 0 then begin
      message, "'polynomialDegree' must be greater equal 0"
    endif
  endif 

  inLabels   = hubIOImgInputImage(parameters['inputLabels'], /Regression)
  
  inMask = []
  if parameters.hubIsa('inputMask') then begin
    inMask = hubIOImgInputImage(parameters['inputMask'], /Mask)
  endif
    
  inFeatureDIV = inFeatures.getMeta('data ignore value')
  nFeatures = inFeatures.getMeta('bands')
  inLabelDIV   = inLabels.getMeta('data ignore value')
  if ~isa(inLabelDIV) then message, 'Label File must have a data ignore value'
   
  tileLines = hubIOImg_getTileLines(nSamples, nBands+1, inFeatures.getMeta('data type'))
  
  subsetBandPositions = List(featureIndices,0)
  
  ;init all readers
  hubApp_manageReaders, [inFeatures, inLabels], tileLines $
          , maskImages=[inMask] $
          , /Init, /Slice $
          , SubsetBandPositions=subsetBandPositions
          
  nPixelCount = 0ll
  
  ;extract features and labels
  features = []
  labels = []
  while ~inFeatures.tileProcessingDone() do begin
      featureData = inFeatures.getData()
      labelData   = inLabels.getData()
      
      mask = helper.getImageMask(featureData, 1, imageDIV=inFeatureDIV)
      mask and= labelData ne inLabelDIV
      if isa(inMask) then begin
        mask and= inMask.getData() eq 1 
      endif 
      
      iValid = where(mask ne 0, nValid, /NULL)
      if isa(iValid) then begin
        features = [[features], [featureData[*, iValid]]]
        labels  = [labels  , labelData[iValid]]
        nPixelCount += nValid
      endif

  endwhile
  
  ;general model settings
  model = hash()
  model += parameters.hubGetSubHash(['type','inputImage','inputLabels','featureIndices','degree'])
  model['nBands'] = nBands
  model['data ignore value'] = inLabelDIV
  ;type specific model settings
  if type eq 'ULSR' then begin
    coeff = poly_fit(features, labels, degree, /Double, Status=status)
    model['coeff'] = n_elements(coeff) gt 1 ? coeff[1:*] : !NULL
    model['const'] = coeff[0]
  endif
  
  if type eq 'MLSR' then begin
    model['coeff'] = regress(features, labels, const=const, /Double, status=status)
    model['const'] = const
  endif
  model['status'] = status
  model['created'] = systime()
  
  
  ;save model
  save, model, FILENAME=parameters['model']
  enmapBox_openFile, parameters['model']
  inFeatures.cleanup
  inLabels.cleanup
  if isa(inMask) then inMask.cleanup

end

;+
; :Hidden:
;-
pro test_hubApp_LSR_parameterize_processing

  ;['type', 'inputImage', 'inputLabels', 'featureIndices','outputModel']
  parameters = hash()
  parameters['type'] = 'ULSR'
  parameters['degree'] = 0
  parameters['inputImage'] = enmapBox_getTestImage('AF_Image')
  parameters['inputLabels'] = enmapBox_getTestImage('AF_LAI_Training')
  parameters['model'] = 'D:\outputModel'
  parameters['featureIndices'] = 40
  hubApp_LSR_parameterize_processing, parameters, settings
end