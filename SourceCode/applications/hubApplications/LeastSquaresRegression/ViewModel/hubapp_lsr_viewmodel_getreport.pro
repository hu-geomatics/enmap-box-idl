;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;
;+
; :Description:
;    This function returns a `hubReport()` describing a certain models parameterization. 
;
; :Params:
;    parameters: in, required, type=hash
;     This hash must contain the key `model` describing the file path of the
;     least squares regression model you want to generate a report for.
;     
;
;
;
;-
function hubApp_LSR_viewModel_getReport, parameters
  hubApp_checkKeys, parameters, 'model'
  
  modelPath = parameters['model']
  model = hubApp_lsr_loadModel(modelPath, ErrorMessage=ErrorMessage)
  if ~isa(model) then begin
      message, errorMessage
  endif
  
  case model['type'] of
    'ULSR' : title = 'Univariate Least Squares Regression Model'
    'MLSR' : title = 'Multivariate Least Squares Regression Model'
    else : message, 'unknown model type'
  endcase
  report = hubReport(Title=title)
  
  info = list()
  report.addHeading, 'Model File', 2
  info.add, 'Path : ' + modelPath
  info.add, 'Type : ' + title
  report.addMonospace, info.toArray()
  
  report.addHeading, 'Training Data', 2
  info = list()
  info.add, 'Image         : ' + model['inputImage']
  info.add, 'Band number(s): '+ strtrim(n_elements(model['featureIndices']),2)
  info.add, 'Labels        : ' + model['inputLabels']
  
  report.addMonospace, info.toArray()
  
  info = list()
  report.addHeading, 'Model Specification', 2
  info.add, 'Data ignore value for masked pixels: '+strtrim(model['data ignore value'],2)
  info.add, ''
  info.add, 'Constant    : '+strtrim(model['const'],2)
  info.add, 'Coefficients:
  info.add, ''

  coefficients = model['coeff']
  featureIndices = model['featureIndices']
  case model['type'] of
    'ULSR': begin
;              headings = ['#', 'coeff', 'exp']
;              formats  = ['(%"%i")','(%"%10.5f")','(%"%i")']
              table = hubReportTable();headings, formats=formats)  
              table.addColumn, name='#'
              table.addColumn, name='coeff'  
              table.addColumn, name='exp'       
              foreach c, coefficients, i do begin
                table.addRow, list(i+1, c, i+1)
              endforeach
              info.add, table.getFormatedASCII(/Trim), /Extract
            end
    'MLSR': begin
;              headings = ['#', 'coeff', 'related band']
;              formats  = ['(%"%i")','(%"%10.5f")','(%"%-s")']
              table = hubReportTable();headings, formats=formats)
              table.addColumn, name='#'
              table.addColumn, name='coeff'
              table.addColumn, name='related band'            
              foreach c, coefficients, i do begin
                table.addRow, list(i+1, c, string(format='(%"Band %-4i")',featureIndices[i]+1))
              endforeach
              info.add, table.getFormatedASCII(/Trim), /Extract
            end
  endcase
  report.addMonospace, '   ' + info.toArray()
  return, report
  
end

;+
; :Hidden:
;    
;-
pro test_hubApp_LSR_viewModel_getReport
  model = hash()
  model['type'] = 'ULSR'
  model['type'] = 'MLSR'
  model['inputImage'] = enmapBox_getTestImage('AF_Image')
  model['inputLabels'] = enmapBox_getTestImage('AF_LAI_Training')
  model['featureIndices'] = [4,5,3]
  model['degree'] = 1
  model['nBands'] = 245
  model['coeff'] = [0.,4.,3.]
  model['const'] = .45
  model['status'] = 'Test Model'
  model['data ignore value'] = -999
  
  fn = 'D:\testModel.lsr'
  save, model, FILENAME=fn
  report = hubApp_LSR_viewModel_getReport(Hash('title','test','model',fn))
  report.saveHTML, /SHOW
end