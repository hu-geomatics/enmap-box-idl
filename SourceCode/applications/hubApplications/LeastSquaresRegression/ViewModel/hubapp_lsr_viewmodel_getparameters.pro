

;+
; :Description:
;    Explicit consistency check function used by `hubApp_LSR_viewModel_getParameters`.
;-
function hubApp_LSR_viewModel_getParametersCheck, resultHash, message=message
  msg = []
  model = hubApp_lsr_loadModel(resultHash['model'], ErrorMessage=ErrorMessage)
  if ~isa(model) then begin
    msg = [msg, errorMessage]
  endif
  message = msg
  return, ~isa(msg)
end


;+
; :Description:
;    This function opens a widget dialog to collect and return the models file path you
;    want to view the parameterization for.
;
; :Params:
;    settings: in, optional, type=hash
;     Use this hash to provide following basic application infos::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | this applications title 
;       ------------+--------+---------------  
;
;
;
;-
function hubApp_LSR_viewModel_getParameters, settings
  if ~isa(settings) then settings = hash()
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings.hubGetValue('title')
  hubAMW_frame ,   Title =  'Input'
    hubAMW_inputFilename,'model' $
                        , TITLE='Model' $
                        , value=settings.hubgetValue('model') $
                        , Extension='lsr' $
                        , TOOLTIP='Select a Least Squares Regression Model (*.lsr)'
                        
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_LSR_viewModel_getParametersCheck')
  if parameters['accept'] then begin
  endif
  return, parameters
end

;+
; :Hidden:
;-
pro test_hubApp_LSR_viewModel_getParameters
  model = hash()
  model['type'] = 'ULSR'
  model['inputImage'] = enmapBox_getTestImage('AF_Image')
  model['inputLabels'] = enmapBox_getTestImage('AF_LAI_Training')
  model['featureIndices'] = 1
  model['degree'] = 1
  model['nBands'] = 245
  model['coeff'] = [0.,4.,3.]
  model['const'] = .45
  model['status'] = 'Test Model'
  
  fn = 'D:\testModel.lsr'
  save, model, FILENAME=fn
  print, hubApp_LSR_viewModel_getParameters(Hash('title','test','model',fn))
end