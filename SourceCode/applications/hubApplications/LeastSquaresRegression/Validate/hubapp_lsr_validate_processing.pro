;+
; :Description:
;    This routine peforms a fast accuracy assessment for a least squares regression model.
;
; :Params:
;    parameters: in, required, type=hash
;     Hash containing the following input values::
;    
;       key            | type   | description
;       ---------------+--------+---------------
;       model          | string | filepath of LSR model
;       inputImage     | string | filepath of feature image
;       inputReference | string | filepath of reference image 
;       ---------------+--------+---------------  
;           
;    settings: in, optional, type=hash
;     Use this hash to provide following basic application infos::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | this applications title 
;       ------------+--------+---------------  
;
;
;
;
;-
function hubApp_LSR_validate_processing, parameters, settings

  if ~isa(settings) then settings = hash()
  hubApp_checkKeys, parameters, ['model','inputImage','inputReference']
  
  tempEstimation =  string(format='(%"tmp_%s_%i")' $
                  , file_basename(parameters['model'], '.*') $
                  , systime(/seconds))
  tempEstimation = FILEPATH(tempEstimation, /TMP)
  
  applyParameters = Hash()
  applyParameters += parameters.hubGetSubHash(['model','inputImage'])
  applyParameters['outputEstimation'] = tempEstimation
  noOpenState = settings.hubGetValue('noOpen')
  settings['noOpen']=1b
  hubApp_LSR_apply_processing, applyParameters, settings
  settings['noOpen'] = noOpenState
  
  accAssParameters = Hash()
  accAssParameters['inputReference']  = parameters['inputReference']
  accAssParameters['inputEstimation'] = tempEstimation
  
  ;write an error image if required
  if parameters.hubIsa('outputErrorImage') then begin
     accAssParameters['outputErrorImage'] = parameters['outputErrorImage']
  endif
  
  performance = hubApp_accuracyAssessment_processing(accAssParameters, settings)
  performance['model'] = parameters['model']
  model = hubapp_lsr_loadModel(parameters['model'])
  modelInfo = list()
  
  case model['type'] of
    'ULSR' : modelInfo.add, 'Univariate Least Squares Regression (ULSR)'
    'MLSR' : modelInfo.add, 'Multivariate Least Squares Regression (MLSR)
  endcase
  modelInfo.add, 'Input Bands      :'+strjoin(strtrim(model['featureIndices']+1,2),', ')
  modelInfo.add, 'Data Ignore Value: '+strtrim(model['data ignore value'],2)
  modelInfo.add, 'created          :'+model['created']
  performance['modelInfo'] = modelInfo.toArray()
  return, performance
end

;+
; :Hidden:
;-
pro test_hubApp_LSR_validate_processing

  p = Hash()
  p['model'] = 'D:\modelMLSR.lsr'
  p['inputImage'] = enmapBox_getTestImage('AF_Image')
  p['inputReference'] = enmapBox_getTestImage('AF_LAI_Validation')
  performance = hubApp_LSR_validate_processing(p, settings)
  
  report = hubApp_accuracyAssessment_getReport(performance, /createPlots, GroupLeader=GroupLeader)
  report.SaveHTML, /show
 
  file_delete, performance['inputEstimation']  $
             , performance['inputEstimation'] + '.hdr', /ALLOW_NONEXISTENT
             
 
end