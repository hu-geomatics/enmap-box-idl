;+
; :Description:
;   Explicit consistency check function used by `hubApp_LSR_validate_getParameters`.
;
;-
function hubApp_LSR_validate_getParametersCheck, results, message=message

  msg =[]
  hubApp_lsr_loadModel, result['model'], ErrorMessage=ErrorMessage
  msg = [msg, errorMessage]

  message = msg
  return, ~isa(msg)

end

;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    settings: in, optional, type=hash
;     Use this hash to provide the following basic application infos::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | this applications title 
;       ------------+--------+---------------  
;
;
;
;-
function hubApp_LSR_validate_getParameters, settings

  hubAMW_program, settings.hubGetValue('groupLeader'), Title= settings['title']
  hubAMW_frame, Title='Input'   
  tsize=125  
  hubAMW_inputFilename ,'model', Title='Model' $
                       , EXTENSION='lsr' $
                       , Value = settings.hubGetValue('model') $
                       , tsize=tsize
                       
  hubAMW_inputSampleSet,'inputSampleSet' $
                , Title='Image' $
                , Value = settings.hubGetValue('inputImage') $
                , ReferenceTitle = 'Reference Areas' $
                , /Regression $
                , tsize=tsize
                
  result = hubAMW_manage()
  if result['accept'] then begin 
    result['inputImage'] = (result['inputSampleSet'])['featureFilename']
    result['inputReference']  = (result['inputSampleSet'])['labelFilename']
    result.hubRemove, ['inputSampleSet']
  endif 
  
  return, result
  
end