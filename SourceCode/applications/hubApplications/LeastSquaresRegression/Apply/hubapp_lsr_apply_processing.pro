;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    This procedure applys an Least Squares Regression Model to an Image.
;
; :Params:
;    parameters: in, required, type = Hash
;     Use the following hash entries to control this routine (*=optional)::
;     
;           key (string)     | type   | description
;         -------------------+--------+----------------------------------------------------------------
;           model            | string | LSR Model (ULSR or MLSR)
;           inputImage       | string | Path of spectral image file
;           outputEstimation | string | Path of final regression estimation image
;         -------------------+--------+----------------------------------------------------------------
;
;    settings: in, optional, type = hash
;     Use this hash to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       noOpen      | bool   | set this to avoid opening the estimated image in the EnMAP-Box file list
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+---------------
;
;
;
;-
pro hubApp_LSR_apply_processing, parameters, settings
  hubApp_checkKeys, parameters, ['model','inputImage','outputEstimation']
  if ~isa(settings) then settings = hash()
  
  helper = hubHelper()
  if ~settings.hubIsa('noOpen') then settings['noOpen'] = 0b
  
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressBar( $
            title = settings.hubGetValue('title', default='Apply Least Squares Regression Model') $
          , GroupLeader = settings.hubGetValue('groupLeader') $
          , info = 'Please wait ...')
    pDone = 0l
  endif
   
  ;load the model
  model = hubApp_lsr_loadModel(parameters['model'], ErrorMessage=ErrorMessage)
  if ~isa(model) then begin
    message, errorMessage
  endif
  modelFeatures = model['featureIndices']
  modelType     = model['type']
  modelDIV      = model['data ignore value']
  modelBands    = model['nBands']
  modelConst    = model['const']
  modelCoeff    = model['coeff']
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])  
  inputImageDIV = inputImage.getMeta('data ignore value') 
  nSamples = inputImage.getMeta('samples')
  nLines   = inputImage.getMeta('lines')
  nBands   = inputImage.getMeta('bands')
  
  if isa(pBar) then begin
    pBar.setRange, [0, nLines] 
  endif
  
  if model['nBands'] ne nBands then begin
    message, 'Input image must have the same number of bands the model was trained for ('+strtrim(modelBands,2)+')'
  endif
  
  inputMask = []
  if parameters.hubIsa('inputMask') then begin
    inputMask = hubIOImgInputImage(parameters['inputImage'], /Mask)
    if ~inputImage.correctSpatialSize(inputMask.getSpatialSize()) then begin
      message, 'Input image and mask do not have same spatial size' 
    endif
  endif
  
  tileLines = hubIOImg_getTileLines(nSamples, nBands, inputImage.getMeta('data type'))
  

  ;init all readers
  hubApp_manageReaders, [inputImage], tileLines $
          , maskImages=[inputMask] $
          , /Init, /Slice $
          , SubsetBandPositions=List(modelFeatures)
  
  
  ;create and init writer
  outputEstimation = hubIOImgOutputImage(parameters['outputEstimation'] $
                     , noOpen = settings.hubKeywordSet('noOpen'))
  outputEstimation.copyMeta, inputImage, /COPYSPATIALINFORMATION
  outputEstimation.setMeta, 'data ignore value', modelDIV
  outputEstimation.setMeta, 'file type', 'EnMAP-Box Regression'
  outputEstimation.initWriter, inputImage.getWriterSettings(SetBands=1, SetDataType='float') 
        
  nPixelCount = 0ll
  
  
  while ~inputImage.tileProcessingDone() do begin
      featureData = inputImage.getData()
      nPixel = n_elements(featureData[0,*])
      if isa(inputMask) then begin
        maskData = inputMask.getData()
      endif
      mask = helper.getImageMask(featureData, 1, imageDIV=inputImageDIV, EXTERNALMASK=maskData)
      
      outputData = make_array(nPixel, /Float, value=modelDIV) 
      iValid = where(mask ne 0, nValid, /NULL)
      if isa(iValid) then begin
        ;add the model constant
        outputData[iValid] = modelConst
        
        ;add coefficients
        if modelType eq 'ULSR' then begin
          foreach c, modelCoeff, exponent do begin
            outputData[iValid] = outputData[iValid] + c * featureData[*,iValid]^(exponent+1)
          endforeach
        endif

        if modelType eq 'MLSR' then begin
          foreach c, modelCoeff, iBand do begin
            outputData[iValid] = outputData[iValid] + c * featureData[iBand,iValid]
          endforeach
        endif
      endif
      outputEstimation.writeData, outputData
      
      if isa(pBar) then begin
        pDone += tileLines
        pBar.setProgress, pDone 
      endif
  
  endwhile
  
  
  inputImage.cleanup
  outputEstimation.cleanup
  
  if isa(inputMask) then inputMask.cleanup
  if isa(pBar) then pBar.cleanup
end


;+
; :hidden:
;-
pro test_hubApp_LSR_apply_processing
  p = Hash()
  p['model'] = 'D:\model.lsr'
  p['model'] = 'D:\modelMLSR.lsr'
  p['inputImage'] = enmapBox_getTestImage('AF_Image')
  p['outputEstimation'] = 'D:\LSRModelOutput'
  hubApp_LSR_apply_processing, p
end