;+
; :Author: Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Copyright:
; 	Geomatics Lab, Humboldt-Universität zu Berlin, 2012.
;-
;+
; :Description:
;    Explicit consistency check function called by `hubApp_LSR_apply_getParameters`
;    to validate the user input.
;-
function hubApp_LSR_apply_getParametersCheck, resultHash, message=message
    msg = []
    model = hubApp_lsr_loadModel(resultHash['model'], ErrorMessage=ErrorMessage)
    if ~isa(model) then begin
      msg = [msg, errorMessage]
    endif
  message = msg
  return, ~isa(msg)
end



;+
; :Description:
;    This function opens a widget dialog to collect the parameters required to run a LSR model  
;    in `hubApp_LSR_apply`.
;    
;
; :Params:
;    settings: in, optional, type=hash
;     Use this hash to provide basic application, GUI infos or default values::
;    
;       key         | type   | description
;       ------------+--------+---------------
;       groupLeader | string | ID of Group Leader Widget
;       title       | string | application title (required) 
;       ------------+--------+---------------  
;
;
;
;-
function hubApp_LSR_apply_getParameters, settings

  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title'] 

  tsize=75
  hubAMW_frame ,   Title =  'Input'
  hubAMW_inputSampleSet,'inputSampleSet' $
                , value = settings.hubgetValue('inputImage') $
                , Title = 'Image', ReferenceTitle='Mask' $
                , /Masking, /ReferenceOptional, tsize=tsize
                
  hubAMW_inputFilename,'model'     , TITLE='Model' $
                      , value = settings.hubGetValue('model') $
                      , Extension='lsr', tsize=tsize
  
  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputFilename,'outputEstimation' $
          , Title='Estimation' $
          , value=settings.hubgetValue('outputEstimation', default='estimation') $
          , tsize=tsize
          
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_LSR_apply_getParametersCheck')
  if parameters['accept'] then begin
    parameters['inputImage'] = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputMask']  = (parameters['inputSampleSet'])['labelFilename']
    parameters.hubRemove, ['inputSampleSet']
  endif
  return, parameters
end