;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('hubApp_scatterPlot.conf', ROOT=hubApp_scatterPlot_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, hubApp_scatterPlot_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function hubApp_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('hubApplications.conf', ROOT=hubApp_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  settings['tileLines'] = fix(settings['tileLines'])
  settings['textLines'] = fix(settings['textLines'])
  return, settings

end

;+
; :Hidden:
;-
pro test_hubApp_getSettings

  print, hubApp_getSettings()

end
