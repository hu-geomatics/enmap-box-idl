;+
; :Description:
;    Use this procedure to create a mask out of an image. 
;    All pixels with profiles completely consisting of data ignore values are set to 0. 
;    All other pixels are set to 1.
;     
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        keys               | type     | description
;        -------------------+----------+--------------------------------------------------
;        inputFilename      | string   | file path of the input image
;        outputFilename     | string   | file path of the output image
;        -------------------+----------+--------------------------------------------------
;        
;        optional keys      | type     | description
;        -------------------+----------+--------------------------------------------------
;        dataIgnoreValue    | number   | Value defining the mask background. 
;                           |          | Default is the data ignore value of the image image
;        noOpen             | boolean  | Set this keyword to not open the output image inside the EnMAP-Box or ENVI
;        -------------------+----------+-------------------------------------------------- 
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro hubApp_extractMask_processing, parameters, Title=title, GroupLeader=groupLeader

  required = ['inputFilename','outputFilename']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
 
  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader)
  progressBar.setInfo, 'create mask'

  inputImage = hubIOImgInputImage(parameters['inputFilename'])
  outputImage = hubIOImgOutputImage(parameters['outputFilename'], NoOpen=parameters.getValue('noOpen'))
  numberOfTileLines = hubIOImg_getTileLines(image=inputImage)
  inputImage.initReader, numberOfTileLines, /Cube, /TileProcessing, /Mask
  writerSettings = inputImage.getWriterSettings(SetBands=1, SetDataType='byte')
  outputImage.initWriter, writerSettings
  while ~inputImage.tileProcessingDone(Progress=progress) do begin
    progressBar.setProgress, progress
    cubeTile = inputImage.getData()
    maskTile = product(cubeTile, 3)
    outputImage.writeData, maskTile
  endwhile
  inputImage.cleanup
  outputImage.cleanup
  progressBar.cleanup

end

pro test_hubApp_extractMask_processing
  
  parameters = hash()
  parameters['inputFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
  parameters['outputFilename'] = filepath('mask', /TMP)
  hubApp_extractMask_processing, parameters

end