;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;          Benjamin Jakimow (benjamin.jakimow@geo.hu-berlin.de)
; :Private:
;-

pro hubApp_make, distribution=distribution

  codeDir = hubApp_getDirname(/SourceCode)
  appDir = hubApp_getDirname()
  enmapBoxMake_copyDefaultDirs, codeDir, appDir
  
  if ~enmapboxmake.CopyOnly() then begin
    SAVFile = filepath('hubApplications.sav', ROOT=hubApp_getDirname(), SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, nolog=enmapboxmake.nolog()
    
    if ~enmapboxmake.NoIDLDoc() then begin
      docDir = filepath('', ROOT_DIR=hubApp_getDirname(), SUBDIR=['help','idldoc'])
      title='hubApplications Documentation'
      hub_idlDoc, codeDir, docDir, title, NOSHOW=enmapboxmake.noshow()
    endif
    
    if isa(distribution) then begin
    
      if ~isa(distribution, 'string') then begin
        title = 'Select a directory for writing the distribution.'
        directory = dialog_pickfile(/DIRECTORY, /WRITE, PATH=filepath('',/TMP),Title=title)
        if directory eq '' then return
      endif else begin
        directory = distribution
      endelse
      
      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'enmapProject','applications'])
      file_copy, appDir, destination, /OVERWRITE, /RECURSIVE
      destination = filepath(file_basename(appdir), ROOT_DIR=directory, SUBDIRECTORY=[file_basename(appdir)+'Distribution', 'EnMAP-Box', 'SourceCode','applications'])
      file_copy, codeDir, destination, /OVERWRITE, /RECURSIVE
      
    endif
  endif
end