function hubApp_dataExport_imgToGDAL_getParameters, settings
  if ~isa(settings) then begin
    settings = hash()
    settings['title'] = 'Export Image'
  endif
  
  hubAMW_program , settings.hubgetValue('groupLeader') $
                 , Title = settings.hubGetValue('title')
  hubpython.initPythonBridge,/raise_errors
  raster_formats = hubGDALTools.raster_formats()
  tsize= 100;
  
  hubAMW_inputImageFilename, 'inputImage' $
            , Title='Input Image', value = settings.hubGetValue('inputImage') $
            , TSIZE=tsize
  
  w = max(strlen((raster_formats.keys()).toArray()))
  lnames = list()
  drivers = list()
  foreach k, raster_formats.keys() do begin
    t = raster_formats[k]
    drivers.add, k
    lnames.add, string(format='(%"%s %s")', k, t[1])
  endforeach
  hubAMW_combobox, 'drvDst' $
        , title=  'Output Format', list=lnames.toArray(), TSIZE=tsize
  hubAMW_outputFilename,'outputImage' $
        , Title = 'Output Image  ' $
        , value = settings.hubGetValue('outputImage', default='TIFFImport') $
        , TSIZE=tsize
  results = hubAMW_manage()
  if results['accept'] then begin
    results['drvDst'] = drivers[results['drvDst']]
    
  endif
  
  return, results

end

pro test_hubApp_dataExport_imgToGDAL_getParameters
  print, hubApp_dataExport_imgToGDAL_getParameters()
end