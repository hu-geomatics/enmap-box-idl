;+
; :Description:
;    Routine to export an ENVI style image as GeoTIFF file.
;
; :Params:
;    parameters: in, required, type = hash
;     Hash containing the following values::
;       
;         key         | type   | description
;       --------------+--------+----------------
;         inputImage  | string | file path
;         outputTIFF  | string | file path
;       * compression | int    | 0 - none
;                     |        | 1 - LZW
;                     |        | 2 - PackBits
;                     |        | 3 - JPEG
;       --------------+--------+----------------
;       
;    settings: in, optional, type = hash
;
;
;
;-
pro hubApp_dataExport_ImgToTiff_processing, parameters, settings
  
  if ~isa(settings) then settings = hash()
  
  hubApp_checkKeys, parameters, ['inputImage', 'outputTIFF' ]
  compression = parameters.hubGetValue('compression', default=0)
  if compression lt 0 or compression gt 3 then message, "'compression' must be 0 (none),1 (LZW), 2 (PackBits) or 3 (JPEG)"
  
  if ~settings.hubKeywordSet('NoShow') then begin
    pBar = hubProgressBar(title='GeoTIFF Export', Info='Read Data...')
  endif
  inputImage = parameters['inputImage']
  img = hubIOImgInputImage(inputImage)
  dataTypeName = typename(img.getMeta('data type'))
  
  nSamples = img.getMeta('samples') 
  nLines = img.getMeta('lines')
  nBands = img.getMeta('bands')
  
  
  geoTags = !NULL
  if img.hasMeta('map info') then begin
    MI = img.getMeta('map info')
    
      values = hash()
      values['ModelPixelScaleTag'] = [MI.sizeX, MI.sizeY, 0]
      values['ModelTiePointTag']   = [MI.pixelX $
                                    , MI.pixelY $
                                    , 0. $
                                    , MI.Easting $
                                    , MI.Northing $
                                    , 0. $
                                    ]
      values['GTRasterTypeGeoKey'] = 1 ;Raster Pixel is Area
      ;set GTModelTypeGeoKey
      ;1 = ModelTypeProjected
      ;2 = ModelTypeGeographic
      ;3 = ModelTypeGeocentric (not supported by map info)
      
   
      unit = strtrim(STRLOWCASE(MI.units),2)
      
      if STREGEX(MI.units, '(meter|kilometer|centimeter|yard|link|mile|foot|km)', /FOLD_CASE, /BOOLEAN) then begin
        values['GTModelTypeGeoKey'] = 1
        
        ;TODO ask user for detailed specification?
      endif 
      
      if STREGEX(MI.units, '(radian|degree|minute|second|grad|gon|dms)', /FOLD_CASE, /BOOLEAN) then begin
        values['GTModelTypeGeoKey'] = 2
        
      
      endif
      geoTags = values.toStruct()
    
  endif
  
  img.initReader, /CUBE
  data = img.getData() 
  
  if isa(pBar) then pBar.setInfo, 'Write as GeoTIFF...'
  
  description = img.getMeta('description')
  if isa(description) then description = strjoin(description, string(13b)) 
  write_tiff, parameters['outputTIFF'] $
            , data $
            , geotiff = geoTags $
            , planarconfig = 2 $ ;because [smaples, lines, bands]
            , SIGNED = total(dataTypeName eq ['UINT','ULONG', 'ULONG64']) eq 0 $ 
            , L64 = total(dataTypeName eq ['LONG64','ULONG64']) ne 0 $
            , SHORT = dataTypeName eq 'INT' $
            , LONG = dataTypeName eq 'LONG' $
            , FLOAT = dataTypeName eq 'FLOAT' $
            , DOUBLE = dataTypeName eq 'DOUBLE' $
            , COMPLEX = dataTypeName eq 'COMPLEX' $
            , DCOMPLEX = dataTypeName eq 'DCOMPLEX' $
            , compression=compression $
            , description=description $
            , document_Name = string(format='(%"GeoTIFF exported from %s")', inputImage) 
  img.cleanup
  
  if isa(pBar) then pBar.cleanup
end


;+
; :Hidden:
;-
pro test_hubApp_dataImport_ImgToTiff_processing
  inputImage = enmapBox_getTestImage('AF_Image')
  outPath = 'D:\temp\AF_ImageTIFF.tif'
  parameters = hash('inputImage',inputImage, 'outputTIFF',outputTIFF)
  hubApp_dataExport_ImgToTiff_processing, parameters
end