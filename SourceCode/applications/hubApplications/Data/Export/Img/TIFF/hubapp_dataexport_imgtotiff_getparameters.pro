function hubApp_dataExport_imgToTiff_getParameters, settings
  hubAMW_program , settings.hubgetValue('groupLeader') $
                 , Title = settings.hubGetValue('title')
  tsize= 90;
  hubAMW_inputImageFilename, 'inputImage' $
            ,   Title='Input Image', value = settings.hubGetValue('inputTIFF') $
            , TSIZE=tsize
                             
  hubAMW_outputFilename,'outputTIFF' $
        , Title = 'Output TIFF' $
        , EXTENSION='tif' $
        , value = settings.hubGetValue('outputImage', default='TIFFImport') $
        , TSIZE=tsize
  return, hubAMW_manage()

end