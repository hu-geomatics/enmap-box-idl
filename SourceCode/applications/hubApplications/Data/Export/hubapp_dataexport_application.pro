;+
; :Author: Benjamin Jakimow (benjamin.jakimow[at]geo.hu-berlin.de)
;-

;+
; :Description:
;   This routine controls the program flow of all import routines.
; :Params:
;    applicationInfo : in, optional, type=hash
;       The application info resulting from `enmapBoxUserApp_getApplicationInfo`.
;       Contains at least the key `exportType`, which can have the following values::
;       
;         'imgTIFF'      -> Export image as GeoTIFF file.;       
;       
;       Furthermore this hash can contain default values for::
;       
;         key         | type   | description
;         ------------+--------+---------------
;         outputImage | string | default path for output Images
;         ------------+--------+---------------
;       
;
;-
pro hubApp_dataExport_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  
  ; save application info to settings hash
  settings += applicationInfo
  
  ;generate default path values in case they do not exist
  if ~settings.hubIsa('outputImage') then settings['outputImage'] = filepath('image',/TMP)
  if ~settings.hubIsa('outputSL') then settings['outputSL'] = filepath('specLib',/TMP) 
  
  exportType = settings['exportType']
  ;1. define dialog title for the export filter
  case exportType of
    'imgTIFF'      : settings['title'] = 'Export Image to GeoTIFF'
    'imgGDAL'      : settings['title'] = 'Export Image'
    else: message, 'Not implemented exportType of ' + exportType 
  endcase
  
  ;2. Collect required parameters
  case exportType of
    'imgTIFF'  : parameters = hubApp_dataExport_imgToTiff_getParameters(settings)
    'imgGDAL'  : parameters = hubApp_dataExport_imgToGDAL_getParameters(settings) 
  endcase
   
  ;3. Run the export filter's processing routine
  if parameters['accept'] then begin
    case exportType of
      'imgGDAL' : hubGDALTools.gdal_translate, parameters['inputImage'], parameters['outputImage'], parameters['drvDst']
      'imgTIFF' : hubApp_dataExport_ImgToTiff_processing, parameters, settings 
    endcase
  endif
end

;+
; :Hidden:
;-
pro test_hubApp_dataExport_application
  appInfo = Hash()
  
  exportType = 'imgTIFF'
  
  importType = 'slASCII_CSV'
  appInfo['outputSL'] = 'D:\Sandbox\ASCII_Data\testSLCSV_out'
  appInfo['inputTextFile']         = 'D:\EnMAP-Box\SourceCode\enmapBox\_resource\testData\speclib\ESL_Ascii.txt'
  
  appInfo['importType'] = importType
  appInfo['title'] = 'Import filter TestAPP: '+importType['importType']
  hubApp_dataImport_application, appInfo

end  
