;+
; :Description:
;    This function reads a text file and tries to collect information about its
;    possible ASCII-Table or ASCII-Grid structure.
;
; :Params:
;    dialogSettings: in, required, type=hash
;     This hash contains general information that are required to interprete the ascii textfile::
;        
;        -----------------+---------+-----------------------------------------
;             key         | type    | description
;        -----------------+---------+-----------------------------------------
;          importType     | string  | type of ascii file that is to import. possible value are
;                         |         | 'imgASCIIGrid', 'imgASCIIList'
;        delimiters       | string[]| list of possible delimiter strings
;        delimiterIsRegex | bool[]  | is the delimiter in 'delimiters' a regular expression?
;        -----------------+---------+-----------------------------------------
;
;    textFile: in, required, type=file path
;     Path of ASCII file to read.
;
; :Returns: 
;    This function returns a hash that /might/ contain proposed values for following 
;    the ENVI meta tag values: `samples, lines, bands, map info, file type, data type, map info, classes, class names, class lookup`
;    as well as the following keys::
;    
;        ------------------+----------+-----------------------------------------
;                 key      | type     | description
;        ------------------+----------+-----------------------------------------
;        hasColumnHeaders  | bool     | 
;        firstDataLine     | int      | index of first data line (including row with column header values) 
;        fileTypeIndex     | int      | supposed values can be: 
;                          |          | 0 = ENVI Standard, 1 = ENVI Classification, 2 = EnMAP-Box Regression
;        dataTypeIndex     |          | index of supposed data type in dialogSettings['dataTypeCodes'] or dialogSettings['dataTypeNames']  
;        delimiterIndex    | int      | index of supposed delimiter in dialogSettings['delimiters']
;        previewLines      | string[] | the first n-text lines with n=firstDataLine + 25
;        ------------------+----------+------------------------------------------
;        
;-
function hubApp_dataImport_fromASCII_getSuggestions, dialogSettings, textFile
  helper = hubHelper()
  suggest = Hash()
  
  ;1. try to find the first data line
  regfirstDataLine = '^[ '+string(9b)+']*[+-]?[0-9]+'
  openr, lun, textFile, /Get_LUN
  firstDataLine = 0
  openBrackets = 0
  while not eof(lun) do begin
    firstDataLine++
    line = ''
    readf, lun, line
    opened = n_elements(strsplit(line,'{',/PRESERVE_NULL))-1
    closed = n_elements(strsplit(line,'}',/PRESERVE_NULL))-1
    openBrackets += opened
    openBrackets -= closed
    if openBrackets eq 0 && $
      stregex(line, regFirstDataLine, /Boolean) then begin
      break
    endif
  endwhile
  free_lun, lun
  lastHeaderLine = firstDataLine - 1 
  suggest['firstDataLine'] = firstDataLine
    
  ;Use different parsing methods to get explicitely defined meta values
  metas = list()
  metas += hubIOASCIIHeader_parseArcGISHeader(textFile, lines = lastHeaderLine, /TOLERANT)
  metas += hubIOASCIIHeader_parseENVIASCIIHeader(textFile, lines=lastHeaderLine)
  metas += hubIOImgHeader_parseHeader(textFile, lines=lastHeaderLine, /Tolerant)
  foreach meta, metas do begin
    suggest[meta.getName()] = meta.getValue()
  endforeach
 
  reader = hubIOASCIIInput(textFile)
  previewLines = strmid(reader.getData(firstDataLine + 1000), 0, 200)
  suggest['previewLines'] = reform(string(format='(%"%2i:")',indgen(n_elements(previewLines))+1) + previewLines, 1, n_elements(previewLines))
  reader.cleanup
  
  suggest['delimiterIndex'] = 0
  if n_elements(previewLines) gt firstDataLine then begin
    exampleRow = strtrim(previewLines[firstDataLine],2)
    ;get suggestion for best matching delimiter
    matched = Hash()
    nMaxMatches = 0
    delimiters = dialogSettings['delimiters']
    foreach delimiter, delimiters, i do begin
      isRegex = (dialogSettings['delimiterIsRegex'])[i]
      ;replace 
      
      nMatches  = n_elements(strsplit(exampleRow, delimiter, regex=isRegex, /PRESERVE_NULL)) -1 
      if nMatches gt 0 && nMatches gt nMaxMatches then begin
        nMaxMatches = nMatches
        suggest['delimiterIndex'] = i
      endif
    endforeach  
  endif
  
  
;  ;extract implicitely defined meta values
;  ;Read first data lines + header row (if existent)
;  reader = hubIOASCIIInputTable(textFile)
;  iDelimiter = suggest['delimiterIndex']
;  reader.initReader, 2 $ ;get 2 data lines
;        , numberOfColumns = -1 $ ;try to extract the column values
;        , firstDataLine = firstDataLine-1 $ ;get a possible header line too
;        , delimiter = (dialogSettings['delimiters'])[iDelimiter] $
;        , isRegex   = (dialogSettings['delimiterIsRegex'])[iDelimiter]
;  exampleRows = reader.getData()
;  reader.cleanup
;  
;  if n_elements(exampleRows) gt 2 then begin 
;    exampleRow = exampleRows[*,1]
;    suggest['nColumns'] = n_elements(exampleRow)
;    for i=0, n_elements(exampleRows[*,0])-1 do begin
;      if helper.canConvert(exampleRows[i,0], 'Double') ne $
;         helper.canConvert(exampleRows[i,1], 'Double') then begin
;         suggest['columnHeaders'] = exampleRows[*,0]
;         break
;      endif else begin
;         suggest['columnHeaders'] = exampleRows[*,1]
;      endelse
;    endfor
;  endif
  
;  iWLColumn = where(strcmp(suggest['columnHeaders'], 'wavelength', /Fold_case) eq 1b, /NULL)
;  if isa(iWLColumn) then begin
;    suggest['iWavelength'] = iWLColumn
;  endif
  
  
  ;implicit values
  ;if not set, derive samples from number of columns 
  if dialogSettings['importType'] eq 'imgASCIIGrid' then begin
    if ~suggest.hubIsa('samples') then begin
      suggest['samples'] = hubIOImgMeta_createFromEnviString('samples', n_elements(exampleRow))
    endif  
  endif  
  if dialogSettings['importType'] eq 'asciiListGeo' or $
     dialogSettings['importType'] eq 'asciiListPixel' then begin
    if ~suggest.hubIsa('bands') then begin
      suggest['bands'] = hubIOImgMeta_createFromEnviString('bands', n_elements(exampleRow)-2 > 1)
    endif
  endif
  

  return, suggest
end

;+
; :Hidden:
;-
pro test_hubApp_dataImport_fromASCII_getSuggestions
  dialogSettings = hash()
  dialogSettings['delimiters']     = hubApp_getASCIIdelimiters() 
  dialogSettings['delimiterIsRegex'] = hubApp_getASCIIdelimiters(/DelimiterIsRegex)
  dialogSettings['delimiterNames'] = hubApp_getASCIIdelimiters( /DelimiterDescriptions)
  
  helper = hubHelper()
  
  dialogSettings['fileTypes']     = ['ENVI Standard','ENVI Classification','EnMAP-Box Regression']
  dialogSettings['fileTypeDescriptions'] = ['ENVI Standard (multiple bands)','ENVI Classification (1 band, value 0 = unclassified)','EnMAP-Box Regression (1 band, data ignore value)']
  dialogSettings['fileTypeOneBandOnly'] = [0b, 1b, 1b]
  
  dialogSettings['coordinateTypes'] = ['pixel', 'geo']
  dialogSettings['coordinateTypeDescriptions'] = ['Pixel Coordinates (map info not requiured)' $
                                                 ,'Geo Coordinates (requires map info)']
  
  dialogSettings['importType'] = 'imgASCIIGrid'
  dialogSettings['importType'] = 'imgASCIIList'
  
  textFile = 'D:\Sandbox\ASCII_Data\fromarcgis.txt'
  ;textFile = 'D:\Sandbox\ASCII_Data\ASD_Ascii.txt'
  ;textFile = 'D:\Sandbox\ASCII_Data\ESL_Ascii.txt'
  ;textFile = 'D:\Sandbox\ASCII_Data\Hymap_Berlin-A_Image.txt'
  ;textFile = 'D:\Sandbox\ASCII_Data\Measurements.txt'
  print, hubApp_dataImport_fromASCII_getSuggestions(dialogSettings, textFile)
end
