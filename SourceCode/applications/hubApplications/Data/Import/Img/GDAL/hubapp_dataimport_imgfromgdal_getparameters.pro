function hubApp_dataImport_imgFromGDAL_getParameters, settings
  if ~isa(settings) then settings = Hash('title','Import GDAL readable raster')
  
  hubpython.initPythonBridge,/raise_errors
  
  hubAMW_program , settings.hubgetValue('groupLeader'), Title = settings.hubGetValue('title')
  hubAMW_inputFilename, 'inputImage' , Title='Input Raster ', value = settings.hubGetValue('inputRaster')
  hubAMW_outputFilename,'outputImage', Title='Output Raster', EXTENSION='img', value = settings.hubGetValue('outputImage', default='imported.bsq')
  
  results = hubAMW_manage()
  if results['accept'] eq 1b then begin
    results['drvDst'] = 'ENVI'
  endif
  
  return, results

end