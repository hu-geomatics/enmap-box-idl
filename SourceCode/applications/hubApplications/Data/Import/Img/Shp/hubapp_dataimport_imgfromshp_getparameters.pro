function hubApp_dataImport_ImgFromShp_getParameters_Part2, parameters1, settings
  groupLeader = settings.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title']
  ;read shape properties
  shapeReader = hubIOShapeFileReader(parameters1['inputShape'])
  columnNames = shapeReader.getAttributeNames()
  columnTypeNames = STRUPCASE(shapeReader.getAttributeTypes(/TypeNames))
  columnDescriptions = columnNames + ' ['+columnTypeNames+']'
  iNumericTypes = where(columnTypeNames ne 'STRING', /NULL)
  
  
  shapeReader.cleanup
  ;read ref image properties
  
  fileType = parameters1['file type']
  useMultiple = fileType eq 'ENVI Standard'
  isClassification = fileType eq 'ENVI Classification' 
  case fileType of 
    'ENVI Standard'        : begin
            description = ['Select shapefile columns with numeric values' $
                          ,'Each column is rasterized into a new image band']  
            columnNames = columnNames[iNumericTypes]
            columnDescriptions = columnDescriptions[iNumericTypes]
                             end
    'ENVI Classification'  : begin
            description = 'Select shapefile column with classification labels (numeric/string)'
                             end
    else : message,'unhandled file type: '+fileType
  endcase

  hubAMW_frame,   Title =  'Parameters'
    hubAMW_label, description
    hubAMW_list, 'labelColumns', list=columnDescriptions, EXTRACT=columnNames $
        , XSIZE=150, Ysize=200 $
        , MULTIPLESELECTION = useMultiple
  
  hubAMW_frame, Title = 'Output'
    hubAMW_label, 'File Type: '+fileType
    if ~isClassification then begin
       hubAMW_parameter, 'data ignore value', Title='Data ignore value' $
                    , /FLOAT, OPTIONAL=1, value=-999
    endif
   
    hubAMW_outputFilename, 'outputImage', Title='Image' $
                         , value=parameters1.hubGetValue('outputImage',  default='imageFromShp')
  parameters = hubAMW_manage()

  
  if parameters['accept'] then begin
    ; if required, perform some additional changes on the parameters hash
    
  endif
  return, parameters

end

function hubApp_dataImport_ImgFromShp_getParameters_BasicCheck, result, message=message
  msg = !NULL
  message = !NULL
  shapeReader = hubIOShapeFileReader(result['inputShape'], /Test)
  refImage = hubIOImgInputImage(result['inputReference'])
  geoHelper = hubGeoHelper(refImage)
  
  if ~isa(shapeReader) then begin
    msg = [msg, string(format='(%"File ''%s'' is not a valid shape file.")' $
                , FILE_BASENAME(result['inputShape']))]
  endif else begin
    if total(shapeReader.getShapeType() ne ['POINT','POLYGON','POLYLINE']) eq 0 then begin
      msg = [msg, 'Shapefile must contain POINT, POLYGON or POLYLINE data.']
    endif 
    
    if shapeReader.getNumberOfEntities() eq 0 then begin
      msg = [msg, 'Shapefile is empty.']
    endif else begin
    
    ;TODO: check if numeric columns are available
    if ~STREGEX(result['file type'], 'classification', /BOOLEAN, /FOLD_CASE) then begin
      columnTypeNames = STRUPCASE(shapeReader.getAttributeTypes(/TypeNames))
      if ~total(columnTypeNames ne 'STRING') then begin
        msg = [msg, 'Shapefile does not contain any column with numeric data type.']
      endif
    endif
      
      bb1 = geoHelper.getImageBoundaries()
      bb2 = shapeReader.getBoundingBox()
      
      if ~geoHelper.isBoundingBoxIntersection(bb1,bb2) then begin
        msg = [msg, 'No spatial intersection between shapefile and image. Do they use the same spatial reference system?']
      endif
      
    endelse
    shapeReader.cleanup
  endelse
  refImage.cleanup
  
  if isa(msg) then begin
    message = '- '+msg+string(13b)
  endif
  
  return, ~isa(msg)
end

function hubApp_dataImport_ImgFromShp_getParameters_Basic, defaultParameters, settings
  groupLeader = defaultParameters.hubGetValue('groupLeader')
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_frame ,   Title =  'Input'
  tsize=120
  hubAMW_inputFilename, 'inputShape', title='Shapefile' $
                      , EXTENSION='shp' $
                      , tsize=tsize $
                      , value = defaultParameters.hubgetValue('inputShape') 
  
  
  hubAMW_inputImageFilename, 'inputReference', title='Reference Image' , tsize=tsize $
                           , tooltip = 'Image with same spatial reference system as the shapefile.' $
                           , value = defaultParameters.hubgetValue('inputReference')
  
  hubAMW_frame , Title = 'Parameters'
  fileTypes = ['ENVI Standard','ENVI Classification']
  if  defaultParameters.hubIsa('file type') then begin
    value = where(fileTypes eq defaultParameters['file type'], /NULL) 
  endif
  
  hubAMW_combobox, 'file type' $
            , Title = 'Rasterize as' $
            , list=filetypes, extract=filetypes $
            , value = value
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_dataImport_ImgFromShp_getParameters_BasicCheck')

  
  if parameters['accept'] then begin
    ; if required, perform some additional changes on the parameters hash
    
  endif
  return, parameters

end

function hubApp_dataImport_ImgFromShp_getParameters, defaultParameters, settings
  if ~isa(defaultParameters) then defaultParameters = Hash()
  if ~isa(settings) then settings = Hash('title','Rasterize Shapefile')
  CANCEL = Hash('accept',0b)
  
  parameters = defaultParameters + hubApp_dataImport_ImgFromShp_getParameters_Basic(defaultParameters, settings) 
  if ~parameters['accept'] then return, CANCEL
  
  parameters += hubApp_dataImport_ImgFromShp_getParameters_Part2(parameters, settings)
  if ~parameters['accept'] then return, CANCEL
  
  return, parameters

end

pro test_hubApp_dataImport_ImgFromShp_getParameters
  pathSHP = '/Users/benjaminjakimow/Documents/SVNCheckouts/Berlin-II/testInput/testROI.shp'
  pathSHP = 'D:\Berlin_II\Berlin-II-IDLCode\output\testShapeFromASCII_NEW.shp'
  pathREF = 'D:\Berlin_II\Daten\Analysis\RandomSampling\HyMap01_Scaled_Subset1.bsq'
  defaultValues = hash()
  defaultValues['inputReference'] = pathREF
  defaultValues['inputShape'] = pathSHP 
  parameters = hubApp_dataImport_ImgFromShp_getParameters(defaultValues)
  print, parameters
end