
;+
; :Description:
;    This routine imports shape file data and writes it into raster file.
;
; :Params:
;    parameters: in, required, type=hash
;    settings:in, optional, type=hash
;
;
;
; :Author: geo_beja
;-
function hubApp_dataImport_imgFromShp_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputShape','inputReference' $
                                , 'file type', 'outputImage', 'labelColumns']
  if ~isa(settings) then settings = Hash('noShow',1b)
  
  fileType = parameters['file type']
  isClassification = stregex(fileType,'classification', /Fold_case, /Boolean)
 
  ;read ShapeFile
  shpReader= hubIOShapeFileReader(parameters['inputShape'])
  shapeType = shpReader.getShapeType()
  if total(shapeType eq ['POINT','POLYGON','POLYLINE']) eq 0 then message, 'Shape file with POINT or POLYGON data required'
  attrTypes = shpReader.getAttributeTypes(/Typecodes)
  attrNames = shpReader.getAttributeNames()
  
  entitiesPerStep = 200
  nEntities = shpReader.getNumberOfEntities()
  if nEntities eq 0 then message, 'Shape file is empty'
  
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressBar(/Cancel $
                , Title=settings.hubGetValue('title', default='Shape To Classification Image') $
                , Range = [0, nEntities] $
                , Info='Read Shape file...')
  endif
  
  iLabelColumns = !NULL
  foreach labelColumn, parameters['labelColumns'] do begin
    iColumn = where(attrNames eq labelColumn, /NULL)
    if ~isa(iColumn) then message, string(format='(%"Shapefile ''%s'' has no column ''%s''")' $
                                 , FILE_BASENAME(parameters['inputShape']), labelColumn)  
    iLabelColumns = [iLabelColumns, iColumn]  
  endforeach
  
  nLabelColumns = n_elements(iLabelColumns)
  if isClassification && nLabelColumns ne 1 then begin
     message, 'more than one label column defined for file type :'+ parameters['file type']
  endif
  

  inputRef = hubIOImgInputImage(parameters['inputReference'])
  geoHelper = hubGeoHelper(inputRef)
  nSamples = inputRef.getMeta('samples')
  nLines   = inputRef.getMeta('lines')  
  mapInfo = inputRef.getMeta('map info')
  if ~isa(mapInfo) then message, 'map info does not exists'
  
  outputImage = hubIOImgOutputImage(parameters['outputImage'],NOOPEN=settings.hubKeywordSet('noOpen') )
  outputImage.copyMeta, inputref, /COPYSPATIALINFORMATION
  outputImage.setMeta, 'lines', nLines
  outputImage.setMeta, 'samples', nSamples
  outputImage.setMeta, 'bands', nLabelcolumns
  outputImage.setMeta, 'band names', attrNames[iLabelColumns]
  
  outputImage.setMeta, 'file type', filetype
  if isClassification then begin 
    ;lookup table for class values 
    outputImage.setMeta, 'file type', 'ENVI Classification'
    attributes = shpReader.getAttributeData()
    classNames = list()
    foreach i, iLabelColumns do begin
      classNames.add, strtrim(attributes[*].(i),2), /Extract
    endforeach
    classNames = strtrim(classNames.toArray(),2)
    classNames = hubMathHelper.getSet(classNames)
    
    classNames = classNames[where(classNames ne '', /NULL)]
    if isa(classNames) then begin
      classNames = classNames[sort(classNames)]
      classNames = ['unclassified', classNames]
    endif else begin
      print, 'No other class values found than empty fields'
      classNames = ['unclassified']
    endelse
    
    
    classNamesLUT = hash('',0)
    foreach className, classNames, i do classNamesLUT[className] = i
    
    nClasses = n_elements(classNames)
    case 1b of
      nClasses le 256ul: dataType = 1
      nClasses le 32767ul: dataType = 2 ;int
      nClasses le 65535ul: dataType = 12 ;Uint
      nClasses le 2147483647ul : dataType = 3 ;long
      else: dataType = 15 ;this is a very unlikely case
    endcase
    outputImage.setMeta, 'data type', dataType
    outputImage.setMeta, 'classes', nClasses
    outputImage.setMeta, 'class names', classNames
    classLookup = bytscl(randomu(systime(1), 3, nClasses))
    classLookup[*,0]= 0
    outputImage.setMeta, 'class lookup', classLookup
    
  endif else begin
    outputImage.setMeta, 'file type', 'ENVI Standard'
    
    ;get data type that fits best 
    attrTypes = attrTypes[ilabelcolumns]
    if total(attrTypes eq 7) then message, 'can not convert string columns to numeric data typ'
    if isa(where(attrTypes eq 4 or attrTypes eq 5, /NULL)) then begin
      dataType = 5 ;all as double
    endif else begin
      dataType = max(attrTypes)
    endelse
    outputImage.setMeta, 'data type', dataType
    
    if parameters.hubIsa('data ignore value') then begin
      outputImage.setMeta,'data ignore value', parameters['data ignore value']
    endif
  endelse
  
  
  
  ;init image reader and writers
  inputRef.initReader, /Profiles
  writerSettings = inputRef.getWriterSettings(SetDataType=dataType, SETBANDS=nLabelColumns)
  writerSettings['tileProcessing'] = 0b
  writerSettings['dataFormat'] = 'profiles'
  outputImage.initWriter, writerSettings
  shpReader.initReader, entitiesPerStep 
  
  
  
  handledShapes = 0ll
  writtenPixels = 0ll
  while ~shpReader.readingDone() do begin
     shpData = shpReader.getData()
     
     ;lists to store entity information
     entityInfoList = list()
     
     for iEntity = 0, shpData.numberOfEntities - 1 do begin 
        ;if iEntity eq 22 then stop
        rejectEntity = 0b
        ;label data
        labelVector = make_array(nLabelColumns, TYPE=dataType)

        foreach iLabelColumn, iLabelColumns, i do begin
          attributeValue = shpData.attributes[iEntity].(iLabelColumn)
          if isClassification then begin
            className = strtrim(attributeValue,2)
            className = className.replace('[.,]','_')
            rejectEntity = className eq '' 
            labelVector[i] = classNamesLUT[strtrim(className,2)]
          endif else begin
            labelVector[i] = attributeValue
          endelse  
        endforeach
        
        if rejectEntity then continue
        
        ;geometry
        geometries = (shpData.geometries)[iEntity]
        foreach geom, geometries do begin
          pixelIndices = !NULL
          case geom.type of
            'point'   : begin
                          if geoHelper.imageContains(geom) then begin
                              pixelCoord = geoHelper.convertCoordinate2PixelIndex(geom)
                              pixelIndices = pixelCoord.pixelIndices
                              if n_elements(pixelIndices) gt 1 then stop
                          endif
                        end
            'polyline'   : begin
                          foreach part, geom.parts do begin
                            if geoHelper.imageContains(part) then begin
                              pixelCoord = geoHelper.convertCoordinate2PixelIndex(part)
                              pxX = pixelCoord.x[*]
                              pxY = pixelCoord.y[*]
                              ;temp = polyfillv(pixelCoord.x, pixelCoord.y, nSamples, nLines)
                              pxO = 1.0
                              temp = polyfillv([pxX-pxO, REVERSE(pxX)+pxO], [pxY-pxO, reverse(pxY)+pxO], nSamples, nLines)
                              if temp[0] eq -1 then begin
                                ;vertex coordinate too close to enclose other pixel
                                ;add as single pixels?
                                ;print, 'Polygon too small'
                                temp = !NULL
                              endif
                              pixelIndices = [pixelIndices, temp]
                            endif
                          endforeach
              
                        end
            'polygon' : begin
                          foreach part, geom.parts do begin
                            if geoHelper.imageContains(part) then begin
                              pixelCoord = geoHelper.convertCoordinate2PixelIndex(part)
                              temp = polyfillv(pixelCoord.x, pixelCoord.y, nSamples, nLines)
                              if temp[0] eq -1 then begin
                                ;vertex coordinate too close to enclose other pixel
                                ;add as single pixels?
                                ;print, 'Polygon too small'
                                temp = !NULL
                              endif 
                              pixelIndices = [pixelIndices, temp]
                            endif
                          endforeach
                        end
             else : message, 'Unknown type: '+geom.type
          endcase
          
          if isa(pixelindices) then begin
            entityInfoList.add, {pixelIndices:pixelIndices, labels:labelVector}
          endif
       endforeach
      endfor
      
      ;write the data tile-wise
      if n_elements(entityInfoList) gt 0 then begin
        nPixels = 0l
        foreach entityInfo, entityInfoList do begin
          nPixels += n_elements(entityInfo.pixelIndices)
        endforeach
         
        pixelData = make_array(nLabelColumns, nPixels, Type=dataType)
        pixelPositions = make_array(nPixels, /LONG)
        iStart = 0l
        foreach entityInfo, entityInfoList do begin
          nEntityPixels = n_elements(entityInfo.pixelIndices) 
          iEnd = iStart + nEntityPixels - 1
          for iLabelColumn=0, nLabelColumns - 1 do begin
            pixelData[iLabelColumn, iStart:iEnd] = entityInfo.labels[iLabelColumn]
          endfor
          pixelPositions[iStart:iEnd] = entityInfo.pixelIndices
          iStart += nEntityPixels
        endforeach

        outputImage.writeData, temporary(pixelData) , pixelPositions
        writtenPixels = n_elements(temporary(pixelPositions))
      endif
      handledShapes += shpData.numberOfEntities
      if isa(pBar) then pBar.setProgress, handledShapes
  endwhile
      
     
  outputImage.cleanup
  inputRef.cleanup
  shpReader.cleanup
  
  if isa(pBar) then pBar.cleanup

  report = Hash()
  report['numberOfShapes'] = handledShapes
  report['numberOfWrittenPixels'] = writtenPixels
  
  return, report
end

pro test_hubApp_dataImport_imgFromShp_processing
  parameters = Hash()
  ;parameters['inputShape'] = 'D:\Berlin_II\Berlin-II-IDLCode\output\testShapeFromASCII_NEW.shp'
  ;parameters['inputShape'] = 'D:\Berlin_II\Berlin-II-IDLCode\output\ROI_Points.shp'
  ;parameters['inputShape'] = 'G:\temp\temp_akpona\4BJ\Reference_LC_2005_new.shp'
  
  
  parameters['inputReference'] = 'G:\temp\temp_akpona\4BJ\Reference_image.bsq'
  parameters['outputImage'] = 'D:\Users\geo_beja\AppData\Local\Temp\imageFromShp'
  
  
  
  parameters['labelColumns'] = ['Label']
  parameters['file type'] = 'ENVI Classification'
  
  print, hubApp_dataImport_imgFromShp_processing(parameters)
end