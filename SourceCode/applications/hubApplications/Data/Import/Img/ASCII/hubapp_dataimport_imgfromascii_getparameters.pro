;+
; :Description:
;    Returns the column names related to a CSV table.
;    In case the column names are not defined the function returns 
;    artificial names e.g. 'Column 1', 'Column 2' etc.
;
; :Params:
;    parameters: in, required, type=hash
;     This is the parameters hash as returned by `hubApp_dataImport_ImgFromASCII_getParameters_Dialog2`.
;
;
;
;-
function hubApp_dataImport_ImgFromASCII_getParameters_ColumnNames, parameters
  
  reader = hubIOASCIIInput(parameters['inputTextFile'])
  reader.initReader, parameters['firstDataLine'] $ ;get example row
      , firstline = 1
  exampleRows = strtrim(reader.getData(),2)
  reader.cleanup

  
  nLines = n_elements(exampleRows)
  delimiter = parameters['delimiter']
  isRegex = parameters.hubKeywordSet('delimiterIsRegex')
  dataValues = strsplit(exampleRows[nLines-1], delimiter, Regex = isRegex, /EXTRACT)
  nColumns = n_elements(dataValues)
  
  columnNames = []
  for i = nLines - 2, 0, -1 do begin
    headerValues = strsplit(exampleRows[i], delimiter, Regex = isRegex, /EXTRACT)
    if n_elements(headerValues) eq nColumns then begin
      columnNames = headerValues
      break
    endif
  endfor
  
  if ~isa(columnNames) then begin
    columnNames = 'Column ' + strtrim(indgen(n_elements(exampleRows))+1,2)
  endif
  
  return, columnNames
end


;+
; :Description:
;    Function to perform an explicit consistency check on user input values. 
;-
function hubApp_dataImport_ImgFromASCII_getParameters_Dialog3Check, resultHash $
      , message=message $
      , userInformation=userInformation
  isValid = 1b
  msg = []
  
  importType = userInformation['importType'] 
  
  ;get final image dimensions
  if resultHash['dimensionSource'] eq 0 then begin
    ;dimensions from external image
    refImage = hubIOImgInputImage(resultHash['dimensionReference'])
    nBands = refImage.getMeta('bands')
    nSamples = refImage.getMeta('samples')
    nLines = refImage.getMeta('lines') 
    refImage.cleanup
  endif else begin
    ;user-defined dimensions
    nBands = resultHash.hubGetValue('bands', default=1)
    nLines = resultHash['lines']
    nSamples = resultHash['samples']
  endelse
  
  if resultHash.hubGetValue('coordinateType') eq 'geo' then begin
    if resultHash['dimensionSource'] eq 0 then begin
      ;check if the reference image has a map info
      refImage = hubIOImgInpuImage(resultHash['dimensionReference'])
      if ~refImage.hasMeta('map info') then begin
        msg = [msg, "Spatial dimensions reference image has no 'map info'"]
      endif
      refImage.cleanup
          
    endif else begin
      if resultHash['newMapInfo'] eq 0 then begin
        msg = [msg, 'Requires map info from reference image or new definition']
      endif
    endelse
  endif
  
  ;check map info definition
  if resultHash.hubIsa('newMapInfo') && resultHash['newMapInfo'] eq 1 then begin
    ;not required - each tie point is allowed
;    ;check if reference pixel is a valid image pixel index 
;    if resultHash['mapInfoImgX'] gt nSamples then begin
;      msg = [msg, 'map info reference pixel x exceeds total number of samples']
;    endif
;    if resultHash['mapInfoImgY'] gt nLines then begin
;      msg = [msg, 'map info reference pixel y exceeds total number of lines']
;    endif
  endif  
  
  if resultHash.hubIsa('classificationReference') then begin
    inputImage = hubIOImgInputImage(resultHash['classificationReference'])
    if ~inputImage.isClassification() then begin
      msg = [msg, string('(%"File %s is not a valid classification reference")', $
                FILE_BASENAME(resultHash['classificationReference']))]
    endif
    inputImage.cleanup
  endif
  
  message = msg
  return, ~isa(msg)
end


;+
; :Description:
;    This dialog ask a user how to map the text file values into a final image.
;
; :Params:
;    dialogSettings: in, required, type=hash
;    parameters2: in, required, type=hash
;     This hash contains the values collected from the previous called dialogs.
;
;
;
;-
function hubApp_dataImport_ImgFromASCII_getParameters_Dialog3, dialogSettings, parameters2
  groupLeader = dialogSettings.hubGetValue('groupLeader')
  helper = dialogSettings['helper']
  

  ;get suggestion for input widgets
  suggest = parameters2['suggestions']


  reader = hubIOASCIIInputTable(parameters2['inputTextFile'])
  reader.initReader, 1 $ ;get example row
      , firstdataline = parameters2['firstDataLine'] $
      , numberOfColumns = -1 $ ;try to extract the column values
      , delimiter=parameters2['delimiter'] $ ;... by this regular expression
      , isRegex=parameters2['delimiterIsRegex']                    ;
  exampleRow = reader.getData()
  reader.cleanup
  
   
  isClassification = strcmp(parameters2['file type'], 'ENVI Classification', /FOLD_CASE)
  allowMultipleBands = ~stregex(parameters2['file type'], 'Classification|Regression',/BOOLEAN, /FOLD_CASE)
  
  hubAMW_program , dialogSettings['groupLeader'], Title = dialogSettings['title']
  
  hubAMW_tabFrame
  hubAMW_tabPage, Title='Preview'
       hubAMW_text, 'textPreview', Title='' $
             , EDITABLE=0, WRAP=0, /Scroll $
             , XSize=600 $ 
             , YSIZE=350 $
             , VALUE=suggest['previewLines']
      
  if isClassification then begin
    hasSuggestions = suggest.hubIsa(['classes','class names','class lookup'], /EvaluateOR)
    
    ;Ask for target classification scheme
    hubAMW_tabPage ,   Title =  'Classification'
      hubAMW_subframe, 'classificationSource', Title='Copy from reference image', /Row, SETBUTTON=~hasSuggestions
        hubAMW_inputImageFilename, 'classificationReference', Title='Target Classification', SIZE=250
      hubAMW_subframe, 'classificationSource', Title='New Definition', /Row, SETBUTTON=hasSuggestions
        hubAMW_parameter, 'nNewClasses', title='Classes', IsGT=0, IsLT=256, /INTEGER, VALUE=suggest.hubGetValue('classes')
  
  endif 
  
  ;ASCII List specific settings: how top map a columns value to the image  
  if dialogSettings['importType'] eq 'imgASCIIList' then begin
    hubAMW_tabPage, Title = 'Columns'
    columnNames = hubApp_dataImport_ImgFromASCII_getParameters_ColumnNames(parameters2)
    columnNames2Show = string(format='(%"%2i ")', indgen(n_elements(columnNames))+1) + strtrim(columnNames,2)
    
    lowNames = strlowcase(columnNames)
    
    base0 = widget_base(hubAMW_getCurrentBase(), /Row)
    baseL = widget_base(base0, /Column)
    baseR = widget_base(base0, /Column)
    
    hubAMW_setCurrentBase, baseL
    
    case parameters2['file type'] of
      'ENVI Standard'       : bandValuesTitle = 'Select band value columns' 
      'ENVI Classification' : bandValuesTitle = 'Select class value column'
      'EnMAP-Box Regression': bandValuesTitle = 'Select band value column'
    endcase
    hubAMW_list, 'iDataColumns' $
               , Title = bandValuesTitle $
               , List=columnNames2Show $
               , XSIZE=250, YSIZE=150 $
               , MULTIPLESELECTION = allowMultipleBands $
               , ALLOWEMPTYSELECTION = 0b
    
    hubAMW_setCurrentBase, baseR
    iX = where(stregex(lowNames, '(east|west|x|lon|right|left|rechts|links|horiz|ost)', /Boolean) ne 0, /NULL)
    iY = where(stregex(lowNames, '(north|south|y|lat|hoch|vert|nord|süd)', /Boolean) ne 0, /NULL)
    hubAMW_label, 'Coordinate Columns:'
    hubAMW_label, 'X/Easting'
    hubAMW_combobox, 'iColumnXCoord', TITLE='', list=columnNames2Show, value=isa(iX)? iX[0]:0, size=150
    hubAMW_label, 'Y/Northing'
    hubAMW_combobox, 'iColumnYCoord', TITLE='', list=columnNames2Show, value=isa(iY)? iY[0]:0, size=150
    
    coordTypeNames = ['Pixel Coordinates (map info not required)','Geo Coordinate (requires map info)']
    coordTypeCode = ['pixel','geo']
    hubAMW_label, 'Coordinate Type'
    hubAMW_combobox, 'coordinateType', Title='' $
                   , list=dialogsettings['coordinateTypeDescriptions'] $
                   , EXTRACT=coordTypeCode $
                   , value=suggest.hubIsa('map info')
    
    
  endif
  
  
  hubAMW_tabPage, TITLE = 'Image Dimensions'
  hasImgSize = suggest.hubIsa('bands') || suggest.hubIsa('lines') || suggest.hubIsa('samples') 
  
  hasMapInfo = suggest.hubIsa('map info')
  
  mapInfoZoneNSList = ['North','South']
  mapInfoUnitsList  = ['meters','km','feet','miles','nautic miles']
  if hasMapInfo then begin
        mapInfo = suggest['map info']
        mapInfoZoneNSValue = where(strcmp(mapInfo.NorthSouth,mapInfoZoneNSList, /FOLD_CASE), /NULL)
        mapInfoUnitsValue  = where(strcmp(mapInfo.units,mapInfoUnitsList, /FOLD_CASE), /NULL)
  endif else begin
        mapInfoZoneNSValue = 0
        mapInfoUnitsValue = 0
        mapInfo = (hubIOImgMeta_mapInfo()).createEmptyMapInfoStruct() 
  endelse
  
  hubAMW_subframe, 'dimensionSource', Title='Copy from Reference Image', /Row, SetButton=~(hasMapInfo || hasImgSize)
    hubAMW_inputImageFilename, 'dimensionReference', Title='  image ', SIZE=250
      
  hubAMW_subframe, 'dimensionSource', Title='New Definition', SetButton=(hasMapInfo || hasImgSize), /Column
      baseSpatial = widget_base(hubAMW_getCurrentBase(), /Column, XPAD=10)
      baseSpatialImg = widget_base(baseSpatial, /Row, /BASE_ALIGN_CENTER)
      
      baseSpatialGeo = widget_base(baseSpatial, /Column)
      
      hubAMW_setCurrentBase, baseSpatialImg
      hubAMW_parameter, 'samples', Title='Samples/Columns', /Integer, IsGT=0, value=suggest.hubGetValue('samples'), Size=6
      hubAMW_parameter, 'lines'  , Title=' Lines/Rows', /Integer, IsGT=0, value=suggest.hubGetValue('lines'), Size=6
      if dialogSettings['importType'] eq 'imgASCIIGrid' then begin
        if parameters2['file type'] eq 'ENVI Standard' then begin
          hubAMW_parameter, 'bands'  , Title=' Bands', optional=0, /Integer, IsGT=0, value=suggest.hubGetValue('bands'), Size=6
        endif else begin
          hubAMW_label, ' Bands = 1'
        endelse 
      endif 

      hubAMW_setCurrentBase, baseSpatialGeo
      hubAMW_subframe, 'newMapInfo', TITLE='Without Georeference / Map Info', /COLUMN, targetBase=baseSpatialGeo, SETBUTTON=~hasmapinfo
      hubAMW_subframe, 'newMapInfo', TITLE='Specify Georeference / Map Info', /COLUMN, targetBase=baseSpatialGeo, SETBUTTON=hasmapinfo
      baseSpatialGeoMapInfo = widget_base(hubAMW_getCurrentBase(), /Column, XPAD=5)
      baseSpatialGeoMapInfo = widget_base(baseSpatialGeoMapInfo, /Column, Frame=0)
        hubAMW_subframe, /Row, targetBase=baseSpatialGeoMapInfo
        hubAMW_parameter,'mapInfoProj', Title='Projection', value= mapInfo.Projection , /String, Size=4
        hubAMW_parameter,'mapInfoZone', Title=' Zone', /integer, value= mapInfo.zone , IsGE=1, IsLE=60, SIZE=2
        hubAMW_Combobox, 'mapInfoZoneNS', Title='', list=mapInfoZoneNSList, value=mapInfoZoneNSValue
        hubAMW_parameter,'mapInfoDatum', Title='Datum', /String, value= mapInfo.datum , SIZE=6
        
        hubAMW_subframe, /Row, targetBase=baseSpatialGeoMapInfo
        hubAMW_Combobox, 'mapInfoUnits', Title='Length units', list=mapInfoUnitsList, value=mapInfoUnitsValue
        hubAMW_subframe, /Row, targetBase=baseSpatialGeoMapInfo, TITLE='Coordinates:'
        
        ;Row 1
        hubAMW_setCurrentBase, widget_base(baseSpatialGeoMapInfo, /Row)
        hubAMW_label, '          '
        hubAMW_label, ['  Pixel ','  Size']
        hubAMW_label, ['  Image  ','  Coord.']
        hubAMW_label, ['      ','Geo Coordinate']
        
        ;Row 2
        hubAMW_setCurrentBase, widget_base(baseSpatialGeoMapInfo, /Row)
        hubAMW_label, ' X/Easting'
        hubAMW_parameter, 'mapInfoSizeX', Title='', value = mapInfo.sizeX, /Float, IsGT=0, Size=5
        hubAMW_parameter, 'mapInfoImgX',Title=''  , value = mapInfo.pixelX, /FLOAT, SIZE=5
        hubAMW_parameter, 'mapInfoGeoX',Title=''  , value = mapInfo.easting, /FLOAT, Size=20, IsGT=0
        
        ;Row 3
        hubAMW_setCurrentBase, widget_base(baseSpatialGeoMapInfo, /Row)
        hubAMW_label, 'Y/Northing'
        hubAMW_parameter, 'mapInfoSizeY', Title='', value=mapInfo.sizeY, /Float, IsGT=0, Size=5
        hubAMW_parameter, 'mapInfoImgY' , Title='', value=mapInfo.pixelY, /FLOAT, SIZE=5
        hubAMW_parameter, 'mapInfoGeoY' , Title='', value=mapInfo.Northing, /FLOAT, Size=20, IsGT=0
        
  hubAMW_frame, Title='Output Parameters'
  ; Ask for final data type
  
  typeCodes = hubHelper.getDataTypeInfo(/TypeCodes)
  typeDescriptions = hubHelper.getDataTypeInfo(/TypeDescriptions)
  typeNames = hubHelper.getDataTypeInfo(/TypeNames)
  iCodes = isClassification ? $
      where(stregex(typeNames, 'byte|int|long', /Boolean, /FOLD_CASE) eq 1,/NULL) : $
      indgen(n_elements(typeNames))
      
  tsize = 150
  hubAMW_combobox,  'data type' $
                    , Title = 'Data Type' $
                    , List=typeDescriptions[iCodes] $ 
                    , Extract=typeCodes[iCodes] $
                    , value = 3 $
                    , tsize=tsize
  
  if ~isClassification then begin
    isOptional = parameters2['file type'] eq 'ENVI Standard' 
    if isOptional && ~suggest.hubIsa('data ignore value') then isOptional = 2
    hubAMW_parameter, 'data ignore value' $
                    , Title = 'Data Ignore Value' $
                    , OPTIONAL = isOptional $
                    , /FLOAT, value=suggest.hubgetValue('data ignore value') $
                    , tsize=tsize
  endif
  
  hubAMW_outputFilename, 'outputImage' $
                  , Title='Image', value=dialogsettings.hubGetValue('outputImage') $
                  , tsize=tsize, SIZE=450
  
  parameters = hubAMW_manage( $
      ConsistencyCheckFunction='hubApp_dataImport_ImgFromASCII_getParameters_Dialog3Check' $
      ,userInformation = parameters2+dialogSettings.hubGetSubHash(['importType']) $
                            )
    
  if parameters['accept'] then begin
    if dialogSettings['importType'] eq 'imgASCIIList' then begin
      parameters['bands'] = n_elements(parameters['iDataColumns'])
      parameters['band names'] = columnNames[parameters['iDataColumns']]
    endif
  
    if parameters.hubIsa('classificationSource') then begin
      if parameters['classificationSource'] eq 1 then begin
        classParameters = hubApp_reClassify_getParameters_specifyClasses(dialogSettings, helper, parameters['nNewClasses'], SEED=23, DEFAULTVALUES=suggest)
        if ~classParameters['accept'] then return, Hash('accept', 0b)
        parameters['classes'] = classParameters['classes']
        parameters['class names'] = classParameters['class names']
        parameters['class lookup'] = classParameters['class lookup']
      endif else begin
        ;copy classification info
        classImage = hubIOImgInputImage(parameters['classificationReference'], /Classification)
        parameters['classes'] = classImage.getMeta('classes')
        parameters['class names'] = classImage.getMeta('class names')
        parameters['class lookup'] = classImage.getMeta('class lookup')
        classImage.cleanup
        
      endelse
    endif
    
    if parameters['dimensionSource'] eq 0 then begin
      dimensionReferenceImage = hubIOImgInputImage(parameters['dimensionReference'])
      
      toCopy = ['samples','lines','bands','map info']
      foreach tag, toCopy do begin
        tagValue = dimensionReferenceImage.getMeta(tag)
        if isa(tagValue) then parameters[tag] = tagValue  
      endforeach
      
      dimensionReferenceImage.cleanup
    endif else begin
      if parameters.hubKeywordSet('newMapInfo') then begin
        ;build new map info
        mapInfo = (hubIOImgMeta_mapInfo()).createEmptyMapInfoStruct() 
        mapInfo.projection = parameters['mapInfoProj']
        mapInfo.pixelX = parameters['mapInfoImgX']
        mapInfo.pixelY = parameters['mapInfoImgY']
        mapInfo.Easting = parameters['mapInfoGeoX'] 
        mapInfo.Northing = parameters['mapInfoGeoY']
        mapInfo.sizeX = parameters['mapInfoSizeX']
        mapInfo.sizeY = parameters['mapInfoSizeY']
        ;mapInfo.isMapBased = total(strcmp(mapInfo.projection, ['','Arbitrary'], /Fold_case)) eq 0
        mapInfo['zone'] = parameters['mapInfoZone'] 
        mapInfo['NorthSouth'] = parameters['mapInfoZoneNS'] 
        mapInfo['Datum'] = parameters['mapInfoDatum'] 
        mapInfo['units'] = mapInfoUnitsList[parameters['mapInfoUnits']]
        parameters['map info'] = mapInfo
        
        ;remove leftovers
        parameters.hubRemove, ['mapInfoProj','mapInfoZoneNS','mapInfoUnits','mapInfoDatum','mapInfoZone','mapInfoZoneNS']
        parameters.hubRemove, ['mapInfoImgX','mapInfoGeoX','mapInfoSizeX']
        parameters.hubRemove, ['mapInfoImgY','mapInfoGeoY','mapInfoSizeY']
      endif 
    endelse
    
   
    ;not ENVI Standard? -> set number of bands to 1
    if ~allowMultipleBands then parameters['bands'] = 1 
  endif
  
  ;remove left-overs
  parameters.hubRemove,['newMapInfo','textPreview','dimensionSource']
  return, parameters
end


;+
; :Description:
;   Explicit consistency check function to be called from `hubApp_dataImport_ImgFromASCII_getParameters_Dialog2Check`.
;-
function hubApp_dataImport_ImgFromASCII_getParameters_Dialog2Check, resultHash $
      , message=message $
      , userInformation=userInformation
  isValid = 1b
  msg = []
  if resultHash.hubIsa('hasColumnHeaders') then begin
      ;TODO Check if it is possible to extract data
     input = hubIOASCIIInput(userInformation['inputTextFile'])
     headerLine = resultHash['firstDataLine']-1
     dataLine = resultHash['firstDataLine']
     lineHeader = input._getLines(headerLine,1)
     linedata = input._getLines(dataLine,1)
     input.cleanup
     
     delimiter =  (userinformation['delimiters'])[resultHash['delimiter']]
     isRegex = (userinformation['delimiterIsRegex'])[resultHash['delimiter']]
     nHeadings = n_elements(strsplit(lineHeader[0], delimiter, Regex=isRegex, /Extract))
     nColumns  = n_elements(strsplit(lineData[0], delimiter, Regex=isRegex, /Extract))
     
     if nHeadings lt nColumns then begin
      msg = [msg, string(format='(%"Less column names in lines %i than data values in line %s.")', headerLine+1,dataLine+1)]
     endif
     if nHeadings gt nColumns then begin
      msg = [msg, string(format='(%"Too many column names in lines %i than data values in line %s.")', headerLine+1,dataLine+1)]
     endif
  endif 
  
  message = msg
  return, n_elements(msg) gt 0
end


;+
; :Description:
;    This dialog is used to collect ASCII file specific settings,
;    as the delimiter used, where the data values start etc. 
;
;-
function hubApp_dataImport_ImgFromASCII_getParameters_Dialog2, dialogSettings, parameters1

  helper = dialogSettings['helper']
  
  suggest = parameters1['suggestions']
  previewSizeX = 700
  previewSizeY = 250
  pathLength = 60
  hubAMW_program , dialogSettings.hubGetValue('groupLeader'), Title = dialogSettings['title']
  hubAMW_frame, Title='Input Text File'
  hubAMW_label, 'Path: '+helper.getShortenedString(parameters1['inputTextFile'], pathLength)
  hubAMW_text, 'textPreview', Title='Preview' $
             , EDITABLE=0, WRAP=0, /Scroll $
             , YSIZE=previewSizeY, XSize=previewSizeX, VALUE=suggest['previewLines']
  hubAMW_combobox,  'delimiter' $
                  , Title='Delimiter          ', LIST=dialogSettings['delimiterNames'], value=suggest.hubGetValue('delimiterIndex')
  hubAMW_subframe, /Row
  hubAMW_parameter, 'firstDataLine' $
                  , Title='First Data Line    ', /INTEGER, IsGT=0 $
                  , Value=suggest.hubGetValue('firstDataLine') $
                  , SIZE=3, UNIT='  '
  
  hubAMW_frame, Title=''
  hubAMW_combobox, 'file type' $
                  , Title='Import as', LIST=dialogSettings['fileTypeDescriptions']
  
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_dataImport_ImgFromASCII_getParameters_Dialog2Check' $
                           , USERINFORMATION=dialogSettings+parameters1)
  if parameters['accept'] then begin
    parameters['delimiterIsRegex'] = (dialogSettings['delimiterIsRegex'])[parameters['delimiter']]
    parameters['delimiter'] = (dialogSettings['delimiters'])[parameters['delimiter']]
    parameters['suggestions'] = suggest
    parameters['file type'] = (dialogSettings['fileTypes'])[parameters['file type']] 
    parameters['firstDataLine'] = parameters['firstDataLine']
  endif
  
  return, parameters
  

end


;+
; :Description:
;    This dialog asks the user for the text file which is to import as an image. 
;
;-
function hubApp_dataImport_ImgFromASCII_getParameters_Dialog1, dialogSettings
  helper = dialogSettings['helper']
  hubAMW_program , dialogSettings.hubGetValue('groupLeader'), Title = dialogSettings['title']
  hubAMW_inputFilename, 'inputTextFile', TITLE='Input ASCII File' $
              , value = dialogSettings.hubGetValue('inputTextFile')
  parameters = hubAMW_manage()
  return, parameters
end

;+
; :Description:
;    This routine can be used to collect required parameters for creating an image out of 
;    values stored in an ASCII text file.
;    
;
; :Params:
;    settings: in, optional, type=hash
;     Hash with GUI related settings:: 
;       
;            key     | type   | description
;        ------------+--------+--------------
;        NoShow      | bool   | set this on true to avoid showing a progressbar.
;        title       | string | title of GUI application
;        groupLeader | int    | groupLeader ID from calling application
;        ------------+--------+--------------
;
;
;
;
;-
function hubApp_dataImport_ImgFromASCII_getParameters, settings
  CANCELED = Hash('accept',0b)
  
  importType = settings['importType']
  dialogSettings = Hash()
  dialogSettings += settings
  if ~dialogSettings.hubIsa('groupLeader') then dialogSettings['groupLeader'] = !NULL
  helper = hubHelper()
  dialogSettings['helper'] = helper
  dialogSettings['nPreviewLines'] = 10
  dialogSettings['delimiters']     = hubApp_getASCIIdelimiters() 
  dialogSettings['delimiterIsRegex'] = hubApp_getASCIIdelimiters(/DelimiterIsRegex)
  dialogSettings['delimiterNames'] = hubApp_getASCIIdelimiters( /DelimiterDescriptions)
  
  ;dialogSettings['dataTypeCodes']     = helper.getDataTypeInfo(/TypeCode) 
  ;dialogSettings['dataTypeDescriptions'] = helper.getDataTypeInfo(/TypeDescription)
  ;dialogSettings['dataTypeNames'] = helper.getDataTypeInfo(/TypeName)
  
  dialogSettings['fileTypes']     = ['ENVI Standard','ENVI Classification','EnMAP-Box Regression']
  dialogSettings['fileTypeDescriptions'] = ['ENVI Standard (multiple bands)','ENVI Classification (1 band, value 0 = unclassified)','EnMAP-Box Regression (1 band, data ignore value)']
  dialogSettings['fileTypeOneBandOnly'] = [0b, 1b, 1b]
  
  dialogSettings['coordinateTypes'] = ['pixel', 'geo']
  dialogSettings['coordinateTypeDescriptions'] = ['Pixel Coordinates (map info not requiured)' $
                                                 ,'Geo Coordinates (requires map info)']
  
  dialogSettings['importType'] = importType
  if ~dialogSettings.HasKey('outputImage') then begin
    dialogSettings['outputImage'] = filepath('image',/TMP)
  endif
  
  
  parameters1 = hubApp_dataImport_ImgFromASCII_getParameters_Dialog1(dialogSettings)
  if ~parameters1['accept'] then return, CANCELED
  
  ;get suggestion for input widgets 
  parameters1['suggestions'] = hubApp_dataImport_FromASCII_getSuggestions(dialogSettings, parameters1['inputTextFile']) 
  
  parameters2 = hubApp_dataImport_ImgFromASCII_getParameters_Dialog2(dialogSettings, parameters1)
  if ~parameters2['accept'] then return, CANCELED
  
  parameters2 += parameters1.hubGetSubhash(['inputTextFile','suggestions','outputImage','outputSL'])
  case importType of
    'imgASCIIGrid'    : parameters3 = hubApp_dataImport_ImgFromASCII_getParameters_Dialog3(dialogSettings, parameters2)
    'imgASCIIList'    : parameters3 = hubApp_dataImport_ImgFromASCII_getParameters_Dialog3(dialogSettings, parameters2)
    else : message, 'not implemented importType = ' + importType
  endcase
  
  if ~parameters3['accept'] then return, CANCELED
  
  finalParameters = parameters1 + parameters2 + parameters3
  finalParameters.hubRemove, ['newMapInfo', 'textPreview','suggestions', 'dimensionSource']
  finalParameters['importType'] = importType 
   
  return, finalParameters

end

;+
; :Hidden:
; :private:
;-

pro test_hubApp_dataImport_ImgFromASCII_getParameters
  settings = Hash()
  settings['groupLeader'] = !NULL
  settings['importType'] = 'imgASCIIGrid'
;  settings['importType'] = 'imgASCIIList'
  settings['inputTextFile'] = 'D:\Sandbox\ASCII_Data\Hymap_Berlin-B_Image.txt'
  
;  settings['inputTextFile'] = 'D:\EnMAP-Box\enmapProject\enmapBox\resource\testData\ascii\ASCII_List.txt'
;  settings['inputTextFile'] = 'T:\ar\fromarcgis.txt'
  settings['outputImage'] = 'D:\Sandbox\ASCII_Data\testOutput'
  settings['title'] = 'test'
  print, hubApp_dataImport_ImgFromASCII_getParameters(settings)
end