;+
; :Description:
;    This routine is used to check parameter values that are required to run
;    `hubAPP_dataImport_ASCII_processing_asciiList` or `hubAPP_dataImport_ASCII_processing_asciiGrid`. 
;
; :Params:
;    parameters: in, required, type=hash
;     The hash containing the required parameters::
;     
;             key         | type    | description
;        -----------------+---------+-----------------------------------------
;        inputTextFile    | string  | path to input ascii text file    
;        outputImage      | string  | path of image that is to create
;        firstDataLine    | integer | zero based index number of first  
;                         |         | text line containing data values
;        delimiter        | string  | ASCII character/string that separates values in a row
;        delimiterIsRegex | bool    | set this on true use 'delimiter' as regular expression
;        lines            | integer | number of outputImage lines
;        samples          | integer | number of outputImage samples
;        data type        | byte    | IDL data type code of outputImage
;        file type        | string  | file type of outputImage. Can be set to
;                         |         | 'ENVI Standard', 'ENVI Classification' or 'EnMAP-Box Regression'       
;        -----------------+---------+-----------------------------------------
;        
;     If `file type = 'ENVI Classification'` the following keys are required too:: 
;        
;        nC = number of classes in total
;        -----------------+------------+-----------------------------------------
;        classes          | integer    | the number of classes except class 'unclassified'
;                         |            | = nC-1
;        class names      | string[nC] | class names with name for class value = 0, usually called 'unclassified' 
;        class lookup     | byte[3][nC]| 3D array with RGB value for each class   
;                         |            |
;        -----------------+------------+-----------------------------------------
;        
;     If `file type = 'EnMAP-Box Regression'` it is required to define::
;     
;             key         | type    | description
;        -----------------+---------+-----------------------------------------------
;        data ignore value| numeric | value that marks pixels without 'real value'
;        -----------------+---------+--------------------------------------------
;-
pro hubapp_dataimport_imgfromascii_processing_checkparameters, parameters
  
  ;perform checks
  required = ['delimiter','delimiterIsRegex','firstDataLine' $
             ,'inputTextFile','outputImage' $
             ,'lines','samples','data type','file type']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  ;file type related checks
  if strcmp(parameters['file type'], 'ENVI Classification', /FOLD_CASE) then begin
    required = ['classes', 'class names', 'class lookup']
    if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
      message, 'file type = ENVI Classification. missing values for keys: '+strjoin(required[missing],', ')
    endif
    if parameters['bands'] ne 1 then message, 'bands must be 1 for file type = ENVI Classification'
  endif
  
  if strcmp(parameters['file type'], 'EnMAP-Box Regression', /FOLD_CASE) then begin
    required = ['data ignore value']
    if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
      message, 'file type = EnMAP-Box Regression. missing values for keys: '+strjoin(required[missing],', ')
    endif
    if parameters['bands'] ne 1 then message, 'bands must be 1 for file type = EnMAP-Box Regression'
  endif
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
end

