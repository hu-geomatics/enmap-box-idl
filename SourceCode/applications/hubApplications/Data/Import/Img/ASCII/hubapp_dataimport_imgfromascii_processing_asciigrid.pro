
;+
; :Description:
;    Describe the procedure.
;
; :Params:
;   parameters: in, required, type=hash
;     The hash containing the required parameters::
;     
;             key         | type    | description
;        -----------------+---------+-----------------------------------------
;        inputTextFile    | string  | path to input ascii text file    
;        outputImage      | string  | path of image that is to create
;        firstDataLine    | integer | zero based index number of first  
;                         |         | text line containing data values
;        delimiter        | string  | ASCII character/string that separates values in a row
;        delimiterIsRegex | bool    | set this on true use 'delimiter' as regular expression
;        lines            | integer | number of lines in outputImage
;        samples          | integer | number of samples in outputImage
;        bands            | integer | number of bands in outputImage
;        data type        | byte    | IDL data type code of outputImage
;        file type        | string  | file type of outputImage. Can be set to
;                         |         | 'ENVI Standard', 'ENVI Classification' or 'EnMAP-Box Regression'       
;        -----------------+---------+-----------------------------------------
;        
;    If `file type = 'ENVI Classification'` the following keys are required too:: 
;        
;        nC = number of classes in total
;        -----------------+------------+-----------------------------------------
;        classes          | integer    | the number of classes except class 'unclassified'
;                         |            | = nC-1
;        class names      | string[nC] | class names with name for class value = 0, usually called 'unclassified' 
;        class lookup     | byte[3][nC]| 3D array with RGB value for each class   
;                         |            |
;        -----------------+------------+-----------------------------------------
;        
;    If `file type = 'EnMAP-Box Regression'` it is required to define::
;     
;             key         | type    | description
;        -----------------+---------+-----------------------------------------------
;        data ignore value| numeric | the numberic value that denotes
;                         |         | pixels without value
;        -----------------+---------+--------------------------------------------
;
;    settings: in, optional, type=hash
;     Hash with GUI related settings:: 
;       
;            key     | type   | description
;        ------------+--------+--------------
;        NoShow      | bool   | set this on true to avoid showing a progressbar.
;        title       | string | title of GUI application
;        groupLeader | int    | groupLeader ID from calling application
;        ------------+--------+--------------
;;
;
;
;-
pro hubapp_dataimport_imgfromascii_processing_asciigrid, parameters, settings
  ;general ASCII Import checks
  hubapp_dataimport_imgfromascii_processing_checkparameters, parameters
  
  ;ASCII List specific checks
  required = ['bands']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  helper = hubHelper()
  
  
  ;set defaults
  if ~parameters.hubIsa('delimiterIsRegex') then parameters['delimiterIsRegex'] = 1b
  if ~parameters.hubIsa('firstDataLine') then parameters['firstDataLine'] = 0
  
  
  ;init the progressbar
  if ~settings.hubKeywordSet('NoShow') then begin
    progressbar = hubProgressbar(title=settings.hubGetValue('title', default='Import ASCII Grid') $
                  , GroupLeader=settings.hubGetValue('groupLeader') $
                  , info='Please wait...')
    progressDone = 0
  endif
  
  
  dataIgnoreString = parameters.hubGetValue('dataIgnoreString')
  nSamples = parameters['samples']
  nLines = parameters['lines']
  nBands = parameters['bands']
  
  reader = hubIOASCIIInputTable(parameters['inputTextFile'])

  
  tileLines = parameters.hubGetValue('tileLines', default=100)
  tileLines = tileLines < nLines
  
  
  outputImage = hubIOImgOutputImage(parameters['outputImage'], /NoOpen)
  toCopy = ['bands','samples','lines' $
           ,'data type', 'file type','data ignore value' $ 
           ,'band names','map info','wavelengths','wavelength units' $
           ,'classes','class names','class lookup' $
           ]
  foreach tag, toCopy do begin
    if parameters.hubIsa(tag) then outputImage.setMeta, tag, parameters[tag]
  endforeach
  
  ;define writer settings 
  writerSettings = hash()
  writerSettings += parameters.hubGetSubHash(['data type','samples','lines','bands','file type'])
  writerSettings['dataFormat'] = 'band'
  writerSettings['tileProcessing'] = 0b
  writerSettings['neighborhoodHeight'] = 0
  writerSettings['numberOfTiles'] = fix((nLines / tileLines) * nBands)  
  
  ;init reader and writer
  reader.initreader, tileLines $
      , delimiter=delimiter $
      , isRegex = parameters['delimiterIsRegex'] $
      , firstDataLine=parameters['firstDataLine'] $
      , numberOfColumns = nSamples 
  outputImage.initWriter, writerSettings
    
  hdrInfo = hubIOImgHeader_parseHeader(parameters['inputTextFile'], lines=parameters['firstDataLine']-1, tolerant=1b)
  additional = Hash()
  foreach hdrMeta, hdrInfo do begin
    name =  hdrMeta.getName()
    value = hdrMeta.getValue()
    if not outputImage.hasMeta(name) then begin
        outputImage.setMeta, name, value
    endif
  endforeach
  
  
  
  ;2D Array with band values
  remainingTextLines = long(nLines) * nBands
  remainingValues    = long(nLines) * nBands * nSamples
  if isa(progressbar) then progressbar.setRange, [0, remainingTextLines]
  
  while ~reader.readingDone() and remainingTextLines gt 0 do begin
    linesToRead = tileLines < remainingTextLines
    
    data = reader.getData(linesToRead)
    if n_elements(data[*,0]) ne nSamples then begin
      message, string(format='(%"unable to read %i sample/column values in a line")', nSamples)
    endif 
    nTextLines = n_elements(data[0,*])
    remainingTextLines -= nTextLines
    remainingValues -= n_elements(data)
    
    if parameters.hubIsa(['dataIgnoreString','data ignore value'], /EvaluateAND) then begin
      data[where(data eq dataIgnoreString)] = string(parameters['data ignore value'])
    endif
    
    data = hubMathHelper.convertData(data, parameters['data type'], /NoCopy)
    
    if strcmp(parameters['file type'], 'ENVI Classification', /FOLD_CASE) then begin
      if isa(where(data gt parameters['classes'], /NULL)) then begin
        msg = string(format='(%"ASCII values out of range: 0 <= class value <= number of classes. Max. number of classes is %i.")',parameters['classes'])       
        message, msg
      endif
    endif
    
    ;print, remainingtextlines, remainingValues
    outputImage.writeData, data
    
    if isa(progressbar) then begin
      progressDone += nTextLines 
      progressbar.setProgress, progressDone
    endif
  endwhile
  
  
  reader.cleanup
  
  if isa(progressbar) then progressbar.cleanup
  
  fnImage    = outputImage.getMeta('filename data')  
  fnImageHdr = outputImage.getMeta('filename header')
  outputImage.cleanup
  if remainingTextLines gt 0 then begin
    FILE_DELETE, fnImage, fnImageHdr
    message, 'Could not finish writing because '+strtrim(remainingTextLines,2)+' text lines with data values are missing'
  endif else begin
    enmapBox_openImage, fnImage
  endelse
end

pro test_hubapp_dataimport_imgfromascii_processing_asciigrid
parameters = Hash()
parameters['bands'] =1
parameters['delimiterIsRegex'] = 0b
parameters['lines'] = 267
parameters['data type'] =4
parameters['delimiter'] = string(9b)
parameters['outputImage'] = 'C:\Users\geo_beja\AppData\Local\Temp\test'
parameters['firstDataLine'] = 5
parameters['importType'] = 'imgASCIIGrid'
parameters['accept'] =1
parameters['inputTextFile'] = 'T:\bj\LAI_GRID.txt'

parameters['data ignore value'] =-1.0000000
parameters['samples'] =433
parameters['map info'] = '{UTM, 1, 1, 665645.367000, 5304502.492000, 30.000000, 30.000000, 32, North, WGS-84,units=meters}'
parameters['textLines'] = 10
parameters['file type'] = 'ENVI Standard'

parameters['inputTextFile'] = 'D:\Sandbox\ASCII_Data\Hymap_Berlin-A_Image.txt'
parameters['samples'] = 300
parameters['lines'] = 300
parameters['bands'] = 2
parameters['delimiter'] = '[ \t]+'
parameters['delimiterIsRegex'] = 1b

settings = Hash()
settings['appName']='hubApplications'
settings['tileLines']=100
settings['version']=1.1
settings['textLines']= 10
hubapp_dataimport_imgfromascii_processing_asciigrid, parameters, settings
end