;+
; :Description:
;    This procedure reads an ASCII List and create an image from.  
;
; :Params:
;    parameters: in, required, type=Hash
;     The hash containing the following  parameters (`*` = optional)::
;     
;             key         | type    | description
;        -----------------+---------+-----------------------------------------
;        inputTextFile    | string  | path to input ascii text file    
;        outputImage      | string  | path of image that is to create
;        firstDataLine    | integer | zero based index number of first  
;                         |         | text line containing data values
;        delimiter        | string  | ASCII character/string that separates values in a row
;        delimiterIsRegex | bool    | set this on true use 'delimiter' as regular expression
;        lines            | integer | number of outputImage lines
;        samples          | integer | number of outputImage samples
;        data type        | byte    | IDL data type code of outputImage
;        file type        | string  | file type of outputImage. Can be set to
;                         |         | 'ENVI Standard', 'ENVI Classification' or 'EnMAP-Box Regression'       
;        iDataColumns     | bool    | text column indices (zero-based) with band/data values
;        band names       | string[]| name for each band
;        iColumnXCoord    | int     | text column index (zero-based) with X/Easting coordinates  
;        iColumnYCoord    | int     | text column index (zero-based) with Y/Northing coordinates
;        coordinateType   | string  | 'pixel' -> coordinates are given as zero-based pixel indices
;                         |         | 'geo'   -> coordinates are given as geo coordinates
;        -----------------+---------+-----------------------------------------
;        
;     If `file type = 'ENVI Classification'` the following keys are required too:: 
;        
;        nC = number of classes in total
;        -----------------+------------+-----------------------------------------
;        classes          | integer    | the number of classes except class 'unclassified'
;                         |            | = nC-1
;        class names      | string[nC] | class names with name for class value = 0, usually called 'unclassified' 
;        class lookup     | byte[3][nC]| 3D array with RGB value for each class   
;                         |            |
;        -----------------+------------+-----------------------------------------
;        
;     If `file type = 'EnMAP-Box Regression'` it is required to define::
;     
;             key         | type    | description
;        -----------------+---------+-----------------------------------------------
;        data ignore value| numeric | the numberic value that denotes
;                         |         | pixels without value
;        -----------------+---------+--------------------------------------------
;
;    
;    settings: in, optional, type=hash
;     Hash with GUI related settings:: 
;       
;            key     | type   | description
;        ------------+--------+--------------
;        NoShow      | bool   | set this on true to avoid showing a progressbar.
;        title       | string | title of GUI application
;        groupLeader | int    | groupLeader ID from calling application
;        ------------+--------+--------------
;
;
;
;-
pro hubapp_dataimport_imgfromascii_processing_asciilist, parameters, settings
  ;general ASCII Import checks
  hubapp_dataimport_imgfromascii_processing_checkparameters, parameters
  ;ASCII List specific checks
  required = ['iDataColumns','iColumnXCoord','iColumnYCoord','coordinateType']
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  if total(parameters['coordinateType'] eq ['pixel', 'geo']) ne 1 then begin
    message, "parameter 'coordinateType' must be set to 'pixel' or 'geo'"
  endif 
   
  if parameters['coordinateType'] eq 'geo' and ~parameters.hubIsa('map info') then begin
    message, "parameter 'map info' must be set when using geo-coordinates"
  endif
  
  ;set defaults
  if ~parameters.hubIsa('delimiterIsRegex') then parameters['delimiterIsRegex'] = 1b
  if ~parameters.hubIsa('firstDataLine') then parameters['firstDataLine'] = 0
   
  
  ;init the progressbar
  if isa(settings) && ~settings.hubKeywordSet('NoShow') then begin
    progressbar = hubProgressbar( $
                    title=settings.hubGetValue('title', default='Import ASCII List') $
                  , GroupLeader=settings.hubGetValue('groupLeader') $
                  , info='Please wait...')
    progressDone = 0
  endif
  
  
  helper = hubHelper()
  
  textLines = parameters.hubGetValue('textLines', default=100)
  
  nTextColumns = max([parameters['iDataColumns'] $
                     ,parameters['iColumnXCoord'] $
                     ,parameters['iColumnYCoord'] $
                     ])+1
  
  reader = hubIOASCIIInputTable(parameters['inputTextFile'])
  if isa(progressbar) then begin
    progressbar.setRange, [0, reader.numberOfFileLines()]
  endif
  
  outputImage = hubIOImgOutputImage(parameters['outputImage'])
  
  nSamples = parameters['samples']
  nLines   = parameters['lines']
  nBands   = n_elements(parameters['iDataColumns'])
  
  toCopy = ['lines','samples','file type','data type','data ignore value','map info', 'band names']
  foreach tag, toCopy do begin
    if parameters.hubIsa(tag) then outputImage.setMeta, tag, parameters[tag]
  endforeach
  outputImage.setMeta, 'bands', nBands
  
  ;define writer settings 
  writerSettings = hash()
  writerSettings += parameters.hubGetSubHash(['data type','data ignore value','samples','lines','file type'])
  writerSettings['dataFormat'] = 'profiles'
  writerSettings['tileProcessing'] = 0b
  writerSettings['neighborhoodHeight'] = 0
  writerSettings['bands'] = nBands
  
  if parameters['coordinateType'] eq 'geo' then begin
    geoHelper = hubGeoHelper(outputImage)
  endif
  
  ;init reader
  reader.initreader, textLines $
      , delimiter=parameters['delimiter'] $
      , isRegex = parameters['delimiterIsRegex'] $
      , firstDataLine=parameters['firstDataLine'] $
      , numberOfColumns = -1

  ;init outputImage-writer
  outputImage.initWriter, writerSettings
  
  
  while ~reader.readingDone()  do begin
    data = reader.getData()
    if ~isa(data) then continue
    positionX = data[parameters['iColumnXCoord'],*]
    positionY = data[parameters['iColumnYCoord'],*]
    dataValues = data[parameters['iDataColumns'],*]
    
   
    if parameters['coordinateType'] eq 'geo' then begin
      points = geoHelper.createGeometry(double(positionX),double(positionY)) 
      points = geoHelper.convertCoordinate2PixelIndex(points, Truncate=1)
      if ~isa(points) then continue 
      positionX = points.X
      positionY = points.Y
    endif else begin
      positionX = long(positionX)
      positionY = long(positionY)
      ;truncation mode handling?
      iPx = where(positionX ge 0 and positionX lt nSamples and $
                  positionY ge 0 and positionY lt nLines, /NULL)
      if ~isa(iPx) then continue
      positionX = positionX[iPx]
      positionY = positionY[iPx]
    endelse
    
    ;write the data
    if isa(positionX) then begin
      outputImage.writeData, dataValues, long(nSamples) * positionY + positionX
    endif
    
    if isa(progressBar) then begin
      progressDone += n_elements(data[0,*]) 
      progressBar.setProgress, progressDone
    endif
    
  endwhile
  
  outputImage.finishWriter
  outputImage.cleanup
  reader.cleanup
  if isa(progressBar) then progressBar.cleanup
end

;+
; :HIDDEN:
; :private:
;-
pro test_hubAPP_dataImport_imgfromascii_processing_asciiList
parameters =  Hash()
parameters['bands']=5
parameters['delimiterIsRegex']=0
parameters['lines']=1404
parameters['data type']=4
parameters['coordinateType']='geo'
parameters['delimiter']=string(9b)
parameters['outputImage']='D:\Sandbox\ASCII_Data\testOutput'
parameters['firstDataLine']=5
parameters['importType']='imgASCIIList'
parameters['accept']=1
parameters['iColumnYCoord']=1
parameters['iColumnXCoord']=0
parameters['hasColumnHeaders']=1
parameters['inputTextFile']='D:\Sandbox\ASCII_Data\Measurements.txt'
parameters['data ignore value']=-1.0000000
parameters['samples']=1050

meta = hubIOImgMeta_mapInfo()
meta.setValueFromENVIString, '{UTM, 1, 1, 330644.987387, 5388507.015154, 4.000000, 4.000000, 33, North, WGS-84,units=meters}'
parameters['map info'] = meta.getValue()
parameters['iDataColumns']=[0,1,2,3,4]
parameters['file type']='ENVI Standard'
parameters['textLines'] = 10
settings =  Hash()

settings['importType']='imgASCIIList'
settings['appName']='hubApplications'
settings['version']=1.1
settings['title']='Import filter TestAPP: asciiList'
settings['outputImage']='D:\Sandbox\ASCII_Data\testOutput'
settings['inputTextFile']='D:\Sandbox\ASCII_Data\Measurements.txt'

  hubAPP_dataImport_imgfromascii_processing_asciiList, parameters, settings
end