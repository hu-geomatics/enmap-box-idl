



;+
; :Hidden:
;
; :Description:
;    Same as hubApp_dataImport_ImgFromTIFF_processing, but returns the file names of all 
;    written images.
;
;-
function hubApp_dataImport_ImgFromTIFF_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputTIFF','outputImage']
  
  if ~isa(settings) then settings = hash()
  if ~settings.hubKeywordSet('NoShow') then begin
    pBar = hubProgressBar(title='GeoTIFF Import' $
                         , Info='Read Data...' $
                         , GroupLeader=settings.hubGetValue('groupLeader'))
  endif
  
  ;Query tiff information
  isValidTIFF = QUERY_TIFF(parameters['inputTIFF'], Info, GEOTIFF=geoInfo, IMAGE_INDEX=index)
  if ~isValidTIFF then begin
    message, string(format='(%"File %s is not a valid TIFF image")', parameters['inputTIFF'])
  endif
  
  writtenImages = list()
  nImages = info.num_images
  
  if nImages gt 1 && ~parameters.hubKeywordSet('extractAll') then nImages = 1 
  
  if isa(pBar)  then begin
    pBar.setRange, [0, info.num_images * info.channels]
    pBarProgress = 1
  endif
  ;init the output image

  for iImage = 0, nImages - 1 do begin
    
    isValidTIFF = QUERY_TIFF(parameters['inputTIFF'], Info, GEOTIFF=geoInfo, IMAGE_INDEX=iImage)
    if isValidTIFF then begin
      nBands   = info.channels
      nSamples = info.dimensions[0]
      nLines   = info.dimensions[1]
      data_type = info.pixel_type
      orientation = info.orientation
      
      description = strtrim(string(format='(%"Imported TIFF: %s %s %s ")' $
        , info.document_name, info.date_time, info.description),2) 
      pathOut = parameters['outputImage']
      suffix = ''
      if nImages gt 1 then begin
        
        description += string(format='(%" Image %i/%i")', iImage+1, nImages)
        
        dn = FILE_DIRNAME(pathOut, /MARK_DIRECTORY )
        bn = FILE_BASENAME(pathOut)
        ext = stregex(bn, '.[[:alnum:]]{1,4}$', /EXTRACT)
        if ext ne '' then begin
          bn = strmid(bn, 0, strlen(bn)-strlen(ext))
        endif
        pathOut = dn + bn + suffix + '_'+strtrim(iImage+1,2) + ext 
      endif
      
      outputImage = hubIOImgOutputImage(pathOut)
      outputImage.setMeta, 'description', description
      
      if isa(geoInfo) && ~ISA(geoINFO, /SCALAR) then begin
        geoInfo = DICTIONARY(geoInfo, /EXTRACT)
        geoInfo = hubApp_dataImport_ImgFromTIFF_extractGeoInfo(geoInfo)
        if isa(geoInfo.mapInfo) then outputImage.setMeta, 'map info', geoInfo.mapInfo
        if isa(geoInfo.coordinateSystemString) then outputImage.setMeta, 'coordinate system string', geoInfo.coordinateSystemString
      endif
      
      writerSettings = Hash()
      print, 'ns', nSamples, 'nl', nLines
      writerSettings['bands'] = nBands
      writerSettings['lines'] = info.orientation le 4 ?  nLines : nSamples
      writerSettings['samples'] = info.orientation le 4 ? nSamples : nLines
      writerSettings['data type'] = data_type
      writerSettings['tileProcessing'] = 0
      writerSettings['dataFormat'] = 'band'
      
      outputImage.initWriter, writerSettings
      
      if isa(pBar) then pBar.setInfo, 'Import Bands...'
      for iBand = 0, nBands - 1 do begin
        if isa(pBar) && nBands gt 1 then pBar.setProgress, pBarProgress++
        data = read_tiff(parameters['inputTIFF'],CHANNELS=iBand,IMAGE_INDEX=iImage $
                        ,INTERLEAVE=2, PLANARCONFIG=planarconfig $
                        ,GEOTIFF=geoInfo2)
        
        dim = size(data, /DIMENSIONS)
        ;if dim[0] ne nSamples then stop
        ;if dim[1] ne nLines then stop
        case orientation of
          ;Image orientation (columns, rows):
          ;1 = Left to right, top to bottom (default)
          1 : ;nothing
          ;2 = Right to left, top to bottom
          2: data = reverse(data,1, /OVERWRITE)
         
          ;3 = Right to left, bottom to top
          3: data = rotate(data,2)
          
          ;4 = Left to right, bottom to top
          4: data = reverse(data,2, /OVERWRITE)
          
          ;5 = Top to bottom, left to right
          5: data = reverse(rotate(data,1),1, /OVERWRITE)
          
          ;6 = Top to bottom, right to left 
          6: data = rotate(data,1)
          
          ;7 = Bottom to top, right to left
          7: data = rotate(data,6)
          
          ;8 = Bottom to top, left to right
          8:  data = rotate(data,3)
          else: message, 'Unknown scanline orientation index :' + strtrim(orientation,2) 
        endcase
        outputImage.writeData, temporary(data)
        
      endfor
      
      outputImage.cleanup
      writtenImages.add, pathOut
    endif
  endfor
  
  if isa(pBar) then pBar.cleanup
  return, writtenImages.toArray()
end

;+
; :Description:
;    Describe the procedure.
;
; :Params:
;    parameters: in, required, type=hash
;
;     Hash containg the following parameters::
;
;       key         | type   | description
;       ------------+--------+---------------
;       inputTIFF   | string | path of TIFF file
;       outputImage | string | path of imported image file
;       extractAll  | bool   | if set, all images are extracted, e.g. different zoom levels
;                   |        | default = false
;       ------------+--------+---------------
;
;    settings: in, optional, type = hash
;     Use this hash to provide optional GUI relevant parameters::
;
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+---------------
;
;-
pro hubApp_dataImport_ImgFromTIFF_processing, parameters, settings
  !NULL = hubApp_dataImport_ImgFromTIFF_processing(parameters, settings)
end

;+
; :Hidden:
;-
pro test_hubApp_dataImport_ImgFromTiff_processing
  parameters = Hash()
  parameters['tileLines'] = 10
  parameters['noShow'] = 0b
  
  
  ;load test images:
  dirExamples = 'T:\BJ\ExamplesTIFFOrientation\'
  dirOut = 'T:\BJ\ExamplesTIFFOut\'
  
  filesIn = FILE_SEARCH(dirExamples, '*.tif', /FOLD_CASE)
  
  ;FILE_MKDIR, dirOut, /NOEXPAND_PATH
  offset = 0
  limit = -1
  cnt = 0
  for i = 0 + offset, n_elements(filesIn)-1 do begin
    cnt++
    fileIn = filesIn[i]
    bn = FILE_BASENAME(fileIn, '.tif')
    parameters = Hash()
    parameters['inputTIFF'] = fileIn
    parameters['outputImage'] = dirOut+bn+'.bsq'
    !NULL = QUERY_TIFF(fileIn, info)
    print, format='(%"%i %s --> %s orienation: %i")', i $
        , FILE_BASENAME(parameters['inputTIFF']), FILE_BASENAME(parameters['outputImage']) $
        , info.orientation
    

    hubApp_dataImport_ImgFromTIFF_processing, parameters
    
    if isa(limit) && limit ge 0 && cnt ge limit then break
  endfor
  
  print, 'test done'
end