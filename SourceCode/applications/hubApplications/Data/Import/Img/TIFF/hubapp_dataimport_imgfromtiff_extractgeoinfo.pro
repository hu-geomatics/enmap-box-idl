function hubApp_dataImport_ImgFromTIFF_removeEPSGprefix, value
  if ~isa(value) then return, !NULL
  prefix = stregex(value, '^[^_]+_', /EXTRACT, /FOLD_CASE)
  return, STRMID(value, STRLEN(prefix))
end

function hubApp_dataImport_ImgFromTIFF_extractProjInfo, projKey
  result = DICTIONARY()
  
  result['datum'] = !NULL
  result['zone'] = !NULL
  result['projection'] = !NULL 
  result['ns'] = !NULL 
  
;  Ranges:
;  [    1,   1000]  = Obsolete EPSG/POSC Projection System Codes
;  [20000,  32760]  = EPSG Projection System codes
;  32767            = user-defined
;  [32768,  65535]  = Private User Implementations
;  Special Ranges:
;  1. For PCS utilizing GeogCS with code in range 4201 through 4321: As far
;  as is possible the PCS code will be of the format gggzz where ggg is
;  (geodetic datum code -4000) and zz is zone.
;  2. For PCS utilizing GeogCS with code out of range 4201 through 4321
;  (i.e. geodetic datum code 6201 through 6319). PCS code 20xxx where
;  xxx is a sequential number.
;  3. Other:
;  WGS72 / UTM northern hemisphere: 322zz where zz is UTM zone number
;  WGS72 / UTM southern hemisphere: 323zz where zz is UTM zone number
;  WGS72BE / UTM northern hemisphere: 324zz where zz is UTM zone number
;  WGS72BE / UTM southern hemisphere: 325zz where zz is UTM zone number
;  WGS84 / UTM northern hemisphere: 326zz where zz is UTM zone number
;  WGS84 / UTM southern hemisphere: 327zz where zz is UTM zone number
;  US State Plane (NAD27):  267xx/320xx
;  US State Plane (NAD83):  269xx/321xx
 

 if isa(projKey, /NUMBER) then begin 
  if projKey eq 32767 then begin
    return, result
  endif
  if projKey ge 20000 && projKey le  32760 then begin
    p = fix(projKey / 100)
    
    case 1b of 
      total(p eq [322,323]) : result.datum = 'WGS72'
      total(p eq [324,325]) : result.datum = 'WGS72BE'
      total(p eq [326,327]) : result.datum = 'WGS84'
      total(p eq [267,320]) : result.datum = 'NAD27'
      total(p eq [269,321]) : result.datum = 'NAD83'
      else : ;nothing
    endcase
    
    ;UTM 
    if p ge 322 && p le 327 then begin
      result.zone = projKey - (p * 100)
      result.projection = 'UTM'
      
      case 1b of
        total(p eq [322,324,326]) : result.ns = 'North'
        total(p eq [323,325,327]) : result.ns = 'South'
        else : ;nothing
      endcase
      
    endif
    
    ;State plane systems
    if total(p eq [267,320,269,321]) then begin 
      case 1b of
        total(p eq [267,320]) : result.projection = 'State Plane (NAD 27)'
        total(p eq [269,321]) : result.projection = 'State Plane (NAD 83)'
        else : ;nothing
      endcase
      result.zone = projKey - (p * 100) ;not a zone, but sequential number?
    endif
  endif

 endif 
 
 if TYPENAME(projKey) eq 'STRING' then begin
    
    ;fallback solution
    result.projection = 'Arbitrary'
    result.datum = 'WGS84'
    
 endif
  
 return, result
end

;+
; :Description:
;    This function extracts information from the `GeoINFO` returned by calling IDLs `QUERY_TIFF(filename, GeoTIFF=GeoInfo)` function,
;    
;    It returns a struct containing the following tags::
;       
;       tag                    | description
;       -----------------------+--------------------------------------------
;       mapInfo                | map info as used by hubIOImgImages()
;       coordinateSystemString | coordinate string used by hubIOImgImages()
;       -----------------------+--------------------------------------------
;       
; :Params:
;    geoInfo: in, required, type=struct
;    
;     This is the structure returned by the `GeoTIFF` keyword of IDLs 
;     `QueryTIFF` function.
;
;
;
;-
function hubApp_dataImport_ImgFromTIFF_extractGeoInfo, geoInfo
  result = Dictionary('mapInfo',!NULL, 'coordinateSystemString', !NULL)
 
  geoHelper = hubGeoHelper()
  mapInfo = hubIOImgMeta_mapInfo.createEmptyMapInfoStruct()
  
  
  ;map info always requires tie point + pixel scale definition
  if ~geoInfo.hasKey('ModelPixelScaleTag') || $
     ~geoInfo.hasKey('ModelTiePointTag') then begin
      return, result
  endif
     
   ;http://www.remotesensing.org/geotiff/spec/geotiff6.html#6.2
   ;http://www.remotesensing.org/geotiff/spec/geotiff3.html
  mapInfo.sizeX = (geoInfo.ModelPixelScaleTag)[0]
  mapInfo.sizeY = (geoInfo.ModelPixelScaleTag)[1]
  mapInfo.pixelX   = (geoInfo.ModelTiePointTag)[0]
  mapInfo.Easting  = (geoInfo.ModelTiePointTag)[3]
  mapInfo.pixelY   = (geoInfo.ModelTiePointTag)[1]
  mapInfo.Northing = (geoInfo.ModelTiePointTag)[4]
  
  if geoInfo.hasKey('GTRASTERTYPEGEOKEY') then begin
    ;RasterPixelIsArea  = 1
    ;RasterPixelIsPoint = 2
    
    if geoInfo.GTRASTERTYPEGEOKEY eq 2 then begin
      ;set tie point to upper left coordinate???
      
    endif
  endif
  
  
  ;find geoCitation string
  citationString = !NULL
  foreach key, ['GTCITATIONGEOKEY','GEOGCITATIONGEOKEY','PCSCITATIONGEOKEY'] do begin
    if geoInfo.hasKey(key) then begin
      citationString = geoInfo[key]
      break
    endif
  endforeach
  
  ;find projection string
  EPSGTypeCodes  = geohelper.getEPSGTypeCodes()
  projString = !NULL
  foreach key, ['PROJECTEDCSTYPEGEOKEY','GeographicTypeGeoKey'] do begin
    if geoInfo.hasKey(key) then begin
      projKey = geoInfo[key]
      projString = EPSGTypeCodes.hubGetValue(projKey)
      projString = hubApp_dataImport_ImgFromTIFF_removeEPSGprefix(projString)
      break
    endif
  endforeach
  
  ;combine geoCitation and projection string  
  PINFO = strjoin(['',citationString, projString],' ')
  
  ;find EPSG unit string
  unitString = !NULL
  foreach key, ['ProjLinearUnitsGeoKey','GeogAngularUnitsGeoKey'] do begin
    if geoInfo.hasKey(key) then begin
      unitKey = geoInfo[key]
      unitString = EPSGTypeCodes.hubGetValue(unitKey)
      unitString = hubApp_dataImport_ImgFromTIFF_removeEPSGprefix(unitString)
      break
    endif
  endforeach
  if ~isa(unitString) then begin
    ;try to extract from PINFO
    knownUnits = '(kilometer|meter|foot|feet|yard|mile|degree|Minute|second)s*'
    unitString = stregex(PINFO, knownUnits, /EXTRACT, /FOLD_CASE)
    if unitString eq '' then unitString = !NULL
  endif
  
  ;find EPSG datum
  datumString = !NULL
  foreach key, ['GeogGeodeticDatumGeoKey'] do begin
    if geoInfo.hasKey(key) then begin
      datumKey = geoInfo[key]
      datumString = EPSGTypeCodes.hubGetValue(datumKey)
      datumString = hubApp_dataImport_ImgFromTIFF_removeEPSGprefix(datumString)
      break
    endif
  endforeach
  if ~isa(datumString) then begin
    knownData = '(WGS[- ]?84|WGS[- ]?72BE|WGS[- ]?72|NAD[- ]?83|NAD[- ]?27|DHDN_Germany)'
    datumString = stregex(pInfo, knownData, /EXTRACT)
    if datumString eq '' then datumString = !NULL
  endif


  proDict = hubApp_dataImport_ImgFromTIFF_extractProjInfo(isa(projKey) ? projKey : pInfo)
  foreach key, proDict.keys() do begin
    value = proDict[key]
    if isa(value) then mapInfo[key] = value
  endforeach


  ;SET MAP INFO VALUES
 

  ;set defaults
  ;geoInfo
  if ~isa(mapInfo.units) && isa(unitString) then mapInfo.units = unitString
  if ~isa(mapInfo.units) then mapInfo.units = 'meters' ;default
  if ~isa(mapInfo.projection) then mapInfo.projection = 'Arbitrary'
  IF ~isa(mapInfo.datum) then mapInfo.datum = 'WGS84'
  

  mi = OBJ_NEW('hubIOImgMeta_mapInfo')
  ;only commit consistent map infos
  if mi.isConsistentValue(mapInfo) then begin
    result.mapInfo = mapInfo
  endif else begin
    stop
  endelse
  
  return, result
end


;+
; :Hidden:
;-
pro test_hubApp_dataImport_ImgFromTIFF_extractGeoInfo
  parameters = Hash()
  parameters['tileLines'] = 10
  parameters['noShow'] = 0b
  
  
  ;load test images:
  dirExamples = 'D:\Sandbox\GeoTiff\ExamplesIN'
  dirOut = 'D:\Sandbox\GeoTiff\ExamplesOut\'
  
  filesIn = FILE_SEARCH(dirExamples, '*.tif', /FOLD_CASE)
  
  ;FILE_MKDIR, dirOut, /NOEXPAND_PATH
  offset = 0
  for i = 0 + offset, n_elements(filesIn)-1 do begin
    fileIn = filesIn[i]
    bn = FILE_BASENAME(fileIn, '.tif')
    parameters = Hash()
    parameters['inputTIFF'] = fileIn
    parameters['outputImage'] = dirOut+bn+'.bsq'
    print, format='(%"%i %s --> %s")', i, FILE_BASENAME(parameters['inputTIFF']), FILE_BASENAME(parameters['outputImage'])
    
    if i eq 30 then stop
    hubApp_dataImport_ImgFromTIFF_processing, parameters
  endfor


  
  print, 'test done'
end