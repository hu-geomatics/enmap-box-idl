;+
; :Description:
;    Graphical Dialog to collect parameters required for importing (Geo)TIFF - Files using
;    `hubApp_dataImport_ImgFromTIFF_processing`.
;    
;    Returns the following hash::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       inputTIFF   | string | path of TIFF file
;       outputImage | string | path of imported image file
;       ------------+--------+---------------
;
; :Params:
;    settings: in, optional, type = hash
;     Hash to provide GUI relevant parameters (* = optional)::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       title       | string | title of calling GUI application
;                   |        | default: 'Import GeoTIFF'
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       ------------+--------+---------------
;
;
;
;-
function hubApp_dataImport_ImgFromTIFF_getParameters, settings
  if ~isa(settings) then settings = Hash('title','Import GeoTIFF')
  
  hubAMW_program , settings.hubgetValue('groupLeader'), Title = settings.hubGetValue('title')
  hubAMW_inputFilename, 'inputTIFF',   Title='Input TIFF File', value = settings.hubGetValue('inputTIFF')
  hubAMW_outputFilename,'outputImage', Title='Output Image   ' $
        , EXTENSION='bsq' $
        , value = settings.hubGetValue('outputImage', default='TIFFImport.bsq')
  return, hubAMW_manage()
end