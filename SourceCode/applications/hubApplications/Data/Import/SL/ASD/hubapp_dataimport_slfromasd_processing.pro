;+
; :Description:
;    Procedure to import multiple ASD FieldSpec Binary Spectra into a single
;    ENVI Spectral Library. 
;    
; :Params:
;    parameters: in, required, type=hash
;     Parameterization hash containing the following values (* = optional)::
;     
;       key            | type     | description
;       ---------------+----------+---------------
;          inputFiles  | string[] | array of file pathes for all input ASD spectra  
;          outputSL    | string   | path of final spectral library
;        * order       | string   | Order of spectra in 'outputSL'
;                      |          | 'time' order by time of spectrum aquisition
;                      |          | 'filename' order by file name
;       ---------------+----------+---------------
;       
;    settings: in, optional, type = hash
;     Use this hash to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+--------------- 
;
; :TODO: 
;   It is planned to support the INDICO Format (Versions 7 and 8) too.
;
;
;-
pro hubApp_dataImport_SLfromASD_processing, parameters, settings

  hubApp_checkKeys, parameters, ['inputFiles','outputSL']
  
  fileNames = parameters['inputFiles']
  nFiles = n_elements(fileNames)
  
  if nFiles le 0 then message, 'no input files defined'
  reader = hubIOASDReader()

  timeStrings = make_array(nFiles, /String)
  spectraNames = make_array(nFiles, /String)
  
  timeJulDay = make_array(nFiles, /Long)
  header = !NULL
  spectra = !NULL
  
  
  ;read all spectra
  for i=0, nFiles-1 do begin
    reader.setFileName, fileNames[i]
    hdr = reader.getHeader()
    bands = reader.getBands()
    
    if i eq 0 then begin
      headers = replicate(hdr, nFiles)
      spectra = make_array(nFiles, hdr.channels, /Double)
      bands0 = bands
      nBands = n_elements(bands0)
      bandNames = strtrim(bands, 2)
    endif else begin
      if n_elements(bands) ne n_elements(bands0) then message, 'Number of bands/channels is different'
      if total(bands ne bands0) gt 0 then message, 'Wavelength definition is different'
    endelse


    spectraNames[i] = FILE_BASENAME(fileNames[i])
    t = hdr.when
    timeJulDay[i] =julday(t.tm_mon, t.tm_mday, t.tm_year + 1900, t.tm_hour, t.tm_min, t.tm_sec)
    timeStrings[i] = string(format='(%"%4.4i-%2.2i-%2.2i %2.2i:%2.2i:%2.2i")' $
          , t.tm_year+1900, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec)
    spectra[i,*] = reader.getSpectrum()
    headers[i] = hdr 
  endfor
  reader.cleanup
  
  ;Consistency Checks
  if parameters.hubKeywordSet('checkEqualSensorSettings') then begin
    if stdev(headers[*].it) ne 0. then message, 'Integration time is not the same'
    if stdev(headers[*].swir1_gain) ne 0. then message, 'SWIR1 Gain is not the same'
    if stdev(headers[*].swir2_gain) ne 0. then message, 'SWIR2 Gain is not the same'
    if stdev(headers[*].swir1_offset) ne 0. then message, 'SWIR1 Offset is not the same'
    if stdev(headers[*].swir2_offset) ne 0. then message, 'SWIR2 Offset is not the same'
  endif
  
  
  ;Set writing order
  iOrder = indgen(nFiles)
  if parameters.hubIsa('order') then begin
    case STRLOWCASE(parameters['order']) of
      'time': iOrder = sort(timeJulDay)
      'filename': iOrder = sort(spectraNames)
    endcase
  endif
  spectra = spectra[iOrder,*]
  
  ;write spectral library
  outputESL = hubIOSLOutputSpeclib(parameters['outputSL'] $
                                  , numberOfBands=n_elements(bands0))
  outputESL.setMeta, 'wavelength', bands0
  outputESL.setMeta, 'wavelength units', 'Nanometers'
  outputESL.setMeta, 'spectra names', spectraNames[iOrder]
  outputESL.writeData, transpose(spectra)
  outputESL.cleanup

end

;+
; :hidden:
;-
pro test_hubApp_dataImport_SLfromASD_processing
  parameters = Hash()
  p = 'D:\Sandbox\Messprotokoll\In\gi'
  
  parameters['inputFiles'] = p + string(format='(%".%3.3i")', indgen(20))
  parameters['outputSL'] = 'D:\Sandbox\temp\testESL'
  
  hubApp_dataImport_SLfromASD_processing, parameters
  print, 'done'
end