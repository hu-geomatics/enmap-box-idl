;+
; :Description:
;    This function opens a widget and collects the parameterization values required to
;    run `hubApp_dataImport_SLfromASD_processing`.
;
; :Params:
;    settings:
;       Hash with GUI relevant parameters ( * = optional)::
;     
;       key           | type   | description
;       --------------+--------+---------------
;       * groupLeader | long   | set this to the widget ID of a calling widget 
;                     |        | to use the same group leader
;         title       | string | title of calling GUI application
;       --------------+--------+--------------- 
;
;
;
;-
function hubApp_dataImport_SLfromASD_getParameters, settings
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
  hubAMW_pickfile, 'inputFiles'    , TITLE='Input ASD files', /Multiple_Files
  hubAMW_frame ,   Title =  'Output'
  hubAMW_outputFilename, 'outputSL', Title='Output SpecLib ', value=settings.hubGetValue('outputSL')             
  parameters = hubAMW_manage()
  return, parameters

end

;+
; :hidden:
;-
pro test_hubApp_dataImport_SLfromASD_getParameters
  settings = Hash('title','title','groupLeader',!NULL)
  print, hubApp_dataImport_SLfromASD_getParameters(settings)
end