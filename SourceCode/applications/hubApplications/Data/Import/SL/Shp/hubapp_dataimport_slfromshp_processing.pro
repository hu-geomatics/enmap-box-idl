function hubApp_dataImport_SLFromShp_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputShape','inputFeatures','nameColumns' $
                                ,'outputSL']
  if ~isa(settings) then settings = Hash()
  
  nFeatureFiles = n_elements(parameters['inputFeatures'])
  
  mandatoryColumns = parameters.hubGetValue('mandatoryColumns')
  
  ;read ShapeFile
  shpReader= hubIOShapeFileReader(parameters['inputShape'])
  shapeType = shpReader.getShapeType()
  if total(shapeType eq ['POINT','POLYGON']) eq 0 then message, 'Shape file with POINT or POLYGON data required'
  attrTypes = shpReader.getAttributeTypes(/Typecodes)
  attrNames = shpReader.getAttributeNames()
  
  entitiesPerStep = 200
  nEntities = shpReader.getNumberOfEntities()
  if nEntities eq 0 then message, 'Shape file is empty'
  
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressBar(/Cancel $
                , Title=settings.hubGetValue('title', default='Shape To Spectral Library') $
                , Range = [0, nEntities * nFeatureFiles] $
                , Info='Read Shape file...')
  endif
  
  iMandatoryColumns = List()
  if isa(mandatoryColumns) then begin
    foreach column, mandatoryColumns do begin
      iColumn = where(attrNames eq column, /NULL)
      
      if ~isa(iColumn) then message, string(format='(%"Shapefile ''%s'' has no column ''%s''")' $
                                    , FILE_BASENAME(parameters['inputShape']), column)
      iMandatoryColumns.add, iColumn
    endforeach
  endif
  
  iNameColumns = !NULL
  foreach nameColumn, parameters['nameColumns'] do begin
    iColumn = where(attrNames eq nameColumn, /NULL)
    if ~isa(iColumn) then message, string(format='(%"Shapefile ''%s'' has no column ''%s''")' $
                                 , FILE_BASENAME(parameters['inputShape']), labelColumn)  
    iNameColumns = [iNameColumns, iColumn]  
  endforeach
  nNameColumns = n_elements(iNameColumns)

  spectraNames = List()
  writtenSpectra = 0l
  foreach inputFeaturePath, parameters['inputFeatures'], iFeatureImage do begin
    if settings.hubKeywordSet('debug') then print, 'Reader spectra from '+inputFeaturePath
    inputFeatures = hubIOImgInputImage(inputFeaturePath)
    
    handledShapes = 0l
    
    geoHelper = hubGeoHelper(inputFeatures)
    nSamples = inputFeatures.getMeta('samples')
    nLines   = inputFeatures.getMeta('lines')  
    mapInfo = inputFeatures.getMeta('map info')
    dataIgnoreValue = inputFeatures.getMeta('data ignore value')
    if ~isa(mapInfo) then message, 'map info does not exists'
    
    if ~isa(outputSL) then begin
      outputSL = hubIOSLOutputSpeclib(parameters['outputSL'], NOOPEN=settings.hubKeywordSet('noOpen'))
      toCopy = ['fwhm','band names', 'wavelength', 'wavelength units', 'data ignore value']
      foreach tag, toCopy do begin
        if inputFeatures.hasMeta(tag) then outputSL.setMeta, tag, inputFeatures.getMeta(tag)
      endforeach
    endif
    
    inputFeatures.initReader, /Profiles, TILEPROCESSING=0
    shpReader.initReader
    while ~shpReader.readingDone() do begin
       shpData = shpReader.getData()
       
       ;lists to store entity information
       entityInfoList = list()
       
       for iEntity = 0, shpData.numberOfEntities - 1 do begin 
            ;label data
            
            
            hasMandatoryColumns = 1b
            foreach iColumn, iMandatoryColumns do begin
              hasMandatoryColumns and= strtrim(shpData.attributes[iEntity].(iColumn) , 2) ne ''
            endforeach
            if ~hasMandatoryColumns then begin
              continue ;goto next entity
            endif
            
            nameVector = make_array(nNameColumns, /STRING)
            foreach iNameColumn, iNameColumns, i do begin
              name = strtrim(shpData.attributes[iEntity].(iNameColumn) , 2)
              nameVector[i] = name
            endforeach

            
          ;geometry
          geometries = (shpData.geometries)[iEntity]
          foreach geom, geometries do begin
            if geoHelper.imageContains(geom) then begin
               pixelCoord = geoHelper.convertCoordinate2PixelIndex(geom)
               
               ;get 1D-pixel indices
               case geom.type of
                'point' :   pixelIndices = pixelCoord.pixelIndices  
                'polygon' : pixelIndices = polyfillv(pixelCoord.x, pixelCoord.y, nSamples, nLines)
               endcase
               spectrumName = strjoin(nameVector, ' ')
               spectrumName = strjoin(strsplit(spectrumName, ',',/EXTRACT ),'_')
               spectraNames.add, spectrumName
               entityInfoList.add, {pixelIndices:pixelIndices}
               
            endif
          endforeach
        endfor
        
        ;write the data tile-wise
        if n_elements(entityInfoList) gt 0 then begin
          
          
          nPixels = 0l
          foreach entityInfo, entityInfoList do begin
            nPixels += n_elements(entityInfo.pixelIndices)
          endforeach
          
          pixelPositions = make_array(nPixels, /LONG)
          iStart = 0l
          foreach entityInfo, entityInfoList do begin
            nEntityPixels = n_elements(entityInfo.pixelIndices) 
            iEnd = iStart + nEntityPixels - 1
            pixelPositions[iStart:iEnd] = entityInfo.pixelIndices
            iStart += nEntityPixels
          endforeach
          
         
          ;read the data from the feature image
          data = inputfeatures.getData(pixelPositions)
          outputSL.writeData, data
          writtenSpectra += n_elements(temporary(pixelPositions))
        endif
        
        if isa(pBar) then pBar.setProgress, (nEntities * iFeatureImage) + handledShapes 
    endwhile
  
  
    inputFeatures.cleanup
  endforeach  
    
  outputSL.setMeta, 'spectra names', spectraNames.toArray()    
  outputSL.cleanup
  
  shpReader.cleanup
  
  if isa(pBar) then pBar.cleanup

  report = Hash()
  
  
  return, report
end

pro test_hubApp_dataImport_SLFromShp_processing
  parameters = Hash()
  parameters['inputShape'] = 'G:\Project_Berlin_II\BJ\RS_Points\ROI_Points.shp'
  parameters['inputShape'] = 'D:\Berlin_II\Berlin-II-IDLCode\output\ROI_PointsSubset.shp'
  parameters['inputFeatures'] = 'D:\Berlin_II\Daten\Analysis\RandomSampling\HyMap01_Scaled_Subset1.bsq'; $
                                ;,'D:\Berlin_II\Daten\Analysis\RandomSampling\HyMap01_Scaled_Subset2.bsq' $
                                ;]
  parameters['outputSL'] = 'D:\Sandbox\testSL'
  parameters['nameColumns'] = ['Level_BII','file','roi_id']
  
  parameters['file type'] = 'ENVI Standard'
  
  
  print, hubApp_dataImport_SLFromShp_processing(parameters)
end