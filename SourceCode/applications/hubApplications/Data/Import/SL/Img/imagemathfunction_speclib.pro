function imageMathFunction_speclib, imageFilename $
  , ResultFilename=resultFilename, Reflection=reflection

  if keyword_set(reflection) then begin
    description = [$
      'This functions converts all pixels of an image into',$
      'an ENVI Spectral Library' $
      ]
    reflection = {functionClass:'external', description:description}
    return, reflection
  endif

  if ~isa(imageFilename) then message, 'Missing argument: image'

  parameters = hash()
  parameters['inputImage'] = imageFilename
  parameters['outputSL'] = resultFilename
 
  hubApp_dataImport_slfromImg_processing, parameters, hash('noOpen', 1b)
  return, !null
end
