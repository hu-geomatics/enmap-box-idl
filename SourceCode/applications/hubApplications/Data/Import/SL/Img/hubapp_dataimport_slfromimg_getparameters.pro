




;+
; :Description:
;    Explicit Consistency Check function to validate the user input in `hubApp_dataImport_SLfromImg_getParameters`. 
;
;-
function hubApp_dataImport_SLfromImg_getParametersCheck, parameters, message=message
  msg = !NULL
  
  if parameters.hubIsa('inputLabels') then begin
    inputImage = hubIOImgInputImage(parameters['inputImage'])
    
    if inputImage.getMeta('bands') eq 1 then begin
      msg = [msg, 'Input image must have more than one band']
    endif

    if parameters.hubIsa('inputLabel') then begin
      inputLabel = hubIOInputImage(parameters['inputLabel'])
      if ~inputImage.iscorrectSpatialSize(inputLabel.getSpatialSize()) then begin
        msg = [msg, 'Spatial size of label image is different to input image']
      endif
      
      if ~inputLabel.hasMeta('data ignore value') then begin
        msg = [msg, 'Label file requires data ignore value']
      endif
      inputLabel.cleanup
    endif    
    
    if parameters.hubIsa('inputMask') then begin
      inputMask = hubIOImgInputImage(parameters['inputMask'])
      if ~inputImage.iscorrectSpatialSize(inputMask.getSpatialSize()) then begin
        msg = [msg, 'Spatial size of mask image is different to input image']
      endif
      
      if ~inputMask.isMask() then begin
        msg = [msg, 'Mask file is not a correct mask image']
      endif
      inputMask.cleanup
    endif    
    
    
    inputImage.cleanup
  endif
  
  message = msg
  return, ~isa(msg)
end


;+
; :Description:
;    Widget Dialog to collect the parameters required to extract a spectral library out of an image,
;    as well as a related label file.
;
; :Params:
;    settings: in, required, type=Hash()
;      This hash provides GUI relevant parameters, as the `groupLeader` and the `title`.
;
;
;-
function hubApp_dataImport_SLfromImg_getParameters, settings
  hubAMW_program , settings.hubGetValue('groupLeader'), Title = settings['title']
  hubAMW_frame ,   Title =  'Input'
  tsize=130
  hubAMW_inputImageFilename, 'inputImage' , title='Image', tsize=tsize, value=settings.hubGetValue('inputImage')
  hubAMW_inputImageFilename, 'inputLabels', title='Reference Areas', Optional=1, tsize=tsize, value=settings.hubGetValue('inputLabels')
  hubAMW_inputImageFilename, 'inputMask'  , title='Mask', Optional=2, tsize=tsize, value=settings.hubGetValue('inputMask')
  
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputSL'      $
        , Title='SpecLib' $
        , value=settings.hubGetValue('outputSL', default='outputSL') $
        , tsize=tsize             
  parameters = hubAMW_manage(CONSISTENCYCHECKFUNCTION='hubApp_dataImport_SLfromImg_getParametersCheck')
  return, parameters

end

;+
; :Hidden:
;-
pro test_hubApp_dataImport_SLfromImg_getParameters
  settings =Hash('title', 'test title')
  print, hubApp_dataImport_SLfromImg_getParameters(settings)

end


