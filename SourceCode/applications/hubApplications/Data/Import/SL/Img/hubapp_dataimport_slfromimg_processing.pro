;+
; :Description:
;    This procedure creates a spectral library out of an image.
;   
;
; :Params:
;    parameters: in, required, type=hash
;     The parameterization hash containg the following values (* = optional)::
;     
;       key              | type   | description
;       -----------------+--------+---------------
;         inputImage     | string | File path of image where to extract the profiles from
;       * inputLabels    | string | File path of label image to extract additional profile labels from
;                        |        | The label file must have one band only.
;       * inputMask      | string | File path of mask image.
;         outputSL       | string | File path of final output spectral library.  
;       * outputSLLabels | string | File path output labels. 
;                        |        | Default: outputSL + 'Labels' 
;       -----------------+--------+--------------- 
;    settings:in, optional, type=hash
;     Use this hash to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       noOpen      | bool   | set this to avoid opening new created SpecLibs/labe images
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+--------------- 
;
;
;
;-
pro hubApp_dataImport_SLfromImg_processing, parameters, settings
  hubApp_checkKeys, parameters, ['inputImage','outputSL']
  if ~isa(settings) then settings = hash()
  helper = hubHelper()
  
  inputImage = hubIOImgInputImage(parameters['inputImage'])
  
  
  
  spectraNames = inputImage.hasMeta('spectra names') ? inputImage.getMeta('spectra names') : !NULL
  
  nLines = inputImage.getMeta('lines')
  nSamples = inputImage.getMeta('samples')
  nBands = inputImage.getMeta('bands')
   
  imageSet = [inputImage]
  imgDIV = inputImage.getMeta('data ignore value')
  
  if ~settings.hubKeywordSet('noShow') then begin
    pBar = hubProgressBar(TITLE=settings.hubGetValue('title', default = 'Create Spectral Library'))
    pBar.setRange, [0, nLines]
    linesDone = 0l
  endif
  
  tileLines = hubIOImg_getTileLines(image=inputImage)
  
  if parameters.hubIsa('inputLabels') then begin
    inputLabels = hubIOImgInputImage(parameters['inputLabels'])
    if inputLabels.isClassification() then begin
      labelDIV = 0b
      spectraNamesFromClassification = List()
      classNames = inputLabels.getMeta('class names')
    endif else begin
      labelDIV = inputLabels.getMeta('data ignore value') 
    endelse
    
    imageSet = [imageSet, inputLabels]
    outputSLLabels = parameters.hubGetValue('outputSLLabels', default = parameters['outputSL']+'_Labels') 
    outputLabels = hubIOImgOutputImage(outputSLLabels, NOOPEN=settings.hubKeywordSet('noOpen'))
    outputLabels.setMeta, 'description', string(format='(%"label source file: %s")', parameters['inputLabels']) 
  endif
  if parameters.hubIsa('inputMask') then begin
    inputMask = hubIOImgInputImage(parameters['inputMask'], /Mask)
  endif
  
  ;init all input files
  hubApp_manageReaders, imageSet, maskimages=inputMask, tileLines, /Slice, /INIT
  
  if isa(outputLabels) then begin
    writerSettings = inputLabels.getWriterSettings(SetSamples=1)
    outputLabels.copyMeta, inputLabels, /COPYLABELINFORMATION, /COPYFILETYPE
    outputLabels.initWriter, writerSettings
  endif
  
  ;create the speclib writer
  writerSL = hubIOSLOutputSpeclib(parameters['outputSL'], NOOPEN=settings.hubKeywordSet('noOpen'))
  
  
  
  nProfiles = 0ul ;counter for written profiles
  while ~inputImage.tileProcessingDone() do begin
    imageData = inputImage.getData()
    
    
    mask = make_array(n_elements(imageData[0,*]), /Byte, value = 1)   
    ;internal mask (defined by an images data ignore values)
    if isa(imgDiv) then begin
      for iB = 0, nBands-1 do begin
        mask and= imageData[iB, *] ne imgDiv
      endfor
    endif
    
    ;combine with mask defined by the label file
    if isa(inputLabels) && isa(labelDiv) then begin
      labelData = inputLabels.getData()
      mask and= labelData ne labelDiv
    endif
    
    ;combine with explicit defined mask file
    if isa(inputMask) then mask *= inputMask.getData()  
    
    iProfile = where(mask ne 0, /NULL)
    if isa(iProfile) then begin
      ;write spectral values
      writerSL.writeData, imageData[*,iProfile]
      
      ;write label data
      if isa(labelData) then begin
        outputLabels.writeData, labelData[*, iProfile]
      endif
      
      ;count written profiles
      nProfiles += n_elements(iProfile)
      
      ;care on spetra names
      if isa(spectraNamesFromClassification) then begin
        spectraNamesFromClassification.add,  strtrim(classNames[labelData[*, iProfile]],2), /Extract
      endif
      
    endif
    
    linesDone += (n_elements(imageData[0,*]) / nSamples)
    if isa(pBar) then pBar.setProgress, linesDone
  endwhile
  
  if isa(spectraNames) then begin
    if n_elements(spectraNames) eq nProfiles then begin
        writerSL.setMeta, 'spectra names', spectraNames
    endif
  endif
  
  if isa(spectraNamesFromClassification) then begin
     if n_elements(spectraNamesFromClassification) eq nProfiles then begin
        writerSL.setMeta, 'spectra names', spectraNamesFromClassification.toArray()
    endif
  endif
  
  toCopy = ['wavelength', 'wavelength units']
  foreach tag, toCopy do begin
    if inputImage.hasMeta(tag) then writerSL.setMeta, tag, inputImage.getMeta(tag)
  endforeach
  
  ;cleanup & finish everything
  writerSL.cleanup
  if isa(outputLabels) then begin
    ;set image dimensions on true values 
    outputLabels.setMeta, 'samples',1
    outputLabels.setMeta, 'lines',nProfiles
    outputLabels.cleanup
  endif
  
  ;finish all image readers
  hubApp_manageReaders, imageSet, maskimages=inputMask, /FINISH 
  foreach img, imageSet do begin
    img.cleanup
  endforeach
  if isa(inputMask) then inputMask.cleanup
  if isa(pBar) then pBar.cleanup
end


;+
; :Hidden:
;-
pro test_hubApp_dataImport_SLfromImg_processing
 parameters = Hash()
 parameters['inputImage'] = enmapBox_getTestImage('AF_Image')
 parameters['inputLabels'] = enmapBox_getTestImage('AF_LC_Validation')
 parameters['inputLabels'] = enmapBox_getTestImage('AF_LAI_Validation')
 parameters['outputSL']  = 'D:\testOutput'

 hubApp_dataImport_SLfromImg_processing, parameters
 print, 'done!'
end