;+
; :Description:
;    This routine reads a text file stored in ENVI Spectral Library ASCII format and converts it into
;    an ENVI Spectral Library.
;
; :Params:
;    parameters: in, required, type = hash
;     Use this hash to provide the following parameters (* = optional)::
;
;       key                | type   | description
;       -------------------+--------+---------------
;       inputTextFile      | string | filepath of input ESL ASCII file 
;       outputSL            | string | filepath of output spectral library
;       * textLines        | int    | max. number of text lines to read in one step
;                          |        | default = 100
;       * wavelength units | string | units of wavelength
;                          |        | default = 'nanometers' 
;       -------------------+--------+---------------
;      
;       
;
;    settings:  in, optional, type = hash
;     This hash can be used to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+---------------
;
; :Examples:
;   This example shows how to import the text data stored in `ESL_Ascii.txt` 
;   and save it as ENVI Spectral Library::
;   
;       parameters = Hash()
;       parameters['inputTextFile'] = enmapBox_getTestFile('ESL_Ascii')
;       parameters['outputSL'] = FILEPATH('myImportedSL',/TMP)
;       hubApp_dataImport_SLfromASCII_processing_ESL, parameters
;
;
;-
pro hubApp_dataImport_SLfromASCII_processing_ESL, parameters, settings
  hubApp_checkkeys, parameters, ['inputTextFile','outputSL']
  
  if ~isa(settings) then settings = Hash()
  textLines = parameters.hubGetValue('textLines', default=100)
  
  
  if ~isa(settings) then settings = Hash()
  if ~settings.hubKeywordSet('noShow') then begin
    progressbar = hubProgressbar(title=settings.hubGetValue('title', default='Import ASCII ENVI Spectral Library') $
                  , GroupLeader=settings.hubGetValue('groupLeader') $
                  , info='Please wait...')
    progressDone = 0
    progressBar.setInfo, 'Read column definitions...'
  endif
  
  ;I. Read Column Definition
  columnNames = List()
  reader = hubIOASCIIInput(parameters['inputTextFile'])
  reader.initReader, 0, /ExcludeEmptyLines
  if isa(progressBar) then progressBar.setRange, [0, reader.numberOfFileLines()]
  
  firstDataLine = 0
  readColumnDef = 0b
  readSpectrumDef = 0b
  while ~reader.readingDone() do begin
    line = reader.getData(1)
    if reader.getRecentLine() eq 2 then begin
    
      if ~stregex(line, 'ENVI ASCII Plot File', /Boolean, /Fold_case) then begin
        message, "Wrong file format. Must start with 'ENVI ASCII Plot File'"
      endif else begin
        continue
      endelse
    endif
    
    ;Awaiting lines with <columnKey>:<columnDescription>, 
    ;e.g.: Column 1: Wavelength
    ;      Column 2: X:93 Y:143~~1  etc.
    ;columnKey = stregex(line, '^[ ]*Column[ ]*[0123456789]+:', /Extract, /Fold_case)
    if stregex(line, '^  ([0-9]|.)', /BOOLEAN, /FOLD_CASE) then begin
      firstDataLine = reader.getRecentLine()-1
      break    
    endif else begin
      ;parse column names
      columnName = line
      columnKey = stregex(columnName, '^[^:]+:', /Extract, /Fold_case)
      if columnKey ne '' then begin
          columnKeyLength = strlen(columnKey)
        columnName = strtrim(strmid(line, columnkeylength),2)
      endif  
      columnNames.add, columnName
    endelse

  endwhile
  reader.cleanup
  
  ;Do we have a column with wavelength definition?
  iWavelengthColumn = []
  iSpectralColumns = List()
  nColumns = n_elements(columnNames)
  if nColumns eq 0  then message, 'Could not detect any column name (e.g. "Column 1: Wavelength")
  for i = 0, nColumns -1 do begin
    if stregex(columnNames[i], 'Wavelength', /Boolean, /Fold_case) then begin
      iWavelengthColumn = i
      wavelengthValues = List()
    endif else begin
      iSpectralColumns.add, i 
    endelse
  endfor
  iSpectralColumns = uint(iSpectralColumns.ToArray()) 
  spectralValues = List()
  
  ;get spectrum names
  spectraNames = list()
  foreach i, iSpectralColumns do begin
    spectraNames.add, columnNames[i]
  endforeach
 
  
  ;II. Read Values
  reader = hubIOASCIIInputTable(parameters['inputTextFile'])
  
  nSpectra = n_elements(iSpectralColumns)
  nBands = 0l
  
  if isa(progressBar) then begin
    progressDone = firstDataLine
    progressBar.setInfo, 'Read Spectral Values...'
    progressBar.setRange, [0, reader.numberOfFileLines()]
  endif
  
  reader.initReader, textLines $
                   , firstDataLine=firstDataLine $
                   , NumberOfColumns=nColumns $
                   , delimiter='[ \t][ \t]+', /isRegex
                   
  while ~reader.readingDone() do begin
    values =  reader.getData()
    nLines = n_elements(values[0,*])
    nBands += nLines
    
    for iLine = 0, nLines - 1 do begin
      spectralValues.add, float(values[iSpectralColumns, iLine])
    endfor
    
    if isa(iWavelengthColumn) then begin
      for iLine = 0, nLines - 1 do begin
        wavelengthValues.add, values[iWavelengthColumn, iLine]
      endfor
    endif 
    
    if isa(progressBar) then begin
      progressBar.setProgress, reader.getRecentLine()
    endif 
  endwhile
  reader.cleanup
  
  
  spectraNames = columnNames[ispectralcolumns]
  spectra = make_array(nBands, nSpectra)
  for iBand = 0, nBands - 1 do begin
    spectra[iBand,*] = float(spectralValues[iBand]) 
  endfor
  
  ;III. Write Spectra
  eslWriter = hubIOSLOutputSpeclib(parameters['outputSL'])
  
  
  ;Re-Order Spectral Information
  if isa(wavelengthValues) then begin
    wavelengthUnits = parameters.hubGetValue('wavelength units')
    if ~isa(wavellengthUnits) && n_elements(wavelengthValues) gt 0 then begin
      if hubHelper.canConvert(wavelengthValues[-1], 'float') then begin
        wlMax = float(wavelengthValues[-1])
        if wlMax lt 100 then begin
          wavelengthUnits = 'micrometers'
        endif else begin
          wavelengthUnits = 'nanometers'
        endelse
      endif else begin
        wavelengthUnits = 'unknown'
      endelse
    endif
  
  
    eslWriter.setMeta, 'wavelength', float(wavelengthValues.toArray())
    eslWriter.setMeta, 'wavelength units', wavelengthUnits
    
  endif
  
  if n_elements(spectranames) gt 0 then eslWriter.setMeta, 'spectra names', spectranames.toArray()
  
  eslWriter.writeData, spectra
  eslWriter.cleanup ;calls finishWriter implicitly
  if isa(progressBar) then progressBar.cleanup
  

end


;+
; :Hidden:
;-
pro test_hubApp_dataImport_SLfromASCII_processing_ESL 
  parameters = Hash()
  parameters['inputTextFile'] = enmapBox_getTestFile('ESL_Ascii')
  parameters['inputTextFile'] = 'D:\Berlin_II\Daten\Analysis\01_Spectral_Library\02_ASCII_SpecLib\SpectralLibrary_ASCII.txt'
  parameters['outputSL'] = 'D:\ESL_Ascii2SLExample'
  hubApp_dataImport_SLfromASCII_processing_ESL, parameters
end