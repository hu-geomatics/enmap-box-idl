

;+
; :Hidden:
; :Description:
;    Collect further parameters required to read a CSV table.
;
; :Params:
;    dialogSettings: in, required, type=hash
;     GUI relevant settings as prepared in `hubApp_dataImport_SLFromASCII_getParameters`. 
;
;    parameters1: in, required, type=hash
;     The parameters hash returned from `hubApp_dataImport_SLFromASCII_getParameters_DialogBasic`. 
;
;
;
;-
function hubApp_dataImport_SLFromASCII_getParameters_DialogCSV, dialogSettings, parameters1
  previewSizeX = 700
  previewSizeY = 250
  pathLength = 60
  
  ;read preview lines
  valueOrder = ['rows','columns']
  
  
  helper = hubHelper()
  suggest = hubApp_dataImport_fromASCII_getSuggestions(dialogSettings, parameters1['inputTextFile'])
  
  hubAMW_program , dialogSettings.hubgetValue('groupLeader'), Title = dialogSettings['title']
  hubAMW_frame, Title='Parameters'
  hubAMW_label, 'Path: '+helper.getShortenedString(parameters1['inputTextFile'], pathLength)
  hubAMW_text, 'textPreview', Title='Preview' $
             , EDITABLE=0, WRAP=0, /Scroll $
             , YSIZE=previewSizeY, XSize=previewSizeX, VALUE=suggest['previewLines']
  hubAMW_combobox,  'delimiter' $
                  , Title='Delimiter          ', LIST=dialogSettings['delimiterNames'], value=suggest.hubGetValue('delimiterIndex')
  hubAMW_subframe, /Row
  hubAMW_parameter, 'firstDataLine' $
                  , Title='First Data Line    ', /INTEGER, IsGT=0 $
                  , Value=suggest.hubGetValue('firstDataLine') $
                  , SIZE=3, UNIT='  '
                  
  hubAMW_subframe, 'hasWL', Title='No Wavelength Column', /ROW
  hubAMW_subframe, 'hasWL', Title='Has Wavelength Column', /ROW, /SETBUTTON
  hubAMW_parameter, 'iWavelengthColumn' $
                  , Title='Wavelength Column', /INTEGER $
                  , IsGT=0, Value= 1, SIZE=3
  hubAMW_combobox, 'wavelength units', Title='Wavelength units' $
                 , List = ['nanometers','micrometers'], /EXTRACT
  
  hubAMW_frame, Title='Output'
  hubAMW_combobox, 'data type' $
                  , Title='Data Type' $
                  , LIST=dialogSettings['dataTypeDescriptions'] $
                  , EXTRACT = dialogSettings['dataTypeCodes'] $
                  , value = 4
  hubAMW_outputFilename, 'outputSL' $
              , Title    = 'SpecLib  ' $
              , value = dialogsettings.hubGetValue('outputSL', default='spectralLibrary')                 
  
  parameters = hubAMW_manage()
  
  if parameters['accept'] then begin
    iDelimiter = parameters['delimiter']
    parameters['delimiterIsRegex'] = (dialogSettings['delimiterIsRegex'])[iDelimiter]
    parameters['delimiter'] = (dialogSettings['delimiters'])[iDelimiter]
    parameters['firstDataLine'] = parameters['firstDataLine']
    if parameters.hubIsa('iWavelengthColumn') then begin
      parameters['iWavelengthColumn'] = parameters['iWavelengthColumn']-1
    endif
  endif
  
  return, parameters
  

end


;+
; :Hidden:
; :Description:
;    Collects the file path `inputTextfile` and `outputSL`.
;
; :Params:
;    dialogSettings: in, required, type=hash
;     GUI relevant settings as prepared in `hubApp_dataImport_SLFromASCII_getParameters`. 
;
;
;
;-
function hubApp_dataImport_SLFromASCII_getParameters_DialogBasic, dialogSettings
  helper = dialogSettings['helper']
  dataType_names  = helper.getDataTypeInfo(/TypeDescriptions)
  dataType_values = helper.getDataTypeInfo(/TypeCodes)
  
  hubAMW_program , dialogSettings.hubGetValue('groupLeader'), Title = dialogSettings.hubGetValue('title')
  hubAMW_inputFilename, 'inputTextFile' $
              , TITLE = 'Input ASCII File' $
              , value = dialogSettings.hubGetValue('inputTextFile')
  if dialogSettings['importType'] eq 'slASCII_ESL' then begin
      hubAMW_outputFilename, 'outputSL' $
              , Title = 'Output SpecLib  ' $
              , value = dialogsettings.hubGetValue('outputSL', default='spectralLibrary')                 
  endif
  
  parameters = hubAMW_manage()
  return, parameters
end

;+
; :Description:
;    Use this function to show some widget dialogs to collect the required input parameters
;    for running `hubApp_dataImport_SLFromASCII_processing_csvtable` or
;    `hubApp_dataImport_SLFromASCII_processing_esl`.
;
; :Params:
;    appInfo: in, required, type=hash
;     Application Info hash containing the following keys ( * = optional).. 
;     
;       key         | type   | description
;       ------------+--------+---------------
;       importType  | string | type of ASCII SL you want to import
;                   |        | 'slASCII_ESL' = CSV output from ENVI
;                   |        | 'slASCII_CSV' = normal CSV Table with 1 profile per column
;       * title     | string | widget title, default = 'Import Spectral Library'  
;       ------------+--------+---------------
;
;
;-
function hubApp_dataImport_SLFromASCII_getParameters, appInfo
  settings = Hash()
  helper = hubHelper()
  settings['nPreviewLines']     = 40
  settings['delimiters']        = hubApp_getASCIIdelimiters() 
  settings['delimiterIsRegex']  = hubApp_getASCIIdelimiters(/DelimiterIsRegex)
  settings['delimiterNames']    = hubApp_getASCIIdelimiters(/DelimiterDescriptions)
  settings['wavelengthUnits']   = ['unknown','Micrometers','Nanometers']
  settings['dataTypeCodes']        = helper.getDataTypeInfo(/TypeCode) 
  settings['dataTypeDescriptions'] = helper.getDataTypeInfo(/TypeDescription)
  settings['dataTypeNames']        = helper.getDataTypeInfo(/TypeName)
  settings['helper'] = helper
  settings += appInfo
  if ~settings.hubIsa('title') then settings['title'] = 'Import Spectral Library' 
  CANCELED = Hash('accept',0b)
  
  parameters = hubApp_dataImport_SLFromASCII_getParameters_DialogBasic(settings)
  if parameters['accept'] eq 0b then return, CANCELED
  
  ;
  case settings['importType'] of 
    'slASCII_ESL': return, parameters
    'slASCII_CSV': begin
        
        parameters += hubApp_dataImport_SLFromASCII_getParameters_DialogCSV(settings, parameters)
        
                   end
    else: message,'importType: '''+ settings['importType']  +''' is not supported by this routine'
  endcase
  
  if parameters['accept'] eq 0b then return, CANCELED
  return, parameters
end

;+
; :hidden:
;-
pro test_hubApp_dataImport_SLFromASCII_getParameters
  settings = Hash()
  settings['inputTextFile'] = 'D:\Sandbox\ASCII_Data\ESL_Ascii.txt'
  settings['inputTextFile'] = 'D:\Sandbox\ASCII_Data\ESL_Ascii2.hdr'
  settings['inputTextFile'] = 'D:\EnMAP-Box\SourceCode\enmapBox\_resource\testData\speclib\ESL_Ascii.txt'
  settings['outputSL'] = 'D:\testSL'
  settings['importType'] = 'slASCII_ESL'
  settings['importType'] = 'slASCII_CSV'
  
  print, hubApp_dataImport_SLFromASCII_getParameters(settings)

end