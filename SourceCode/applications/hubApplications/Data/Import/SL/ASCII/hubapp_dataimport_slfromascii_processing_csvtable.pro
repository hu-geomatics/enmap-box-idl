;+
; :Description:
;    Creates a spectral library from values in a CSV ASCII Table.
;    The values must be ordered by columns (all values of a 
;    single profile are in the same columm).
;
; :Params:
;    parameters: in, required, type=hash
;     Parameterization hash containing the following keys (* = optional)::
;     
;       key                 | type   | description
;       --------------------+--------+---------------
;       inputTextFile       | string | filepath of CSV Table text file with input values
;       outputSL            | string | filepath of Spectral Library that is to create
;       firstDataLine       | int    | number of text line inputTextfile where the spectral data starts
;       * delimiter         | string | delimiter used to separate column values in inputTextFile
;                           |        | default = string(9b) = tabulator
;       * delimiterIsRegex  | bool   | if set, delimiter is interpreted as regular expression
;                           |        | default = false
;       * decimalSeparator  | char   | character used to separate significant digits
;                           |        | default: '.' e.g. 42.24
;                           |        | set this to ',' to ensure german encoded floating point number will be imported correctly 
;       * iWavelengthColumn | int    | index of text column containing the profile channel spectral bands 
;       * wavelength units  | string | ... e.g. 'nanometers' or 'micrometers' 
;       --------------------+--------+---------------
;       
;    settings: in, optional, type = hash
;     Use this hash to provide optional GUI relevant parameters::
;     
;       key         | type   | description
;       ------------+--------+---------------
;       noShow      | bool   | set on true to avoid showing a progress bar
;       groupLeader | long   | set this to the widget ID of a calling widget 
;                   |        | for using the same group leader
;       title       | string | title of calling GUI application
;       ------------+--------+--------------- 
;
; :Examples:
;   This example shows how to import the text data stored in `SL_ASCII.txt` 
;   and save it as ENVI Spectral Library::
;   
;       parameters = Hash()
;       parameters['inputTextFile'] = enmapBox_getTestFile('SL_ASCII')
;       parameters['outputSL'] = FILEPATH('myImportedSL',/TMP)
;       parameters['firstDataLine'] = 2
;       hubApp_dataImport_SLfromASCII_processing_CSV, parameters
;
;-
pro hubApp_dataImport_SLfromASCII_processing_CSVTable, parameters, settings
  if ~isa(settings) then settings = hash()
  hubApp_checkkeys, parameters, ['inputTextFile','outputSL','firstDataLine']
  
  
  delimiter = parameters.hubGetValue('delimiter', default=string(9b))
  delimiterIsRegex = parameters.hubGetValue('delimiterIsRegex', default=0b)
  
  decimalSepDef = '.' ;the default decimal separator
  decimalSep = parameters.hubGetValue('decimalSeparator', default=decimalSepDef)
  changeDecimalSep = decimalSep ne decimalSepDef
  
  writer = hubIOSLOutputSpeclib(parameters['outputSL'])
  reader = hubIOASCIIInputTable(parameters['inputTextFile'])
  linesToRead = reader.numberOfFileLines() - parameters['firstDataLine'] + 1
  reader.initReader, linesToRead $
    , firstDataLine = parameters['firstDataLine'] $
    , delimiter = delimiter, isRegex = delimiterIsRegex $
    , numberOfColumns = -1
  
  data = reader.getData()
  reader.cleanup
  
  
  ;in case of german-style CSV files: replace ',' by '.' of each cell
  if changeDecimalSep then begin
    for i=0, n_elements(data)-1 do begin
      data[i] = strjoin(strsplit(data[i], decimalSep),decimalSepDef) 
    endfor
  endif
  
  
  
  iSpectralColumns = indgen(n_elements(data[*,0]))
  if parameters.hubIsa('iWavelengthColumn') then begin
    iSpectralColumns = where(iSpectralColumns ne parameters['iWavelengthColumn'], /NULL)
    wavelengths = float(data[parameters['iWavelengthColumn'],*])
    
    
    wavelengthUnits = parameters.hubgetValue('wavelength units') 
    if ~isa(wavelengthUnits) then begin
      range = hubRange(wavelengths)
      wavelengthUnits = 'unknown'
      
      if range[0] gt 200 && range[1] lt 3000 then begin
         wavelengthUnits = 'nanometers'
      endif 
      if range[0] lt 10 then begin
         wavelengthUnits = 'micrometers'
      endif 
    endif
    
    writer.setMeta, 'wavelength', wavelengths
    writer.setMeta, 'wavelength units', wavelengthUnits 
    
  endif 
  
  data = hubmathHelper.convertData(data[iSpectralColumns,*] $
              , parameters.hubGetValue('data type', default='float') $
              , /NoCopy) 
  
  nProfiles = n_elements(data[*,0])
  nBands    = n_elements(data[0,*])
  writer.writeData, transpose(data[ispectralColumns,*])
  writer.cleanup
end

