;+
; :Author: Benjamin Jakimow (benjamin.jakimow[at]geo.hu-berlin.de)
;-

;+
; :Description:
;   This routine controls the program flow of all import routines.
; :Params:
;    applicationInfo : in, optional, type=hash
;       The application info resulting from `enmapBoxUserApp_getApplicationInfo`.
;       Contains at least the key `importType`, which can have the following values::
;       
;         'imgASCIIGrid' -> Import image from text file with ASCII Grid structure. 
;         'imgASCIIList' -> Import image from text file with ASCII List structure. 
;         'imgGeoTIFF'   -> Import (Geo)TIFF image.
;         'imgGDAL'      -> Import a GDAL readable raster. Requires IDL Python bridge.
;         'imgShapeFile' -> Import image from Shape file.
;         'slASCII_ESL'  -> Import spectral library from text file with ENVI Spectral Libray structure.
;                           This you get when exporting a Spectral Libray from ENVI as text file.
;         'slASCII_CSV'  -> Import spectral library from text file with CSV list structure.
;                           Each column contains one spectrum.
;         'slImg'        -> Import spectral library from non-masked image spectra.
;       
;       
;       Furthermore this hash can contain default values for::
;       
;         key         | type   | description
;         ------------+--------+---------------
;         outputImage | string | default path for output Images
;         outputSL    | string | default path for output Spectral Libraries
;         ------------+--------+---------------
;       
;
;-
pro hubApp_dataImport_application, applicationInfo
  
  ; get global settings for this application
  settings = hubApp_getSettings()
  
  
  state = hub_getAppState('hubApp', 'stateHash_dataImport')
  if isa(state) then settings += state
  
  settings += applicationInfo
  
  ;generate default path values in case they do not exist
  if ~settings.hubIsa('outputImage') then settings['outputImage'] = filepath('image',/TMP)
  if ~settings.hubIsa('outputSL') then settings['outputSL'] = filepath('specLib',/TMP) 
  
  ;1. define dialog title for the impiort filter
  case settings['importType'] of
    'imgASCIIGrid' : settings['title'] = 'Import Image from ASCII Grid'
    'imgASCIIList' : settings['title'] = 'Import Image from ASCII List'
    'imgGeoTIFF'   : settings['title'] = 'Import GeoTIFF Image'
    'imgGDAL'      : settings['title'] = 'Import GDAL-readable Raster Image'
    'imgShapeFile' : settings['title'] = 'Rasterize Shape File'
    'slASCII_ESL'  : settings['title'] = 'Import Spectra from ENVI ASCII Table'
    'slASCII_CSV'  : settings['title'] = 'Import Spectra from CSV Table'
    'slASD'        : settings['title'] = 'Import ASD FieldSpec Files'
    'slImg'        : settings['title'] = 'Generate Spectral Library from Image'
    else: message, 'Not implemented importType of ' + settings['importType'] 
  endcase
  
  ;2. Collect required parameters
  case settings['importType'] of
    'imgASCIIGrid'  : parameters = hubApp_dataImport_ImgFromASCII_getParameters(settings)
    'imgASCIIList'  : parameters = hubApp_dataImport_ImgFromASCII_getParameters(settings)
    'imgGeoTIFF'    : parameters = hubApp_dataImport_ImgFromTIFF_getParameters(settings)
    'imgGDAL'       : parameters = hubApp_dataImport_imgFromGDAL_getParameters(settings)
    'imgShapeFile'  : parameters = hubApp_dataImport_ImgFromShp_getParameters(settings)
    'slASCII_ESL'   : parameters = hubApp_dataImport_SLFromASCII_getParameters(settings)
    'slASCII_CSV'   : parameters = hubApp_dataImport_SLFromASCII_getParameters(settings)
    'slASD'         : parameters = hubApp_dataImport_SLfromASD_getParameters(settings)
    'slImg'         : parameters = hubApp_dataImport_SLfromImg_getParameters(settings)
    else: message, 'Not implemented importType of ' + settings['importType'] 
  endcase
   
  ;3. Run the import filter's processing routine
  if parameters['accept'] then begin
    
    hub_setAppState,'hubApp', 'stateHash_dataImport', parameters
    
    
    parameters['importType'] = importType
    parameters['textLines'] = settings.hubGetValue('textLines', default=500)
    parameters['importType'] = settings['importType']
    
    case settings['importType'] of 
      'imgASCIIGrid' : hubApp_dataImport_ImgFromASCII_processing_asciiGrid, parameters, settings
      'imgASCIIList' : hubApp_dataImport_ImgFromASCII_processing_asciiList, parameters, settings
      'imgGeoTIFF'   : hubApp_dataImport_ImgFromTIFF_processing, parameters, settings
      'imgGDAL'      : begin
                       hubGDALTools.gdal_translate, parameters['inputImage'], parameters['outputImage'], 'ENVI'
                       hubProEnvHelper.openImage, parameters['outputImage']
                       end
                       
      'imgShapeFile' : begin
            results = hubApp_dataImport_imgFromShp_processing(parameters, settings)
            if results['numberOfShapes'] gt 0 && results['numberOfWrittenPixels'] eq 0 then begin
            !NULL = DIALOG_MESSAGE(['No spatial overlap between shape file and reference image.' $
                         ,'No label values written to new image.'], /INFORMATION)
            endif
                       end
      'slASCII_ESL'  : hubApp_dataImport_SLfromASCII_processing_ESL, parameters, settings
      'slASCII_CSV'  : hubApp_dataImport_SLfromASCII_processing_CSVTable, parameters, settings
      'slASD'        : hubApp_dataImport_SLfromASD_processing, parameters, settings
      'slImg'        : hubApp_dataImport_SLfromImg_processing, parameters, settings
      else: message, 'Not implemented importType of ' + settings['importType'] 
    endcase
  endif
end

;+
; :Hidden:
;-
pro test_hubApp_dataImport_application
  appInfo = Hash()
  
  importType = 'imgASCIIGrid'
  importType = 'imgASCIIList'
  
  appInfo['inputTextFile'] = 'D:\Sandbox\ASCII_Data\Hymap_Berlin-B_Image.txt'
  appInfo['inputTextFile'] = 'D:\Sandbox\ASCII_Data\Measurements.txt'
  appInfo['outputImg'] = 'D:\Sandbox\ASCII_Data\testOutput'
  
  importType = 'slASCII_CSV'
  appInfo['outputSL'] = 'D:\Sandbox\ASCII_Data\testSLCSV_out'
  appInfo['inputTextFile']         = 'D:\EnMAP-Box\SourceCode\enmapBox\_resource\testData\speclib\ESL_Ascii.txt'
  
  appInfo = Hash()
  importType = 'imgASCIIList'
  importType = 'imgShapeFile'
  importType = 'imgASCIIGrid'
  appInfo['importType'] = importType
  appInfo['inputTextFile'] = 'C:\Users\geo_beja\Documents\tmp\ASCII-GRID_Example.txt'
  ;appInfo['inputTextFile'] = '/Users/benjaminjakimow/Documents/temp/GFZPraktikant/ASCII_FEO_Cal.txt'
  appInfo['outputImage'] = 'C:\Users\geo_beja\Documents\tmp\test.bsq'
  appInfo['title'] = 'Import filter TestAPP: '+importType['importType']
  hubApp_dataImport_application, appInfo

end  
