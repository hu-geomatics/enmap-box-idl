function check_featureClustering_parameterize_getParameters, resultHash, Message=message

  msg=!NULL
  isConsistent = 1

  ;spectral dimension
  inputImage = hubIOImgInputImage((resultHash['inputSampleSet'])['featureFilename'])
  bands = inputImage.getMeta('bands')
  ncluster = resultHash['ncluster']
  
  if ncluster ge bands then begin
    msg = 'Cluster size too big!'
    isConsistent = 0b
  endif

  message=msg
  return, isConsistent
end

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided
;    by the` hubAPI` library).
;
; 
;    
;
;-
function featureClustering_parameterize_getParameters, Title=title, GroupLeader=groupLeader
  
  tsize=75
  defaultValues = hub_getAppState('featureClustering','stateHash_featureClustering',default=hash())
  hubAMW_program, groupLeader, Title=title+': Parameterize'
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleSet, 'inputSampleSet', title='Image', /Masking $
    , ReferenceTitle='Mask ', /ReferenceOptional $
    , Value = defaultValues.hubGetValue('inputFilename') $
    , ReferenceValue = defaultValues.hubGetValue('inputMask') $
    , TSize=tsize
  hubAMW_frame, Title='Parameters'
  hubAMW_parameter, 'randomSamples', Title='draw random samples', Value=1000, IsGE=10, IsLE=10000, /Integer, Optional=1
  hubAMW_checkbox, 'onlyNeighbours', Title=' cluster only adjacent features (adds plots to report)',VALUE=0 
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'clusterIndices', Title= 'File', Extension=extension, TSize=tSize, Value='clusterIndices.fc'
 
  result = hubAMW_manage(/Flat);,CONSISTENCYCHECKFUNCTION='check_featureClustering_parameterize_getParameters')

  if result['accept'] then begin
    parameters = result
    parameters['inputMask'] = parameters['inputSampleSet_labelFilename']
    parameters['inputFilename'] = parameters['inputSampleSet_featureFilename']
    if ~parameters.haskey('randomSamples') and ~isa(parameters['inputMask']) then begin
      ok = dialog_message('Do you really want to calculate the statistics from the entire image cube?', /QUESTION, TITLE='Feature Clustering')
      if ok eq 'No' then begin
        parameters = !null  
      endif
    endif
  endif else begin
    parameters = !null
  endelse
  return, parameters
end

;+
; :Hidden:
;-
pro test_featureClustering_parameterize_getParameters

  ; test your routine

  parameters = featureClustering_parameterize_getParameters(TITLE='Feature Clustering')
  print, parameters

end  
