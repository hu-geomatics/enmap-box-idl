;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `featureClustering_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `featureClustering_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `featureClustering_showReport`).
;
; 
;   
;     
;
;-
pro featureClustering_parameterize_application, GroupLeader=groupLeader
  
 
  title = 'Feature Clustering'
 
  defaultValues = hub_getAppState('featureClustering','stateHash_featureClustering',default=hash())
  parameters = featureClustering_parameterize_getParameters(Title=title, GroupLeader=groupLeader)
  
  if isa(parameters) then begin
    hub_setAppState, 'featureClustering', 'stateHash_featureClustering', parameters
    reportInfo = featureClustering_parameterize_processing(parameters, Title=title, GroupLeader=groupLeader)
   
    featureClustering_parameterize_showReport, reportInfo, GroupLeader=groupLeader, Title=title

    ok = dialog_message('Do you want to extract cluster representatives?', /QUESTION, TITLE='Cluster Representatives')
    if ok eq 'Yes' then begin
      featureClustering_apply_application, Title=title, GroupLeader=groupLeader
    endif
  endif
   
end

;+
; :Hidden:
;-
pro test_featureClustering_parameterize_application

  featureClustering_parameterize_application, GroupLeader=groupLeader

end  
