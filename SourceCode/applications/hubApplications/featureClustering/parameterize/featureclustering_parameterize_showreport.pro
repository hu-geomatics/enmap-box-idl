;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    result : in, required, type=hash
;      Pass the return value of the `featureClustering_processing` function to this argument. 
;      
;   
;
;
;-
pro featureClustering_parameterize_showReport, result, GroupLeader=groupLeader, Title=title

  progressBar = hubProgressBar(Title='Feature Clustering Report', /Cancel)
  nb = n_elements(result['cluster_index'])
  ncluster = max(result['cluster_index'])
  spectrum = result['spectrum']

  loadct, 74, RGB_TABLE=clusterColors
    
  if nb gt 256 then begin
    clusterColors = transpose(temporary(clusterColors))
    clusterColorsReBin = intarr(3,256*(nb/256+1))
    clusterColorsReBin[0,*] =  rebin(reform(temporary(clustercolors[0,*])),256*(nb/256+1),/SAMPLE)
    clusterColorsReBin[1,*] =  rebin(reform(temporary(clustercolors[1,*])),256*(nb/256+1),/SAMPLE)
    clusterColorsReBin[2,*] =  rebin(reform(temporary(clustercolors[2,*])),256*(nb/256+1),/SAMPLE)
    clusterColors = clustercolorsReBin
  endif else begin
    clusterColors = temporary(transpose(clusterColors[0:nb*(256/nb)-1,*]))
    clusterColorsReBin = intarr(3,nb)
    clusterColorsReBin[0,*] =  rebin(reform(temporary(clustercolors[0,*])),nb,/SAMPLE)
    clusterColorsReBin[1,*] =  rebin(reform(temporary(clustercolors[1,*])),nb,/SAMPLE)
    clusterColorsReBin[2,*] =  rebin(reform(temporary(clustercolors[2,*])),nb,/SAMPLE)
    clusterColors = clustercolorsReBin   
  endelse

  RepList = hubReportList(title = 'Feature Clustering', /NoShow )
 
  progressBar.setInfo, 'Creating Report'
  plotsDone = 0
  progressBar.setRange, [0, nb-ncluster-1]
  
  for i=0, nb-ncluster-1 do begin
    rep = RepList.newReport(Title=strcompress(/Remove_all,(n_elements(result['clust_history'])+1-i))+' Clusters', /NoShow)
    if result['onlyNeighbours'] eq 1 then begin
      wavelength = result['wavelength']
      c12_history = (result['c12_history'])[i]+1
      clust_history = (result['clust_history'])[i]+1
      clustering_plot $
            ,spectrum $
            ,clust_history $
            ,c12_history $
            ,wavelength $
            ,rep $
            ,clusterColors
      progressBar.setProgress, plotsDone++
    endif           
  
    clusteringResultTable = hubReportTable()
    cluster_index = (result['clust_history'])[i]
    ;set cluster indexes to the 1...NCLUSTER range
    index_arr = cluster_index[uniq(cluster_index,sort(cluster_index))]
    index_flag = cluster_index*0
    for j=0ULL,n_elements(index_arr)-1 do begin
      tmp = where((cluster_index eq index_arr[j]) and (index_flag ne 1))
      cluster_index[tmp] = j+1
      index_flag[tmp] = 1
    endfor
                
    clusters = indgen(max(cluster_index))+1
    firstColumn=list()
    secondColumn=list()
    
    for k=0,max(cluster_index)-1 do begin
      firstColumn.add, clusters[k]
      secondColumn.add, strcompress(strjoin(where(cluster_index eq k+1)+1))
    endfor
        
    clusteringResultTable.addColumn, firstColumn, NAME='Cluster Nr.'
    clusteringResultTable.addColumn, secondColumn, NAME='Features'
    
    rep.addMonospace, 'max r value: '+ string((result['r_history'])[i])
    rep.addHTML, clusteringResultTable.getHTMLTable(ATTRIBUTESTABLE='border="1"')
    clusteringResultTable.cleanup
  endfor
  toc
   RepList.saveHTML, /Show
   RepList.cleanup
     
  progressBar.cleanup
  
  
end

pro test_featureClustering_parameterize_showReport

  parameters = hash()
;  parameters['inputFilename'] = 'Y:\temp\matthias_cornelius\MODIS_Berlin\NDVI_Smoothed\ndvi_smoothed_stack_2006season_scaled'
  parameters['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
;  parameters['randomSamples'] = 500
  parameters['inputMask'] = !NULL;hub_getTestImage('Hymap_Berlin-A_Mask')
;  parameters['ncluster'] = 10
  parameters['onlyNeighbours'] = 1
  parameters['clusterIndices'] = filepath('clusterInfo.fc', /TMP)

  
  result = featureClustering_parameterize_processing(parameters)
  featureClustering_parameterize_showReport, result
  print, result
end