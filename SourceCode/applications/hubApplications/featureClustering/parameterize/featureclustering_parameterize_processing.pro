pro clustering_plot $
    ,spectrum $
    ,clust_history $
    ,c12_history $
    ,wavelength $
    ,rep $
    ,clusterColors
   
  endlist = list()
  for i=0, n_elements(clust_history)-1 do begin
    value = clust_history[i]
    if i+1 eq n_elements(clust_history) and clust_history[i] ne clust_history[i-1] then begin
      endlist.add, i
      break
    endif
    while value eq clust_history[i+1] do begin
      i = i+1
      if i+1 eq n_elements(clust_history) then begin
        endlist.add, i
        break
      endif
    endwhile
    if i+1 ne n_elements(clust_history) then begin
      endlist.add, i
    endif
  endfor
  endArray = endlist.toArray()
  startArray = [0, endArray[0:-2]+1]
  if isa(wavelength) then begin
    startPolygonWL = wavelength[startArray]
    endPolygonWL = wavelength[endArray]
    
    deltaPlus = (startPolygonWL[1:-1]-endPolygonWL[0:-2])/2
    deltaPlus = [deltaPlus, deltaPlus[-1]] < (10./1000)
    
    deltaMinus = (startPolygonWL[1:-1]-endPolygonWL[0:-2])/2
    deltaMinus = [deltaMinus[0], deltaMinus] < (10./1000)
    
    xRight = endPolygonWL+deltaPlus
    xLeft = startPolygonWL-deltaMinus
  
    window = window(DIMENSIONS=[1000,300],/BUFFER)
    window.refresh, /Disable
    spectrumPlot=plot(wavelength,spectrum,DIMENSIONS=[1000,300],XRANGE=[min(wavelength),max(wavelength)],/CURRENT,MARGIN=[.1,.1,.1,.1])
    
    barsList = list()    
    for i=0, n_elements(startArray)-1 do begin
      if startPolygonWL[i] ne endPolygonWL[i] then begin
        polygons = polygon([xLeft[i],xRight[i],xRight[i],xLeft[i]] $
          ,[0,0,max(spectrum)*1.1,max(spectrum)*1.1] $
          ,/DATA,TRANSPARENCY=30 $
          ,FILL_COLOR=clusterColors[*,(clust_history(uniq(clust_history)))[i]-1])  
      endif else begin
        barsList.add, startPolygonWL[i]
      endelse
    endfor
    if n_elements(barsList) gt 0 then !null = barplot(barsList.toarray(),replicate(max(spectrum)*1.1,n_elements(barsList.toarray())),/OVERPLOT,FILL_COLOR='black',WIDTH=.001)

  endif else begin
    window = window(DIMENSIONS=[1000,300],/BUFFER)
    window.refresh, /Disable
    spectrumPlot=plot(indgen(n_elements(clust_history))+1,spectrum,DIMENSIONS=[1000,300],XRANGE=[0,n_elements(clust_history)+1], /CURRENT,MARGIN=[.1,.1,.1,.1])
  
    barsList = list()    
    for i=0, n_elements(startArray)-1 do begin
      if startArray[i] ne endArray[i] then begin
        polygons = polygon([startArray[i]+1,endArray[i]+1,endArray[i]+1,startArray[i]+1] $
          ,[0,0,max(spectrum)*1.1,max(spectrum)*1.1] $
          ,/DATA,TRANSPARENCY=30 $
          ,FILL_COLOR=clusterColors[*,(clust_history(uniq(clust_history)))[i]-1])
      endif else begin
         barsList.add, startArray[i]+1
      endelse
    endfor
    if n_elements(barsList) gt 0 then !null = barplot(barsList.toarray(),replicate(max(spectrum)*1.1,n_elements(barsList.toarray())),/OVERPLOT,FILL_COLOR='black',WIDTH=.001)
  endelse
   
  spectrumPlotImage = transpose(spectrumPlot.CopyWindow(WIDTH=1000), [1,2,0])
  spectrumPlot.close
  rep.addImage, spectrumPlotImage, ''
  
end

pro featureClustering_processing_initialize $
  ,cluster_index $
  ,cluster_similarity $
  ,features $
  ,parameters $
  ,spectrum $
  ,GroupLeader=groupLeader $
  ,nSamples

  ;on_error,2

  if isa(features) then begin 
    nb = n_elements(features[*,0])
    ns = n_elements(features[0,*])
    cluster_index      = lindgen(nb)
    cluster_similarity = fltarr(nb,nb)-1
    str=['Number of Features = '+strcompress(/REMOVE_ALL,nb) $
        ,'Number of Samples  = '+strcompress(/REMOVE_ALL,ns)]
    progressBar = hubProgressBar(Title='Feature Clustering Initialization', /Cancel,groupLeader=groupleader)
    progressBar.setInfo, str
    for i=0UL,nb-2 do for k=i+1,nb-1 do begin
      progressBar.setProgress, float(i)/(nb-2)
      r_2 = correlate(features[i,*], features[k,*])^2
      cluster_similarity[i,k] = r_2
      cluster_similarity[k,i] = r_2
    endfor 
     progressBar.hideBar 
     progressBar.cleanup
  endif else begin
    inputImage = hubIOImgInputImage(parameters['inputFilename'])
    nb = inputImage.getMeta('bands')
    nl = inputImage.getMeta('lines')
    numberOfTileLines = hubIOImg_getTileLines(image=inputImage)

    inputImage.initReader, numberOfTileLines, /SLICE, /TILEPROCESSING
  
    str=['Number of Features = '+strcompress(/REMOVE_ALL,nb) $
      ,'Number of Samples  = '+strcompress(/REMOVE_ALL,nSamples)]
    progressBar = hubProgressBar(Title='Feature Clustering Initialization', /Cancel,groupLeader=groupleader)
    progressBar.setInfo, str

    incMeans = objarr(nb)
    for i=0, nb-1 do incMeans[i] = hubMathIncMean()
    while ~inputImage.tileProcessingDone() do begin
      data = inputImage.getData()
      for i=0, nb-1 do (incMeans[i]).addData, data[i,*]
    endwhile

    ;initialize 2-d hubMathIncCorrelation object array with band means
    incCors = objarr(nb,nb)
    for i=0, nb-1 do begin
      for j=i, nb-1 do begin
        incCors[i,j] = hubMathIncCorrelation((incMeans[i]).getResult(),(incMeans[j]).getResult())
      endfor
    endfor

    inputImage.cleanup
    
    inputImage = hubIOImgInputImage(parameters['inputFilename'])
    dataIgnoreValue = inputImage.getMeta('data ignore value')
    inputImage.initReader, numberOfTileLines, /SLICE, /TILEPROCESSING
    
    tilesDone = 0
    progressbar.setRange, [0,nl]
    progressBar.setProgress,0
    tic
    while ~inputImage.tileProcessingDone() do begin
   
      data = inputImage.getData()
      for i=0UL, nb-2 do begin
        for j=i+1, nb-1 do begin
          if isa(dataignorevalue) then validIndices = where(data[i,*] ne dataIgnoreValue and data[j,*] ne dataIgnoreValue, /NULL)
          if isa(validindices) then begin
            (incCors[i,j]).addData, data[i,validIndices], data[j,validIndices]
            spectrum = data[*,validIndices[0]]
          endif else begin
            (incCors[i,j]).addData, data[i,*], data[j,*]
            spectrum = data[*,n_elements(data[0,*])/2]
          endelse
        endfor
      endfor
      progressBar.setProgress,tilesDone
      tilesDone+= numberOfTileLines
      tilesDone<= nl
    endwhile
    toc

    inputImage.cleanup
   
    cluster_index      = lindgen(nb)
    cluster_similarity = fltarr(nb,nb)-1

    for i=0UL, nb-2 do begin
      for j=i+1, nb-1 do begin
        r_2 = ((incCors[i,j]).getResult())^2
        cluster_similarity[i,j] = r_2
        cluster_similarity[j,i] = r_2
      endfor
    endfor
    progressBar.hideBar
    progressBar.cleanup
  endelse
end

pro feature_clustering_combine_cluster $
    ,cluster_index $
    ,cluster_similarity $
    ,r_matrix $
    ,parameters $
    ,c1 $
    ,c2
  
  on_error,2
  nb = n_elements(cluster_index)
  onlyNeighbours = parameters['onlyNeighbours']
  if onlyNeighbours eq 1 then begin
    cluster_similarity_neighboursOnly = cluster_similarity
    for i=0UL,nb-2 do for k=i+1,nb-1 do begin
      if cluster_similarity[i,k] eq -1 then continue
      featuresI = where(cluster_index eq i)
      iLast = featuresI[-1]
      featuresK = where(cluster_index eq k)
      kFirst = featuresK[0]
      isNeighbour = (iLast+1) eq kFirst
      if ~isNeighbour then begin
        cluster_similarity_neighboursOnly[i,k] = -1
        cluster_similarity_neighboursOnly[k,i] = -1
      endif
    endfor
    max_similarity = max(cluster_similarity_neighboursOnly)
    tmp = (where(cluster_similarity_neighboursOnly eq max_similarity))[0]
  endif else begin
    max_similarity = max(cluster_similarity)
    tmp = (where(cluster_similarity eq max_similarity))[0]
  endelse
  
  tmp = [tmp mod nb, tmp / nb]
  c1 = min(tmp)
  c2 = max(tmp)
   
  ;select all features of C1 and C2
  c12_features = where((cluster_index eq c1) or (cluster_index eq c2))
  
  ;update CLUSTER_INDEX and CLUSTER_SIMILARITY
  cluster_index[where(cluster_index eq c2)] = c1
  cluster_similarity[c1,*] = -1
  cluster_similarity[*,c1] = -1
  cluster_similarity[c2,*] = -1
  cluster_similarity[*,c2] = -1
  ;calculate similarity between C12 and old clusters
  for i_c=0UL,nb-1 do begin
    if i_c eq c1 then continue
    if i_c eq c2 then continue
    if (where(cluster_index eq i_c))[0] eq -1 then continue
    i_c_features = where(cluster_index eq i_c)
    meanMatrix=fltarr(nb,nb)-1
    for i=0,n_elements(c12_features)-1 do begin
      for j=0,n_elements(i_c_features)-1 do begin
        meanMatrix[i,j]=r_matrix[c12_features[i],i_c_features[j]]
      endfor
    endfor
    meanR = mean(meanmatrix[where(meanmatrix ge 0)])
    cluster_similarity[c1,i_c] = meanR
    cluster_similarity[i_c,c1] = meanR
  endfor
end

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`featureClustering_showReport`).
;
; :Returns:
;   Returns a hash with features' corresponding cluster numbers.
;
; :Params:
;    parameters : in, required, type=hash
;      A hash that contains the following parameters::
;
;        keys                      | type     | description
;        --------------------------+----------+--------------------------------------------------
;        inputFilename             | string   | file path of feature image
;        clusterIndices            | string   | file path of created file with cluster indices
;        onlyNeighbours            | number   | 0 = clusters can be formed by non-adjacent features
;                                  |          | 1 = clusters only formed by adjacent features
;                                          
;        optional keys             | type     | description
;        --------------------------+----------+--------------------------------------------------
;        inputMask                 | string   | file path of mask/reference image
;        randomSamples             | number   | number of samples drawn from input image
;        --------------------------+----------+--------------------------------------------------
;   
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
function featureClustering_parameterize_processing, parameters, Title=title, GroupLeader=groupLeader

;--begin get data


  inputImage = hubIOImgInputImage(parameters['inputFilename'])
  bands  = inputImage.getMeta('bands') 
  numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)
  ;read wavelengths for use in report, returns !NULL if not defined
  wavelength = inputImage.getMeta('wavelength')
  
  ;read image data from sample set
  parameters['deleteStrataMask'] = 1b
  parameters['inputImage']=parameters['inputFilename']
  parameters['ignoreValueHandling'] = 'ANY'
  ;find valid data
  strataMask = hubApp_randomSampling_getStrataMask(parameters)
  if parameters.HasKey('randomSamples') then begin
    nSamples = parameters['randomSamples']
    parameters['inputImage']=strataMask['stratificationMask']
    parameters['stratificationMask']=strataMask['stratificationMask']
    parameters['outputSamples']=FILEPATH('outputSamples',/TMP)
    if isa(parameters['inputMask']) then begin
      if (strataMask['strataCounts'])[1] lt nSamples then nSamples = (strataMask['strataCounts'])[1]
    endif
    parameters['samplesPerStratum']=[0,nSamples]
    hubApp_randomSampling_processing,parameters,NoShow=NoShow
    randomPoints = hubIOImgSampleSetForDensity(parameters['inputFilename'],parameters['outputSamples'])
  endif else begin
    randomPoints = hubIOImgSampleSetForDensity(parameters['inputFilename'],strataMask['stratificationMask'])
    nSamples = (strataMask['strataCounts'])[1]
  endelse
  if isa(parameters['inputMask']) or parameters.HasKey('randomSamples') then begin
    features = (randomPoints.getSample()).FEATURES
  endif

  inputImage.cleanup

;-- end get data

 
  featureClustering_processing_initialize $
    ,cluster_index $
    ,cluster_similarity $
    ,features $
    ,parameters $
    ,spectrum $
    ,GroupLeader=groupLeader $
    ,nSamples
 
  r_matrix=cluster_similarity
  nb=n_elements(cluster_index)
  clust_history = list()
  c12_history = list()
  r_history = list()
  
  for i=1,nb-1 do begin
    feature_clustering_combine_cluster $
      ,cluster_index $
      ,cluster_similarity $
      ,r_matrix $
      ,parameters $
      ,c1 $
      ,c2
      
    r_history.add, max(cluster_similarity)
    clust_history.add, cluster_index
    c12_history.add, [c1,c2]
    if nb-i eq 2 then break
  endfor
  
  ;set cluster indexes to the 1...NCLUSTER range
  index_arr = cluster_index[uniq(cluster_index,sort(cluster_index))]
  index_flag = cluster_index*0
  
  for i=0ULL,n_elements(index_arr)-1 do begin
    tmp = where((cluster_index eq index_arr[i]) and (index_flag ne 1))
    cluster_index[tmp] = i+1
    index_flag[tmp] = 1
  endfor

  result = hash()
  result['r_history'] = r_history
  result['clust_history'] = clust_history
  result['c12_history'] = c12_history
  result['spectrum'] = isa(features) ? features[*,5] : spectrum
  result['cluster_index'] = cluster_index
  result['featureNr'] = indgen(nb)+1
  result['onlyNeighbours'] = parameters['onlyNeighbours']
  result['bands'] = bands
  if result['onlyNeighbours'] eq 1 then result['wavelength'] = wavelength
  save, result, FILENAME=parameters['clusterIndices']
  
  return, result

end

;+
; :Hidden:
;-
pro test_featureClustering_parameterize_processing

  ; test your routine
  
  parameters = hash() 
  parameters['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['inputMask'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
;  parameters['randomSamples'] = 500
;  parameters['inputMask'] = !NULL
  parameters['onlyNeighbours'] = 0
  parameters['clusterIndices'] = FILEPATH('clusterIndices.fc',/TMP)
  clusterResult = featureClustering_parameterize_processing(parameters, Title=title, GroupLeader=groupLeader)
 
;  applyParameters = hash()
;  applyParameters['numberOfClusters'] = 5
;  
;  featureClustering_apply_processing, applyParameters
  print, clusterResult

end  
