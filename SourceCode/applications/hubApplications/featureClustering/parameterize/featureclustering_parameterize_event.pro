;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application event handler. It is called when a user starts the application 
;    from the EnMAP-Box (or ENVI) menu bar. It sets up a default error handler 
;    (see `huberrorcatch` batch file provided by the `hubAPI` library)
;    and starts the applications main procedure `featureClustering_application`.
;    The error handler catches every unhandled error and displays the traceback report
;    on the screen.
;
; :Params:
;    event: in, required, type=button event structure
;-
pro featureClustering_parameterize_event, event
  
  @huberrorcatch

  featureClustering_parameterize_application, GroupLeader=event.top
  
end
