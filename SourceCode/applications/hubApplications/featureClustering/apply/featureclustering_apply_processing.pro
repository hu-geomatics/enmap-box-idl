;+
; :Description:
;    Use this procedure for extracting cluster representatives using the cluster indices.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;      
;        keys                      | type     | description
;        --------------------------+----------+--------------------------------------------------
;        clusterIndices            | string   | file path for model with cluster indices
;        inputFilename             | string   | file path of feature image
;        outputFilename            | string   | file path of created image with reduced number of features
;        representatives           | number   | 0 = generate image with mean representatives
;                                  |          | 1 = generate image with random representatives  
;        numberOfClusters          | number   | number of clusters used 
;    
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro featureClustering_apply_processing, parameters, Title=title, GroupLeader=groupLeader

  progressBar = hubProgressBar(Title='Feature Clustering Apply', /Cancel)
  restore, FILENAME=parameters['clusterIndices']
  clust_history_index = n_elements(result['clust_history'])-parameters['numberOfClusters']+2
  ncluster = parameters['numberOfClusters']
  
  cluster_index = (result['clust_history'])[clust_history_index-1]
  ;set cluster indexes to the 1...NCLUSTER range
  index_arr = cluster_index[uniq(cluster_index,sort(cluster_index))]
  index_flag = cluster_index*0

  for i=0ULL,n_elements(index_arr)-1 do begin
    tmp = where((cluster_index eq index_arr[i]) and (index_flag ne 1))
    cluster_index[tmp] = i+1
    index_flag[tmp] = 1
  endfor
  
  inputImage  = hubIOImgInputImage(parameters['inputFilename'])
  outputImage = hubIOImgOutputImage(parameters['outputFilename'])
  numberOfTileLines = hubIOImg_getTileLines(Image=inputImage)
  inputImage.initReader, numberOfTileLines, /Slice, /TileProcessing, DataType='float'
  outputImage.copyMeta, inputImage,/COPYSPATIALINFORMATION
  writerSettings = inputImage.getWriterSettings(SetBands=ncluster)
  outputImage.initWriter, writerSettings
  outputImage.setMeta, 'data ignore value', inputImage.getMeta('data ignore value')
  
  if parameters['representatives'] eq 0 then begin
    outputImage.setMeta, 'description', 'Cluster mean representatives of ' + parameters['inputFilename']
    while ~inputImage.tileProcessingDone() do begin
      sliceTileData = inputImage.getData()
      means = fltarr(ncluster,n_elements(sliceTileData[0,*]))
      for i=1, ncluster do begin
        clusterData = sliceTileData[where(cluster_index eq i),*]
        means[i-1,*] = mean(clusterData,DIMENSION=1)
      endfor
    outputImage.writeData, means
    endwhile
  endif
  
  if parameters['representatives'] eq 1 then begin
    outputImage.setMeta, 'description', 'Cluster random representatives of ' + parameters['inputFilename']
    clusterRepresentatives = indgen(ncluster)
    generalRepresentatives = intarr(ncluster)
    for i=1,ncluster do begin
      clusterRepresentatives[i-1]= randomu(seed,1,/LONG) mod n_elements(where(cluster_index eq i))
      indices = where(cluster_index eq i)
      generalRepresentatives[i-1] = indices[clusterRepresentatives[i-1]]
    endfor
    while ~inputImage.tileProcessingDone() do begin
      sliceTileData = inputImage.getData()
      randomFeatures = fltarr(ncluster,n_elements(sliceTileData[0,*]))
      randomFeatures = sliceTileData[generalRepresentatives,*]
      outputImage.writeData, randomFeatures
    endwhile
  endif
 progressBar.cleanup  
 outputimage.cleanup
 inputimage.cleanup
   
end

;+
; :Hidden:
;-
pro test_featureClustering_apply_processing

  ; train a small model
  parameters = hash() 
  parameters['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['randomSamples'] = 500
  parameters['inputMask'] = !NULL
  parameters['onlyNeighbours'] = 0
  parameters['clusterIndices'] = FILEPATH('clusterIndices.fc',/TMP)
  result = featureClustering_parameterize_processing(parameters, Title=title, GroupLeader=groupLeader)
  print, result
  
  p = hash()
  p['inputFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  p['outputFilename'] = FILEPATH('outputImage',/TMP)
  p['clusterIndices'] = parameters['clusterIndices']
  p['representatives'] = 0
  p['numberOfClusters'] = 10
  
  featureClustering_apply_processing, p
 
end  
