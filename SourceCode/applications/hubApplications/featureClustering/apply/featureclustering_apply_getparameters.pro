function check_featureClustering_apply_getParameters, resultHash, Message=message

  msg=!NULL
  isConsistent = 1
  restore, FILENAME=resultHash['clusterIndices']
 
  ;spectral dimension
  applyImage = hubIOImgInputImage(resultHash['inputFilename'])
  if applyImage.isCorrectSpectralSize(result['bands']) ne 1 then begin
    msg = 'Spectral dimensions of the model and the image do not match'
    isConsistent = 0b
  endif
  
  message=msg
  return, isConsistent
end

;+
; :Author: Matthias Held
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided
;    by the` hubAPI` library).
;
; 
;-
function featureClustering_apply_getParameters, defaultValues, GroupLeader=groupLeader

  tsize=70
  defaultValues = hub_getAppState('featureClustering','stateHash_featureClustering',default=hash())
  hubAMW_program, Title='featureClustering: Apply Model', groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputImageFilename, 'inputFilename', TITLE='Image', value=defaultValues.hubGetValue('inputFilename'), tsize=tsize
;  hubAMW_inputSampleSet, 'inputSampleSet', title='Image', /Masking $
;  , ReferenceTitle='Mask ', /ReferenceOptional $
;  , Value = defaultValues.hubGetValue('inputFilename') $
;  , ReferenceValue = defaultValues.hubGetValue('inputMask') $
;  , TSize=tsize
  hubAMW_inputFilename,'clusterIndices', Title='Model', tsize=tsize, Value=defaultValues.hubGetValue('clusterIndices')
  hubAMW_frame, Title='Parameters'
  hubAMW_parameter,'numberOfClusters', Title='Number of clusters', /Integer, Value=5, ISGE=2
  hubAMW_label, 'Choose option to extract cluster representatives'
  hubAMW_subframe, 'representatives', Title='cluster means', /Row, /SetButton
  hubAMW_subframe, 'representatives', Title='random features', /Row
 ;hubAMW_subframe, 'representatives', Title='principal components', /Row
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputFilename', Title='Image',$
    Value=filepath('clusterRepresentatives', /TMP), TSize=tSize
  amwResult = hubAMW_manage(/FLAT, CONSISTENCYCHECKFUNCTION='check_featureClustering_apply_getParameters')

  if amwResult['accept'] then begin 
    result = amwResult
  endif else begin
    result = !NULL
  endelse

  return, result
  
end

;+
; :Hidden:
;-

pro test_featureClustering_apply_getParameters

  parameters = featureClustering_apply_getParameters()
  print, parameters

end  
