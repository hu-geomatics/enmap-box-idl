;+
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro featureClustering_apply_application, Title=title, GroupLeader=groupLeader

 
  defaultValues = hub_getAppState('featureClustering','stateHash_featureClustering',default=hash())
  result = featureClustering_apply_getParameters(defaultValues, GroupLeader=groupLeader)
  if isa(result) then begin
    hub_setAppState, 'hubApp_featureClustering', 'stateHash_featureClustering', result
    stopWatch = hubProgressStopWatch()
    featureClustering_apply_processing, result, Title=title, GroupLeader=groupLeader
    stopWatch.showResults, title='title', description='Calculations completed.'
  endif 
  
  
end
