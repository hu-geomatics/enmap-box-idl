; EnGeoMAP - base algorithm
; Based on: Algorithms from Christian Mielke (CM), Nina Boesche (NB), Christian Rogass (CR) for mineral identification
; Courtesy: GFZ Potsdam, 2016
; Change Log:
; 06.01.2016; CM, CR; first version
; 07.01.2016; CR; debugging 
; 08.01.2016; CR; added 1nm interpolation and bbl functionality
; 11.01.2015; CR; included ratio and sum feature retrieval, BVLS unmixing
; 12.01.2016; CR; Unmixing debugging
; 13.01.2016; CR; Bugfixing classification - pos error / cleanup
; 14.01.2016; CR; added output functionality
; 15.01.2016; CR; bugfixing
; 18.01.2016; CR; added map info export
; 06.03.2016; CR, NB, MH; some I/O bug fixing, progress bar integration and code cleanup
; Contact: name.surname@gfz-potsdam.de
; Version: 2.009

pro engeomap_base,$
    dat_name=dat_name,$; name of the file that shall be analysed
    lib_name=lib_name,$; name of the library that is used for the analysis
    bbl_wvl = bbl_wvl,$; bad bands to exclude
    fit_threshold = fit_threshold, $; minimum fitting threshold
    cont_removal_type = cont_removal_type, $;  0 means ratio, 1 means sum; 2 means both sum and ratio
    cont_max_iter = cont_max_iter,$ ; gives the maximum number of iterations for continuum removal
    flag_1nm = flag_1nm,$ ; inertpolate to 1 nm lib and unknown spectrum - highly recommended
    unmix_type = unmix_type,$ ; type of unmixing
    fitmethod=fitmethod,$; type of fitting - will be extended from version to version
      gfzlib=  gfzlib,$; uses a hard coded lookup table for the gfz lib
      outdir=outdir, mask_name=mask_name
    
COMPILE_OPT idl2 


; setting defaults
!except = 0
  dat_name                  =       ~n_elements(dat_name)?                dialog_pickfile()         :           dat_name
  lib_name                  =       ~n_elements(lib_name)?                dialog_pickfile()         :           lib_name    
  bbl_wvl                   =       ~n_elements(bbl_wvl)?                 [1300,1450,1750,2010]     :           bbl_wvl
  fit_threshold             =       ~n_elements(fit_threshold)?           [0.001,0.001]             :           0>fit_threshold    
  cont_removal_type         =       ~n_elements(cont_removal_type)?       2                         :           cont_removal_type; 0 means ratio, 1 means sum; 2 mean both
  cont_max_iter             =       ~n_elements(cont_max_iter)?           10                        :           1>cont_max_iter; 0 means ratio, 1 means sum; 2 mean both
  flag_1nm                  =       ~n_elements(flag_1nm)?                0                         :           flag_1nm
  unmix_type                =       ~n_elements(unmix_type)?              0                         :           unmix_type
  fitmethod                 =       ~n_elements(fitmethod)?               0                         :           0>fitmethod
  gfzlib                    =       ~n_elements(gfzlib)?                  1                         :           gfzlib
 ;stop
  
  if dat_name eq '' or lib_name eq '' or outdir eq '' then return
  
  dat_meta = hubIOImgInputImage(dat_name)
  lib_meta = hubIOImgInputImage(lib_name)
  if mask_name ne '' then begin
    mask_meta = hubIOImgInputImage(mask_name)
    mask_meta.initreader,/cube
    mask = mask_meta.getdata()
  endif
  
  dat_ns = dat_meta.getmeta('samples')
  dat_nl = dat_meta.getmeta('lines')
  dat_nb = dat_meta.getmeta('bands')
  dat_wl = dat_meta.getmeta('wavelength')
  dat_wu   = dat_meta.getmeta('wavelength units')
  map_info = dat_meta.getmeta('map info')
  
  lib_ns = lib_meta.getmeta('samples')
  lib_nl = lib_meta.getmeta('lines')
  lib_nb = lib_meta.getmeta('bands')
  lib_wl = lib_meta.getmeta('wavelength')
  lib_spec_names = lib_meta.getmeta('spectra names')
  lib_wu   = lib_meta.getmeta('wavelength units')
  
  dat_meta.initreader,/cube
  lib_meta.initreader,/cube
  
  ; reading data
  dat = dat_meta.getdata();make_array(dat_ns,dat_nl,dat_nb,type=dat_data_type)
  lib = lib_meta.getdata();make_array(lib_ns,lib_nl,lib_nb,type=lib_data_type)
  
  ; if mask is missing try to build own
  mask = ~n_elements(mask)? total(dat,3,/nan) ne 0 : mask
  lib = reform(reform(lib),lib_nl,1,lib_nb,/over);reform(transpose(lib),lib_nl,1,lib_ns)
  lib_sz = size(lib,/dimensions)
  lib_ns = lib_sz[0]
  lib_nl = lib_sz[1]
  lib_nb = lib_sz[2]
  
  if dat_meta.getmeta('file type') ne 'envi standard' then begin
    dat = reform(transpose(dat),dat_nl,1,dat_ns)
    dat_sz = size(dat,/dimensions)
    dat_ns = dat_sz[0]
    dat_nl = dat_sz[1]
    dat_nb = dat_sz[2] 
  endif
  
  ;interpol data to 1nm 
  run=0ul
  if flag_1nm then begin
    new_wl = findgen(dat_wl[dat_nb-1l] - dat_wl[0]+1) + dat_wl[0]
    lib_nb = n_elements(new_wl)
    lib2 = fltarr(lib_ns,lib_nl,lib_nb)
    for x=0l,lib_ns-1l do for y=0l,lib_nl-1l do begin
      lib2[x,y,*] = interpol(lib[x,y,*],lib_wl,new_wl)
      run++
    endfor
    lib_wl = new_wl  
    lib = temporary(lib2)
  endif

  ; define bbl if shoulders were given through bbl_wvl
  bbl_wvl = ~n_elements(bbl_wvl)? [0,0] : bbl_wvl
  if bbl_wvl[0] ne 0 and bbl_wvl[1] ne 0 then begin
    bbl = bytarr(lib_nb)
    n_bbl=n_elements(bbl_wvl)/2
    for i=0l,n_bbl-1 do begin
      range = where(lib_wl ge bbl_wvl[i*2] and lib_wl le bbl_wvl[2*i+1],range_c)
      if range_c gt 0 then bbl[range]=1
    endfor
  endif
  
  
  ; get lookup table
  if gfzlib then begin
    lookup_file = file_search(outdir,'GFZ_LIB_lookup.csv')
    nel_lookup   = (file_lines(lookup_file))[0]
    lookup = strarr(nel_lookup)
    lookup_names =  strarr(nel_lookup)
    lookup_class_gfz = bytarr(3,nel_lookup)
    close,1
    openr,1,lookup_file
    readf,1,lookup
    close,1
    for i=0l,nel_lookup-1l do begin
      st = strsplit(lookup[i],';',/extract)
      lookup_names[i] = st[0] 
      lookup_class_gfz [*,i] = byte(float(st[1:*]))
    endfor
    for i=0l,lib_ns-1l do begin
        good = where(lookup_names eq lib_spec_names[i],goodc)
        if goodc eq 0 then continue
        lookup_class_gfz2 = n_elements(lookup_class_gfz2) eq 0? [lookup_class_gfz[*,good[0]]] : [[lookup_class_gfz2],[lookup_class_gfz[*,good[0]]]]
    endfor
    lookup_class_gfz = lookup_class_gfz2
  endif

 
  ;check compatibility
  flag = lib_nb eq dat_nb || lib_wu eq dat_wu
  if ~flag then stop
  if max(dat,/nan) gt 1000 then dat/=10000.
  if max(lib,/nan) gt 1000 then lib/=10000.
  
  ;analyse lib
  print,'***** Preparing library'
  lib_meta = ptrarr(lib_ns); make flexible pointer array
  if cont_removal_type eq 2 then lib_meta2 = ptrarr(lib_ns)

  close,1
  openw,1,FILEPATH(/TMP,'features.csv')
  run=0ul
  for x=0l,lib_ns-1l do begin
    spectrum = reform(lib[x,0,*])
    case (1b) of
      cont_removal_type lt 2  : begin
                                    tempcont    =   cm_cr_cont_spec(spectrum*1.,full=1.,maxiter=cont_max_iter,cont_removal_type=cont_removal_type,bbl=bbl)
                                    lib_meta[x] =   ptr_new(tempcont)
                                    shoulders = lib_wl[(*lib_meta[x]).shoulders[*]]
                                    printf,1,strjoin([lib_spec_names[x],';',strjoin(string(shoulders),';')      ])
                                end    
      cont_removal_type eq 2  : begin

                                    tempcont    =   cm_cr_cont_spec(spectrum*1.,agl1=agl1,bgl2=bgl2,cgl11=cgl11,dgl22=dgl22,full=1.,maxiter=cont_max_iter,cont_removal_type=0,bbl=bbl)
                                    tempcont2   =   cm_cr_cont_spec(spectrum*1.,agl1=agl1,bgl2=bgl2,cgl11=cgl11,dgl22=dgl22,full=1.,maxiter=cont_max_iter,cont_removal_type=1,bbl=bbl)
                                    _ = size(temporary(agl1))
                                    _ = size(temporary(bgl2))
                                    _ = size(temporary(cgl11))
                                    _ = size(temporary(dgl22))
                                    lib_meta[x] =   ptr_new(tempcont)
                                    lib_meta2[x]=   ptr_new(tempcont2)
                                    shoulders = lib_wl[(*lib_meta[x]).shoulders[*]]
                                    printf,1,strjoin([lib_spec_names[x],';',strjoin(string(shoulders),';')      ])
                                    shoulders2 = lib_wl[(*lib_meta2[x]).shoulders[*]]
                                    printf,1,strjoin([lib_spec_names[x],';',strjoin(string(shoulders2),';')      ])
                                end
    endcase
    run++
  endfor
  close,1

at02 = systime(1)

good = where(~bbl,goodc) & if goodc eq 0 then stop
progressBar = hubprogressBar(Title='Interpolating Data to 1 nm', /Cancel)

if flag_1nm then begin
  run=0ul
  
  progressBar.setInfo,['Processing...'] & wait,1

  dat2      = fltarr(dat_ns,dat_nl,lib_nb)
  for x=0l,dat_ns-1l do for y=0l,dat_nl-1l do begin
    if mask[x,y] eq 0 then continue
    dat2[x,y,*] = interpol((reform(dat[x,y,*]))[good],dat_wl[good] ,new_wl)
    progressBar.setProgress, (run+1.)/(dat_ns*dat_nl) &wait,0.001
    run++
  endfor
  dat = temporary(dat2)
  dat_wl = lib_wl
  dat_nb = lib_nb
endif 
if cont_removal_type eq 2 then dat2 = dat
  progressBar.hideBar
  progressBar.setInfo, ['finish processing']
  wait,1
  progressBar.cleanup

  run=0ul
  ;preprocessing to spare time....
  fil1 = ~n_elements(fil1) ?  fltarr(2>round(lib_nb*0.02))+1.  : fil1
  fil2 = ~n_elements(fil2) ?  fltarr(3>round(lib_nb*0.1 ))+1.  : fil2
  fil1 /=total(fil1)
  fil2 /=total(fil2)
  fil1 = reform(fil1,1,1,n_elements(fil1))
  fil2 = reform(fil2,1,1,n_elements(fil2))

gl1 = ~n_elements(gl1)? convol(dat,fil1,/center,/edge_trunc,/nan) : gl1
gl2 = ~n_elements(gl2)? convol(dat,fil2,/center,/edge_trunc,/nan) : gl2
gl11= ~n_elements(gl11)? convol(gl1,fil1,/center,/edge_trunc,/nan) : gl11
gl22= ~n_elements(gl22)? convol(gl2,fil2,/center,/edge_trunc,/nan) : gl22


progressBar = hubprogressBar(Title='Data continuum removal', /Cancel)
progressBar.setInfo,['Processing...'] & wait,1  
for x=0l,dat_ns-1l do $
  for y=0l,dat_nl-1l do begin
    if mask[x,y] eq 0 then continue
    spectrum = dat[x,y,*]

    case (1b) of
      cont_removal_type eq 0  : begin
                                  tempcont    =   cm_cr_cont_spec(spectrum,maxiter=cont_max_iter,cont_removal_type=cont_removal_type,bbl=bbl,$
                                    agl1=(gl1[x,y,*])[*],bgl2=(gl2[x,y,*])[*],cgl11=(gl11[x,y,*])[*],dgl22=(gl22[x,y,*])[*],fil1=fil1,fil2=fil2)
                                  spectrum   /=   tempcont

                                end
      cont_removal_type eq 1  : begin
                                  tempcont = cm_cr_cont_spec(spectrum,maxiter=cont_max_iter,cont_removal_type=cont_removal_type,bbl=bbl,$
                                  agl1=(gl1[x,y,*])[*],bgl2=(gl2[x,y,*])[*],cgl11=(gl11[x,y,*])[*],dgl22=(gl22[x,y,*])[*],fil1=fil1,fil2=fil2)
                                  spectrum   += 1. - tempcont
                                end
      cont_removal_type eq 2  : begin
                                  tempcont = cm_cr_cont_spec(spectrum,maxiter=cont_max_iter,cont_removal_type=0,bbl=bbl,$
                                  agl1=(gl1[x,y,*])[*],bgl2=(gl2[x,y,*])[*],cgl11=(gl11[x,y,*])[*],dgl22=(gl22[x,y,*])[*],fil1=fil1,fil2=fil2)
                                  tempcont2= cm_cr_cont_spec(spectrum,maxiter=cont_max_iter,cont_removal_type=1,bbl=bbl,$
                                  agl1=(gl1[x,y,*])[*],bgl2=(gl2[x,y,*])[*],cgl11=(gl11[x,y,*])[*],dgl22=(gl22[x,y,*])[*],fil1=fil1,fil2=fil2)
                                  spectrum2 = spectrum
                                  spectrum   /= tempcont
                                  spectrum2   += 1. - tempcont2
                                  spectrum2    <=  1.; dangerous if above value scaling was erroneous for cont_removal_type gt 0
                                  spectrum2    >=  0.
                                  dat2[x,y,*] = spectrum2
                                end
    endcase
    spectrum    <=  1.; dangerous if above value scaling was erroneous for cont_removal_type gt 0
    spectrum    >=  0.
    dat[x,y,*] = spectrum
    progressBar.setProgress, (run+1.)/(dat_ns*dat_nl) &wait,0.001          
    run++
  endfor
  progressBar.hideBar
  progressBar.setInfo, ['finish processing']
  wait,1
  progressBar.cleanup

et02 = systime(1)

 print,'***** Analysing...'
 
 nan=0
  ;analyse image
  class       = lonarr(dat_ns,dat_nl);,dat_nb)
  class_fits  = fltarr(dat_ns,dat_nl);,dat_nb)
  if cont_removal_type eq 2 then begin
    class2       = lonarr(dat_ns,dat_nl);,dat_nb)
    class_fits2  = fltarr(dat_ns,dat_nl);,dat_nb)
  endif
  if unmix_type eq 1 then begin
    class3       = lonarr(dat_ns,dat_nl);,dat_nb)
    class_fits3  = fltarr(dat_ns,dat_nl);,dat_nb)
  endif
  if unmix_type eq 1 and cont_removal_type eq 2 then begin
  class4       = lonarr(dat_ns,dat_nl);,dat_nb)
  class_fits4  = fltarr(dat_ns,dat_nl);,dat_nb)
  endif
  

  tx1=systime(1)
  fit = fltarr(dat_ns,dat_nl,lib_ns)
  fit2 = fltarr(dat_ns,dat_nl,lib_ns)
  progressBar = hubprogressBar(Title='Data analysis', /Cancel)
  progressBar.setInfo,['Processing...'] & wait,1
  run=0ul
  for x=0l,dat_ns-1l do $
    for y=0l,dat_nl-1l do begin
          if mask[x,y] eq 0 then continue
          for i=0l,lib_ns-1l do begin
            mat = *lib_meta[i]
            nfeat = n_elements(mat.depth)
            feat_fit = fltarr(nfeat)
            for j=0l,nfeat-1l do begin
              left  = mat.shoulders[0,j]
              right = mat.shoulders[1,j]
              ; fitmethod 0 and 1 are diffente implementations of the same - mean reduced and normalized dot product vs. correlate
              case (1b) of
                fitmethod eq 0: begin
                                    lib_feat_cont_spectrum_reduced = mat.cont_spectrum_reduced[left:right,j]
                                    dat_feat_cont_spectrum_reduced = dat[x,y,left:right]
                                    dat_feat_cont_spectrum_reduced -= mean(dat_feat_cont_spectrum_reduced,nan=nan)
                                    dat_feat_cont_spectrum_reduced /= sqrt(total(dat_feat_cont_spectrum_reduced*dat_feat_cont_spectrum_reduced,nan=nan))
                                    feat_fit[j] = total(dat_feat_cont_spectrum_reduced * lib_feat_cont_spectrum_reduced,nan=nan) * mat.weights[j]
                                end
                fitmethod eq 1: begin
                                    lib_feat_cont_spectrum_reduced = mat.cont_spectrum[left:right]
                                    dat_feat_cont_spectrum_reduced = dat[x,y,left:right]
                                    feat_fit[j] = correlate(dat_feat_cont_spectrum_reduced , lib_feat_cont_spectrum_reduced) * mat.weights[j]
                                end
              endcase
            endfor
            fit[x,y,i] = total(feat_fit,/nan)
          endfor
          maxfit = max(fit[x,y,*],/nan,pos)
          if maxfit ge fit_threshold[0] then begin
            class_fits[x,y] = maxfit
            class[x,y] = pos+1
          endif          
    progressBar.setProgress, (run+1.)/(dat_ns*dat_nl) &wait,0.001     
    run++
  endfor
  progressBar.hideBar
  progressBar.setInfo, ['finish processing']
  wait,1
  progressBar.cleanup
  
  outputFilename = outdir + path_sep() + file_basename(dat_name,'bsq')+'_fit_values';filepath(dat_name, /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)
  writerSettings = hash()
  writerSettings['samples'] = dat_ns
  writerSettings['lines'] = dat_nl
  writerSettings['bands'] = 1
  writerSettings['data type'] = 4
  writerSettings['dataFormat'] = 'band'
  writerSettings['interleave'] = 'bsq'
  writerSettings['byte order'] = 0
  outputImage.initWriter, writerSettings
  outputImage.writeData, float(class_fits)
  outputImage.cleanup
  
   
  
  
  u = uniq(class,sort(Class))
  num_classes = n_elements(u) + long(class[u[0]] eq 0? 0 : 1)
  if ~gfzlib then begin &$
    lookup = bytscl(randomn(seed,3,num_classes)) & lookup[*,0] = 0  &$
    endif else begin &$
    lookup =  [[0,0,0],[lookup_class_gfz]]; [0,0,0, 170,43,0, 170,43,0, 255,0,0, 250,160,185, 255,255,0, 0,255,211, 0,255,211, 0,125,125, 128,128,128, 255,210,0, 255,210,0, 255,210,0, 250,160,185, 40,188,255, 145,25,55, 230,100,0, 250,150,0, 250,150,0, 250,100,0, 0,0,255, 0,0,255, 165,20,200, 25,25,25, 0,255,255, 255,255,255, 200,200,200, 128,128,128,120,190,30, 255,205,170, 0,255,0, 0,180,0, 0,100,0, 0,100,0]
    lookup = byte(reform(lookup, 3,n_elements(lookup)/3))
     lookup = lookup[*,[class[u]]] &$
  endelse &$
class_names = (['Unknown',lib_spec_names])[class[u]]  



outputFilename = outdir + path_sep() + file_basename(dat_name,'bsq')+'_class';filepath(dat_name, /TMP)
outputImage = hubIOImgOutputImage(outputFilename)
writerSettings = hash()
writerSettings['samples'] = dat_ns
writerSettings['lines'] = dat_nl
writerSettings['bands'] = 1
writerSettings['data type'] = 'byte'
writerSettings['dataFormat'] = 'band'
outputImage.initWriter, writerSettings
outputImage.writeData, class
outputImage.setMeta, 'file type', 'envi classification'
outputImage.setMeta, 'class lookup', lookup;[0,0,0,0,255,0,255,0,0,255,255,0,0,255,255,0,0,255]
outputImage.setMeta, 'class names', class_names;['Unclassified','class 1','class 2','class 3','class 4','class 5']
outputImage.setMeta, 'classes', num_classes
outputImage.cleanup


if unmix_type eq 1 then begin
  progressBar = hubprogressBar(Title='BVLS unmixing', /Cancel)
  progressBar.setInfo,['Processing...'] & wait,1
  
  lib_cont = fltarr(lib_ns,lib_nb)
   for i=0l,lib_ns-1l do lib_cont[i,*] = (*lib_meta2[i]).cont_spectrum;important to use absolut differences to fullfil the requiremnts of linear modelling
   lib_cont = transpose(lib_cont)
   bnd = rebin([0.,1.],2,lib_ns)
   res   = fltarr(dat_ns,dat_nl,lib_ns)
   errors  = fltarr(dat_ns,dat_nl)
   run2 = 0ul
  run=0ul
   tx1 = systime(1)
  for x=0l,dat_ns-1l do $
    for y=0l,dat_nl-1l do begin
      if mask[x,y] eq 0 then begin
        run++
        continue
      endif
      good = where(fit[x,y,*] ge fit_threshold[0] ,goodc)
      if goodc eq 0 then begin
        run++
        continue
      endif
      xx    = fltarr(goodc)
      bnd2  = bnd[*,good]
      lib_cont_good = lib_cont[*,good] 
      good2 = where(total(lib_cont_good ne 1,2) gt 0)
      b = (dat2[x,y,*])[good2]; uses the absolute absorptions
      lib_cont_good2 = lib_cont_good[good2,*]
      bvls,lib_cont_good2*1.,b*1.,bnd2,xx,RNORM=RNORM
      errors[x,y] = rnorm
      res[x,y,good]=xx
      progressBar.setProgress, (run+1.)/(dat_ns*dat_nl) &wait,0.001     
    run++
  endfor
  progressBar.hideBar
  progressBar.setInfo, ['finish processing']
  wait,1
  progressBar.cleanup

  maxfit3 = max(res,dimension=3,/nan,pos3)
  class_fits3= maxfit3;[x,y] = maxfit3
  class3= (pos3 /(dat_ns*dat_nl)) +1;[x,y] = pos3+1
  bad = where(maxfit3 eq 0,badc) & if badc gt 0 then class3[bad] = 0; to be 'unknown'
    
  
  outputFilename = outdir + path_sep() + file_basename(dat_name,'bsq')+'_BVLS_residuals';filepath(dat_name, /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)
  writerSettings = hash()
  writerSettings['samples'] = dat_ns
  writerSettings['lines'] = dat_nl
  writerSettings['bands'] = 1
  writerSettings['data type'] = 4
  writerSettings['dataFormat'] = 'band'
  writerSettings['interleave'] = '0'
  outputImage.initWriter, writerSettings
  outputImage.writeData, float(class_fits3)
  outputImage.setmeta, 'map info', map_info
  outputImage.cleanup
  
  outputFilename = outdir + path_sep() + file_basename(dat_name,'bsq')+'_BVLS_error';filepath(dat_name, /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)
  writerSettings = hash()
  writerSettings['samples'] = dat_ns
  writerSettings['lines'] = dat_nl
  writerSettings['bands'] = 1
  writerSettings['data type'] = 4
  writerSettings['interleave'] = '0'
  writerSettings['dataFormat'] = 'band'
  outputImage.initWriter, writerSettings
  outputImage.writeData, float(errors)
  outputImage.setmeta, 'map info', map_info
  outputImage.cleanup
  
  
  u3 = uniq(class3,sort(Class3))
  num_classes3 = n_elements(u3) + long(class3[u3[0]] eq 0? 0 : 1)
  if ~gfzlib then begin
    lookup3 = bytscl(randomn(seed,3,num_classes3)) & lookup3[*,0] = 0
  endif else begin
    lookup3 =  [[0,0,0],[lookup_class_gfz]];[0,0,0, 170,43,0, 170,43,0, 255,0,0, 250,160,185, 255,255,0, 0,255,211, 0,255,211, 0,125,125, 128,128,128, 255,210,0, 255,210,0, 255,210,0, 250,160,185, 40,188,255, 145,25,55, 230,100,0, 250,150,0, 250,150,0, 250,100,0, 0,0,255, 0,0,255, 165,20,200, 25,25,25, 0,255,255, 255,255,255, 200,200,200, 128,128,128,120,190,30, 255,205,170, 0,255,0, 0,180,0, 0,100,0, 0,100,0]
    lookup3 = byte(reform(lookup3, 3,n_elements(lookup3)/3))
    lookup3 = lookup3[*,[class3[u3]]]
  endelse
  class_names3 = (['Unknown',lib_spec_names])[class3[u3]]
    
  
  outputFilename = outdir + path_sep() + file_basename(dat_name,'bsq')+'_class_unmix';filepath(dat_name, /TMP)
  outputImage = hubIOImgOutputImage(outputFilename)
  writerSettings = hash()
  writerSettings['samples'] = dat_ns
  writerSettings['lines'] = dat_nl
  writerSettings['bands'] = 1
  writerSettings['data type'] = 'byte'
  writerSettings['dataFormat'] = 'band'
  
    
  outputImage.initWriter, writerSettings
  outputImage.writeData, class3
  outputImage.setMeta, 'file type', 'envi classification'
  outputImage.setMeta, 'class lookup', lookup3;[0,0,0,0,255,0,255,0,0,255,255,0,0,255,255,0,0,255]
  outputImage.setMeta, 'class names', class_names3;['Unclassified','class 1','class 2','class 3','class 4','class 5']
  outputImage.setMeta, 'classes', num_classes3
  outputImage.setmeta, 'map info', map_info
  outputImage.cleanup
  
  
endif

end