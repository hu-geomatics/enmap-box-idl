function cr_remove_continua_delaunay,l,r,s,full=full
; spectra should be in columns
	sz		=	size(reform(s,/over),/dimensions)
	if n_elements(sz) eq 1 then s=reform(s,1,sz[0])
	sz		=	size(s,/dimensions)
	inf		=	{res:fltarr(sz[0],r[0]-l[0]+1l),cont:fltarr(sz[0],r[0]-l[0]+1l),	flag	:	bytarr(sz[0])}
	nels	=	sz[1]
	find	=	findgen(nels)
for i=0l,sz[0]-1l do begin
		find2	=	(find[l:r])
		s2		=	s[i,l:r]
		if	sqrt((moment(/nan,s[i,l:r]))[1])	eq 0 then $
		if 	total(s[i,l:r],/nan) eq 0. then  begin
			inf.res[i,*]		=	s[i,l:r]
			inf.cont[i,*]		=	s[i,l:r]
			continue
		endif else begin
			inf.res[i,*]		=	s[i,l:r]/s[i,l:r]
			inf.cont[i,*]		=	(s[i,l:r])
			continue
		endelse
	colinear	=	mean(/nan,(s[i,l:r]-shift(s[i,l:r],1))[1:*]) eq (s[i,l:r]-shift(s[i,l:r],1))[1]
	if colinear then begin
		inf.res[i,*]	=	replicate(1.,n_elements(s[i,l:r]))
		inf.cont[i,*]	=	s[i,l:r]
	endif else begin
		triangulate,find[l:r],s[i,l:r]>0.,tr,b
		b1	=	b[[0,where(([0,b]-[b,0])[1:*] ge 0.)]]
	 	b1	=	b1[uniq(b1,sort(b1))]
	 	nel	=	n_elements(b1)
		if nel eq 2 then begin
				left2	=	(find[l:r])[b1[0]]
				right2	=	(find[l:r])[b1[1l]]
				nel2	=	(right2-left2)+1l
				inf.cont[i,*]	=	((s[i,l:r])[0]+find[0:nel2-1l]*((s[i,l:r])[nel2-1l]-(s[i,l:r])[0])/(right2-left2))
				inf.res[i,*]	=	(s[i,l:r])/inf.cont[i,*]
		endif else begin
			contarr	=	fltarr(sz[0],r-l+1l)
			for m=0l, nel-2l do begin
				contarr[i,b1[m]:b1[m+1l]]	=	(s[i,(find[l:r])[b1[m]]]+find[0:((find[l:r])[b1[m+1l]]-(find[l:r])[b1[m]])]*$
											(s[i,(find[l:r])[b1[m+1l]]]-s[i,(find[l:r])[b1[m]]])/((find[l:r])[b1[m+1l]]-(find[l:r])[b1[m]]))
			endfor
			inf.cont[i,*]	=	contarr[i,*]
			inf.res[i,*]	=	0.>((s[i,l:r])/inf.cont[i,*])<1.
			inf.flag[i]=1b
		endelse
	endelse
endfor
return,~keyword_set(full)?	0.>inf.res<1.	:	inf
end