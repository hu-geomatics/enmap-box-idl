function cr_gau,sigma,t=t
  !except=0
  t = ~n_elements(t)? 0.9999d : t
  npixel = sigma*100.
  npixel -= npixel mod 2
  npixel += 1.
  dind = dindgen(npixel) - (npixel-1)/2
  gau = exp ( - (dind^(2d))/(2d * (sigma^(2d))   )   )
  gau /= total(gau,/double)
  good = where(total(gau,/cum) ge t,goodc)
  range = good[0] - n_elements(gau)/2
  gau = gau[n_elements(gau)/2 - range : n_elements(gau)/2 + range]
  return,gau
end

function cr_scale,im,upper,lower,double=double
  upper = n_elements(upper) eq 0? 1d  : upper
  lower = n_elements(lower) eq 0? 0d  : lower
  type  = size(upper,/type)
  case type of $
    1b  : im  = byte(temporary(im))
    2b  : im  = fix(temporary(im))
    3b  : im  = long(temporary(im))
    4b  : im  = float(temporary(im))
    5b  : im  = double(temporary(im))
    6b  : im  = complex(temporary(im))
    9b  : im  = dcomplex(temporary(im))
    12b : im  = uint(temporary(im))
    13b : im  = ulong(temporary(im))
    14b : im  = long64(temporary(im))
    15b : im  = ulong64(temporary(im))
    else:
  endcase
  minv  = min(im,/nan,max=maxv)
  im    +=  -minv
  range = abs(maxv + (-minv))
  im    /=  range ne 0? range : 1d
  range2  = upper-lower
  im    *=  range2 eq 0?  1:  range2
  im    +=  lower
  return,im
end

function cr_quant,im,nbins,max=max,min=min,binsize=binsize,omax=omax,omin=omin
  h=histogram(im,nbins=nbins,reverse_indices=r,/nan,max=max,min=min,binsize=binsize,omax=omax,omin=omin)
  for i=0l,n_elements(h)-1l do begin
    if h[i] eq 0 then continue
    im[R[R[I] : R[i+1]-1]]=i
  endfor
  return,im
end

; REEMMAP Algorithm
; Based on: Algorithms from Nina Boesche (NB), Christian Mielke (CM), Christian Rogass (CR)
; Courtesy: GFZ Potsdam, 2016
; Change Log:
; 31.12.15; NB; REEMAP
; 12.01.2016; CR; first GEOMAP implementation
; 13.01.2016; CR; bug fixing
; 15.01.2016; CR, NB; multitemporal analysis included
; 18.01.2016; CR; fixed no data bug
; 19.01.2016; CR; fixed NAN bug
; 25.02.2016; CR; fixed bsq pyramid file search bug
; 06.03.2016; CR, NB; added Pixel Adaptive Normalization (PAN), code cleanup, classification: scaling of fit values
; Contact: name.surname@gfz-potsdam.de
; Version: 1.009

pro engeomap_ree,$
    dat_name=dat_name,$; name of the file that shall be analysed
    lib_name=lib_name,$; name of the library that is used for the analysis
    feat_name=feat_name,$; name of the feature library as CSV
    bbl_wvl = bbl_wvl,$; bad bands to exclude
    fit_threshold = fit_threshold, $; minimum fitting threshold
    flag_1nm = flag_1nm,$ ; interpolate to 1 nm lib and unknown spectrum - highly recommended
    mask=mask,$
      pattern=pattern,$
      outdir=outdir, mask_name=mask_name

  ; setting defaults
  !EXCEPT=2  
  lib_name                  =       ~n_elements(lib_name)?                dialog_pickfile(title='Library file')         :           lib_name
  feat_name                  =      ~n_elements(feat_name)?               dialog_pickfile(title='Feature file')         :           feat_name
  bbl_wvl                   =       ~n_elements(bbl_wvl)?                 [1300,1450,1750,2010]                         :           bbl_wvl
  fit_threshold             =       ~n_elements(fit_threshold)?           .5                                            :           0>fit_threshold
  flag_1nm                  =       ~n_elements(flag_1nm)?                 1                                            :           flag_1nm
    
  
  
  lib_meta = hubIOImgInputImage(lib_name)

  lib_ns = lib_meta.getmeta('samples')
  lib_nl = lib_meta.getmeta('lines')
  lib_nb = lib_meta.getmeta('bands')
  lib_wl = lib_meta.getmeta('wavelength')
  lib_spec_names = lib_meta.getmeta('spectra names')
  lib_wu   = lib_meta.getmeta('wavelength units')
  lib_meta.initreader,/cube
  lib = lib_meta.getdata();make_array(lib_ns,lib_nl,lib_nb,type=lib_data_type)
  lib = reform(reform(lib),lib_nl,1,lib_nb,/over);reform(transpose(lib),lib_nl,1,lib_ns)
  lib_sz = size(lib,/dimensions)
  lib_ns = lib_sz[0]
  lib_nl = lib_sz[1]
  lib_nb = lib_sz[2]
  
  
  run=0ul
  if flag_1nm then begin
    print,'***** Interpolating Library'
    new_wl = findgen(lib_wl[lib_nb-1l] - lib_wl[0]+1) + lib_wl[0]
    lib_nb = n_elements(new_wl)
    lib2 = fltarr(lib_ns,lib_nl,lib_nb)
    for x=0l,lib_ns-1l do for y=0l,lib_nl-1l do begin
      lib2[x,y,*] = interpol(lib[x,y,*],lib_wl,new_wl,/nan)
      run++
    endfor
    lib_wl = new_wl
    lib = temporary(lib2)
  endif
  ; define bbl if shoulders were given through bbl_wvl
  bbl_wvl = ~n_elements(bbl_wvl)? [0,0] : bbl_wvl
  if bbl_wvl[0] ne 0 and bbl_wvl[1] ne 0 then begin
    bbl = bytarr(lib_nb)
    n_bbl=n_elements(bbl_wvl)/2
    for i=0l,n_bbl-1 do begin
      range = where(lib_wl ge bbl_wvl[i*2] and lib_wl le bbl_wvl[2*i+1],range_c)
      if range_c gt 0 then bbl[range]=1
    endfor
  endif
if max(lib,/nan) gt 1000 then lib/=10000.
 
          ;analyse lib
          print,'***** Preparing library'

          ; read feature file
          nel_feats   = file_lines(feat_name)
          feats = strarr(nel_feats)
          close,1
          openr,1,feat_name
          readf,1,feats
          close,1
          lib_meta = ptrarr(nel_feats); make flexible pointer array
          for i=0l,nel_feats-1l do begin &$
            st = strsplit(feats[i],';',/extract) &$
          name = st[0] &$
            ree_type = float(st[1]) &$
            feature_vals = st[2:*] &$
            nfeatures = n_elements(feature_vals)/4 &$
            lib_meta[i] = ptr_new({name : name, ree_type: ree_type, feature : replicate({left:0., right:0.,gau_sigma:0., gau_width:0.},nfeatures)}) &$
            for j=0l,nfeatures-1l do begin &$
            (*(lib_meta[i])).feature[j].left = float(feature_vals[j*4]) &$
            (*(lib_meta[i])).feature[j].right = float(feature_vals[j*4+1]) &$
            (*(lib_meta[i])).feature[j].gau_sigma = float(feature_vals[j*4+2]) &$
            (*(lib_meta[i])).feature[j].gau_width = float(feature_vals[j*4+3]) &$
            print,name,(*(lib_meta[i])).feature[j].left,(*(lib_meta[i])).feature[j].right,(*(lib_meta[i])).feature[j].gau_sigma,(*(lib_meta[i])).feature[j].gau_width
            endfor &$
        endfor
 
  pattern = ~n_elements(pattern)? '*.bsq' : pattern
  files=file_search(outdir,pattern ,/FULLY_QUALIFY_PATH )
  files = files[where(strpos(files,'Pyramid') eq -1)]
  ;stop
  
  
  nf = n_elements(files)
 
  dat_meta = hubIOImgInputImage(files[0])
  dat_ns = dat_meta.getmeta('samples')
  dat_nl = dat_meta.getmeta('lines')
  dat_nb = dat_meta.getmeta('bands')
  dat_wl = dat_meta.getmeta('wavelength')
  dat_wu   = dat_meta.getmeta('wavelength units')
  map_info = dat_meta.getmeta('map info')
  dat_meta.initreader,/cube
  ;dat = dat_meta.getdata();
 
 ;stop

    print,'***** Deconvolution and fitting'
    res = fltarr(dat_ns,dat_nl,nel_feats); nel_feats = n library entries
    small=(machar()).eps
    run = 0ul
    progressBar = hubprogressBar(Title='Analysing spectra', /Cancel)
    progressBar.setInfo,['Processing...'] & wait,1
    for i=0l,nel_feats-1l do begin; element
      index     = where(lib_spec_names eq (*(lib_meta[i])).name)
      print,'Library entry: ',lib_spec_names[index],' Number: ',index[0]
      lib_fit_names = i eq 0? lib_spec_names[index] : [lib_fit_names,lib_spec_names[index]]
      lib_spectrum  = reform(lib[index,0,*])
      nel_element_features = n_elements((*(lib_meta[i])).feature)
      print,'Features: ',nel_element_features
      
      res_element = fltarr(dat_ns,dat_nl,nel_element_features)
          for j=0l,nel_element_features-1l do begin; feature
        _ = min(abs(lib_wl - (*(lib_meta[i])).feature[j].left),left)
        _ = min(abs(lib_wl - (*(lib_meta[i])).feature[j].right),right)
        gau_sigma = (*(lib_meta[i])).feature[j].gau_sigma
        gau_width = (*(lib_meta[i])).feature[j].gau_width; not used
        print,'Current feature: ',(*(lib_meta[i])).feature[j].left, (*(lib_meta[i])).feature[j].right
        gaussian_fil = cr_gau(gau_sigma)
        gaussian_fil /= total(gaussian_fil)
        gaussian_fil = reform(gaussian_fil,1,1,n_elements(gaussian_fil))

         
         variance_vector  = fltarr(nf)
        for k=0l,nf-1l do begin; file
          print,'Files: ',k+1
          dat_meta = hubIOImgInputImage(files[k])
          dat_ns = dat_meta.getmeta('samples')
          dat_nl = dat_meta.getmeta('lines')
          dat_nb = dat_meta.getmeta('bands')
          dat_wl = dat_meta.getmeta('wavelength')
          dat_wu   = dat_meta.getmeta('wavelength units')
          dat_meta.initreader,/cube
          dat = dat_meta.getdata();

          ;PAN
          s = size(im_mean,/dim)*1.
          pan = total(dat,3,/nan)
          pan /= dat_nl
          pan /= mean(pan,/nan)
          for ii=0l,dat_nl-1l do dat[*,*,i]/=pan
          _=size(temporary(pan))


          _ = min(abs(dat_wl - (*(lib_meta[i])).feature[j].left),left2)
          _ = min(abs(dat_wl - (*(lib_meta[i])).feature[j].right),right2)   
          pan = total(dat[*,*,left2:right2],3,/nan)
          quan = cr_quant(pan*1.,10)
          sigma = erode(quan le 2,[1,1])
          ;sigma = erode(quan le mean(quan,/nan),[1,1])
          
          variance_vector[k] = mean(pan * sigma[where(sigma)],/nan) / (right2-left2+1)
          if variance_vector[k] eq 0 then stop
          
          
        endfor
          variance_vector /= total(variance_vector,/nan)
          datm = fltarr(dat_ns,dat_nl,dat_nb)
          
        for k=0l,nf-1l do begin; file
          dat_meta = hubIOImgInputImage(files[k])
          dat_ns = dat_meta.getmeta('samples')
          dat_nl = dat_meta.getmeta('lines')
          dat_nb = dat_meta.getmeta('bands')
          dat_wl = dat_meta.getmeta('wavelength')
          dat_wu   = dat_meta.getmeta('wavelength units')
          dat_meta.initreader,/cube
          dat = dat_meta.getdata();
          datm += dat * variance_vector[k]
        endfor 
        if flag_1nm then begin

          interpol_vec_z = interpol(findgen(dat_nb) , dat_wl, new_wl,/nan)
          interpol_vec_z = interpol_vec_z[where(interpol_vec_z ge 0)]
          interpol_vec_x = findgen(dat_ns)
          interpol_vec_y = findgen(dat_nl)
          dat2m = interpolate(datm,interpol_vec_x,interpol_vec_y,interpol_vec_z,/grid)
          if total(~finite(datm)) gt 0 then stop
          datm = temporary(dat2m)
          dat_wl = lib_wl
          dat_nb = lib_nb
        endif
        if max(datm,/nan) gt 1000 then datm/=10000.
        
        if n_elements(mask2) eq 0 then begin
          ; masking unlikely results
          re = 670
          ir  = 770
          IFD1 = 732
          IFD2 = 1275
          IFD3 = 874

          _=min(abs(lib_wl-re),red)
          _=min(abs(lib_wl-ir),infrared)
          _=min(abs(lib_wl-IFD1),IFD_l)
          _=min(abs(lib_wl-IFD2),IFD_r)
          _=min(abs(lib_wl-IFD3),IFD_m)
          mask2=(datm[*,*,infrared]-datm[*,*,red])/(datm[*,*,infrared]+datm[*,*,red])
          mask2 lt=0.3

          l560 =  min(abs(lib_wl-560.),p560)
          l650 =  min(abs(lib_wl-650.),p650)
          l865 =  min(abs(lib_wl-865.),p865)
          l1600=  min(abs(lib_wl-1600.),p1600)
          ratio = (lib_wl[p865] - lib_wl[p650])/(lib_wl[p1600] - lib_wl[p650])
          ifd_im865int = datm[*,*,p650]+(datm[*,*,p1600] - datm[*,*,p650])*ratio
          nifd = (ifd_im865int - datm[*,*,p865])/(ifd_im865int + datm[*,*,p865])
          nifd lt=0.9
          mask2*=temporary(nIFD)
          mask2*= total(datm,3,/nan) ge (0.01*dat_nb); change version 1.005 - 0.01 to mask out missing data in aviris data
        endif
        
        
          spec11_log=convol(datm, gaussian_fil ,/edge_trunc,/center,/nan)
          spec11_log>=0.
          spec11_log<=1.
          spec11_log=alog(1./(spec11_log+small))
          spec11_log*=convol(spec11_log/convol(spec11_log,gaussian_fil,/edge_trunc,/center,/nan),gaussian_fil*1.,total(gaussian_fil),/edge_trunc,/center,/nan)
          spec11_log = exp(1./spec11_log)
          
          lib11_log=convol(lib_spectrum, gaussian_fil[*] ,/edge_trunc,/center,/nan)
          lib11_log>=0.
          lib11_log<=1.
          lib11_log=alog(1./(lib11_log+small))
          lib11_log*=convol(lib11_log/convol(lib11_log,gaussian_fil[*],/edge_trunc,/center,/nan),gaussian_fil[*]*1.,total(gaussian_fil[*]),/edge_trunc,/center,/nan)
          lib11_log = exp(1./lib11_log)
          for x=0l,dat_ns-1l do for y=0l,dat_nl-1l do begin
            if mask2[x,y] eq 0 then continue
            corr = correlate((spec11_log[x,y,left:right])[*], lib11_log[left:right] ); weighting is here possible
            res_element[x,y,j]=corr
          endfor    
      endfor; features

      res[*,*,i] = total(res_element,3,/nan) / float(nel_element_features); only fitting features?
      progressBar.setProgress, (run+1.)/(nel_element_features) &wait,0.001    
      run++
    endfor; elements
    progressBar.hideBar
    progressBar.setInfo, ['finish processing']
    wait,1
    progressBar.cleanup

res *= rebin(mask2,dat_ns,dat_nl,lib_nb,/sample)

res_max=max((max((0.>res), dimension=1)),dimension=1)
res2=(0.>res)/rebin(reform(res_max,1,1,nel_feats),dat_ns,dat_nl,nel_feats,/sample)
bad = where(~finite(res2), badc)
if badc gt 0 then res[bad]=0

maxfit = 0.> max(res2 * (res2 ge fit_threshold),dimension=3,/nan,pos)
class= (pos /(dat_ns*dat_nl)) +1
bad = where(maxfit le fit_threshold,badc) & if badc gt 0 then class[bad] = 0; to be 'unknown'
class_fits= maxfit & if badc gt 0 then class_fits[bad]=0
bad = where(~mask2,badc) & if badc gt 0 then class[bad] = 0; to be 'unknown'
if badc gt 0 then class_fits[bad]=0

outputFilename = outdir + path_sep() + 'REE_max_fit';filepath(dat_name, /TMP)
outputImage = hubIOImgOutputImage(outputFilename)
writerSettings = hash()
writerSettings['samples'] = dat_ns
writerSettings['lines'] = dat_nl
writerSettings['bands'] = 1
writerSettings['data type'] = 'float'
writerSettings['dataFormat'] = 'band'
writerSettings['interleave'] = 'bsq'
writerSettings['byte order'] = 0
outputImage.initWriter, writerSettings
outputImage.writeData, float(class_fits)
outputImage.setmeta, 'map info', map_info
outputImage.cleanup

bnames=['fit ' + lib_fit_names]

outputFilename = outdir + path_sep() + 'REE_fit_values';filepath(dat_name, /TMP)
outputImage = hubIOImgOutputImage(outputFilename)
writerSettings = hash()
writerSettings['samples'] = dat_ns
writerSettings['lines'] = dat_nl
writerSettings['bands'] = nel_feats
writerSettings['data type'] = 'float'
writerSettings['dataFormat'] = 'cube'
outputImage.initWriter, writerSettings
outputImage.writeData, res*1.
outputImage.setMeta, 'file type', 'envi standard'
outputImage.setMeta, 'band names', bnames;['Unclassified','class 1','class 2','class 3','class 4','class 5']
outputImage.setmeta, 'map info', map_info
outputImage.cleanup


u = uniq(class,sort(Class))
num_classes = n_elements(u) + long(class[u[0]] eq 0? 0 : 1)
lookup = bytscl(randomn(seed,3,num_classes)) & lookup[*,0] = 0
class_names = (['Unknown',lib_fit_names])[class[u]]

outputFilename = outdir + path_sep() + 'REE_class';filepath(dat_name, /TMP)
outputImage = hubIOImgOutputImage(outputFilename)
writerSettings = hash()
writerSettings['samples'] = dat_ns
writerSettings['lines'] = dat_nl
writerSettings['bands'] = 1
writerSettings['data type'] = 'byte'
writerSettings['dataFormat'] = 'band'
outputImage.initWriter, writerSettings
outputImage.writeData, class
outputImage.setMeta, 'file type', 'envi classification'
outputImage.setMeta, 'class lookup', lookup;[0,0,0,0,255,0,255,0,0,255,255,0,0,255,255,0,0,255]
outputImage.setMeta, 'class names', class_names;['Unclassified','class 1','class 2','class 3','class 4','class 5']
outputImage.setMeta, 'classes', num_classes
outputImage.setmeta, 'map info', map_info
outputImage.cleanup


end