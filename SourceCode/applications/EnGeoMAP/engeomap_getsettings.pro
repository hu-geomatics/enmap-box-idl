;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('EnGeoMAP.conf', ROOT=EnGeoMAP_getDirname(), SUBDIR='resource')`.
;
; :Returns:
;    Returns a hash with all settings.
;
; :Examples:
;    Print settings stored inside settings file::
;      print, EnGeoMAP_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function EnGeoMAP_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('EnGeoMAP.conf', ROOT=EnGeoMAP_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()

  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']

  return, settings

end

;+
; :Hidden:
;-
pro test_EnGeoMAP_getSettings

  print, EnGeoMAP_getSettings()

end
