pro engeomap_wrapper_base, GroupLeader=groupleader
version = '2.009'
  hubAMW_program, groupleader, Title='GFZ EnGeoMAP - Base v'+version
  hubAMW_frame,title='Input/output'
  hubAMW_inputFilename, 'dat_name',            Title = 'Data file',size=521
  hubAMW_inputFilename, 'lib_name',           Title = 'Library file',size=500
  hubAMW_inputFilename, 'mask_name',           Title = 'Mask file',size=521,AllowEmptyFilename =1
  hubAMW_outputDirectoryName, 'outdir',Title = 'Output folder'
  hubAMW_frame,title='Flags'
  hubAMW_checklist, 'flag_1nm', List=['Yes', 'No'], Value=1, title='1 nm precision',$
    tooltip = 'If selected data and library are interpolated to have 1nm bandpass that may improve the results for sensors having a too heterogenous bandpasses'
  hubAMW_checklist, 'unmix_type', List=['Yes', 'No'], Value=1, title='Addtional Bounded Values Least Squares unmixing?'    
  hubAMW_checklist, 'gfzlib', List=['Yes', 'No'], Value=0, title='GFZ spectral library used? GFZ_LIB_lookup.csv must exist!',$
    tooltip = 'If selected a hard coded lookup table is used that uses similar lookup tables as USGS speclab'
  hubAMW_frame,title='Exclusion of Atmospheric absortion features'
  hubAMW_label,['Example for left shoulders box: 1300;1750 or -1 to disable','Example for right shoulders box: 1450;2010 or -1 to disable']
  hubAMW_parameter, 'bbl_wvl_left', Title='Left shoulders of excluded atmospheric features', Unit='nm', Size=5,/string,defaultresult='1300;1750'
  hubAMW_parameter, 'bbl_wvl_right', Title='Right shoulders of excluded atmospheric features', Unit='nm', Size=5,/string,defaultresult='1450;2010'
  hubAMW_frame,title='Fitting threshold'
  hubAMW_parameter, 'fit_threshold', Title='Default 0.0', Size=5,/float,defaultresult=0.0
  result = hubAMW_manage()
  
  
  ;[1300,1450,1750,2010] 
  
  dat_name = result['dat_name']
  lib_name = result['lib_name']
  mask_name = result['mask_name']
  outdir = result['outdir']
  flag_1nm = ~(result['flag_1nm'])[0]
  gfzlib   = ~(result['gfzlib'])[0]
  bbl_wvl_left  = float(strsplit(result['bbl_wvl_left'],';',/extract))
  bbl_wvl_right = float(strsplit(result['bbl_wvl_right'],';',/extract))
  if bbl_wvl_left[0] ne -1  || bbl_wvl_left[0] ne -1 then bbl_wvl = (transpose([[bbl_wvl_left],[bbl_wvl_right]]))[*]
  unmix_type   = ~(result['unmix_type'])[0]
  fit_threshold   = (result['fit_threshold'])[0]
  
  ;stop
  
  engeomap_base,$
    dat_name=dat_name,$; name of the file that shall be analysed
    lib_name=lib_name,$; name of the library that is used for the analysis
    bbl_wvl = bbl_wvl,$; bad bands to exclude
    fit_threshold = fit_threshold, $; minimum fitting threshold
    flag_1nm = flag_1nm,$ ; inertpolate to 1 nm lib and unknown spectrum - highly recommended
    unmix_type = unmix_type,$ ; type of unmixing
    gfzlib=  gfzlib,$; uses a hard coded lookup table for the gfz lib
    outdir=outdir,$
    mask_name=mask_name
  




end