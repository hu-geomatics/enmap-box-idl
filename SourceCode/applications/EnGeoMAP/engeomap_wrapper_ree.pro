pro engeomap_wrapper_ree, GroupLeader=groupleader
version = '1.009'

  hubAMW_program, Title='GFZ EnGeoMAP - REE v'+version
  hubAMW_frame,title='Input/output'
  hubAMW_inputFilename, 'feat_name',            Title = 'Feature file',size=521
  hubAMW_inputFilename, 'lib_name',           Title = 'Library file',size=500
  ;hubAMW_inputFilename, 'mask_name',           Title = 'Mask file',size=521,AllowEmptyFilename =1
  hubAMW_outputDirectoryName, 'outdir',Title = 'Input/Output folder'
  hubAMW_frame,title='Flags'
  hubAMW_checklist, 'flag_1nm', List=['Yes', 'No'], Value=0, title='1 nm precision',$
    tooltip = 'If selected data and library are interpolated to have 1nm bandpass that may improve the results for sensors having a too heterogenous bandpasses'
  hubAMW_frame,title='Exclusion of Atmospheric absortion features'
  hubAMW_label,['Example for left shoulders box: 1300;1750 or -1 to disable','Example for right shoulders box: 1450;2010 or -1 to disable']
  hubAMW_parameter, 'bbl_wvl_left', Title='Left shoulders of excluded atmospheric features', Unit='nm', Size=5,/string,defaultresult='1300;1750'
  hubAMW_parameter, 'bbl_wvl_right', Title='Right shoulders of excluded atmospheric features', Unit='nm', Size=5,/string,defaultresult='1450;2010'
  hubAMW_frame,title='Fitting threshold'
  hubAMW_parameter, 'fit_threshold', Title='Default 0.5', Size=5,/float,defaultresult=0.5
  result = hubAMW_manage()
  
  
  ;[1300,1450,1750,2010] 
  feat_name = result['feat_name']
  lib_name = result['lib_name']
  ;mask_name = result['mask_name']
  outdir = result['outdir']
  flag_1nm = ~(result['flag_1nm'])[0]
  bbl_wvl_left  = float(strsplit(result['bbl_wvl_left'],';',/extract))
  bbl_wvl_right = float(strsplit(result['bbl_wvl_right'],';',/extract))
  if bbl_wvl_left[0] ne -1  || bbl_wvl_left[0] ne -1 then bbl_wvl = (transpose([[bbl_wvl_left],[bbl_wvl_right]]))[*]
  fit_threshold   = (result['fit_threshold'])[0]
  
  ;stop
  
  engeomap_ree,$
    feat_name=feat_name,$; name of the feature file which gives the shoulders for fitting
    lib_name=lib_name,$; name of the library that is used for the analysis
    bbl_wvl = bbl_wvl,$; bad bands to exclude
    fit_threshold = fit_threshold, $; minimum fitting threshold
    flag_1nm = flag_1nm,$ ; inertpolate to 1 nm lib and unknown spectrum - highly recommended
    outdir=outdir;,$
    ;mask_name=mask_name
  




end