;+
; :Author: <author name> (<email>)
;-

function EnGeoMAP_getDirname
  
  result = filepath('EnGeoMAP', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_EnGeoMAP_getDirname

  print, EnGeoMAP_getDirname()
  print, EnGeoMAP_getDirname(/SourceCode)

end
