;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `EnWaterMAP_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `EnWaterMAP_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `EnWaterMAP_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro EnWaterMAP_application, applicationInfo
  
  ; get global settings for this application
  settings = EnWaterMAP_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  parameters = EnWaterMAP_getParameters(settings)
  
  if parameters['accept'] then begin
  
    reportInfo = EnWaterMAP_processing(parameters, settings)
    
    if parameters['showReport'] then begin
      EnWaterMAP_showReport, reportInfo, settings
    endif
  
  endif
  
end

;+
; :Hidden:
;-
pro test_EnWaterMAP_application
  applicationInfo = Hash()
  EnWaterMAP_application, applicationInfo

end  
