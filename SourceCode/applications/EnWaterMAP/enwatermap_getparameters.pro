;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de), Mathias Bochow (mathias.bochow@gfz-potsdam.de)
;-

function EnWaterMAP_getParametersConsistencyCheckFunction, resultHash, Message=message

  ; open input image
  inputImage = hubIOImgInputImage(resultHash['inputImage'])
  isConsistent = 1b
  wvl = inputImage.getMeta('wavelength')
  IF (wvl EQ !NULL) THEN BEGIN
    message = 'Wavelength need to be defined in the input image header.'
    isConsistent = 0b
    inputImage.cleanUp
    return, isConsistent 
  ENDIF
  
  wvl_unit = inputImage.getMeta('wavelength units')
  IF ((strcmp(wvl_unit,'Nanometers',/FOLD_CASE) EQ 0) AND (strcmp(wvl_unit,'Micrometers',/FOLD_CASE) EQ 0)) THEN BEGIN
    message = 'Wavelength unit of input image has to be nanometers or micrometers.'
    isConsistent = 0b
    inputImage.cleanUp
    return, isConsistent
  ENDIF
  
  ; check number of bands of input image
  IF (n_elements(wvl) LE 1) THEN BEGIN
    message = 'Input image needs to have more than one band.'
    isConsistent = 0b 
    inputImage.cleanUp
    return, isConsistent
  ENDIF
  
  ; check wavelength range (470-880 nm)
  IF (strcmp(wvl_unit,'Nanometers',/FOLD_CASE) EQ 1) THEN BEGIN
    IF (wvl[0] GT 470) THEN BEGIN
      message = 'Wavelength of first spectral band has to be smaller than or equal to 470 nm.'
      isConsistent = 0b
      inputImage.cleanUp
      return, isConsistent
    ENDIF
    IF (wvl[n_elements(wvl)-1] LT 880) THEN BEGIN
      message = 'Wavelength of last spectral band has to be greater than or equal to 880 nm.'
      isConsistent = 0b
      inputImage.cleanUp
      return, isConsistent
    ENDIF
  ENDIF ELSE IF (strcmp(wvl_unit,'Micrometers',/FOLD_CASE) EQ 1) THEN BEGIN 
   IF (wvl[0] GT 0.47) THEN BEGIN
      message = 'Wavelength of first spectral band has to be smaller than or equal to 0.47 micrometers.'
      isConsistent = 0b
      inputImage.cleanUp
      return, isConsistent
    ENDIF
    IF (wvl[n_elements(wvl)-1] LT 0.88) THEN BEGIN
      message = 'Wavelength of last spectral band has to be greater than or equal to 0.88 micrometers.'
      isConsistent = 0b
      inputImage.cleanUp
      return, isConsistent
    ENDIF
  ENDIF
  
  inputImage.cleanUp
  return, isConsistent
end

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function EnWaterMAP_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Input'
    ; insert your widgets
  hubAMW_inputImageFilename, 'inputImage', Title='Hyperspectral reflectance image', Tooltip=['Required spectral range: 450-900 nm.']
  
  hubAMW_frame ,   Title =  'Optional input parameters'
  hubAMW_parameter, 'r100', Title='Grey value of 100 % reflectance in the input image', optional=2 $
                  ,Size=8,/Integer, IsGT=0, Tooltip=['In some cases (e.g., most of the image is covered by water)', 'it is difficult to estimate this value based on the image.', 'If the resulting water mask is totally wrong,', 'run EnWaterMAP again giving the grey value corresponding to 100 % reflectance here.']
  hubAMW_parameter, 'pxsize', Title='Pixel size of input image in meters', optional=2 $
                  ,Size=5,/Float, IsGT=0, Tooltip='If this information is not given in the image header, please specify here.'
                  
  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Output'
    ; insert your widgets
  hubAMW_outputFilename, 'outputImage', Title='WaterMask image'
    
  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b
  ;IF (strcmp(!VERSION.OS_FAMILY,'Windows',/FOLD_CASE) EQ 1) THEN hubAMW_checkbox, 'showTerminal', Title='Show error messages for bug report', Value=0b
  ;IF (strcmp(!VERSION.OS_FAMILY,'unix',/FOLD_CASE) EQ 1) THEN hubAMW_checkbox, 'showTerminal', Title='Show error messages for bug report', Value=0b
  
  parameters = hubAMW_manage(ConsistencyCheckFunction='EnWaterMAP_getParametersConsistencyCheckFunction')

  return, parameters
end

;+
; :Hidden:
;-
pro test_EnWaterMAP_getParameters

  ; test your routine
  settings = EnWaterMAP_getSettings()
  parameters = EnWaterMAP_getParameters(settings)
  print, parameters
end  
