;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de), Mathias Bochow (mathias.bochow@gfz-potsdam.de)
;-

;+
; :Description:
;    This is the application report routine. It creates a HTML report for presenting
;    usefull result to the user in the form of text, image or table. 
;
; :Params:
;    reportInfo : in, required, type=hash
;      Pass the return value of the `EnWaterMAP_processing` function to this argument. 
;      
;    settings : in, optional, type=hash
;
;
;-
pro EnWaterMAP_showReport, reportInfo, settings

  report = hubReport(Title=settings['title'])

  report.addHeading, 'EnWaterMAP report'
  
  IF reportInfo.hasKey('summary') THEN BEGIN
    report.addHeading, 'Summary', 2
    report.addParagraph, reportInfo['summary']
  ENDIF
  
  IF reportInfo.hasKey('rgb') THEN BEGIN
    report.addImage, reportInfo['rgb'], 'Your input image (true colour composite)'
  ENDIF
  
  IF reportInfo.hasKey('wmask') THEN BEGIN
    report.addImage, reportInfo['wmask'], 'The EnWaterMAP water mask'
  ENDIF
  
  report.saveHTML, /Show

  
end
