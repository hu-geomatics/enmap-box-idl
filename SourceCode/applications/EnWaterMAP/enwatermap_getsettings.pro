;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('EnWaterMAP.conf', ROOT=EnWaterMAP_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, EnWaterMAP_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function EnWaterMAP_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('EnWaterMAP.conf', ROOT=EnWaterMAP_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  return, settings

end

;+
; :Hidden:
;-
pro test_EnWaterMAP_getSettings

  print, EnWaterMAP_getSettings()

end
