
PRO read_envi_header_ws, file_hdr, ncols, nrows, wl_center, wl_fwhm,data_type

if file_test(file_hdr + '.bsq.hdr') then file_hdr = file_hdr + '.bsq.hdr' else file_hdr = file_hdr + '.hdr'

a = ''
close,1,/force,exit_status=status
openr, 1, file_hdr
while ~eof(1) do begin
  readf, 1, a
  str = strsplit(a, ' = ', /extract)
  if str[0] eq 'samples' then ncols = long(str[1])
  if str[0] eq 'lines'   then nrows = long(str[1])
  if n_elements(str) gt 1 then if strjoin(str[0:1]) eq 'datatype'   then data_type = long(str[2])
  
  if str[0] eq 'bands'   then begin
    num_bd = long(str[1])
    wl_center = fltarr(num_bd)
    wl_fwhm   = fltarr(num_bd)
  endif
  if str[0] eq 'wavelength' then if str[1] ne 'units' then begin
    b= ''
    while str[0] ne 'fwhm' do begin
      readf, 1, a
      b = b + a
      str = strsplit(a, ' = ', /extract)
    endwhile
    str = strsplit(b, ', ', /extract)
    wl_center[0:num_bd -2] = float(str[0:num_bd -2])
    str = strsplit(str[num_bd -1], '}', /extract)
    wl_center[num_bd -1]   = float(str[0])
    b= ''
    while ~eof(1) do begin
      readf, 1, a
      b = b + a
      str = strsplit(a, ' = ', /extract)
    endwhile
    str = strsplit(b, ', ', /extract)
    wl_fwhm[0:num_bd -2] = float(str[0:num_bd -2])
    str = strsplit(str[num_bd -1], '}', /extract)
    wl_fwhm[num_bd -1] = float(str[0])
  endif
endwhile

close, 1

if wl_center[0] lt 1 then begin
  wl_center = wl_center * 0.001
  wl_fwhm   = wl_fwhm * 0.001
endif

if ~keyword_set(ncols) or ~keyword_set(nrows) then begin
  print, 'Error, wrong Header'
  stop
endif

END




pro mb_shadow_water_plants,path,l2file,flag_shadow,flag_water,flag_plants,scale,l2file_out

slash = path_sep()
cd, current   = path_act
  Help, Calls=callStack
  path_act  = strmid(((str=strmid(((c=callstack[0])),((start=strpos(c,'<')+1)),$
          strpos(c,'(')-start))),0,strpos(str,FILE_basename (strmid(c,((start=strpos(c,'<')+1)),strpos(c,'(')-start))))
path_act  = path_act + slash
read_envi_header_ws, filext(l2file, /path, /name), ncols, nrows, wvl, wl_fwhm,data_type
wl_center=wvl
num_bd = n_elements(wvl)
im = make_array(ncols, nrows, num_bd,type=data_type)
;im=reform(read_binary(l2file,type=data_type),ncols, nrows, num_bd,/over)
close,1,/force
openr, 1, l2file,/stdio
readu, 1, im
close, 1
im*=1.
im/=10d^(round(alog(scale)/alog(10d))-2d)
t0    = systime(1)
edifetot  = 445d /(2d *wvl)
if min(wl_center) lt 1 then wl_center*=1000.
sz=size(im,/dim)


  ;wvl=wvl[start:ende]
  ;im=im[*,*,start:ende]
  _=min(abs(wvl-450.),start)
  _=min(abs(wvl-500.),w500)
  _=min(abs(wvl-572.),w572)
  _=min(abs(wvl-604.),w604)
  _=min(abs(wvl-615.),w615)
  _=min(abs(wvl-640.),w640)
  _=min(abs(wvl-650.),w650)
  _=min(abs(wvl-665.),w665)
  _=min(abs(wvl-670.),w670)
  _=min(abs(wvl-675.),w675)
  _=min(abs(wvl-677.),w677)
  _=min(abs(wvl-680.),w680)
  _=min(abs(wvl-710.),w710)
  _=min(abs(wvl-720.),w720)
  _=min(abs(wvl-740.),w740)
  _=min(abs(wvl-810.),w810)
  _=min(abs(wvl-815.),w815)
  _=min(abs(wvl-860.),w860)
  _=min(abs(wvl-900.),ende)
  nel2=double(n_elements(im[0,0,start:ende]))
;  print,'Start wavelength: ',wvl[start]
;  print,'End wavelength: ',wvl[ende]
  im*=1d
  wvl*=1d
  ;envi_enter_data,im[*,*,start:ende],wl=wvl[start:ende]
  w677    +=  w677 eq w675? 1l  : 0l
  _     = min(   abs(    wvl-(wvl[w860]+wvl[[-1ul]])/2.    )   ,nir)
  ;bandüberlappungen vermeiden!

  nm710_max   = max(im[*,*,[w710,w720]],dimension=3,nm710_maxi)
  nm710_maxi    = (rebin(reform(  wvl[[w710,w720]],1,1,2),sz[0],sz[1],2))[nm710_maxi]
  vi        = nm710_max/im[*,*,w680]
  nb        = double(n_elements(wvl))
  specmean    = total(im[*,*,start:ende],3,/double)/nel2
  ;print,nel2,(im[0,0,start:ende])[*],total(im[0,0,start:ende],/double)

  mean_nir    = total(im[*,*,w860:ende],3,/double)/double(n_elements(im[0,0,w860:ende]))
  algae_slope710  = (im[*,*,w740]-nm710_max) / (wvl[w740] - nm710_maxi)
  algae_slope815  = (mean_nir-im[*,*,w815]) /(wvl[nir]-wvl[w815])
  max_vis     = max(im[*,*,w615:w675],dimension=3)
  vis_nir_ratio   =   max_vis / mean_nir

  aux = nm710_max & infaux=['nm710']

  aux = [[[aux]],[[vi]]]& infaux=[infaux,'vi']
  aux = [[[aux]],[[specmean]]]& infaux=[infaux,'specmean']
  aux = [[[aux]],[[mean_nir]]]& infaux=[infaux,'mean_nir']
  aux = [[[aux]],[[algae_slope710]]]& infaux=[infaux,'algae slope@710nm']
  aux = [[[aux]],[[algae_slope815]]]& infaux=[infaux,'algae slope@815nm']
  aux = [[[aux]],[[max_vis]]]&  infaux=[infaux,'maxvis']
  aux = [[[aux]],[[vis_nir_ratio]]]&  infaux=[infaux,'VIS/NIR ratio']


nel=double(n_elements(wvl))
u   = w604  & l   = w572
coeff1  = u-l gt 1? cr_ls_regr(im,wvl,l,u)  : u eq l?  (im[*,*,u+1l]-im[*,*,l])/((wvl[u+1l]-wvl[l])[0]) :  (im[*,*,u]-im[*,*,l])/((wvl[u]-wvl[l])[0])
u   = w665  & l   = w650
coeff2  = u-l gt 1? cr_ls_regr(im,wvl,l,u)  : u eq l?  (im[*,*,u+1l]-im[*,*,l])/((wvl[u+1l]-wvl[l])[0]) :  (im[*,*,u]-im[*,*,l])/((wvl[u]-wvl[l])[0])
u   = w677  & l   = w670
coeff3  = u-l gt 1? cr_ls_regr(im,wvl,l,u)  : u eq l?  (im[*,*,u+1l]-im[*,*,l])/((wvl[u+1l]-wvl[l])[0]) :  (im[*,*,u]-im[*,*,l])/((wvl[u]-wvl[l])[0])
u   = w740  & l   = w680
coeff4  = u-l gt 1? cr_ls_regr(im,wvl,l,u)  : u eq l?  (im[*,*,u+1l]-im[*,*,l])/((wvl[u+1l]-wvl[l])[0]) :  (im[*,*,u]-im[*,*,l])/((wvl[u]-wvl[l])[0])
u   = w810  & l   = ende
coeff5  = u-l gt 1? cr_ls_regr(im,wvl,l,u)  : u eq l?  (im[*,*,u+1l]-im[*,*,l])/((wvl[u+1l]-wvl[l])[0]) :  (im[*,*,u]-im[*,*,l])/((wvl[u]-wvl[l])[0])
fullspec_polyfit1_coeff = cr_ls_regr(im,wvl,start,ende)

if ~n_elements(smask2) then begin
  smask = total(im[*,*,start:ende] gt 0.00001,3,/double) ne 0d
  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask without background']

  smask *=  cr_scale(specmean*1.,100.,0.) lt 5d
  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask for dark pixel']
  ;return,smask
  smask *=  max(im[*,*,start:ende],dimension=3) gt 0.00001d
  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask wo background w dark pixel']
endif else begin
  smask = smask2
  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask without background']

  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask for dark pixel']

  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask wo background w dark pixel']
endelse


  waterslopes     = rebin(reform([1,2,4,8,16],1,1,5),sz[0],sz[1],5)
  waterslopes     *=  rebin(smask,sz[0],sz[1],5)
  waterslopes[*,*,0]  *=  coeff1 lt 0d
  waterslopes[*,*,1]  *=  coeff2 lt 0d
  waterslopes[*,*,2]  *=  coeff3 gt 0d
  waterslopes[*,*,3]  *=  coeff4 lt 0d
  waterslopes[*,*,4]  *=  coeff5 lt 0d

  aux   = [[[aux]],[[waterslopes]]]&  infaux=[infaux,'Coefficients for range: '+sindgen(5)]

  plants_under_water_mask =   smask
  aux   = [[[aux]],[[plants_under_water_mask]]]&  infaux=[infaux,'smask']
  plants_under_water_mask *=  (vi gt 1.)
  aux   = [[[aux]],[[plants_under_water_mask]]]&  infaux=[infaux,'Plants - vi gt 1']
  plants_under_water_mask *=  ((algae_slope710 lt -0.001) + (algae_slope815 lt -0.01)) ge 1.
  aux   = [[[aux]],[[plants_under_water_mask]]]&  infaux=[infaux,'Plants by algae slopes']
  smask         *=  ~plants_under_water_mask
  aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'Smask wo plants under water']
  smask_ohne_algen    =   smask
  combin          = total(waterslopes,3,/double)
  aux   = [[[aux]],[[combin]]]& infaux=[infaux,'sum water slopes']
  cmask_shadow      = combin lt 8d
  cmask_shadow      +=  combin eq 16d
  cmask_shadow      +=  combin eq 18d
  cmask_shadow      +=  combin eq 20d
  cmask_shadow      +=  combin eq 21d
  cmask_shadow      +=  combin eq 22d
  cmask_shadow      ge= 1
  aux   = [[[aux]],[[cmask_shadow]]]& infaux=[infaux,'shadow mask from slopes']
  cmask_shadow      *=  ~plants_under_water_mask
  aux   = [[[aux]],[[cmask_shadow]]]& infaux=[infaux,'shadow mask from slopes wo plants']
  cmask_water       = combin eq 9d
  cmask_water       +=  combin eq 10d
  cmask_water       +=  combin eq 11d
  cmask_water       +=  combin eq 15d
  cmask_water       +=  combin eq 26d
  cmask_water       +=  combin eq 27d
  cmask_water       +=  combin eq 31d
  aux   = [[[aux]],[[cmask_water]]]&  infaux=[infaux,'water mask from slopes']
  cmask_water       +=  plants_under_water_mask

  cmask_water       ge= 1d
  aux   = [[[aux]],[[cmask_water]]]&  infaux=[infaux,'shadow mask from slopes plus plants']

  cmask_unsure      = combin eq 8d
  cmask_unsure      +=  combin eq 12d
  cmask_unsure      +=  combin eq 13d
  cmask_unsure      +=  combin eq 14d
  cmask_unsure      +=  combin eq 17d
  cmask_unsure      +=  combin eq 19d
  cmask_unsure      +=  combin eq 23d
  cmask_unsure      +=  combin eq 24d
  cmask_unsure      +=  combin eq 25d
  cmask_unsure      +=  combin eq 28d
  cmask_unsure      +=  combin eq 29d
  cmask_unsure      +=  combin eq 30d
  aux   = [[[aux]],[[cmask_unsure]]]& infaux=[infaux,'unsure mask from slopes']
  cmask_unsure      *=  ~plants_under_water_mask

  cmask_unsure      ge= 1d
  aux   = [[[aux]],[[cmask_unsure]]]& infaux=[infaux,'unsure mask from slopes wo plants']

ksize       = 15d;15
cmask_still_unsure  = cmask_unsure
i         = 0d
print,'Iteration: ',i ,' filter size: ',ksize,' Unsure pixel: ',((tm=total(cmask_unsure)))
while tm gt 0 && ksize lt 50 do begin
  ksize       +=  i*2d
  fil         = replicate([1d],ksize,ksize)
  i++
  cmask_shadow_count  = convolve(cmask_shadow,fil)
  cmask_shadow_count  *=  cmask_still_unsure
  cmask_water_count = convolve(cmask_water,fil)
  cmask_water_count *=  cmask_still_unsure
  cmask_unsure2water  =   cmask_water_count
  cmask_unsure2water  gt= cmask_shadow_count
    cmask_unsure2shadow =   cmask_water_count
    cmask_unsure2shadow lt= cmask_shadow_count
  cmask_still_unsure  *=  cmask_water_count eq cmask_shadow_count
  tm          = total(cmask_still_unsure,/double)
  print,'Iteration: ',i ,' filter size: ',ksize,' Unsure pixel: ',tm
endwhile

aux   = [[[aux]],[[cmask_shadow_count]]]& infaux=[infaux,'shadow counts']
aux   = [[[aux]],[[cmask_water_count]]]&  infaux=[infaux,'water counts']
aux   = [[[aux]],[[cmask_unsure2water]]]& infaux=[infaux,'unsure water counts']
aux   = [[[aux]],[[cmask_unsure2shadow]]]&  infaux=[infaux,'unsure shadow counts']
aux   = [[[aux]],[[cmask_still_unsure]]]& infaux=[infaux,'still unsure']

watermask = cmask_water+cmask_unsure2water

aux   = [[[aux]],[[watermask]]]&  infaux=[infaux,'watermask + unsure water']
watermask +=  plants_under_water_mask&  infaux=[infaux,'water + plants']
watermask   ge= 1
aux   = [[[aux]],[[watermask]]]&  infaux=[infaux,'water + plants']
smask   *=  ~watermask
aux   = [[[aux]],[[smask]]]&  infaux=[infaux,'smask wo water']

;smask_ohne_wasser_und_algen = smask
ksize     = 3d
countim     = smask
countim     +=  convolve(smask,rebin([1],ksize,ksize)) ge (ksize*ksize -1d)
countim     ge= 1
aux   = [[[aux]],[[countim]]]&  infaux=[infaux,'smask + local filter pyramid']
smask_spatenh = countim
smask_spatenh *=  ~dilate(watermask,rebin([1d],ksize,ksize))
aux   = [[[aux]],[[smask_spatenh]]]&  infaux=[infaux,'smask with bufferzone']

;if certain_flag then begin
;  print, 'computing uncertainties'
;  im          = reform(im,sz[0]*sz[1],sz[2],/over)
;  certainty_shadow  = double(smask_spatenh)
;  l         = label_region(certainty_shadow,/all_neighbors,/ulong)
;  h         = histogram(double(l),reverse_indices=r)
;  wh          = where(h[1:*],c)
;  for i=0l,c-1l do begin
;    ind     = R[R[wh[I]] : R[wh[i]+1]-1]
;    nind    = double(n_elements(ind))
;    meanspec  = total(im[ind,*],1,/double)/nind
;    for j=0l,nind-1l do certainty_shadow[ind[j]]  = cr_correlate(meanspec,(im[ind[j],*])[*],/double)
;  endfor
;  certainty_shadow*=double(smask_spatenh)
;  certainty_shadow>=0d
;
;  certainty_water   = double(watermask)
;  l         = label_region(certainty_water,/all_neighbors,/ulong)
;  h         = histogram(double(l),reverse_indices=r)
;  wh          = where(h[1:*],c)
;  for i=0l,c-1l do begin
;    ind     = R[R[wh[I]] : R[wh[i]+1]-1]
;    nind    = double(n_elements(ind))
;    meanspec  = total(im[ind,*],1,/double)/nind
;    for j=0l,nind-1l do certainty_water[ind[j]] = cr_correlate(meanspec,(im[ind[j],*])[*],/double)
;  endfor
;  certainty_water*=double(watermask)
;  certainty_water>=0d
;
;
;  certainty_plants  = double(plants_under_water_mask)
;  l         = label_region(certainty_plants,/all_neighbors,/ulong)
;  h         = histogram(double(l),reverse_indices=r)
;  wh          = where(h[1:*],c)
;  for i=0l,c-1l do begin
;    ind     = R[R[wh[I]] : R[wh[i]+1]-1]
;    nind    = double(n_elements(ind))
;    meanspec  = total(im[ind,*],1,/double)/nind
;    for j=0l,nind-1l do certainty_plants[ind[j]]  = cr_correlate(meanspec,(im[ind[j],*])[*],/double)
;  endfor
;  certainty_plants*=double(plants_under_water_mask)
;  certainty_plants>=0d
;
;  certainty = [[[temporary(certainty_shadow)]],[[temporary(certainty_water)]],[[temporary(certainty_plants)]]]
;  im      = reform(im,sz,/over)
;endif
undefine,mask
flag_shadow*=total(smask_spatenh) gt 0
flag_water*=total(watermask) gt 0
flag_plants*=total(plants_under_water_mask) gt 0
if flag_shadow then mask=smask_spatenh
if flag_water then mask=~n_elements(mask)? watermask*2b : [[[mask]],[[watermask*2b]]]
if flag_plants then mask=~n_elements(mask)? plants_under_water_mask*3b : [[[mask]],[[plants_under_water_mask*3b]]]
 
 mask=size(mask,/n_dimensions) gt 2? total(mask,3,/nan) : temporary(mask)
u=uniq(mask,sort(mask))
nu=n_elements(u)

openw, 1, l2file_out
writeu, 1, byte(mask);
close, 1
desc_str1 = 'Shadows'
desc_str2 = 'Water'
desc_str3 = 'Plants under water'
file_hdr  = filext(l2file_out, /path, /name) + '.hdr'
desc_str  = [' Unclassified',flag_shadow? [', ',desc_str1] : '',flag_water? [', ',desc_str2] :'',flag_plants? [', ',desc_str3] :'']
wl_center = [1,flag_shadow,flag_water,flag_plants]
wh        = where(wl_center eq 1,c)
wl_center = indgen(c)


;write_header_LG, file_hdr, ncols, nrows, wl_center, desc_str, 1


;num_bd = n_elements(wl_center)
;if num_bd gt 1 then begin
;  wl_center_str = strtrim(wl_center[0:num_bd-2], 2) + ','
;  wl_center_str = [wl_center_str, strtrim(wl_center[num_bd-1], 2)+'}']
;  wl_fwhm_str = strtrim(wl_fwhm[0:num_bd-2], 2) + ','
;  wl_fwhm_str = [wl_fwhm_str, strtrim(wl_fwhm[num_bd-1], 2)+'}']
;endif else begin
;  wl_center_str = '}'
;  wl_fwhm_str = '}'
;endelse

class_lookup = [[0,   0,   0],$
                [255,   0,   0],$
                [0, 255,   0],$
                [0,   0, 255]]
class_lookup=class_lookup[*,wh]
class_lookup=strjoin(strcompress(class_lookup[*],/remove_all),',')
openw, 1, file_hdr
printf, 1, 'ENVI'
printf, 1, 'description = {Water and shadowmask}'
printf, 1, 'samples = ' + strtrim(ncols, 2)
printf, 1, 'lines   = ' + strtrim(nrows, 2)
printf, 1, 'bands   = 1'
printf, 1, 'header offset = 0'
printf, 1, 'file type = ENVI Classification'
printf, 1, 'data type = 1'
printf, 1, 'interleave = bsq'
printf, 1, 'byte order = 0'
printf, 1, 'sensor type = Unknown'
printf, 1, 'wavelength units = Unknown'
printf, 1, 'classes = '+strtrim(c, 2)+' '
printf, 1, 'class lookup = {'
printf, 1, class_lookup+'}'
printf, 1, 'class names = {' 
printf, 1, strjoin(desc_str)+'}'

close, 1





end

pro enmap_ws_wrapper, settings
if ~isa(settings) then settings = hash()

siz=20
Help, Calls=callStack
  path_act  = strmid(((str=strmid(((c=callstack[0])),((start=strpos(c,'<')+1)),$
          strpos(c,'(')-start))),0,strpos(str,FILE_basename (strmid(c,((start=strpos(c,'<')+1)),strpos(c,'(')-start))))
slash     = path_sep()
;path_act  = path_act + slash
;sample_dir  = path_act + 'sample_data' 
;sample_file = sample_dir  + slash + 'behy2005_ref_sub1.bsq'

sample_dir = settings.hubGetValue('ws_sample_dir')
sample_file = settings.hubgetValue('ws_sample_file')

hubAMW_program, top,Title='EnMAP Water and Shadow detection v0.001 (MB 2011, CR 2012)'
hubAMW_frame, Title='File I/O'
hubAMW_inputDirectoryName, title='Processing path ','path',value=sample_dir
hubAMW_inputFilename,      title='Reflectance file             ','l2file',value=sample_file
hubAMW_outputFilename,     title='Output file                  ','l2file_out',value= isa(sample_file) ? filext(sample_file, /path, /name)+'_masks.bsq' : !NULL
hubAMW_checkbox,           title='Detect shadows               ','flag_shadow',value=1
hubAMW_checkbox,           title='Detect water                 ','flag_water',value=1
hubAMW_checkbox,           title='Detect plants under water    ','flag_plants',value=1
hubAMW_parameter,         'scale', /float,     Title='Radiometric resolution (maximum value)', Size=siz,value=10000.,isge=0,isle=100000.;,unit=''
result = hubAMW_manage(/structure)
if result.accept eq 0 then return
mb_shadow_water_plants,result.path,result.l2file,result.flag_shadow,result.flag_water,result.flag_plants,result.scale ,result.l2file_out
end