;--------------------------------------------------------------------------------

function cr_ls_regr,im,wvl,l1,l2;only slope
sz  = size(im,/dim)
xm  = wvl[l1:l2-1l]
nel = double(n_elements(xm))
xm  -=  mean(xm,/double)
xmm = (xm##transpose(xm))
return,reform(xm##reform(im[*,*,l1:l2-1l],sz[0]*sz[1],nel)/rebin(xmm,sz[0]*sz[1]),sz[0],sz[1])
end