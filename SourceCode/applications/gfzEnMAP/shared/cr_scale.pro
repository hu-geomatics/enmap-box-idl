function cr_scale,im,upper,lower,double=double

upper = n_elements(upper) eq 0? 1d  : upper
lower = n_elements(lower) eq 0? 0d  : lower
;type = size(im,/type)
type  = size(upper,/type)

case type of $
  1b  : im  = byte(temporary(im))
  2b  : im  = fix(temporary(im))
  3b  : im  = long(temporary(im))
  4b  : im  = float(temporary(im))
  5b  : im  = double(temporary(im))
  6b  : im  = complex(temporary(im))
  9b  : im  = dcomplex(temporary(im))
  12b : im  = uint(temporary(im))
  13b : im  = ulong(temporary(im))
  14b : im  = long64(temporary(im))
  15b : im  = ulong64(temporary(im))

  else:
endcase

minv  = min(im,/nan,max=maxv)
im    +=  -minv
range = abs(maxv + (-minv))
im    /=  range ne 0? range : 1d
range2  = upper-lower
im    *=  range2 eq 0?  1:  range2
im    +=  lower
return,im
end