
;---------------------------------------------------------------------

; id filext.pro Version 1.0 by Peter Strobl, April 1994
;+
; NAME:
;    FILEXT
;
; PURPOSE:
;    Extracting PATH, NAME and EXTENSION of a filename
;
; CALLING SEQUENCE:
;    output_string = filext(fname, path=path, name=name, ext=ext,
;    low=low, up=up)
;
; PARAMETERS:
;    fname    : string containing input filename
;
; KEYWORDS:
;    path     : path is selected from input filename
;    filpath  : path is selected without delimiter in the end
;    name     : name is selected from input filename (without extensions)
;    ext      : extensions are selected from input filename
;    ver      : version is selected from input filename (vms only)
;    low      : uppercase characters of the output string are
;               modified into lowercase characters
;    up       : lowercase characters of the output string are
;               modified into uppercase characters
;
; COMMON BLOCKS:
;    none
; PROCEDURE:
;    straight forward
; SIDE EFFECTS:
;    none
; RESTRICTIONS:
;    none
; COMMENTS:
;    none
; MODIFICATION HISTORY:
;    11/99  Beisl,DLR    Win/MAC file names may contain blanks
;
;-----------------------------------------------------------------------------

FUNCTION filext, fname, path=path, filpath = filpath, name=name, ext=ext, $
                 ver=ver, low=low, up=up

opsys = !version.os_family
case opsys of
  'Windows' : slash = '\'
  'vms'     : slash = ']'
  'MacOS'   : slash = ':'
  else      : slash = '/'
endcase

;Win/MAC file names may include spaces! Trim only leading and trailing blanks
IF opsys EQ 'Windows' OR opsys EQ 'MacOS' THEN fname = [strtrim(fname,2)] $
ELSE                                           fname = [strcompress(fname,/rem)]

nnam = n_elements(fname)

posi = intarr(4,nnam)
strout = strarr(nnam)

for inam = 0,nnam-1 do begin

  posi(0,inam) = -1
  repeat posi(0,inam) = posi(0,inam)+1 $   ;**** Searching position of last slash
        until min(strpos(fname(inam),slash,posi(0,inam))) eq -1

  posi(1,inam) = posi(0,inam)-1
  repeat posi(1,inam) = posi(1,inam)+1 $    $
                                ;**** Searching position of last '.'
        until min(strpos(fname(inam),'.',posi(1,inam))) eq -1

  posi(1,inam) = posi(1,inam)-1

  if opsys eq 'vms' then begin
    posi(2,inam) = posi(1,inam)
    repeat posi(2,inam) = posi(2,inam)+1 $    ;**** Searching position of ';'
        until min(strpos(fname(inam),';',posi(2,inam))) eq -1
    posi(3,inam) = strlen(fname(inam))
    posi(2,inam) = posi(2,inam)-1
  endif else posi(2,inam) = strlen(fname(inam))

  if posi(2,inam) eq posi(1,inam)   then posi(2,inam) = posi(3,inam)
  if posi(1,inam) eq posi(0,inam)-1 then posi(1,inam) = posi(2,inam)
  if posi(1,inam) eq -1 then posi(1,inam) = posi(2,inam)

; **** Definition of new output string ****

  if keyword_set(path) then strout(inam) = strout(inam) + $
                            strmid(fname(inam),           0,posi(0,inam))
  if keyword_set(filpath) then strout(inam) = strout(inam) + $
                            strmid(fname(inam),           0,posi(0,inam)-1)
  if keyword_set(name) then strout(inam) = strout(inam) + $
                            strmid(fname(inam),posi(0,inam),posi(1,inam)-posi(0,inam))
  if keyword_set(ext)  then strout(inam) = strout(inam) + $
                            strmid(fname(inam),posi(1,inam),posi(2,inam)-posi(1,inam))
  if keyword_set(ver)  then strout(inam) = strout(inam) + $
                            strmid(fname(inam),posi(2,inam),posi(3,inam)-posi(2,inam))
endfor

; **** depending on the keyword characters are modified into lower- or uppercase characters ****

if keyword_set(up)   then strout = strupcase (strout)
if keyword_set(low)  then strout = strlowcase(strout)

if nnam eq 1 then strout = strout(0)
if nnam eq 1 then fname = fname(0)

return, strout
end
