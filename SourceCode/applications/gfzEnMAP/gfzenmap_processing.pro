;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application processing function. All (scientific) calculations
;    are performed inside this routine. Finally, a hash with report information
;    is returned and can be passed to the application report routine (`gfzEnMAP_showReport`). 
;
; :Returns:
;   Returns information for the application report routine. 
;  
; :Params:
; 
;    parameters : in, required, type=hash
;    
;    settings : in, optional, type=hash
;
;-
function gfzEnMAP_processing, parameters, settings

  ; check required parameters

  ; perform (image tile) processing
  ; if suitable, split your processing routine into individual image and data processing routines
  
  ; store results to be reported inside a hash
  
  result = hash()
  
  result['description'] = 'This application does nothing usefull. It is for demonstration only.'+ $
    strjoin(replicate('gfzEnMAP text ', 100))

  result['table'] = [ $
    'gfzEnMAP accuracy = 90%',$
    '',$
    'gfzEnMAP 1 | gfzEnMAP 2 | gfzEnMAP 3',$
    '------------------------------------',$
    '      100% |       100% |       80% ',$
    '       70% |        50% |       80% ']

  result['image'] = bytscl(randomu(seed, 300, 300, 3))

  return, result

end

;+
; :Hidden:
;-
pro test_gfzEnMAP_processing

  ; test your routine
  
  parameters = hash() ;replace by own hash definition
  settings = gfzEnMAP_getSettings()
  
  result = gfzEnMAP_processing(parameters, settings)
  print, result

end  
