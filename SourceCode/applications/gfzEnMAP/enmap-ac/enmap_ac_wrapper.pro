pro enmap_ac_wrapper, settings
  if ~isa(settings) then settings = hash()

siz=20

Help, Calls=callStack
  path_act  = strmid(((str=strmid(((c=callstack[0])),((start=strpos(c,'<')+1)),$
          strpos(c,'(')-start))),0,strpos(str,FILE_basename (strmid(c,((start=strpos(c,'<')+1)),strpos(c,'(')-start))))

slash     = path_sep()
;path_act  = path_act; + slash
;sample_dir  = path_act + 'sample_data' + slash + 'sample1'
;sample_file = sample_dir  + slash + 'TOARAD_sub.bsq'

sample_dir = settings.hubGetValue('ac_sample_dir')
sample_file = settings.hubGetValue('ac_sample_file')

;top=widget_base(scr_xsize=600,scr_ysize=800,/scroll)
hubAMW_program, top,Title='EnMAP Atmospheric Correction v0.003 (LG 2010, CR 2012)'
hubAMW_frame, Title='File I/O'
hubAMW_inputDirectoryName, title='Processing path ','path',value=sample_dir
hubAMW_inputFilename,      title='Radiance file   ','l1file',value=sample_file
hubAMW_inputFilename,      title='DEM file        ','DEMfile',/optional
hubAMW_inputFilename,      title='Cloud file      ','img_cl_mask',/optional
hubAMW_inputFilename,      title='Quality mask    ','img_qa_mask',/optional
;hubAMW_inputFilename,      title='Water vapor file','img_wv_mask',/optional


;hubAMW_frame,              Title='Correction parameters'

hubAMW_frame,              Title='Smile'
hubAMW_parameter,         'smile_fil', /float,     Title='Filter size for spectral smoothing', Size=siz,value=9,isge=3,isle=19,unit='bands'
hubAMW_parameter,         'smile_bands', /string,  Title='Absorption bands', Size=siz,value='749;777',unit='nm'

hubAMW_frame,              Title='AOT'
hubAMW_checkbox,                            title='Automatic AOT retrieval  ','aot_val',value=1
hubAMW_parameter, 'aot550_val', /float,     Title='Fixed visibility (automatic AOT mode must be off)                        ', Size=siz,value=100,isge=0.1,isle=1000,unit='km'
hubAMW_parameter, 'cld_aot_thre', /float,   Title='Probability threshold for AOT retrieval (related to cloud image)         ', Size=siz,value=2,isge=0,isle=100,unit='%'
hubAMW_parameter, 'cld_rfl_thre', /float,   Title='Probability threshold for REFL (& CWV) retrieval (related to cloud image)', Size=siz,value=5,isge=0,isle=100,unit='%'
hubAMW_parameter,         'aot_bands', /string,  Title='Absorption bands', Size=siz,value='420;690',unit='nm'
hubAMW_parameter,         'ndvi_bands', /string,  Title='NDVI bands', Size=siz,value='670;785',unit='nm'
hubAMW_parameter,         'ndvi_thres', /string,  Title='NDVI thresholds', Size=siz,value='0.01;0.1;0.4;0.9     ',unit='nm'

hubAMW_frame,              Title='Water vapour'
hubAMW_parameter,         'wv_bands', /string,  Title='Absorption bands', Size=siz,value='861;892;921         ',unit='nm'

hubAMW_frame,              Title='Adjacency'
hubAMW_checkbox,                            title='Adjacency correction     ','ady_flg',value=1
hubAMW_parameter, 'fac_dst', /float,        Title='Adjacency range (Adjacency correction must be selected)', Size=siz,value=33,isge=1,isle=500,unit='pixel'

hubAMW_frame,              Title='General'
hubAMW_checkbox,                            title='Smooth spectra with boxcar (broadest two atmospheric windows will be excluded)','smooth_flg',value=1
hubAMW_parameter, 'smooth_dst', /float,     Title='Filter size for spectral smoothing', Size=siz,value=3,isge=3,isle=9,unit='bands'







hubAMW_frame,              Title='Scene parameters     '

;hubAMW_parameter, 'gmt', /float, Title='GMT', IsGE=0., IsLE=24., Size=5
;hubAMW_parameter, 'jday', /float, Title='Julian Day', Size=5
hubAMW_checkbox,           title='Intercalary','intercalary',value=0
hubAMW_parameter, 'year', /float,    Title='Year                         ', Size=siz,value=2012
hubAMW_parameter, 'month', /float,   Title='Month                        ', Size=siz,isge=0,isle=12,value=8
hubAMW_parameter, 'day', /float,     Title='Day                          ', Size=siz,isge=0,isle=31,value=11
hubAMW_parameter, 'hour', /float,    Title='Hour (UTC)                   ', Size=siz,isge=0,isle=24,value=9
hubAMW_parameter, 'minute', /float,  Title='Minute                       ', Size=siz,isge=0,isle=60,value=30
hubAMW_parameter, 'second', /float,  Title='Second                       ', Size=siz,isge=0,isle=60,value=0
hubAMW_parameter, 'vza_inp', /float, Title='Viewing Zenit Angle          ', IsGE=0., IsLE=90., $
Unit=string(176b)+' (mostly 0,only relevant for side looking)' , Size=siz,value=0.
hubAMW_parameter, 'vaa_inp', /float, Title='Viewing Azimut Angle         ', IsGE=-360., IsLE=360., $
Unit=string(176b)+' (mostly 97.666)' , Size=siz,value=97.666
hubAMW_parameter, 'lat', /float,     Title='Latitude of center           ', Size=siz,isge=-90.,isle=90.,unit=string(176b),value=50.7 
hubAMW_parameter, 'lon', /float,     Title='Longitude of center          ', Size=siz,isge=-180.,isle=180,unit=string(176b),value=12.
hubAMW_parameter, 'mdem', /float,    Title='Mean DEM Height        ', Size=siz,isge=-2,isle=9,unit='km (uncheck if DEM is available)',optional=1,value=0.05

hubAMW_frame,              Title='Output parameters    '
hubAMW_checkbox,           title='Generate additional plots','enmap_ac_verbose',value=0
hubAMW_checkbox,           title='Additional products (Map for water vapor)','aux_flg',value=1
hubAMW_checkbox,           title='Mark atmospheric bands as bad in the result','bbl_flg',value=1


;vza_inp, vaa_inp, gmt, jday, month, lat, lon, dem_name, ady_flg
result = hubAMW_manage(/structure)
;WIDGET_CONTROL, /REALIZE,top
;xmanager,'menu',top
if result.accept eq 0 then return
aot550_val  = result.aot_val eq 1?  0 : 3.912/result.aot550_val ;Koschmieder

;cld_aot_thre = 0.02 ; probability threshold for AOT retrieval
;cld_rfl_thre = 0.05 ; probability threshold for REFL (& CWV) retrieval


;case (1b) of 
;  result.path eq ''       :  begin & _=dialog_message('You must enter the path',/error) &return &end
;  result.l1file eq ''     :  begin & _=dialog_message('You must enter the file',/error) &return &end
;  result.vza_inp eq ''    :  begin & _=dialog_message('You must enter the VZA',/error) &return &end
;  result.vaa_inp eq ''    :  begin & _=dialog_message('You must enter the VAA',/error) &return &end
;  result.gmt eq ''        :  begin & _=dialog_message('You must enter the hour',/error) &return &end
;  result.minute eq ''       :  begin & _=dialog_message('You must enter the minute',/error) &return &end
;  result.second eq ''       :  begin & _=dialog_message('You must enter the second',/error) &return &end
;  result.year eq ''        :  begin & _=dialog_message('You must enter the year',/error) &return &end
;  result.month eq ''       :  begin & _=dialog_message('You must enter the month',/error) &return &end
;  result.day eq ''       :  begin & _=dialog_message('You must enter the day',/error) &return &end  
;endcase

jday=julday(result.month,result.day,result.year,result.hour,result.minute,result.second)
gmt = result.hour+result.minute/60. + result.second/(60.*60.)
EnMAP_AtmCor_LUTv004, $
path_dat=result.path,$
img_rad=result.l1file,$
verbose=result.enmap_ac_verbose,$
ady_flg=result.ady_flg,$
vza_inp=result.vza_inp,$
vaa_inp=result.vaa_inp,$
gmt=gmt,$
jday=jday,$
month=result.month,$
lat=result.lat,$
lon=result.lon,$
mdem=result.mdem,$
dem_name=total(tag_names(result) eq strupcase('demfile')) eq 1?  result.demfile  : '',$
img_qa_mask=total(tag_names(result) eq strupcase('img_qa_mask')) eq 1?  result.img_qa_mask  : '',$
img_cl_mask=total(tag_names(result) eq strupcase('img_cl_mask')) eq 1?  result.img_cl_mask  : '',$
aux_flg=result.aux_flg,$
aot550_val=aot550_val,$
cld_aot_thre=result.cld_aot_thre/100.,$
cld_rfl_thre=result.cld_rfl_thre/100.,$
intercalary=result.intercalary,$
bbl_flg=result.bbl_flg,$
fac_dst=result.fac_dst,$
smooth_flg=result.smooth_flg,smooth_dst=result.smooth_dst,$
smile_bands=float(strsplit(result.smile_bands,';',/extract)),$
smile_fil=result.smile_fil,$
aot_bands=float(strsplit(result.aot_bands,';',/extract)),$
ndvi_bands=float(strsplit(result.ndvi_bands,';',/extract)),$
ndvi_thres=float(strsplit(result.ndvi_thres,';',/extract)),$
wv_bands=float(strsplit(result.wv_bands,';',/extract))
;,$
;img_wv_mask=total(tag_names(result) eq strupcase('img_wv_mask')) eq 1?  result.img_wv_mask  : ''


;EnMAP_AtmCor_LUT,path_dat=result.path,img_rad=result.l1file,dem_name=result.demfile,verbose=result.enmap_ac_verbose
;stop
end