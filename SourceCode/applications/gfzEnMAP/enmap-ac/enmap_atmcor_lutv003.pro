;********************************************************************************************
;********************************************************************************************
;  EnMAP_AtmCor_LUT.pro
;
;  version 23/Nov/2010
;
;  Luis Guanter, Freie UniversitÃ¤t Berlin
;  luis.guanter@wew.fu-berlin.de
;
;********************************************************************************************

;********************************************************************************************
; *** Purpose:
;
; To perform atmospheric correction of simulated EnMAP images
;
; *** Main features:
;
; - Atmospheric parameters provided by a LUT generated with MODTRAN4
;
; - Automatic smile characterization, aerosol and water vapor retrieval
;     and adjacency correction
;
; *** Inputs:
;
;  Mandatory
; - Simulated Top-of-atmosphere Radiance image in ENVI format
; - log file produced in the forward simulation
; - Co-registered DEM in ENVI format
;
;  Optional
; - Cloud and quality masks
;
; *** Outputs:

; - Reflectance image in ENVI format (scaled *10000)
; - Water vapor map as a by product of the processing
;
;********************************************************************************************
;********************************************************************************************
; mods: C. Rogass, v0001,extensions to fit the enmap box, 11062012
;       C. Rogass, v0002,bugfixing, e.g. wv output,19062012
;       C. Rogass, v0003, water vapour input, 27082012


;****************************
FUNCTION interpol_lut, avis, asol, phi, hsurf, aot, wv
common lut_inp, lut1, lut2, num_par, nm_bnd, xnodes, nm_nodes, ndim, lim, lut_cell, x_cell

;*** lectura de parÃ¡metros

vtest = [avis, asol, hsurf, aot, phi, wv]

for i = 0, ndim-1 do begin
  wh = where(vtest[i] lt xnodes[i, *])
  lim[0, i] = wh[0] - 1
  lim[1, i] = wh[0]
endfor

cont=0
for i = 0, 1 do $
  for j = 0, 1 do $
    for k = 0, 1 do $
      for ii = 0, 1 do $
        for jj = 0, 1 do $
          for kk = 0, 1 do begin

          lut_cell[0, cont, *] = lut1[0, *, 0, lim[jj,4], lim[ii,3], lim[k,2], lim[j,1], lim[i,0]]
          for ind = 1, num_par - 1 do lut_cell[ind, cont, *] = lut2[ind-1, *, lim[kk,5], 0, lim[ii,3], lim[k,2], lim[j,1], lim[i,0]]
          cont = cont + 1

          endfor

; se puede sustituir por 0's y 1's en los nodos (normalizar solo el vector a interpolar)
for i = 0, ndim - 1 do vtest[i] = (vtest[i] - xnodes[i, lim[0, i]]) / (xnodes[i, lim[1, i]] - xnodes[i, lim[0, i]])

; en este punto: nodos con posiciÃ³n x_cell y valor lut_cell. Los nodos se numeran de 0 a 63,
; siguiendo un orden 'binario': 000000, 000001, 000010, ..., 111110, 111111 (0=inf, 1=sup)

;*** se contruye la interpolacion
; f(x,y) = V000000*f(x0,y0,z0,xx0,yy0,zz0)+V000001*f(x0,y0,z0,xx0,yy0,zz1)+...+V111111*f(x1,y1,z1,xx1,yy1,zz1)
; con V111111=(x-x0)(y-y0)(z-z0)(xx-xx0)(yy-yy0)(zz-zz0)

f_int = fltarr(num_par, nm_bnd)
for i = 0, nm_nodes - 1 do begin
  weight = abs(product(vtest - x_cell[*, nm_nodes - 1 -i]))
  for ind = 0, num_par - 1 do f_int[ind, *] = f_int[ind, *] + (weight * lut_cell[ind, i, *])
endfor

return, f_int

END


FUNCTION chisq_AHS_WV_refl, wv
common chi_sq_wv_refl, lpw, egl, sab, toa_AHS, refl_pix, wv_gr, dim_wv, wv_p, wv_inf

if (wv gt wv_gr[0] and wv lt wv_gr[dim_wv - 1]) then begin

  wv = wv[0]
  wh = where(wv gt wv_gr, cnt)
  wv_inf = wh[cnt - 1]
  wv_p = (wv - wv_gr[wv_inf]) / (wv_gr[wv_inf + 1] - wv_gr[wv_inf])

  lpw_int = reform((lpw[wv_inf +  1, *] - lpw[wv_inf, *]) * wv_p + lpw[wv_inf, *])
  egl_int = reform((egl[wv_inf +  1, *] - egl[wv_inf, *]) * wv_p + egl[wv_inf, *])
  sab_int = reform((sab[wv_inf +  1, *] - sab[wv_inf, *]) * wv_p + sab[wv_inf, *])

  toa_sim = lpw_int + refl_pix * egl_int / (1. - sab_int * refl_pix) / !pi

  chisq = total(toa_AHS - toa_sim)

endif else chisq = 5000.

return, chisq
END


PRO smile_processor, toa_img, ncols, nrows, wl_center, wl_fwhm, s_norm_ini, vza_inp, sza_inp, phi_inp, hsf_inp, $
cld_img, cld_aot_thre, wvl_LUT, aot550, msk_img, dwl_arr_sm,verbose=verbose,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands
common inputs_shift, wvl, wl_center2, wl_fwhm2, num_bd, lpw, egl, sab, toa, refl_smooth, wh_abs, denom, exp_arr

wvl = wvl_LUT
wl_center2 = wl_center
wl_fwhm2 = wl_fwhm

num_bd = n_elements(wl_center)

mus_inp = cos(sza_inp * !dtor)

tot_pix = long(ncols) * nrows
wh_land = where(cld_img le cld_aot_thre and msk_img eq 1, cnt_land)
toa_sub = fltarr(cnt_land, num_bd)
for i = 0, num_bd - 1 do toa_sub[*, i] = toa_img[wh_land + tot_pix * i]

sp_resol = abs(mean(wl_center[0:num_bd-2]-wl_center[1:num_bd-1]))

;***********************************************************************************************************************

dim_cwv = n_elements(cwv_gr)

num_wvl_LUT = n_elements(wvl_LUT)

a=interpol_lut(vza_inp, sza_inp, phi_inp, hsf_inp, aot550, 2.)
lpw = a[0, *]*1.e+4
egl = (a[1, *] * mus_inp + a[2, *])*1.e+4
sab = a[3, *]

;**********************************************************************************
; Calculate smile

toa_ac_tr = fltarr(ncols, num_bd)
for ind = 0, ncols - 1 do for i = 0, num_bd - 1 do begin
  col_arr= toa_img[ind, *, i]
  wh = where(col_arr ne 0., cnt)
  if cnt gt 0 then toa_ac_tr[ind, i] = mean(col_arr[wh]) else toa_ac_tr[ind, i] = 0.
endfor

;;;;;;;;;;;;;

exp_max = 2.
exp_min = 2.
exp_arr = exp_max + (exp_min - exp_max) * findgen(num_bd) / (num_bd-1)
c_arr = (1./(2.^exp_arr*alog(2.)))^(1./exp_arr)
denom = 1. / (wl_fwhm * c_arr)

;wvl_o2 = [749., 777.] ;wvl_o2 = [749., 820.]
wvl_o2 = ~n_elements(smile_bands)?  [749., 777.]  : smile_bands ;wvl_o2 = [749., 820.]
wh_o2 = where(wl_center ge wvl_o2[0] and wl_center le wvl_o2[1], cnt_o2)
lim_1 = wh_o2[0]
lim_2 = wh_o2[cnt_o2 - 1]

wh_abs = wh_o2

norm_int = 1. / (wl_center[lim_2] - wl_center[lim_1])

dwl = fltarr(ncols)

xa_ini = -6.
xb_ini = 0.
xc_ini = 6.
ftol = 1.e-5 ; convergence value for minF_parabolic
niter_max = 1.e+3  ; maximum number of iterations for minF_parabolic

for ind = 0, ncols - 1 do begin

  toa = toa_ac_tr[ind, *]

  surf_refl_retriever, 0., surf_refl_ini

;  refl_smooth = reform(smooth(surf_refl_ini, 10))  ; smoothed to remove possible spikes due to spectral shift
  refl_smooth = reform(smooth(surf_refl_ini, ~n_elements(smile_fil)?  10  : smile_fil,/EDGE_TRUNCATE))  ; smoothed to remove possible spikes due to spectral shift

  t = (surf_refl_ini[lim_2] - surf_refl_ini[lim_1]) * (wl_center[wh_o2] - wl_center[lim_1]) * norm_int + surf_refl_ini[lim_1]
  refl_smooth[wh_o2]  = t

  minF_parabolic, xa_ini,xb_ini,xc_ini, xmin, fmin, FUNC_NAME='chisq_CASI_shift', MAX_ITERATIONS=maxit, TOLERANCE=TOL
  dwl[ind] = xmin

endfor

dwl_arr_sm = smooth(dwl, sp_resol * 2, /edge)
if verbose then begin
  window, /free, title = 'Smile', xsize = 700, ysize = 600
  plot, dwl_arr_sm, background=255, color=0, xtitle = 'Across Track (#)', ytitle =textoidl('\Delta\lambda')+' (nm)', $
  charsize=2, yrange=[min([dwl_arr_sm, dwl_arr_sm]), max([dwl_arr_sm, dwl_arr_sm])], xrange=[0, ncols-1], /xs
  oplot, replicate(0., ncols), linestyle=2, color=0
endif


END



PRO EnMAP_AOT_inv_land, toa_sub, lpw_aot, egl_aot, sab_aot, dn2rad, wl_center, wl_fwhm, aot_gr_inv, dem_sub, mus_sub, aot550, valid_flg,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands

common fits_atm, aot_old
common fits, toa, chi_sq
common inversion, lpw, egl, sab, ro_veg, ro_sue, aot_gr_inv2, dim_aot_inv, num_pix, num_bd, ref_pix, wl_center_inv
common static, lpw_int, egl_int, sab_int

lpw = lpw_aot
egl = egl_aot
sab = sab_aot

num_bd = n_elements(wl_center)


wl_MER = [0.412545, 0.442401, 0.489744, 0.509700, 0.559634, 0.619620, 0.664640, 0.680902, 0.708426, 0.753472, 0.761606, 0.778498, 0.864833, 0.884849, 0.899860] *1000.
ro_veg_MER_1 = [0.0235,0.0267,0.0319,0.0342,0.0526,0.0425,0.0371,0.0369,0.0789,0.3561,0.3698,0.3983,0.4248,0.4252,0.4254] ;veg_verde_new
;ro_veg_MER_1[1] = ro_veg_MER_1[0] + (ro_veg_MER_1[2]-ro_veg_MER_1[0]) * (wl_MER[1]-wl_MER[0])/(wl_MER[2]-wl_MER[0])
ro_veg_MER_2 = [0.0206,0.02984,0.0445,0.0498,0.0728,0.0821,0.0847,0.0870,0.1301,0.1994,0.2020,0.2074,0.2365,0.2419,0.2459] ;bosque
;ro_veg_MER_2[1] = ro_veg_MER_2[0] + (ro_veg_MER_2[2]-ro_veg_MER_2[0]) * (wl_MER[1]-wl_MER[0])/(wl_MER[2]-wl_MER[0])
ro_veg_MER_3 = [0.0138,0.0158,0.0188,0.021,0.0395,0.0279,0.0211,0.0206,0.0825,0.2579,0.2643,0.2775,0.3201,0.3261,0.330700]

ro_sue_MER = [0.0490,0.0860,0.1071,0.1199,0.1679,0.2425,0.2763,0.2868,0.3148,0.3470,0.3498,0.3558,0.3984,0.4062,0.4120]

ro_veg1 = interpol(ro_veg_MER_1, wl_MER, wl_center)
ro_veg2 = interpol(ro_veg_MER_2, wl_MER, wl_center)
ro_veg3 = interpol(ro_veg_MER_3, wl_MER, wl_center)
ro_sue = interpol(ro_sue_MER, wl_MER, wl_center)

ro_veg_all = [[ro_veg1], [ro_veg2], [ro_veg3]]

num_pix = 5
aot_gr_inv2 = aot_gr_inv
dim_aot_inv = n_elements(aot_gr_inv)
if ~n_elements(ndvi_bands) then begin
wh_red = where(wl_center gt 670.)
wh_nir = where(wl_center gt 785.)
endif else begin
wh_red = where(wl_center gt ndvi_bands[0])
wh_nir = where(wl_center gt ndvi_bands[1])
;stop
endelse

ind_red = wh_red[0] & ind_nir = wh_nir[0]
rad_red = toa_sub[*, ind_red]
rad_nir = toa_sub[*, ind_nir]
ndvi_img = (rad_nir - rad_red) / (rad_nir + rad_red)
rad_red = 0 & rad_nir = 0

dem_mean = mean(dem_sub)
mus_mean = mean(mus_sub)

dem_lim = [0.8 * dem_mean, 1.2 * dem_mean]  ; Se permite un 15% de variaciï¿½ en hs y SZA (corregido, no suave)
mus_lim = [0.9 * mus_mean, 1.1 * mus_mean]

if ~n_elements(ndvi_thres) then begin
index_hi = where(ndvi_img gt 0.4 and ndvi_img lt 0.90 and dem_sub gt dem_lim[0] and dem_sub lt dem_lim[1] and mus_sub gt mus_lim[0] and mus_sub lt mus_lim[1], cont_hi)
                                                                   ;index_* : indices de pixeles que cumplen condicion
index_me = where(ndvi_img gt 0.10 and ndvi_img lt 0.4 and dem_sub gt dem_lim[0] and dem_sub lt dem_lim[1] and mus_sub gt mus_lim[0] and mus_sub lt mus_lim[1], cont_me)
                                                                   ;cont_*  : num de pixeles que cumplen codicion
index_lo = where(ndvi_img gt 0.01 and ndvi_img lt 0.10 and dem_sub gt dem_lim[0] and dem_sub lt dem_lim[1] and mus_sub gt mus_lim[0] and mus_sub lt mus_lim[1], cont_lo)
endif else begin
index_hi = where(ndvi_img gt ndvi_thres[2] and ndvi_img lt ndvi_thres[3] and dem_sub gt dem_lim[0] and dem_sub lt dem_lim[1] and mus_sub gt mus_lim[0] and mus_sub lt mus_lim[1], cont_hi)
                                                                   ;index_* : indices de pixeles que cumplen condicion
index_me = where(ndvi_img gt ndvi_thres[1] and ndvi_img lt ndvi_thres[2] and dem_sub gt dem_lim[0] and dem_sub lt dem_lim[1] and mus_sub gt mus_lim[0] and mus_sub lt mus_lim[1], cont_me)
                                                                   ;cont_*  : num de pixeles que cumplen codicion
index_lo = where(ndvi_img gt ndvi_thres[0] and ndvi_img lt ndvi_thres[1] and dem_sub gt dem_lim[0] and dem_sub lt dem_lim[1] and mus_sub gt mus_lim[0] and mus_sub lt mus_lim[1], cont_lo)
endelse





max_cat = 100

if (2 + cont_me lt num_pix) or (cont_hi lt 2) then valid_flg = 0 else begin

  cont_hi = cont_hi < max_cat
  cont_me = cont_me < max_cat
  cont_lo = cont_lo < max_cat


  valid_flg = 1

  pos_ref_hi = lonarr(cont_hi)

  ref_hi = lonarr(cont_hi)                                     ;ref_* : indices de pixeles elegidos
  ord_arr = reverse(sort(ndvi_img[index_hi]))
  for i = 0, cont_hi - 1 do ref_hi[i] = ord_arr[i]        ; Se asigna pixeles con max(NDVI)
  pos_ref_hi = index_hi[ref_hi]
  ref_pix_hi = fltarr(cont_hi, num_bd)
  for i=0, cont_hi - 1 do begin
    ref_pix_hi[i, *] = toa_sub[pos_ref_hi[i], *]
;   window, i
;   ndvi_val = (ref_pix_hi[i, 9]/ solirr9 - ref_pix_hi[i, 7]/ solirr7) / (ref_pix_hi[i, 9]/ solirr9 + ref_pix_hi[i, 7]/ solirr7)
;   plot, wl_center, ref_pix_hi[i, *], title='NDVI= '+string(ndvi_val);+', (X,Y) = '+string(x_pos)+', '+ string(y_pos)
  endfor

;stop
  if cont_lo gt 0 then begin
    pos_ref_lo = lonarr(cont_lo)
    ref_lo = lonarr(cont_lo)                                     ;ref_* : indices de pixeles elegidos
    ord_arr = sort(ndvi_img[index_lo])
    for i = 0, cont_lo - 1 do ref_lo[i] = ord_arr[i]
    pos_ref_lo = index_lo[ref_lo]
    ref_pix_lo = fltarr(cont_lo, num_bd)
    for i=0, cont_lo - 1 do begin
      ref_pix_lo[i, *] = toa_sub[pos_ref_lo[i], *]
;    window, i
;    ndvi_val = (ref_pix_lo[i, 9]/ solirr9 - ref_pix_lo[i, 7]/ solirr7) / (ref_pix_lo[i, 9]/ solirr9 + ref_pix_lo[i, 7]/ solirr7)
;    plot, wl_center, ref_pix_lo[i, *], title='NDVI= '+string(ndvi_val);+', (X,Y) = '+string(x_pos)+', '+ string(y_pos)
    endfor
  endif else ref_pix_lo = 0
;stop
  if cont_me gt 0 then begin
    pos_ref_me = lonarr(cont_me)
    ref_me = lonarr(cont_me)                                     ;ref_* : indices de pixeles elegidos
    ord_arr = reverse(sort(ndvi_img[index_me]))
    for i = 0, cont_me - 1 do ref_me[i] = ord_arr[i]        ; Se asigna pixeles con max(NDVI)
    pos_ref_me = index_me[ref_me]
    ref_pix_me = fltarr(cont_me, num_bd)
    for i=0, cont_me - 1 do begin
      ref_pix_me[i, *] = toa_sub[pos_ref_me[i], *]
;    window, i
;    ndvi_val = (ref_pix_me[i, 9]/ solirr9 - ref_pix_me[i, 7]/ solirr7) / (ref_pix_me[i, 9]/ solirr9 + ref_pix_me[i, 7]/ solirr7)
;    plot, wl_center, ref_pix_me[i, *], title='NDVI= '+string(ndvi_val);+', (X,Y) = '+string(x_pos)+', '+ string(y_pos)
    endfor
  endif else ref_pix_me = 0

  n_lim = (cont_hi / 2) < (cont_me / 3)
  ref_pix_all = fltarr(n_lim, num_pix, num_bd)
  for i = 0, n_lim - 1 do begin
    ref_pix_all[i, 0:1, *] = ref_pix_hi[2*i:2*i+1, *]
    if i lt cont_lo then begin
      ref_pix_all[i, 2:3, *] = ref_pix_me[2*i:2*i+1, *]
      ref_pix_all[i, 4, *] = ref_pix_lo[i, *]
    endif else ref_pix_all[i, 2:4, *] = ref_pix_me[2*i:2*i+2, *]
  endfor

  n_EM_veg = 3
  lim_ref_sets = 5

  n_ref_sets = n_elements(ref_pix_all[*, 0, 0])
  n_ref_sets = n_ref_sets < lim_ref_sets

  ftol = 1.e-4

;***************************************************
  aot_arr = fltarr(n_ref_sets)
  fmin_arr = fltarr(n_EM_veg)
  aot_arr_aux = fltarr(n_EM_veg)
;stop

; ; Inicializaciï¿½ de variables
; ;stop
;
   wl_center_inv = 1000. / wl_center
   wh_nir_no = where(wl_center gt 850., complement = wh_vis)
   wl_center_inv[wh_nir_no] = 0.
   wl_center_inv[0] = 0.

   num_x = 2*num_pix+1


  for ind = 0, n_ref_sets - 1 do begin

    ref_pix = reform(ref_pix_all[ind, *, *])

    for ind_EM = 0, n_EM_veg - 1 do begin

      weight = [2., 2., 1.5, 1.5, 1.]


      P = fltarr(num_x)
      for i = 0, num_pix - 1 do begin
        ndvi_val = (ref_pix[i, ind_nir] - ref_pix[i, ind_red]) / (ref_pix[i, ind_nir] + ref_pix[i, ind_red])
        dum = 1.3 * ndvi_val + 0.25
        P[2*i] = dum  > 0.
        P[2*i+1] = (1. - dum) > 0.
 ;  print, ndvi_val
      endfor
; ;stop
      P[10] = aot_gr_inv[dim_aot_inv / 2]
      aot_old = aot_gr_inv[dim_aot_inv / 2] - 0.05
;
      ftol = 1.e-7
      xi = fltarr(num_x, num_x)
      ind2 = indgen(num_x)
      for i = 0, num_x - 1 do xi[ind2[i], ind2[i]] = 1.

      ro_veg = ro_veg_all[*, ind_EM]

      POWELL, P, xi, ftol, fmin, 'minim_TOA', ITER = iter, ITMAX = 20000

       mean_chi = mean(chi_sq)
       wh_chi = where(chi_sq gt 2*mean_chi, cnt_chi)
;     for i = 0, num_pix - 1 do begin
;       window, /free, title = strtrim((p[10]), 2) + '1'
;       plot, wl_center, ref_pix[i,*], xrange=[400, 1000]
;       oplot, wl_center, toa[i,*], thick = 2, color=200
;     endfor
; stop
      if cnt_chi gt 0 then begin

        weight[wh_chi] = 0.
        POWELL, P, xi, ftol, fmin, 'minim_TOA', ITER = iter, ITMAX = 20000
;     for i = 0, num_pix - 1 do begin
;       window, /free, title = strtrim((p[10]), 2) + 'bucle sin peor caso'
;       plot, wl_center, ref_pix[i,*]
;       oplot, wl_center, toa[i,*], thick = 2, color=200
;     endfor
; stop
      endif

      aot_arr_aux[ind_EM] = p[num_x-1]
      fmin_arr[ind_EM] = fmin / (5. - cnt_chi)

;    stop
    endfor

    wh_fmin = where(fmin_arr eq min(fmin_arr))
    aot_arr[ind] = aot_arr_aux[wh_fmin[0]]
    EM_code = wh_fmin[0] + 1

  endfor
;stop
  if n_ref_sets gt 1 then begin
    aot_mean = mean(aot_arr)
    aot_stddev = stddev(aot_arr)

    wh_stddev = where(aot_arr ge aot_mean - aot_stddev * 1.5 and aot_arr le aot_mean + aot_stddev * 1.5, cnt_wh_stddev)
    if cnt_wh_stddev gt 0 then begin
      aot550 = mean(aot_arr[wh_stddev])
      aot_stddev = stddev(aot_arr[wh_stddev])
    endif else vis = vis_mean
  endif else begin
    aot = aot_arr[0]
    aot_stddev = 0.
  endelse

endelse

;***************************************************

END

FUNCTION minim_TOA, x
common fits, toa, chi_sq
common fits_atm, vis_old
common inversion, lpw, egl, sab, ro_veg, ro_sue, vis_gr, dim_vis, n_pix, num_bd, ref_pix, wl_center_inv
common static, lpw_int, egl_int, sab_int

wh = where(x lt 0., cont_neg)
if cont_neg le 0. and x[10] gt vis_gr[0] and x[10] lt vis_gr[dim_vis - 1] then begin

  weight = [2., 2., 1., 1., 1.5]

  vis = x[10] ;vis = ln(ln(vis))
  if vis ne vis_old then begin
    wh = where(vis gt vis_gr)
    vis_inf = wh[n_elements(wh) - 1]

    delta = 1. / (vis_gr[vis_inf + 1] - vis_gr[vis_inf])

    lpw_int = ((lpw[*, vis_inf + 1] - lpw[*, vis_inf]) * vis + lpw[*, vis_inf] * vis_gr[vis_inf + 1] - $
                       lpw[*, vis_inf + 1] * vis_gr[vis_inf]) * delta

    egl_int = ((egl[*, vis_inf + 1] - egl[*, vis_inf]) * vis + egl[*, vis_inf] * vis_gr[vis_inf + 1] - $
                       egl[*, vis_inf + 1] * vis_gr[vis_inf]) * delta

    sab_int = ((sab[*, vis_inf + 1] - sab[*, vis_inf]) * vis + sab[*, vis_inf] * vis_gr[vis_inf + 1] - $
                       sab[*, vis_inf + 1] * vis_gr[vis_inf]) * delta
  endif

  toa = fltarr(n_pix, num_bd)
  chi_sq = fltarr(n_pix)
  for i = 0, n_pix - 1 do begin
    surf_refl = x[2 * i] * ro_veg + x[2 * i + 1] * ro_sue
    toa[i, *] = lpw_int + surf_refl * egl_int / !pi /(1 - sab_int * surf_refl)
    chi_sq[i] = weight[i] * total(wl_center_inv * (ref_pix[i,*] - toa[i,*]) ^ 2.) ;ref_pix ya corregidos con (1.e-4 * d * d)
  endfor

  minim = total(chi_sq)
;  endelse
  vis_old = vis

endif else minim = 5.e+8

;stop
return, minim

END


function chisq_CASI_shift, dwl
common inputs_shift, wvl_LUT, wl_center, wl_fwhm, num_bd, lpw, egl, sab, toa, refl_smooth, wh_o2, denom, exp_arr
common outputs, surf_refl_retr

; exp_max = 6.
; exp_min = 2.
; exp_arr = exp_max + (exp_min - exp_max) * findgen(num_bd) / (num_bd-1)
; c_arr = (1./(2.^exp_arr*alog(2.)))^(1./exp_arr)
; denom = 1. / (wl_fwhm * c_arr)

lpw_filter = fltarr(num_bd)
egl_filter = fltarr(num_bd)
sab_filter = fltarr(num_bd)

dwl=dwl[0]
for bd = 0, num_bd - 1 do begin
  wl_center_sh = wl_center + dwl
  li1 = where (wvl_LUT ge (wl_center_sh[bd] - 2.* wl_fwhm[bd]) and wvl_LUT le (wl_center_sh[bd] + 2.* wl_fwhm[bd]), cnt)
  if (cnt gt 0) then begin
    tmp =abs(wl_center_sh[bd] - wvl_LUT[li1])*denom[bd]
    s = exp(-(tmp^exp_arr[bd]))
    norm = total(s)
    lpw_filter[bd] = total(lpw[li1]*s) / norm
    egl_filter[bd] = total(egl[li1]*s) / norm
    sab_filter[bd] = total(sab[li1]*s) / norm
  endif
endfor

xterm = !pi * (toa - lpw_filter)/egl_filter
surf_refl_retr = reform(xterm / (1. + sab_filter * xterm))

chi_sq = total((refl_smooth[wh_o2] - surf_refl_retr[wh_o2]) ^ 2)
;stop
return, chi_sq
end


pro surf_refl_retriever, dwl , surf_refl
common inputs_shift, wvl_LUT, wl_center, wl_fwhm, num_bd, lpw, egl, sab, toa, refl_smooth, wh_o2, denom, exp_arr

lpw_filter = fltarr(num_bd)
egl_filter = fltarr(num_bd)
sab_filter = fltarr(num_bd)

dwl=dwl[0]
for bd = 0, num_bd - 1 do begin
  wl_center_sh = wl_center + dwl
  li1 = where (wvl_LUT ge (wl_center_sh[bd] - 2.* wl_fwhm[bd]) and wvl_LUT le (wl_center_sh[bd] + 2.* wl_fwhm[bd]), cnt)
  if (cnt gt 0) then begin
    tmp =abs(wl_center_sh[bd] - wvl_LUT[li1])*denom[bd]
    s = exp(-(tmp^exp_arr[bd]))
    norm = total(s)
    lpw_filter[bd] = total(lpw[li1]*s) / norm
    egl_filter[bd] = total(egl[li1]*s) / norm
    sab_filter[bd] = total(sab[li1]*s) / norm
  endif
endfor

xterm = !pi * (toa - lpw_filter)/egl_filter
surf_refl = xterm / (1. + sab_filter * xterm)

;stop

end



pro minF_parabolic, xa,xb,xc, xmin, fmin, FUNC_NAME=func_name,    $
                                          MAX_ITERATIONS=maxit,   $
                                          TOLERANCE=TOL,          $
                                          POINT_NDIM=pn, DIRECTION=dirn
;+
; NAME:
;       MINF_PARABOLIC
; PURPOSE:
;       Minimize a function using Brent's method with parabolic interpolation
; EXPLANATION:
;       Find a local minimum of a 1-D function up to specified tolerance.
;       This routine assumes that the function has a minimum nearby.
;       (recommend first calling minF_bracket, xa,xb,xc, to bracket minimum).
;       Routine can also be applied to a scalar function of many variables,
;       for such case the local minimum in a specified direction is found,
;       This routine is called by minF_conj_grad, to locate minimum in the
;       direction of the conjugate gradient of function of many variables.
;
; CALLING EXAMPLES:
;       minF_parabolic, xa,xb,xc, xmin, fmin, FUNC_NAME="name"  ;for 1-D func.
;  or:
;       minF_parabolic, xa,xb,xc, xmin, fmin, FUNC="name", $
;                                         POINT=[0,1,1],   $
;                                         DIRECTION=[2,1,1]     ;for 3-D func.
; INPUTS:
;       xa,xb,xc = scalars, 3 points which bracket location of minimum,
;               that is, f(xb) < f(xa) and f(xb) < f(xc), so minimum exists.
;               When working with function of N variables
;               (xa,xb,xc) are then relative distances from POINT_NDIM,
;               in the direction specified by keyword DIRECTION,
;               with scale factor given by magnitude of DIRECTION.
; INPUT KEYWORDS:
;      FUNC_NAME = function name (string)
;               Calling mechanism should be:  F = func_name( px )
;               where:
;                       px = scalar or vector of independent variables, input.
;                       F = scalar value of function at px.
;
;      POINT_NDIM = when working with function of N variables,
;               use this keyword to specify the starting point in N-dim space.
;               Default = 0, which assumes function is 1-D.
;      DIRECTION = when working with function of N variables,
;               use this keyword to specify the direction in N-dim space
;               along which to bracket the local minimum, (default=1 for 1-D).
;               (xa, xb, xc, x_min are then relative distances from POINT_NDIM)
;      MAX_ITER = maximum allowed number iterations, default=100.
;      TOLERANCE = desired accuracy of minimum location, default=sqrt(1.e-7).
; OUTPUTS:
;       xmin = estimated location of minimum.
;               When working with function of N variables,
;               xmin is the relative distance from POINT_NDIM,
;               in the direction specified by keyword DIRECTION,
;               with scale factor given by magnitude of DIRECTION,
;               so that min. Loc. Pmin = Point_Ndim + xmin * Direction.
;       fmin = value of function at xmin (or Pmin).
; PROCEDURE:
;       Brent's method to minimize a function by using parabolic interpolation.
;       Based on function BRENT in Numerical Recipes in FORTRAN (Press et al.
;       1992),  sec.10.2 (p. 397).
; MODIFICATION HISTORY:
;       Written, Frank Varosi NASA/GSFC 1992.
;       Converted to IDL V5.0   W. Landsman   September 1997
;-
        zeps = 1.e-7                    ;machine epsilon, smallest addition.
        goldc = 1 - (sqrt(5)-1)/2       ;complement of golden mean.

        if N_elements( TOL ) NE 1 then TOL = sqrt( zeps )
        if N_elements( maxit ) NE 1 then maxit = 100

        if N_elements( pn ) LE 0 then begin
                pn = 0
                dirn = 1
           endif

        xLo = xa < xc
        xHi = xa > xc
        xmin = xb
        fmin = call_function( func_name, pn + xmin * dirn )
        xv = xmin  &  xw = xmin
        fv = fmin  &  fw = fmin
        es = 0.

        for iter = 1,maxit do begin

                goldstep = 1
                xm = (xLo + xHi)/2.
                TOL1 = TOL * abs(xmin) + zeps
                TOL2 = 2*TOL1

                if ( abs( xmin - xm ) LE ( TOL2 - (xHi-xLo)/2. ) ) then return

                if (abs( es ) GT TOL1) then begin

                        r = (xmin-xw) * (fmin-fv)
                        q = (xmin-xv) * (fmin-fw)
                        p = (xmin-xv) * q + (xmin-xw) * r
                        q = 2 * (q-r)
                        if (q GT 0) then p = -p
                        q = abs( q )
                        etemp = es
                        es = ds

                        if (p GT q*(xLo-xmin)) AND $
                           (p LT q*(xHi-xmin)) AND $
                           (abs( p ) LT abs( q*etemp/2 )) then begin
                                ds = p/q
                                xu = xmin + ds
                                if (xu-xLo LT TOL2) OR (xHi-xu LT TOL2) then $
                                        ds = TOL1 * (1-2*((xm-xmin) LT 0))
                                goldstep = 0
                           endif
                   endif

                if (goldstep) then begin
                        if (xmin GE xm) then  es = xLo-xmin  else  es = xHi-xmin
                        ds = goldc * es
                   endif

                xu = xmin + (1-2*(ds LT 0)) * ( abs( ds ) > TOL1 )
                fu = call_function( func_name, pn + xu * dirn )

                if (fu LE fmin) then begin

                        if (xu GE xmin) then xLo=xmin else xHi=xmin
                        xv = xw  &  fv = fw
                        xw = xmin  &  fw = fmin
                        xmin = xu  &  fmin = fu

                  endif else begin

                        if (xu LT xmin) then xLo=xu else xHi=xu

                        if (fu LE fw) OR (xw EQ xmin) then begin

                                xv = xw  &  fv = fw
                                xw = xu  &  fw = fu

                          endif else if (fu LE fv) OR (xv EQ xmin) $
                                                   OR (xv EQ xw) then begin
                                xv = xu  &  fv = fu
                           endif
                   endelse
          endfor

        message,"exceeded maximum number of iterations: "+strtrim(iter,2),/INFO
return
end
;********************************************************************************
;***
;***  varsol: IDL de 6S, da DSOL en forma de Richter
;***
;********************************************************************************
PRO varsol, jday, month, dsol

if month gt 2 and month le 8 then J=31*(MONTH-1)-((MONTH-1)/2)-2+JDAY
if month le 2 then J=31*(MONTH-1)+JDAY
if month gt 8 then J=31*(MONTH-1)-((MONTH-2)/2)-2+JDAY
PI=2.*ACOS (0.)
OM=(.9856*FLOAT(J-4))*PI/180.
DSOL=1./((1.-.01673*COS(OM))^2)
DSOL = 1./DSOL^0.5
return
END


pro zensun,day,time,lat,lon,zenith,azimuth,solfac,sunrise,sunset,local=local,$
            latsun=latsun,lonsun=lonsun
;+
; ROUTINE:      zensun
;
; PURPOSE:      Compute solar position information as a function of
;               geographic coordinates, date and time.
;
; USEAGE:       zensun,day,time,lat,lon,zenith,azimuth,solfac,sunrise,sunset,
;                  local=local
;
; INPUT:
;   day         Julian day (positive scalar or vector)
;               (spring equinox =  80)
;               (summer solstice= 171)
;               (fall equinox   = 266)
;               (winter solstice= 356)
;
;   time        Universal Time in hours (scalar or vector)
;
;   lat         geographic latitude of point on earth's surface (degrees)
;
;   lon         geographic longitude of point on earth's surface (degrees)
;
; OUTPUT:
;
;   zenith      solar zenith angle (degrees)
;
;   azimuth     solar azimuth  (degrees)
;               Azimuth is measured clockwise from due north
;
;   solfac      Solar flux multiplier.  SOLFAC=cosine(ZENITH)/RSUN^2
;               where rsun is the current earth-sun distance in
;               astronomical units.
;
;               NOTE: SOLFAC is negative when the sun is below the horizon
;
;   sunrise     UT of sunrise (hours)
;   sunset      UT of sunset  (hours)
;
;
; KEYWORD INPUT:
;
;   local       if set, TIME is specified as a local time and SUNRISE
;               and SUNSET are output in terms of local time
;
;               NOTE: "local time" is defined as UT + local_offset
;                     where local_offset is fix((lon+sign(lon)*7.5)/15)
;                     with -180 < lon < 180
;
;                     Be aware, there are no fancy provisions for
;                     day-light savings time and other such nonsense.
;
; KEYWORD OUTPUT:
;
;   latsun      the latitude of the sub-solar point (fairly constant over day)
;               Note that daily_minimum_zenith=abs(latsun-lat)
;
;   lonsun      the longitude of the sub-solar point
;               Note that at 12 noon local time (lon-lonsun)/15. is the
;               number of minutes by which the sun leads the clock.
;
;
;; EXAMPLE 1:   Compute the solar flux at Palmer Station for day 283
;
;               time=findgen(1+24*60)/60
;               zensun,283,time,-64.767,-64.067,z,a,sf
;               solflx=sf*s
;               plot,time,solflx
;
;               where s is the TOA solar irradiance at normal incidence:
;
;               s=1618.8   ; W/m2/micron for AVHRR1 GTR100
;               s= 976.9   ; W/m2/micron for AVHRR2 GTR100
;               s=1685.1   ; W/m2/micron for 410nm GTR100
;               s= 826.0   ; W/m2/micron for 936nm GTR100
;               s=1.257e17 ; photons/cm2/s PAR GTR100
;               s=1372.9   ; w/m2 total broadband
;
;
;; EXAMPLE 2:   Find time of sunrise and sunset for current day
;
;     doy=julday()-julday(1,0,1994)
;     zensun,doy,12,34.456,-119.813,z,a,s,sr,ss,/local &$
;     zensun,doy,[sr,.5*(sr+ss),ss],34.456,-119.813,z,az,/local &$
;     print,'sunrise: '+hms(3600*sr)+      ' PST   azimuth angle: ',az(0)  &$
;     print,'sunset:  '+hms(3600*ss)+      ' PST   azimuth angle: ',az(2)  &$
;     print,'zenith:  '+hms(1800*(ss+sr))+ ' PST   zenith angle:  ',z(1)
;
; PROCEDURE:
;
; 1.  Calculate the subsolar point latitude and longitude, based on
;     DAY and TIME. Since each year is 365.25 days long the exact
;     value of the declination angle changes from year to year.  For
;     precise values consult THE AMERICAN EPHEMERIS AND NAUTICAL
;     ALMANAC published yearly by the U.S. govt. printing office.  The
;     subsolar coordinates used in this code were provided by a
;     program written by Jeff Dozier.
;
;  2. Given the subsolar latitude and longitude, spherical geometry is
;     used to find the solar zenith, azimuth and flux multiplier.
;
;  AUTHOR:      Paul Ricchiazzi        23oct92
;               Earth Space Research Group,  UCSB
;
;  REVISIONS:
;
; jan94: use spline fit to interpolate on EQT and DEC tables
; jan94: output SUNRISE and SUNSET, allow input/output in terms of local time
; jan97: declare eqtime and decang as floats.  previous version
;         this caused small offsets in the time of minimum solar zenith
;-
;  eqt = equation of time (minutes)  ; solar longitude correction = -15*eqt
;  dec = declination angle (degrees) = solar latitude
;
; LOWTRAN v7 data (25 points)
;     The LOWTRAN solar position data is characterized by only 25 points.
;     This should predict the subsolar angles within one degree.  For
;     increased accuracy add more data points.
;
;nday=[   1.,    9.,   21.,   32.,   44.,   60.,  91.,  121.,  141.,  152.,$
;       160.,  172.,  182.,  190.,  202.,  213., 244.,  274.,  305.,  309.,$
;       325.,  335.,  343.,  355.,  366.]
;
;eqt=[ -3.23, -6.83,-11.17,-13.57,-14.33,-12.63, -4.2,  2.83,  3.57,  2.45,$
;       1.10, -1.42, -3.52, -4.93, -6.25, -6.28,-0.25, 10.02, 16.35, 16.38,$
;       14.3, 11.27,  8.02,  2.32, -3.23]
;
;dec=[-23.07,-22.22,-20.08,-17.32,-13.62, -7.88, 4.23, 14.83, 20.03, 21.95,$
;      22.87, 23.45, 23.17, 22.47, 20.63, 18.23, 8.58, -2.88,-14.18,-15.45,$
;     -19.75,-21.68,-22.75,-23.43,-23.07]
;
; Analemma information from Jeff Dozier
;     This data is characterized by 74 points
;

if n_params() eq 0 then begin
  xhelp,'zensun'
  return
endif

nday=[  1.0,   6.0,  11.0,  16.0,  21.0,  26.0,  31.0,  36.0,  41.0,  46.0,$
       51.0,  56.0,  61.0,  66.0,  71.0,  76.0,  81.0,  86.0,  91.0,  96.0,$
      101.0, 106.0, 111.0, 116.0, 121.0, 126.0, 131.0, 136.0, 141.0, 146.0,$
      151.0, 156.0, 161.0, 166.0, 171.0, 176.0, 181.0, 186.0, 191.0, 196.0,$
      201.0, 206.0, 211.0, 216.0, 221.0, 226.0, 231.0, 236.0, 241.0, 246.0,$
      251.0, 256.0, 261.0, 266.0, 271.0, 276.0, 281.0, 286.0, 291.0, 296.0,$
      301.0, 306.0, 311.0, 316.0, 321.0, 326.0, 331.0, 336.0, 341.0, 346.0,$
      351.0, 356.0, 361.0, 366.0]

eqt=[ -3.23, -5.49, -7.60, -9.48,-11.09,-12.39,-13.34,-13.95,-14.23,-14.19,$
     -13.85,-13.22,-12.35,-11.26,-10.01, -8.64, -7.18, -5.67, -4.16, -2.69,$
      -1.29, -0.02,  1.10,  2.05,  2.80,  3.33,  3.63,  3.68,  3.49,  3.09,$
       2.48,  1.71,  0.79, -0.24, -1.33, -2.41, -3.45, -4.39, -5.20, -5.84,$
      -6.28, -6.49, -6.44, -6.15, -5.60, -4.82, -3.81, -2.60, -1.19,  0.36,$
       2.03,  3.76,  5.54,  7.31,  9.04, 10.69, 12.20, 13.53, 14.65, 15.52,$
      16.12, 16.41, 16.36, 15.95, 15.19, 14.09, 12.67, 10.93,  8.93,  6.70,$
       4.32,  1.86, -0.62, -3.23]

dec=[-23.06,-22.57,-21.91,-21.06,-20.05,-18.88,-17.57,-16.13,-14.57,-12.91,$
     -11.16, -9.34, -7.46, -5.54, -3.59, -1.62,  0.36,  2.33,  4.28,  6.19,$
       8.06,  9.88, 11.62, 13.29, 14.87, 16.34, 17.70, 18.94, 20.04, 21.00,$
      21.81, 22.47, 22.95, 23.28, 23.43, 23.40, 23.21, 22.85, 22.32, 21.63,$
      20.79, 19.80, 18.67, 17.42, 16.05, 14.57, 13.00, 11.33,  9.60,  7.80,$
       5.95,  4.06,  2.13,  0.19, -1.75, -3.69, -5.62, -7.51, -9.36,-11.16,$
     -12.88,-14.53,-16.07,-17.50,-18.81,-19.98,-20.99,-21.85,-22.52,-23.02,$
     -23.33,-23.44,-23.35,-23.06]

;
; compute the subsolar coordinates
;

tt=((fix(day)+time/24.-1.) mod 365.25) +1.  ;; fractional day number
                                            ;; with 12am 1jan = 1.

if n_elements(tt) gt 1 then begin
  eqtime=tt-tt                              ;; this used to be day-day, caused
  decang=eqtime                             ;; error in eqtime & decang when a
  ii=sort(tt)                               ;; single integer day was input
  eqtime(ii)=spline(nday,eqt,tt(ii))/60.
  decang(ii)=spline(nday,dec,tt(ii))
endif else begin
  eqtime=spline(nday,eqt,tt)/60.
  decang=spline(nday,dec,tt)
endelse
latsun=decang

if keyword_set(local) then begin
  lonorm=((lon + 360 + 180 ) mod 360 ) - 180.
  tzone=fix((lonorm+7.5)/15)
  index = where(lonorm lt 0, cnt)
  if (cnt gt 0) then tzone(index) = fix((lonorm(index)-7.5)/15)
  ut=(time-tzone+24.) mod 24.                  ; universal time
  noon=tzone+12.-lonorm/15.                    ; local time of noon
endif else begin
  ut=time
  noon=12.-lon/15.                             ; universal time of noon
endelse

lonsun=-15.*(ut-12.+eqtime)

; compute the solar zenith, azimuth and flux multiplier

t0=(90.-lat)*!dtor                            ; colatitude of point
t1=(90.-latsun)*!dtor                         ; colatitude of sun

p0=lon*!dtor                                  ; longitude of point
p1=lonsun*!dtor                               ; longitude of sun

zz=cos(t0)*cos(t1)+sin(t0)*sin(t1)*cos(p1-p0) ; up          \
xx=sin(t1)*sin(p1-p0)                         ; east-west    > rotated coor
yy=sin(t0)*cos(t1)-cos(t0)*sin(t1)*cos(p1-p0) ; north-south /

azimuth=atan(xx,yy)/!dtor                     ; solar azimuth
zenith=acos(zz)/!dtor                         ; solar zenith

rsun=1.-0.01673*cos(.9856*(tt-2.)*!dtor)      ; earth-sun distance in AU
solfac=zz/rsun^2                              ; flux multiplier

if n_params() gt 7 then begin
  ;angsun=6.96e10/(1.5e13*rsun)                ; solar disk half-angle
  angsun=0.
  arg=-(sin(angsun)+cos(t0)*cos(t1))/(sin(t0)*sin(t1))
  sunrise = arg - arg
  sunset  = arg - arg + 24.
  index = where(abs(arg) le 1, cnt)
  if (cnt gt 0) then begin
    dtime=acos(arg(index))/(!dtor*15)
    sunrise(index)=noon-dtime-eqtime(index)
    sunset(index)=noon+dtime-eqtime(index)
  endif
endif

return
end

PRO gfunct, X, A, F, pder
  F = A[0] * X
  pder = X
END

FUNCTION ZBRENT, x1, x2, FUNC_NAME=func_name,    $
                         MAX_ITERATIONS=maxit, TOLERANCE=TOL
;+
; NAME:
;     ZBRENT
; PURPOSE:
;     Find the zero of a 1-D function up to specified tolerance.
; EXPLANTION:
;     This routine assumes that the function is known to have a zero.
;     Adapted from procedure of the same name in "Numerical Recipes" by
;     Press et al. (1992), Section 9.3
;
; CALLING:
;       x_zero = ZBRENT( x1, x2, FUNC_NAME="name", MaX_Iter=, Tolerance= )
;
; INPUTS:
;       x1, x2 = scalars, 2 points which bracket location of function zero,
;                                               that is, F(x1) < 0 < F(x2).
;       Note: computations are performed with
;       same precision (single/double) as the inputs and user supplied function.
;
; REQUIRED INPUT KEYWORD:
;       FUNC_NAME = function name (string)
;               Calling mechanism should be:  F = func_name( px )
;               where:  px = scalar independent variable, input.
;                       F = scalar value of function at px,
;                           should be same precision (single/double) as input.
;
; OPTIONAL INPUT KEYWORDS:
;       MAX_ITER = maximum allowed number iterations, default=100.
;       TOLERANCE = desired accuracy of minimum location, default = 1.e-3.
;
; OUTPUTS:
;       Returns the location of zero, with accuracy of specified tolerance.
;
; PROCEDURE:
;       Brent's method to find zero of a function by using bracketing,
;       bisection, and inverse quadratic interpolation,
;
; EXAMPLE:
;       Find the root of the COSINE function between 1. and 2.  radians
;
;        IDL> print, zbrent( 1, 2, FUNC = 'COS')
;
;       and the result will be !PI/2 within the specified tolerance
; MODIFICATION HISTORY:
;       Written, Frank Varosi NASA/GSFC 1992.
;       FV.1994, mod to check for single/double prec. and set zeps accordingly.
;       Converted to IDL V5.0   W. Landsman   September 1997
;       Use MACHAR() to define machine precision   W. Landsman September 2002
;-
        if N_params() LT 2 then begin
             print,'Syntax - result = ZBRENT( x1, x2, FUNC_NAME = ,'
             print,'                  [ MAX_ITER = , TOLERANCE = ])'
             return, -1
        endif

        if N_elements( TOL ) NE 1 then TOL = 1.e-3
        if N_elements( maxit ) NE 1 then maxit = 100

        if size(x1,/TNAME) EQ 'DOUBLE' OR size(x2,/TNAME) EQ 'DOUBLE' then begin
                xa = double( x1 )
                xb = double( x2 )
                zeps = (machar(/DOUBLE)).eps   ;machine epsilon in double.
          endif else begin
                xa = x1
                xb = x2
                zeps = (machar(/DOUBLE)).eps   ;machine epsilon, in single
           endelse

        fa = call_function( func_name, xa )
        fb = call_function( func_name, xb )
        fc = fb

        if (fb*fa GT 0) then begin
;                message,"root must be bracketed by the 2 inputs",/INFO
                return,xa
           endif

        for iter = 1,maxit do begin

                if (fb*fc GT 0) then begin
                        xc = xa
                        fc = fa
                        Din = xb - xa
                        Dold = Din
                   endif

                if (abs( fc ) LT abs( fb )) then begin
                        xa = xb   &   xb = xc   &   xc = xa
                        fa = fb   &   fb = fc   &   fc = fa
                   endif

                TOL1 = 0.5*TOL + 2*abs( xb ) * zeps     ;Convergence check
                xm = (xc - xb)/2.

                if (abs( xm ) LE TOL1) OR (fb EQ 0) then return,xb

                if (abs( Dold ) GE TOL1) AND (abs( fa ) GT abs( fb )) then begin

                        S = fb/fa       ;attempt inverse quadratic interpolation

                        if (xa EQ xc) then begin
                                p = 2 * xm * S
                                q = 1-S
                          endif else begin
                                T = fa/fc
                                R = fb/fc
                                p = S * (2*xm*T*(T-R) - (xb-xa)*(R-1) )
                                q = (T-1)*(R-1)*(S-1)
                           endelse

                        if (p GT 0) then q = -q
                        p = abs( p )
                        test = ( 3*xm*q - abs( q*TOL1 ) ) < abs( Dold*q )

                        if (2*p LT test)  then begin
                                Dold = Din              ;accept interpolation
                                Din = p/q
                          endif else begin
                                Din = xm                ;use bisection instead
                                Dold = xm
                           endelse

                  endif else begin

                        Din = xm    ;Bounds decreasing to slowly, use bisection
                        Dold = xm
                   endelse

                xa = xb
                fa = fb         ;evaluate new trial root.

                if (abs( Din ) GT TOL1) then xb = xb + Din $
                                        else xb = xb + TOL1 * (1-2*(xm LT 0))

                fb = call_function( func_name, xb )
          endfor

        message,"exceeded maximum number of iterations: "+strtrim(iter,2),/INFO

return, xb
END




; $Id: utils_all.pro,v 1.1 1999/09/24 10:23:26 dschlapf Exp $
;+
; NAME:
;   load_cols
;
; PURPOSE:
;   function for reading in ascii data formatted into columns
;
; CALLING SEQUENCE:
;   num_cols = load_cols(name,values, head=head)
;
; PARAMETERS:
;   name:    name of file to read from.
;   values:  fltarr(num_cols,num_lines) containing the data on exit
;
; KEYWORDS:
;   none
;
; RETURN VALUE:
;   load_cols returns the number of columns the data contains or,
;     -1 for an invalid file.
;
; COMMON BLOCKS:
;   none
;
; PROCEDURE:
;   read the file in one line at a time and only allow valid
;     bytes ('0'-'9','+','-','e','E','.') in the column data.  allows
;     for some 'invalid' text before the data starts, however nothing
;     invalid after the data starts is allowed.
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; COMMENTS:
;   generalized version of load_ascii routine
;
; MODIFICATION HISTORY:
;   23 November 1991, ABL - Initial revision.
;    5 December 1991, ATS - Routine now returns instead of crashing when
;                             called with an invalid file name.
;    2 January  1992, PJB - Standardized comment header.
;    9 June     1994, A.M.- Skip n headerlines
; $Log: utils_all.pro,v $
; Revision 1.1  1999/09/24 10:23:26  dschlapf
; Initial revision
;
; Revision 2.3  92/01/02  09:36:58  barloon
; Standardized comment header.
;
; Revision 2.2  91/12/05  16:33:36  shapiro
; load_cols now returns instead of crashing when called with an invlid
; file name.
;
; Revision 2.1  91/12/04  11:49:08  shapiro
; Initial Revision.
;
;-
function load_cols,name,values, head=head
!error = 0 & on_ioerror,trouble
lut = bytarr(256) & lut = lut + 1
lut(byte(' ')) = 0 & lut(byte(',')) = 0 & lut(byte('.')) = 0
lut(byte('e')) = 0 & lut(byte('E')) = 0
lut(byte('+')) = 0 & lut(byte('-')) = 0
lut(48:57) = 0 ; byte('0') - byte('9')
nlut = bytarr(256) & nlut(48:57) = 1
openr,unit,name,/get
;
; check for a carriage return ascii-10 in the first 1024 bytes. if there
; isn't one then don't consider this formatted data
;
info = fstat(unit)
if (info.size eq 0) then begin
  ;
  ; empty file
  ;
  close,unit & free_lun,unit & return,-1
endif
stuff = bytarr(1024<info.size) & readu,unit,stuff
ret = where(stuff eq 10,count)
if (count eq 0) then begin
  ;
  ; no carriage return in first 1024 bytes
  ;
  close,unit & free_lun,unit & return,-1
endif
point_lun,unit,0 ; reset the file pointer
if keyword_set(head) ne 0 then begin
      dum1 = ''
      for i=0, head-1 do readf,unit,dum1
      head = dum1
endif

count = 0 & & first_line = 1
while (not eof(unit)) do begin
  line = '' & readf,unit,line
  bline = byte(strtrim(strcompress(line),2))
  if (total(lut(bline)) eq 0 and total(nlut(bline)) gt 0) then begin
    ;
    ; only valid characters are present in the string
    ;
    sptr = where(bline eq 32, ncol)
    if (first_line eq 1) then begin
      num_cols = ncol + 1 & vals = fltarr(num_cols,32000) & first_line = 0
    endif else $
      if (num_cols ne ncol + 1) then begin
        ;
        ; data contains different number of columns
        ;
        close,unit & free_lun,unit & return,-1
  print,'3'
      endif
    bline = string(bline) & slen = strlen(bline)
    vals(0,count) = float(bline)
    ;
    ; parse out the columned data into floating point values
    ;
    for i=1,num_cols-1 do $
      vals(i,count) = float(strmid(bline,sptr(i-1)+1,slen))
    count = count + 1
  endif else if (first_line eq 0) then begin
    ;
    ; data contains erroneous text after data has started
    ;
    close,unit & free_lun,unit & return,-1
  endif
endwhile
if (count eq 0) then begin
  ;
  ; no valid data was read
  ;
  close,unit & free_lun,unit & return,-1
endif
values = vals(*,0:count-1)
;
; if an io error occured, clean up and return -1
;
trouble: if (!error ne 0) then num_cols = -1
if (n_elements(unit) ne 0) then free_lun,unit
return,num_cols
end

PRO read_LUT_EnMAP_formatted, file_LUT, vza_arr, sza_arr, phi_arr, hsf_arr, aot_arr, cwv_arr, wvl

common lut_inp, lut1, lut2, num_par, num_bd, xnodes, nm_nodes, ndim, lim, lut_cell, x_cell

ndim = 6 ; vza, sza, hsf, aot, phi, cwv

read_var = 1
openr, 1, file_LUT
readu, 1, read_var
num_bd = read_var
wvl = fltarr(num_bd)
readu, 1, wvl

readu, 1, read_var
dim_vza = read_var
vza_arr = fltarr(dim_vza)
readu, 1, vza_arr

readu, 1, read_var
dim_sza = read_var
sza_arr = fltarr(dim_sza)
readu, 1, sza_arr

readu, 1, read_var
dim_hsf = read_var
hsf_arr = fltarr(dim_hsf)
readu, 1, hsf_arr

readu, 1, read_var
dim_aot = read_var
aot_arr = fltarr(dim_aot)
readu, 1, aot_arr

readu, 1, read_var
dim_phi = read_var
phi_arr = fltarr(dim_phi)
readu, 1, phi_arr

readu, 1, read_var
dim_cwv = read_var
cwv_arr = fltarr(dim_cwv)
readu, 1, cwv_arr

readu, 1, read_var
npar1 = read_var

readu, 1, read_var
npar2 = read_var

lut1 = fltarr(npar1, num_bd, 1, dim_phi, dim_aot, dim_hsf, dim_sza, dim_vza)
lut2 = fltarr(npar2, num_bd, dim_cwv, 1, dim_aot, dim_hsf, dim_sza, dim_vza)

readu, 1, lut1
readu, 1, lut2
close, 1

dim_arr1 = [dim_vza, dim_sza, dim_phi, dim_hsf, dim_aot, 1]
dim_arr2 = [dim_vza, dim_sza, 1, dim_hsf, dim_aot, dim_cwv]

dim_max = max(dim_arr1)
xnodes1 = fltarr(ndim, dim_max)
xnodes1[0, 0:dim_arr1[0]-1] = vza_arr
xnodes1[1, 0:dim_arr1[1]-1] = sza_arr
xnodes1[2, 0:dim_arr1[2]-1] = phi_arr
xnodes1[3, 0:dim_arr1[3]-1] = hsf_arr
xnodes1[4, 0:dim_arr1[4]-1] = aot_arr
xnodes1[5, 0:dim_arr1[5]-1] = 1

dim_max = max(dim_arr2)
xnodes2 = fltarr(ndim, dim_max)
xnodes2[0, 0:dim_arr2[0]-1] = vza_arr
xnodes2[1, 0:dim_arr2[1]-1] = sza_arr
xnodes2[2, 0:dim_arr2[2]-1] = 1
xnodes2[3, 0:dim_arr2[3]-1] = hsf_arr
xnodes2[4, 0:dim_arr2[4]-1] = aot_arr
xnodes2[5, 0:dim_arr2[5]-1] = cwv_arr

num_par = npar1 + npar2 ; lpw, edir, edif, sab, rat

dim_arr = fltarr(ndim)
dim_arr[0:3] = dim_arr1[[0,1,3,4]]
dim_arr[4] = dim_arr1[2]
dim_arr[5] = dim_arr2[5]
max_dim = max(dim_arr)
xnodes = fltarr(ndim, max(dim_arr1))
xnodes[0:3, *] = xnodes1[[0,1,3,4], *]
xnodes[4, *] = xnodes1[2, *]
xnodes[5, *] = xnodes2[5, *]
lim = fltarr(2, ndim)
nm_nodes = 2^ndim
lut_cell = fltarr(num_par, nm_nodes, num_bd)

x_cell = fltarr(ndim, nm_nodes)
cont = 0
for i = 0, 1 do $
  for j = 0, 1 do $
    for k = 0, 1 do $
      for ii = 0, 1 do $
        for jj = 0, 1 do $
          for kk = 0, 1  do begin

            x_cell[0, cont] = i
            x_cell[1, cont] = j
            x_cell[2, cont] = k
            x_cell[3, cont] = ii
            x_cell[4, cont] = jj
            x_cell[5, cont] = kk

            cont = cont + 1
          endfor


elip = 0.0001
vza_arr[0] = vza_arr[0] + elip & vza_arr[dim_arr[0] - 1] = vza_arr[dim_arr[0] - 1] - elip
sza_arr[0] = sza_arr[0] + elip & sza_arr[dim_arr[1] - 1] = sza_arr[dim_arr[1] - 1] - elip
hsf_arr[0] = hsf_arr[0] + elip & hsf_arr[dim_arr[2] - 1] = hsf_arr[dim_arr[2] - 1] - elip
aot_arr[0] = aot_arr[0] + elip & aot_arr[dim_arr[3] - 1] = aot_arr[dim_arr[3] - 1] - elip
phi_arr[0] = phi_arr[0] + elip & phi_arr[dim_arr[4] - 1] = phi_arr[dim_arr[4] - 1] - elip
cwv_arr[0] = cwv_arr[0] + elip & cwv_arr[dim_arr[5] - 1] = cwv_arr[dim_arr[5] - 1] - elip

;stop

END




FUNCTION generate_filter, wvl_M, wvl, wl_resol

num_wvl_M = n_elements(wvl_M)
num_wvl = n_elements(wvl)

s_norm_M = fltarr(num_wvl_M, num_wvl)
exp_max = 2.
exp_min = 2.
exp_arr = exp_max + (exp_min - exp_max) * findgen(num_wvl) / (num_wvl-1)
c_arr = (1./(2.^exp_arr*alog(2.)))^(1./exp_arr)
for bd = 0, num_wvl - 1 do begin
  li1 = where (wvl_M ge (wvl[bd] - 2.* wl_resol[bd]) and wvl_M le (wvl[bd] + 2.* wl_resol[bd]), cnt)
  if (cnt gt 0) then begin
    tmp =abs(wvl[bd] - wvl_M[li1])/(wl_resol[bd] *c_arr[bd])
    s = exp(-(tmp^exp_arr[bd]))
    s_norm_M[li1, bd] = s / total(s)
  endif
endfor

return, s_norm_M

END


PRO read_envi_header, file_hdr, ncols, nrows, wl_center, wl_fwhm,data_type

if file_test(file_hdr + '.bsq.hdr') then file_hdr = file_hdr + '.bsq.hdr' else file_hdr = file_hdr + '.hdr'

a = ''
openr, 1, file_hdr
while ~eof(1) do begin
  readf, 1, a
  str = strsplit(a, ' = ', /extract)
  if str[0] eq 'samples' then ncols = long(str[1])
  if str[0] eq 'lines'   then nrows = long(str[1])
  if n_elements(str) gt 1 then if strjoin(str[0:1]) eq 'datatype'   then data_type = long(str[2])
  
  if str[0] eq 'bands'   then begin
    num_bd = long(str[1])
    wl_center = fltarr(num_bd)
    wl_fwhm   = fltarr(num_bd)
  endif
  if str[0] eq 'wavelength' then if str[1] ne 'units' then begin
    b= ''
    while str[0] ne 'fwhm' do begin
      readf, 1, a
      b = b + a
      str = strsplit(a, ' = ', /extract)
    endwhile
    str = strsplit(b, ', ', /extract)
    wl_center[0:num_bd -2] = float(str[0:num_bd -2])
    str = strsplit(str[num_bd -1], '}', /extract)
    wl_center[num_bd -1]   = float(str[0])
    b= ''
    while ~eof(1) do begin
      readf, 1, a
      b = b + a
      str = strsplit(a, ' = ', /extract)
    endwhile
    str = strsplit(b, ', ', /extract)
    wl_fwhm[0:num_bd -2] = float(str[0:num_bd -2])
    str = strsplit(str[num_bd -1], '}', /extract)
    wl_fwhm[num_bd -1] = float(str[0])
  endif
endwhile

close, 1

if wl_center[0] lt 1 then begin
  wl_center = wl_center * 0.001
  wl_fwhm   = wl_fwhm * 0.001
endif

if ~keyword_set(ncols) or ~keyword_set(nrows) or ~keyword_set(wl_center) or ~keyword_set(wl_fwhm) then begin
  print, 'Error, wrong Header'
  stop
endif

END

PRO read_log_file, log_file, vza_inp, vaa_inp, gmt, jday, month, lat, lon, dem_name, ady_flg

a=''
openr, 1, log_file
for i =1,8 do readf, 1, a
readf, 1, a
str = strsplit(a, ' = ', /extract)
vza_inp = float(str[1])
readf, 1, a
str = strsplit(a, ' = ', /extract)
vaa_inp = float(str[1])
readf, 1, a
str = strsplit(a, ' = ', /extract)
gmt = float(str[1])
readf, 1, a
str = strsplit(a, ' = ', /extract)
jday = float(str[1])
readf, 1, a
str = strsplit(a, ' = ', /extract)
month = float(str[1])
readf, 1, a
str = strsplit(a, ' = ', /extract)
lat = float(str[1])
readf, 1, a
str = strsplit(a, ' = ', /extract)
lon = float(str[1])
readf, 1, a
readf, 1, a
readf, 1, a
str = strsplit(a, ' = ', /extract)
dem_name = str[2]
readf, 1, a
str = strsplit(a, ' = ', /extract)
dem_2 = str[1]
readf, 1, a
str = strsplit(a, ' = ', /extract)
ady_flg = float(str[2])

close, 1

END

PRO write_header_LG, file_hdr, ncols, nrows, wl_center, wl_fwhm, desc_str, data_type,bbl=bbl

num_bd = n_elements(wl_center)
if num_bd gt 1 then begin
  wl_center_str = strtrim(wl_center[0:num_bd-2], 2) + ','
  wl_center_str = [wl_center_str, strtrim(wl_center[num_bd-1], 2)+'}']
  wl_fwhm_str = strtrim(wl_fwhm[0:num_bd-2], 2) + ','
  wl_fwhm_str = [wl_fwhm_str, strtrim(wl_fwhm[num_bd-1], 2)+'}']
endif else begin
  wl_center_str = '}'
  wl_fwhm_str = '}'
endelse

openw, 1, file_hdr
printf, 1, 'ENVI'
printf, 1, 'description = {'
printf, 1, desc_str + '}'
printf, 1, 'samples = ' + strtrim(ncols, 2)
printf, 1, 'lines   = ' + strtrim(nrows, 2)
printf, 1, 'bands   = ' + strtrim(num_bd, 2)
printf, 1, 'header offset = 0'
printf, 1, 'file type = ENVI Standard'
printf, 1, 'data type = ' + strtrim(data_type, 2)
printf, 1, 'interleave = bsq'
printf, 1, 'sensor type = Unknown'
printf, 1, 'byte order = 0'
printf, 1, 'wavelength units = nanometers'
printf, 1, 'wavelength = {'
printf, 1, wl_center_str
printf, 1, 'fwhm = {'
printf, 1, wl_fwhm_str
if n_elements(bbl) gt 1 then begin
  bbl_str = strtrim(bbl[0:num_bd-2], 2) + ','
  bbl_str = [bbl_str, strtrim(bbl[num_bd-1], 2)+'}']
  printf, 1, 'bbl = {'
  printf,1,bbl_str
endif
close, 1

END


PRO display_RGB, toa_img, wl_center, ncols, nrows

wvl_red   = 650.
wvl_green = 550.
wvl_blue  = 450.
dif = abs(wl_center - wvl_red)
wh = where(dif eq min(dif)) & ind_red = wh[0]
dif = abs(wl_center - wvl_green)
wh = where(dif eq min(dif)) & ind_green = wh[0]
dif = abs(wl_center - wvl_blue)
wh = where(dif eq min(dif)) & ind_blue = wh[0]

window, /free, title = 'RGB composite', xsize = ncols, ysize = nrows
img_plt = fltarr(ncols, nrows, 3)
img_plt[*, *, 0] = toa_img[*, *, ind_red]
img_plt[*, *, 1] = toa_img[*, *, ind_green]
img_plt[*, *, 2] = toa_img[*, *, ind_blue]

tvscl, img_plt, true = 3, /order

END


PRO AOT_retr, toa_img, ncols, nrows, wl_center, s_norm_ini, vza, sza, phi, cld_img, msk_img, dem_img, aot_gr, cld_aot_thre, aot550,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands

common lut_inp, lut1, lut2, num_par, num_wvl_LUT, xnodes, nm_nodes, ndim, lim, lut_cell, x_cell

num_bd = n_elements(wl_center)

mus = cos(sza*!dtor)

wv_ref = 2.

num_min_pix = 100

vis_lim = ~n_elements(aot_bands)? [420., 690.]  : aot_bands
wh_vis = where(wl_center ge vis_lim[0] and wl_center le vis_lim[1], num_bd_vis)

hsurf = mean(dem_img)

min_arr = fltarr(num_min_pix, num_bd_vis)
dum = fltarr(ncols, nrows)
for i = 0, num_bd_vis - 1 do begin
  dum = toa_img[*, *, wh_vis[i]] * msk_img
  wh_no0 = where(dum ne 0.)
  dum_tmp = dum[wh_no0]
  ind_min=sort(dum_tmp)
  min_arr[*, i] = dum_tmp[ind_min[0:num_min_pix-1]]
endfor

dim_aot = n_elements(aot_gr)
cnt_neg = 0
j=0
while cnt_neg lt num_min_pix and j lt dim_aot do begin
  f_int = interpol_lut(vza, sza, phi, hsurf, aot_gr[j], wv_ref)
  a = f_int # s_norm_ini
  lpw_vza = a[0, *] * 10000.
  cnt_neg = 0
  for k = 0, num_min_pix-1 do begin
    wh_neg = where(min_arr[k, *]-lpw_vza[wh_vis] le 0., cnt_cont)
    if cnt_cont gt 0 then cnt_neg = cnt_neg + 1
  endfor
  j = j+1
endwhile
j_max = j - 2 > 0

if j_max ne 0 then begin
  j=1
  stp = 0.005
  cnt_neg=0
  while cnt_neg lt num_min_pix do begin
    aot_lp = aot_gr[j_max] + j*stp
    f_int = interpol_lut(vza, sza, phi, hsurf, aot_lp, wv_ref)
    a = f_int # s_norm_ini
    lpw_vza = a[0, *] * 10000.
    cnt_neg = 0
    for k = 0, num_min_pix-1 do begin
      wh_neg = where(min_arr[k, *]-lpw_vza[wh_vis] le 0., cnt_cont)
      if cnt_cont gt 0 then cnt_neg = cnt_neg + 1
    endfor
    j = j+1
  endwhile
  aot_max = aot_lp - stp
endif else begin
  aot550 = aot_gr[0]
  return
endelse

mus_img = replicate(mus, ncols, nrows)

tot_pix = long(ncols)*nrows
wh_land = where(cld_img le cld_aot_thre and msk_img eq 1, cnt_land)
toa_sub = fltarr(cnt_land, num_bd)
for i = 0, num_bd - 1 do toa_sub[*, i] = toa_img[wh_land + i*tot_pix]

dem_sub = dem_img[wh_land]
mus_sub = mus_img[wh_land]

dem_mean = mean(dem_sub)
mus_mean = mean(mus_sub)

n_pts_gr = 10
aot_min = aot_gr[0]
aot_gr_inv = aot_min + (aot_max - aot_min) * findgen(n_pts_gr) / (n_pts_gr - 1)
lpw_aot = fltarr(num_bd, n_pts_gr)
egl_aot = fltarr(num_bd, n_pts_gr)
sab_aot = fltarr(num_bd, n_pts_gr)
for i = 0, n_pts_gr - 1 do begin
  f_int = interpol_lut(vza, sza, phi, dem_mean, aot_gr_inv[i], wv_ref)
  a = f_int # s_norm_ini
  lpw_aot[*, i] = a[0, *] * 1.e+4
  egl_aot[*, i] = (a[1, *] * mus_mean + a[2, *]) * 1.e+4
  sab_aot[*, i] = a[3, *]
endfor

EnMAP_AOT_inv_land, toa_sub, lpw_aot, egl_aot, sab_aot, dn2rad, wl_center, wl_fwhm, aot_gr_inv, dem_sub, mus_sub, aot550, valid_flg,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands
if valid_flg eq 0 then aot550 = aot_max
print, '  --> Calculated AOT550 = ' + strtrim(aot550, 2)


END


PRO EnMAP_AC, toa_img, ncols, nrows, wl_center, wl_fwhm, s_norm_ini, vza_inp, sza_inp, phi_inp, hsf_inp, dem_img, cld_img, msk_img, cwv_gr, aot550, $
doy, wvl_LUT, SpecPol_flg, ady_flg, cld_aot_thre, cld_rfl_thre, wv_img, refl_img, dwl_arr_sm, cal_coef, path_dat, str_name,verbose=verbose,$
aux_flg=aux_flg,bbl_flg=bbl_flg,fac_dst=fac_dst,smooth_flg=smooth_flg,smooth_dst=smooth_dst,$
smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands
;stop
wv_max = 2.
wv_min = 0.5
arg_arr = findgen(365) * !pi / 364
wv_doy = (wv_max - wv_min) * sin(arg_arr) + wv_min
wv_val = wv_doy[doy]

print, ''
print, 'Performing spectral characterization...'

smile_processor, toa_img, ncols, nrows, wl_center, wl_fwhm, s_norm_ini, vza_inp, sza_inp, $
phi_inp, hsf_inp, cld_img, cld_aot_thre, wvl_LUT, aot550, msk_img, dwl_arr_sm,verbose=verbose,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands

num_bd = n_elements(wl_center)
dim_cwv = n_elements(cwv_gr)

tot_pix = long(ncols) * nrows
;
num_wvl_LUT = n_elements(wvl_LUT)

wv_img =fltarr(ncols, nrows)
refl_img = intarr(ncols, nrows, num_bd)

mat_tmp = fltarr(ncols, nrows)
for i = 0, ncols - 1 do mat_tmp[i, *] = i

n_dem = 2
lpw_hs_wv_col = fltarr(ncols, n_dem, dim_cwv, num_bd)
edr_hs_wv_col = fltarr(ncols, n_dem, dim_cwv, num_bd)
edf_hs_wv_col = fltarr(ncols, n_dem, dim_cwv, num_bd)
sab_hs_wv_col = fltarr(ncols, n_dem, dim_cwv, num_bd)

;**********************************************************************************
; Atmospheric correction

print, ''
print, 'Performing WV & Reflectance Retrieval'

wh_land = where(cld_img le cld_rfl_thre and msk_img eq 1, cnt_land)
toa_sub = fltarr(cnt_land, num_bd)
for i = 0, num_bd - 1 do toa_sub[*, i] = toa_img[wh_land + tot_pix * i]
toa_img = 0

mat_arr = mat_tmp[wh_land]

ndim = 5

dem_sub = dem_img[wh_land]
mus_sub = replicate(cos(sza_inp *!dtor), cnt_land)
dem_lim = [min(dem_sub), max(dem_sub)]


a = fltarr(ndim, n_dem, dim_cwv, num_wvl_LUT)
for j = 0, n_dem - 1 do for i = 0, dim_cwv - 1 do a[*, j, i, *]=interpol_lut(vza_inp, sza_inp, phi_inp, dem_lim[j], aot550, cwv_gr[i])

for ind = 0, ncols - 1 do begin
  s_norm_sh = generate_filter(wvl_LUT, wl_center + dwl_arr_sm[ind], wl_fwhm)
  for j = 0, n_dem - 1 do begin
    for i = 0, dim_cwv - 1 do begin
      a_flt = reform(a[*, j, i, *]) # s_norm_sh
      lpw_hs_wv_col[ind, j, i, *] = a_flt[0, *]*1.e+4
      edr_hs_wv_col[ind, j, i, *] = a_flt[1, *]*1.e+4
      edf_hs_wv_col[ind, j, i, *] = a_flt[2, *]*1.e+4
      sab_hs_wv_col[ind, j, i, *] = a_flt[3, *]
    endfor
  endfor
endfor

EnMAP_retrieve_wv, toa_sub, dem_sub, dem_lim, mus_sub, lpw_hs_wv_col, edr_hs_wv_col, edf_hs_wv_col, $
sab_hs_wv_col, wl_center, cwv_gr, wv_val, mat_arr, wv_sub, refl_sub,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands

toa_sub = 0

refl_img = uintarr(ncols, nrows, num_bd)
for i = 0, num_bd - 1 do refl_img[wh_land + i * tot_pix] = refl_sub[*, i]
refl_sub = 0

wh_high = where(refl_img gt 1.e+4, cnt_high)
if cnt_high gt 0 then refl_img[wh_high] = 0

if ady_flg eq 1 then begin
  print,'Performing adjacency correction...'
  px_size= 30.
  ;fac_dst = 1000. / px_size
  a=interpol_lut(vza_inp, sza_inp, phi_inp, hsf_inp, aot550, mean(wv_sub))
  rat_mean = reform(a[4, *]#s_norm_ini)
  wh = where(rat_mean lt 0 or rat_mean gt 1, cnt)
  if cnt gt 0 then rat_mean[wh] = 0
  for bd = 0, num_bd - 1 do begin
     refl_tmp = refl_img[*, *, bd] * 1.e-4
     refl_tmp = (refl_tmp + rat_mean[bd] * (refl_tmp - smooth(refl_tmp, fac_dst, /edge))) > 0.
     ;refl_img[*, *, bd] = uint(refl_tmp*1.e+4)
     refl_img[*, *, bd] = uint(round(refl_tmp*1.e+4))
  endfor
endif

num_pix_plt = 5
indx = fix(randomu(1L, num_pix_plt) * ncols)
indy = fix(randomu(1L, num_pix_plt) * nrows)
a_plt = fltarr(num_pix_plt, num_bd)
for i = 0, num_pix_plt - 1 do a_plt[i, *] = reform(refl_img[indx[i], indy[i], *])
col_arr = 60 + indgen(num_pix_plt) * (254.-60) / (num_pix_plt - 1)
if verbose then begin
  window, /free, title = 'Sample reflectance spectra', xsize=700, ysize=600
  plot, wl_center, a_plt[0, *], /nodata, background=255, color=0, xtitle = 'Wavelength (nm)', ytitle = 'Reflectance', xrange = [400, 1050], yrange = [0., 6000.], /xs, charsize=2
  for i = 0, num_pix_plt - 1 do oplot, wl_center, a_plt[i, *], psym=-8, color = col_arr[i]
endif

bbl = bytarr(n_elements(wl_center))+1
bbl[129:135]  = 0
bbl[169:185]  = 0
image_name = path_dat + str_name + '_REFL.img'
if smooth_flg then begin
  print,'Smoothing spectra with boxcar...'
  refl_img=uint(round(smooth(refl_img,[0,0,smooth_dst],/edge_trunc,missing=0,/nan)))
;  mask  = float(rebin(reform(bbl,1,1,n_elements(wl_center)),ncols,nrows,n_elements(wl_center)))
;  ref = refl_img
;  ref = smooth(ref*mask,[0,0,smooth_dst],missing=0,/nan,/edge_trunc)
;  ref+= float(~temporary(mask))*refl_img
;  refl_img  = temporary(ref)
  ;refl_img=smooth(refl_img*mask,[0,0,smooth_dst],/edge_trunc,missing=0,/nan)+refl_img*float(~mask)
endif

openw, 1, image_name
writeu, 1, refl_img;
close, 1
desc_str = 'Surface reflectance (0-10000)'
file_hdr = filext(image_name, /path, /name) + '.hdr'


if bbl_flg then $
write_header_LG, file_hdr, ncols, nrows, wl_center, wl_fwhm, desc_str, 12,bbl=bbl else $
write_header_LG, file_hdr, ncols, nrows, wl_center, wl_fwhm, desc_str, 12



;;;;;;;;;;;;;;;;;new!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


if  n_elements(wv_sub) eq n_elements(wv_img) then wv_img=reform(wv_sub,(size(refl_img,/dimensions))[0:1]) else $
wv_img[((ww=where(cld_img eq 0)))]=wv_sub
if aux_flg then begin
  image_name = path_dat + str_name + '_WV.img'
  openw, 1, image_name
  writeu, 1, wv_img
  close, 1
  desc_str = 'Water vapor (gcm-2)'
  file_hdr = filext(image_name, /path, /name) + '.hdr'
  write_header_LG, file_hdr, ncols, nrows, [0], [0], desc_str, 4
endif
END


PRO EnMAP_retrieve_wv, toa_sub, dem_sub, dem_lim, mus_sub, lpw_hs_wv_col, edr_hs_wv_col, edf_hs_wv_col, $
sab_hs_wv_col, wl_center, wv_gr, wv_val, mat_arr, wv_arr, refl_arr,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands
common chi_sq_wv_refl, lpw_wvc2, egl_wvc2, sab_wvc2, toa_wv, refl_pix, wv_gr2, dim_wv, wv_p, wv_inf

cnt_land = n_elements(toa_sub[*, 0])
ncols = n_elements(lpw_hs_wv_col[*, 0, 0, 0])


wv_gr2 = alog(wv_gr)
dim_wv = n_elements(wv_gr)

num_bd = n_elements(wl_center)
if ~n_elements(wv_bands) then begin
wvl_wv = [861., 921.]
wvl_out = 892.
endif else begin
wvl_wv=wv_bands[[0,2]]
wvl_out=wv_bands[1]
endelse

d_wvl = wl_center[0:num_bd-2] - wl_center[1:num_bd-1]
wh_t= where(d_wvl gt 0, cnt_wh_t)
if cnt_wh_t gt 0 then ind_spec_1 = indgen(wh_t[0]) else ind_spec_1 = indgen(num_bd)

wh_wv = where(wl_center[ind_spec_1] ge wvl_wv[0] and wl_center[ind_spec_1] le wvl_wv[1], complement = wh_no_wv, cnt_wv)
lim_1 = wh_wv[0]
lim_2 = wh_wv[cnt_wv - 1]
wh_out = where(wl_center[ind_spec_1] le wvl_out, cnt_out)
lim_out = wh_out[cnt_out - 1]

wv_lim = [alog(wv_gr[0] + 0.001), alog(wv_gr[dim_wv - 1] - 0.001)]

if ~keyword_set(wv_val) then wv_val = 1.8
wv_retr = wv_val
val = abs(wv_gr - wv_retr)
wh = where(val eq min(val)) & ind_retr_wv = wh[0]

ind_retr_dem = 0
mus_inp = mean(mus_sub)

if dem_lim[1] ne dem_lim[0] then dem_fac = (dem_sub - dem_lim[0]) / (dem_lim[1] - dem_lim[0]) else dem_fac = replicate(0., cnt_land)

wv_arr = fltarr(cnt_land)
refl_arr = uintarr(cnt_land, num_bd)

for ind_col = 0, ncols - 1 do begin

  lpw_hs_wv = reform(lpw_hs_wv_col[ind_col, *, *, *])
  edr_hs_wv = reform(edr_hs_wv_col[ind_col, *, *, *])
  edf_hs_wv = reform(edf_hs_wv_col[ind_col, *, *, *])
  sab_hs_wv = reform(sab_hs_wv_col[ind_col, *, *, *])

  lpw_wv = reform(lpw_hs_wv[ind_retr_dem, ind_retr_wv, wh_wv])
  egl_wv = reform(edr_hs_wv[ind_retr_dem, ind_retr_wv, wh_wv])* mus_inp + reform(edf_hs_wv[ind_retr_dem, ind_retr_wv, wh_wv])
  sab_wv = reform(sab_hs_wv[ind_retr_dem, ind_retr_wv, wh_wv])

  wh_col = where(mat_arr eq ind_col, cnt_land_col)

  if cnt_land_col gt 0 then begin

    tmp = toa_sub[wh_col, *]
    rad_wv = tmp[*, wh_wv]
    refl_wv = fltarr(cnt_land_col, cnt_wv)
    lim_sp = lim_out - lim_1
    for j = 0, lim_sp do begin
      xterm = !pi * (rad_wv[*, j] - lpw_wv[j]) / (egl_wv[j])
      refl_wv[*, j] = xterm / (1. + sab_wv[j] * xterm)
    endfor

    wv_arr_col = fltarr(cnt_land_col)
    refl_arr_col = fltarr(cnt_land_col, num_bd)

    for ind = 0L, cnt_land_col - 1 do begin

      coef = linfit(wl_center[wh_wv[0:lim_sp]], refl_wv[ind, 0:lim_sp])
      refl_wv[ind, (lim_sp + 1):(cnt_wv - 1)] = coef[0] + coef[1] * wl_center[wh_wv[(lim_sp + 1):(cnt_wv - 1)]]

      dem_fac_pix = dem_fac[wh_col[ind]]

      lpw_wvc = reform(lpw_hs_wv[0, *, *] + dem_fac_pix * (lpw_hs_wv[1, *, *] - lpw_hs_wv[0, *, *]))
      edr_wvc = edr_hs_wv[0, *, *] + dem_fac_pix * (edr_hs_wv[1, *, *] - edr_hs_wv[0, *, *])
      edf_wvc = edf_hs_wv[0, *, *] + dem_fac_pix * (edf_hs_wv[1, *, *] - edf_hs_wv[0, *, *])
      sab_wvc = reform(sab_hs_wv[0, *, *] + dem_fac_pix * (sab_hs_wv[1, *, *] - sab_hs_wv[0, *, *]))
      egl_wvc = reform(edr_wvc * mus_sub[wh_col[ind]] + edf_wvc)
      lpw_wvc2 = lpw_wvc[*, wh_wv]
      egl_wvc2 = egl_wvc[*, wh_wv]
      sab_wvc2 = sab_wvc[*, wh_wv]

      toa_wv  = reform(rad_wv[ind, *])
      refl_pix = reform(refl_wv[ind, *])

      wv_arr_col[ind] = zbrent(wv_lim[0], wv_lim[1], FUNC_NAME = 'chisq_AHS_WV_refl', MaX_Iter = 10000, Tolerance = 1.e-4)

      lpw_int = (lpw_wvc[wv_inf +  1, *] - lpw_wvc[wv_inf, *]) * wv_p + lpw_wvc[wv_inf, *]
      egl_int = (egl_wvc[wv_inf +  1, *] - egl_wvc[wv_inf, *]) * wv_p + egl_wvc[wv_inf, *]
      sab_int = (sab_wvc[wv_inf +  1, *] - sab_wvc[wv_inf, *]) * wv_p + sab_wvc[wv_inf, *]

      xterm = !pi * (toa_sub[wh_col[ind], *] - lpw_int) / (egl_int)
      refl_arr_col[ind, *] = xterm / (1. + sab_int * xterm)

    endfor

    wv_arr[wh_col] = wv_arr_col
;    refl_arr[wh_col, *] = uint(refl_arr_col*1.e+4 > 0)
    refl_arr[wh_col, *] = uint(round(refl_arr_col*1.e+4 > 0))

  endif

endfor

wv_arr = exp(wv_arr)

END


PRO EnMAP_AtmCor_LUTv003,path_dat=path_dat,img_rad=img_rad,dem_name=dem_name,verbose=verbose,$;v001
ady_flg=ady_flg,vza_inp=vza_inp,vaa_inp=vaa_inp,gmt=gmt,jday=jday,month=month,$
lat=lat,lon=lon,mdem=mdem,img_qa_mask=img_qa_mask,img_cl_mask=img_cl_mask,aux_flg=aux_flg,$
aot550_val=aot550_val,cld_aot_thre=cld_aot_thre,cld_rfl_thre=cld_rfl_thre,intercalary=intercalary,$
bbl_flg=bbl_flg,fac_dst=fac_dst,smooth_flg=smooth_flg,smooth_dst=smooth_dst,img_wv_mask=img_wv_mask,$
smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands


common lut_inp, lut1, lut2, num_par, num_wvl_LUT, xnodes, nm_nodes, ndim, lim, lut_cell, x_cell
!except=0
errors=check_math(mask=144)
t0  = systime(1)

;forward_function zbrent
;forward_function interpol_lut
;forward_function filext
;forward_function generate_filter
;forward_function ENMAP_AC
;forward_function ENMAP_RETRIEVE_WV
;forward_function CHISQ_AHS_WV_REFL
;forward_function SMILE_PROCESSOR
;forward_function AOT_RETR
;forward_function ENMAP_AOT_INV_LAND
;forward_function MINIM_TOA
;forward_function CHISQ_CASI_SHIFT
;forward_function SURF_REFL_RETRIEVER
;forward_function MINF_PARABOLIC
;forward_function VARSOL
;forward_function ZENSUN
;forward_function GFUNCT
;forward_function LOAD_COLS
;forward_function READ_LUT_ENMAP_FORMATTED
;forward_function READ_ENVI_HEADER
;forward_function READ_LOG_FILE 
;forward_function WRITE_HEADER_LG 
;forward_function DISPLAY_RGB
 
  

;stop

;v0002
if $
~n_elements(path_dat) or $
~n_elements(img_rad) $;or $
;~n_elements(dem_name) $
then begin
  message,'No appropiate input given - exiting...',/ioerror
  return
endif

verbose=~n_elements(verbose)? 0 : verbose
aux_flg=~n_elements(aux_flg)? 0 : aux_flg




; in case any file is open upon error
close, /all

; Plotting stuff
while !d.window ne -1 do wdelete, !d.window
device, decomposed = 0
loadct, 39
X = [-1, 0, 1, 0, -1]
Y = [0, 1, 0, -1, 0]
USERSYM, X, Y, /fill

;if !version.os_family eq 'Windows' then slash = '\' else slash = '/'
slash = path_sep()
cd, current   = path_act


  Help, Calls=callStack
  path_act  = strmid(((str=strmid(((c=callstack[0])),((start=strpos(c,'<')+1)),$
          strpos(c,'(')-start))),0,strpos(str,FILE_basename (strmid(c,((start=strpos(c,'<')+1)),strpos(c,'(')-start))))


path_act  = path_act + slash

;********************************************************
;********************************************************
; INPUTS

;img_case = 3; to choose the image to process --> to be read with GUI

;aot550_val = 0; =0, the code calculates AOT automatically, =x, aot set to x

;stop

;cld_aot_thre = 0.02 ; probability threshold for AOT retrieval
;cld_rfl_thre = 0.05 ; probability threshold for REFL (& CWV) retrieval

;path_dat = '/home/luis/WORK/DATA/data_EnMAP/AC_TBOX' + slash; path to the input data
path_dat  += slash; path to the input data; v001

; MODTRAN LUT
file_LUT  = path_act + 'AC_data' + slash + 'MOMO_EnMAP_LUT_formatted_1nm'
;stop
;case img_case of
;1: begin
;  img_rad  = path_dat + 'L1CO_makhtesh6_M.bsq'
;  img_qa_mask  = ''  ; quality_mask
;  img_cl_mask  = ''  ; cloud_mask
;end
;2: begin
;  img_rad  = path_dat + 'L1CO_munich_Rot0.bsq'
;  img_qa_mask  = ''  ; quality_mask
;  img_cl_mask  = ''  ; cloud_mask
;end
;3: begin
;  img_rad  = path_dat + 'OutSpatImD1_munich1_RAD.bsq'
;;  img_rad   = path_dat + 'OutSpatImD1_munich1_RAD_subset.bsq'
;  img_qa_mask  = ''  ; quality_mask
;  img_cl_mask  = ''  ; cloud_mask
;end
;endcase
;********************************************************
;********************************************************

print, '*** EnMAP Interactive Atmospheric Correction Tool ***'
print, ''
print, ' - Processing ' + filext(img_rad, /name)

; reads header and .log file
read_envi_header, filext(img_rad, /path, /name), ncols, nrows, wl_center, wl_fwhm,data_type
;read_log_file, filext(img_rad, /path, /name) + '_log.txt', vza_inp, vaa_inp, gmt, jday, month, lat, lon, dem_name, ady_flg

; DEM mandatory
if ~n_elements(dem_name) then img_dem = dem_name else begin
  dem_img = fltarr(ncols,nrows)+(mdem eq 0? 0:mdem)*1000.
endelse

; calculates correction factor for mean Earth-Sun distance
daysxmonth = intercalary? [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
CALDAT, jday, Month, Day, Year, Hour, Minute, Second

doy = total(daysxmonth[0:month-1]) - daysxmonth[month - 1] + day
;stop
varsol, jday, month, dsol
dn2rad = dsol * dsol

; reads and displays TOA image
num_bd = n_elements(wl_center)



toa_img = make_array(ncols, nrows, num_bd,type=data_type)
openr, 1, img_rad,/stdio
readu, 1, toa_img
close, 1
toa_img*=1.

;toa_img = toa_img * dn2rad
toa_img *= dn2rad
if verbose then display_RGB, toa_img, wl_center, ncols, nrows

; reads and displays DEM
if ~n_elements(dem_img) then begin
  dem_img = uintarr(ncols, nrows)
  openr, 1, img_dem
  readu, 1, dem_img
  close, 1
  if verbose then begin
    window, /free, title='DEM', xsize= ncols, ysize = nrows
    loadct, 0
    tvscl, dem_img, /order
    loadct, 39
  endif
endif

; reads and displays wv image
;if n_elements(im_wv_mask) then begin
;  wv_img = uintarr(ncols, nrows)
;  openr, 1, im_wv_mask
;  readu, 1, wv_img
;  close, 1
;  if verbose then begin
;    window, /free, title='WV', xsize= ncols, ysize = nrows
;    loadct, 0
;    tvscl, WV_img, /order
;    loadct, 39
;  endif
;endif

;dem_img = dem_img * 0.001
dem_img *= 0.001
hsf_inp = mean(dem_img)

; calculates SZA & SAA
zensun, doy, gmt, lat, lon, sza_inp, saa, solfac, sunrise, sunset, local=local, latsun=latsun, lonsun=lonsun
if saa lt 0. then saa = 360. + saa
phi_inp = abs(vaa_inp - saa)
if phi_inp gt 180. then phi_inp = 360. - phi_inp

; reads cloud mask if available
if img_cl_mask ne '' then begin
  ;cld_img = fltarr(ncols, nrows)
  ; must be byte
  cld_img = reform(read_binary(img_cl_mask,type=1),ncols,nrows)
;  openr, 1, img_cl_mask,/stdio
;  readu, 1, cld_img
;  close, 1
if verbose then begin
  window, /free, title='Cloud mask', xsize= ncols, ysize = nrows
  loadct, 0
  tvscl, cld_img, /order
  loadct, 39
endif
endif else cld_img = fltarr(ncols, nrows)

; reads quality mask if available
if img_qa_mask ne '' then begin
  msk_img = fltarr(ncols, nrows)
  openr, 1, img_qa_mask
  readu, 1, msk_img
  close, 1
  if verbose then begin
    window, /free, title='Quality mask', xsize= ncols, ysize = nrows
    loadct, 0
    tvscl, msk_img, /order
    loadct, 39
  endif
endif else msk_img = replicate(1, ncols, nrows)

;;;;masking is still buggy, CR
;endif else msk_img = total(toa_img,3) eq 0
;mask=total(toa_img,3) eq 0

; reads MODTRAN LUT with atmospheric parameters
read_LUT_EnMAP_formatted, file_LUT, vza_gr, sza_gr, phi_gr, hsf_gr, aot_gr, cwv_gr, wvl_LUT

; generates spectral convolution filter based on nominal spectral calibration values
s_norm_ini = generate_filter(wvl_LUT, wl_center, wl_fwhm)

; estimates AOT if aot550_val ne 0
if aot550_val eq 0 then begin
   print, ''
   print, 'Performing AOT retrieval...'
   AOT_retr, toa_img, ncols, nrows, wl_center, s_norm_ini, vza_inp, sza_inp, phi_inp, $
            cld_img, msk_img, dem_img, aot_gr, cld_aot_thre, aot550,smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands
   print,'AOT550: ',aot550
   print,'Visibility [km]: ',3.912/aot550
endif else begin
   aot550 = aot550_val
   print, ''
   print, 'AOT set by user, AOT550 = ' + strtrim(aot550_val, 2)
endelse




; retrieves surface reflectance, including smile characterization and pixel-based WV retrieval
EnMAP_AC, toa_img, ncols, nrows, wl_center, wl_fwhm, s_norm_ini, vza_inp, sza_inp, phi_inp, hsf_inp, dem_img, cld_img, msk_img, cwv_gr, aot550, $
doy, wvl_LUT, spc_pol_flg, ady_flg, cld_aot_thre, cld_rfl_thre, wv_img, refl_img, dwl_arr_sm, cal_coef, path_dat, filext(img_rad, /name),$
verbose=verbose,aux_flg=aux_flg,fac_dst=fac_dst,bbl_flg=bbl_flg,smooth_flg=smooth_flg,smooth_dst=smooth_dst,$
smile_bands=smile_bands,$
smile_fil=smile_fil,$
aot_bands=aot_bands,$
ndvi_bands=ndvi_bands,$
ndvi_thres=ndvi_thres,$
wv_bands=wv_bands

print, ''
print, 'Done!'
print, 'Time consumption: ',systime(1)-t0

END

