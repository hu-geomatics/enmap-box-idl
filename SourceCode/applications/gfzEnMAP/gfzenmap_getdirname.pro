;+
; :Author: <author name> (<email>)
;-

function gfzEnMAP_getDirname
  
  result = filepath('gfzEnMAP', ROOT=enmapBox_getDirname(/Applications))
  return, result
  
end

;+
; :Hidden:
;-
pro test_gfzEnMAP_getDirname

  print, gfzEnMAP_getDirname()
  print, gfzEnMAP_getDirname(/SourceCode)

end
