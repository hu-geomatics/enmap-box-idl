;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    Use this function to query application settings. The settings file is located under:
;    `filepath('gfzEnMAP.conf', ROOT=gfzEnMAP_getDirname(), SUBDIR='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, gfzEnMAP_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function gfzEnMAP_getSettings
  ;read settings from configuration file
  settingsFilename = filepath('gfzenmap.conf', ROOT=gfzEnMAP_getDirname(), SUBDIR='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()
  
  ;derive additional settings
  settings['title'] = settings['appName']+' '+'v.'+settings['version']
  
  ;TODO: change
  settings['ac_sample_dir'] = filepath('' $
                                , ROOT=enmapBox_getDirname(/SOURCECODE, /Applications) $
                                , SUBDIRECTORY=['gfzEnMAP','enmap-ac','sample_data','sample1'])
  settings['ws_sample_dir'] = filepath('' $
                                , ROOT=enmapBox_getDirname(/SOURCECODE, /Applications) $
                                , SUBDIRECTORY=['gfzEnMAP','enmap-ac','sample_data'])
  
  settings['enmap_lut'] = filepath('MOMO_EnMAP_LUT_formatted_1nm' $
                                , ROOT=enmapBox_getDirname(/SOURCECODE, /Applications) $
                                , SUBDIRECTORY=['gfzEnMAP','enmap-ac','AC_data'])

  
  
  if settings.hubIsa('ac_sample_file') then begin
    ;is relative to sample dir
    if ~(file_info(settings['ac_sample_file'])).exists then begin
      settings['ac_sample_file'] = filepath(settings['ac_sample_file'] $
                        , ROOT_DIR=settings.hubGetValue('ac_sample_dir', default=''))
    endif
  endif
  
  if settings.hubIsa('ws_sample_file') then begin
    ;is relative to sample dir
    if ~(file_info(settings['ws_sample_file'])).exists then begin
      settings['ws_sample_file'] = filepath(settings['ws_sample_file'] $
                        , ROOT_DIR=settings.hubGetValue('ws_sample_dir', default=''))
    endif
  endif
  
  ;sample data 
  return, settings

end

;+
; :Hidden:
;-
pro test_gfzEnMAP_getSettings

  print, gfzEnMAP_getSettings()

end
