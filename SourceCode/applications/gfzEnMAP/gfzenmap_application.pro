;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application main procedure. It queries user input (see `gfzEnMAP_getParameters`)
;    via a widget program. The user input is passed to the application processing routine
;    (see `gfzEnMAP_processing`). For detailed information about implementing image processing
;    routines `ToDo Tutorial`. Finally, all results are presented in a HTML report 
;    (see `gfzEnMAP_showReport`).
;
; :Params:
;    applicationInfo : in, optional, type=hash
;       See `enmapBoxUserApp_getApplicationInfo`.
;
;-
pro gfzEnMAP_application, applicationInfo
  
  ; get global settings for this application
  settings = gfzEnMAP_getSettings()
  
  ; save application info to settings hash
  settings = settings + applicationInfo
  
  case STRLOWCASE(settings['argument']) of
    'enmap_ac' : enmap_ac_wrapper, settings
    'enmap_ws' : enmap_ws_wrapper, settings
  endcase
  
;  parameters = gfzEnMAP_getParameters(settings)
;  
;  if parameters['accept'] then begin
;  
;    reportInfo = gfzEnMAP_processing(parameters, settings)
;    
;    if parameters['showReport'] then begin
;      gfzEnMAP_showReport, reportInfo, settings
;    endif
;  
;  endif
  
end

;+
; :Hidden:
;-
pro test_gfzEnMAP_application
  applicationInfo = Hash()
  gfzEnMAP_application, applicationInfo

end  
