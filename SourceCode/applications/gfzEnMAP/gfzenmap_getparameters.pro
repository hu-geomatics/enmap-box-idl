;+
; :Author: <author name> (<email>)
;-

;+
; :Description:
;    This is the application user input function. It queries all required user input
;    via a widget program (for details see auto-managed widget programs provided 
;    by the` hubAPI` library). 
;
; :Params:
;    settings : in, optional, type=hash
;
;-
function gfzEnMAP_getParameters, settings
  
  groupLeader = settings.hubGetValue('groupLeader')
  
  hubAMW_program , groupLeader, Title = settings['title']
  hubAMW_label, 'This is '+settings['title']

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Input'
    ; insert your widgets

  ; frame for output options & parameters
  hubAMW_frame ,   Title =  'Output'
    ; insert your widgets
  hubAMW_checkbox, 'showReport', Title='Show Report', Value=1b

  parameters = hubAMW_manage()

  ; if required, perform some additional changes on the parameters hash
  
  return, parameters
end

;+
; :Hidden:
;-
pro test_gfzEnMAP_getParameters

  ; test your routine
  settings = gfzEnMAP_getSettings()
  parameters = gfzEnMAP_getParameters(settings)
  print, parameters

end  
