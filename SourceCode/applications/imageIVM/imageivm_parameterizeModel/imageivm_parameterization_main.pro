pro imageivm_parameterization_main $
   ,GROUP_LEADER=group_leader

; user input   
   parameters = imageivm_parameterization_input(group_leader)
  
if isa(parameters) then begin
  stopWatch = hubProgressStopWatch() 
  imageivm_parameterization, parameters, groupLeader = groupLeader, NoShow = 0
  stopWatch.showResults, title='imageIVM', description='Parameterization completed'  
  report = imageIVM_view_model(parameters, groupLeader=GroupLeader)
  report.saveHTML, /Show 
  ;apply model to image
  ok = dialog_message('Do you want to apply the model to an image?', /QUESTION, TITLE=parameters['title'])
  if ok eq 'Yes' then begin
    imageIVM_apply_main,GROUP_LEADER=group_leader
  endif
endif 
     
END

