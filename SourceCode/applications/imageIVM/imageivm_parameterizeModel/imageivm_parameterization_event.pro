pro imageivm_parameterization_event $
  ,event

  @huberrorcatch

  if (STRMATCH( getenv('path'), '*imageIVM*') EQ 0) then begin
    setenv, 'PATH='+getenv('path')+path_sep(/SEARCH_PATH)+filepath('lib', ROOT_DIR=imageIVM_getDirname())  
  endif
    
  buttonInfo = hubProEnvHelper.getMenuEventInfo(event)
  imageivm_parameterization_main,GROUP_LEADER=event.top
  
end
