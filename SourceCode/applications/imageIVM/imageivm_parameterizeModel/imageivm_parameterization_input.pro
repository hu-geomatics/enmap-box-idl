function imageivm_parameterization_input, groupLeader

  results = Hash()
  stateHash = hub_getAppState('imageIVM', 'stateIVC')
  if isa(stateHash) then begin
    results += stateHash
  endif
  stateHash = results;
  
  tsize = 100;
  
  hubAMW_program, title='imageIVM: IVC Parameterization', groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleset,'inputSampleSet', Title='Image' $   ; specify the feature image title
    , value = stateHash.hubGetValue('inputImage') $         ; specify the default image filename
    , Regression = 0, Classification=1 $                    ; boolean to select a classification sample set
    , ReferenceTitle='Reference Areas' $                    ; specify the label image title
    , ReferenceValue = stateHash.hubGetValue('inputAnno') $ ; specify the default image filename
    , tsize=tsize                                           ; widget width in pixel 
       
  ; Advanced Parameters 
  hubAMW_frame, Title='Advanced Parameters', /ADVANCED 
  hubAMW_parameter      ,'maxNumberOfIV'   $
                        ,Title='Max Number of Import Vectors' $
                        ,Size=10,/Integer,IsGE=1,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('maxNumberOfIV', default=100) 
  hubAMW_parameter      ,'stopCriterion'   $
                        ,Title='Stopping Criterion Threshold' $
                        ,Size=10,/FLOAT,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('stopCriterion', default=0.001) 
  hubAMW_parameter      ,'tempFilt'   $
                        ,Title='Temporal Filtering' $
                        ,Size=10,/FLOAT,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('tempFilt', default=0.95) 

  ; Gridsearch 
  hubAMW_frame, Title='Grid Search', /ADVANCED 
  hubAMW_label, 'Number of Folds for Crossvalidation'
  hubAMW_subframe, 'numberOfFolds'  ,Title='3-fold', /Row,/SetButton
  hubAMW_subframe, 'numberOfFolds'  ,Title='5-fold', /Row
  
  hubAMW_subframe, Title='Gaussian RBF Kernel Parameter g', /Row
  hubAMW_parameter ,'minGamma'   $
                        ,Title='Min(g) 2^' $
                        ,Size=5,/Integer,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('minSigma', default=-8)
  hubAMW_parameter ,'maxGamma'   $
                        ,Title='Max(g) 2^' $
                        ,Size=5,/Integer,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('maxSigma', default=3)
                                           
  hubAMW_subframe, Title='Regularization Parameter C', /Row
  hubAMW_parameter ,'minLambda'   $
                        ,Title='Min(C)  exp' $
                        ,Size=5,/Integer,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('minLambda', default=-12)                
  hubAMW_parameter ,'maxLambda'   $
                        ,Title='Max(C)  exp' $
                        ,Size=5,/Integer,ISLE=50000 $
                        ,VALUE= stateHash.hubGetValue('maxLambda', default=-3)  
  hubAMW_subframe, /Row                                                                     
  hubAMW_parameter, 'numberOfCandidates' $ 
                        ,Title='Maximum Number of Tested Candidate Import Vectors'$
                        ,Size=5,/Integer, IsGE=1, ISLE=60000 $
                        ,VALUE=stateHash.hubGetValue('numberOfCandidates', default=100)   
                                                             
  ; Output file                      
  hubAMW_frame, Title='Output'
  extension = 'ivc';
  hubAMW_outputFilename,'model', Title= 'IVC Model', EXTENSION=extension $
                       , VALUE=stateHash.hubGetValue('model', default= extension+'Model.'+extension) $
                       , tsize=tsize
                                                              
  result = hubAMW_manage()  
  
  if result['accept'] then begin 
    parameters = result
    parameters['title'] = title
    numberOfBands = (hubIOImgInputImage((parameters['inputSampleSet'])['featureFilename'])).getMeta('bands')
    
    parameters['inputImage']  = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputLabels'] = (parameters['inputSampleSet'])['labelFilename']
    parameters.remove, 'inputSampleSet'
    
    imageIVM_setAppStateHash, parameters
  endif else begin
    parameters = !NULL
  endelse
  return, parameters

end

pro TEST_JAVA
  oJSession = OBJ_NEW('IDLjavaObject$IDLJAVABRIDGESESSION')
  oJVersion = oJSession -> GetVersionObject()
  PRINT, "Java version=", oJVersion -> GetJavaVersion()
    
  phi = dindgen(3, 10)
  labels = Double([1.0, 1.0, 2.0, 1.0, 1.0, 2.0, 2.0, 2.0, 1.0, 2.0])
  numberOfCandidates = 50
  maxNumberOfIV = 100
  stopCriterion = 0.001
  tempFilt = 1
  minGamma = -8
  maxGamma = 3
  minLambda = -8
  maxLambda = -3
  ;test = obj_new("IDLJavaObject$GAUSSIANKERNEL",'GaussianKernel', phi, phi2, gamma)
  ;a = test.getKernel()
  ;print, a
  test2 = obj_new("IDLJavaObject$IVM",'IVM', phi, labels,  numberOfCandidates, maxNumberOfIV, stopCriterion, tempFilt, minGamma, maxGamma, minLambda, maxLambda)
END