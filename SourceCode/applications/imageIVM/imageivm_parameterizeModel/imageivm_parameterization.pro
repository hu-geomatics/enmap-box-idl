function imageivm_parameterize_getFeaturesAndLabels, imgFeatures, imgLabels
  
  nBands = imgFeatures.getMeta('bands')
  nSamples = imgFeatures.getMeta('samples')
  nLines   = imgFeatures.getMeta('lines')
  
  divFeatures = imgFeatures.getMeta('data ignore value')
  divLabels = imgLabels.isClassification() ? 0b : imgLabels.getMeta('data ignore value')
  
  tileLines = hubIOImg_getTileLines(nSamples, nBands+3, imgFeatures.getMeta('data type'))
  imgFeatures.initReader, tileLines, /TileProcessing, /Slice
  imgLabels.initReader, tileLines, /TileProcessing, /Slice
  
  finalFeatures = []
  finalLabels   = []
  while ~imgFeatures.tileProcessingDone() do begin
    featureData = imgFeatures.getData()
    labelData   = imgLabels.getData()
    nPixels = n_elements(labelData)
    mask = make_array(nPixels, /Byte, value=1)
    if isa(divFeatures) then begin
      for iB = 0, nBands-1 do begin
        mask and= featureData[iB, *] ne divFeatures
      endfor  
    endif
    if isa(divLabels) then begin
      mask and= labelData ne divLabels
    endif  
    iValid = where(mask ne 0, /NULL)
    if isa(iValid) then begin
      finalFeatures = [[finalFeatures], [featureData[*,iValid]]]
      finalLabels = [[finalLabels], [labelData[*,iValid]]]
    endif 
  endwhile
  
  imgFeatures.finishReader
  imgLabels.finishReader
   return, {features:finalFeatures, labels:finalLabels}
end

PRO imageivm_parameterization, parameters $
    , groupLeader = groupLeader $
    , NoShow = Noshow
  
  HELPER = hubHelper()
  progressBar = hubProgressBar(Title='imageIVM', GroupLeader=group_leader)
  progressBar.setInfo, 'Load data..'
  ;; check input and parameters
  ;required / non-optional keywords
  required = ['model', 'inputImage', 'inputLabels']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif

  ; check input data
  if  (FILE_INFO(parameters['model'])).exists and ~(FILE_INFO(parameters['model'])).WRITE THEN message, 'Model is not writable!!'
  if ~(FILE_INFO(parameters['inputImage'])).EXISTS then message, "Image doesn't exist!!"
  if ~(FILE_INFO(parameters['inputLabels'])).EXISTS then message, "Labeled reference doesn't exist!!"
  
  ; check advanced parameters
  if ~parameters.hubIsa('numberOfCandidates') then begin
     parameters['numberOfCandidates'] = 100 
  endif else begin
      if parameters['numberOfCandidates'] lt 1 then message, 'Number of tested candidates is not valid!'
  endelse
  
  if ~parameters.hubIsa('maxNumberOfIV') then begin
     parameters['maxNumberOfIV'] = 500
  endif else begin
      if parameters['maxNumberOfIV'] lt 1 then message, 'Maximum number of import vectors is not valid!'
  endelse  
  
  if ~parameters.hubIsa('stopCriterion') then begin
     parameters['stopCriterion'] = 0.001
  endif else begin
      if parameters['stopCriterion'] lt 0 $
      or parameters['stopCriterion'] gt 1 then message, 'Stop criterion is not valid!'
  endelse 
  
  if ~parameters.hubIsa('tempFilt') then begin
     parameters['tempFilt'] = 0.95
  endif else begin
      if parameters['tempFilt'] lt 0 $
      or parameters['tempFilt'] gt 1 then message, 'Value for temporal filtering is not valid!'
  endelse 
  
  if ~parameters.hubIsa('minGamma') then begin
     parameters['minGamma'] = -8
  endif else begin
      if parameters['minGamma'] gt parameters['maxGamma'] then message, 'Maximum value for gamma should be larger than minimum value for gamma!'
  endelse 
  
  if ~parameters.hubIsa('maxGamma') then begin
     parameters['maxGamma'] = 3
  endif else begin
      if parameters['maxGamma'] lt parameters['minGamma'] then message, 'Minimum value for gamma should be smaller than maximum value for gamma!'
  endelse 
  
  if ~parameters.hubIsa('minLambda') then begin
     parameters['minLambda'] = -10
  endif else begin
      if parameters['minLambda'] gt parameters['maxLambda'] then message, 'Maximum value for lambda should be larger than minimum value for lambda!'
  endelse 
  
  if ~parameters.hubIsa('maxLambda') then begin
     parameters['maxLambda'] = 3
  endif else begin
      if parameters['maxLambda'] lt parameters['minLambda'] then message, 'Minimum value for lambda should be smaller than maximum value for lambda!'
  endelse 

  ;; Load training data
  ; get images and meta information             
  inputImg = hubIOImgInputImage(parameters['inputImage'])
  inputRef = hubIOImgInputImage(parameters['inputLabels'], /Classification)
  
  ; extract meta information
  numberOfClasses = inputRef.getMeta('classes')
  class_names =     inputRef.getMeta('class names')
  lookup      =     inputRef.getMeta('class lookup')
  numberOfBands  = inputImg.getMeta('bands')
  dataType       = inputImg.getMeta('data type')
  
  ; get features and labels and 
  featuresAndLabels = imageivm_parameterize_getFeaturesAndLabels(inputImg, inputRef)
  features = featuresAndLabels.features
  referenceClasses = double(reform(featuresAndLabels.labels))
  
  features = double(features) 
  
  if (parameters['numberOfFolds'] eq 0) then folds = 3
  if (parameters['numberOfFolds'] eq 1) then folds = 5
      
  ; initialize ivm object and set parameters in java program  
  ivm_object = obj_new("IDLJavaObject$IVM",'IVM', features, referenceClasses $
    , long(parameters['numberOfCandidates']), long(parameters['maxNumberOfIV']) $ 
    , parameters['stopCriterion'], parameters['tempFilt'] $ 
    , long(parameters['minGamma']), long(parameters['maxGamma']) $ 
    , long(parameters['minLambda']), long(parameters['maxLambda']))
  progressBar.setInfo, 'Learn model..'
  ; normalize features
  features = ivm_object -> ivm_scaleData(features)
  
  ; crossvalidation
  ivm_object -> do_crossvalidation, features, referenceClasses, folds

  ; learn model with fixed parameters
  ivm_object -> ivm_learn, features, referenceClasses
  
  ; save model
  alpha = ivm_object -> get_parameters(); parameters
  IVs = ivm_object -> get_importVectors(); import vectors
  trainAcc = ivm_object -> get_trainAcc(); classification error on training data
  gamma = ivm_object -> get_kernelParameter(); kernel parameter
  lambda = ivm_object -> get_regularizationParameter(); regularization parameter
  cvaccuracy = ivm_object -> get_crossvalidationAccuracy(); crossvalidation accuracies
  normalizeMean = ivm_object -> get_normalizeMean(); normalization mean
  normalizeStd = ivm_object -> get_normalizeStd(); normalization standard deviation
  
  IVM_Model = Hash()
 
  settings = {numberOfClasses : numberOfClasses $
    ,namesOfClasses : class_names $
    ,numberOfBands : numberOfBands $
    ,lookup : lookup $
    ,inputImage : parameters['inputImage'] $
    ,inputLabels : parameters['inputLabels'] $
    ,datatype : inputImg.getMeta('data type') $
    ,datatype_y : inputRef.getMeta('data type') $
    ,parameters : parameters $
    }
 
  SAVE, settings, features, referenceClasses, IVs, alpha, gamma, trainAcc, lambda, cvaccuracy, normalizeMean, normalizeStd, $
   Filename = parameters['model'], /COMPRESS
   
; Cleanup everything 
  inputImg.cleanup
  inputRef.cleanup
  progressBar.cleanup
 
END