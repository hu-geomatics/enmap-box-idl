;+
; :Author: <author name> (<email>)
;-

function imageIVM_getDirname, SourceCode=sourceCode
  
  result = filepath('imageIVM', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result
  
end

;+
; :Hidden:
;-
pro test_imageIVM_getDirname

  print, imageIVM_getDirname()
  print, imageIVM_getDirname(/SourceCode)

end