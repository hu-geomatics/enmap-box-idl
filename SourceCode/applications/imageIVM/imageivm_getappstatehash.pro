
;+
; :Description:
;     Restores state from previous application calls to retrieve default values and
;     returns a hash.
;
;-
function imageIVM_getAppStateHash
  results = Hash()
  stateHash = hub_getAppState('imageIVM', 'stateIVC')
  if isa(stateHash) then begin
    results += stateHash
  endif
  return, results
end