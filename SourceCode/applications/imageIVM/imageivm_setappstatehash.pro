;+
; :Description:
;    Saves the state of a certain imageRF session.
;-
pro imageIVM_setAppStateHash, stateHash
  hub_setAppState, 'imageIVM', 'stateIVC', stateHash
end