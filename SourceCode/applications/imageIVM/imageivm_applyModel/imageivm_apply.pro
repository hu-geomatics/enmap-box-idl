pro imageivm_apply, parameters $
        , NoOpen=noOpen $
        , GroupLeader= groupLeader $
        , NoShow = NoShow
  
   HELPER = hubHelper()
   progressBarInfo = ['Classify Image..']
                     
   progressBar = hubProgressBar( $
            title = (parameters.hubIsa( 'title')) ? parameters['title'] : !NULL $
          , info = progressBarInfo $
          , GroupLeader = groupLeader $
          , NoShow = NoShow)  
                     

  ;check parameters 
  required =  ['model', 'inputImage', 'outputImgEst']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  IF ~(FILE_INFO(parameters['model'])).EXISTS THEN MESSAGE, "Model doesn't exist!!"
  IF ~(FILE_INFO(parameters['inputImage'])).EXISTS THEN MESSAGE, "Image doesn't exist!!"
 
  useMask = parameters.hubIsa( 'inputMask')
  if useMask then begin
      if ~(FILE_INFO(parameters['inputMask'])).EXISTS THEN MESSAGE, "Mask doesn't exist!!"
  endif

  
  imageIVM_loadModel $
    ,parameters['model'] $
    ,SETTINGS=settings $
    ,IVS=IVs $
    ,ALPHA = alpha $
    ,GAMMA = gamma $
    ,NORMALIZEMEAN = normalizeMean $
    ,NORMALIZESTD = normalizeStd
 
  outputImage = hubIOImgOutputImage(parameters['outputImgEst'], NoOpen=NoOpen) 
  inputImage = hubIOImgInputImage(parameters['inputImage'])
 
  if useMask then begin
    maskImage = hubIOImgInputImage(parameters['inputMask'])
  endif
  
  
  ;Default Values
  def_tilelines = hubIOImg_getTileLines(inputImage.getMeta('samples') $
                                      , inputImage.getMeta('bands') $
                                      , inputImage.getMeta('data type'))
  
  
  IF settings.numberOfBands ne inputImage.getMeta('bands') THEN MESSAGE, "Number of bands don't match!!"
  
  outputImage.copyMeta,inputImage,['samples','lines','map info','coordinate system string','x start','y start']
  outputImage.setMeta,'bands',1
  outputImage.setMeta,'interleave','bsq'
  outputImage.setMeta,'file type','ENVI Classification'
  outputImage.setMeta,'data type', 1
  outputImage.setMeta,'classes',settings.numberOfClasses
  outputImage.setMeta,'class lookup',settings.lookup
  outputImage.setMeta,'class names',settings.namesOfClasses
  probabilityImageFile = hubIOImgOutputImage(parameters['outputImgEst']+'_prob')
  probabilityImageFile.copyMeta,inputImage,['map info','coordinate system string','x start','y start']
  probabilityImageFile.setMeta, 'band names', settings.namesofclasses[1:*]
 
 
  ;init readers & writers
  inputImage.initReader, def_tilelines, /Slice, /TileProcessing
  writerSettings = inputImage.getWriterSettings(SetBands=1, SetDataType=outputImage.getMeta('data type'))
  outputImage.initWriter, writerSettings
  IF useMask THEN maskImage.initReader, def_tilelines, /Slice, /TileProcessing, /Mask
  
  ;use same writerSetting as for input Image (with exception of the number of bands)
  probabilityImageWriterSettings = inputImage.getWriterSettings(SetBands = settings.numberOfClasses - 1, SetDataType='float')
  probabilityImageFile.initWriter, probabilityImageWriterSettings 
  
  ; get ivm object
  ivm_object = obj_new("IDLJavaObject$IVM",'IVM')
      
  ;start tile processing
  progress_max = float(outputImage.getMeta('lines')) 
  progress_lines = 0l 
  while ~inputImage.tileProcessingDone() do begin
    
    progressBar.setProgress, progress_lines/progress_max
    progress_lines += def_tilelines
    features = inputImage.getData()
    IF useMask THEN BEGIN 
      mask = maskImage.getData()
    ENDIF
    s = size(features)
    numLabels = s[2]
    
    no_pixels = (size(features, /dimension))[1]
    imgsize = [1, no_pixels]
    ;get Mask Indizes
    if keyword_set(mask) then begin
      if n_elements(mask) ne no_pixels then message, 'Number of pixels in mask and featureset differs!'
      imageIndex = WHERE(mask ne 0, /NULL , unmaskedCount, complement=maskedIndex)
    endif else begin
      imageIndex = LINDGEN(no_pixels)
    endelse
    imageIndexSize = size(imageIndex, /DIMENSIONS)
    
    ; scale features (with usage of mask if available)
    features = ivm_object -> ivm_scaleData(features[*, imageIndex], normalizeMean, normalizeStd)
    
    ; classify test vectors in image
    ivm_object -> ivm_test, features, alpha, IVs, gamma
    estimates = MAKE_ARRAY(no_pixels, 1, /LONG, value=-1)
    estimatesMask = ivm_object -> get_estimLabels()
    estimates[imageIndex, *] = estimatesMask
    probabilitiesMask = ivm_object -> get_probabilities()
    probabilityImage = MAKE_ARRAY(settings.numberOfClasses - 1, no_pixels, /DOUBLE, value=0)
    probabilityImageMask = reform(probabilitiesMask, [settings.numberOfClasses - 1, imageIndexSize])
    probabilityImage[*, imageIndex] = probabilityImageMask 
    
    ; write data
    outputImage.writeData, (estimates + 1)
    probabilityImageFile.writeData, probabilityImage
  endwhile ;end Tile-Processing
  
  ;clean & close everything
  inputImage.cleanup
  outputImage.cleanup
  if isa(maskImage) then maskImage.cleanup
  if isa(probabilityImageFile) then probabilityImageFile.cleanup 
  
  progressBar.cleanup
END