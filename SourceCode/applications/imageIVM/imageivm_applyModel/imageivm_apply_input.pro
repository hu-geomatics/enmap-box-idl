function imageIVM_apply_getParametersCheck, resultHash, Message=message

  msg = !NULL
  isConsistent = 1b
  
  inputImageFilename = (resultHash['inputSampleSet'])['featureFilename']
  inputImage = hubIOImgInputImage(inputImageFilename)
  imageIVM_loadModel, resultHash['model'] , SETTINGS=settings
  
  if inputImage.getMeta('bands') ne settings.numberOfBands then begin
    msg = [msg $ 
          , string(settings.numberOfBands $
          , format='(%"This model requires an input image with %i bands.")') $
          , string(file_basename(inputImageFilename), inputImage.getMeta('bands') $
          , format='(%"(Bands in \"%s\": %i)")') $
          
          ]
    isConsistent *= 0b
  
  endif
  inputImage.cleanup
  message=msg
  return, isConsistent
end

function imageIVM_apply_input, groupLeader

  title_prog  = 'imageIVM: Classify Image'
  title_model = 'IVC Model'
  file_ext    = 'ivc'
  
  stateHash = imageIVM_getAppStateHash()
  
  tsize=100
  hubAMW_program ,Title=title_prog                 ,groupLeader
    hubAMW_frame ,Title='Input'
    
    hubAMW_inputFilename  ,'model'        ,Title=title_model, EXTENSION=file_ext $
          , Value=stateHash.hubGetValue('model'), tsize=tsize 
    hubAMW_inputSampleSet ,'inputSampleSet'  ,Title='Image    ' $
          , /Masking, /ReferenceOptional $
          , value = stateHash.hubGetValue('inputImage'), tsize=tsize
          
    hubAMW_frame                             ,Title='Output'
    
    hubAMW_outputFilename ,'outputImgEst'   ,Title= 'IVC Estimation' $
      , VALUE=stateHash.hubGetValue('outputImgEst', default = file_ext+'Estimation') $
      , tsize=tsize
  
  result = hubAMW_manage(CONSISTENCYCHECKFUNCTION='imageIVM_apply_getParametersCheck')
  if result['accept'] then begin
    parameters = result
    parameters['title'] = title_prog
    parameters['inputImage'] = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputMask']  = (parameters['inputSampleSet'])['labelFilename']
    parameters.remove, 'inputSampleSet'  
    
    imageIVM_setAppStateHash, parameters
  endif else begin
    parameters = !NULL
  endelse
  return, parameters
END