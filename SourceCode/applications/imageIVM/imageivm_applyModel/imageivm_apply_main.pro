
PRO imageIVM_apply_main $
  ,GROUP_LEADER=group_leader
  
  parameters = imageIVM_apply_input(group_Leader)
  
  if isa(parameters) then begin  
      stopWatch = hubProgressStopWatch() 
      imageIVM_apply, parameters
      stopWatch.showResults, title='imageIVM', description='Calculations completed.'    
  endif
END