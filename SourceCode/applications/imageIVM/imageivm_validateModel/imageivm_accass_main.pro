pro imageivm_accass_main $
,GroupLeader=groupLeader    
    
  parameters = imageIVM_accass_input(groupLeader)
   
  ; accuracy assessment
  if isa(parameters) then begin
     performance = imageivm_accass(parameters, groupLeader=groupLeader)
     parameters['type'] = 'IVC (Import Vector Classification)'

     report = hubApp_accuracyAssessment_getReport(performance, /createPlots, GroupLeader=GroupLeader)
     report.SaveHTML, /show
     
     file_delete, performance['inputEstimation']  $
                , performance['inputEstimation'] + '.hdr', /ALLOW_NONEXISTENT
     
     
  endif else begin
;    print, 'Canceled by user'
  endelse
end