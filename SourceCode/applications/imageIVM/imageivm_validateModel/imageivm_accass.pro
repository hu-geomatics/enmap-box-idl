function imageIVM_accass, parameters $
    , TileLines = tileLines $
    , groupLeader = groupLeader $
    , NoShow = NoShow
  
  
  ;check parameters
  helper = hubHelper() 
  required = ['model', 'inputImage', 'inputReference']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  settings = hash('groupLeader', groupLeader)
  
  ;create path for temporary estimation file
  fn_est_tmp = string(format='(%"tmp_%s_%i")' $
      , file_basename(parameters['model'], '.*') $
      ;, file_basename((parameters['inputSampleSet'])['labelFilename']) $
      , systime(/seconds))
  fn_est_tmp = strjoin(strsplit(fn_est_tmp,':. ',/EXTRACT))
  
  processing_parameters = Hash()
  processing_parameters['outputImgEst'] = filepath(fn_est_tmp, /TMP)
  processing_parameters['inputMask'] = parameters['inputReference']
  processing_parameters['model'] = parameters['model']
  processing_parameters['inputImage'] = parameters['inputImage']
  processing_parameters['title'] = parameters.hubIsa('title') ? parameters['title']:'imageIVM: Fast Accuracy Assessment' 
  
  imageIVM_apply, processing_parameters, /NoOpen, NoShow = NoShow

  accass_parameters = Hash()
  accass_parameters['inputReference']  = parameters['inputReference']
  accass_parameters['inputEstimation'] = processing_parameters['outputImgEst']
  
  ;write an error image if required
  if parameters.hubIsa( 'outputErrorImage') then begin
     accass_parameters['outputErrorImage'] = parameters['outputErrorImage']
  endif
  performance = hubApp_accuracyAssessment_processing(accass_parameters, settings)
  
  if parameters.hubKeywordSet('deleteEstimationImage') then begin
    file_delete, processing_parameters['outputImgEst']  $
               , processing_parameters['outputImgEst'] + '.hdr', /ALLOW_NONEXISTENT
  endif
  return, performance
END