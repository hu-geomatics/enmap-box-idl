function imageIVM_accass_input, groupLeader

  stateHash = imageIVM_getAppStateHash()
    
  title = 'imageIVM: Fast Accuracy Assessment'
  hubAMW_program, groupLeader, Title= title
  hubAMW_frame, Title='Input'

  title_model = 'IVC Model      '
  title_img   = 'Image          '
  title_ref =   'Reference Areas'
  ext_model = 'ivc' 
  flag_reg = 0
  flag_class = 1
      
  hubAMW_inputFilename ,'model'       ,Title=title_model, EXTENSION=ext_model $
          , Value = stateHash.hubGetValue('model')
          
  if ~(flag_reg and flag_class) then begin
    hubAMW_inputSampleSet,'inputSampleSet' , Title=title_img, referenceTitle=title_ref $
                      , value=stateHash.hubGetValue('inputImage') $
                      , classification=flag_class, regression=flag_reg 
    result = hubAMW_manage()
  endif else begin
    ;TODO typ selbst bestimmen
    ;hubAMW_program, groupLeader, Title='Parameters Fast Accuracy Assessment'
    ;hubAMW_frame, Title='Input Parameters'
  endelse
  
  ;hubAMW_inputImageFilename, 'fn_valid', title='Reference'
  
  
  if result['accept'] then begin 
    parameters = result
    parameters['title'] = title
    parameters['inputImage'] = (parameters['inputSampleSet'])['featureFilename']
    parameters['inputReference']  = (parameters['inputSampleSet'])['labelFilename']
    parameters['rfType'] = rfType
    
    imageIVM_setAppStateHash, parameters
    
  endif else begin
    parameters = !NULL
  endelse  
  
  return, parameters


end