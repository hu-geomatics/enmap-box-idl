function imageIVM_view_model, parameters, GroupLeader=groupLeader

compile_opt strictarr

  IF ~(FILE_INFO(parameters['model'])).EXISTS THEN MESSAGE, "File doesn't exist!!"
  
  report = hubReport(title = 'View IVC Model Parameters')

  imageIVM_loadModel $
    ,parameters['model'] $
    ,SETTINGS=settings $
    ,FEATURES=features $
    ,IVS=IVs $
    ,ALPHA = alpha $
    ,GAMMA = gamma $
    ,LAMBDA = lambda $
    ,CVACCURACY = cvaccuracy
  
  sizeIVs = size(IVs)
  sizeIVs = sizeIVs[2]
  sizeFeatures = size(features)
  sizeFeatures = sizeFeatures[2]
  
  gammaList = list()
  for i=settings.parameters.hubGetValue('minGamma'), settings.parameters.hubGetValue('maxGamma') do begin &$
    gammalist.add, i &$
  endfor
  lambdaList = list()
  for i=settings.parameters.hubGetValue('minLambda'), settings.parameters.hubGetValue('maxLambda') do begin 
    lambdaList.add, i 
  endfor
  
  report.addHeading, 'imageIVM Model', 1
  report.addMonospace, 'Filename: '+ parameters['model']
  
  report.addHeading, 'Input Files', 1
  report.addMonospace, ['  Image:           ' + settings.inputImage, $
                        '  Reference Areas: ' + settings.inputLabels]
  
  classnames = settings.namesOfClasses + [REPLICATE(',', N_ELEMENTS(settings.namesOfClasses)-1), '$']
  report.addHeading,    'Training Data', 1
  report.addMonospace,  [ $ 
              'Number of Samples:   ' + STRTRIM(sizeFeatures, 2) $
             ,'Number of Features:  ' + STRTRIM(settings.numberOfBands,2) $
             ,'Number of Classes:   ' + STRTRIM(settings.numberOfClasses-1 , 2) $
             ,'Class Names          ' + strjoin((settings.NAMESOFCLASSES)[1:*],', ') $
   ]     
           
  report.addHeading, 'Model Parameter',1
  report.addMonospace,  [ $  
      'Gaussian RBF Kernel Parameter g: 2^(' + STRCOMPRESS(String((alog(gamma) / alog(2)), Format='(F10.2)'),/Remove_all) + ')' $
      ,'Regularization Parameter C:      exp(' + STRCOMPRESS(String((alog(lambda)), Format='(F10.2)'),/Remove_all) + ')' $
      ,'Number of Import Vectors:        ' + STRTRIM(sizeIVs, 2) $
   ] 

  if (settings.parameters.hubGetValue('numberOfFolds') eq 0) then folds = 3
  if (settings.parameters.hubGetValue('numberOfFolds') eq 1) then folds = 5
  
  report.addHeading,    'Parameter Search', 1
  report.addHeading,    'Settings', 2
  text =               ['Gaussian RBF Kernel Parameters g: 2^('+ strcompress(strjoin(gammalist.toarray(),',')) +')',$
                         'Regularization Parameters C:      exp('+ strcompress(strjoin(lambdalist.toarray(),',')) +')',$
                         'Number of Cross Validation Folds: '+ STRCOMPRESS(folds,/Remove_all)]
  report.addMonospace, text

  gIndex = indgen(settings.parameters.hubGetValue('maxGamma')-settings.parameters.hubGetValue('minGamma')+1)
  cIndex = indgen(settings.parameters.hubGetValue('maxLambda')-settings.parameters.hubGetValue('minLambda')+1)
  report.addHeading,    'Search History', 2
  text = list()
  text.add, '  Performance*  g(2^(*))  C(exp(*))'
  text.add, '  ---------------------------------'
  foreach c, cIndex do begin 
    foreach g, gIndex do begin 
      text.add, string(cvaccuracy[g,c], Format='(F10.2)')+string(settings.parameters.hubGetValue('minGamma')+g, Format='(F10.2)')+string(settings.parameters.hubGetValue('minLambda')+c, Format='(F10.2)') 
    endforeach
  endforeach
  text.add, '*Average F1 Accuracy'
  
  report.addMonospace, text.toArray()
  
  performances = cvaccuracy
  dims=size(performances,/DIMENSIONS)
  nCol=size(gIndex,/DIMENSIONS) ; number of C parameters
  nRow=size(cIndex,/DIMENSIONS) ; number of g parameters
  
  s=image(rebin(performances $
              , /SAMPLE $
              , nCol*nRow*10,nCol*nRow*10) $
              , RGB_TABLE=1 $
              , POSITION=[0,.2,.7,.93]$
              , /ORDER $
              , /BUFFER $
              , MIN_VALUE=0)
  c = COLORBAR(TARGET=s, ORIENTATION=1 $
              , POSITION=[0.7,.2,.75,.93] $
              , TITLE='Average F1 Accuracy' $
              , TEXTPOS = 1 $
              , TICKDIR = 0 $
              , SUBTICKLEN=0 $
              , TICKVALUES=[min(performances),max(performances)] $
              , BORDER_ON = 1 $
              , COLOR = 'black' $
              , FONT_STYLE = 'Italic' $
              , FONT_SIZE = 12)
;Two Arrows with corresponding letters g and C
  gArrow = ARROW([[0, -nCol*nRow/2],[nCol*nRow*10, -nCol*nRow/2]] $            
              , TARGET=s $
              , /DATA $
              , ARROW_STYLE=1 $
              , COLOR='black' $
              , THICK=1)
;RESOLVE_ROUTINE, 'text', /IS_FUNCTION 
  gLetter = TEXT(-nCol*nRow/2, nCol*nRow*10.3, TARGET=s, 'g' $
              , /DATA $
              , COLOR='black' $
              , FONT_SIZE=14 $
              , CLIP=0)
  
   cArrow = ARROW([[-nCol*nRow/2,nCol*nRow*10],[-nCol*nRow/2, 0]]$ 
              , TARGET=s $
              , /DATA $
              , ARROW_STYLE=1 $
              , COLOR='black' $
              , THICK=1)
  cLetter = TEXT(nCol*nRow*10.3, -nCol*nRow/2, TARGET=s, 'C' $
    , /DATA $
    , COLOR='black' $
    , FONT_SIZE=14 $
    , CLIP=0)            
  
  imageContent = transpose(s.CopyWindow(BORDER=0), [1,2,0])
  s.Close
  report.addImage, imageContent, 'Performance Surface' 
   
  return, report
END
