function imageIVM_view_model_input, groupLeader
  title = 'imageIVM: View Parameters'
  
  stateHash = imageIVM_getAppStateHash()
  
  hubAMW_program ,Title= title ,groupLeader
  hubAMW_frame   ,Title='Input'
  hubAMW_inputFilename ,'model', Title='IVC Model'  $
         , Value = stateHash.hubGetValue('model') $
         , EXTENSION='ivc'
         
  result = hubAMW_manage()

  if result['accept'] then begin 
    parameters = result
    parameters['title'] = title
    
    imageIVM_setAppStateHash, parameters
  endif else begin
    parameters = !NULL
  endelse  
  
  return, parameters
END