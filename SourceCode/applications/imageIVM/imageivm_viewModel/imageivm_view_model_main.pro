PRO imageIVM_view_model_main $
  ,GroupLeader = groupLeader
  
  
;--------------
;user input
  parameters = imageIVM_view_model_input(GroupLeader)
  
  if isa(parameters) then begin
    report = imageIVM_view_model(parameters, groupLeader=GroupLeader)
    report.saveHTML, /Show
  endif

END