;+
; :hidden:
;-
;+
; :Private:
; :hidden_file:
; :Description:
;    Loads a RF model and checks the variables and settings.
;
; :Params:
;    model : in, required, type=string
;                   Path and filename to model.
;
; :Keywords:
;    SETTINGS : out, optional, type=structure
;       Information and settings for the model.
;    FEATURES : out, optional
;       The training data. Required to determine the variable importance.
;    REFERENCECLASSES : out, optional, type=byte
;       The class labels for classification.
;
; :Author: Ribana Roscher
;-
PRO imageIVM_loadModel $
  ,model $
  ,SETTINGS=settings $
  ,FEATURES=features $
  ,REFERENCECLASSES=referenceClasses $
  ,IVS=IVs $
  ,ALPHA=alpha $
  ,GAMMA=gamma $
  ,LAMBDA = lambda $
  ,TRAINACC=trainAcc $
  ,CVACCURACY = cvaccuracy $
  ,NORMALIZEMEAN = normalizeMean $
  ,NORMALIZESTD = normalizeStd
  
  ;ON_ERROR,2

  model = IDL_Savefile(model)
  model.restore, 'settings'
  model.restore, 'features'
  model.restore, 'IVs'
  model.restore, 'alpha'
  model.restore, 'gamma'
  model.restore, 'lambda'
  model.restore, 'trainAcc'
  model.restore, 'cvaccuracy'
  model.restore, 'normalizeMean'
  model.restore, 'normalizeStd'
  
  model.cleanup

END