function imageSVMValidate::init, svmType, title, groupLeader
  case svmType of
    'svc' : labelType = 'classification'
    'svr' : labelType = 'regression'
  endcase
  return, self.hubAppWrapperAccuracyAssessment::init(labelType, title, groupLeader)
end

pro imageSVMValidate::apply, parameters, Title=title, GroupLeader=groupLeader
  imageSVM_apply_processing, parameters, Title=title, GroupLeader=groupLeader, /NoOpen
end

function imageSVMValidate::getGuiSvmType
  case self.labelType of
    'classification' : return, 'svc'
    'regression' :     return, 'svr'
  endcase
end

function imageSVMValidate::getGUIModelTitle
  return, strupcase(self.getGUISvmType())+' Model'
end

function imageSVMValidate::getGuiModelExtension
  return, self.getGUISvmType()
end

function imageSVMValidate::getGUIModelDefaultFilename
  return, hub_getappState('imageSVM', self.getGUISvmType()+'ModelFilename')
end

pro imageSVMValidate__define
 struct = {imageSVMValidate $
   , inherits hubAppWrapperAccuracyAssessment $
   }
end

pro test_imageSVMValidate
  imageSVMValidate = imageSVMValidate('classification', 'My AccAss', groupLeader)
  parameters = hash()
  parameters['modelFilename'] = 'd:\svcModel.svc'
  parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Validation-Sample')
  
  if isa(parameters) then begin
    print, parameters
    imageSVMValidate.setParameters, parameters
    imageSVMValidate.processing
  endif
end