pro imageSVM_validate_event, event
  
  @huberrorcatch
    
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  svmType = menuEventInfo.argument
  imageSVM_validate_application, svmType, GroupLeader=event.top, Title=menuEventInfo.name
  
end