pro imageSVM_validate_application, svmType, Title=title, GroupLeader=groupLeader

  imageSVMValidate = imageSVMValidate(svmType, title, groupLeader)
  parameters = imageSVMValidate.getParameters()
  if isa(parameters) then begin
    imageSVMValidate.processing, parameters
  endif

end
