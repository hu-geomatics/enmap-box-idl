;+
; :Description:
;    returns imageSVM default settings. The settings file is located under:
;    `filepath('imageSVM.conf', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='resource')`.
;    
; :Returns:
;    Returns a hash with all settings. 
;
; :Examples:
;    Print settings stored inside settings file::
;      print, imageSVM_getSettings()
;    IDL prints something like::
;      appName: NewApp
;      version: 1.0
;-
function imageSVM_getSettings
  
  settingsFilename = filepath('imageSVM.conf', ROOT_DIR=imageSVM_getDirname(), SUBDIRECTORY='resource')
  settings = (hubSettingsManager(settingsFilename)).getValue()

  settings['grid search c'] = float(settings['grid search c'])
  settings['grid search g'] = float(settings['grid search g'])
  settings['grid search termination criterion'] = float(settings['grid search termination criterion'])
  settings['training termination criterion'] = float(settings['training termination criterion'])
  settings['number of folds'] = fix(settings['number of folds'])
  
  ;settings['textLines'] = fix(settings['textLines'])
  return, settings

end

