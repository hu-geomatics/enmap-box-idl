function imageSVM_parameterize_getParameters, svmType, Title=title, GroupLeader=groupLeader

  tsize=100
     
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputSampleset,'sampleSet', Title='Image',$
    Regression=svmType eq 'svr', Classification=svmType eq 'svc', TSize=tSize,$
    VALUE=hub_getappState('imageSVM', svmType+'FeatureFilename'), ReferenceValue=hub_getAppState('imageSVM',  svmType+'LabelFilename'),$
    /SelectSpectralSubset

  hubAMW_frame, Title='Parameters', /Advanced
;  hubAMW_label, 'Gaussian RBF Kernel Parameter Range' 
  hubAMW_subframe, 'kernelIndex', /Row, Title='Linear Kernel'
  hubAMW_subframe, 'kernelIndex', /Row, Title='Gaussian RBF Kernel Parameter Range', /SetButton
  hubAMW_parameter, 'minG', Title='min(g)', Size=10, /Float, IsGT=0, Value=0.01
  hubAMW_parameter, 'maxG', Title=' max(g)', Size=10, /Float, IsGT=0, Value=1000.
  hubAMW_parameter, 'multiplierG', Title=' multiplier(g)', Size=10, /Float, IsGT=1, Value=10.
  hubAMW_checklist, 'testLinearKernel', Title=' Include linear kernel?', VALUE=1, List=['no','yes']
  hubAMW_subframe, /Column
  hubAMW_label, 'Regularization Parameter Range' 
  hubAMW_subframe, /Row
  hubAMW_parameter, 'minC', Title='   min(C)', Size=10, /Float, IsGT=0, Value=0.01
  hubAMW_parameter, 'maxC', Title=' max(C)', Size=10, /Float, IsGT=0, Value=1000.
  hubAMW_parameter, 'multiplierC', Title=' multiplier(C)', Size=10, /Float, IsGT=1, Value=10.
  hubAMW_subframe, /Column
  hubAMW_label, 'Cross Validation'
  hubAMW_parameter, 'numberOfFolds',      Title='   number of folds                          ', Size=3, /Integer, IsGE=3, Value=3
  hubAMW_label, 'Termination Criterion'
  hubAMW_parameter, 'stopGridSearch',     Title='   termination criterion for grid search    ', Size=10, /Float, IsGT=0, Value=0.1
  hubAMW_parameter, 'stopTraining',       Title='   termination criterion for final training ', Size=10, /Float, IsGT=0, Value=0.001
  if svmType eq 'svr' then begin
    hubAMW_label, 'Epsilon insensitive Loss Function Parameter'
    hubAMW_subframe, 'lossSelection', Title='automatic search', /ROW, /SetButton
    hubAMW_subframe, 'lossSelection', Title='user defined', /ROW
    hubAMW_parameter, 'lossUser',     Title='epsilon', Size=10, /Float, IsGE=0, Value='0.'
    hubAMW_subframe, /Column     
  endif
  if svmType eq 'svc' then begin
    hubAMW_subframe, 'performanceSelection', /Row, Title='Standard SVC Performance', /SetButton
    hubAMW_subframe, 'performanceSelection', /Column, Title='SVCQuant Performance'
    fractionList = ['0% 10% 20% 30% 40% 50% 60% 70% 80% 90% 100%', '0% 20% 40% 60% 80% 100%', '0% 25% 50% 75% 100%', '0% 33% 67% 100%']
    hubAMW_combobox,  'svcQuantFractions', Title='mixing fractions', Value=2, List=fractionList
    hubAMW_parameter, 'svcQuantSamples',   Title='number of randomly drawn synthetically mixed samples', Size=5, /INTEGER, IsGE=0, Value='1000'
  endif
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename,'modelFilename', Title= strupcase(svmType) + ' File', Extension=strlowcase(svmType), TSize=tSize, Value=svmType+'Model.'+svmType

  result = hubAMW_manage(/Flat)

  if result['accept'] then begin 
    parameters = result
    v1 = parameters.remove('minC')
    v2 = parameters.remove('maxC')
    v3 = parameters.remove('multiplierC')    
    exp1 = double(round(alog10(v1)/alog10(v3)))
    exp2 = double(round(alog10(v2)/alog10(v3)))
    vector = v3^(dindgen(exp2-exp1+1)+exp1)
    parameters['cArray'] = vector

    case parameters.remove('kernelIndex') of
      0 : vector = [0d]
      1 : begin
        v1 = parameters.remove('minG')
        v2 = parameters.remove('maxG')
        v3 = parameters.remove('multiplierG')
        exp1 = double(round(alog10(v1)/alog10(v3)))
        exp2 = double(round(alog10(v2)/alog10(v3)))
        vector = v3^(dindgen(exp2-exp1+1)+exp1)
        if parameters.remove('testLinearKernel') then vector = [0d,vector]
      end
    endcase
    parameters['gArray'] = vector

    parameters['featureFilename'] = parameters.remove('sampleSet_featureFilename')
    parameters['labelFilename'] = parameters.remove('sampleSet_labelFilename')
    parameters['spectralSubset'] = parameters.remove('sampleSet_spectralSubset')
    
    parameters['svmType'] = svmType
    if svmType eq 'svr' then begin
      if parameters.remove('lossSelection') eq 1 then begin
        parameters['loss'] = parameters.remove('lossUser')
      endif
    endif
    if svmType eq 'svc' then begin
      if parameters.remove('performanceSelection') eq 1 then begin
        fractions = fractionList[parameters.remove('svcQuantFractions')]
        parameters['svcQuant'] = 1
        parameters['svcQuantFractions'] = float(strsplit(fractions,/EXTRACT,'% '))/100.
      endif
    endif


  endif else begin
    parameters = !NULL
  endelse

  return, parameters
end

pro test_imageSVM_parameterize_getParameters

  svmType = 'svc'
  parameters = imageSVM_parameterize_getParameters(svmType);, Title='Support Vector Regression')
  print,parameters

end
