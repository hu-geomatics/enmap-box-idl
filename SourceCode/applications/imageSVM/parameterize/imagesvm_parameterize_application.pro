; :Params:
;    svmType: in, required, type=['svc' | 'svr']
;      Use this argument to specify the svm type::
;        
;        'svc' for svm classification
;        'svr' for svm regression
;           
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro imageSVM_parameterize_application, svmType, Title=title, GroupLeader=groupLeader, FollowedByApply=followedByApply, FollowedByFeatureSelection=followedByFeatureSelection

  parameters = imageSVM_parameterize_getParameters(svmType, Title=title, GroupLeader=groupLeader)
  if isa(parameters) then begin
    imageSVM_parameterize_processing, parameters, Title=title, GroupLeader=groupLeader
    hub_setAppState, 'imageSVM', parameters['svmType']+'ModelFilename', parameters['modelFilename']
    hub_setAppState, 'imageSVM', parameters['svmType']+'FeatureFilename', parameters['featureFilename']
    hub_setAppState, 'imageSVM', parameters['svmType']+'LabelFilename', parameters['labelFilename'] 
    imageSVM_viewModel_processing, parameters, GroupLeader=groupLeader, Title=title
  
    if keyword_set(followedByApply) then begin   
      ok = dialog_message('Do you want to apply the model to an image?', /QUESTION, TITLE=title)
      if ok eq 'Yes' then begin
        title = svmType eq 'svr' ? 'Apply SVR to Image' : 'Apply SVC to Image'
        imageSVM_apply_application, svmType, Title=title, GroupLeader=groupLeader
      endif
    endif
    
    if keyword_set(followedByFeatureSelection) then begin
      ok = dialog_message('Do you want to perform a feature selection?', /QUESTION, TITLE=title)
      if ok eq 'Yes' then begin
        
        imageSVM_featureSelection_application, svmType, Title=title, GroupLeader=groupLeader
      endif
    endif

  endif 
end
