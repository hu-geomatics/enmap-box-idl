pro imageSVM_parameterize_event, event
  
  @huberrorcatch
  
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  svmType = menuEventInfo.argument
  imageSVM_parameterize_application, svmType, GroupLeader=event.top, Title=menuEventInfo.name, /FollowedByApply 
  
end
