;+
; :Description:
;    Use this procedure to parameterize a SVM model via grid search and cross validation.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        keys               | type     | description
;        -------------------+----------+--------------------------------------------------
;        featureFilename    | string   | file path of feature image
;        labelFilename      | string   | file path of label image
;        modelFilename      | string   | file path of created SVM model
;        svmType            | string   | 'svc' for classification or 'svr' for regression
;        
;        optional keys      | type     | description
;        -------------------+----------+--------------------------------------------------
;        cArray             | number[] | c values for grid search, default is:
;                           |          | [0.1, 1, 10, 100, 1000]
;        gArray             | number[] | g values for grid search, default is:
;                           |          | [0.01, 0.1, 1, 10, 100, 1000]
;        loss               | number   | epsilon insensitive loss function parameter
;                           |          | if not defined, a search heuristic is applied (Rabe et al., Simplifying Support Vector Regression Parameterisation by Heuristic Search for Optimal Epsilon-Loss)
;        stopGridSearch     | number   | stopping criterion for grid search, default is 0.1
;        stopTraining       | number   | stopping criterion for final training, default is 0.01
;        numberOfFolds      | number   | number of folds used for cross validation, default is 3
;        spectralSubset     | number[] | array of band indices used for training, default is to use all bands
;        svcQuant           | boolean  | if set, use SVCQuant inside grid search
;        svcQuantFractions  | float[]  | array of possible mixing fractions, e.g. [0.25, 0.5, 0.75] 
;        svcQuantSamples    | innteger | number randomly drawn SVCQuant test samples
;        -------------------+----------+-------------------------------------------------- 
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro imageSVM_parameterize_processing, parameters, Title=title, GroupLeader=groupLeader

  required = ['modelFilename','featureFilename','labelFilename','svmType']
  
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
 
  defaultCArray = [0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
  defaultGArray = [0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
  defaultNumberOfFolds = 3
  defaultStopGridSearch = 0.1
  defaultStopTraining = 0.01
  
  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader)
  progressBar.setInfo, 'load data'

  svc = parameters['svmType'] eq 'svc'
  svr = parameters['svmType'] eq 'svr'

  ; perform grid search
  sampleSet = imageSVMSampleSet(SVC=svc, SVR=svr, Features=parameters['featureFilename'], Labels=parameters['labelFilename'], /File)

  if parameters.hubIsa('spectralSubset') then begin
    sampleSet.setSelectedFeatures, parameters['spectralSubset']
  endif 
  parameter = imageSVMTrainingParameter(SVC=svc, SVR=svr, Loss=parameters.hubGetValue('loss'), Stop=parameters.hubGetValue('stopGridSearch', Default=defaultStopGridSearch))
  gridSearchResult = imageSVM_gridSearch(sampleSet, parameter, ProgressBar=progressBar,$
    parameters.hubGetValue('cArray', Default=defaultCArray),$
    parameters.hubGetValue('gArray', Default=defaultGArray),$
    parameters.hubGetValue('loss', Default=defaultLoss),$
    NumberOfFolds=parameters.hubGetValue('numberOfFolds', Default=defaultNumberOfFolds),$
    SVCQuant=parameters.hubGetValue('svcQuant'), QuantFractions=parameters.hubGetValue('svcQuantFractions'), QuantSamples=parameters.hubGetValue('svcQuantSamples'))
  
  ; find best parameter combination
  case parameters['svmType'] of
    'svc' : if parameters.hubKeywordSet('svcQuant') then begin
              bestParameterCombination = gridSearchResult.getBestParameterCombination('meanAbsoluteError')
            endif else begin
              bestParameterCombination = gridSearchResult.getBestParameterCombination('averageF1Accuracy')
            endelse
    'svr' : bestParameterCombination = gridSearchResult.getBestParameterCombination('meanAbsoluteError')
  endcase

  ; train final model
  parameter.setC, bestParameterCombination['c']
  parameter.setG, bestParameterCombination['g']
  parameter.setStop, parameters.hubGetValue('stopTraining', Default=defaultStopTraining)
  if parameters['svmType'] eq 'svr' then begin
    parameter.setLoss, bestParameterCombination['loss']
  endif
  
  model = imageSVM_training(sampleSet, parameter, ProgressBar=progressBar, SVCQuant=parameters.hubGetValue('svcQuant'))

  imageSVM_saveModel, parameters['modelFilename'], model, gridSearchResult, bestParameterCombination
  progressBar.cleanup

end

pro test_imageSVM_parameterize_processing, parameters
  enmapbox
  parameters = hash()
  svmType = 'svr'
  parameters['svmType'] = svmType
  case svmType of
    'svc' : begin
      parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
      parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
    end
    'svr' : begin
      parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
      parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
    end
  endcase
  parameters['modelFilename'] = hub_getUserTemp('model.'+svmType)
;  parameters['cArray'] = [1,10,100,1000]
;  parameters['gArray'] = [0.]
;  parameters['loss'] = 0.0001
;  parameters['spectralSubset'] = [0]
  imageSVM_parameterize_processing, parameters
  imageSVM_viewModel_processing, parameters

return  
end

pro test_imageSVM_parameterize_processing_SVCQuant, parameters
  ;enmapbox
  parameters = hash()
  svmType = 'svc'
  parameters['svmType'] = svmType
  parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  parameters['modelFilename'] = hub_getUserTemp('model.'+svmType)
;  parameters['cArray'] = [100]
;  parameters['gArray'] = [0.01]
  parameters['svcQuant'] = 1b
  parameters['svcQuantFractions'] = [0.25,0.5,0.75]
  parameters['svcQuantSamples'] = 100

  imageSVM_parameterize_processing, parameters
  imageSVM_viewModel_processing, parameters

  
end

