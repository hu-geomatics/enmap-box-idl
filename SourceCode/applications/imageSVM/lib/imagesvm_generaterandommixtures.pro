function imageSVM_generateRandomMixtures, features, labels, nrandom, possibleFractions

  ; calc histogram
  nclasses = max(labels)
  histo = histogram(labels, MIN=1, MAX=nclasses,REVERSE_INDICES=reverse_indices )
  indices = hubHelper.getHistogramLocationIndices(reverse_indices, nclasses)
  
  ; draw random class combinations
  pairs = intarr(2,nclasses*(nclasses-1)/2)
  pair = 0
  for i=0,nclasses-2 do for j=i+1,nclasses-1 do pairs[0,pair++] = [i,j]+1
  randomPairs = pairs[*, randomu(seed, nrandom, /LONG) mod n_elements(pairs)]

  bands = n_elements(features[*,0])
  featuresA = fltarr(bands,nrandom, /NOZERO)
  featuresB = fltarr(bands,nrandom, /NOZERO)
  for class=1,nclasses do begin
    ; first class
    classIndices = where(/NULL, randomPairs[0,*] eq class)
    if isa(classIndices) then begin
      randomA = randomu(seed, n_elements(classIndices), /LONG) mod n_elements(indices[class-1])
      featuresA[*,classIndices] = features[*,(indices[class-1])[randomA]]
    endif

    ; second class
    classIndices = where(/NULL, randomPairs[1,*] eq class)
    if isa(classIndices) then begin
      randomB = randomu(seed, n_elements(classIndices), /LONG) mod n_elements(indices[class-1])
      featuresB[*,classIndices] = features[*,(indices[class-1])[randomB]]
    endif
  endfor
  
  ;possibleFractions = [0.25,0.75]
  fractions = possibleFractions[randomu(seed, nrandom, /LONG) mod n_elements(possibleFractions)]
  fractionsGrid = rebin(transpose(fractions), bands,nrandom)
  mixedFeatures = featuresA*fractionsGrid + featuresB*(1.-fractionsGrid)
  mixedFractions = fltarr(nclasses,nrandom)
  mixedFractions[reform(randomPairs[0,*]-1), [0:nrandom-1]] = fractions
  mixedFractions[reform(randomPairs[1,*]-1), [0:nrandom-1]] = 1.-fractions

  
  
  return, {fractions : mixedFractions, features : mixedFeatures}
end

pro test_imageSVM_generateRandomMixtures

end