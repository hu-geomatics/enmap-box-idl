function imageSVMLibSVM::init

  ; create only a single instance of the libsvm java object
  idljb_imagesvm = hub_getAppState('imageSVM', 'idljb_imagesvm')
  if obj_valid(idljb_imagesvm) then begin
    self.idljb_imagesvm = idljb_imagesvm
  endif else begin
    enmapJavaBridge_init
    self.idljb_imagesvm = obj_new("IDLJavaObject$IDLJB_IMAGESVM",'idljb_imagesvm')
    hub_setAppState, 'imageSVM', 'idljb_imagesvm', self.idljb_imagesvm
  endelse
  
  return,1b
end

function imageSVMLibSVM::trainModel
  self.idljb_imagesvm.train_model
  libsvmModel = self.idljb_imagesvm.get_model()
  numberOfClasses = libsvmModel.get_nr_class()
  numberOfTotalSV = libsvmModel.get_l()

  emptyModel = numberOfTotalSV eq 0

  if self.imageSVMSampleSet.isSVC() then begin
    if emptyModel then begin
      numberOfClassSV = replicate(0, numberOfClasses)
    endif else begin
      numberOfClassSV = libsvmModel.get_nsv()
    endelse
    numberOfClassSV_ = numberOfClassSV
  endif
  if self.imageSVMSampleSet.isSVR() then begin
    numberOfClassSV = numberOfTotalSV
    numberOfClassSV_ = [numberOfTotalSV,0]
  endif

  if emptyModel then begin
    imageSVMAy = []
    sv = []
  endif else begin
    libsvmAy = libsvmModel.get_sv_coef()
    imageSVMAy = self._convertAy(numberOfClasses, numberOfTotalSV, numberOfClassSV_, libsvmAy)
    sv = float(libsvmModel.get_sv())
    sv = transpose(sv)
    !null = reform(/OVERWRITE, sv, n_elements(sv[*,0]),n_elements(sv[0,*]))
  endelse

  result = hash()
  result['rho'] = libsvmModel.get_rho()
  result['proba'] = libsvmModel.get_proba()
  result['probb'] = libsvmModel.get_probb()
  result['numberOfClasses'] = numberOfClasses
  result['numberOfTotalSV'] = numberOfTotalSV
  result['numberOfClassSV'] = numberOfClassSV
  result['ay'] = imageSVMAy
  result['sv'] = sv

  return, result
end

function imageSVMLibSVM::applyCrossValidation
  result = self.idljb_imagesvm->do_cross_validation()
  if product(finite(result)) ne 1 then begin
    message, 'Unexpected error: LIBSVMs cross-validation prediction contains NaN values.' 
  endif
  return, result
end

function imageSVMLibSVM::_convertAy $
  ,ncl, nsv_total, nsv, sv_coef
  
;  ncl=self.svm_model.ncl
;  nsv=*self.svm_model.nsv
;  nsv_total = self.svm_model.nsv_total
  nrule = ncl*(ncl-1)/2
  nsv_cum = nsv
  for i=1UL,ncl-1 do nsv_cum[i] += nsv_cum[i-1]
   
  sv_start = [0UL,nsv_cum[0:ncl-2]]
  sv_end   = nsv_cum-1
  
  dummy1 = hub_fix2d(ulonarr(ncl-1,ncl))
  dummy2 = hub_fix2d(ulonarr(ncl-1,ncl))
  dummy3 = hub_fix2d(ulonarr(ncl-1,ncl))

  for class=1,ncl do for icl=1UL,ncl-1 do begin
    if icl le class then dummy1[icl-1,class-1]=icl else dummy1[icl-1,class-1]=class
  endfor 
      
  for class=1,ncl do for icl=2UL,ncl do begin
    if icl ge class then dummy2[icl-2,class-1]=icl else dummy2[icl-2,class-1]=class
  endfor 
      
  ay = hub_fix2d(fltarr(nrule,nsv_total))
  i_rule = 0UL

  for cl1=1,ncl-1 do for cl2=cl1+1,ncl do begin
    dummy3[where(dummy1 eq cl1 and dummy2 eq cl2)] = i_rule
    i_rule++
  endfor
   
  for i_rule=0UL,nrule-1 do begin
    tmp = where(dummy3 eq i_rule)
    row = tmp mod (ncl-1)
    column = tmp / (ncl-1)
    
    npos = sv_end[column[0]] - sv_start[column[0]] +1
    nneg = sv_end[column[1]] - sv_start[column[1]] +1
    
    if npos ge 1 then begin
      pos_sv = sv_coef[row[0],sv_start[column[0]]:sv_end[column[0]]]
      ay[i_rule,sv_start[column[0]]:sv_end[column[0]]]=pos_sv
    endif
    if nneg ge 1 then begin
      neg_sv = sv_coef[row[1],sv_start[column[1]]:sv_end[column[1]]]
      ay[i_rule,sv_start[column[1]]:sv_end[column[1]]]=neg_sv
    endif
  endfor
  return, ay
end

pro imageSVMLibSVM::setImageSVMSampleSet, imageSVMSampleSet
  self.imageSVMSampleSet = imageSVMSampleSet
  
  standardizedFeatures = imageSVMSampleSet.getFeatures(/Subset,/Scale)
  standardizedFeatures = hub_fix2d(transpose(standardizedFeatures))
  labels = imageSVMSampleSet.getLabels()
  self.idljb_imagesvm.set_problem, standardizedFeatures, labels
end

pro imageSVMLibSVM::setImageSVMTrainingParameter, imageSVMTrainingParameter
  self.setDefaultParameter
  (self.parameters)['s'] = imageSVMTrainingParameter.getType(/Code)
  (self.parameters)['c'] = float(imageSVMTrainingParameter.getC())
  (self.parameters)['g'] = float(imageSVMTrainingParameter.getG())
  if imageSVMTrainingParameter.isSVR() then begin
    (self.parameters)['p'] = float(imageSVMTrainingParameter.getLoss())
    (self.parameters)['b'] = 0
  endif
  (self.parameters)['e'] = float(imageSVMTrainingParameter.getStop())

  if (self.parameters)['g'] eq 0. then (self.parameters)['t'] = 0 ; linear kernel
  self._javaSetParameter
end

pro imageSVMLibSVM::setNumberOfFolds, numberOfFolds
  (self.parameters)['v'] = fix(numberOfFolds)
  self._javaSetParameter
end

pro imageSVMLibSVM::setDefaultParameter
  self.parameters = hash()
  (self.parameters)['s'] = 0 
  (self.parameters)['t'] = 2 ; RBF kernel
  (self.parameters)['d'] = 3 ; not used
  (self.parameters)['g'] = 0 
  (self.parameters)['r'] = 0 ; not used
  (self.parameters)['c'] = 1
  (self.parameters)['n'] = 0.5 ; not used
  (self.parameters)['p'] = 0.1 
  (self.parameters)['m'] = 100 ; always 100MB
  (self.parameters)['e'] = 0.001
  (self.parameters)['h'] = 1 ; always use shrinking heuristic
  (self.parameters)['b'] = 1 
  (self.parameters)['v'] = 3
  (self.parameters)['q'] = 1 ; always quite mode
end

pro imageSVMLibSVM::getProperty, Parameters=parameters
  parameters = self.parameters
end

pro imageSVMLibSVM::_javaSetParameter
  self.idljb_imagesvm->set_parameter $
    ,(self.parameters)['s'] $
    ,(self.parameters)['t'] $
    ,(self.parameters)['d'] $
    ,(self.parameters)['g'] $
    ,(self.parameters)['r'] $
    ,(self.parameters)['c'] $
    ,(self.parameters)['n'] $
    ,(self.parameters)['p'] $
    ,(self.parameters)['m'] $
    ,(self.parameters)['e'] $
    ,(self.parameters)['h'] $
    ,(self.parameters)['b'] $
    ,(self.parameters)['v'] $
    ,(self.parameters)['q']
end

pro imageSVMLibSVM__define
  struct = {imageSVMLibSVM, inherits IDL_Object, $
    idljb_imagesvm : obj_new(), $
    parameters : hash(), $
    imageSVMSampleSet : obj_new() $
    } 
end