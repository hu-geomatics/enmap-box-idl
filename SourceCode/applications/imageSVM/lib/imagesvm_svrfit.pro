function imageSVM_svrFit, x, y, c, g, loss, stop, libsvmObj
  sampleSet = imageSVMSampleSet(/SVR, Features=x, Labels=y)
  parameter = imageSVMTrainingParameter(/SVR, C=c, G=g, Loss=loss, Stop=stop)
  model = imageSVM_training(sampleSet, parameter, UsedImageSVMLibSVM=libsvmObj)
  
  return, model
end

pro test_imageSVM_svrFit
  compile_opt idl2

  x = randomu(seed, 100)
  y = sin(x*6)+randomu(seed, 100)*0.3
  !null = plot(x,y, SYMBOL=4, LINESTYLE=6)
  c = 100 & g = 1 & loss = 0 & stop = 0.001
  svr = imageSVM_svrFit(transpose(x), y, c, g, loss, stop, libsvmObj)
  
  xGrid = [0:1:0.01]
  yGrid = (svr.apply(transpose(xGrid))).labels
  !null = plot(/OVERPLOT, xGrid, yGrid)
  
end