
function imagesvm_binaryProbabilitiesToClassProbabilities, binaryProbabilities, numberOfClasses

  nrule  = n_elements(binaryProbabilities)
  numberOfClasses = ulong(0.5+sqrt(0.25+2*nrule))
  r = fltarr(numberOfClasses, numberOfClasses)
  p = replicate(1./numberOfClasses, numberOfClasses)
  q = fltarr(numberOfClasses, numberOfClasses)

  ;prepare r matrix
  i_rule = 0UL
  for cl1=0,numberOfClasses-2 do for cl2=cl1+1,numberOfClasses-1 do begin
    r[cl1,cl2] = binaryProbabilities[i_rule]
    r[cl2,cl1] = 1.-binaryProbabilities[i_rule]
    i_rule++
  endfor

  ;prepare Q matrix
  for cl1=0,numberOfClasses-1 do for cl2=0,numberOfClasses-1 do begin
    if cl1 eq cl2 then begin
      for s=0,numberOfClasses-1 do if s ne cl1 then q[cl1,cl2] += r[s,cl1]^2 
    endif else begin
      q[cl1,cl2] = -1.*r[cl1,cl2]*r[cl2,cl1]
    endelse
  endfor

  ;numeric optimization
  convergenceCriterion = 0.001/numberOfClasses
  count = 0
  repeat begin  
    for t=0,numberOfClasses-1 do begin
      tmp1 = 0.
      for j=0,numberOfClasses-1 do if j ne t then tmp1 += q[t,j]*p[j]
      tmp2 = p##q
      tmp2 = total(tmp2*p)
      p_t = 1./q[t,t] *(-1.*tmp1 + tmp2)
      if ~finite(p_t) then continue
      p[t] = p_t
      p /= total(p)
    endfor
    
  ;check stopping criterion
    tmp2 = p##q
    tmp2 = total(tmp2*p)
    tmp1 = q##p
    max_diff = max(tmp1-tmp2, /NAN)
    ;print, max_diff
    converged = ~finite(max_diff) || max_diff lt convergenceCriterion

  endrep until converged or (count++ gt 100) 
  return,p
end


;test input       [0.933002,0.771716 ,0.212695], 
;expected output  [0.729929,0.0549997,0.215072]