function imageSVMTrainingParameter::init,$
  SVC=svc, SVR=svr, C=c, G=g, Loss=loss, Stop=stop

  self.parameters = hash()
    
  if ~hub_isExclusiveKeyword(svc,svr) then begin
    message, 'Set one of the keywords: SVC, SVR.
  endif

  case 1 of 
    keyword_set(svc) : self.setType, 'svc'
    keyword_set(svr) : self.setType, 'svr'
  endcase

  self.setC, c
  self.setG, g
  self.setLoss, loss
  self.setStop, stop
  return,1b
end

pro imageSVMTrainingParameter::setType, value, Code=code
  if keyword_set(code) then begin
    (self.parameters)['s'] = value
  endif else begin
    case value of
      'svc' : (self.parameters)['s'] = 0
      'svr' : (self.parameters)['s'] = 3
    endcase
  endelse
end

function imageSVMTrainingParameter::getType, Code=code
  if keyword_set(code) then begin
    result = (self.parameters)['s']
  endif else begin
    case (self.parameters)['s'] of
      0 : result = 'svc'
      3 : result = 'svr'
    endcase
  endelse
  return, result
end

pro imageSVMTrainingParameter::setC, value
  (self.parameters)['c'] = isa(value) ? float(value) : !null
end

function imageSVMTrainingParameter::getC
  if ~self.parameters.hubIsa('c') then message, 'Parameter c is undefined.' 
  return, (self.parameters)['c']
end

pro imageSVMTrainingParameter::setG, value
  (self.parameters)['g'] = isa(value) ? float(value) : !null
end

function imageSVMTrainingParameter::getG
  if ~self.parameters.hubIsa('g') then message, 'Parameter g is undefined.'
  return, (self.parameters)['g']
end

pro imageSVMTrainingParameter::setLoss, value
  (self.parameters)['p'] = isa(value) ? float(value) : !null
end

function imageSVMTrainingParameter::getLoss
  if ~self.parameters.hubIsa('p') then message, 'Parameter loss is undefined.'
  return, self.parameters.hubGetValue('p')
end

pro imageSVMTrainingParameter::setStop, value
  (self.parameters)['e'] = isa(value) ? float(value) : !null
end

function imageSVMTrainingParameter::getStop
  if ~self.parameters.hubIsa('e') then message, 'Parameter stop is undefined.'
  return, (self.parameters)['e']
end

function imageSVMTrainingParameter::isSVC
  return, self.getType() eq 'svc'
end

function imageSVMTrainingParameter::isSVR
  return, self.getType() eq 'svr'
end

function imageSVMTrainingParameter::copy
  result = imageSVMTrainingParameter(SVC=self.isSVC(), SVR=self.isSVR(), $
    C=self.getC(), G=self.getG(), Stop=self.getStop())
  if self.isSVR() then begin
    result.setLoss, self.getLoss()
  endif 
  return, result
end

function imageSVMTrainingParameter::_overloadPrint
  result = list()
  result.add, 'Type: '+self.getType()
  result.add, 'c:   '+strcompress(self.getC())
  result.add, 'g:   '+strcompress(self.getG())
  if self.isSVR() then begin
    result.add, 'loss:'+strcompress(self.getLoss())
  endif
  result.add, 'stop:'+strcompress(self.getStop())
  result = transpose(result.toArray())
  return, result
end

pro imageSVMTrainingParameter__define
  struct = {imageSVMTrainingParameter, inherits IDL_Object,$
    parameters : hash() $
    } 
end

pro test_imageSVMTrainingParameter
  parameter = imageSVMTrainingParameter(/SVC, C=10,G=1)
  parameter.setC, 12
  print, parameter
end