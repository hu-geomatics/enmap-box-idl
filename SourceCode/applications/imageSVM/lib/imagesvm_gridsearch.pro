function imageSVM_gridSearch, imageSVMSampleSet, imageSVMTrainingParameter,$
  cArray, gArray, loss, NumberOfFolds=numberOfFolds,$
  ProgressBar=progressBar, SVCQuant=svcQuant, QuantFractions=svcQuantFractions, QuantSamples=svcQuantSamples 

  progressBar.setInfo, 'grid search for C and g parameter'

  enmapJavaBridge_init

  if ~isa(imageSVMTrainingParameter, 'imageSVMTrainingParameter') then begin
    message, 'Wrong argument type: imageSVMTrainingParameter.'
  endif

  if ~isa(imageSVMSampleSet, 'imageSVMSampleSet') then begin
    message, 'Wrong argument type: imageSVMSampleSet.'
  endif
  
  if ~isa(cArray, /ARRAY) then begin
    message, 'Argument cArray must be an array of numbers.'
  endif

  if ~isa(gArray, /ARRAY) then begin
    message, 'Argument gArray must be an array of numbers.'
  endif
  factor = 0.6166
  if imageSVMSampleSet.isSVR() then begin
    if isa(loss) then begin
      tuneLoss = 0b
      imageSVMTrainingParameter.setLoss, loss
    endif else begin
      tuneLoss = 1b
      currentLoss = stddev(imageSVMSampleSet.getLabels())*factor
    endelse
  endif else begin
    tuneLoss = 0b
  endelse
  
  gridPerformance = objarr(n_elements(cArray), n_elements(gArray))
  progressBar.setRange, [0,n_elements(gridPerformance)-1]
  
;  print,'     c           g               iter.   loss            newLoss              stddev'
  
  ; create svcQuant validation data
  if keyword_set(svcQuant) then begin
    ; draw random mixtures
    trainingFeatures = imageSVMSampleSet.getFeatures(/Subset)
    trainingLabels = imageSVMSampleSet.getLabels()
    mixedSample = imageSVM_generateRandomMixtures(trainingFeatures, trainingLabels, svcQuantSamples, svcQuantFractions)
  endif
  
  foreach g,gArray,gIndex  do begin
    foreach c,cArray,cIndex do begin
      progressBar.setProgress, cIndex+n_elements(cArray)*gIndex
      imageSVMTrainingParameter.setC, c
      imageSVMTrainingParameter.setG, g
      if tuneLoss then begin
        ; tune epsilon loss (max. iterations is 10; terminats if change is under 5%)
        lossTested = []
        for i=1,10 do begin
          lossTested = [lossTested, currentLoss]
          imageSVMTrainingParameter.setLoss, currentLoss
          crossValidationPerformance = imageSVM_crossValidation(imageSVMSampleSet, imageSVMTrainingParameter, NumberOfFolds=numberOfFolds, UsedImageSVMLibSVM=usedImageSVMLibSVM)
          oldLoss = currentLoss
          newLoss = crossValidationPerformance['residualsStandardDeviation']*factor
;          print, c,g,i,double(currentLoss), double(newLoss), crossValidationPerformance['residualsStandardDeviation']
          if 1.-(min([oldLoss,newLoss])/max([oldLoss,newLoss])) le 0.05 then begin
            break
          endif else begin
            currentLoss = newLoss
          endelse
        endfor
        crossValidationPerformance['lossTested'] = lossTested
      endif else begin
        if keyword_set(svcQuant) then begin
          model = imageSVM_training(imageSVMSampleSet, imageSVMTrainingParameter, UsedImageSVMLibSVM=usedImageSVMLibSVM)
          svcQuantEstimation = model.apply(mixedSample.features)
          crossValidationPerformance = hubMathRegressionPerformance((mixedSample.fractions)[*], (svcQuantEstimation['classProbabilities'])[*])
        endif else begin
          crossValidationPerformance = imageSVM_crossValidation(imageSVMSampleSet, imageSVMTrainingParameter, NumberOfFolds=numberOfFolds, UsedImageSVMLibSVM=usedImageSVMLibSVM)
          if imageSVMSampleSet.isSVR() then begin
            crossValidationPerformance['lossTested'] = imageSVMTrainingParameter.getLoss()
          endif
        endelse
      endelse
      crossValidationPerformance['cIndex'] = cIndex
      crossValidationPerformance['gIndex'] = gIndex
      gridPerformance[cIndex, gIndex] = crossValidationPerformance
    endforeach
  endforeach

  gridSearchResult = hash()
  gridSearchResult['cArray'] = float(cArray)
  gridSearchResult['gArray'] = float(gArray)
  gridSearchResult['gridPerformance'] = gridPerformance
  gridSearchResult['numberOfFolds'] = fix(numberOfFolds)
  if imageSVMSampleSet.isSVR() then begin
    gridSearchResult['loss'] = usedImageSVMLibSVM.parameters.hubGetValue('p')
    gridSearchResult['userdefinedLoss'] = ~tuneLoss
  endif
  gridSearchResult['stop'] = usedImageSVMLibSVM.parameters.hubGetValue('e')
  gridSearchResult['svmType'] = imageSVMSampleSet.getType()
  result = imageSVMGridSearchResult(gridSearchResult)
  return, result

end