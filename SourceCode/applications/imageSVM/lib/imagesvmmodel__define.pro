function imageSVMModel::init, libSVMOutput, imageSVMSampleSet, imageSVMTrainingParameter, SVCQuant=svcQuant
  self.libSVMOutput = libSVMOutput
  self.imageSVMSampleSet = imageSVMSampleSet
  self.imageSVMTrainingParameter = imageSVMTrainingParameter
  self.svcQuant = keyword_set(svcQuant)
  self.compressLinearModel
  
  return,1b
end

pro imageSVMModel::compressLinearModel
  if self.imageSVMTrainingParameter.getG() ne 0. then return
  if (nsv=(self.libSVMOutput)['numberOfTotalSV']) eq 0 then return
  ay = self.libSVMOutput['ay']
  sv = self.libSVMOutput['sv']
  numberOfBinarySVM =n_elements(ay[*,0])
  w = hub_fix2d(make_array(self.imageSVMSampleSet.getNumberOfFeatures(), numberOfBinarySVM))
  for i=0,numberOfBinarySVM-1 do begin
    i_ay = ay[i,*]
    i_w = replicate(0., self.imageSVMSampleSet.getNumberOfFeatures())
    for k=0,nsv-1 do begin
      i_w += i_ay[k]*sv[*,k]
    endfor
    w[*,i] = i_w
  endfor
  self.libSVMOutput['w'] = w
end

pro imageSVMModel::getProperty, $
  LibSVMOutput=libSVMOutput,$
  ImageSVMSampleSet=imageSVMSampleSet, $
  ImageSVMTrainingParameter=imageSVMTrainingParameter, $
  SVCQuant=svcQuant
  
  if arg_present(imageSVMSampleSet) then begin
     imageSVMSampleSet = self.imageSVMSampleSet
  endif
  
  if arg_present(imageSVMTrainingParameter) then begin
     imageSVMTrainingParameter = self.imageSVMTrainingParameter
  endif
  
  if arg_present(libSVMOutput) then begin
     libSVMOutput = self.libSVMOutput
  endif
  
  if arg_present(svcQuant) then begin
    svcQuant = self.svcQuant
  endif
end

function imageSVMModel::getNumberOfSupportVectors
  return, (self.libSVMOutput)['numberOfTotalSV']
end

function imageSVMModel::apply, features

  ; handle !null
  if ~isa(features) then begin
    if self.imageSVMTrainingParameter.isSVR() then begin
      result = dictionary('labels', !null)
    endif
    
    if self.imageSVMTrainingParameter.isSVC() then begin
      result = dictionary($
        'binaryDecisionValues', !null,$
        'binaryProbabilities', !null,$
        'classProbabilities', !null,$
        'labels', !null)
    endif
    return, result
  endif

  if size(/N_DIMENSIONS, features) ne 2 then begin
    message, 'Argument features must be a two dimensional array.'
  endif

  numberOfFeatures = n_elements(features[*,0])
  selectedFeatures = self.imageSVMSampleSet.getSelectedFeatures()
  numberOfSelectedFeatures = n_elements(selectedFeatures)
  
  if self.imageSVMSampleSet.getNumberOfFeatures() ne numberOfFeatures then begin
    message, 'Number of features do not match.'
  endif
  numberOfSamples = n_elements(features[0,*])

  emptyModel = (self.libSVMOutput)['numberOfTotalSV'] eq 0

  ;prepare variables
  b = -(self.libSVMOutput)['rho']
  numberOfClasses = (self.libSVMOutput)['numberOfClasses']
  numberOfBinarySVM = numberOfClasses*(numberOfClasses-1)/2
  numberOfTotalSV = (self.libSVMOutput)['numberOfTotalSV']
;numberOfTotalSV = 10000

  if ~emptyModel then begin
    standardizedFeatures = self.imageSVMSampleSet.scaleFeaturesForward(features)
    standardizedFeatures = standardizedFeatures[selectedFeatures,*]
    x = hub_fix2d(standardizedFeatures)
    sv = hub_fix2d((self.libSVMOutput)['sv'])
    ay = hub_fix2d((self.libSVMOutput)['ay'])
    g = self.imageSVMTrainingParameter.getG()
    isLinearKernel = g eq 0.
    if isLinearKernel then begin
      w = hub_fix2d((self.libSVMOutput)['w'])
    endif else begin
      ; kernel matrix - kernel(x,sv_i) = exp(-gamma*|x-sv_i|^2)
      kernelMatrix = hub_fix2d(fltarr(numberOfSamples, numberOfTotalSV, /NOZERO))
      hubMath_mathErrorsOff
      for i=0, numberOfTotalSV-1 do begin
        isv = sv[*,i]
        ;replicate SV
        tmp = rebin(isv, /SAMPLE, numberOfSelectedFeatures, numberOfSamples)
        ;calculate: x-sv
        tmp = x-tmp
        ;calculate: exp(-gamma*|x-sv|^2)
        kernelMatrix[0,i] = exp(-g*total(hub_fix2d(tmp^2), 1))
      endfor
    endelse
  endif

  ; binary decision values; f(x) = sum[ ay_i * kernel(x,sv_i) ] + b
  binaryDecisionValues = hub_fix2d(make_array(numberOfSamples, numberOfBinarySVM, /NOZERO))

  for iBinary=0,numberOfBinarySVM-1 do begin
    ;add bias b
    binaryDecisionValues[*,iBinary] = b[iBinary]
    ;add sum
    if ~emptyModel then begin
      
      if isLinearKernel then begin
        wReplicated = rebin(w[*,iBinary], /SAMPLE, numberOfSelectedFeatures, numberOfSamples)
        binaryDecisionValues[0,iBinary] += total(hub_fix2d(wReplicated*x), 1)
      endif else begin
        binarySVIndices =  where(/NULL, ay[iBinary,*] ne 0., numberOfBinarySV)
        if isa(binarySVIndices) then begin
          iAy = hub_fix2d(rebin(/SAMPLE, ay[iBinary,binarySVIndices], numberOfSamples, numberOfBinarySV))
          iKernel = hub_fix2d(kernelMatrix[*,binarySVIndices])
          binaryDecisionValues[0,iBinary] += total(temporary(iAy)*temporary(iKernel), 2)
        endif  
      endelse
    endif
  endfor  
  hubMath_mathErrorsOn
  
  ; regression estimate
  if self.imageSVMTrainingParameter.isSVR() then begin
    result = dictionary(/NO_COPY, 'labels', binaryDecisionValues)
  endif

  ; classification estimate
  if self.imageSVMTrainingParameter.isSVC() then begin
    ; pair-wise probability
    binaryProbabilities = make_array(size(/DIMENSIONS, binaryDecisionValues), /NOZERO)
    proba = (self.libSVMOutput)['proba']
    probb = (self.libSVMOutput)['probb']
    for iBinary=0L,numberOfBinarySVM-1 do begin
      binaryProbabilities[0,iBinary] = 1. / (1.+exp(proba[iBinary]*binaryDecisionValues[*,iBinary] + probb[iBinary]))
    endfor
  
    ; class-wise probability
    classProbabilities = make_array(numberOfClasses, numberOfSamples, /NOZERO)
    for i=0,numberOfSamples-1 do begin
      classProbabilities[*,i] = imagesvm_binaryProbabilitiesToClassProbabilities(binaryProbabilities[i,*], numberOfClasses)
    endfor
    
    ; class label 
    !null = max(classProbabilities, max_subscript, DIMENSION=1)
    labels = byte((max_subscript mod numberOfClasses)+1)

    result = dictionary(/NO_COPY,$
      'binaryDecisionValues', binaryDecisionValues,$
      'binaryProbabilities', binaryProbabilities,$
      'classProbabilities', classProbabilities,$
      'labels', labels)

  endif
  return, result
end

function imageSVMModel::_overloadPrint
  result = list()
  result.add, self.imageSVMSampleSet._overloadPrint(), /Extract
  result.add, self.imageSVMTrainingParameter._overloadPrint(), /Extract
  result.add, self.libSVMOutput._overloadPrint(), /Extract
  result = transpose(result.toArray())
  return, result
end

pro imageSVMModel__define
  struct = {imageSVMModel, inherits IDL_Object, $
    libSVMOutput : hash(), $
    imageSVMSampleSet : obj_new(), $
    imageSVMTrainingParameter : obj_new(), $
    svcQuant:0b $
  } 
end