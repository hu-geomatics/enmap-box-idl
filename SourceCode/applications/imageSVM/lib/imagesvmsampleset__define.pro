function imageSVMSampleSet::init,$
  SVC=svc, SVR=svr,$
  Features=features, Labels=labels, DataIgnoreValue=dataIgnoreValue, File=file
  
  if ~hub_isExclusiveKeyword(svc,svr) then begin
    message, 'Set one of the keywords: SVC, SVR.
  endif
  
  self.meta = hash()
     
  case 1 of
    keyword_set(svc) : self.SVMType = 'svc'
    keyword_set(svr) : self.SVMType = 'svr'
  endcase
  
  if keyword_set(file) then begin
    self.loadSampleSet, features, labels, dataIgnoreValue
  endif else begin
    self.setFeatures, features
    self.setLabels, labels
    self.setDataIgnoreValue, dataIgnoreValue
  endelse
  
  self.sortSamples
  
  if ~self.isConsistent(Message=message) then begin
    message, message
  endif

  return,1b
end

pro imageSVMSampleSet::sortSamples
;  if self.isSVC() then begin
    sortedIndices = sort(*self.labels)
    *self.features = (*self.features)[*,sortedIndices]
    *self.labels   = (*self.labels)[sortedIndices]
    self.sorted = 1b
;  endif
end

function imageSVMSampleSet::scaleFeaturesForward, features
  mean = rebin(*self.featuresMean, size(/DIMENSIONS, features))
  standardDeviation = rebin(*self.featuresStandardDeviation, size(/DIMENSIONS, features))
  result = (features-mean)/standardDeviation
  return, result
end

function imageSVMSampleSet::scaleFeaturesBackward, standardizedFeatures
  mean = rebin(*self.featuresMean, size(/DIMENSIONS, standardizedFeatures))
  standardDeviation = rebin(*self.featuresStandardDeviation, size(/DIMENSIONS, standardizedFeatures))
  result = standardizedFeatures*standardDeviation + mean
  return, result
end

pro imageSVMSampleSet::setFeatures, features
  self.features = ptr_new(hub_fix2d(float(features)))
  self.featuresMean = ptr_new([mean(hub_fix2d(*self.features), DIMENSION=2)])
  if self.getNumberOfFeatures() gt 1 then begin
    featuresStandardDeviation = stddev(*self.features, DIMENSION=2)
    featuresStandardDeviation[where(/NULL, featuresStandardDeviation eq 0.)] = 1 ; set zero stddev to 1
  endif else begin
    featuresStandardDeviation = replicate(1., self.getNumberOfFeatures())
  endelse
  self.featuresStandardDeviation = ptr_new(featuresStandardDeviation)
  self.setSelectedFeatures, lindgen(self.getNumberOfFeatures())
  self.sorted = 0b
end

function imageSVMSampleSet::getFeatures, Scale=scale, Subset=subset
  result = *self.features
  if keyword_set(scale) then begin
    result = self.scaleFeaturesForward(result)
  endif
  if keyword_set(subset) then begin
    result = hub_fix2d(result[self.getSelectedFeatures(),*])
  endif
  return, result
end

pro imageSVMSampleSet::setSelectedFeatures, indices
  self.selectedFeatures = ptr_new(indices)
end

function imageSVMSampleSet::getSelectedFeatures, Complement=complement
  selected = *self.selectedFeatures
  if arg_present(complement) then begin
    complementFlag = replicate(1b, self.getNumberOfFeatures())
    complementFlag[selected] = 0b
    complement = where(/NULL, complementFlag)
  endif
  return, selected
end

pro imageSVMSampleSet::setLabels, labels
  if self.isSVC() then begin
    self.labels = ptr_new(byte(labels))
  endif
  if self.isSVR() then begin
    self.labels = ptr_new(float(labels))
  endif
  self.sorted = 0b
end

function imageSVMSampleSet::getLabels, Scaled=scaled
  result = *self.labels
  if keyword_set(scaled) and self.isSVR() then begin
    result -= *self.labelsMean
    result /= *self.labelsStandardDeviation
  endif
  return, result
end

pro imageSVMSampleSet::setInformation, name, value
  if isa(name, 'hash') then begin
    self.meta = self.meta + name
  endif else begin
    (self.meta)[name] = value
  endelse
end

function imageSVMSampleSet::getInformation, name
  if isa(name) then begin
    result = (self.meta)[name]
  endif else begin
    result = self.meta
  endelse
  return, result
end

pro imageSVMSampleSet::setDataIgnoreValue, value
  self.dataIgnoreValue = ptr_new( (self.isSVC() and ~isa(value)) ? 0 : value)
end

function imageSVMSampleSet::getDataIgnoreValue
  result = *self.dataIgnoreValue 
  return, result
end

pro imageSVMSampleSet::loadSampleSet, filenameFeatures, filenameLabels, dataIgnoreValue
  
  if self.isSVC() then sampleSet = hubIOImgSampleSetForClassification(filenameFeatures, filenameLabels)
  if self.isSVR() then sampleSet = hubIOImgSampleSetForRegression(filenameFeatures, filenameLabels)

;  indices = sampleSet.getIndices()
;  self.setFeatures, sampleSet.getFeatures(indices)
;  self.setLabels, sampleSet.getLabels(indices)

  sample = sampleSet.getSample()
  self.setFeatures, sample.features
  self.setLabels, sample.labels

  self.setDataIgnoreValue, isa(dataIgnoreValue) ? dataIgnoreValue : sampleSet.getBackgroundValue()
  if self.isSVC() then begin
    self.setInformation, 'numberOfClasses', sampleSet.getLabelMeta('classes')-1
    self.setInformation, 'classNames', (sampleSet.getLabelMeta('class names'))[1:*]
    self.setInformation, 'classColors', (sampleSet.getLabelMeta('class lookup'))[3:*]
    self.setInformation, 'dataIgnoreValue', 0b
  endif
  if self.isSVR() then begin
    self.setInformation, 'dataIgnoreValue', float(sampleSet.getLabelMeta('data ignore value'))
  endif
  self.setInformation, 'featureFilename', filenameFeatures
  self.setInformation, 'featureNames', sampleSet.getFeatureMeta('band names')
  self.setInformation, 'labelFilename', filenameLabels
end

function imageSVMSampleSet::getType
  return, self.SVMType
end

function imageSVMSampleSet::isSVC
  return, self.getType() eq 'svc'
end

function imageSVMSampleSet::isSVR
  return, self.getType() eq 'svr'
end

function imageSVMSampleSet::getNumberOfSamples
  return, n_elements((*self.features)[0,*]) 
end

function imageSVMSampleSet::getNumberOfFeatures
  return, n_elements((*self.features)[*,0]) 
end

function imageSVMSampleSet::getForwardSelectionCandidates
  selectedFeatures = self.getSelectedFeatures(Complement=unselectedFeatures)
  if n_elements(unselectedFeatures) eq 0 then begin
    message, 'All features are already selected.'
  endif
  candidates = list()
  foreach feature, unselectedFeatures do begin
    candidates.add, [selectedFeatures, feature]
  endforeach
  return, candidates
end

function imageSVMSampleSet::getBackwardSelectionCandidates
  selectedFeatures = self.getSelectedFeatures()
  if n_elements(selectedFeatures) eq 0 then begin
    message, 'All features are already excluded.'
  endif
  candidates = list()
  foreach feature, selectedFeatures do begin
    candidates.add, selectedFeatures[where(/NULL, selectedFeatures ne feature)]
  endforeach
  return, candidates
end

function imageSVMSampleSet::isConsistent, Message=message

  if n_elements((*self.features)[0,*]) ne n_elements(*self.labels) then begin
    message = 'Number of feature vectors must match number of labels'
    return, 0b 
  endif

  if n_elements(*self.labels) eq 0 then begin
    message = 'Sample set is empty.'
    return, 0b 
  endif

  if self.isSVC() then begin
    if ~self.sorted then begin
      message = 'Sample set for SVC must be sorted.'
      return, 0b
    endif
    
  endif
 
  return, 1b
end

function imageSVMSampleSet::copy
  result = imageSVMSampleSet(SVC=self.isSVC(), SVR=self.isSVR(), $
    Features=self.getFeatures(), Labels=self.getLabels(),$
    DataIgnoreValue=self.getDataIgnoreValue())
  result.setInformation, self.getInformation()
  result.setSelectedFeatures, self.getSelectedFeatures()
    
  return, result
end

function imageSVMSampleSet::_overloadPrint, NoInformation=noInformation
  
  result = list()
  result.add,   'Number of Samples: '+strcompress(self.getNumberOfSamples())
  result.add,   'Number of Features:'+strcompress(self.getNumberOfFeatures())
  result.add,   'Features Subset:   '+strjoin(strcompress(self.getSelectedFeatures()+1),',')
  if self.isSVC() then begin
    result.add,   'Number of Classes: '+strcompress(self.getInformation('numberOfClasses'))
    result.add,   'Class Names:        '+strjoin(self.getInformation('classNames'),', ')
  endif
  if self.isSVR() then begin
    result.add,   'Data ignore Value: '+strcompress(self.getDataIgnoreValue()+0)
  endif    
  if ~keyword_set(noInformation) then begin
    result.add, self.meta._overloadPrint(), /Extract
  endif
  result = transpose(result.toArray())
  return, result
end

pro imageSVMSampleSet__define
  struct = {imageSVMSampleSet, inherits IDL_Object,$
    SVMType : '', $
    features : ptr_new(), $
    featuresMean : ptr_new(), $
    featuresStandardDeviation : ptr_new(), $
    featureSubset : ptr_new(), $
    selectedFeatures : ptr_new(), $
    labels : ptr_new(), $
    dataIgnoreValue : ptr_new(), $
    meta : hash(),$
    sorted : 0b,$
    scalingParameters : hash()}
end

pro test_imageSVMSampleSet__loadSampleSet
  features = hub_getTestImage('Hymap_Berlin-A_Image')
  labels = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  sampleSet = imageSVMSampleSet(/SVR, Features=features, Labels=labels, /File)

;  sampleSet.setSelectedFeatures, [10,20,30]
;  candidates = sampleSet.getBackwardSelectionCandidates()
;  foreach candidate,candidates do print, candidate

end