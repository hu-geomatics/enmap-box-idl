pro imageSVM_saveModel, filename, model, gridSearch, selectedParameterPerformance
  save, model, gridSearch, selectedParameterPerformance, Filename=filename
  enmapBox_openFile, filename
end
