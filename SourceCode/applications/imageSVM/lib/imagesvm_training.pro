function imageSVM_training, imageSVMSampleSet, imageSVMTrainingParameter, SVCQuant=svcQuant,$
  UsedImageSVMLibSVM=usedImageSVMLibSVM, ProgressBar=progressBar
  
  if ~isa(imageSVMTrainingParameter, 'imageSVMTrainingParameter') then begin
    message, 'Wrong argument type: imageSVMTrainingParameter.'
  endif
  if ~isa(imageSVMSampleSet, 'imageSVMSampleSet') then begin
    message, 'Wrong argument type: imageSVMSampleSet.'
  endif
  
  if isa(progressBar) then begin
    progressBar.setProgress, /Hide
    progressBar.setInfo, 'train model'
  endif

  if ~isa(usedImageSVMLibSVM) then begin
    imageSVMLibSVM = imageSVMLibSVM()
    imageSVMLibSVM.setImageSVMSampleSet, imageSVMSampleSet
  endif else begin
    imageSVMLibSVM = usedImageSVMLibSVM
  endelse

  ; get libSVM model
  imageSVMLibSVM.setImageSVMTrainingParameter, imageSVMTrainingParameter
  libSVMOutput = imageSVMLibSVM.trainModel()

  ; create imageSVM model
  model = imageSVMModel(libSVMOutput, imageSVMSampleSet.copy(), imageSVMTrainingParameter.copy(), SVCQuant=svcQuant)

  usedImageSVMLibSVM = imageSVMLibSVM

  ; workaround for a bug that unexpectedly crashes IDL in runtime mode
  ; no idea why it happens or why the wordaround solves the problem :-) 
  if lmgr(/RUNTIME) then begin
    tlb = widget_base()
    widget_control, tlb, /REALIZE
    widget_control, tlb, /DESTROY
  endif

  return, model
end