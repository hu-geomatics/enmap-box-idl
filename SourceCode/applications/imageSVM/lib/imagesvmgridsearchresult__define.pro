function imageSVMGridSearchResult::init, parameters
  self.parameters = parameters
  return,1b
end

function imageSVMGridSearchResult::getGrid, measureName
  performanceHash = (self.parameters)['gridPerformance']
  grid = make_array(n_elements((self.parameters)['cArray']), n_elements((self.parameters)['gArray']))
  foreach c,(self.parameters)['cArray'],cIndex do begin
    foreach g,(self.parameters)['gArray'],gIndex do begin
      grid[cIndex, gIndex] = (performanceHash[cIndex,gIndex])[measureName]
    endforeach
  endforeach
  return, hub_fix2d(grid)  
end

function imageSVMGridSearchResult::getLossVector, cIndex, gIndex
  performanceHash = (self.parameters)['gridPerformance']
  lossVector = (performanceHash[cIndex,gIndex])['lossTested']
  return, lossVector
end

function imageSVMGridSearchResult::getPerformance, cIndex, gIndex
  return, ((self.parameters)['gridPerformance'])[cIndex,gIndex]
end

function imageSVMGridSearchResult::getBestParameterCombination, measureName

  performanceGrid = self.getGrid(measureName)
  accuracyGrid = hub_fix2d(self.isErrorMeasure(measureName) ? -performanceGrid : performanceGrid)
  indexBest = (where(accuracyGrid eq max(accuracyGrid)))[0]
  indexBest2d = array_indices(hub_fix2d(accuracyGrid), indexBest)
  cIndex = indexBest2d[0]
  gIndex = indexBest2d[1]
  
  result = hash()
  result['c'] = ((self.parameters)['cArray'])[cIndex]
  result['g'] = ((self.parameters)['gArray'])[gIndex]
  result['performance'] = self.getPerformance(cIndex, gIndex)
  if (self.parameters)['svmType'] eq 'svr' then begin
    result['loss'] = (self.parameters)['loss']
    result['lossVector'] = self.getLossVector(cIndex, gIndex)
  endif
  return, result
end

function imageSVMGridSearchResult::isErrorMeasure, measureName
  case measureName of
    'meanSquaredError' : return, 1b
    'rootMeanSquaredError' : return, 1b
    'meanAbsoluteError' : return, 1b
    else : return, 0b
  endcase
end

pro imageSVMGridSearchResult::getProperty, Parameters=parameters
  parameters = self.parameters
end

pro imageSVMGridSearchResult__define
  struct = {imageSVMGridSearchResult, inherits IDL_Object, $
    parameters : hash()} 
end