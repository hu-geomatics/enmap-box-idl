function imageSVM_loadModel, filename, GridSearch=gridSearch, SelectedParameterPerformance=selectedParameterPerformance
  restore, filename,RELAXED_STRUCTURE_ASSIGNMENT=1 
  return, model
end