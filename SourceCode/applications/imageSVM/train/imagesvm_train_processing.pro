;+
; :Description:
;    Use this procedure to train a SVM model with user defined parameters.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        key                | type     | description
;        -------------------+----------+--------------------------------------------------
;        featureFilename    | string   | file path of feature image
;        labelFilename      | string   | file path of label image
;        modelFilename      | string   | file path of created SVM model
;        c                  | number   | regularization parameter
;        g                  | number   | RBF kernel parameter
;        loss               | number   | epsilon insensitive loss function parameter
;        stop               | number   | stopping criterion
;        svmType            | string   | 'svc' or 'svr'
;        -------------------+----------+-------------------------------------------------- 
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro imageSVM_train_processing, parameters, Title=title, GroupLeader=groupLeader

  required = ['modelFilename','featureFilename','labelFilename',$
    'c','g','stop','svmType']
  if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif
  
  if parameters['svmType'] eq 'svr' then begin
    required = ['loss']
    if ~parameters.hubIsa( required, /EvaluateAND, indicesFalse=missing) then begin
      message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
    endif
  endif

  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader)
  progressBar.setInfo, 'load data'

  svc = parameters['svmType'] eq 'svc'
  svr = parameters['svmType'] eq 'svr'
  
  ; perform grid search
  sampleSet = imageSVMSampleSet(SVC=svc, SVR=svr, Features=parameters['featureFilename'], Labels=parameters['labelFilename'], /File)
  parameter = imageSVMTrainingParameter(SVC=svc, SVR=svr, C=parameters['c'], G=parameters['g'], Loss=parameters.hubGetValue('loss'), Stop=parameters['stop'])
  model = imageSVM_training(sampleSet, parameter, ProgressBar=progressBar)

  imageSVM_saveModel, parameters['modelFilename'], model
  progressBar.cleanup
  
end

pro test_imageSVM_train_processing
  enmapbox
  tic
  parameters = hash()
  svmType = 'svr'
  case svmType of
    'svc' : begin
      parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
      parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
    end
    'svr' : begin
      parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
      parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
    end
  endcase
  parameters['modelFilename'] = hub_getUserTemp('model.'+svmType)
  parameters['c'] = 1
  ;parameters['g'] = 0.001
  parameters['g'] = 0. ; linear kernel
  parameters['loss'] = 0.0001
  parameters['stop'] = 0.0001
  parameters['svmType'] = svmType
  imageSVM_train_processing, parameters
  parameters['outputLabelFilename'] = hub_getUserTemp(svmType+'Estimation')
  imageSVM_apply_processing, parameters
  toc
end