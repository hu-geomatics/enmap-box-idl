function imageSVM_viewModel_getParameters, svmType, Title=title, GroupLeader=groupLeader
  
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputFilename,'modelFilename', Title=strupcase(svmType)+' File', Extension=svmType, Value=hub_getAppState('imageSVM',  svmType+'ModelFilename')
  result = hubAMW_manage()

  if result['accept'] then begin 
    parameters = result
  endif else begin
    parameters = !NULL
  endelse
  return, parameters
end