;+
; :Description:
;    This procedure creates a HTML report with information about a SVM model.
;
; :Params:
;    parameters: in, required, type=Hash
;       A hash that contains the following parameters::
;       
;           key           | type   | description
;           --------------+--------+--------------------------------------------------
;           modelFilename | string | file path of SVM model
;           --------------+--------+-------------------------------------------------- 
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
; 
;-
pro imageSVM_viewModel_processing, parameters, GroupLeader=groupLeader, Title=title

  report = hubReport(Title=title)
  model = imageSVM_loadModel(parameters['modelFilename'], GridSearch=gridSearch)
  sampleSet = model.imageSVMSampleSet
  parameter = model.imageSVMTrainingParameter
  isLinearKernel = parameter.getG() eq 0
  libSVMOutput = model.libSVMOutput
  
  report.addHeading,    'imageSVM Model', 1
  report.addMonospace,  'Filename: '+ parameters['modelFilename']
  
  report.addHeading,    'Input Files', 1
  report.addMonospace, ['Image:           ' + sampleSet.getInformation('featureFilename'),$
                        'Reference Areas: ' + sampleSet.getInformation('labelFilename')]
                        
  report.addHeading,    'Training Data', 1
  report.addMonospace, sampleSet._overloadPrint(/NoInformation)
  
  report.addHeading,    'Model Parameter', 1
  text = list()
  if isLinearKernel then begin
    text.add,             'Linear Kernel'    
  endif else begin
    text.add,             'Gaussian RBF Kernel Parameters g:'+strcompress(parameter.getG())
  endelse
  text.add,             'Regularization Parameters C:'+strcompress(parameter.getC())
  if sampleSet.isSVR() then begin
    text.add,           'Epsilon insensitive Loss Function Parameter:'+strcompress(parameter.getLoss())
  endif
  text.add,             'Termination Criterion:'+strcompress(parameter.getStop())
  text.add,             'Number of Support Vectors:'+strcompress(libSVMOutput['numberOfTotalSV'])
  if sampleSet.isSVC() then begin
    text.add,           'Class-wise Support Vectors:'+strjoin(strcompress(libSVMOutput['numberOfClassSV']),',')  
  endif
  report.addMonospace, text.toArray()
  
  report.addHeading,    'Parameter Search', 1
  if model.svcQuant then begin
    report.addParagraph,  'SVCQuant search: model performance is measured in terms of how well svc probabilities correlate with synthetic between class mixture fractions.' 
  endif

  report.addHeading,    'Settings', 2
  if isLinearKernel then begin
    text =              'Linear Kernel (g=0)'
  endif else begin
    text =              'Gaussian RBF Kernel Parameters g:'+strcompress(strjoin((gridSearch.parameters)['gArray'],','))
  endelse
  text =               [text, $
                        'Regularization Parameters C:'+strcompress(strjoin((gridSearch.parameters)['cArray'],',')),$
                        model.svcQuant ? [] : 'Number of Cross Validation Folds:'+strcompress((gridSearch.parameters)['numberOfFolds']),$
                        'Termination Criterion:'+strcompress((gridSearch.parameters)['stop'])]
  if sampleSet.isSVR() then begin
    text = [text,       'Epsilon insensitive Loss Function Parameter:']
    text[-1] += (gridSearch.parameters)['userdefinedLoss'] ? strcompress((gridSearch.parameters)['loss'])+' (user defined)' : ' via search heuristic'
  endif
  report.addMonospace, text

  report.addHeading,    'Search History', 2
  text = list()
  text.add, '  Performance*   c         g   '+(sampleSet.isSVR()?'  [epsilons tested]   ':'')
  text.add, '  -----------------------------'+(sampleSet.isSVR()?'----------------------':'')
  if sampleSet.isSVR() then performanceName = 'meanAbsoluteError'
  if sampleSet.isSVC() then begin
    if model.svcQuant then begin
      performanceName = 'meanAbsoluteError'
    endif else begin
      performanceName = 'averageF1Accuracy'
    endelse
  endif
  performances = fltarr(n_elements((gridSearch.parameters)['cArray']), n_elements((gridSearch.parameters)['gArray']))
  foreach c, (gridSearch.parameters)['cArray'], cIndex do begin
    foreach g, (gridSearch.parameters)['gArray'], gIndex do begin
      performanceValue = (((gridSearch.parameters)['gridPerformance'])[cIndex,gIndex])[performanceName]
      text.add, string(performanceValue, Format='(F10.2)')+string(c, Format='(F10.2)')+string(g, Format='(F10.2)')
      if sampleSet.isSVR() then begin
        lossTested = (((gridSearch.parameters)['gridPerformance'])[cIndex,gIndex])['lossTested']
        text[-1] = text[-1]+'   ['+strtrim(strcompress(strjoin(string(Format='(F10.3)',lossTested),',')),1)+']'
      endif
      performances[cIndex,gIndex] = performanceValue
    endforeach
  endforeach
  if sampleSet.isSVR() then text.add, '*mean absolute error'
  if sampleSet.isSVC() then begin
    if model.svcQuant then begin
      text.add, '*mean absolute error'
    endif else begin
      text.add, '*averaged F1 measure'
    endelse
  endif
  report.addMonospace, text.toArray()
  
  performanceSurface = imageSVM_viewModel_plot(performances, performanceName)
  report.addImage, performanceSurface, 'Performance Surface' 

  report.saveHTML, /SHOW

end


;+
; :Hidden:
;-
pro test_imageSVM_viewModel_processing
  
  ;train a small model
  p = hash()
  p['svmType'] = 'svc'
  
  case p['svmType'] of
    'svc' : begin
      p['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
      p['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
      p['modelFilename'] = filepath('test.svc',/TMP) 
    end
   'svr' : begin
      p['featureFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
      p['labelFilename'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
      p['modelFilename'] = filepath('test.svr',/TMP) 
    end
  endcase
  p['cArray'] = [0.1,10,100.]
  p['gArray'] = [0.1,10,100.] 
  p['numberOfFolds'] = 3
  p['stopGridSearch'] = 0.1d
  p['stopTraining'] = 0.1d
  
  imageSVM_parameterize_processing, p
  
  p = Hash()
  p['modelFilename'] = "G:\temp\temp_suess\4ao\run B\04_Model\large-x116_s840xl630_SVRmodel_ep0#05.svr"
  imageSVM_viewModel_processing, p

end