function imageSVM_viewModel_plot, performances, performanceName
  dims=size(hub_fix2d(performances),/DIMENSIONS)
  nCol=dims[0] ; number of C parameters
  nRow=dims[1] ; number of g parameters
  imageExtent = nCol*nRow*10
  performancesRebin = rebin(performances, /SAMPLE, imageExtent, imageExtent)
  s = image(performancesRebin $
              , RGB_TABLE=1 $
              , POSITION=[0,.2,.7,.93]$
              , /ORDER $
              , /BUFFER)
  if performanceName eq 'meanAbsoluteError' then begin
    s.rgb_table = rotate(s.rgb_table,-1)
    colleft = ((where(performancesRebin eq min(performancesRebin)))[0] mod n_elements(performancesRebin[*,0]))[0]
;    colright =((where(performancesRebin eq min(performancesRebin)))[-1] mod n_elements(performancesRebin[*,0]))[0]
    colright = colleft + imageExtent/nCol - 1
    rowup = imageExtent - ((where(performancesRebin eq min(performancesRebin)))[0] / n_elements(performancesRebin[*,0]))[0]
    rowdown = rowup - imageExtent/nRow - 1
;    rowdown = imageExtent - ((where(performancesRebin eq min(performancesRebin)))[-1] / n_elements(performancesRebin[*,0]))[0]
  endif else begin
    colleft = ((where(performancesRebin eq max(performancesRebin)))[0] mod n_elements(performancesRebin[*,0]))[0]
;    colright =((where(performancesRebin eq max(performancesRebin)))[-1] mod n_elements(performancesRebin[*,0]))[0]
   colright = colleft + imageExtent/nCol - 1
    rowup = imageExtent - ((where(performancesRebin eq max(performancesRebin)))[0] / n_elements(performancesRebin[*,0]))[0]
    rowdown = rowup - imageExtent/nRow - 1
;    rowdown = imageExtent - ((where(performancesRebin eq max(performancesRebin)))[-1] / n_elements(performancesRebin[*,0]))[0]
  endelse
  !NULL = plot([colleft,colright],[rowup,rowup],COLOR='red',/OVERPLOT,THICK=2)
  !NULL = plot([colleft,colright],[rowdown,rowdown],COLOR='red',/OVERPLOT,THICK=2)
  !NULL = plot([colleft,colleft],[rowdown,rowup],COLOR='red',/OVERPLOT,THICK=2)
  !NULL = plot([colright,colright],[rowdown,rowup],COLOR='red',/OVERPLOT,THICK=2)
  
  c = COLORBAR(TARGET=s, ORIENTATION=1 $
              , POSITION = [0.7,.2,.75,.93] $
              , TITLE = performanceName $
              , TEXTPOS = 1 $
              , TICKDIR = 0 $
              , SUBTICKLEN=0 $
              , TICKVALUES=[min(performances),max(performances)] $
              , BORDER_ON = 1 $
              , COLOR = 'black' $
              , FONT_STYLE = 'Italic' $
              , FONT_SIZE = 12)
  ;Two Arrows with corresponding letters g and C
  cArrow = ARROW([[0, -nCol*nRow/2],[nCol*nRow*10, -nCol*nRow/2]] $            
              , TARGET=s $
              , /DATA $
              , ARROW_STYLE=1 $
              , COLOR='black' $
              , THICK=1)
  cLetter = TEXT(nCol*nRow*10.3, -nCol*nRow/2, TARGET=s, 'C' $
              , /DATA $
              , COLOR='black' $
              , FONT_SIZE=14 $
              , CLIP=0)
  
  gArrow = ARROW([[-nCol*nRow/2,nCol*nRow*10],[-nCol*nRow/2, 0]]$ 
              , TARGET=s $
              , /DATA $
              , ARROW_STYLE=1 $
              , COLOR='black' $
              , THICK=1)
  gLetter = TEXT(-nCol*nRow/2, nCol*nRow*10.3, TARGET=s, 'g' $
              , /DATA $
              , COLOR='black' $
              , FONT_SIZE=14 $
              , CLIP=0)
  
  imageContent = transpose(s.CopyWindow(BORDER=0), [1,2,0])
  s.Close

  return, imageContent
  
end

