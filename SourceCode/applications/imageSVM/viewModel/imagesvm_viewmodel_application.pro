pro imageSVM_viewModel_application, svmType, GroupLeader=groupLeader, Title=title

  parameters = imageSVM_viewModel_getParameters(svmType, Title=title, GroupLeader=groupLeader)

  if isa(parameters) then begin
    imageSVM_viewModel_processing, parameters, GroupLeader=groupLeader, Title=title
  endif

end


