function imageSVM_getDirname, SourceCode=sourceCode
  
  result = filepath('imageSVM', ROOT=enmapBox_getDirname(/Applications, SourceCode=sourceCode))
  return, result

end

;+
; :Hidden:
;-
pro test_imageSVM_getDirname

  print, imageSVM_getDirname()
  print, imageSVM_getDirname(/SourceCode)

end

