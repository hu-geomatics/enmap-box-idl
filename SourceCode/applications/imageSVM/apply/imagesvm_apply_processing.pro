;+
; :Description:
;    Use this procedure to parameterize a SVM model via grid search and cross validation.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        keys                      | type     | description
;        --------------------------+----------+--------------------------------------------------
;        modelFilename             | string   | file path of SVM model
;        featureFilename           | string   | file path of feature image
;        outputLabelFilename       | string   | file path of created label image
;
;        optional keys             | type     | description
;        --------------------------+----------+--------------------------------------------------
;        maskFilename              | string   | file path of mask image
;        outputProbabilities       | boolean  | set to 1, create an image with class probabilities
;        outputPropabilityFilename | number   | file path of created probability image
;        --------------------------+----------+-------------------------------------------------- 
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
pro imageSVM_apply_processing, parameters, ParametersJSON=parametersJSON, Title=title, GroupLeader=groupLeader, NoOpen=noOpen

  if isa(parametersJSON) then begin
    hubMath_mathErrorsOn
    parameters = json_parse(parametersJSON)
    if parameters.hasKey('subsetLineRange') then begin
      parameters['subsetLineRange'] = (parameters['subsetLineRange']).toArray()
    endif
  endif

  parameters.hubCheckKeys, ['modelFilename','featureFilename','outputLabelFilename']

  ; load model
  model = imageSVM_loadModel(parameters['modelFilename'])

  ; apply model
  useMask = parameters.hubIsa('maskFilename')
  outputProbabilities = parameters.hubKeywordSet('outputProbabilities')

  ; - init image objects
  inputImage = hubIOImgInputImage(parameters['featureFilename'])
  outputLabelImage = hubIOImgOutputImage(parameters['outputLabelFilename'], NoOpen=noOpen)
  if useMask then begin
    inputMask = hubIOImgInputImage(parameters['maskFilename'])
  endif
  if outputProbabilities then begin
    outputPropabilityFilename = parameters.hubGetValue('outputPropabilityFilename', Default=parameters['outputLabelFilename']+'ClassProbabilities')
    outputProbabilityImage = hubIOImgOutputImage(outputPropabilityFilename)
  endif
  
  ; - set meta
  outputLabelImage.copyMeta, inputImage, /CopySpatialInformation
  if model.imageSVMSampleSet.isSVC() then begin
    outputLabelImage.setMeta, 'file type', 'ENVI Classification'
    outputLabelImage.setMeta, 'classes', model.imageSVMSampleSet.getInformation('numberOfClasses')+1
    outputLabelImage.setMeta, 'class names', ['unclassified', model.imageSVMSampleSet.getInformation('classNames')]
    outputLabelImage.setMeta, 'class lookup', [0, 0, 0, model.imageSVMSampleSet.getInformation('classColors')]
    outputLabelDataType = 'byte'
  endif
  if model.imageSVMSampleSet.isSVR() then begin
;    outputLabelImage.setMeta, 'file type', 'EnMAP-Box Regression'  
    outputLabelImage.setMeta, 'data ignore value', model.imageSVMSampleSet.getInformation('dataIgnoreValue')
    outputLabelDataType = 'float'
  endif
  if outputProbabilities then begin
    outputProbabilityImage.copyMeta, inputImage, /CopySpatialInformation
    outputProbabilityImage.setMeta, 'band names', model.imageSVMSampleSet.getInformation('classNames')
  endif
  
  ; - init reader 
  numberOfSupportVectors = model.getNumberOfSupportVectors()
  numberOfTileLines = hubIOImg_getTileLines(inputImage.getMeta('samples'),$
                                            max([inputImage.getMeta('bands'),numberOfSupportVectors]) ,$
                                            inputImage.getMeta('data type'))
  inputImage.initReader, numberOfTileLines, /Slice, /TileProcessing, SubsetLineRange=parameters.hubGetValue('subsetLineRange')
  if useMask then begin
    inputMask.initReader, numberOfTileLines, /Slice, /TileProcessing, /Mask, SubsetLineRange=parameters.hubGetValue('subsetLineRange')
  endif

  ; - init writer
  writerSettingsLabels = inputImage.getWriterSettings(SetDataType=outputLabelDataType, SetBands=1)
  outputLabelImage.initWriter, writerSettingsLabels
  if outputProbabilities then begin
    writerSettingsProbabilities = inputImage.getWriterSettings(SetDataType='float', SetBands=model.imageSVMSampleSet.getInformation('numberOfClasses'))
    outputProbabilityImage.initWriter, writerSettingsProbabilities
  endif

  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader, Info='apply model', Range=[0, writerSettingsLabels['numberOfTiles']])
  tile = 1
  while ~inputImage.tileProcessingDone() do begin
    progressBar.setProgress, tile++                                            
    featureSlice = inputImage.getData()
    featureSliceSize = size(/DIMENSIONS, featureSlice)
    
    ; - extract masked pixels
    if useMask then begin
      maskSlice = inputMask.getData()
      maskIndices = where(/NULL, maskSlice)
      featureSlice = hub_fix2d(featureSlice[*, maskIndices])
    endif
      
    ; - get estimation
    estimation = model.apply(featureSlice)
    labelSlice = estimation.remove('labels')
    if outputProbabilities then probabilitySlice = estimation.remove('classProbabilities')

    ; - restore slice
    if useMask then begin

      labelSlice_ = make_array(featureSliceSize[1], VALUE=model.imageSVMSampleSet.getInformation('dataIgnoreValue'))
      labelSlice_[maskIndices] = isa(maskIndices) ? labelSlice : 'dummy'
      labelSlice = temporary(labelSlice_)

      if outputProbabilities then begin
        probabilitySlice_ = make_array(model.imageSVMSampleSet.getInformation('numberOfClasses'), featureSliceSize[1], VALUE=0.)
        probabilitySlice_[*,maskIndices] = isa(maskIndices) ? probabilitySlice : 'dummy'
        probabilitySlice = temporary(probabilitySlice_)
      endif
    endif

    outputLabelImage.writeData, labelSlice
    if outputProbabilities then begin
      outputProbabilityImage.writeData, probabilitySlice
    endif

  endwhile
  
  inputImage.cleanup
  outputLabelImage.cleanup
  if outputProbabilities then  outputProbabilityImage.cleanup
  progressBar.cleanup

end

pro test_imageSVM_apply_processing
  enmapbox
  parameters = hash()
  parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  parameters['modelFilename'] = filepath('svcModel.svc', /TMP)
  parameters['svmType'] = 'svc'
;  imageSVM_parameterize_processing, parameters

;  parameters['subsetLineRange'] = [100,200]
  parameters['outputLabelFilename'] = filepath('svmEstimation2', /TMP)
  imageSVM_apply_processing, parameters
  
end
