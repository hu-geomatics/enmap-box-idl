; :Params:
;    svmType: in, required, type=['svc' | 'svr']
;      Use this argument to specify the svm type::
;        
;        'svc' for svm classification
;        'svr' for svm regression
;           
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro imageSVM_apply_application, svmType, Title=title, GroupLeader=groupLeader

  parameters = imageSVM_apply_getParameters(svmType, Title=title, GroupLeader=groupLeader)
  if isa(parameters) then begin
    hub_setAppState, 'imageSVM', svmType+'ModelFilename', parameters['modelFilename']
    hub_setAppState, 'imageSVM', svmType+'FeatureFilename', parameters['featureFilename']
    hub_setAppState, 'imageSVM', svmType+'MaskFilename', parameters.hubGetValue('maskFilename')
    imageSVM_apply_processing, parameters, Title=title, GroupLeader=groupLeader
  endif 
end
