;+
; :Description:
;    Multi-core version of `imageSVM_apply_processing`.
;
pro imageSVM_apply_processingMulticore, parameters, Title=title, GroupLeader=groupLeader, NoOpen=noOpen

  if ~parameters.hasKey('subsetLineRange') then begin
    lines = hubIOImg_getMeta(parameters['featureFilename'], 'lines')
    linesStart = 0ul
    linesEnd = lines-1
  endif else begin  
    lines = hubMathSpan(parameters['subsetLineRange'])
    linesStart = (parameters['subsetLineRange'])[0]
    linesEnd = (parameters['subsetLineRange'])[1]
  endelse

  numberOfTasks = !cpu.hw_ncpu < lines
  taskLines = floor(lines/numberOfTasks) > 1
  linesStarts = linesStart + ulindgen(numberOfTasks)*taskLines
  linesEnds   = [linesStarts[1:*]-1, linesEnd] 
   
  logdir = file_dirname(parameters['outputLabelFilename'])
  taskManager = hubIDLTaskManager()
  taskParameters = parameters+hash()
  outputLabelFilenames = list()
  outputPropabilityFilenames = list()
  logFilenames = list()
  
  for i=0,numberOfTasks-1 do begin
    strI = strtrim(i, 2)
    taskParameters['subsetLineRange'] = [linesStarts[i],linesEnds[i]]
    taskParameters['outputLabelFilename'] = parameters['outputLabelFilename']+'.task'+strI
    if parameters.hubKeywordSet('outputProbabilities') then begin
      taskParameters['outputPropabilityFilename'] = parameters['outputPropabilityFilename']+'.task'+strI
    endif
    
    logFilename = filepath('logfile'+strI+'.txt', Root_dir=logdir)
    taskManager.addTask, 'imageSVM_apply_processing', ParametersJSON=json_serialize(taskParameters), logFilename
    outputLabelFilenames.add, taskParameters['outputLabelFilename']
    if parameters.hubKeywordSet('outputProbabilities') then begin
      outputPropabilityFilenames.add, taskParameters['outputPropabilityFilename']
    endif    
    logFilenames.add, logFilename
  endfor
  taskManager.runTaskManager,START=1,CLOSEAFTERWORK=2

  ; mosaicking
  ; - estimation
  mosaickingParameters = hash()
  mosaickingParameters['inputFilenames'] = outputLabelFilenames
  mosaickingParameters['outputFilename'] = parameters['outputLabelFilename']
  hubApp_mosaicking_processing, mosaickingParameters, /NoOpen

  ; - probabilities
  if parameters.hubKeywordSet('outputProbabilities') then begin  
    mosaickingParameters = hash()
    mosaickingParameters['inputFilenames'] = outputPropabilityFilenames
    mosaickingParameters['outputFilename'] = parameters['outputPropabilityFilename']
    hubApp_mosaicking_processing, mosaickingParameters, /NoOpen
  endif

  ; setting up metadata and open images
  hubIOImg_copyMeta, (mosaickingParameters['inputFilenames'])[0], mosaickingParameters['outputFilename'], /CopyLabelInformation, /CopyFileType
  hubIOImg_copyMeta, parameters['featureFilename'], mosaickingParameters['outputFilename'], /CopySpatialInformation, /open
  
  ; delete temp files
  filenames = (outputLabelFilenames+outputPropabilityFilenames).toArray()
  file_delete, filenames, /NOEXPAND_PATH
  file_delete, filenames+'.hdr', /NOEXPAND_PATH
;  file_delete, logFilenames.toArray(), /NOEXPAND_PATH
    
end

pro test_imageSVM_apply_processingMulticore

  enmapbox

  ; parameterize model
  parameters = hash()
  parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
  parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
  parameters['modelFilename'] = filepath('svcModel.svc', /TMP)
  parameters['svmType'] = 'svc'
  imageSVM_parameterize_processing, parameters

  ; apply model
  parameters['outputLabelFilename'] = filepath('svcEstimation', /TMP)
  imageSVM_apply_processingMulticore, parameters
  
  print, 'Test done!'
end
