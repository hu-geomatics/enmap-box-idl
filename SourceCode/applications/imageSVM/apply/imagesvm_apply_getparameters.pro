;+
; :Description:
;    This function collecs user input and returns it as inside a hash. 
;
; :Params:
;
;    svmType: in, required, type=['svc' | 'svr']
;      Use this argument to specify the svm type::
;        
;        'svc' for svm classification
;        'svr' for svm regression
;
; :Keywords:
; 
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
function imageSVM_apply_getParameters, svmType, Title=title, GroupLeader=groupLeader

  tsize=100
  defaultFeature = hub_getappState('imageSVM', svmType+'FeatureFilename')
  defaultMask = hub_getappState('imageSVM', svmType+'MaskFilename')
  defaultModel = hub_getappState('imageSVM', svmType+'ModelFilename')
  
  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputFilename, 'modelFilename', Title=strupcase(svmType)+' File', TSize=tSize, Extension=svmType,$
    Value=defaultModel
  hubAMW_inputSampleSet, 'sample', /Masking, Title='Image', TSize=tSize,$
    ReferenceTitle='Mask', ReferenceOptional=1,$
    Value=defaultFeature, ReferenceValue=defaultMask
  hubAMW_frame, Title='Output'
  hubAMW_outputFilename, 'outputLabelFilename', Title=strupcase(svmType)+' Estimation',$
    Value=filepath(svmType+'Estimation', /TMP), TSize=tSize
  if svmType eq 'svc' then begin
    hubAMW_checkbox, 'outputProbabilities', Title='Output Probabilities', VALUE=1
  endif
  amwResult = hubAMW_manage(/Flat)

  if amwResult['accept'] then begin 
    parameters = amwResult
    parameters.remove, 'accept'
    parameters['featureFilename'] = parameters.remove('sample_featureFilename')
    parameters['maskFilename'] = parameters.remove('sample_labelFilename')
  endif else begin
    parameters = !NULL
  endelse

  return, parameters
end

pro test_imageSVM_apply_getParameters

  svmType = 'svc'
  parameters = imageSVM_apply_getParameters(svmType)
  print, parameters
  
end
