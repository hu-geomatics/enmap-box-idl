;+
; :Description:
;    This procedure is the event handler for a EnMAP-Box Menu button. 
;    
; :Params:
;    event: in, required, type=button event structure
;
;-
pro imageSVM_apply_event, event
  
  @huberrorcatch
    
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  svmType = menuEventInfo.argument
  imageSVM_apply_application, svmType, GroupLeader=event.top, Title=menuEventInfo.name
  
end