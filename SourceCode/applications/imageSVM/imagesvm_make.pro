;+
; :Author: Andreas Rabe (andreas.rabe@geo.hu-berlin.de)
; :Private:
;-

pro imageSVM_make

  codeDir = imageSVM_getDirname(/SourceCode)
  targetDir = imageSVM_getDirname()
  enmapBoxMake_copyDefaultDirs, codeDir, targetDir
  
  if ~enmapboxmake.CopyOnly() then begin
    SAVFile = filepath('imageSVM.sav', ROOT=imageSVM_getDirname(), SUBDIR='lib')
    hubDev_compileDirectory, codeDir, SAVFile, nolog=enmapboxmake.noLog()
  
    if ~enmapboxmake.NoIDLDoc() then begin
      docDir = filepath('', ROOT_DIR=imageSVM_getDirname(), Subdirectory=['help','idldoc'])
      title='imageSVM Documentation'
      hub_idlDoc, codeDir, docDir, title, noShow=enmapboxmake.noShow()
    endif
  endif
end
