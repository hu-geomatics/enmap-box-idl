function imageSVM_crossValidation, imageSVMSampleSet, imageSVMTrainingParameter, NumberOfFolds=numberOfFolds, $
  UsedImageSVMLibSVM=usedImageSVMLibSVM

  if ~isa(imageSVMTrainingParameter, 'imageSVMTrainingParameter') then begin
    message, 'Wrong argument type: imageSVMTrainingParameter.'
  endif

  if ~isa(imageSVMSampleSet, 'imageSVMSampleSet') then begin
    message, 'Wrong argument type: imageSVMSampleSet.'
  endif

  numberOfFolds = isa(numberOfFolds) ? numberOfFolds : 3
  
  if ~isa(usedImageSVMLibSVM) then begin
    imageSVMLibSVM = imageSVMLibSVM()
    imageSVMLibSVM.setImageSVMSampleSet, imageSVMSampleSet
  endif else begin
    imageSVMLibSVM = usedImageSVMLibSVM
  endelse
  imageSVMLibSVM.setImageSVMTrainingParameter, imageSVMTrainingParameter
  imageSVMLibSVM.setNumberOfFolds, numberOfFolds

  cvLabels = imageSVMLibSVM.applyCrossValidation()
  trainingLabels = imageSVMSampleSet.getLabels()
  case imageSVMTrainingParameter.getType() of
    'svc' : result = hubMathClassificationPerformance(trainingLabels, cvLabels, imageSVMSampleSet.getInformation('numberOfClasses'))
    'svr' : result = hubMathRegressionPerformance(trainingLabels, cvLabels)
  endcase
  result['numberOfFolds'] = numberOfFolds
  result['residualsStandardDeviation'] = stddev(trainingLabels-cvLabels)
  usedImageSVMLibSVM = imageSVMLibSVM
  return, result

end