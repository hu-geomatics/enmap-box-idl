;+
; :Description:
;    Use this procedure to parameterize a SVM model via grid search and cross validation.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        keys               | type     | description
;        -------------------+----------+--------------------------------------------------
;        modelFilename      | string   | file path of the SVM model
;        svmType            | string   | 'svc' for classification or 'svr' for regression
;        
;        optional keys      | type     | description
;        -------------------+----------+--------------------------------------------------
;        stopGridSearch     | number   | stopping criterion for grid search, default is 0.1
;        numberOfFolds      | number   | number of folds used for cross validation, default is 3
;        spectralSubset     | number[] | array of indices defining the spectral subset, default is to use the bands used for training the model  
;        -------------------+----------+-------------------------------------------------- 
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
function imageSVM_crossValidation_processing, parameters, Title=title, GroupLeader=groupLeader

  required = ['modelFilename','svmType']
  
  if ~parameters.hubIsa(required, /EvaluateAND, indicesFalse=missing) then begin
    message, 'incomplete parameter hash. missing values for keys: '+strjoin(required[missing],', ')
  endif

  defaultNumberOfFolds = 10
  defaultStopGridSearch = 0.001
  
  svc = parameters['svmType'] eq 'svc'
  svr = parameters['svmType'] eq 'svr'
  
  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader)
;  progressBar.setInfo, 'load data'

  ; load model
  model = imageSVM_loadModel(parameters['modelFilename'], GridSearch=gridSearch)
  sampleSet = model.imageSVMSampleSet
  trainingParameter = model.imageSVMTrainingParameter
    
  ; set parameters
  trainingParameter.setStop, parameters.hubGetValue('stopGridSearch', Default=defaultStopGridSearch)
  sampleSet.setSelectedFeatures, parameters.hubgetValue('spectralSubset', Default=sampleSet.getSelectedFeatures())
  numberOfFolds = parameters.hubGetValue('numberOfFolds', Default=defaultNumberOfFolds)
  crossValidationPerformance = imageSVM_crossValidation(sampleSet, trainingParameter, NumberOfFolds=numberOfFolds)
  progressBar.cleanup

  return, crossValidationPerformance
end

pro test_imageSVM_crossValidation_processing
  
  ; train model
  parameters = hash()
  parameters['svmType'] = 'svc'
  case parameters['svmType'] of
    'svr' : begin
      parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
      parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
      parameters['modelFilename'] = filepath('svrModel.svr', /TMP)
      parameters['svmType'] = 'svr'
    end
    'svc' : begin
      parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
      parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
      parameters['modelFilename'] = filepath('svcModel.svc', /TMP)
      parameters['svmType'] = 'svc'
    end
  endcase
  imageSVM_parameterize_processing, parameters
  
  ; cross validate model
  parameters['stopGridSearch'] = 0.1
  parameters['numberOfFolds'] = 3
;  parameters['spectralSubset'] = [0,1,2]
  performance = imageSVM_crossValidation_processing(parameters)
  print, performance

  report = hubApp_accuracyAssessment_getReport_Classification(performance, /CreatePlots)
  report.SaveHTML, /show

end