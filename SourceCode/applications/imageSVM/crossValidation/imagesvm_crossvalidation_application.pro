; :Params:
;    svmType: in, required, type=['svc' | 'svr']
;      Use this argument to specify the svm type::
;        
;        'svc' for svm classification
;        'svr' for svm regression
;           
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
pro imageSVM_crossValidation_application, svmType, Title=title, GroupLeader=groupLeader

  parameters = imageSVM_crossValidation_getParameters(svmType, Title=title, GroupLeader=groupLeader)
  if isa(parameters) then begin
    result = imageSVM_crossValidation_processing(parameters, Title=title, GroupLeader=groupLeader)
    hub_setAppState, 'imageSVM', parameters['svmType']+'ModelFilename', parameters['modelFilename']
    XDISPLAYFILE, TEXT=result._overloadPrint()
  
  endif 
end
