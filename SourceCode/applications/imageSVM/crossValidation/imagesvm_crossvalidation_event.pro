pro imageSVM_crossvalidation_event, event
  
  @huberrorcatch
    
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  svmType = menuEventInfo.argument
  imageSVM_crossvalidation_application, svmType, GroupLeader=event.top, Title=menuEventInfo.name
  
end
