function imageSVM_crossValidation_getParameters, svmType, Title=title, GroupLeader=groupLeader

  hubAMW_program, Title=title, groupLeader
  hubAMW_frame, Title='Input'
  hubAMW_inputFilename,'modelFilename', Title=strupcase(svmType)+' File', Extension=svmType, Value=hub_getAppState('imageSVM',  svmType+'ModelFilename'), TSize=tSize
  hubAMW_parameter, 'numberOfFolds',      Title='Cross Validation Folds', Size=3, /Integer, IsGE=3, Value=10
  hubAMW_parameter, 'stopGridSearch',     Title='Termination Criterion ', Size=10, /Float, IsGT=0, Value=0.001

  result = hubAMW_manage(/Flat)

  if result['accept'] then begin 
    parameters = result
    parameters['svmType'] = svmType
  endif else begin
    parameters = !NULL
  endelse

  return, parameters
end

pro test_imageSVM_crossValidation_getParameters

  svmType = 'svr'
  parameters = imageSVM_crossValidation_getParameters(svmType)
  print,parameters

end
