pro imageSVM_featureSelection_event, event
  
  @huberrorcatch
    
  menuEventInfo = hubProEnvHelper.getMenuEventInfo(event)
  svmType = menuEventInfo.argument
  imageSVM_featureSelection_application, svmType, GroupLeader=event.top, Title=menuEventInfo.name
  
end