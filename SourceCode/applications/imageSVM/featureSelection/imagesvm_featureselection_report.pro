pro imageSVM_featureSelection_report, featureSelectionResult, GroupLeader=groupLeader, Title=title

  report = hubReport(Title=title)
   
  report.addHeading,    'imageSVM Model', 1
  report.addMonospace,  'Filename: '+ featureSelectionResult['modelFilename']
  
  report.addHeading,    'Feature Selection Parameters', 1
  text = list()
  text.add,             'Search Type: '+featureSelectionResult['selectionType']
  text.add,             'Number of Cross Validation Folds: '+strtrim(featureSelectionResult['numberOfFolds'], 2)
  case featureSelectionResult['svmType'] of
    'svc' : performanceName = 'Mean F1 Measure'
    'svr' : performanceName = 'Mean Absolute Error'
  endcase
  text.add, 'Performance measured in Terms of '+performanceName
  report.addMonospace, text.toArray()
    
  report.addHeading,    'Feature Ranking', 1
  text = list()
  model = imageSVM_loadModel(featureSelectionResult['modelFilename'])

  text.add,             'Feature names: ' + strjoin((model.imageSVMSampleSet.getInformation('featureNames'))[featureSelectionResult['rankedFeatures']], '  ')
  text.add,             'Feature #: '+strjoin(string(featureSelectionResult['rankedFeatures']+1, Format='(I10)'))
  text.add,             'Performace:'+strjoin(string(featureSelectionResult['rankedPerformances'], Format='(F10.2)'))
  report.addMonospace, text.toArray()
 
  windowSizeX = 1000
  if featureSelectionResult['svmType'] eq 'svc' then yrange = [0,100]

  learningCurvePlot = plot(indgen(featureSelectionResult['numberOfFeatures'])+1, featureSelectionResult['rankedPerformances'], $
    XTITLE='Number of Selected Features', YTITLE=performanceName, NAME=performanceName, /BUFFER, YRANGE=yrange, DIMENSIONS=[windowSizeX,600], MARGIN=[.1,.1,.25,0.01])
 
  if featureSelectionResult['svmType'] eq 'svc' then begin
    classNames = model.imageSVMSampleSet.getInformation('classNames')
    numberOfClasses = model.imageSVMSampleSet.getInformation('numberOfClasses')
    classColors = reform(model.imageSVMSampleSet.getInformation('classColors'),3,numberOfClasses)
    rankedPerformancesHashes = featureSelectionResult['rankedPerformancesHashes']
    leg = LEGEND(TARGET=learningCurvePlot $
      , POSITION=[950, 600] $
      , FONT_SIZE=8 $
      , /Device)
    for i=0, numberOfClasses-1 do begin 
      classF1Values = list() 
      for j=0, featureSelectionResult['numberOfFeatures']-1 do begin & $
        classF1Values.add, (((rankedPerformancesHashes)[j])['f1Accuracy'])[i] 
      endfor 
      legendString  = strlen(strtrim(classNames[i],2)) gt 18 ? strmid((strtrim(classNames[i],2)),0,17) : strtrim(classNames[i],2)
      leg.add, plot(indgen(featureSelectionResult['numberOfFeatures'])+1 $
                   , classF1Values.toarray() $
                   , name = legendString $
                   , color = classColors[*,i] $
                   , ytitle = 'F1 Measure' $
                   , /OVERPLOT)
    endfor 
  endif
  
  learningCurveImage = transpose(learningCurvePlot.CopyWindow(WIDTH=windowSizeX), [1,2,0])
  learningCurvePlot.close
  report.addImage, learningCurveImage, 'Feature Selection Learning Curve'

  report.addHeading,    'Search History', 1

  case featureSelectionResult['selectionType'] of
    'forward' : begin
      for i=1,featureSelectionResult['numberOfFeatures'] do begin
        report.addHeading, 'Iteration '+strcompress(/REMOVE_ALL,i), 2
        text = list()
        text.add, 'Performance | Subset'
        text.add, '------------|--------'
        iResult = featureSelectionResult[i]
        for k=1,iResult['numberOfCandidates'] do begin
          candidateResult = iResult[k]
          text.add, string(candidateResult.performance, Format='(F11.2)')+' | '+strcompress(strjoin(candidateResult.subset+1)) 
       ;   print,candidateResult.subset+1    
        endfor
        bestCandidateResult = iResult['best']
        text.add, ''
        text.add, 'Best Performing Subset: '+strcompress(strjoin(bestCandidateResult.subset+1))
        text.add, 'Selected Feature:       '+strcompress((featureSelectionResult['rankedFeatures'])[i-1]+1)
        report.addMonospace, text.toArray()
      endfor
    end
    'backward' : begin
      for i=2,featureSelectionResult['numberOfFeatures'] do begin
        report.addHeading, 'Iteration '+strcompress(/REMOVE_ALL,i-1), 2
        text = list()
        text.add, 'Performance | Subset'
        text.add, '------------|--------'
        iResult = featureSelectionResult[i]
        for k=1,iResult['numberOfCandidates'] do begin
          candidateResult = iResult[k]
          text.add, string(candidateResult.performance, Format='(F11.2)')+' | '+strcompress(strjoin(candidateResult.subset+1)) 
       ;   print,candidateResult.subset+1    
        endfor
        bestCandidateResult = iResult['best']
        text.add, ''
        text.add, 'Best Performing Subset: '+strcompress(strjoin(bestCandidateResult.subset+1))
        text.add, 'Dropped Feature:        '+strcompress((featureSelectionResult['rankedFeatures'])[-i+1]+1)
        report.addMonospace, text.toArray()
      endfor
    end
  endcase
  report.saveHTML, /SHOW

end