;+
; :Description:
;    Returns feature forward/backward selection for a given SVM model.
;
; :Params:
;    parameters: in, required, type=Hash
;      A hash that contains the following parameters::
;       
;        keys               | type     | description
;        -------------------+----------+--------------------------------------------------
;        modelFilename      | string   | file path of SVM model
;        selectionType      | string   | 'forward' or 'backward' selection
;        
;        optional keys      | type     | description
;        -------------------+----------+--------------------------------------------------
;        numberOfFolds      | number   | number of folds used for cross validation, default is 3
;        stop               | number   | stopping criterion for training, default is 0.1
;
; :Keywords:
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;-
function imageSVM_featureSelection_processing, parameters, Title=title, GroupLeader=groupLeader

  parameters.hubCheckKeys, ['modelFilename','selectionType']
  
  model = imageSVM_loadModel(parameters['modelFilename'])

  imageSVMTrainingParameter = model.imageSVMTrainingParameter
  imageSVMTrainingParameter.setStop, parameters.hubGetValue('stop', Default=0.1)
  imageSVMSampleSet = model.imageSVMSampleSet
  svmType = imageSVMSampleSet.getType()
  numberOfFolds = parameters.hubGetValue('numberOfFolds', Default=3)
  numberOfFeatures = imageSVMSampleSet.getNumberOfFeatures()        
  progressBar = hubProgressBar(Title=title, GroupLeader=groupLeader)
  
  case svmType of
    'svc' : performanceName = 'averageF1Accuracy'
    'svr' : performanceName = 'meanAbsoluteError'
  endcase
  performanceFactor = svmType eq 'svr' ? -1 : 1
  performances = []
  rankedFeatures = []
  
  case parameters['selectionType'] of
    'forward' : imageSVMSampleSet.setSelectedFeatures, []
    'backward' : imageSVMSampleSet.setSelectedFeatures, lindgen(numberOfFeatures)
  endcase

  lastBestCandidate = imageSVMSampleSet.getSelectedFeatures()
  result = hash()
  rankedPerformancesHashes = list()
  for iteration=1,numberOfFeatures do begin
    progressBar.setProgress, 1.*iteration/numberOfFeatures
    case parameters['selectionType'] of
      'forward' :  candidates = imageSVMSampleSet.getForwardSelectionCandidates()
      'backward' : candidates = (iteration eq 1) ? list(imageSVMSampleSet.getSelectedFeatures()) : imageSVMSampleSet.getBackwardSelectionCandidates()
    endcase

    bestPerformance = -!values.f_infinity
    resultIteration = hash()
    resultIteration['numberOfCandidates'] = n_elements(candidates)
    foreach candidate,candidates,iCandidate do begin
      progressBar.checkCancel
      imageSVMSampleSet.setSelectedFeatures, candidate
      crossValidationPerformance = imageSVM_crossValidation(imageSVMSampleSet, imageSVMTrainingParameter, NumberOfFolds=numberOfFolds)
      currentPerformance = crossValidationPerformance[performanceName]*performanceFactor
      if currentPerformance gt bestPerformance then begin
        bestPerformance = currentPerformance
bestPerformanceHash = crossValidationPerformance
        bestCrossValidationPerformance =crossValidationPerformance
        bestCandidate = candidate
      endif
;      print, string(crossValidationPerformance[performanceName])+strjoin((candidate), ',')

      resultIteration[iCandidate+1] = {performance: crossValidationPerformance[performanceName], subset:candidate}
    endforeach
    case parameters['selectionType'] of
      'forward' : begin
        rankedFeatures = [rankedFeatures, bestCandidate[-1]]
        rankedPerformancesHashes.add, bestPerformanceHash
      end
      'backward' : begin
        rankedFeatures = [hubMathSetDifference(lastBestCandidate, bestCandidate), rankedFeatures]
        rankedPerformancesHashes.add, bestPerformanceHash, 0
      end
    endcase

    lastBestCandidate = bestCandidate
    imageSVMSampleSet.setSelectedFeatures, bestCandidate
    performances = [performances, bestPerformance*performanceFactor]
;    print, 'best candidate:', performances[-1], (bestCandidate)
;    print, rankedFeatures
;    print
    
    resultIteration['best'] = {performance: bestPerformance, subset:bestCandidate}
    result[iteration] = resultIteration
;    (result[iteration])['bestCandidate'] = bestCrossValidationPerformance
;     
;    resultIterations.add, resultIteration
  endfor
  obj_destroy, progressBar  
  
  
  case parameters['selectionType'] of
    'backward' : begin
      rankedFeatures = [bestCandidate, rankedFeatures]
      rankedPerformances = reverse(performances)
    end
    'forward' : begin
      rankedPerformances = performances
    end
  endcase
    
  result['numberOfFeatures'] = numberOfFeatures
  result['numberOfFolds'] = numberOfFolds
  result['selectionType'] = parameters['selectionType']
  result['rankedFeatures'] = rankedFeatures
  result['rankedPerformances'] = rankedPerformances
  result['rankedPerformancesHashes'] = rankedPerformancesHashes

  result['modelFilename'] = parameters['modelFilename']
  result['svmType'] = parameters['svmType']
  
  
  return, result
 
end

pro test_imageSVM_featureSelection_processing
  
  parameters = hash()

  parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-B_Image')
; parameters['featureFilename'] = 'D:\Dropbox\svmFS\hymapB4'
  parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-B_Regression-Training-Sample')
  parameters['modelFilename'] = filepath('svrModel.svr', /TMP)
  parameters['svmType'] = 'svr'
  parameters['cArray'] = [1000]
  parameters['gArray'] = [0.01]
  parameters['selectionType'] = 'forward' 

;  parameters['featureFilename'] = hub_getTestImage('Hymap_Berlin-A_Image')
;  parameters['labelFilename'] = hub_getTestImage('Hymap_Berlin-A_Classification-Training-Sample')
;  parameters['modelFilename'] = filepath('svcModel.svc', /TMP)
;  parameters['svmType'] = 'svc'

  imageSVM_parameterize_processing, parameters, Title=title, GroupLeader=groupLeader
;  imageSVM_viewModel_processing, parameters

  featureSelectionResult = imageSVM_featureSelection_processing(parameters, Title=title, GroupLeader=groupLeader)
  print, 'Ranked Features:',featureSelectionResult['rankedFeatures']
  print, 'Ranked Performance:',featureSelectionResult['rankedPerformances']

end
