;+
; :Description:
;    This function collecs user input and returns it as inside a hash. 
;
; :Params:
;
;    svmType: in, required, type=['svc' | 'svr']
;      Use this argument to specify the svm type::
;        
;        'svc' for svm classification
;        'svr' for svm regression
;
; :Keywords:
; 
;    Title: in, optional, type=string
;      Use this keyword to specify the title for used widgets.
;      
;    GroupLeader: in, optional, type=int
;      Use this keyword to specify the group leader for used widgets.
;    
;-
function imageSVM_featureSelection_getParameters, svmType, Title=title, GroupLeader=groupLeader

  defaultModel = hub_getappState('imageSVM', svmType+'ModelFilename')
  
  hubAMW_program, Title=title, groupLeader
  
  hubAMW_frame, Title='Input'
  hubAMW_inputFilename, 'modelFilename', Title=strupcase(svmType)+' File', Extension=svmType, Value=defaultModel

  hubAMW_frame, Title='Parameters'
  hubAMW_checklist, 'selectionType', Title='Selection Type', VALUE=0, List=['Forward Selection','Backward Elimination'], Extract=['forward', 'backward'] 
  hubAMW_parameter, 'numberOfFolds', Title='Number of Cross Validation folds', Size=3, /Integer, IsGE=3, Value=3
  hubAMW_parameter, 'stop', Title='Termination Criterion', Size=10, /Float, IsGT=0, Value='0.1'

  amwResult = hubAMW_manage()

  if amwResult['accept'] then begin 
    parameters = amwResult
    parameters['svmType'] = svmType
    parameters.remove, 'accept'
  endif else begin
    parameters = !NULL
  endelse

  return, parameters
end

pro test_imageSVM_featureSelection_getParameters

  svmType = 'svc'
  parameters = imageSVM_featureSelection_getParameters(svmType)
  print, parameters
  
end
