pro imageSVM_featureSelection_application, svmType, Title=title, GroupLeader=groupLeader

  parameters = imageSVM_featureSelection_getParameters(svmType, Title=title, GroupLeader=groupLeader)
  if isa(parameters) then begin
    hub_setAppState, 'imageSVM', parameters['svmType']+'ModelFilename', parameters['modelFilename']
    result = imageSVM_featureSelection_processing(parameters, Title=title, GroupLeader=groupLeader)
    imageSVM_featureSelection_report, result
  endif
end
