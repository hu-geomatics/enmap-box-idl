FasdUAS 1.101.10   ��   ��    k             l      ��  ��    
This script creates a "double-clickable" icon for a runtime IDL
application defined by the idlApp variable. This script should be placed
at the top level of a runtime application hierarchy. The 
Utils_applescripts.scpt file must be in the same directory.
     � 	 	  
 T h i s   s c r i p t   c r e a t e s   a   " d o u b l e - c l i c k a b l e "   i c o n   f o r   a   r u n t i m e   I D L 
 a p p l i c a t i o n   d e f i n e d   b y   t h e   i d l A p p   v a r i a b l e .   T h i s   s c r i p t   s h o u l d   b e   p l a c e d 
 a t   t h e   t o p   l e v e l   o f   a   r u n t i m e   a p p l i c a t i o n   h i e r a r c h y .   T h e   
 U t i l s _ a p p l e s c r i p t s . s c p t   f i l e   m u s t   b e   i n   t h e   s a m e   d i r e c t o r y . 
   
  
 l     ��������  ��  ��        l      ��  ��    K E
Specify the path to the IDL SAVE file that launches the application
     �   � 
 S p e c i f y   t h e   p a t h   t o   t h e   I D L   S A V E   f i l e   t h a t   l a u n c h e s   t h e   a p p l i c a t i o n 
      l     ��������  ��  ��        l     ����  r         c         m        �   X . . / I D L V M C r o s s P l a t f o r m / e n m a p B o x V M L a u n c h e r . s a v  m    ��
�� 
TEXT  o      ���� 0 idlapp idlApp��  ��        l     ��������  ��  ��        l      ��   !��     o i
Specify the path to the top directory of the IDL distribution, 
relative to the location of the script.
    ! � " " � 
 S p e c i f y   t h e   p a t h   t o   t h e   t o p   d i r e c t o r y   o f   t h e   I D L   d i s t r i b u t i o n ,   
 r e l a t i v e   t o   t h e   l o c a t i o n   o f   t h e   s c r i p t . 
   # $ # l    %���� % r     & ' & c    	 ( ) ( m     * * � + + 6 / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / ) m    ��
�� 
TEXT ' o      ���� 0 idldirfolder idlDirFolder��  ��   $  , - , l     ��������  ��  ��   -  . / . l   , 0���� 0 O    , 1 2 1 k    + 3 3  4 5 4 r     6 7 6 c     8 9 8 l    :���� : n     ; < ; m    ��
�� 
ctnr < l    =���� = I   �� >��
�� .earsffdralis        afdr >  f    ��  ��  ��  ��  ��   9 m    ��
�� 
TEXT 7 o      ���� 0 mycontainer myContainer 5  ? @ ? l    �� A B��   A D >	set IDLDirFolder to POSIX path of myContainer & idlDir & "/"     B � C C | 	 s e t   I D L D i r F o l d e r   t o   P O S I X   p a t h   o f   m y C o n t a i n e r   &   i d l D i r   &   " / "   @  D E D r    # F G F n    ! H I H 1    !��
�� 
strq I l    J���� J b     K L K o    ���� 0 idldirfolder idlDirFolder L m     M M � N N  b i n��  ��   G o      ���� 0 idlrunfolder IDLRunFolder E  O�� O r   $ + P Q P c   $ ) R S R b   $ ' T U T o   $ %���� 0 mycontainer myContainer U m   % & V V � W W . U t i l s _ a p p l e s c r i p t s . s c p t S m   ' (��
�� 
TEXT Q o      ���� ,0 applescriptutilsfile ApplescriptUtilsFile��   2 m     X X�                                                                                  MACS  alis    Z  Mac                        �L�H+   O
Finder.app                                                      2���j        ����  	                CoreServices    �L��      ��\     O L K  -Mac:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p    M a c  &System/Library/CoreServices/Finder.app  / ��  ��  ��   /  Y Z Y l     ��������  ��  ��   Z  [ \ [ l  - : ]���� ] r   - : ^ _ ^ c   - 6 ` a ` b   - 4 b c b b   - 2 d e d n   - 0 f g f 1   . 0��
�� 
psxp g o   - .���� 0 mycontainer myContainer e m   0 1 h h � i i  / c o   2 3���� 0 idlapp idlApp a m   4 5��
�� 
TEXT _ o      ���� 0 	myapppath 	myAppPath��  ��   \  j k j l  ; N l���� l Z   ; N m n���� m =  ; @ o p o o   ; <���� 0 idlapp idlApp p m   < ? q q � r r   n r   C J s t s m   C F u u � v v   t o      ���� 0 	myapppath 	myAppPath��  ��  ��  ��   k  w x w l     ��������  ��  ��   x  y z y l  O \ {���� { r   O \ | } | b   O X ~  ~ b   O T � � � o   O P���� 0 idldirfolder idlDirFolder � m   P S � � � � �  b i n / i d l   - v m =  o   T W���� 0 	myapppath 	myAppPath } o      ���� 0 idlcmd idlCmd��  ��   z  � � � l     ��������  ��  ��   �  � � � l  ] k ����� � r   ] k � � � I  ] g�� ���
�� .sysoloadscpt        file � 4   ] c�� �
�� 
file � o   a b���� ,0 applescriptutilsfile ApplescriptUtilsFile��   � o      ���� $0 applescriptutils ApplescriptUtils��  ��   �  � � � l  l � ����� � O   l � � � � k   r � � �  � � � r   r { � � � I   r w�������� 0 	launchx11 	LaunchX11��  ��   � o      ���� 0 xresult XResult �  ��� � I   | ��� ����� $0 environmentsetup EnvironmentSetup �  ��� � o   } ~���� 0 idldirfolder idlDirFolder��  ��  ��   � o   l o���� $0 applescriptutils ApplescriptUtils��  ��   �  � � � l     ��������  ��  ��   �  � � � l  � � ����� � Z   � � � ����� � =  � � � � � o   � ����� 0 xresult XResult � m   � �����   � k   � � � �  � � � r   � � � � � b   � � � � � b   � � � � � b   � � � � � b   � � � � � b   � � � � � b   � � � � � b   � � � � � b   � � � � � b   � � � � � o   � ����� 0 shellcmd shellCmd � m   � � � � � � �  ' � o   � ����� 0 fullsetupcmd fullSetupCmd � m   � � � � � � �  ;   � o   � ����� 0 
displaycmd 
DisplayCmd � m   � � � � � � �  ;   c d     � o   � ����� 0 idlrunfolder IDLRunFolder � m   � � � � � � � 4 ;   / u s r / X 1 1 R 6 / b i n / x t e r m   - e   � o   � ����� 0 idlcmd idlCmd � m   � � � � � � � , '   >   / d e v / n u l l     2 > & 1   &   � o      ���� 0 
thecommand 
theCommand �  � � � l  � ��� � ���   �  display dialog theCommand    � � � � 2 d i s p l a y   d i a l o g   t h e C o m m a n d �  ��� � r   � � � � � I  � ��� ���
�� .sysoexecTEXT���     TEXT � o   � ����� 0 
thecommand 
theCommand��   � o      ���� 0 results  ��  ��  ��  ��  ��   �  ��� � l     ��������  ��  ��  ��       �� � �  * � � � � � ��� � � � � �����   � ��������������������~�}�|�{�z�y
�� .aevtoappnull  �   � ****�� 0 idlapp idlApp�� 0 idldirfolder idlDirFolder�� 0 mycontainer myContainer�� 0 idlrunfolder IDLRunFolder�� ,0 applescriptutilsfile ApplescriptUtilsFile�� 0 	myapppath 	myAppPath�� 0 idlcmd idlCmd�� $0 applescriptutils ApplescriptUtils� 0 xresult XResult�~ 0 fullsetupcmd fullSetupCmd�} 0 
displaycmd 
DisplayCmd�| 0 shellcmd shellCmd�{ 0 
thecommand 
theCommand�z 0 results  �y   � �x ��w�v � ��u
�x .aevtoappnull  �   � **** � k     � � �   � �  # � �  . � �  [ � �  j � �  y � �  � � �  � � �  ��t�t  �w  �v   �   � & �s�r *�q X�p�o�n M�m�l V�k�j h�i q u ��h�g�f�e�d�c�b�a ��` ��_ � � ��^�]�\
�s 
TEXT�r 0 idlapp idlApp�q 0 idldirfolder idlDirFolder
�p .earsffdralis        afdr
�o 
ctnr�n 0 mycontainer myContainer
�m 
strq�l 0 idlrunfolder IDLRunFolder�k ,0 applescriptutilsfile ApplescriptUtilsFile
�j 
psxp�i 0 	myapppath 	myAppPath�h 0 idlcmd idlCmd
�g 
file
�f .sysoloadscpt        file�e $0 applescriptutils ApplescriptUtils�d 0 	launchx11 	LaunchX11�c 0 xresult XResult�b $0 environmentsetup EnvironmentSetup�a 0 shellcmd shellCmd�` 0 fullsetupcmd fullSetupCmd�_ 0 
displaycmd 
DisplayCmd�^ 0 
thecommand 
theCommand
�] .sysoexecTEXT���     TEXT�\ 0 results  �u ���&E�O��&E�O� )j �,�&E�O��%�,E�O��%�&E�UO��,�%�%�&E` O�a   a E` Y hO�a %_ %E` O*a �/j E` O_  *j+ E` O*�k+ UO_ j  :_ a %_ %a %_ %a  %�%a !%_ %a "%E` #O_ #j $E` %Y h � � � � b M a c : U s e r s : a n d r e a s : D o w n l o a d s : E n M A P - B o x : I D L V M O S X 6 4 : � � � � @ ' / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / b i n ' � � � � � M a c : U s e r s : a n d r e a s : D o w n l o a d s : E n M A P - B o x : I D L V M O S X 6 4 : U t i l s _ a p p l e s c r i p t s . s c p t � � � � � / U s e r s / a n d r e a s / D o w n l o a d s / E n M A P - B o x / I D L V M O S X 6 4 / / . . / I D L V M C r o s s P l a t f o r m / e n m a p B o x V M L a u n c h e r . s a v � � � � / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / b i n / i d l   - v m = / U s e r s / a n d r e a s / D o w n l o a d s / E n M A P - B o x / I D L V M O S X 6 4 / / . . / I D L V M C r o s s P l a t f o r m / e n m a p B o x V M L a u n c h e r . s a v � �[ �  ��[   � k       � �  � � � l     �Z � ��Z   � � }Launch x11 on the machine if the process isn't already running.  Wait to be sure it starts prior to starting the application.    � � � � � L a u n c h   x 1 1   o n   t h e   m a c h i n e   i f   t h e   p r o c e s s   i s n ' t   a l r e a d y   r u n n i n g .     W a i t   t o   b e   s u r e   i t   s t a r t s   p r i o r   t o   s t a r t i n g   t h e   a p p l i c a t i o n . �  � � � i      � � � I      �Y�X�W�Y 0 	launchx11 	LaunchX11�X  �W   � k     � � �  � � � r      � � � I    �V ��U
�V .sysoexecTEXT���     TEXT � m      � � � � � . s w _ v e r s   - p r o d u c t V e r s i o n�U   � o      �T�T 0 	osversion 	OSVersion �    l   �S�R�Q�S  �R  �Q    r     l   �P�O I   �N�M
�N .sysoexecTEXT���     TEXT m    	 �		 
 i d   - u�M  �P  �O   o      �L�L 0 userid userID 

 r     b     m     � 8 l s   - l n   / t m p / . X 1 1 - u n i x |   g r e p   o    �K�K 0 userid userID o      �J�J 0 
displaydir 
Displaydir  r     I    �I�H�G�I 0 getdisplaynum GetDisplayNum�H  �G   o      �F�F 0 
displaynum 
DisplayNum  l   �E�D�C�E  �D  �C    O    � Z   " ��B�A l  " ,�@�? H   " , E   " + !  l  " )"�>�=" c   " )#$# n   " '%&% 1   % '�<
�< 
pnam& 2  " %�;
�; 
prcs$ m   ' (�:
�: 
list�>  �=  ! m   ) *'' �((  X 1 1�@  �?   k   / �)) *+* I  / 4�9,�8
�9 .ascrnoop****      � ****, m   / 0--�                                                                                  x11a  alis    L  Mac                        �L�H+   rXQuartz.app                                                     M�Ϝ��        ����  	                	Utilities     �L��      Ϝ��     r q  (Mac:Applications: Utilities: XQuartz.app    X Q u a r t z . a p p    M a c  "Applications/Utilities/XQuartz.app  / ��  �8  + ./. r   5 8010 m   5 622 �33  1 o      �7�7 0 results  / 4�64 Q   9 �5675 k   < �88 9:9 r   < ?;<; m   < =�5�5 < o      �4�4 0 x  : =>= l  @ @�3?@�3  ? : 4 Timeout after 30 seconds in case there's a problem.   @ �AA h   T i m e o u t   a f t e r   3 0   s e c o n d s   i n   c a s e   t h e r e ' s   a   p r o b l e m .> BCB W   @ tDED k   P oFF GHG Q   P cIJ�2I r   S ZKLK I  S X�1M�0
�1 .sysoexecTEXT���     TEXTM o   S T�/�/ 0 
displaydir 
Displaydir�0  L l     N�.�-N o      �,�, 0 results  �.  �-  J R      �+�*�)
�+ .ascrerr ****      � ****�*  �)  �2  H OPO r   d iQRQ [   d gSTS o   d e�(�( 0 x  T m   e f�'�' R o      �&�& 0 x  P U�%U I  j o�$V�#
�$ .sysodelanull��� ��� nmbrV m   j k�"�" �#  �%  E G   D OWXW l  D GY�!� Y >   D GZ[Z o   D E�� 0 results  [ m   E F\\ �]]  �!  �   X l  J M^��^ ?   J M_`_ o   J K�� 0 x  ` m   K L�� �  �  C aba Z  u �cd��c ?  u xefe o   u v�� 0 x  f m   v w�� d R   { ���g
� .ascrerr ****      � ****�  g �h�
� 
errnh m    �����  �  �  b i�i l  � �����  �  �  �  6 R      ��j
� .ascrerr ****      � ****�  j �k�

� 
errnk m      �	�	��
  7 k   � �ll mnm I  � ����
� .miscactvnull��� ��� obj �  �  n o�o I  � ��p�
� .sysodlogaskr        TEXTp m   � �qq �rr � U n a b l e   t o   o b t a i n   d i s p l a y   v a r i a b l e   f r o m   / t m p / . X 1 1 - u n i x .     P l e a s e   m a k e   s u r e   y o u r   X 1 1   e n v i r o n m e n t   i s   s t a r t i n g   p r o p e r l y .�  �  �6  �B  �A   m    ss�                                                                                  MACS  alis    Z  Mac                        �L�H+   O
Finder.app                                                      2���j        ����  	                CoreServices    �L��      ��\     O L K  -Mac:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p    M a c  &System/Library/CoreServices/Finder.app  / ��   tut l  � ���� �  �  �   u vwv l  � ���������  ��  ��  w x��x L   � �yy m   � �����  ��   � z{z l     ��������  ��  ��  { |}| l     ��~��  ~ 9 3 Look at the socket number to determine the display    ��� f   L o o k   a t   t h e   s o c k e t   n u m b e r   t o   d e t e r m i n e   t h e   d i s p l a y} ��� i    ��� I      �������� 0 getdisplaynum GetDisplayNum��  ��  � k     5�� ��� r     ��� I    �����
�� .sysoexecTEXT���     TEXT� m     �� ���  e c h o   $ D I S P L A Y��  � o      ���� 0 
displayenv 
displayEnv� ��� l   ��������  ��  ��  � ���� Z    5������ =   ��� o    	���� 0 
displayenv 
displayEnv� m   	 
�� ���  � k    0�� ��� r    ��� l   ������ I   �����
�� .sysoexecTEXT���     TEXT� m    �� ���  i d   - u   &��  ��  ��  � o      ���� 0 userid userID� ��� r    ��� b    ��� b    ��� m    �� ��� : ` l s   - l n   / t m p / . X 1 1 - u n i x |   g r e p  � o    ���� 0 userid userID� m    �� ���  `� o      ���� 0 
displaydir 
Displaydir� ��� l   ��������  ��  ��  � ��� l   ������  � ' ! Get Socket number to set DISPLAY   � ��� B   G e t   S o c k e t   n u m b e r   t o   s e t   D I S P L A Y� ��� r    %��� b    #��� b    !��� m    �� ���  b b b =� o     ���� 0 
displaydir 
Displaydir� m   ! "�� ��� ( ;   e c h o   $ { b b b / * X / : } . 0� o      ���� 0 	socketcmd 	SocketCmd� ��� r   & -��� I  & +�����
�� .sysoexecTEXT���     TEXT� o   & '���� 0 	socketcmd 	SocketCmd��  � o      ���� 0 
displaynum 
DisplayNum� ���� L   . 0�� o   . /���� 0 
displaynum 
DisplayNum��  ��  � L   3 5�� o   3 4���� 0 
displayenv 
displayEnv��  � ��� l     ��������  ��  ��  � ��� l     ������  � A ; Sets up the DISPLAY and SHELL environment for ENVI and IDL   � ��� v   S e t s   u p   t h e   D I S P L A Y   a n d   S H E L L   e n v i r o n m e n t   f o r   E N V I   a n d   I D L� ���� i    ��� I      ������� $0 environmentsetup EnvironmentSetup� ���� o      ���� 0 idldirfolder idlDirFolder��  ��  � k     ��� ��� l     ��������  ��  ��  � ��� p      �� ������ 0 fullsetupcmd fullSetupCmd��  � ��� p      �� ������ 0 
displaycmd 
DisplayCmd��  � ��� p      �� ������ 0 shellcmd shellCmd��  � ��� l     ��������  ��  ��  � ��� r     ��� m     ��
�� 
TEXT� o      ���� 0 fullsetupcmd fullSetupCmd� ��� r    ��� m    ��
�� 
TEXT� o      ���� 0 
displaycmd 
DisplayCmd� ��� r    ��� m    	��
�� 
TEXT� o      ���� 0 shellcmd shellCmd� ��� r    ��� m    ��
�� 
TEXT� o      ���� 0 	idldircmd 	IDLDirCmd� ��� l   ��������  ��  ��  � ��� r    ��� I    �������� 0 getdisplaynum GetDisplayNum��  ��  � o      ���� 0 
displaynum 
DisplayNum� ��� l   ��������  ��  ��  � ��� l   ������  �   Use the users' shell   � ��� *   U s e   t h e   u s e r s '   s h e l l� ��� r       I   ����
�� .sysoexecTEXT���     TEXT m     �  e c h o   $ S H E L L��   o      ���� 0 shellenv  �  r     % b     #	
	 o     !���� 0 shellenv  
 m   ! " �    - i   - c   o      ���� 0 shellcmd shellCmd  l  & &��������  ��  ��    l  & &����   J DDetermine the shell and set up to source init file only if it exists    � � D e t e r m i n e   t h e   s h e l l   a n d   s e t   u p   t o   s o u r c e   i n i t   f i l e   o n l y   i f   i t   e x i s t s  Z   & � E   & ) o   & '���� 0 shellenv   m   ' ( �  b a s h k   , 9   r   , 1!"! b   , /#$# m   , -%% �&&  e x p o r t   D I S P L A Y =$ o   - .���� 0 
displaynum 
DisplayNum" o      ���� 0 
displaycmd 
DisplayCmd  '(' r   2 7)*) b   2 5+,+ m   2 3-- �..  e x p o r t   I D L _ D I R =, o   3 4���� 0 idldirfolder idlDirFolder* o      ���� 0 	idldircmd 	IDLDirCmd( /��/ l  8 8��01��  0   tcsh	   1 �22    t c s h 	��   343 E   < ?565 o   < =���� 0 shellenv  6 m   = >77 �88  t c s h4 9:9 k   B O;; <=< r   B G>?> b   B E@A@ m   B CBB �CC  s e t e n v   D I S P L A Y  A o   C D���� 0 
displaynum 
DisplayNum? o      ���� 0 
displaycmd 
DisplayCmd= DED r   H MFGF b   H KHIH m   H IJJ �KK  s e t e n v   I D L _ D I R  I o   I J���� 0 idldirfolder idlDirFolderG o      ���� 0 	idldircmd 	IDLDirCmdE L��L l  N N��MN��  M 	 csh   N �OO  c s h��  : PQP E   R URSR o   R S���� 0 shellenv  S m   S TTT �UU  / c s hQ VWV k   X gXX YZY r   X ][\[ b   X []^] m   X Y__ �``  s e t e n v   D I S P L A Y  ^ o   Y Z���� 0 
displaynum 
DisplayNum\ o      ���� 0 
displaycmd 
DisplayCmdZ aba r   ^ ecdc b   ^ cefe m   ^ agg �hh  s e t e n v   I D L _ D I R  f o   a b���� 0 idldirfolder idlDirFolderd o      ���� 0 	idldircmd 	IDLDirCmdb i��i l  f f��jk��  j  
 sh or zsh   k �ll    s h   o r   z s h��  W mnm G   j {opo l  j oq����q E   j orsr o   j k���� 0 shellenv  s m   k ntt �uu  / s h��  ��  p l  r wv����v E   r wwxw o   r s���� 0 shellenv  x m   s vyy �zz  / z s h��  ��  n {��{ k   ~ �|| }~} r   ~ �� b   ~ ���� m   ~ ��� ���  e x p o r t   D I S P L A Y =� o   � ����� 0 
displaynum 
DisplayNum� o      ���� 0 
displaycmd 
DisplayCmd~ ���� r   � ���� b   � ���� m   � ��� ���  e x p o r t   I D L _ D I R =� o   � ����� 0 idldirfolder idlDirFolder� o      ���� 0 	idldircmd 	IDLDirCmd��  ��   l  � ����� k   � ��� ��� r   � ���� m   � ��� ���  / b i n / b a s h   - c  � o      ���� 0 shellcmd shellCmd� ��� r   � ���� b   � ���� m   � ��� ���  e x p o r t   D I S P L A Y =� o   � ����� 0 
displaynum 
DisplayNum� o      ���� 0 
displaycmd 
DisplayCmd� ���� r   � ���� b   � ���� m   � ��� ���  e x p o r t   I D L _ D I R =� o   � ����� 0 idldirfolder idlDirFolder� o      �� 0 	idldircmd 	IDLDirCmd��  � , & Default to use bash if not recognized   � ��� L   D e f a u l t   t o   u s e   b a s h   i f   n o t   r e c o g n i z e d ��� l  � ��~�}�|�~  �}  �|  � ��� l  � ��{���{  � C = Create the setup command.  Only add in the user's init setup   � ��� z   C r e a t e   t h e   s e t u p   c o m m a n d .     O n l y   a d d   i n   t h e   u s e r ' s   i n i t   s e t u p� ��� l  � ��z���z  �   if it was present   � ��� $   i f   i t   w a s   p r e s e n t� ��� l  � ��y�x�w�y  �x  �w  � ��� r   � ���� o   � ��v�v 0 	idldircmd 	IDLDirCmd� o      �u�u 0 fullsetupcmd fullSetupCmd� ��� l  � ��t�s�r�t  �s  �r  � ��� Z   � ����q�� G   � ���� D   � ���� o   � ��p�p 0 shellenv  � m   � ��� ���  / c s h� D   � ���� o   � ��o�o 0 shellenv  � m   � ��� ��� 
 / t c s h� r   � ���� b   � ���� m   � ��� ��� . u n s e t e n v   G L _ R E S O U R C E S ;  � o   � ��n�n 0 fullsetupcmd fullSetupCmd� o      �m�m 0 fullsetupcmd fullSetupCmd�q  � r   � ���� b   � ���� m   � ��� ��� ( u n s e t   G L _ R E S O U R C E S ;  � o   � ��l�l 0 fullsetupcmd fullSetupCmd� o      �k�k 0 fullsetupcmd fullSetupCmd� ��� l  � ��j�i�h�j  �i  �h  � ��g� l  � ��f�e�d�f  �e  �d  �g  ��   � �c�����c  � �b�a�`�b 0 	launchx11 	LaunchX11�a 0 getdisplaynum GetDisplayNum�` $0 environmentsetup EnvironmentSetup� �_ ��^�]���\�_ 0 	launchx11 	LaunchX11�^  �]  � �[�Z�Y�X�W�V�[ 0 	osversion 	OSVersion�Z 0 userid userID�Y 0 
displaydir 
Displaydir�X 0 
displaynum 
DisplayNum�W 0 results  �V 0 x  �  ��U�Ts�S�R�Q'-�P2\�O�N�M�L�K�J�I��Hq�G
�U .sysoexecTEXT���     TEXT�T 0 getdisplaynum GetDisplayNum
�S 
prcs
�R 
pnam
�Q 
list
�P .ascrnoop****      � ****�O 
�N 
bool�M  �L  
�K .sysodelanull��� ��� nmbr
�J 
errn�I�� �F�E�D
�F 
errn�E��D  
�H .miscactvnull��� ��� obj 
�G .sysodlogaskr        TEXT�\ ��j E�O�j E�O�%E�O*j+ E�O� �*�-�,�&� v�j O�E�O TkE�O 3h��
 ���& �j E�W X  hO�kE�Okj [OY��O�� )a a lhY hOPW X  *j Oa j Y hUOj� �C��B�A���@�C 0 getdisplaynum GetDisplayNum�B  �A  � �?�>�=�<�;�? 0 
displayenv 
displayEnv�> 0 userid userID�= 0 
displaydir 
Displaydir�< 0 	socketcmd 	SocketCmd�; 0 
displaynum 
DisplayNum� ��:������
�: .sysoexecTEXT���     TEXT�@ 6�j E�O��  '�j E�O�%�%E�O�%�%E�O�j E�O�Y �� �9��8�7���6�9 $0 environmentsetup EnvironmentSetup�8 �5��5 �  �4�4 0 idldirfolder idlDirFolder�7  � �3�2�1�0�3 0 idldirfolder idlDirFolder�2 0 	idldircmd 	IDLDirCmd�1 0 
displaynum 
DisplayNum�0 0 shellenv  � �/�.�-�,�+�*%-7BJT_gty�)���������
�/ 
TEXT�. 0 fullsetupcmd fullSetupCmd�- 0 
displaycmd 
DisplayCmd�, 0 shellcmd shellCmd�+ 0 getdisplaynum GetDisplayNum
�* .sysoexecTEXT���     TEXT
�) 
bool�6 ��E�O�E�O�E�O�E�O*j+ E�O�j E�O��%E�O�� �%E�O�%E�OPY k�� �%E�O��%E�OPY U�� �%E�Oa �%E�OPY =�a 
 �a a & a �%E�Oa �%E�Y a E�Oa �%E�Oa �%E�O�E�O�a 
 �a a & a �%E�Y 	a �%E�OP��   � ��� | u n s e t   G L _ R E S O U R C E S ;   e x p o r t   I D L _ D I R = / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / � ��� t e x p o r t   D I S P L A Y = / t m p / l a u n c h - 9 W T C 0 z / o r g . m a c o s f o r g e . x q u a r t z : 0 � ���   / b i n / b a s h   - i   - c   � ���� / b i n / b a s h   - i   - c   ' u n s e t   G L _ R E S O U R C E S ;   e x p o r t   I D L _ D I R = / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / ;   e x p o r t   D I S P L A Y = / t m p / l a u n c h - 9 W T C 0 z / o r g . m a c o s f o r g e . x q u a r t z : 0 ;   c d     ' / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / b i n ' ;   / u s r / X 1 1 R 6 / b i n / x t e r m   - e   / A p p l i c a t i o n s / e x e l i s / i d l 8 5 / b i n / i d l   - v m = / U s e r s / a n d r e a s / D o w n l o a d s / E n M A P - B o x / I D L V M O S X 6 4 / / . . / I D L V M C r o s s P l a t f o r m / e n m a p B o x V M L a u n c h e r . s a v '   >   / d e v / n u l l     2 > & 1   &   � ���  ��  ascr  ��ޭ